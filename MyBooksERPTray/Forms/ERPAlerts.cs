﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERPTray
{
    public partial class ERPAlerts : DevComponents.DotNetBar.Balloon 
    {
        public string PsServer;
        Random random = new Random();

        public ERPAlerts()
        {
            InitializeComponent();
        }

        private void SmartTradeAlerts_CloseButtonClick(object sender, EventArgs e)
        {
            this.AutoClose = true;
            this.AutoCloseTimeOut = 1;
        }

        private void tmrScroll_Tick(object sender, EventArgs e)
        {
            try
            {
                tmrScroll.Stop();
                tmrScroll.Enabled = false;
                this.AutoClose = true;
                this.AutoCloseTimeOut = 20;
            }
            catch (Exception)
            {
                return;
            }
        }

        private void pnlAlert_Scroll(object sender, ScrollEventArgs e)
        {
            lblData.Refresh();
        }

        private void lblData_TextChanged(object sender, EventArgs e)
        {
            lblData.Refresh();
        }

        private void SmartTradeAlerts_Load(object sender, EventArgs e)
        {
            try
            {
                this.lblData.Text = string.Empty;
                this.AutoClose = true;
                this.AutoCloseTimeOut = 20;
                this.Fill_SummaryGrid();
            }
            catch (Exception ex)
            {
                lblCount.Text = "";
                lblData.Text = "";
            }
        }

        private void Fill_SummaryGrid()
        {
            int intCount = 0;
            bool blnDataFound = false;
            StringBuilder strLabel = new StringBuilder();
            StringBuilder strCount = new StringBuilder();
            clsMindSoftERPAlerts objclsMindSoftERPAlerts = new clsMindSoftERPAlerts(PsServer);

        
            intCount = objclsMindSoftERPAlerts.GetSalesQuotationDueDate();

            if (intCount > 0)
            {
                strCount.Append(intCount.ToString());
                strCount.Append(Environment.NewLine);
                strCount.Append(Environment.NewLine);
                strLabel.Append(" Sales quotation due date reached");
                strLabel.Append(Environment.NewLine);
                strLabel.Append(Environment.NewLine);
                blnDataFound = true;
            }

             intCount = objclsMindSoftERPAlerts.GetSalesOrderDueDate();

            if (intCount > 0)
            {
                strCount.Append(intCount.ToString());
                strCount.Append(Environment.NewLine);
                strCount.Append(Environment.NewLine);
                strLabel.Append(" Sales order due date reached");
                strLabel.Append(Environment.NewLine);
                strLabel.Append(Environment.NewLine);
                blnDataFound = true;
            }

            intCount = objclsMindSoftERPAlerts.GetSalesInvoiceDueDate();

            if (intCount > 0)
            {
                strCount.Append(intCount.ToString());
                strCount.Append(Environment.NewLine);
                strCount.Append(Environment.NewLine);
                strLabel.Append(" Sales invoice due date reached");
                strLabel.Append(Environment.NewLine);
                strLabel.Append(Environment.NewLine);
                blnDataFound = true;
            }

            intCount = objclsMindSoftERPAlerts.GetDeliveryNote();

            if (intCount > 0)
            {
                strCount.Append(intCount.ToString());
                strCount.Append(Environment.NewLine);
                strCount.Append(Environment.NewLine);
                strLabel.Append(" Delivery note generated");
                strLabel.Append(Environment.NewLine);
                strLabel.Append(Environment.NewLine);
                blnDataFound = true;
            }

            intCount = objclsMindSoftERPAlerts.GetReOrderLevelReached();

            if (intCount > 0)
            {
                strCount.Append(intCount.ToString());
                strCount.Append(Environment.NewLine);
                strCount.Append(Environment.NewLine);
                strLabel.Append(" Items reorder level reached");
                strLabel.Append(Environment.NewLine);
                strLabel.Append(Environment.NewLine);
                blnDataFound = true;
            }

 

            intCount = objclsMindSoftERPAlerts.GetPurchaseQuotationDueDate();

            if (intCount > 0)
            {
                strCount.Append(intCount.ToString());
                strCount.Append(Environment.NewLine);
                strCount.Append(Environment.NewLine);
                strLabel.Append(" Purchase quotation due date reached");
                strLabel.Append(Environment.NewLine);
                strLabel.Append(Environment.NewLine);
                blnDataFound = true;
            }

            intCount = objclsMindSoftERPAlerts.GetPurchaseOrderDueDate();

            if (intCount > 0)
            {
                strCount.Append(intCount.ToString());
                strCount.Append(Environment.NewLine);
                strCount.Append(Environment.NewLine);
                strLabel.Append(" Purchase order due date reached");
                strLabel.Append(Environment.NewLine);
                strLabel.Append(Environment.NewLine);
                blnDataFound = true;
            }

            intCount = objclsMindSoftERPAlerts.GetDocumentExpiry();

            if (intCount > 0)
            {
                strCount.Append(intCount.ToString());
                strCount.Append(Environment.NewLine);
                strCount.Append(Environment.NewLine);
                strLabel.Append(" Document expired");
                strLabel.Append(Environment.NewLine);
                strLabel.Append(Environment.NewLine);
                blnDataFound = true;
            }

            this.lblData.Text = strLabel.ToString();
            this.lblCount.Text = strCount.ToString();

            if (!blnDataFound)
            {
                lblCount.Visible = false;
                lblData.Visible = false;
                lblNoAlerts.Visible = true;
            }
            else
            {
                lblCount.Visible = true;
                lblData.Visible = true;
                lblNoAlerts.Visible = false;
            }
        }

        private void pnlAlert_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
