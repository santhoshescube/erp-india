﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using DevComponents.DotNetBar;
using System.Collections;

namespace MyBooksERPTray
{
    public partial class frmMain : System.Windows.Forms.Form
    {
       // thasni
        private string mServerName;
        private string strUrlPath;
        private string IsServer;

        private LogWriter Logger;
        private clsConnection mObjMainCon;
        private clsERPAlerts objERPAlerts;
        private ERPAlerts m_MindSoftERPAlerts = null;

        private Machine MachineType
        {
            get { return this.IsServer == "0" ? Machine.Server : Machine.Client; }
            set { this.IsServer = (value == Machine.Client) ? "1" : "0"; }
        }

        public frmMain()
        {
            InitializeComponent();

            // Set Default value
            this.MachineType = Machine.Server;

            //mServerName = "DBSERVER\\DBSERVER";
            strUrlPath = "http://msg.mindsoft.ae/Updates/MsERPHBT/";

             //Set value to variable IsServer from registry
            new clsRegistry().ReadFromRegistry("SOFTWARE\\Mindsoft\\MyERP", "MsERP", out this.IsServer);

            new clsRegistry().ReadFromRegistry("SOFTWARE\\MsbErpserver", "MsbErpservername", out this.mServerName);

            mObjMainCon = new clsConnection(mServerName);
            objERPAlerts = new clsERPAlerts(mServerName);
            Logger = new LogWriter(Application.StartupPath);
        }

        #region Main()
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (!ThisProcessAlreadyExists())
            {
                Application.Run(new frmMain());
            }
        }
        #endregion

        #region Tray
        private static bool ThisProcessAlreadyExists()
        {
            try
            {
                int count = 0;
                Process thisProcess = Process.GetCurrentProcess();

                Process[] pl = Process.GetProcessesByName("MindSoftERPTray");

                foreach (Process p in pl)
                {
                    if (thisProcess.ProcessName == p.ProcessName)
                    {
                        count++;
                    }
                }
                if (count > 1)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void ShowLoadAlert()
        {
            m_MindSoftERPAlerts = new ERPAlerts();
            m_MindSoftERPAlerts.PsServer = mServerName;
            Rectangle r = Screen.GetWorkingArea(this);
            m_MindSoftERPAlerts.Location = new Point(r.Right - m_MindSoftERPAlerts.Width, r.Bottom - m_MindSoftERPAlerts.Height);
            m_MindSoftERPAlerts.AutoClose = true;
            m_MindSoftERPAlerts.AutoCloseTimeOut = 15;
            m_MindSoftERPAlerts.AlertAnimation = eAlertAnimation.BottomToTop;
            m_MindSoftERPAlerts.AlertAnimationDuration = 100;
            m_MindSoftERPAlerts.Show(false);
        }
        #endregion
        
        private bool CheckDbConnection()
        {
            try
            {
                string strDate = mObjMainCon.GetSysDate();
                return true;
            }
            catch (Exception Ex)
            {
                Logger.WriteLog("Err on Tray testconnection failed." + Ex.Message.ToString());
                return false;
            }
        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            if (m_MindSoftERPAlerts != null)
            {
                m_MindSoftERPAlerts.Close();
                m_MindSoftERPAlerts.Dispose();
                m_MindSoftERPAlerts = null;
            }
            ShowLoadAlert();
        }

        private void tmrDay_Tick(object sender, EventArgs e)
        {
            ShowLoadAlert();
            PostPDCVoucher();
            tmrDay.Enabled = true;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            if (CheckDbConnection() == false)
            {
                Application.Exit();
            }
            else if (this.MachineType == Machine.Server)
            {
                tmrDay.Enabled = true;
            }

            ShowLoadAlert();
        }
        private void PostPDCVoucher()
        {
            objERPAlerts.PostPDCVoucher();
        }        
    }
}
