﻿namespace MyBooksERPTray
{
    partial class ERPAlerts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlAlert = new System.Windows.Forms.Panel();
            this.lblCount = new System.Windows.Forms.Label();
            this.lblData = new System.Windows.Forms.Label();
            this.lblNoAlerts = new DevComponents.DotNetBar.LabelX();
            this.tmrScroll = new System.Windows.Forms.Timer(this.components);
            this.pnlAlert.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlAlert
            // 
            this.pnlAlert.AutoScroll = true;
            this.pnlAlert.BackColor = System.Drawing.Color.Transparent;
            this.pnlAlert.Controls.Add(this.lblCount);
            this.pnlAlert.Controls.Add(this.lblData);
            this.pnlAlert.Controls.Add(this.lblNoAlerts);
            this.pnlAlert.Location = new System.Drawing.Point(12, 61);
            this.pnlAlert.Name = "pnlAlert";
            this.pnlAlert.Size = new System.Drawing.Size(229, 161);
            this.pnlAlert.TabIndex = 0;
            this.pnlAlert.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlAlert_Paint);
            this.pnlAlert.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlAlert_Scroll);
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.BackColor = System.Drawing.Color.Transparent;
            this.lblCount.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.ForeColor = System.Drawing.Color.Black;
            this.lblCount.Location = new System.Drawing.Point(6, 15);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(28, 13);
            this.lblCount.TabIndex = 8;
            this.lblCount.Text = "100";
            this.lblCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.BackColor = System.Drawing.Color.Transparent;
            this.lblData.ForeColor = System.Drawing.Color.Black;
            this.lblData.Location = new System.Drawing.Point(34, 15);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(223, 13);
            this.lblData.TabIndex = 7;
            this.lblData.Text = "Purchase Quotations Submitted For Approval";
            this.lblData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNoAlerts
            // 
            this.lblNoAlerts.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblNoAlerts.BackgroundStyle.Class = "";
            this.lblNoAlerts.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblNoAlerts.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoAlerts.ForeColor = System.Drawing.Color.Black;
            this.lblNoAlerts.Location = new System.Drawing.Point(65, 72);
            this.lblNoAlerts.Name = "lblNoAlerts";
            this.lblNoAlerts.Size = new System.Drawing.Size(135, 23);
            this.lblNoAlerts.TabIndex = 5;
            this.lblNoAlerts.Text = "No Alerts Today";
            this.lblNoAlerts.Visible = false;
            // 
            // tmrScroll
            // 
            this.tmrScroll.Interval = 1000;
            this.tmrScroll.Tick += new System.EventHandler(this.tmrScroll_Tick);
            // 
            // SmartTradeAlerts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MyBooksERPTray.Properties.Resources.Tray_Box;
            this.ClientSize = new System.Drawing.Size(253, 242);
            this.Controls.Add(this.pnlAlert);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "SmartTradeAlerts";
            this.TipPosition = DevComponents.DotNetBar.eTipPosition.Bottom;
            this.Load += new System.EventHandler(this.SmartTradeAlerts_Load);
            this.CloseButtonClick += new System.EventHandler(this.SmartTradeAlerts_CloseButtonClick);
            this.pnlAlert.ResumeLayout(false);
            this.pnlAlert.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlAlert;
        private DevComponents.DotNetBar.LabelX lblNoAlerts;
        private System.Windows.Forms.Timer tmrScroll;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.Label lblCount;

    }
}