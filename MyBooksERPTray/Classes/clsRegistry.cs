﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32; 

   public class clsRegistry
    {
        public bool ReadFromRegistry(string sMainKey,string sKeyToread,out string rVal)
        {
            rVal = "";
            try
            {
                RegistryKey rKey = Registry.LocalMachine;
                RegistryKey subKey = rKey.OpenSubKey(sMainKey, RegistryKeyPermissionCheck.ReadSubTree, System.Security.AccessControl.RegistryRights.ReadKey);
                if (subKey != null)
                {
                    rVal = subKey.GetValue(sKeyToread).ToString();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
    

