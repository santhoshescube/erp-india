﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;

namespace MyBooksERPTray
{
    public class clsMindSoftERPAlerts
    {
        clsConnection MobjClsConnection;
        ArrayList prmMindSoftERPAlerts;
        string mStrServerName;

        public clsMindSoftERPAlerts(string strServerName)
        {
            MobjClsConnection = new clsConnection(strServerName);
            mStrServerName = strServerName;
        }

        public int GetSalesQuotationSubmittedForApproval()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 4));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationStatusType.SQuotationSubmittedForApproval));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetSalesQuotationApprovedOrSubmitted()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 5));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationStatusType.SQuotationApproved));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID1", (int)OperationStatusType.SQuotationSubmitted));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetSalesQuotationCancelled()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 6));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationStatusType.SQuotationCancelled));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetSalesQuotationDueDate()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 7));            
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationStatusType.SQuotationOpen));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID1", (int)OperationStatusType.SQuotationApproved));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID2", (int)OperationStatusType.SQuotationSubmittedForApproval));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetSalesOrderCreated()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 8));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationStatusType.SOrderOpen));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetSalesOrderAdvancePaid()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 9));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationType.SalesOrder));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID1", (int)PaymentTypes.AdvancePayment));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetSalesOrderSubmittedForApproval()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 10));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationStatusType.SOrderSubmittedForApproval));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetSalesOrderApprovedOrSubmitted()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 11));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationStatusType.SOrderApproved));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID1", (int)OperationStatusType.SOrderSubmitted));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetSalesOrderRejected()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 12));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationStatusType.SOrderRejected));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetSalesOrderDueDate()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 13));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationStatusType.SOrderOpen));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID1", (int)OperationStatusType.SOrderSubmittedForApproval));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID2", (int)OperationStatusType.SOrderApproved));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetSalesInvoiceDueDate()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 14));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationStatusType.SInvoiceOpen));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID1", (int)OperationStatusType.SScheduledForDelivery));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetMaterialIssue()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 15));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationOrderType.DNOTDMR));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetReceipt()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 17));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationType.Receipt));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetDeliveryNote()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 18));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationOrderType.DNOTSalesInvoice));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetReOrderLevelReached()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 19));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetPurchaseOrderApprovedOrSubmitted()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 20));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationStatusType.POrderApproved));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID1", (int)OperationStatusType.POrderSubmitted));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetRFQDueDate()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 21));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)RFQStatus.Opened));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID1", (int)RFQStatus.SubmittedForApproval));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID2", (int)RFQStatus.Approved));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetPurchaseQuotationDueDate()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 22));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationStatusType.PQuotationOpened));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID1", (int)OperationStatusType.PQuotationApproved));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID2", (int)OperationStatusType.PQuotationSubmittedForApproval));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetPurchaseOrderDueDate()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 23));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)OperationStatusType.POrderOpened));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID1", (int)OperationStatusType.POrderApproved));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID2", (int)OperationStatusType.POrderSubmittedForApproval));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }

        public int GetDocumentExpiry()
        {
            prmMindSoftERPAlerts = new ArrayList();
            prmMindSoftERPAlerts.Add(new SqlParameter("@Mode", 24));
            prmMindSoftERPAlerts.Add(new SqlParameter("@StatusID", (int)AlertSettingsTypes.DocumentExpiry));
            DataTable dt = MobjClsConnection.ExecuteDataTable("spAlertsInTray", prmMindSoftERPAlerts);
            if (dt.Rows.Count > 0)
                return Convert.ToInt32(dt.Rows[0]["Total"]);
            return 0;
        }        
    }
}
