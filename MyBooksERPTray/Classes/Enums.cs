﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBooksERPTray
{
    public enum Machine
    {
        Server = 0,
        Client = 1
    }

    public enum AlertSettingsTypes
    {
        DocumentExpiry = 28
    }

    public enum OperationOrderType
    {
        DNOTDMR = 11,
        DNOTSalesInvoice = 8
    }

    public enum OperationStatusType
    {
        JONew = 46,
        SiteVisitRequested = 5,
        JOSiteVisitCompleted = 71,
        SQuotationSubmittedForApproval = 56,
        SQuotationApproved = 54,
        SQuotationSubmitted = 95,
        SQuotationCancelled = 53,
        SQuotationOpen = 51,
        SOrderRejected = 61,
        SOrderOpen = 57,
        SOrderSubmittedForApproval = 62,
        SOrderApproved = 60,
        SOrderSubmitted = 96,
        SInvoiceOpen = 63,
        SScheduledForDelivery = 40,
        JOMovedToStock = 75,
        POrderApproved = 18,
        POrderSubmitted = 94,
        PQuotationOpened = 11,
        PQuotationApproved = 13,
        PQuotationSubmittedForApproval = 26,
        POrderOpened = 16,
        POrderSubmittedForApproval = 27
    }

    public enum OperationType
    {
        SalesOrder = 8,
        Receipt = 21
    }

    public enum PaymentTypes
    {
        AdvancePayment = 1,
        InvoicePayment = 2,
        DirectExpense = 3,
        InvoiceExpense = 4
    }

    public enum RFQStatus
    {
        Opened = 103,
        SubmittedForApproval = 105,
        Approved = 108
    }
}
