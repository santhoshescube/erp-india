﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;

namespace MyBooksERPTray
{
    public class clsERPAlerts
    {
        clsConnection MobjClsConnection;
        ArrayList prmAlerts;
        string mStrServerName;

        public clsERPAlerts(string strServerName)
        {
            MobjClsConnection = new clsConnection(strServerName);
            mStrServerName = strServerName;
        }

        public int PostPDCVoucher()
        {
            
            prmAlerts = new ArrayList();
            prmAlerts.Add(new SqlParameter("@Mode", 12));
            object RowsAffected =MobjClsConnection.ExecutesQuery(prmAlerts,"spAccJournalRecurrenceSetup");
            return Convert.ToInt32(RowsAffected == DBNull.Value ? 0 : RowsAffected);         
            
        }

       
    }
}
