using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Data.SqlClient;
using System.Data.Common;

/// <summary>
/// Summary description for clsConnection
/// purpose : Handle Database Transactions
/// created : Bijoy George
/// Date    : 01-07-2009
/// </summary>
public class clsConnection
{
    string mConnectionString;
    SqlConnection mConnection;
    SqlCommand mCommand;

	public clsConnection(string strServer)
	{
    mConnectionString = "Data Source=" + strServer + ";Initial Catalog=PhoenixSet;uid=erpadmin;pwd=ErpMsb!2345;Connect Timeout=0;";
       // mConnectionString = "Data Source=" + strServer + ";Initial Catalog=PhoenixSetVer3;uid=msadmin;pwd=msSoft!234;Connect Timeout=0;";
        mConnection = new SqlConnection(mConnectionString);


	}
    public SqlConnection SqlConnection()
    {
        return mConnection;
    }
    public void Connect()
    {
        if (mConnection.State == ConnectionState.Broken)
        {
            mConnection.Close();
        }
        if (mConnection.State == ConnectionState.Closed)
        {
            mConnection.Open();
        }
    }

    public void Disconnect()
    {
        mConnection.Close();
    }

    public bool ExecutesCommand(string insertString)
    {
        Connect();

        using (mCommand = new SqlCommand(insertString, mConnection))
        {
            mCommand.ExecuteNonQuery();
        }

        Disconnect();
        return true;
    }
    public bool Chk_Duplicate(string SqlString)
    {
        bool bRet = false;
        Connect();
        using (mCommand = new SqlCommand(SqlString, mConnection))
        {
            using (SqlDataReader dr = mCommand.ExecuteReader(CommandBehavior.CloseConnection))
            {
                if (dr.Read() == true)
                    bRet = true;
                else
                    bRet = false;
            }
        }
        Disconnect();

        return bRet;
    }

    // Added by Ratheesh
    public object ExecuteScalar(string commandText, ArrayList parameters)
    {
        this.Connect();
        try
        {
            mCommand = new SqlCommand(commandText);
            
            foreach (SqlParameter parameter in parameters)
                mCommand.Parameters.Add(parameter);

            mCommand.CommandType = CommandType.StoredProcedure;
            mCommand.Connection = this.mConnection;
            return mCommand.ExecuteScalar();
        }
        finally
        {
            this.mCommand.Dispose();
            this.Disconnect();
        }
    }

    public object ExecuteScalar(string commandText)
    {
        this.Connect();
        try
        {
            mCommand = new SqlCommand(commandText);
            mCommand.CommandType = CommandType.StoredProcedure;
            mCommand.Connection = this.mConnection;
            return mCommand.ExecuteScalar();
        }
        finally
        {
            this.mCommand.Dispose();
            this.Disconnect();
        }
    }

    public SqlDataReader ExecutesReader(string cmdText)
    {
        Connect();
        SqlDataReader Reader;
        mCommand = new SqlCommand(cmdText, mConnection);
        mCommand.CommandType = CommandType.Text;
        Reader = mCommand.ExecuteReader(CommandBehavior.CloseConnection);
        return Reader;
    }
    public SqlDataReader ExecutesReader(string commandText, ArrayList parameters, CommandBehavior cmdBehavior)
    {
        SqlDataReader Reader;

        Connect();

        SqlCommand mCommand = new SqlCommand(commandText, mConnection);

        //SqlCmd.CommandTimeout = Settings.CommandTimeOutValue;

        mCommand.CommandType = CommandType.StoredProcedure;

        if (parameters != null && parameters.Count != 0)
        {
            foreach (SqlParameter Param in parameters)
            {
                mCommand.Parameters.Add(Param);
            }
        }

        //Try-catch blocks are not needed here since the exceptions should be handled in the calling code

        Reader = mCommand.ExecuteReader(cmdBehavior);

        return Reader;
    }

    public bool ExecutesQuery(ArrayList parameters,string cmdText)
    {
        Connect();
        mCommand = new SqlCommand(cmdText, mConnection);
        mCommand.CommandType = CommandType.StoredProcedure;
        if (parameters.Count != 0)
        {
            foreach (SqlParameter param in parameters)
                mCommand.Parameters.Add(param);
        }

        int rowsaff = mCommand.ExecuteNonQuery();
        ArrayList resultparametes = new ArrayList(mCommand.Parameters);

        return true;
    }
    public bool ExecutesQuery(ArrayList parameters, string cmdText,out long recpid)
    {
        recpid = 0;
        Connect();

        mCommand = new SqlCommand(cmdText, mConnection);
        mCommand.CommandType = CommandType.StoredProcedure;
        if (parameters.Count != 0)
        {
            foreach (SqlParameter param in parameters)
                mCommand.Parameters.Add(param);
        }

        int rowsaff = mCommand.ExecuteNonQuery();
        ArrayList resultparametes = new ArrayList(mCommand.Parameters);
        foreach (SqlParameter param in resultparametes)
        {
            if (param.Direction == ParameterDirection.ReturnValue)
                recpid = Convert.ToInt64(param.Value);
        }

        Disconnect();
        return true;

    }
    public DataSet FillDataSet(string commandText)
    {
        DataSet ResultSet;
        Connect();
        mCommand = new SqlCommand(commandText, mConnection);
        SqlDataAdapter oDa = new SqlDataAdapter(mCommand);
        mCommand.CommandType = CommandType.Text;
        ResultSet = new DataSet();

        oDa.Fill(ResultSet);
        oDa.Dispose();

        Disconnect();

        return ResultSet;
    }
    public DataSet FillDataSet(string commandText, ArrayList parameters)
    {
        DataSet ResultSet;
        Connect();
        SqlDataAdapter SqlDa = new SqlDataAdapter(commandText, mConnection);

        SqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;

        if (parameters != null && parameters.Count != 0)
        {
            foreach (SqlParameter Param in parameters)
            {
                SqlDa.SelectCommand.Parameters.Add(Param);
            }
        }

        ResultSet = new DataSet();
        //Try-catch blocks are not needed here since the exceptions should be handled in the calling code
        SqlDa.Fill(ResultSet);
        SqlDa.Dispose();

        Disconnect();
        return ResultSet;
    }

    public DataSet FillDataSet(string commandText, ArrayList parameters, out ArrayList resultParameters)
    {
        DataSet ResultSet;
        Connect();
        SqlDataAdapter SqlDa = new SqlDataAdapter(commandText, mConnection);

        SqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;

        if (parameters != null && parameters.Count != 0)
        {
            foreach (SqlParameter Param in parameters)
            {
                SqlDa.SelectCommand.Parameters.Add(Param);
            }
        }

        ResultSet = new DataSet();
        //Try-catch blocks are not needed here since the exceptions should be handled in the calling code
        SqlDa.Fill(ResultSet);
        resultParameters = new ArrayList(SqlDa.SelectCommand.Parameters);

        SqlDa.Dispose();
        Disconnect();
        return ResultSet;
    }
    public DataTable ExecuteDataTable(string command, ArrayList parameters)
    {
        DataTable dtResult = new DataTable();
        Connect();
        SqlCommand cmd = new SqlCommand(command,SqlConnection());
        cmd.CommandType = CommandType.StoredProcedure;


        foreach (DbParameter param in parameters)
        {
            cmd.Parameters.Add(param);
        }

        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        sda.Fill(dtResult);
        Disconnect();
        return dtResult;
    }

    public DataSet ExecuteDataSet(string command, ArrayList parameters)
    {
        DataSet dtResult = new DataSet();
        Connect();
        SqlCommand cmd = new SqlCommand(command, SqlConnection());
        cmd.CommandType = CommandType.StoredProcedure;


        foreach (DbParameter param in parameters)
            cmd.Parameters.Add(param);

        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        sda.Fill(dtResult);
        Disconnect();
        return dtResult;
    }

    //get the current date from server by bijoy
    public string GetSysDate()
    {
        string strDate = "";
        Connect();
        SqlCommand cmdDate = new SqlCommand("select convert(nvarchar,getdate(),103)sdate", mConnection);
        SqlDataReader drDate = cmdDate.ExecuteReader();
        if (drDate.Read() == true)
            strDate = Convert.ToString(drDate[0]);
        else
            strDate = DateTime.Now.ToShortDateString();
        drDate.Close();

        Disconnect();

        return strDate;
    }

}
