﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using ProjectSettings;

namespace MyBooksERP
{
    public partial class FrmLogin : DevComponents.DotNetBar.Office2007Form 
    {
        clsBLLLogin MobjClsBLLLogin;
        clsRegistry objRegistry;
        bool blnIsFromFormClose = true;

        public FrmLogin()
        {
            InitializeComponent();

            MobjClsBLLLogin = new clsBLLLogin();
            objRegistry = new clsRegistry();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            txtUserName.Focus();
        }

        private void SetPassword()
        {
            try
            {
                MobjClsBLLLogin.UpdateRemember(txtUserName.Text.Trim(),chkRemember.Checked);
                if (txtUserName.Text.Trim() != "" && chkRemember.Checked)
                {
                    objRegistry.WriteToRegistry("SOFTWARE\\Mindsoft\\MsBLogin", "MsBLogin", txtUserName.Text.Trim(), txtUserName.Text.Trim());
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            blnIsFromFormClose = false;
            try
            {
                if (ValidateForm())
                {
                    string strPassword = clsBLLCommonUtility.Encrypt(txtPassword.Text.Trim(), ClsCommonSettings.strEncryptionKey);
                    DataTable dtUserDetails = MobjClsBLLLogin.GetUserInfo(txtUserName.Text, strPassword);

                    if (dtUserDetails.Rows.Count > 0)
                    {
                        SetPassword();
                        ClsCommonSettings.UserID = Convert.ToInt32(dtUserDetails.Rows[0]["UserID"]);
                        ClsCommonSettings.RoleID = Convert.ToInt32(dtUserDetails.Rows[0]["RoleID"]);
                        ClsCommonSettings.strUserName = Convert.ToString(dtUserDetails.Rows[0]["UserName"]);
                        ClsCommonSettings.strEmployeeName = Convert.ToString(dtUserDetails.Rows[0]["EmployeeName"]);
                        ClsCommonSettings.intEmployeeID = Convert.ToInt32(dtUserDetails.Rows[0]["EmployeeID"]);
                        ClsCommonSettings.intDepartmentID = Convert.ToInt32(dtUserDetails.Rows[0]["DepartmentID"]);
                        ClsCommonSettings.CompanyID = Convert.ToInt32(dtUserDetails.Rows[0]["CompanyID"]);
                                                
                        SetCommonSettingsCompanyInfo();
                        SetCommonSettingsConfigurationSettingsInfo();

                        clsBLLCommonUtility objCommonUtility = new clsBLLCommonUtility();
                        DataTable datCurrencyDetails = objCommonUtility.FillCombos(new string[] { "CR.Scale", "CompanyMaster CM  Inner Join CurrencyReference CR On CM.CurrencyId = CR.CurrencyID", "CM.CompanyID = " + ClsCommonSettings.CompanyID });
                        
                        if (datCurrencyDetails.Rows.Count > 0)
                            ClsCommonSettings.Scale = datCurrencyDetails.Rows[0]["Scale"].ToInt32();

                        ClsMainSettings.blnLoginStatus = true;
                        this.Close();                        
                    }
                    else
                    {
                        ClsMainSettings.blnLoginStatus = false;
                        lblLoginStatus.Text = "Invalid User Try Again";
                        txtUserName.Focus();
                        Timerlogin.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show(ex.Message);
            }
        }

        private void SetCommonSettingsCompanyInfo()
        {
            DataTable dtCompanyInfo = new DataTable();

            if(ClsCommonSettings.RoleID==1 || ClsCommonSettings.RoleID == 2)
             dtCompanyInfo= MobjClsBLLLogin.GetTopMostCompany();
            else
                dtCompanyInfo = MobjClsBLLLogin.GetUserCompany();

            if (dtCompanyInfo.Rows.Count > 0)
            {
                ClsCommonSettings.CompanyID = Convert.ToInt32(dtCompanyInfo.Rows[0]["CompanyID"]);
                ClsCommonSettings.strCompanyName = Convert.ToString(dtCompanyInfo.Rows[0]["CompanyName"]);
                SetCommonSettingsCompanySettingsInfo();
            }
        }

        public void SetCommonSettingsCompanySettingsInfo()
        {
            DataTable dtCompanySettingsInfo = MobjClsBLLLogin.GetCompanySettings(ClsCommonSettings.CompanyID);

            string strValue = "";

            ClsCommonSettings.strSQPrefix = GetCompanySettingsValue("Sales Quotation", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnSQAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Sales Quotation", "Autogenerate", dtCompanySettingsInfo));
            ClsCommonSettings.strSQDueDateLimit = GetCompanySettingsValue("Sales Quotation", "Due Date Limit", dtCompanySettingsInfo);
            
            ClsCommonSettings.strSOPrefix = GetCompanySettingsValue("Sales Order", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnSOAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Sales Order", "Autogenerate", dtCompanySettingsInfo));
            ClsCommonSettings.strSODueDateLimit = GetCompanySettingsValue("Sales Order", "Due Date Limit", dtCompanySettingsInfo);
            
            ClsCommonSettings.strSIPrefix = GetCompanySettingsValue("Sales Invoice", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnSIAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Sales Invoice", "Autogenerate", dtCompanySettingsInfo));
            ClsCommonSettings.strSIDueDateLimit = GetCompanySettingsValue("Sales Invoice", "Due Date Limit", dtCompanySettingsInfo);
            
            ClsCommonSettings.strPOSPrefix = GetCompanySettingsValue("POS", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnPOSAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("POS", "Autogenerate", dtCompanySettingsInfo));
            
            ClsCommonSettings.strSRPrefix = GetCompanySettingsValue("Sales Return", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnSRAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Sales Return", "Autogenerate", dtCompanySettingsInfo));

            ClsCommonSettings.PIPrefix = GetCompanySettingsValue("Purchase Indent", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.PIAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Indent", "Autogenerate", dtCompanySettingsInfo));
            ClsCommonSettings.PIDueDateLimit = GetCompanySettingsValue("Purchase Indent", "Due Date Limit", dtCompanySettingsInfo);
            
            ClsCommonSettings.PQPrefix = GetCompanySettingsValue("Purchase Quotation", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.PQAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Quotation", "Autogenerate", dtCompanySettingsInfo));
            ClsCommonSettings.PQDueDateLimit = GetCompanySettingsValue("Purchase Quotation", "Due Date Limit", dtCompanySettingsInfo);
            
            ClsCommonSettings.POPrefix = GetCompanySettingsValue("Purchase Order", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.POAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Order", "Autogenerate", dtCompanySettingsInfo));
            ClsCommonSettings.PODueDateLimit = GetCompanySettingsValue("Purchase Order", "Due Date Limit", dtCompanySettingsInfo);
            
            ClsCommonSettings.PINVPrefix = GetCompanySettingsValue("Purchase Invoice", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.PINVAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Invoice", "Autogenerate", dtCompanySettingsInfo));
            
            ClsCommonSettings.PGRNPrefix = GetCompanySettingsValue("Goods Receipt Note", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.PGRNAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Goods Receipt Note", "Autogenerate", dtCompanySettingsInfo));
            
            ClsCommonSettings.PRPrefix = GetCompanySettingsValue("Purchase Return", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.PRAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Return", "Autogenerate", dtCompanySettingsInfo));

            ClsCommonSettings.ItemIssuePrefix = GetCompanySettingsValue("Item Issue", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.ItemIssueAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Item Issue", "Autogenerate", dtCompanySettingsInfo));
            
            ClsCommonSettings.PaymentsPrefix = GetCompanySettingsValue("Payments", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.PaymentsAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Payments", "Autogenerate", dtCompanySettingsInfo));
            
            ClsCommonSettings.ReceiptsPrefix = GetCompanySettingsValue("Receipts", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.ReceiptsAutoGenerate = ConvertStringToBoolean(GetCompanySettingsValue("Receipts", "Autogenerate", dtCompanySettingsInfo));

            ClsCommonSettings.strEmployeeNumberPrefix = GetCompanySettingsValue("Employee Number", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnEmployeeNumberAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Employee Number", "Prefix Editable", dtCompanySettingsInfo));
            
            ClsCommonSettings.strCustomerCodePrefix = GetCompanySettingsValue("Customer Code", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnCustomerAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Customer Code", "Autogenerate", dtCompanySettingsInfo));
            
            ClsCommonSettings.strSupplierCodePrefix = GetCompanySettingsValue("Supplier Code", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnSupplierAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Supplier Code", "Autogenerate", dtCompanySettingsInfo));
            
            ClsCommonSettings.strProductCodePrefix = GetCompanySettingsValue("Product Code", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnProductCodeAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Product Code", "Autogenerate", dtCompanySettingsInfo));
            ClsCommonSettings.strProductGroupCodePrefix = GetCompanySettingsValue("Product Assembly Code", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnProductGroupCodeAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Product Assembly Code", "Autogenerate", dtCompanySettingsInfo));
            
            ClsCommonSettings.strStockAdjustmentNumberPrefix = GetCompanySettingsValue("Stock Adjustment Number", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnStockAdjustmentNumberAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Stock Adjustment Number", "Autogenerate", dtCompanySettingsInfo));
            
            ClsCommonSettings.strRFQNumber = GetCompanySettingsValue("RFQ", "Prefix", dtCompanySettingsInfo);
            ClsCommonSettings.blnRFQAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("RFQ", "Autogenerate", dtCompanySettingsInfo));
            ClsCommonSettings.strRFQDueDateLimit = GetCompanySettingsValue("RFQ", "Due Date Limit", dtCompanySettingsInfo);
            
            ClsCommonSettings.strWareHouseLocationPrefix = GetCompanySettingsValue("WareHouse Prefix", "Location", dtCompanySettingsInfo);
            ClsCommonSettings.strWareHouseRowPrefix = GetCompanySettingsValue("WareHouse Prefix", "Row", dtCompanySettingsInfo);
            ClsCommonSettings.strWareHouseBlockPrefix = GetCompanySettingsValue("WareHouse Prefix", "Block", dtCompanySettingsInfo);
            ClsCommonSettings.strWareHouseLotPrefix = GetCompanySettingsValue("WareHouse Prefix", "Lot", dtCompanySettingsInfo);

            ClsCommonSettings.PblnAutoGeneratePartyAccount = ConvertStringToBoolean(GetCompanySettingsValue("Advanced", "Auto generate Party accounts", dtCompanySettingsInfo));
            
            ClsCommonSettings.RFQApproval = ConvertStringToBoolean(GetCompanySettingsValue("RFQ", "Approval", dtCompanySettingsInfo));
            ClsCommonSettings.PQApproval = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Quotation", "Approval", dtCompanySettingsInfo));
            ClsCommonSettings.POApproval = ConvertStringToBoolean(GetCompanySettingsValue("Purchase Order", "Approval", dtCompanySettingsInfo));
            ClsCommonSettings.SQApproval = ConvertStringToBoolean(GetCompanySettingsValue("Sales Quotation", "Approval", dtCompanySettingsInfo));
            ClsCommonSettings.SOApproval = ConvertStringToBoolean(GetCompanySettingsValue("Sales Order", "Approval", dtCompanySettingsInfo));
            ClsCommonSettings.StockTransferApproval = ConvertStringToBoolean(GetCompanySettingsValue("Transfer Order", "Approval", dtCompanySettingsInfo));
        }

        private bool ConvertStringToBoolean(string strValue)
        {
            if (strValue == "Yes")
                return true;
            return false;
        }

        public void SetCommonSettingsConfigurationSettingsInfo()
        {
            DataTable dtConfigurationSettings = MobjClsBLLLogin.GetDefaultConfigurationSettings();

            string strValue = "";

            ClsCommonSettings.MessageCaption = GetConfigurationValue("MessageCaption", dtConfigurationSettings);
            ClsCommonSettings.ReportFooter = GetConfigurationValue("ReportFooter", dtConfigurationSettings);
            ClsCommonSettings.strProductSelectionURL = GetConfigurationValue("ProductSelectionURL", dtConfigurationSettings);
            ClsCommonSettings.ProductImageUrl = GetConfigurationValue("ProductImageUrl", dtConfigurationSettings);

            if (Strings.UCase(GetConfigurationValue("IncludeProduction", dtConfigurationSettings)) == "YES")
                ClsCommonSettings.PblnIncludeProduction = true;
            else
                ClsCommonSettings.PblnIncludeProduction = false;

            strValue = GetConfigurationValue("BackupInterval", dtConfigurationSettings);

            if (!string.IsNullOrEmpty(strValue))
                ClsCommonSettings.intBackupInterval = Convert.ToInt32(strValue);
            else
                ClsCommonSettings.intBackupInterval = 0;

            strValue = GetConfigurationValue("DefaultStatusBarMessageTime", dtConfigurationSettings);

            if (!string.IsNullOrEmpty(strValue))
                ClsCommonSettings.TimerInterval = Convert.ToInt32(strValue);
            else
                ClsCommonSettings.TimerInterval = 1000;

            if (Strings.UCase(GetConfigurationValue("SalaryDayIsEditable", dtConfigurationSettings)) == "YES")
                ClsCommonSettings.GlbSalaryDayIsEditable = true;
            else
                ClsCommonSettings.GlbSalaryDayIsEditable = false;

            if (Strings.UCase(GetConfigurationValue("SalarySlipIsEditable", dtConfigurationSettings)) == "YES")
                ClsCommonSettings.GlSalarySlipIsEditable = true;
            else
                ClsCommonSettings.GlSalarySlipIsEditable = false;

            if (Strings.UCase(GetConfigurationValue("24HourTimeFormat", dtConfigurationSettings)) == "YES")
                ClsCommonSettings.Glb24HourFormat = true;
            else
                ClsCommonSettings.Glb24HourFormat = false;

            if (Strings.UCase(GetConfigurationValue("DisplayLeaveSummaryInSalarySlip", dtConfigurationSettings)) == "YES")
                ClsCommonSettings.GlDisplayLeaveSummaryInSalarySlip = true;
            else
                ClsCommonSettings.GlDisplayLeaveSummaryInSalarySlip = false;

            strValue = GetConfigurationValue("LocationWise", dtConfigurationSettings);

            if (!string.IsNullOrEmpty(strValue) && strValue.ToUpper() == "YES")
                ClsCommonSettings.PblnIsLocationWise = true;
            else
                ClsCommonSettings.PblnIsLocationWise = false;

            strValue = GetConfigurationValue("CustomerLoginRequired", dtConfigurationSettings);

            if (!string.IsNullOrEmpty(strValue) && strValue.ToUpper() == "YES")
                ClsCommonSettings.PblnIsCustLoginRequired = true;
            else
                ClsCommonSettings.PblnIsCustLoginRequired = false;

            if (Strings.UCase(GetConfigurationValue("BOQEnabled", dtConfigurationSettings)) == "YES")
                ClsMainSettings.BOQEnabled = true;
            else
                ClsMainSettings.BOQEnabled = false;

            if (Strings.UCase(GetConfigurationValue("AccountDuplicationInJV", dtConfigurationSettings)) == "YES")
                ClsMainSettings.JVAccDuplication = true;
            else
                ClsMainSettings.JVAccDuplication = false;

            if (Strings.UCase(GetConfigurationValue("ExpensePosted", dtConfigurationSettings)) == "YES")
                ClsMainSettings.ExpensePosted = true;
            else
                ClsMainSettings.ExpensePosted = false;

            if (Strings.UCase(GetConfigurationValue("AutoVoucherNumber", dtConfigurationSettings)) == "YES")
                ClsMainSettings.AutoVoucherNo = true;
            else
                ClsMainSettings.AutoVoucherNo = false;

            if (Strings.UCase(GetConfigurationValue("ProductionEnabled", dtConfigurationSettings)) == "YES")
                ClsMainSettings.ProductionEnabled = true;
            else
                ClsMainSettings.ProductionEnabled = false;

            if (Strings.UCase(GetConfigurationValue("QCEnabled", dtConfigurationSettings)) == "YES")
                ClsMainSettings.QCEnabled = true;
            else
                ClsMainSettings.QCEnabled = false;

            if (Strings.UCase(GetConfigurationValue("PayrollEnabled", dtConfigurationSettings)) == "YES")
                ClsMainSettings.PayrollEnabled = true;
            else
                ClsMainSettings.PayrollEnabled = false;

            if (Strings.UCase(GetConfigurationValue("ExpenseShownInSalesBill", dtConfigurationSettings)) == "YES")
                ClsMainSettings.ExpenseShownInSalesBill = true;
            else
                ClsMainSettings.ExpenseShownInSalesBill = false;

            if (Strings.UCase(GetConfigurationValue("AlMajdal - Pre Printed Format", dtConfigurationSettings)) == "YES")
                ClsMainSettings.AlMajdalPrePrintedFormat = true;
            else
                ClsMainSettings.AlMajdalPrePrintedFormat = false;

            if (Strings.UCase(GetConfigurationValue("CommercialInvoice", dtConfigurationSettings)) == "YES")
                ClsMainSettings.CommercialInvoice = true;
            else
                ClsMainSettings.CommercialInvoice = false;
        }

        private string GetConfigurationValue(string strConfigurationItem,DataTable dtConfigurationSettings)
        {
            dtConfigurationSettings.DefaultView.RowFilter = "ConfigurationItem = '" + strConfigurationItem + "'";

            if (dtConfigurationSettings.DefaultView.ToTable().Rows.Count > 0)
                return dtConfigurationSettings.DefaultView.ToTable().Rows[0]["ConfigurationValue"].ToString();

            return "";
        }

        public string GetCompanySettingsValue(string strConfigurationItem, string strSubItem, DataTable dtCompanySettings)
        {
            dtCompanySettings.DefaultView.RowFilter = "ConfigurationItem = '" + strConfigurationItem + "' And SubItem = '" + strSubItem + "'";

            if (dtCompanySettings.DefaultView.ToTable().Rows.Count > 0)
                return dtCompanySettings.DefaultView.ToTable().Rows[0]["ConfigurationValue"].ToString();

            return "";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ClsMainSettings.blnLoginStatus = false;
        }

        private void FrmLogin_Shown(object sender, EventArgs e)
        {
            txtUserName.Select();
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                ProcessTabKey(true);
        }

        private bool ValidateForm()
        {
            if (string.IsNullOrEmpty(txtUserName.Text.Trim()))
            {
                MessageBox.Show("Please enter user name");
                txtUserName.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(txtPassword.Text.Trim()))
            {
                MessageBox.Show("Please enter password");
                txtPassword.Focus();
                return false;
            }

            return true;
        }

        private void Timerlogin_Tick(object sender, EventArgs e)
        {
             lblLoginStatus.Text = "";
             Timerlogin.Enabled = false;
        }

        private void FrmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (blnIsFromFormClose || ClsMainSettings.blnLoginStatus)
                e.Cancel = false;
            else
            {
                e.Cancel = true;
                blnIsFromFormClose = true;
            }
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {
            try
            {
                if (txtUserName.Text.Trim() != "")
                {
                    txtPassword.Text = "";
                    chkRemember.Checked = MobjClsBLLLogin.GetRememberStatus(txtUserName.Text.Trim());

                    if (chkRemember.Checked)
                    {
                        string sUsername = "";
                        objRegistry.ReadFromRegistry("SOFTWARE\\Mindsoft\\MsBLogin", txtUserName.Text.Trim(), out sUsername);

                        if (sUsername != "")
                        {
                            string strPassWord = MobjClsBLLLogin.GetPassword(sUsername);
                            txtPassword.Text = clsBLLCommonUtility.Decrypt(strPassWord, ClsCommonSettings.strEncryptionKey);
                            chkRemember.Checked = true;
                        }
                        else
                        {
                            txtPassword.Text = "";
                            chkRemember.Checked = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        private void InsertOpStockAccount()
        {
            try
            {
                MobjClsBLLLogin.InsertOpStockAccount(ClsCommonSettings.CompanyID);
            }
            catch { }
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}