﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class frmCompanySelection : DevComponents.DotNetBar.Office2007Form 
    {
        clsBLLLogin MobjClsBLLLogin;
        bool blnCancel = false;

        public frmCompanySelection()
        {
            InitializeComponent();
            MobjClsBLLLogin = new clsBLLLogin();
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            if (FormValidation())
            {
                getCompanyDetails();
                this.Close();
            }
        }

        private bool FormValidation()
        {
            if (cboCompany.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a company", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboCompany.Focus();
                return false;
            }

            if (cboFinancialYearStart.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a start date", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboFinancialYearStart.Focus();
                return false;
            }

            if (cboFinancialYearEnd.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a end date", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboFinancialYearEnd.Focus();
                return false;
            }

            if (cboFinancialYearEnd.Text.ToDateTime() < cboFinancialYearStart.Text.ToDateTime())
            {
                MessageBox.Show("Please select valid date range", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboFinancialYearEnd.Focus();
                return false;
            }

            return true;
        }

        private void getCompanyDetails()
        {
            bool isChanged = false;

            if (ClsCommonSettings.LoginCompanyID == 0)
                isChanged = true;
            else if (ClsCommonSettings.LoginCompanyID != cboCompany.SelectedValue.ToInt32())
            {
                if (blnCancel)
                {
                    if (MessageBox.Show("Do you want to go with " + cboCompany.Text.Trim() + "?", ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        isChanged = true;

                    blnCancel = false;
                }
                else
                    isChanged = true;
            }

            if (isChanged)
            {
                ClsCommonSettings.LoginCompanyID = cboCompany.SelectedValue.ToInt32();
                ClsCommonSettings.LoginCompany = cboCompany.Text.Trim();
                DataTable datTemp = MobjClsBLLLogin.GetCompanyDetails(ClsCommonSettings.LoginCompanyID);
                ClsCommonSettings.CompanyID = datTemp.Rows[0]["CompanyID"].ToInt32();
                ClsCommonSettings.CurrencyID = datTemp.Rows[0]["CurrencyID"].ToInt32();
                ClsCommonSettings.Currency = datTemp.Rows[0]["Currency"].ToString();

                if (cboFinancialYearStart.Text.Trim() != "")
                    ClsCommonSettings.FinYearStartDate = cboFinancialYearStart.Text.ToDateTime();

                if (cboFinancialYearEnd.Text.Trim() != "")
                    ClsCommonSettings.FinYearEndDate = cboFinancialYearEnd.Text.ToDateTime();
            }
        }

        private void LoadCombos(int intType)
        {
            if (intType == 0)
            {
                if (ClsCommonSettings.UserID == 1)
                    cboCompany.DataSource = new ClsCommonUtility().FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                else
                    cboCompany.DataSource = MobjClsBLLLogin.getCompanyList(ClsCommonSettings.UserID);

                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
            }

            if (intType == 1)
            {
                DataTable datTemp = MobjClsBLLLogin.GetBookKeepingYears(cboCompany.SelectedValue.ToInt32());
                DataTable datTemp1 = datTemp.Copy();

                cboFinancialYearStart.DataSource = datTemp;
                cboFinancialYearStart.DisplayMember = "BookKeepingYearStart";
                cboFinancialYearStart.DisplayMember = "BookKeepingYearStart";

                cboFinancialYearEnd.DataSource = datTemp1;
                cboFinancialYearEnd.ValueMember = "BookKeepingYearEnd";
                cboFinancialYearEnd.DisplayMember = "BookKeepingYearEnd";
            }
        }

        private void frmCompanySelection_Load(object sender, EventArgs e)
        {
            LoadCombos(0);
            btnEnter.Focus();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            blnCancel = true;
            getCompanyDetails();
            this.Close();
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedIndex >= 0)
                LoadCombos(1);
        }

        private void frmCompanySelection_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ClsCommonSettings.LoginCompanyID == 0)
                getCompanyDetails();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}