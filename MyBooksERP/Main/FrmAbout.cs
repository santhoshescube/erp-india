using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace MyBooksERP
{
	/// <summary>
	/// Summary description for frmAbout.
	/// </summary>
    public class frmAbout : DevComponents.DotNetBar.Office2007Form
    {
        internal Label lblVersion;
        internal Label Label3;
        internal Label Label2;
        internal Label lblLicency;
        internal Button btnOk;
        internal Label lblSerialNo;
        internal Label Label1;
        internal PictureBox PictureBox1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmAbout()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAbout));
            this.lblVersion = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.lblLicency = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.lblSerialNo = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(4, 110);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(73, 13);
            this.lblVersion.TabIndex = 13;
            this.lblVersion.Text = "MyBooksERP";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(4, 84);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(183, 13);
            this.Label3.TabIndex = 12;
            this.Label3.Text = "Mindsoft Technologies India Pvt. Ltd.";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(4, 204);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(332, 13);
            this.Label2.TabIndex = 11;
            this.Label2.Text = "This product is licensed under the Mindsoft Software License Terms.";
            // 
            // lblLicency
            // 
            this.lblLicency.AutoSize = true;
            this.lblLicency.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLicency.Location = new System.Drawing.Point(4, 228);
            this.lblLicency.Name = "lblLicency";
            this.lblLicency.Size = new System.Drawing.Size(130, 13);
            this.lblLicency.TabIndex = 10;
            this.lblLicency.Text = "This Product is License to ";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(327, 247);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(70, 23);
            this.btnOk.TabIndex = 9;
            this.btnOk.Text = "&OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblSerialNo
            // 
            this.lblSerialNo.AutoSize = true;
            this.lblSerialNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSerialNo.Location = new System.Drawing.Point(4, 247);
            this.lblSerialNo.Name = "lblSerialNo";
            this.lblSerialNo.Size = new System.Drawing.Size(80, 13);
            this.lblSerialNo.TabIndex = 8;
            this.lblSerialNo.Text = "Serial Number :";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(4, 133);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(381, 52);
            this.Label1.TabIndex = 7;
            this.Label1.Text = resources.GetString("Label1.Text");
            // 
            // PictureBox1
            // 
            this.PictureBox1.Image = global::MyBooksERP.Properties.Resources.About1;
            this.PictureBox1.Location = new System.Drawing.Point(0, 1);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(409, 79);
            this.PictureBox1.TabIndex = 14;
            this.PictureBox1.TabStop = false;
            // 
            // frmAbout
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(409, 284);
            this.Controls.Add(this.PictureBox1);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.lblLicency);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.lblSerialNo);
            this.Controls.Add(this.Label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAbout";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "About Mindsoft MyBooksERP";
            this.Load += new System.EventHandler(this.frmAbout_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void linkLabel1_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{

		}

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmAbout_Load(object sender, EventArgs e)
        {
            lblVersion.Text += " " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            lblSerialNo.Text += ClsMainSettings.strProductSerial;
            lblLicency.Text += "";
        }
	}
}
