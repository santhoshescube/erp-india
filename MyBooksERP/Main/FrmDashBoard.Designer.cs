﻿namespace MyBooksERP
{
    partial class FrmDashBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.Instrumentation.GaugeCircularScale gaugeCircularScale1 = new DevComponents.Instrumentation.GaugeCircularScale();
            DevComponents.Instrumentation.GradientFillColor gradientFillColor1 = new DevComponents.Instrumentation.GradientFillColor();
            DevComponents.Instrumentation.GradientFillColor gradientFillColor2 = new DevComponents.Instrumentation.GradientFillColor();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDashBoard));
            DevComponents.Instrumentation.GaugePointer gaugePointer1 = new DevComponents.Instrumentation.GaugePointer();
            DevComponents.Instrumentation.GaugeSection gaugeSection1 = new DevComponents.Instrumentation.GaugeSection();
            DevComponents.Instrumentation.GaugeCircularScale gaugeCircularScale2 = new DevComponents.Instrumentation.GaugeCircularScale();
            DevComponents.Instrumentation.GaugePointer gaugePointer2 = new DevComponents.Instrumentation.GaugePointer();
            DevComponents.Instrumentation.GaugeSection gaugeSection2 = new DevComponents.Instrumentation.GaugeSection();
            DevComponents.Instrumentation.GradientFillColor gradientFillColor3 = new DevComponents.Instrumentation.GradientFillColor();
            DevComponents.Instrumentation.GradientFillColor gradientFillColor4 = new DevComponents.Instrumentation.GradientFillColor();
            DevComponents.Instrumentation.GaugeCircularScale gaugeCircularScale3 = new DevComponents.Instrumentation.GaugeCircularScale();
            DevComponents.Instrumentation.GaugePointer gaugePointer3 = new DevComponents.Instrumentation.GaugePointer();
            DevComponents.Instrumentation.GaugeSection gaugeSection3 = new DevComponents.Instrumentation.GaugeSection();
            DevComponents.Instrumentation.GaugeCircularScale gaugeCircularScale4 = new DevComponents.Instrumentation.GaugeCircularScale();
            DevComponents.Instrumentation.GaugeSection gaugeSection4 = new DevComponents.Instrumentation.GaugeSection();
            DevComponents.Instrumentation.GradientFillColor gradientFillColor5 = new DevComponents.Instrumentation.GradientFillColor();
            DevComponents.Instrumentation.GradientFillColor gradientFillColor6 = new DevComponents.Instrumentation.GradientFillColor();
            DevComponents.Instrumentation.GaugeCircularScale gaugeCircularScale5 = new DevComponents.Instrumentation.GaugeCircularScale();
            DevComponents.Instrumentation.GaugePointer gaugePointer4 = new DevComponents.Instrumentation.GaugePointer();
            DevComponents.Instrumentation.GaugeRange gaugeRange1 = new DevComponents.Instrumentation.GaugeRange();
            DevComponents.Instrumentation.GaugeSection gaugeSection5 = new DevComponents.Instrumentation.GaugeSection();
            DevComponents.Instrumentation.GradientFillColor gradientFillColor7 = new DevComponents.Instrumentation.GradientFillColor();
            DevComponents.Instrumentation.GradientFillColor gradientFillColor8 = new DevComponents.Instrumentation.GradientFillColor();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.pnlLeft = new DevComponents.DotNetBar.PanelEx();
            this.pnlLeftMenu = new DevComponents.DotNetBar.PanelEx();
            this.btnProductMovement = new DevComponents.DotNetBar.ButtonX();
            this.btnItemwiseSalesProfit = new DevComponents.DotNetBar.ButtonX();
            this.btnPDCIssued = new DevComponents.DotNetBar.ButtonX();
            this.btnPDCReceived = new DevComponents.DotNetBar.ButtonX();
            this.btnIncomeExpense = new DevComponents.DotNetBar.ButtonX();
            this.btnPurchase = new DevComponents.DotNetBar.ButtonX();
            this.btnSales = new DevComponents.DotNetBar.ButtonX();
            this.btnReceivablesAndPayables = new DevComponents.DotNetBar.ButtonX();
            this.pnlRight = new DevComponents.DotNetBar.PanelEx();
            this.gaugeControl3 = new DevComponents.Instrumentation.GaugeControl();
            this.gaugeControl2 = new DevComponents.Instrumentation.GaugeControl();
            this.gaugeControl1 = new DevComponents.Instrumentation.GaugeControl();
            this.chartDetails = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.cboChartType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.SplineRangeD = new DevComponents.Editors.ComboItem();
            this.PieD = new DevComponents.Editors.ComboItem();
            this.ColumnD = new DevComponents.Editors.ComboItem();
            this.DoughnutD = new DevComponents.Editors.ComboItem();
            this.BarD = new DevComponents.Editors.ComboItem();
            this.FunnelD = new DevComponents.Editors.ComboItem();
            this.PyramidD = new DevComponents.Editors.ComboItem();
            this.pnlDetails = new DevComponents.DotNetBar.PanelEx();
            this.dgvDetails = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.pnlChart = new DevComponents.DotNetBar.PanelEx();
            this.chartSummary = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.expDetails = new DevComponents.DotNetBar.ExpandablePanel();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.cboItem = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.cboChartTypeSummary = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.SplineRangeS = new DevComponents.Editors.ComboItem();
            this.PieS = new DevComponents.Editors.ComboItem();
            this.ColumnS = new DevComponents.Editors.ComboItem();
            this.DoughnutS = new DevComponents.Editors.ComboItem();
            this.BarS = new DevComponents.Editors.ComboItem();
            this.FunnelS = new DevComponents.Editors.ComboItem();
            this.PyramidS = new DevComponents.Editors.ComboItem();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.dtpMonthYear = new System.Windows.Forms.DateTimePicker();
            this.expsRight = new DevComponents.DotNetBar.ExpandableSplitter();
            this.expsLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.pnlLeft.SuspendLayout();
            this.pnlLeftMenu.SuspendLayout();
            this.pnlRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gaugeControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gaugeControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gaugeControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartDetails)).BeginInit();
            this.pnlDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetails)).BeginInit();
            this.pnlChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartSummary)).BeginInit();
            this.expDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlLeft
            // 
            this.pnlLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlLeft.Controls.Add(this.pnlLeftMenu);
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(225, 495);
            this.pnlLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlLeft.Style.GradientAngle = 90;
            this.pnlLeft.TabIndex = 0;
            // 
            // pnlLeftMenu
            // 
            this.pnlLeftMenu.CanvasColor = System.Drawing.Color.Empty;
            this.pnlLeftMenu.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlLeftMenu.Controls.Add(this.btnProductMovement);
            this.pnlLeftMenu.Controls.Add(this.btnItemwiseSalesProfit);
            this.pnlLeftMenu.Controls.Add(this.btnPDCIssued);
            this.pnlLeftMenu.Controls.Add(this.btnPDCReceived);
            this.pnlLeftMenu.Controls.Add(this.btnIncomeExpense);
            this.pnlLeftMenu.Controls.Add(this.btnPurchase);
            this.pnlLeftMenu.Controls.Add(this.btnSales);
            this.pnlLeftMenu.Controls.Add(this.btnReceivablesAndPayables);
            this.pnlLeftMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLeftMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlLeftMenu.Name = "pnlLeftMenu";
            this.pnlLeftMenu.Size = new System.Drawing.Size(225, 483);
            this.pnlLeftMenu.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlLeftMenu.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.pnlLeftMenu.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.pnlLeftMenu.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlLeftMenu.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlLeftMenu.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlLeftMenu.Style.GradientAngle = 90;
            this.pnlLeftMenu.TabIndex = 0;
            // 
            // btnProductMovement
            // 
            this.btnProductMovement.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnProductMovement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.btnProductMovement.ColorTable = DevComponents.DotNetBar.eButtonColor.Orange;
            this.btnProductMovement.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProductMovement.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProductMovement.ForeColor = System.Drawing.Color.White;
            this.btnProductMovement.HoverImage = global::MyBooksERP.Properties.Resources.product_movement_hover;
            this.btnProductMovement.Image = global::MyBooksERP.Properties.Resources.product_movement;
            this.btnProductMovement.ImageFixedSize = new System.Drawing.Size(48, 48);
            this.btnProductMovement.ImageTextSpacing = 5;
            this.btnProductMovement.Location = new System.Drawing.Point(0, 420);
            this.btnProductMovement.Name = "btnProductMovement";
            this.btnProductMovement.Size = new System.Drawing.Size(225, 58);
            this.btnProductMovement.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnProductMovement.TabIndex = 7;
            this.btnProductMovement.Text = "Product Movement";
            this.btnProductMovement.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnProductMovement.Click += new System.EventHandler(this.btnProductMovement_Click);
            // 
            // btnItemwiseSalesProfit
            // 
            this.btnItemwiseSalesProfit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnItemwiseSalesProfit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.btnItemwiseSalesProfit.ColorTable = DevComponents.DotNetBar.eButtonColor.Orange;
            this.btnItemwiseSalesProfit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnItemwiseSalesProfit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemwiseSalesProfit.ForeColor = System.Drawing.Color.White;
            this.btnItemwiseSalesProfit.HoverImage = global::MyBooksERP.Properties.Resources.itemvise_product_report_hover;
            this.btnItemwiseSalesProfit.Image = global::MyBooksERP.Properties.Resources.itemvise_product_report;
            this.btnItemwiseSalesProfit.ImageFixedSize = new System.Drawing.Size(48, 48);
            this.btnItemwiseSalesProfit.ImageTextSpacing = 5;
            this.btnItemwiseSalesProfit.Location = new System.Drawing.Point(0, 360);
            this.btnItemwiseSalesProfit.Name = "btnItemwiseSalesProfit";
            this.btnItemwiseSalesProfit.Size = new System.Drawing.Size(225, 58);
            this.btnItemwiseSalesProfit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnItemwiseSalesProfit.TabIndex = 6;
            this.btnItemwiseSalesProfit.Text = "Monthly Sales";
            this.btnItemwiseSalesProfit.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnItemwiseSalesProfit.Click += new System.EventHandler(this.btnItemwiseSalesProfit_Click);
            // 
            // btnPDCIssued
            // 
            this.btnPDCIssued.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPDCIssued.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.btnPDCIssued.ColorTable = DevComponents.DotNetBar.eButtonColor.Orange;
            this.btnPDCIssued.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPDCIssued.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPDCIssued.ForeColor = System.Drawing.Color.White;
            this.btnPDCIssued.HoverImage = global::MyBooksERP.Properties.Resources.post_dated_cheque_issued_hover;
            this.btnPDCIssued.Image = global::MyBooksERP.Properties.Resources.post_dated_cheque_issued;
            this.btnPDCIssued.ImageFixedSize = new System.Drawing.Size(48, 48);
            this.btnPDCIssued.ImageTextSpacing = 5;
            this.btnPDCIssued.Location = new System.Drawing.Point(0, 300);
            this.btnPDCIssued.Name = "btnPDCIssued";
            this.btnPDCIssued.Size = new System.Drawing.Size(225, 58);
            this.btnPDCIssued.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPDCIssued.TabIndex = 5;
            this.btnPDCIssued.Text = "PDC Issued";
            this.btnPDCIssued.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnPDCIssued.Click += new System.EventHandler(this.btnPDCIssued_Click);
            // 
            // btnPDCReceived
            // 
            this.btnPDCReceived.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPDCReceived.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.btnPDCReceived.ColorTable = DevComponents.DotNetBar.eButtonColor.Orange;
            this.btnPDCReceived.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPDCReceived.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPDCReceived.ForeColor = System.Drawing.Color.White;
            this.btnPDCReceived.HoverImage = global::MyBooksERP.Properties.Resources.post_dated_cheque_recieved_hover;
            this.btnPDCReceived.Image = global::MyBooksERP.Properties.Resources.post_dated_cheque_recieved;
            this.btnPDCReceived.ImageFixedSize = new System.Drawing.Size(48, 48);
            this.btnPDCReceived.ImageTextSpacing = 5;
            this.btnPDCReceived.Location = new System.Drawing.Point(0, 240);
            this.btnPDCReceived.Name = "btnPDCReceived";
            this.btnPDCReceived.Size = new System.Drawing.Size(225, 58);
            this.btnPDCReceived.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPDCReceived.TabIndex = 4;
            this.btnPDCReceived.Text = "PDC Received";
            this.btnPDCReceived.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnPDCReceived.Click += new System.EventHandler(this.btnPDCReceived_Click);
            // 
            // btnIncomeExpense
            // 
            this.btnIncomeExpense.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnIncomeExpense.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.btnIncomeExpense.ColorTable = DevComponents.DotNetBar.eButtonColor.Orange;
            this.btnIncomeExpense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIncomeExpense.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncomeExpense.ForeColor = System.Drawing.Color.White;
            this.btnIncomeExpense.HoverImage = global::MyBooksERP.Properties.Resources.expense_and_income_hover;
            this.btnIncomeExpense.Image = global::MyBooksERP.Properties.Resources.expense_and_income;
            this.btnIncomeExpense.ImageFixedSize = new System.Drawing.Size(48, 48);
            this.btnIncomeExpense.ImageTextSpacing = 5;
            this.btnIncomeExpense.Location = new System.Drawing.Point(0, 60);
            this.btnIncomeExpense.Name = "btnIncomeExpense";
            this.btnIncomeExpense.Size = new System.Drawing.Size(225, 58);
            this.btnIncomeExpense.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnIncomeExpense.TabIndex = 1;
            this.btnIncomeExpense.Text = "Income && Expense";
            this.btnIncomeExpense.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnIncomeExpense.Click += new System.EventHandler(this.btnIncomeExpense_Click);
            // 
            // btnPurchase
            // 
            this.btnPurchase.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPurchase.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.btnPurchase.ColorTable = DevComponents.DotNetBar.eButtonColor.Orange;
            this.btnPurchase.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPurchase.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPurchase.ForeColor = System.Drawing.Color.White;
            this.btnPurchase.HoverImage = global::MyBooksERP.Properties.Resources.purchase_hover;
            this.btnPurchase.Image = global::MyBooksERP.Properties.Resources.purchase_DashBoard;
            this.btnPurchase.ImageFixedSize = new System.Drawing.Size(48, 48);
            this.btnPurchase.ImageTextSpacing = 5;
            this.btnPurchase.Location = new System.Drawing.Point(0, 180);
            this.btnPurchase.Name = "btnPurchase";
            this.btnPurchase.Size = new System.Drawing.Size(225, 58);
            this.btnPurchase.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPurchase.TabIndex = 3;
            this.btnPurchase.Text = "Purchase";
            this.btnPurchase.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnPurchase.Click += new System.EventHandler(this.btnPurchase_Click);
            // 
            // btnSales
            // 
            this.btnSales.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSales.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.btnSales.ColorTable = DevComponents.DotNetBar.eButtonColor.Orange;
            this.btnSales.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSales.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSales.ForeColor = System.Drawing.Color.White;
            this.btnSales.HoverImage = global::MyBooksERP.Properties.Resources.sales_hover;
            this.btnSales.Image = global::MyBooksERP.Properties.Resources.sales_DashBoard;
            this.btnSales.ImageFixedSize = new System.Drawing.Size(48, 48);
            this.btnSales.ImageTextSpacing = 5;
            this.btnSales.Location = new System.Drawing.Point(0, 120);
            this.btnSales.Name = "btnSales";
            this.btnSales.Size = new System.Drawing.Size(225, 58);
            this.btnSales.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSales.TabIndex = 2;
            this.btnSales.Text = "Sales";
            this.btnSales.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnSales.Click += new System.EventHandler(this.btnSales_Click);
            // 
            // btnReceivablesAndPayables
            // 
            this.btnReceivablesAndPayables.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnReceivablesAndPayables.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.btnReceivablesAndPayables.ColorTable = DevComponents.DotNetBar.eButtonColor.Orange;
            this.btnReceivablesAndPayables.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReceivablesAndPayables.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReceivablesAndPayables.ForeColor = System.Drawing.Color.White;
            this.btnReceivablesAndPayables.HoverImage = global::MyBooksERP.Properties.Resources.recievables_payables_hover;
            this.btnReceivablesAndPayables.Image = global::MyBooksERP.Properties.Resources.recievables_payables;
            this.btnReceivablesAndPayables.ImageFixedSize = new System.Drawing.Size(48, 48);
            this.btnReceivablesAndPayables.ImageTextSpacing = 5;
            this.btnReceivablesAndPayables.Location = new System.Drawing.Point(0, 0);
            this.btnReceivablesAndPayables.Name = "btnReceivablesAndPayables";
            this.btnReceivablesAndPayables.Padding = new System.Windows.Forms.Padding(2);
            this.btnReceivablesAndPayables.Size = new System.Drawing.Size(225, 58);
            this.btnReceivablesAndPayables.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnReceivablesAndPayables.TabIndex = 0;
            this.btnReceivablesAndPayables.Tag = "";
            this.btnReceivablesAndPayables.Text = "Receivables && Payables";
            this.btnReceivablesAndPayables.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnReceivablesAndPayables.Click += new System.EventHandler(this.btnReceivablesAndPayables_Click);
            // 
            // pnlRight
            // 
            this.pnlRight.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlRight.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlRight.Controls.Add(this.gaugeControl3);
            this.pnlRight.Controls.Add(this.gaugeControl2);
            this.pnlRight.Controls.Add(this.gaugeControl1);
            this.pnlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlRight.Location = new System.Drawing.Point(1254, 0);
            this.pnlRight.Name = "pnlRight";
            this.pnlRight.Size = new System.Drawing.Size(10, 495);
            this.pnlRight.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlRight.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlRight.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlRight.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlRight.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlRight.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlRight.Style.GradientAngle = 90;
            this.pnlRight.TabIndex = 2;
            this.pnlRight.Visible = false;
            // 
            // gaugeControl3
            // 
            this.gaugeControl3.Anchor = System.Windows.Forms.AnchorStyles.None;
            gaugeCircularScale1.Labels.Layout.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            gaugeCircularScale1.Labels.Layout.RotateLabel = false;
            gaugeCircularScale1.MajorTickMarks.Interval = 1;
            gradientFillColor1.BorderColor = System.Drawing.Color.DimGray;
            gradientFillColor1.BorderWidth = 1;
            gradientFillColor1.Color1 = System.Drawing.Color.White;
            gaugeCircularScale1.MajorTickMarks.Layout.FillColor = gradientFillColor1;
            gaugeCircularScale1.MajorTickMarks.Layout.Length = 0.263F;
            gaugeCircularScale1.MajorTickMarks.Layout.Style = DevComponents.Instrumentation.GaugeMarkerStyle.Circle;
            gaugeCircularScale1.MajorTickMarks.Layout.Width = 0.263F;
            gaugeCircularScale1.MaxPin.Name = "MaxPin";
            gaugeCircularScale1.MaxValue = 10;
            gaugeCircularScale1.MinorTickMarks.Interval = 0.5;
            gradientFillColor2.Color1 = System.Drawing.Color.Black;
            gaugeCircularScale1.MinorTickMarks.Layout.FillColor = gradientFillColor2;
            gaugeCircularScale1.MinorTickMarks.Layout.Length = 0.2F;
            gaugeCircularScale1.MinPin.Name = "MinPin";
            gaugeCircularScale1.Name = "Scale1";
            gaugeCircularScale1.PivotPoint = ((System.Drawing.PointF)(resources.GetObject("gaugeCircularScale1.PivotPoint")));
            gaugePointer1.CapFillColor.BorderColor = System.Drawing.Color.DimGray;
            gaugePointer1.CapFillColor.BorderWidth = 1;
            gaugePointer1.CapFillColor.Color1 = System.Drawing.Color.WhiteSmoke;
            gaugePointer1.CapFillColor.Color2 = System.Drawing.Color.Brown;
            gaugePointer1.CapFillColor.GradientFillType = DevComponents.Instrumentation.GradientFillType.Center;
            gaugePointer1.CapStyle = DevComponents.Instrumentation.NeedlePointerCapStyle.Style1;
            gaugePointer1.CapWidth = 0.4F;
            gaugePointer1.FillColor.BorderColor = System.Drawing.Color.DarkSlateGray;
            gaugePointer1.FillColor.BorderWidth = 1;
            gaugePointer1.FillColor.Color1 = System.Drawing.Color.Turquoise;
            gaugePointer1.Length = 0.54F;
            gaugePointer1.Name = "Pointer1";
            gaugePointer1.NeedleStyle = DevComponents.Instrumentation.NeedlePointerStyle.Style6;
            gaugePointer1.Placement = DevComponents.Instrumentation.DisplayPlacement.Near;
            gaugePointer1.Style = DevComponents.Instrumentation.PointerStyle.Needle;
            gaugePointer1.Width = 0.2F;
            gaugeCircularScale1.Pointers.AddRange(new DevComponents.Instrumentation.GaugePointer[] {
            gaugePointer1});
            gaugeCircularScale1.Radius = 0.092F;
            gaugeSection1.FillColor.Color1 = System.Drawing.Color.CornflowerBlue;
            gaugeSection1.Name = "Section1";
            gaugeCircularScale1.Sections.AddRange(new DevComponents.Instrumentation.GaugeSection[] {
            gaugeSection1});
            gaugeCircularScale1.Width = 0.139F;
            gaugeCircularScale2.MaxPin.Name = "MaxPin";
            gaugeCircularScale2.MinPin.Name = "MinPin";
            gaugeCircularScale2.Name = "Scale2";
            gaugePointer2.CapFillColor.BorderColor = System.Drawing.Color.DimGray;
            gaugePointer2.CapFillColor.BorderWidth = 1;
            gaugePointer2.CapFillColor.Color1 = System.Drawing.Color.WhiteSmoke;
            gaugePointer2.CapFillColor.Color2 = System.Drawing.Color.DimGray;
            gaugePointer2.FillColor.BorderColor = System.Drawing.Color.DimGray;
            gaugePointer2.FillColor.BorderWidth = 1;
            gaugePointer2.FillColor.Color1 = System.Drawing.Color.WhiteSmoke;
            gaugePointer2.FillColor.Color2 = System.Drawing.Color.Red;
            gaugePointer2.Length = 0.358F;
            gaugePointer2.Name = "Pointer1";
            gaugePointer2.Style = DevComponents.Instrumentation.PointerStyle.Needle;
            gaugeCircularScale2.Pointers.AddRange(new DevComponents.Instrumentation.GaugePointer[] {
            gaugePointer2});
            gaugeSection2.FillColor.Color1 = System.Drawing.Color.CornflowerBlue;
            gaugeSection2.FillColor.Color2 = System.Drawing.Color.Purple;
            gaugeSection2.Name = "Section1";
            gaugeCircularScale2.Sections.AddRange(new DevComponents.Instrumentation.GaugeSection[] {
            gaugeSection2});
            this.gaugeControl3.CircularScales.AddRange(new DevComponents.Instrumentation.GaugeCircularScale[] {
            gaugeCircularScale1,
            gaugeCircularScale2});
            gradientFillColor3.Color1 = System.Drawing.Color.Gainsboro;
            gradientFillColor3.Color2 = System.Drawing.Color.DarkGray;
            this.gaugeControl3.Frame.BackColor = gradientFillColor3;
            gradientFillColor4.BorderColor = System.Drawing.Color.Gainsboro;
            gradientFillColor4.BorderWidth = 1;
            gradientFillColor4.Color1 = System.Drawing.Color.White;
            gradientFillColor4.Color2 = System.Drawing.Color.DimGray;
            this.gaugeControl3.Frame.FrameColor = gradientFillColor4;
            this.gaugeControl3.Frame.Style = DevComponents.Instrumentation.GaugeFrameStyle.Circular;
            //this.gaugeControl3.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.gaugeControl3.Location = new System.Drawing.Point(-72, 338);
            this.gaugeControl3.Name = "gaugeControl3";
            this.gaugeControl3.Size = new System.Drawing.Size(155, 155);
            this.gaugeControl3.TabIndex = 2;
            this.gaugeControl3.Text = "gaugeControl3";
            // 
            // gaugeControl2
            // 
            this.gaugeControl2.Anchor = System.Windows.Forms.AnchorStyles.None;
            gaugeCircularScale3.MaxPin.Name = "MaxPin";
            gaugeCircularScale3.MinPin.Name = "MinPin";
            gaugeCircularScale3.Name = "Scale1";
            gaugePointer3.CapFillColor.BorderColor = System.Drawing.Color.DimGray;
            gaugePointer3.CapFillColor.BorderWidth = 1;
            gaugePointer3.CapFillColor.Color1 = System.Drawing.Color.WhiteSmoke;
            gaugePointer3.CapFillColor.Color2 = System.Drawing.Color.DimGray;
            gaugePointer3.FillColor.BorderColor = System.Drawing.Color.DimGray;
            gaugePointer3.FillColor.BorderWidth = 1;
            gaugePointer3.FillColor.Color1 = System.Drawing.Color.WhiteSmoke;
            gaugePointer3.FillColor.Color2 = System.Drawing.Color.Red;
            gaugePointer3.Length = 0.358F;
            gaugePointer3.Name = "Pointer1";
            gaugePointer3.Style = DevComponents.Instrumentation.PointerStyle.Needle;
            gaugeCircularScale3.Pointers.AddRange(new DevComponents.Instrumentation.GaugePointer[] {
            gaugePointer3});
            gaugeSection3.FillColor.Color1 = System.Drawing.Color.CornflowerBlue;
            gaugeSection3.FillColor.Color2 = System.Drawing.Color.Purple;
            gaugeSection3.Name = "Section1";
            gaugeCircularScale3.Sections.AddRange(new DevComponents.Instrumentation.GaugeSection[] {
            gaugeSection3});
            gaugeCircularScale4.MajorTickMarks.Interval = 20;
            gaugeCircularScale4.MaxPin.Name = "MaxPin";
            gaugeCircularScale4.MaxValue = 200;
            gaugeCircularScale4.MinorTickMarks.Interval = 4;
            gaugeCircularScale4.MinPin.Name = "MinPin";
            gaugeCircularScale4.Name = "Scale2";
            gaugeCircularScale4.Radius = 0.24F;
            gaugeSection4.FillColor.Color1 = System.Drawing.Color.CornflowerBlue;
            gaugeSection4.Name = "Section1";
            gaugeCircularScale4.Sections.AddRange(new DevComponents.Instrumentation.GaugeSection[] {
            gaugeSection4});
            this.gaugeControl2.CircularScales.AddRange(new DevComponents.Instrumentation.GaugeCircularScale[] {
            gaugeCircularScale3,
            gaugeCircularScale4});
            gradientFillColor5.Color1 = System.Drawing.Color.Gainsboro;
            gradientFillColor5.Color2 = System.Drawing.Color.DarkGray;
            this.gaugeControl2.Frame.BackColor = gradientFillColor5;
            gradientFillColor6.BorderColor = System.Drawing.Color.Gainsboro;
            gradientFillColor6.BorderWidth = 1;
            gradientFillColor6.Color1 = System.Drawing.Color.White;
            gradientFillColor6.Color2 = System.Drawing.Color.DimGray;
            this.gaugeControl2.Frame.FrameColor = gradientFillColor6;
            this.gaugeControl2.Frame.Style = DevComponents.Instrumentation.GaugeFrameStyle.Circular;
            //this.gaugeControl2.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.gaugeControl2.Location = new System.Drawing.Point(-72, 168);
            this.gaugeControl2.Name = "gaugeControl2";
            this.gaugeControl2.Size = new System.Drawing.Size(155, 155);
            this.gaugeControl2.TabIndex = 1;
            this.gaugeControl2.Text = "gaugeControl2";
            // 
            // gaugeControl1
            // 
            this.gaugeControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            gaugeCircularScale5.MaxPin.Name = "MaxPin";
            gaugeCircularScale5.MinPin.Name = "MinPin";
            gaugeCircularScale5.Name = "Scale1";
            gaugePointer4.CapFillColor.BorderColor = System.Drawing.Color.DimGray;
            gaugePointer4.CapFillColor.BorderWidth = 1;
            gaugePointer4.CapFillColor.Color1 = System.Drawing.Color.WhiteSmoke;
            gaugePointer4.CapFillColor.Color2 = System.Drawing.Color.DimGray;
            gaugePointer4.FillColor.BorderColor = System.Drawing.Color.DimGray;
            gaugePointer4.FillColor.BorderWidth = 1;
            gaugePointer4.FillColor.Color1 = System.Drawing.Color.WhiteSmoke;
            gaugePointer4.FillColor.Color2 = System.Drawing.Color.Red;
            gaugePointer4.Length = 0.358F;
            gaugePointer4.Name = "Pointer1";
            gaugePointer4.Style = DevComponents.Instrumentation.PointerStyle.Needle;
            gaugeCircularScale5.Pointers.AddRange(new DevComponents.Instrumentation.GaugePointer[] {
            gaugePointer4});
            gaugeRange1.FillColor.BorderColor = System.Drawing.Color.DimGray;
            gaugeRange1.FillColor.BorderWidth = 1;
            gaugeRange1.FillColor.Color1 = System.Drawing.Color.Lime;
            gaugeRange1.FillColor.Color2 = System.Drawing.Color.Red;
            gaugeRange1.Name = "Range1";
            gaugeRange1.ScaleOffset = 0.28F;
            gaugeRange1.StartValue = 70;
            gaugeCircularScale5.Ranges.AddRange(new DevComponents.Instrumentation.GaugeRange[] {
            gaugeRange1});
            gaugeSection5.FillColor.Color1 = System.Drawing.Color.CornflowerBlue;
            gaugeSection5.FillColor.Color2 = System.Drawing.Color.Purple;
            gaugeSection5.Name = "Section1";
            gaugeCircularScale5.Sections.AddRange(new DevComponents.Instrumentation.GaugeSection[] {
            gaugeSection5});
            this.gaugeControl1.CircularScales.AddRange(new DevComponents.Instrumentation.GaugeCircularScale[] {
            gaugeCircularScale5});
            this.gaugeControl1.ForeColor = System.Drawing.SystemColors.ControlText;
            gradientFillColor7.Color1 = System.Drawing.Color.Gainsboro;
            gradientFillColor7.Color2 = System.Drawing.Color.DarkGray;
            this.gaugeControl1.Frame.BackColor = gradientFillColor7;
            gradientFillColor8.BorderColor = System.Drawing.Color.Gainsboro;
            gradientFillColor8.BorderWidth = 1;
            gradientFillColor8.Color1 = System.Drawing.Color.White;
            gradientFillColor8.Color2 = System.Drawing.Color.DimGray;
            this.gaugeControl1.Frame.FrameColor = gradientFillColor8;
            this.gaugeControl1.Frame.Style = DevComponents.Instrumentation.GaugeFrameStyle.Circular;
            //this.gaugeControl1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.gaugeControl1.Location = new System.Drawing.Point(-72, 3);
            this.gaugeControl1.Name = "gaugeControl1";
            this.gaugeControl1.Size = new System.Drawing.Size(155, 153);
            this.gaugeControl1.TabIndex = 0;
            this.gaugeControl1.Text = "Working Capital";
            // 
            // chartDetails
            // 
            this.chartDetails.BackColor = System.Drawing.Color.Transparent;
            this.chartDetails.BackSecondaryColor = System.Drawing.Color.Transparent;
            this.chartDetails.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.chartDetails.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            this.chartDetails.BorderSkin.BackColor = System.Drawing.Color.Transparent;
            this.chartDetails.BorderSkin.BorderColor = System.Drawing.Color.Transparent;
            this.chartDetails.BorderSkin.PageColor = System.Drawing.Color.Transparent;
            chartArea1.AlignmentOrientation = ((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations)((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Vertical | System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal)));
            chartArea1.Area3DStyle.IsClustered = true;
            chartArea1.Area3DStyle.IsRightAngleAxes = false;
            chartArea1.Area3DStyle.LightStyle = System.Windows.Forms.DataVisualization.Charting.LightStyle.Realistic;
            chartArea1.Area3DStyle.Rotation = 0;
            chartArea1.AxisX.Interval = 1;
            chartArea1.AxisX.IsLabelAutoFit = false;
            chartArea1.AxisX.IsStartedFromZero = false;
            chartArea1.AxisX.LabelStyle.Angle = -45;
            chartArea1.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            chartArea1.AxisX.ScaleBreakStyle.Enabled = true;
            chartArea1.AxisX.ScaleBreakStyle.MaxNumberOfBreaks = 5;
            chartArea1.AxisX.ScaleBreakStyle.Spacing = 2;
            chartArea1.AxisX.ScaleBreakStyle.StartFromZero = System.Windows.Forms.DataVisualization.Charting.StartFromZero.No;
            chartArea1.AxisY.IsLabelAutoFit = false;
            chartArea1.AxisY.TitleAlignment = System.Drawing.StringAlignment.Near;
            chartArea1.BackColor = System.Drawing.Color.White;
            chartArea1.CursorX.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.CursorX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.CursorX.IsUserEnabled = true;
            chartArea1.CursorX.IsUserSelectionEnabled = true;
            chartArea1.Name = "ChartArea1";
            chartArea1.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.chartDetails.ChartAreas.Add(chartArea1);
            this.chartDetails.Dock = System.Windows.Forms.DockStyle.Left;
            legend1.Name = "Legend1";
            this.chartDetails.Legends.Add(legend1);
            this.chartDetails.Location = new System.Drawing.Point(0, 0);
            this.chartDetails.Name = "chartDetails";
            this.chartDetails.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.BrightPastel;
            series1.ShadowColor = System.Drawing.Color.Empty;
            this.chartDetails.Series.Add(series1);
            this.chartDetails.Size = new System.Drawing.Size(550, 264);
            this.chartDetails.TabIndex = 1;
            this.chartDetails.Text = "Receivables & Payables";
            // 
            // labelX11
            // 
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.Class = "";
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(566, 33);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(94, 23);
            this.labelX11.TabIndex = 64;
            this.labelX11.Text = "Detail Chart Type";
            // 
            // cboChartType
            // 
            this.cboChartType.DisplayMember = "Text";
            this.cboChartType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboChartType.FormattingEnabled = true;
            this.cboChartType.ItemHeight = 14;
            this.cboChartType.Items.AddRange(new object[] {
            this.SplineRangeD,
            this.PieD,
            this.ColumnD,
            this.DoughnutD,
            this.BarD,
            this.FunnelD,
            this.PyramidD});
            this.cboChartType.Location = new System.Drawing.Point(669, 36);
            this.cboChartType.Name = "cboChartType";
            this.cboChartType.Size = new System.Drawing.Size(100, 20);
            this.cboChartType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboChartType.TabIndex = 65;
            this.cboChartType.Visible = false;
            this.cboChartType.SelectedIndexChanged += new System.EventHandler(this.cboChartType_SelectedIndexChanged);
            // 
            // SplineRangeD
            // 
            this.SplineRangeD.Text = "SplineRange";
            // 
            // PieD
            // 
            this.PieD.Text = "Pie";
            // 
            // ColumnD
            // 
            this.ColumnD.Text = "Column";
            // 
            // DoughnutD
            // 
            this.DoughnutD.Text = "Doughnut";
            // 
            // BarD
            // 
            this.BarD.Text = "Bar";
            // 
            // FunnelD
            // 
            this.FunnelD.Text = "Funnel";
            // 
            // PyramidD
            // 
            this.PyramidD.Text = "Pyramid";
            // 
            // pnlDetails
            // 
            this.pnlDetails.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlDetails.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlDetails.Controls.Add(this.dgvDetails);
            this.pnlDetails.Controls.Add(this.pnlChart);
            this.pnlDetails.Controls.Add(this.expDetails);
            this.pnlDetails.Controls.Add(this.expsRight);
            this.pnlDetails.Controls.Add(this.expsLeft);
            this.pnlDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDetails.Location = new System.Drawing.Point(225, 0);
            this.pnlDetails.Name = "pnlDetails";
            this.pnlDetails.Padding = new System.Windows.Forms.Padding(2);
            this.pnlDetails.Size = new System.Drawing.Size(1029, 495);
            this.pnlDetails.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlDetails.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlDetails.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlDetails.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlDetails.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlDetails.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlDetails.Style.GradientAngle = 90;
            this.pnlDetails.TabIndex = 1;
            // 
            // dgvDetails
            // 
            this.dgvDetails.AllowUserToAddRows = false;
            this.dgvDetails.AllowUserToDeleteRows = false;
            this.dgvDetails.AllowUserToResizeColumns = false;
            this.dgvDetails.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.dgvDetails.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDetails.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDetails.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvDetails.Location = new System.Drawing.Point(6, 292);
            this.dgvDetails.Name = "dgvDetails";
            this.dgvDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDetails.Size = new System.Drawing.Size(1017, 201);
            this.dgvDetails.TabIndex = 2;
            this.dgvDetails.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvDetails_DataError);
            // 
            // pnlChart
            // 
            this.pnlChart.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlChart.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlChart.Controls.Add(this.chartSummary);
            this.pnlChart.Controls.Add(this.chartDetails);
            this.pnlChart.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlChart.Location = new System.Drawing.Point(6, 28);
            this.pnlChart.Name = "pnlChart";
            this.pnlChart.Size = new System.Drawing.Size(1017, 264);
            this.pnlChart.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlChart.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlChart.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlChart.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlChart.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlChart.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlChart.Style.GradientAngle = 90;
            this.pnlChart.TabIndex = 62;
            // 
            // chartSummary
            // 
            this.chartSummary.BackColor = System.Drawing.Color.Transparent;
            this.chartSummary.BackSecondaryColor = System.Drawing.Color.Transparent;
            this.chartSummary.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.chartSummary.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            this.chartSummary.BorderSkin.BackColor = System.Drawing.Color.Transparent;
            this.chartSummary.BorderSkin.BorderColor = System.Drawing.Color.Transparent;
            this.chartSummary.BorderSkin.PageColor = System.Drawing.Color.Transparent;
            chartArea2.AlignmentOrientation = ((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations)((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Vertical | System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal)));
            chartArea2.Area3DStyle.IsClustered = true;
            chartArea2.Area3DStyle.IsRightAngleAxes = false;
            chartArea2.Area3DStyle.LightStyle = System.Windows.Forms.DataVisualization.Charting.LightStyle.Realistic;
            chartArea2.Area3DStyle.Rotation = 0;
            chartArea2.AxisX.Interval = 1;
            chartArea2.AxisX.IsLabelAutoFit = false;
            chartArea2.AxisX.IsStartedFromZero = false;
            chartArea2.AxisX.LabelStyle.Angle = -45;
            chartArea2.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            chartArea2.AxisX.ScaleBreakStyle.Enabled = true;
            chartArea2.AxisX.ScaleBreakStyle.MaxNumberOfBreaks = 5;
            chartArea2.AxisX.ScaleBreakStyle.Spacing = 2;
            chartArea2.AxisX.ScaleBreakStyle.StartFromZero = System.Windows.Forms.DataVisualization.Charting.StartFromZero.No;
            chartArea2.AxisY.IsLabelAutoFit = false;
            chartArea2.AxisY.TitleAlignment = System.Drawing.StringAlignment.Near;
            chartArea2.BackColor = System.Drawing.Color.White;
            chartArea2.CursorX.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea2.CursorX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea2.CursorX.IsUserEnabled = true;
            chartArea2.CursorX.IsUserSelectionEnabled = true;
            chartArea2.Name = "ChartArea1";
            chartArea2.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.chartSummary.ChartAreas.Add(chartArea2);
            this.chartSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Name = "Legend1";
            this.chartSummary.Legends.Add(legend2);
            this.chartSummary.Location = new System.Drawing.Point(550, 0);
            this.chartSummary.Name = "chartSummary";
            this.chartSummary.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            series2.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.BrightPastel;
            series2.ShadowColor = System.Drawing.Color.Empty;
            this.chartSummary.Series.Add(series2);
            this.chartSummary.Size = new System.Drawing.Size(467, 264);
            this.chartSummary.TabIndex = 0;
            this.chartSummary.Text = "Receivables & Payables";
            // 
            // expDetails
            // 
            this.expDetails.CanvasColor = System.Drawing.SystemColors.Control;
            this.expDetails.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expDetails.Controls.Add(this.labelX2);
            this.expDetails.Controls.Add(this.cboItem);
            this.expDetails.Controls.Add(this.labelX1);
            this.expDetails.Controls.Add(this.cboChartTypeSummary);
            this.expDetails.Controls.Add(this.labelX11);
            this.expDetails.Controls.Add(this.labelX12);
            this.expDetails.Controls.Add(this.cboChartType);
            this.expDetails.Controls.Add(this.dtpMonthYear);
            this.expDetails.Dock = System.Windows.Forms.DockStyle.Top;
            this.expDetails.Expanded = false;
            this.expDetails.ExpandedBounds = new System.Drawing.Rectangle(6, 2, 1017, 66);
            this.expDetails.Location = new System.Drawing.Point(6, 2);
            this.expDetails.Name = "expDetails";
            this.expDetails.Size = new System.Drawing.Size(1017, 26);
            this.expDetails.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expDetails.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expDetails.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expDetails.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expDetails.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expDetails.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expDetails.Style.GradientAngle = 90;
            this.expDetails.TabIndex = 0;
            this.expDetails.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expDetails.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expDetails.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expDetails.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expDetails.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expDetails.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expDetails.TitleStyle.GradientAngle = 90;
            this.expDetails.TitleText = "Chart Details";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.Class = "";
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(203, 33);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(29, 23);
            this.labelX2.TabIndex = 68;
            this.labelX2.Text = "Item";
            // 
            // cboItem
            // 
            this.cboItem.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboItem.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboItem.DisplayMember = "Text";
            this.cboItem.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboItem.DropDownHeight = 105;
            this.cboItem.FormattingEnabled = true;
            this.cboItem.IntegralHeight = false;
            this.cboItem.ItemHeight = 14;
            this.cboItem.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2,
            this.comboItem3,
            this.comboItem4,
            this.comboItem5,
            this.comboItem6,
            this.comboItem7});
            this.cboItem.Location = new System.Drawing.Point(241, 36);
            this.cboItem.Name = "cboItem";
            this.cboItem.Size = new System.Drawing.Size(309, 20);
            this.cboItem.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboItem.TabIndex = 69;
            this.cboItem.Visible = false;
            this.cboItem.SelectedIndexChanged += new System.EventHandler(this.cboItem_SelectedIndexChanged);
            this.cboItem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboItem_KeyPress);
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "SplineRange";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "Pie";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "Column";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "Doughnut";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "Bar";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "Funnel";
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "Pyramid";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(778, 33);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(114, 23);
            this.labelX1.TabIndex = 66;
            this.labelX1.Text = "Summary Chart Type";
            // 
            // cboChartTypeSummary
            // 
            this.cboChartTypeSummary.DisplayMember = "Text";
            this.cboChartTypeSummary.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboChartTypeSummary.FormattingEnabled = true;
            this.cboChartTypeSummary.ItemHeight = 14;
            this.cboChartTypeSummary.Items.AddRange(new object[] {
            this.SplineRangeS,
            this.PieS,
            this.ColumnS,
            this.DoughnutS,
            this.BarS,
            this.FunnelS,
            this.PyramidS});
            this.cboChartTypeSummary.Location = new System.Drawing.Point(901, 36);
            this.cboChartTypeSummary.Name = "cboChartTypeSummary";
            this.cboChartTypeSummary.Size = new System.Drawing.Size(100, 20);
            this.cboChartTypeSummary.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboChartTypeSummary.TabIndex = 67;
            this.cboChartTypeSummary.Visible = false;
            this.cboChartTypeSummary.SelectedIndexChanged += new System.EventHandler(this.cboChartType_SelectedIndexChanged);
            // 
            // SplineRangeS
            // 
            this.SplineRangeS.Text = "SplineRange";
            // 
            // PieS
            // 
            this.PieS.Text = "Pie";
            // 
            // ColumnS
            // 
            this.ColumnS.Text = "Column";
            // 
            // DoughnutS
            // 
            this.DoughnutS.Text = "Doughnut";
            // 
            // BarS
            // 
            this.BarS.Text = "Bar";
            // 
            // FunnelS
            // 
            this.FunnelS.Text = "Funnel";
            // 
            // PyramidS
            // 
            this.PyramidS.Text = "Pyramid";
            // 
            // labelX12
            // 
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.Class = "";
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(17, 33);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(68, 23);
            this.labelX12.TabIndex = 62;
            this.labelX12.Text = "Month && Year";
            // 
            // dtpMonthYear
            // 
            this.dtpMonthYear.CustomFormat = "MMM yyyy";
            this.dtpMonthYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonthYear.Location = new System.Drawing.Point(94, 36);
            this.dtpMonthYear.Name = "dtpMonthYear";
            this.dtpMonthYear.Size = new System.Drawing.Size(100, 20);
            this.dtpMonthYear.TabIndex = 63;
            this.dtpMonthYear.ValueChanged += new System.EventHandler(this.dtpMonthYear_ValueChanged);
            // 
            // expsRight
            // 
            this.expsRight.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expsRight.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expsRight.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expsRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.expsRight.ExpandableControl = this.pnlRight;
            this.expsRight.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expsRight.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expsRight.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expsRight.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expsRight.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expsRight.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expsRight.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expsRight.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expsRight.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.expsRight.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(179)))), ((int)(((byte)(219)))));
            this.expsRight.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expsRight.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expsRight.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expsRight.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expsRight.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expsRight.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expsRight.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expsRight.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expsRight.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expsRight.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expsRight.Location = new System.Drawing.Point(1023, 2);
            this.expsRight.Name = "expsRight";
            this.expsRight.Size = new System.Drawing.Size(4, 491);
            this.expsRight.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expsRight.TabIndex = 3;
            this.expsRight.TabStop = false;
            this.expsRight.Visible = false;
            // 
            // expsLeft
            // 
            this.expsLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expsLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expsLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expsLeft.ExpandableControl = this.pnlLeft;
            this.expsLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expsLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expsLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expsLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expsLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expsLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expsLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expsLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expsLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.expsLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(179)))), ((int)(((byte)(219)))));
            this.expsLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expsLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expsLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expsLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expsLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expsLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expsLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expsLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expsLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expsLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expsLeft.Location = new System.Drawing.Point(2, 2);
            this.expsLeft.Name = "expsLeft";
            this.expsLeft.Size = new System.Drawing.Size(4, 491);
            this.expsLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expsLeft.TabIndex = 1;
            this.expsLeft.TabStop = false;
            // 
            // FrmDashBoard
            // 
            this.ClientSize = new System.Drawing.Size(1264, 495);
            this.Controls.Add(this.pnlDetails);
            this.Controls.Add(this.pnlRight);
            this.Controls.Add(this.pnlLeft);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmDashBoard";
            this.Text = "Dash Board";
            this.Load += new System.EventHandler(this.FrmDashBoard_Load);
            this.pnlLeft.ResumeLayout(false);
            this.pnlLeftMenu.ResumeLayout(false);
            this.pnlRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gaugeControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gaugeControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gaugeControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartDetails)).EndInit();
            this.pnlDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetails)).EndInit();
            this.pnlChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartSummary)).EndInit();
            this.expDetails.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx pnlLeft;
        private DevComponents.DotNetBar.PanelEx pnlLeftMenu;
        private DevComponents.DotNetBar.ButtonX btnProductMovement;
        private DevComponents.DotNetBar.ButtonX btnItemwiseSalesProfit;
        private DevComponents.DotNetBar.ButtonX btnPDCIssued;
        private DevComponents.DotNetBar.ButtonX btnPDCReceived;
        private DevComponents.DotNetBar.ButtonX btnIncomeExpense;
        private DevComponents.DotNetBar.ButtonX btnPurchase;
        private DevComponents.DotNetBar.ButtonX btnSales;
        private DevComponents.DotNetBar.ButtonX btnReceivablesAndPayables;
        private DevComponents.DotNetBar.PanelEx pnlRight;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartDetails;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboChartType;
        private DevComponents.DotNetBar.PanelEx pnlDetails;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvDetails;
        private DevComponents.DotNetBar.LabelX labelX12;
        private System.Windows.Forms.DateTimePicker dtpMonthYear;
        private DevComponents.DotNetBar.ExpandableSplitter expsRight;
        private DevComponents.DotNetBar.ExpandableSplitter expsLeft;
        private DevComponents.Instrumentation.GaugeControl gaugeControl2;
        private DevComponents.Instrumentation.GaugeControl gaugeControl1;
        private DevComponents.DotNetBar.ExpandablePanel expDetails;
        private DevComponents.Instrumentation.GaugeControl gaugeControl3;
        private DevComponents.DotNetBar.PanelEx pnlChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartSummary;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboChartTypeSummary;
        private DevComponents.Editors.ComboItem SplineRangeD;
        private DevComponents.Editors.ComboItem PieD;
        private DevComponents.Editors.ComboItem ColumnD;
        private DevComponents.Editors.ComboItem DoughnutD;
        private DevComponents.Editors.ComboItem BarD;
        private DevComponents.Editors.ComboItem FunnelD;
        private DevComponents.Editors.ComboItem PyramidD;
        private DevComponents.Editors.ComboItem SplineRangeS;
        private DevComponents.Editors.ComboItem PieS;
        private DevComponents.Editors.ComboItem ColumnS;
        private DevComponents.Editors.ComboItem DoughnutS;
        private DevComponents.Editors.ComboItem BarS;
        private DevComponents.Editors.ComboItem FunnelS;
        private DevComponents.Editors.ComboItem PyramidS;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboItem;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.Editors.ComboItem comboItem7;
    }
}