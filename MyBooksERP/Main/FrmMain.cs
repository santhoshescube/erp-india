using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Rendering;
using MyBooksERP.Reports;
using System.Text;
using System.Globalization;

namespace MyBooksERP
{
    /// <summary>
    /// Summary description for frmMain.
    /// </summary>
    public class FrmMain : DevComponents.DotNetBar.Office2007RibbonForm
    {
        #region Controls Declaration

        private System.ComponentModel.IContainer components;
        private BalloonSearch m_Search = null;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem labelStatus;
        private LabelItem labelPosition;
        private DevComponents.DotNetBar.RibbonControl ribbonControl;
        private RibbonTabItem Company;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel1;
        private RibbonTabItem Purchase;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel3;
        private DevComponents.DotNetBar.RibbonBar ribbonBar1;
        private DevComponents.DotNetBar.ButtonItem NewCompany;
        private DevComponents.DotNetBar.RibbonBar ribbonBar5;
        private DevComponents.DotNetBar.ButtonItem PurchaseOrder;
        private DevComponents.DotNetBar.ButtonItem PurchaseInvoice;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.Editors.ComboItem comboItem7;
        private DevComponents.Editors.ComboItem comboItem8;
        private DevComponents.Editors.ComboItem comboItem9;
        private DevComponents.DotNetBar.RibbonPanel Inventory1;
        private DevComponents.DotNetBar.RibbonBar ribbonBar6;
        private DevComponents.DotNetBar.RibbonTabItemGroup ribbonTabItemGroup1;
        private DevComponents.DotNetBar.ButtonItem StockAdjustment;
        private RibbonTabItem Inventory;
        private DevComponents.DotNetBar.ButtonItem buttonChangeStyle;
        private DevComponents.DotNetBar.SuperTooltip superTooltip1;
        private DevComponents.DotNetBar.ButtonItem buttonStyleOffice2007Blue;
        private DevComponents.DotNetBar.ButtonItem buttonStyleOffice2007Black;
        private DevComponents.DotNetBar.QatCustomizeItem qatCustomizeItem1;
        private DevComponents.DotNetBar.ButtonItem buttonStyleOffice2007Silver;
        private DevComponents.DotNetBar.ColorPickerDropDown buttonStyleCustom;
        private DevComponents.DotNetBar.ItemContainer itemContainer13;
        private Command AppCommandNew;
        private Command AppCommandTheme;
        private Command AppCommandExit;
        private DevComponents.DotNetBar.ButtonItem buttonItem60;
        private ButtonItem GoodsReceiptNote;
        private RibbonPanel ribbonPanel4;
        private RibbonBar ribbonBar9;
        private ButtonItem SalesInvoice;
        private RibbonTabItem Sales;
        private ButtonItem SalesQuotation;
        private ButtonItem SalesOrder;
        private RibbonPanel ribbonPanel6;
        private RibbonTabItem TradingReports;
        private RibbonBar ribbonBar13;
        private RibbonPanel ribbonPanel7;
        private RibbonBar ribbonBar14;
        private ButtonItem ReportProducts;
        private RibbonTabItem Settings;
        private ButtonItem Email;
        private RibbonBar ribbonBar15;
        private ButtonItem User;
        private ButtonItem UserRole;
        private RibbonPanel ribbonPanel8;
        private RibbonBar ribbonBar16;
        private ButtonItem About;
        private RibbonTabItem Help;
        private ButtonItem bSales;
        private RibbonPanel rpHome;
        private RibbonBar HomeBar;
        private RibbonTabItem Home;

        private Command AppCmdSales;
        private Command AppCmdRoleSettings;
        private RibbonBar ribbonBar19;
        private ButtonItem Uom;
        private ButtonItem Configuration;
        private ButtonItem UomConversion;
        private ButtonItem POS;
        private ButtonItem buttonItem47;
        private ButtonItem buttonItem48;
        private ButtonItem buttonItem49;
        #endregion

        #region Variable Declaration

        private Command AppCmdPurchase;
        private ButtonItem OpeningStock;
        private ButtonItem CompanySettings;
        private ButtonItem ReportEmployee;
        private ButtonItem ReportsAssociates;
        private ButtonItem ReportsStock;
        private ButtonItem ReportsSales;
        private ButtonItem ReportsPurchase;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private BackgroundWorker bwrAlert;
        public Timer tmrAlert;
        private Command AppCommandReports;
        private ButtonItem AlertSettings;
        private ButtonItem BackUp;
        private ButtonItem btnPaymentsReceipts;
        private DockSite barTopDockSite;
        private DockSite barBottomDockSite;
        private Bar barTaskList;
        private PanelDockContainer PanelDockContainer1;
        internal PictureBox pctNoalerts;
        internal DataGridView dgvAlerts;
        private PanelDockContainer PanelDockContainer2;
        internal WebBrowser WebRss;
        private DockContainerItem dockRss;
        private DockContainerItem dockAlertList;
        private DockSite barLeftDockSite;
        private DockSite barRightDockSite;
        private DotNetBarManager dotNetBarManager1;
        private DockSite dockSite1;
        private DockSite dockSite2;
        private DockSite dockSite3;
        private DockSite dockSite4;
        private ButtonItem StockTransfer;
        private Command AppCmdInventory;

        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;

        private clsBLLMain objclsBLLMain;
        ClsNotification MObjClsNotification;
        private ButtonItem Receipts;
        private ButtonItem Payments;
        private ButtonItem PurchaseQuotation;
        private ButtonItem Products;
        private ButtonItem btnHome;
        private Office2007StartButton buttonFile;
        private ButtonItem Warehouse;
        private ButtonItem Currency;
        private ButtonItem DeliveryNote;
        private TabStrip tabStrip1;
        private ButtonItem btnItmCustomers;
        private RibbonBar rbSales;
        private RibbonBar rbInventory;
        private ButtonItem btnItmDeliveryNote;
        private ButtonItem btnItmPOS;
        private ButtonItem btnItmReceipts;
        private ButtonItem btnItmSuppliers;
        private RibbonBar rbReports;
        private ButtonItem btnItmRptStock;
        private ButtonItem btnItmRptPaymentsAndReceipts;
        private ButtonItem ChangePassword;
        private ButtonItem BtnDebitNote;
        private ButtonItem BtnCreditNote;
        private ButtonItem BtnDiscounts;
        private ButtonItem BtnPricingScheme;
        private ButtonItem BtnTermsAndConditions;
        private ButtonItem BtnSalesH;
        private ButtonItem BtnPaymentsInv;
        private ButtonItem BtnSummary;
        private ButtonItem btnExpense;
        private ButtonItem BtnHierarchySetting;
        private ContextMenuStrip cmnuAlertOptions;
        private ToolStripMenuItem tsmiMarkAsUnRead;
        private ToolStripMenuItem tsmiMarkAsRead;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private ToolStripMenuItem tsmiFilterOptions;
        private ToolStripMenuItem tsmiAll;
        private ToolStripMenuItem tsmiUnread;
        private ToolStripMenuItem tsmiRead;
        private ToolStripMenuItem tsmiBlocked;
        private ToolStripMenuItem tsmiBlock;
        private DataGridViewTextBoxColumn colAlertMsg;
        private DataGridViewTextBoxColumn clmnAlertSettingsID;
        private DataGridViewTextBoxColumn clmnReferenceID;
        private DataGridViewTextBoxColumn clmnStatusID;
        private DataGridViewTextBoxColumn clmnAlertID;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;

        public string strAlertStatusID = "60,67";
        private ToolStripMenuItem tsmiUnBlock;
        public int PintNewAlertCount = 0;
        private ButtonItem DirectDeliveryNote;
        private ButtonItem DirectGRN;
        private ButtonItem btnItmDirectDelivery;
        private ButtonItem btnRptPurchaseSummary;
        private ButtonItem btnRptAgeing;
        private ButtonItem btnProductMovement;
        private ButtonItem btnRptItemWiseProfit;
        private ButtonItem BtnItemIssueReport;
        private ButtonItem ProductGroups;
        private ButtonItem btnRptGRN;
        private ButtonItem btnRptPendingDelivery;
        internal Timer tmRssmain;
        private BackgroundWorker BackRssWorker;
        private Timer tmCheckStatus;
        private RibbonPanel ribbonPanel2;
        private RibbonPanel ribbonPanel5;
        private RibbonTabItem AccountTransactions;
        private RibbonTabItem AccountSummary;
        private RibbonBar rbPolicies;
        private ButtonItem btnPaymentVoucher;
        private ButtonItem btnReceiptVoucher;
        private ButtonItem btnContraVoucher;
        private ButtonItem BtnJVoucher;
        private ButtonItem btnPurchaseVoucher;
        private ButtonItem btnDebitNoteVoucher;
        private ButtonItem btnSalesVoucher;
        private ButtonItem btnCreditNoteVoucher;
        private RibbonBar ribbonBar4;
        private ButtonItem btnPayments;
        private ButtonItem btnReceipts;
        private RibbonPanel ribbonPanel9;
        private RibbonBar ribbonBar20;
        private ButtonItem btnTrialBalance;
        private ButtonItem btnGroupSummary;
        private ButtonItem btnGL;
        private ButtonItem BtnDayBook;
        private ButtonItem btnCashBook;
        private ButtonItem btnProfitAndLossSummary;
        private ButtonItem btnBalanceSheetSummary;
        private RibbonTabItem AccountReports;
        private RibbonPanel ribbonPanel10;
        private RibbonBar ribbonBar2;
        private ButtonItem btnProfitnLoss;
        private ButtonItem btnBalancesheet;
        private ButtonItem btnCashFlow;
        private ButtonItem btnFundFlow;
        private ButtonItem btnINcomestmt;
        private ButtonItem btnStockSummary;
        private RibbonTabItem Documents;
        private RibbonBar ribbonBar3;
        private ButtonItem Vendor;
        private ButtonItem Customer;
        private ButtonItem Employee;
        private ButtonItem btnChartofAcconuts;
        private ButtonItem btnOP;
        private ButtonItem btnAccSettings;
        private RibbonBar ribbonBar11;
        private ButtonItem StockRegister;
        private ButtonItem btnNewDocument;
        private RibbonBar ribbonBar12;
        private ButtonItem Document;
        private ButtonItem btnAlert;
        private ItemContainer itemContainer1;
        private LabelItem lblCompany;
        private ButtonItem btnLogout;
        private ItemContainer itemContainer2;
        private LabelItem lblCompanyCurrency;
        private ButtonItem btnVendorMapping;
        private ItemContainer itemContainer3;
        private LabelItem lblFinancialYear;
        private ButtonItem btnExtraChargePayment;
        private ButtonItem btnSalesExtraCharge;
        private ButtonItem btnSalesExtraChargePayment;
        private ButtonItem btnPurExtraChargePayment;
        private ButtonItem btnGroupJV;
        private ButtonItem btnStatementofAccounts;
        private RibbonBar rbProduction;
        private ButtonItem btnProductionTemplate;
        private ButtonItem btnProductionProcess;
        private ButtonItem btnMaterialIssue;
        private ButtonItem btnQC;
        private RibbonPanel ribbonPanel11;
        private RibbonBar ribbonBarPayroll;
        private ButtonItem btnSalaryStructure;
        private ButtonItem btnProcess;
        private ButtonItem btnRelease;
        private RibbonBar ribbonBar7;
        private ButtonItem btnShift;
        private ButtonItem btnWorkPolicy;
        private ButtonItem btnLeavePolicy;
        private ButtonItem btnCalendar;
        private ButtonItem btnVacationPolicy;
        private ButtonItem btnSettlementPolicy;
        private RibbonTabItem rtiPayroll;
        private RibbonBar ribbonBarTask;
        private ButtonItem btnSalaryAdvance;
        private ButtonItem btnVacationEntry;
        private ButtonItem btnSettlementEntry;
        private ButtonItem btnNewAttendance;
        private RibbonPanel ribbonPanel12;
        private RibbonBar ribbonBarEmployee;
        private ButtonItem btnEmployeeReport;
        private ButtonItem btnRptSalaryStructureReport;
        private ButtonItem btnRptAttendance;
        private ButtonItem btnPaymentsReport;
        //private ButtonItem btnLeaveSummaryReport;
        private ButtonItem btnSalaryAdvanceReport;
        private ButtonItem btnPaySlip;
        private RibbonTabItem rtiPayrollReports;
        private ButtonItem btnStockLedger;
        private ButtonItem btnLeaveEntry;
        private ButtonItem btnLeaveStructure;
        private ButtonItem btnLeaveSummaryReport;
        public long lngAlertID = 0;

        #endregion

        public FrmMain()
        {
            // Required for Windows Form Designer support
            InitializeComponent();

            this.objclsBLLMain = new clsBLLMain();

            eOffice2007ColorScheme colorScheme = (eOffice2007ColorScheme)Enum.Parse(typeof(eOffice2007ColorScheme), "Silver");
            ribbonControl.Office2007ColorTable = colorScheme;

            ClsMainSettings.blnProductStatus = true;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevComponents.DotNetBar.SuperTooltipInfo superTooltipInfo1 = new DevComponents.DotNetBar.SuperTooltipInfo();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            DevComponents.DotNetBar.SuperTooltipInfo superTooltipInfo2 = new DevComponents.DotNetBar.SuperTooltipInfo();
            DevComponents.DotNetBar.SuperTooltipInfo superTooltipInfo3 = new DevComponents.DotNetBar.SuperTooltipInfo();
            DevComponents.DotNetBar.SuperTooltipInfo superTooltipInfo4 = new DevComponents.DotNetBar.SuperTooltipInfo();
            DevComponents.DotNetBar.SuperTooltipInfo superTooltipInfo5 = new DevComponents.DotNetBar.SuperTooltipInfo();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.labelStatus = new DevComponents.DotNetBar.LabelItem();
            this.itemContainer13 = new DevComponents.DotNetBar.ItemContainer();
            this.labelPosition = new DevComponents.DotNetBar.LabelItem();
            this.itemContainer1 = new DevComponents.DotNetBar.ItemContainer();
            this.lblCompany = new DevComponents.DotNetBar.LabelItem();
            this.itemContainer2 = new DevComponents.DotNetBar.ItemContainer();
            this.lblCompanyCurrency = new DevComponents.DotNetBar.LabelItem();
            this.itemContainer3 = new DevComponents.DotNetBar.ItemContainer();
            this.lblFinancialYear = new DevComponents.DotNetBar.LabelItem();
            this.ribbonControl = new DevComponents.DotNetBar.RibbonControl();
            this.rpHome = new DevComponents.DotNetBar.RibbonPanel();
            this.rbReports = new DevComponents.DotNetBar.RibbonBar();
            this.btnItmRptStock = new DevComponents.DotNetBar.ButtonItem();
            this.btnItmRptPaymentsAndReceipts = new DevComponents.DotNetBar.ButtonItem();
            this.BtnSalesH = new DevComponents.DotNetBar.ButtonItem();
            this.rbInventory = new DevComponents.DotNetBar.RibbonBar();
            this.btnItmDeliveryNote = new DevComponents.DotNetBar.ButtonItem();
            this.btnItmDirectDelivery = new DevComponents.DotNetBar.ButtonItem();
            this.rbSales = new DevComponents.DotNetBar.RibbonBar();
            this.btnItmPOS = new DevComponents.DotNetBar.ButtonItem();
            this.btnItmReceipts = new DevComponents.DotNetBar.ButtonItem();
            this.HomeBar = new DevComponents.DotNetBar.RibbonBar();
            this.btnItmSuppliers = new DevComponents.DotNetBar.ButtonItem();
            this.btnItmCustomers = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel6 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar13 = new DevComponents.DotNetBar.RibbonBar();
            this.ReportProducts = new DevComponents.DotNetBar.ButtonItem();
            this.ReportEmployee = new DevComponents.DotNetBar.ButtonItem();
            this.ReportsAssociates = new DevComponents.DotNetBar.ButtonItem();
            this.ReportsPurchase = new DevComponents.DotNetBar.ButtonItem();
            this.ReportsSales = new DevComponents.DotNetBar.ButtonItem();
            this.BtnItemIssueReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnPaymentsReceipts = new DevComponents.DotNetBar.ButtonItem();
            this.ReportsStock = new DevComponents.DotNetBar.ButtonItem();
            this.btnExpense = new DevComponents.DotNetBar.ButtonItem();
            this.BtnSummary = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptPurchaseSummary = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptAgeing = new DevComponents.DotNetBar.ButtonItem();
            this.btnProductMovement = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptItemWiseProfit = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptGRN = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptPendingDelivery = new DevComponents.DotNetBar.ButtonItem();
            this.btnStockLedger = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel11 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBarTask = new DevComponents.DotNetBar.RibbonBar();
            this.btnSalaryAdvance = new DevComponents.DotNetBar.ButtonItem();
            this.btnVacationEntry = new DevComponents.DotNetBar.ButtonItem();
            this.btnSettlementEntry = new DevComponents.DotNetBar.ButtonItem();
            this.btnLeaveEntry = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBarPayroll = new DevComponents.DotNetBar.RibbonBar();
            this.btnSalaryStructure = new DevComponents.DotNetBar.ButtonItem();
            this.btnNewAttendance = new DevComponents.DotNetBar.ButtonItem();
            this.btnProcess = new DevComponents.DotNetBar.ButtonItem();
            this.btnRelease = new DevComponents.DotNetBar.ButtonItem();
            this.btnLeaveStructure = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar7 = new DevComponents.DotNetBar.RibbonBar();
            this.btnShift = new DevComponents.DotNetBar.ButtonItem();
            this.btnWorkPolicy = new DevComponents.DotNetBar.ButtonItem();
            this.btnLeavePolicy = new DevComponents.DotNetBar.ButtonItem();
            this.btnCalendar = new DevComponents.DotNetBar.ButtonItem();
            this.btnVacationPolicy = new DevComponents.DotNetBar.ButtonItem();
            this.btnSettlementPolicy = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel2 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar4 = new DevComponents.DotNetBar.RibbonBar();
            this.btnPayments = new DevComponents.DotNetBar.ButtonItem();
            this.btnReceipts = new DevComponents.DotNetBar.ButtonItem();
            this.btnExtraChargePayment = new DevComponents.DotNetBar.ButtonItem();
            this.rbPolicies = new DevComponents.DotNetBar.RibbonBar();
            this.btnPaymentVoucher = new DevComponents.DotNetBar.ButtonItem();
            this.btnReceiptVoucher = new DevComponents.DotNetBar.ButtonItem();
            this.btnContraVoucher = new DevComponents.DotNetBar.ButtonItem();
            this.BtnJVoucher = new DevComponents.DotNetBar.ButtonItem();
            this.btnPurchaseVoucher = new DevComponents.DotNetBar.ButtonItem();
            this.btnDebitNoteVoucher = new DevComponents.DotNetBar.ButtonItem();
            this.btnSalesVoucher = new DevComponents.DotNetBar.ButtonItem();
            this.btnCreditNoteVoucher = new DevComponents.DotNetBar.ButtonItem();
            this.btnGroupJV = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel12 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBarEmployee = new DevComponents.DotNetBar.RibbonBar();
            this.btnEmployeeReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptSalaryStructureReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptAttendance = new DevComponents.DotNetBar.ButtonItem();
            this.btnPaymentsReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnSalaryAdvanceReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnPaySlip = new DevComponents.DotNetBar.ButtonItem();
            this.btnLeaveSummaryReport = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel9 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar2 = new DevComponents.DotNetBar.RibbonBar();
            this.btnProfitnLoss = new DevComponents.DotNetBar.ButtonItem();
            this.btnBalancesheet = new DevComponents.DotNetBar.ButtonItem();
            this.btnCashFlow = new DevComponents.DotNetBar.ButtonItem();
            this.btnFundFlow = new DevComponents.DotNetBar.ButtonItem();
            this.btnINcomestmt = new DevComponents.DotNetBar.ButtonItem();
            this.btnStockSummary = new DevComponents.DotNetBar.ButtonItem();
            this.btnStatementofAccounts = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel5 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar20 = new DevComponents.DotNetBar.RibbonBar();
            this.btnTrialBalance = new DevComponents.DotNetBar.ButtonItem();
            this.btnGroupSummary = new DevComponents.DotNetBar.ButtonItem();
            this.btnGL = new DevComponents.DotNetBar.ButtonItem();
            this.BtnDayBook = new DevComponents.DotNetBar.ButtonItem();
            this.btnCashBook = new DevComponents.DotNetBar.ButtonItem();
            this.btnProfitAndLossSummary = new DevComponents.DotNetBar.ButtonItem();
            this.btnBalanceSheetSummary = new DevComponents.DotNetBar.ButtonItem();
            this.Inventory1 = new DevComponents.DotNetBar.RibbonPanel();
            this.rbProduction = new DevComponents.DotNetBar.RibbonBar();
            this.btnProductionTemplate = new DevComponents.DotNetBar.ButtonItem();
            this.btnMaterialIssue = new DevComponents.DotNetBar.ButtonItem();
            this.btnProductionProcess = new DevComponents.DotNetBar.ButtonItem();
            this.btnQC = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar6 = new DevComponents.DotNetBar.RibbonBar();
            this.Products = new DevComponents.DotNetBar.ButtonItem();
            this.ProductGroups = new DevComponents.DotNetBar.ButtonItem();
            this.OpeningStock = new DevComponents.DotNetBar.ButtonItem();
            this.StockAdjustment = new DevComponents.DotNetBar.ButtonItem();
            this.StockTransfer = new DevComponents.DotNetBar.ButtonItem();
            this.DeliveryNote = new DevComponents.DotNetBar.ButtonItem();
            this.DirectDeliveryNote = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPaymentsInv = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel1 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar3 = new DevComponents.DotNetBar.RibbonBar();
            this.Vendor = new DevComponents.DotNetBar.ButtonItem();
            this.Customer = new DevComponents.DotNetBar.ButtonItem();
            this.btnVendorMapping = new DevComponents.DotNetBar.ButtonItem();
            this.btnChartofAcconuts = new DevComponents.DotNetBar.ButtonItem();
            this.btnOP = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar1 = new DevComponents.DotNetBar.RibbonBar();
            this.NewCompany = new DevComponents.DotNetBar.ButtonItem();
            this.Warehouse = new DevComponents.DotNetBar.ButtonItem();
            this.Currency = new DevComponents.DotNetBar.ButtonItem();
            this.Employee = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel8 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar16 = new DevComponents.DotNetBar.RibbonBar();
            this.About = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel7 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar19 = new DevComponents.DotNetBar.RibbonBar();
            this.CompanySettings = new DevComponents.DotNetBar.ButtonItem();
            this.Uom = new DevComponents.DotNetBar.ButtonItem();
            this.UomConversion = new DevComponents.DotNetBar.ButtonItem();
            this.BtnDiscounts = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPricingScheme = new DevComponents.DotNetBar.ButtonItem();
            this.BtnTermsAndConditions = new DevComponents.DotNetBar.ButtonItem();
            this.AlertSettings = new DevComponents.DotNetBar.ButtonItem();
            this.btnAccSettings = new DevComponents.DotNetBar.ButtonItem();
            this.BackUp = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar15 = new DevComponents.DotNetBar.RibbonBar();
            this.User = new DevComponents.DotNetBar.ButtonItem();
            this.UserRole = new DevComponents.DotNetBar.ButtonItem();
            this.ChangePassword = new DevComponents.DotNetBar.ButtonItem();
            this.BtnHierarchySetting = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar14 = new DevComponents.DotNetBar.RibbonBar();
            this.Configuration = new DevComponents.DotNetBar.ButtonItem();
            this.Email = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel10 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar12 = new DevComponents.DotNetBar.RibbonBar();
            this.Document = new DevComponents.DotNetBar.ButtonItem();
            this.btnAlert = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar11 = new DevComponents.DotNetBar.RibbonBar();
            this.btnNewDocument = new DevComponents.DotNetBar.ButtonItem();
            this.StockRegister = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel4 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar9 = new DevComponents.DotNetBar.RibbonBar();
            this.SalesQuotation = new DevComponents.DotNetBar.ButtonItem();
            this.SalesOrder = new DevComponents.DotNetBar.ButtonItem();
            this.SalesInvoice = new DevComponents.DotNetBar.ButtonItem();
            this.BtnCreditNote = new DevComponents.DotNetBar.ButtonItem();
            this.POS = new DevComponents.DotNetBar.ButtonItem();
            this.Receipts = new DevComponents.DotNetBar.ButtonItem();
            this.btnSalesExtraCharge = new DevComponents.DotNetBar.ButtonItem();
            this.btnSalesExtraChargePayment = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel3 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar5 = new DevComponents.DotNetBar.RibbonBar();
            this.PurchaseQuotation = new DevComponents.DotNetBar.ButtonItem();
            this.PurchaseOrder = new DevComponents.DotNetBar.ButtonItem();
            this.PurchaseInvoice = new DevComponents.DotNetBar.ButtonItem();
            this.GoodsReceiptNote = new DevComponents.DotNetBar.ButtonItem();
            this.DirectGRN = new DevComponents.DotNetBar.ButtonItem();
            this.BtnDebitNote = new DevComponents.DotNetBar.ButtonItem();
            this.Payments = new DevComponents.DotNetBar.ButtonItem();
            this.btnPurExtraChargePayment = new DevComponents.DotNetBar.ButtonItem();
            this.Home = new DevComponents.DotNetBar.RibbonTabItem();
            this.Company = new DevComponents.DotNetBar.RibbonTabItem();
            this.Purchase = new DevComponents.DotNetBar.RibbonTabItem();
            this.Sales = new DevComponents.DotNetBar.RibbonTabItem();
            this.Inventory = new DevComponents.DotNetBar.RibbonTabItem();
            this.TradingReports = new DevComponents.DotNetBar.RibbonTabItem();
            this.rtiPayroll = new DevComponents.DotNetBar.RibbonTabItem();
            this.rtiPayrollReports = new DevComponents.DotNetBar.RibbonTabItem();
            this.AccountTransactions = new DevComponents.DotNetBar.RibbonTabItem();
            this.AccountSummary = new DevComponents.DotNetBar.RibbonTabItem();
            this.AccountReports = new DevComponents.DotNetBar.RibbonTabItem();
            this.Documents = new DevComponents.DotNetBar.RibbonTabItem();
            this.Settings = new DevComponents.DotNetBar.RibbonTabItem();
            this.Help = new DevComponents.DotNetBar.RibbonTabItem();
            this.buttonChangeStyle = new DevComponents.DotNetBar.ButtonItem();
            this.buttonStyleOffice2007Blue = new DevComponents.DotNetBar.ButtonItem();
            this.AppCommandTheme = new DevComponents.DotNetBar.Command(this.components);
            this.buttonStyleOffice2007Black = new DevComponents.DotNetBar.ButtonItem();
            this.buttonStyleOffice2007Silver = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem60 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonStyleCustom = new DevComponents.DotNetBar.ColorPickerDropDown();
            this.buttonFile = new DevComponents.DotNetBar.Office2007StartButton();
            this.btnLogout = new DevComponents.DotNetBar.ButtonItem();
            this.btnHome = new DevComponents.DotNetBar.ButtonItem();
            this.bSales = new DevComponents.DotNetBar.ButtonItem();
            this.qatCustomizeItem1 = new DevComponents.DotNetBar.QatCustomizeItem();
            this.ribbonTabItemGroup1 = new DevComponents.DotNetBar.RibbonTabItemGroup();
            this.AppCommandExit = new DevComponents.DotNetBar.Command(this.components);
            this.AppCommandNew = new DevComponents.DotNetBar.Command(this.components);
            this.buttonItem47 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem48 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem49 = new DevComponents.DotNetBar.ButtonItem();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.comboItem8 = new DevComponents.Editors.ComboItem();
            this.comboItem9 = new DevComponents.Editors.ComboItem();
            this.superTooltip1 = new DevComponents.DotNetBar.SuperTooltip();
            this.AppCmdPurchase = new DevComponents.DotNetBar.Command(this.components);
            this.AppCmdSales = new DevComponents.DotNetBar.Command(this.components);
            this.AppCmdRoleSettings = new DevComponents.DotNetBar.Command(this.components);
            this.bwrAlert = new System.ComponentModel.BackgroundWorker();
            this.tmrAlert = new System.Windows.Forms.Timer(this.components);
            this.AppCommandReports = new DevComponents.DotNetBar.Command(this.components);
            this.dotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.barBottomDockSite = new DevComponents.DotNetBar.DockSite();
            this.barTaskList = new DevComponents.DotNetBar.Bar();
            this.cmnuAlertOptions = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiMarkAsRead = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMarkAsUnRead = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiBlock = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiUnBlock = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFilterOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAll = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiUnread = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRead = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiBlocked = new System.Windows.Forms.ToolStripMenuItem();
            this.PanelDockContainer1 = new DevComponents.DotNetBar.PanelDockContainer();
            this.pctNoalerts = new System.Windows.Forms.PictureBox();
            this.dgvAlerts = new System.Windows.Forms.DataGridView();
            this.colAlertMsg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnAlertSettingsID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnReferenceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnStatusID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnAlertID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelDockContainer2 = new DevComponents.DotNetBar.PanelDockContainer();
            this.WebRss = new System.Windows.Forms.WebBrowser();
            this.dockRss = new DevComponents.DotNetBar.DockContainerItem();
            this.dockAlertList = new DevComponents.DotNetBar.DockContainerItem();
            this.barLeftDockSite = new DevComponents.DotNetBar.DockSite();
            this.barRightDockSite = new DevComponents.DotNetBar.DockSite();
            this.dockSite4 = new DevComponents.DotNetBar.DockSite();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.dockSite2 = new DevComponents.DotNetBar.DockSite();
            this.dockSite3 = new DevComponents.DotNetBar.DockSite();
            this.barTopDockSite = new DevComponents.DotNetBar.DockSite();
            this.AppCmdInventory = new DevComponents.DotNetBar.Command(this.components);
            this.tabStrip1 = new DevComponents.DotNetBar.TabStrip();
            this.tmRssmain = new System.Windows.Forms.Timer(this.components);
            this.BackRssWorker = new System.ComponentModel.BackgroundWorker();
            this.tmCheckStatus = new System.Windows.Forms.Timer(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.ribbonControl.SuspendLayout();
            this.rpHome.SuspendLayout();
            this.ribbonPanel6.SuspendLayout();
            this.ribbonPanel11.SuspendLayout();
            this.ribbonPanel2.SuspendLayout();
            this.ribbonPanel12.SuspendLayout();
            this.ribbonPanel9.SuspendLayout();
            this.ribbonPanel5.SuspendLayout();
            this.Inventory1.SuspendLayout();
            this.ribbonPanel1.SuspendLayout();
            this.ribbonPanel8.SuspendLayout();
            this.ribbonPanel7.SuspendLayout();
            this.ribbonPanel10.SuspendLayout();
            this.ribbonPanel4.SuspendLayout();
            this.ribbonPanel3.SuspendLayout();
            this.barBottomDockSite.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barTaskList)).BeginInit();
            this.barTaskList.SuspendLayout();
            this.cmnuAlertOptions.SuspendLayout();
            this.PanelDockContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctNoalerts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlerts)).BeginInit();
            this.PanelDockContainer2.SuspendLayout();
            this.dockSite4.SuspendLayout();
            this.SuspendLayout();
            // 
            // bar1
            // 
            this.bar1.AccessibleDescription = "DotNetBar Bar (bar1)";
            this.bar1.AccessibleName = "DotNetBar Bar";
            this.bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.StatusBar;
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Bottom;
            this.bar1.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.ResizeHandle;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelStatus,
            this.itemContainer13,
            this.itemContainer1,
            this.itemContainer2,
            this.itemContainer3});
            this.bar1.ItemSpacing = 2;
            this.bar1.Location = new System.Drawing.Point(0, 1);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(1290, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.bar1.TabIndex = 7;
            this.bar1.TabStop = false;
            this.bar1.Text = "barStatus";
            // 
            // labelStatus
            // 
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.PaddingLeft = 2;
            this.labelStatus.PaddingRight = 2;
            this.labelStatus.SingleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.labelStatus.Stretch = true;
            // 
            // itemContainer13
            // 
            // 
            // 
            // 
            this.itemContainer13.BackgroundStyle.Class = "Office2007StatusBarBackground2";
            this.itemContainer13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer13.Name = "itemContainer13";
            this.itemContainer13.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelPosition});
            // 
            // labelPosition
            // 
            this.labelPosition.Name = "labelPosition";
            this.labelPosition.PaddingLeft = 2;
            this.labelPosition.PaddingRight = 2;
            this.labelPosition.SingleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.labelPosition.Tooltip = "User";
            this.labelPosition.Width = 100;
            // 
            // itemContainer1
            // 
            // 
            // 
            // 
            this.itemContainer1.BackgroundStyle.Class = "Office2007StatusBarBackground2";
            this.itemContainer1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer1.Name = "itemContainer1";
            this.itemContainer1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lblCompany});
            // 
            // lblCompany
            // 
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.PaddingLeft = 2;
            this.lblCompany.PaddingRight = 2;
            this.lblCompany.SingleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.lblCompany.Tooltip = "Company";
            this.lblCompany.Width = 200;
            // 
            // itemContainer2
            // 
            // 
            // 
            // 
            this.itemContainer2.BackgroundStyle.Class = "Office2007StatusBarBackground2";
            this.itemContainer2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer2.Name = "itemContainer2";
            this.itemContainer2.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lblCompanyCurrency});
            // 
            // lblCompanyCurrency
            // 
            this.lblCompanyCurrency.Name = "lblCompanyCurrency";
            this.lblCompanyCurrency.PaddingLeft = 2;
            this.lblCompanyCurrency.PaddingRight = 2;
            this.lblCompanyCurrency.SingleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.lblCompanyCurrency.Tooltip = "Company";
            this.lblCompanyCurrency.Width = 60;
            // 
            // itemContainer3
            // 
            // 
            // 
            // 
            this.itemContainer3.BackgroundStyle.Class = "Office2007StatusBarBackground2";
            this.itemContainer3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer3.Name = "itemContainer3";
            this.itemContainer3.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lblFinancialYear});
            // 
            // lblFinancialYear
            // 
            this.lblFinancialYear.Name = "lblFinancialYear";
            this.lblFinancialYear.PaddingLeft = 2;
            this.lblFinancialYear.PaddingRight = 2;
            this.lblFinancialYear.SingleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.lblFinancialYear.Tooltip = "Company";
            this.lblFinancialYear.Width = 200;
            // 
            // ribbonControl
            // 
            this.ribbonControl.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.ribbonControl.BackgroundStyle.Class = "";
            this.ribbonControl.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonControl.CaptionVisible = true;
            this.ribbonControl.Controls.Add(this.rpHome);
            this.ribbonControl.Controls.Add(this.ribbonPanel6);
            this.ribbonControl.Controls.Add(this.ribbonPanel11);
            this.ribbonControl.Controls.Add(this.ribbonPanel2);
            this.ribbonControl.Controls.Add(this.ribbonPanel12);
            this.ribbonControl.Controls.Add(this.ribbonPanel9);
            this.ribbonControl.Controls.Add(this.ribbonPanel5);
            this.ribbonControl.Controls.Add(this.Inventory1);
            this.ribbonControl.Controls.Add(this.ribbonPanel1);
            this.ribbonControl.Controls.Add(this.ribbonPanel8);
            this.ribbonControl.Controls.Add(this.ribbonPanel7);
            this.ribbonControl.Controls.Add(this.ribbonPanel10);
            this.ribbonControl.Controls.Add(this.ribbonPanel4);
            this.ribbonControl.Controls.Add(this.ribbonPanel3);
            this.ribbonControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonControl.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.Home,
            this.Company,
            this.Purchase,
            this.Sales,
            this.Inventory,
            this.TradingReports,
            this.rtiPayroll,
            this.rtiPayrollReports,
            this.AccountTransactions,
            this.AccountSummary,
            this.AccountReports,
            this.Documents,
            this.Settings,
            this.Help,
            this.buttonChangeStyle});
            this.ribbonControl.KeyTipsFont = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonControl.Location = new System.Drawing.Point(5, 1);
            this.ribbonControl.MdiSystemItemVisible = false;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.Office2007ColorTable = DevComponents.DotNetBar.Rendering.eOffice2007ColorScheme.VistaGlass;
            this.ribbonControl.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.ribbonControl.QuickToolbarItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonFile,
            this.btnHome,
            this.bSales,
            this.qatCustomizeItem1});
            this.ribbonControl.Size = new System.Drawing.Size(1290, 149);
            this.ribbonControl.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonControl.SystemText.MaximizeRibbonText = "&Maximize the Ribbon";
            this.ribbonControl.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon";
            this.ribbonControl.SystemText.QatAddItemText = "&Add to Quick Access Toolbar";
            this.ribbonControl.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>";
            this.ribbonControl.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar...";
            this.ribbonControl.SystemText.QatDialogAddButton = "&Add >>";
            this.ribbonControl.SystemText.QatDialogCancelButton = "Cancel";
            this.ribbonControl.SystemText.QatDialogCaption = "Customize Quick Access Toolbar";
            this.ribbonControl.SystemText.QatDialogCategoriesLabel = "&Choose commands from:";
            this.ribbonControl.SystemText.QatDialogOkButton = "OK";
            this.ribbonControl.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl.SystemText.QatDialogRemoveButton = "&Remove";
            this.ribbonControl.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon";
            this.ribbonControl.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar";
            this.ribbonControl.TabGroupHeight = 14;
            this.ribbonControl.TabGroups.AddRange(new DevComponents.DotNetBar.RibbonTabItemGroup[] {
            this.ribbonTabItemGroup1});
            this.ribbonControl.TabGroupsVisible = true;
            this.ribbonControl.TabIndex = 8;
            this.ribbonControl.Text = "Task";
            this.ribbonControl.BeforeRibbonPanelPopupClose += new DevComponents.DotNetBar.RibbonPopupCloseEventHandler(this.ribbonControl1_BeforeRibbonPanelPopupClose);
            // 
            // rpHome
            // 
            this.rpHome.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.rpHome.Controls.Add(this.rbReports);
            this.rpHome.Controls.Add(this.rbInventory);
            this.rpHome.Controls.Add(this.rbSales);
            this.rpHome.Controls.Add(this.HomeBar);
            this.rpHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpHome.Location = new System.Drawing.Point(0, 57);
            this.rpHome.Name = "rpHome";
            this.rpHome.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.rpHome.Size = new System.Drawing.Size(1290, 90);
            // 
            // 
            // 
            this.rpHome.Style.Class = "";
            this.rpHome.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rpHome.StyleMouseDown.Class = "";
            this.rpHome.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rpHome.StyleMouseOver.Class = "";
            this.rpHome.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rpHome.TabIndex = 10;
            this.rpHome.Visible = true;
            // 
            // rbReports
            // 
            this.rbReports.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbReports.BackgroundMouseOverStyle.Class = "";
            this.rbReports.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbReports.BackgroundStyle.Class = "";
            this.rbReports.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbReports.ContainerControlProcessDialogKey = true;
            this.rbReports.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbReports.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnItmRptStock,
            this.btnItmRptPaymentsAndReceipts,
            this.BtnSalesH});
            this.rbReports.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.rbReports.Location = new System.Drawing.Point(578, 0);
            this.rbReports.Name = "rbReports";
            this.rbReports.Size = new System.Drawing.Size(290, 87);
            this.rbReports.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.rbReports.TabIndex = 3;
            this.rbReports.Text = "Reports";
            // 
            // 
            // 
            this.rbReports.TitleStyle.Class = "";
            this.rbReports.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbReports.TitleStyleMouseOver.Class = "";
            this.rbReports.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbReports.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnItmRptStock
            // 
            this.btnItmRptStock.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnItmRptStock.FixedSize = new System.Drawing.Size(90, 10);
            this.btnItmRptStock.Image = global::MyBooksERP.Properties.Resources.Stock_Reports1;
            this.btnItmRptStock.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnItmRptStock.Name = "btnItmRptStock";
            this.btnItmRptStock.SubItemsExpandWidth = 14;
            this.btnItmRptStock.Text = "&Stock";
            this.btnItmRptStock.Tooltip = "Stock Reports";
            this.btnItmRptStock.Click += new System.EventHandler(this.ReportsStock_Click);
            // 
            // btnItmRptPaymentsAndReceipts
            // 
            this.btnItmRptPaymentsAndReceipts.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnItmRptPaymentsAndReceipts.FixedSize = new System.Drawing.Size(90, 10);
            this.btnItmRptPaymentsAndReceipts.Image = global::MyBooksERP.Properties.Resources.Payments___Receipts1;
            this.btnItmRptPaymentsAndReceipts.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnItmRptPaymentsAndReceipts.Name = "btnItmRptPaymentsAndReceipts";
            this.btnItmRptPaymentsAndReceipts.SubItemsExpandWidth = 14;
            this.btnItmRptPaymentsAndReceipts.Text = "&Payments && Receipts";
            this.btnItmRptPaymentsAndReceipts.Tooltip = "Payments && Receipts";
            this.btnItmRptPaymentsAndReceipts.Click += new System.EventHandler(this.btnPaymentsReceipts_Click);
            // 
            // BtnSalesH
            // 
            this.BtnSalesH.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnSalesH.FixedSize = new System.Drawing.Size(90, 10);
            this.BtnSalesH.Image = global::MyBooksERP.Properties.Resources.Sales_Reports1;
            this.BtnSalesH.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnSalesH.Name = "BtnSalesH";
            this.BtnSalesH.SubItemsExpandWidth = 14;
            this.BtnSalesH.Text = "S&ales";
            this.BtnSalesH.Tooltip = "Sales Reports";
            this.BtnSalesH.Click += new System.EventHandler(this.ReportsSales_Click);
            // 
            // rbInventory
            // 
            this.rbInventory.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbInventory.BackgroundMouseOverStyle.Class = "";
            this.rbInventory.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbInventory.BackgroundStyle.Class = "";
            this.rbInventory.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbInventory.ContainerControlProcessDialogKey = true;
            this.rbInventory.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbInventory.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnItmDeliveryNote,
            this.btnItmDirectDelivery});
            this.rbInventory.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.rbInventory.Location = new System.Drawing.Point(382, 0);
            this.rbInventory.Name = "rbInventory";
            this.rbInventory.Size = new System.Drawing.Size(196, 87);
            this.rbInventory.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.rbInventory.TabIndex = 2;
            this.rbInventory.Text = "Inventory";
            // 
            // 
            // 
            this.rbInventory.TitleStyle.Class = "";
            this.rbInventory.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbInventory.TitleStyleMouseOver.Class = "";
            this.rbInventory.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbInventory.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnItmDeliveryNote
            // 
            this.btnItmDeliveryNote.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnItmDeliveryNote.FixedSize = new System.Drawing.Size(90, 10);
            this.btnItmDeliveryNote.Image = global::MyBooksERP.Properties.Resources.Item_Issue_Delivery_Note_;
            this.btnItmDeliveryNote.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnItmDeliveryNote.Name = "btnItmDeliveryNote";
            this.btnItmDeliveryNote.SubItemsExpandWidth = 14;
            this.btnItmDeliveryNote.Text = "Delivery &Note";
            this.btnItmDeliveryNote.Tooltip = "Delivery Note";
            this.btnItmDeliveryNote.Click += new System.EventHandler(this.ItemIssue_Click);
            // 
            // btnItmDirectDelivery
            // 
            this.btnItmDirectDelivery.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnItmDirectDelivery.FixedSize = new System.Drawing.Size(90, 10);
            this.btnItmDirectDelivery.Image = global::MyBooksERP.Properties.Resources.Item_Issue_Delivery_Note_;
            this.btnItmDirectDelivery.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnItmDirectDelivery.Name = "btnItmDirectDelivery";
            this.btnItmDirectDelivery.SubItemsExpandWidth = 14;
            this.btnItmDirectDelivery.Text = "Direct &Delivery &Note";
            this.btnItmDirectDelivery.Tooltip = "Delivery Note";
            this.btnItmDirectDelivery.Click += new System.EventHandler(this.buttonItem1_Click);
            // 
            // rbSales
            // 
            this.rbSales.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbSales.BackgroundMouseOverStyle.Class = "";
            this.rbSales.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbSales.BackgroundStyle.Class = "";
            this.rbSales.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbSales.ContainerControlProcessDialogKey = true;
            this.rbSales.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbSales.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnItmPOS,
            this.btnItmReceipts});
            this.rbSales.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.rbSales.Location = new System.Drawing.Point(194, 0);
            this.rbSales.Name = "rbSales";
            this.rbSales.Size = new System.Drawing.Size(188, 87);
            this.rbSales.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.rbSales.TabIndex = 1;
            this.rbSales.Text = "Sales";
            // 
            // 
            // 
            this.rbSales.TitleStyle.Class = "";
            this.rbSales.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbSales.TitleStyleMouseOver.Class = "";
            this.rbSales.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbSales.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnItmPOS
            // 
            this.btnItmPOS.FixedSize = new System.Drawing.Size(90, 10);
            this.btnItmPOS.GlobalName = "POS";
            this.btnItmPOS.Image = global::MyBooksERP.Properties.Resources.Point_of_sale;
            this.btnItmPOS.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnItmPOS.Name = "btnItmPOS";
            this.btnItmPOS.SubItemsExpandWidth = 14;
            this.btnItmPOS.Text = "&Point Of Sales";
            this.btnItmPOS.Tooltip = "Point Of Sales";
            this.btnItmPOS.Click += new System.EventHandler(this.POS_Click);
            // 
            // btnItmReceipts
            // 
            this.btnItmReceipts.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnItmReceipts.FixedSize = new System.Drawing.Size(90, 10);
            this.btnItmReceipts.GlobalName = "Receipts";
            this.btnItmReceipts.Image = global::MyBooksERP.Properties.Resources.Receipts;
            this.btnItmReceipts.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnItmReceipts.Name = "btnItmReceipts";
            this.btnItmReceipts.SubItemsExpandWidth = 14;
            this.btnItmReceipts.Text = "&Receipts";
            this.btnItmReceipts.Tooltip = "Receipts";
            this.btnItmReceipts.Click += new System.EventHandler(this.Receipts_Click);
            // 
            // HomeBar
            // 
            this.HomeBar.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.HomeBar.BackgroundMouseOverStyle.Class = "";
            this.HomeBar.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.HomeBar.BackgroundStyle.Class = "";
            this.HomeBar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.HomeBar.ContainerControlProcessDialogKey = true;
            this.HomeBar.Dock = System.Windows.Forms.DockStyle.Left;
            this.HomeBar.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnItmSuppliers,
            this.btnItmCustomers});
            this.HomeBar.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.HomeBar.Location = new System.Drawing.Point(3, 0);
            this.HomeBar.Name = "HomeBar";
            this.HomeBar.Size = new System.Drawing.Size(191, 87);
            this.HomeBar.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            superTooltipInfo1.FooterText = "test";
            superTooltipInfo1.HeaderText = "Easy View";
            this.superTooltip1.SetSuperTooltip(this.HomeBar, superTooltipInfo1);
            this.HomeBar.TabIndex = 0;
            this.HomeBar.Text = "Home";
            // 
            // 
            // 
            this.HomeBar.TitleStyle.Class = "";
            this.HomeBar.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.HomeBar.TitleStyleMouseOver.Class = "";
            this.HomeBar.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.HomeBar.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnItmSuppliers
            // 
            this.btnItmSuppliers.FixedSize = new System.Drawing.Size(90, 10);
            this.btnItmSuppliers.GlobalName = "Vendor";
            this.btnItmSuppliers.Image = global::MyBooksERP.Properties.Resources.Supplier;
            this.btnItmSuppliers.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnItmSuppliers.Name = "btnItmSuppliers";
            this.btnItmSuppliers.Text = "Supplier";
            this.btnItmSuppliers.Tooltip = "Supplier";
            this.btnItmSuppliers.Click += new System.EventHandler(this.Vendor_Click);
            // 
            // btnItmCustomers
            // 
            this.btnItmCustomers.FixedSize = new System.Drawing.Size(90, 10);
            this.btnItmCustomers.GlobalName = "Customer";
            this.btnItmCustomers.Image = ((System.Drawing.Image)(resources.GetObject("btnItmCustomers.Image")));
            this.btnItmCustomers.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnItmCustomers.Name = "btnItmCustomers";
            this.btnItmCustomers.Text = "Customer";
            this.btnItmCustomers.Tooltip = "Customer";
            this.btnItmCustomers.Click += new System.EventHandler(this.Customer_Click);
            // 
            // ribbonPanel6
            // 
            this.ribbonPanel6.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel6.Controls.Add(this.ribbonBar13);
            this.ribbonPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel6.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel6.Name = "ribbonPanel6";
            this.ribbonPanel6.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel6.Size = new System.Drawing.Size(1290, 90);
            // 
            // 
            // 
            this.ribbonPanel6.Style.Class = "";
            this.ribbonPanel6.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel6.StyleMouseDown.Class = "";
            this.ribbonPanel6.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel6.StyleMouseOver.Class = "";
            this.ribbonPanel6.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel6.TabIndex = 7;
            this.ribbonPanel6.Visible = false;
            // 
            // ribbonBar13
            // 
            this.ribbonBar13.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar13.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar13.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar13.BackgroundStyle.Class = "";
            this.ribbonBar13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar13.ContainerControlProcessDialogKey = true;
            this.ribbonBar13.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar13.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ReportProducts,
            this.ReportEmployee,
            this.ReportsAssociates,
            this.ReportsPurchase,
            this.ReportsSales,
            this.BtnItemIssueReport,
            this.btnPaymentsReceipts,
            this.ReportsStock,
            this.btnExpense,
            this.BtnSummary,
            this.btnRptPurchaseSummary,
            this.btnRptAgeing,
            this.btnProductMovement,
            this.btnRptItemWiseProfit,
            this.btnRptGRN,
            this.btnRptPendingDelivery,
            this.btnStockLedger});
            this.ribbonBar13.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar13.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar13.Name = "ribbonBar13";
            this.ribbonBar13.Size = new System.Drawing.Size(1296, 87);
            this.ribbonBar13.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar13.TabIndex = 0;
            this.ribbonBar13.Text = "Reports";
            // 
            // 
            // 
            this.ribbonBar13.TitleStyle.Class = "";
            this.ribbonBar13.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar13.TitleStyleMouseOver.Class = "";
            this.ribbonBar13.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar13.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // ReportProducts
            // 
            this.ReportProducts.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.ReportProducts.FixedSize = new System.Drawing.Size(70, 10);
            this.ReportProducts.Image = global::MyBooksERP.Properties.Resources.Product_Reports1;
            this.ReportProducts.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ReportProducts.Name = "ReportProducts";
            this.ReportProducts.SubItemsExpandWidth = 14;
            this.ReportProducts.Text = "&Products";
            this.ReportProducts.Tooltip = "Raw Materials & Products Reports";
            this.ReportProducts.Click += new System.EventHandler(this.ReportProducts_Click);
            // 
            // ReportEmployee
            // 
            this.ReportEmployee.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.ReportEmployee.FixedSize = new System.Drawing.Size(70, 10);
            this.ReportEmployee.Image = global::MyBooksERP.Properties.Resources.Employee_Reports1;
            this.ReportEmployee.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ReportEmployee.Name = "ReportEmployee";
            this.ReportEmployee.SubItemsExpandWidth = 14;
            this.ReportEmployee.Text = "&Employee";
            this.ReportEmployee.Tooltip = "Employee Reports";
            this.ReportEmployee.Click += new System.EventHandler(this.ReportEmployee_Click);
            // 
            // ReportsAssociates
            // 
            this.ReportsAssociates.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.ReportsAssociates.FixedSize = new System.Drawing.Size(80, 10);
            this.ReportsAssociates.Image = global::MyBooksERP.Properties.Resources.Associates__Reports1;
            this.ReportsAssociates.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ReportsAssociates.Name = "ReportsAssociates";
            this.ReportsAssociates.SubItemsExpandWidth = 14;
            this.ReportsAssociates.Text = "&Associates";
            this.ReportsAssociates.Tooltip = "Customers & Suppliers Reports";
            this.ReportsAssociates.Click += new System.EventHandler(this.ReportsAssociates_Click);
            // 
            // ReportsPurchase
            // 
            this.ReportsPurchase.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.ReportsPurchase.FixedSize = new System.Drawing.Size(80, 10);
            this.ReportsPurchase.Image = global::MyBooksERP.Properties.Resources.Purchase_Reports1;
            this.ReportsPurchase.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ReportsPurchase.Name = "ReportsPurchase";
            this.ReportsPurchase.SubItemsExpandWidth = 14;
            this.ReportsPurchase.Text = "&Purchase";
            this.ReportsPurchase.Tooltip = "Purchase Reports";
            this.ReportsPurchase.Click += new System.EventHandler(this.ReportsPurchase_Click);
            // 
            // ReportsSales
            // 
            this.ReportsSales.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.ReportsSales.FixedSize = new System.Drawing.Size(70, 10);
            this.ReportsSales.Image = global::MyBooksERP.Properties.Resources.Sales_Reports1;
            this.ReportsSales.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ReportsSales.Name = "ReportsSales";
            this.ReportsSales.SubItemsExpandWidth = 14;
            this.ReportsSales.Text = "&Sales";
            this.ReportsSales.Tooltip = "Sales Reports";
            this.ReportsSales.Click += new System.EventHandler(this.ReportsSales_Click);
            // 
            // BtnItemIssueReport
            // 
            this.BtnItemIssueReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnItemIssueReport.FixedSize = new System.Drawing.Size(80, 10);
            this.BtnItemIssueReport.Image = global::MyBooksERP.Properties.Resources.Delivery_Reports1;
            this.BtnItemIssueReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnItemIssueReport.Name = "BtnItemIssueReport";
            this.BtnItemIssueReport.SubItemsExpandWidth = 14;
            this.BtnItemIssueReport.Text = "Delivery Note";
            this.BtnItemIssueReport.Tooltip = "Delivery Note Report";
            this.BtnItemIssueReport.Click += new System.EventHandler(this.BtnItemIssueReport_Click_1);
            // 
            // btnPaymentsReceipts
            // 
            this.btnPaymentsReceipts.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPaymentsReceipts.FixedSize = new System.Drawing.Size(80, 10);
            this.btnPaymentsReceipts.Image = global::MyBooksERP.Properties.Resources.Payments___Receipts1;
            this.btnPaymentsReceipts.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPaymentsReceipts.Name = "btnPaymentsReceipts";
            this.btnPaymentsReceipts.SubItemsExpandWidth = 14;
            this.btnPaymentsReceipts.Text = "&Paymen&ts && Receipts";
            this.btnPaymentsReceipts.Tooltip = "Payments & Receipts Reports";
            this.btnPaymentsReceipts.Click += new System.EventHandler(this.btnPaymentsReceipts_Click);
            // 
            // ReportsStock
            // 
            this.ReportsStock.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.ReportsStock.FixedSize = new System.Drawing.Size(70, 10);
            this.ReportsStock.Image = global::MyBooksERP.Properties.Resources.Stock_Reports1;
            this.ReportsStock.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ReportsStock.Name = "ReportsStock";
            this.ReportsStock.SubItemsExpandWidth = 14;
            this.ReportsStock.Text = "St&ock";
            this.ReportsStock.Tooltip = "Stock Reports";
            this.ReportsStock.Click += new System.EventHandler(this.ReportsStock_Click);
            // 
            // btnExpense
            // 
            this.btnExpense.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnExpense.FixedSize = new System.Drawing.Size(80, 10);
            this.btnExpense.Image = global::MyBooksERP.Properties.Resources.ExpenseReports1;
            this.btnExpense.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnExpense.Name = "btnExpense";
            this.btnExpense.SubItemsExpandWidth = 14;
            this.btnExpense.Text = "Expense";
            this.btnExpense.Tooltip = "Expense";
            this.btnExpense.Click += new System.EventHandler(this.btnExpense_Click);
            // 
            // BtnSummary
            // 
            this.BtnSummary.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnSummary.FixedSize = new System.Drawing.Size(80, 10);
            this.BtnSummary.Image = global::MyBooksERP.Properties.Resources.Summary;
            this.BtnSummary.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnSummary.Name = "BtnSummary";
            this.BtnSummary.SubItemsExpandWidth = 14;
            this.BtnSummary.Text = "&Inventory Summary";
            this.BtnSummary.Tooltip = "Inventory Summary Reports";
            this.BtnSummary.Click += new System.EventHandler(this.BtnSummary_Click);
            // 
            // btnRptPurchaseSummary
            // 
            this.btnRptPurchaseSummary.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptPurchaseSummary.FixedSize = new System.Drawing.Size(80, 10);
            this.btnRptPurchaseSummary.Image = global::MyBooksERP.Properties.Resources.Purchase_Reports1;
            this.btnRptPurchaseSummary.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptPurchaseSummary.Name = "btnRptPurchaseSummary";
            this.btnRptPurchaseSummary.SubItemsExpandWidth = 14;
            this.btnRptPurchaseSummary.Text = "Purchase Summary";
            this.btnRptPurchaseSummary.Tooltip = "Purchase Summary Report";
            this.btnRptPurchaseSummary.Click += new System.EventHandler(this.btnRptPurchaseSummary_Click);
            // 
            // btnRptAgeing
            // 
            this.btnRptAgeing.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptAgeing.FixedSize = new System.Drawing.Size(70, 10);
            this.btnRptAgeing.Image = global::MyBooksERP.Properties.Resources.Expense1;
            this.btnRptAgeing.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptAgeing.Name = "btnRptAgeing";
            this.btnRptAgeing.SubItemsExpandWidth = 14;
            this.btnRptAgeing.Text = "Ageing ";
            this.btnRptAgeing.Tooltip = "Ageing Report";
            this.btnRptAgeing.Click += new System.EventHandler(this.btnRptAgeing_Click);
            // 
            // btnProductMovement
            // 
            this.btnProductMovement.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnProductMovement.FixedSize = new System.Drawing.Size(80, 10);
            this.btnProductMovement.Image = ((System.Drawing.Image)(resources.GetObject("btnProductMovement.Image")));
            this.btnProductMovement.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnProductMovement.Name = "btnProductMovement";
            this.btnProductMovement.SubItemsExpandWidth = 14;
            this.btnProductMovement.Text = "Product Movement";
            this.btnProductMovement.Tooltip = "Product Movement Report";
            this.btnProductMovement.Click += new System.EventHandler(this.btnProductMovement_Click);
            // 
            // btnRptItemWiseProfit
            // 
            this.btnRptItemWiseProfit.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptItemWiseProfit.FixedSize = new System.Drawing.Size(80, 10);
            this.btnRptItemWiseProfit.Image = ((System.Drawing.Image)(resources.GetObject("btnRptItemWiseProfit.Image")));
            this.btnRptItemWiseProfit.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptItemWiseProfit.Name = "btnRptItemWiseProfit";
            this.btnRptItemWiseProfit.SubItemsExpandWidth = 14;
            this.btnRptItemWiseProfit.Text = "Item Wise Profit";
            this.btnRptItemWiseProfit.Tooltip = "Item Wise Profit Report";
            this.btnRptItemWiseProfit.Click += new System.EventHandler(this.btnRptItemWiseProfit_Click);
            // 
            // btnRptGRN
            // 
            this.btnRptGRN.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptGRN.FixedSize = new System.Drawing.Size(70, 10);
            this.btnRptGRN.Image = ((System.Drawing.Image)(resources.GetObject("btnRptGRN.Image")));
            this.btnRptGRN.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptGRN.Name = "btnRptGRN";
            this.btnRptGRN.SubItemsExpandWidth = 14;
            this.btnRptGRN.Text = "Direct GRN ";
            this.btnRptGRN.Tooltip = "Direct GRN Report";
            this.btnRptGRN.Click += new System.EventHandler(this.btnrRptGRN_Click);
            // 
            // btnRptPendingDelivery
            // 
            this.btnRptPendingDelivery.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptPendingDelivery.FixedSize = new System.Drawing.Size(70, 10);
            this.btnRptPendingDelivery.Image = ((System.Drawing.Image)(resources.GetObject("btnRptPendingDelivery.Image")));
            this.btnRptPendingDelivery.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptPendingDelivery.Name = "btnRptPendingDelivery";
            this.btnRptPendingDelivery.SubItemsExpandWidth = 14;
            this.btnRptPendingDelivery.Text = "Direct Delivery";
            this.btnRptPendingDelivery.Tooltip = "Direct  Delivery Report";
            this.btnRptPendingDelivery.Click += new System.EventHandler(this.btnRptPendingDelivery_Click);
            // 
            // btnStockLedger
            // 
            this.btnStockLedger.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnStockLedger.FixedSize = new System.Drawing.Size(70, 10);
            this.btnStockLedger.Image = global::MyBooksERP.Properties.Resources.Stock_Book1;
            this.btnStockLedger.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnStockLedger.Name = "btnStockLedger";
            this.btnStockLedger.SubItemsExpandWidth = 14;
            this.btnStockLedger.Text = "Stock Ledger";
            this.btnStockLedger.Tooltip = "Stock Ledger";
            this.btnStockLedger.Click += new System.EventHandler(this.btnStockLedger_Click);
            // 
            // ribbonPanel11
            // 
            this.ribbonPanel11.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel11.Controls.Add(this.ribbonBarTask);
            this.ribbonPanel11.Controls.Add(this.ribbonBarPayroll);
            this.ribbonPanel11.Controls.Add(this.ribbonBar7);
            this.ribbonPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel11.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel11.Name = "ribbonPanel11";
            this.ribbonPanel11.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel11.Size = new System.Drawing.Size(1290, 90);
            // 
            // 
            // 
            this.ribbonPanel11.Style.Class = "";
            this.ribbonPanel11.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel11.StyleMouseDown.Class = "";
            this.ribbonPanel11.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel11.StyleMouseOver.Class = "";
            this.ribbonPanel11.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel11.TabIndex = 15;
            this.ribbonPanel11.Visible = false;
            // 
            // ribbonBarTask
            // 
            this.ribbonBarTask.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBarTask.BackgroundMouseOverStyle.Class = "";
            this.ribbonBarTask.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBarTask.BackgroundStyle.Class = "";
            this.ribbonBarTask.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBarTask.ContainerControlProcessDialogKey = true;
            this.ribbonBarTask.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBarTask.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSalaryAdvance,
            this.btnVacationEntry,
            this.btnSettlementEntry,
            this.btnLeaveEntry});
            this.ribbonBarTask.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBarTask.Location = new System.Drawing.Point(939, 0);
            this.ribbonBarTask.Name = "ribbonBarTask";
            this.ribbonBarTask.Size = new System.Drawing.Size(359, 87);
            this.ribbonBarTask.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBarTask.TabIndex = 9;
            this.ribbonBarTask.Text = "Task";
            // 
            // 
            // 
            this.ribbonBarTask.TitleStyle.Class = "";
            this.ribbonBarTask.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBarTask.TitleStyleMouseOver.Class = "";
            this.ribbonBarTask.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBarTask.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnSalaryAdvance
            // 
            this.btnSalaryAdvance.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSalaryAdvance.FixedSize = new System.Drawing.Size(85, 10);
            this.btnSalaryAdvance.Image = global::MyBooksERP.Properties.Resources.Salary_Advance2;
            this.btnSalaryAdvance.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSalaryAdvance.Name = "btnSalaryAdvance";
            this.btnSalaryAdvance.SubItemsExpandWidth = 14;
            this.btnSalaryAdvance.Text = "&Salary Advance";
            this.btnSalaryAdvance.Tooltip = "Salary Advance";
            this.btnSalaryAdvance.Click += new System.EventHandler(this.btnSalaryAdvance_Click);
            // 
            // btnVacationEntry
            // 
            this.btnVacationEntry.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnVacationEntry.FixedSize = new System.Drawing.Size(80, 10);
            this.btnVacationEntry.Image = global::MyBooksERP.Properties.Resources.vacation_process;
            this.btnVacationEntry.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnVacationEntry.Name = "btnVacationEntry";
            this.btnVacationEntry.SubItemsExpandWidth = 14;
            this.btnVacationEntry.Text = "Vacation Entry";
            this.btnVacationEntry.Tooltip = "Vacation Entry";
            this.btnVacationEntry.Click += new System.EventHandler(this.btnVacationEntry_Click);
            // 
            // btnSettlementEntry
            // 
            this.btnSettlementEntry.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSettlementEntry.FixedSize = new System.Drawing.Size(85, 10);
            this.btnSettlementEntry.Image = global::MyBooksERP.Properties.Resources.SettlementEntry;
            this.btnSettlementEntry.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSettlementEntry.Name = "btnSettlementEntry";
            this.btnSettlementEntry.SubItemsExpandWidth = 14;
            this.btnSettlementEntry.Text = "Settlement Entry";
            this.btnSettlementEntry.Tooltip = "Settlement Entry";
            this.btnSettlementEntry.Click += new System.EventHandler(this.btnSettlementEntry_Click);
            // 
            // btnLeaveEntry
            // 
            this.btnLeaveEntry.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnLeaveEntry.FixedSize = new System.Drawing.Size(100, 10);
            this.btnLeaveEntry.Image = global::MyBooksERP.Properties.Resources.leave_entry;
            this.btnLeaveEntry.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLeaveEntry.Name = "btnLeaveEntry";
            this.btnLeaveEntry.SubItemsExpandWidth = 14;
            this.btnLeaveEntry.Text = "Leave Entry";
            this.btnLeaveEntry.Tooltip = "Leave Entry";
            this.btnLeaveEntry.Click += new System.EventHandler(this.btnLeaveEntry_Click);
            // 
            // ribbonBarPayroll
            // 
            this.ribbonBarPayroll.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBarPayroll.BackgroundMouseOverStyle.Class = "";
            this.ribbonBarPayroll.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBarPayroll.BackgroundStyle.Class = "";
            this.ribbonBarPayroll.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBarPayroll.ContainerControlProcessDialogKey = true;
            this.ribbonBarPayroll.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBarPayroll.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSalaryStructure,
            this.btnNewAttendance,
            this.btnProcess,
            this.btnRelease,
            this.btnLeaveStructure});
            this.ribbonBarPayroll.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBarPayroll.Location = new System.Drawing.Point(498, 0);
            this.ribbonBarPayroll.Name = "ribbonBarPayroll";
            this.ribbonBarPayroll.Size = new System.Drawing.Size(441, 87);
            this.ribbonBarPayroll.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBarPayroll.TabIndex = 8;
            this.ribbonBarPayroll.Text = "Payroll";
            // 
            // 
            // 
            this.ribbonBarPayroll.TitleStyle.Class = "";
            this.ribbonBarPayroll.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBarPayroll.TitleStyleMouseOver.Class = "";
            this.ribbonBarPayroll.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBarPayroll.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnSalaryStructure
            // 
            this.btnSalaryStructure.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSalaryStructure.FixedSize = new System.Drawing.Size(85, 10);
            this.btnSalaryStructure.Image = global::MyBooksERP.Properties.Resources.salarystructure;
            this.btnSalaryStructure.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSalaryStructure.Name = "btnSalaryStructure";
            this.btnSalaryStructure.SubItemsExpandWidth = 14;
            this.btnSalaryStructure.Text = "Salary Structure";
            this.btnSalaryStructure.Tooltip = "Salary Structure";
            this.btnSalaryStructure.Click += new System.EventHandler(this.btnSalaryStructure_Click);
            // 
            // btnNewAttendance
            // 
            this.btnNewAttendance.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnNewAttendance.FixedSize = new System.Drawing.Size(80, 10);
            this.btnNewAttendance.Image = global::MyBooksERP.Properties.Resources.Attendance_Manual;
            this.btnNewAttendance.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnNewAttendance.Name = "btnNewAttendance";
            this.btnNewAttendance.SubItemsExpandWidth = 14;
            this.btnNewAttendance.Text = "Attendance";
            this.btnNewAttendance.Tooltip = "Attendance";
            this.btnNewAttendance.Click += new System.EventHandler(this.btnNewAttendance_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnProcess.FixedSize = new System.Drawing.Size(80, 10);
            this.btnProcess.Image = global::MyBooksERP.Properties.Resources.Salary_processing1;
            this.btnProcess.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.SubItemsExpandWidth = 14;
            this.btnProcess.Text = "Processing";
            this.btnProcess.Tooltip = "Salary Process";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnRelease
            // 
            this.btnRelease.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRelease.FixedSize = new System.Drawing.Size(80, 10);
            this.btnRelease.Image = global::MyBooksERP.Properties.Resources.Salary_release;
            this.btnRelease.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRelease.Name = "btnRelease";
            this.btnRelease.SubItemsExpandWidth = 14;
            this.btnRelease.Text = "Releasing";
            this.btnRelease.Tooltip = "Salary Release";
            this.btnRelease.Click += new System.EventHandler(this.btnRelease_Click);
            // 
            // btnLeaveStructure
            // 
            this.btnLeaveStructure.FixedSize = new System.Drawing.Size(100, 10);
            this.btnLeaveStructure.Image = ((System.Drawing.Image)(resources.GetObject("btnLeaveStructure.Image")));
            this.btnLeaveStructure.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLeaveStructure.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnLeaveStructure.Name = "btnLeaveStructure";
            this.btnLeaveStructure.SubItemsExpandWidth = 14;
            this.btnLeaveStructure.Text = "Leave Structure";
            this.btnLeaveStructure.Tooltip = "Leave Structure";
            this.btnLeaveStructure.Click += new System.EventHandler(this.btnLeaveStructure_Click);
            // 
            // ribbonBar7
            // 
            this.ribbonBar7.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar7.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar7.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar7.BackgroundStyle.Class = "";
            this.ribbonBar7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar7.ContainerControlProcessDialogKey = true;
            this.ribbonBar7.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar7.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnShift,
            this.btnWorkPolicy,
            this.btnLeavePolicy,
            this.btnCalendar,
            this.btnVacationPolicy,
            this.btnSettlementPolicy});
            this.ribbonBar7.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar7.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar7.Name = "ribbonBar7";
            this.ribbonBar7.Size = new System.Drawing.Size(495, 87);
            this.ribbonBar7.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar7.TabIndex = 6;
            this.ribbonBar7.Text = "Policies";
            // 
            // 
            // 
            this.ribbonBar7.TitleStyle.Class = "";
            this.ribbonBar7.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar7.TitleStyleMouseOver.Class = "";
            this.ribbonBar7.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar7.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnShift
            // 
            this.btnShift.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnShift.FixedSize = new System.Drawing.Size(80, 10);
            this.btnShift.Image = global::MyBooksERP.Properties.Resources.Shift;
            this.btnShift.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnShift.Name = "btnShift";
            this.btnShift.SubItemsExpandWidth = 14;
            this.btnShift.Text = "Shift";
            this.btnShift.Tooltip = "Shift Policy";
            this.btnShift.Click += new System.EventHandler(this.btnShift_Click);
            // 
            // btnWorkPolicy
            // 
            this.btnWorkPolicy.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnWorkPolicy.FixedSize = new System.Drawing.Size(80, 10);
            this.btnWorkPolicy.Image = global::MyBooksERP.Properties.Resources.Work_Policy;
            this.btnWorkPolicy.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnWorkPolicy.Name = "btnWorkPolicy";
            this.btnWorkPolicy.SubItemsExpandWidth = 14;
            this.btnWorkPolicy.Text = "Work";
            this.btnWorkPolicy.Tooltip = "Work Policy";
            this.btnWorkPolicy.Click += new System.EventHandler(this.btnWorkPolicy_Click);
            // 
            // btnLeavePolicy
            // 
            this.btnLeavePolicy.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnLeavePolicy.FixedSize = new System.Drawing.Size(80, 10);
            this.btnLeavePolicy.Image = global::MyBooksERP.Properties.Resources.Leave;
            this.btnLeavePolicy.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLeavePolicy.Name = "btnLeavePolicy";
            this.btnLeavePolicy.SubItemsExpandWidth = 14;
            this.btnLeavePolicy.Text = "Leave";
            this.btnLeavePolicy.Tooltip = "Leave Policy";
            this.btnLeavePolicy.Click += new System.EventHandler(this.btnLeavePolicy_Click);
            // 
            // btnCalendar
            // 
            this.btnCalendar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnCalendar.FixedSize = new System.Drawing.Size(80, 10);
            this.btnCalendar.Image = global::MyBooksERP.Properties.Resources.holiday_Policy;
            this.btnCalendar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCalendar.Name = "btnCalendar";
            this.btnCalendar.SubItemsExpandWidth = 14;
            this.btnCalendar.Text = "Holiday Calander";
            this.btnCalendar.Tooltip = "Holiday Calander";
            this.btnCalendar.Click += new System.EventHandler(this.btnCalendar_Click);
            // 
            // btnVacationPolicy
            // 
            this.btnVacationPolicy.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnVacationPolicy.FixedSize = new System.Drawing.Size(80, 10);
            this.btnVacationPolicy.Image = global::MyBooksERP.Properties.Resources.vacation_policy;
            this.btnVacationPolicy.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnVacationPolicy.Name = "btnVacationPolicy";
            this.btnVacationPolicy.SubItemsExpandWidth = 14;
            this.btnVacationPolicy.Text = "Vacation";
            this.btnVacationPolicy.Tooltip = "Vacation";
            this.btnVacationPolicy.Click += new System.EventHandler(this.btnVacationPolicy_Click);
            // 
            // btnSettlementPolicy
            // 
            this.btnSettlementPolicy.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSettlementPolicy.FixedSize = new System.Drawing.Size(80, 10);
            this.btnSettlementPolicy.Image = global::MyBooksERP.Properties.Resources.SettlementPolicy;
            this.btnSettlementPolicy.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSettlementPolicy.Name = "btnSettlementPolicy";
            this.btnSettlementPolicy.SubItemsExpandWidth = 14;
            this.btnSettlementPolicy.Text = "Settlement";
            this.btnSettlementPolicy.Tooltip = "Settlement";
            this.btnSettlementPolicy.Visible = false;
            this.btnSettlementPolicy.Click += new System.EventHandler(this.btnSettlementPolicy_Click);
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel2.Controls.Add(this.ribbonBar4);
            this.ribbonPanel2.Controls.Add(this.rbPolicies);
            this.ribbonPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel2.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel2.Name = "ribbonPanel2";
            this.ribbonPanel2.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel2.Size = new System.Drawing.Size(1290, 90);
            // 
            // 
            // 
            this.ribbonPanel2.Style.Class = "";
            this.ribbonPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseDown.Class = "";
            this.ribbonPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseOver.Class = "";
            this.ribbonPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel2.TabIndex = 11;
            this.ribbonPanel2.Visible = false;
            // 
            // ribbonBar4
            // 
            this.ribbonBar4.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar4.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar4.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar4.BackgroundStyle.Class = "";
            this.ribbonBar4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar4.ContainerControlProcessDialogKey = true;
            this.ribbonBar4.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar4.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnPayments,
            this.btnReceipts,
            this.btnExtraChargePayment});
            this.ribbonBar4.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar4.Location = new System.Drawing.Point(931, 0);
            this.ribbonBar4.Name = "ribbonBar4";
            this.ribbonBar4.Size = new System.Drawing.Size(316, 87);
            this.ribbonBar4.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar4.TabIndex = 6;
            this.ribbonBar4.Text = "Transactions";
            // 
            // 
            // 
            this.ribbonBar4.TitleStyle.Class = "";
            this.ribbonBar4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar4.TitleStyleMouseOver.Class = "";
            this.ribbonBar4.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar4.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnPayments
            // 
            this.btnPayments.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPayments.FixedSize = new System.Drawing.Size(100, 10);
            this.btnPayments.Image = global::MyBooksERP.Properties.Resources.Payments;
            this.btnPayments.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPayments.Name = "btnPayments";
            this.btnPayments.SubItemsExpandWidth = 14;
            this.btnPayments.Text = "&Payments";
            this.btnPayments.Tooltip = "Payments";
            this.btnPayments.Click += new System.EventHandler(this.btnPayments_Click);
            // 
            // btnReceipts
            // 
            this.btnReceipts.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnReceipts.FixedSize = new System.Drawing.Size(100, 10);
            this.btnReceipts.Image = global::MyBooksERP.Properties.Resources.Receipts;
            this.btnReceipts.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnReceipts.Name = "btnReceipts";
            this.btnReceipts.SubItemsExpandWidth = 14;
            this.btnReceipts.Text = "&Receipts";
            this.btnReceipts.Tooltip = "Receipts";
            this.btnReceipts.Click += new System.EventHandler(this.btnReceipts_Click);
            // 
            // btnExtraChargePayment
            // 
            this.btnExtraChargePayment.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnExtraChargePayment.FixedSize = new System.Drawing.Size(100, 10);
            this.btnExtraChargePayment.Image = global::MyBooksERP.Properties.Resources.Payments___Receipts;
            this.btnExtraChargePayment.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnExtraChargePayment.Name = "btnExtraChargePayment";
            this.btnExtraChargePayment.SubItemsExpandWidth = 14;
            this.btnExtraChargePayment.Text = "Extra Charge Payment";
            this.btnExtraChargePayment.Tooltip = "Extra Charge Payment";
            this.btnExtraChargePayment.Click += new System.EventHandler(this.btnExtraChargePayment_Click);
            // 
            // rbPolicies
            // 
            this.rbPolicies.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbPolicies.BackgroundMouseOverStyle.Class = "";
            this.rbPolicies.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbPolicies.BackgroundStyle.Class = "";
            this.rbPolicies.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbPolicies.ContainerControlProcessDialogKey = true;
            this.rbPolicies.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbPolicies.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnPaymentVoucher,
            this.btnReceiptVoucher,
            this.btnContraVoucher,
            this.BtnJVoucher,
            this.btnPurchaseVoucher,
            this.btnDebitNoteVoucher,
            this.btnSalesVoucher,
            this.btnCreditNoteVoucher,
            this.btnGroupJV});
            this.rbPolicies.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.rbPolicies.Location = new System.Drawing.Point(3, 0);
            this.rbPolicies.Name = "rbPolicies";
            this.rbPolicies.Size = new System.Drawing.Size(928, 87);
            this.rbPolicies.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.rbPolicies.TabIndex = 5;
            this.rbPolicies.Text = "Vouchers";
            // 
            // 
            // 
            this.rbPolicies.TitleStyle.Class = "";
            this.rbPolicies.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbPolicies.TitleStyleMouseOver.Class = "";
            this.rbPolicies.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbPolicies.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnPaymentVoucher
            // 
            this.btnPaymentVoucher.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPaymentVoucher.FixedSize = new System.Drawing.Size(100, 10);
            this.btnPaymentVoucher.Image = global::MyBooksERP.Properties.Resources.Extra_Charges;
            this.btnPaymentVoucher.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPaymentVoucher.Name = "btnPaymentVoucher";
            this.btnPaymentVoucher.SubItemsExpandWidth = 14;
            this.btnPaymentVoucher.Text = "Payment";
            this.btnPaymentVoucher.Tooltip = "Payment";
            this.btnPaymentVoucher.Click += new System.EventHandler(this.btnPaymentVoucher_Click);
            // 
            // btnReceiptVoucher
            // 
            this.btnReceiptVoucher.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnReceiptVoucher.FixedSize = new System.Drawing.Size(100, 10);
            this.btnReceiptVoucher.Image = global::MyBooksERP.Properties.Resources.costEstimation;
            this.btnReceiptVoucher.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnReceiptVoucher.Name = "btnReceiptVoucher";
            this.btnReceiptVoucher.SubItemsExpandWidth = 14;
            this.btnReceiptVoucher.Text = "Receipt";
            this.btnReceiptVoucher.Tooltip = "Receipt";
            this.btnReceiptVoucher.Click += new System.EventHandler(this.btnReceiptVoucher_Click);
            // 
            // btnContraVoucher
            // 
            this.btnContraVoucher.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnContraVoucher.FixedSize = new System.Drawing.Size(100, 10);
            this.btnContraVoucher.Image = global::MyBooksERP.Properties.Resources.Check_Receipt;
            this.btnContraVoucher.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnContraVoucher.Name = "btnContraVoucher";
            this.btnContraVoucher.SubItemsExpandWidth = 14;
            this.btnContraVoucher.Text = "Contra";
            this.btnContraVoucher.Tooltip = "Contra";
            this.btnContraVoucher.Click += new System.EventHandler(this.btnContraVoucher_Click);
            // 
            // BtnJVoucher
            // 
            this.BtnJVoucher.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnJVoucher.FixedSize = new System.Drawing.Size(100, 10);
            this.BtnJVoucher.Image = global::MyBooksERP.Properties.Resources.GereralReceipts_Paymrnts1;
            this.BtnJVoucher.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnJVoucher.Name = "BtnJVoucher";
            this.BtnJVoucher.SubItemsExpandWidth = 14;
            this.BtnJVoucher.Text = "Journal";
            this.BtnJVoucher.Tooltip = "Journal";
            this.BtnJVoucher.Click += new System.EventHandler(this.BtnJVoucher_Click);
            // 
            // btnPurchaseVoucher
            // 
            this.btnPurchaseVoucher.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPurchaseVoucher.FixedSize = new System.Drawing.Size(100, 10);
            this.btnPurchaseVoucher.Image = global::MyBooksERP.Properties.Resources.Purchase_Invoice;
            this.btnPurchaseVoucher.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPurchaseVoucher.Name = "btnPurchaseVoucher";
            this.btnPurchaseVoucher.SubItemsExpandWidth = 14;
            this.btnPurchaseVoucher.Text = "Purchase (View)";
            this.btnPurchaseVoucher.Tooltip = "Purchase (View)";
            this.btnPurchaseVoucher.Click += new System.EventHandler(this.btnPurchaseVoucher_Click);
            // 
            // btnDebitNoteVoucher
            // 
            this.btnDebitNoteVoucher.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDebitNoteVoucher.FixedSize = new System.Drawing.Size(100, 10);
            this.btnDebitNoteVoucher.Image = global::MyBooksERP.Properties.Resources.DebitNote1;
            this.btnDebitNoteVoucher.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnDebitNoteVoucher.Name = "btnDebitNoteVoucher";
            this.btnDebitNoteVoucher.SubItemsExpandWidth = 14;
            this.btnDebitNoteVoucher.Text = "Debit Note (View)";
            this.btnDebitNoteVoucher.Tooltip = "Debit Note (View)";
            this.btnDebitNoteVoucher.Click += new System.EventHandler(this.btnDebitNoteVoucher_Click);
            // 
            // btnSalesVoucher
            // 
            this.btnSalesVoucher.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSalesVoucher.FixedSize = new System.Drawing.Size(100, 10);
            this.btnSalesVoucher.Image = global::MyBooksERP.Properties.Resources.Sales_invoice;
            this.btnSalesVoucher.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSalesVoucher.Name = "btnSalesVoucher";
            this.btnSalesVoucher.SubItemsExpandWidth = 14;
            this.btnSalesVoucher.Text = "Sales (View)";
            this.btnSalesVoucher.Tooltip = "Sales (View)";
            this.btnSalesVoucher.Click += new System.EventHandler(this.btnSalesVoucher_Click);
            // 
            // btnCreditNoteVoucher
            // 
            this.btnCreditNoteVoucher.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnCreditNoteVoucher.FixedSize = new System.Drawing.Size(110, 10);
            this.btnCreditNoteVoucher.Image = global::MyBooksERP.Properties.Resources.Sales_Return;
            this.btnCreditNoteVoucher.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCreditNoteVoucher.Name = "btnCreditNoteVoucher";
            this.btnCreditNoteVoucher.SubItemsExpandWidth = 14;
            this.btnCreditNoteVoucher.Text = "Credit Note (View)";
            this.btnCreditNoteVoucher.Tooltip = "Credit Note (View)";
            this.btnCreditNoteVoucher.Click += new System.EventHandler(this.btnCreditNoteVoucher_Click);
            // 
            // btnGroupJV
            // 
            this.btnGroupJV.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnGroupJV.FixedSize = new System.Drawing.Size(100, 10);
            this.btnGroupJV.Image = global::MyBooksERP.Properties.Resources.GereralReceipts_Paymrnts1;
            this.btnGroupJV.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGroupJV.Name = "btnGroupJV";
            this.btnGroupJV.SubItemsExpandWidth = 14;
            this.btnGroupJV.Text = "Group Journal";
            this.btnGroupJV.Tooltip = "Group Journal";
            this.btnGroupJV.Click += new System.EventHandler(this.btnGroupJV_Click);
            // 
            // ribbonPanel12
            // 
            this.ribbonPanel12.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel12.Controls.Add(this.ribbonBarEmployee);
            this.ribbonPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel12.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel12.Name = "ribbonPanel12";
            this.ribbonPanel12.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel12.Size = new System.Drawing.Size(1290, 90);
            // 
            // 
            // 
            this.ribbonPanel12.Style.Class = "";
            this.ribbonPanel12.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel12.StyleMouseDown.Class = "";
            this.ribbonPanel12.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel12.StyleMouseOver.Class = "";
            this.ribbonPanel12.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel12.TabIndex = 16;
            this.ribbonPanel12.Visible = false;
            // 
            // ribbonBarEmployee
            // 
            this.ribbonBarEmployee.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBarEmployee.BackgroundMouseOverStyle.Class = "";
            this.ribbonBarEmployee.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBarEmployee.BackgroundStyle.Class = "";
            this.ribbonBarEmployee.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBarEmployee.ContainerControlProcessDialogKey = true;
            this.ribbonBarEmployee.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBarEmployee.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnEmployeeReport,
            this.btnRptSalaryStructureReport,
            this.btnRptAttendance,
            this.btnPaymentsReport,
            this.btnSalaryAdvanceReport,
            this.btnPaySlip,
            this.btnLeaveSummaryReport});
            this.ribbonBarEmployee.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBarEmployee.Location = new System.Drawing.Point(3, 0);
            this.ribbonBarEmployee.Name = "ribbonBarEmployee";
            this.ribbonBarEmployee.Size = new System.Drawing.Size(720, 87);
            this.ribbonBarEmployee.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBarEmployee.TabIndex = 4;
            this.ribbonBarEmployee.Text = "MIS Reports";
            // 
            // 
            // 
            this.ribbonBarEmployee.TitleStyle.Class = "";
            this.ribbonBarEmployee.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBarEmployee.TitleStyleMouseOver.Class = "";
            this.ribbonBarEmployee.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBarEmployee.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnEmployeeReport
            // 
            this.btnEmployeeReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnEmployeeReport.FixedSize = new System.Drawing.Size(100, 10);
            this.btnEmployeeReport.Image = global::MyBooksERP.Properties.Resources.EmployeeProfileReport;
            this.btnEmployeeReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnEmployeeReport.Name = "btnEmployeeReport";
            this.btnEmployeeReport.SubItemsExpandWidth = 14;
            this.btnEmployeeReport.Text = "Employee Profile";
            this.btnEmployeeReport.Click += new System.EventHandler(this.btnEmployeeReport_Click);
            // 
            // btnRptSalaryStructureReport
            // 
            this.btnRptSalaryStructureReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptSalaryStructureReport.FixedSize = new System.Drawing.Size(100, 10);
            this.btnRptSalaryStructureReport.Image = global::MyBooksERP.Properties.Resources.SalaryStructureReport;
            this.btnRptSalaryStructureReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptSalaryStructureReport.Name = "btnRptSalaryStructureReport";
            this.btnRptSalaryStructureReport.SubItemsExpandWidth = 14;
            this.btnRptSalaryStructureReport.Text = "Salary Structure";
            this.btnRptSalaryStructureReport.Click += new System.EventHandler(this.btnRptSalaryStructureReport_Click);
            // 
            // btnRptAttendance
            // 
            this.btnRptAttendance.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptAttendance.FixedSize = new System.Drawing.Size(100, 10);
            this.btnRptAttendance.Image = global::MyBooksERP.Properties.Resources.Attendance_Report;
            this.btnRptAttendance.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptAttendance.Name = "btnRptAttendance";
            this.btnRptAttendance.SubItemsExpandWidth = 14;
            this.btnRptAttendance.Text = "Attendance";
            this.btnRptAttendance.Click += new System.EventHandler(this.btnRptAttendance_Click);
            // 
            // btnPaymentsReport
            // 
            this.btnPaymentsReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPaymentsReport.FixedSize = new System.Drawing.Size(100, 10);
            this.btnPaymentsReport.Image = global::MyBooksERP.Properties.Resources.Payment;
            this.btnPaymentsReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPaymentsReport.Name = "btnPaymentsReport";
            this.btnPaymentsReport.SubItemsExpandWidth = 14;
            this.btnPaymentsReport.Text = "Payments";
            this.btnPaymentsReport.Click += new System.EventHandler(this.btnPaymentsReport_Click);
            // 
            // btnSalaryAdvanceReport
            // 
            this.btnSalaryAdvanceReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSalaryAdvanceReport.FixedSize = new System.Drawing.Size(100, 10);
            this.btnSalaryAdvanceReport.Image = global::MyBooksERP.Properties.Resources.SalaryAdvanceReport;
            this.btnSalaryAdvanceReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSalaryAdvanceReport.Name = "btnSalaryAdvanceReport";
            this.btnSalaryAdvanceReport.SubItemsExpandWidth = 14;
            this.btnSalaryAdvanceReport.Text = "Salary Advance";
            this.btnSalaryAdvanceReport.Tooltip = "Salary Advance";
            this.btnSalaryAdvanceReport.Click += new System.EventHandler(this.btnSalaryAdvanceReport_Click);
            // 
            // btnPaySlip
            // 
            this.btnPaySlip.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPaySlip.FixedSize = new System.Drawing.Size(100, 10);
            this.btnPaySlip.Image = global::MyBooksERP.Properties.Resources.PaySlipReport;
            this.btnPaySlip.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPaySlip.Name = "btnPaySlip";
            this.btnPaySlip.SubItemsExpandWidth = 14;
            this.btnPaySlip.Text = "Pay Slip";
            this.btnPaySlip.Tooltip = "Pay Slip";
            this.btnPaySlip.Click += new System.EventHandler(this.btnPaySlip_Click);
            // 
            // btnLeaveSummaryReport
            // 
            this.btnLeaveSummaryReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnLeaveSummaryReport.FixedSize = new System.Drawing.Size(100, 10);
            this.btnLeaveSummaryReport.Image = ((System.Drawing.Image)(resources.GetObject("btnLeaveSummaryReport.Image")));
            this.btnLeaveSummaryReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLeaveSummaryReport.Name = "btnLeaveSummaryReport";
            this.btnLeaveSummaryReport.SubItemsExpandWidth = 14;
            this.btnLeaveSummaryReport.Text = "Leave ";
            this.btnLeaveSummaryReport.Tooltip = "Leave";
            this.btnLeaveSummaryReport.Click += new System.EventHandler(this.btnLeaveSummaryReport_Click);
            // 
            // ribbonPanel9
            // 
            this.ribbonPanel9.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel9.Controls.Add(this.ribbonBar2);
            this.ribbonPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel9.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel9.Name = "ribbonPanel9";
            this.ribbonPanel9.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel9.Size = new System.Drawing.Size(1290, 90);
            // 
            // 
            // 
            this.ribbonPanel9.Style.Class = "";
            this.ribbonPanel9.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel9.StyleMouseDown.Class = "";
            this.ribbonPanel9.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel9.StyleMouseOver.Class = "";
            this.ribbonPanel9.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel9.TabIndex = 13;
            this.ribbonPanel9.Visible = false;
            // 
            // ribbonBar2
            // 
            this.ribbonBar2.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar2.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar2.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar2.BackgroundStyle.Class = "";
            this.ribbonBar2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar2.ContainerControlProcessDialogKey = true;
            this.ribbonBar2.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar2.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnProfitnLoss,
            this.btnBalancesheet,
            this.btnCashFlow,
            this.btnFundFlow,
            this.btnINcomestmt,
            this.btnStockSummary,
            this.btnStatementofAccounts});
            this.ribbonBar2.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar2.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar2.Name = "ribbonBar2";
            this.ribbonBar2.Size = new System.Drawing.Size(720, 87);
            this.ribbonBar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar2.TabIndex = 1;
            this.ribbonBar2.Text = "Reports";
            // 
            // 
            // 
            this.ribbonBar2.TitleStyle.Class = "";
            this.ribbonBar2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar2.TitleStyleMouseOver.Class = "";
            this.ribbonBar2.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar2.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnProfitnLoss
            // 
            this.btnProfitnLoss.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnProfitnLoss.FixedSize = new System.Drawing.Size(100, 10);
            this.btnProfitnLoss.Image = global::MyBooksERP.Properties.Resources.Profot___Loss_Report;
            this.btnProfitnLoss.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnProfitnLoss.Name = "btnProfitnLoss";
            this.btnProfitnLoss.SubItemsExpandWidth = 14;
            this.btnProfitnLoss.Text = "&Profit && Loss";
            this.btnProfitnLoss.Tooltip = "Profit && Loss";
            this.btnProfitnLoss.Click += new System.EventHandler(this.btnProfitnLoss_Click);
            // 
            // btnBalancesheet
            // 
            this.btnBalancesheet.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnBalancesheet.FixedSize = new System.Drawing.Size(100, 10);
            this.btnBalancesheet.Image = global::MyBooksERP.Properties.Resources.Balancesheet;
            this.btnBalancesheet.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnBalancesheet.Name = "btnBalancesheet";
            this.btnBalancesheet.SubItemsExpandWidth = 14;
            this.btnBalancesheet.Text = "Balance Sheet";
            this.btnBalancesheet.Tooltip = "Balance Sheet";
            this.btnBalancesheet.Click += new System.EventHandler(this.btnBalancesheet_Click);
            // 
            // btnCashFlow
            // 
            this.btnCashFlow.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnCashFlow.FixedSize = new System.Drawing.Size(100, 10);
            this.btnCashFlow.Image = global::MyBooksERP.Properties.Resources.Cash_Flow__Report;
            this.btnCashFlow.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCashFlow.Name = "btnCashFlow";
            this.btnCashFlow.SubItemsExpandWidth = 14;
            this.btnCashFlow.Text = "Cash Flow";
            this.btnCashFlow.Tooltip = "Cash Flow";
            this.btnCashFlow.Click += new System.EventHandler(this.btnCashFlow_Click);
            // 
            // btnFundFlow
            // 
            this.btnFundFlow.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnFundFlow.FixedSize = new System.Drawing.Size(100, 10);
            this.btnFundFlow.Image = global::MyBooksERP.Properties.Resources.Fund_flow_report;
            this.btnFundFlow.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnFundFlow.Name = "btnFundFlow";
            this.btnFundFlow.SubItemsExpandWidth = 14;
            this.btnFundFlow.Text = "Fund Flow";
            this.btnFundFlow.Tooltip = "Fund Flow";
            this.btnFundFlow.Click += new System.EventHandler(this.btnFundFlow_Click);
            // 
            // btnINcomestmt
            // 
            this.btnINcomestmt.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnINcomestmt.FixedSize = new System.Drawing.Size(100, 10);
            this.btnINcomestmt.Image = global::MyBooksERP.Properties.Resources.Income_Statement;
            this.btnINcomestmt.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnINcomestmt.Name = "btnINcomestmt";
            this.btnINcomestmt.SubItemsExpandWidth = 14;
            this.btnINcomestmt.Text = "Income Statement";
            this.btnINcomestmt.Tooltip = "Income && Expenditure Statement";
            this.btnINcomestmt.Click += new System.EventHandler(this.btnINcomestmt_Click);
            // 
            // btnStockSummary
            // 
            this.btnStockSummary.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnStockSummary.FixedSize = new System.Drawing.Size(100, 10);
            this.btnStockSummary.Image = global::MyBooksERP.Properties.Resources.Stock_Book1;
            this.btnStockSummary.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnStockSummary.Name = "btnStockSummary";
            this.btnStockSummary.SubItemsExpandWidth = 14;
            this.btnStockSummary.Text = "Stock Summary";
            this.btnStockSummary.Tooltip = "Stock Summary";
            this.btnStockSummary.Click += new System.EventHandler(this.btnStockSummary_Click);
            // 
            // btnStatementofAccounts
            // 
            this.btnStatementofAccounts.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnStatementofAccounts.FixedSize = new System.Drawing.Size(100, 10);
            this.btnStatementofAccounts.Image = global::MyBooksERP.Properties.Resources.AccountsReports;
            this.btnStatementofAccounts.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnStatementofAccounts.Name = "btnStatementofAccounts";
            this.btnStatementofAccounts.SubItemsExpandWidth = 14;
            this.btnStatementofAccounts.Text = "Statement of Accounts";
            this.btnStatementofAccounts.Tooltip = "Stock Summary";
            this.btnStatementofAccounts.Click += new System.EventHandler(this.btnStatementofAccounts_Click);
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel5.Controls.Add(this.ribbonBar20);
            this.ribbonPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel5.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel5.Name = "ribbonPanel5";
            this.ribbonPanel5.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel5.Size = new System.Drawing.Size(1290, 90);
            // 
            // 
            // 
            this.ribbonPanel5.Style.Class = "";
            this.ribbonPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel5.StyleMouseDown.Class = "";
            this.ribbonPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel5.StyleMouseOver.Class = "";
            this.ribbonPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel5.TabIndex = 12;
            this.ribbonPanel5.Visible = false;
            // 
            // ribbonBar20
            // 
            this.ribbonBar20.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar20.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar20.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar20.BackgroundStyle.Class = "";
            this.ribbonBar20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar20.ContainerControlProcessDialogKey = true;
            this.ribbonBar20.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar20.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnTrialBalance,
            this.btnGroupSummary,
            this.btnGL,
            this.BtnDayBook,
            this.btnCashBook,
            this.btnProfitAndLossSummary,
            this.btnBalanceSheetSummary});
            this.ribbonBar20.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar20.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar20.Name = "ribbonBar20";
            this.ribbonBar20.Size = new System.Drawing.Size(723, 87);
            this.ribbonBar20.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar20.TabIndex = 1;
            this.ribbonBar20.Text = "Summary";
            // 
            // 
            // 
            this.ribbonBar20.TitleStyle.Class = "";
            this.ribbonBar20.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar20.TitleStyleMouseOver.Class = "";
            this.ribbonBar20.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar20.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnTrialBalance
            // 
            this.btnTrialBalance.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnTrialBalance.FixedSize = new System.Drawing.Size(100, 10);
            this.btnTrialBalance.Image = global::MyBooksERP.Properties.Resources.Trial_Balance;
            this.btnTrialBalance.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnTrialBalance.Name = "btnTrialBalance";
            this.btnTrialBalance.SubItemsExpandWidth = 14;
            this.btnTrialBalance.Text = "Trial Balance";
            this.btnTrialBalance.Tooltip = "Trial Balance";
            this.btnTrialBalance.Click += new System.EventHandler(this.btnTrialBalance_Click);
            // 
            // btnGroupSummary
            // 
            this.btnGroupSummary.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnGroupSummary.FixedSize = new System.Drawing.Size(100, 10);
            this.btnGroupSummary.Image = global::MyBooksERP.Properties.Resources.Account_Entry;
            this.btnGroupSummary.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGroupSummary.Name = "btnGroupSummary";
            this.btnGroupSummary.SubItemsExpandWidth = 14;
            this.btnGroupSummary.Text = "Group Summary";
            this.btnGroupSummary.Tooltip = "Group Summary";
            this.btnGroupSummary.Click += new System.EventHandler(this.btnGroupSummary_Click);
            // 
            // btnGL
            // 
            this.btnGL.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnGL.FixedSize = new System.Drawing.Size(100, 10);
            this.btnGL.Image = global::MyBooksERP.Properties.Resources.General_Ledger;
            this.btnGL.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGL.Name = "btnGL";
            this.btnGL.SubItemsExpandWidth = 14;
            this.btnGL.Text = "General Ledger";
            this.btnGL.Tooltip = "General Ledger";
            this.btnGL.Click += new System.EventHandler(this.btnGL_Click);
            // 
            // BtnDayBook
            // 
            this.BtnDayBook.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnDayBook.FixedSize = new System.Drawing.Size(100, 10);
            this.BtnDayBook.Image = global::MyBooksERP.Properties.Resources.Day_book;
            this.BtnDayBook.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnDayBook.Name = "BtnDayBook";
            this.BtnDayBook.SubItemsExpandWidth = 14;
            this.BtnDayBook.Text = "Day Book";
            this.BtnDayBook.Tooltip = "Day Book";
            this.BtnDayBook.Click += new System.EventHandler(this.BtnDayBook_Click);
            // 
            // btnCashBook
            // 
            this.btnCashBook.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnCashBook.FixedSize = new System.Drawing.Size(100, 10);
            this.btnCashBook.Image = global::MyBooksERP.Properties.Resources.Cash_Book__report_;
            this.btnCashBook.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCashBook.Name = "btnCashBook";
            this.btnCashBook.SubItemsExpandWidth = 14;
            this.btnCashBook.Text = "Cash Book";
            this.btnCashBook.Tooltip = "Cash Book";
            this.btnCashBook.Click += new System.EventHandler(this.btnCashBook_Click);
            // 
            // btnProfitAndLossSummary
            // 
            this.btnProfitAndLossSummary.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnProfitAndLossSummary.FixedSize = new System.Drawing.Size(100, 10);
            this.btnProfitAndLossSummary.Image = global::MyBooksERP.Properties.Resources.Profot___Loss_Report;
            this.btnProfitAndLossSummary.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnProfitAndLossSummary.Name = "btnProfitAndLossSummary";
            this.btnProfitAndLossSummary.SubItemsExpandWidth = 14;
            this.btnProfitAndLossSummary.Text = "&Profit && Loss";
            this.btnProfitAndLossSummary.Tooltip = "Profit && Loss";
            this.btnProfitAndLossSummary.Click += new System.EventHandler(this.btnProfitAndLossSummary_Click);
            // 
            // btnBalanceSheetSummary
            // 
            this.btnBalanceSheetSummary.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnBalanceSheetSummary.FixedSize = new System.Drawing.Size(100, 10);
            this.btnBalanceSheetSummary.Image = global::MyBooksERP.Properties.Resources.Balancesheet;
            this.btnBalanceSheetSummary.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnBalanceSheetSummary.Name = "btnBalanceSheetSummary";
            this.btnBalanceSheetSummary.SubItemsExpandWidth = 14;
            this.btnBalanceSheetSummary.Text = "Balance Sheet";
            this.btnBalanceSheetSummary.Tooltip = "Balance Sheet";
            this.btnBalanceSheetSummary.Click += new System.EventHandler(this.btnBalanceSheetSummary_Click);
            // 
            // Inventory1
            // 
            this.Inventory1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.Inventory1.Controls.Add(this.rbProduction);
            this.Inventory1.Controls.Add(this.ribbonBar6);
            this.Inventory1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Inventory1.Location = new System.Drawing.Point(0, 57);
            this.Inventory1.Name = "Inventory1";
            this.Inventory1.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.Inventory1.Size = new System.Drawing.Size(1290, 90);
            // 
            // 
            // 
            this.Inventory1.Style.Class = "";
            this.Inventory1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.Inventory1.StyleMouseDown.Class = "";
            this.Inventory1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.Inventory1.StyleMouseOver.Class = "";
            this.Inventory1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Inventory1.TabIndex = 4;
            this.Inventory1.Visible = false;
            // 
            // rbProduction
            // 
            this.rbProduction.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbProduction.BackgroundMouseOverStyle.Class = "";
            this.rbProduction.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbProduction.BackgroundStyle.Class = "";
            this.rbProduction.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbProduction.ContainerControlProcessDialogKey = true;
            this.rbProduction.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbProduction.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnProductionTemplate,
            this.btnMaterialIssue,
            this.btnProductionProcess,
            this.btnQC});
            this.rbProduction.ItemSpacing = 5;
            this.rbProduction.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.rbProduction.Location = new System.Drawing.Point(760, 0);
            this.rbProduction.Name = "rbProduction";
            this.rbProduction.ResizeItemsToFit = false;
            this.rbProduction.Size = new System.Drawing.Size(398, 87);
            this.rbProduction.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            superTooltipInfo2.BodyText = "Assigning the Super Tooltip to the Ribbon Bar control will display it when mouse " +
                "hovers over the Dialog Launcher button.";
            superTooltipInfo2.HeaderText = "SuperTooltip for Dialog Launcher Button";
            this.superTooltip1.SetSuperTooltip(this.rbProduction, superTooltipInfo2);
            this.rbProduction.TabIndex = 3;
            this.rbProduction.Text = "Production";
            // 
            // 
            // 
            this.rbProduction.TitleStyle.Class = "";
            this.rbProduction.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbProduction.TitleStyleMouseOver.Class = "";
            this.rbProduction.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbProduction.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnProductionTemplate
            // 
            this.btnProductionTemplate.FixedSize = new System.Drawing.Size(90, 10);
            this.btnProductionTemplate.Image = global::MyBooksERP.Properties.Resources.ProductReceipt2;
            this.btnProductionTemplate.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnProductionTemplate.Name = "btnProductionTemplate";
            this.btnProductionTemplate.Text = "Bill of Material";
            this.btnProductionTemplate.Tooltip = "Bill of Material (BOM)";
            this.btnProductionTemplate.Click += new System.EventHandler(this.btnProductionTemplate_Click);
            // 
            // btnMaterialIssue
            // 
            this.btnMaterialIssue.FixedSize = new System.Drawing.Size(90, 10);
            this.btnMaterialIssue.Image = global::MyBooksERP.Properties.Resources.Material_Issue1;
            this.btnMaterialIssue.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnMaterialIssue.Name = "btnMaterialIssue";
            this.btnMaterialIssue.Text = "Material Issue";
            this.btnMaterialIssue.Tooltip = "Material Issue";
            this.btnMaterialIssue.Click += new System.EventHandler(this.btnMaterialIssue_Click_1);
            // 
            // btnProductionProcess
            // 
            this.btnProductionProcess.FixedSize = new System.Drawing.Size(90, 10);
            this.btnProductionProcess.Image = global::MyBooksERP.Properties.Resources.ProductionProcess;
            this.btnProductionProcess.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnProductionProcess.Name = "btnProductionProcess";
            this.btnProductionProcess.Text = "Process";
            this.btnProductionProcess.Tooltip = "Production Process";
            this.btnProductionProcess.Click += new System.EventHandler(this.btnProductionProcess_Click);
            // 
            // btnQC
            // 
            this.btnQC.FixedSize = new System.Drawing.Size(90, 10);
            this.btnQC.Image = global::MyBooksERP.Properties.Resources.productSelection;
            this.btnQC.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnQC.Name = "btnQC";
            this.btnQC.Text = "Quality Check";
            this.btnQC.Tooltip = "Quality Check";
            this.btnQC.Click += new System.EventHandler(this.btnQC_Click);
            // 
            // ribbonBar6
            // 
            this.ribbonBar6.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar6.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar6.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar6.BackgroundStyle.Class = "";
            this.ribbonBar6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar6.ContainerControlProcessDialogKey = true;
            this.ribbonBar6.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar6.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.Products,
            this.ProductGroups,
            this.OpeningStock,
            this.StockAdjustment,
            this.StockTransfer,
            this.DeliveryNote,
            this.DirectDeliveryNote,
            this.BtnPaymentsInv});
            this.ribbonBar6.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar6.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar6.Name = "ribbonBar6";
            this.ribbonBar6.Size = new System.Drawing.Size(757, 87);
            this.ribbonBar6.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar6.TabIndex = 0;
            this.ribbonBar6.Text = "Inventory";
            // 
            // 
            // 
            this.ribbonBar6.TitleStyle.Class = "";
            this.ribbonBar6.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar6.TitleStyleMouseOver.Class = "";
            this.ribbonBar6.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar6.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // Products
            // 
            this.Products.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.Products.FixedSize = new System.Drawing.Size(95, 10);
            this.Products.Image = global::MyBooksERP.Properties.Resources.RawMaterials_Products1;
            this.Products.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Products.Name = "Products";
            this.Products.SubItemsExpandWidth = 14;
            this.Products.Text = "Products";
            this.Products.Tooltip = "Products";
            this.Products.Click += new System.EventHandler(this.Products_Click);
            // 
            // ProductGroups
            // 
            this.ProductGroups.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.ProductGroups.FixedSize = new System.Drawing.Size(100, 10);
            this.ProductGroups.GlobalName = "ProductGroups";
            this.ProductGroups.Image = global::MyBooksERP.Properties.Resources.Product_Groups;
            this.ProductGroups.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ProductGroups.Name = "ProductGroups";
            this.ProductGroups.SubItemsExpandWidth = 14;
            this.ProductGroups.Text = "Product &Assembly";
            this.ProductGroups.Tooltip = "Product Assembly";
            this.ProductGroups.Click += new System.EventHandler(this.ProductGroups_Click);
            // 
            // OpeningStock
            // 
            this.OpeningStock.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.OpeningStock.FixedSize = new System.Drawing.Size(95, 10);
            this.OpeningStock.Image = global::MyBooksERP.Properties.Resources.Opening_Stock3;
            this.OpeningStock.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.OpeningStock.Name = "OpeningStock";
            this.OpeningStock.SubItemsExpandWidth = 14;
            this.OpeningStock.Text = "Opening &Stock";
            this.OpeningStock.Tooltip = "Opening Stock";
            this.OpeningStock.Click += new System.EventHandler(this.OpeningStock_Click);
            // 
            // StockAdjustment
            // 
            this.StockAdjustment.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.StockAdjustment.FixedSize = new System.Drawing.Size(95, 10);
            this.StockAdjustment.GlobalName = "StockAdjustment";
            this.StockAdjustment.Image = ((System.Drawing.Image)(resources.GetObject("StockAdjustment.Image")));
            this.StockAdjustment.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.StockAdjustment.Name = "StockAdjustment";
            this.StockAdjustment.RibbonWordWrap = false;
            this.StockAdjustment.Text = "Stock Ad&justment";
            this.StockAdjustment.Tooltip = "Stock Adjustment";
            this.StockAdjustment.Click += new System.EventHandler(this.StockAdjustment_Click);
            // 
            // StockTransfer
            // 
            this.StockTransfer.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.StockTransfer.FixedSize = new System.Drawing.Size(90, 10);
            this.StockTransfer.GlobalName = "StockTransfer";
            this.StockTransfer.Image = global::MyBooksERP.Properties.Resources.Stock_transfer;
            this.StockTransfer.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.StockTransfer.Name = "StockTransfer";
            this.StockTransfer.RibbonWordWrap = false;
            this.StockTransfer.Text = "Stock &Transfer";
            this.StockTransfer.Tooltip = "Stock Transfer";
            this.StockTransfer.Click += new System.EventHandler(this.StockTransfer_Click);
            // 
            // DeliveryNote
            // 
            this.DeliveryNote.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.DeliveryNote.FixedSize = new System.Drawing.Size(90, 10);
            this.DeliveryNote.Image = global::MyBooksERP.Properties.Resources.Item_Issue_Delivery_Note_;
            this.DeliveryNote.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.DeliveryNote.Name = "DeliveryNote";
            this.DeliveryNote.SubItemsExpandWidth = 14;
            this.DeliveryNote.Text = "Delivery &Note";
            this.DeliveryNote.Tooltip = "Delivery Note";
            this.DeliveryNote.Click += new System.EventHandler(this.ItemIssue_Click);
            // 
            // DirectDeliveryNote
            // 
            this.DirectDeliveryNote.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.DirectDeliveryNote.FixedSize = new System.Drawing.Size(90, 10);
            this.DirectDeliveryNote.Image = global::MyBooksERP.Properties.Resources.Item_Issue_Delivery_Note_;
            this.DirectDeliveryNote.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.DirectDeliveryNote.Name = "DirectDeliveryNote";
            this.DirectDeliveryNote.SubItemsExpandWidth = 14;
            this.DirectDeliveryNote.Text = "Direct &Delivery &Note";
            this.DirectDeliveryNote.Tooltip = "Delivery Note";
            this.DirectDeliveryNote.Click += new System.EventHandler(this.DirectDeliveryNote_Click);
            // 
            // BtnPaymentsInv
            // 
            this.BtnPaymentsInv.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnPaymentsInv.FixedSize = new System.Drawing.Size(90, 10);
            this.BtnPaymentsInv.GlobalName = "Payments";
            this.BtnPaymentsInv.Image = global::MyBooksERP.Properties.Resources.Payments;
            this.BtnPaymentsInv.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnPaymentsInv.Name = "BtnPaymentsInv";
            this.BtnPaymentsInv.SubItemsExpandWidth = 14;
            this.BtnPaymentsInv.Text = "Payments";
            this.BtnPaymentsInv.Tooltip = "Payments";
            this.BtnPaymentsInv.Click += new System.EventHandler(this.BtnPaymentsInv_Click);
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel1.Controls.Add(this.ribbonBar3);
            this.ribbonPanel1.Controls.Add(this.ribbonBar1);
            this.ribbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel1.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel1.Name = "ribbonPanel1";
            this.ribbonPanel1.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel1.Size = new System.Drawing.Size(1290, 90);
            // 
            // 
            // 
            this.ribbonPanel1.Style.Class = "";
            this.ribbonPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseDown.Class = "";
            this.ribbonPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseOver.Class = "";
            this.ribbonPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel1.TabIndex = 1;
            this.ribbonPanel1.Visible = false;
            // 
            // ribbonBar3
            // 
            this.ribbonBar3.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar3.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar3.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar3.BackgroundStyle.Class = "";
            this.ribbonBar3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar3.ContainerControlProcessDialogKey = true;
            this.ribbonBar3.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar3.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.Vendor,
            this.Customer,
            this.btnVendorMapping,
            this.btnChartofAcconuts,
            this.btnOP});
            this.ribbonBar3.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar3.Location = new System.Drawing.Point(415, 0);
            this.ribbonBar3.Name = "ribbonBar3";
            this.ribbonBar3.Size = new System.Drawing.Size(513, 87);
            this.ribbonBar3.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            superTooltipInfo3.BodyImage = ((System.Drawing.Image)(resources.GetObject("superTooltipInfo3.BodyImage")));
            superTooltipInfo3.Color = DevComponents.DotNetBar.eTooltipColor.Cyan;
            superTooltipInfo3.FooterText = "Add,Edit,Delete company information";
            superTooltipInfo3.HeaderText = "Company Information";
            this.superTooltip1.SetSuperTooltip(this.ribbonBar3, superTooltipInfo3);
            this.ribbonBar3.TabIndex = 1;
            this.ribbonBar3.Text = "Accounts";
            // 
            // 
            // 
            this.ribbonBar3.TitleStyle.Class = "";
            this.ribbonBar3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar3.TitleStyleMouseOver.Class = "";
            this.ribbonBar3.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar3.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // Vendor
            // 
            this.Vendor.FixedSize = new System.Drawing.Size(100, 10);
            this.Vendor.GlobalName = "Vendor";
            this.Vendor.Image = global::MyBooksERP.Properties.Resources.Supplier;
            this.Vendor.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Vendor.Name = "Vendor";
            this.Vendor.Text = "Supplier";
            this.Vendor.Tooltip = "Supplier";
            this.Vendor.Click += new System.EventHandler(this.Vendor_Click);
            // 
            // Customer
            // 
            this.Customer.FixedSize = new System.Drawing.Size(100, 10);
            this.Customer.GlobalName = "Customer";
            this.Customer.Image = ((System.Drawing.Image)(resources.GetObject("Customer.Image")));
            this.Customer.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Customer.Name = "Customer";
            this.Customer.Text = "Customer";
            this.Customer.Tooltip = "Customer";
            this.Customer.Click += new System.EventHandler(this.Customer_Click);
            // 
            // btnVendorMapping
            // 
            this.btnVendorMapping.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnVendorMapping.FixedSize = new System.Drawing.Size(100, 10);
            this.btnVendorMapping.Image = global::MyBooksERP.Properties.Resources.Vendor;
            this.btnVendorMapping.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnVendorMapping.Name = "btnVendorMapping";
            this.btnVendorMapping.SubItemsExpandWidth = 14;
            this.btnVendorMapping.Text = "Vendor Mapping";
            this.btnVendorMapping.Tooltip = "Opening Balance";
            this.btnVendorMapping.Click += new System.EventHandler(this.btnVendorMapping_Click);
            // 
            // btnChartofAcconuts
            // 
            this.btnChartofAcconuts.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnChartofAcconuts.FixedSize = new System.Drawing.Size(100, 10);
            this.btnChartofAcconuts.Image = global::MyBooksERP.Properties.Resources.Chart_Of_Accounts__Account;
            this.btnChartofAcconuts.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnChartofAcconuts.Name = "btnChartofAcconuts";
            this.btnChartofAcconuts.SubItemsExpandWidth = 14;
            this.btnChartofAcconuts.Text = "Chart of &Accounts";
            this.btnChartofAcconuts.Tooltip = "Chart of Accounts";
            this.btnChartofAcconuts.Click += new System.EventHandler(this.btnChartofAcconuts_Click);
            // 
            // btnOP
            // 
            this.btnOP.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnOP.FixedSize = new System.Drawing.Size(100, 10);
            this.btnOP.Image = global::MyBooksERP.Properties.Resources.Opening_Stock;
            this.btnOP.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnOP.Name = "btnOP";
            this.btnOP.SubItemsExpandWidth = 14;
            this.btnOP.Text = "Opening Balance";
            this.btnOP.Tooltip = "Opening Balance";
            this.btnOP.Click += new System.EventHandler(this.btnOP_Click);
            // 
            // ribbonBar1
            // 
            this.ribbonBar1.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundStyle.Class = "";
            this.ribbonBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar1.ContainerControlProcessDialogKey = true;
            this.ribbonBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.NewCompany,
            this.Warehouse,
            this.Currency,
            this.Employee});
            this.ribbonBar1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar1.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar1.Name = "ribbonBar1";
            this.ribbonBar1.Size = new System.Drawing.Size(412, 87);
            this.ribbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            superTooltipInfo4.BodyImage = ((System.Drawing.Image)(resources.GetObject("superTooltipInfo4.BodyImage")));
            superTooltipInfo4.Color = DevComponents.DotNetBar.eTooltipColor.Cyan;
            superTooltipInfo4.FooterText = "Add,Edit,Delete company information";
            superTooltipInfo4.HeaderText = "Company Information";
            this.superTooltip1.SetSuperTooltip(this.ribbonBar1, superTooltipInfo4);
            this.ribbonBar1.TabIndex = 0;
            this.ribbonBar1.Text = "&Masters";
            // 
            // 
            // 
            this.ribbonBar1.TitleStyle.Class = "";
            this.ribbonBar1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.TitleStyleMouseOver.Class = "";
            this.ribbonBar1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar1.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            this.ribbonBar1.LaunchDialog += new System.EventHandler(this.LaunchRibbonDialog);
            // 
            // NewCompany
            // 
            this.NewCompany.FixedSize = new System.Drawing.Size(100, 10);
            this.NewCompany.GlobalName = "NewCompany";
            this.NewCompany.Image = global::MyBooksERP.Properties.Resources.New_company;
            this.NewCompany.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.NewCompany.Name = "NewCompany";
            this.NewCompany.SplitButton = true;
            this.NewCompany.Text = "&Company";
            this.NewCompany.Tooltip = "Company";
            this.NewCompany.Click += new System.EventHandler(this.NewCompany_Click);
            // 
            // Warehouse
            // 
            this.Warehouse.FixedSize = new System.Drawing.Size(100, 10);
            this.Warehouse.Image = global::MyBooksERP.Properties.Resources.Warehouse2;
            this.Warehouse.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Warehouse.Name = "Warehouse";
            this.Warehouse.SubItemsExpandWidth = 14;
            this.Warehouse.Text = "&Warehouse";
            this.Warehouse.Tooltip = " Warehouse";
            this.Warehouse.Click += new System.EventHandler(this.Warehouse_Click);
            // 
            // Currency
            // 
            this.Currency.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.Currency.FixedSize = new System.Drawing.Size(100, 10);
            this.Currency.Image = global::MyBooksERP.Properties.Resources.Currency___Exchange_Rate2;
            this.Currency.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Currency.Name = "Currency";
            this.Currency.SubItemsExpandWidth = 14;
            this.Currency.Text = "&Currency";
            this.Currency.Tooltip = "Currency";
            this.Currency.Click += new System.EventHandler(this.btnCurrency_Click);
            // 
            // Employee
            // 
            this.Employee.FixedSize = new System.Drawing.Size(100, 10);
            this.Employee.GlobalName = "Executives";
            this.Employee.Image = ((System.Drawing.Image)(resources.GetObject("Employee.Image")));
            this.Employee.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Employee.Name = "Employee";
            this.Employee.Text = "&Employees";
            this.Employee.Tooltip = "Employees";
            this.Employee.Click += new System.EventHandler(this.Employee_Click);
            // 
            // ribbonPanel8
            // 
            this.ribbonPanel8.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel8.Controls.Add(this.ribbonBar16);
            this.ribbonPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel8.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel8.Name = "ribbonPanel8";
            this.ribbonPanel8.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel8.Size = new System.Drawing.Size(1290, 90);
            // 
            // 
            // 
            this.ribbonPanel8.Style.Class = "";
            this.ribbonPanel8.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel8.StyleMouseDown.Class = "";
            this.ribbonPanel8.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel8.StyleMouseOver.Class = "";
            this.ribbonPanel8.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel8.TabIndex = 9;
            this.ribbonPanel8.Visible = false;
            // 
            // ribbonBar16
            // 
            this.ribbonBar16.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar16.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar16.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar16.BackgroundStyle.Class = "";
            this.ribbonBar16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar16.ContainerControlProcessDialogKey = true;
            this.ribbonBar16.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar16.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.About});
            this.ribbonBar16.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar16.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar16.Name = "ribbonBar16";
            this.ribbonBar16.Size = new System.Drawing.Size(112, 87);
            this.ribbonBar16.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar16.TabIndex = 0;
            this.ribbonBar16.Text = "Help";
            // 
            // 
            // 
            this.ribbonBar16.TitleStyle.Class = "";
            this.ribbonBar16.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar16.TitleStyleMouseOver.Class = "";
            this.ribbonBar16.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar16.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // About
            // 
            this.About.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.About.FixedSize = new System.Drawing.Size(100, 10);
            this.About.Image = global::MyBooksERP.Properties.Resources.About;
            this.About.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.About.Name = "About";
            this.About.SubItemsExpandWidth = 14;
            this.About.Text = "&About ";
            this.About.Tooltip = "About My Books ERP";
            this.About.Click += new System.EventHandler(this.About_Click);
            // 
            // ribbonPanel7
            // 
            this.ribbonPanel7.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel7.Controls.Add(this.ribbonBar19);
            this.ribbonPanel7.Controls.Add(this.ribbonBar15);
            this.ribbonPanel7.Controls.Add(this.ribbonBar14);
            this.ribbonPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel7.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel7.Name = "ribbonPanel7";
            this.ribbonPanel7.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel7.Size = new System.Drawing.Size(1290, 90);
            // 
            // 
            // 
            this.ribbonPanel7.Style.Class = "";
            this.ribbonPanel7.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel7.StyleMouseDown.Class = "";
            this.ribbonPanel7.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel7.StyleMouseOver.Class = "";
            this.ribbonPanel7.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel7.TabIndex = 8;
            this.ribbonPanel7.Visible = false;
            // 
            // ribbonBar19
            // 
            this.ribbonBar19.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar19.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar19.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar19.BackgroundStyle.Class = "";
            this.ribbonBar19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar19.ContainerControlProcessDialogKey = true;
            this.ribbonBar19.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar19.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.CompanySettings,
            this.Uom,
            this.UomConversion,
            this.BtnDiscounts,
            this.BtnPricingScheme,
            this.BtnTermsAndConditions,
            this.AlertSettings,
            this.btnAccSettings,
            this.BackUp});
            this.ribbonBar19.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar19.Location = new System.Drawing.Point(387, 0);
            this.ribbonBar19.Name = "ribbonBar19";
            this.ribbonBar19.Size = new System.Drawing.Size(826, 87);
            this.ribbonBar19.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar19.TabIndex = 2;
            // 
            // 
            // 
            this.ribbonBar19.TitleStyle.Class = "";
            this.ribbonBar19.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar19.TitleStyleMouseOver.Class = "";
            this.ribbonBar19.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar19.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // CompanySettings
            // 
            this.CompanySettings.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.CompanySettings.FixedSize = new System.Drawing.Size(95, 10);
            this.CompanySettings.Image = ((System.Drawing.Image)(resources.GetObject("CompanySettings.Image")));
            this.CompanySettings.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.CompanySettings.Name = "CompanySettings";
            this.CompanySettings.SubItemsExpandWidth = 14;
            this.CompanySettings.Text = "Company &Settings";
            this.CompanySettings.Tooltip = "Company Settings";
            this.CompanySettings.Click += new System.EventHandler(this.CompanySettings_Click);
            // 
            // Uom
            // 
            this.Uom.FixedSize = new System.Drawing.Size(90, 10);
            this.Uom.Image = global::MyBooksERP.Properties.Resources.Unit_of_Measurement;
            this.Uom.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Uom.Name = "Uom";
            this.Uom.SubItemsExpandWidth = 14;
            this.Uom.Text = "U&om";
            this.Uom.Tooltip = "Unit Of Measurement";
            this.Uom.Click += new System.EventHandler(this.Uom_Click);
            // 
            // UomConversion
            // 
            this.UomConversion.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.UomConversion.FixedSize = new System.Drawing.Size(90, 10);
            this.UomConversion.Image = global::MyBooksERP.Properties.Resources.UMO_Conversion;
            this.UomConversion.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.UomConversion.Name = "UomConversion";
            this.UomConversion.SubItemsExpandWidth = 14;
            this.UomConversion.Text = "Uom Con&version";
            this.UomConversion.Tooltip = "Uom Conversion";
            this.UomConversion.Click += new System.EventHandler(this.UomConversion_Click);
            // 
            // BtnDiscounts
            // 
            this.BtnDiscounts.Name = "BtnDiscounts";
            // 
            // BtnPricingScheme
            // 
            this.BtnPricingScheme.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnPricingScheme.FixedSize = new System.Drawing.Size(90, 10);
            this.BtnPricingScheme.Image = global::MyBooksERP.Properties.Resources.Pricing_Scheme11;
            this.BtnPricingScheme.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnPricingScheme.Name = "BtnPricingScheme";
            this.BtnPricingScheme.SubItemsExpandWidth = 14;
            this.BtnPricingScheme.Text = "Pricing Sc&heme";
            this.BtnPricingScheme.Tooltip = "PricingScheme";
            this.BtnPricingScheme.Click += new System.EventHandler(this.BtnPricingScheme_Click);
            // 
            // BtnTermsAndConditions
            // 
            this.BtnTermsAndConditions.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnTermsAndConditions.FixedSize = new System.Drawing.Size(90, 10);
            this.BtnTermsAndConditions.Image = global::MyBooksERP.Properties.Resources.TermsandConditions1;
            this.BtnTermsAndConditions.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnTermsAndConditions.Name = "BtnTermsAndConditions";
            this.BtnTermsAndConditions.SubItemsExpandWidth = 14;
            this.BtnTermsAndConditions.Text = "&Terms && Conditions";
            this.BtnTermsAndConditions.Tooltip = "Terms && Conditions";
            this.BtnTermsAndConditions.Click += new System.EventHandler(this.BtnTermsAndConditions_Click_1);
            // 
            // AlertSettings
            // 
            this.AlertSettings.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.AlertSettings.FixedSize = new System.Drawing.Size(90, 10);
            this.AlertSettings.Image = global::MyBooksERP.Properties.Resources.Alert_Settings1;
            this.AlertSettings.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.AlertSettings.Name = "AlertSettings";
            this.AlertSettings.SubItemsExpandWidth = 14;
            this.AlertSettings.Text = "&Alert Settings";
            this.AlertSettings.Tooltip = "Alert Settings";
            this.AlertSettings.Click += new System.EventHandler(this.AlertSettings_Click);
            // 
            // btnAccSettings
            // 
            this.btnAccSettings.FixedSize = new System.Drawing.Size(100, 10);
            this.btnAccSettings.Image = global::MyBooksERP.Properties.Resources.Account_Settings;
            this.btnAccSettings.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAccSettings.Name = "btnAccSettings";
            this.btnAccSettings.Text = "Account Settings";
            this.btnAccSettings.Tooltip = "Account Settings";
            this.btnAccSettings.Click += new System.EventHandler(this.btnAccSettings_Click);
            // 
            // BackUp
            // 
            this.BackUp.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BackUp.FixedSize = new System.Drawing.Size(75, 10);
            this.BackUp.Image = global::MyBooksERP.Properties.Resources.Backup1;
            this.BackUp.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BackUp.Name = "BackUp";
            this.BackUp.SubItemsExpandWidth = 14;
            this.BackUp.Text = "&BackUp";
            this.BackUp.Tooltip = "BackUp Database";
            this.BackUp.Click += new System.EventHandler(this.BackUp_Click);
            // 
            // ribbonBar15
            // 
            this.ribbonBar15.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar15.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar15.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar15.BackgroundStyle.Class = "";
            this.ribbonBar15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar15.ContainerControlProcessDialogKey = true;
            this.ribbonBar15.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar15.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.User,
            this.UserRole,
            this.ChangePassword,
            this.BtnHierarchySetting});
            this.ribbonBar15.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar15.Location = new System.Drawing.Point(198, 0);
            this.ribbonBar15.Name = "ribbonBar15";
            this.ribbonBar15.Size = new System.Drawing.Size(189, 87);
            this.ribbonBar15.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar15.TabIndex = 1;
            this.ribbonBar15.Text = "User";
            // 
            // 
            // 
            this.ribbonBar15.TitleStyle.Class = "";
            this.ribbonBar15.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar15.TitleStyleMouseOver.Class = "";
            this.ribbonBar15.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar15.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // User
            // 
            this.User.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.User.FixedSize = new System.Drawing.Size(90, 10);
            this.User.GlobalName = "User";
            this.User.Image = global::MyBooksERP.Properties.Resources.User1;
            this.User.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.User.Name = "User";
            this.User.SubItemsExpandWidth = 14;
            this.User.Text = "&User";
            this.User.Tooltip = "User";
            this.User.Click += new System.EventHandler(this.User_Click);
            // 
            // UserRole
            // 
            this.UserRole.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.UserRole.FixedSize = new System.Drawing.Size(90, 10);
            this.UserRole.GlobalName = "UserRole";
            this.UserRole.Image = global::MyBooksERP.Properties.Resources.UserRole;
            this.UserRole.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.UserRole.Name = "UserRole";
            this.UserRole.SubItemsExpandWidth = 14;
            this.UserRole.Text = "&Role Settings";
            this.UserRole.Tooltip = "Role Settings";
            this.UserRole.Click += new System.EventHandler(this.UserRole_Click);
            // 
            // ChangePassword
            // 
            this.ChangePassword.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.ChangePassword.FixedSize = new System.Drawing.Size(95, 10);
            this.ChangePassword.Image = global::MyBooksERP.Properties.Resources.changePassword2;
            this.ChangePassword.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ChangePassword.Name = "ChangePassword";
            this.ChangePassword.SubItemsExpandWidth = 14;
            this.ChangePassword.Text = "Change &Password";
            this.ChangePassword.Tooltip = "Change Password";
            this.ChangePassword.Click += new System.EventHandler(this.ChangePassword_Click);
            // 
            // BtnHierarchySetting
            // 
            this.BtnHierarchySetting.FixedSize = new System.Drawing.Size(90, 10);
            this.BtnHierarchySetting.Image = global::MyBooksERP.Properties.Resources.User_Heirarchy;
            this.BtnHierarchySetting.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnHierarchySetting.Name = "BtnHierarchySetting";
            this.BtnHierarchySetting.SubItemsExpandWidth = 14;
            this.BtnHierarchySetting.Text = "Hierarchy Settings";
            this.BtnHierarchySetting.Tooltip = "Hierarchy Settings";
            this.BtnHierarchySetting.Visible = false;
            this.BtnHierarchySetting.Click += new System.EventHandler(this.BtnHierarchySetting_Click);
            // 
            // ribbonBar14
            // 
            this.ribbonBar14.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar14.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar14.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar14.BackgroundStyle.Class = "";
            this.ribbonBar14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar14.ContainerControlProcessDialogKey = true;
            this.ribbonBar14.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar14.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.Configuration,
            this.Email});
            this.ribbonBar14.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar14.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar14.Name = "ribbonBar14";
            this.ribbonBar14.Size = new System.Drawing.Size(195, 87);
            this.ribbonBar14.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar14.TabIndex = 0;
            this.ribbonBar14.Text = "Configuration";
            // 
            // 
            // 
            this.ribbonBar14.TitleStyle.Class = "";
            this.ribbonBar14.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar14.TitleStyleMouseOver.Class = "";
            this.ribbonBar14.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar14.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // Configuration
            // 
            this.Configuration.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.Configuration.FixedSize = new System.Drawing.Size(90, 10);
            this.Configuration.Image = global::MyBooksERP.Properties.Resources.Configuration3;
            this.Configuration.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Configuration.Name = "Configuration";
            this.Configuration.SubItemsExpandWidth = 14;
            this.Configuration.Text = "&Configuration";
            this.Configuration.Tooltip = "Configuration";
            this.Configuration.Click += new System.EventHandler(this.Configuration_Click);
            // 
            // Email
            // 
            this.Email.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.Email.FixedSize = new System.Drawing.Size(90, 10);
            this.Email.Image = global::MyBooksERP.Properties.Resources.EmailSettings;
            this.Email.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Email.Name = "Email";
            this.Email.SubItemsExpandWidth = 14;
            this.Email.Text = "&Email";
            this.Email.Tooltip = "Email";
            this.Email.Click += new System.EventHandler(this.Email_Click);
            // 
            // ribbonPanel10
            // 
            this.ribbonPanel10.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel10.Controls.Add(this.ribbonBar12);
            this.ribbonPanel10.Controls.Add(this.ribbonBar11);
            this.ribbonPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel10.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel10.Name = "ribbonPanel10";
            this.ribbonPanel10.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel10.Size = new System.Drawing.Size(1290, 90);
            // 
            // 
            // 
            this.ribbonPanel10.Style.Class = "";
            this.ribbonPanel10.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel10.StyleMouseDown.Class = "";
            this.ribbonPanel10.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel10.StyleMouseOver.Class = "";
            this.ribbonPanel10.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel10.TabIndex = 14;
            this.ribbonPanel10.Visible = false;
            // 
            // ribbonBar12
            // 
            this.ribbonBar12.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar12.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar12.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar12.BackgroundStyle.Class = "";
            this.ribbonBar12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar12.ContainerControlProcessDialogKey = true;
            this.ribbonBar12.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar12.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.Document,
            this.btnAlert});
            this.ribbonBar12.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar12.Location = new System.Drawing.Point(208, 0);
            this.ribbonBar12.Name = "ribbonBar12";
            this.ribbonBar12.Size = new System.Drawing.Size(205, 87);
            this.ribbonBar12.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar12.TabIndex = 8;
            this.ribbonBar12.Text = "Reports";
            // 
            // 
            // 
            this.ribbonBar12.TitleStyle.Class = "";
            this.ribbonBar12.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar12.TitleStyleMouseOver.Class = "";
            this.ribbonBar12.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar12.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            this.ribbonBar12.Visible = false;
            // 
            // Document
            // 
            this.Document.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.Document.FixedSize = new System.Drawing.Size(100, 10);
            this.Document.Image = global::MyBooksERP.Properties.Resources.document_register;
            this.Document.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Document.Name = "Document";
            this.Document.SubItemsExpandWidth = 14;
            this.Document.Text = "Document";
            this.Document.Tooltip = "Document";
            // 
            // btnAlert
            // 
            this.btnAlert.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAlert.FixedSize = new System.Drawing.Size(100, 10);
            this.btnAlert.Image = ((System.Drawing.Image)(resources.GetObject("btnAlert.Image")));
            this.btnAlert.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAlert.Name = "btnAlert";
            this.btnAlert.SubItemsExpandWidth = 14;
            this.btnAlert.Text = "Alerts";
            this.btnAlert.Tooltip = "Alerts";
            // 
            // ribbonBar11
            // 
            this.ribbonBar11.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar11.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar11.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar11.BackgroundStyle.Class = "";
            this.ribbonBar11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar11.ContainerControlProcessDialogKey = true;
            this.ribbonBar11.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar11.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnNewDocument,
            this.StockRegister});
            this.ribbonBar11.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar11.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar11.Name = "ribbonBar11";
            this.ribbonBar11.Size = new System.Drawing.Size(205, 87);
            this.ribbonBar11.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar11.TabIndex = 7;
            // 
            // 
            // 
            this.ribbonBar11.TitleStyle.Class = "";
            this.ribbonBar11.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar11.TitleStyleMouseOver.Class = "";
            this.ribbonBar11.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar11.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnNewDocument
            // 
            this.btnNewDocument.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnNewDocument.FixedSize = new System.Drawing.Size(100, 10);
            this.btnNewDocument.Image = global::MyBooksERP.Properties.Resources.Document_handover;
            this.btnNewDocument.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnNewDocument.Name = "btnNewDocument";
            this.btnNewDocument.SubItemsExpandWidth = 14;
            this.btnNewDocument.Text = "Documents";
            this.btnNewDocument.Tooltip = "Documents";
            this.btnNewDocument.Click += new System.EventHandler(this.btnNewDocument_Click);
            // 
            // StockRegister
            // 
            this.StockRegister.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.StockRegister.FixedSize = new System.Drawing.Size(90, 10);
            this.StockRegister.Image = ((System.Drawing.Image)(resources.GetObject("StockRegister.Image")));
            this.StockRegister.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.StockRegister.Name = "StockRegister";
            this.StockRegister.SubItemsExpandWidth = 14;
            this.StockRegister.Text = "Register";
            this.StockRegister.Tooltip = "Register";
            this.StockRegister.Click += new System.EventHandler(this.StockRegister_Click);
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel4.Controls.Add(this.ribbonBar9);
            this.ribbonPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel4.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel4.Name = "ribbonPanel4";
            this.ribbonPanel4.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel4.Size = new System.Drawing.Size(1290, 90);
            // 
            // 
            // 
            this.ribbonPanel4.Style.Class = "";
            this.ribbonPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseDown.Class = "";
            this.ribbonPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseOver.Class = "";
            this.ribbonPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel4.TabIndex = 5;
            this.ribbonPanel4.Visible = false;
            // 
            // ribbonBar9
            // 
            this.ribbonBar9.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar9.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar9.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar9.BackgroundStyle.Class = "";
            this.ribbonBar9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar9.ContainerControlProcessDialogKey = true;
            this.ribbonBar9.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar9.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.SalesQuotation,
            this.SalesOrder,
            this.SalesInvoice,
            this.BtnCreditNote,
            this.POS,
            this.Receipts,
            this.btnSalesExtraCharge,
            this.btnSalesExtraChargePayment});
            this.ribbonBar9.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar9.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar9.Name = "ribbonBar9";
            this.ribbonBar9.Size = new System.Drawing.Size(817, 87);
            this.ribbonBar9.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar9.TabIndex = 0;
            this.ribbonBar9.Text = "Sales";
            // 
            // 
            // 
            this.ribbonBar9.TitleStyle.Class = "";
            this.ribbonBar9.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar9.TitleStyleMouseOver.Class = "";
            this.ribbonBar9.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar9.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // SalesQuotation
            // 
            this.SalesQuotation.Category = "SalesQuotation";
            this.SalesQuotation.FixedSize = new System.Drawing.Size(100, 10);
            this.SalesQuotation.Image = global::MyBooksERP.Properties.Resources.Sales_quotation;
            this.SalesQuotation.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.SalesQuotation.Name = "SalesQuotation";
            this.SalesQuotation.SubItemsExpandWidth = 14;
            this.SalesQuotation.Text = "Sales &Quotation";
            this.SalesQuotation.Tooltip = "Sales Quotation";
            this.SalesQuotation.Click += new System.EventHandler(this.SalesQuotation_Click);
            // 
            // SalesOrder
            // 
            this.SalesOrder.FixedSize = new System.Drawing.Size(100, 10);
            this.SalesOrder.GlobalName = "SalesOrder";
            this.SalesOrder.Image = global::MyBooksERP.Properties.Resources.sales_order;
            this.SalesOrder.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.SalesOrder.Name = "SalesOrder";
            this.SalesOrder.SubItemsExpandWidth = 14;
            this.SalesOrder.Text = "Sales &Order";
            this.SalesOrder.Tooltip = "Sales Order";
            this.SalesOrder.Click += new System.EventHandler(this.SalesOrder_Click);
            // 
            // SalesInvoice
            // 
            this.SalesInvoice.FixedSize = new System.Drawing.Size(100, 10);
            this.SalesInvoice.GlobalName = "SalesInvoice";
            this.SalesInvoice.Image = global::MyBooksERP.Properties.Resources.Sales_invoice;
            this.SalesInvoice.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.SalesInvoice.Name = "SalesInvoice";
            this.SalesInvoice.SubItemsExpandWidth = 14;
            this.SalesInvoice.Text = "Sales &Invoice";
            this.SalesInvoice.Tooltip = "Sales Invoice";
            this.SalesInvoice.Click += new System.EventHandler(this.SalesInvoice_Click);
            // 
            // BtnCreditNote
            // 
            this.BtnCreditNote.FixedSize = new System.Drawing.Size(100, 10);
            this.BtnCreditNote.Image = global::MyBooksERP.Properties.Resources.CreditDebit_Note;
            this.BtnCreditNote.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnCreditNote.Name = "BtnCreditNote";
            this.BtnCreditNote.SubItemsExpandWidth = 14;
            this.BtnCreditNote.Text = "Credit &Note";
            this.BtnCreditNote.Tooltip = "Credit Note";
            this.BtnCreditNote.Click += new System.EventHandler(this.BtnCreditNote_Click);
            // 
            // POS
            // 
            this.POS.FixedSize = new System.Drawing.Size(100, 10);
            this.POS.GlobalName = "POS";
            this.POS.Image = global::MyBooksERP.Properties.Resources.Point_of_sale;
            this.POS.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.POS.Name = "POS";
            this.POS.SubItemsExpandWidth = 14;
            this.POS.Text = "&Point Of Sales";
            this.POS.Tooltip = "Point Of Sales";
            this.POS.Visible = false;
            this.POS.Click += new System.EventHandler(this.POS_Click);
            // 
            // Receipts
            // 
            this.Receipts.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.Receipts.FixedSize = new System.Drawing.Size(100, 10);
            this.Receipts.GlobalName = "Receipts";
            this.Receipts.Image = global::MyBooksERP.Properties.Resources.Receipts;
            this.Receipts.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Receipts.Name = "Receipts";
            this.Receipts.SubItemsExpandWidth = 14;
            this.Receipts.Text = "&Receipts";
            this.Receipts.Tooltip = "Receipts";
            this.Receipts.Click += new System.EventHandler(this.Receipts_Click);
            // 
            // btnSalesExtraCharge
            // 
            this.btnSalesExtraCharge.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSalesExtraCharge.FixedSize = new System.Drawing.Size(100, 10);
            this.btnSalesExtraCharge.Image = global::MyBooksERP.Properties.Resources.Extra_Charges;
            this.btnSalesExtraCharge.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSalesExtraCharge.Name = "btnSalesExtraCharge";
            this.btnSalesExtraCharge.SubItemsExpandWidth = 14;
            this.btnSalesExtraCharge.Text = "Extra Charge";
            this.btnSalesExtraCharge.Tooltip = "Extra Charge";
            this.btnSalesExtraCharge.Visible = false;
            this.btnSalesExtraCharge.Click += new System.EventHandler(this.btnSalesExtraCharge_Click);
            // 
            // btnSalesExtraChargePayment
            // 
            this.btnSalesExtraChargePayment.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSalesExtraChargePayment.FixedSize = new System.Drawing.Size(100, 10);
            this.btnSalesExtraChargePayment.Image = global::MyBooksERP.Properties.Resources.Payments___Receipts;
            this.btnSalesExtraChargePayment.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSalesExtraChargePayment.Name = "btnSalesExtraChargePayment";
            this.btnSalesExtraChargePayment.SubItemsExpandWidth = 14;
            this.btnSalesExtraChargePayment.Text = "Extra Charge Payment";
            this.btnSalesExtraChargePayment.Tooltip = "Extra Charge Payment";
            this.btnSalesExtraChargePayment.Visible = false;
            this.btnSalesExtraChargePayment.Click += new System.EventHandler(this.btnSalesExtraChargePayment_Click);
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel3.Controls.Add(this.ribbonBar5);
            this.ribbonPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel3.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel3.Name = "ribbonPanel3";
            this.ribbonPanel3.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel3.Size = new System.Drawing.Size(1290, 90);
            // 
            // 
            // 
            this.ribbonPanel3.Style.Class = "";
            this.ribbonPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseDown.Class = "";
            this.ribbonPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseOver.Class = "";
            this.ribbonPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel3.TabIndex = 3;
            this.ribbonPanel3.Visible = false;
            // 
            // ribbonBar5
            // 
            this.ribbonBar5.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar5.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar5.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar5.BackgroundStyle.Class = "";
            this.ribbonBar5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar5.ContainerControlProcessDialogKey = true;
            this.ribbonBar5.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar5.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.PurchaseQuotation,
            this.PurchaseOrder,
            this.PurchaseInvoice,
            this.GoodsReceiptNote,
            this.DirectGRN,
            this.BtnDebitNote,
            this.Payments,
            this.btnPurExtraChargePayment});
            this.ribbonBar5.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar5.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar5.Name = "ribbonBar5";
            this.ribbonBar5.Size = new System.Drawing.Size(823, 87);
            this.ribbonBar5.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar5.TabIndex = 1;
            this.ribbonBar5.Text = "Purchase";
            // 
            // 
            // 
            this.ribbonBar5.TitleStyle.Class = "";
            this.ribbonBar5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar5.TitleStyleMouseOver.Class = "";
            this.ribbonBar5.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar5.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            this.ribbonBar5.LaunchDialog += new System.EventHandler(this.LaunchRibbonDialog);
            // 
            // PurchaseQuotation
            // 
            this.PurchaseQuotation.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.PurchaseQuotation.FixedSize = new System.Drawing.Size(100, 10);
            this.PurchaseQuotation.Image = global::MyBooksERP.Properties.Resources.Purchase_Quotation;
            this.PurchaseQuotation.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.PurchaseQuotation.Name = "PurchaseQuotation";
            this.PurchaseQuotation.SubItemsExpandWidth = 14;
            this.PurchaseQuotation.Text = "Purchase &Quotation";
            this.PurchaseQuotation.Tooltip = "Purchase Quotation";
            this.PurchaseQuotation.Click += new System.EventHandler(this.PurchaseQuotation_Click);
            // 
            // PurchaseOrder
            // 
            this.PurchaseOrder.FixedSize = new System.Drawing.Size(100, 10);
            this.PurchaseOrder.GlobalName = "PurchaseOrder";
            this.PurchaseOrder.Image = global::MyBooksERP.Properties.Resources.Purchase_Order;
            this.PurchaseOrder.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.PurchaseOrder.Name = "PurchaseOrder";
            this.PurchaseOrder.Text = "Purchase &Order";
            this.PurchaseOrder.Tooltip = "Purchase Order";
            this.PurchaseOrder.Click += new System.EventHandler(this.PurchaseOrder_Click);
            // 
            // PurchaseInvoice
            // 
            this.PurchaseInvoice.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.PurchaseInvoice.FixedSize = new System.Drawing.Size(100, 10);
            this.PurchaseInvoice.GlobalName = "PurchaseInvoice";
            this.PurchaseInvoice.Image = global::MyBooksERP.Properties.Resources.Purchase_Invoice;
            this.PurchaseInvoice.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.PurchaseInvoice.Name = "PurchaseInvoice";
            this.PurchaseInvoice.Text = "Purchase &Invoice";
            this.PurchaseInvoice.Tooltip = "Purchase Invoice";
            this.PurchaseInvoice.Click += new System.EventHandler(this.PurchaseInvoice_Click);
            // 
            // GoodsReceiptNote
            // 
            this.GoodsReceiptNote.FixedSize = new System.Drawing.Size(100, 10);
            this.GoodsReceiptNote.GlobalName = "GoodsReceiptNote";
            this.GoodsReceiptNote.Image = global::MyBooksERP.Properties.Resources.GRN2;
            this.GoodsReceiptNote.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.GoodsReceiptNote.Name = "GoodsReceiptNote";
            this.GoodsReceiptNote.SubItemsExpandWidth = 14;
            this.GoodsReceiptNote.Text = "&Goods Receipt Note(GRN)";
            this.GoodsReceiptNote.Tooltip = "Goods Receipt Note";
            this.GoodsReceiptNote.Click += new System.EventHandler(this.GoodsReceiptNote_Click);
            // 
            // DirectGRN
            // 
            this.DirectGRN.FixedSize = new System.Drawing.Size(100, 10);
            this.DirectGRN.GlobalName = "DirectGoodsReceiptNote";
            this.DirectGRN.Image = global::MyBooksERP.Properties.Resources.GRN2;
            this.DirectGRN.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.DirectGRN.Name = "DirectGRN";
            this.DirectGRN.SubItemsExpandWidth = 14;
            this.DirectGRN.Text = "&Direct Goods Receipt Note(GRN)";
            this.DirectGRN.Tooltip = "Direct Goods Receipt Note";
            this.DirectGRN.Click += new System.EventHandler(this.DirectGRN_Click);
            // 
            // BtnDebitNote
            // 
            this.BtnDebitNote.FixedSize = new System.Drawing.Size(100, 10);
            this.BtnDebitNote.Image = global::MyBooksERP.Properties.Resources.DebitNote1;
            this.BtnDebitNote.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnDebitNote.Name = "BtnDebitNote";
            this.BtnDebitNote.SubItemsExpandWidth = 14;
            this.BtnDebitNote.Text = "&Debit Note";
            this.BtnDebitNote.Tooltip = "Debit Note";
            this.BtnDebitNote.Click += new System.EventHandler(this.BtnDebitNote_Click);
            // 
            // Payments
            // 
            this.Payments.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.Payments.FixedSize = new System.Drawing.Size(100, 10);
            this.Payments.GlobalName = "Payments";
            this.Payments.Image = global::MyBooksERP.Properties.Resources.Payments;
            this.Payments.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Payments.Name = "Payments";
            this.Payments.SubItemsExpandWidth = 14;
            this.Payments.Text = "Payments";
            this.Payments.Tooltip = "Payments";
            this.Payments.Click += new System.EventHandler(this.Payments_Click);
            // 
            // btnPurExtraChargePayment
            // 
            this.btnPurExtraChargePayment.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPurExtraChargePayment.FixedSize = new System.Drawing.Size(100, 10);
            this.btnPurExtraChargePayment.Image = global::MyBooksERP.Properties.Resources.Payments___Receipts;
            this.btnPurExtraChargePayment.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPurExtraChargePayment.Name = "btnPurExtraChargePayment";
            this.btnPurExtraChargePayment.SubItemsExpandWidth = 14;
            this.btnPurExtraChargePayment.Text = "Extra Charge Payment";
            this.btnPurExtraChargePayment.Tooltip = "Extra Charge Payment";
            this.btnPurExtraChargePayment.Click += new System.EventHandler(this.btnPurExtraChargePayment_Click);
            // 
            // Home
            // 
            this.Home.Checked = true;
            this.Home.HotFontBold = true;
            this.Home.Name = "Home";
            this.Home.Panel = this.rpHome;
            this.Home.Text = "&Home";
            this.Home.Tooltip = "Home";
            this.Home.Click += new System.EventHandler(this.Home_Click);
            // 
            // Company
            // 
            this.Company.GlobalName = "Company";
            this.Company.HotFontBold = true;
            this.Company.Name = "Company";
            this.Company.Panel = this.ribbonPanel1;
            this.Company.Text = "&Masters";
            this.Company.Tooltip = "Masters Module";
            // 
            // Purchase
            // 
            this.Purchase.GlobalName = "Purchase";
            this.Purchase.HotFontBold = true;
            this.Purchase.Name = "Purchase";
            this.Purchase.Panel = this.ribbonPanel3;
            this.Purchase.Text = "&Purchase";
            this.Purchase.Tooltip = "Purchase Module";
            // 
            // Sales
            // 
            this.Sales.GlobalName = "Sales";
            this.Sales.HotFontBold = true;
            this.Sales.Name = "Sales";
            this.Sales.Panel = this.ribbonPanel4;
            this.Sales.Text = "&Sales";
            this.Sales.Tooltip = "Sales Module";
            // 
            // Inventory
            // 
            this.Inventory.HotFontBold = true;
            this.Inventory.Name = "Inventory";
            this.Inventory.Panel = this.Inventory1;
            this.Inventory.Text = "&Inventory";
            this.Inventory.Tooltip = "Inventory Module";
            this.Inventory.Click += new System.EventHandler(this.Inventory_Click);
            // 
            // TradingReports
            // 
            this.TradingReports.HotFontBold = true;
            this.TradingReports.Name = "TradingReports";
            this.TradingReports.Panel = this.ribbonPanel6;
            this.TradingReports.Text = "Trading Reports";
            this.TradingReports.Tooltip = "Trading Reports";
            this.TradingReports.Click += new System.EventHandler(this.Reports_Click);
            // 
            // rtiPayroll
            // 
            this.rtiPayroll.Name = "rtiPayroll";
            this.rtiPayroll.Panel = this.ribbonPanel11;
            this.rtiPayroll.Text = "Payroll";
            // 
            // rtiPayrollReports
            // 
            this.rtiPayrollReports.Name = "rtiPayrollReports";
            this.rtiPayrollReports.Panel = this.ribbonPanel12;
            this.rtiPayrollReports.Text = "Payroll Reports";
            // 
            // AccountTransactions
            // 
            this.AccountTransactions.Name = "AccountTransactions";
            this.AccountTransactions.Panel = this.ribbonPanel2;
            this.AccountTransactions.Text = "Vouchers";
            // 
            // AccountSummary
            // 
            this.AccountSummary.Name = "AccountSummary";
            this.AccountSummary.Panel = this.ribbonPanel5;
            this.AccountSummary.Text = "Account Summary";
            // 
            // AccountReports
            // 
            this.AccountReports.Name = "AccountReports";
            this.AccountReports.Panel = this.ribbonPanel9;
            this.AccountReports.Text = "Account Reports";
            // 
            // Documents
            // 
            this.Documents.Name = "Documents";
            this.Documents.Panel = this.ribbonPanel10;
            this.Documents.Text = "Documents";
            // 
            // Settings
            // 
            this.Settings.GlobalName = "Settings";
            this.Settings.HotFontBold = true;
            this.Settings.Name = "Settings";
            this.Settings.Panel = this.ribbonPanel7;
            this.Settings.Text = "Se&ttings";
            this.Settings.Tooltip = "Settings Module";
            // 
            // Help
            // 
            this.Help.HotFontBold = true;
            this.Help.Name = "Help";
            this.Help.Panel = this.ribbonPanel8;
            this.Help.Text = "H&elp";
            this.Help.Tooltip = "Help";
            // 
            // buttonChangeStyle
            // 
            this.buttonChangeStyle.AutoExpandOnClick = true;
            this.buttonChangeStyle.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Far;
            this.buttonChangeStyle.Name = "buttonChangeStyle";
            this.buttonChangeStyle.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonStyleOffice2007Blue,
            this.buttonStyleOffice2007Black,
            this.buttonStyleOffice2007Silver,
            this.buttonItem60,
            this.buttonStyleCustom});
            superTooltipInfo5.BodyText = "Change the style of all DotNetBar User Interface elements.";
            superTooltipInfo5.HeaderText = "Change the style";
            this.superTooltip1.SetSuperTooltip(this.buttonChangeStyle, superTooltipInfo5);
            this.buttonChangeStyle.Text = "Style";
            // 
            // buttonStyleOffice2007Blue
            // 
            this.buttonStyleOffice2007Blue.Command = this.AppCommandTheme;
            this.buttonStyleOffice2007Blue.CommandParameter = "Blue";
            this.buttonStyleOffice2007Blue.Name = "buttonStyleOffice2007Blue";
            this.buttonStyleOffice2007Blue.OptionGroup = "style";
            this.buttonStyleOffice2007Blue.Text = "Office 2007 <font color=\"Blue\"><b>Blue</b></font>";
            // 
            // AppCommandTheme
            // 
            this.AppCommandTheme.Name = "AppCommandTheme";
            this.AppCommandTheme.Executed += new System.EventHandler(this.AppCommandTheme_Executed);
            // 
            // buttonStyleOffice2007Black
            // 
            this.buttonStyleOffice2007Black.Command = this.AppCommandTheme;
            this.buttonStyleOffice2007Black.CommandParameter = "Black";
            this.buttonStyleOffice2007Black.Name = "buttonStyleOffice2007Black";
            this.buttonStyleOffice2007Black.OptionGroup = "style";
            this.buttonStyleOffice2007Black.Text = "Office 2007 <font color=\"black\"><b>Black</b></font>";
            // 
            // buttonStyleOffice2007Silver
            // 
            this.buttonStyleOffice2007Silver.Command = this.AppCommandTheme;
            this.buttonStyleOffice2007Silver.CommandParameter = "Silver";
            this.buttonStyleOffice2007Silver.Name = "buttonStyleOffice2007Silver";
            this.buttonStyleOffice2007Silver.OptionGroup = "style";
            this.buttonStyleOffice2007Silver.Text = "Office 2007 <font color=\"Silver\"><b>Silver</b></font>";
            // 
            // buttonItem60
            // 
            this.buttonItem60.Checked = true;
            this.buttonItem60.Command = this.AppCommandTheme;
            this.buttonItem60.CommandParameter = "VistaGlass";
            this.buttonItem60.Name = "buttonItem60";
            this.buttonItem60.OptionGroup = "style";
            this.buttonItem60.Text = "Mindsoft";
            // 
            // buttonStyleCustom
            // 
            this.buttonStyleCustom.BeginGroup = true;
            this.buttonStyleCustom.Command = this.AppCommandTheme;
            this.buttonStyleCustom.Name = "buttonStyleCustom";
            this.buttonStyleCustom.Text = "Custom scheme";
            this.buttonStyleCustom.Tooltip = "Custom color scheme is created based on currently selected color table. Try selec" +
                "ting Silver or Blue color table and then creating custom color scheme.";
            this.buttonStyleCustom.SelectedColorChanged += new System.EventHandler(this.buttonStyleCustom_SelectedColorChanged);
            this.buttonStyleCustom.ColorPreview += new DevComponents.DotNetBar.ColorPreviewEventHandler(this.buttonStyleCustom_ColorPreview);
            this.buttonStyleCustom.ExpandChange += new System.EventHandler(this.buttonStyleCustom_ExpandChange);
            // 
            // buttonFile
            // 
            this.buttonFile.AutoExpandOnClick = true;
            this.buttonFile.CanCustomize = false;
            this.buttonFile.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.buttonFile.Icon = ((System.Drawing.Icon)(resources.GetObject("buttonFile.Icon")));
            this.buttonFile.Image = ((System.Drawing.Image)(resources.GetObject("buttonFile.Image")));
            this.buttonFile.ImagePaddingHorizontal = 2;
            this.buttonFile.ImagePaddingVertical = 2;
            this.buttonFile.Name = "buttonFile";
            this.buttonFile.ShowSubItems = false;
            this.buttonFile.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnLogout});
            this.buttonFile.Text = "&File";
            // 
            // btnLogout
            // 
            this.btnLogout.Image = ((System.Drawing.Image)(resources.GetObject("btnLogout.Image")));
            this.btnLogout.ImageFixedSize = new System.Drawing.Size(24, 24);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Text = "Log Out";
            this.btnLogout.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // btnHome
            // 
            this.btnHome.Name = "btnHome";
            this.btnHome.Text = "Home";
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // bSales
            // 
            this.bSales.Image = global::MyBooksERP.Properties.Resources.Minimize;
            this.bSales.Name = "bSales";
            this.bSales.Text = "Minimize Menu";
            this.bSales.Tooltip = "Minimize Menu";
            this.bSales.Click += new System.EventHandler(this.bSales_Click);
            // 
            // qatCustomizeItem1
            // 
            this.qatCustomizeItem1.Name = "qatCustomizeItem1";
            this.qatCustomizeItem1.Visible = false;
            // 
            // ribbonTabItemGroup1
            // 
            this.ribbonTabItemGroup1.Color = DevComponents.DotNetBar.eRibbonTabGroupColor.Orange;
            this.ribbonTabItemGroup1.GroupTitle = "Smart";
            this.ribbonTabItemGroup1.Name = "ribbonTabItemGroup1";
            // 
            // 
            // 
            this.ribbonTabItemGroup1.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(158)))), ((int)(((byte)(159)))));
            this.ribbonTabItemGroup1.Style.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(225)))), ((int)(((byte)(226)))));
            this.ribbonTabItemGroup1.Style.BackColorGradientAngle = 90;
            this.ribbonTabItemGroup1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderBottomWidth = 1;
            this.ribbonTabItemGroup1.Style.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(58)))), ((int)(((byte)(59)))));
            this.ribbonTabItemGroup1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderLeftWidth = 1;
            this.ribbonTabItemGroup1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderRightWidth = 1;
            this.ribbonTabItemGroup1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderTopWidth = 1;
            this.ribbonTabItemGroup1.Style.Class = "";
            this.ribbonTabItemGroup1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonTabItemGroup1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.ribbonTabItemGroup1.Style.TextColor = System.Drawing.Color.Black;
            this.ribbonTabItemGroup1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // AppCommandExit
            // 
            this.AppCommandExit.Name = "AppCommandExit";
            this.AppCommandExit.Executed += new System.EventHandler(this.AppCommandExit_Executed);
            // 
            // AppCommandNew
            // 
            this.AppCommandNew.Name = "AppCommandNew";
            // 
            // buttonItem47
            // 
            this.buttonItem47.BeginGroup = true;
            this.buttonItem47.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem47.Image")));
            this.buttonItem47.Name = "buttonItem47";
            this.buttonItem47.Text = "Search for Templates Online...";
            // 
            // buttonItem48
            // 
            this.buttonItem48.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem48.Image")));
            this.buttonItem48.Name = "buttonItem48";
            this.buttonItem48.Text = "Browse for Templates...";
            // 
            // buttonItem49
            // 
            this.buttonItem49.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem49.Image")));
            this.buttonItem49.Name = "buttonItem49";
            this.buttonItem49.Text = "Save Current Template...";
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "6";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "7";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "8";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "9";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "10";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "11";
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "12";
            // 
            // comboItem8
            // 
            this.comboItem8.Text = "13";
            // 
            // comboItem9
            // 
            this.comboItem9.Text = "14";
            // 
            // superTooltip1
            // 
            this.superTooltip1.DefaultFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.superTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.superTooltip1.MinimumTooltipSize = new System.Drawing.Size(150, 50);
            // 
            // AppCmdPurchase
            // 
            this.AppCmdPurchase.Name = "AppCmdPurchase";
            this.AppCmdPurchase.Executed += new System.EventHandler(this.AppCmdPurchase_Executed);
            // 
            // AppCmdSales
            // 
            this.AppCmdSales.Name = "AppCmdSales";
            this.AppCmdSales.Executed += new System.EventHandler(this.AppCmdSales_Executed);
            // 
            // AppCmdRoleSettings
            // 
            this.AppCmdRoleSettings.Name = "AppCmdRoleSettings";
            this.AppCmdRoleSettings.Executed += new System.EventHandler(this.AppCmdRoleSettings_Executed);
            // 
            // bwrAlert
            // 
            this.bwrAlert.WorkerSupportsCancellation = true;
            this.bwrAlert.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwrAlert_DoWork);
            this.bwrAlert.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwrAlert_RunWorkerCompleted);
            // 
            // tmrAlert
            // 
            this.tmrAlert.Tick += new System.EventHandler(this.tmrAlert_Tick);
            // 
            // AppCommandReports
            // 
            this.AppCommandReports.Name = "AppCommandReports";
            this.AppCommandReports.Executed += new System.EventHandler(this.AppCommandReports_Executed);
            // 
            // dotNetBarManager1
            // 
            this.dotNetBarManager1.BottomDockSite = this.barBottomDockSite;
            this.dotNetBarManager1.LeftDockSite = this.barLeftDockSite;
            this.dotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.dotNetBarManager1.MdiSystemItemVisible = false;
            this.dotNetBarManager1.ParentForm = this;
            this.dotNetBarManager1.RightDockSite = this.barRightDockSite;
            this.dotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.dotNetBarManager1.ToolbarBottomDockSite = this.dockSite4;
            this.dotNetBarManager1.ToolbarLeftDockSite = this.dockSite1;
            this.dotNetBarManager1.ToolbarRightDockSite = this.dockSite2;
            this.dotNetBarManager1.ToolbarTopDockSite = this.dockSite3;
            this.dotNetBarManager1.TopDockSite = this.barTopDockSite;
            // 
            // barBottomDockSite
            // 
            this.barBottomDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.barBottomDockSite.Controls.Add(this.barTaskList);
            this.barBottomDockSite.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barBottomDockSite.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer(new DevComponents.DotNetBar.DocumentBaseContainer[] {
            ((DevComponents.DotNetBar.DocumentBaseContainer)(new DevComponents.DotNetBar.DocumentBarContainer(this.barTaskList, 1290, 116)))}, DevComponents.DotNetBar.eOrientation.Vertical);
            this.barBottomDockSite.Location = new System.Drawing.Point(5, 357);
            this.barBottomDockSite.Name = "barBottomDockSite";
            this.barBottomDockSite.Size = new System.Drawing.Size(1290, 119);
            this.barBottomDockSite.TabIndex = 16;
            this.barBottomDockSite.TabStop = false;
            // 
            // barTaskList
            // 
            this.barTaskList.AccessibleDescription = "DotNetBar Bar (barTaskList)";
            this.barTaskList.AccessibleName = "DotNetBar Bar";
            this.barTaskList.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.barTaskList.AlwaysDisplayDockTab = true;
            this.barTaskList.ContextMenuStrip = this.cmnuAlertOptions;
            this.barTaskList.Controls.Add(this.PanelDockContainer1);
            this.barTaskList.Controls.Add(this.PanelDockContainer2);
            this.barTaskList.FadeEffect = true;
            this.barTaskList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barTaskList.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Caption;
            this.barTaskList.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.dockRss,
            this.dockAlertList});
            this.barTaskList.LayoutType = DevComponents.DotNetBar.eLayoutType.DockContainer;
            this.barTaskList.Location = new System.Drawing.Point(0, 3);
            this.barTaskList.Name = "barTaskList";
            this.barTaskList.SelectedDockTab = 1;
            this.barTaskList.Size = new System.Drawing.Size(1290, 116);
            this.barTaskList.Stretch = true;
            this.barTaskList.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.barTaskList.TabIndex = 0;
            this.barTaskList.TabNavigation = true;
            this.barTaskList.TabStop = false;
            this.barTaskList.Text = "My Window";
            // 
            // cmnuAlertOptions
            // 
            this.cmnuAlertOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiMarkAsRead,
            this.tsmiMarkAsUnRead,
            this.tsmiBlock,
            this.tsmiUnBlock,
            this.tsmiFilterOptions});
            this.cmnuAlertOptions.Name = "cmnuAlertOptions";
            this.cmnuAlertOptions.Size = new System.Drawing.Size(178, 114);
            // 
            // tsmiMarkAsRead
            // 
            this.tsmiMarkAsRead.Name = "tsmiMarkAsRead";
            this.tsmiMarkAsRead.Size = new System.Drawing.Size(177, 22);
            this.tsmiMarkAsRead.Text = "Mark As Read";
            this.tsmiMarkAsRead.Click += new System.EventHandler(this.tsmiMarkAsRead_Click);
            // 
            // tsmiMarkAsUnRead
            // 
            this.tsmiMarkAsUnRead.Name = "tsmiMarkAsUnRead";
            this.tsmiMarkAsUnRead.Size = new System.Drawing.Size(177, 22);
            this.tsmiMarkAsUnRead.Text = "Mark As Unread";
            this.tsmiMarkAsUnRead.Click += new System.EventHandler(this.tsmiMarkAsUnRead_Click);
            // 
            // tsmiBlock
            // 
            this.tsmiBlock.Name = "tsmiBlock";
            this.tsmiBlock.Size = new System.Drawing.Size(177, 22);
            this.tsmiBlock.Text = "Mark As Blocked";
            this.tsmiBlock.Click += new System.EventHandler(this.blockedToolStripMenuItem_Click);
            // 
            // tsmiUnBlock
            // 
            this.tsmiUnBlock.Name = "tsmiUnBlock";
            this.tsmiUnBlock.Size = new System.Drawing.Size(177, 22);
            this.tsmiUnBlock.Text = "Mark As Unblocked";
            this.tsmiUnBlock.Click += new System.EventHandler(this.tsmiUnBlock_Click);
            // 
            // tsmiFilterOptions
            // 
            this.tsmiFilterOptions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAll,
            this.tsmiUnread,
            this.tsmiRead,
            this.tsmiBlocked});
            this.tsmiFilterOptions.Name = "tsmiFilterOptions";
            this.tsmiFilterOptions.Size = new System.Drawing.Size(177, 22);
            this.tsmiFilterOptions.Text = "Filter Options";
            // 
            // tsmiAll
            // 
            this.tsmiAll.CheckOnClick = true;
            this.tsmiAll.Name = "tsmiAll";
            this.tsmiAll.Size = new System.Drawing.Size(116, 22);
            this.tsmiAll.Text = "All";
            this.tsmiAll.Click += new System.EventHandler(this.allToolStripMenuItem_Click);
            // 
            // tsmiUnread
            // 
            this.tsmiUnread.CheckOnClick = true;
            this.tsmiUnread.Name = "tsmiUnread";
            this.tsmiUnread.Size = new System.Drawing.Size(116, 22);
            this.tsmiUnread.Text = "Unread";
            this.tsmiUnread.Click += new System.EventHandler(this.unreadToolStripMenuItem1_Click);
            // 
            // tsmiRead
            // 
            this.tsmiRead.CheckOnClick = true;
            this.tsmiRead.Name = "tsmiRead";
            this.tsmiRead.Size = new System.Drawing.Size(116, 22);
            this.tsmiRead.Text = "Read";
            this.tsmiRead.Click += new System.EventHandler(this.readToolStripMenuItem_Click);
            // 
            // tsmiBlocked
            // 
            this.tsmiBlocked.CheckOnClick = true;
            this.tsmiBlocked.Name = "tsmiBlocked";
            this.tsmiBlocked.Size = new System.Drawing.Size(116, 22);
            this.tsmiBlocked.Text = "Blocked";
            this.tsmiBlocked.Click += new System.EventHandler(this.blockedToolStripMenuItem1_Click);
            // 
            // PanelDockContainer1
            // 
            this.PanelDockContainer1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelDockContainer1.Controls.Add(this.pctNoalerts);
            this.PanelDockContainer1.Controls.Add(this.dgvAlerts);
            this.PanelDockContainer1.Location = new System.Drawing.Point(3, 23);
            this.PanelDockContainer1.Name = "PanelDockContainer1";
            this.PanelDockContainer1.Size = new System.Drawing.Size(1284, 65);
            this.PanelDockContainer1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelDockContainer1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.PanelDockContainer1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.PanelDockContainer1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.PanelDockContainer1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.PanelDockContainer1.Style.GradientAngle = 90;
            this.PanelDockContainer1.TabIndex = 1;
            this.PanelDockContainer1.Visible = true;
            // 
            // pctNoalerts
            // 
            this.pctNoalerts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pctNoalerts.Image = global::MyBooksERP.Properties.Resources.bg;
            this.pctNoalerts.Location = new System.Drawing.Point(0, 0);
            this.pctNoalerts.Name = "pctNoalerts";
            this.pctNoalerts.Size = new System.Drawing.Size(1284, 65);
            this.pctNoalerts.TabIndex = 1;
            this.pctNoalerts.TabStop = false;
            // 
            // dgvAlerts
            // 
            this.dgvAlerts.AllowUserToAddRows = false;
            this.dgvAlerts.AllowUserToDeleteRows = false;
            this.dgvAlerts.AllowUserToOrderColumns = true;
            this.dgvAlerts.BackgroundColor = System.Drawing.Color.White;
            this.dgvAlerts.ColumnHeadersVisible = false;
            this.dgvAlerts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colAlertMsg,
            this.clmnAlertSettingsID,
            this.clmnReferenceID,
            this.clmnStatusID,
            this.clmnAlertID});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAlerts.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAlerts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAlerts.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvAlerts.Location = new System.Drawing.Point(0, 0);
            this.dgvAlerts.Name = "dgvAlerts";
            this.dgvAlerts.ReadOnly = true;
            this.dgvAlerts.RowHeadersVisible = false;
            this.dgvAlerts.RowTemplate.ReadOnly = true;
            this.dgvAlerts.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvAlerts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAlerts.Size = new System.Drawing.Size(1284, 65);
            this.dgvAlerts.TabIndex = 0;
            this.dgvAlerts.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAlerts_CellDoubleClick);
            // 
            // colAlertMsg
            // 
            this.colAlertMsg.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colAlertMsg.HeaderText = "";
            this.colAlertMsg.Name = "colAlertMsg";
            this.colAlertMsg.ReadOnly = true;
            // 
            // clmnAlertSettingsID
            // 
            this.clmnAlertSettingsID.HeaderText = "AlertSettingsID";
            this.clmnAlertSettingsID.Name = "clmnAlertSettingsID";
            this.clmnAlertSettingsID.ReadOnly = true;
            this.clmnAlertSettingsID.Visible = false;
            // 
            // clmnReferenceID
            // 
            this.clmnReferenceID.HeaderText = "ReferenceID";
            this.clmnReferenceID.Name = "clmnReferenceID";
            this.clmnReferenceID.ReadOnly = true;
            this.clmnReferenceID.Visible = false;
            // 
            // clmnStatusID
            // 
            this.clmnStatusID.HeaderText = "StatusID";
            this.clmnStatusID.Name = "clmnStatusID";
            this.clmnStatusID.ReadOnly = true;
            this.clmnStatusID.Visible = false;
            // 
            // clmnAlertID
            // 
            this.clmnAlertID.HeaderText = "AlertID";
            this.clmnAlertID.Name = "clmnAlertID";
            this.clmnAlertID.ReadOnly = true;
            this.clmnAlertID.Visible = false;
            // 
            // PanelDockContainer2
            // 
            this.PanelDockContainer2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelDockContainer2.Controls.Add(this.WebRss);
            this.PanelDockContainer2.Location = new System.Drawing.Point(3, 23);
            this.PanelDockContainer2.Name = "PanelDockContainer2";
            this.PanelDockContainer2.Size = new System.Drawing.Size(1284, 65);
            this.PanelDockContainer2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelDockContainer2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.PanelDockContainer2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.PanelDockContainer2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.PanelDockContainer2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.PanelDockContainer2.Style.GradientAngle = 90;
            this.PanelDockContainer2.TabIndex = 2;
            this.PanelDockContainer2.Visible = true;
            // 
            // WebRss
            // 
            this.WebRss.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WebRss.Location = new System.Drawing.Point(0, 0);
            this.WebRss.MinimumSize = new System.Drawing.Size(20, 20);
            this.WebRss.Name = "WebRss";
            this.WebRss.ScrollBarsEnabled = false;
            this.WebRss.Size = new System.Drawing.Size(1284, 65);
            this.WebRss.TabIndex = 0;
            // 
            // dockRss
            // 
            this.dockRss.Control = this.PanelDockContainer2;
            this.dockRss.DefaultFloatingSize = new System.Drawing.Size(256, 196);
            this.dockRss.GlobalItem = true;
            this.dockRss.GlobalName = "dockSearchResults";
            this.dockRss.Image = global::MyBooksERP.Properties.Resources.rss;
            this.dockRss.MinimumSize = new System.Drawing.Size(64, 64);
            this.dockRss.Name = "dockRss";
            this.dockRss.Text = "Product Updates";
            // 
            // dockAlertList
            // 
            this.dockAlertList.Control = this.PanelDockContainer1;
            this.dockAlertList.DefaultFloatingSize = new System.Drawing.Size(256, 196);
            this.dockAlertList.GlobalItem = true;
            this.dockAlertList.GlobalName = "dockTaskList";
            this.dockAlertList.Image = global::MyBooksERP.Properties.Resources.Alert;
            this.dockAlertList.MinimumSize = new System.Drawing.Size(64, 64);
            this.dockAlertList.Name = "dockAlertList";
            this.dockAlertList.Text = "Alerts Details";
            // 
            // barLeftDockSite
            // 
            this.barLeftDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.barLeftDockSite.Dock = System.Windows.Forms.DockStyle.Left;
            this.barLeftDockSite.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.barLeftDockSite.Location = new System.Drawing.Point(5, 1);
            this.barLeftDockSite.Name = "barLeftDockSite";
            this.barLeftDockSite.Size = new System.Drawing.Size(0, 475);
            this.barLeftDockSite.TabIndex = 10;
            this.barLeftDockSite.TabStop = false;
            // 
            // barRightDockSite
            // 
            this.barRightDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.barRightDockSite.Dock = System.Windows.Forms.DockStyle.Right;
            this.barRightDockSite.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.barRightDockSite.Location = new System.Drawing.Point(1295, 1);
            this.barRightDockSite.Name = "barRightDockSite";
            this.barRightDockSite.Size = new System.Drawing.Size(0, 475);
            this.barRightDockSite.TabIndex = 12;
            this.barRightDockSite.TabStop = false;
            // 
            // dockSite4
            // 
            this.dockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite4.Controls.Add(this.bar1);
            this.dockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite4.Location = new System.Drawing.Point(5, 476);
            this.dockSite4.Name = "dockSite4";
            this.dockSite4.Size = new System.Drawing.Size(1290, 20);
            this.dockSite4.TabIndex = 20;
            this.dockSite4.TabStop = false;
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.Location = new System.Drawing.Point(5, 1);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 475);
            this.dockSite1.TabIndex = 17;
            this.dockSite1.TabStop = false;
            // 
            // dockSite2
            // 
            this.dockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite2.Location = new System.Drawing.Point(1295, 1);
            this.dockSite2.Name = "dockSite2";
            this.dockSite2.Size = new System.Drawing.Size(0, 475);
            this.dockSite2.TabIndex = 18;
            this.dockSite2.TabStop = false;
            // 
            // dockSite3
            // 
            this.dockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite3.Location = new System.Drawing.Point(5, 1);
            this.dockSite3.Name = "dockSite3";
            this.dockSite3.Size = new System.Drawing.Size(1290, 0);
            this.dockSite3.TabIndex = 19;
            this.dockSite3.TabStop = false;
            // 
            // barTopDockSite
            // 
            this.barTopDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.barTopDockSite.Dock = System.Windows.Forms.DockStyle.Top;
            this.barTopDockSite.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.barTopDockSite.Location = new System.Drawing.Point(5, 1);
            this.barTopDockSite.Name = "barTopDockSite";
            this.barTopDockSite.Size = new System.Drawing.Size(1290, 0);
            this.barTopDockSite.TabIndex = 14;
            this.barTopDockSite.TabStop = false;
            // 
            // AppCmdInventory
            // 
            this.AppCmdInventory.Name = "AppCmdInventory";
            this.AppCmdInventory.Executed += new System.EventHandler(this.AppCmdInventory_Executed);
            // 
            // tabStrip1
            // 
            this.tabStrip1.AutoSelectAttachedControl = true;
            this.tabStrip1.CanReorderTabs = true;
            this.tabStrip1.CloseButtonOnTabsVisible = true;
            this.tabStrip1.CloseButtonVisible = false;
            this.tabStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabStrip1.Location = new System.Drawing.Point(5, 150);
            this.tabStrip1.MdiForm = this;
            this.tabStrip1.MdiTabbedDocuments = true;
            this.tabStrip1.Name = "tabStrip1";
            this.tabStrip1.SelectedTab = null;
            this.tabStrip1.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabStrip1.Size = new System.Drawing.Size(1290, 26);
            this.tabStrip1.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tabStrip1.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Top;
            this.tabStrip1.TabIndex = 6;
            this.tabStrip1.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tabStrip1.Text = "tabStrip1";
            // 
            // tmRssmain
            // 
            this.tmRssmain.Interval = 15000;
            this.tmRssmain.Tick += new System.EventHandler(this.tmRssmain_Tick);
            // 
            // BackRssWorker
            // 
            this.BackRssWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BackRssWorker_DoWork);
            // 
            // tmCheckStatus
            // 
            this.tmCheckStatus.Enabled = true;
            this.tmCheckStatus.Interval = 330000;
            this.tmCheckStatus.Tick += new System.EventHandler(this.tmCheckStatus_Tick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Alert Message";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "AlertSettingsID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "ReferenceID";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "StatusID";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "AlertID";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // FrmMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(243)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(1300, 498);
            this.Controls.Add(this.tabStrip1);
            this.Controls.Add(this.ribbonControl);
            this.Controls.Add(this.barTopDockSite);
            this.Controls.Add(this.barBottomDockSite);
            this.Controls.Add(this.barLeftDockSite);
            this.Controls.Add(this.barRightDockSite);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.dockSite2);
            this.Controls.Add(this.dockSite3);
            this.Controls.Add(this.dockSite4);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MyBooksERP";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.MdiChildActivate += new System.EventHandler(this.MdiChildActivated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmMain_Closing);
            this.Move += new System.EventHandler(this.frmMain_Move);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmMain_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ribbonControl.ResumeLayout(false);
            this.ribbonControl.PerformLayout();
            this.rpHome.ResumeLayout(false);
            this.ribbonPanel6.ResumeLayout(false);
            this.ribbonPanel11.ResumeLayout(false);
            this.ribbonPanel2.ResumeLayout(false);
            this.ribbonPanel12.ResumeLayout(false);
            this.ribbonPanel9.ResumeLayout(false);
            this.ribbonPanel5.ResumeLayout(false);
            this.Inventory1.ResumeLayout(false);
            this.ribbonPanel1.ResumeLayout(false);
            this.ribbonPanel8.ResumeLayout(false);
            this.ribbonPanel7.ResumeLayout(false);
            this.ribbonPanel10.ResumeLayout(false);
            this.ribbonPanel4.ResumeLayout(false);
            this.ribbonPanel3.ResumeLayout(false);
            this.barBottomDockSite.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barTaskList)).EndInit();
            this.barTaskList.ResumeLayout(false);
            this.cmnuAlertOptions.ResumeLayout(false);
            this.PanelDockContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pctNoalerts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlerts)).EndInit();
            this.PanelDockContainer2.ResumeLayout(false);
            this.dockSite4.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        #region AppCreation
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new FrmMain());
        }
        #endregion

        public void EditContextMenu()
        {
            //bEditPopup.Displayed=false;
            //bEditPopup.PopupMenu(Control.MousePosition);
        }

        /// <summary>
        /// Verifies current context and enables/disables menu items...
        /// </summary>
        /// 

        private void LoadMessage()
        {
            MObjClsNotification = new ClsNotification();
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.MainFormRibbon, 4);
            MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.MainFormRibbon, 4);
        }

        private void MdiChildActivated(object sender, System.EventArgs e)
        {
            UpdateTitle();
            if (this.ActiveMdiChild != null)
            {
                switch (this.ActiveMdiChild.Name)
                {
                    case "FrmPurchaseIndent":
                        FrmPurchaseIndent frmPurchaseIndent = (FrmPurchaseIndent)this.ActiveMdiChild;
                        frmPurchaseIndent.RefreshForm();
                        break;
                    case "FrmPurchase":
                        FrmPurchase frmPurchase = (FrmPurchase)this.ActiveMdiChild;
                        frmPurchase.RefreshForm();
                        break;
                    case "FrmPurchaseOrder":
                        FrmPurchaseOrder frmPurchaseOrder = (FrmPurchaseOrder)this.ActiveMdiChild;
                        frmPurchaseOrder.RefreshForm();
                        break;
                    case "FrmPurchaseInvoice":
                        FrmPurchaseInvoice frmPurchaseInvoice = (FrmPurchaseInvoice)this.ActiveMdiChild;
                        frmPurchaseInvoice.RefreshForm();
                        break;
                    case "FrmGRN":
                        FrmGRN frmGRN = (FrmGRN)this.ActiveMdiChild;
                        frmGRN.RefreshForm();
                        break;
                    case "FrmDirectGRN":
                        FrmDirectGRN frmDirectGRN = (FrmDirectGRN)this.ActiveMdiChild;
                        frmDirectGRN.RefreshForm();
                        break;
                    case "FrmDebitNote":
                        FrmDebitNote frmPurchaseReturn = (FrmDebitNote)this.ActiveMdiChild;
                        frmPurchaseReturn.RefreshForm();
                        break;
                    case "FrmSales":
                        FrmSales frmSales = (FrmSales)this.ActiveMdiChild;
                        frmSales.RefreshForm();
                        break;
                    case "FrmSalesOrder":
                        FrmSalesOrder frmSalesOrder = (FrmSalesOrder)this.ActiveMdiChild;
                        frmSalesOrder.RefreshForm();
                        break;
                    case "FrmSalesInvoice":
                        FrmSalesInvoice frmSalesInvoice = (FrmSalesInvoice)this.ActiveMdiChild;
                        frmSalesInvoice.RefreshForm();
                        break;
                    case "FrmCreditNote":
                        FrmCreditNote frmSalesReturn = (FrmCreditNote)this.ActiveMdiChild;
                        frmSalesReturn.RefreshForm();
                        break;
                    case "FrmItemIssue":
                        FrmItemIssue frmItemIssue = (FrmItemIssue)this.ActiveMdiChild;
                        frmItemIssue.RefreshForm();
                        break;
                    case "FrmDirectDelivery":
                        FrmDirectDelivery frmDirectDelivery = (FrmDirectDelivery)this.ActiveMdiChild;
                        frmDirectDelivery.RefreshForm();
                        break;
                    case "frmItemMaster":
                        frmItemMaster frmItemMaster = (frmItemMaster)this.ActiveMdiChild;
                        frmItemMaster.RefreshForm();
                        break;
                    case "FrmRFQ":
                        FrmRFQ frmRFQ = (FrmRFQ)this.ActiveMdiChild;
                        frmRFQ.RefreshForm();
                        break;
                }
            }
        }

        private void frmMain_Load(object sender, System.EventArgs e)
        {
            SetInitials();

            using (FrmLogin objFrmLogin = new FrmLogin())
            {
                objFrmLogin.ShowDialog();
            }

            if (ClsMainSettings.blnLoginStatus)
            {
                using (frmCompanySelection objCompanySelection = new frmCompanySelection())
                {
                    objCompanySelection.ShowDialog();
                }

                AppCommandNew.Execute();

                // Load Quick Access Toolbar layout if one is saved from last session...
                Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\DevComponents\Ribbon");

                if (key != null)
                {
                    try
                    {
                        string layout = key.GetValue("RibbonPadCSLayout", "").ToString();
                        if (layout != "" && layout != null)
                            ribbonControl.QatLayout = layout;
                    }
                    finally
                    {
                        key.Close();
                    }
                }

                LoadMessage();
                UpdateTitle();

                // Pulse the Application Button
                buttonFile.Pulse(11);
                SetPermissions();//set permission
                ClsMainSettings.objFrmMain = this;
                Application.CurrentCulture = new CultureInfo("en-US");
                DateTimeFormatInfo dt = new DateTimeFormatInfo();
                dt.ShortDatePattern = "dd MMM yyyy";
                Application.CurrentCulture.DateTimeFormat = dt;
                ClsCommonSettings.TimerInterval = 10;

                if (ClsMainSettings.BOQEnabled)
                    ProductGroups.Text = "BOQ";
                rbProduction.Visible = ClsMainSettings.ProductionEnabled;
                rtiPayroll.Visible = rtiPayrollReports.Visible = ClsMainSettings.PayrollEnabled;

                LoadInitialRssdetails();

                StatusCheck();

                tmRssmain.Enabled = true;

                CallDashBoard();
            }
            else
            {
                Application.Exit();
            }            
        }

        private void CallDashBoard()
        {
            FrmDashBoard objDashBoard = null;
            
            try
            {
                objDashBoard = new FrmDashBoard();
                objDashBoard.MdiParent = this;
                objDashBoard.WindowState = FormWindowState.Maximized;
                objDashBoard.Show();
            }
            catch (OutOfMemoryException)
            {
                objDashBoard.Dispose();
                System.GC.Collect();
                CallDashBoard();
            }
        }

        private void SetInitials()
        {
            FrmSplash objSplash = new FrmSplash();
            objSplash.ShowDialog();
            objSplash = null;

            ProjectSettings.clsRegistry objRegistry = new ProjectSettings.clsRegistry();
            string strTemp = "";
            objRegistry.ReadFromRegistry("SOFTWARE\\MsbErpserver", "MsbErpservername", out strTemp);
            ClsCommonSettings.ServerName = strTemp;

            objRegistry.ReadFromRegistry("SOFTWARE\\MsbErpserver", "MsbErpserverpath", out strTemp);
            ClsCommonSettings.strServerPath = strTemp;

            ClsCommonSettings.DbName = "PhoenixSet";
            MsDb.ConnectionString = "Data Source=" + ClsCommonSettings.ServerName + ";Initial Catalog=" + ClsCommonSettings.DbName + ";uid=erpadmin;pwd=ErpMsb!2345;Connect Timeout=0;";
            //////---------------------------------------------------

            //ClsCommonSettings.ServerName = "ESCUBE_1\\SQLEXPRESS";
            //ClsCommonSettings.DbName = "PhoenixSet";
            //MsDb.ConnectionString = "Data Source=" + ClsCommonSettings.ServerName + ";Initial Catalog=" + ClsCommonSettings.DbName + ";uid=erpadmin;pwd=ErpMsb!2345;Connect Timeout=0";
           
            ClsCommonSettings.TimerInterval = 1000;            
        }
      
        private bool SetPermissions()
        {
            clsBLLPermissionSettings ObjPermission = new clsBLLPermissionSettings();
            btnGroupJV.Visible = false;//ClsMainSettings.JVAccDuplication;
            btnQC.Visible = ClsMainSettings.QCEnabled;
            clsUtilities.GetPermissionCounts();

            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                DataTable dt = ObjPermission.GetMenuPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID);
                if (dt.Rows.Count == 0)
                {
                    //Home
                    btnItmCustomers.Enabled = btnItmSuppliers.Enabled = btnItmPOS.Enabled =
                            btnItmReceipts.Enabled = btnItmDeliveryNote.Enabled = 
                            btnItmRptStock.Enabled = btnItmRptPaymentsAndReceipts.Enabled = BtnSalesH.Enabled = btnItmDirectDelivery.Enabled = false;
                    // Masters
                    NewCompany.Enabled = Vendor.Enabled = Customer.Enabled = Employee.Enabled = Warehouse.Enabled = Currency.Enabled = false;

                    // Purchase
                    PurchaseQuotation.Enabled = PurchaseOrder.Enabled = PurchaseInvoice.Enabled = GoodsReceiptNote.Enabled = BtnDebitNote.Enabled =
                        Payments.Enabled = DirectGRN.Enabled =  btnPurExtraChargePayment.Enabled = false;

                    // Sales
                    SalesQuotation.Enabled = SalesOrder.Enabled = SalesInvoice.Enabled = POS.Enabled = BtnCreditNote.Enabled
                        = btnSalesExtraCharge.Enabled = btnSalesExtraChargePayment.Enabled = false;

                    //Inventory
                    DeliveryNote.Enabled = DirectDeliveryNote.Enabled =  false;
                    Products.Enabled = ProductGroups.Enabled = OpeningStock.Enabled = StockAdjustment.Enabled = StockTransfer.Enabled = 
                        BtnPaymentsInv.Enabled = btnProductionTemplate.Enabled = btnMaterialIssue.Enabled = btnProductionProcess.Enabled =
                        btnQC.Enabled = false;

                    // Reports
                    ReportProducts.Enabled = ReportEmployee.Enabled = ReportsAssociates.Enabled = ReportsSales.Enabled = ReportsPurchase.Enabled = btnPaymentsReceipts.Enabled = ReportsStock.Enabled = BtnSummary.Enabled = btnRptPurchaseSummary.Enabled = btnRptAgeing.Enabled = btnRptItemWiseProfit.Enabled = btnProductMovement.Enabled = BtnItemIssueReport.Enabled = btnExpense.Enabled = btnRptGRN.Enabled =  btnRptPendingDelivery.Enabled =  false;

                    // Settings
                    Configuration.Enabled = Email.Enabled = User.Enabled = UserRole.Enabled = CompanySettings.Enabled = Uom.Enabled = UomConversion.Enabled = ChangePassword.Enabled = BtnDiscounts.Enabled = BtnPricingScheme.Enabled = BtnTermsAndConditions.Enabled = AlertSettings.Enabled = BackUp.Enabled = false;

                    //help
                    About.Enabled = false;

                    // Accounts Voucher
                    btnPaymentVoucher.Enabled = btnReceiptVoucher.Enabled = btnContraVoucher.Enabled = BtnJVoucher.Enabled = btnPurchaseVoucher.Enabled = btnDebitNoteVoucher.Enabled = btnSalesVoucher.Enabled = btnCreditNoteVoucher.Enabled = btnGroupJV.Enabled = false;
                    btnPayments.Enabled = btnReceipts.Enabled = btnExtraChargePayment.Enabled = false;

                    // Account Summary
                    btnTrialBalance.Enabled = btnGroupSummary.Enabled = btnGL.Enabled = BtnDayBook.Enabled = btnCashBook.Enabled = btnProfitAndLossSummary.Enabled = btnBalanceSheetSummary.Enabled = false;

                    // Account REports
                    btnProfitnLoss.Enabled = btnBalancesheet.Enabled = btnCashFlow.Enabled = btnFundFlow.Enabled = btnINcomestmt.Enabled = btnStockSummary.Enabled = false;
                    return false;
                }
                else
                {
                    //=-------------------------------------------create or modify---------------
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.NewCompany).ToString();
                    NewCompany.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Supplier).ToString();
                    btnItmSuppliers.Enabled = Vendor.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Customer).ToString();
                    btnItmCustomers.Enabled = Customer.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Employee).ToString();
                    Employee.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Warehouse).ToString();
                    Warehouse.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ExchangeCurrency).ToString();
                    Currency.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    //------------------------------------------------------------purchase-----------------
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PurchaseQuotation).ToString();
                    PurchaseQuotation.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PurchaseOrder).ToString();
                    PurchaseOrder.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PurchaseInvoice).ToString();
                    PurchaseInvoice.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.GRN).ToString();
                    GoodsReceiptNote.Enabled = DirectGRN.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.DebitNote).ToString();
                    BtnDebitNote.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                 

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ExtraChargePayment).ToString();
                    btnPurExtraChargePayment.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    //--------------------------------------sales-----------------------------------
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SalesQuotation).ToString();
                    SalesQuotation.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SalesOrder).ToString();
                    SalesOrder.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SalesInvoice).ToString();
                    SalesInvoice.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.POS).ToString();
                    btnItmPOS.Enabled = POS.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.CreditNote).ToString();
                    BtnCreditNote.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ExtraCharges).ToString();
                    btnSalesExtraCharge.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ExtraChargePayment).ToString();
                    btnSalesExtraChargePayment.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    //----------------------------------------delivery
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.DeliveryNote).ToString();
                    btnItmDeliveryNote.Enabled = DeliveryNote.Enabled = DirectDeliveryNote.Enabled = btnItmDirectDelivery.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

 
                    //----------------------------------inventory
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Products).ToString();
                    Products.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ProductGroups).ToString();
                    ProductGroups.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.OpeningStock).ToString();
                    OpeningStock.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.StockAdjustment).ToString();
                    StockAdjustment.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.StockTransfer).ToString();
                    StockTransfer.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ProductionTemplate).ToString();
                    btnProductionTemplate.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.MaterialIssue).ToString();
                    btnMaterialIssue.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ProductionProcess).ToString();
                    btnProductionProcess.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.QC).ToString();
                    btnQC.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0); 

                    //------------------------------task
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Receipt).ToString();
                    btnItmReceipts.Enabled = Receipts.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Payment).ToString();
                    BtnPaymentsInv.Enabled = Payments.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    //-------------------------------report
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ProductReports).ToString();
                    ReportProducts.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.EmployeeReports).ToString();
                    ReportEmployee.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AssociateReports).ToString();
                    ReportsAssociates.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.StockReports).ToString();
                    btnItmRptStock.Enabled = ReportsStock.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SalesReports).ToString();
                    BtnSalesH.Enabled = ReportsSales.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PurchaseReports).ToString();
                    ReportsPurchase.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PaymentReceiptsReports).ToString();
                    btnItmRptPaymentsAndReceipts.Enabled = btnPaymentsReceipts.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.InventorySummary).ToString();
                    BtnSummary.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ItemIssueReport).ToString();
                    BtnItemIssueReport.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.GRNReport).ToString();
                    btnRptGRN.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.DeliveryReport).ToString();
                    btnRptPendingDelivery.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    //---------------------------settings
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ConfigurationSetting).ToString();
                    Configuration.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.EmailSettings).ToString();
                    Email.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.User).ToString();
                    User.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Role).ToString();
                    UserRole.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.CompanySettings).ToString();
                    CompanySettings.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.UOM).ToString();
                    Uom.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.UOMConversions).ToString();
                    UomConversion.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AlertSettings).ToString();
                    AlertSettings.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Backup).ToString();
                    BackUp.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ChangePassword).ToString();
                    ChangePassword.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Discounts).ToString();
                    BtnDiscounts.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.TermsAndConditions).ToString();
                    BtnTermsAndConditions.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PricingScheme).ToString();
                    BtnPricingScheme.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ExpenseReport).ToString();
                    btnExpense.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PurchaseSummaryReport).ToString();
                    btnRptPurchaseSummary.Enabled  = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AgeingReport).ToString();
                    btnRptAgeing.Enabled  = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ProductMovementReport).ToString();
                    btnProductMovement.Enabled =  (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ItemwiseProfitReport).ToString();
                    btnRptItemWiseProfit.Enabled  = (dt.DefaultView.ToTable().Rows.Count > 0);

                    // Accounts Voucher
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PaymentVoucher).ToString();
                    btnPaymentVoucher.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PaymentVoucher).ToString();
                    btnPaymentVoucher.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ReceiptVoucher).ToString();
                    btnReceiptVoucher.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ContraVoucher).ToString();
                    btnContraVoucher.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.JournalVoucher).ToString();
                    BtnJVoucher.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PurchaseVoucher).ToString();
                    btnPurchaseVoucher.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.DebitNoteVoucher).ToString();
                    btnDebitNoteVoucher.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SalesVoucher).ToString();
                    btnSalesVoucher.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.CreditNoteVoucher).ToString();
                    btnCreditNoteVoucher.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.GroupJV).ToString();
                    btnGroupJV.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Payment).ToString();
                    btnPayments.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Receipt).ToString();
                    btnReceipts.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ExtraChargePayment).ToString();
                    btnExtraChargePayment.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    // Account Summary
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.TrialBalance).ToString();
                    btnExtraChargePayment.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.TrialBalance).ToString();
                    btnTrialBalance.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AccountsSummary).ToString();
                    btnGroupSummary.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.GeneralLedger).ToString();
                    btnGL.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.DayBook).ToString();
                    BtnDayBook.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.CashBook).ToString();
                    btnCashBook.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ProfitAndLossAccount).ToString();
                    btnProfitAndLossSummary.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.BalanceSheet).ToString();
                    btnBalanceSheetSummary.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    // Account REports
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ProfitAndLossAccount).ToString();
                    btnProfitnLoss.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.BalanceSheet).ToString();
                    btnBalancesheet.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.CashFlow).ToString();
                    btnCashFlow.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.FundFlow).ToString();
                    btnFundFlow.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.IncomeStatement).ToString();
                    btnINcomestmt.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.StockSummary).ToString();
                    btnStockSummary.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.StatementofAccounts).ToString();
                    btnStatementofAccounts.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);
                    
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        private void frmMain_Move(object sender, System.EventArgs e)
        {
            this.CloseSearch();
        }

        private void UnloadPopup(object sender, System.EventArgs e)
        {
            ButtonItem item = sender as ButtonItem;
            if (item.Name == "bTabColor")
            {
                DevComponents.DotNetBar.PopupContainerControl container = item.PopupContainerControl as PopupContainerControl;
                ColorPicker clr = container.Controls[0] as ColorPicker;
                if (clr.SelectedColor != Color.Empty)
                {
                    tabStrip1.ColorScheme.TabBackground = ControlPaint.LightLight(clr.SelectedColor);
                    tabStrip1.ColorScheme.TabBackground2 = clr.SelectedColor;
                    tabStrip1.Refresh();
                }
                // Close popup menu, since it is not closed when Popup Container is closed...
                item.Parent.Expanded = false;
            }
        }

        private void TaskPaneShowAtStartup(object sender, EventArgs e)
        {
            //MessageBoxEx.Show("This item is here for demonstration purposes only and code should be added to save the state of it.");
        }

        private void dotNetBarManager1_MouseEnter(object sender, System.EventArgs e)
        {
            // Sync Status-bar with the item tooltip...
            BaseItem item = sender as BaseItem;
            if (item == null)
                return;
            labelStatus.Text = item.Tooltip;
        }

        private void dotNetBarManager1_MouseLeave(object sender, System.EventArgs e)
        {
            labelStatus.Text = "";
        }

        private void CloseSearch()
        {
            if (m_Search != null)
            {
                m_Search.Close();
                m_Search.Dispose();
                m_Search = null;
            }
        }

        private void LaunchRibbonDialog(object sender, System.EventArgs e)
        {
            //MessageBoxEx.Show("Start <i>Ribbon Dialog</i> with more options here...", "Ribbon Demo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void buttonFind_Click(object sender, System.EventArgs e)
        {

        }

        private void MdiClientControlAddRemove(object sender, ControlEventArgs e)
        {
            if (this.MdiChildren.Length > 0)
            {
                if (!Inventory.Visible)
                {
                    Inventory.Visible = true;
                    ribbonControl.RecalcLayout();
                }
            }
            else
            {
                if (Inventory.Visible)
                {
                    if (Inventory.Checked)
                        Company.Checked = true;
                    //Inventory.Visible = false;
                    ribbonControl.RecalcLayout();
                }
            }
        }

        private void frmMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Save Quick Access Toolbar layout if it has changed...
            //MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 4214, out MmessageIcon).Replace("#", "").Trim();
            //if (MessageBox.Show(MsMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
            //    DialogResult.Yes)
            //{
            //    if (ribbonControl.QatLayoutChanged)
            //    {
            //        Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(@"Software\DevComponents\Ribbon");
            //        try
            //        {
            //            key.SetValue("RibbonPadCSLayout", ribbonControl.QatLayout);
            //        }
            //        finally
            //        {
            //            key.Close();
            //        }
            //    }
            //}
            //else
            //    e.Cancel = true;

        }

        private void ribbonControl1_BeforeRibbonPanelPopupClose(object sender, DevComponents.DotNetBar.RibbonPopupCloseEventArgs e)
        {
            // Don't close ribbon tab menu if Find button is clicked since it display the balloon popup
            //if (e.Source == buttonFind)
            //    e.Cancel = true;
        }

        #region Automatic Color Scheme creation based on the selected color table
        private bool m_ColorSelected = false;
        private eOffice2007ColorScheme m_BaseColorScheme = eOffice2007ColorScheme.Blue;
        private void buttonStyleCustom_ExpandChange(object sender, System.EventArgs e)
        {
            if (buttonStyleCustom.Expanded)
            {
                // Remember the starting color scheme to apply if no color is selected during live-preview
                m_ColorSelected = false;
                m_BaseColorScheme = ((Office2007Renderer)GlobalManager.Renderer).ColorTable.InitialColorScheme;
            }
            else
            {
                if (!m_ColorSelected)
                {
                    ribbonControl.Office2007ColorTable = m_BaseColorScheme;
                }
            }
        }

        private void buttonStyleCustom_ColorPreview(object sender, DevComponents.DotNetBar.ColorPreviewEventArgs e)
        {
            RibbonPredefinedColorSchemes.ChangeOffice2007ColorTable(this, m_BaseColorScheme, e.Color);
        }

        private void buttonStyleCustom_SelectedColorChanged(object sender, System.EventArgs e)
        {
            m_ColorSelected = true; // Indicate that color was selected for buttonStyleCustom_ExpandChange method
            buttonStyleCustom.CommandParameter = buttonStyleCustom.SelectedColor;
        }
        #endregion

        private void buttonFileSaveAs_ExpandChange(object sender, System.EventArgs e)
        {
            //// Position the Save As sub menu on top of the recent files box
            //if (buttonFileSaveAs.Expanded)
            //{
            //    buttonFileSaveAs.PopupLocation = menuFileMRU.DisplayRectangle.Location;
            //}
        }

        /// <summary>
        /// Updates the title displayed on Ribbon using the rich text markup
        /// </summary>
        private void UpdateTitle()
        {
            string t = "<b> MyBooksERP </b>";
            if (this.ActiveMdiChild != null)
            {
                // Note the usage of SysCaptionTextExtra for as the value of color attribute.
                // It specifies the system color that changes based on selected color table.
                t += "<font color=\"SysCaptionTextExtra\">" + this.ActiveMdiChild.Text + "</font> ";
            }
            //t+= "<b>MyBooksERP</b>";
            t += " <b><a name=\"tip\"><font color=\"SysCaptionTextExtra\"></font></a></b>";
            ribbonControl.TitleText = t;

            labelPosition.Text = ClsCommonSettings.strUserName; 
            lblCompany.Text = ClsCommonSettings.LoginCompany;
            lblCompanyCurrency.Text = ClsCommonSettings.Currency;
            lblFinancialYear.Text = ClsCommonSettings.FinYearStartDate.ToString("dd MMM yyyy") + " - " + ClsCommonSettings.FinYearEndDate.ToString("dd MMM yyyy");
        }

        #region ProductRss

        private void StatusCheck()
        {
            try
            {
                //labelStatus.Text = "You are using UAT version of MyBooksERP[Mindsoft Technologies (I) Pvt Ltd]";
                clsFiles objFiles = new clsFiles();
                if (!objFiles.Checksysfile(Application.StartupPath))
                {
                    labelStatus.Text = "You are using a cracked software of Mindsoft Technologies (I) Pvt Ltd.";
                }
            }
            catch (Exception)
            {
                labelStatus.Text = "You are using a cracked software of Mindsoft Technologies (I) Pvt Ltd.";
            }
        }

        private void tmRssmain_Tick(object sender, EventArgs e)
        {
            tmRssmain.Stop();
            tmRssmain.Enabled = false;
            tmRssmain.Dispose();

            BackRssWorker.RunWorkerAsync(1);
        }

        private bool LoadInitialRssdetails()
        {
            WebRss.DocumentText = new clsRss().GetRssFeedInitial();
            return true;
        }

        private bool LoadRssfromWeb()
        {
            try
            {
                ClsInternet objINet = new ClsInternet();
                if (objINet.IsInternetConnected())
                {

                    string sRssUrlLive = "http://msg.mindsoft.ae/rssimages/Erpupdate.jpg";
                    if (objINet.IsUrlAvailable(sRssUrlLive))
                    {
                        string sRssFedd = new clsRss().GetRssFeedLive();
                        if (sRssFedd != "")
                        {
                            WebRss.DocumentText = sRssFedd;
                            string sRssLocalfile = Application.StartupPath + "\\rss\\rssimages\\Erpupdate.jpg";

                            System.Net.WebClient wbclient = new System.Net.WebClient();
                            wbclient.DownloadFileCompleted += new AsyncCompletedEventHandler(wbclient_DownloadFileCompleted);
                            wbclient.DownloadFileAsync(new Uri(sRssUrlLive), sRssLocalfile);
                        }
                    }
                }
                
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void tmCheckStatus_Tick(object sender, EventArgs e)
        {
            if (!BackRssWorker.IsBusy)
                BackRssWorker.RunWorkerAsync(0);
        }

        private void CheckProductStatus()
        {
            try
            {
                if (ClsMainSettings.blnProductStatus)
                {
                    string dt = "31-Jan-2021";
                    if (ClsMainSettings.strProductSerial == "" || ClsMainSettings.strProductSerial == null)
                        dt = "01 Jan 2020";

                    clsFiles objSet = new clsFiles();
                    ClsMainSettings.blnProductStatus = objSet.CheckProductMaxUsage(dt);
                }
            }
            catch (Exception)
            {

            }
        }

        private void wbclient_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            //BackRssWorker.Dispose();
        }

        private void BackRssWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            CheckProductStatus();
        }

        #endregion

        #region Commands Implementation
        private void AppCommandTheme_Executed(object sender, EventArgs e)
        {
            ICommandSource source = sender as ICommandSource;
            if (source.CommandParameter is string)
            {
                eOffice2007ColorScheme colorScheme = (eOffice2007ColorScheme)Enum.Parse(typeof(eOffice2007ColorScheme), source.CommandParameter.ToString());
                // This is all that is needed to change the color table for all controls on the form
                ribbonControl.Office2007ColorTable = colorScheme;
            }
            else if (source.CommandParameter is Color)
            {
                RibbonPredefinedColorSchemes.ChangeOffice2007ColorTable(this, m_BaseColorScheme, (Color)source.CommandParameter);
            }
            this.Invalidate();
        }

        private void AppCommandExit_Executed(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void AppCmdPurchase_Executed(object sender, EventArgs e)
        {
            if (ClsMainSettings.blnProductStatus && ClsMainSettings.Itmcount <= Productval.ItmLimit)
            {
                FrmPurchase objpurchase = null;

                try
                {
                    int iType = 1;

                    if (AppCmdPurchase.Text == "Purchase Quotation")
                        iType = 1;
                    else if (AppCmdPurchase.Text == "Purchase Order")
                        iType = 2;
                    else if (AppCmdPurchase.Text == "GRN")
                        iType = 3;
                    else if (AppCmdPurchase.Text == "Purchase Invoice")
                        iType = 4;
                    else if (AppCmdPurchase.Text == "Purchase Return")
                        iType = 5;
                    else if (AppCmdPurchase.Text == "Material Return")
                        iType = 3;

                    objpurchase = new FrmPurchase(iType);


                    objpurchase.Text = AppCmdPurchase.Text + " " + (this.MdiChildren.Length + 1);
                    objpurchase.MdiParent = this;
                    objpurchase.objFrmTradingMain = this;
                    objpurchase.WindowState = FormWindowState.Maximized;
                    objpurchase.Show();
                    objpurchase.Update();

                    ExpandRibbon();
                }
                catch (OutOfMemoryException ex)
                {
                    objpurchase.Dispose();
                    System.GC.Collect();
                    AppCmdPurchase.Execute();
                }
            }
        }
        private void CallPurchaseOrder()
        {
            if (ClsMainSettings.blnProductStatus && ClsMainSettings.Itmcount <= Productval.ItmLimit)
            {
            FrmPurchaseOrder objPurchaseOrder = null;

            try
            {

                objPurchaseOrder = new FrmPurchaseOrder(2);
                objPurchaseOrder.Text = "Purchase Order  " + (this.MdiChildren.Length + 1);
                objPurchaseOrder.MdiParent = this;
                objPurchaseOrder.objFrmTradingMain = this;
                objPurchaseOrder.WindowState = FormWindowState.Maximized;
                objPurchaseOrder.Show();
                objPurchaseOrder.Update();

                ExpandRibbon();
            }
            catch (OutOfMemoryException)
            {
                objPurchaseOrder.Dispose();
                System.GC.Collect();
                CallPurchaseOrder();
            }
        }
        }
        private void CallGRN()
        {
            if (ClsMainSettings.blnProductStatus && ClsMainSettings.Itmcount <= Productval.ItmLimit)
            {
                FrmGRN objGRN = null;

                try
                {

                    objGRN = new FrmGRN(3);
                    objGRN.Text = "GRN " + (this.MdiChildren.Length + 1);
                    objGRN.MdiParent = this;
                    objGRN.objFrmTradingMain = this;
                    objGRN.WindowState = FormWindowState.Maximized;
                    objGRN.Show();
                    objGRN.Update();

                    ExpandRibbon();
                }
                catch (OutOfMemoryException ex)
                {
                    objGRN.Dispose();
                    System.GC.Collect();
                    CallGRN();
                }
            }
        }
        private void CallDirectGRN()
        {
            if (ClsMainSettings.blnProductStatus && ClsMainSettings.Itmcount <= Productval.ItmLimit)
            {
                FrmDirectGRN objDirectGRN = null;

                try
                {

                    objDirectGRN = new FrmDirectGRN(3);
                    objDirectGRN.Text = "Direct GRN " + (this.MdiChildren.Length + 1);
                    objDirectGRN.MdiParent = this;
                    objDirectGRN.objFrmTradingMain = this;
                    objDirectGRN.WindowState = FormWindowState.Maximized;
                    objDirectGRN.Show();
                    objDirectGRN.Update();

                    ExpandRibbon();
                }
                catch (OutOfMemoryException ex)
                {
                    objDirectGRN.Dispose();
                    System.GC.Collect();
                    CallDirectGRN();
                }
            }
        }
        private void CallPurchaseInvoice()
        {
            if (ClsMainSettings.blnProductStatus && ClsMainSettings.Itmcount <= Productval.ItmLimit)
            {
                FrmPurchaseInvoice objPurchaseInvoice = null;

                try
                {

                    objPurchaseInvoice = new FrmPurchaseInvoice(4);
                    objPurchaseInvoice.Text = "Purchase Invoice   " + (this.MdiChildren.Length + 1);
                    objPurchaseInvoice.MdiParent = this;
                    objPurchaseInvoice.objFrmTradingMain = this;
                    objPurchaseInvoice.WindowState = FormWindowState.Maximized;
                    objPurchaseInvoice.Show();
                    objPurchaseInvoice.Update();

                    ExpandRibbon();
                }
                catch (OutOfMemoryException ex)
                {
                    objPurchaseInvoice.Dispose();
                    System.GC.Collect();
                    CallPurchaseInvoice();
                }
            }
        }
        private void PurchaseIndent_Click(object sender, EventArgs e)
        {
            CallPurchaseIndent();
            ExpandRibbon();
        }

        private void CallPurchaseIndent()
        {
            if (ClsMainSettings.blnProductStatus && ClsMainSettings.Itmcount <= Productval.ItmLimit)
            {
                FrmPurchaseIndent objPurchaseIndent = null;

                try
                {
                    objPurchaseIndent = new FrmPurchaseIndent();
                    objPurchaseIndent.Text = "Purchase Indent " + (this.MdiChildren.Length + 1);
                    objPurchaseIndent.MdiParent = this;
                    objPurchaseIndent.WindowState = FormWindowState.Maximized;
                    objPurchaseIndent.Show();
                }
                catch (OutOfMemoryException ex)
                {
                    objPurchaseIndent.Dispose();
                    System.GC.Collect();
                    CallPurchaseIndent();
                }
            }
        }

        private void PurchaseOrder_Click(object sender, EventArgs e)
        {
            CallPurchaseOrder();
        }

        private void PurchaseInvoice_Click(object sender, EventArgs e)
        {
            CallPurchaseInvoice();
        }

        private void GoodsReceiptNote_Click(object sender, EventArgs e)
        {
            CallGRN();
        }

        private void PurchaseReturn_Click(object sender, EventArgs e)
        {
            CallDebitNote();
            ExpandRibbon();
        }

        private void CallDebitNote()
        {
            if (ClsMainSettings.blnProductStatus && ClsMainSettings.Itmcount <= Productval.ItmLimit)
            {
                FrmDebitNote objDebitNote = null;

                try
                {
                    objDebitNote = new FrmDebitNote();

                    objDebitNote.Text = "Debit Note " + (this.MdiChildren.Length + 1);
                    objDebitNote.WindowState = FormWindowState.Maximized;
                    objDebitNote.MdiParent = this;
                    objDebitNote.Update();
                    objDebitNote.Show();
                }
                catch (OutOfMemoryException ex)
                {
                    objDebitNote.Dispose();
                    System.GC.Collect();
                    CallDebitNote();
                }
            }
        }

        private void NewCompany_Click(object sender, EventArgs e)
        {
            using (FrmCompany objCom = new FrmCompany())
            {
                objCom.ShowDialog();
            }
        }

        private void Vendor_Click(object sender, EventArgs e)
        {
            using (FrmVendor objVendor = new FrmVendor(2))
            {
                objVendor.ShowDialog();
            }
        }

        private void Customer_Click(object sender, EventArgs e)
        {
            using (FrmVendor objVendor = new FrmVendor(1))
            {
                objVendor.ShowDialog();
            }
        }

        private void CompanySettings_Click(object sender, EventArgs e)
        {
            using (FrmCompanySettings objFrmCompSettings = new FrmCompanySettings())
            {
                objFrmCompSettings.ShowDialog();
            }
        }

        private void StockAdjustment_Click(object sender, EventArgs e)
        {
            CallStockAdjustment();
            ExpandRibbon();
        }

        private void CallStockAdjustment()
        {
            FrmStockAdjustment objStockAdjustment = null;

            try
            {
                objStockAdjustment = new FrmStockAdjustment();
                objStockAdjustment.Text = "Stock Adjustment " + (this.MdiChildren.Length + 1);
                objStockAdjustment.MdiParent = this;
                objStockAdjustment.WindowState = FormWindowState.Maximized;
                objStockAdjustment.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objStockAdjustment.Dispose();
                System.GC.Collect();
                CallStockAdjustment();
            }
        }

        private void Products_Click(object sender, EventArgs e)
        {
            CallProducts();
            ExpandRibbon();
        }

        private void CallProducts()
        {
            frmItemMaster objItemMaster = null;

            try
            {
                objItemMaster = new frmItemMaster(false);
                objItemMaster.MdiParent = this;
                objItemMaster.WindowState = FormWindowState.Maximized;
                objItemMaster.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objItemMaster.Dispose();
                System.GC.Collect();
                CallProducts();
            }
        }
        private void CallSalesOrder()
        {
            if (ClsMainSettings.blnProductStatus && ClsMainSettings.Itmcount <= Productval.ItmLimit)
            {
                FrmSalesOrder objSalesOrder = null;

                try
                {

                    objSalesOrder = new FrmSalesOrder(2, 0);
                    objSalesOrder.Text = "Sales Order" + (this.MdiChildren.Length + 1);
                    objSalesOrder.WindowState = FormWindowState.Maximized;
                    objSalesOrder.MdiParent = this;
                    objSalesOrder.objFrmTradingMain = this;
                    objSalesOrder.Update();
                    objSalesOrder.Show();

                    ExpandRibbon();
                }
                catch (OutOfMemoryException ex)
                {
                    objSalesOrder.Dispose();
                    System.GC.Collect();
                    CallSalesOrder();
                }
            }
        }
        private void CallSalesInvoice()
        {
            if (ClsMainSettings.blnProductStatus && ClsMainSettings.Itmcount <= Productval.ItmLimit)
            {
                FrmSalesInvoice objSalesInvoice = null;

                try
                {

                    objSalesInvoice = new FrmSalesInvoice(3, 0);
                    objSalesInvoice.Text = "Sales Invoice" + (this.MdiChildren.Length + 1);
                    objSalesInvoice.WindowState = FormWindowState.Maximized;
                    objSalesInvoice.MdiParent = this;
                    objSalesInvoice.objFrmTradingMain = this;
                    objSalesInvoice.Update();
                    objSalesInvoice.Show();

                    ExpandRibbon();
                }
                catch (OutOfMemoryException ex)
                {
                    objSalesInvoice.Dispose();
                    System.GC.Collect();
                    CallSalesInvoice();
                }
            }
        }
        private void SaleInvoice_Click(object sender, EventArgs e)
        {
            AppCmdSales.Text = "SQ";
            AppCmdSales.Execute();
        }

        private void AppCmdSales_Executed(object sender, EventArgs e)
        {
            if (ClsMainSettings.blnProductStatus && ClsMainSettings.Itmcount <= Productval.ItmLimit)
            {
                FrmSales objSales = null;

                try
                {
                    int iType = 1;

                    if (AppCmdSales.Text == "Sales Quotation")
                        iType = 1;
                    else if (AppCmdSales.Text == "Sales Order")
                        iType = 2;
                    else if (AppCmdSales.Text == "Sales Invoice")
                        iType = 3;
                    else if (AppCmdSales.Text == "POS")
                        iType = 4;
                    else if (AppCmdSales.Text == "Credit Note")
                        iType = 5;


                    objSales = new FrmSales(iType, 0);

                    objSales.Text = AppCmdSales.Text + (this.MdiChildren.Length + 1);
                    objSales.WindowState = FormWindowState.Maximized;
                    objSales.MdiParent = this;
                    objSales.objFrmTradingMain = this;
                    objSales.Update();
                    objSales.Show();

                    ExpandRibbon();
                }
                catch (OutOfMemoryException ex)
                {
                    objSales.Dispose();
                    System.GC.Collect();
                    AppCmdSales.Execute();
                }
            }
        }

        private void SalesQuotation_Click(object sender, EventArgs e)
        {
            AppCmdSales.Text = "Sales Quotation";
            AppCmdSales.Execute();
        }

        private void SalesOrder_Click(object sender, EventArgs e)
        {
            CallSalesOrder();
        }
        private void SalesInvoice_Click(object sender, EventArgs e)
        {
            CallSalesInvoice();
        }

        private void SalesReturn_Click(object sender, EventArgs e)
        {
            AppCmdSales.Text = "Credit Note";
            AppCmdSales.Execute();
        }

        private void OpeningStock_Click(object sender, EventArgs e)
        {
            using (FrmOpeningStock objOpeningStock = new FrmOpeningStock())
            {
                objOpeningStock.ShowDialog();
            }
        }

        private void Uom_Click(object sender, EventArgs e)
        {
            using (FrmUnitOfMeasurement objUoM = new FrmUnitOfMeasurement())
            {
                objUoM.ShowDialog();
            }
        }

        private void User_Click(object sender, EventArgs e)
        {
            using (FrmUser objUser = new FrmUser())
            {
                objUser.ShowDialog();
            }
        }

        private void UserRole_Click(object sender, EventArgs e)
        {
            using (frmRole objfrmRole = new frmRole())
            {
                objfrmRole.ShowDialog();
            }
        }

        private void AppCmdRoleSettings_Executed(object sender, EventArgs e)
        {
            frmRole objFrmRoleSettings = null;

            try
            {
                objFrmRoleSettings = new frmRole();

                objFrmRoleSettings.Text = "User Role" + (this.MdiChildren.Length + 1);
                objFrmRoleSettings.WindowState = FormWindowState.Maximized;
                objFrmRoleSettings.MdiParent = this;
                objFrmRoleSettings.Update();
                objFrmRoleSettings.Show();

                ExpandRibbon();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRoleSettings.Dispose();
                System.GC.Collect();
                AppCmdRoleSettings.Execute();
            }
        }

        //private void CallRoleSettings()
        //{
        //    frmRole objFrmRoleSettings = null;

        //    try
        //    {
        //        objFrmRoleSettings = new frmRole();
        //        objFrmRoleSettings.Text = "Role Settings " + (this.MdiChildren.Length + 1);
        //        objFrmRoleSettings.MdiParent = this;
        //        //objFrmRoleSettings.WindowState = FormWindowState.Maximized;
        //        objFrmRoleSettings.Update();
        //        objFrmRoleSettings.Show();
        //        ExpandRibbon();
        //    }
        //    catch (OutOfMemoryException ex)
        //    {
        //        objFrmRoleSettings.Dispose();
        //        System.GC.Collect();
        //        CallRoleSettings();
        //    }
        //}

        private void Configuration_Click(object sender, EventArgs e)
        {
            FrmConfigurationSetting objConfigurationSetting = new FrmConfigurationSetting();
            objConfigurationSetting.ModuleID = 2;// (int)ModuleID.Company;
            objConfigurationSetting.Show();
        }

        private void ChangePassword_Click(object sender, EventArgs e)
        {
            using (FrmChangePassword objFrmChangePassword = new FrmChangePassword())
            {
                objFrmChangePassword.ShowDialog();
            }
        }

        private void PricingScheme_Click(object sender, EventArgs e)
        {
            using (frmPricingScheme objfrmPricingScheme = new frmPricingScheme())
            {
                objfrmPricingScheme.ShowDialog();
            }
        }

        private void AccountSettings_Click(object sender, EventArgs e)
        {
            using (FrmAccountSettings objAccountSettings = new FrmAccountSettings())
            {
                objAccountSettings.ShowDialog();
            }
        }

        private void UomConversion_Click(object sender, EventArgs e)
        {
            using (FrmUOMConversions objUOMConversions = new FrmUOMConversions())
            {
                objUOMConversions.ShowDialog();
            }
        }

        private void PurchaseQuotation_Click(object sender, EventArgs e)
        {
            AppCmdPurchase.Text = "Purchase Quotation";
            AppCmdPurchase.Execute();
        }

        private void Employee_Click(object sender, EventArgs e)
        {
            using (FrmEmployee objEmployee = new FrmEmployee())
            {
                objEmployee.ShowDialog();
            }
        }

        private void Expense_Click(object sender, EventArgs e)
        {
            using (frmExpense objExpense = new frmExpense(true))
            {
                objExpense.ShowDialog();
            }
        }

        private void Email_Click(object sender, EventArgs e)
        {
            using (FrmEmailSettings objFrmEmailSettings = new FrmEmailSettings())
            {
                objFrmEmailSettings.ShowDialog();
            }
        }

        private void btnScanning_Click(object sender, EventArgs e)
        {
            using (FrmScanning objFrmScanning = new FrmScanning())
            {
                objFrmScanning.ShowDialog();
            }
        }

        private void Payments_Click(object sender, EventArgs e)
        {
            using (frmReceiptsAndPayments objFrmReceiptsAndPayments = new frmReceiptsAndPayments((int)eMenuID.Payment, (int)ReceiptsAndPaymentsModes.PurchasePayments))
            {
                objFrmReceiptsAndPayments.objFrmTradingMain = this;
                objFrmReceiptsAndPayments.ShowDialog();
            }
        }

        private void Receipts_Click(object sender, EventArgs e)
        {
            using (frmReceiptsAndPayments objFrmReceiptsAndPayments = new frmReceiptsAndPayments((int)eMenuID.Receipt, (int)ReceiptsAndPaymentsModes.SalesReceipts))
            {
                objFrmReceiptsAndPayments.objFrmTradingMain = this;
                objFrmReceiptsAndPayments.ShowDialog();
            }
        }

        private void ItemIssue_Click(object sender, EventArgs e)
        {
            CallItemIssue(1, 0, 0, 0, 0);
            ExpandRibbon();
        }

        private void CallItemIssue(int intFormType, int intCompanyID, int intOrderTypeID, int intReferenceID, long lngItemIssueID)
        {
            if (ClsMainSettings.blnProductStatus && ClsMainSettings.Itmcount <= Productval.ItmLimit)
            {
                FrmItemIssue objItemIssue = null;

                try
                {
                    objItemIssue = new FrmItemIssue();
                    objItemIssue.PintFormType = intFormType;

                    objItemIssue.PintCompanyID = intCompanyID;
                    objItemIssue.PintOrderTypeID = intOrderTypeID;
                    objItemIssue.PlngReferenceID = intReferenceID;
                    objItemIssue.lngItemIssueID = lngItemIssueID;

                    if (intFormType == 2)
                        objItemIssue.Icon = ((System.Drawing.Icon)(Properties.Resources.Material_Issue3));

                    objItemIssue.MdiParent = this;
                    objItemIssue.objFrmTradingMain = this;
                    objItemIssue.WindowState = FormWindowState.Maximized;
                    objItemIssue.Show();
                }
                catch (OutOfMemoryException ex)
                {
                    objItemIssue.Dispose();
                    System.GC.Collect();
                    CallItemIssue(intFormType, intCompanyID, intOrderTypeID, intReferenceID, lngItemIssueID);
                }
            }
        }
        private void CallDirectDelivery(int intFormType, int intCompanyID, int intOrderTypeID, int intReferenceID, long lngItemIssueID)
        {
            if (ClsMainSettings.blnProductStatus && ClsMainSettings.Itmcount <= Productval.ItmLimit)
            {
                FrmDirectDelivery objDirectDelivery = null;

                try
                {
                    objDirectDelivery = new FrmDirectDelivery();
                    objDirectDelivery.PintFormType = intFormType;

                    objDirectDelivery.PintCompanyID = intCompanyID;
                    objDirectDelivery.PintOrderTypeID = intOrderTypeID;
                    objDirectDelivery.PlngReferenceID = intReferenceID;
                    objDirectDelivery.lngItemIssueID = lngItemIssueID;

                    if (intFormType == 2)
                        objDirectDelivery.Icon = ((System.Drawing.Icon)(Properties.Resources.Material_Issue3));

                    objDirectDelivery.MdiParent = this;
                    objDirectDelivery.objFrmTradingMain = this;
                    objDirectDelivery.WindowState = FormWindowState.Maximized;
                    objDirectDelivery.Show();
                }
                catch (OutOfMemoryException ex)
                {
                    objDirectDelivery.Dispose();
                    System.GC.Collect();
                    CallDirectDelivery(intFormType, intCompanyID, intOrderTypeID, intReferenceID, lngItemIssueID);
                }
            }
        }
        private void CallMaterialIssue(long lngMaterialIssueID)
        {
            FrmMaterialIssue objMaterialIssue = null;

            try
            {
                objMaterialIssue = new FrmMaterialIssue();
                objMaterialIssue.lngMaterialIssueID = lngMaterialIssueID;
                objMaterialIssue.MdiParent = this;
                objMaterialIssue.objFrmTradingMain = this;
                objMaterialIssue.WindowState = FormWindowState.Maximized;
                objMaterialIssue.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objMaterialIssue.Dispose();
                System.GC.Collect();
                CallMaterialIssue(lngMaterialIssueID);
            }
        }

        private void CallMaterialReturn()
        {
            FrmMaterialReturn objMaterialReturn = null;

            try
            {
                objMaterialReturn = new FrmMaterialReturn();
                objMaterialReturn.MdiParent = this;
                objMaterialReturn.WindowState = FormWindowState.Maximized;
                objMaterialReturn.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objMaterialReturn.Dispose();
                System.GC.Collect();
                CallMaterialReturn();
            }
        }

        private void btnPurchases_Click(object sender, EventArgs e)
        {
            this.Purchase.Select();
        }

        private void btnSale_Click(object sender, EventArgs e)
        {
            this.Sales.Select();
        }

        private void btnInventory_Click(object sender, EventArgs e)
        {
            this.Inventory.Select();
        }

        private void btnReports_Click(object sender, EventArgs e)
        {
            this.TradingReports.Select();
        }

        private void btnCurrency_Click(object sender, EventArgs e)
        {
            using (FrmCurrency objFrmCurrency = new FrmCurrency())
            {
                objFrmCurrency.ShowDialog();
            }
        }

        private void btnCompany_Click(object sender, EventArgs e)
        {

        }

        private void About_Click(object sender, EventArgs e)
        {
            using (frmAbout objfrmAbout = new frmAbout())
            {
                objfrmAbout.ShowDialog();
            }
        }

        private void buttonItem20_Click(object sender, EventArgs e)
        {
            using (FrmCompany objCom = new FrmCompany())
            {
                objCom.ShowDialog();
            }
        }

        private void buttonItem21_Click(object sender, EventArgs e)
        {
            using (FrmVendor objVendor = new FrmVendor(2))
            {
                objVendor.ShowDialog();
            }
        }

        private void buttonItem23_Click(object sender, EventArgs e)
        {
            using (FrmVendor objVendor = new FrmVendor(3))
            {
                objVendor.ShowDialog();
            }
        }

        private void buttonItem24_Click(object sender, EventArgs e)
        {
            using (FrmEmployee objEmployee = new FrmEmployee())
            {
                objEmployee.ShowDialog();
            }
        }

        private void buttonOptions_Click(object sender, EventArgs e)
        {
            FrmConfigurationSetting objConfigurationSetting = new FrmConfigurationSetting();
            objConfigurationSetting.Show();
        }

        private void bSales_Click(object sender, EventArgs e)
        {
            ribbonControl.Expanded = !ribbonControl.Expanded;

            if (ribbonControl.Expanded)
            {
                bSales.Tooltip = "Minimize Menu";
                bSales.Image = global::MyBooksERP.Properties.Resources.Minimize;
            }
            else
            {
                ribbonControl.Expanded = false;
                bSales.Tooltip = "Maximize Menu";
                bSales.Image = global::MyBooksERP.Properties.Resources.Maximize;
            }
        }

        private void ReportProducts_Click(object sender, EventArgs e)
        {
            AppCommandReports.Text = "Products";
            AppCommandReports.Execute();
        }

        private void ExpandRibbon()
        {
            //ribbonControl.Expanded = false;
            //bSales.Tooltip = "Maximize Menu";
            //bSales.Image = global::MyBooksERP.Properties.Resources.Maximize;
        }

        private void ReportEmployee_Click(object sender, EventArgs e)
        {
            AppCommandReports.Text = "Employee";
            AppCommandReports.Execute();
        }

        private void ReportsAssociates_Click(object sender, EventArgs e)
        {
            AppCommandReports.Text = "Associates";
            AppCommandReports.Execute();
        }

        private void ReportsSales_Click(object sender, EventArgs e)
        {
            AppCommandReports.Text = "Sales";
            AppCommandReports.Execute();
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void ReportsPurchase_Click(object sender, EventArgs e)
        {
            AppCommandReports.Text = "Purchase";
            AppCommandReports.Execute();
        }

        private void LoadAlerts()
        {
            DataTable datAlert = null;
            int intModuleID = (int)eModuleID.Inventory;

            try
            {
                this.dgvAlerts.Rows.Clear();
                datAlert = this.objclsBLLMain.GetUserAlerts(intModuleID);

                if (datAlert == null || datAlert.Rows.Count == 0)
                {
                    this.pctNoalerts.Visible = true;
                    return;
                }

                this.pctNoalerts.Visible = false;
                foreach (DataRow dtRow in datAlert.Rows)
                {
                    int ipos = this.dgvAlerts.Rows.Add();
                    this.dgvAlerts.Rows[ipos].Cells["colAlertMsg"].Value = dtRow["AlertMessage"].ToString();
                }
                this.dgvAlerts.Update();
            }
            catch (Exception)
            {

            }
        }

        private void bwrAlert_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (!bwrAlert.IsBusy)
                {
                    int intModuleID = (int)eModuleID.Inventory;

                    e.Result = this.objclsBLLMain.GetUserAlerts(intModuleID);
                }
            }
            catch (Exception)
            {
                e.Cancel = true;
            }
        }

        private void bwrAlert_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DataTable datAlert = null;

            if (e.Error != null)
                return;
            else if (e.Cancelled)
                return;

            if (e.Result != null)
            {
                datAlert = e.Result as DataTable;

                FillAlerts(datAlert);
            }

            bwrAlert.Dispose();
        }

        private void FillAlerts(DataTable datAlert)
        {
            int intNewAlertCount = 0;

            try
            {
                this.tmrAlert.Interval = 6000;
                //    this.tmrAlert.Enabled = true;

                this.dgvAlerts.Rows.Clear();
                this.dgvAlerts.Update();

                if (datAlert == null || datAlert.Rows.Count == 0)
                {
                    this.pctNoalerts.Visible = true;
                    return;
                }
                this.pctNoalerts.Visible = false;

                if (strAlertStatusID == "0")
                    datAlert.DefaultView.RowFilter = "";
                else
                    datAlert.DefaultView.RowFilter = "StatusID In (" + strAlertStatusID + ")";

                datAlert.DefaultView.Sort = " StatusID ASC";

                foreach (DataRow dtRow in datAlert.DefaultView.ToTable().Rows)
                {
                    int ipos = this.dgvAlerts.Rows.Add();
                    this.dgvAlerts.Rows[ipos].Cells["colAlertMsg"].Value = dtRow["AlertMessage"].ToString();
                    this.dgvAlerts.Rows[ipos].Cells["clmnAlertSettingsID"].Value = dtRow["AlertSettingID"].ToString();
                    this.dgvAlerts.Rows[ipos].Cells["clmnReferenceID"].Value = dtRow["ReferenceID"].ToString();
                    this.dgvAlerts.Rows[ipos].Cells["clmnStatusID"].Value = dtRow["StatusID"].ToString();
                    this.dgvAlerts.Rows[ipos].Cells["clmnAlertID"].Value = dtRow["AlertID"].ToString();

                    if (dtRow["StatusID"].ToInt32() == (int)AlertStatus.Open)
                    {
                        this.dgvAlerts.Rows[ipos].DefaultCellStyle.BackColor = Color.LightSlateGray;
                        this.dgvAlerts.Rows[ipos].DefaultCellStyle.ForeColor = Color.White;
                        intNewAlertCount += 1;
                    }
                    else if (dtRow["StatusID"].ToInt32() == (int)AlertStatus.Blocked)
                    {
                        this.dgvAlerts.Rows[ipos].DefaultCellStyle.BackColor = Color.LightSlateGray;
                        this.dgvAlerts.Rows[ipos].DefaultCellStyle.ForeColor = Color.Red;
                    }
                    else
                    {
                        this.dgvAlerts.Rows[ipos].DefaultCellStyle.BackColor = Color.LightSlateGray;
                        this.dgvAlerts.Rows[ipos].DefaultCellStyle.ForeColor = Color.Black;
                    }
                }
                this.dgvAlerts.Update();

                if (intNewAlertCount > 0)
                {
                    dockAlertList.Text = "Alerts (" + intNewAlertCount + ") ";

                    if (intNewAlertCount > PintNewAlertCount)
                    {
                        barTaskList.AutoHide = false;
                        PintNewAlertCount = intNewAlertCount;
                    }
                }
                else
                    dockAlertList.Text = "Alerts ";

                this.dgvAlerts.ClearSelection();
            }
            catch (Exception)
            {

            }
        }

        private void tmrAlert_Tick(object sender, EventArgs e)
        {
            // this.tmrAlert.Enabled = false;
            //this.bwrAlert.RunWorkerAsync();

            DataTable datAlert = null;

            switch (lngAlertID)
            {
                case (long)AlertSettingsTypes.FirstDisplay:
                    int intModuleID = (int)eModuleID.Inventory;
                    datAlert = this.objclsBLLMain.GetUserAlerts(intModuleID);
                    break;
                case (long)AlertSettingsTypes.PurchaseOrderApproved:
                    datAlert = this.objclsBLLMain.GetPurchaseOrderApprovedAlert();
                    break;
                case (long)AlertSettingsTypes.SalesQuotationSubmitted:
                    datAlert = this.objclsBLLMain.GetSalesQuotationSubmittedAlert();
                    break;
                case (long)AlertSettingsTypes.SalesQuotationApproved:
                    datAlert = this.objclsBLLMain.GetSalesQuotationApprovedAlert();
                    break;
                case (long)AlertSettingsTypes.SalesQuotationCancelled:
                    datAlert = this.objclsBLLMain.GetSalesQuotationCancelledAlert();
                    break;
                case (long)AlertSettingsTypes.SalesOrderAdvancePaid:
                    datAlert = this.objclsBLLMain.GetSalesOrderAdvancePaidAlert();
                    break;
                case (long)AlertSettingsTypes.SaleOrderCreated:
                    datAlert = this.objclsBLLMain.GetSalesOrderCreatedAlert();
                    break;
                case (long)AlertSettingsTypes.SaleOrderSubmitted:
                    datAlert = this.objclsBLLMain.GetSaleOrderSubmittedAlert();
                    break;
                case (long)AlertSettingsTypes.SaleOrderApproved:
                    datAlert = this.objclsBLLMain.GetSaleOrderApprovedAlert();
                    break;
                case (long)AlertSettingsTypes.SaleOrderRejected:
                    datAlert = this.objclsBLLMain.GetSaleOrderRejectedAlert();
                    break;
                case (long)AlertSettingsTypes.Receipt:
                    datAlert = this.objclsBLLMain.GetReceiptAlert();
                    break;
                case (long)AlertSettingsTypes.MaterialIssue:
                    datAlert = this.objclsBLLMain.GetMaterialIssueAlert();
                    break;
                case (long)AlertSettingsTypes.DeliveryNote:
                    datAlert = this.objclsBLLMain.GetDeliveryNotetAlert();
                    break;
                default:
                    break;
            }


            if (datAlert != null)
            {
                FillAlerts(datAlert);
            }

            tmrAlert.Stop();
        }

        private void AppCommandReports_Executed(object sender, EventArgs e)
        {
            FrmRptPurchase objFrmRptPurchase = null;
            frmRptSales objfrmRptSales = null;
            FrmRptAssociates objFrmRptAssociates = null;
            FrmRptProducts objFrmRptProducts = null;
            FrmRptEmployee objFrmRptEmployee = null;
            FrmRptStock objFrmRptStock = null;

            try
            {
                if (AppCommandReports.Text == "Purchase")
                {
                    objFrmRptPurchase = new FrmRptPurchase();
                    objFrmRptPurchase.MdiParent = this;
                    objFrmRptPurchase.WindowState = FormWindowState.Maximized;
                    objFrmRptPurchase.Show();
                    objFrmRptPurchase.Update();
                }
                else if (AppCommandReports.Text == "Sales")
                {
                    objfrmRptSales = new frmRptSales();
                    objfrmRptSales.MdiParent = this;
                    objfrmRptSales.WindowState = FormWindowState.Maximized;
                    objfrmRptSales.Show();
                    objfrmRptSales.Update();
                }
                else if (AppCommandReports.Text == "Associates")
                {
                    objFrmRptAssociates = new FrmRptAssociates();
                    objFrmRptAssociates.MdiParent = this;
                    objFrmRptAssociates.WindowState = FormWindowState.Maximized;
                    objFrmRptAssociates.Show();
                    objFrmRptAssociates.Update();
                }
                else if (AppCommandReports.Text == "Products")
                {
                    objFrmRptProducts = new FrmRptProducts();
                    objFrmRptProducts.MdiParent = this;
                    objFrmRptProducts.WindowState = FormWindowState.Maximized;
                    objFrmRptProducts.Show();
                    objFrmRptProducts.Update();
                }
                else if (AppCommandReports.Text == "Employee")
                {
                    objFrmRptEmployee = new FrmRptEmployee();
                    objFrmRptEmployee.MdiParent = this;
                    objFrmRptEmployee.WindowState = FormWindowState.Maximized;
                    objFrmRptEmployee.Show();
                    objFrmRptEmployee.Update();
                }
                else if (AppCommandReports.Text == "Stock")
                {
                    objFrmRptStock = new FrmRptStock();
                    objFrmRptStock.MdiParent = this;
                    objFrmRptStock.WindowState = FormWindowState.Maximized;
                    objFrmRptStock.Show();
                    objFrmRptStock.Update();
                }
                ExpandRibbon();
            }
            catch (OutOfMemoryException)
            {
                if (AppCommandReports.Text == "Purchase")
                    objFrmRptPurchase.Dispose();
                else if (AppCommandReports.Text == "Sales")
                    objfrmRptSales.Dispose();
                else if (AppCommandReports.Text == "Associates")
                    objFrmRptAssociates.Dispose();
                else if (AppCommandReports.Text == "Products")
                    objFrmRptProducts.Dispose();
                else if (AppCommandReports.Text == "Employee")
                    objFrmRptEmployee.Dispose();
                else if (AppCommandReports.Text == "Stock")
                    objFrmRptStock.Dispose();

                System.GC.Collect();
                AppCommandReports.Execute();
            }
        }

        private void AlertSettings_Click(object sender, EventArgs e)
        {
            try
            {
                // Calling Reference form for UserRoles\

                FrmAlertSettings objFrmAlertSetting;
                objFrmAlertSetting = new FrmAlertSettings();
                objFrmAlertSetting.ModuleID = 2;// (int)ModuleID.Company;
                objFrmAlertSetting.ShowDialog();
                objFrmAlertSetting = null;

                //LoadCombos(1);
            }
            catch (Exception Ex)
            {

                MessageBox.Show("Error on AlertSettings_Click() " + Ex.Message.ToString());
            }
        }

        private void UserHierachy_Click(object sender, EventArgs e)
        {
            try
            {
                // Calling Reference form for UserRoles\

                FrmOrganizationHierarchySetting objFrmHierarchySetting;
                objFrmHierarchySetting = new FrmOrganizationHierarchySetting();
                objFrmHierarchySetting.ShowDialog();
                objFrmHierarchySetting = null;

                //LoadCombos(1);
            }
            catch (Exception Ex)
            {

                MessageBox.Show("Error on UserHierachy_Click() " + Ex.Message.ToString());
            }
        }

        private void ReportsStock_Click(object sender, EventArgs e)
        {
            AppCommandReports.Text = "Stock";
            AppCommandReports.Execute();
        }

        private void BackUp_Click(object sender, EventArgs e)
        {
            using (FrmBackup objBackup = new FrmBackup())
            {
                objBackup.ShowDialog();
            }
        }

        private void CreditNote_Click(object sender, EventArgs e)
        {
            CallCreditNote();
        }

        private void CallCreditNote()
        {
            FrmCreditNote objSalesReturn = null;

            try
            {
                objSalesReturn = new FrmCreditNote();

                objSalesReturn.Text = "Credit Note " + (this.MdiChildren.Length + 1);
                objSalesReturn.WindowState = FormWindowState.Maximized;
                objSalesReturn.MdiParent = this;
                objSalesReturn.Update();
                objSalesReturn.Show();

                ExpandRibbon();
            }
            catch (OutOfMemoryException ex)
            {
                objSalesReturn.Dispose();
                System.GC.Collect();
                CallCreditNote();
            }
        }

        private void btnRFQ_Click(object sender, EventArgs e)
        {
            CallRFQ();
        }

        private void CallRFQ()
        {
            FrmRFQ objRFQ = null;

            try
            {
                objRFQ = new FrmRFQ();

                objRFQ.Text = "RFQ " + (this.MdiChildren.Length + 1);
                objRFQ.WindowState = FormWindowState.Maximized;
                objRFQ.MdiParent = this;
                objRFQ.Update();
                objRFQ.Show();

                ExpandRibbon();
            }
            catch (OutOfMemoryException ex)
            {
                objRFQ.Dispose();
                System.GC.Collect();
                CallRFQ();
            }
        }

        private void btnPaymentsReceipts_Click(object sender, EventArgs e)
        {
            CallRptPaymentReceipt();
        }

        private void CallRptPaymentReceipt()
        {
            FrmRptPaymentReceipt objFrmRptPaymentReceipt = null;
            try
            {
                objFrmRptPaymentReceipt = new FrmRptPaymentReceipt();

                objFrmRptPaymentReceipt.Text = "Payment/Receipts " + (this.MdiChildren.Length + 1);
                objFrmRptPaymentReceipt.WindowState = FormWindowState.Maximized;
                objFrmRptPaymentReceipt.MdiParent = this;
                objFrmRptPaymentReceipt.Update();
                objFrmRptPaymentReceipt.Show();

                ExpandRibbon();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptPaymentReceipt.Dispose();
                System.GC.Collect();
                CallRptPaymentReceipt();
            }
        }

        private void POS_Click(object sender, EventArgs e)
        {
            CallPOS();
        }

        private void CallPOS()
        {
            FrmPOS objPOS = null;

            try
            {
                objPOS = new FrmPOS();

                objPOS.Text = "POS " + (this.MdiChildren.Length + 1);
                objPOS.WindowState = FormWindowState.Maximized;
                objPOS.MdiParent = this;
                objPOS.Update();
                objPOS.Show();

                ExpandRibbon();
            }
            catch (OutOfMemoryException ex)
            {
                objPOS.Dispose();
                System.GC.Collect();
                CallPOS();
            }
        }

        private void MandatoryDocumentTypes_Click(object sender, EventArgs e)
        {
            using (FrmMandatoryDocumentTypes objMandatoryDocumentTypes = new FrmMandatoryDocumentTypes())
            {
                objMandatoryDocumentTypes.ShowDialog();
            }
        }

        private void btnVendorHistory_Click(object sender, EventArgs e)
        {
            using (FrmVendorHistory objFrmVendorHistory = new FrmVendorHistory())
            {
                objFrmVendorHistory.ShowDialog();
            }
        }

        private void btnTermsAndConditions_Click(object sender, EventArgs e)
        {
            using (FrmTermsAndConditions objFrmTermsAndConditions = new FrmTermsAndConditions())
            {
                objFrmTermsAndConditions.ShowDialog();
            }
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            this.Settings.Select();
        }

        private void ItemAllocation_Click(object sender, EventArgs e)
        {
            using (FrmLocationTransfer objLocationTransfer = new FrmLocationTransfer())
            {
                objLocationTransfer.ShowDialog();
            }
        }

        private void StockTransfer_Click(object sender, EventArgs e)
        {
            AppCmdInventory.Text = "Stock Transfer";
            AppCmdInventory.Execute();

        }

        private void BtnSummary_Click(object sender, EventArgs e)
        {
            CallSummary();
        }

        private void CallSummary()
        {
            FrmSummary objFrmSummary = null;

            try
            {
                objFrmSummary = new FrmSummary();

                objFrmSummary.Text = "Summary " + (this.MdiChildren.Length + 1);
                objFrmSummary.WindowState = FormWindowState.Maximized;
                objFrmSummary.MdiParent = this;
                objFrmSummary.Update();
                objFrmSummary.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmSummary.Dispose();
                System.GC.Collect();
                CallSummary();
            }
        }

        private void AppCmdInventory_Executed(object sender, EventArgs e)
        {
            CallTransferOrder();
        }

        private void CallTransferOrder()
        {
            FrmWarehouseTransfer objWarehouseTransfer = null;

            try
            {
                objWarehouseTransfer = new FrmWarehouseTransfer();
                objWarehouseTransfer.Text = AppCmdInventory.Text + " " + (this.MdiChildren.Length + 1);
                objWarehouseTransfer.MdiParent = this;
                objWarehouseTransfer.WindowState = FormWindowState.Maximized;
                objWarehouseTransfer.Show();
                objWarehouseTransfer.Update();

                ExpandRibbon();
            }
            catch (OutOfMemoryException ex)
            {
                objWarehouseTransfer.Dispose();
                System.GC.Collect();
                CallTransferOrder();
            }
        }

        private void BtnReportTemplate_Click(object sender, EventArgs e)
        {
            CallReportTemplate();
        }

        private void CallReportTemplate()
        {
            FrmInvoiceTemplateDesign objFrmInvoiceTemplateDesign = null;

            try
            {
                objFrmInvoiceTemplateDesign = new FrmInvoiceTemplateDesign();
                objFrmInvoiceTemplateDesign.Text = "Report Template " + (this.MdiChildren.Length + 1);
                objFrmInvoiceTemplateDesign.MdiParent = this;
                objFrmInvoiceTemplateDesign.WindowState = FormWindowState.Maximized;
                objFrmInvoiceTemplateDesign.Show();
                objFrmInvoiceTemplateDesign.Update();

                ExpandRibbon();
            }
            catch (OutOfMemoryException)
            {
                objFrmInvoiceTemplateDesign.Dispose();
                System.GC.Collect();
                CallReportTemplate();
            }
        }

        private void BtnEProducts_Click(object sender, EventArgs e)
        {
            CallProducts();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnMaterialIssue_Click(object sender, EventArgs e)
        {
            CallMaterialIssue(0);
            ExpandRibbon();
        }

        private void BtnMaterialReturn_Click(object sender, EventArgs e)
        {
            CallMaterialReturn();
            ExpandRibbon();
        }

        private void Warehouse_Click(object sender, EventArgs e)
        {
            using (FrmWarehouse objFrmWarehouse = new FrmWarehouse())
            {
                objFrmWarehouse.ShowDialog();
            }
        }

        private void BtnDebitNote_Click(object sender, EventArgs e)
        {
            CallDebitNote();
        }

        private void BtnCreditNote_Click(object sender, EventArgs e)
        {
            CallCreditNote();
        }

        private void BtnPricingScheme_Click(object sender, EventArgs e)
        {
            using (frmPricingScheme objfrmPricingScheme = new frmPricingScheme())
            {
                objfrmPricingScheme.ShowDialog();
            }
        }

        private void BtnTermsAndConditions_Click_1(object sender, EventArgs e)
        {
            using (FrmTermsAndConditions objFrmTermsAndConditions = new FrmTermsAndConditions())
            {
                objFrmTermsAndConditions.ShowDialog();
            }
        }

        private void BtnMandatoryDocuments_Click(object sender, EventArgs e)
        {
            using (FrmMandatoryDocumentTypes objFrmMandatoryDocumentTypes = new FrmMandatoryDocumentTypes())
            {
                objFrmMandatoryDocumentTypes.ShowDialog();
            }
        }

        private void BtnPExtraCharges_Click(object sender, EventArgs e)
        {
            using (frmExpense objfrmExpense = new frmExpense(true))
            {
                objfrmExpense.intModuleID = 1; //Purchase Direct Expense
                objfrmExpense.ShowDialog();
            }
        }

        private void BtnSExtraCharges_Click(object sender, EventArgs e)
        {
            using (frmExpense objfrmExpense = new frmExpense(true))
            {
                objfrmExpense.intModuleID = 2; //Sales Direct Expense
                objfrmExpense.ShowDialog();
            }
        }

        private void BtnIExtraCharges_Click(object sender, EventArgs e)
        {
            using (frmExpense objfrmExpense = new frmExpense(true))
            {
                objfrmExpense.intModuleID = 3; //Inventory Direct Expense
                objfrmExpense.ShowDialog();
            }
        }

        private void BtnPaymentsInv_Click(object sender, EventArgs e)
        {
            using (frmReceiptsAndPayments objFrmReceiptsAndPayments = new frmReceiptsAndPayments((int)eMenuID.Payment, (int)ReceiptsAndPaymentsModes.InventoryPayments))
            {
                objFrmReceiptsAndPayments.objFrmTradingMain = this;
                objFrmReceiptsAndPayments.ShowDialog();
            }
        }

        private void CallItemIssueReport()
        {
            FrmRptItemIssue objFrmRptItemIssue = null;
            try
            {
                objFrmRptItemIssue = new FrmRptItemIssue();
                objFrmRptItemIssue.Text = "Item Issue Report";
                objFrmRptItemIssue.MdiParent = this;
                objFrmRptItemIssue.WindowState = FormWindowState.Maximized;
                objFrmRptItemIssue.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptItemIssue.Dispose();
                System.GC.Collect();
                CallItemIssueReport();
            }
        }

        private void btnExpense_Click(object sender, EventArgs e)
        {
            ExpenseReport();
        }

        private void ExpenseReport()
        {

            FrmRptExpense objFrmRptExpense = null;

            try
            {
                objFrmRptExpense = new FrmRptExpense();
                objFrmRptExpense.Text = "Expense Report";
                objFrmRptExpense.MdiParent = this;
                objFrmRptExpense.WindowState = FormWindowState.Maximized;
                objFrmRptExpense.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptExpense.Dispose();
                System.GC.Collect();
                ExpenseReport();
            }
        }

        private void BtnHierarchySetting_Click(object sender, EventArgs e)
        {
            using (FrmOrganizationHierarchySetting objFrmOrganizationHierarchySetting = new FrmOrganizationHierarchySetting())
            {
                objFrmOrganizationHierarchySetting.ShowDialog();
            }
        }

        private void CallPurchase(int intPurchaseID, int intType, bool blnIsFromApproval, int PintApprovalPermission)
        {
            FrmPurchase objpurchase = null;

            try
            {
                string strFormLabel = "";

                if (intType == 1)
                    strFormLabel = "Purchase Quotation";
                else if (intType == 2)
                    strFormLabel = "Purchase Order";

                objpurchase = new FrmPurchase(intType);
                objpurchase.Text = strFormLabel + " " + (this.MdiChildren.Length + 1);
                objpurchase.MdiParent = this;
                objpurchase.objFrmTradingMain = this;

                if (blnIsFromApproval)
                    objpurchase.intPurchaseIDToLoad = intPurchaseID;
                else
                    objpurchase.lngReferenceID = intPurchaseID;

                objpurchase.blnIsFrmApproval = blnIsFromApproval;
                objpurchase.PintApprovePermission = PintApprovalPermission;
                objpurchase.WindowState = FormWindowState.Maximized;
                objpurchase.Show();
                objpurchase.Update();
            }
            catch (OutOfMemoryException ex)
            {
                objpurchase.Dispose();
                System.GC.Collect();
                CallPurchase(intPurchaseID, intType, blnIsFromApproval, PintApprovalPermission);
            }
        }

        private void CallSales(int intSaleID, int intType, int intReferenceID, bool blnIsFromApproval, int PintApprovalPermission)
        {
            try
            {
                string strFormLabl = "";

                if (intType == 1)
                {
                    strFormLabl = "Sales Quotation";
                    FrmSales objFrmSales = null;
                    if (blnIsFromApproval)
                    objFrmSales = new FrmSales(intType, intSaleID);
                    else
                    {
                        objFrmSales = new FrmSales(intType, 0);
                        objFrmSales.lngSalesID = intSaleID;
                    }

                    objFrmSales.Text = strFormLabl + " " + (this.MdiChildren.Length + 1);
                    objFrmSales.MdiParent = this;
                    objFrmSales.objFrmTradingMain = this;

                    if (!blnIsFromApproval)
                        objFrmSales.lngReferenceID = intReferenceID;

                    objFrmSales.PblnIsFromApproval = blnIsFromApproval;
                    objFrmSales.PintApprovalPermission = PintApprovalPermission;
                    objFrmSales.WindowState = FormWindowState.Maximized;
                    objFrmSales.Show();
                    objFrmSales.Update();
                }
                else if (intType == 2)
                {
                    strFormLabl = "Sales Order";
                    FrmSalesOrder objFrmSales = null;
                    if (blnIsFromApproval)
                        objFrmSales = new FrmSalesOrder(intType, intSaleID);
                    else
                    {
                        objFrmSales = new FrmSalesOrder(intType, 0);
                        objFrmSales.lngSalesID = intSaleID;
                    }

                    objFrmSales.Text = strFormLabl + " " + (this.MdiChildren.Length + 1);
                    objFrmSales.MdiParent = this;
                    objFrmSales.objFrmTradingMain = this;

                    if (!blnIsFromApproval)
                        objFrmSales.lngReferenceID = intReferenceID;

                    objFrmSales.PblnIsFromApproval = blnIsFromApproval;
                    objFrmSales.PintApprovalPermission = PintApprovalPermission;
                    objFrmSales.WindowState = FormWindowState.Maximized;
                    objFrmSales.Show();
                    objFrmSales.Update();
                }
                else if (intType == 3)
                {
                    strFormLabl = "Sales Invoice";
                    FrmSalesInvoice objFrmSales = null;
                    if (blnIsFromApproval)
                        objFrmSales = new FrmSalesInvoice(intType, intSaleID);
                    else
                    {
                        objFrmSales = new FrmSalesInvoice(intType, 0);
                        objFrmSales.lngSalesID = intSaleID;
                    }

                    objFrmSales.Text = strFormLabl + " " + (this.MdiChildren.Length + 1);
                    objFrmSales.MdiParent = this;
                    objFrmSales.objFrmTradingMain = this;

                    if (!blnIsFromApproval)
                        objFrmSales.lngReferenceID = intReferenceID;

                    objFrmSales.PblnIsFromApproval = blnIsFromApproval;
                    objFrmSales.PintApprovalPermission = PintApprovalPermission;
                    objFrmSales.WindowState = FormWindowState.Maximized;
                    objFrmSales.Show();
                    objFrmSales.Update();
                }
            }
            catch (OutOfMemoryException ex)
            {
                System.GC.Collect();
                CallSales(intSaleID, intType, intReferenceID, blnIsFromApproval, PintApprovalPermission);
            }
        }

        private void blockedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvAlerts.SelectedRows.Count <= 0)
                return;

            foreach (DataGridViewRow dtRow in dgvAlerts.SelectedRows)
                this.objclsBLLMain.SetReadStatus(dtRow.Cells["clmnAlertID"].Value.ToInt32(), (int)AlertStatus.Blocked);

            strAlertStatusID = "60,67";
            CallFillAlerts();
            tsmiBlocked.Checked = tsmiRead.Checked = tsmiUnread.Checked = tsmiAll.Checked = false;
        }

        private void allToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tsmiRead.Checked = tsmiUnread.Checked = tsmiBlocked.Checked = false;

            strAlertStatusID = "0";
            CallFillAlerts();
        }

        private void CallFillAlerts()
        {
            int intModuleID = (int)eModuleID.Inventory;
            DataTable datAlert = this.objclsBLLMain.GetUserAlerts(intModuleID);
            FillAlerts(datAlert);
        }

        private void unreadToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tsmiRead.Checked = tsmiAll.Checked = tsmiBlocked.Checked = false;

            strAlertStatusID = ((int)AlertStatus.Open).ToString();
            CallFillAlerts();
        }

        private void readToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tsmiAll.Checked = tsmiUnread.Checked = tsmiBlocked.Checked = false;

            strAlertStatusID = ((int)AlertStatus.Read).ToString();
            CallFillAlerts();
        }

        private void blockedToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tsmiRead.Checked = tsmiUnread.Checked = tsmiAll.Checked = false;

            strAlertStatusID = ((int)AlertStatus.Blocked).ToString();
            CallFillAlerts();
        }

        private void tsmiMarkAsRead_Click(object sender, EventArgs e)
        {
            if (dgvAlerts.SelectedRows.Count <= 0)
                return;

            foreach (DataGridViewRow dtRow in dgvAlerts.SelectedRows)
                this.objclsBLLMain.SetReadStatus(dtRow.Cells["clmnAlertID"].Value.ToInt32(), (int)AlertStatus.Read);

            strAlertStatusID = "86,98";
            CallFillAlerts();
            tsmiBlocked.Checked = tsmiRead.Checked = tsmiUnread.Checked = tsmiAll.Checked = false;
        }

        private void tsmiMarkAsUnRead_Click(object sender, EventArgs e)
        {
            if (dgvAlerts.SelectedRows.Count <= 0)
                return;

            foreach (DataGridViewRow dtRow in dgvAlerts.SelectedRows)
                this.objclsBLLMain.SetReadStatus(dtRow.Cells["clmnAlertID"].Value.ToInt32(), (int)AlertStatus.Open);

            strAlertStatusID = "60,67";
            CallFillAlerts();
            tsmiBlocked.Checked = tsmiRead.Checked = tsmiUnread.Checked = tsmiAll.Checked = false;
        }

        private void tsmiUnBlock_Click(object sender, EventArgs e)
        {
            if (dgvAlerts.SelectedRows.Count <= 0)
                return;

            foreach (DataGridViewRow dtRow in dgvAlerts.SelectedRows)
                this.objclsBLLMain.SetReadStatus(dtRow.Cells["clmnAlertID"].Value.ToInt32(), (int)AlertStatus.Open);

            strAlertStatusID = "60,67";
            CallFillAlerts();
            tsmiBlocked.Checked = tsmiRead.Checked = tsmiUnread.Checked = tsmiAll.Checked = false;
        }

        private void dgvAlerts_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvAlerts.CurrentRow == null || dgvAlerts.Rows.Count <= 0)
                return;

            this.objclsBLLMain.SetReadStatus(dgvAlerts.CurrentRow.Cells["clmnAlertID"].Value.ToInt32(), (int)AlertStatus.Read);

            if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.SalesQuotationSubmitted)
            {
                //  CallSales(dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), 1, 0, true, (int)ApprovePermission.Approve);
                CallSales(0, 2, dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), false, (int)ApprovePermission.Approve);

            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.SalesQuotationCancelled)
            {
                CallSales(dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), 1, 0, false, (int)ApprovePermission.Approve);
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.SalesQuotationApproved)
            {
                CallSales(0, 2, dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), false, (int)ApprovePermission.Approve);
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.SaleOrderCreated)
            {
                int intCompanyID = 0;
                int intVendorID = 0;
                int intReceiptAndPaymentID = 0;
                int intPaymentTermsID = 0;

                DataTable datTemp1 = objclsBLLMain.FillCombos(new string[] { " CompanyID,VendorID,PaymentTermsID ", "  InvSalesOrderMaster ", "  SalesOrderID = " + dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32() });

                if (datTemp1 != null && datTemp1.Rows.Count > 0)
                {
                    intCompanyID = datTemp1.Rows[0]["CompanyID"].ToInt32();
                    intVendorID = datTemp1.Rows[0]["VendorID"].ToInt32();
                    intPaymentTermsID = datTemp1.Rows[0]["PaymentTermsID"].ToInt32();
                }

                DataTable datTemp = objclsBLLMain.FillCombos(new string[] { " Isnull(PM.ReceiptAndPaymentID,0) As ReceiptAndPaymentID,SO.VendorID ",   
                                    "  AccReceiptAndPaymentMaster PM INNER JOIN AccReceiptAndPaymentDetails PD ON PM.ReceiptAndPaymentID = PD.ReceiptAndPaymentID " + 
                                    "  INNER JOIN InvSalesOrderMaster SO ON SO.SalesOrderID = PD.ReferenceID " , 
                                    "  PM.OperationTypeID = " + (int)OperationType.SalesOrder + " And SO.SalesOrderID = " + dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32() });

                if (datTemp != null && datTemp.Rows.Count > 0)
                {
                    intReceiptAndPaymentID = datTemp.Rows[0]["ReceiptAndPaymentID"].ToInt32();
                    intVendorID = datTemp.Rows[0]["VendorID"].ToInt32();
                }
                if (intReceiptAndPaymentID == 0 && intPaymentTermsID == (int)PaymentTerms.Credit)
                {
                    using (frmReceiptsAndPayments objFrmReceiptsAndPayments = new frmReceiptsAndPayments((int)eMenuID.Receipt, intCompanyID, (int)OperationType.SalesOrder, (int)PaymentTypes.AdvancePayment, intVendorID))
                    {
                        objFrmReceiptsAndPayments.ShowDialog();
                    }
                }
                else
                {
                    CallSales(dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), 2, 0, false, (int)ApprovePermission.Approve);
                }
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.SaleOrderSubmitted)
            {
                int intOrderTypeID = 0;

                DataTable datTemp1 = objclsBLLMain.FillCombos(new string[] { "OrderTypeID", 
                                                                                "InvSalesOrderMaster", 
                                                                                "SalesOrderID = " + dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32() });
                if (datTemp1 != null)
                {
                    if (datTemp1.Rows.Count > 0)
                    {
                        intOrderTypeID = datTemp1.Rows[0]["OrderTypeID"].ToInt32();

                        CallSales(0, 3, dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), false, (int)ApprovePermission.Approve);
                    }
                }
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.SaleOrderApproved)
            {
                int intOrderTypeID = 0;

                DataTable datTemp1 = objclsBLLMain.FillCombos(new string[] { "OrderTypeID", 
                                                                                "InvSalesOrderMaster", 
                                                                                "SalesOrderID = " + dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32() });
                if (datTemp1 != null)
                {
                    if (datTemp1.Rows.Count > 0)
                    {
                        intOrderTypeID = datTemp1.Rows[0]["OrderTypeID"].ToInt32();

                        CallSales(0, 3, dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), false, (int)ApprovePermission.Approve);
                    }
                }
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.SaleOrderRejected)
            {
                CallSales(dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), 2, 0, false, (int)ApprovePermission.Approve);
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.SalesQuotationDueDate)
            {
                DataTable datTemp = objclsBLLMain.FillCombos(new string[] { " StatusID ",   
                                    " InvSalesQuotationMaster ",  
                                    " SalesQuotationID = " + dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32() });
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        if (datTemp.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.SQuotationApproved)
                        {
                            CallSales(dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), 2, 0, false, (int)ApprovePermission.Approve);
                        }
                        else
                        {
                            CallSales(dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), 1, 0, false, (int)ApprovePermission.Approve);
                        }
                    }
                }
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.SalesOrderDueDate)
            {
                DataTable datTemp = objclsBLLMain.FillCombos(new string[] { " StatusID ",   
                                    " InvSalesOrderMaster ",  
                                    " SalesOrderID = " + dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32() });
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        if (datTemp.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.SOrderApproved)
                        {
                            CallSales(dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), 3, 0, false, (int)ApprovePermission.Approve);
                        }
                        else
                        {
                            CallSales(dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), 2, 0, false, (int)ApprovePermission.Approve);
                        }
                    }
                }
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.SalesInvoiceDueDate)
            {
                int intCompanyID = 0;

                DataTable datTemp = objclsBLLMain.FillCombos(new string[] { " CompanyID,StatusID ",   
                                    " InvSalesInvoiceMaster ",  
                                    " SalesInvoiceID = " + dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32() });
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        CallSales(dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), 3, 0, false, 0);
                    }
                }
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.DeliveryNote)
            {
                CallItemIssue(1, 0, 0, 0, dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt64());
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.SalesOrderAdvancePaid)
            {
                int intSalesOrderID = 0;

                DataTable datTemp = objclsBLLMain.FillCombos(new string[] { " ReferenceID ",   
                                    " AccReceiptAndPaymentDetails ",  
                                    " ReceiptAndPaymentID = " + dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32() });
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        intSalesOrderID = datTemp.Rows[0]["ReferenceID"].ToInt32();
                        CallSales(intSalesOrderID, 2, 0, false, (int)ApprovePermission.Approve);
                    }
                }
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.Receipt)
            {
                int intCompanyID = 0, intStatusID = 0;
                long lngSalesInvoiceID = 0, lngJobOrderID = 0, lngReceiptAndPaymentID = 0;

                lngReceiptAndPaymentID = dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt64();

                DataTable datTemp = objclsBLLMain.FillCombos(new string[] {"D.ReferenceID","AccReceiptAndPaymentDetails D Inner Join AccReceiptAndPaymentMaster M On M.ReceiptAndPaymentID = D.ReceiptAndPaymentID",
                                                                            "M.OperationTypeID = "+(int)OperationType.SalesInvoice +" And M.ReceiptAndPaymentTypeID = "+(int)PaymentTypes.InvoicePayment+" And M.ReceiptAndPaymentID = "+lngReceiptAndPaymentID});
                if (datTemp.Rows.Count > 0)
                    lngSalesInvoiceID = datTemp.Rows[0]["ReferenceID"].ToInt64();

                datTemp = objclsBLLMain.FillCombos(new string[] { "SI.CompanyID,SI.StatusID ",   
                                    " InvSalesInvoiceMaster SI  Inner Join InvSalesInvoiceReferenceDetails SIRD ON SIRD.SalesInvoiceID = SI.SalesInvoiceID AND SI.OrderTypeID = 15 INNER JOIN InvSalesOrderMaster SO On SO.SalesOrderID = SIRD.ReferenceID  "+
                                    " Left Join InvSalesOrderReferenceDetails SORD ON SORD.SalesOrderID = SO.SalesOrderID Left Join  InvSalesQuotationMaster SQ On SQ.SalesQuotationID = SORD.ReferenceID AND SO.OrderTypeID = 11",  
                                    " SI.SalesInvoiceID = " + lngSalesInvoiceID });
                if (datTemp.Rows.Count > 0)
                {
                    intCompanyID = datTemp.Rows[0]["CompanyID"].ToInt32();
                    intStatusID = datTemp.Rows[0]["StatusID"].ToInt32();
                }

                if (intStatusID == (int)OperationStatusType.SDelivered)
                {
                    frmReceiptsAndPayments objFrmReceiptAndPayments = new frmReceiptsAndPayments();
                    objFrmReceiptAndPayments.lngRPID = lngReceiptAndPaymentID;
                    objFrmReceiptAndPayments.blnTempIsReceipt = true;
                    objFrmReceiptAndPayments.objFrmTradingMain = this;
                    objFrmReceiptAndPayments.ShowDialog();
                }
                else
                {
                    CallItemIssue(1, intCompanyID, (int)OperationOrderType.DNOTSalesInvoice, lngSalesInvoiceID.ToInt32(), 0);
                }
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.ReorderLevelEntered)
            {
                AppCmdPurchase.Text = "Purchase Order";
                AppCmdPurchase.Execute();
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.PurchaseOrderApproved)
            {
                CallPurchase(dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), 2, false, (int)ApprovePermission.Approve);
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.PurchaseQuotationDueDate)
            {
                DataTable datTemp = objclsBLLMain.FillCombos(new string[] { " StatusID ",   
                                    " InvPurchaseQuotationMaster ",  
                                    " PurchaseQuotationID = " + dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32() });
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        if (datTemp.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.PQuotationApproved)
                        {
                            CallPurchase(dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), 2, false, (int)ApprovePermission.Approve);
                        }
                        else
                        {
                            CallPurchase(dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), 1, false, (int)ApprovePermission.Approve);
                        }
                    }
                }
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.PurchaseOrderDueDate)
            {
                DataTable datTemp = objclsBLLMain.FillCombos(new string[] { " StatusID ",   
                                    " InvPurchaseOrderMaster ",  
                                    " PurchaseOrderID = " + dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32() });
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        if (datTemp.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.POrderApproved)
                        {
                            CallPurchase(dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), 3, false, (int)ApprovePermission.Approve);
                        }
                        else
                        {
                            CallPurchase(dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32(), 2, false, (int)ApprovePermission.Approve);
                        }
                    }
                }
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.DocumentExpiry)
            {
                DataTable datTemp = objclsBLLMain.FillCombos(new string[] { " DocumentID ",   
                                    " DocDocumentMaster ",  
                                    " DocumentID = " + dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32() });
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        using (frmDocumentMaster objfrmDocumentMaster = new frmDocumentMaster())
                        {
                            objfrmDocumentMaster.PintDocumentID = dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt32();
                            objfrmDocumentMaster.ShowDialog();
                        }
                    }
                }
            }
            else if (dgvAlerts.CurrentRow.Cells["clmnAlertSettingsID"].Value.ToInt32() == (int)AlertSettingsTypes.MaterialIssue)
            {
                long lngMaterialIssueID = 0;

                lngMaterialIssueID = dgvAlerts.CurrentRow.Cells["clmnReferenceID"].Value.ToInt64();
                CallMaterialIssue(lngMaterialIssueID);
            }
            strAlertStatusID = "60,67";
            CallFillAlerts();
            tsmiBlocked.Checked = tsmiRead.Checked = tsmiUnread.Checked = tsmiAll.Checked = false;
        }

        private void btnProject_Click(object sender, EventArgs e)
        {
            using (FrmProjects objFrmProjects = new FrmProjects())
            {
                objFrmProjects.ShowDialog();
            }
        }

        private void DirectDeliveryNote_Click(object sender, EventArgs e)
        {
            CallDirectDelivery(1, 0, 0, 0, 0);
        }

        private void DirectGRN_Click(object sender, EventArgs e)
        {
            CallDirectGRN();
        }

        private void buttonItem1_Click(object sender, EventArgs e)
        {
            CallDirectDelivery(1, 0, 0, 0, 0);
            ExpandRibbon();
        }

        private void btnRptPurchaseSummary_Click(object sender, EventArgs e)
        {
            FrmRptPurchaseSummary objFrmRptPurchaseSummary = new FrmRptPurchaseSummary();
            try
            {
                objFrmRptPurchaseSummary.Text = "Purchase Summary Report";
                objFrmRptPurchaseSummary.MdiParent = this;
                objFrmRptPurchaseSummary.WindowState = FormWindowState.Maximized;
                objFrmRptPurchaseSummary.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptPurchaseSummary.Dispose();
                System.GC.Collect();
            }
        }

        private void btnRptAgeing_Click(object sender, EventArgs e)
        {
            FrmRptAgeing objFrmRptAgeing = new FrmRptAgeing();
            try
            {
                objFrmRptAgeing.Text = "Ageing Report";
                objFrmRptAgeing.MdiParent = this;
                objFrmRptAgeing.WindowState = FormWindowState.Maximized;
                objFrmRptAgeing.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptAgeing.Dispose();
                System.GC.Collect();
            }
        }

        private void btnProductMovement_Click(object sender, EventArgs e)
        {

            FrmRptProductMovement objFrmRptProductMovement = new FrmRptProductMovement();
            try
            {
                objFrmRptProductMovement.Text = "Product Movement Report";
                objFrmRptProductMovement.MdiParent = this;
                objFrmRptProductMovement.WindowState = FormWindowState.Maximized;
                objFrmRptProductMovement.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptProductMovement.Dispose();
                System.GC.Collect();
            }
        }

        private void btnRptItemWiseProfit_Click(object sender, EventArgs e)
        {

            FrmRptItemWiseProfit objFrmRptItemWiseProfit = new FrmRptItemWiseProfit();
            try
            {
                objFrmRptItemWiseProfit.Text = "Item Wise Profit Report";
                objFrmRptItemWiseProfit.MdiParent = this;
                objFrmRptItemWiseProfit.WindowState = FormWindowState.Maximized;
                objFrmRptItemWiseProfit.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptItemWiseProfit.Dispose();
                System.GC.Collect();
            }
        }

        private void BtnItemIssueReport_Click_1(object sender, EventArgs e)
        {
            FrmRptItemIssue objFrmRptItemIssue = null;
            try
            {
                objFrmRptItemIssue = new FrmRptItemIssue();
                objFrmRptItemIssue.Text = "Item Issue Report";
                objFrmRptItemIssue.MdiParent = this;
                objFrmRptItemIssue.WindowState = FormWindowState.Maximized;
                objFrmRptItemIssue.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptItemIssue.Dispose();
                System.GC.Collect();
                CallItemIssueReport();
            }
        }

        private void Home_Click(object sender, EventArgs e)
        {

        }

        private void Reports_Click(object sender, EventArgs e)
        {

        }

        private void ProductGroups_Click(object sender, EventArgs e)
        {
            using (frmItemGroupMaster objItemGroupMaster = new frmItemGroupMaster())
            {
                objItemGroupMaster.ShowDialog();
            }
        }

        private void btnrRptGRN_Click(object sender, EventArgs e)
        {
            FrmRptGRN objFrmRptGRN = null;
            try
            {
                objFrmRptGRN = new FrmRptGRN();
                objFrmRptGRN.Text = "GRN Report";
                objFrmRptGRN.MdiParent = this;
                objFrmRptGRN.WindowState = FormWindowState.Maximized;
                objFrmRptGRN.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptGRN.Dispose();
                System.GC.Collect();
            }
        }

        private void btnRptPendingDelivery_Click(object sender, EventArgs e)
        {
            FrmRptPendingDelivery objFrmRptPendingDelivery = null;
            try
            {
                objFrmRptPendingDelivery = new FrmRptPendingDelivery();
                objFrmRptPendingDelivery.Text = "Delivery Report";
                objFrmRptPendingDelivery.MdiParent = this;
                objFrmRptPendingDelivery.WindowState = FormWindowState.Maximized;
                objFrmRptPendingDelivery.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptPendingDelivery.Dispose();
                System.GC.Collect();
            }
        }

        private void btnChartofAcconuts_Click(object sender, EventArgs e)
        {
            using (FrmChartOfAccounts objVendor = new FrmChartOfAccounts())
            {
                objVendor.ShowDialog();
            }
        }

        private void btnOP_Click(object sender, EventArgs e)
        {
            using (frmOpeningBalance objVendor = new frmOpeningBalance())
            {
                objVendor.ShowDialog();
            }
        }

        private void CallVoucher(int intMenuID, string strHeader, object objICO)
        {
            frmJournalVoucher objJournalVoucher = null;
            try
            {
                objJournalVoucher = new frmJournalVoucher();
                objJournalVoucher.Icon = ((System.Drawing.Icon)(objICO));
                objJournalVoucher.TempMenuID = intMenuID;
                objJournalVoucher.Text = strHeader;
                objJournalVoucher.MdiParent = this;
                objJournalVoucher.WindowState = FormWindowState.Maximized;
                objJournalVoucher.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objJournalVoucher.Dispose();
                System.GC.Collect();
                CallVoucher(intMenuID, strHeader, objICO);
            }
        }

        private void btnPaymentVoucher_Click(object sender, EventArgs e)
        {
            CallVoucher((int)eMenuID.PaymentVoucher, "Payment Voucher", Properties.Resources.Extra_Charges1);
        }

        private void btnReceiptVoucher_Click(object sender, EventArgs e)
        {
            CallVoucher((int)eMenuID.ReceiptVoucher, "Receipt Voucher", Properties.Resources.costEstimation3);
        }

        private void btnContraVoucher_Click(object sender, EventArgs e)
        {
            CallVoucher((int)eMenuID.ContraVoucher, "Contra Voucher", Properties.Resources.Check_Receipt1);
        }

        private void BtnJVoucher_Click(object sender, EventArgs e)
        {
            CallVoucher((int)eMenuID.JournalVoucher, "Journal Voucher", Properties.Resources.GereralReceipts_Paymrnts);
        }

        private void btnPurchaseVoucher_Click(object sender, EventArgs e)
        {
            CallVoucher((int)eMenuID.PurchaseVoucher, "Purchase Voucher", Properties.Resources.Purchase_Invoice1);
        }

        private void btnDebitNoteVoucher_Click(object sender, EventArgs e)
        {
            CallVoucher((int)eMenuID.DebitNoteVoucher, "Debit Note Voucher", Properties.Resources.DebitNote);
        }

        private void btnSalesVoucher_Click(object sender, EventArgs e)
        {
            CallVoucher((int)eMenuID.SalesVoucher, "Sales Voucher", Properties.Resources.Sales_invoice1);
        }

        private void btnCreditNoteVoucher_Click(object sender, EventArgs e)
        {
            CallVoucher((int)eMenuID.CreditNoteVoucher, "Credit Note Voucher", Properties.Resources.Sales_Return1);
        }

        private void CallAccountSummary(int SummaryId, string strHeader, object objICO)
        {
            frmAccountSummary objAccountSummary = null;
            try
            {

                objAccountSummary = new frmAccountSummary();
                objAccountSummary.Icon = ((System.Drawing.Icon)(objICO));
                objAccountSummary.SummaryId = SummaryId;
                objAccountSummary.Text = strHeader; //Account Summary
                objAccountSummary.MdiParent = this;
                objAccountSummary.WindowState = FormWindowState.Maximized;

                objAccountSummary.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objAccountSummary.Dispose();
                System.GC.Collect();
                CallAccountSummary(SummaryId, strHeader, objICO);
            }
        }

        private void btnTrialBalance_Click(object sender, EventArgs e)
        {
            CallAccountSummary((int)eMenuID.TrialBalance, "Trial Balance", Properties.Resources.Trial_Balance1);
        }

        private void btnGroupSummary_Click(object sender, EventArgs e)
        {
            CallAccountSummary((int)eMenuID.AccountsSummary, "Group Summary", Properties.Resources.Acconut_summary1);
        }

        private void btnGL_Click(object sender, EventArgs e)
        {
            CallAccountSummary((int)eMenuID.GeneralLedger, "General Ledger", Properties.Resources.General_Ledger1);
        }

        private void BtnDayBook_Click(object sender, EventArgs e)
        {
            CallAccountSummary((int)eMenuID.DayBook, "Day Book", Properties.Resources.Day_book1);
        }

        private void btnCashBook_Click(object sender, EventArgs e)
        {
            CallAccountSummary((int)eMenuID.CashBook, "Cash Book", Properties.Resources.Cash_Book__report_1);
        }

        private void btnProfitAndLossSummary_Click(object sender, EventArgs e)
        {
            CallAccountSummary((int)eMenuID.ProfitAndLossAccount, "Profit and Loss", Properties.Resources.Profot___Loss_Report1);
        }

        private void btnBalanceSheetSummary_Click(object sender, EventArgs e)
        {
            CallAccountSummary((int)eMenuID.BalanceSheet, "Balance Sheet", Properties.Resources.Balancesheet1);
        }

        private void CallAccountsFormReport(eMenuID MenuID, string strHeader, object objICO)
        {
            frmAccountsFormReport objAccountsFormReport = null;
            try
            {
                objAccountsFormReport = new frmAccountsFormReport();
                objAccountsFormReport.Icon = ((System.Drawing.Icon)(objICO));
                objAccountsFormReport.TempMenuID = MenuID;
                objAccountsFormReport.Text = strHeader;
                objAccountsFormReport.MdiParent = this;
                objAccountsFormReport.WindowState = FormWindowState.Maximized;
                objAccountsFormReport.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objAccountsFormReport.Dispose();
                System.GC.Collect();
                CallAccountsFormReport(MenuID, strHeader, objICO);
            }
        }

        private void btnProfitnLoss_Click(object sender, EventArgs e)
        {
            CallAccountsFormReport(eMenuID.ProfitAndLossAccount, "Profit & Loss A/c", Properties.Resources.Profot___Loss_Report1);
        }

        private void btnBalancesheet_Click(object sender, EventArgs e)
        {
            CallAccountsFormReport(eMenuID.BalanceSheet, "Balance Sheet", Properties.Resources.Balancesheet1);
        }

        private void btnCashFlow_Click(object sender, EventArgs e)
        {
            CallAccountsFormReport(eMenuID.CashFlow, "Cash Flow", Properties.Resources.Cash_Flow__Report1);
        }

        private void btnFundFlow_Click(object sender, EventArgs e)
        {
            CallAccountsFormReport(eMenuID.FundFlow, "Fund Flow", Properties.Resources.Fund_flow_report1);
        }

        private void btnINcomestmt_Click(object sender, EventArgs e)
        {
            CallAccountsFormReport(eMenuID.IncomeStatement, "Income & Expenditure Statement", Properties.Resources.Income_Statement1);
        }

        private void btnStockSummary_Click(object sender, EventArgs e)
        {
            CallAccountsFormReport(eMenuID.StockSummary, "Stock Summary", Properties.Resources.Stock_Book2);
        }

        private void btnStatementofAccounts_Click(object sender, EventArgs e)
        {
            CallAccountsFormReport(eMenuID.StatementofAccounts, "Statement of Accounts", Properties.Resources.AccountsReports1);
        }

        private void btnAccSettings_Click(object sender, EventArgs e)
        {
            using (FrmAccountSettings objVendor = new FrmAccountSettings())
            {
                objVendor.ShowDialog();
            }
        }

        private void btnPayments_Click(object sender, EventArgs e)
        {
            using (frmReceiptsAndPayments objReceiptsAndPayments = new frmReceiptsAndPayments((int)eMenuID.Payment))
            {
                objReceiptsAndPayments.objFrmTradingMain = this;
                objReceiptsAndPayments.ShowDialog();
            }
        }

        private void btnReceipts_Click(object sender, EventArgs e)
        {
            using (frmReceiptsAndPayments objReceiptsAndPayments = new frmReceiptsAndPayments((int)eMenuID.Receipt))
            {
                objReceiptsAndPayments.objFrmTradingMain = this;
                objReceiptsAndPayments.ShowDialog();
            }
        }

        private void btnNewDocument_Click(object sender, EventArgs e)
        {
            using (frmDocumentMaster objDocumentMaster = new frmDocumentMaster())
            {
                objDocumentMaster.ShowDialog();
            }
        }

        private void CallStockRegister()
        {
            frmStockRegister objStockRegister = null;

            try
            {
                objStockRegister = new frmStockRegister();
                objStockRegister.Text = "Document Register";
                objStockRegister.MdiParent = this;
                objStockRegister.WindowState = FormWindowState.Maximized;
                objStockRegister.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objStockRegister.Dispose();
                System.GC.Collect();
                CallStockRegister();
            }
        }

        private void StockRegister_Click(object sender, EventArgs e)
        {
            CallStockRegister();
        }

        private void FrmMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F2)
            {
                using (frmCompanySelection objCompanySelection = new frmCompanySelection())
                {
                    objCompanySelection.ShowDialog();

                    if (this.HasChildren)
                    {
                        foreach (Form f in this.MdiChildren)
                            f.Close();
                    }

                    UpdateTitle();
                    CallDashBoard();
                }                
            }
        }

        private void btnVendorMapping_Click(object sender, EventArgs e)
        {
            using (FrmVendorMapping objVendorMapping = new FrmVendorMapping())
            {
                objVendorMapping.ShowDialog();
            }
        }

        private void btnPurExtraCharge_Click(object sender, EventArgs e)
        {
            using (frmExtraCharges objExtraCharge = new frmExtraCharges())
            {
                objExtraCharge.PintFormType = 1;
                objExtraCharge.ShowDialog();
            }
        }

        private void btnPurExtraChargePayment_Click(object sender, EventArgs e)
        {
            using (frmExtraChargePayment objExtraCharge = new frmExtraChargePayment())
            {
                objExtraCharge.PintFormType = 1;
                objExtraCharge.ShowDialog();
            }
        }

        private void btnSalesExtraCharge_Click(object sender, EventArgs e)
        {
            using (frmExtraCharges objExtraCharge = new frmExtraCharges())
            {
                objExtraCharge.PintFormType = 2;
                objExtraCharge.ShowDialog();
            }
        }

        private void btnSalesExtraChargePayment_Click(object sender, EventArgs e)
        {
            using (frmExtraChargePayment objExtraCharge = new frmExtraChargePayment())
            {
                objExtraCharge.PintFormType = 2;
                objExtraCharge.ShowDialog();
            }
        }

        private void btnExtraChargePayment_Click(object sender, EventArgs e)
        {
            using (frmExtraChargePayment objExtraCharge = new frmExtraChargePayment())
            {
                objExtraCharge.PintFormType = 0;
                objExtraCharge.ShowDialog();
            }
        }

        private void btnGroupJV_Click(object sender, EventArgs e)
        {
            CallGroupJV();
        }

        private void CallGroupJV()
        {
            frmGroupJV objfrmGroupJV = null;

            try
            {
                objfrmGroupJV = new frmGroupJV();
                objfrmGroupJV.Text = "Group Journal";
                objfrmGroupJV.MdiParent = this;
                objfrmGroupJV.WindowState = FormWindowState.Maximized;
                objfrmGroupJV.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objfrmGroupJV.Dispose();
                System.GC.Collect();
                CallGroupJV();
            }
        }

        private void btnProductionTemplate_Click(object sender, EventArgs e)
        {
            using (frmProductionTemplate objProductionTemplate = new frmProductionTemplate())
            {
                objProductionTemplate.ShowDialog();
            }
        }

        private void btnProductionProcess_Click(object sender, EventArgs e)
        {
            using (frmProductionProcess objProductionProcess = new frmProductionProcess())
            {
                objProductionProcess.ShowDialog();
            }
        }

        private void btnMaterialIssue_Click_1(object sender, EventArgs e)
        {
            using (FrmMaterialIssue objMaterialIssue = new FrmMaterialIssue())
            {
                objMaterialIssue.ShowDialog();
            }
        }

        private void Inventory_Click(object sender, EventArgs e)
        {

        }

        private void btnQC_Click(object sender, EventArgs e)
        {
            using (frmProductionQC objProductionQC = new frmProductionQC())
            {
                objProductionQC.ShowDialog();
            }
        }

        public void ShowDialogForm(Form f, bool disposeForm)
        {
            try
            {
                f.FormBorderStyle = FormBorderStyle.FixedSingle;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.MaximizeBox = false;
                f.ShowDialog();
            }
            finally
            {
                if (disposeForm)
                    f.Dispose();
            }
        }

        private void btnShift_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmShiftPolicy(), true);
        }

        private void btnWorkPolicy_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmWorkPolicy(), true);
        }

        private void btnLeavePolicy_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmLeavePolicy(), true);
        }

        private void btnCalendar_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmHolidayCalender(), true);
        }

        private void btnVacationPolicy_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new frmVacationPolicy(), true);
        }

        private void btnSettlementPolicy_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmSettlementPolicy(), true);
        }

        private void btnSalaryStructure_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmSalaryStructure(), true);
        }

        private void btnNewAttendance_Click(object sender, EventArgs e)
        {
            ShowAttendance();
        }

        private void ShowAttendance()
        {
            FrmAttendance objAttendance = null;
            try
            {
                objAttendance = new FrmAttendance();
                objAttendance.Text = "Attendance ";
                objAttendance.MdiParent = this;
                objAttendance.WindowState = FormWindowState.Maximized;
                objAttendance.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objAttendance.Dispose();
                System.GC.Collect();
                ShowAttendance();
            }
        }

        //private void btnProcess_Click(object sender, EventArgs e)
        //{
        //    Program.objMain.ShowDialogForm(new FrmSalaryProcess { objFrmPayrollMain = this }, true);
        //}

        //private void btnRelease_Click(object sender, EventArgs e)
        //{
        //    ShowRelease();
        //}

        //private void ShowRelease()
        //{
        //    frmSalaryRelease objSalaryRelease = null;

        //    try
        //    {
        //        objSalaryRelease = new frmSalaryRelease();
        //        objSalaryRelease.Text = "Salary Release";
        //        objSalaryRelease.MdiParent = this;
        //        objSalaryRelease.WindowState = FormWindowState.Maximized;
        //        objSalaryRelease.Show();
        //    }
        //    catch (OutOfMemoryException ex)
        //    {
        //        objSalaryRelease.Dispose();
        //        System.GC.Collect();
        //        ShowRelease();
        //    }
        //}

        private void btnSalaryAdvance_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmSalaryAdvance(), true);
        }

        private void btnVacationEntry_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmVacationEntry(), true);
        }

        private void btnSettlementEntry_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmSettlementEntry(), true);
        }

        private void btnEmployeeReport_Click(object sender, EventArgs e)
        {
            ShowEmployeeReport();
        }

        private void ShowEmployeeReport()
        {
            FrmRptEmployeeProfile objFrmRptEmployeeProfile = null;
            try
            {
                objFrmRptEmployeeProfile = new FrmRptEmployeeProfile();
                objFrmRptEmployeeProfile.Text = "Employee Profile Report";
                objFrmRptEmployeeProfile.MdiParent = this;
                objFrmRptEmployeeProfile.WindowState = FormWindowState.Maximized;
                objFrmRptEmployeeProfile.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptEmployeeProfile.Dispose();
                System.GC.Collect();
                ShowEmployeeReport();
            }
        }

        private void btnRptSalaryStructureReport_Click(object sender, EventArgs e)
        {
            ShowSalStructureReport();
        }

        private void ShowSalStructureReport()
        {
            FrmRptSalaryStructure objFrmRptSalaryStructure = null;
            try
            {
                objFrmRptSalaryStructure = new FrmRptSalaryStructure();
                objFrmRptSalaryStructure.Text = "Salary Structure Report";
                objFrmRptSalaryStructure.MdiParent = this;
                objFrmRptSalaryStructure.WindowState = FormWindowState.Maximized;
                objFrmRptSalaryStructure.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptSalaryStructure.Dispose();
                System.GC.Collect();
                ShowSalStructureReport();
            }
        }

        private void btnRptAttendance_Click(object sender, EventArgs e)
        {
            ShowAttendanceReport();
        }

        private void ShowAttendanceReport()
        {
            FrmAttendanceReport objAttendance = null;
            try
            {
                objAttendance = new FrmAttendanceReport();
                objAttendance.Text = "Attendance Report";
                objAttendance.MdiParent = this;
                objAttendance.WindowState = FormWindowState.Maximized;
                objAttendance.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objAttendance.Dispose();
                System.GC.Collect();
                ShowAttendanceReport();
            }
        }

        private void btnPaymentsReport_Click(object sender, EventArgs e)
        {
            ShowPaymetsReport();
        }

        private void ShowPaymetsReport()
        {
            FrmRptPayments objFrmRptPayments = null;
            try
            {
                objFrmRptPayments = new FrmRptPayments();
                objFrmRptPayments.Text = "Payments Report";
                objFrmRptPayments.MdiParent = this;
                objFrmRptPayments.WindowState = FormWindowState.Maximized;
                objFrmRptPayments.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptPayments.Dispose();
                System.GC.Collect();
                ShowPaymetsReport();
            }
        }

        private void btnLeaveSummaryReport_Click(object sender, EventArgs e)
        {
            ShowLeaveSummaryReport();
        }

        private void ShowLeaveSummaryReport()
        {
            FrmRptLeaveSummary objFrmRptLeaveSummary = null;
            try
            {
                objFrmRptLeaveSummary = new FrmRptLeaveSummary();
                objFrmRptLeaveSummary.Text = "Leave Report";
                objFrmRptLeaveSummary.MdiParent = this;
                objFrmRptLeaveSummary.WindowState = FormWindowState.Maximized;
                objFrmRptLeaveSummary.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptLeaveSummary.Dispose();
                System.GC.Collect();
                ShowLeaveSummaryReport();
            }
        }

        private void btnSalaryAdvanceReport_Click(object sender, EventArgs e)
        {
            ShowSalaryAdvance();
        }

        private void ShowSalaryAdvance()
        {
            FrmRptSalaryAdvance objSalaryAdvance = null;

            try
            {
                objSalaryAdvance = new FrmRptSalaryAdvance();
                objSalaryAdvance.Text = "Salary Advance Summary";
                objSalaryAdvance.MdiParent = this;
                objSalaryAdvance.WindowState = FormWindowState.Maximized;
                objSalaryAdvance.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objSalaryAdvance.Dispose();
                System.GC.Collect();
                ShowSalarySlip();
            }
        }

        private void btnPaySlip_Click(object sender, EventArgs e)
        {
            ShowSalarySlip();
        }

        private void ShowSalarySlip()
        {
            FrmSalarySlip objSalarySlip = null;

            try
            {
                objSalarySlip = new FrmSalarySlip();
                objSalarySlip.Text = "Pay Slip";
                objSalarySlip.MdiParent = this;
                objSalarySlip.WindowState = FormWindowState.Maximized;
                objSalarySlip.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objSalarySlip.Dispose();
                System.GC.Collect();
                ShowSalarySlip();
            }
        }
        public void CallSalaryRelease(int intSalaryProcessForm, object objICO)
        {
            FrmSalaryProcessRelease nwRelease = new FrmSalaryProcessRelease();
            try
            {
                //nwRelease.Icon = ((System.Drawing.Icon)(objICO));
                nwRelease.MdiParent = this;
                nwRelease.intSalaryProcessForm = intSalaryProcessForm;

                if (intSalaryProcessForm == 1)
                {

                        nwRelease.Text = "Salary Process " + this.MdiChildren.Length.ToString();
                }
                else
                {
                        nwRelease.Text = "Salary Release " + this.MdiChildren.Length.ToString();
                }
                nwRelease.WindowState = FormWindowState.Maximized;
                nwRelease.Show();
                nwRelease.Update();
            }
            catch (OutOfMemoryException ex)
            {
                nwRelease.Dispose();
                System.GC.Collect();
                CallSalaryRelease(intSalaryProcessForm, objICO);
            }
        }

        private void btnStockLedger_Click(object sender, EventArgs e)
        {
            CallAccountsFormReport(eMenuID.StockLedger, "Stock Ledger", Properties.Resources.Stock_Book2);
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            CallSalaryRelease(1, Properties.Resources.salarystructure);
        }

        private void btnRelease_Click(object sender, EventArgs e)
        {
            CallSalaryRelease(2, Properties.Resources.salarystructure);
        }

        private void btnLeaveEntry_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmLeaveEntry(), true);
        }
        private void ShowLeaveStructure()
        {
            FrmEmployeeLeaveStructure f = new FrmEmployeeLeaveStructure();
            try
            {

                f.FormBorderStyle = FormBorderStyle.FixedSingle;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.MaximizeBox = false;
                f.ShowDialog();
                if (f.PblnShowReport)
                {
                    ShowLeaveSummaryReport();
                }
            }
            finally
            {
                f.Dispose();
            }
        }
        private void btnLeaveStructure_Click(object sender, EventArgs e)
        {
            ShowLeaveStructure();
        }



       
    }
}