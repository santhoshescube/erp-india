﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevComponents.Editors;
using System.Windows.Forms.DataVisualization.Charting;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public partial class FrmDashBoard : DevComponents.DotNetBar.Office2007Form
    {
        clsBLLDashBoard MobjClsBLLDashBoard;
        int intMenuID = 1;

        public FrmDashBoard()
        {
            InitializeComponent();
            MobjClsBLLDashBoard = new clsBLLDashBoard();
        }

        private void FrmDashBoard_Load(object sender, EventArgs e)
        {
            try { LoadItems(); }
            catch { }
            try { DisplayDetails(); }
            catch { }
        }

        private void LoadItems()
        {
            cboItem.DataSource = MobjClsBLLDashBoard.getItems(Convert.ToDateTime(dtpMonthYear.Value.ToString("MMM yyyy")));
            cboItem.ValueMember = "ItemID";
            cboItem.DisplayMember = "ItemName";
        }

        private void DisplayDetails()
        {
            GetChartDetails();
            GetDetails();
            GetChartSummary();
            dgvDetails.ClearSelection();
            btnReceivablesAndPayables.Focus();

            switch (intMenuID)
            {
                case (1):
                    expDetails.TitleText = "Receivables && Payables Details";
                    break;
                case (2):
                    expDetails.TitleText = "Income && Expense Details";
                    break;
                case (3):
                    expDetails.TitleText = "Sales Details";
                    break;
                case (4):
                    expDetails.TitleText = "Purchase Details";
                    break;
                case (5):
                    expDetails.TitleText = "PDC Received Details";
                    break;
                case (6):
                    expDetails.TitleText = "PDC Issued Details";
                    break;
                case (7):
                    expDetails.TitleText = "Monthly Sales";
                    break;
                case (8):
                    expDetails.TitleText = "Product Movement";
                    break;
            }
        }

        private void dtpMonthYear_ValueChanged(object sender, EventArgs e)
        {
            LoadItems();
            DisplayDetails();
        }

        private void cboItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplayDetails();
        }

        private void cboChartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetChartDetails();
            GetChartSummary();
        }

        private void GetChartDetails()
        {
            try
            {
                DataTable datTemp = MobjClsBLLDashBoard.GetChartDetails(Convert.ToDateTime(dtpMonthYear.Value.ToString("MMM yyyy")), intMenuID,
                    cboItem.SelectedValue.ToInt32());

                if (datTemp != null)
                {
                    //if (datTemp.Rows.Count > 0)
                    //{
                        if (cboChartType.SelectedIndex >= 0)
                        {
                            if (cboChartType.SelectedIndex == 0) // Line
                                chartDetails.Series["Series1"].ChartType = SeriesChartType.SplineRange;
                            else if (cboChartType.SelectedIndex == 1) // Pie
                                chartDetails.Series["Series1"].ChartType = SeriesChartType.Pie;
                            else if (cboChartType.SelectedIndex == 2) // Column
                                chartDetails.Series["Series1"].ChartType = SeriesChartType.Column;
                            else if (cboChartType.SelectedIndex == 3) // Doughnut
                                chartDetails.Series["Series1"].ChartType = SeriesChartType.Doughnut;
                            else if (cboChartType.SelectedIndex == 4) // Bar
                                chartDetails.Series["Series1"].ChartType = SeriesChartType.Bar;
                            else if (cboChartType.SelectedIndex == 5) // Funnel
                                chartDetails.Series["Series1"].ChartType = SeriesChartType.Funnel;
                            else if (cboChartType.SelectedIndex == 6) // Pyramid
                                chartDetails.Series["Series1"].ChartType = SeriesChartType.Pyramid;
                        }

                        if (cboChartType.SelectedIndex == 0 || cboChartType.SelectedIndex == 2 || cboChartType.SelectedIndex == 4)
                            chartDetails.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
                        else
                        {
                            if (cboChartType.SelectedIndex == -1)
                                chartDetails.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
                            else
                                chartDetails.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                        }

                        chartDetails.Series["Series1"].IsValueShownAsLabel = true;
                        chartDetails.DataSource = datTemp;

                        if (intMenuID == 8)
                        {
                            chartDetails.Series["Series1"].XValueMember = "OperationType";
                            chartDetails.Series["Series1"].YValueMembers = "Amount";
                        }
                        else if (intMenuID == 7)
                        {
                            chartDetails.Series["Series1"].XValueMember = "VoucherDate";
                            chartDetails.Series["Series1"].YValueMembers = "Amount";
                        }
                        else
                        {
                            chartDetails.Series["Series1"].XValueMember = "CompanyName";
                            chartDetails.Series["Series1"].YValueMembers = "Amount";
                        }

                        switch (intMenuID)
                        {
                            case (1):
                                if (cboChartType.SelectedIndex < 0)
                                {
                                    chartDetails.Series["Series1"].ChartType = SeriesChartType.Column;
                                    chartDetails.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
                                }
                                break;
                            case (2):
                                if (cboChartType.SelectedIndex < 0)
                                {
                                    chartDetails.Series["Series1"].ChartType = SeriesChartType.Bar;
                                    chartDetails.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
                                }
                                break;
                            case (3):
                                if (cboChartType.SelectedIndex < 0)
                                {
                                    chartDetails.Series["Series1"].ChartType = SeriesChartType.Pyramid;
                                    chartDetails.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                                }
                                break;
                            case (4):
                                if (cboChartType.SelectedIndex < 0)
                                {
                                    chartDetails.Series["Series1"].ChartType = SeriesChartType.Pie;
                                    chartDetails.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                                }
                                break;
                            case (5):
                                if (cboChartType.SelectedIndex < 0)
                                {
                                    chartDetails.Series["Series1"].ChartType = SeriesChartType.Funnel;
                                    chartDetails.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                                }
                                break;
                            case (6):
                                if (cboChartType.SelectedIndex < 0)
                                {
                                    chartDetails.Series["Series1"].ChartType = SeriesChartType.Doughnut;
                                    chartDetails.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                                }
                                break;
                            case (7):
                                if (cboChartType.SelectedIndex < 0)
                                {
                                    chartDetails.Series["Series1"].ChartType = SeriesChartType.SplineRange;
                                    chartDetails.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
                                }
                                break;
                            case (8):
                                if (cboChartType.SelectedIndex < 0)
                                {
                                    chartDetails.Series["Series1"].ChartType = SeriesChartType.Doughnut;
                                    chartDetails.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                                }
                                break;
                        }

                        chartDetails.DataBind();
                    //}
                }
            }
            catch { }
        }

        private void GetChartSummary()
        {
            try
            {
                DataTable datTemp = MobjClsBLLDashBoard.GetChartSummary(Convert.ToDateTime(dtpMonthYear.Value.ToString("MMM yyyy")),
                    intMenuID, cboItem.SelectedValue.ToInt32());

                if (datTemp != null)
                {
                    //if (datTemp.Rows.Count > 0)
                    //{
                        if (cboChartTypeSummary.SelectedIndex >= 0)
                        {
                            if (cboChartTypeSummary.SelectedIndex == 0) // Line
                                chartSummary.Series["Series1"].ChartType = SeriesChartType.SplineRange;
                            else if (cboChartTypeSummary.SelectedIndex == 1) // Pie
                                chartSummary.Series["Series1"].ChartType = SeriesChartType.Pie;
                            else if (cboChartTypeSummary.SelectedIndex == 2) // Column
                                chartSummary.Series["Series1"].ChartType = SeriesChartType.Column;
                            else if (cboChartTypeSummary.SelectedIndex == 3) // Doughnut
                                chartSummary.Series["Series1"].ChartType = SeriesChartType.Doughnut;
                            else if (cboChartTypeSummary.SelectedIndex == 4) // Bar
                                chartSummary.Series["Series1"].ChartType = SeriesChartType.Bar;
                            else if (cboChartTypeSummary.SelectedIndex == 5) // Funnel
                                chartSummary.Series["Series1"].ChartType = SeriesChartType.Funnel;
                            else if (cboChartTypeSummary.SelectedIndex == 6) // Pyramid
                                chartSummary.Series["Series1"].ChartType = SeriesChartType.Pyramid;
                        }

                        if (cboChartTypeSummary.SelectedIndex == 0 || cboChartTypeSummary.SelectedIndex == 2 || cboChartTypeSummary.SelectedIndex == 4)
                            chartSummary.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
                        else
                        {
                            if (cboChartTypeSummary.SelectedIndex == -1)
                                chartSummary.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
                            else
                                chartSummary.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                        }

                        chartSummary.Series["Series1"].IsValueShownAsLabel = true;
                        chartSummary.DataSource = datTemp;
                        chartSummary.Series["Series1"].XValueMember = "ReferenceValue";
                        chartSummary.Series["Series1"].YValueMembers = "Amount";

                        switch (intMenuID)
                        {
                            case (1):
                                if (cboChartTypeSummary.SelectedIndex < 0)
                                {
                                    chartSummary.Series["Series1"].ChartType = SeriesChartType.Doughnut;
                                    chartSummary.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                                }
                                break;
                            case (2):
                                if (cboChartTypeSummary.SelectedIndex < 0)
                                {
                                    chartSummary.Series["Series1"].ChartType = SeriesChartType.Pie;
                                    chartSummary.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                                }
                                break;
                            case (3):
                                if (cboChartTypeSummary.SelectedIndex < 0)
                                {
                                    chartSummary.Series["Series1"].ChartType = SeriesChartType.Funnel;
                                    chartSummary.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                                }
                                break;
                            case (4):
                                if (cboChartTypeSummary.SelectedIndex < 0)
                                {
                                    chartSummary.Series["Series1"].ChartType = SeriesChartType.Pyramid;
                                    chartSummary.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                                }
                                break;
                            case (5):
                                if (cboChartTypeSummary.SelectedIndex < 0)
                                {
                                    chartSummary.Series["Series1"].ChartType = SeriesChartType.Column;
                                    chartSummary.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
                                }
                                break;
                            case (6):
                                if (cboChartTypeSummary.SelectedIndex < 0)
                                {
                                    chartSummary.Series["Series1"].ChartType = SeriesChartType.Bar;
                                    chartSummary.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
                                }
                                break;
                            case (7):
                                if (cboChartTypeSummary.SelectedIndex < 0)
                                {
                                    chartSummary.Series["Series1"].ChartType = SeriesChartType.Pyramid;
                                    chartSummary.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                                }
                                break;
                            case (8):
                                if (cboChartTypeSummary.SelectedIndex < 0)
                                {
                                    chartSummary.Series["Series1"].ChartType = SeriesChartType.Bar;
                                    chartSummary.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
                                }
                                break;
                        }

                        chartSummary.DataBind();
                    //}
                }
            }
            catch { }
        }

        private void GetDetails()
        {
            try
            {
                DataTable datTemp = MobjClsBLLDashBoard.GetDetails(Convert.ToDateTime(dtpMonthYear.Value.ToString("MMM yyyy")),
                    intMenuID, 0);
                dgvDetails.DataSource = null;
                dgvDetails.DataSource = datTemp;

                for (int iCounter = 0; iCounter <= dgvDetails.Columns.Count - 1; iCounter++)
                    dgvDetails.Columns[iCounter].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                dgvDetails.ClearSelection();
            }
            catch { }
        }

        private void btnReceivablesAndPayables_Click(object sender, EventArgs e)
        {
            intMenuID = 1;
            DisplayDetails();
        }

        private void btnIncomeExpense_Click(object sender, EventArgs e)
        {
            intMenuID = 2;
            DisplayDetails();
        }

        private void btnSales_Click(object sender, EventArgs e)
        {
            intMenuID = 3;
            DisplayDetails();
        }

        private void btnPurchase_Click(object sender, EventArgs e)
        {
            intMenuID = 4;
            DisplayDetails();
        }

        private void btnPDCReceived_Click(object sender, EventArgs e)
        {
            intMenuID = 5;
            DisplayDetails();
        }

        private void btnPDCIssued_Click(object sender, EventArgs e)
        {
            intMenuID = 6;
            DisplayDetails();
        }

        private void btnItemwiseSalesProfit_Click(object sender, EventArgs e)
        {
            intMenuID = 7;
            DisplayDetails();
        }

        private void btnProductMovement_Click(object sender, EventArgs e)
        {
            intMenuID = 8;
            DisplayDetails();
        }

        private void dgvDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private void cboItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboItem.DroppedDown = false;
        }
    }
}