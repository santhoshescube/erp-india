﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    class clsInitialSettings
    {
        public string EncryptValue(string argmacid)
        {
            string strencryString = "";
            for (int i = 0; i <= argmacid.Length - 1; i++)
            {
                strencryString = strencryString + EncryptConvertion(argmacid.Substring(i, 1).ToUpper());
            }
            return strencryString;
        }

        public string DecryptValue(string argmacid)
        {
            string strencryString = "";
            for (int i = 0; i <= argmacid.Length - 1; i++)
            {
                strencryString = strencryString + DecryptConvertion(argmacid.Substring(i, 1).ToUpper());
            }
            return strencryString;
        }

        private string EncryptConvertion(string argchr)
        {
            switch (argchr)
            {
                case "1":
                    return "A";
                case "2":
                    return "I";
                case "3":
                    return "C";
                case "4":
                    return "Z";
                case "5":
                    return "Y";
                case "6":
                    return "X";
                case "7":
                    return "D";
                case "8":
                    return "E";
                case "9":
                    return "F";
                case "0":
                    return "G";
                case "A":
                    return "V";
                case "B":
                    return "T";
                case "C":
                    return "Q";
                case "D":
                    return "U";
                case "E":
                    return "H";
                case "F":
                    return "B";
                case "G":
                    return "J";
                case "H":
                    return "K";
                case "I":
                    return "L";
                case "J":
                    return "M";
                case "K":
                    return "N";
                case "L":
                    return "9";
                case "M":
                    return "P";
                case "N":
                    return "W";
                case "O":
                    return "R";
                case "P":
                    return "S";
                case "Q":
                    return "0";
                case "R":
                    return "1";
                case "S":
                    return "2";
                case "T":
                    return "3";
                case "U":
                    return "4";
                case "V":
                    return "5";
                case "W":
                    return "6";
                case "X":
                    return "7";
                case "Y":
                    return "8";
                case "Z":
                    return "O";

            }

            return "";

        }

        private string DecryptConvertion(string argchr)
        {

            switch (argchr)
            {
                case "A":
                    return "1";
                case "B":
                    return "F";
                case "C":
                    return "3";
                case "Z":
                    return "4";
                case "Y":
                    return "5";
                case "X":
                    return "6";
                case "D":
                    return "7";
                case "E":
                    return "8";
                case "F":
                    return "9";
                case "G":
                    return "0";
                case "V":
                    return "A";
                case "T":
                    return "B";
                case "W":
                    return "N";
                case "U":
                    return "D";
                case "H":
                    return "E";
                case "I":
                    return "2";
                case "J":
                    return "G";
                case "K":
                    return "H";
                case "L":
                    return "I";
                case "M":
                    return "J";
                case "N":
                    return "K";
                case "O":
                    return "Z";
                case "P":
                    return "M";
                case "Q":
                    return "C";
                case "R":
                    return "O";
                case "S":
                    return "P";
                case "0":
                    return "Q";
                case "1":
                    return "R";
                case "2":
                    return "S";
                case "3":
                    return "T";
                case "4":
                    return "U";
                case "5":
                    return "V";
                case "6":
                    return "W";
                case "7":
                    return "X";
                case "8":
                    return "Y";
                case "9":
                    return "L";

            }
            return "";
        }
    }
}
