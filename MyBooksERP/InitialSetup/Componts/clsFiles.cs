﻿using System;
using System.Data;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Runtime;
using System.Resources;
using System.Runtime.InteropServices;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using System.IO;


namespace MyBooksERP
{
    class clsFiles
    {
        public bool Checksysfile(string strPath)
        {
            string flName = Path.Combine(strPath, "ProjectSettings.dll");
            FileInfo objfile = new FileInfo(flName);
            if(objfile.Exists)
            {
                if (objfile.Length != 43008)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool CheckProductFile()
        {
            string strFileName = System.Environment.SystemDirectory + "\\mbhm2oy9ar_xds.dll";
            
            FileInfo fInfo = new FileInfo(strFileName);
            if (fInfo.Exists)
                return true;
            else
                return false;

        }

        public bool CheckValidProduct(string dtMaxExtend)
        {
            string strRegPath = "TechAsuskr20";
               //mpf

            RegClass objRegistry = new RegClass();
            clsInitialSettings objInitial = new clsInitialSettings();
            string strProductValue = "";
            string strRegKey = "";
            DateTime dExpDate = DateTime.Today.Date;
            string sMnth = "";
            int nTimes = 0;

            if (objRegistry.ReadFromRegistry("SOFTWARE\\Microsoft\\" + strRegPath, "Sintelanpbmno", out strRegKey))
            {
                strProductValue = objInitial.DecryptValue(strRegKey);
                strProductValue = objInitial.DecryptValue(strProductValue);

                dExpDate = Convert.ToDateTime(strProductValue.Substring(0, 2) + "-" + strProductValue.Substring(6, 3) + "-" + strProductValue.Substring(9, 4));
                nTimes = Convert.ToInt32(strProductValue.Substring(2, 4));
                sMnth = strProductValue.Substring(6, 3);

                if (dExpDate < Convert.ToDateTime(dtMaxExtend))
                {
                    nTimes = 30;
                    dExpDate = Convert.ToDateTime(dtMaxExtend);
                }

                if (nTimes <= 0 || DateValid(dtMaxExtend) < 0)
                {
                    nTimes = 0;
                    strRegKey = dExpDate.Day.ToString("00") + nTimes.ToString("0000") + sMnth + dExpDate.Year.ToString("0000");
                    UpdateRegistry(strRegPath, strRegKey);
                    return false;
                }
                else
                {
                    nTimes -= 1;
                    strRegKey = dExpDate.Day.ToString("00") + nTimes.ToString("0000") + sMnth + dExpDate.Year.ToString("0000");
                    strProductValue = objInitial.EncryptValue(strRegKey);
                    object objKval = objInitial.EncryptValue(strProductValue);

                    objRegistry.WriteToRegistry("SOFTWARE\\Microsoft\\" + strRegPath, strRegPath, "Sintelanpbmno", objKval);
                    return true;
                }
            }
            else
            {
                if (DateValid(dtMaxExtend) < 0)
                {
                    nTimes = 0;
                    strRegKey = dExpDate.Day.ToString("00") + nTimes.ToString("0000") + sMnth + dExpDate.Year.ToString("0000");
                    UpdateRegistry(strRegPath, strRegKey);
                    return false;
                }
                else
                    return true;
            }

        }

        public bool CheckProductMaxUsage(string dtMaxExtend)
        {
            string strRegPath = "TechAsuskr20";
            RegClass objRegistry = new RegClass();
            clsInitialSettings objInitial = new clsInitialSettings();
            string strProductValue = "";
            string strRegKey = "";
            DateTime dExpDate = DateTime.Today.Date;
            string sMnth = "";
            int nTimes = 0;

            if (objRegistry.ReadFromRegistry("SOFTWARE\\Microsoft\\" + strRegPath, "Sintelanpbmno", out strRegKey))
            {
                strProductValue = objInitial.DecryptValue(strRegKey);
                strProductValue = objInitial.DecryptValue(strProductValue);

                dExpDate = Convert.ToDateTime(strProductValue.Substring(0, 2) + "-" + strProductValue.Substring(6, 3) + "-" + strProductValue.Substring(9, 4));
                nTimes = Convert.ToInt32(strProductValue.Substring(2, 4));
                sMnth = strProductValue.Substring(6, 3);

                if (dExpDate < Convert.ToDateTime(dtMaxExtend))
                {
                    nTimes = 30;
                    dExpDate = Convert.ToDateTime(dtMaxExtend);
                }

                if (DateValid(dtMaxExtend) < 0)  //nTimes <= 0 ||
                {
                    nTimes = 0;
                    strRegKey = dExpDate.Day.ToString("00") + nTimes.ToString("0000") + sMnth + dExpDate.Year.ToString("0000");
                    UpdateRegistry(strRegPath, strRegKey);
                    return false;
                }
                else
                    return true;

            }
            else
            {
                if (DateValid(dtMaxExtend) < 0)
                {
                    nTimes = 0;
                    strRegKey = dExpDate.Day.ToString("00") + nTimes.ToString("0000") + sMnth + dExpDate.Year.ToString("0000");
                    UpdateRegistry(strRegPath, strRegKey);
                    return false;
                }
                else
                    return true;
            }



        }
        private int DateValid(string strDate)
        {
            int iSts = 0;
            try
            {
                iSts = new DataLayer().ExecuteScalar("Select datediff(day,getdate(),N'" + strDate + "')").ToInt32();
            }
            catch (Exception)
            { }
            return iSts;
        }
        private void UpdateRegistry(string sPath, string sKey)
        {
            try
            {
                clsInitialSettings objInitialSet = new clsInitialSettings();
                string strValue = objInitialSet.EncryptValue(sKey);
                object objVal = objInitialSet.EncryptValue(strValue);
                new RegClass().WriteToRegistry("SOFTWARE\\Microsoft\\" + sPath, sPath, "Sintelanpbmno", objVal);
            }
            catch (Exception)
            { return; }
        }

        public bool UpdateRegistryInitial(int iDays)
        {
            string strRegPath = "TechAsuskr20";
            RegClass objRegistry = new RegClass();
            clsInitialSettings objInitial = new clsInitialSettings();
            string strProductValue = "";
            string strRegKey = "";
            DateTime dExpDate;
            string sMnth = "";
            int nTimes = 0;

            objRegistry.ReadFromRegistry("SOFTWARE\\Microsoft\\" + strRegPath, "Sintelanpbmno", out strRegKey);
            if (strRegKey != "")
            {
                strProductValue = objInitial.DecryptValue(strRegKey);
                strProductValue = objInitial.DecryptValue(strProductValue);

                dExpDate = Convert.ToDateTime(strProductValue.Substring(0, 2) + "-" + strProductValue.Substring(6, 3) + "-" + strProductValue.Substring(9, 4));
                nTimes = Convert.ToInt32(strProductValue.Substring(2, 4));
                sMnth = strProductValue.Substring(6, 3);
            }
            else
            {
                dExpDate = DateTime.Now.Date.AddDays(iDays);
                nTimes = iDays * 16;
                sMnth = GetMonth(dExpDate.Month);
            }

            strRegKey = dExpDate.Day.ToString("00") + nTimes.ToString("0000") + sMnth + dExpDate.Year.ToString("0000");
            strProductValue = objInitial.EncryptValue(strRegKey);
            object objKval = objInitial.EncryptValue(strProductValue);

            objRegistry.WriteToRegistry("SOFTWARE\\Microsoft\\" + strRegPath, strRegPath, "Sintelanpbmno", objKval);
            return true;

        }

        private string GetMonth(int mn)
        {
            string mnth = "JAN";
            switch (mn)
            {
                case 1:
                    mnth = "JAN";
                    break;
                case 2:
                    mnth = "FEB";
                    break;
                case 3:
                    mnth = "MAR";
                    break;
                case 4:
                    mnth = "APR";
                    break;
                case 5:
                    mnth = "MAY";
                    break;
                case 6:
                    mnth = "JUN";
                    break;
                case 7:
                    mnth = "JUL";
                    break;
                case 8:
                    mnth = "AUG";
                    break;
                case 9:
                    mnth = "SEP";
                    break;
                case 10:
                    mnth = "OCT";
                    break;
                case 11:
                    mnth = "NOV";
                    break;
                case 12:
                    mnth = "DEC";
                    break;
            }
            return mnth;
        }



    }

    public class clsConfig
    {

        private string GetLocalIP()
        {
            string ipAddress = "";
            try
            {
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                {
                    IPAddress[] ipAddr = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
                    for (int i = 0; i < ipAddr.Length; i++)
                    {
                        if (ipAddr[i].AddressFamily != System.Net.Sockets.AddressFamily.InterNetworkV6)
                        {
                            if (!IPAddress.IsLoopback(ipAddr[i]))
                            {
                                ipAddress = ipAddr[i].ToString();
                                break;
                            }
                        }
                    }
                }

                return ipAddress;
            }
            catch (Exception)
            {
                return ipAddress;
            }
        }

        private string GetExtIP()
        {
            string extIP = "";
            try
            {
                extIP = new WebClient().DownloadString("http://wanip.info/");
                extIP = new WebClient().DownloadString("http://checkip.dyndns.org/");
                extIP = new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}").Matches(extIP)[0].ToString();
                return extIP;
            }
            catch (Exception)
            {
                return extIP;
            }
        }

       
     


    }

    class Archlist
    {
        public static string GLSlno { get { return "65186395"; } }
    }

}
