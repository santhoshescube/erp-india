﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace MyBooksERP
{
    class clsSettings
    {
        RegClass objRegistry;
        MachineSettings objMHSettings;
        public clsSettings()
        {
            objMHSettings = new MachineSettings();
            objRegistry = new RegClass();
        }

        public bool CheckActivation()
        {
            string strMhNumber = objMHSettings.GetMacNumber().Trim();
            string strRegSlNo = "";

            if (GetProductSerialNo(out strRegSlNo))
            {
                string regActKey = GetRegistryActKey().Trim();
                if (regActKey != "")
                {
                    string strMacid = DecryptCrypto(regActKey);

                    strMacid = Decrypt(strMacid);

                    string strRegPassKey = GetRegistrykeyNo();
                    if (strRegPassKey == "")
                        return false;

                    if (strMhNumber == strMacid)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            else
                return false;

        }

        public bool GetProductSerialNo(out string strSerialNo)
        {
            strSerialNo = "";
            return objRegistry.ReadFromRegistry("SOFTWARE\\Mindsoft\\BooksErpSlNo", "MsBooksErpSlNo", out strSerialNo);
        }

        private string GetRegistryActKey()
        {
            string strActKey = "";
            objRegistry.ReadFromRegistry("SOFTWARE\\MyBkerpActivation", "MsBookerpAct", out strActKey);
            return strActKey;
        }

        public string GetRegistrykeyNo()
        {
            string strRegistryNo = "";
            objRegistry.ReadFromRegistry("SOFTWARE\\Microsoft\\Aemrdptance", "Aemrdptanceno", out strRegistryNo);
            return strRegistryNo;
        }

        public bool CreateRegistrykeyNo(string strKey)
        {
            if (strKey.Length == 4)
            {
                Random objRan = new Random();
                string sNo = objRan.Next(100, 999).ToString();
                sNo = Encrypt(sNo);
                strKey = strKey + sNo;
            }
            object objKeyval = strKey;
            return objRegistry.WriteToRegistry("SOFTWARE\\Microsoft\\Aemrdptance", "Aemrdptance", "Aemrdptanceno", objKeyval);
                    

            
        }

        public string GetRequestKey(string strSerialNo)
        {
            string RequestNumber = "";
            string strMacNo = objMHSettings.GetMacNumber();
            string strEncrymac = Encrypt(strMacNo);
            RequestNumber = Encrypt(strSerialNo.Substring(0, 4)) + strEncrymac + Encrypt(strSerialNo.Substring(4, 4));
            return RequestNumber;
        }

        public string Encrypt(string argmacid)
        {
            string strencryString = "";
            for (int i = 0; i <= argmacid.Length - 1; i++)
            {
                strencryString = strencryString + EncryptConvertion(argmacid.Substring(i, 1).ToUpper());
            }
            return strencryString;
        }

        public string Decrypt(string argmacid)
        {
            string strencryString = "";
            for (int i = 0; i <= argmacid.Length - 1; i++)
            {
                strencryString = strencryString + DecryptConvertion(argmacid.Substring(i, 1).ToUpper());
            }
            return strencryString;
        }

        private string EncryptConvertion(string argchr)
        {
            switch (argchr)
            {
                case "1":
                    return "A";
                case "2":
                    return "B";
                case "3":
                    return "C";
                case "4":
                    return "Z";
                case "5":
                    return "Y";
                case "6":
                    return "X";
                case "7":
                    return "D";
                case "8":
                    return "E";
                case "9":
                    return "F";
                case "0":
                    return "G";
                case "A":
                    return "V";
                case "B":
                    return "T";
                case "C":
                    return "W";
                case "D":
                    return "U";
                case "E":
                    return "H";
                case "F":
                    return "I";
                case "G":
                    return "J";
                case "H":
                    return "K";
                case "I":
                    return "L";
                case "J":
                    return "M";
                case "K":
                    return "N";
                case "L":
                    return "O";
                case "M":
                    return "P";
                case "N":
                    return "Q";
                case "O":
                    return "R";
                case "P":
                    return "S";
                case "Q":
                    return "0";
                case "R":
                    return "1";
                case "S":
                    return "2";
                case "T":
                    return "3";
                case "U":
                    return "4";
                case "V":
                    return "5";
                case "W":
                    return "6";
                case "X":
                    return "7";
                case "Y":
                    return "8";
                case "Z":
                    return "9";

            }
            return "";

        }

        private string DecryptConvertion(string argchr)
        {

            switch (argchr)
            {
                case "A":
                    return "1";
                case "B":
                    return "2";
                case "C":
                    return "3";
                case "Z":
                    return "4";
                case "Y":
                    return "5";
                case "X":
                    return "6";
                case "D":
                    return "7";
                case "E":
                    return "8";
                case "F":
                    return "9";
                case "G":
                    return "0";
                case "V":
                    return "A";
                case "T":
                    return "B";
                case "W":
                    return "C";
                case "U":
                    return "D";
                case "H":
                    return "E";
                case "I":
                    return "F";
                case "J":
                    return "G";
                case "K":
                    return "H";
                case "L":
                    return "I";
                case "M":
                    return "J";
                case "N":
                    return "K";
                case "O":
                    return "L";
                case "P":
                    return "M";
                case "Q":
                    return "N";
                case "R":
                    return "O";
                case "S":
                    return "P";
                case "0":
                    return "Q";
                case "1":
                    return "R";
                case "2":
                    return "S";
                case "3":
                    return "T";
                case "4":
                    return "U";
                case "5":
                    return "V";
                case "6":
                    return "W";
                case "7":
                    return "X";
                case "8":
                    return "Y";
                case "9":
                    return "Z";

            }
            return "";
        }

        public static string EncryptCrypto(string strText)
        {
            string strEncrKey = "Jesus-Itrust-Inyou";
            byte[] byKey = null;
            byte[] IV = { 0X12, 0X34, 0X56, 0X78, 0X90, 0XAB, 0XCD, 0XEF };

            try
            {
                byKey = System.Text.Encoding.UTF8.GetBytes(strEncrKey.Substring(0, 8));

                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = Encoding.UTF8.GetBytes(strText);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());

            }
            catch (Exception)
            {
                return strText;
            }
        }

        public static string DecryptCrypto(string strText)
        {
            string sDecrKey = "Jesus-Itrust-Inyou";
            byte[] byKey = null;
            byte[] IV = { 0X12, 0X34, 0X56, 0X78, 0X90, 0XAB, 0XCD, 0XEF };
            byte[] inputByteArray = new byte[strText.Length + 1];

            try
            {
                byKey = System.Text.Encoding.UTF8.GetBytes(sDecrKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(strText);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);

                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;

                return encoding.GetString(ms.ToArray());

            }
            catch (Exception)
            {
                return strText;
            }
        }

    }

    static class Productval
    {
        public static int EmpLimit { get { return 200; } }
        public static int CmpLimit { get { return 1; } }
        public static int BranchLimit { get { return 6; } }
        public static int ItmLimit { get { return 5000; } }
    }
}
