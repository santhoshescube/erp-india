﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Security.Cryptography;
using System.IO;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MyBooksERP
{
    class clsBulksetup
    {
        clsSendmail objNet;

        public clsBulksetup()
        {
            objNet = new clsSendmail();
        }

        private bool CheckNetConnection()
        {
            return objNet.IsInternet_Connected();
        }

        public void ProductdbStatus()
        {
            try
            {
                UpdateDb();
                string mnSl = "0";
                DataLayer objCon = new DataLayer();
                string sqlS = "Select MnuID,MnuType,Description from MenuTypes";
                DataTable dtS = objCon.ExecuteDataTable(sqlS);
                if (dtS.Rows.Count > 0)
                {
                    //mnSl = clsSettings.DecryptCrypto(Convert.ToString(dtS.Rows[0]["Description"]));
                    //if (mnSl != Archlist.GLSlno)
                    //    ClsMainSettings.blnProductStatus = false;

                    string mnuTy = clsSettings.DecryptCrypto(Convert.ToString(dtS.Rows[0]["MnuType"]));
                    if (objCon.ExecuteScalar("Select datediff(day,getdate(),N'" + mnuTy + "')").ToInt32() < 0)
                        ClsMainSettings.blnProductStatus = false;
                }
                else
                {
                    string mnT = clsSettings.EncryptCrypto(DateTime.Parse("30 Mar 2018").ToShortDateString());
                    mnSl = clsSettings.EncryptCrypto(ClsMainSettings.strProductSerial);
                    ArrayList prmDbs = new ArrayList();
                    prmDbs.Add(new SqlParameter("@mnType", mnT));
                    prmDbs.Add(new SqlParameter("@mnDes", mnSl));
                    sqlS = "INSERT INTO MenuTypes ([MnuID],[MnuType],[MnuDate],[CreatedBy],[Description]) VALUES (1,@mnType,GETDATE(),1,@mnDes)";
                    objCon.ExecuteNonQuery(prmDbs, sqlS);
                   
                }
            }
            catch (Exception)
            {
                ClsMainSettings.blnProductStatus = false;
            }
        }

        private void UpdateDb()
        {
            string sqlQ = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MenuTypes]') AND type in (N'U')) " +
                        "BEGIN CREATE TABLE [dbo].[MenuTypes]([MnuID] [int] NULL,[MnuType] [varchar](100) NULL," +
                        "[MnuDate] [datetime] NOT NULL CONSTRAINT [DF_MenuTypes_MnuDate]  DEFAULT (getdate())," +
                        "[CreatedBy] [int] NULL,[Description] [varchar](100) NULL) ON [PRIMARY] END";
            new DataLayer().ExecuteScalar(sqlQ);
        }

        public short GetProductStatus()
        {
            short sBlockStatus = 0;
            clsSettings objSettings = new clsSettings();
            string sKeyVal = objSettings.GetRegistrykeyNo();
            sKeyVal = sKeyVal.Remove(0, 4).Trim ();
            sKeyVal = objSettings.Decrypt(sKeyVal);
            if (sKeyVal == "9B5" || sKeyVal == "")
                sBlockStatus = 1;

            return sBlockStatus;
        }

       

        private void SetPrdB(short bStatus, int iPrd)
        {
            try
            {
                clsSettings objSettings = new clsSettings ();
                //update registry
                if (bStatus==1)
                {
                    string sNo = "9B5";
                    sNo = objSettings.Encrypt(sNo);
                    string sKeyVal = objSettings.GetRegistrykeyNo();
                    if (sKeyVal.Length > 4)
                        sKeyVal = sKeyVal.Substring(0, 4);

                    sKeyVal = sKeyVal + sNo;
                    objSettings.CreateRegistrykeyNo(sKeyVal);
                }
                else
                {
                    Random objRan = new Random();
                    string sNo = objRan.Next(100, 999).ToString();
                    sNo = objSettings.Encrypt(sNo);
                    string sKeyVal = objSettings.GetRegistrykeyNo();
                    if (sKeyVal.Length > 4)
                        sKeyVal = sKeyVal.Substring(0, 4);

                    sKeyVal = sKeyVal + sNo;
                    objSettings.CreateRegistrykeyNo(sKeyVal);
                }
            }
            catch (Exception)
            {

            }
        }

       

        public static string EncryptCrypto(string strText)
        {
            string strEncrKey = "Belives-in-God";
            byte[] byKey = null;
            byte[] IV = { 0X12, 0X34, 0X56, 0X78, 0X90, 0XAB, 0XCD, 0XEF };

            try
            {
                byKey = System.Text.Encoding.UTF8.GetBytes(strEncrKey.Substring(0, 8));

                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = Encoding.UTF8.GetBytes(strText);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());

            }
            catch (Exception)
            {
                return strText;
            }
        }

        public static string DecryptCrypto(string strText)
        {
            string sDecrKey = "Belives-in-God";
            byte[] byKey = null;
            byte[] IV = { 0X12, 0X34, 0X56, 0X78, 0X90, 0XAB, 0XCD, 0XEF };
            byte[] inputByteArray = new byte[strText.Length + 1];

            try
            {
                byKey = System.Text.Encoding.UTF8.GetBytes(sDecrKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(strText);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);

                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;

                return encoding.GetString(ms.ToArray());

            }
            catch (Exception)
            {
                return strText;
            }
        }

      

        private string GetLocalIP()
        {
            string ipAddress = "";
            try
            {
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                {
                    IPAddress[] ipAddr = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
                    for (int i = 0; i < ipAddr.Length; i++)
                    {
                        if (ipAddr[i].AddressFamily != System.Net.Sockets.AddressFamily.InterNetworkV6)
                        {
                            if (!IPAddress.IsLoopback(ipAddr[i]))
                            {
                                ipAddress = ipAddr[i].ToString();
                                break;
                            }
                        }
                    }
                }

                return ipAddress;
            }
            catch (Exception)
            {
                return ipAddress;
            }
        }

        private string GetExtIP()
        {
            string extIP = "";
            try
            {
                extIP = new WebClient().DownloadString("http://wanip.info/");
                extIP = new WebClient().DownloadString("http://checkip.dyndns.org/");
                extIP = new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}").Matches(extIP)[0].ToString();
                if (extIP.Length > 30)
                    extIP = extIP.Substring(0, 30);

                return extIP;
            }
            catch (Exception)
            {
                return extIP;
            }
        }

    }

    static class MsiPorts
    {
        public static string MsIPPort { get; set; }

        public static void SetPorts()
        {
            
        }

        static void wb_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                WebBrowser wb = sender as WebBrowser;
                string sPorts = wb.DocumentText;
                if (sPorts.Trim() != "")
                {
                    sPorts = sPorts.Remove(0, sPorts.IndexOf("<PRE>")).Replace("<PRE>", "").Trim();
                    sPorts = sPorts.Substring(0, sPorts.IndexOf("</PRE>")).Trim();
                }

                if (sPorts.Length > 16 && sPorts.Length < 22)
                    MsIPPort = sPorts;
            }
            catch (Exception)
            {

            }
        }

    }

}
