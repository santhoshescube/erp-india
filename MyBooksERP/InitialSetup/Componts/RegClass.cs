﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Security;
using System.Security.Permissions;
using System.Security.AccessControl;
using System.Security.Principal;

namespace MyBooksERP
{
    public class RegClass
    {
        public bool ReadFromRegistry(string sMainKey, string sKeyToread, out string rVal)
        {
            rVal = "";
            try
            {
                RegistryKey rKey = Registry.LocalMachine;
                RegistryKey subKey = rKey.OpenSubKey(sMainKey, RegistryKeyPermissionCheck.ReadSubTree, System.Security.AccessControl.RegistryRights.ReadKey);
                if (subKey != null)
                {
                    rVal = subKey.GetValue(sKeyToread).ToString();
                }
                if (rVal == "")
                    return false;
                else
                    return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool WriteToRegistry(string ParentKey, string sKeyname, string subkeyname, object subkeyval)
        {
            try
            {
                //Get 'Everyone' account
                System.Security.Principal.SecurityIdentifier sid = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.WorldSid, null);
                System.Security.Principal.NTAccount acct = sid.Translate(typeof(System.Security.Principal.NTAccount)) as System.Security.Principal.NTAccount;
                string strEveryoneAccount = acct.ToString();

                //Get parent key in HKLM
                RegistryKey keySLP = default(RegistryKey);
                RegistrySecurity rs = default(RegistrySecurity);
                RegistryKey rkey = Registry.LocalMachine.OpenSubKey(ParentKey, true);
                if (rkey == null)
                {
                    ParentKey = ParentKey.Substring(0, ParentKey.LastIndexOf("\\"));
                    rkey = Registry.LocalMachine.OpenSubKey(ParentKey, true);
                    keySLP = rkey.CreateSubKey(sKeyname);
                    keySLP.SetValue(subkeyname, subkeyval);
                    rs = rkey.GetAccessControl(AccessControlSections.Access);
                    rs.AddAccessRule(new RegistryAccessRule(strEveryoneAccount, RegistryRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow));
                    //Apply access control definition
                    keySLP.SetAccessControl(rs);

                }
                else
                {
                    keySLP = Registry.LocalMachine.OpenSubKey(ParentKey, true);
                    keySLP.SetValue(subkeyname, subkeyval);
                    rs = rkey.GetAccessControl(AccessControlSections.Access);
                    rs.AddAccessRule(new RegistryAccessRule(strEveryoneAccount, RegistryRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow));
                    //Apply access control definition
                    keySLP.SetAccessControl(rs);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool WriteToRegistrySerialNo(string ParentKey, string sKeyname, string subkeyname, object subkeyval)
        {
            try
            {
                //Get 'Everyone' account
                System.Security.Principal.SecurityIdentifier sid = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.WorldSid, null);
                System.Security.Principal.NTAccount acct = sid.Translate(typeof(System.Security.Principal.NTAccount)) as System.Security.Principal.NTAccount;
                string strEveryoneAccount = acct.ToString();

                //Get parent key in HKLM
                RegistryKey keySLP = default(RegistryKey);
                RegistrySecurity rs = default(RegistrySecurity);
                RegistryKey rkey = Registry.LocalMachine.OpenSubKey(ParentKey, true);
                if (rkey == null)
                {
                    rkey = Registry.LocalMachine.OpenSubKey("SOFTWARE", true);
                    keySLP = rkey.CreateSubKey("Mindsoft");

                    rs = rkey.GetAccessControl(AccessControlSections.Access);
                    rs.AddAccessRule(new RegistryAccessRule(strEveryoneAccount, RegistryRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow));
                    //Apply access control definition
                    keySLP.SetAccessControl(rs);

                    rkey = Registry.LocalMachine.OpenSubKey(ParentKey, true);
                    keySLP = rkey.CreateSubKey(sKeyname);
                    keySLP.SetValue(subkeyname, subkeyval);

                    rs = rkey.GetAccessControl(AccessControlSections.Access);
                    rs.AddAccessRule(new RegistryAccessRule(strEveryoneAccount, RegistryRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow));
                    //Apply access control definition
                    keySLP.SetAccessControl(rs);
                }
                else
                {
                    keySLP = rkey.CreateSubKey(sKeyname);
                    keySLP.SetValue(subkeyname, subkeyval);

                    rs = rkey.GetAccessControl(AccessControlSections.Access);
                    rs.AddAccessRule(new RegistryAccessRule(strEveryoneAccount, RegistryRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow));
                    //Apply access control definition
                    keySLP.SetAccessControl(rs);
                }

                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }
    }
}
