﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;
using System.Net;
using Microsoft.Win32;

namespace MyBooksERP
{
    public class MachineSettings
    {
        public string GetMacNumber()
        {
            string macId = "";
            string sOsversion = GetOSVersion();

            if (sOsversion == "Vista" || sOsversion == "Latest")
            {
                ManagementObjectSearcher objSystem = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_NetworkAdapter where physicaladapter= True");

                foreach (ManagementObject objMg in objSystem.Get())
                {
                    macId = Convert.ToString(objMg["MACAddress"]).Replace(":", "").Trim();
                    if (macId != "")
                        break;
                }
            }
            else
            {
                macId = getMacAddress();
            }
            return macId;
        }

        private string getMacAddress()
        {
            string mcAddress = string.Empty;

            ManagementClass objMc = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objCol = objMc.GetInstances();
            foreach (ManagementObject objMg in objCol)
            {
                if (Convert.ToBoolean(objMg["IPenabled"]))
                {
                    mcAddress = Convert.ToString(objMg["macaddress"]).Replace(":", "").Trim();
                    if (mcAddress != "")
                    {
                        objMg.Dispose();
                        break;
                    }
                }
            }
            objCol.Dispose();
            return mcAddress;
        }

        public string GetOSVersion()
        {
            string sOsver = "";
            switch (System.Environment.OSVersion.Platform.ToString())
            {
                case "Win32S":
                    sOsver = "Win 3.1";
                    break;
                case "Win32Windows":
                    switch (System.Environment.OSVersion.Version.Minor)
                    {
                        case 0:
                            sOsver = "Win95";
                            break;
                        case 10:
                            sOsver = "Win98";
                            break;
                        case 90:
                            sOsver = "WinME";
                            break;
                        default:
                            sOsver = "Unknown";
                            break;
                    }
                    break;
                case "Win32NT":
                    switch (System.Environment.OSVersion.Version.Major)
                    {
                        case 3:
                            sOsver = "NT 3.51";
                            break;
                        case 4:
                            sOsver = "NT 4.0";
                            break;
                        case 5:
                            switch (System.Environment.OSVersion.Version.Minor)
                            {
                                case 0:
                                    sOsver = "Win2000";
                                    break;
                                case 1:
                                    sOsver = "WinXP";
                                    break;
                                case 2:
                                    sOsver = "Win2003";
                                    break;
                            }
                            break;
                        case 6:
                            sOsver = "Vista";
                            break;
                        default:
                            sOsver = "Latest";
                            break;
                    }
                    break;
                case "WinCE":
                    sOsver = "Win CE";
                    break;
            }
            return sOsver;
        }

        public string GetIPAddress()
        {
            string ipAddress = "";
            try
            {
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                {
                    IPAddress[] ipAddr = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
                    for (int i = 0; i < ipAddr.Length; i++)
                    {
                        if (ipAddr[i].AddressFamily != System.Net.Sockets.AddressFamily.InterNetworkV6)
                        {
                            if (!IPAddress.IsLoopback(ipAddr[i]))
                            {
                                ipAddress = ipAddr[i].ToString();
                                break;
                            }
                        }
                    }
                }

                return ipAddress;
            }
            catch (Exception)
            {
                return ipAddress;
            }
        }
    }
}
