﻿namespace MyBooksERP
{
    partial class frmRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label FaxLabel;
            System.Windows.Forms.Label POBoxLabel;
            System.Windows.Forms.Label StreetLabel;
            System.Windows.Forms.Label CityLabel;
            System.Windows.Forms.Label AreraCodeLabel;
            System.Windows.Forms.Label lblSlNo;
            System.Windows.Forms.Label WebsiteLabel;
            System.Windows.Forms.Label TelephoneLabel;
            System.Windows.Forms.Label EmailLabel;
            System.Windows.Forms.Label CustomerNameLabel;
            System.Windows.Forms.Label JobTitleLabel;
            System.Windows.Forms.Label CompanyLabel;
            System.Windows.Forms.Label CountryIDLabel;
            System.Windows.Forms.Label MobileLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegistration));
            this.webPrintform = new System.Windows.Forms.WebBrowser();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnBottomCancel = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.lblstatus = new System.Windows.Forms.Label();
            this.PictureBox2 = new System.Windows.Forms.PictureBox();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.FaxTextBox = new System.Windows.Forms.TextBox();
            this.POBoxTextBox = new System.Windows.Forms.TextBox();
            this.StreetTextBox = new System.Windows.Forms.TextBox();
            this.CityTextBox = new System.Windows.Forms.TextBox();
            this.AreraCodeTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSlNo = new System.Windows.Forms.TextBox();
            this.txtWebSite = new System.Windows.Forms.TextBox();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtcountry = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtJob = new System.Windows.Forms.TextBox();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.txtMobile = new System.Windows.Forms.TextBox();
            FaxLabel = new System.Windows.Forms.Label();
            POBoxLabel = new System.Windows.Forms.Label();
            StreetLabel = new System.Windows.Forms.Label();
            CityLabel = new System.Windows.Forms.Label();
            AreraCodeLabel = new System.Windows.Forms.Label();
            lblSlNo = new System.Windows.Forms.Label();
            WebsiteLabel = new System.Windows.Forms.Label();
            TelephoneLabel = new System.Windows.Forms.Label();
            EmailLabel = new System.Windows.Forms.Label();
            CustomerNameLabel = new System.Windows.Forms.Label();
            JobTitleLabel = new System.Windows.Forms.Label();
            CompanyLabel = new System.Windows.Forms.Label();
            CountryIDLabel = new System.Windows.Forms.Label();
            MobileLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // webPrintform
            // 
            this.webPrintform.Location = new System.Drawing.Point(503, 198);
            this.webPrintform.MinimumSize = new System.Drawing.Size(20, 20);
            this.webPrintform.Name = "webPrintform";
            this.webPrintform.Size = new System.Drawing.Size(149, 126);
            this.webPrintform.TabIndex = 114;
            this.webPrintform.Visible = false;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.SystemColors.Control;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Location = new System.Drawing.Point(76, 475);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(64, 23);
            this.btnClear.TabIndex = 17;
            this.btnClear.Text = "C&lear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnBottomCancel
            // 
            this.btnBottomCancel.BackColor = System.Drawing.SystemColors.Control;
            this.btnBottomCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBottomCancel.Location = new System.Drawing.Point(15, 475);
            this.btnBottomCancel.Name = "btnBottomCancel";
            this.btnBottomCancel.Size = new System.Drawing.Size(58, 23);
            this.btnBottomCancel.TabIndex = 16;
            this.btnBottomCancel.Text = "&Cancel";
            this.btnBottomCancel.UseVisualStyleBackColor = false;
            this.btnBottomCancel.Click += new System.EventHandler(this.btnBottomCancel_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.SystemColors.Control;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Location = new System.Drawing.Point(283, 475);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(58, 23);
            this.btnPrint.TabIndex = 15;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.BackColor = System.Drawing.SystemColors.Control;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.Location = new System.Drawing.Point(347, 475);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(58, 23);
            this.btnSubmit.TabIndex = 14;
            this.btnSubmit.Text = "&Submit";
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // lblstatus
            // 
            this.lblstatus.AutoSize = true;
            this.lblstatus.ForeColor = System.Drawing.Color.Red;
            this.lblstatus.Location = new System.Drawing.Point(16, 443);
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 13);
            this.lblstatus.TabIndex = 122;
            // 
            // PictureBox2
            // 
            this.PictureBox2.Location = new System.Drawing.Point(0, 464);
            this.PictureBox2.Name = "PictureBox2";
            this.PictureBox2.Size = new System.Drawing.Size(417, 46);
            this.PictureBox2.TabIndex = 117;
            this.PictureBox2.TabStop = false;
            // 
            // PictureBox1
            // 
            this.PictureBox1.BackgroundImage = global::MyBooksERP.Properties.Resources.Registration_form_2;
            this.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PictureBox1.Location = new System.Drawing.Point(-1, -1);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(419, 58);
            this.PictureBox1.TabIndex = 47;
            this.PictureBox1.TabStop = false;
            // 
            // FaxLabel
            // 
            FaxLabel.AutoSize = true;
            FaxLabel.Location = new System.Drawing.Point(215, 312);
            FaxLabel.Name = "FaxLabel";
            FaxLabel.Size = new System.Drawing.Size(24, 13);
            FaxLabel.TabIndex = 162;
            FaxLabel.Text = "Fax";
            // 
            // FaxTextBox
            // 
            this.FaxTextBox.Location = new System.Drawing.Point(215, 328);
            this.FaxTextBox.MaxLength = 20;
            this.FaxTextBox.Name = "FaxTextBox";
            this.FaxTextBox.Size = new System.Drawing.Size(171, 20);
            this.FaxTextBox.TabIndex = 11;
            // 
            // POBoxLabel
            // 
            POBoxLabel.AutoSize = true;
            POBoxLabel.Location = new System.Drawing.Point(19, 193);
            POBoxLabel.Name = "POBoxLabel";
            POBoxLabel.Size = new System.Drawing.Size(40, 13);
            POBoxLabel.TabIndex = 159;
            POBoxLabel.Text = "POBox";
            // 
            // POBoxTextBox
            // 
            this.POBoxTextBox.Location = new System.Drawing.Point(19, 210);
            this.POBoxTextBox.MaxLength = 10;
            this.POBoxTextBox.Name = "POBoxTextBox";
            this.POBoxTextBox.Size = new System.Drawing.Size(175, 20);
            this.POBoxTextBox.TabIndex = 4;
            // 
            // StreetLabel
            // 
            StreetLabel.AutoSize = true;
            StreetLabel.Location = new System.Drawing.Point(215, 195);
            StreetLabel.Name = "StreetLabel";
            StreetLabel.Size = new System.Drawing.Size(35, 13);
            StreetLabel.TabIndex = 160;
            StreetLabel.Text = "Street";
            // 
            // StreetTextBox
            // 
            this.StreetTextBox.Location = new System.Drawing.Point(215, 211);
            this.StreetTextBox.MaxLength = 50;
            this.StreetTextBox.Name = "StreetTextBox";
            this.StreetTextBox.Size = new System.Drawing.Size(171, 20);
            this.StreetTextBox.TabIndex = 5;
            // 
            // CityLabel
            // 
            CityLabel.AutoSize = true;
            CityLabel.Location = new System.Drawing.Point(215, 234);
            CityLabel.Name = "CityLabel";
            CityLabel.Size = new System.Drawing.Size(24, 13);
            CityLabel.TabIndex = 140;
            CityLabel.Text = "City";
            // 
            // CityTextBox
            // 
            this.CityTextBox.Location = new System.Drawing.Point(215, 250);
            this.CityTextBox.MaxLength = 50;
            this.CityTextBox.Name = "CityTextBox";
            this.CityTextBox.Size = new System.Drawing.Size(171, 20);
            this.CityTextBox.TabIndex = 7;
            // 
            // AreraCodeLabel
            // 
            AreraCodeLabel.AutoSize = true;
            AreraCodeLabel.Location = new System.Drawing.Point(19, 234);
            AreraCodeLabel.Name = "AreraCodeLabel";
            AreraCodeLabel.Size = new System.Drawing.Size(57, 13);
            AreraCodeLabel.TabIndex = 161;
            AreraCodeLabel.Text = "Area Code";
            // 
            // AreraCodeTextBox
            // 
            this.AreraCodeTextBox.Location = new System.Drawing.Point(19, 251);
            this.AreraCodeTextBox.MaxLength = 5;
            this.AreraCodeTextBox.Name = "AreraCodeTextBox";
            this.AreraCodeTextBox.Size = new System.Drawing.Size(175, 20);
            this.AreraCodeTextBox.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(226, 422);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(185, 13);
            this.label2.TabIndex = 158;
            this.label2.Text = "Fields marked in yellow are mandatory";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label3.Font = new System.Drawing.Font("Wingdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label3.ForeColor = System.Drawing.SystemColors.Info;
            this.label3.Location = new System.Drawing.Point(201, 420);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 15);
            this.label3.TabIndex = 157;
            this.label3.Text = "n";
            // 
            // lblSlNo
            // 
            lblSlNo.AutoSize = true;
            lblSlNo.Location = new System.Drawing.Point(19, 398);
            lblSlNo.Name = "lblSlNo";
            lblSlNo.Size = new System.Drawing.Size(50, 13);
            lblSlNo.TabIndex = 156;
            lblSlNo.Text = "Serial No";
            // 
            // txtSlNo
            // 
            this.txtSlNo.BackColor = System.Drawing.SystemColors.Info;
            this.txtSlNo.Location = new System.Drawing.Point(19, 415);
            this.txtSlNo.MaxLength = 8;
            this.txtSlNo.Name = "txtSlNo";
            this.txtSlNo.Size = new System.Drawing.Size(148, 20);
            this.txtSlNo.TabIndex = 13;
            // 
            // WebsiteLabel
            // 
            WebsiteLabel.AutoSize = true;
            WebsiteLabel.Location = new System.Drawing.Point(215, 273);
            WebsiteLabel.Name = "WebsiteLabel";
            WebsiteLabel.Size = new System.Drawing.Size(46, 13);
            WebsiteLabel.TabIndex = 153;
            WebsiteLabel.Text = "Website";
            // 
            // txtWebSite
            // 
            this.txtWebSite.Location = new System.Drawing.Point(215, 289);
            this.txtWebSite.MaxLength = 50;
            this.txtWebSite.Name = "txtWebSite";
            this.txtWebSite.Size = new System.Drawing.Size(171, 20);
            this.txtWebSite.TabIndex = 9;
            // 
            // TelephoneLabel
            // 
            TelephoneLabel.AutoSize = true;
            TelephoneLabel.Location = new System.Drawing.Point(19, 316);
            TelephoneLabel.Name = "TelephoneLabel";
            TelephoneLabel.Size = new System.Drawing.Size(58, 13);
            TelephoneLabel.TabIndex = 154;
            TelephoneLabel.Text = "Telephone";
            // 
            // txtTelephone
            // 
            this.txtTelephone.BackColor = System.Drawing.SystemColors.Info;
            this.txtTelephone.Location = new System.Drawing.Point(19, 333);
            this.txtTelephone.MaxLength = 20;
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(175, 20);
            this.txtTelephone.TabIndex = 10;
            // 
            // EmailLabel
            // 
            EmailLabel.AutoSize = true;
            EmailLabel.Location = new System.Drawing.Point(19, 357);
            EmailLabel.Name = "EmailLabel";
            EmailLabel.Size = new System.Drawing.Size(196, 13);
            EmailLabel.TabIndex = 155;
            EmailLabel.Text = "Email (Product key will sent to this email)";
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.SystemColors.Info;
            this.txtEmail.Location = new System.Drawing.Point(19, 374);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(367, 20);
            this.txtEmail.TabIndex = 12;
            // 
            // txtcountry
            // 
            this.txtcountry.BackColor = System.Drawing.SystemColors.Info;
            this.txtcountry.Location = new System.Drawing.Point(19, 292);
            this.txtcountry.MaxLength = 40;
            this.txtcountry.Name = "txtcountry";
            this.txtcountry.Size = new System.Drawing.Size(175, 20);
            this.txtcountry.TabIndex = 8;
            // 
            // CustomerNameLabel
            // 
            CustomerNameLabel.AutoSize = true;
            CustomerNameLabel.Location = new System.Drawing.Point(19, 111);
            CustomerNameLabel.Name = "CustomerNameLabel";
            CustomerNameLabel.Size = new System.Drawing.Size(117, 13);
            CustomerNameLabel.TabIndex = 148;
            CustomerNameLabel.Text = "Name (Contact Person)";
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.Info;
            this.txtName.Location = new System.Drawing.Point(19, 128);
            this.txtName.MaxLength = 50;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(367, 20);
            this.txtName.TabIndex = 1;
            // 
            // JobTitleLabel
            // 
            JobTitleLabel.AutoSize = true;
            JobTitleLabel.Location = new System.Drawing.Point(19, 152);
            JobTitleLabel.Name = "JobTitleLabel";
            JobTitleLabel.Size = new System.Drawing.Size(47, 13);
            JobTitleLabel.TabIndex = 149;
            JobTitleLabel.Text = "Job Title";
            // 
            // txtJob
            // 
            this.txtJob.BackColor = System.Drawing.SystemColors.Info;
            this.txtJob.Location = new System.Drawing.Point(19, 169);
            this.txtJob.MaxLength = 50;
            this.txtJob.Name = "txtJob";
            this.txtJob.Size = new System.Drawing.Size(175, 20);
            this.txtJob.TabIndex = 2;
            // 
            // CompanyLabel
            // 
            CompanyLabel.AutoSize = true;
            CompanyLabel.Location = new System.Drawing.Point(19, 70);
            CompanyLabel.Name = "CompanyLabel";
            CompanyLabel.Size = new System.Drawing.Size(121, 13);
            CompanyLabel.TabIndex = 150;
            CompanyLabel.Text = "Company / Organization";
            // 
            // txtCompany
            // 
            this.txtCompany.BackColor = System.Drawing.SystemColors.Info;
            this.txtCompany.Location = new System.Drawing.Point(19, 87);
            this.txtCompany.MaxLength = 50;
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(367, 20);
            this.txtCompany.TabIndex = 0;
            // 
            // CountryIDLabel
            // 
            CountryIDLabel.AutoSize = true;
            CountryIDLabel.Location = new System.Drawing.Point(19, 275);
            CountryIDLabel.Name = "CountryIDLabel";
            CountryIDLabel.Size = new System.Drawing.Size(43, 13);
            CountryIDLabel.TabIndex = 151;
            CountryIDLabel.Text = "Country";
            // 
            // MobileLabel
            // 
            MobileLabel.AutoSize = true;
            MobileLabel.Location = new System.Drawing.Point(215, 156);
            MobileLabel.Name = "MobileLabel";
            MobileLabel.Size = new System.Drawing.Size(38, 13);
            MobileLabel.TabIndex = 152;
            MobileLabel.Text = "Mobile";
            // 
            // txtMobile
            // 
            this.txtMobile.Location = new System.Drawing.Point(215, 172);
            this.txtMobile.MaxLength = 20;
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new System.Drawing.Size(171, 20);
            this.txtMobile.TabIndex = 3;
            // 
            // frmRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 507);
            this.Controls.Add(FaxLabel);
            this.Controls.Add(this.FaxTextBox);
            this.Controls.Add(POBoxLabel);
            this.Controls.Add(this.POBoxTextBox);
            this.Controls.Add(StreetLabel);
            this.Controls.Add(this.StreetTextBox);
            this.Controls.Add(CityLabel);
            this.Controls.Add(this.CityTextBox);
            this.Controls.Add(AreraCodeLabel);
            this.Controls.Add(this.AreraCodeTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(lblSlNo);
            this.Controls.Add(this.txtSlNo);
            this.Controls.Add(WebsiteLabel);
            this.Controls.Add(this.txtWebSite);
            this.Controls.Add(TelephoneLabel);
            this.Controls.Add(this.txtTelephone);
            this.Controls.Add(EmailLabel);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtcountry);
            this.Controls.Add(CustomerNameLabel);
            this.Controls.Add(this.txtName);
            this.Controls.Add(JobTitleLabel);
            this.Controls.Add(this.txtJob);
            this.Controls.Add(CompanyLabel);
            this.Controls.Add(this.txtCompany);
            this.Controls.Add(CountryIDLabel);
            this.Controls.Add(MobileLabel);
            this.Controls.Add(this.txtMobile);
            this.Controls.Add(this.lblstatus);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnBottomCancel);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.webPrintform);
            this.Controls.Add(this.PictureBox2);
            this.Controls.Add(this.PictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRegistration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Registration";
            this.Load += new System.EventHandler(this.frmRegistration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.WebBrowser webPrintform;
        internal System.Windows.Forms.PictureBox PictureBox2;
        internal System.Windows.Forms.Button btnClear;
        internal System.Windows.Forms.Button btnBottomCancel;
        internal System.Windows.Forms.Button btnPrint;
        internal System.Windows.Forms.Button btnSubmit;
        internal System.Windows.Forms.Label lblstatus;
        internal System.Windows.Forms.TextBox FaxTextBox;
        internal System.Windows.Forms.TextBox POBoxTextBox;
        internal System.Windows.Forms.TextBox StreetTextBox;
        internal System.Windows.Forms.TextBox CityTextBox;
        internal System.Windows.Forms.TextBox AreraCodeTextBox;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.TextBox txtSlNo;
        internal System.Windows.Forms.TextBox txtWebSite;
        internal System.Windows.Forms.TextBox txtTelephone;
        internal System.Windows.Forms.TextBox txtEmail;
        internal System.Windows.Forms.TextBox txtcountry;
        internal System.Windows.Forms.TextBox txtName;
        internal System.Windows.Forms.TextBox txtJob;
        internal System.Windows.Forms.TextBox txtCompany;
        internal System.Windows.Forms.TextBox txtMobile;
    }
}