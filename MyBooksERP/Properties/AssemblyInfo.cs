﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MyBooksERP")]
[assembly: AssemblyDescription("MyBooksERP")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mindsoft Technologies India Pvt. Ltd.")]
[assembly: AssemblyProduct("MyBooksERP")]
[assembly: AssemblyCopyright("Copyright ©  2009 Mindsoft Technologies India Pvt. Ltd.")]
[assembly: AssemblyTrademark("Mindsoft")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("da5b7564-7b15-4a6c-ad61-174a8cfe1354")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.5.3")]
[assembly: AssemblyFileVersion("1.0.5.3")]
