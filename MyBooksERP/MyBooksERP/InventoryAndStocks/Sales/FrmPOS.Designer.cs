﻿namespace MyBooksERP
{
    partial class FrmPOS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPOS));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.lblCancelReason = new System.Windows.Forms.Label();
            this.txtCancelReason = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnCounterReference = new DevComponents.DotNetBar.ButtonX();
            this.cboWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.cboCounter = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCounter = new System.Windows.Forms.Label();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.lblEmployeeNames = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new System.Windows.Forms.Label();
            this.txtBillNumber = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.dtpPOSDate = new System.Windows.Forms.DateTimePicker();
            this.cboCustomer = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnVendor = new DevComponents.DotNetBar.ButtonX();
            this.lblBillNumber = new System.Windows.Forms.Label();
            this.lblDueDate = new System.Windows.Forms.Label();
            this.dtpDueDate = new System.Windows.Forms.DateTimePicker();
            this.chkDelivered = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.dockSite7 = new DevComponents.DotNetBar.DockSite();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.btnActions = new DevComponents.DotNetBar.ButtonItem();
            this.btnInstallments = new DevComponents.DotNetBar.ButtonItem();
            this.btnExpense = new DevComponents.DotNetBar.ButtonItem();
            this.bnSubmitForApproval = new DevComponents.DotNetBar.ButtonItem();
            this.btnApprove = new DevComponents.DotNetBar.ButtonItem();
            this.btnReject = new DevComponents.DotNetBar.ButtonItem();
            this.btnDocuments = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorClearItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorCancelItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorDeleteItem = new DevComponents.DotNetBar.ButtonItem();
            this.BtnDiscount = new DevComponents.DotNetBar.ButtonItem();
            this.BtnItem = new DevComponents.DotNetBar.ButtonItem();
            this.BtnTax = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorSaveItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorAddNewItem = new DevComponents.DotNetBar.ButtonItem();
            this.PurchaseOrderBindingNavigator = new DevComponents.DotNetBar.Bar();
            this.BtnUom = new DevComponents.DotNetBar.ButtonItem();
            this.btnAccountSettings = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.BtnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.BtnHelp = new DevComponents.DotNetBar.ButtonItem();
            this.TmrFocus = new System.Windows.Forms.Timer(this.components);
            this.TmrPOS = new System.Windows.Forms.Timer(this.components);
            this.ErrSales = new System.Windows.Forms.ErrorProvider(this.components);
            this.CMSVendorAddress = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ImgSales = new System.Windows.Forms.ImageList(this.components);
            this.bckgrndWrkrSendSms = new System.ComponentModel.BackgroundWorker();
            this.panelBottom = new DevComponents.DotNetBar.PanelEx();
            this.tcPOS = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvPOS = new ClsInnerGridBar();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Uom = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtColGrandAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tax = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Discount = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.DiscountAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesOrderID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SelectedTaxID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SelectedDiscountID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsGroup = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.QtyAvailable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsGroupItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiItemDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvLocationDetails = new ClsInnerGridBar();
            this.LItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LBatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LBatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LAvailableQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LUOM = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LUOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LLocation = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LRow = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LBlock = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LLot = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tiLocationDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.panelGridBottom = new DevComponents.DotNetBar.PanelEx();
            this.lblAmountIn = new System.Windows.Forms.Label();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.panelEx3 = new DevComponents.DotNetBar.PanelEx();
            this.txtNetAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblNetAmount = new System.Windows.Forms.Label();
            this.lblTendered = new System.Windows.Forms.Label();
            this.lblChange = new System.Windows.Forms.Label();
            this.lblDiscountScheme = new System.Windows.Forms.Label();
            this.txtSubTotal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblDiscountAmount = new System.Windows.Forms.Label();
            this.txtDiscountAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtTotal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtTendered = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtChange = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblSubTotal = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.cboDiscountScheme = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnRemove = new DevComponents.DotNetBar.ButtonX();
            this.btnNewItem = new DevComponents.DotNetBar.ButtonX();
            this.cboPaymentMode = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtExpense = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.txtRemarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblPaymentMode = new System.Windows.Forms.Label();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.wbCreatedBy = new DevComponents.DotNetBar.Controls.WarningBox();
            this.wbCreatedDate = new DevComponents.DotNetBar.Controls.WarningBox();
            this.wbStatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblPOSstatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.btnNew = new DevComponents.DotNetBar.ButtonX();
            this.lblExpense = new System.Windows.Forms.Label();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.lblAmountInWords = new System.Windows.Forms.Label();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.dockSite2 = new DevComponents.DotNetBar.DockSite();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.dotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.dockSite4 = new DevComponents.DotNetBar.DockSite();
            this.dockSite8 = new DevComponents.DotNetBar.DockSite();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.dockSite6 = new DevComponents.DotNetBar.DockSite();
            this.dockSite3 = new DevComponents.DotNetBar.DockSite();
            this.expandableSplitterLeftPOS = new DevComponents.DotNetBar.ExpandableSplitter();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.DgvBillNo = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.lnkLabel = new System.Windows.Forms.LinkLabel();
            this.cboCounterSearch = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCounterSearch = new System.Windows.Forms.Label();
            this.lblBillNoSearch = new System.Windows.Forms.Label();
            this.dtpSToSearch = new System.Windows.Forms.DateTimePicker();
            this.dtpSFromSearch = new System.Windows.Forms.DateTimePicker();
            this.lblToSearch = new System.Windows.Forms.Label();
            this.lblFromSearch = new System.Windows.Forms.Label();
            this.cboCashierSearch = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCashierSearch = new System.Windows.Forms.Label();
            this.btnSearch = new DevComponents.DotNetBar.ButtonX();
            this.txtBillNoSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboStatusSearch = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboCustomerSearch = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboCompanySearch = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompanySearch = new System.Windows.Forms.Label();
            this.lblCustomerSearch = new System.Windows.Forms.Label();
            this.lblStatusSearch = new System.Windows.Forms.Label();
            this.LblSCountStatus = new DevComponents.DotNetBar.LabelX();
            this.CommandZoom = new DevComponents.DotNetBar.Command(this.components);
            this.dockContainerItem1 = new DevComponents.DotNetBar.DockContainerItem();
            this.tmrCreatedDateTime = new System.Windows.Forms.Timer(this.components);
            this.tmrMessage = new System.Windows.Forms.Timer(this.components);
            this.clmItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmOrderDetailID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmQtyReceived = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmSalesOrderID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmBatchNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CntxtHistory = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ShowItemHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowSaleRateHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.showGroupItemDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.panelTop.SuspendLayout();
            this.dockSite7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrSales)).BeginInit();
            this.panelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcPOS)).BeginInit();
            this.tcPOS.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPOS)).BeginInit();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocationDetails)).BeginInit();
            this.panelGridBottom.SuspendLayout();
            this.panelEx3.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.panelEx1.SuspendLayout();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvBillNo)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            this.panelEx2.SuspendLayout();
            this.CntxtHistory.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelTop.Controls.Add(this.lblCancelReason);
            this.panelTop.Controls.Add(this.txtCancelReason);
            this.panelTop.Controls.Add(this.btnCounterReference);
            this.panelTop.Controls.Add(this.cboWarehouse);
            this.panelTop.Controls.Add(this.lblWarehouse);
            this.panelTop.Controls.Add(this.cboCounter);
            this.panelTop.Controls.Add(this.lblCounter);
            this.panelTop.Controls.Add(this.lblCustomer);
            this.panelTop.Controls.Add(this.lblEmployeeNames);
            this.panelTop.Controls.Add(this.lblDate);
            this.panelTop.Controls.Add(this.cboCompany);
            this.panelTop.Controls.Add(this.lblCompany);
            this.panelTop.Controls.Add(this.txtBillNumber);
            this.panelTop.Controls.Add(this.dtpPOSDate);
            this.panelTop.Controls.Add(this.cboCustomer);
            this.panelTop.Controls.Add(this.btnVendor);
            this.panelTop.Controls.Add(this.lblBillNumber);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1017, 65);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 0;
            // 
            // lblCancelReason
            // 
            this.lblCancelReason.AutoSize = true;
            this.lblCancelReason.Location = new System.Drawing.Point(786, 14);
            this.lblCancelReason.Name = "lblCancelReason";
            this.lblCancelReason.Size = new System.Drawing.Size(80, 13);
            this.lblCancelReason.TabIndex = 263;
            this.lblCancelReason.Text = "Cancel Reason";
            this.lblCancelReason.Visible = false;
            // 
            // txtCancelReason
            // 
            // 
            // 
            // 
            this.txtCancelReason.Border.Class = "TextBoxBorder";
            this.txtCancelReason.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCancelReason.Location = new System.Drawing.Point(872, 7);
            this.txtCancelReason.MaxLength = 200;
            this.txtCancelReason.Multiline = true;
            this.txtCancelReason.Name = "txtCancelReason";
            this.txtCancelReason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtCancelReason.Size = new System.Drawing.Size(140, 49);
            this.txtCancelReason.TabIndex = 262;
            this.txtCancelReason.TabStop = false;
            this.txtCancelReason.Visible = false;
            this.txtCancelReason.TextChanged += new System.EventHandler(this.txtCancelReason_TextChanged);
            // 
            // btnCounterReference
            // 
            this.btnCounterReference.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCounterReference.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCounterReference.Location = new System.Drawing.Point(225, 36);
            this.btnCounterReference.Name = "btnCounterReference";
            this.btnCounterReference.Size = new System.Drawing.Size(29, 20);
            this.btnCounterReference.TabIndex = 261;
            this.btnCounterReference.TabStop = false;
            this.btnCounterReference.Text = "...";
            this.btnCounterReference.Click += new System.EventHandler(this.btnCounterReference_Click);
            // 
            // cboWarehouse
            // 
            this.cboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWarehouse.DisplayMember = "Text";
            this.cboWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWarehouse.DropDownHeight = 75;
            this.cboWarehouse.FormattingEnabled = true;
            this.cboWarehouse.IntegralHeight = false;
            this.cboWarehouse.ItemHeight = 14;
            this.cboWarehouse.Location = new System.Drawing.Point(616, 36);
            this.cboWarehouse.Name = "cboWarehouse";
            this.cboWarehouse.Size = new System.Drawing.Size(164, 20);
            this.cboWarehouse.TabIndex = 3;
            this.cboWarehouse.SelectedIndexChanged += new System.EventHandler(this.cboWarehouse_SelectedIndexChanged);
            this.cboWarehouse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboWarehouse_KeyPress);
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(548, 40);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(62, 13);
            this.lblWarehouse.TabIndex = 260;
            this.lblWarehouse.Text = "Warehouse";
            // 
            // cboCounter
            // 
            this.cboCounter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCounter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCounter.DisplayMember = "Text";
            this.cboCounter.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCounter.DropDownHeight = 75;
            this.cboCounter.FormattingEnabled = true;
            this.cboCounter.IntegralHeight = false;
            this.cboCounter.ItemHeight = 14;
            this.cboCounter.Location = new System.Drawing.Point(53, 36);
            this.cboCounter.Name = "cboCounter";
            this.cboCounter.Size = new System.Drawing.Size(170, 20);
            this.cboCounter.TabIndex = 1;
            this.cboCounter.SelectedIndexChanged += new System.EventHandler(this.cboCounter_SelectedIndexChanged);
            this.cboCounter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCounter_KeyPress);
            // 
            // lblCounter
            // 
            this.lblCounter.AutoSize = true;
            this.lblCounter.Location = new System.Drawing.Point(2, 40);
            this.lblCounter.Name = "lblCounter";
            this.lblCounter.Size = new System.Drawing.Size(44, 13);
            this.lblCounter.TabIndex = 256;
            this.lblCounter.Text = "Counter";
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Location = new System.Drawing.Point(266, 40);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(51, 13);
            this.lblCustomer.TabIndex = 253;
            this.lblCustomer.Text = "Customer";
            // 
            // lblEmployeeNames
            // 
            this.lblEmployeeNames.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEmployeeNames.AutoSize = true;
            this.lblEmployeeNames.Location = new System.Drawing.Point(629, 60);
            this.lblEmployeeNames.Name = "lblEmployeeNames";
            this.lblEmployeeNames.Size = new System.Drawing.Size(0, 13);
            this.lblEmployeeNames.TabIndex = 0;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(548, 14);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(46, 13);
            this.lblDate.TabIndex = 240;
            this.lblDate.Text = "Bill Date";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 75;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(53, 10);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(201, 20);
            this.cboCompany.TabIndex = 0;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCompany_KeyPress);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Location = new System.Drawing.Point(2, 14);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 237;
            this.lblCompany.Text = "Company";
            // 
            // txtBillNumber
            // 
            this.txtBillNumber.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtBillNumber.Border.Class = "TextBoxBorder";
            this.txtBillNumber.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtBillNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBillNumber.Location = new System.Drawing.Point(332, 7);
            this.txtBillNumber.MaxLength = 20;
            this.txtBillNumber.Name = "txtBillNumber";
            this.txtBillNumber.ReadOnly = true;
            this.txtBillNumber.Size = new System.Drawing.Size(201, 26);
            this.txtBillNumber.TabIndex = 1;
            this.txtBillNumber.TabStop = false;
            this.txtBillNumber.WordWrap = false;
            this.txtBillNumber.TextChanged += new System.EventHandler(this.txtBillNumber_TextChanged);
            // 
            // dtpPOSDate
            // 
            this.dtpPOSDate.CustomFormat = "dd-MMM-yyyy hh:mm:ss tt";
            this.dtpPOSDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPOSDate.Location = new System.Drawing.Point(616, 10);
            this.dtpPOSDate.Name = "dtpPOSDate";
            this.dtpPOSDate.Size = new System.Drawing.Size(164, 20);
            this.dtpPOSDate.TabIndex = 8;
            this.dtpPOSDate.TabStop = false;
            this.dtpPOSDate.ValueChanged += new System.EventHandler(this.dtpPOSDate_ValueChanged);
            // 
            // cboCustomer
            // 
            this.cboCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCustomer.DisplayMember = "Text";
            this.cboCustomer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCustomer.DropDownHeight = 75;
            this.cboCustomer.FormattingEnabled = true;
            this.cboCustomer.IntegralHeight = false;
            this.cboCustomer.ItemHeight = 14;
            this.cboCustomer.Location = new System.Drawing.Point(332, 36);
            this.cboCustomer.Name = "cboCustomer";
            this.cboCustomer.Size = new System.Drawing.Size(170, 20);
            this.cboCustomer.TabIndex = 2;
            this.cboCustomer.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCustomer.WatermarkText = "No Customer Selected";
            this.cboCustomer.SelectedIndexChanged += new System.EventHandler(this.cboCustomer_SelectedIndexChanged);
            this.cboCustomer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCustomer_KeyPress);
            // 
            // btnVendor
            // 
            this.btnVendor.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnVendor.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnVendor.Location = new System.Drawing.Point(504, 36);
            this.btnVendor.Name = "btnVendor";
            this.btnVendor.Size = new System.Drawing.Size(29, 20);
            this.btnVendor.TabIndex = 6;
            this.btnVendor.TabStop = false;
            this.btnVendor.Text = "...";
            this.btnVendor.Click += new System.EventHandler(this.btnVendor_Click);
            // 
            // lblBillNumber
            // 
            this.lblBillNumber.AutoSize = true;
            this.lblBillNumber.Location = new System.Drawing.Point(266, 14);
            this.lblBillNumber.Name = "lblBillNumber";
            this.lblBillNumber.Size = new System.Drawing.Size(60, 13);
            this.lblBillNumber.TabIndex = 185;
            this.lblBillNumber.Text = "Bill Number";
            // 
            // lblDueDate
            // 
            this.lblDueDate.AutoSize = true;
            this.lblDueDate.Location = new System.Drawing.Point(383, 47);
            this.lblDueDate.Name = "lblDueDate";
            this.lblDueDate.Size = new System.Drawing.Size(53, 13);
            this.lblDueDate.TabIndex = 258;
            this.lblDueDate.Text = "Due Date";
            this.lblDueDate.Visible = false;
            // 
            // dtpDueDate
            // 
            this.dtpDueDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDueDate.Location = new System.Drawing.Point(386, 66);
            this.dtpDueDate.Name = "dtpDueDate";
            this.dtpDueDate.Size = new System.Drawing.Size(50, 20);
            this.dtpDueDate.TabIndex = 257;
            this.dtpDueDate.Visible = false;
            this.dtpDueDate.ValueChanged += new System.EventHandler(this.dtpDueDate_ValueChanged);
            // 
            // chkDelivered
            // 
            // 
            // 
            // 
            this.chkDelivered.BackgroundStyle.Class = "";
            this.chkDelivered.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkDelivered.Checked = true;
            this.chkDelivered.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDelivered.CheckValue = "Y";
            this.chkDelivered.Location = new System.Drawing.Point(370, 18);
            this.chkDelivered.Name = "chkDelivered";
            this.chkDelivered.Size = new System.Drawing.Size(100, 23);
            this.chkDelivered.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkDelivered.TabIndex = 254;
            this.chkDelivered.TabStop = false;
            this.chkDelivered.Text = "Delivered";
            this.chkDelivered.Visible = false;
            this.chkDelivered.CheckedChanged += new System.EventHandler(this.chkDelivered_CheckedChanged);
            // 
            // dockSite7
            // 
            this.dockSite7.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite7.Controls.Add(this.bar1);
            this.dockSite7.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite7.Location = new System.Drawing.Point(0, 0);
            this.dockSite7.Name = "dockSite7";
            this.dockSite7.Size = new System.Drawing.Size(1020, 25);
            this.dockSite7.TabIndex = 32;
            this.dockSite7.TabStop = false;
            this.dockSite7.Visible = false;
            // 
            // bar1
            // 
            this.bar1.AccessibleDescription = "DotNetBar Bar (bar1)";
            this.bar1.AccessibleName = "DotNetBar Bar";
            this.bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.bar1.Location = new System.Drawing.Point(0, 0);
            this.bar1.MenuBar = true;
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(36, 24);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.bar1.TabIndex = 0;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // btnActions
            // 
            this.btnActions.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnActions.Image = ((System.Drawing.Image)(resources.GetObject("btnActions.Image")));
            this.btnActions.Name = "btnActions";
            this.btnActions.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnInstallments,
            this.btnExpense,
            this.bnSubmitForApproval,
            this.btnApprove,
            this.btnReject,
            this.btnDocuments});
            this.btnActions.Text = "Actions";
            this.btnActions.Tooltip = "Actions";
            this.btnActions.Visible = false;
            // 
            // btnInstallments
            // 
            this.btnInstallments.Image = ((System.Drawing.Image)(resources.GetObject("btnInstallments.Image")));
            this.btnInstallments.Name = "btnInstallments";
            this.btnInstallments.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlH);
            this.btnInstallments.Text = "Installments";
            this.btnInstallments.Tooltip = "Installments";
            // 
            // btnExpense
            // 
            this.btnExpense.Image = ((System.Drawing.Image)(resources.GetObject("btnExpense.Image")));
            this.btnExpense.Name = "btnExpense";
            this.btnExpense.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.btnExpense.Text = "Expense";
            this.btnExpense.Tooltip = "Expense";
            // 
            // bnSubmitForApproval
            // 
            this.bnSubmitForApproval.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnSubmitForApproval.Image = ((System.Drawing.Image)(resources.GetObject("bnSubmitForApproval.Image")));
            this.bnSubmitForApproval.Name = "bnSubmitForApproval";
            this.bnSubmitForApproval.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.bnSubmitForApproval.Text = "Submit For Approval";
            this.bnSubmitForApproval.Tooltip = "Submit For Approval";
            this.bnSubmitForApproval.Visible = false;
            // 
            // btnApprove
            // 
            this.btnApprove.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnApprove.Image = ((System.Drawing.Image)(resources.GetObject("btnApprove.Image")));
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.btnApprove.Text = "Approve";
            this.btnApprove.Tooltip = "Approve";
            this.btnApprove.Visible = false;
            // 
            // btnReject
            // 
            this.btnReject.Image = ((System.Drawing.Image)(resources.GetObject("btnReject.Image")));
            this.btnReject.Name = "btnReject";
            this.btnReject.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlR);
            this.btnReject.Text = "Reject";
            this.btnReject.Visible = false;
            // 
            // btnDocuments
            // 
            this.btnDocuments.Image = ((System.Drawing.Image)(resources.GetObject("btnDocuments.Image")));
            this.btnDocuments.Name = "btnDocuments";
            this.btnDocuments.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlD);
            this.btnDocuments.Text = "Documents";
            this.btnDocuments.Tooltip = "Documents";
            // 
            // BindingNavigatorClearItem
            // 
            this.BindingNavigatorClearItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorClearItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorClearItem.Image")));
            this.BindingNavigatorClearItem.Name = "BindingNavigatorClearItem";
            this.BindingNavigatorClearItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlR);
            this.BindingNavigatorClearItem.Text = "Clear";
            this.BindingNavigatorClearItem.Tooltip = "Clear";
            this.BindingNavigatorClearItem.Click += new System.EventHandler(this.BindingNavigatorClearItem_Click);
            // 
            // BindingNavigatorCancelItem
            // 
            this.BindingNavigatorCancelItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorCancelItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorCancelItem.Image")));
            this.BindingNavigatorCancelItem.Name = "BindingNavigatorCancelItem";
            this.BindingNavigatorCancelItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlShiftC);
            this.BindingNavigatorCancelItem.Text = "Cancel";
            this.BindingNavigatorCancelItem.Tooltip = "Cancel";
            this.BindingNavigatorCancelItem.Click += new System.EventHandler(this.BindingNavigatorCancelItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlDel);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Tooltip = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BtnDiscount
            // 
            this.BtnDiscount.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnDiscount.Icon = ((System.Drawing.Icon)(resources.GetObject("BtnDiscount.Icon")));
            this.BtnDiscount.Name = "BtnDiscount";
            this.BtnDiscount.Text = "Discount";
            this.BtnDiscount.Tooltip = "Discount";
            this.BtnDiscount.Visible = false;
            // 
            // BtnItem
            // 
            this.BtnItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnItem.Image = ((System.Drawing.Image)(resources.GetObject("BtnItem.Image")));
            this.BtnItem.Name = "BtnItem";
            this.BtnItem.Text = "Product";
            this.BtnItem.Tooltip = "Product";
            this.BtnItem.Visible = false;
            // 
            // BtnTax
            // 
            this.BtnTax.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnTax.Icon = ((System.Drawing.Icon)(resources.GetObject("BtnTax.Icon")));
            this.BtnTax.Name = "BtnTax";
            this.BtnTax.Text = "Tax";
            this.BtnTax.Tooltip = "Tax";
            this.BtnTax.Visible = false;
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Text = "&Save";
            this.BindingNavigatorSaveItem.Tooltip = "Save(F12)";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.Tooltip = "Add New Information(F5)";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // PurchaseOrderBindingNavigator
            // 
            this.PurchaseOrderBindingNavigator.AccessibleDescription = "DotNetBar Bar (PurchaseOrderBindingNavigator)";
            this.PurchaseOrderBindingNavigator.AccessibleName = "DotNetBar Bar";
            this.PurchaseOrderBindingNavigator.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.PurchaseOrderBindingNavigator.Dock = System.Windows.Forms.DockStyle.Top;
            this.PurchaseOrderBindingNavigator.DockLine = 1;
            this.PurchaseOrderBindingNavigator.DockOffset = 73;
            this.PurchaseOrderBindingNavigator.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.PurchaseOrderBindingNavigator.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.PurchaseOrderBindingNavigator.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorClearItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorCancelItem,
            this.btnActions,
            this.BtnItem,
            this.BtnDiscount,
            this.BtnTax,
            this.BtnUom,
            this.btnAccountSettings,
            this.BtnPrint,
            this.BtnEmail,
            this.BtnHelp});
            this.PurchaseOrderBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.PurchaseOrderBindingNavigator.Name = "PurchaseOrderBindingNavigator";
            this.PurchaseOrderBindingNavigator.Size = new System.Drawing.Size(1017, 25);
            this.PurchaseOrderBindingNavigator.Stretch = true;
            this.PurchaseOrderBindingNavigator.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PurchaseOrderBindingNavigator.TabIndex = 12;
            this.PurchaseOrderBindingNavigator.TabStop = false;
            this.PurchaseOrderBindingNavigator.Text = "bar2";
            // 
            // BtnUom
            // 
            this.BtnUom.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnUom.Icon = ((System.Drawing.Icon)(resources.GetObject("BtnUom.Icon")));
            this.BtnUom.Name = "BtnUom";
            this.BtnUom.Text = "Uom";
            this.BtnUom.Tooltip = "Unit Of Measurement";
            this.BtnUom.Visible = false;
            // 
            // btnAccountSettings
            // 
            this.btnAccountSettings.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAccountSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnAccountSettings.Image")));
            this.btnAccountSettings.Name = "btnAccountSettings";
            this.btnAccountSettings.Text = "Account";
            this.btnAccountSettings.Tooltip = "Account Settings";
            this.btnAccountSettings.Visible = false;
            // 
            // BtnPrint
            // 
            this.BtnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // BtnHelp
            // 
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Tooltip = "Help";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // TmrPOS
            // 
            this.TmrPOS.Interval = 2000;
            this.TmrPOS.Tick += new System.EventHandler(this.TmrPOS_Tick);
            // 
            // ErrSales
            // 
            this.ErrSales.ContainerControl = this;
            this.ErrSales.RightToLeft = true;
            // 
            // CMSVendorAddress
            // 
            this.CMSVendorAddress.Name = "ContextMenuStrip1";
            this.CMSVendorAddress.Size = new System.Drawing.Size(61, 4);
            // 
            // ImgSales
            // 
            this.ImgSales.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgSales.ImageStream")));
            this.ImgSales.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgSales.Images.SetKeyName(0, "Purchase Indent.ICO");
            this.ImgSales.Images.SetKeyName(1, "Purchase Order.ico");
            this.ImgSales.Images.SetKeyName(2, "Purchase Invoice.ico");
            this.ImgSales.Images.SetKeyName(3, "GRN.ICO");
            this.ImgSales.Images.SetKeyName(4, "Purchase Order Return.ico");
            this.ImgSales.Images.SetKeyName(5, "Add.png");
            this.ImgSales.Images.SetKeyName(6, "save.PNG");
            // 
            // panelBottom
            // 
            this.panelBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelBottom.Controls.Add(this.tcPOS);
            this.panelBottom.Controls.Add(this.panelGridBottom);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBottom.Location = new System.Drawing.Point(0, 90);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1017, 429);
            this.panelBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelBottom.Style.GradientAngle = 90;
            this.panelBottom.TabIndex = 18;
            this.panelBottom.Text = "panelEx3";
            // 
            // tcPOS
            // 
            this.tcPOS.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tcPOS.BackColor = System.Drawing.Color.Transparent;
            this.tcPOS.CanReorderTabs = true;
            this.tcPOS.Controls.Add(this.tabControlPanel1);
            this.tcPOS.Controls.Add(this.tabControlPanel2);
            this.tcPOS.Location = new System.Drawing.Point(0, 0);
            this.tcPOS.Name = "tcPOS";
            this.tcPOS.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcPOS.SelectedTabIndex = 0;
            this.tcPOS.Size = new System.Drawing.Size(1017, 236);
            this.tcPOS.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcPOS.TabIndex = 1;
            this.tcPOS.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcPOS.Tabs.Add(this.tiItemDetails);
            this.tcPOS.Tabs.Add(this.tiLocationDetails);
            this.tcPOS.Text = "tabControl1";
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.dgvPOS);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1017, 214);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 1;
            this.tabControlPanel1.TabItem = this.tiItemDetails;
            // 
            // dgvPOS
            // 
            this.dgvPOS.AddNewRow = false;
            this.dgvPOS.AlphaNumericCols = new int[0];
            this.dgvPOS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPOS.BackgroundColor = System.Drawing.Color.White;
            this.dgvPOS.CapsLockCols = new int[0];
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPOS.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPOS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPOS.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.BatchNo,
            this.Quantity,
            this.Uom,
            this.Rate,
            this.txtColGrandAmount,
            this.Tax,
            this.Discount,
            this.DiscountAmount,
            this.Total,
            this.SalesOrderID,
            this.ItemID,
            this.TaxAmount,
            this.SelectedTaxID,
            this.SelectedDiscountID,
            this.BatchID,
            this.IsGroup,
            this.QtyAvailable,
            this.IsGroupItem});
            this.dgvPOS.DecimalCols = new int[] {
        3,
        5};
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPOS.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvPOS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPOS.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvPOS.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvPOS.HasSlNo = false;
            this.dgvPOS.LastRowIndex = 0;
            this.dgvPOS.Location = new System.Drawing.Point(1, 1);
            this.dgvPOS.Name = "dgvPOS";
            this.dgvPOS.NegativeValueCols = new int[0];
            this.dgvPOS.NumericCols = new int[0];
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPOS.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvPOS.RowHeadersWidth = 50;
            this.dgvPOS.Size = new System.Drawing.Size(1015, 212);
            this.dgvPOS.TabIndex = 0;
            this.dgvPOS.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPOS_CellValueChanged);
            this.dgvPOS.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPOS_CellMouseClick);
            this.dgvPOS.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvPOS_CellBeginEdit);
            this.dgvPOS.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvPOS_RowsAdded);
            this.dgvPOS.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPOS_CellEndEdit);
            this.dgvPOS.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvPOS_Textbox_TextChanged);
            this.dgvPOS.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvPOS_CurrentCellDirtyStateChanged);
            this.dgvPOS.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvPOS_DataError);
            this.dgvPOS.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPOS_CellEnter);
            this.dgvPOS.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvPOS_RowsRemoved);
            // 
            // ItemCode
            // 
            this.ItemCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ItemCode.DefaultCellStyle = dataGridViewCellStyle2;
            this.ItemCode.FillWeight = 44.51458F;
            this.ItemCode.HeaderText = "Item Code";
            this.ItemCode.MinimumWidth = 75;
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ItemName
            // 
            this.ItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ItemName.DefaultCellStyle = dataGridViewCellStyle3;
            this.ItemName.FillWeight = 507.6143F;
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.MinimumWidth = 150;
            this.ItemName.Name = "ItemName";
            this.ItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BatchNo
            // 
            this.BatchNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.BatchNo.DefaultCellStyle = dataGridViewCellStyle4;
            this.BatchNo.FillWeight = 44.51458F;
            this.BatchNo.HeaderText = "Batch No";
            this.BatchNo.MinimumWidth = 125;
            this.BatchNo.Name = "BatchNo";
            this.BatchNo.ReadOnly = true;
            this.BatchNo.Visible = false;
            // 
            // Quantity
            // 
            this.Quantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.NullValue = null;
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle5;
            this.Quantity.FillWeight = 44.51458F;
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.MinimumWidth = 100;
            this.Quantity.Name = "Quantity";
            this.Quantity.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Quantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Uom
            // 
            this.Uom.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Uom.DefaultCellStyle = dataGridViewCellStyle6;
            this.Uom.FillWeight = 136.2691F;
            this.Uom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Uom.HeaderText = "UOM";
            this.Uom.MinimumWidth = 60;
            this.Uom.Name = "Uom";
            this.Uom.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Uom.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Uom.Width = 60;
            // 
            // Rate
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            this.Rate.DefaultCellStyle = dataGridViewCellStyle7;
            this.Rate.FillWeight = 44.51458F;
            this.Rate.HeaderText = "Rate";
            this.Rate.MaxInputLength = 10;
            this.Rate.MinimumWidth = 100;
            this.Rate.Name = "Rate";
            this.Rate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtColGrandAmount
            // 
            this.txtColGrandAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.txtColGrandAmount.DefaultCellStyle = dataGridViewCellStyle8;
            this.txtColGrandAmount.FillWeight = 44.51458F;
            this.txtColGrandAmount.HeaderText = "Amount";
            this.txtColGrandAmount.MinimumWidth = 125;
            this.txtColGrandAmount.Name = "txtColGrandAmount";
            this.txtColGrandAmount.ReadOnly = true;
            // 
            // Tax
            // 
            this.Tax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Tax.HeaderText = "Tax";
            this.Tax.Name = "Tax";
            this.Tax.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Tax.Visible = false;
            // 
            // Discount
            // 
            this.Discount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Discount.DefaultCellStyle = dataGridViewCellStyle9;
            this.Discount.FillWeight = 44.51458F;
            this.Discount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Discount.HeaderText = "Discount";
            this.Discount.MinimumWidth = 125;
            this.Discount.Name = "Discount";
            this.Discount.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // DiscountAmount
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscountAmount.DefaultCellStyle = dataGridViewCellStyle10;
            this.DiscountAmount.FillWeight = 44.51458F;
            this.DiscountAmount.HeaderText = "DiscountAmount";
            this.DiscountAmount.MaxInputLength = 10;
            this.DiscountAmount.MinimumWidth = 100;
            this.DiscountAmount.Name = "DiscountAmount";
            this.DiscountAmount.ReadOnly = true;
            // 
            // Total
            // 
            this.Total.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N2";
            this.Total.DefaultCellStyle = dataGridViewCellStyle11;
            this.Total.FillWeight = 44.51458F;
            this.Total.HeaderText = "Gross Amount";
            this.Total.MaxInputLength = 10;
            this.Total.MinimumWidth = 125;
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // SalesOrderID
            // 
            dataGridViewCellStyle12.Format = "N0";
            dataGridViewCellStyle12.NullValue = "0";
            this.SalesOrderID.DefaultCellStyle = dataGridViewCellStyle12;
            this.SalesOrderID.HeaderText = "SalesOrderID";
            this.SalesOrderID.Name = "SalesOrderID";
            this.SalesOrderID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SalesOrderID.Visible = false;
            // 
            // ItemID
            // 
            dataGridViewCellStyle13.Format = "N0";
            dataGridViewCellStyle13.NullValue = "0";
            this.ItemID.DefaultCellStyle = dataGridViewCellStyle13;
            this.ItemID.HeaderText = "ItemID";
            this.ItemID.Name = "ItemID";
            this.ItemID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemID.Visible = false;
            // 
            // TaxAmount
            // 
            this.TaxAmount.HeaderText = "TaxAmount";
            this.TaxAmount.MaxInputLength = 10;
            this.TaxAmount.Name = "TaxAmount";
            this.TaxAmount.Visible = false;
            // 
            // SelectedTaxID
            // 
            this.SelectedTaxID.HeaderText = "SelectedTaxID";
            this.SelectedTaxID.Name = "SelectedTaxID";
            this.SelectedTaxID.Visible = false;
            // 
            // SelectedDiscountID
            // 
            this.SelectedDiscountID.HeaderText = "SelectedDiscountID";
            this.SelectedDiscountID.Name = "SelectedDiscountID";
            this.SelectedDiscountID.Visible = false;
            // 
            // BatchID
            // 
            this.BatchID.HeaderText = "BatchID";
            this.BatchID.Name = "BatchID";
            this.BatchID.Visible = false;
            // 
            // IsGroup
            // 
            this.IsGroup.HeaderText = "IsGroup";
            this.IsGroup.Name = "IsGroup";
            this.IsGroup.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsGroup.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.IsGroup.Visible = false;
            // 
            // QtyAvailable
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.QtyAvailable.DefaultCellStyle = dataGridViewCellStyle14;
            this.QtyAvailable.HeaderText = "QtyAvailable";
            this.QtyAvailable.Name = "QtyAvailable";
            this.QtyAvailable.Visible = false;
            // 
            // IsGroupItem
            // 
            this.IsGroupItem.HeaderText = "IsGroupItem";
            this.IsGroupItem.Name = "IsGroupItem";
            this.IsGroupItem.Visible = false;
            // 
            // tiItemDetails
            // 
            this.tiItemDetails.AttachedControl = this.tabControlPanel1;
            this.tiItemDetails.Name = "tiItemDetails";
            this.tiItemDetails.Text = "Item Details";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.dgvLocationDetails);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(1017, 214);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            this.tabControlPanel2.TabItem = this.tiLocationDetails;
            // 
            // dgvLocationDetails
            // 
            this.dgvLocationDetails.AddNewRow = false;
            this.dgvLocationDetails.AlphaNumericCols = new int[0];
            this.dgvLocationDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvLocationDetails.CapsLockCols = new int[0];
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLocationDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dgvLocationDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocationDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LItemCode,
            this.LItemName,
            this.LItemID,
            this.LBatchID,
            this.LBatchNo,
            this.LQuantity,
            this.LAvailableQuantity,
            this.LUOM,
            this.LUOMID,
            this.LLocation,
            this.LRow,
            this.LBlock,
            this.LLot});
            this.dgvLocationDetails.DecimalCols = new int[0];
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLocationDetails.DefaultCellStyle = dataGridViewCellStyle19;
            this.dgvLocationDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLocationDetails.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvLocationDetails.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvLocationDetails.HasSlNo = false;
            this.dgvLocationDetails.LastRowIndex = 0;
            this.dgvLocationDetails.Location = new System.Drawing.Point(1, 1);
            this.dgvLocationDetails.Name = "dgvLocationDetails";
            this.dgvLocationDetails.NegativeValueCols = new int[0];
            this.dgvLocationDetails.NumericCols = new int[0];
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLocationDetails.RowHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.dgvLocationDetails.Size = new System.Drawing.Size(1015, 212);
            this.dgvLocationDetails.TabIndex = 1;
            // 
            // LItemCode
            // 
            this.LItemCode.HeaderText = "Item Code";
            this.LItemCode.Name = "LItemCode";
            this.LItemCode.Width = 60;
            // 
            // LItemName
            // 
            this.LItemName.HeaderText = "Item Name";
            this.LItemName.Name = "LItemName";
            this.LItemName.Width = 250;
            // 
            // LItemID
            // 
            this.LItemID.HeaderText = "ItemID";
            this.LItemID.Name = "LItemID";
            this.LItemID.Visible = false;
            // 
            // LBatchID
            // 
            this.LBatchID.HeaderText = "BatchID";
            this.LBatchID.Name = "LBatchID";
            this.LBatchID.Visible = false;
            // 
            // LBatchNo
            // 
            this.LBatchNo.HeaderText = "Batch No";
            this.LBatchNo.Name = "LBatchNo";
            this.LBatchNo.Width = 130;
            // 
            // LQuantity
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.LQuantity.DefaultCellStyle = dataGridViewCellStyle18;
            this.LQuantity.HeaderText = "Quantity";
            this.LQuantity.Name = "LQuantity";
            this.LQuantity.Width = 75;
            // 
            // LAvailableQuantity
            // 
            this.LAvailableQuantity.HeaderText = "Qty Available";
            this.LAvailableQuantity.Name = "LAvailableQuantity";
            this.LAvailableQuantity.ReadOnly = true;
            this.LAvailableQuantity.Width = 75;
            // 
            // LUOM
            // 
            this.LUOM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LUOM.HeaderText = "UOM";
            this.LUOM.Name = "LUOM";
            this.LUOM.ReadOnly = true;
            this.LUOM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LUOM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.LUOM.Width = 60;
            // 
            // LUOMID
            // 
            this.LUOMID.HeaderText = "UOMID";
            this.LUOMID.Name = "LUOMID";
            this.LUOMID.Visible = false;
            // 
            // LLocation
            // 
            this.LLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LLocation.HeaderText = "Location";
            this.LLocation.Name = "LLocation";
            // 
            // LRow
            // 
            this.LRow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LRow.HeaderText = "Row";
            this.LRow.Name = "LRow";
            // 
            // LBlock
            // 
            this.LBlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LBlock.HeaderText = "Block";
            this.LBlock.Name = "LBlock";
            // 
            // LLot
            // 
            this.LLot.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LLot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LLot.HeaderText = "Lot";
            this.LLot.Name = "LLot";
            // 
            // tiLocationDetails
            // 
            this.tiLocationDetails.AttachedControl = this.tabControlPanel2;
            this.tiLocationDetails.Name = "tiLocationDetails";
            this.tiLocationDetails.Text = "Location Details";
            this.tiLocationDetails.Visible = false;
            // 
            // panelGridBottom
            // 
            this.panelGridBottom.CanvasColor = System.Drawing.Color.Transparent;
            this.panelGridBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelGridBottom.Controls.Add(this.lblAmountIn);
            this.panelGridBottom.Controls.Add(this.buttonX3);
            this.panelGridBottom.Controls.Add(this.panelEx3);
            this.panelGridBottom.Controls.Add(this.btnRemove);
            this.panelGridBottom.Controls.Add(this.btnNewItem);
            this.panelGridBottom.Controls.Add(this.cboPaymentMode);
            this.panelGridBottom.Controls.Add(this.dtpDueDate);
            this.panelGridBottom.Controls.Add(this.txtExpense);
            this.panelGridBottom.Controls.Add(this.lblDueDate);
            this.panelGridBottom.Controls.Add(this.lblRemarks);
            this.panelGridBottom.Controls.Add(this.txtRemarks);
            this.panelGridBottom.Controls.Add(this.lblPaymentMode);
            this.panelGridBottom.Controls.Add(this.pnlBottom);
            this.panelGridBottom.Controls.Add(this.chkDelivered);
            this.panelGridBottom.Controls.Add(this.btnNew);
            this.panelGridBottom.Controls.Add(this.lblExpense);
            this.panelGridBottom.Controls.Add(this.btnSave);
            this.panelGridBottom.Controls.Add(this.lblAmountInWords);
            this.panelGridBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGridBottom.Location = new System.Drawing.Point(0, 230);
            this.panelGridBottom.Name = "panelGridBottom";
            this.panelGridBottom.Size = new System.Drawing.Size(1017, 199);
            this.panelGridBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelGridBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelGridBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelGridBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelGridBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelGridBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelGridBottom.Style.GradientAngle = 90;
            this.panelGridBottom.TabIndex = 2;
            // 
            // lblAmountIn
            // 
            this.lblAmountIn.AutoSize = true;
            this.lblAmountIn.Location = new System.Drawing.Point(2, 84);
            this.lblAmountIn.Name = "lblAmountIn";
            this.lblAmountIn.Size = new System.Drawing.Size(89, 13);
            this.lblAmountIn.TabIndex = 283;
            this.lblAmountIn.Text = "Amount In Words";
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Enabled = false;
            this.buttonX3.Location = new System.Drawing.Point(370, 115);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Size = new System.Drawing.Size(18, 26);
            this.buttonX3.TabIndex = 281;
            this.buttonX3.TabStop = false;
            this.buttonX3.Text = "...";
            this.buttonX3.Visible = false;
            // 
            // panelEx3
            // 
            this.panelEx3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.panelEx3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelEx3.CanvasColor = System.Drawing.Color.Transparent;
            this.panelEx3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx3.Controls.Add(this.txtNetAmount);
            this.panelEx3.Controls.Add(this.lblNetAmount);
            this.panelEx3.Controls.Add(this.lblTendered);
            this.panelEx3.Controls.Add(this.lblChange);
            this.panelEx3.Controls.Add(this.lblDiscountScheme);
            this.panelEx3.Controls.Add(this.txtSubTotal);
            this.panelEx3.Controls.Add(this.lblDiscountAmount);
            this.panelEx3.Controls.Add(this.txtDiscountAmount);
            this.panelEx3.Controls.Add(this.txtTotal);
            this.panelEx3.Controls.Add(this.txtTendered);
            this.panelEx3.Controls.Add(this.txtChange);
            this.panelEx3.Controls.Add(this.lblSubTotal);
            this.panelEx3.Controls.Add(this.lblTotal);
            this.panelEx3.Controls.Add(this.cboDiscountScheme);
            this.panelEx3.Location = new System.Drawing.Point(392, 4);
            this.panelEx3.Name = "panelEx3";
            this.panelEx3.Size = new System.Drawing.Size(622, 165);
            this.panelEx3.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx3.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx3.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx3.Style.GradientAngle = 90;
            this.panelEx3.TabIndex = 3;
            // 
            // txtNetAmount
            // 
            this.txtNetAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNetAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtNetAmount.Border.Class = "TextBoxBorder";
            this.txtNetAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtNetAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNetAmount.Location = new System.Drawing.Point(83, 7);
            this.txtNetAmount.MaxLength = 12;
            this.txtNetAmount.Name = "txtNetAmount";
            this.txtNetAmount.Size = new System.Drawing.Size(240, 44);
            this.txtNetAmount.TabIndex = 0;
            this.txtNetAmount.Text = "999999999.99";
            this.txtNetAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNetAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNetAmount_KeyPress);
            this.txtNetAmount.TextChanged += new System.EventHandler(this.txtNetAmount_TextChanged);
            this.txtNetAmount.Click += new System.EventHandler(this.txtNetAmount_Click);
            // 
            // lblNetAmount
            // 
            this.lblNetAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNetAmount.AutoSize = true;
            this.lblNetAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNetAmount.Location = new System.Drawing.Point(2, 14);
            this.lblNetAmount.Name = "lblNetAmount";
            this.lblNetAmount.Size = new System.Drawing.Size(81, 15);
            this.lblNetAmount.TabIndex = 275;
            this.lblNetAmount.Text = "Net Amount";
            // 
            // lblTendered
            // 
            this.lblTendered.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTendered.AutoSize = true;
            this.lblTendered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTendered.Location = new System.Drawing.Point(10, 68);
            this.lblTendered.Name = "lblTendered";
            this.lblTendered.Size = new System.Drawing.Size(68, 15);
            this.lblTendered.TabIndex = 276;
            this.lblTendered.Text = "Tendered";
            // 
            // lblChange
            // 
            this.lblChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChange.AutoSize = true;
            this.lblChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChange.Location = new System.Drawing.Point(22, 122);
            this.lblChange.Name = "lblChange";
            this.lblChange.Size = new System.Drawing.Size(56, 15);
            this.lblChange.TabIndex = 277;
            this.lblChange.Text = "Change";
            // 
            // lblDiscountScheme
            // 
            this.lblDiscountScheme.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDiscountScheme.AutoSize = true;
            this.lblDiscountScheme.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscountScheme.Location = new System.Drawing.Point(328, 51);
            this.lblDiscountScheme.Name = "lblDiscountScheme";
            this.lblDiscountScheme.Size = new System.Drawing.Size(83, 15);
            this.lblDiscountScheme.TabIndex = 278;
            this.lblDiscountScheme.Text = "Disc: Scheme";
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSubTotal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtSubTotal.Border.Class = "TextBoxBorder";
            this.txtSubTotal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubTotal.Location = new System.Drawing.Point(412, 7);
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.ReadOnly = true;
            this.txtSubTotal.Size = new System.Drawing.Size(205, 38);
            this.txtSubTotal.TabIndex = 262;
            this.txtSubTotal.TabStop = false;
            this.txtSubTotal.Text = "0.00";
            this.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSubTotal.TextChanged += new System.EventHandler(this.txtSubTotal_TextChanged);
            // 
            // lblDiscountAmount
            // 
            this.lblDiscountAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDiscountAmount.AutoSize = true;
            this.lblDiscountAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscountAmount.Location = new System.Drawing.Point(328, 86);
            this.lblDiscountAmount.Name = "lblDiscountAmount";
            this.lblDiscountAmount.Size = new System.Drawing.Size(55, 15);
            this.lblDiscountAmount.TabIndex = 277;
            this.lblDiscountAmount.Text = "Discount";
            // 
            // txtDiscountAmount
            // 
            this.txtDiscountAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiscountAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDiscountAmount.Border.Class = "TextBoxBorder";
            this.txtDiscountAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDiscountAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscountAmount.Location = new System.Drawing.Point(412, 81);
            this.txtDiscountAmount.MaxLength = 12;
            this.txtDiscountAmount.Name = "txtDiscountAmount";
            this.txtDiscountAmount.ReadOnly = true;
            this.txtDiscountAmount.Size = new System.Drawing.Size(205, 38);
            this.txtDiscountAmount.TabIndex = 264;
            this.txtDiscountAmount.TabStop = false;
            this.txtDiscountAmount.Text = "0.00";
            this.txtDiscountAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscountAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDiscountAmount_KeyPress);
            this.txtDiscountAmount.TextChanged += new System.EventHandler(this.txtDiscountAmount_TextChanged);
            // 
            // txtTotal
            // 
            this.txtTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTotal.Border.Class = "TextBoxBorder";
            this.txtTotal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(412, 124);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.Size = new System.Drawing.Size(205, 38);
            this.txtTotal.TabIndex = 270;
            this.txtTotal.TabStop = false;
            this.txtTotal.Text = "123456789.99";
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTendered
            // 
            this.txtTendered.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTendered.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTendered.Border.Class = "TextBoxBorder";
            this.txtTendered.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTendered.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTendered.Location = new System.Drawing.Point(83, 62);
            this.txtTendered.MaxLength = 12;
            this.txtTendered.Name = "txtTendered";
            this.txtTendered.Size = new System.Drawing.Size(240, 44);
            this.txtTendered.TabIndex = 1;
            this.txtTendered.Text = "123456789.12";
            this.txtTendered.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTendered.Enter += new System.EventHandler(this.txtTendered_Enter);
            this.txtTendered.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTendered_KeyPress);
            this.txtTendered.TextChanged += new System.EventHandler(this.txtTendered_TextChanged);
            this.txtTendered.Click += new System.EventHandler(this.txtTendered_Click);
            // 
            // txtChange
            // 
            this.txtChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtChange.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtChange.Border.Class = "TextBoxBorder";
            this.txtChange.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChange.Location = new System.Drawing.Point(83, 117);
            this.txtChange.MaxLength = 12;
            this.txtChange.Name = "txtChange";
            this.txtChange.ReadOnly = true;
            this.txtChange.Size = new System.Drawing.Size(240, 44);
            this.txtChange.TabIndex = 272;
            this.txtChange.TabStop = false;
            this.txtChange.Text = "999999999.99";
            this.txtChange.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblSubTotal
            // 
            this.lblSubTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubTotal.AutoSize = true;
            this.lblSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubTotal.Location = new System.Drawing.Point(328, 12);
            this.lblSubTotal.Name = "lblSubTotal";
            this.lblSubTotal.Size = new System.Drawing.Size(59, 15);
            this.lblSubTotal.TabIndex = 275;
            this.lblSubTotal.Text = "Sub Total";
            // 
            // lblTotal
            // 
            this.lblTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(328, 130);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(49, 15);
            this.lblTotal.TabIndex = 274;
            this.lblTotal.Text = "TOTAL";
            // 
            // cboDiscountScheme
            // 
            this.cboDiscountScheme.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cboDiscountScheme.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDiscountScheme.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDiscountScheme.DisplayMember = "Text";
            this.cboDiscountScheme.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDiscountScheme.DropDownHeight = 80;
            this.cboDiscountScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDiscountScheme.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDiscountScheme.FormattingEnabled = true;
            this.cboDiscountScheme.IntegralHeight = false;
            this.cboDiscountScheme.ItemHeight = 20;
            this.cboDiscountScheme.Location = new System.Drawing.Point(412, 50);
            this.cboDiscountScheme.Name = "cboDiscountScheme";
            this.cboDiscountScheme.Size = new System.Drawing.Size(205, 26);
            this.cboDiscountScheme.TabIndex = 261;
            this.cboDiscountScheme.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDiscountScheme.SelectedIndexChanged += new System.EventHandler(this.cboDiscountScheme_SelectedIndexChanged);
            this.cboDiscountScheme.Enter += new System.EventHandler(this.cboDiscountScheme_Enter);
            this.cboDiscountScheme.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboDiscountScheme_KeyPress);
            // 
            // btnRemove
            // 
            this.btnRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRemove.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.Location = new System.Drawing.Point(83, 115);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(69, 50);
            this.btnRemove.TabIndex = 280;
            this.btnRemove.Text = "<div align=\"center\"><font color=\"green\"><b>F3</b></font><br></br>\r\nRemove  </div>" +
                "";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnNewItem
            // 
            this.btnNewItem.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnNewItem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNewItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnNewItem.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnNewItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewItem.Location = new System.Drawing.Point(8, 115);
            this.btnNewItem.Name = "btnNewItem";
            this.btnNewItem.Size = new System.Drawing.Size(69, 50);
            this.btnNewItem.TabIndex = 0;
            this.btnNewItem.TabStop = false;
            this.btnNewItem.Text = "<div align=\"center\"><font color=\"green\"><b>F2</b></font><br></br>\r\nNew Item</div>" +
                "";
            this.btnNewItem.Click += new System.EventHandler(this.btnNewItem_Click);
            // 
            // cboPaymentMode
            // 
            this.cboPaymentMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cboPaymentMode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboPaymentMode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboPaymentMode.DisplayMember = "Text";
            this.cboPaymentMode.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboPaymentMode.DropDownHeight = 75;
            this.cboPaymentMode.FormattingEnabled = true;
            this.cboPaymentMode.IntegralHeight = false;
            this.cboPaymentMode.ItemHeight = 14;
            this.cboPaymentMode.Location = new System.Drawing.Point(94, 11);
            this.cboPaymentMode.Name = "cboPaymentMode";
            this.cboPaymentMode.Size = new System.Drawing.Size(256, 20);
            this.cboPaymentMode.TabIndex = 0;
            this.cboPaymentMode.SelectedIndexChanged += new System.EventHandler(this.cboPaymentMode_SelectedIndexChanged);
            this.cboPaymentMode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboPaymentMode_KeyPress);
            // 
            // txtExpense
            // 
            this.txtExpense.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtExpense.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtExpense.Border.Class = "TextBoxBorder";
            this.txtExpense.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtExpense.Enabled = false;
            this.txtExpense.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExpense.Location = new System.Drawing.Point(356, 115);
            this.txtExpense.Name = "txtExpense";
            this.txtExpense.ReadOnly = true;
            this.txtExpense.Size = new System.Drawing.Size(14, 26);
            this.txtExpense.TabIndex = 263;
            this.txtExpense.TabStop = false;
            this.txtExpense.Text = "0.00";
            this.txtExpense.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtExpense.Visible = false;
            this.txtExpense.TextChanged += new System.EventHandler(this.txtExpense_TextChanged);
            // 
            // lblRemarks
            // 
            this.lblRemarks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(2, 50);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 237;
            this.lblRemarks.Text = "Remarks";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            // 
            // 
            // 
            this.txtRemarks.Border.Class = "TextBoxBorder";
            this.txtRemarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtRemarks.Location = new System.Drawing.Point(94, 35);
            this.txtRemarks.MaxLength = 200;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(256, 42);
            this.txtRemarks.TabIndex = 1;
            this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
            // 
            // lblPaymentMode
            // 
            this.lblPaymentMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblPaymentMode.AutoSize = true;
            this.lblPaymentMode.Location = new System.Drawing.Point(2, 16);
            this.lblPaymentMode.Name = "lblPaymentMode";
            this.lblPaymentMode.Size = new System.Drawing.Size(78, 13);
            this.lblPaymentMode.TabIndex = 236;
            this.lblPaymentMode.Text = "Payment Mode";
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.wbCreatedBy);
            this.pnlBottom.Controls.Add(this.wbCreatedDate);
            this.pnlBottom.Controls.Add(this.wbStatus);
            this.pnlBottom.Controls.Add(this.lblPOSstatus);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 173);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1017, 26);
            this.pnlBottom.TabIndex = 247;
            // 
            // wbCreatedBy
            // 
            this.wbCreatedBy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.wbCreatedBy.CloseButtonVisible = false;
            this.wbCreatedBy.Dock = System.Windows.Forms.DockStyle.Right;
            this.wbCreatedBy.Location = new System.Drawing.Point(265, 0);
            this.wbCreatedBy.Name = "wbCreatedBy";
            this.wbCreatedBy.OptionsButtonVisible = false;
            this.wbCreatedBy.Size = new System.Drawing.Size(250, 24);
            this.wbCreatedBy.TabIndex = 264;
            this.wbCreatedBy.Text = "Cashier";
            // 
            // wbCreatedDate
            // 
            this.wbCreatedDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.wbCreatedDate.CloseButtonVisible = false;
            this.wbCreatedDate.Dock = System.Windows.Forms.DockStyle.Right;
            this.wbCreatedDate.Location = new System.Drawing.Point(515, 0);
            this.wbCreatedDate.Name = "wbCreatedDate";
            this.wbCreatedDate.OptionsButtonVisible = false;
            this.wbCreatedDate.Size = new System.Drawing.Size(250, 24);
            this.wbCreatedDate.TabIndex = 263;
            this.wbCreatedDate.Text = "Bill Date";
            // 
            // wbStatus
            // 
            this.wbStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.wbStatus.CloseButtonVisible = false;
            this.wbStatus.Dock = System.Windows.Forms.DockStyle.Right;
            this.wbStatus.Location = new System.Drawing.Point(765, 0);
            this.wbStatus.Name = "wbStatus";
            this.wbStatus.OptionsButtonVisible = false;
            this.wbStatus.Size = new System.Drawing.Size(250, 24);
            this.wbStatus.TabIndex = 261;
            this.wbStatus.Text = "Status";
            // 
            // lblPOSstatus
            // 
            this.lblPOSstatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblPOSstatus.CloseButtonVisible = false;
            this.lblPOSstatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPOSstatus.Image = ((System.Drawing.Image)(resources.GetObject("lblPOSstatus.Image")));
            this.lblPOSstatus.Location = new System.Drawing.Point(0, 0);
            this.lblPOSstatus.Name = "lblPOSstatus";
            this.lblPOSstatus.OptionsButtonVisible = false;
            this.lblPOSstatus.Size = new System.Drawing.Size(1015, 24);
            this.lblPOSstatus.TabIndex = 20;
            // 
            // btnNew
            // 
            this.btnNew.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnNew.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.Location = new System.Drawing.Point(158, 115);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(93, 50);
            this.btnNew.TabIndex = 3;
            this.btnNew.Text = "<div align=\"center\"><font color=\"green\"><b>F5</b></font><br></br>\r\nNew Sale</div>" +
                "";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // lblExpense
            // 
            this.lblExpense.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblExpense.AutoSize = true;
            this.lblExpense.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpense.Location = new System.Drawing.Point(353, 126);
            this.lblExpense.Name = "lblExpense";
            this.lblExpense.Size = new System.Drawing.Size(55, 15);
            this.lblExpense.TabIndex = 276;
            this.lblExpense.Text = "Expense";
            this.lblExpense.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(257, 115);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(93, 50);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "<div align=\"center\"><font color=\"green\"><b>F12</b></font><br></br>\r\nPrint  &&  Sa" +
                "ve</div>";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblAmountInWords
            // 
            this.lblAmountInWords.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountInWords.Location = new System.Drawing.Point(94, 82);
            this.lblAmountInWords.Name = "lblAmountInWords";
            this.lblAmountInWords.Size = new System.Drawing.Size(256, 29);
            this.lblAmountInWords.TabIndex = 284;
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx1.Controls.Add(this.panelBottom);
            this.panelEx1.Controls.Add(this.panelTop);
            this.panelEx1.Controls.Add(this.PurchaseOrderBindingNavigator);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(3, 25);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(1017, 519);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 0;
            this.panelEx1.Text = "panelEx1";
            // 
            // dockSite2
            // 
            this.dockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite2.Location = new System.Drawing.Point(1020, 0);
            this.dockSite2.Name = "dockSite2";
            this.dockSite2.Size = new System.Drawing.Size(0, 544);
            this.dockSite2.TabIndex = 27;
            this.dockSite2.TabStop = false;
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(0, 0);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 544);
            this.dockSite1.TabIndex = 26;
            this.dockSite1.TabStop = false;
            // 
            // dotNetBarManager1
            // 
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.dotNetBarManager1.BottomDockSite = this.dockSite4;
            this.dotNetBarManager1.EnableFullSizeDock = false;
            this.dotNetBarManager1.LeftDockSite = this.dockSite1;
            this.dotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.dotNetBarManager1.ParentForm = null;
            this.dotNetBarManager1.RightDockSite = this.dockSite2;
            this.dotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.dotNetBarManager1.ToolbarBottomDockSite = this.dockSite8;
            this.dotNetBarManager1.ToolbarLeftDockSite = this.dockSite5;
            this.dotNetBarManager1.ToolbarRightDockSite = this.dockSite6;
            this.dotNetBarManager1.ToolbarTopDockSite = this.dockSite7;
            this.dotNetBarManager1.TopDockSite = this.dockSite3;
            // 
            // dockSite4
            // 
            this.dockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite4.Location = new System.Drawing.Point(0, 544);
            this.dockSite4.Name = "dockSite4";
            this.dockSite4.Size = new System.Drawing.Size(1020, 0);
            this.dockSite4.TabIndex = 29;
            this.dockSite4.TabStop = false;
            // 
            // dockSite8
            // 
            this.dockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite8.Location = new System.Drawing.Point(0, 544);
            this.dockSite8.Name = "dockSite8";
            this.dockSite8.Size = new System.Drawing.Size(1020, 0);
            this.dockSite8.TabIndex = 33;
            this.dockSite8.TabStop = false;
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(0, 0);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 544);
            this.dockSite5.TabIndex = 30;
            this.dockSite5.TabStop = false;
            // 
            // dockSite6
            // 
            this.dockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite6.Location = new System.Drawing.Point(1020, 0);
            this.dockSite6.Name = "dockSite6";
            this.dockSite6.Size = new System.Drawing.Size(0, 544);
            this.dockSite6.TabIndex = 31;
            this.dockSite6.TabStop = false;
            // 
            // dockSite3
            // 
            this.dockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite3.Location = new System.Drawing.Point(0, 0);
            this.dockSite3.Name = "dockSite3";
            this.dockSite3.Size = new System.Drawing.Size(1020, 0);
            this.dockSite3.TabIndex = 28;
            this.dockSite3.TabStop = false;
            // 
            // expandableSplitterLeftPOS
            // 
            this.expandableSplitterLeftPOS.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeftPOS.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeftPOS.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeftPOS.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeftPOS.Expanded = false;
            this.expandableSplitterLeftPOS.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeftPOS.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeftPOS.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeftPOS.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeftPOS.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeftPOS.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeftPOS.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeftPOS.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeftPOS.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterLeftPOS.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterLeftPOS.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeftPOS.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeftPOS.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeftPOS.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeftPOS.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeftPOS.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeftPOS.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeftPOS.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeftPOS.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeftPOS.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeftPOS.Location = new System.Drawing.Point(0, 25);
            this.expandableSplitterLeftPOS.Name = "expandableSplitterLeftPOS";
            this.expandableSplitterLeftPOS.Size = new System.Drawing.Size(3, 519);
            this.expandableSplitterLeftPOS.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeftPOS.TabIndex = 25;
            this.expandableSplitterLeftPOS.TabStop = false;
            this.expandableSplitterLeftPOS.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitterLeftPOS_ExpandedChanged);
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.DgvBillNo);
            this.PanelLeft.Controls.Add(this.expandablePanel1);
            this.PanelLeft.Controls.Add(this.LblSCountStatus);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 25);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(175, 519);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 24;
            this.PanelLeft.Text = "panelEx1";
            this.PanelLeft.Visible = false;
            // 
            // DgvBillNo
            // 
            this.DgvBillNo.AllowUserToAddRows = false;
            this.DgvBillNo.AllowUserToDeleteRows = false;
            this.DgvBillNo.AllowUserToResizeColumns = false;
            this.DgvBillNo.AllowUserToResizeRows = false;
            this.DgvBillNo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvBillNo.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvBillNo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.DgvBillNo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvBillNo.DefaultCellStyle = dataGridViewCellStyle22;
            this.DgvBillNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvBillNo.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DgvBillNo.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvBillNo.Location = new System.Drawing.Point(0, 249);
            this.DgvBillNo.Name = "DgvBillNo";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvBillNo.RowHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.DgvBillNo.RowHeadersVisible = false;
            this.DgvBillNo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvBillNo.Size = new System.Drawing.Size(175, 244);
            this.DgvBillNo.TabIndex = 112;
            this.DgvBillNo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvBillNo_CellClick);
            this.DgvBillNo.CurrentCellDirtyStateChanged += new System.EventHandler(this.DgvBillNo_CurrentCellDirtyStateChanged);
            this.DgvBillNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DgvBillNo_KeyPress);
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expandablePanel1.Controls.Add(this.panelEx2);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(175, 249);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 111;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Search";
            // 
            // panelEx2
            // 
            this.panelEx2.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx2.Controls.Add(this.lnkLabel);
            this.panelEx2.Controls.Add(this.cboCounterSearch);
            this.panelEx2.Controls.Add(this.lblCounterSearch);
            this.panelEx2.Controls.Add(this.lblBillNoSearch);
            this.panelEx2.Controls.Add(this.dtpSToSearch);
            this.panelEx2.Controls.Add(this.dtpSFromSearch);
            this.panelEx2.Controls.Add(this.lblToSearch);
            this.panelEx2.Controls.Add(this.lblFromSearch);
            this.panelEx2.Controls.Add(this.cboCashierSearch);
            this.panelEx2.Controls.Add(this.lblCashierSearch);
            this.panelEx2.Controls.Add(this.btnSearch);
            this.panelEx2.Controls.Add(this.txtBillNoSearch);
            this.panelEx2.Controls.Add(this.cboStatusSearch);
            this.panelEx2.Controls.Add(this.cboCustomerSearch);
            this.panelEx2.Controls.Add(this.cboCompanySearch);
            this.panelEx2.Controls.Add(this.lblCompanySearch);
            this.panelEx2.Controls.Add(this.lblCustomerSearch);
            this.panelEx2.Controls.Add(this.lblStatusSearch);
            this.panelEx2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEx2.Location = new System.Drawing.Point(0, 26);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(175, 223);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 110;
            // 
            // lnkLabel
            // 
            this.lnkLabel.AutoSize = true;
            this.lnkLabel.Location = new System.Drawing.Point(3, 205);
            this.lnkLabel.Name = "lnkLabel";
            this.lnkLabel.Size = new System.Drawing.Size(87, 13);
            this.lnkLabel.TabIndex = 260;
            this.lnkLabel.TabStop = true;
            this.lnkLabel.Text = "Advance Search";
            this.lnkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLabel_LinkClicked);
            // 
            // cboCounterSearch
            // 
            this.cboCounterSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboCounterSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCounterSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCounterSearch.DisplayMember = "Text";
            this.cboCounterSearch.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCounterSearch.DropDownHeight = 75;
            this.cboCounterSearch.FormattingEnabled = true;
            this.cboCounterSearch.IntegralHeight = false;
            this.cboCounterSearch.ItemHeight = 14;
            this.cboCounterSearch.Location = new System.Drawing.Point(58, 33);
            this.cboCounterSearch.Name = "cboCounterSearch";
            this.cboCounterSearch.Size = new System.Drawing.Size(111, 20);
            this.cboCounterSearch.TabIndex = 257;
            // 
            // lblCounterSearch
            // 
            this.lblCounterSearch.AutoSize = true;
            this.lblCounterSearch.Location = new System.Drawing.Point(3, 36);
            this.lblCounterSearch.Name = "lblCounterSearch";
            this.lblCounterSearch.Size = new System.Drawing.Size(44, 13);
            this.lblCounterSearch.TabIndex = 258;
            this.lblCounterSearch.Text = "Counter";
            // 
            // lblBillNoSearch
            // 
            this.lblBillNoSearch.AutoSize = true;
            this.lblBillNoSearch.Location = new System.Drawing.Point(3, 174);
            this.lblBillNoSearch.Name = "lblBillNoSearch";
            this.lblBillNoSearch.Size = new System.Drawing.Size(37, 13);
            this.lblBillNoSearch.TabIndex = 256;
            this.lblBillNoSearch.Text = "Bill No";
            // 
            // dtpSToSearch
            // 
            this.dtpSToSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpSToSearch.CustomFormat = "dd-MMM-yyyy";
            this.dtpSToSearch.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSToSearch.Location = new System.Drawing.Point(58, 148);
            this.dtpSToSearch.Name = "dtpSToSearch";
            this.dtpSToSearch.Size = new System.Drawing.Size(111, 20);
            this.dtpSToSearch.TabIndex = 255;
            // 
            // dtpSFromSearch
            // 
            this.dtpSFromSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpSFromSearch.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFromSearch.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFromSearch.Location = new System.Drawing.Point(58, 125);
            this.dtpSFromSearch.Name = "dtpSFromSearch";
            this.dtpSFromSearch.Size = new System.Drawing.Size(111, 20);
            this.dtpSFromSearch.TabIndex = 254;
            // 
            // lblToSearch
            // 
            this.lblToSearch.AutoSize = true;
            this.lblToSearch.Location = new System.Drawing.Point(3, 151);
            this.lblToSearch.Name = "lblToSearch";
            this.lblToSearch.Size = new System.Drawing.Size(20, 13);
            this.lblToSearch.TabIndex = 253;
            this.lblToSearch.Text = "To";
            // 
            // lblFromSearch
            // 
            this.lblFromSearch.AutoSize = true;
            this.lblFromSearch.Location = new System.Drawing.Point(3, 128);
            this.lblFromSearch.Name = "lblFromSearch";
            this.lblFromSearch.Size = new System.Drawing.Size(30, 13);
            this.lblFromSearch.TabIndex = 252;
            this.lblFromSearch.Text = "From";
            // 
            // cboCashierSearch
            // 
            this.cboCashierSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboCashierSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCashierSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCashierSearch.DisplayMember = "Text";
            this.cboCashierSearch.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCashierSearch.DropDownHeight = 75;
            this.cboCashierSearch.FormattingEnabled = true;
            this.cboCashierSearch.IntegralHeight = false;
            this.cboCashierSearch.ItemHeight = 14;
            this.cboCashierSearch.Location = new System.Drawing.Point(58, 56);
            this.cboCashierSearch.Name = "cboCashierSearch";
            this.cboCashierSearch.Size = new System.Drawing.Size(111, 20);
            this.cboCashierSearch.TabIndex = 21;
            // 
            // lblCashierSearch
            // 
            this.lblCashierSearch.AutoSize = true;
            this.lblCashierSearch.Location = new System.Drawing.Point(3, 59);
            this.lblCashierSearch.Name = "lblCashierSearch";
            this.lblCashierSearch.Size = new System.Drawing.Size(53, 13);
            this.lblCashierSearch.TabIndex = 126;
            this.lblCashierSearch.Text = "Employee";
            // 
            // btnSearch
            // 
            this.btnSearch.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSearch.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSearch.Location = new System.Drawing.Point(98, 195);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(71, 23);
            this.btnSearch.TabIndex = 24;
            this.btnSearch.Text = "Refresh";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtBillNoSearch
            // 
            this.txtBillNoSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtBillNoSearch.Border.Class = "TextBoxBorder";
            this.txtBillNoSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtBillNoSearch.Location = new System.Drawing.Point(58, 171);
            this.txtBillNoSearch.MaxLength = 20;
            this.txtBillNoSearch.Name = "txtBillNoSearch";
            this.txtBillNoSearch.Size = new System.Drawing.Size(111, 20);
            this.txtBillNoSearch.TabIndex = 19;
            this.txtBillNoSearch.WatermarkEnabled = false;
            this.txtBillNoSearch.WatermarkText = "Sales Return No";
            // 
            // cboStatusSearch
            // 
            this.cboStatusSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboStatusSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboStatusSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboStatusSearch.DisplayMember = "Text";
            this.cboStatusSearch.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboStatusSearch.DropDownHeight = 75;
            this.cboStatusSearch.FormattingEnabled = true;
            this.cboStatusSearch.IntegralHeight = false;
            this.cboStatusSearch.ItemHeight = 14;
            this.cboStatusSearch.Location = new System.Drawing.Point(58, 102);
            this.cboStatusSearch.Name = "cboStatusSearch";
            this.cboStatusSearch.Size = new System.Drawing.Size(111, 20);
            this.cboStatusSearch.TabIndex = 23;
            // 
            // cboCustomerSearch
            // 
            this.cboCustomerSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboCustomerSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCustomerSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCustomerSearch.DisplayMember = "Text";
            this.cboCustomerSearch.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCustomerSearch.DropDownHeight = 75;
            this.cboCustomerSearch.FormattingEnabled = true;
            this.cboCustomerSearch.IntegralHeight = false;
            this.cboCustomerSearch.ItemHeight = 14;
            this.cboCustomerSearch.Location = new System.Drawing.Point(58, 79);
            this.cboCustomerSearch.Name = "cboCustomerSearch";
            this.cboCustomerSearch.Size = new System.Drawing.Size(111, 20);
            this.cboCustomerSearch.TabIndex = 22;
            // 
            // cboCompanySearch
            // 
            this.cboCompanySearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboCompanySearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompanySearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompanySearch.DisplayMember = "Text";
            this.cboCompanySearch.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompanySearch.DropDownHeight = 75;
            this.cboCompanySearch.FormattingEnabled = true;
            this.cboCompanySearch.IntegralHeight = false;
            this.cboCompanySearch.ItemHeight = 14;
            this.cboCompanySearch.Location = new System.Drawing.Point(58, 10);
            this.cboCompanySearch.Name = "cboCompanySearch";
            this.cboCompanySearch.Size = new System.Drawing.Size(111, 20);
            this.cboCompanySearch.TabIndex = 20;
            this.cboCompanySearch.SelectedIndexChanged += new System.EventHandler(this.cboCompanySearch_SelectedIndexChanged);
            // 
            // lblCompanySearch
            // 
            this.lblCompanySearch.AutoSize = true;
            this.lblCompanySearch.Location = new System.Drawing.Point(3, 13);
            this.lblCompanySearch.Name = "lblCompanySearch";
            this.lblCompanySearch.Size = new System.Drawing.Size(51, 13);
            this.lblCompanySearch.TabIndex = 109;
            this.lblCompanySearch.Text = "Company";
            // 
            // lblCustomerSearch
            // 
            this.lblCustomerSearch.AutoSize = true;
            this.lblCustomerSearch.Location = new System.Drawing.Point(3, 82);
            this.lblCustomerSearch.Name = "lblCustomerSearch";
            this.lblCustomerSearch.Size = new System.Drawing.Size(51, 13);
            this.lblCustomerSearch.TabIndex = 108;
            this.lblCustomerSearch.Text = "Customer";
            // 
            // lblStatusSearch
            // 
            this.lblStatusSearch.AutoSize = true;
            this.lblStatusSearch.Location = new System.Drawing.Point(3, 105);
            this.lblStatusSearch.Name = "lblStatusSearch";
            this.lblStatusSearch.Size = new System.Drawing.Size(37, 13);
            this.lblStatusSearch.TabIndex = 107;
            this.lblStatusSearch.Text = "Status";
            // 
            // LblSCountStatus
            // 
            // 
            // 
            // 
            this.LblSCountStatus.BackgroundStyle.Class = "";
            this.LblSCountStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblSCountStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LblSCountStatus.Location = new System.Drawing.Point(0, 493);
            this.LblSCountStatus.Name = "LblSCountStatus";
            this.LblSCountStatus.Size = new System.Drawing.Size(175, 26);
            this.LblSCountStatus.TabIndex = 0;
            this.LblSCountStatus.Text = "...";
            // 
            // CommandZoom
            // 
            this.CommandZoom.Name = "CommandZoom";
            // 
            // dockContainerItem1
            // 
            this.dockContainerItem1.Name = "dockContainerItem1";
            this.dockContainerItem1.Text = "dockContainerItem1";
            // 
            // tmrCreatedDateTime
            // 
            this.tmrCreatedDateTime.Interval = 1000;
            this.tmrCreatedDateTime.Tick += new System.EventHandler(this.tmrCreatedDateTime_Tick);
            // 
            // tmrMessage
            // 
            this.tmrMessage.Interval = 1500;
            this.tmrMessage.Tick += new System.EventHandler(this.tmrMessage_Tick);
            // 
            // clmItemCode
            // 
            this.clmItemCode.HeaderText = "Item Code";
            this.clmItemCode.Name = "clmItemCode";
            this.clmItemCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmItemCode.Width = 150;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.HeaderText = "BatchID";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.Visible = false;
            // 
            // clmItemName
            // 
            this.clmItemName.HeaderText = "Item Name";
            this.clmItemName.Name = "clmItemName";
            this.clmItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmItemName.Width = 180;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.HeaderText = "PurchaseOrderID";
            this.dataGridViewTextBoxColumn21.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn21.Visible = false;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.HeaderText = "SelectedDiscountID";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.Visible = false;
            // 
            // clmQuantity
            // 
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmQuantity.DefaultCellStyle = dataGridViewCellStyle24;
            this.clmQuantity.HeaderText = "Quantity";
            this.clmQuantity.MaxInputLength = 6;
            this.clmQuantity.Name = "clmQuantity";
            this.clmQuantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn22.Visible = false;
            // 
            // clmOrderDetailID
            // 
            this.clmOrderDetailID.HeaderText = "OrderDetailID";
            this.clmOrderDetailID.Name = "clmOrderDetailID";
            this.clmOrderDetailID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmOrderDetailID.Visible = false;
            // 
            // clmQtyReceived
            // 
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmQtyReceived.DefaultCellStyle = dataGridViewCellStyle25;
            this.clmQtyReceived.HeaderText = "QtyReceived";
            this.clmQtyReceived.MaxInputLength = 6;
            this.clmQtyReceived.Name = "clmQtyReceived";
            this.clmQtyReceived.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmStatus
            // 
            this.clmStatus.HeaderText = "Status";
            this.clmStatus.Name = "clmStatus";
            this.clmStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmSalesOrderID
            // 
            this.clmSalesOrderID.HeaderText = "SalesOrderID";
            this.clmSalesOrderID.Name = "clmSalesOrderID";
            this.clmSalesOrderID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmSalesOrderID.Visible = false;
            // 
            // clmBatchNumber
            // 
            this.clmBatchNumber.HeaderText = "Batch Number";
            this.clmBatchNumber.Name = "clmBatchNumber";
            this.clmBatchNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmBatchNumber.Visible = false;
            // 
            // clmTotal
            // 
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmTotal.DefaultCellStyle = dataGridViewCellStyle26;
            this.clmTotal.HeaderText = "Total";
            this.clmTotal.Name = "clmTotal";
            this.clmTotal.ReadOnly = true;
            this.clmTotal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.HeaderText = "OrderDetailID";
            this.dataGridViewTextBoxColumn20.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn20.Visible = false;
            // 
            // clmRate
            // 
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmRate.DefaultCellStyle = dataGridViewCellStyle27;
            this.clmRate.HeaderText = "Rate";
            this.clmRate.MaxInputLength = 6;
            this.clmRate.Name = "clmRate";
            this.clmRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn19
            // 
            dataGridViewCellStyle28.Format = "N0";
            dataGridViewCellStyle28.NullValue = "0";
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle28;
            this.dataGridViewTextBoxColumn19.HeaderText = "Status";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn19.Visible = false;
            // 
            // dataGridViewTextBoxColumn18
            // 
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn18.DefaultCellStyle = dataGridViewCellStyle29;
            this.dataGridViewTextBoxColumn18.HeaderText = "Total";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn18.Visible = false;
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle30;
            this.dataGridViewTextBoxColumn16.HeaderText = "Batch Number";
            this.dataGridViewTextBoxColumn16.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle31;
            this.dataGridViewTextBoxColumn15.HeaderText = "Return Quantity";
            this.dataGridViewTextBoxColumn15.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle32;
            this.dataGridViewTextBoxColumn17.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn17.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Width = 150;
            // 
            // dataGridViewTextBoxColumn14
            // 
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle33;
            this.dataGridViewTextBoxColumn14.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn14.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "Item Name";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Width = 180;
            // 
            // clmItemID
            // 
            this.clmItemID.HeaderText = "ItemID";
            this.clmItemID.Name = "clmItemID";
            this.clmItemID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmItemID.Visible = false;
            // 
            // CntxtHistory
            // 
            this.CntxtHistory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ShowItemHistory,
            this.ShowSaleRateHistory,
            this.showGroupItemDetails});
            this.CntxtHistory.Name = "CntxtHistory";
            this.CntxtHistory.Size = new System.Drawing.Size(205, 92);
            // 
            // ShowItemHistory
            // 
            this.ShowItemHistory.Name = "ShowItemHistory";
            this.ShowItemHistory.Size = new System.Drawing.Size(204, 22);
            this.ShowItemHistory.Text = "Show Item History";
            this.ShowItemHistory.Click += new System.EventHandler(this.ShowItemHistory_Click);
            // 
            // ShowSaleRateHistory
            // 
            this.ShowSaleRateHistory.Name = "ShowSaleRateHistory";
            this.ShowSaleRateHistory.Size = new System.Drawing.Size(204, 22);
            this.ShowSaleRateHistory.Text = "Show Sale Rates";
            this.ShowSaleRateHistory.Visible = false;
            this.ShowSaleRateHistory.Click += new System.EventHandler(this.ShowSaleRateHistory_Click);
            // 
            // showGroupItemDetails
            // 
            this.showGroupItemDetails.Name = "showGroupItemDetails";
            this.showGroupItemDetails.Size = new System.Drawing.Size(204, 22);
            this.showGroupItemDetails.Text = "Show Group Item Details";
            this.showGroupItemDetails.Click += new System.EventHandler(this.showGroupItemDetails_Click);
            // 
            // FrmPOS
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.ClientSize = new System.Drawing.Size(1020, 544);
            this.Controls.Add(this.panelEx1);
            this.Controls.Add(this.expandableSplitterLeftPOS);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.dockSite7);
            this.Controls.Add(this.dockSite2);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.dockSite3);
            this.Controls.Add(this.dockSite4);
            this.Controls.Add(this.dockSite5);
            this.Controls.Add(this.dockSite6);
            this.Controls.Add(this.dockSite8);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmPOS";
            this.Text = "Point of Sale";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmPointofSale_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPOS_FormClosing);
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.dockSite7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrSales)).EndInit();
            this.panelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcPOS)).EndInit();
            this.tcPOS.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPOS)).EndInit();
            this.tabControlPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocationDetails)).EndInit();
            this.panelGridBottom.ResumeLayout(false);
            this.panelGridBottom.PerformLayout();
            this.panelEx3.ResumeLayout(false);
            this.panelEx3.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            this.panelEx1.ResumeLayout(false);
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvBillNo)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            this.panelEx2.ResumeLayout(false);
            this.panelEx2.PerformLayout();
            this.CntxtHistory.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.DockSite dockSite7;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.ButtonItem btnActions;
        private DevComponents.DotNetBar.ButtonItem btnInstallments;
        private DevComponents.DotNetBar.ButtonItem btnExpense;
        private DevComponents.DotNetBar.ButtonItem bnSubmitForApproval;
        private DevComponents.DotNetBar.ButtonItem btnApprove;
        private DevComponents.DotNetBar.ButtonItem btnReject;
        private DevComponents.DotNetBar.ButtonItem btnDocuments;
        private DevComponents.DotNetBar.ButtonItem BindingNavigatorClearItem;
        private DevComponents.DotNetBar.ButtonItem BindingNavigatorCancelItem;
        private DevComponents.DotNetBar.ButtonItem BindingNavigatorDeleteItem;
        private DevComponents.DotNetBar.ButtonItem BtnDiscount;
        private DevComponents.DotNetBar.ButtonItem BtnItem;
        private DevComponents.DotNetBar.ButtonItem BtnTax;
        private DevComponents.DotNetBar.ButtonItem BindingNavigatorSaveItem;
        private DevComponents.DotNetBar.ButtonItem BindingNavigatorAddNewItem;
        private DevComponents.DotNetBar.Bar PurchaseOrderBindingNavigator;
        private DevComponents.DotNetBar.ButtonItem BtnUom;
        private DevComponents.DotNetBar.ButtonItem btnAccountSettings;
        private DevComponents.DotNetBar.ButtonItem BtnPrint;
        private DevComponents.DotNetBar.ButtonItem BtnEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmOrderDetailID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmQtyReceived;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmSalesOrderID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmBatchNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        internal System.Windows.Forms.Timer TmrFocus;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        internal System.Windows.Forms.Timer TmrPOS;
        internal System.Windows.Forms.ErrorProvider ErrSales;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.PanelEx panelBottom;
        private ClsInnerGridBar dgvPOS;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterLeftPOS;
        private DevComponents.DotNetBar.PanelEx PanelLeft;
        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.LabelX LblSCountStatus;
        private DevComponents.DotNetBar.DockSite dockSite2;
        private DevComponents.DotNetBar.DockSite dockSite1;
        private DevComponents.DotNetBar.DockSite dockSite3;
        private DevComponents.DotNetBar.DockSite dockSite4;
        private DevComponents.DotNetBar.DockSite dockSite5;
        private DevComponents.DotNetBar.DockSite dockSite6;
        private DevComponents.DotNetBar.DockSite dockSite8;
        internal System.Windows.Forms.ContextMenuStrip CMSVendorAddress;
        private System.Windows.Forms.ImageList ImgSales;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.ComponentModel.BackgroundWorker bckgrndWrkrSendSms;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private DevComponents.DotNetBar.DotNetBarManager dotNetBarManager1;
        public DevComponents.DotNetBar.Command CommandZoom;
        private DevComponents.DotNetBar.DockContainerItem dockContainerItem1;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmItemID;
        private DevComponents.DotNetBar.ButtonItem BtnHelp;
        private DevComponents.DotNetBar.PanelEx panelEx2;
        private System.Windows.Forms.Label lblBillNoSearch;
        private System.Windows.Forms.DateTimePicker dtpSToSearch;
        private System.Windows.Forms.DateTimePicker dtpSFromSearch;
        private System.Windows.Forms.Label lblToSearch;
        private System.Windows.Forms.Label lblFromSearch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCashierSearch;
        private System.Windows.Forms.Label lblCashierSearch;
        private DevComponents.DotNetBar.ButtonX btnSearch;
        private DevComponents.DotNetBar.Controls.TextBoxX txtBillNoSearch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboStatusSearch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCustomerSearch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompanySearch;
        private System.Windows.Forms.Label lblCompanySearch;
        private System.Windows.Forms.Label lblCustomerSearch;
        private System.Windows.Forms.Label lblStatusSearch;
        private DevComponents.DotNetBar.PanelEx panelTop;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.Label lblEmployeeNames;
        private System.Windows.Forms.Label lblDate;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private System.Windows.Forms.Label lblCompany;
        private DevComponents.DotNetBar.Controls.TextBoxX txtBillNumber;
        private System.Windows.Forms.DateTimePicker dtpPOSDate;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCustomer;
        private DevComponents.DotNetBar.ButtonX btnVendor;
        private System.Windows.Forms.Label lblBillNumber;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkDelivered;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCounter;
        private System.Windows.Forms.Label lblCounter;
        private System.Windows.Forms.Label lblDueDate;
        private System.Windows.Forms.DateTimePicker dtpDueDate;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCounterSearch;
        private System.Windows.Forms.Label lblCounterSearch;
        private DevComponents.DotNetBar.Controls.DataGridViewX DgvBillNo;
        private DevComponents.DotNetBar.PanelEx panelGridBottom;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.ButtonX btnRemove;
        private DevComponents.DotNetBar.ButtonX btnNewItem;
        private System.Windows.Forms.Label lblDiscountScheme;
        private System.Windows.Forms.Label lblDiscountAmount;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboPaymentMode;
        private System.Windows.Forms.Label lblRemarks;
        private DevComponents.DotNetBar.Controls.TextBoxX txtRemarks;
        private System.Windows.Forms.Label lblPaymentMode;
        private System.Windows.Forms.Label lblExpense;
        private System.Windows.Forms.Label lblTotal;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDiscountScheme;
        private System.Windows.Forms.Label lblSubTotal;
        private DevComponents.DotNetBar.Controls.TextBoxX txtChange;
        private DevComponents.DotNetBar.Controls.TextBoxX txtNetAmount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTendered;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTotal;
        private System.Windows.Forms.Panel pnlBottom;
        private DevComponents.DotNetBar.Controls.WarningBox wbCreatedBy;
        private DevComponents.DotNetBar.Controls.WarningBox wbCreatedDate;
        private DevComponents.DotNetBar.Controls.WarningBox wbStatus;
        private DevComponents.DotNetBar.Controls.WarningBox lblPOSstatus;
        private DevComponents.DotNetBar.ButtonX btnNew;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDiscountAmount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtExpense;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSubTotal;
        private System.Windows.Forms.Label lblChange;
        private System.Windows.Forms.Label lblTendered;
        private System.Windows.Forms.Label lblNetAmount;
        private DevComponents.DotNetBar.PanelEx panelEx3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboWarehouse;
        private System.Windows.Forms.Label lblWarehouse;
        private System.Windows.Forms.Timer tmrCreatedDateTime;
        private System.Windows.Forms.Timer tmrMessage;
        private DevComponents.DotNetBar.TabControl tcPOS;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private DevComponents.DotNetBar.TabItem tiItemDetails;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private ClsInnerGridBar dgvLocationDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn LItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn LItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LItemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn LBatchID;
        private System.Windows.Forms.DataGridViewTextBoxColumn LBatchNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn LQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn LAvailableQuantity;
        private System.Windows.Forms.DataGridViewComboBoxColumn LUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn LUOMID;
        private System.Windows.Forms.DataGridViewComboBoxColumn LLocation;
        private System.Windows.Forms.DataGridViewComboBoxColumn LRow;
        private System.Windows.Forms.DataGridViewComboBoxColumn LBlock;
        private System.Windows.Forms.DataGridViewComboBoxColumn LLot;
        private DevComponents.DotNetBar.TabItem tiLocationDetails;
        private DevComponents.DotNetBar.ButtonX btnCounterReference;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCancelReason;
        private System.Windows.Forms.Label lblCancelReason;
        private System.Windows.Forms.Label lblAmountIn;
        private System.Windows.Forms.Label lblAmountInWords;
        private System.Windows.Forms.LinkLabel lnkLabel;
        private System.Windows.Forms.ContextMenuStrip CntxtHistory;
        private System.Windows.Forms.ToolStripMenuItem ShowItemHistory;
        private System.Windows.Forms.ToolStripMenuItem ShowSaleRateHistory;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewComboBoxColumn Uom;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtColGrandAmount;
        private System.Windows.Forms.DataGridViewComboBoxColumn Tax;
        private System.Windows.Forms.DataGridViewComboBoxColumn Discount;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscountAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesOrderID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn SelectedTaxID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SelectedDiscountID;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtyAvailable;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsGroupItem;
        private System.Windows.Forms.ToolStripMenuItem showGroupItemDetails;
    }
}