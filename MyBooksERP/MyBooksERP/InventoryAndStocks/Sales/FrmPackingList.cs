﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmPackingList : DevComponents.DotNetBar.Office2007Form
    {
        clsBLLPackingList objclsBLLPackingList;
        clsBLLCommonUtility MobjClsCommonUtility;
        bool blnAddStatus;
        public long plngInvoiceID;
        public string pstrInvoiceNo;
        public string pstrCompany;
        public int pintCurrencyID;
        public string pstrCurrency;
        public int intScale;
        public string pstrAccountOf1;
        public string pstrAccountOf2;
        public DataTable datInvoiceDetails;

        public FrmPackingList()
        {
            InitializeComponent();
            objclsBLLPackingList = new clsBLLPackingList();
            MobjClsCommonUtility = new clsBLLCommonUtility();
            datInvoiceDetails = new DataTable();
            datInvoiceDetails.Columns.Add("Item");
            datInvoiceDetails.Columns.Add("Quantity");
            datInvoiceDetails.Columns.Add("UOM");
            datInvoiceDetails.Columns.Add("UnitPrice");
            datInvoiceDetails.Columns.Add("Total");
        }

        private void FrmPackingList_Load(object sender, EventArgs e)
        {
            LoadDetails();
        }

        private void LoadDetails()
        {
            AddNew();

            objclsBLLPackingList.clsDTOPackingList.lngInvoiceID = plngInvoiceID;
            DataSet dsTemp = objclsBLLPackingList.GetDetails();

            if (dsTemp != null)
            {
                if (dsTemp.Tables.Count > 0)
                {
                    if (dsTemp.Tables[0] != null)
                    {
                        if (dsTemp.Tables[0].Rows.Count > 0)
                        {
                            blnAddStatus = false;
                            txtInvoiceNo.Tag = plngInvoiceID;
                            dtpDate.Value = dsTemp.Tables[0].Rows[0]["InvoiceDate"].ToDateTime();
                            txtHSCode.Text = dsTemp.Tables[0].Rows[0]["HSCode"].ToStringCustom();
                            txtInvoiceNo.Text = dsTemp.Tables[0].Rows[0]["InvoiceNo"].ToStringCustom();
                            txtGrossWeight.Text = dsTemp.Tables[0].Rows[0]["GrossWeight"].ToStringCustom();
                            txtNetWeight.Text = dsTemp.Tables[0].Rows[0]["NetWeight"].ToStringCustom();
                            txtAccountOf1.Text = dsTemp.Tables[0].Rows[0]["AccountOf1"].ToStringCustom();
                            txtAccountOf2.Text = dsTemp.Tables[0].Rows[0]["AccountOf2"].ToStringCustom();
                            txtAccountOf3.Text = dsTemp.Tables[0].Rows[0]["AccountOf3"].ToStringCustom();
                            txtDrawnUnder1.Text = dsTemp.Tables[0].Rows[0]["DrawnUnder1"].ToStringCustom();
                            txtDrawnUnder2.Text = dsTemp.Tables[0].Rows[0]["DrawnUnder2"].ToStringCustom();
                            txtDrawnUnder3.Text = dsTemp.Tables[0].Rows[0]["DrawnUnder3"].ToStringCustom();
                            txtGoodsDescription.Text = dsTemp.Tables[0].Rows[0]["Description"].ToStringCustom();
                            txtShippingMarks.Text = dsTemp.Tables[0].Rows[0]["ShippingMarks"].ToStringCustom();
                            txtTradeMark.Text = dsTemp.Tables[0].Rows[0]["TradeMark"].ToStringCustom();
                            txtManufacturer.Text = dsTemp.Tables[0].Rows[0]["ManufactureName"].ToStringCustom();

                            cboNationality.SelectedValue = dsTemp.Tables[0].Rows[0]["Nationality"].ToStringCustom();
                            if (cboNationality.SelectedIndex == -1)
                                cboNationality.Text = dsTemp.Tables[0].Rows[0]["Nationality"].ToStringCustom();

                            txtAddress.Text = dsTemp.Tables[0].Rows[0]["Address"].ToStringCustom();
                            txtTelephone.Text = dsTemp.Tables[0].Rows[0]["Telephone"].ToStringCustom();
                            txtUOM.Text = dsTemp.Tables[0].Rows[0]["UOM"].ToStringCustom();
                            txtTotalUOM.Text = dsTemp.Tables[0].Rows[0]["TotalUOM"].ToStringCustom();
                            diFOBValue.Text = dsTemp.Tables[0].Rows[0]["FOBValue"].ToStringCustom();
                            diFreightCharge.Text = dsTemp.Tables[0].Rows[0]["FreightCharge"].ToStringCustom();
                            txtCFR.Text = dsTemp.Tables[0].Rows[0]["CFR"].ToStringCustom();
                            diTotalCFR.Text = dsTemp.Tables[0].Rows[0]["TotalCFR"].ToStringCustom();
                            txtDeclaration.Text = dsTemp.Tables[0].Rows[0]["Declaration"].ToStringCustom();
                            txtGrossTotal.Text = dsTemp.Tables[0].Rows[0]["GrossTotal"].ToStringCustom();
                            diLCPercentage.Text = dsTemp.Tables[0].Rows[0]["LCPercentage"].ToStringCustom();
                            txtNetTotal.Text = dsTemp.Tables[0].Rows[0]["NetTotal"].ToStringCustom(); 
                        }
                    }

                    if (dsTemp.Tables[1] != null)
                    {
                        if (dsTemp.Tables[1].Rows.Count > 0)
                        {
                            dgvDetails.Rows.Clear();

                            for (int iCounter = 0; iCounter <= dsTemp.Tables[1].Rows.Count - 1; iCounter++)
                            {
                                dgvDetails.Rows.Add();
                                dgvDetails.Rows[iCounter].Cells["NoOfRolls"].Value = dsTemp.Tables[1].Rows[iCounter]["NoOfRolls"].ToStringCustom();
                                dgvDetails.Rows[iCounter].Cells["Roll"].Value = dsTemp.Tables[1].Rows[iCounter]["Roll"].ToDecimal();
                                dgvDetails.Rows[iCounter].Cells["Item"].Value = dsTemp.Tables[1].Rows[iCounter]["Item"].ToStringCustom();
                                dgvDetails.Rows[iCounter].Cells["Quantity"].Value = dsTemp.Tables[1].Rows[iCounter]["Quantity"].ToDecimal();
                                dgvDetails.Rows[iCounter].Cells["UOM"].Value = dsTemp.Tables[1].Rows[iCounter]["UOM"].ToStringCustom();
                                dgvDetails.Rows[iCounter].Cells["UnitPrice"].Value = dsTemp.Tables[1].Rows[iCounter]["UnitPrice"].ToDecimal();
                                dgvDetails.Rows[iCounter].Cells["EachCartoon"].Value = dsTemp.Tables[1].Rows[iCounter]["EachCartoon"].ToStringCustom();
                                dgvDetails.Rows[iCounter].Cells["Total"].Value = dsTemp.Tables[1].Rows[iCounter]["Total"].ToDecimal();
                            }
                        }
                    }
                }
            }
        }

        private void AddNew()
        {
            blnAddStatus = true;
            txtInvoiceNo.Tag = plngInvoiceID;
            txtInvoiceNo.Text = pstrInvoiceNo;
            lblCurrencyValue.Text = pstrCurrency;
            txtAccountOf1.Text = pstrAccountOf1;
            txtAccountOf2.Text = pstrAccountOf2;
            dtpDate.Value = ClsCommonSettings.GetServerDate();
            txtHSCode.Text = txtGrossWeight.Text = txtNetWeight.Text = "";
            txtAccountOf3.Text = "";
            txtDrawnUnder1.Text = txtDrawnUnder2.Text = txtDrawnUnder3.Text = "";
            txtGoodsDescription.Text = txtShippingMarks.Text = txtTradeMark.Text = txtManufacturer.Text = "";
            LoadNationality();
            cboNationality.SelectedIndex = -1;
            txtUOM.Text = "Rolls";
            txtAddress.Text = txtTelephone.Text = txtTotalUOM.Text = "";
            diFOBValue.Text = diFreightCharge.Text = diTotalCFR.Text = diLCPercentage.Text = "0";
            txtCFR.Text = txtDeclaration.Text = txtGrossTotal.Text = txtNetTotal.Text = "";
            lblAmountInWordsValue.Text = "";
            dgvDetails.Rows.Clear();

            for (int iCounter = 0; iCounter <= datInvoiceDetails.Rows.Count - 1; iCounter++)
            {
                dgvDetails.Rows.Add();
                dgvDetails.Rows[iCounter].Cells["NoOfRolls"].Value = "";
                dgvDetails.Rows[iCounter].Cells["Roll"].Value = "";
                dgvDetails.Rows[iCounter].Cells["Item"].Value = datInvoiceDetails.Rows[iCounter]["Item"].ToStringCustom();
                dgvDetails.Rows[iCounter].Cells["Quantity"].Value = datInvoiceDetails.Rows[iCounter]["Quantity"].ToDecimal();
                dgvDetails.Rows[iCounter].Cells["UOM"].Value = datInvoiceDetails.Rows[iCounter]["UOM"].ToStringCustom();
                dgvDetails.Rows[iCounter].Cells["UnitPrice"].Value = datInvoiceDetails.Rows[iCounter]["UnitPrice"].ToDecimal();
                dgvDetails.Rows[iCounter].Cells["EachCartoon"].Value = "";
                dgvDetails.Rows[iCounter].Cells["Total"].Value = datInvoiceDetails.Rows[iCounter]["Total"].ToDecimal();
            }
        }

        private void LoadNationality()
        {
            cboNationality.DataSource = MobjClsCommonUtility.FillCombos(new string[] { "CountryName AS Country", "CountryReference", "" });
            cboNationality.ValueMember = "Country";
            cboNationality.DisplayMember = "Country";
        }

        private void btnNationality_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objFrmCommonRef = new FrmCommonRef("Country", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "CountryID,CountryName As Country", "CountryReference", "");
                objFrmCommonRef.ShowDialog();
                objFrmCommonRef.Dispose();
                string strCountry = Convert.ToString(cboNationality.SelectedValue);
                LoadNationality();
                if (objFrmCommonRef.NewID != 0)
                    cboNationality.SelectedValue = objFrmCommonRef.NewID;
                else
                    cboNationality.SelectedValue = strCountry;
            }
            catch { }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to save the details?", ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (SaveDetails())
                {
                    string strMessage = "";

                    if (blnAddStatus)
                        strMessage = "Details saved successfully";
                    else
                        strMessage = "Details updated successfully";
                    
                    MessageBox.Show(strMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblStatus.Text = strMessage;
                    tmrStatus.Enabled = true;
                    blnAddStatus = false;
                }
            }
        }

        private bool SaveDetails()
        {
            FillParameters();
            FillParameterDetails();

            if (objclsBLLPackingList.SaveCommercialInvoice(blnAddStatus))
                return true;
            else
                return false;
        }

        private void FillParameters()
        {
            objclsBLLPackingList.clsDTOPackingList.lngInvoiceID = txtInvoiceNo.Tag.ToInt64();
            objclsBLLPackingList.clsDTOPackingList.dtInvoiceDate = (dtpDate.Value.ToString("dd MMM yyyy")).ToDateTime();
            objclsBLLPackingList.clsDTOPackingList.strHSCode = txtHSCode.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strInvoiceNo = txtInvoiceNo.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strGrossWeight = txtGrossWeight.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strNetWeight = txtNetWeight.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strAccountOf1 = txtAccountOf1.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strAccountOf2 = txtAccountOf2.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strAccountOf3 = txtAccountOf3.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strDrawnUnder1 = txtDrawnUnder1.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strDrawnUnder2 = txtDrawnUnder2.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strDrawnUnder3 = txtDrawnUnder3.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strDescription = txtGoodsDescription.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strShippingMarks = txtShippingMarks.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strTradeMark = txtTradeMark.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strManufactureName = txtManufacturer.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strNationality = cboNationality.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strAddress = txtAddress.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strTelephone = txtTelephone.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.strUOM = txtUOM.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.decTotalUOM = txtTotalUOM.Text.ToDecimal();
            objclsBLLPackingList.clsDTOPackingList.decFOBValue = diFOBValue.Text.ToDecimal();
            objclsBLLPackingList.clsDTOPackingList.decFreightCharge = diFreightCharge.Text.ToDecimal();
            objclsBLLPackingList.clsDTOPackingList.strCFR = txtCFR.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.decTotalCFR = diTotalCFR.Text.ToDecimal();
            objclsBLLPackingList.clsDTOPackingList.strDeclaration = txtDeclaration.Text.Trim();
            objclsBLLPackingList.clsDTOPackingList.decGrossTotal = txtGrossTotal.Text.ToDecimal();
            objclsBLLPackingList.clsDTOPackingList.decLCPercentage = diLCPercentage.Text.ToDecimal();
            objclsBLLPackingList.clsDTOPackingList.decNetTotal = txtNetTotal.Text.ToDecimal();
        }

        private void FillParameterDetails()
        {
            objclsBLLPackingList.clsDTOPackingList.lstclsDTOPackingListDetails = new List<clsDTOPackingListDetails>();

            for (int iCounter = 0; iCounter <= dgvDetails.Rows.Count - 2; iCounter++)
            {
                clsDTOPackingListDetails objclsDTOPackingListDetails = new clsDTOPackingListDetails();
                objclsDTOPackingListDetails.strNoofRolls = dgvDetails.Rows[iCounter].Cells["NoOfRolls"].Value.ToStringCustom();
                objclsDTOPackingListDetails.decRoll = dgvDetails.Rows[iCounter].Cells["Roll"].Value.ToDecimal();
                objclsDTOPackingListDetails.strItem = dgvDetails.Rows[iCounter].Cells["Item"].Value.ToStringCustom();
                objclsDTOPackingListDetails.decQuantity = dgvDetails.Rows[iCounter].Cells["Quantity"].Value.ToDecimal();
                objclsDTOPackingListDetails.strUOM = dgvDetails.Rows[iCounter].Cells["UOM"].Value.ToStringCustom();
                objclsDTOPackingListDetails.decUnitPrice = dgvDetails.Rows[iCounter].Cells["UnitPrice"].Value.ToDecimal();
                objclsDTOPackingListDetails.strEachCartoon = dgvDetails.Rows[iCounter].Cells["EachCartoon"].Value.ToStringCustom();
                objclsDTOPackingListDetails.decTotal = dgvDetails.Rows[iCounter].Cells["Total"].Value.ToDecimal();
                objclsBLLPackingList.clsDTOPackingList.lstclsDTOPackingListDetails.Add(objclsDTOPackingListDetails);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to delete the details?", ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (DeleteDetails())
                {
                    string strMessage = "Details deleted successfully";
                    MessageBox.Show(strMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblStatus.Text = strMessage;
                    tmrStatus.Enabled = true;
                    AddNew();
                }
            }
        }

        private bool DeleteDetails()
        {
            if (objclsBLLPackingList.DeleteCI())
                return true;
            else
                return false;
        }

        private void tmrStatus_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            tmrStatus.Enabled = false;
        }

        private void dgvDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvDetails.Columns["Quantity"].Index || e.ColumnIndex == dgvDetails.Columns["UnitPrice"].Index)
                    CalculateTotal();
                else if (e.ColumnIndex == dgvDetails.Columns["Roll"].Index)
                    CalculateTotalUom();
            }
        }

        private void CalculateTotal()
        {
            decimal decTotal = 0, decLCPercentage = 0;

            for (int iCounter = 0; iCounter <= dgvDetails.Rows.Count - 1; iCounter++)
            {
                dgvDetails.Rows[iCounter].Cells["Total"].Value = (dgvDetails.Rows[iCounter].Cells["Quantity"].Value.ToDecimal() *
                    dgvDetails.Rows[iCounter].Cells["UnitPrice"].Value.ToDecimal()).ToString("F" + intScale);

                decTotal += dgvDetails.Rows[iCounter].Cells["Total"].Value.ToDecimal();
            }

            txtGrossTotal.Text = decTotal.ToString("F" + intScale);
            txtNetTotal.Text = decTotal.ToString("F" + intScale);
            decLCPercentage = diLCPercentage.Text.ToDecimal();

            if (decLCPercentage > 0)
                txtNetTotal.Text = (decTotal * (decLCPercentage / 100)).ToString("F" + intScale);

            //txtNetTotal.Text = (decTotal - (decTotal * (decLCPercentage / 100))).ToString("F" + intScale);
        }

        private void CalculateTotalUom()
        {
            decimal decTotalUOM = 0;

            for (int iCounter = 0; iCounter <= dgvDetails.Rows.Count - 1; iCounter++)
                decTotalUOM += dgvDetails.Rows[iCounter].Cells["Roll"].Value.ToDecimal();

            txtTotalUOM.Text = decTotalUOM.ToStringCustom();
        }

        private void diLCPercentage_ValueChanged(object sender, EventArgs e)
        {
            CalculateTotal();
        }

        private void dgvDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvDetails.IsCurrentCellDirty)
            {
                if (dgvDetails.CurrentCell != null)
                    dgvDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dgvDetails_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjClsCommonUtility.SetSerialNo(dgvDetails, e.RowIndex, false);
        }

        private void dgvDetails_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            CalculateTotal();
            MobjClsCommonUtility.SetSerialNo(dgvDetails, e.RowIndex, false);
        }

        private void txtNetTotal_TextChanged(object sender, EventArgs e)
        {
            lblAmountInWordsValue.Text = MobjClsCommonUtility.ConvertToWord(txtNetTotal.Text, pintCurrencyID);
        }

        private void txtUOM_TextChanged(object sender, EventArgs e)
        {
            if (txtUOM.Text.Trim() == "")
                txtUOM.Text = "Rolls";

            lblTotalUOM.Text = "Total " + txtUOM.Text;
            dgvDetails.Columns["NoOfRolls"].HeaderText = "No. of " + txtUOM.Text.Trim();
            dgvDetails.Columns["Roll"].HeaderText = "Total " + txtUOM.Text.Trim();
        }

        private void cboNationality_TextChanged(object sender, EventArgs e)
        {
            txtDeclaration.Text = "We Certify That Goods are of " + cboNationality.Text.Trim() + " Origin and Cintents to be True and Correct.";
        }

        private void btnPackingList_Click(object sender, EventArgs e)
        {
            ShowPrint((int)FormID.PackingList);
        }

        private void btnCommercialInvoice_Click(object sender, EventArgs e)
        {
            ShowPrint((int)FormID.CommercialInvoice);
        }

        private void ShowPrint(int intFormID)
        {
            FrmReportviewer ObjViewer = new FrmReportviewer();
            ObjViewer.PiRecId = plngInvoiceID;
            ObjViewer.PiFormID = intFormID;
            ObjViewer.strCompany = pstrCompany;
            ObjViewer.strCurrency = pstrCurrency;
            ObjViewer.strAmountInWord = lblAmountInWordsValue.Text;            
            ObjViewer.ShowDialog();
        }
    }
}