﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmSaleRateHistory : Form
    {
        public int ItemID;
        public int CompanyID;

        clsBLLSTSales objclsBLLSTSales;

        public FrmSaleRateHistory()
        {
            InitializeComponent();
            objclsBLLSTSales = new clsBLLSTSales();
        }

        private void FrmSaleRateHistory_Load(object sender, EventArgs e)
        {
            dgvSaleRateHistory.DataSource = objclsBLLSTSales.GetSaleRates(ItemID, CompanyID);
        }
    }
}
