using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data;
using System.Data.SqlClient;

using System.Collections.Generic;


using System.Text;

namespace MyBooksERP
{
    /// <summary>
    /// Summary description for frmDocument.
    /// </summary>
    public class FrmCreditNote : DevComponents.DotNetBar.Office2007Form
    {
        #region Controls Declaration
        public Command CommandZoom;
        private DockContainerItem dockContainerItem1;
        private ExpandableSplitter expandableSplitterLeft;
        private PanelEx PanelLeft;
        private IContainer components;
        private clsBLLCommonUtility MobjclsBLLCommonUtility;

        private DotNetBarManager dotNetBarManager1;
        private DockSite dockSite4;
        private DockSite dockSite1;
        private DockSite dockSite2;
        private DockSite dockSite3;
        private DockSite dockSite5;
        private DockSite dockSite6;
        private Bar bar1;
        private DockSite dockSite8;
        private PanelEx panelEx1;
        private Bar PurchaseOrderBindingNavigator;
        private ButtonItem BindingNavigatorAddNewItem;
        private ExpandableSplitter expandableSplitterTop;
        private PanelEx panelTop;
        private ExpandablePanel expandablePanel1;
        private LabelX lblSCountStatus;
        private PanelEx panelBottom;
        private PanelEx panelGridBottom;
        private PanelEx panelLeftTop;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvSalesReturnDisplay;
        private Label LblScompany;
        private Label LblSCustomer;
        private Label LblSStatus;
        private ClsInnerGridBar dgvSales;
        private ButtonItem BindingNavigatorSaveItem;
        private ButtonItem BindingNavigatorClearItem;
        private ButtonItem BindingNavigatorDeleteItem;
        private ButtonX BtnSRefresh;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtSsearch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboSStatus;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboSCustomer;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboSCompany;
        private System.Windows.Forms.DateTimePicker dtpReturnDate;
        private Label lblVendorAddress;
        private Label lblStatus;
        private Label lblDate;
        private Label lblReturnNo;
        internal Timer TmrSalesReturn;
        internal Timer TmrFocus;
        internal ErrorProvider ErrSalesReturn;
        private ButtonItem BtnPrint;
        private ButtonItem BtnEmail;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCustomer;
        public DevComponents.DotNetBar.Controls.TextBoxX txtVendorAddress;
        private DevComponents.DotNetBar.Controls.TextBoxX txtReturnNo;
        private Label lblDescription;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDescription;
        private ImageList ImgSalesReturn;

        //private bool MbViewPermission = false;
        private bool MbAddPermission = false;
        private bool MbUpdatePermission = false;
        private bool MbDeletePermission = false;
        private bool MbCancelPermission = false;
        private bool MbPrintEmailPermission = false;
        //private bool MBlnIsFromCancellation = false;
        private bool MBlnEditMode = false;
        private bool blnCalculate = true;
        private bool blnCanShow = true;
        private bool MblnIsFromClear = false;
        private int MintExchangeCurrencyScale = 0;
        private string MsMessageCommon;
        private bool MblnIsFromCancellation = false;

        private DataTable datMessages;                  // Error Message display
        private DataTable MaStatusMessage;
        private DataTable datWarehouse;
        decimal decDiscountPerItem = 0;
        private MessageBoxIcon MmessageIcon;

        ClsLogWriter mObjLogs;
        clsBLLSalesReturnMaster MobjclsBLLSalesReturn;
        clsBLLPermissionSettings objClsBLLPermissionSettings;

        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCurrency;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private Label lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboInvoiceNo;
        private Label lblInvoiceNo;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private DataGridViewTextBoxColumn clmItemCode;
        private DataGridViewTextBoxColumn clmItemName;
        private DataGridViewTextBoxColumn clmQuantity;
        private DataGridViewTextBoxColumn clmQtyReceived;
        private DataGridViewTextBoxColumn clmBatchNumber;
        private DataGridViewTextBoxColumn clmRate;
        private DataGridViewTextBoxColumn clmTotal;
        private DataGridViewTextBoxColumn clmStatus;
        private DataGridViewTextBoxColumn clmOrderDetailID;
        private DataGridViewTextBoxColumn clmSalesOrderID;
        private DataGridViewTextBoxColumn clmItemID;
        ClsNotificationNew mObjNotification;
        private Label lblCurrency;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private Label lblStatusLabel;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSExecutive;
        private Label lblExecutive;
        private PanelEx panelEx2;
        private Label lblTotalAmount;
        private Label lblDiscount;
        private Label lblSubtotal;
        private DevComponents.DotNetBar.Controls.TextBoxX txtExchangeCurrencyRate;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDiscount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSubtotal;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTotalAmount;
        private Label lblCustomer;
        private DevComponents.DotNetBar.Controls.TextBoxX txtRemarks;
        private Label lblPaymentRemarks;
        private Label lblGrandReturnAmount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGrandReturnAmount;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private Label lblSalesNo;
        private System.Windows.Forms.DateTimePicker dtpSTo;
        private System.Windows.Forms.DateTimePicker dtpSFrom;
        private Label lblTo;
        private Label lblFrom;
        private ButtonItem btnCustomerHistory;
        private Panel pnlBottom;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedByText;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedDateValue;
        private DevComponents.DotNetBar.Controls.WarningBox lblSalesReturnStatus;
        private DevComponents.DotNetBar.TabControl tcGeneral;
        private TabControlPanel tabControlPanel3;
        private TabItem tiGeneral;
        private DevComponents.DotNetBar.TabControl tcSalesReturn;
        private TabControlPanel tabControlPanel1;
        private TabItem tpItemDetails;
        private Label lblAmountIn;
        private Label lblAmountInWords;
        private LinkLabel lnkLabel;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboAccount;
        private Label lblAccount;
        private ButtonItem BindingNavigatorCancelItem;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDiscount;
        private Label label1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtExpense;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DataGridViewTextBoxColumn ItemCode;
        private DataGridViewTextBoxColumn ItemName;
        private DataGridViewTextBoxColumn ItemID;
        private DataGridViewTextBoxColumn ReferenceSerialNo;
        private DataGridViewTextBoxColumn BatchID;
        private DataGridViewTextBoxColumn BatchNo;
        private DataGridViewTextBoxColumn InvoicedQuantity;
        private DataGridViewTextBoxColumn InvQty;
        private DataGridViewTextBoxColumn InvUomID;
        private DataGridViewTextBoxColumn InvRate;
        private DataGridViewTextBoxColumn IssuedUOMID;
        private DataGridViewTextBoxColumn ReturnedQty;
        private DataGridViewTextBoxColumn ReturnQuantity;
        private DataGridViewComboBoxColumn Uom;
        private DataGridViewTextBoxColumn Rate;
        private DataGridViewTextBoxColumn GrandAmount;
        private DataGridViewComboBoxColumn Discount;
        private DataGridViewTextBoxColumn DiscountAmount;
        private DataGridViewTextBoxColumn NetAmount;
        private DataGridViewTextBoxColumn ReturnAmount;
        private DataGridViewComboBoxColumn Reason;
        private DataGridViewTextBoxColumn WarehouseID;
        private DataGridViewTextBoxColumn IsGroup;
        private DataGridViewTextBoxColumn SalesInvoicedQuantity;
        private DataGridViewTextBoxColumn SalesInvoiceDiscountAmount;
        private DataGridViewTextBoxColumn SalesInvoiceRate;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboTaxScheme;
        private Label label2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTaxAmount;


        public long PlngReferenceID;
        #endregion

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCreditNote));
            this.CommandZoom = new DevComponents.DotNetBar.Command(this.components);
            this.dockContainerItem1 = new DevComponents.DotNetBar.DockContainerItem();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvSalesReturnDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelLeftTop = new DevComponents.DotNetBar.PanelEx();
            this.lnkLabel = new System.Windows.Forms.LinkLabel();
            this.lblSalesNo = new System.Windows.Forms.Label();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cboSExecutive = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblExecutive = new System.Windows.Forms.Label();
            this.BtnSRefresh = new DevComponents.DotNetBar.ButtonX();
            this.TxtSsearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.CboSStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboSCustomer = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboSCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.LblScompany = new System.Windows.Forms.Label();
            this.LblSCustomer = new System.Windows.Forms.Label();
            this.LblSStatus = new System.Windows.Forms.Label();
            this.lblSCountStatus = new DevComponents.DotNetBar.LabelX();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.dotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.dockSite4 = new DevComponents.DotNetBar.DockSite();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.dockSite2 = new DevComponents.DotNetBar.DockSite();
            this.dockSite8 = new DevComponents.DotNetBar.DockSite();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.dockSite6 = new DevComponents.DotNetBar.DockSite();
            this.dockSite3 = new DevComponents.DotNetBar.DockSite();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.panelBottom = new DevComponents.DotNetBar.PanelEx();
            this.tcSalesReturn = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvSales = new ClsInnerGridBar();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReferenceSerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvoicedQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvUomID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IssuedUOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Uom = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GrandAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Discount = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.DiscountAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Reason = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.WarehouseID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesInvoicedQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesInvoiceDiscountAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesInvoiceRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpItemDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.panelGridBottom = new DevComponents.DotNetBar.PanelEx();
            this.cboDiscount = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.cboAccount = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblAccount = new System.Windows.Forms.Label();
            this.lblAmountInWords = new System.Windows.Forms.Label();
            this.lblAmountIn = new System.Windows.Forms.Label();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.lblCreatedByText = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblCreatedDateValue = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblSalesReturnStatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.label1 = new System.Windows.Forms.Label();
            this.txtExpense = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblGrandReturnAmount = new System.Windows.Forms.Label();
            this.txtGrandReturnAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.lblDiscount = new System.Windows.Forms.Label();
            this.lblSubtotal = new System.Windows.Forms.Label();
            this.txtSubtotal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtTotalAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtDiscount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtTaxAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtExchangeCurrencyRate = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtRemarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblPaymentRemarks = new System.Windows.Forms.Label();
            this.expandableSplitterTop = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.tcGeneral = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.cboTaxScheme = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.label2 = new System.Windows.Forms.Label();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.lblReturnNo = new System.Windows.Forms.Label();
            this.lblVendorAddress = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblStatusLabel = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.dtpReturnDate = new System.Windows.Forms.DateTimePicker();
            this.cboInvoiceNo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboCustomer = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblInvoiceNo = new System.Windows.Forms.Label();
            this.txtVendorAddress = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboCurrency = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtReturnNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtDescription = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblCompany = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.tiGeneral = new DevComponents.DotNetBar.TabItem(this.components);
            this.PurchaseOrderBindingNavigator = new DevComponents.DotNetBar.Bar();
            this.BindingNavigatorAddNewItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorSaveItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorClearItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorDeleteItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorCancelItem = new DevComponents.DotNetBar.ButtonItem();
            this.btnCustomerHistory = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.BtnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.TmrSalesReturn = new System.Windows.Forms.Timer(this.components);
            this.TmrFocus = new System.Windows.Forms.Timer(this.components);
            this.ErrSalesReturn = new System.Windows.Forms.ErrorProvider(this.components);
            this.ImgSalesReturn = new System.Windows.Forms.ImageList(this.components);
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmQtyReceived = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmBatchNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmOrderDetailID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmSalesOrderID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesReturnDisplay)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.panelEx1.SuspendLayout();
            this.panelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcSalesReturn)).BeginInit();
            this.tcSalesReturn.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSales)).BeginInit();
            this.panelGridBottom.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.panelEx2.SuspendLayout();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).BeginInit();
            this.tcGeneral.SuspendLayout();
            this.tabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrSalesReturn)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandZoom
            // 
            this.CommandZoom.Name = "CommandZoom";
            // 
            // dockContainerItem1
            // 
            this.dockContainerItem1.Name = "dockContainerItem1";
            this.dockContainerItem1.Text = "dockContainerItem1";
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.dgvSalesReturnDisplay);
            this.PanelLeft.Controls.Add(this.expandablePanel1);
            this.PanelLeft.Controls.Add(this.lblSCountStatus);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(200, 544);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 9;
            this.PanelLeft.Text = "panelEx1";
            this.PanelLeft.Visible = false;
            // 
            // dgvSalesReturnDisplay
            // 
            this.dgvSalesReturnDisplay.AllowUserToAddRows = false;
            this.dgvSalesReturnDisplay.AllowUserToDeleteRows = false;
            this.dgvSalesReturnDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSalesReturnDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.dgvSalesReturnDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSalesReturnDisplay.DefaultCellStyle = dataGridViewCellStyle32;
            this.dgvSalesReturnDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSalesReturnDisplay.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSalesReturnDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSalesReturnDisplay.Location = new System.Drawing.Point(0, 226);
            this.dgvSalesReturnDisplay.Name = "dgvSalesReturnDisplay";
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSalesReturnDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this.dgvSalesReturnDisplay.RowHeadersVisible = false;
            this.dgvSalesReturnDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSalesReturnDisplay.Size = new System.Drawing.Size(200, 292);
            this.dgvSalesReturnDisplay.TabIndex = 25;
            this.dgvSalesReturnDisplay.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSalesReturnDisplay_CellDoubleClick);
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expandablePanel1.Controls.Add(this.panelLeftTop);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(200, 226);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 111;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Search";
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelLeftTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelLeftTop.Controls.Add(this.lnkLabel);
            this.panelLeftTop.Controls.Add(this.lblSalesNo);
            this.panelLeftTop.Controls.Add(this.dtpSTo);
            this.panelLeftTop.Controls.Add(this.dtpSFrom);
            this.panelLeftTop.Controls.Add(this.lblTo);
            this.panelLeftTop.Controls.Add(this.lblFrom);
            this.panelLeftTop.Controls.Add(this.cboSExecutive);
            this.panelLeftTop.Controls.Add(this.lblExecutive);
            this.panelLeftTop.Controls.Add(this.BtnSRefresh);
            this.panelLeftTop.Controls.Add(this.TxtSsearch);
            this.panelLeftTop.Controls.Add(this.CboSStatus);
            this.panelLeftTop.Controls.Add(this.CboSCustomer);
            this.panelLeftTop.Controls.Add(this.CboSCompany);
            this.panelLeftTop.Controls.Add(this.LblScompany);
            this.panelLeftTop.Controls.Add(this.LblSCustomer);
            this.panelLeftTop.Controls.Add(this.LblSStatus);
            this.panelLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftTop.Location = new System.Drawing.Point(0, 26);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(200, 200);
            this.panelLeftTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelLeftTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelLeftTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelLeftTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelLeftTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelLeftTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelLeftTop.Style.GradientAngle = 90;
            this.panelLeftTop.TabIndex = 108;
            // 
            // lnkLabel
            // 
            this.lnkLabel.AutoSize = true;
            this.lnkLabel.Location = new System.Drawing.Point(3, 181);
            this.lnkLabel.Name = "lnkLabel";
            this.lnkLabel.Size = new System.Drawing.Size(87, 13);
            this.lnkLabel.TabIndex = 260;
            this.lnkLabel.TabStop = true;
            this.lnkLabel.Text = "Advance Search";
            this.lnkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLabel_LinkClicked);
            // 
            // lblSalesNo
            // 
            this.lblSalesNo.AutoSize = true;
            this.lblSalesNo.Location = new System.Drawing.Point(3, 151);
            this.lblSalesNo.Name = "lblSalesNo";
            this.lblSalesNo.Size = new System.Drawing.Size(56, 13);
            this.lblSalesNo.TabIndex = 256;
            this.lblSalesNo.Text = "Return No";
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(58, 125);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.Size = new System.Drawing.Size(136, 20);
            this.dtpSTo.TabIndex = 255;
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(58, 102);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.Size = new System.Drawing.Size(136, 20);
            this.dtpSFrom.TabIndex = 254;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(3, 128);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 253;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(3, 105);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 252;
            this.lblFrom.Text = "From";
            // 
            // cboSExecutive
            // 
            this.cboSExecutive.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSExecutive.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSExecutive.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSExecutive.DisplayMember = "Text";
            this.cboSExecutive.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSExecutive.DropDownHeight = 75;
            this.cboSExecutive.FormattingEnabled = true;
            this.cboSExecutive.IntegralHeight = false;
            this.cboSExecutive.ItemHeight = 14;
            this.cboSExecutive.Location = new System.Drawing.Point(58, 33);
            this.cboSExecutive.Name = "cboSExecutive";
            this.cboSExecutive.Size = new System.Drawing.Size(136, 20);
            this.cboSExecutive.TabIndex = 21;
            // 
            // lblExecutive
            // 
            this.lblExecutive.AutoSize = true;
            this.lblExecutive.Location = new System.Drawing.Point(3, 36);
            this.lblExecutive.Name = "lblExecutive";
            this.lblExecutive.Size = new System.Drawing.Size(54, 13);
            this.lblExecutive.TabIndex = 126;
            this.lblExecutive.Text = "Executive";
            // 
            // BtnSRefresh
            // 
            this.BtnSRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnSRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BtnSRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnSRefresh.Location = new System.Drawing.Point(123, 171);
            this.BtnSRefresh.Name = "BtnSRefresh";
            this.BtnSRefresh.Size = new System.Drawing.Size(71, 23);
            this.BtnSRefresh.TabIndex = 24;
            this.BtnSRefresh.Text = "Refresh";
            this.BtnSRefresh.Click += new System.EventHandler(this.BtnSRefresh_Click);
            // 
            // TxtSsearch
            // 
            this.TxtSsearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.TxtSsearch.Border.Class = "TextBoxBorder";
            this.TxtSsearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtSsearch.Location = new System.Drawing.Point(58, 148);
            this.TxtSsearch.MaxLength = 20;
            this.TxtSsearch.Name = "TxtSsearch";
            this.TxtSsearch.Size = new System.Drawing.Size(136, 20);
            this.TxtSsearch.TabIndex = 19;
            this.TxtSsearch.WatermarkEnabled = false;
            this.TxtSsearch.WatermarkText = "Sales Return No";
            // 
            // CboSStatus
            // 
            this.CboSStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CboSStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSStatus.DisplayMember = "Text";
            this.CboSStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboSStatus.DropDownHeight = 75;
            this.CboSStatus.FormattingEnabled = true;
            this.CboSStatus.IntegralHeight = false;
            this.CboSStatus.ItemHeight = 14;
            this.CboSStatus.Location = new System.Drawing.Point(58, 79);
            this.CboSStatus.Name = "CboSStatus";
            this.CboSStatus.Size = new System.Drawing.Size(136, 20);
            this.CboSStatus.TabIndex = 23;
            // 
            // CboSCustomer
            // 
            this.CboSCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CboSCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSCustomer.DisplayMember = "Text";
            this.CboSCustomer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboSCustomer.DropDownHeight = 75;
            this.CboSCustomer.FormattingEnabled = true;
            this.CboSCustomer.IntegralHeight = false;
            this.CboSCustomer.ItemHeight = 14;
            this.CboSCustomer.Location = new System.Drawing.Point(58, 56);
            this.CboSCustomer.Name = "CboSCustomer";
            this.CboSCustomer.Size = new System.Drawing.Size(136, 20);
            this.CboSCustomer.TabIndex = 22;
            this.CboSCustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // CboSCompany
            // 
            this.CboSCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CboSCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSCompany.DisplayMember = "Text";
            this.CboSCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboSCompany.DropDownHeight = 75;
            this.CboSCompany.FormattingEnabled = true;
            this.CboSCompany.IntegralHeight = false;
            this.CboSCompany.ItemHeight = 14;
            this.CboSCompany.Location = new System.Drawing.Point(58, 10);
            this.CboSCompany.Name = "CboSCompany";
            this.CboSCompany.Size = new System.Drawing.Size(136, 20);
            this.CboSCompany.TabIndex = 20;
            this.CboSCompany.SelectedIndexChanged += new System.EventHandler(this.CboSCompany_SelectedIndexChanged);
            this.CboSCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // LblScompany
            // 
            this.LblScompany.AutoSize = true;
            this.LblScompany.Location = new System.Drawing.Point(3, 13);
            this.LblScompany.Name = "LblScompany";
            this.LblScompany.Size = new System.Drawing.Size(51, 13);
            this.LblScompany.TabIndex = 109;
            this.LblScompany.Text = "Company";
            // 
            // LblSCustomer
            // 
            this.LblSCustomer.AutoSize = true;
            this.LblSCustomer.Location = new System.Drawing.Point(3, 59);
            this.LblSCustomer.Name = "LblSCustomer";
            this.LblSCustomer.Size = new System.Drawing.Size(51, 13);
            this.LblSCustomer.TabIndex = 108;
            this.LblSCustomer.Text = "Customer";
            // 
            // LblSStatus
            // 
            this.LblSStatus.AutoSize = true;
            this.LblSStatus.Location = new System.Drawing.Point(3, 82);
            this.LblSStatus.Name = "LblSStatus";
            this.LblSStatus.Size = new System.Drawing.Size(37, 13);
            this.LblSStatus.TabIndex = 107;
            this.LblSStatus.Text = "Status";
            // 
            // lblSCountStatus
            // 
            // 
            // 
            // 
            this.lblSCountStatus.BackgroundStyle.Class = "";
            this.lblSCountStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSCountStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblSCountStatus.Location = new System.Drawing.Point(0, 518);
            this.lblSCountStatus.Name = "lblSCountStatus";
            this.lblSCountStatus.Size = new System.Drawing.Size(200, 26);
            this.lblSCountStatus.TabIndex = 0;
            this.lblSCountStatus.Text = "...";
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.Expanded = false;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(179)))), ((int)(((byte)(219)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(0, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 544);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 10;
            this.expandableSplitterLeft.TabStop = false;
            this.expandableSplitterLeft.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitterLeft_ExpandedChanged);
            // 
            // dotNetBarManager1
            // 
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.dotNetBarManager1.BottomDockSite = this.dockSite4;
            this.dotNetBarManager1.EnableFullSizeDock = false;
            this.dotNetBarManager1.LeftDockSite = this.dockSite1;
            this.dotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.dotNetBarManager1.ParentForm = this;
            this.dotNetBarManager1.RightDockSite = this.dockSite2;
            this.dotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.dotNetBarManager1.ToolbarBottomDockSite = this.dockSite8;
            this.dotNetBarManager1.ToolbarLeftDockSite = this.dockSite5;
            this.dotNetBarManager1.ToolbarRightDockSite = this.dockSite6;
            this.dotNetBarManager1.TopDockSite = this.dockSite3;
            // 
            // dockSite4
            // 
            this.dockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite4.Location = new System.Drawing.Point(0, 544);
            this.dockSite4.Name = "dockSite4";
            this.dockSite4.Size = new System.Drawing.Size(1303, 0);
            this.dockSite4.TabIndex = 18;
            this.dockSite4.TabStop = false;
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(3, 0);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 544);
            this.dockSite1.TabIndex = 15;
            this.dockSite1.TabStop = false;
            // 
            // dockSite2
            // 
            this.dockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite2.Location = new System.Drawing.Point(1303, 0);
            this.dockSite2.Name = "dockSite2";
            this.dockSite2.Size = new System.Drawing.Size(0, 544);
            this.dockSite2.TabIndex = 16;
            this.dockSite2.TabStop = false;
            // 
            // dockSite8
            // 
            this.dockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite8.Location = new System.Drawing.Point(0, 544);
            this.dockSite8.Name = "dockSite8";
            this.dockSite8.Size = new System.Drawing.Size(1303, 0);
            this.dockSite8.TabIndex = 22;
            this.dockSite8.TabStop = false;
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(0, 0);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 544);
            this.dockSite5.TabIndex = 19;
            this.dockSite5.TabStop = false;
            // 
            // dockSite6
            // 
            this.dockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite6.Location = new System.Drawing.Point(1303, 0);
            this.dockSite6.Name = "dockSite6";
            this.dockSite6.Size = new System.Drawing.Size(0, 544);
            this.dockSite6.TabIndex = 20;
            this.dockSite6.TabStop = false;
            // 
            // dockSite3
            // 
            this.dockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite3.Location = new System.Drawing.Point(0, 0);
            this.dockSite3.Name = "dockSite3";
            this.dockSite3.Size = new System.Drawing.Size(1303, 0);
            this.dockSite3.TabIndex = 17;
            this.dockSite3.TabStop = false;
            // 
            // bar1
            // 
            this.bar1.AccessibleDescription = "DotNetBar Bar (bar1)";
            this.bar1.AccessibleName = "DotNetBar Bar";
            this.bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.bar1.Location = new System.Drawing.Point(0, 0);
            this.bar1.MenuBar = true;
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(36, 24);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.bar1.TabIndex = 0;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx1.Controls.Add(this.panelBottom);
            this.panelEx1.Controls.Add(this.expandableSplitterTop);
            this.panelEx1.Controls.Add(this.panelTop);
            this.panelEx1.Controls.Add(this.PurchaseOrderBindingNavigator);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(3, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(1300, 544);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 23;
            this.panelEx1.Text = "panelEx1";
            // 
            // panelBottom
            // 
            this.panelBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelBottom.Controls.Add(this.tcSalesReturn);
            this.panelBottom.Controls.Add(this.panelGridBottom);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBottom.Location = new System.Drawing.Point(0, 150);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1300, 394);
            this.panelBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelBottom.Style.GradientAngle = 90;
            this.panelBottom.TabIndex = 1;
            this.panelBottom.Text = "panelEx3";
            // 
            // tcSalesReturn
            // 
            this.tcSalesReturn.BackColor = System.Drawing.Color.Transparent;
            this.tcSalesReturn.CanReorderTabs = true;
            this.tcSalesReturn.Controls.Add(this.tabControlPanel1);
            this.tcSalesReturn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcSalesReturn.Location = new System.Drawing.Point(0, 0);
            this.tcSalesReturn.Name = "tcSalesReturn";
            this.tcSalesReturn.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcSalesReturn.SelectedTabIndex = 0;
            this.tcSalesReturn.Size = new System.Drawing.Size(1300, 272);
            this.tcSalesReturn.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcSalesReturn.TabIndex = 10;
            this.tcSalesReturn.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcSalesReturn.Tabs.Add(this.tpItemDetails);
            this.tcSalesReturn.TabStop = false;
            this.tcSalesReturn.Text = "tabControl1";
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.dgvSales);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1300, 250);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(147)))), ((int)(((byte)(160)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 11;
            this.tabControlPanel1.TabItem = this.tpItemDetails;
            // 
            // dgvSales
            // 
            this.dgvSales.AddNewRow = false;
            this.dgvSales.AlphaNumericCols = new int[0];
            this.dgvSales.BackgroundColor = System.Drawing.Color.White;
            this.dgvSales.CapsLockCols = new int[0];
            this.dgvSales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSales.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.ItemID,
            this.ReferenceSerialNo,
            this.BatchID,
            this.BatchNo,
            this.InvoicedQuantity,
            this.InvQty,
            this.InvUomID,
            this.InvRate,
            this.IssuedUOMID,
            this.ReturnedQty,
            this.ReturnQuantity,
            this.Uom,
            this.Rate,
            this.GrandAmount,
            this.Discount,
            this.DiscountAmount,
            this.NetAmount,
            this.ReturnAmount,
            this.Reason,
            this.WarehouseID,
            this.IsGroup,
            this.SalesInvoicedQuantity,
            this.SalesInvoiceDiscountAmount,
            this.SalesInvoiceRate});
            this.dgvSales.DecimalCols = new int[0];
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle42.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle42.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSales.DefaultCellStyle = dataGridViewCellStyle42;
            this.dgvSales.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSales.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvSales.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSales.HasSlNo = false;
            this.dgvSales.LastRowIndex = 0;
            this.dgvSales.Location = new System.Drawing.Point(1, 1);
            this.dgvSales.Name = "dgvSales";
            this.dgvSales.NegativeValueCols = new int[0];
            this.dgvSales.NumericCols = new int[0];
            this.dgvSales.RowHeadersWidth = 50;
            this.dgvSales.Size = new System.Drawing.Size(1298, 248);
            this.dgvSales.TabIndex = 12;
            this.dgvSales.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSales_CellValueChanged);
            this.dgvSales.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvSales_CellBeginEdit);
            this.dgvSales.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvSales_RowsAdded);
            this.dgvSales.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSales_CellEndEdit);
            this.dgvSales.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvSales_Textbox_TextChanged);
            this.dgvSales.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvSales_EditingControlShowing);
            this.dgvSales.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvSales_CurrentCellDirtyStateChanged);
            this.dgvSales.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSales_CellEnter);
            this.dgvSales.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvSales_RowsRemoved);
            // 
            // ItemCode
            // 
            this.ItemCode.HeaderText = "Item Code";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemCode.Width = 60;
            // 
            // ItemName
            // 
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemName.Width = 170;
            // 
            // ItemID
            // 
            dataGridViewCellStyle34.Format = "N0";
            dataGridViewCellStyle34.NullValue = "0";
            this.ItemID.DefaultCellStyle = dataGridViewCellStyle34;
            this.ItemID.HeaderText = "ItemID";
            this.ItemID.Name = "ItemID";
            this.ItemID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemID.Visible = false;
            // 
            // ReferenceSerialNo
            // 
            this.ReferenceSerialNo.HeaderText = "ReferenceSerialNo";
            this.ReferenceSerialNo.Name = "ReferenceSerialNo";
            this.ReferenceSerialNo.Visible = false;
            // 
            // BatchID
            // 
            this.BatchID.HeaderText = "BatchID";
            this.BatchID.Name = "BatchID";
            this.BatchID.Visible = false;
            // 
            // BatchNo
            // 
            this.BatchNo.HeaderText = "BatchNo";
            this.BatchNo.Name = "BatchNo";
            this.BatchNo.ReadOnly = true;
            this.BatchNo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BatchNo.Visible = false;
            this.BatchNo.Width = 140;
            // 
            // InvoicedQuantity
            // 
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle35.Format = "N2";
            this.InvoicedQuantity.DefaultCellStyle = dataGridViewCellStyle35;
            this.InvoicedQuantity.HeaderText = "Delivered Qty";
            this.InvoicedQuantity.MaxInputLength = 10;
            this.InvoicedQuantity.Name = "InvoicedQuantity";
            this.InvoicedQuantity.ReadOnly = true;
            this.InvoicedQuantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.InvoicedQuantity.Width = 80;
            // 
            // InvQty
            // 
            this.InvQty.HeaderText = "InvQty";
            this.InvQty.Name = "InvQty";
            this.InvQty.Visible = false;
            // 
            // InvUomID
            // 
            this.InvUomID.HeaderText = "InvUomID";
            this.InvUomID.Name = "InvUomID";
            this.InvUomID.Visible = false;
            // 
            // InvRate
            // 
            this.InvRate.HeaderText = "InvRate";
            this.InvRate.Name = "InvRate";
            this.InvRate.Visible = false;
            // 
            // IssuedUOMID
            // 
            this.IssuedUOMID.HeaderText = "IssuedUOMID";
            this.IssuedUOMID.Name = "IssuedUOMID";
            this.IssuedUOMID.Visible = false;
            // 
            // ReturnedQty
            // 
            this.ReturnedQty.HeaderText = "ReturnedQty";
            this.ReturnedQty.Name = "ReturnedQty";
            this.ReturnedQty.ReadOnly = true;
            this.ReturnedQty.Width = 80;
            // 
            // ReturnQuantity
            // 
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle36.Format = "N3";
            this.ReturnQuantity.DefaultCellStyle = dataGridViewCellStyle36;
            this.ReturnQuantity.HeaderText = "Quantity";
            this.ReturnQuantity.MaxInputLength = 10;
            this.ReturnQuantity.Name = "ReturnQuantity";
            this.ReturnQuantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ReturnQuantity.Width = 80;
            // 
            // Uom
            // 
            this.Uom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Uom.HeaderText = "UOM";
            this.Uom.Name = "Uom";
            this.Uom.ReadOnly = true;
            this.Uom.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Uom.Width = 60;
            // 
            // Rate
            // 
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle37.Format = "N3";
            this.Rate.DefaultCellStyle = dataGridViewCellStyle37;
            this.Rate.HeaderText = "Rate";
            this.Rate.MaxInputLength = 10;
            this.Rate.Name = "Rate";
            this.Rate.ReadOnly = true;
            this.Rate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Rate.Width = 70;
            // 
            // GrandAmount
            // 
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle38.Format = "N3";
            this.GrandAmount.DefaultCellStyle = dataGridViewCellStyle38;
            this.GrandAmount.HeaderText = "Grand Amount";
            this.GrandAmount.Name = "GrandAmount";
            this.GrandAmount.ReadOnly = true;
            this.GrandAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.GrandAmount.Width = 80;
            // 
            // Discount
            // 
            this.Discount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Discount.HeaderText = "Discount";
            this.Discount.Items.AddRange(new object[] {
            "%",
            "Amount"});
            this.Discount.Name = "Discount";
            this.Discount.ReadOnly = true;
            this.Discount.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Discount.Visible = false;
            // 
            // DiscountAmount
            // 
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.DiscountAmount.DefaultCellStyle = dataGridViewCellStyle39;
            this.DiscountAmount.HeaderText = "Discount Amount";
            this.DiscountAmount.MaxInputLength = 10;
            this.DiscountAmount.Name = "DiscountAmount";
            this.DiscountAmount.ReadOnly = true;
            this.DiscountAmount.Width = 80;
            // 
            // NetAmount
            // 
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.NetAmount.DefaultCellStyle = dataGridViewCellStyle40;
            this.NetAmount.HeaderText = "Net Amount";
            this.NetAmount.Name = "NetAmount";
            this.NetAmount.ReadOnly = true;
            this.NetAmount.Width = 80;
            // 
            // ReturnAmount
            // 
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle41.Format = "N3";
            this.ReturnAmount.DefaultCellStyle = dataGridViewCellStyle41;
            this.ReturnAmount.FillWeight = 80F;
            this.ReturnAmount.HeaderText = "Return Amt";
            this.ReturnAmount.MaxInputLength = 10;
            this.ReturnAmount.Name = "ReturnAmount";
            this.ReturnAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ReturnAmount.Width = 80;
            // 
            // Reason
            // 
            this.Reason.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Reason.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Reason.HeaderText = "Reason";
            this.Reason.MinimumWidth = 100;
            this.Reason.Name = "Reason";
            // 
            // WarehouseID
            // 
            this.WarehouseID.HeaderText = "WarehouseID";
            this.WarehouseID.Name = "WarehouseID";
            this.WarehouseID.Visible = false;
            // 
            // IsGroup
            // 
            this.IsGroup.HeaderText = "IsGroup";
            this.IsGroup.Name = "IsGroup";
            this.IsGroup.Visible = false;
            // 
            // SalesInvoicedQuantity
            // 
            this.SalesInvoicedQuantity.HeaderText = "SalesInvoicedQuantity";
            this.SalesInvoicedQuantity.Name = "SalesInvoicedQuantity";
            this.SalesInvoicedQuantity.Visible = false;
            // 
            // SalesInvoiceDiscountAmount
            // 
            this.SalesInvoiceDiscountAmount.HeaderText = "SalesInvoiceDiscountAmount";
            this.SalesInvoiceDiscountAmount.Name = "SalesInvoiceDiscountAmount";
            this.SalesInvoiceDiscountAmount.Visible = false;
            // 
            // SalesInvoiceRate
            // 
            this.SalesInvoiceRate.HeaderText = "SalesInvoiceRate";
            this.SalesInvoiceRate.Name = "SalesInvoiceRate";
            this.SalesInvoiceRate.Visible = false;
            // 
            // tpItemDetails
            // 
            this.tpItemDetails.AttachedControl = this.tabControlPanel1;
            this.tpItemDetails.Name = "tpItemDetails";
            this.tpItemDetails.Text = "Item Details";
            // 
            // panelGridBottom
            // 
            this.panelGridBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelGridBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelGridBottom.Controls.Add(this.cboAccount);
            this.panelGridBottom.Controls.Add(this.lblAccount);
            this.panelGridBottom.Controls.Add(this.lblAmountInWords);
            this.panelGridBottom.Controls.Add(this.lblAmountIn);
            this.panelGridBottom.Controls.Add(this.pnlBottom);
            this.panelGridBottom.Controls.Add(this.panelEx2);
            this.panelGridBottom.Controls.Add(this.txtExchangeCurrencyRate);
            this.panelGridBottom.Controls.Add(this.txtRemarks);
            this.panelGridBottom.Controls.Add(this.lblPaymentRemarks);
            this.panelGridBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGridBottom.Location = new System.Drawing.Point(0, 272);
            this.panelGridBottom.Name = "panelGridBottom";
            this.panelGridBottom.Size = new System.Drawing.Size(1300, 122);
            this.panelGridBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelGridBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelGridBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelGridBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelGridBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelGridBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelGridBottom.Style.GradientAngle = 90;
            this.panelGridBottom.TabIndex = 25;
            // 
            // cboDiscount
            // 
            this.cboDiscount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDiscount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDiscount.DisplayMember = "Text";
            this.cboDiscount.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDiscount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDiscount.Enabled = false;
            this.cboDiscount.FormattingEnabled = true;
            this.cboDiscount.ItemHeight = 14;
            this.cboDiscount.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2});
            this.cboDiscount.Location = new System.Drawing.Point(3, 22);
            this.cboDiscount.Name = "cboDiscount";
            this.cboDiscount.Size = new System.Drawing.Size(81, 20);
            this.cboDiscount.TabIndex = 258;
            this.cboDiscount.TabStop = false;
            this.cboDiscount.Visible = false;
            this.cboDiscount.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDiscount.WatermarkText = "Select Discount";
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "%";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "Amount";
            // 
            // cboAccount
            // 
            this.cboAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAccount.DisplayMember = "Text";
            this.cboAccount.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboAccount.DropDownHeight = 75;
            this.cboAccount.FormattingEnabled = true;
            this.cboAccount.IntegralHeight = false;
            this.cboAccount.ItemHeight = 14;
            this.cboAccount.Location = new System.Drawing.Point(540, 12);
            this.cboAccount.Name = "cboAccount";
            this.cboAccount.Size = new System.Drawing.Size(132, 20);
            this.cboAccount.TabIndex = 257;
            this.cboAccount.TabStop = false;
            this.cboAccount.SelectedIndexChanged += new System.EventHandler(this.cboAccount_SelectedIndexChanged);
            // 
            // lblAccount
            // 
            this.lblAccount.AutoSize = true;
            this.lblAccount.Location = new System.Drawing.Point(475, 12);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(47, 13);
            this.lblAccount.TabIndex = 256;
            this.lblAccount.Text = "Account";
            // 
            // lblAmountInWords
            // 
            this.lblAmountInWords.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountInWords.Location = new System.Drawing.Point(94, 59);
            this.lblAmountInWords.Name = "lblAmountInWords";
            this.lblAmountInWords.Size = new System.Drawing.Size(428, 32);
            this.lblAmountInWords.TabIndex = 253;
            // 
            // lblAmountIn
            // 
            this.lblAmountIn.AutoSize = true;
            this.lblAmountIn.Location = new System.Drawing.Point(3, 59);
            this.lblAmountIn.Name = "lblAmountIn";
            this.lblAmountIn.Size = new System.Drawing.Size(92, 13);
            this.lblAmountIn.TabIndex = 252;
            this.lblAmountIn.Text = "Amount In Words:";
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.lblCreatedByText);
            this.pnlBottom.Controls.Add(this.lblCreatedDateValue);
            this.pnlBottom.Controls.Add(this.lblSalesReturnStatus);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 96);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1300, 26);
            this.pnlBottom.TabIndex = 251;
            // 
            // lblCreatedByText
            // 
            this.lblCreatedByText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCreatedByText.CloseButtonVisible = false;
            this.lblCreatedByText.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblCreatedByText.Location = new System.Drawing.Point(918, 0);
            this.lblCreatedByText.Name = "lblCreatedByText";
            this.lblCreatedByText.OptionsButtonVisible = false;
            this.lblCreatedByText.Size = new System.Drawing.Size(380, 24);
            this.lblCreatedByText.TabIndex = 258;
            this.lblCreatedByText.Text = "Created By";
            // 
            // lblCreatedDateValue
            // 
            this.lblCreatedDateValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCreatedDateValue.CloseButtonVisible = false;
            this.lblCreatedDateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCreatedDateValue.Location = new System.Drawing.Point(393, 0);
            this.lblCreatedDateValue.Name = "lblCreatedDateValue";
            this.lblCreatedDateValue.OptionsButtonVisible = false;
            this.lblCreatedDateValue.Size = new System.Drawing.Size(905, 24);
            this.lblCreatedDateValue.TabIndex = 256;
            this.lblCreatedDateValue.Text = "Created Date";
            // 
            // lblSalesReturnStatus
            // 
            this.lblSalesReturnStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblSalesReturnStatus.CloseButtonVisible = false;
            this.lblSalesReturnStatus.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblSalesReturnStatus.Image = ((System.Drawing.Image)(resources.GetObject("lblSalesReturnStatus.Image")));
            this.lblSalesReturnStatus.Location = new System.Drawing.Point(0, 0);
            this.lblSalesReturnStatus.Name = "lblSalesReturnStatus";
            this.lblSalesReturnStatus.OptionsButtonVisible = false;
            this.lblSalesReturnStatus.Size = new System.Drawing.Size(393, 24);
            this.lblSalesReturnStatus.TabIndex = 20;
            // 
            // panelEx2
            // 
            this.panelEx2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.panelEx2.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx2.Controls.Add(this.cboDiscount);
            this.panelEx2.Controls.Add(this.label1);
            this.panelEx2.Controls.Add(this.txtExpense);
            this.panelEx2.Controls.Add(this.lblGrandReturnAmount);
            this.panelEx2.Controls.Add(this.txtGrandReturnAmount);
            this.panelEx2.Controls.Add(this.lblTotalAmount);
            this.panelEx2.Controls.Add(this.lblDiscount);
            this.panelEx2.Controls.Add(this.lblSubtotal);
            this.panelEx2.Controls.Add(this.txtSubtotal);
            this.panelEx2.Controls.Add(this.txtTotalAmount);
            this.panelEx2.Controls.Add(this.txtDiscount);
            this.panelEx2.Controls.Add(this.txtTaxAmount);
            this.panelEx2.Location = new System.Drawing.Point(904, 9);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(396, 82);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 241;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 9);
            this.label1.TabIndex = 257;
            this.label1.Text = "Invoice Expense";
            this.label1.Visible = false;
            // 
            // txtExpense
            // 
            this.txtExpense.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtExpense.Border.Class = "TextBoxBorder";
            this.txtExpense.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtExpense.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExpense.Location = new System.Drawing.Point(4, 1);
            this.txtExpense.Name = "txtExpense";
            this.txtExpense.ReadOnly = true;
            this.txtExpense.Size = new System.Drawing.Size(198, 20);
            this.txtExpense.TabIndex = 256;
            this.txtExpense.TabStop = false;
            this.txtExpense.Text = "0.00";
            this.txtExpense.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtExpense.Visible = false;
            // 
            // lblGrandReturnAmount
            // 
            this.lblGrandReturnAmount.AutoSize = true;
            this.lblGrandReturnAmount.BackColor = System.Drawing.Color.White;
            this.lblGrandReturnAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrandReturnAmount.Location = new System.Drawing.Point(5, 63);
            this.lblGrandReturnAmount.Name = "lblGrandReturnAmount";
            this.lblGrandReturnAmount.Size = new System.Drawing.Size(56, 9);
            this.lblGrandReturnAmount.TabIndex = 255;
            this.lblGrandReturnAmount.Text = "Return Amount";
            // 
            // txtGrandReturnAmount
            // 
            this.txtGrandReturnAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtGrandReturnAmount.Border.Class = "TextBoxBorder";
            this.txtGrandReturnAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGrandReturnAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrandReturnAmount.Location = new System.Drawing.Point(3, 45);
            this.txtGrandReturnAmount.MaxLength = 15;
            this.txtGrandReturnAmount.Name = "txtGrandReturnAmount";
            this.txtGrandReturnAmount.Size = new System.Drawing.Size(199, 35);
            this.txtGrandReturnAmount.TabIndex = 17;
            this.txtGrandReturnAmount.Text = "0";
            this.txtGrandReturnAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGrandReturnAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGrandReturnAmount_KeyPress);
            this.txtGrandReturnAmount.TextChanged += new System.EventHandler(this.txtGrandReturnAmount_TextChanged);
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.BackColor = System.Drawing.Color.White;
            this.lblTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(211, 63);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(45, 9);
            this.lblTotalAmount.TabIndex = 253;
            this.lblTotalAmount.Text = "Net Amount";
            // 
            // lblDiscount
            // 
            this.lblDiscount.AutoSize = true;
            this.lblDiscount.BackColor = System.Drawing.Color.White;
            this.lblDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscount.Location = new System.Drawing.Point(211, 30);
            this.lblDiscount.Name = "lblDiscount";
            this.lblDiscount.Size = new System.Drawing.Size(46, 9);
            this.lblDiscount.TabIndex = 251;
            this.lblDiscount.Text = "Tax Amount";
            // 
            // lblSubtotal
            // 
            this.lblSubtotal.AutoSize = true;
            this.lblSubtotal.BackColor = System.Drawing.Color.White;
            this.lblSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubtotal.Location = new System.Drawing.Point(211, 9);
            this.lblSubtotal.Name = "lblSubtotal";
            this.lblSubtotal.Size = new System.Drawing.Size(37, 9);
            this.lblSubtotal.TabIndex = 250;
            this.lblSubtotal.Text = "Sub Total";
            // 
            // txtSubtotal
            // 
            this.txtSubtotal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtSubtotal.Border.Class = "TextBoxBorder";
            this.txtSubtotal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubtotal.Location = new System.Drawing.Point(208, 3);
            this.txtSubtotal.Name = "txtSubtotal";
            this.txtSubtotal.ReadOnly = true;
            this.txtSubtotal.Size = new System.Drawing.Size(185, 20);
            this.txtSubtotal.TabIndex = 14;
            this.txtSubtotal.TabStop = false;
            this.txtSubtotal.Text = "0.00";
            this.txtSubtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTotalAmount.Border.Class = "TextBoxBorder";
            this.txtTotalAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(208, 45);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.Size = new System.Drawing.Size(185, 35);
            this.txtTotalAmount.TabIndex = 18;
            this.txtTotalAmount.TabStop = false;
            this.txtTotalAmount.Text = "0.00";
            this.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtDiscount
            // 
            this.txtDiscount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDiscount.Border.Class = "TextBoxBorder";
            this.txtDiscount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscount.Location = new System.Drawing.Point(83, 22);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.ReadOnly = true;
            this.txtDiscount.Size = new System.Drawing.Size(119, 20);
            this.txtDiscount.TabIndex = 16;
            this.txtDiscount.TabStop = false;
            this.txtDiscount.Text = "0.00";
            this.txtDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscount.Visible = false;
            // 
            // txtTaxAmount
            // 
            this.txtTaxAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTaxAmount.Border.Class = "TextBoxBorder";
            this.txtTaxAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTaxAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxAmount.Location = new System.Drawing.Point(208, 24);
            this.txtTaxAmount.Name = "txtTaxAmount";
            this.txtTaxAmount.ReadOnly = true;
            this.txtTaxAmount.Size = new System.Drawing.Size(187, 20);
            this.txtTaxAmount.TabIndex = 258;
            this.txtTaxAmount.TabStop = false;
            this.txtTaxAmount.Text = "0.00";
            this.txtTaxAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtExchangeCurrencyRate
            // 
            this.txtExchangeCurrencyRate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtExchangeCurrencyRate.Border.Class = "TextBoxBorder";
            this.txtExchangeCurrencyRate.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtExchangeCurrencyRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExchangeCurrencyRate.Location = new System.Drawing.Point(713, 45);
            this.txtExchangeCurrencyRate.Multiline = true;
            this.txtExchangeCurrencyRate.Name = "txtExchangeCurrencyRate";
            this.txtExchangeCurrencyRate.ReadOnly = true;
            this.txtExchangeCurrencyRate.Size = new System.Drawing.Size(185, 38);
            this.txtExchangeCurrencyRate.TabIndex = 248;
            this.txtExchangeCurrencyRate.Text = "0.00";
            this.txtExchangeCurrencyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtExchangeCurrencyRate.Visible = false;
            // 
            // txtRemarks
            // 
            // 
            // 
            // 
            this.txtRemarks.Border.Class = "TextBoxBorder";
            this.txtRemarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtRemarks.Location = new System.Drawing.Point(94, 12);
            this.txtRemarks.MaxLength = 1000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(353, 41);
            this.txtRemarks.TabIndex = 13;
            this.txtRemarks.TextChanged += new System.EventHandler(this.Changestatus);
            // 
            // lblPaymentRemarks
            // 
            this.lblPaymentRemarks.AutoSize = true;
            this.lblPaymentRemarks.Location = new System.Drawing.Point(3, 14);
            this.lblPaymentRemarks.Name = "lblPaymentRemarks";
            this.lblPaymentRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblPaymentRemarks.TabIndex = 237;
            this.lblPaymentRemarks.Text = "Remarks";
            // 
            // expandableSplitterTop
            // 
            this.expandableSplitterTop.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterTop.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitterTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitterTop.ExpandableControl = this.panelTop;
            this.expandableSplitterTop.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.expandableSplitterTop.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(179)))), ((int)(((byte)(219)))));
            this.expandableSplitterTop.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterTop.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterTop.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.Location = new System.Drawing.Point(0, 147);
            this.expandableSplitterTop.Name = "expandableSplitterTop";
            this.expandableSplitterTop.Size = new System.Drawing.Size(1300, 3);
            this.expandableSplitterTop.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterTop.TabIndex = 16;
            this.expandableSplitterTop.TabStop = false;
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelTop.Controls.Add(this.tcGeneral);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1300, 122);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 0;
            // 
            // tcGeneral
            // 
            this.tcGeneral.BackColor = System.Drawing.Color.Transparent;
            this.tcGeneral.CanReorderTabs = true;
            this.tcGeneral.Controls.Add(this.tabControlPanel3);
            this.tcGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcGeneral.Location = new System.Drawing.Point(0, 0);
            this.tcGeneral.Name = "tcGeneral";
            this.tcGeneral.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcGeneral.SelectedTabIndex = 0;
            this.tcGeneral.Size = new System.Drawing.Size(1300, 122);
            this.tcGeneral.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcGeneral.TabIndex = 0;
            this.tcGeneral.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcGeneral.Tabs.Add(this.tiGeneral);
            this.tcGeneral.TabStop = false;
            this.tcGeneral.Text = "tabControl1";
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel3.Controls.Add(this.cboTaxScheme);
            this.tabControlPanel3.Controls.Add(this.label2);
            this.tabControlPanel3.Controls.Add(this.cboCompany);
            this.tabControlPanel3.Controls.Add(this.lblCustomer);
            this.tabControlPanel3.Controls.Add(this.lblReturnNo);
            this.tabControlPanel3.Controls.Add(this.lblVendorAddress);
            this.tabControlPanel3.Controls.Add(this.lblDate);
            this.tabControlPanel3.Controls.Add(this.lblStatusLabel);
            this.tabControlPanel3.Controls.Add(this.lblStatus);
            this.tabControlPanel3.Controls.Add(this.lblCurrency);
            this.tabControlPanel3.Controls.Add(this.dtpReturnDate);
            this.tabControlPanel3.Controls.Add(this.cboInvoiceNo);
            this.tabControlPanel3.Controls.Add(this.cboCustomer);
            this.tabControlPanel3.Controls.Add(this.lblInvoiceNo);
            this.tabControlPanel3.Controls.Add(this.txtVendorAddress);
            this.tabControlPanel3.Controls.Add(this.cboCurrency);
            this.tabControlPanel3.Controls.Add(this.txtReturnNo);
            this.tabControlPanel3.Controls.Add(this.txtDescription);
            this.tabControlPanel3.Controls.Add(this.lblCompany);
            this.tabControlPanel3.Controls.Add(this.lblDescription);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(1300, 100);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(147)))), ((int)(((byte)(160)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 1;
            this.tabControlPanel3.TabItem = this.tiGeneral;
            // 
            // cboTaxScheme
            // 
            this.cboTaxScheme.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboTaxScheme.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTaxScheme.DisplayMember = "Text";
            this.cboTaxScheme.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboTaxScheme.DropDownHeight = 75;
            this.cboTaxScheme.Enabled = false;
            this.cboTaxScheme.FormattingEnabled = true;
            this.cboTaxScheme.IntegralHeight = false;
            this.cboTaxScheme.ItemHeight = 14;
            this.cboTaxScheme.Location = new System.Drawing.Point(633, 7);
            this.cboTaxScheme.Name = "cboTaxScheme";
            this.cboTaxScheme.Size = new System.Drawing.Size(114, 20);
            this.cboTaxScheme.TabIndex = 270;
            this.cboTaxScheme.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTaxScheme.WatermarkText = "Select Supplier";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(560, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 271;
            this.label2.Text = "Tax Scheme";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 75;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(91, 7);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(223, 20);
            this.cboCompany.TabIndex = 2;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.BackColor = System.Drawing.Color.Transparent;
            this.lblCustomer.Location = new System.Drawing.Point(21, 33);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(51, 13);
            this.lblCustomer.TabIndex = 254;
            this.lblCustomer.Text = "Customer";
            // 
            // lblReturnNo
            // 
            this.lblReturnNo.AutoSize = true;
            this.lblReturnNo.BackColor = System.Drawing.Color.Transparent;
            this.lblReturnNo.Location = new System.Drawing.Point(333, 10);
            this.lblReturnNo.Name = "lblReturnNo";
            this.lblReturnNo.Size = new System.Drawing.Size(56, 13);
            this.lblReturnNo.TabIndex = 185;
            this.lblReturnNo.Text = "Return No";
            // 
            // lblVendorAddress
            // 
            this.lblVendorAddress.AutoSize = true;
            this.lblVendorAddress.BackColor = System.Drawing.Color.White;
            this.lblVendorAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendorAddress.Location = new System.Drawing.Point(92, 54);
            this.lblVendorAddress.Name = "lblVendorAddress";
            this.lblVendorAddress.Size = new System.Drawing.Size(33, 9);
            this.lblVendorAddress.TabIndex = 194;
            this.lblVendorAddress.Text = "Address";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Location = new System.Drawing.Point(333, 51);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 188;
            this.lblDate.Text = "Date";
            // 
            // lblStatusLabel
            // 
            this.lblStatusLabel.AutoSize = true;
            this.lblStatusLabel.BackColor = System.Drawing.Color.Transparent;
            this.lblStatusLabel.Location = new System.Drawing.Point(769, 10);
            this.lblStatusLabel.Name = "lblStatusLabel";
            this.lblStatusLabel.Size = new System.Drawing.Size(37, 13);
            this.lblStatusLabel.TabIndex = 252;
            this.lblStatusLabel.Text = "Status";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(839, 7);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(43, 20);
            this.lblStatus.TabIndex = 14;
            this.lblStatus.Text = "New";
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrency.Location = new System.Drawing.Point(333, 72);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(49, 13);
            this.lblCurrency.TabIndex = 251;
            this.lblCurrency.Text = "Currency";
            // 
            // dtpReturnDate
            // 
            this.dtpReturnDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpReturnDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpReturnDate.Location = new System.Drawing.Point(408, 51);
            this.dtpReturnDate.Name = "dtpReturnDate";
            this.dtpReturnDate.Size = new System.Drawing.Size(132, 20);
            this.dtpReturnDate.TabIndex = 7;
            this.dtpReturnDate.ValueChanged += new System.EventHandler(this.Changestatus);
            // 
            // cboInvoiceNo
            // 
            this.cboInvoiceNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboInvoiceNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboInvoiceNo.DisplayMember = "Text";
            this.cboInvoiceNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboInvoiceNo.DropDownHeight = 75;
            this.cboInvoiceNo.FormattingEnabled = true;
            this.cboInvoiceNo.IntegralHeight = false;
            this.cboInvoiceNo.ItemHeight = 14;
            this.cboInvoiceNo.Location = new System.Drawing.Point(408, 29);
            this.cboInvoiceNo.Name = "cboInvoiceNo";
            this.cboInvoiceNo.Size = new System.Drawing.Size(132, 20);
            this.cboInvoiceNo.TabIndex = 6;
            this.cboInvoiceNo.SelectedIndexChanged += new System.EventHandler(this.cboInvoiceNo_SelectedIndexChanged);
            // 
            // cboCustomer
            // 
            this.cboCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCustomer.DisplayMember = "Text";
            this.cboCustomer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCustomer.DropDownHeight = 75;
            this.cboCustomer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomer.FormattingEnabled = true;
            this.cboCustomer.IntegralHeight = false;
            this.cboCustomer.ItemHeight = 14;
            this.cboCustomer.Location = new System.Drawing.Point(91, 30);
            this.cboCustomer.Name = "cboCustomer";
            this.cboCustomer.Size = new System.Drawing.Size(223, 20);
            this.cboCustomer.TabIndex = 3;
            this.cboCustomer.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCustomer.WatermarkText = "Select Customer";
            this.cboCustomer.SelectedIndexChanged += new System.EventHandler(this.cboCustomer_SelectedIndexChanged);
            this.cboCustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblInvoiceNo
            // 
            this.lblInvoiceNo.AutoSize = true;
            this.lblInvoiceNo.BackColor = System.Drawing.Color.Transparent;
            this.lblInvoiceNo.Location = new System.Drawing.Point(333, 31);
            this.lblInvoiceNo.Name = "lblInvoiceNo";
            this.lblInvoiceNo.Size = new System.Drawing.Size(59, 13);
            this.lblInvoiceNo.TabIndex = 249;
            this.lblInvoiceNo.Text = "Invoice No";
            // 
            // txtVendorAddress
            // 
            // 
            // 
            // 
            this.txtVendorAddress.Border.Class = "TextBoxBorder";
            this.txtVendorAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtVendorAddress.Location = new System.Drawing.Point(91, 52);
            this.txtVendorAddress.MaxLength = 20;
            this.txtVendorAddress.Multiline = true;
            this.txtVendorAddress.Name = "txtVendorAddress";
            this.txtVendorAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtVendorAddress.Size = new System.Drawing.Size(223, 43);
            this.txtVendorAddress.TabIndex = 4;
            this.txtVendorAddress.TabStop = false;
            // 
            // cboCurrency
            // 
            this.cboCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCurrency.DisplayMember = "Text";
            this.cboCurrency.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCurrency.DropDownHeight = 75;
            this.cboCurrency.Enabled = false;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.IntegralHeight = false;
            this.cboCurrency.ItemHeight = 14;
            this.cboCurrency.Location = new System.Drawing.Point(408, 74);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(132, 20);
            this.cboCurrency.TabIndex = 8;
            this.cboCurrency.TabStop = false;
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.Changestatus);
            this.cboCurrency.SelectedValueChanged += new System.EventHandler(this.cboCurrency_SelectedValueChanged);
            this.cboCurrency.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // txtReturnNo
            // 
            // 
            // 
            // 
            this.txtReturnNo.Border.Class = "TextBoxBorder";
            this.txtReturnNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtReturnNo.Location = new System.Drawing.Point(408, 6);
            this.txtReturnNo.MaxLength = 20;
            this.txtReturnNo.Name = "txtReturnNo";
            this.txtReturnNo.Size = new System.Drawing.Size(132, 20);
            this.txtReturnNo.TabIndex = 5;
            this.txtReturnNo.TabStop = false;
            // 
            // txtDescription
            // 
            // 
            // 
            // 
            this.txtDescription.Border.Class = "TextBoxBorder";
            this.txtDescription.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDescription.Location = new System.Drawing.Point(843, 36);
            this.txtDescription.MaxLength = 20;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(327, 58);
            this.txtDescription.TabIndex = 9;
            this.txtDescription.TabStop = false;
            this.txtDescription.Visible = false;
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.BackColor = System.Drawing.Color.Transparent;
            this.lblCompany.Location = new System.Drawing.Point(21, 10);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 237;
            this.lblCompany.Text = "Company";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblDescription.Location = new System.Drawing.Point(769, 40);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 13);
            this.lblDescription.TabIndex = 236;
            this.lblDescription.Text = "Description";
            this.lblDescription.Visible = false;
            // 
            // tiGeneral
            // 
            this.tiGeneral.AttachedControl = this.tabControlPanel3;
            this.tiGeneral.Name = "tiGeneral";
            this.tiGeneral.Text = "General";
            // 
            // PurchaseOrderBindingNavigator
            // 
            this.PurchaseOrderBindingNavigator.AccessibleDescription = "DotNetBar Bar (PurchaseOrderBindingNavigator)";
            this.PurchaseOrderBindingNavigator.AccessibleName = "DotNetBar Bar";
            this.PurchaseOrderBindingNavigator.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.PurchaseOrderBindingNavigator.Dock = System.Windows.Forms.DockStyle.Top;
            this.PurchaseOrderBindingNavigator.DockLine = 1;
            this.PurchaseOrderBindingNavigator.DockOffset = 73;
            this.PurchaseOrderBindingNavigator.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.PurchaseOrderBindingNavigator.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.PurchaseOrderBindingNavigator.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorClearItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorCancelItem,
            this.btnCustomerHistory,
            this.BtnPrint,
            this.BtnEmail});
            this.PurchaseOrderBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.PurchaseOrderBindingNavigator.Name = "PurchaseOrderBindingNavigator";
            this.PurchaseOrderBindingNavigator.Size = new System.Drawing.Size(1300, 25);
            this.PurchaseOrderBindingNavigator.Stretch = true;
            this.PurchaseOrderBindingNavigator.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PurchaseOrderBindingNavigator.TabIndex = 12;
            this.PurchaseOrderBindingNavigator.TabStop = false;
            this.PurchaseOrderBindingNavigator.Text = "bar2";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlEnter);
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.Tooltip = "Add New Information";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Text = "&Save";
            this.BindingNavigatorSaveItem.Tooltip = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorClearItem
            // 
            this.BindingNavigatorClearItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorClearItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorClearItem.Image")));
            this.BindingNavigatorClearItem.Name = "BindingNavigatorClearItem";
            this.BindingNavigatorClearItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.BindingNavigatorClearItem.Text = "Clear";
            this.BindingNavigatorClearItem.Tooltip = "Clear";
            this.BindingNavigatorClearItem.Visible = false;
            this.BindingNavigatorClearItem.Click += new System.EventHandler(this.BindingNavigatorClearItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Tooltip = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorCancelItem
            // 
            this.BindingNavigatorCancelItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorCancelItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorCancelItem.Image")));
            this.BindingNavigatorCancelItem.Name = "BindingNavigatorCancelItem";
            this.BindingNavigatorCancelItem.Text = "Cancel";
            this.BindingNavigatorCancelItem.Tooltip = "Cancel";
            this.BindingNavigatorCancelItem.Click += new System.EventHandler(this.BindingNavigatorCancelItem_Click);
            // 
            // btnCustomerHistory
            // 
            this.btnCustomerHistory.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnCustomerHistory.Image = ((System.Drawing.Image)(resources.GetObject("btnCustomerHistory.Image")));
            this.btnCustomerHistory.Name = "btnCustomerHistory";
            this.btnCustomerHistory.Text = "Customer History";
            this.btnCustomerHistory.Tooltip = "Customer History";
            this.btnCustomerHistory.Visible = false;
            this.btnCustomerHistory.Click += new System.EventHandler(this.btnCustomerHistory_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // TmrSalesReturn
            // 
            this.TmrSalesReturn.Interval = 2000;
            // 
            // ErrSalesReturn
            // 
            this.ErrSalesReturn.ContainerControl = this;
            this.ErrSalesReturn.RightToLeft = true;
            // 
            // ImgSalesReturn
            // 
            this.ImgSalesReturn.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgSalesReturn.ImageStream")));
            this.ImgSalesReturn.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgSalesReturn.Images.SetKeyName(0, "Purchase Indent.ICO");
            this.ImgSalesReturn.Images.SetKeyName(1, "Purchase Order.ico");
            this.ImgSalesReturn.Images.SetKeyName(2, "Purchase Invoice.ico");
            this.ImgSalesReturn.Images.SetKeyName(3, "GRN.ICO");
            this.ImgSalesReturn.Images.SetKeyName(4, "Purchase Order Return.ico");
            this.ImgSalesReturn.Images.SetKeyName(5, "Add.png");
            this.ImgSalesReturn.Images.SetKeyName(6, "save.PNG");
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Visible = false;
            this.dataGridViewTextBoxColumn12.Width = 150;
            // 
            // dataGridViewTextBoxColumn13
            // 
            dataGridViewCellStyle43.Format = "N0";
            dataGridViewCellStyle43.NullValue = "0";
            this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle43;
            this.dataGridViewTextBoxColumn13.HeaderText = "Item Name";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Visible = false;
            this.dataGridViewTextBoxColumn13.Width = 180;
            // 
            // dataGridViewTextBoxColumn14
            // 
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle44;
            this.dataGridViewTextBoxColumn14.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn14.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Visible = false;
            this.dataGridViewTextBoxColumn14.Width = 65;
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle45;
            this.dataGridViewTextBoxColumn15.HeaderText = "Return Quantity";
            this.dataGridViewTextBoxColumn15.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Visible = false;
            this.dataGridViewTextBoxColumn15.Width = 190;
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle46;
            this.dataGridViewTextBoxColumn16.HeaderText = "Batch Number";
            this.dataGridViewTextBoxColumn16.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Visible = false;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle47;
            this.dataGridViewTextBoxColumn17.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn17.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn18
            // 
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn18.DefaultCellStyle = dataGridViewCellStyle48;
            this.dataGridViewTextBoxColumn18.HeaderText = "Total";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn18.Visible = false;
            this.dataGridViewTextBoxColumn18.Width = 80;
            // 
            // dataGridViewTextBoxColumn19
            // 
            dataGridViewCellStyle49.Format = "N0";
            dataGridViewCellStyle49.NullValue = "0";
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle49;
            this.dataGridViewTextBoxColumn19.HeaderText = "Status";
            this.dataGridViewTextBoxColumn19.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn19.Visible = false;
            this.dataGridViewTextBoxColumn19.Width = 80;
            // 
            // dataGridViewTextBoxColumn20
            // 
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle50.Format = "N2";
            dataGridViewCellStyle50.NullValue = "0";
            this.dataGridViewTextBoxColumn20.DefaultCellStyle = dataGridViewCellStyle50;
            this.dataGridViewTextBoxColumn20.HeaderText = "OrderDetailID";
            this.dataGridViewTextBoxColumn20.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn20.Visible = false;
            this.dataGridViewTextBoxColumn20.Width = 80;
            // 
            // dataGridViewTextBoxColumn21
            // 
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle51.Format = "N2";
            dataGridViewCellStyle51.NullValue = "0";
            this.dataGridViewTextBoxColumn21.DefaultCellStyle = dataGridViewCellStyle51;
            this.dataGridViewTextBoxColumn21.HeaderText = "PurchaseOrderID";
            this.dataGridViewTextBoxColumn21.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn21.Visible = false;
            this.dataGridViewTextBoxColumn21.Width = 80;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn22.Visible = false;
            // 
            // dataGridViewTextBoxColumn23
            // 
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle52.Format = "N2";
            dataGridViewCellStyle52.NullValue = "0";
            this.dataGridViewTextBoxColumn23.DefaultCellStyle = dataGridViewCellStyle52;
            this.dataGridViewTextBoxColumn23.HeaderText = "SelectedDiscountID";
            this.dataGridViewTextBoxColumn23.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn23.Visible = false;
            this.dataGridViewTextBoxColumn23.Width = 80;
            // 
            // dataGridViewTextBoxColumn24
            // 
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle53.Format = "N4";
            dataGridViewCellStyle53.NullValue = "0";
            this.dataGridViewTextBoxColumn24.DefaultCellStyle = dataGridViewCellStyle53;
            this.dataGridViewTextBoxColumn24.HeaderText = "BatchID";
            this.dataGridViewTextBoxColumn24.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn24.Visible = false;
            this.dataGridViewTextBoxColumn24.Width = 80;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle54.Format = "N0";
            dataGridViewCellStyle54.NullValue = "0";
            this.dataGridViewTextBoxColumn25.DefaultCellStyle = dataGridViewCellStyle54;
            this.dataGridViewTextBoxColumn25.HeaderText = "ReturnAmount";
            this.dataGridViewTextBoxColumn25.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn25.Visible = false;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.HeaderText = "DiscountAmount";
            this.dataGridViewTextBoxColumn26.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Visible = false;
            this.dataGridViewTextBoxColumn26.Width = 80;
            // 
            // dataGridViewTextBoxColumn27
            // 
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle55.Format = "N4";
            dataGridViewCellStyle55.NullValue = "0";
            this.dataGridViewTextBoxColumn27.DefaultCellStyle = dataGridViewCellStyle55;
            this.dataGridViewTextBoxColumn27.HeaderText = "Total";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn27.Width = 80;
            // 
            // dataGridViewTextBoxColumn28
            // 
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle56.Format = "N0";
            dataGridViewCellStyle56.NullValue = "0";
            this.dataGridViewTextBoxColumn28.DefaultCellStyle = dataGridViewCellStyle56;
            this.dataGridViewTextBoxColumn28.FillWeight = 80F;
            this.dataGridViewTextBoxColumn28.HeaderText = "Return Amt";
            this.dataGridViewTextBoxColumn28.MaxInputLength = 15;
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.HeaderText = "WarehouseID";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.Visible = false;
            // 
            // clmItemCode
            // 
            this.clmItemCode.HeaderText = "Item Code";
            this.clmItemCode.Name = "clmItemCode";
            this.clmItemCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmItemCode.Width = 150;
            // 
            // clmItemName
            // 
            this.clmItemName.HeaderText = "Item Name";
            this.clmItemName.Name = "clmItemName";
            this.clmItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmItemName.Width = 180;
            // 
            // clmQuantity
            // 
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmQuantity.DefaultCellStyle = dataGridViewCellStyle57;
            this.clmQuantity.HeaderText = "Quantity";
            this.clmQuantity.MaxInputLength = 6;
            this.clmQuantity.Name = "clmQuantity";
            this.clmQuantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmQtyReceived
            // 
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmQtyReceived.DefaultCellStyle = dataGridViewCellStyle58;
            this.clmQtyReceived.HeaderText = "QtyReceived";
            this.clmQtyReceived.MaxInputLength = 6;
            this.clmQtyReceived.Name = "clmQtyReceived";
            this.clmQtyReceived.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmBatchNumber
            // 
            this.clmBatchNumber.HeaderText = "Batch Number";
            this.clmBatchNumber.Name = "clmBatchNumber";
            this.clmBatchNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmBatchNumber.Visible = false;
            // 
            // clmRate
            // 
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmRate.DefaultCellStyle = dataGridViewCellStyle59;
            this.clmRate.HeaderText = "Rate";
            this.clmRate.MaxInputLength = 6;
            this.clmRate.Name = "clmRate";
            this.clmRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmTotal
            // 
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmTotal.DefaultCellStyle = dataGridViewCellStyle60;
            this.clmTotal.HeaderText = "Total";
            this.clmTotal.Name = "clmTotal";
            this.clmTotal.ReadOnly = true;
            this.clmTotal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmStatus
            // 
            this.clmStatus.HeaderText = "Status";
            this.clmStatus.Name = "clmStatus";
            this.clmStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmOrderDetailID
            // 
            this.clmOrderDetailID.HeaderText = "OrderDetailID";
            this.clmOrderDetailID.Name = "clmOrderDetailID";
            this.clmOrderDetailID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmOrderDetailID.Visible = false;
            // 
            // clmSalesOrderID
            // 
            this.clmSalesOrderID.HeaderText = "SalesOrderID";
            this.clmSalesOrderID.Name = "clmSalesOrderID";
            this.clmSalesOrderID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmSalesOrderID.Visible = false;
            // 
            // clmItemID
            // 
            this.clmItemID.HeaderText = "ItemID";
            this.clmItemID.Name = "clmItemID";
            this.clmItemID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmItemID.Visible = false;
            // 
            // FrmCreditNote
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.ClientSize = new System.Drawing.Size(1303, 544);
            this.Controls.Add(this.panelEx1);
            this.Controls.Add(this.dockSite2);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.expandableSplitterLeft);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.dockSite3);
            this.Controls.Add(this.dockSite4);
            this.Controls.Add(this.dockSite5);
            this.Controls.Add(this.dockSite6);
            this.Controls.Add(this.dockSite8);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmCreditNote";
            this.Text = "Credit Note";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmSalesReturn_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCreditNote_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmSalesReturn_KeyDown);
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesReturnDisplay)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.panelEx1.ResumeLayout(false);
            this.panelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcSalesReturn)).EndInit();
            this.tcSalesReturn.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSales)).EndInit();
            this.panelGridBottom.ResumeLayout(false);
            this.panelGridBottom.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            this.panelEx2.ResumeLayout(false);
            this.panelEx2.PerformLayout();
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).EndInit();
            this.tcGeneral.ResumeLayout(false);
            this.tabControlPanel3.ResumeLayout(false);
            this.tabControlPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrSalesReturn)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        DataTable BatchlessItems = new DataTable();
        DataTable dtGroupDetailsR = new DataTable();
        public FrmCreditNote()
        {
            InitializeComponent();
            //FRM
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotificationNew();
            MmessageIcon = MessageBoxIcon.Information;
            MobjclsBLLSalesReturn = new clsBLLSalesReturnMaster();
            //objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            MobjclsBLLCommonUtility = new clsBLLCommonUtility();
        }

        private void LoadMessage()
        {
            datMessages = new DataTable();
            MaStatusMessage = new DataTable();
            datMessages = mObjNotification.FillMessageArray((int)FormID.SalesInvoice, (int)ClsCommonSettings.ProductID);
        }

        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private bool LoadCombos(int intFillType)
        {
            try
            {
                //bool blnIsEnabled = false;
                DataTable datCombos = new DataTable();

                if (intFillType == 0 || intFillType == 1)
                {
                    datCombos = null;
                    datCombos = MobjclsBLLSalesReturn.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });

                    CboSCompany.ValueMember = "CompanyID";
                    CboSCompany.DisplayMember = "CompanyName";
                    CboSCompany.DataSource = datCombos;

                    datCombos = MobjclsBLLSalesReturn.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });

                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;
                }
                if (intFillType == 0 || intFillType == 2)
                {
                    datCombos = MobjclsBLLSalesReturn.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", "VendorTypeID=" + (int)VendorType.Customer });
                    cboCustomer.ValueMember = "VendorID";
                    cboCustomer.DisplayMember = "VendorName";
                    cboCustomer.DataSource = datCombos;
                }
                if (intFillType == 0 || intFillType == 3)
                {
                    datCombos = MobjclsBLLSalesReturn.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", "VendorTypeID= " + (int)VendorType.Customer });
                    CboSCustomer.ValueMember = "VendorID";
                    CboSCustomer.DisplayMember = "VendorName";
                    DataRow dr = datCombos.NewRow();
                    dr["VendorID"] = -2;
                    dr["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(dr, 0);
                    CboSCustomer.DataSource = datCombos;
                }

                if (intFillType == 0 || intFillType == 5)
                {
                }
                if (intFillType == 0 || intFillType == 6)
                {
                    datCombos = MobjclsBLLSalesReturn.FillCombos(new string[] { "distinct CR.CurrencyID,CR.CurrencyName ", "CurrencyReference CR INNER JOIN CurrencyDetails CD ON CR.CurrencyID=CD.CurrencyID", "CD.CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) });
                    cboCurrency.ValueMember = "CurrencyID";
                    cboCurrency.DisplayMember = "CurrencyName";
                    cboCurrency.DataSource = datCombos;
                }
                if (intFillType == 0 || intFillType == 7)
                {

                }
                if (intFillType == 0 || intFillType == 7)
                {
                    datCombos = MobjclsBLLSalesReturn.FillCombos(new string[] { "ReasonID,Reason", "InvReasonReference", "" });
                    Reason.ValueMember = "ReasonID";
                    Reason.DisplayMember = "Reason";
                    Reason.DataSource = datCombos;
                }
                if (intFillType == 0 || intFillType == 8)
                {
                    datCombos = null;
                    datCombos = MobjclsBLLSalesReturn.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "StatusID In(" + (Int32)OperationStatusType.SSalesReturnOpen + "," + (Int32)OperationStatusType.SSalesReturnCancelled + ")" });
                    CboSStatus.ValueMember = "StatusID";
                    CboSStatus.DisplayMember = "Status";
                    DataRow dr = datCombos.NewRow();
                    dr["StatusID"] = -2;
                    dr["Status"] = "ALL";
                    datCombos.Rows.InsertAt(dr, 0);
                    CboSStatus.DataSource = datCombos;

                }
                if (intFillType == 0 || intFillType == 9)
                {
                    datCombos = null;
                    datCombos = MobjclsBLLSalesReturn.FillCombos(new string[] { "AccountID,AccountName ", "AccAccountMaster", "AccountGroupID=" + (int)AccountGroups.SalesAccounts + "AND AccountID <>" + (int)DefaultAccount.SalesAc });

                    cboAccount.ValueMember = "AccountID";
                    cboAccount.DisplayMember = "AccountName";
                    cboAccount.DataSource = datCombos;
                    //ACCOUNT
                }
                if (intFillType == 0 || intFillType == 10)
                {
                    datCombos = null;
                    datCombos = MobjclsBLLSalesReturn.FillCombos(new string[] { "AccountID,AccountName", "AccAccountMaster", "AccountGroupID=33" }); // For Tax Scheme
                    DataRow dr = datCombos.NewRow();
                    dr[0] = 0;
                    dr[1] = "None";
                    datCombos.Rows.InsertAt(dr, 0);
                    cboTaxScheme.ValueMember = "AccountID";
                    cboTaxScheme.DisplayMember = "AccountName";
                    cboTaxScheme.DataSource = datCombos;
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadCombos() " + ex.Message);
                mObjLogs.WriteLog("Error in LoadCombos() " + ex.Message, 2);
            }
            return false;
        }

        private void SetPermissions()
        {
            try
            {
                // Function for setting permissions
                clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
                if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.CreditNote, out MbPrintEmailPermission, out MbAddPermission, out MbUpdatePermission, out MbDeletePermission);

                    //DataTable datTemp = MobjclsBLLSalesReturn.FillCombos(new string[] { "*", "RoleDetails", "RoleID=" + ClsCommonSettings.RoleID + " " +
                    //    "AND CompanyID=" + ClsCommonSettings.CompanyID + " AND MenuID=" + (int)MenuID.VendorHistory + " AND IsView=1"});
                    //if (datTemp != null)
                    //{
                    //    if (datTemp.Rows.Count > 0)
                    //        btnCustomerHistory.Enabled = true;
                    //    else
                    //        btnCustomerHistory.Enabled = false;
                    //}
                    //else
                    //    btnCustomerHistory.Enabled = false;
                }
                else
                    MbAddPermission = MbCancelPermission = MbPrintEmailPermission = MbUpdatePermission = MbDeletePermission = true;

                MbCancelPermission = MbUpdatePermission;
                //BindingNavigatorAddNewItem.Enabled = MbAddPermission;
                //BindingNavigatorSaveItem.Enabled = MbAddPermission;
                //BindingNavigatorDeleteItem.Enabled = MbDeletePermission;

            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SetPermissions() " + ex.Message);
                mObjLogs.WriteLog("Error in SetPermissions() " + ex.Message, 2);
            }
        }


        private void ClearControls()
        {
            MblnIsFromClear = true;
            ErrSalesReturn.Clear();
            dgvSales.Rows.Clear();
            dgvSales.ClearSelection();
            dgvSales.ReadOnly = false;
            CboSCompany.SelectedIndex = -1;
            CboSCustomer.SelectedIndex = -1;
            cboSExecutive.SelectedIndex = -1;
            cboTaxScheme.SelectedValue = 0;
            CboSStatus.SelectedIndex = -1;

            if (ClsCommonSettings.CompanyID <= 0)
                ClsCommonSettings.CompanyID = MobjclsBLLCommonUtility.GetCompanyID();

            cboCompany.SelectedValue = ClsCommonSettings.CompanyID;
            //if (cboCompany.SelectedValue == null)
            //{
            //    DataTable datTemp = MobjclsBLLSalesReturn.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + ClsCommonSettings.CompanyID });
            //    if (datTemp.Rows.Count > 0)
            //        cboCompany.Text = datTemp.Rows[0]["Name"].ToString();
            //}
            cboCustomer.SelectedIndex = -1;
            cboInvoiceNo.SelectedIndex = -1;
            cboDiscount.SelectedIndex = -1;
            cboCurrency.SelectedIndex = -1;

            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.blnIsCancelled = false;

            txtRemarks.Text = txtVendorAddress.Text = txtDescription.Text = "";
            txtSubtotal.Text = txtDiscount.Text = txtTotalAmount.Text = txtGrandReturnAmount.Text=txtTaxAmount.Text = 0.ToString("F" + MintExchangeCurrencyScale);
            dtpReturnDate.Value = ClsCommonSettings.GetServerDate();
            txtDiscount.Text = "0";
            BindingNavigatorCancelItem.Text = "Cancel";
            lblStatus.Text = "New";
            lblStatus.ForeColor = Color.Black;
            //MBlnIsFromCancellation = false;

            cboCompany.Focus();
            MblnIsFromClear = false;
        }

        private void GenerateSalesReturnNo()
        {
            try
            {
                txtReturnNo.Text = GetSalesReturnPrefix() + MobjclsBLLSalesReturn.GetLastReturnNo(1, cboCompany.SelectedValue.ToInt32());
                if (ClsCommonSettings.blnSRAutogenerate)
                    txtReturnNo.ReadOnly = false;
                else
                    txtReturnNo.ReadOnly = true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in GenerateOrderNo() " + ex.Message);
                mObjLogs.WriteLog("Error in GenerateOrderNo() " + ex.Message, 2);
            }
        }

        private void AddNew()
        {
            try
            {
                MblnIsFromCancellation = false;
                MsMessageCommon = "Add new Credit Note";
                MobjclsBLLSalesReturn = new clsBLLSalesReturnMaster();
                lblCreatedByText.Text = " Created By : " + ClsCommonSettings.strEmployeeName;
                lblCreatedDateValue.Text = " Created Date : " + ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");

                cboCompany.Enabled = cboCustomer.Enabled = cboInvoiceNo.Enabled = true;

                ClearControls();
                GenerateSalesReturnNo();
                DisplaySalesNumbersInSearchGrid();
                lblStatus.Text = "New";

                lblSalesReturnStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmrFocus.Enabled = true;

                ErrSalesReturn.Clear();
                BtnPrint.Enabled = false;
                BtnEmail.Enabled = false;
                BindingNavigatorAddNewItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorClearItem.Enabled = true;
                BindingNavigatorCancelItem.Enabled = false;
                BindingNavigatorClearItem.Enabled = true;
                MBlnEditMode = false;
                cboCompany.Focus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in AddNew() " + ex.Message);
                mObjLogs.WriteLog("Error in AddNew() " + ex.Message, 2);
            }
        }

        private string GetSalesReturnPrefix()
        {
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue));
            return ClsCommonSettings.strSRPrefix;
        }

        private bool DisplaySalesNumbersInSearchGrid()
        {
            try
            {
                DataTable dtSales = new DataTable();
                DataTable dtTemp = null;
                string strFilterCondition = string.Empty;


                if (!string.IsNullOrEmpty(TxtSsearch.Text.Trim()))
                {
                    strFilterCondition = " ReturnNo = '" + TxtSsearch.Text.Trim() + "'";
                }
                else
                {
                    if (CboSCompany.SelectedValue != null && Convert.ToInt32(CboSCompany.SelectedValue) != -2)
                        strFilterCondition += " CompanyID = " + Convert.ToInt32(CboSCompany.SelectedValue);
                    else
                    {
                        dtTemp = null;
                        dtTemp = (DataTable)CboSCompany.DataSource;
                        if (dtTemp.Rows.Count > 0)
                        {
                            strFilterCondition += "CompanyID In (";
                            foreach (DataRow dr in dtTemp.Rows)
                            {
                                strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                            }
                            strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                            strFilterCondition += ")";
                        }
                    }
                    if (CboSCustomer.SelectedValue != null && Convert.ToInt32(CboSCustomer.SelectedValue) != -2)
                    {
                        if (!string.IsNullOrEmpty(strFilterCondition))
                            strFilterCondition += " And ";
                        strFilterCondition += "VendorID = " + Convert.ToInt32(CboSCustomer.SelectedValue);
                    }
                    else
                    {
                        dtTemp = null;
                        dtTemp = (DataTable)CboSCustomer.DataSource;
                        if (dtTemp.Rows.Count > 0)
                        {
                            strFilterCondition += " And VendorID In (";
                            foreach (DataRow dr in dtTemp.Rows)
                            {
                                strFilterCondition += Convert.ToInt32(dr["VendorID"]) + ",";
                            }
                            strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                            strFilterCondition += ")";
                        }
                    }
                    if (cboSExecutive.SelectedValue != null && Convert.ToInt32(cboSExecutive.SelectedValue) != -2)
                    {
                        if (!string.IsNullOrEmpty(strFilterCondition))
                            strFilterCondition += " And ";
                        strFilterCondition += "UserID = " + Convert.ToInt32(cboSExecutive.SelectedValue);
                    }
                    //else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                    //{
                    //    //if (CboSCompany.SelectedValue != null && Convert.ToInt32(CboSCompany.SelectedValue) != -2)
                    //    //{
                    //        dtTemp = null;
                    //        dtTemp = (DataTable)cboSExecutive.DataSource;
                    //        if (dtTemp.Rows.Count > 0)
                    //        {
                    //            //clsBLLPermissionSettings MobjClsBLLPermissionSettings = new clsBLLPermissionSettings();
                    //            //DataTable datTempPer = MobjClsBLLPermissionSettings.GetControlPermissions1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.CreNoteEmployee);
                    //            if (datTempPer != null)
                    //            {
                    //                if (datTempPer.Rows.Count > 0)
                    //                {
                    //                    if (datTempPer.Rows[0]["IsVisible"].ToString() == "True")
                    //                        strFilterCondition += "And EmployeeID In (0,";
                    //                    else
                    //                        strFilterCondition += "And EmployeeID In (";
                    //                }
                    //                else
                    //                    strFilterCondition += "And EmployeeID In (";
                    //            }
                    //            else
                    //                strFilterCondition += "And EmployeeID In (";
                    //            foreach (DataRow dr in dtTemp.Rows)
                    //            {
                    //                strFilterCondition += Convert.ToInt32(dr["EmployeeID"]) + ",";
                    //            }
                    //            strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                    //            strFilterCondition += ")";
                    //        }
                    //    //}
                    //}
                    if (CboSStatus.SelectedValue != null && Convert.ToInt32(CboSStatus.SelectedValue) != -2)
                    {
                        if (!string.IsNullOrEmpty(strFilterCondition))
                            strFilterCondition += " And ";
                        strFilterCondition += "StatusID = " + Convert.ToInt32(CboSStatus.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += " ReturnDate >= '" + dtpSFrom.Value.ToString("dd-MMM-yyyy") + "' And ReturnDate <= '" + dtpSTo.Value.ToString("dd-MMM-yyyy") + "'";
                }
                dgvSalesReturnDisplay.DataSource = null;

                dtSales = MobjclsBLLSalesReturn.GetReturnNumbers("");
                dtSales.DefaultView.RowFilter = strFilterCondition;
                dgvSalesReturnDisplay.DataSource = dtSales.DefaultView.ToTable();

                dgvSalesReturnDisplay.Columns[0].Visible = false;
                dgvSalesReturnDisplay.Columns[2].Visible = false;
                dgvSalesReturnDisplay.Columns[3].Visible = false;
                dgvSalesReturnDisplay.Columns[4].Visible = false;
                dgvSalesReturnDisplay.Columns[5].Visible = false;
                dgvSalesReturnDisplay.Columns[6].Visible = false;
                dgvSalesReturnDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                lblSCountStatus.Text = "Total Sales Returns : " + dgvSalesReturnDisplay.Rows.Count;
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplaySalesNumbersInSearchGrid() " + ex.Message);
                mObjLogs.WriteLog("Error in DisplaySalesNumbersInSearchGrid() " + ex.Message, 2);
            }
            return false;
        }

        private void Changestatus(object sender, EventArgs e)
        {
            if (MBlnEditMode)
                BindingNavigatorSaveItem.Enabled = MbUpdatePermission;
            else
                BindingNavigatorSaveItem.Enabled = MbAddPermission;
            BindingNavigatorAddNewItem.Enabled = MbAddPermission;
            ErrSalesReturn.Clear();
        }
        //new modification---discount case
        //private void CalculateNetTotal()
        //{
        //    try
        //    {
        //        double dDiscountAmt = 0;
        //        double dSubTotal = 0;
        //        double dNetAmount = 0;

        //        dSubTotal = txtSubtotal.Text.ToDouble();
        //        int intDiscountID = Convert.ToInt32(cboDiscount.SelectedValue);


        //        double dBTotal = dSubTotal;
        //        if (intDiscountID != (int)PredefinedDiscounts.DefaultSalesDiscount)//&&intDiscountID !=2)
        //        {
        //            if (intDiscountID > 0)
        //            {
        //                dBTotal = MobjclsBLLSalesReturn.GetItemDiscountAmount(4, 0, intDiscountID, dSubTotal);
        //                dDiscountAmt = (dSubTotal) - dBTotal;
        //                txtDiscount.Text = dDiscountAmt.ToString("F" + MintExchangeCurrencyScale);
        //            }
        //            else
        //            {
        //                dDiscountAmt = 0;
        //                txtDiscount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
        //            }
        //        }
        //        else
        //        {
        //            dDiscountAmt = txtDiscount.Text.ToDouble();
        //        }

        //        dNetAmount = (dSubTotal) - dDiscountAmt;
        //        if (dNetAmount < 0)
        //            dNetAmount = 0;

        //        txtTotalAmount.Text = dNetAmount.ToString("F" + MintExchangeCurrencyScale);
        //        txtGrandReturnAmount.Text = dNetAmount.ToString("F" + MintExchangeCurrencyScale);
        //        //CalculateExchangeCurrencyRate();
        //        // }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ClsCommonSettings.ShowErrorMess)
        //            MessageBox.Show("Error in CalculateBNetNotal() " + ex.Message);
        //        MobjClsLogWriter.WriteLog("Error in CalculateBNetTotal() " + ex.Message, 2);
        //    }
        //}

        private void CalculateNetTotal()
        {
            try
            {
                if (blnCalculate)
                {
                    decimal decAmount = 0, decSubtotal = 0, decNetAmt = 0;
                    decimal decDiscountAmt = 0;
                    decimal dTaxAmount = 0;
                    decimal taxValue = 0;
                   

                    //if (txtSubtotal.Text == "" || Convert.ToDecimal(txtSubtotal.Text) == 0) return;

                   // int intDiscountID = Convert.ToInt32(cboDiscount.SelectedValue);
                    int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);

                    decSubtotal = decAmount = Convert.ToDecimal(txtSubtotal.Text);
                    if (cboTaxScheme.SelectedValue.ToInt32() > 0)
                    {
                        taxValue = MobjclsBLLSalesReturn.getTaxValue(cboTaxScheme.SelectedValue.ToInt32());

                        dTaxAmount = ((decSubtotal * taxValue) / 100).ToDecimal();
                    }
                    //if (intDiscountID > 0)
                    //{
                    //    if (intDiscountID != (int)PredefinedDiscounts.DefaultSalesDiscount)
                    //    {
                    //        MobjclsBLLSalesReturn.GetItemDiscountAmount(9, 0, intDiscountID, intVendorID);

                    //        if (MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.blnDiscountForAmount)
                    //        {
                    //            if (MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.blnPercentCalculation)
                    //                decAmount = decAmount - (decAmount * MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decDiscountValue) / 100;
                    //            else
                    //                decAmount = decAmount - MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decDiscountValue;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        //decAmount = decSubtotal - txtDiscount.Text.ToDecimal();
                    //        decAmount = decSubtotal ;
                    //    }
                    //}

                    //if (decAmount < 0)
                    //{
                    //    //MsMessageCommon = "Discount exceeds the item amount.";
                    //    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2013, out MmessageIcon);
                    //    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    //    txtTotalAmount.Text = "0";
                    //    return;
                    //}

                    //decDiscountAmt = decSubtotal - decAmount;

                    //txtDiscount.Text = decDiscountAmt.ToString("F" + MintExchangeCurrencyScale);

                    MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decGrandDiscountAmount =Convert.ToDecimal( txtDiscount.Text);
                    //if (decAmount < 0)
                    //    decAmount = 0;
                    decNetAmt = decAmount + dTaxAmount;
                    txtTotalAmount.Text = decNetAmt.ToString("F" + MintExchangeCurrencyScale);
                    txtTaxAmount.Text = dTaxAmount.ToString("F" + MintExchangeCurrencyScale);
                    txtGrandReturnAmount.Text = decNetAmt.ToString("F" + MintExchangeCurrencyScale);
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CalculateNetTotal() " + ex.Message);
                mObjLogs.WriteLog("Error in CalculateNetTotal() " + ex.Message, 2);
            }
        }

        //instead of calculateTotalAmount()
        private void CalcuateGridTotal()
        {

            try
            {
                double dGrandAmount = 0;
                if (dgvSales.RowCount > 0)
                {
                    int iGridRowCount = dgvSales.RowCount;
                    iGridRowCount = iGridRowCount - 1;

                    for (int i = 0; i < iGridRowCount; i++)
                    {
                        double dGridNetAmount = 0;
                        if (dgvSales.Rows[i].Cells["ItemID"].Value.ToInt32() != 0)
                        {
                            int iItemID = dgvSales.Rows[i].Cells["ItemID"].Value.ToInt32();
                            double dQty = 0;
                            double decDiscountAmount = dgvSales.Rows[i].Cells["DiscountAmount"].Value.ToDouble();
                            dQty = dgvSales.Rows[i].Cells["ReturnQuantity"].Value.ToDouble();
                            double dDirectRate = dgvSales.Rows[i].Cells["Rate"].Value.ToDouble();
                            double dGridGrandAmount = dQty * dDirectRate;
                            if (dQty > 0 && dDirectRate > 0)
                            {
                                dGridNetAmount = (dDirectRate * dQty) - (decDiscountAmount * dQty);
                            }

                            dgvSales.Rows[i].Cells["GrandAmount"].Value = dGridGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                            dgvSales.Rows[i].Cells["NetAmount"].Value = dGridNetAmount.ToString("F" + MintExchangeCurrencyScale);
                            dgvSales.Rows[i].Cells["ReturnAmount"].Value = dGridNetAmount.ToString("F" + MintExchangeCurrencyScale);
                            dGrandAmount += dGridNetAmount;
                        }
                        else
                        {
                            dGrandAmount += 0;
                            dgvSales.Rows[i].Cells["GrandAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                            dgvSales.Rows[i].Cells["NetAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                            dgvSales.Rows[i].Cells["ReturnAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                            dgvSales.Rows[i].Cells["DiscountAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                        }
                    }
                }

                txtSubtotal.Text = dGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                CalculateNetTotal();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CalculateTotalAmt() " + ex.Message);
                mObjLogs.WriteLog("Error in CalculateTotalAmt() " + ex.Message, 2);
            }
        }
        private void CalculateTotalAmt()
        {
            try
            {
                if (blnCalculate)
                {
                    decimal decDirectRate = 0, decRate = 0, decAmount = 0, decTotal = 0, decSubtotal = 0, decQty = 0;
                    decimal decDiscountAmt = 0;

                    if (dgvSales.RowCount > 0)
                    {
                        for (int intICounter = 0; intICounter < dgvSales.RowCount - 1; intICounter++)
                        {
                            int intDiscountID = 0, intItemID = 0, intVendorID = 0;
                            decQty = decAmount = decDirectRate = decRate = 0;
                            decimal decGrandAmount = 0;
                            intItemID = Convert.ToInt32(dgvSales.Rows[intICounter].Cells["ItemID"].Value);
                            if (dgvSales.Rows[intICounter].Cells["Discount"].Value != null && dgvSales.Rows[intICounter].Cells["Discount"].Value.ToString() != "")
                                intDiscountID = Convert.ToInt32(dgvSales.Rows[intICounter].Cells["Discount"].Value);
                            intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);

                            decQty = Convert.ToDecimal(dgvSales.Rows[intICounter].Cells["ReturnQuantity"].Value);

                            decDirectRate = Convert.ToDecimal(dgvSales.Rows[intICounter].Cells["Rate"].Value);

                            decGrandAmount = decQty * decDirectRate;

                            if (decDirectRate > 0 && intItemID > 0)
                            {
                                if (intDiscountID > 0)
                                {
                                    //if (intDiscountID != (int)PredefinedDiscounts.DefaultSalesDiscount)
                                    //{
                                        MobjclsBLLSalesReturn.GetItemDiscountAmount(9, intItemID, intDiscountID, intVendorID);

                                        if (MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.blnDiscountForAmount)
                                        {
                                            decSubtotal = decAmount = (decQty * decDirectRate);

                                            if (MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.blnPercentCalculation)
                                                decAmount = decAmount - (decAmount * MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decDiscountValue) / 100;
                                            else
                                                decAmount = decAmount - MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decDiscountValue;
                                        }
                                        else
                                        {
                                            decSubtotal = (decQty * decDirectRate);

                                            if (MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.blnPercentCalculation)
                                                decRate = decDirectRate - (decDirectRate * MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decDiscountValue) / 100;
                                            else
                                                decRate = decDirectRate - MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decDiscountValue;

                                            decAmount = (decQty * decRate);
                                        }
                                    //}
                                    //else
                                    //{
                                    //    decSubtotal = (decQty * decDirectRate);
                                    //    decAmount = decSubtotal - dgvSales.Rows[intICounter].Cells[DiscountAmount.Index].Value.ToDecimal();
                                    //}
                                }
                                else
                                {
                                    decSubtotal = decAmount = (decQty * decDirectRate);
                                }

                                //if (decAmount < 0)
                                //{
                                //    //MsMessageCommon = "Discount exceeds the item amount.";
                                //    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2013, out MmessageIcon);
                                //    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                //    dgvSales.Rows[intICounter].Cells["DiscountAmount"].Value = "0";
                                //    dgvSales.Rows[intICounter].Cells["Total"].Value = "0";
                                //    return;
                                //}

                                decDiscountAmt = decSubtotal - decAmount;

                                //dgvSales.Rows[intICounter].Cells["DiscountAmount"].Value = decDiscountAmt.ToString("F" + MintExchangeCurrencyScale);
                                dgvSales.Rows[intICounter].Cells["GrandAmount"].Value = decGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                                dgvSales.Rows[intICounter].Cells["NetAmount"].Value = decAmount.ToString("F" + MintExchangeCurrencyScale);
                                dgvSales.Rows[intICounter].Cells["ReturnAmount"].Value = decAmount.ToString("F" + MintExchangeCurrencyScale);
                                decTotal += decAmount;
                            }
                        }
                        txtSubtotal.Text = decTotal.ToString("F" + MintExchangeCurrencyScale);
                        txtTotalAmount.Text = decTotal.ToString("F" + MintExchangeCurrencyScale);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CalculateTotalAmt() " + ex.Message);
                mObjLogs.WriteLog("Error in CalculateTotalAmt() " + ex.Message, 2);
            }
        }



        private void BtnUom_Click(object sender, EventArgs e)
        {
            using (FrmUnitOfMeasurement objUoM = new FrmUnitOfMeasurement())
            {
                objUoM.ShowDialog();
            }
        }

        private void FillUOMColumn(int intItemID,bool blnIsGroup)
        {
            DataTable dtItemUOMs = new DataTable();

            if (!blnIsGroup)
            {
                dtItemUOMs = MobjclsBLLSalesReturn.GetItemUOMs(intItemID);
                Uom.DataSource = null;
                Uom.ValueMember = "UOMID";
                Uom.DisplayMember = "ShortName";
                Uom.DataSource = dtItemUOMs;
            }
            else
            {
                dtItemUOMs = MobjclsBLLSalesReturn.FillCombos(new string[] { "UOMID,Code as ShortName", "InvUOMReference", "UOMID = " + (int)PredefinedUOMs.Numbers });
                Uom.ValueMember = "UOMID";
                Uom.DisplayMember = "ShortName";
                Uom.DataSource = dtItemUOMs;
            }
        }

        private void DisplaySalesReturnDetDetails(long intSalesReturnID)
        {
            try
            {
                dgvSales.Rows.Clear();
                dgvSales.RowCount = 1;
                DataTable datSalesDetail = null;

                datSalesDetail = MobjclsBLLSalesReturn.GetReturnDetDetails(intSalesReturnID);

                if (dgvSales.Rows.Count > 0)
                {
                    for (int intICounter = 0; intICounter < datSalesDetail.Rows.Count; intICounter++)
                    {
                        int intItemID = Convert.ToInt32(datSalesDetail.Rows[intICounter]["ItemID"]);
                        bool blnIsGroup = Convert.ToBoolean(datSalesDetail.Rows[intICounter]["IsGroup"]);
                        int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                        FillUOMColumn(intItemID,blnIsGroup);

                        dgvSales.RowCount = dgvSales.RowCount + 1;
                        dgvSales.Rows[intICounter].Cells["ReferenceSerialNo"].Value = datSalesDetail.Rows[intICounter]["ReferenceSerialNo"];
                        dgvSales.Rows[intICounter].Cells["ItemID"].Value = datSalesDetail.Rows[intICounter]["ItemID"];
                        dgvSales.Rows[intICounter].Cells["ItemCode"].Value = datSalesDetail.Rows[intICounter]["Code"];
                        dgvSales.Rows[intICounter].Cells["ItemName"].Value = datSalesDetail.Rows[intICounter]["ItemName"];
                        dgvSales.Rows[intICounter].Cells["IsGroup"].Value = datSalesDetail.Rows[intICounter]["IsGroup"];
                        //FillBatchNoColumn(intICounter);
                        dgvSales.Rows[intICounter].Cells["BatchID"].Value = datSalesDetail.Rows[intICounter]["BatchID"];
                        //dgvSales.Rows[intICounter].Cells["BatchNo"].Value = datSalesDetail.Rows[intICounter]["BatchID"];
                        dgvSales.Rows[intICounter].Cells["BatchNo"].Tag = datSalesDetail.Rows[intICounter]["BatchID"];
                        dgvSales.Rows[intICounter].Cells["BatchNo"].Value = datSalesDetail.Rows[intICounter]["BatchNo"];
                        
                        dgvSales.Rows[intICounter].Cells["InvRate"].Value = datSalesDetail.Rows[intICounter]["InvRate"];
                        dgvSales.Rows[intICounter].Cells["InvUomID"].Value = datSalesDetail.Rows[intICounter]["InvUomID"];
                        
                        // dgvSales.Rows[intICounter].Cells["ReturnQuantity"].Value = datSalesDetail.Rows[intICounter]["ReturnQuantity"];

                        dgvSales.Rows[intICounter].Cells["Rate"].Value = datSalesDetail.Rows[intICounter]["Rate"];
                        dgvSales.Rows[intICounter].Cells["Uom"].Tag = datSalesDetail.Rows[intICounter]["UOMID"];
                        dgvSales.Rows[intICounter].Cells["Uom"].Value = datSalesDetail.Rows[intICounter]["UOMID"];
                        dgvSales.Rows[intICounter].Cells["Uom"].Value = dgvSales.Rows[intICounter].Cells["Uom"].FormattedValue;
                        dgvSales.Rows[intICounter].Cells["WarehouseID"].Value = datSalesDetail.Rows[intICounter]["WarehouseID"];

                        int intUomScale = 0;
                        if (dgvSales[Uom.Index, intICounter].Tag.ToInt32() != 0)
                            intUomScale = MobjclsBLLSalesReturn.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvSales[Uom.Index, intICounter].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                        dgvSales.Rows[intICounter].Cells["InvoicedQuantity"].Value = datSalesDetail.Rows[intICounter]["InvQty"].ToDecimal().ToString("F" + intUomScale);
                        dgvSales.Rows[intICounter].Cells["InvQty"].Value = datSalesDetail.Rows[intICounter]["InvQty"].ToDecimal().ToString("F" + intUomScale);
                        dgvSales.Rows[intICounter].Cells["ReturnQuantity"].Value = datSalesDetail.Rows[intICounter]["ReturnQuantity"].ToDecimal().ToString("F" + intUomScale);

                        //dgvSales.Rows[intICounter].Cells["Discount"].Value = datSalesDetail.Rows[intICounter]["DiscountID"];
                        dgvSales.Rows[intICounter].Cells["DiscountAmount"].Value = datSalesDetail.Rows[intICounter]["DiscountAmount"];
                        dgvSales.Rows[intICounter].Cells["GrandAmount"].Value = datSalesDetail.Rows[intICounter]["GrandAmount"];
                        dgvSales.Rows[intICounter].Cells["NetAmount"].Value = datSalesDetail.Rows[intICounter]["NetAmount"];
                        dgvSales.Rows[intICounter].Cells["ReturnAmount"].Value = datSalesDetail.Rows[intICounter]["ReturnAmount"];

                        dgvSales.Rows[intICounter].Cells["Reason"].Value = datSalesDetail.Rows[intICounter]["ReasonID"];
                        dgvSales.Rows[intICounter].Cells["WarehouseID"].Value = datSalesDetail.Rows[intICounter]["WarehouseID"];

                        dgvSales.Rows[intICounter].Cells["ReturnedQty"].Value = MobjclsBLLSalesReturn.GetAlreadyReturnedQty(cboInvoiceNo.SelectedValue.ToInt64(), datSalesDetail.Rows[intICounter]["ItemID"].ToInt32(), datSalesDetail.Rows[intICounter]["BatchID"].ToInt64(), datSalesDetail.Rows[intICounter]["IsGroup"].ToBoolean());
                    }
                    //CalculateTotalAmt();
                    CalculateNetTotal();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplaySalesDetDetails() " + ex.Message);
                mObjLogs.WriteLog("Error in DisplaySalesDetDetails() " + ex.Message, 2);
            }
        }

        private bool DisplaySalesReturnMasterDetails(long intOrderID)
        {
            try
            {
                if (MobjclsBLLSalesReturn.GetSalesReturnMasterDetails(intOrderID))
                {
                    blnCalculate = false;
                    MBlnEditMode = true;
                    txtReturnNo.Tag = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intSalesReturnID;
                    cboCompany.SelectedValue = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intCompanyID;
                    if (cboCompany.SelectedValue == null)
                    {
                        DataTable datTemp = MobjclsBLLSalesReturn.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intCompanyID });
                        if (datTemp.Rows.Count > 0)
                            cboCompany.Text = datTemp.Rows[0]["CompanyName"].ToString();
                    }
                    cboCustomer.SelectedValue = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intVendorID;
                    cboTaxScheme.SelectedValue = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intTaxSchemeID;
                    txtReturnNo.Text = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.strSalesReturnNo;
                    cboInvoiceNo.SelectedValue = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intSalesInvoiceID;
                    cboAccount.SelectedValue = Convert.ToInt32(MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intAccountID);
                    //if (cboAccount.SelectedValue == null)
                    //{

                    //    DataTable datTemp = MobjclsBLLSalesReturn.FillCombos(new string[] { "AccountID,AccountName ", "AccAccountMaster", "AccountGroupID=" + (int)AccountGroups.SalesAccounts + "" });

                    //    if (datTemp.Rows.Count > 0)
                    //        cboAccount.Text = datTemp.Rows[0]["AccountName"].ToString();
                    //}


                    //txtVendorAddress.Text = MobjclsBLLSalesReturn.GetVendorAddress(MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intVendorAddID);
                    dtpReturnDate.Value = Convert.ToDateTime(MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.strReturnDate);
                    cboDiscount.SelectedValue = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intGrandDiscountID;
                    cboCurrency.SelectedValue = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intCurrencyID;
                    txtRemarks.Text = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.strRemarks;
                    
                    DisplaySalesReturnDetDetails(intOrderID);
                    txtTaxAmount.Text = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decTaxAmount.ToString("F" + MintExchangeCurrencyScale);
                    txtSubtotal.Text = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                    txtDiscount.Text = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decGrandDiscountAmount.ToString("F" + MintExchangeCurrencyScale);
                    txtTotalAmount.Text = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decNetAmount.ToString("F" + MintExchangeCurrencyScale);
                    txtGrandReturnAmount.Text = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decGrandReturnAmount.ToString("F" + MintExchangeCurrencyScale);

                    lblCreatedByText.Text = " Created By : " + MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.strCreatedBy;
                    lblCreatedDateValue.Text = " Created Date : " + MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.strCreatedDate.ToDateTime().ToString("dd-MMM-yyyy");

                    BindingNavigatorSaveItem.Enabled = MbUpdatePermission;
                    BindingNavigatorAddNewItem.Enabled = MbAddPermission;
                    BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
                    BtnPrint.Enabled = BtnEmail.Enabled = MbPrintEmailPermission;

                    if (MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intStatusID == (Int32)OperationStatusType.SSalesReturnOpen)
                    {
                        lblStatus.Text = "Open";
                        BindingNavigatorCancelItem.Text = "Cancel";
                        BindingNavigatorCancelItem.Enabled = MbCancelPermission;
                        lblDescription.Visible = txtDescription.Visible = false;
                        dgvSales.ReadOnly = false;
                    }
                    else
                    {
                        lblStatus.Text = "Cancelled";
                        BindingNavigatorCancelItem.Text = "ReOpen";
                        BindingNavigatorCancelItem.Enabled = MbCancelPermission;
                        BindingNavigatorSaveItem.Enabled = false;
                        txtDescription.Text = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.strCancellationDescription;
                        lblCreatedByText.Text += "          |   Cancelled By : " + MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.strCancelledBy;
                        lblCreatedDateValue.Text += "           |   Cancelled Date : " + MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.strCancellationDate.ToDateTime().ToString("dd-MMM-yyyy");
                        lblDescription.Visible = txtDescription.Visible = true;
                        dgvSales.ReadOnly = true;
                    }
                    cboCompany.Enabled = cboCustomer.Enabled = cboInvoiceNo.Enabled = false;
                    blnCalculate = true;
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayReturnMasterDetails() " + ex.Message);
                mObjLogs.WriteLog("Error in DisplaySalesReturnMasterDetails() " + ex.Message, 2);
            }
            return false;
        }


        private void FrmSalesReturn_Load(object sender, EventArgs e)
        {
            try
            {
                SetPermissions();
                dtpSFrom.Value = ClsCommonSettings.GetServerDate();
                dtpSTo.Value = ClsCommonSettings.GetServerDate();
                LoadMessage();
                LoadCombos(0);
                AddNew();
                if (PlngReferenceID > 0)
                {
                    DisplaySalesReturnMasterDetails(PlngReferenceID);
                }

            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FrmSalesReturn_Load() " + ex.Message);
                mObjLogs.WriteLog("Error in FrmSalesReturn_Load() " + ex.Message, 2);
            }
        }


        //private void FillBatchNoColumn(int intRowIndex)
        //{
        //    DataTable datCombos = new DataTable();
        //    //datCombos = MobjclsBLLSalesReturn.FillCombos(new string[] { "BD.BatchID,BD.BatchNo", "InvItemIssueReferenceDetails IRD Inner Join InvItemIssueMaster IM On IM.ItemIssueID = IRD.ItemIssueID Inner Join InvBatchDetails BD On BD.BatchID = IRD.BatchID And BD.ItemID = IRD.ItemID ", "IM.OrderTypeID =8 And IRD.ReferenceID = " + cboInvoiceNo.SelectedValue.ToInt64() + " And IRD.ItemID = " + dgvSales[ItemID.Index, intRowIndex].Value.ToInt32() });
        //    datCombos = MobjclsBLLSalesReturn.GetBatch(cboInvoiceNo.SelectedValue.ToInt32(), dgvSales[ItemID.Index, intRowIndex].Value.ToInt32());
        //    BatchNo.ValueMember = "BatchID";
        //    BatchNo.DisplayMember = "BatchNo";
        //    BatchNo.DataSource = datCombos;
        //    dgvSales[BatchNo.Index, intRowIndex].Tag = datCombos.Rows[0]["BatchID"].ToInt64();
        //    dgvSales[BatchID.Index, intRowIndex].Value = dgvSales[BatchNo.Index, intRowIndex].Tag;
        //    dgvSales[BatchNo.Index, intRowIndex].Value = datCombos.Rows[0]["BatchID"].ToInt64();
        //    dgvSales[BatchNo.Index, intRowIndex].Value = dgvSales[BatchNo.Index, intRowIndex].FormattedValue;
        //   // dgvSales[BatchNo.Index, intRowIndex].Value = dgvSales[BatchNo.Index, intRowIndex].Tag;

        //    //dgvSales.CommitEdit(DataGridViewDataErrorContexts.Commit);
        //}

        private void dgvSales_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dgvSales.PServerName = ClsCommonSettings.ServerName;
                // string[] First = { "ItemCode", "ItemName", "ItemID", "ReferenceSerialNo","BatchID","BatchNo","InvoicedQuantity","InvQty","InvUomID","InvRate","Discount","WarehouseID","IsGroup" };//first grid 
                //string[] second = { "ItemCode", "ItemName", "ItemID", "ReferenceSerialNo", "BatchID", "BatchNo","InvoicedQuantity", "InvQty", "InvUomID","InvRate","Discount","WarehouseID","IsGroup" };//inner grid
                string[] First = { "ItemCode", "ItemName", "ItemID", "IsGroup", "ReferenceSerialNo", "BatchID", "BatchNo", "InvRate", "InvUomID", "Discount" };//first grid 
                string[] second = { "Code", "ItemName", "ItemID", "IsGroup" ,"ReferenceSerialNo", "BatchID", "BatchNo", "InvRate", "InvUomID", "Discount" };//inner 
                dgvSales.aryFirstGridParam = First;
                dgvSales.arySecondGridParam = second;
                dgvSales.PiFocusIndex = ReturnQuantity.Index;
                dgvSales.iGridWidth = 500;
                dgvSales.bBothScrollBar = true;
                dgvSales.ColumnsToHide = new string[] { "ItemID", "InvUomID", "ReferenceSerialNo", "Discount", "IsGroup", "BatchID","BatchNo" };

               
                DataTable dtItemSelectionTable = MobjclsBLLSalesReturn.GetSalesInvoiceItemDetails(Convert.ToInt32(cboInvoiceNo.SelectedValue));
                
                string strFilterCondition = "";
                if (dgvSales.CurrentCell.OwningColumn.Index == 0)
                {
                    strFilterCondition = " Code Like '" + ((dgvSales.TextBoxText)) + "%'";
                    dgvSales.field = "Code";
                }
                else
                {
                    strFilterCondition = " ItemName Like '" + ((dgvSales.TextBoxText)) + "%'";
                    dgvSales.field = "ItemName";
                }

                //DataTable datSelectedIDs = new DataTable();
                //datSelectedIDs.Columns.Add("ReferenceSerialNo");
                //datSelectedIDs.Columns.Add("ItemID");
                //foreach (DataGridViewRow dr in dgvSales.Rows)
                //{
                //    if (dr.Cells["ReferenceSerialNo"].Value != null && Convert.ToInt32(dr.Cells["ReferenceSerialNo"].Value) > 0)
                //    {
                //        DataRow drSelectedRow = datSelectedIDs.NewRow();
                //        drSelectedRow["ReferenceSerialNo"] = Convert.ToInt32(dr.Cells["ReferenceSerialNo"].Value);
                //        drSelectedRow["ItemID"] = Convert.ToInt32(dr.Cells["ItemID"].Value);
                //        datSelectedIDs.Rows.Add(drSelectedRow);
                //    }
                //}

                //if (datSelectedIDs.Rows.Count > 0)
                //{
                //    if (!string.IsNullOrEmpty(strFilterCondition))
                //        strFilterCondition += " And ";
                //    strFilterCondition += "ReferenceSerialNo Not In (";
                //    foreach (DataRow dr in datSelectedIDs.Rows)
                //    {
                //        strFilterCondition += intReferenceSerialNo + ",";
                //    }
                //    strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                //    strFilterCondition += ")";
                //}

                dtItemSelectionTable.DefaultView.RowFilter = strFilterCondition;
                DataTable datTemp = dtItemSelectionTable.DefaultView.ToTable();
                datTemp.DefaultView.Sort = dgvSales.field;
                dgvSales.dtDataSource = datTemp.DefaultView.ToTable();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvSales_Textbox_TextChanged() " + ex.Message);
                mObjLogs.WriteLog("Error in dgvSales_Textbox_TextChanged() " + ex.Message, 2);
            }
        }
        private bool ValidateFields()
        {
            try
            {
                // Validating datas
                if (dgvSales.CurrentCell != null)
                    dgvSales.CurrentCell = dgvSales["ItemCode", dgvSales.CurrentRow.Index];
                bool blnValid = true;
                Control cntrlFocus = null;
                ErrSalesReturn.Clear();
                
                if (cboCompany.SelectedValue == null)
                {
                    //Please select Company
                    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2039, out MmessageIcon);
                    cntrlFocus = cboCompany;
                    blnValid = false;
                }
                //else if (!MobjclsBLLSalesReturn.GetCompanyAccount(Convert.ToInt32(TransactionTypes.SaleReturn), cboCompany.SelectedValue.ToInt32()))
                //{
                //    //MsMessageCommon = "Please create a Sales Return Account for the selected company.";
                //    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2082, out MmessageIcon);
                //    cntrlFocus = cboCompany;
                //    blnValid = false;
                //}
                else if (cboCustomer.SelectedValue == null)
                {
                    //Please select customer
                    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 1951, out MmessageIcon);
                    cntrlFocus = cboCustomer;
                    blnValid = false;
                }
                else if (cboAccount.SelectedIndex == -1)
                {
                    //Please select sales account
                    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2092, out MmessageIcon);
                    cntrlFocus = cboAccount;
                    blnValid = false;
                }
                else if (cboInvoiceNo.SelectedValue == null)
                {
                    //Please select sales invoice
                    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2042, out MmessageIcon);
                    cntrlFocus = cboInvoiceNo;
                    blnValid = false;
                }
                else if (txtReturnNo.Text == "")
                {
                    //Please select currency
                    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2295, out MmessageIcon);
                    cntrlFocus = txtReturnNo;
                    blnValid = false;
                }
                else if (!txtReturnNo.ReadOnly && MobjclsBLLSalesReturn.IsReturnNoExists(txtReturnNo.Text, cboCompany.SelectedValue.ToInt32()))
                {
                    //MsMessageCommon = "Sales return No already Exist";
                    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2062, out MmessageIcon);
                    cntrlFocus = txtReturnNo;
                    blnValid = false;
                }
               
                else if (cboCurrency.SelectedValue == null)
                {
                    //Please select currency
                    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 1958, out MmessageIcon);
                    cntrlFocus = cboCurrency;
                    blnValid = false;
                }
     
                else if (dgvSales.Rows[0].Cells["ItemID"].Value == null)
                {
                    //Please select items
                    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2010, out MmessageIcon);
                    cntrlFocus = dgvSales;
                    blnValid = false;
                }
                else if (txtGrandReturnAmount.Text == string.Empty || txtGrandReturnAmount.Text.Trim() == ".")
                {
                    //MsMessageCommon = "Please enter grand return amount";
                    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2041, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesReturnStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmrSalesReturn.Enabled = true;
                    txtGrandReturnAmount.Focus();
                    return false;
                }
                else if (Convert.ToDecimal(txtGrandReturnAmount.Text) <= 0)
                {
                    //MsMessageCommon = "Please enter grand return amount";
                    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2041, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesReturnStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmrSalesReturn.Enabled = true;
                    txtGrandReturnAmount.Focus();
                    return false;
                }

                else if (blnValid)
                {
                    int iTempID = 0;
                    iTempID = CheckDuplicationInGrid();
                    if (iTempID != -1)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 1954, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblSalesReturnStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        TmrSalesReturn.Enabled = true;
                        dgvSales.CurrentCell = dgvSales["ItemCode", iTempID];
                        dgvSales.Focus();
                        return false;
                    }
                }
                else if (MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intSalesReturnID == 0)
                {
                    if (MobjclsBLLSalesReturn.FillCombos(new string[] { "SalesInvoiceID", "InvSalesInvoiceMaster", "SalesInvoiceID = " + Convert.ToInt32(cboInvoiceNo.SelectedValue) + " And StatusID IN(  " + (Int32)OperationStatusType.SDelivered + "," + (Int32)OperationStatusType.SPartiallyDelivered + ")" }).Rows.Count == 0)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2047, out MmessageIcon);
                        MsMessageCommon = MsMessageCommon.Replace("*", "SalesInvoice");
                        cntrlFocus = cboInvoiceNo;
                        blnValid = false;
                    }
                }
                if (blnValid)
                {
                    DataTable dtInvoice = new DataTable();
                    if (cboInvoiceNo.SelectedValue != null)
                    {
                        dtInvoice = MobjclsBLLSalesReturn.FillCombos(new string[] { "InvoiceDate", "InvSalesInvoiceMaster", "SalesInvoiceID = " + Convert.ToInt32(cboInvoiceNo.SelectedValue) });

                    }
                    else
                    {
                        dtInvoice = null;
                    }
                    if (dtInvoice != null && dtInvoice.Rows[0]["InvoiceDate"] != null)
                    {
                        if (dtpReturnDate.Value.Date < Convert.ToDateTime(dtInvoice.Rows[0]["InvoiceDate"]))
                        {
                            // return date cannot be less than invoice date
                            MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2104, out MmessageIcon);

                            cntrlFocus = dtpReturnDate;
                            dtpReturnDate.Focus();
                            blnValid = false;
                        }
                    }
                    if (dtpReturnDate.Value.Date > ClsCommonSettings.GetServerDate())
                    {
                        // return date cannot be greater than current date
                        MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2103, out MmessageIcon);
                        cntrlFocus = dtpReturnDate;
                        blnValid = false;
                    }
                }
               // blnValid = ValidateCompanyAccount();

                if (!blnValid)
                {
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErrSalesReturn.SetError(cntrlFocus, MsMessageCommon.Replace("#", "").Trim());
                    lblSalesReturnStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrSalesReturn.Enabled = true;
                    cntrlFocus.Focus();
                }
                else if (!ValidateGrid())
                {
                    blnValid = false;
                }
                return blnValid;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in ValidateFields() " + ex.Message);
                mObjLogs.WriteLog("Error in ValidateFields() " + ex.Message, 2);
                return false;
            }
        }

        private bool ValidateCompanyAccount()
        {
            DataTable datTemp = MobjclsBLLSalesReturn.FillCombos(new string[] { "*", "AccAccountSettings", "CompanyID=" + cboCompany.SelectedValue.ToString() + " AND AccountTransactionTypeID=" + (int)TransactionTypes.SaleReturn });
            if (datTemp == null)
            {
                //MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2059, out MmessageIcon);
                //MessageBox.Show(MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //lblSalesReturnStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                //TmrSalesReturn.Enabled = true;
                return false;
            }
            else
            {
                if (datTemp.Rows.Count == 0)
                {
                    //MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2059, out MmessageIcon);
                    //MessageBox.Show(MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    //lblSalesReturnStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    //TmrSalesReturn.Enabled = true;
                    return false;
                }
            }
            return true;
        }

        private decimal ConvertValueToBaseUnitValue(int intUOMID, int intItemID, decimal decValue)
        {
            DataTable dtUomDetails = MobjclsBLLSalesReturn.GetUomConversionValues(intUOMID, intItemID);
            if (dtUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                    decValue = decValue / decConversionValue;
                else if (intConversionFactor == 2)
                    decValue = decValue * decConversionValue;
            }
            return decValue;
        }
        private int CheckDuplicationInGrid()
        {
            int intItemID = 0; bool blnIsGroup = false;
            long lngBatchID = 0;
            int RowIndexTemp = 0;
            bool blnCheckBatchID = false;

            foreach (DataGridViewRow rowValue in dgvSales.Rows)
            {
                if (rowValue.Cells["ItemID"].Value != null)
                {
                    intItemID = Convert.ToInt32(rowValue.Cells["ItemID"].Value);
                    blnIsGroup = Convert.ToBoolean(rowValue.Cells["IsGroup"].Value);
                    lngBatchID = rowValue.Cells["BatchID"].Value.ToInt64();
                    RowIndexTemp = rowValue.Index;
                    foreach (DataGridViewRow row in dgvSales.Rows)
                    {
                        if (RowIndexTemp != row.Index)
                        {
                            if (row.Cells["ItemID"].Value != null && row.Cells["BatchID"].Value.ToInt64() != 0)
                            {
                                if ((Convert.ToInt32(row.Cells["ItemID"].Value) == intItemID) && (row.Cells["BatchID"].Value.ToInt64() == lngBatchID))
                                    return row.Index;
                            }
                            else if (row.Cells["ItemID"].Value != null)
                            {
                                if ((Convert.ToInt32(row.Cells["ItemID"].Value) == intItemID) && Convert.ToBoolean(row.Cells["IsGroup"].Value) == blnIsGroup)
                                    return row.Index;
                            }
                        }
                    }
                }
            }
            return -1;
        }
        private bool ValidateGrid()
        {
            bool blnIsValid = true;
            int intItemCount = 0, intRowIndex = 0;
            string strColumnName = "";

            for (int i = 0; i < dgvSales.Rows.Count; i++)
            {
                if (dgvSales["ReferenceSerialNo", i].Value != null)
                {
                    if (dgvSales["ReturnQuantity", i].Value == null || dgvSales["ReturnQuantity", i].Value.ToDecimal() == 0)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2011, out MmessageIcon);
                        intRowIndex = i;
                        strColumnName = "ReturnQuantity";
                        blnIsValid = false;
                    }
                    else if (dgvSales["Uom", i].Tag == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 1961, out MmessageIcon);
                        intRowIndex = i;
                        strColumnName = "Uom";
                        blnIsValid = false;
                    }
                    else if (dgvSales["Reason", i].Value == null || Convert.ToInt32(dgvSales["Reason", i].Value) == 0)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2043, out MmessageIcon);
                        intRowIndex = i;
                        strColumnName = "Reason";
                        blnIsValid = false;
                    }
                    else if (dgvSales["ReturnAmount", i].Value == null || dgvSales["ReturnAmount", i].Value.ToDecimal() == 0)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2040, out MmessageIcon);
                        intRowIndex = i;
                        strColumnName = "ReturnAmount";
                        blnIsValid = false;
                    }
                    else
                    {
                        int intInvUom = 0, intUom = 0;
                        decimal decInvQty = 0, decRetQty = 0;
                        decInvQty = Convert.ToDecimal(dgvSales["InvQty", i].Value);
                        intInvUom = Convert.ToInt32(dgvSales["InvUomID", i].Value);
                        decRetQty = Convert.ToDecimal(dgvSales["ReturnQuantity", i].Value);
                        intUom = Convert.ToInt32(dgvSales["Uom", i].Tag);

                        decRetQty = ConvertValueToBaseUnitValue(intUom, Convert.ToInt32(dgvSales["ItemID", i].Value), decRetQty);

                        decInvQty = ConvertValueToBaseUnitValue(intInvUom, Convert.ToInt32(dgvSales["ItemID", i].Value), decInvQty);

                        DataTable datSalesReturnDetails = new DataTable();
                        if (!MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.blnIsCancelled)
                            datSalesReturnDetails = MobjclsBLLSalesReturn.FillCombos(new string[] { "ItemID,BatchID,ReturnQuantity,UOMID,IsGroup", "InvSalesReturnDetails Inner Join InvSalesReturnMaster On InvSalesReturnMaster.SalesReturnID = InvSalesReturnDetails.SalesReturnID", "SalesInvoiceID = " + Convert.ToInt32(cboInvoiceNo.SelectedValue) + " And InvSalesReturnMaster.StatusID = " + (Int32)OperationStatusType.SSalesReturnOpen });
                        else
                            datSalesReturnDetails = MobjclsBLLSalesReturn.FillCombos(new string[] { "ItemID,BatchID,ReturnQuantity,UOMID,IsGroup", "InvSalesReturnDetails Inner Join InvSalesReturnMaster On InvSalesReturnMaster.SalesReturnID = InvSalesReturnDetails.SalesReturnID", "SalesInvoiceID = " + Convert.ToInt32(cboInvoiceNo.SelectedValue) + " And InvSalesReturnMaster.StatusID = " + (Int32)OperationStatusType.SSalesReturnOpen + " And InvSalesReturnMaster.SalesReturnID <> " + MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intSalesReturnID });
                        datSalesReturnDetails.DefaultView.RowFilter = "ItemID = " + Convert.ToInt32(dgvSales["ItemID", i].Value) + " And BatchID = " + dgvSales["BatchID", i].Value + "And IsGroup=" + dgvSales["IsGroup", i].Value.ToBoolean();
                        decimal decAlreadyRetQty = 0;
                        foreach (DataRow dr in datSalesReturnDetails.DefaultView.ToTable().Rows)
                        {
                            decimal decRQty = Convert.ToDecimal(dr["ReturnQuantity"]);
                            decRQty = ConvertValueToBaseUnitValue(Convert.ToInt32(dr["UOMID"]), Convert.ToInt32(dgvSales["ItemID", i].Value), decRQty);
                            decAlreadyRetQty += decRQty;
                        }

                        decimal decOldQty = 0;
                        DataTable datOldSalesReturnDetails = MobjclsBLLSalesReturn.FillCombos(new string[] { "ItemID,BatchID,ReturnQuantity,UOMID,IsGroup", "InvSalesReturnDetails Inner Join InvSalesReturnMaster On InvSalesReturnMaster.SalesReturnID = InvSalesReturnDetails.SalesReturnID ", "InvSalesReturnDetails.SalesReturnID = " + MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intSalesReturnID + " And ItemID = " + Convert.ToInt32(dgvSales["ItemID", i].Value) + " And BatchID = " + dgvSales["BatchID", i].Value.ToInt64() + " And InvSalesReturnMaster.StatusID <> " + (Int32)OperationStatusType.SSalesReturnCancelled +"And IsGroup="+Convert.ToInt32(dgvSales["IsGroup",i].Value)});
                        if (datOldSalesReturnDetails.Rows.Count > 0)
                            decOldQty = ConvertValueToBaseUnitValue(Convert.ToInt32(datOldSalesReturnDetails.Rows[0]["UOMID"]), Convert.ToInt32(dgvSales["ItemID", i].Value), Convert.ToDecimal(datOldSalesReturnDetails.Rows[0]["ReturnQuantity"]));
                        if (decInvQty - decAlreadyRetQty + decOldQty < decRetQty)
                        {
                            MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2012, out MmessageIcon);
                            intRowIndex = i;
                            strColumnName = "ReturnQuantity";
                            blnIsValid = false;
                        }
                    }
                    intItemCount++;
                }
            }
            if (intItemCount == 0)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2010, out MmessageIcon);
                intRowIndex = 0;
                strColumnName = "ItemCode";
                blnIsValid = false;
            }
            if (blnIsValid)
            {

            }

            if (!blnIsValid)
            {
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrSalesReturn.SetError(dgvSales, MsMessageCommon.Replace("#", "").Trim());
                lblSalesReturnStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                TmrSalesReturn.Enabled = true;
                dgvSales.CurrentCell = dgvSales[strColumnName, intRowIndex];
                dgvSales.Focus();
            }
            return blnIsValid;
        }

        private void dgvSales_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                try
                {
                    if (e.ColumnIndex == BatchID.Index)
                    {
                        DataTable datBatchWiseIssueDetails = MobjclsBLLSalesReturn.GetBatchWiseIssueDetails(cboInvoiceNo.SelectedValue.ToInt64(), dgvSales[ItemID.Index, e.RowIndex].Value.ToInt32(), dgvSales[BatchID.Index, e.RowIndex].Value.ToInt64(), dgvSales[IsGroup.Index, e.RowIndex].Value.ToBoolean());
                        dgvSales.Rows[e.RowIndex].Cells["IssuedUOMID"].Value = datBatchWiseIssueDetails.Rows[0]["InvUOM"];
                        dgvSales.Rows[e.RowIndex].Cells["WarehouseID"].Value = datBatchWiseIssueDetails.Rows[0]["WarehouseID"];

                        FillUOMColumn(dgvSales.Rows[e.RowIndex].Cells[ItemID.Index].Value.ToInt32(), Convert.ToBoolean(datBatchWiseIssueDetails.Rows[0]["IsGroup"]));
                        dgvSales.Rows[e.RowIndex].Cells["Uom"].Tag = Convert.ToInt32(datBatchWiseIssueDetails.Rows[0]["InvUOM"]);
                        dgvSales.Rows[e.RowIndex].Cells["Uom"].Value = Convert.ToInt32(datBatchWiseIssueDetails.Rows[0]["InvUOM"]);
                        dgvSales.Rows[e.RowIndex].Cells["Uom"].Value = dgvSales.Rows[e.RowIndex].Cells["Uom"].FormattedValue;

                        int intUomScale = 0;

                        if (dgvSales[Uom.Index, e.RowIndex].Tag.ToInt32() != 0)
                            intUomScale = MobjclsBLLSalesReturn.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + datBatchWiseIssueDetails.Rows[0]["InvUOM"].ToInt32() }).Rows[0]["Scale"].ToInt32();

                        dgvSales.Rows[e.RowIndex].Cells["InvoicedQuantity"].Value = datBatchWiseIssueDetails.Rows[0]["InvQty"].ToDecimal().ToString("F" + intUomScale);
                        dgvSales.Rows[e.RowIndex].Cells["InvQty"].Value = datBatchWiseIssueDetails.Rows[0]["InvQty"].ToDecimal().ToString("F" + intUomScale);
                        dgvSales.Rows[e.RowIndex].Cells["SalesInvoicedQuantity"].Value = datBatchWiseIssueDetails.Rows[0]["SInvoicequantity"].ToDecimal().ToString("F" + intUomScale);
                        dgvSales.Rows[e.RowIndex].Cells["SalesInvoiceDiscountAmount"].Value = datBatchWiseIssueDetails.Rows[0]["InvoiceDiscountAmount"];
                        dgvSales.Rows[e.RowIndex].Cells["SalesInvoiceRate"].Value = datBatchWiseIssueDetails.Rows[0]["InvoiceSaleRate"];

                        decDiscountPerItem = Convert.ToDecimal(dgvSales.Rows[e.RowIndex].Cells["SalesInvoiceDiscountAmount"].Value) / Convert.ToDecimal(dgvSales.Rows[e.RowIndex].Cells["SalesInvoicedQuantity"].Value);
                        dgvSales.Rows[e.RowIndex].Cells["DiscountAmount"].Value = decDiscountPerItem.ToDecimal();

                        dgvSales[ReturnQuantity.Index, e.RowIndex].Value = Convert.ToDecimal(1).ToString("F" + intUomScale);
                        dgvSales[ReturnedQty.Index, e.RowIndex].Value = MobjclsBLLSalesReturn.GetAlreadyReturnedQty(cboInvoiceNo.SelectedValue.ToInt32(), dgvSales[ItemID.Index, e.RowIndex].Value.ToInt32(), dgvSales[BatchID.Index, e.RowIndex].Value.ToInt64(), dgvSales[IsGroup.Index, e.RowIndex].Value.ToBoolean()).ToDecimal().ToString("F" + intUomScale);
                   
                        //dgvSales[ReturnQuantity.Index, e.RowIndex].Value = Convert.ToDecimal(1).ToString("F" + MintExchangeCurrencyScale);
                        //dgvSales[ReturnedQty.Index, e.RowIndex].Value = MobjclsBLLSalesReturn.GetAlreadyReturnedQty(cboInvoiceNo.SelectedValue.ToInt32(), dgvSales[ItemID.Index, e.RowIndex].Value.ToInt32(), dgvSales[BatchID.Index, e.RowIndex].Value.ToInt64());
                    }
                    else if (e.ColumnIndex == ItemID.Index)
                    {
                        dgvSales[Uom.Index, e.RowIndex].Value = null;
                        dgvSales[Uom.Index, e.RowIndex].Tag = null;
                        dgvSales[ReturnAmount.Index, e.RowIndex].Value = null;
                        dgvSales[BatchID.Index, e.RowIndex].Value = null;
                        dgvSales[BatchNo.Index, e.RowIndex].Value = null;
                        dgvSales[BatchNo.Index, e.RowIndex].Tag = null;
                        dgvSales[ReturnedQty.Index, e.RowIndex].Value = null;
                        dgvSales[ReturnQuantity.Index, e.RowIndex].Value = null;
                        dgvSales[Rate.Index, e.RowIndex].Value = null;
                        //FillBatchNoColumn(e.RowIndex);
                    }
                    else if (e.ColumnIndex == ReferenceSerialNo.Index)
                    {
                        //dgvSales[ReturnedQty.Index, e.RowIndex].Value = MobjclsBLLSalesReturn.GetAlreadyReturnedQty(cboInvoiceNo.SelectedValue.ToInt32(), dgvSales[ReferenceSerialNo.Index, e.RowIndex].Value.ToInt32());
                    }
                    else if (dgvSales.Columns[e.ColumnIndex].Name == "ReturnQuantity" || dgvSales.Columns[e.ColumnIndex].Name == "Rate")
                    {
                        //if (e.ColumnIndex == ReturnQuantity.Index)
                        //{
                        //    int intDecimalPart = 0;
                        //    if (dgvSales.CurrentCell.Value != null && !string.IsNullOrEmpty(dgvSales.CurrentCell.Value.ToString()) && dgvSales.CurrentCell.Value.ToString().Contains("."))
                        //        intDecimalPart = dgvSales.CurrentCell.Value.ToString().Split('.')[1].Length;
                        //    int intUomScale = 0;
                        //    if (dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                        //        intUomScale = MobjclsBLLSalesReturn.FillCombos(new string[] { "Scale", "STUOMReference", "UOMID = " + dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                        //    if (intDecimalPart > intUomScale)
                        //    {
                        //        dgvSales.CurrentCell.Value = Math.Round(dgvSales.CurrentCell.Value.ToDecimal(), intUomScale).ToString();
                        //        dgvSales.EditingControl.Text = dgvSales.CurrentCell.Value.ToString();
                        //    }
                        //}
                        decimal decValue = 0;
                        try
                        {
                            decValue = Convert.ToDecimal(dgvSales[e.ColumnIndex, e.RowIndex].Value);
                        }
                        catch
                        {
                            dgvSales[e.ColumnIndex, e.RowIndex].Value = 0;
                        }
                        //CalculateTotalAmt();
                        CalcuateGridTotal();
                        CalculateNetTotal();
                    }
                    else if (e.ColumnIndex == IssuedUOMID.Index)
                    {
                        FillUOMColumn(Convert.ToInt32(dgvSales["ItemID", e.RowIndex].Value), Convert.ToBoolean(dgvSales["IsGroup", e.RowIndex].Value));
                        dgvSales["Uom", e.RowIndex].Tag = dgvSales["InvUomID", e.RowIndex].Value;
                        dgvSales["Uom", e.RowIndex].Value = dgvSales["InvUomID", e.RowIndex].Value;
                        dgvSales["Uom", e.RowIndex].Value = dgvSales["Uom", e.RowIndex].FormattedValue;
                    }
                    else if (dgvSales.Columns[e.ColumnIndex].Name == "InvRate")
                    {
                        dgvSales["Rate", e.RowIndex].Value = dgvSales["InvRate", e.RowIndex].Value;
                    }
                    else if (dgvSales.Columns[e.ColumnIndex].Name == "Uom")
                    {
                        if (dgvSales["Uom", e.RowIndex].Value != null)
                        {

                            int intUom = 0, intInvUom = 0;
                            decimal decRate = 0, decInvRate = 0;
                            decInvRate = Convert.ToDecimal(dgvSales["InvRate", e.RowIndex].Value);
                            intInvUom = Convert.ToInt32(dgvSales["InvUomID", e.RowIndex].Value);
                            intUom = Convert.ToInt32(dgvSales["Uom", e.RowIndex].Tag);

                            DataTable dtUomDetails = MobjclsBLLSalesReturn.GetUomConversionValues(intInvUom, Convert.ToInt32(dgvSales["ItemID", e.RowIndex].Value));
                            if (dtUomDetails.Rows.Count > 0)
                            {
                                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                                if (intConversionFactor == 1)
                                    decInvRate = decInvRate * decConversionValue;
                                else if (intConversionFactor == 2)
                                    decInvRate = decInvRate / decConversionValue;
                            }

                            dtUomDetails = MobjclsBLLSalesReturn.GetUomConversionValues(intUom, Convert.ToInt32(dgvSales["ItemID", e.RowIndex].Value));
                            decRate = decInvRate;
                            if (dtUomDetails.Rows.Count > 0)
                            {
                                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                                if (intConversionFactor == 1)
                                    decRate = decInvRate / decConversionValue;
                                else if (intConversionFactor == 2)
                                    decRate = decInvRate * decConversionValue;
                            }
                            dgvSales["Rate", e.RowIndex].Value = decRate;
                        }
                    }
                    else if (dgvSales.Columns[e.ColumnIndex].Name == "ReturnAmount")
                    {
                        decimal decTotalAmount = 0;
                        foreach (DataGridViewRow dr in dgvSales.Rows)
                        {
                            if (dr.Cells["ReturnAmount"].Value != null)
                                decTotalAmount += Convert.ToDecimal(dr.Cells["ReturnAmount"].Value);
                        }
                        txtSubtotal.Text = decTotalAmount.ToString("F" + MintExchangeCurrencyScale);
                        CalculateNetTotal();
                    }
                    else if (dgvSales.Columns[e.ColumnIndex].Name == "NetAmount")
                    {
                        dgvSales[ReturnAmount.Index, e.RowIndex].Value = dgvSales[NetAmount.Index, e.RowIndex].Value;
                    }
                    else if (dgvSales.Columns[e.ColumnIndex].Name == "Discount")
                    {
                        //CalculateTotalAmt();
                        CalcuateGridTotal();
                        CalculateNetTotal();
                    }
                    if (dgvSales.Columns[e.ColumnIndex].Name == "DiscountAmount" || dgvSales.Columns[e.ColumnIndex].Name == "ReturnQuantity")
                    {
                        dgvSales.Rows[e.RowIndex].Cells["DiscountAmount"].Value = (Convert.ToDecimal(dgvSales.Rows[e.RowIndex].Cells["DiscountAmount"].Value)).ToString("F" + MintExchangeCurrencyScale);

                        //dgvSales.Rows[e.RowIndex].Cells["InvRate"].Value = (Convert.ToDecimal( dgvSales.Rows[e.RowIndex].Cells["SalesInvoiceRate"].Value)  - (decDiscountPerItem * Convert.ToDecimal(dgvSales.Rows[e.RowIndex].Cells["ReturnQuantity"].Value))).ToString();
                    }
                    Changestatus(null, null);
                }
                catch (Exception ex)
                {
                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error in dgvSales_CellValueChanged() " + ex.Message);
                    mObjLogs.WriteLog("Error in dgvSales_CellValueChanged() " + ex.Message, 2);
                }
            }
        }


        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveSalesReturn())
                {
                    AddNew();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorSaveItem_Click() " + ex.Message);
                mObjLogs.WriteLog("Error in BindingNavigatorSaveItem_Click() " + ex.Message, 2);
            }
        }

        private bool SaveSalesReturn()
        {
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intCompanyID = cboCompany.SelectedValue.ToInt32();

            if (ValidateFields())
            {

                MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, (MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intSalesReturnID == 0 ? 1 : 3), out MmessageIcon);
                if (blnCanShow)
                {
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                        MessageBoxIcon.Information) == DialogResult.No)
                        return false;
                }

                if (MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intSalesReturnID == 0 && txtReturnNo.ReadOnly && MobjclsBLLSalesReturn.IsReturnNoExists(txtReturnNo.Text, cboCompany.SelectedValue.ToInt32()))
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2061, out MmessageIcon);
                    MsMessageCommon = MsMessageCommon.Replace("*", "Return");
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    else
                        GenerateSalesReturnNo();
                }
                FillMasterParameters();
                FillDetParameters();
                if ((MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intStatusID != (int)OperationStatusType.SSalesReturnCancelled) && MblnIsFromCancellation)
                {

                }
                //else
                //{
                //    string strInValidItem = MobjclsBLLSalesReturn.StockValidation(false);
                //    if (!string.IsNullOrEmpty(strInValidItem))
                //    {
                //        MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2075, out MmessageIcon).Replace("#", "");
                //        MsMessageCommon = MsMessageCommon.Replace("*", strInValidItem);
                //        MessageBox.Show(MsMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //        return false;
                //    }
                //}
                return MobjclsBLLSalesReturn.SaveSalesReturnMaster();
            }
            return false;
        }

        private void FillMasterParameters()
        {
            //if (MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intSalesReturnID == 0)
            //{
            //    DataTable datAccountDetails = MobjclsBLLSalesReturn.FillCombos(new string[] { "AccountID", "CompanyMaster", "CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) });
            //    MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intDebitHeadID = ClsCommonSettings.PintSIDebitHeadID;
            //    MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intCreditHeadID = GetCustomerAccountID(Convert.ToInt32(cboCustomer.SelectedValue));
            //}

            //MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intDebitHeadID = 0;
            //if (MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intDebitHeadID == 0)
            //{
            //    DataTable datTemp = MobjclsBLLSalesReturn.FillCombos(new string[] { "*", "AccountSettings", "CompanyID=" + cboCompany.SelectedValue.ToString() + " AND AccountTransactionTypeID=" + (int)TransactionTypes.SaleReturn });
            //    if (datTemp.Rows.Count > 0)
            //        try { MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intDebitHeadID = Convert.ToInt32(datTemp.Rows[0]["AccountID"].ToString()); }
            //        catch { }
            //}
            //MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intCreditHeadID = (int)Accounts.CashAccount ;

            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intSalesInvoiceID = Convert.ToInt32(cboInvoiceNo.SelectedValue);
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.strSalesReturnNo = txtReturnNo.Text.Trim();
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.strReturnDate = dtpReturnDate.Value.ToString("dd-MMM-yyyy");
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.strRemarks = txtRemarks.Text.Trim();
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intCurrencyID = Convert.ToInt32(cboCurrency.SelectedValue);
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intGrandDiscountID = Convert.ToInt32(cboDiscount.SelectedValue);
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decGrandDiscountAmount = Convert.ToDecimal(txtDiscount.Text.Trim());
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decNetAmount = Convert.ToDecimal(txtTotalAmount.Text.Trim());
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decGrandAmount = txtSubtotal.Text.ToDecimal();
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decGrandReturnAmount = Convert.ToDecimal(txtGrandReturnAmount.Text.Trim());
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intCreatedBy = ClsCommonSettings.UserID.ToInt32();
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.strCreatedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intAccountID = Convert.ToInt32(cboAccount.SelectedValue);
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intTaxSchemeID =Convert.ToInt32(cboTaxScheme.SelectedValue);
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.decTaxAmount = Convert.ToDecimal(txtTaxAmount.Text);
            if (MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intStatusID == (Int32)OperationStatusType.SSalesReturnCancelled)
            {
                MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.strCancellationDescription = txtDescription.Text.Trim();
                MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intStatusID = (Int32)OperationStatusType.SSalesReturnCancelled;
            }
            else
            {
                MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intStatusID = (Int32)OperationStatusType.SSalesReturnOpen;
            }
        }

        private void FillDetParameters()
        {
            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.lstSalesReturnDetails = new List<clsDTOSalesReturnDetails>();
            decimal TotalQtyGrp = 0;
            decimal AvailbleQtyinBatch = 0;
            decimal TotalQtyBatchless = 0;
            //decimal QtySelected = 0;
            DataTable datUsedQtyBatchWise = new DataTable();
            datUsedQtyBatchWise.Columns.Add("ItemID");
            datUsedQtyBatchWise.Columns.Add("BatchID");
            datUsedQtyBatchWise.Columns.Add("ReturnQuantity", typeof(decimal));
            datUsedQtyBatchWise.Columns.Add("UOMID");
            for (int i = 0; i < dgvSales.Rows.Count; i++)
            {
                if (dgvSales["ReferenceSerialNo", i].Value != null && Convert.ToInt32(dgvSales["ReferenceSerialNo", i].Value) > 0)
                {
                    clsDTOSalesReturnDetails objClsDtoSalesReturnDetails = null;
                    datWarehouse = MobjclsBLLSalesReturn.GetWareHouse(cboInvoiceNo.SelectedValue.ToInt64(), dgvSales["ItemID", i].Value.ToInt32(), dgvSales["BatchID", i].Value.ToInt32());

                    //foreach (DataRow dr in datWarehouse.Rows)
                    //{
                    //    if (Convert.ToBoolean(dgvSales["IsGroup", i].Value) == true)
                    //    {
                    //        if (dr["ItemID"].ToInt32() == Convert.ToInt32(dgvSales["ItemID", i].Value) )
                    //            objClsDtoSalesReturnDetails.intWarehouseID = Convert.ToInt32(dr["WarehouseID"]);
                    //    }
                    //    else
                    //    {
                    //        if (dr["ItemID"].ToInt32() == Convert.ToInt32(dgvSales["ItemID", i].Value) && dr["BatchID"].ToInt32() == Convert.ToInt32(dgvSales["BatchID", i].Value))
                    //            objClsDtoSalesReturnDetails.intWarehouseID = Convert.ToInt32(dr["WarehouseID"]);
                    //    }
                    //}
                    if ((Convert.ToInt32(dgvSales["ItemID", i].Value) != null)  && (Convert.ToBoolean(dgvSales["IsGroup", i].Value) != true))
                    {
                        objClsDtoSalesReturnDetails = new clsDTOSalesReturnDetails();
                        foreach (DataRow dr in datWarehouse.Rows)
                        {
                            if (Convert.ToBoolean(dgvSales["IsGroup", i].Value) == true)
                            {
                                if (dr["ItemID"].ToInt32() == Convert.ToInt32(dgvSales["ItemID", i].Value))
                                    objClsDtoSalesReturnDetails.intWarehouseID = Convert.ToInt32(dr["WarehouseID"]);
                            }
                            else
                            {
                                if (dr["ItemID"].ToInt32() == Convert.ToInt32(dgvSales["ItemID", i].Value) && dr["BatchID"].ToInt32() == Convert.ToInt32(dgvSales["BatchID", i].Value))
                                    objClsDtoSalesReturnDetails.intWarehouseID = Convert.ToInt32(dr["WarehouseID"]);
                            }
                        }
                        objClsDtoSalesReturnDetails.intSalesReturnID = Convert.ToInt32(cboInvoiceNo.SelectedValue);
                        objClsDtoSalesReturnDetails.intReferenceSerialNo = Convert.ToInt32(dgvSales["ReferenceSerialNo", i].Value);
                        objClsDtoSalesReturnDetails.intBatchID = Convert.ToInt64(dgvSales["BatchID", i].Value);
                        objClsDtoSalesReturnDetails.decReturnQuantity = Convert.ToDecimal(dgvSales["ReturnQuantity", i].Value);
                        objClsDtoSalesReturnDetails.intReasonID = Convert.ToInt32(dgvSales["Reason", i].Value);
                        objClsDtoSalesReturnDetails.intUOMID = Convert.ToInt32(dgvSales["Uom", i].Tag);
                        //if (dgvSales["Discount", i].Value != DBNull.Value)
                        //    objClsDtoSalesReturnDetails.intDiscountID = Convert.ToInt32(dgvSales["Discount", i].Value);
                        if (dgvSales["DiscountAmount", i].Value != DBNull.Value)
                            objClsDtoSalesReturnDetails.decDiscountAmount = Convert.ToDecimal(dgvSales["DiscountAmount", i].Value);
                        objClsDtoSalesReturnDetails.decNetAmount = Convert.ToDecimal(dgvSales["NetAmount", i].Value);
                        objClsDtoSalesReturnDetails.decGrandAmount = dgvSales["GrandAmount", i].Value.ToDecimal();
                        objClsDtoSalesReturnDetails.decReturnAmount = Convert.ToDecimal(dgvSales["ReturnAmount", i].Value);

                        objClsDtoSalesReturnDetails.decRate = Convert.ToDecimal(dgvSales["Rate", i].Value);
                        objClsDtoSalesReturnDetails.intItemID = Convert.ToInt32(dgvSales["ItemID", i].Value);
                        MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.lstSalesReturnDetails.Add(objClsDtoSalesReturnDetails);
                    }
                    //else if ((Convert.ToInt32(dgvSales["ItemID", i].Value) != null) && (Convert.ToInt64(dgvSales["BatchID", i].Value) == 0) && (Convert.ToBoolean(dgvSales["IsGroup", i].Value) == false))
                    //{



                        
                    //    //objClsDtoSalesReturnDetails = new clsDTOSalesReturnDetails();
                    //    //foreach (DataRow dr in datWarehouse.Rows)
                    //    //{
                    //    //    if (Convert.ToBoolean(dgvSales["IsGroup", i].Value) == true)
                    //    //    {
                    //    //        if (dr["ItemID"].ToInt32() == Convert.ToInt32(dgvSales["ItemID", i].Value))
                    //    //            objClsDtoSalesReturnDetails.intWarehouseID = Convert.ToInt32(dr["WarehouseID"]);
                    //    //    }
                    //    //    else
                    //    //    {
                    //    //        if (dr["ItemID"].ToInt32() == Convert.ToInt32(dgvSales["ItemID", i].Value) && dr["BatchID"].ToInt32() == Convert.ToInt32(dgvSales["BatchID", i].Value))
                    //    //            objClsDtoSalesReturnDetails.intWarehouseID = Convert.ToInt32(dr["WarehouseID"]);
                    //    //    }
                    //    //}
                        
                    //    //objClsDtoSalesReturnDetails.intItemID = Convert.ToInt32(dgvSales["ItemID", i].Value);
                    //    //objClsDtoSalesReturnDetails.intBatchID = Convert.ToInt64(dgvSales["BatchID", i].Value);
                    //    //objClsDtoSalesReturnDetails.decReturnQuantity = Convert.ToDecimal(dgvSales["ReturnQuantity", i].Value);
                    //    //objClsDtoSalesReturnDetails.intUOMID = Convert.ToInt32(dgvSales["Uom", i].Tag);
                    //    decimal QtySelected = 0;
                    //        //if (!blnIsEditMode)
                    //        //{
                    //    BatchlessItems = MobjclsBLLSalesReturn.GetIssueDetailsBatchlessItems(Convert.ToInt32(dgvSales["ItemID", i].Value));

                    //        //}
                    //        //else
                    //        //{
                    //        //    BatchlessItems = MobjClsBLLDirectDelivery.BatchLessItemsEditMode(Convert.ToInt32(dr.Cells["ItemID"].Value), MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID.ToInt32());
                    //        //}
                    //        TotalQtyBatchless = Convert.ToDecimal(dgvSales["ReturnQuantity",i].Value);

                    //        foreach (DataRow drBatchLess in BatchlessItems.Rows)
                    //        {
                    //            if (Convert.ToDecimal(dgvSales["ReturnQuantity", i].Value) == QtySelected)
                    //            {
                    //                break;
                    //            }
                    //              //decimal decReturnQuantity = 0;
                    //            //Total Qty FROM EACH  Item and Batch;decTakenQuantity
                    //            //if (datUsedQtyBatchWise != null && datUsedQtyBatchWise.Rows.Count > 0)
                    //            //{
                    //            //    decReturnQuantity = datUsedQtyBatchWise.Compute("SUM(ReturnQuantity)", "ItemID=" + Convert.ToInt32(dgvSales["ItemID", i].Value) + " AND BatchID=" + Convert.ToInt32(drBatchLess["BatchID"]) + "").ToDecimal();
                    //            //}
                    //            //     //if AvailableQty not equal to 0

                               
                    //            //    //get remaining available qty
                    //            //drBatchLess["AvailableQty"] = (Convert.ToDecimal(drBatchLess["AvailableQty"]) - decReturnQuantity);
                    //                //clsDTOBatchlessItemIssueDetails objBatchless = new clsDTOBatchlessItemIssueDetails();

                    //                objClsDtoSalesReturnDetails = new clsDTOSalesReturnDetails();

                    //                objClsDtoSalesReturnDetails.intSalesReturnID = Convert.ToInt32(cboInvoiceNo.SelectedValue);
                    //                objClsDtoSalesReturnDetails.intReferenceSerialNo = Convert.ToInt32(dgvSales["ReferenceSerialNo", i].Value);

                    //                objClsDtoSalesReturnDetails.intReasonID = Convert.ToInt32(dgvSales["Reason", i].Value);

                    //                if (dgvSales["Discount", i].Value != DBNull.Value)
                    //                    objClsDtoSalesReturnDetails.intDiscountID = Convert.ToInt32(dgvSales["Discount", i].Value);
                    //                if (dgvSales["DiscountAmount", i].Value != DBNull.Value)
                    //                    objClsDtoSalesReturnDetails.decDiscountAmount = Convert.ToDecimal(dgvSales["DiscountAmount", i].Value);
                    //                objClsDtoSalesReturnDetails.decNetAmount = Convert.ToDecimal(dgvSales["NetAmount", i].Value);
                    //                objClsDtoSalesReturnDetails.decGrandAmount = dgvSales["GrandAmount", i].Value.ToDecimal();
                    //                objClsDtoSalesReturnDetails.decReturnAmount = Convert.ToDecimal(dgvSales["ReturnAmount", i].Value);

                    //                objClsDtoSalesReturnDetails.decRate = Convert.ToDecimal(dgvSales["Rate", i].Value);


                    //                objClsDtoSalesReturnDetails.intWarehouseID = Convert.ToInt32(drBatchLess["WarehouseID"]);
                    //                objClsDtoSalesReturnDetails.intItemID = Convert.ToInt32(drBatchLess["ItemID"]);

                    //                objClsDtoSalesReturnDetails.intUOMID = Convert.ToInt32(dgvSales["Uom", i].Tag);




                    //                objClsDtoSalesReturnDetails.intBatchID = Convert.ToInt32(drBatchLess["BatchID"]);
                    //                //objBatchless.intBatchless = iBatchless;

                    //                AvailbleQtyinBatch = Convert.ToDecimal(drBatchLess["Quantity"]);


                    //                if (AvailbleQtyinBatch < TotalQtyBatchless)
                    //                {

                    //                    QtySelected = QtySelected + AvailbleQtyinBatch;
                    //                    TotalQtyBatchless = TotalQtyBatchless - AvailbleQtyinBatch;
                    //                    objClsDtoSalesReturnDetails.decReturnQuantity = AvailbleQtyinBatch;
                    //                }
                    //                else
                    //                {
                    //                    QtySelected = QtySelected + TotalQtyBatchless;

                    //                    objClsDtoSalesReturnDetails.decReturnQuantity = TotalQtyBatchless;
                    //                }


                    //                MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.lstSalesReturnDetails.Add(objClsDtoSalesReturnDetails);
                    //                ////Add selected itemid,batchid and quantity from each batch to the new datatable
                    //                //datUsedQtyBatchWise.Rows.Add();
                    //                //datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["ItemID"] = Convert.ToInt32(dgvSales["ItemID", i].Value);
                    //                //datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["BatchID"] = Convert.ToInt32(drBatchLess["BatchID"]);
                    //                //datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["ReturnQuantity"] = objClsDtoSalesReturnDetails.decReturnQuantity;
                    //                //datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["UOMID"] = Convert.ToInt32(dgvSales["Uom", i].Tag); 
                    //            }
                            
                    //}
                    else if ((Convert.ToInt32(dgvSales["ItemID", i].Value) != null) && (Convert.ToInt64(dgvSales["BatchID", i].Value) == 0) && (Convert.ToBoolean(dgvSales["IsGroup", i].Value) == true))
                    {
                        objClsDtoSalesReturnDetails = new clsDTOSalesReturnDetails();
                        foreach (DataRow dr in datWarehouse.Rows)
                        {
                            if (Convert.ToBoolean(dgvSales["IsGroup", i].Value) == true)
                            {
                                if (dr["ItemID"].ToInt32() == Convert.ToInt32(dgvSales["ItemID", i].Value))
                                    objClsDtoSalesReturnDetails.intWarehouseID = Convert.ToInt32(dr["WarehouseID"]);
                            }
                            else
                            {
                                if (dr["ItemID"].ToInt32() == Convert.ToInt32(dgvSales["ItemID", i].Value) && dr["BatchID"].ToInt32() == Convert.ToInt32(dgvSales["BatchID", i].Value))
                                    objClsDtoSalesReturnDetails.intWarehouseID = Convert.ToInt32(dr["WarehouseID"]);
                            }
                        }
                        objClsDtoSalesReturnDetails.intSalesReturnID = Convert.ToInt32(cboInvoiceNo.SelectedValue);
                        objClsDtoSalesReturnDetails.intReferenceSerialNo = Convert.ToInt32(dgvSales["ReferenceSerialNo", i].Value);
                        objClsDtoSalesReturnDetails.intBatchID = Convert.ToInt64(dgvSales["BatchID", i].Value);
                        objClsDtoSalesReturnDetails.decReturnQuantity = Convert.ToDecimal(dgvSales["ReturnQuantity", i].Value);
                        objClsDtoSalesReturnDetails.intReasonID = Convert.ToInt32(dgvSales["Reason", i].Value);
                        objClsDtoSalesReturnDetails.intUOMID = Convert.ToInt32(dgvSales["Uom", i].Tag);
                        if (dgvSales["Discount", i].Value != DBNull.Value)
                            objClsDtoSalesReturnDetails.intDiscountID = Convert.ToInt32(dgvSales["Discount", i].Value);
                        if (dgvSales["DiscountAmount", i].Value != DBNull.Value)
                            objClsDtoSalesReturnDetails.decDiscountAmount = Convert.ToDecimal(dgvSales["DiscountAmount", i].Value);
                        objClsDtoSalesReturnDetails.decNetAmount = Convert.ToDecimal(dgvSales["NetAmount", i].Value);
                        objClsDtoSalesReturnDetails.decGrandAmount = dgvSales["GrandAmount", i].Value.ToDecimal();
                        objClsDtoSalesReturnDetails.decReturnAmount = Convert.ToDecimal(dgvSales["ReturnAmount", i].Value);

                        objClsDtoSalesReturnDetails.decRate = Convert.ToDecimal(dgvSales["Rate", i].Value);
                        objClsDtoSalesReturnDetails.intItemID = Convert.ToInt32(dgvSales["ItemID", i].Value);
                        objClsDtoSalesReturnDetails.blnIsGroup = Convert.ToBoolean(dgvSales["IsGroup", i].Value);
                        //objClsDtoSalesReturnDetails.intWarehouseID = Convert.ToInt32(dgvSales["WarehouseID", i].Value);

                        if (objClsDtoSalesReturnDetails.blnIsGroup)
                        {
                            dtGroupDetailsR = MobjclsBLLSalesReturn.GetGroupDetails(objClsDtoSalesReturnDetails.intItemID);
                            //MDtItemGroupIssueDetails.DefaultView.RowFilter = "ItemGroupID = " + objDtoItemIssueDetails.intItemID + " And ReferenceNo = " + MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intReferenceID + " And ReferenceSerialNo = " + objDtoItemIssueDetails.intReferenceSerialNo;
                            objClsDtoSalesReturnDetails.lstItemGroupReturnDetails = new List<clsDTOItemGroupReturnDetails>();
                            foreach (DataRow drRow in dtGroupDetailsR.Rows)
                           {
                                clsDTOItemGroupReturnDetails objItemReturnDetails = new clsDTOItemGroupReturnDetails();
                                objItemReturnDetails.intItemID = Convert.ToInt32(drRow["ItemID"]);
                                //GET THE ISSUED QUANTITY FROM GROUPISSUE TABLE
                                //BatchlessItems = MobjclsBLLSalesReturn.GetGroupIssueDetails(objItemReturnDetails.intItemID, objClsDtoSalesReturnDetails.intItemID);
                                if (MBlnEditMode == false)
                                {
                                    BatchlessItems = MobjclsBLLSalesReturn.GetGroupIssueDetails(objItemReturnDetails.intItemID, objClsDtoSalesReturnDetails.intItemID);
                                }
                                else
                                {
                                    BatchlessItems = MobjclsBLLSalesReturn.GetGroupIssueDetailsEditMode(objItemReturnDetails.intItemID, objClsDtoSalesReturnDetails.intItemID);
                                }
                                TotalQtyGrp = ((Convert.ToDecimal(drRow["Quantity"])) * (Convert.ToDecimal(dgvSales["ReturnQuantity", i].Value)));
                                decimal QtySelectedGrp = 0;
                               foreach (DataRow drBatchLess in BatchlessItems.Rows)
                              {
                                    if (((Convert.ToDecimal(drRow["Quantity"])) * (Convert.ToDecimal(dgvSales["ReturnQuantity", i].Value))) == QtySelectedGrp)
                                    {
                                        break;
                                    }
                                    //decimal decReturnQuantityG = 0;
                                    //if (datUsedQtyBatchWise != null && datUsedQtyBatchWise.Rows.Count > 0)
                                    //{
                                        //Total Qty FROM EACH  Item and Batch;decTakenQuantity
                                        //decReturnQuantityG = datUsedQtyBatchWise.Compute("SUM(ReturnQuantity)", "ItemID=" + Convert.ToInt32(drBatchLess["ItemID"]) + " AND BatchID=" + Convert.ToInt32(drBatchLess["BatchID"]) + "").ToDecimal();

                                    //}
                                    //decTakenQuantityG=datUsedQtyBatchWise.Compute("SUM(TakenQuantity)", "ItemID=" + Convert.ToInt32(dr.Cells["ItemID"].Value) + " AND BatchID=" + Convert.ToInt32(drBatchLess["BatchID"]) + "").ToDecimal();

                                    //drBatchLess["AvailableQty"] = (Convert.ToDecimal(drBatchLess["AvailableQty"]) - decReturnQuantityG);
                                        objItemReturnDetails = new clsDTOItemGroupReturnDetails();
                                        clsDTOBatchlessItemIssueDetails objBatchless = new clsDTOBatchlessItemIssueDetails();
                                        objItemReturnDetails.intItemID = Convert.ToInt32(drBatchLess["ItemID"]);
                                        objItemReturnDetails.intBatchID = Convert.ToInt32(drBatchLess["BatchID"]);

                                        objItemReturnDetails.intUOMID = Convert.ToInt32(drBatchLess["UOMID"]);





                                        AvailbleQtyinBatch = Convert.ToDecimal(drBatchLess["Quantity"]);


                                        if (AvailbleQtyinBatch < TotalQtyGrp)
                                        {

                                            QtySelectedGrp = QtySelectedGrp + AvailbleQtyinBatch;
                                            TotalQtyGrp = TotalQtyGrp - AvailbleQtyinBatch;
                                            objItemReturnDetails.decQuantity = AvailbleQtyinBatch;
                                        }
                                        else
                                        {
                                            QtySelectedGrp = QtySelectedGrp + TotalQtyGrp;

                                            objItemReturnDetails.decQuantity = TotalQtyGrp;
                                        }

                                        objClsDtoSalesReturnDetails.lstItemGroupReturnDetails.Add(objItemReturnDetails);
                                         ////Add selected itemid,batchid and quantity from each batch to the new datatable
                                         //   datUsedQtyBatchWise.Rows.Add();
                                         //   datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["ItemID"] = Convert.ToInt32(drBatchLess["ItemID"]);
                                         //   datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["BatchID"] = Convert.ToInt32(drBatchLess["BatchID"]);
                                         //   datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["ReturnQuantity"] = objItemReturnDetails.decQuantity;
                                         //   datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["UOMID"] = Convert.ToInt32(drBatchLess["UOMID"]);
                                    }
                            

                            }
                        
                        }
                        MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.lstSalesReturnDetails.Add(objClsDtoSalesReturnDetails);
                    }
                    //MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.lstSalesReturnDetails.Add(objClsDtoSalesReturnDetails);
                }
            }
        }

        private void dgvSalesReturnDisplay_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvSalesReturnDisplay.Rows.Count > 0)
            {
                if (Convert.ToInt32(dgvSalesReturnDisplay.CurrentRow.Cells[0].Value) != 0)
                {
                    ClearControls();
                    DisplaySalesReturnMasterDetails(Convert.ToInt32(dgvSalesReturnDisplay.CurrentRow.Cells[0].Value));
                    MblnIsFromCancellation = false;
                }
            }
            BindingNavigatorClearItem.Enabled = false;
        }


        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void BindingNavigatorCancelItem_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64(txtReturnNo.Tag) > 0)
            {
                int intOldStatusID = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intStatusID;
                if (BindingNavigatorCancelItem.Text.ToUpper() == "CANCEL")
                {
                    MblnIsFromCancellation = true;
                    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2044, out MmessageIcon);
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                        {
                            objFrmCancellationDetails.ShowDialog();
                            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.strCancellationDate = objFrmCancellationDetails.PDtCancellationDate.ToString("dd-MMM-yyyy");
                            txtDescription.Text = objFrmCancellationDetails.PStrDescription;
                            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intCancelledBy = ClsCommonSettings.UserID;

                            if (!objFrmCancellationDetails.PBlnIsCanelled)
                            {
                                MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.blnIsCancelled = true;
                                MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intStatusID = (int)OperationStatusType.SSalesReturnCancelled;
                                blnCanShow = false;
                                //string strInValidItem = MobjclsBLLSalesReturn.StockValidation(true);
                                //if (!string.IsNullOrEmpty(strInValidItem))
                                //{
                                //    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2294, out MmessageIcon).Replace("#", "").Replace("*", strInValidItem);
                                //    MessageBox.Show(MsMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                //    return;
                                //}
                                string strInValidItem = MobjclsBLLSalesReturn.StockValidation(true);
                                if (!string.IsNullOrEmpty(strInValidItem))
                                {
                                    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2294, out MmessageIcon).Replace("#", "").Replace("*", strInValidItem);
                                    MessageBox.Show(MsMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    return;
                                }
                                if (SaveSalesReturn())
                                {

                                    AddNew();
                                    //BindingNavigatorCancelItem.Text = "Re Open";
                                    //BindingNavigatorSaveItem.Enabled = false;
                                    //BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
                                    //lblStatus.Text = "Cancelled";
                                }
                                else
                                    MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intStatusID = intOldStatusID;
                                blnCanShow = true;
                            }
                            else
                            {
                                txtDescription.Text = "";
                                MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.blnIsCancelled = false;
                            }
                        }
                    }
                }
                else
                {
                    MblnIsFromCancellation = true;
                    MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.blnIsCancelled = false;
                    intOldStatusID = MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intStatusID;

                    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2045, out MmessageIcon);
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intStatusID = (int)OperationStatusType.SSalesReturnOpen;
                        blnCanShow = false;
                        if (SaveSalesReturn())
                            AddNew();
                        else
                            MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intStatusID = intOldStatusID;

                        blnCanShow = true;
                    }
                }
            }
            MblnIsFromCancellation = false;
        }



        private void BtnSRefresh_Click(object sender, EventArgs e)
        {
            DisplaySalesNumbersInSearchGrid();
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCombos(6);
            LoadCombos(9);

            if (MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intSalesReturnID == 0)
                GenerateSalesReturnNo();
            cboInvoiceNo.DataSource = null;

            if (cboCompany.SelectedValue != null)
            {
                int intCompanyAcc = cboCompany.SelectedValue.ToInt32();
                cboAccount.SelectedValue = MobjclsBLLSalesReturn.GetCompanyAccountID(Convert.ToInt32(TransactionTypes.SaleReturn), intCompanyAcc);
            }
            //lblCompanyCurrencyAmount.Text = "Amount in " + MobjclsBLLPurchaseReturn.GetCompanyCurrency(Convert.ToInt32(cboCompany.SelectedValue), out MintBaseCurrencyScale);
        }

        private void cboCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboInvoiceNo.DataSource = null;
            if (cboCustomer.SelectedValue != null)
            {
                DataTable datInvoices = MobjclsBLLSalesReturn.FillCombos(new string[] { "SalesInvoiceID,SalesInvoiceNo", "InvSalesInvoiceMaster", "StatusID IN( " + (Int32)OperationStatusType.SDelivered + ","+(Int32)OperationStatusType.SPartiallyDelivered +") And VendorID = " + Convert.ToInt32(cboCustomer.SelectedValue) + " And CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) });
                cboInvoiceNo.ValueMember = "SalesInvoiceID";
                cboInvoiceNo.DisplayMember = "SalesInvoiceNo";
                cboInvoiceNo.DataSource = datInvoices;
                cboInvoiceNo.SelectedIndex = -1;
                string strVendorAddress = "";
                txtVendorAddress.Text = MobjclsBLLSalesReturn.GetVendorAddress(Convert.ToInt32(cboCustomer.SelectedValue), true, out strVendorAddress);
                lblVendorAddress.Text = strVendorAddress;
            }
            else
            {
                txtVendorAddress.Text = "";
                lblVendorAddress.Text = "Address";
            }
        }

        private void cboInvoiceNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvSales.Rows.Clear();
            if (cboInvoiceNo.SelectedValue != null)
            {
                if (!MblnIsFromClear && MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intSalesReturnID == 0)
                {
                    if (MobjclsBLLSalesReturn.FillCombos(new string[] { "SalesInvoiceID", "InvSalesInvoiceMaster", "SalesInvoiceID = " + Convert.ToInt32(cboInvoiceNo.SelectedValue) + " And StatusID IN( " + (Int32)OperationStatusType.SDelivered + ","+(Int32)OperationStatusType.SPartiallyDelivered +")" }).Rows.Count == 0)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2047, out MmessageIcon);
                        MsMessageCommon = MsMessageCommon.Replace("*", "SalesInvoice");
                        ErrSalesReturn.SetError(cboInvoiceNo, MsMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MsMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblSalesReturnStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        TmrSalesReturn.Enabled = true;
                        cboInvoiceNo.Focus();
                        return;
                    }
                }
                DataTable datInvoiceDetails = MobjclsBLLSalesReturn.FillCombos(new string[] { "*", "InvSalesInvoiceMaster", "SalesInvoiceID = " + Convert.ToInt32(cboInvoiceNo.SelectedValue) });
                
                if (datInvoiceDetails.Rows[0]["IsDiscountPercentage"] != DBNull.Value)
                {
                    if (datInvoiceDetails.Rows[0]["IsDiscountPercentage"].ToBoolean())
                        cboDiscount.SelectedIndex = 0; // Percentage
                    else
                        cboDiscount.SelectedIndex = 1; // Amount

                    txtDiscount.Text = (datInvoiceDetails.Rows[0]["GrandDiscountAmount"].ToDecimal()).ToString("F" + MintExchangeCurrencyScale);
                }
                else
                {
                    cboDiscount.SelectedIndex = -1; // None
                    txtDiscount.Text = 0.ToString("F" + MintExchangeCurrencyScale);
                }

                if (datInvoiceDetails.Rows[0]["ExpenseAmount"] != DBNull.Value)
                    txtExpense.Text = datInvoiceDetails.Rows[0]["ExpenseAmount"].ToStringCustom();
                else
                    txtExpense.Text = Convert.ToDecimal(0).ToString();
                LoadCombos(10);
                cboTaxScheme.SelectedValue = datInvoiceDetails.Rows[0]["TaxSchemeID"].ToInt32();
                cboCurrency.SelectedValue = Convert.ToInt32(datInvoiceDetails.Rows[0]["CurrencyID"]);
                string strVendorAddress = "";
                txtVendorAddress.Text = MobjclsBLLSalesReturn.GetVendorAddress(Convert.ToInt32(datInvoiceDetails.Rows[0]["VendorAddID"]), false, out strVendorAddress);
                lblVendorAddress.Text = strVendorAddress;
            }
            else
            {
                txtVendorAddress.Text = "";
                lblVendorAddress.Text = "Address";
                cboDiscount.SelectedIndex = -1;
            }
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 4268, out MmessageIcon);

                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;

               if (MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intStatusID != (int)OperationStatusType.SSalesReturnCancelled)
                {
                    string strInValidItem = MobjclsBLLSalesReturn.StockValidation(true);
                    if (!string.IsNullOrEmpty(strInValidItem))
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2075, out MmessageIcon).Replace("#", "").Replace("*", strInValidItem);
                        MessageBox.Show(MsMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                    
                }
                if (MobjclsBLLSalesReturn.DeleteSalesReturnMaster())
                {
                    AddNew();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorDeleteItem_Click() " + ex.Message);
                mObjLogs.WriteLog("Error in BindingNavigatorDeleteItem_Click() " + ex.Message, 2);
            }
        }

        private void dgvSales_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 || e.ColumnIndex == 1)
            {
                dgvSales.PiColumnIndex = e.ColumnIndex;
            }
        }

        private void dgvSales_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvSales["ItemID", e.RowIndex].Value != null)
                {
                    if (dgvSales.Columns[e.ColumnIndex].Name == "Uom")
                    {
                        if (dgvSales.CurrentRow.Cells["Uom"].Value != null)
                        {
                            dgvSales.CurrentRow.Cells["Uom"].Tag = dgvSales.CurrentRow.Cells["Uom"].Value;
                            dgvSales.CurrentRow.Cells["Uom"].Value = dgvSales.CurrentRow.Cells["Uom"].FormattedValue;

                            int intUomScale = 0;
                            if (dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                                intUomScale = MobjclsBLLSalesReturn.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                            dgvSales.CurrentRow.Cells["ReturnQuantity"].Value = dgvSales.CurrentRow.Cells["ReturnQuantity"].Value.ToDecimal().ToString("F" + intUomScale);


                            int intUom = 0, intInvUom = 0;
                            decimal decRate = 0, decInvRate = 0;
                            decInvRate = Convert.ToDecimal(dgvSales["InvRate", e.RowIndex].Value);
                            intInvUom = Convert.ToInt32(dgvSales["InvUomID", e.RowIndex].Value);
                            intUom = Convert.ToInt32(dgvSales["Uom", e.RowIndex].Tag);

                            DataTable dtUomDetails = MobjclsBLLSalesReturn.GetUomConversionValues(intInvUom, Convert.ToInt32(dgvSales["ItemID", e.RowIndex].Value));
                            if (dtUomDetails.Rows.Count > 0)
                            {
                                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                                if (intConversionFactor == 1)
                                    decInvRate = decInvRate * decConversionValue;
                                else if (intConversionFactor == 2)
                                    decInvRate = decInvRate / decConversionValue;
                            }

                            dtUomDetails = MobjclsBLLSalesReturn.GetUomConversionValues(intUom, Convert.ToInt32(dgvSales["ItemID", e.RowIndex].Value));
                            decRate = decInvRate;
                            if (dtUomDetails.Rows.Count > 0)
                            {
                                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                                if (intConversionFactor == 1)
                                    decRate = decInvRate / decConversionValue;
                                else if (intConversionFactor == 2)
                                    decRate = decInvRate * decConversionValue;
                            }
                            dgvSales["Rate", e.RowIndex].Value = decRate;
                        }
                    }
                    else if (e.ColumnIndex == BatchNo.Index)
                    {
                        //dgvSales.CurrentRow.Cells["BatchNo"].Tag = dgvSales.CurrentRow.Cells["BatchNo"].Value;
                        //dgvSales.CurrentRow.Cells["BatchNo"].Value = dgvSales.CurrentRow.Cells["BatchNo"].FormattedValue;
                        //dgvSales.CurrentRow.Cells["BatchID"].Value = dgvSales.CurrentRow.Cells["BatchNo"].Tag;

                        DataTable datBatchWiseIssueDetails = MobjclsBLLSalesReturn.GetBatchWiseIssueDetails(cboInvoiceNo.SelectedValue.ToInt64(), dgvSales[ItemID.Index, e.RowIndex].Value.ToInt32(), dgvSales[BatchID.Index, e.RowIndex].Value.ToInt64(), dgvSales[IsGroup.Index, e.RowIndex].Value.ToBoolean());
                        dgvSales.CurrentRow.Cells["InvoicedQuantity"].Value = datBatchWiseIssueDetails.Rows[0]["InvoicedQty"];
                        dgvSales.CurrentRow.Cells["InvQty"].Value = datBatchWiseIssueDetails.Rows[0]["InvQty"];
                        dgvSales.CurrentRow.Cells["IssuedUOMID"].Value = datBatchWiseIssueDetails.Rows[0]["InvUOM"];
                        dgvSales.CurrentRow.Cells["WarehouseID"].Value = datBatchWiseIssueDetails.Rows[0]["WarehouseID"];

                        FillUOMColumn(dgvSales.CurrentRow.Cells[ItemID.Index].Value.ToInt32(), dgvSales.CurrentRow.Cells[BatchID.Index].Value.ToBoolean());
                        dgvSales.CurrentRow.Cells["Uom"].Tag = datBatchWiseIssueDetails.Rows[0]["InvUOM"].ToInt32();
                        dgvSales.CurrentRow.Cells["Uom"].Value = datBatchWiseIssueDetails.Rows[0]["InvUOM"].ToInt32();
                        dgvSales.CurrentRow.Cells["Uom"].Value = dgvSales.CurrentRow.Cells["Uom"].FormattedValue;

                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvSales_CellEndEdit() " + ex.Message);
                mObjLogs.WriteLog("Error in dgvSales_CellEndEdit() " + ex.Message, 2);
            }
        }

        private void dgvSales_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                int iRowIndex = 0;

                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    dgvSales.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    iRowIndex = dgvSales.CurrentCell.RowIndex;

                    if (e.ColumnIndex != ItemCode.Index && e.ColumnIndex != ItemName.Index)
                    {
                        if (Convert.ToString(dgvSales.CurrentRow.Cells[0].Value).Trim() == "")
                        {
                            e.Cancel = true;
                        }
                        else if (dgvSales.Columns[e.ColumnIndex].Name == "Uom")
                        {
                            FillUOMColumn(Convert.ToInt32(dgvSales["ItemID", e.RowIndex].Value), Convert.ToBoolean(dgvSales["IsGroup", e.RowIndex].Value));
                        }
                        else if (e.ColumnIndex == BatchNo.Index)
                        {
                            //FillBatchNoColumn(e.RowIndex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvSales_CellBeginEdit() " + ex.Message);
                mObjLogs.WriteLog("Error in dgvSales_CellBeginEdit() " + ex.Message, 2);
            }
        }

        private void dgvSales_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvSales.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
            dgvSales.CellLeave += new DataGridViewCellEventHandler(dgvSales_CellLeave);
        }

        void dgvSales_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvSales.CurrentCell.OwningColumn.Name == "ReturnQuantity" || dgvSales.CurrentCell.OwningColumn.Name == "ReturnAmount")
            {
                decimal decValue = 0;
                try
                {
                    decValue = Convert.ToDecimal(dgvSales[e.ColumnIndex, e.RowIndex].Value);
                }
                catch
                {
                    dgvSales[e.ColumnIndex, e.RowIndex].Value = 0;
                }
            }
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvSales.CurrentCell.OwningColumn.Name == "ReturnQuantity")
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
                int intUomScale = 0;
                if (dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    intUomScale = MobjclsBLLSalesReturn.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    if (intUomScale == 0)
                    {
                        if (e.KeyChar == '.')
                            e.Handled = true;
                    }
                    else
                    {
                        int dotIndex = -1;
                        if (txt.Text.Contains("."))
                        {
                            dotIndex = txt.Text.IndexOf('.');
                        }
                        if (e.KeyChar == '.')
                        {
                            if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            {
                                e.Handled = true;
                            }
                        }
                        else
                        {
                            if (char.IsDigit(e.KeyChar))
                            {
                                if (dotIndex != -1 && txt.SelectionStart > dotIndex)
                                {
                                    string[] splitText = txt.Text.Split('.');
                                    if (splitText.Length == 2)
                                    {
                                        if (splitText[1].Length - txt.SelectedText.Length >= intUomScale)
                                            e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
                //int intDecimalPart = 0;
                //if (dgvSales.EditingControl != null && !string.IsNullOrEmpty(dgvSales.EditingControl.Text) && dgvSales.EditingControl.Text.Contains("."))
                //    intDecimalPart = dgvSales.EditingControl.Text.Split('.')[1].Length;
                //int intUomScale = 0;
                //if (dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                //    intUomScale = MobjclsBLLSalesReturn.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                //if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                //{
                //    e.Handled = true;
                //}
                //if (e.KeyChar.ToString() == "." && intUomScale == 0)
                //{
                //    e.Handled = true;
                //}

                //if (e.KeyChar != 08 && e.KeyChar !=13 && dgvSales.EditingControl != null && !string.IsNullOrEmpty(dgvSales.EditingControl.Text) && (dgvSales.EditingControl.Text.Contains(".") && ((e.KeyChar == 46) || intDecimalPart == intUomScale)))//checking more than one "."
                //{
                //    e.Handled = true;
                //}
            }
            else if (dgvSales.CurrentCell.OwningColumn.Name == "ReturnAmount")
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (!string.IsNullOrEmpty(dgvSales.EditingControl.Text) && dgvSales.EditingControl.Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true;
                }
            }
            else if (dgvSales.CurrentCell.OwningColumn.Name == "ItemCode" || dgvSales.CurrentCell.OwningColumn.Name == "ItemName")
            {
                if (e.KeyChar.ToString() == "'")
                {
                    e.Handled = true;
                }
            }
        }

        private void BindingNavigatorClearItem_Click(object sender, EventArgs e)
        {
            ClearControls();
        }
        //case 4:
        //             ObjEmailPopUp.MsSubject = "Sales Return Information";
        //             ObjEmailPopUp.EmailFormType = EmailFormID.SalesReturn;
        //             ObjEmailPopUp.EmailSource = MobjclsBLLSTSales.GetSalesReturnReport();
        //             break;


        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {


                if (MobjclsBLLSalesReturn.FillCombos(new string[] { "SalesReturnID", "InvSalesReturnMaster", "SalesReturnID = " + MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intSalesReturnID }).Rows.Count > 0)
                {
                    if (Convert.ToInt32(MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intSalesReturnID) > 0)
                    {
                        FrmReportviewer ObjViewer = new FrmReportviewer();
                        ObjViewer.PsFormName = this.Text;
                        ObjViewer.PiRecId = Convert.ToInt32(MobjclsBLLSalesReturn.PobjClsDTOSalesReturnMaster.intSalesReturnID);
                        ObjViewer.PiFormID = (int)FormID.SalesReturn;
                        ObjViewer.ShowDialog();
                    }
                }
                else
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 2110, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesReturnStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);

                    dgvSales.Focus(); 
                }
               
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on form FrmSalesReturn:LoadReport " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnPrint_Click " + Ex.Message.ToString());
            }

        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Sales Return Information";
                    ObjEmailPopUp.EmailFormType = EmailFormID.SalesReturn;
                    ObjEmailPopUp.EmailSource = MobjclsBLLSalesReturn.GetSalesReturnReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on Email:BtnEmail_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnEmail_Click " + Ex.Message.ToString());
            }

        }

        private void CboSCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable datCombos = null; //bool blnIsEnabled = false;
            string strSearchCondition = "";
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                strSearchCondition = "UM.RoleID Not In (1,2)";
            }
            if (!string.IsNullOrEmpty(strSearchCondition))
                strSearchCondition = strSearchCondition + " AND ";
            strSearchCondition = strSearchCondition + "UM.UserID <> 1";

            if (CboSCompany.SelectedValue == null || Convert.ToInt32(CboSCompany.SelectedValue) == -2)
            {
                datCombos = MobjclsBLLSalesReturn.FillCombos(new string[] { "UM.UserID,IsNull(EM.FirstName,UM.UserName) as UserName", "UserMaster UM Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID", strSearchCondition });
            }
            else
            {
                if (!string.IsNullOrEmpty(strSearchCondition))
                    strSearchCondition = " And " + strSearchCondition;
                datCombos = MobjclsBLLSalesReturn.FillCombos(new string[] { "UM.UserID,IsNull(EM.FirstName,UM.UserName) as UserName", "UserMaster UM Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID ", "Case IsNull(EM.EmployeeID,0) When 0 Then " + CboSCompany.SelectedValue.ToInt32() + " Else EM.CompanyID  End = " + CboSCompany.SelectedValue.ToInt32() + "" + strSearchCondition });
            }
            cboSExecutive.ValueMember = "UserID";
            cboSExecutive.DisplayMember = "UserName";
            cboSExecutive.DataSource = datCombos;
        }

        public void RefreshForm()
        {
            //cboCompany_SelectedIndexChanged(null, null);
            BtnSRefresh_Click(null, null);
        }

        private int GetCustomerAccountID(int intVendorID)
        {
            try
            {
                DataTable dtVendorDetails = MobjclsBLLSalesReturn.FillCombos(new string[] { "AccountID", "VendorInformation", "VendorID = " + intVendorID });
                int intAccountID = 0;
                if (dtVendorDetails.Rows.Count > 0)
                {
                    if (dtVendorDetails.Rows[0]["AccountID"] != DBNull.Value)
                        intAccountID = Convert.ToInt32(dtVendorDetails.Rows[0]["AccountID"]);
                }
                return intAccountID;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in GetCustomerAccountID() " + ex.Message);
                mObjLogs.WriteLog("Error in GetCustomerAccountID() " + ex.Message, 2);
                return 0;
            }
        }

        private void FrmSalesReturn_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Alt | Keys.S:
                        BindingNavigatorSaveItem_Click(sender, new EventArgs());//save
                        break;
                    case Keys.Alt | Keys.R:
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//OK
                        break;


                }
            }
            catch (Exception)
            {
            }
        }

        private void btnCustomerHistory_Click(object sender, EventArgs e)
        {
            if (cboCustomer.SelectedIndex == -1)
            {
                //Please select customer
                MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 1951, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrSalesReturn.SetError(cboCustomer, MsMessageCommon.Replace("#", "").Trim());
                lblSalesReturnStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                TmrSalesReturn.Enabled = true;
                cboCustomer.Focus();
                return;
            }
            using (FrmVendorHistory objFrmVendorHistory = new FrmVendorHistory(3, cboCustomer.SelectedValue.ToInt32()))
            {
                objFrmVendorHistory.ShowDialog();
            }

        }

        private void txtGrandReturnAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
            {
                e.Handled = true;
            }
            if (!string.IsNullOrEmpty(txtGrandReturnAmount.Text) && txtGrandReturnAmount.Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            {
                e.Handled = true;
            }
        }

        private void cboCurrency_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cboCurrency.SelectedValue != null)
            {
                DataTable datCurrency = MobjclsBLLSalesReturn.FillCombos(new string[] { "CurrencyID,Scale", "CurrencyReference", "CurrencyID = " + Convert.ToInt32(cboCurrency.SelectedValue) });
                if (datCurrency.Rows.Count > 0)
                    MintExchangeCurrencyScale = Convert.ToInt32(datCurrency.Rows[0]["Scale"]);
            }
        }

        private void expandableSplitterLeft_ExpandedChanged(object sender, ExpandedChangeEventArgs e)
        {
            if (expandableSplitterLeft.Expanded)
                Reason.AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
            else
                Reason.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            if (expandableSplitterLeft.Expanded)
            {
                if (dgvSalesReturnDisplay.Columns.Count > 0)
                    dgvSalesReturnDisplay.Columns[0].Visible = false;
            }
        }

        private void dgvSales_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvSales.IsCurrentCellDirty)
            {
                if (dgvSales.CurrentCell != null)
                    dgvSales.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
        }

        private void txtGrandReturnAmount_TextChanged(object sender, EventArgs e)
        {
            if (txtGrandReturnAmount.Text != string.Empty && txtGrandReturnAmount.Text != ".")
            lblAmountInWords.Text = MobjclsBLLCommonUtility.ConvertToWord(Convert.ToString(Convert.ToDecimal(txtGrandReturnAmount.Text)), Convert.ToInt32(cboCurrency.SelectedValue));
            Changestatus(sender, e);
        }

        //private int CheckDuplicationInGrid()
        //{
        //    int intItemID = 0;
        //    long lngBatchID = 0;
        //    int RowIndexTemp = 0;

        //    foreach (DataGridViewRow rowValue in dgvSales.Rows)
        //    {
        //        if (rowValue.Cells["ItemID"].Value != null && rowValue.Cells["BatchID"].Value != null)
        //        {
        //            intItemID = Convert.ToInt32(rowValue.Cells["ItemID"].Value);
        //            lngBatchID = rowValue.Cells["BatchID"].Value.ToInt64();
        //            RowIndexTemp = rowValue.Index;
        //            foreach (DataGridViewRow row in dgvSales.Rows)
        //            {
        //                if (RowIndexTemp != row.Index)
        //                {
        //                    if (row.Cells["ItemID"].Value != null && row.Cells["BatchID"].Value.ToInt64() != 0)
        //                    {
        //                        if ((Convert.ToInt32(row.Cells["ItemID"].Value) == intItemID) && (row.Cells["BatchID"].Value.ToInt64() == lngBatchID))
        //                            return row.Index;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return -1;
        //}

        private void FrmCreditNote_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.MdiFormClosing)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(datMessages, 4214, out MmessageIcon).Replace("#", "").Trim();
                if (MessageBox.Show(MsMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                    DialogResult.Yes)
                {
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void lnkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DataTable datAdvanceSearchedData = new DataTable();
            using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.CreditNote))
            {
                objFrmSearchForm.ShowDialog();
                if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                {
                    datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                    datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                    dgvSalesReturnDisplay.DataSource = datAdvanceSearchedData;
                    dgvSalesReturnDisplay.Columns["ID"].Visible = false;
                    dgvSalesReturnDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }

        }

        private void dgvSales_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjclsBLLCommonUtility.SetSerialNo(dgvSales, e.RowIndex, false);

        }

        private void dgvSales_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjclsBLLCommonUtility.SetSerialNo(dgvSales, e.RowIndex, false);

        }

        private void cboAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cboCompany.SelectedIndex != -1)
            //{
            //    int intCompanyAcc = cboCompany.SelectedValue.ToInt32();



            //    cboAccount.SelectedValue = MobjclsBLLSalesReturn.GetCompanyAccountID(Convert.ToInt32(TransactionTypes.SaleReturn), intCompanyAcc);




            //}
        }
    }
}