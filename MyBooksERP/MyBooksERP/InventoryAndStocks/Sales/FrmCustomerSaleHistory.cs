﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmCustomerSaleHistory : Form
    {
        public int intCompanyID;
        public int intCustomerID;
        public int intItemID;
        public int intIsGroup;

        clsBLLSTSales objclsBLLSTSales;

        public FrmCustomerSaleHistory()
        {
            InitializeComponent();
            objclsBLLSTSales = new clsBLLSTSales();
        }

        private void FrmCustomerSaleHistory_Load(object sender, EventArgs e)
        {
            this.dgvCustomerSaleHistory.DataSource = objclsBLLSTSales.GetCustomerSaleHistory(intCompanyID, intCustomerID, intItemID,intIsGroup);
            DataTable datMinMaxSaleRate = new DataTable();
            datMinMaxSaleRate = objclsBLLSTSales.GetMinMaxSaleRate(intItemID, intCompanyID,intIsGroup);
            lblMinSaleRate.Text = datMinMaxSaleRate.Rows[0]["MinRate"].ToString();
            lblMaxSaleRate.Text = datMinMaxSaleRate.Rows[0]["MaxRate"].ToString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}
