﻿namespace MyBooksERP
{
    partial class FrmPackingList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPackingList));
            this.bMenu = new DevComponents.DotNetBar.Bar();
            this.btnSave = new DevComponents.DotNetBar.ButtonItem();
            this.btnDelete = new DevComponents.DotNetBar.ButtonItem();
            this.btnPackingList = new DevComponents.DotNetBar.ButtonItem();
            this.btnCommercialInvoice = new DevComponents.DotNetBar.ButtonItem();
            this.pnlTop = new DevComponents.DotNetBar.PanelEx();
            this.txtAccountOf2 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtDrawnUnder2 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtDrawnUnder1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtAccountOf1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtNetWeight = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtGrossWeight = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtTelephone = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblTelephone = new DevComponents.DotNetBar.LabelX();
            this.txtUOM = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtInvoiceNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnNationality = new DevComponents.DotNetBar.ButtonX();
            this.lblUOM = new DevComponents.DotNetBar.LabelX();
            this.txtShippingMarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblShippingMarks = new DevComponents.DotNetBar.LabelX();
            this.txtAddress = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblAddress = new DevComponents.DotNetBar.LabelX();
            this.cboNationality = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblNationality = new DevComponents.DotNetBar.LabelX();
            this.lblManufaturer = new DevComponents.DotNetBar.LabelX();
            this.txtManufacturer = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtTradeMark = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblTradeMark = new DevComponents.DotNetBar.LabelX();
            this.txtGoodsDescription = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblDescriptionofGoods = new DevComponents.DotNetBar.LabelX();
            this.txtDrawnUnder3 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblDrawnUnder = new DevComponents.DotNetBar.LabelX();
            this.txtAccountOf3 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblAccountOf = new DevComponents.DotNetBar.LabelX();
            this.lblNetWeight = new DevComponents.DotNetBar.LabelX();
            this.lblGrossWeight = new DevComponents.DotNetBar.LabelX();
            this.txtHSCode = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblHSCode = new DevComponents.DotNetBar.LabelX();
            this.lblInvoiceNo = new DevComponents.DotNetBar.LabelX();
            this.dtpDate = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.lblDate = new DevComponents.DotNetBar.LabelX();
            this.pnlBottom = new DevComponents.DotNetBar.PanelEx();
            this.lblCurrencyValue = new DevComponents.DotNetBar.LabelX();
            this.lblCurrency = new DevComponents.DotNetBar.LabelX();
            this.lblAmountInWordsValue = new DevComponents.DotNetBar.LabelX();
            this.txtCFR = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtDeclaration = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblDeclaration = new DevComponents.DotNetBar.LabelX();
            this.txtNetTotal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblNetTotal = new DevComponents.DotNetBar.LabelX();
            this.txtGrossTotal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblGrossTotal = new DevComponents.DotNetBar.LabelX();
            this.lblLCPercentage = new DevComponents.DotNetBar.LabelX();
            this.diLCPercentage = new DevComponents.Editors.DoubleInput();
            this.txtTotalUOM = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblValue = new DevComponents.DotNetBar.LabelX();
            this.lblTotalUOM = new DevComponents.DotNetBar.LabelX();
            this.diTotalCFR = new DevComponents.Editors.DoubleInput();
            this.lblTotalCFR = new DevComponents.DotNetBar.LabelX();
            this.diFreightCharge = new DevComponents.Editors.DoubleInput();
            this.lblFreightCharge = new DevComponents.DotNetBar.LabelX();
            this.diFOBValue = new DevComponents.Editors.DoubleInput();
            this.lblFOBValue = new DevComponents.DotNetBar.LabelX();
            this.lblAmountInWords = new DevComponents.DotNetBar.LabelX();
            this.dgvDetails = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoofRolls = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Roll = new DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn();
            this.Quantity = new DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn();
            this.UOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitPrice = new DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn();
            this.EachCartoon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bStatus = new DevComponents.DotNetBar.Bar();
            this.lblStatus = new DevComponents.DotNetBar.LabelItem();
            this.tmrStatus = new System.Windows.Forms.Timer(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bMenu)).BeginInit();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDate)).BeginInit();
            this.pnlBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.diLCPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diTotalCFR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diFreightCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diFOBValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // bMenu
            // 
            this.bMenu.AntiAlias = true;
            this.bMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.bMenu.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSave,
            this.btnDelete,
            this.btnPackingList,
            this.btnCommercialInvoice});
            this.bMenu.Location = new System.Drawing.Point(0, 0);
            this.bMenu.Name = "bMenu";
            this.bMenu.Size = new System.Drawing.Size(1043, 29);
            this.bMenu.Stretch = true;
            this.bMenu.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bMenu.TabIndex = 0;
            this.bMenu.TabStop = false;
            this.bMenu.Text = "bar1";
            // 
            // btnSave
            // 
            this.btnSave.Image = global::MyBooksERP.Properties.Resources.save;
            this.btnSave.Name = "btnSave";
            this.btnSave.Text = "Save";
            this.btnSave.Tooltip = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Text = "Delete";
            this.btnDelete.Tooltip = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnPackingList
            // 
            this.btnPackingList.Image = global::MyBooksERP.Properties.Resources.ItemMovement1;
            this.btnPackingList.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnPackingList.Name = "btnPackingList";
            this.btnPackingList.Text = "Packing List";
            this.btnPackingList.Tooltip = "Packing List";
            this.btnPackingList.Click += new System.EventHandler(this.btnPackingList_Click);
            // 
            // btnCommercialInvoice
            // 
            this.btnCommercialInvoice.Image = global::MyBooksERP.Properties.Resources.Sales_invoice;
            this.btnCommercialInvoice.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnCommercialInvoice.Name = "btnCommercialInvoice";
            this.btnCommercialInvoice.Text = "Commercial Invoice";
            this.btnCommercialInvoice.Tooltip = "Commercial Invoice";
            this.btnCommercialInvoice.Click += new System.EventHandler(this.btnCommercialInvoice_Click);
            // 
            // pnlTop
            // 
            this.pnlTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlTop.Controls.Add(this.txtAccountOf2);
            this.pnlTop.Controls.Add(this.txtDrawnUnder2);
            this.pnlTop.Controls.Add(this.txtDrawnUnder1);
            this.pnlTop.Controls.Add(this.txtAccountOf1);
            this.pnlTop.Controls.Add(this.txtNetWeight);
            this.pnlTop.Controls.Add(this.txtGrossWeight);
            this.pnlTop.Controls.Add(this.txtTelephone);
            this.pnlTop.Controls.Add(this.lblTelephone);
            this.pnlTop.Controls.Add(this.txtUOM);
            this.pnlTop.Controls.Add(this.txtInvoiceNo);
            this.pnlTop.Controls.Add(this.btnNationality);
            this.pnlTop.Controls.Add(this.lblUOM);
            this.pnlTop.Controls.Add(this.txtShippingMarks);
            this.pnlTop.Controls.Add(this.lblShippingMarks);
            this.pnlTop.Controls.Add(this.txtAddress);
            this.pnlTop.Controls.Add(this.lblAddress);
            this.pnlTop.Controls.Add(this.cboNationality);
            this.pnlTop.Controls.Add(this.lblNationality);
            this.pnlTop.Controls.Add(this.lblManufaturer);
            this.pnlTop.Controls.Add(this.txtManufacturer);
            this.pnlTop.Controls.Add(this.txtTradeMark);
            this.pnlTop.Controls.Add(this.lblTradeMark);
            this.pnlTop.Controls.Add(this.txtGoodsDescription);
            this.pnlTop.Controls.Add(this.lblDescriptionofGoods);
            this.pnlTop.Controls.Add(this.txtDrawnUnder3);
            this.pnlTop.Controls.Add(this.lblDrawnUnder);
            this.pnlTop.Controls.Add(this.txtAccountOf3);
            this.pnlTop.Controls.Add(this.lblAccountOf);
            this.pnlTop.Controls.Add(this.lblNetWeight);
            this.pnlTop.Controls.Add(this.lblGrossWeight);
            this.pnlTop.Controls.Add(this.txtHSCode);
            this.pnlTop.Controls.Add(this.lblHSCode);
            this.pnlTop.Controls.Add(this.lblInvoiceNo);
            this.pnlTop.Controls.Add(this.dtpDate);
            this.pnlTop.Controls.Add(this.lblDate);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 29);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(1043, 190);
            this.pnlTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlTop.Style.GradientAngle = 90;
            this.pnlTop.TabIndex = 0;
            // 
            // txtAccountOf2
            // 
            // 
            // 
            // 
            this.txtAccountOf2.Border.Class = "TextBoxBorder";
            this.txtAccountOf2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtAccountOf2.Location = new System.Drawing.Point(372, 34);
            this.txtAccountOf2.MaxLength = 500;
            this.txtAccountOf2.Multiline = true;
            this.txtAccountOf2.Name = "txtAccountOf2";
            this.txtAccountOf2.Size = new System.Drawing.Size(179, 45);
            this.txtAccountOf2.TabIndex = 7;
            // 
            // txtDrawnUnder2
            // 
            // 
            // 
            // 
            this.txtDrawnUnder2.Border.Class = "TextBoxBorder";
            this.txtDrawnUnder2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDrawnUnder2.Location = new System.Drawing.Point(372, 137);
            this.txtDrawnUnder2.MaxLength = 500;
            this.txtDrawnUnder2.Multiline = true;
            this.txtDrawnUnder2.Name = "txtDrawnUnder2";
            this.txtDrawnUnder2.Size = new System.Drawing.Size(179, 20);
            this.txtDrawnUnder2.TabIndex = 10;
            // 
            // txtDrawnUnder1
            // 
            // 
            // 
            // 
            this.txtDrawnUnder1.Border.Class = "TextBoxBorder";
            this.txtDrawnUnder1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDrawnUnder1.Location = new System.Drawing.Point(372, 111);
            this.txtDrawnUnder1.MaxLength = 100;
            this.txtDrawnUnder1.Name = "txtDrawnUnder1";
            this.txtDrawnUnder1.Size = new System.Drawing.Size(179, 20);
            this.txtDrawnUnder1.TabIndex = 9;
            // 
            // txtAccountOf1
            // 
            // 
            // 
            // 
            this.txtAccountOf1.Border.Class = "TextBoxBorder";
            this.txtAccountOf1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtAccountOf1.Location = new System.Drawing.Point(372, 8);
            this.txtAccountOf1.MaxLength = 100;
            this.txtAccountOf1.Name = "txtAccountOf1";
            this.txtAccountOf1.Size = new System.Drawing.Size(179, 20);
            this.txtAccountOf1.TabIndex = 6;
            // 
            // txtNetWeight
            // 
            // 
            // 
            // 
            this.txtNetWeight.Border.Class = "TextBoxBorder";
            this.txtNetWeight.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtNetWeight.Location = new System.Drawing.Point(96, 112);
            this.txtNetWeight.MaxLength = 50;
            this.txtNetWeight.Name = "txtNetWeight";
            this.txtNetWeight.Size = new System.Drawing.Size(179, 20);
            this.txtNetWeight.TabIndex = 4;
            // 
            // txtGrossWeight
            // 
            // 
            // 
            // 
            this.txtGrossWeight.Border.Class = "TextBoxBorder";
            this.txtGrossWeight.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGrossWeight.Location = new System.Drawing.Point(96, 86);
            this.txtGrossWeight.MaxLength = 50;
            this.txtGrossWeight.Name = "txtGrossWeight";
            this.txtGrossWeight.Size = new System.Drawing.Size(179, 20);
            this.txtGrossWeight.TabIndex = 3;
            // 
            // txtTelephone
            // 
            // 
            // 
            // 
            this.txtTelephone.Border.Class = "TextBoxBorder";
            this.txtTelephone.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTelephone.Location = new System.Drawing.Point(849, 164);
            this.txtTelephone.MaxLength = 50;
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(179, 20);
            this.txtTelephone.TabIndex = 19;
            // 
            // lblTelephone
            // 
            // 
            // 
            // 
            this.lblTelephone.BackgroundStyle.Class = "";
            this.lblTelephone.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTelephone.Location = new System.Drawing.Point(768, 163);
            this.lblTelephone.Name = "lblTelephone";
            this.lblTelephone.Size = new System.Drawing.Size(75, 23);
            this.lblTelephone.TabIndex = 34;
            this.lblTelephone.Text = "Telephone";
            // 
            // txtUOM
            // 
            // 
            // 
            // 
            this.txtUOM.Border.Class = "TextBoxBorder";
            this.txtUOM.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtUOM.Location = new System.Drawing.Point(96, 138);
            this.txtUOM.MaxLength = 50;
            this.txtUOM.Name = "txtUOM";
            this.txtUOM.Size = new System.Drawing.Size(179, 20);
            this.txtUOM.TabIndex = 5;
            this.txtUOM.TextChanged += new System.EventHandler(this.txtUOM_TextChanged);
            // 
            // txtInvoiceNo
            // 
            // 
            // 
            // 
            this.txtInvoiceNo.Border.Class = "TextBoxBorder";
            this.txtInvoiceNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtInvoiceNo.Location = new System.Drawing.Point(96, 34);
            this.txtInvoiceNo.MaxLength = 50;
            this.txtInvoiceNo.Name = "txtInvoiceNo";
            this.txtInvoiceNo.Size = new System.Drawing.Size(179, 20);
            this.txtInvoiceNo.TabIndex = 1;
            // 
            // btnNationality
            // 
            this.btnNationality.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnNationality.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnNationality.Location = new System.Drawing.Point(1005, 62);
            this.btnNationality.Name = "btnNationality";
            this.btnNationality.Size = new System.Drawing.Size(23, 20);
            this.btnNationality.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnNationality.TabIndex = 17;
            this.btnNationality.Text = "...";
            this.btnNationality.Click += new System.EventHandler(this.btnNationality_Click);
            // 
            // lblUOM
            // 
            // 
            // 
            // 
            this.lblUOM.BackgroundStyle.Class = "";
            this.lblUOM.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblUOM.Location = new System.Drawing.Point(15, 137);
            this.lblUOM.Name = "lblUOM";
            this.lblUOM.Size = new System.Drawing.Size(75, 23);
            this.lblUOM.TabIndex = 25;
            this.lblUOM.Text = "UOM";
            // 
            // txtShippingMarks
            // 
            // 
            // 
            // 
            this.txtShippingMarks.Border.Class = "TextBoxBorder";
            this.txtShippingMarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtShippingMarks.Location = new System.Drawing.Point(571, 134);
            this.txtShippingMarks.MaxLength = 2000;
            this.txtShippingMarks.Multiline = true;
            this.txtShippingMarks.Name = "txtShippingMarks";
            this.txtShippingMarks.Size = new System.Drawing.Size(180, 50);
            this.txtShippingMarks.TabIndex = 13;
            // 
            // lblShippingMarks
            // 
            // 
            // 
            // 
            this.lblShippingMarks.BackgroundStyle.Class = "";
            this.lblShippingMarks.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblShippingMarks.Location = new System.Drawing.Point(572, 109);
            this.lblShippingMarks.Name = "lblShippingMarks";
            this.lblShippingMarks.Size = new System.Drawing.Size(111, 23);
            this.lblShippingMarks.TabIndex = 29;
            this.lblShippingMarks.Text = "Shipping Marks";
            // 
            // txtAddress
            // 
            // 
            // 
            // 
            this.txtAddress.Border.Class = "TextBoxBorder";
            this.txtAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtAddress.Location = new System.Drawing.Point(849, 88);
            this.txtAddress.MaxLength = 500;
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(179, 70);
            this.txtAddress.TabIndex = 18;
            // 
            // lblAddress
            // 
            // 
            // 
            // 
            this.lblAddress.BackgroundStyle.Class = "";
            this.lblAddress.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAddress.Location = new System.Drawing.Point(768, 85);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(75, 23);
            this.lblAddress.TabIndex = 33;
            this.lblAddress.Text = "Address";
            // 
            // cboNationality
            // 
            this.cboNationality.DisplayMember = "Text";
            this.cboNationality.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboNationality.DropDownHeight = 105;
            this.cboNationality.FormattingEnabled = true;
            this.cboNationality.IntegralHeight = false;
            this.cboNationality.ItemHeight = 14;
            this.cboNationality.Location = new System.Drawing.Point(849, 62);
            this.cboNationality.MaxLength = 50;
            this.cboNationality.Name = "cboNationality";
            this.cboNationality.Size = new System.Drawing.Size(150, 20);
            this.cboNationality.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboNationality.TabIndex = 16;
            this.cboNationality.TextChanged += new System.EventHandler(this.cboNationality_TextChanged);
            // 
            // lblNationality
            // 
            // 
            // 
            // 
            this.lblNationality.BackgroundStyle.Class = "";
            this.lblNationality.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblNationality.Location = new System.Drawing.Point(768, 61);
            this.lblNationality.Name = "lblNationality";
            this.lblNationality.Size = new System.Drawing.Size(75, 23);
            this.lblNationality.TabIndex = 32;
            this.lblNationality.Text = "Nationality";
            // 
            // lblManufaturer
            // 
            // 
            // 
            // 
            this.lblManufaturer.BackgroundStyle.Class = "";
            this.lblManufaturer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblManufaturer.Location = new System.Drawing.Point(768, 35);
            this.lblManufaturer.Name = "lblManufaturer";
            this.lblManufaturer.Size = new System.Drawing.Size(75, 23);
            this.lblManufaturer.TabIndex = 31;
            this.lblManufaturer.Text = "Manufaturer";
            // 
            // txtManufacturer
            // 
            // 
            // 
            // 
            this.txtManufacturer.Border.Class = "TextBoxBorder";
            this.txtManufacturer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtManufacturer.Location = new System.Drawing.Point(849, 36);
            this.txtManufacturer.MaxLength = 100;
            this.txtManufacturer.Name = "txtManufacturer";
            this.txtManufacturer.Size = new System.Drawing.Size(179, 20);
            this.txtManufacturer.TabIndex = 15;
            // 
            // txtTradeMark
            // 
            // 
            // 
            // 
            this.txtTradeMark.Border.Class = "TextBoxBorder";
            this.txtTradeMark.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTradeMark.Location = new System.Drawing.Point(849, 10);
            this.txtTradeMark.MaxLength = 50;
            this.txtTradeMark.Name = "txtTradeMark";
            this.txtTradeMark.Size = new System.Drawing.Size(179, 20);
            this.txtTradeMark.TabIndex = 14;
            // 
            // lblTradeMark
            // 
            // 
            // 
            // 
            this.lblTradeMark.BackgroundStyle.Class = "";
            this.lblTradeMark.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTradeMark.Location = new System.Drawing.Point(768, 9);
            this.lblTradeMark.Name = "lblTradeMark";
            this.lblTradeMark.Size = new System.Drawing.Size(75, 23);
            this.lblTradeMark.TabIndex = 30;
            this.lblTradeMark.Text = "Trade Mark";
            // 
            // txtGoodsDescription
            // 
            // 
            // 
            // 
            this.txtGoodsDescription.Border.Class = "TextBoxBorder";
            this.txtGoodsDescription.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGoodsDescription.Location = new System.Drawing.Point(572, 32);
            this.txtGoodsDescription.MaxLength = 2000;
            this.txtGoodsDescription.Multiline = true;
            this.txtGoodsDescription.Name = "txtGoodsDescription";
            this.txtGoodsDescription.Size = new System.Drawing.Size(180, 74);
            this.txtGoodsDescription.TabIndex = 12;
            // 
            // lblDescriptionofGoods
            // 
            // 
            // 
            // 
            this.lblDescriptionofGoods.BackgroundStyle.Class = "";
            this.lblDescriptionofGoods.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDescriptionofGoods.Location = new System.Drawing.Point(572, 7);
            this.lblDescriptionofGoods.Name = "lblDescriptionofGoods";
            this.lblDescriptionofGoods.Size = new System.Drawing.Size(111, 23);
            this.lblDescriptionofGoods.TabIndex = 28;
            this.lblDescriptionofGoods.Text = "Description of Goods";
            // 
            // txtDrawnUnder3
            // 
            // 
            // 
            // 
            this.txtDrawnUnder3.Border.Class = "TextBoxBorder";
            this.txtDrawnUnder3.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDrawnUnder3.Location = new System.Drawing.Point(372, 163);
            this.txtDrawnUnder3.MaxLength = 100;
            this.txtDrawnUnder3.Name = "txtDrawnUnder3";
            this.txtDrawnUnder3.Size = new System.Drawing.Size(179, 20);
            this.txtDrawnUnder3.TabIndex = 11;
            // 
            // lblDrawnUnder
            // 
            // 
            // 
            // 
            this.lblDrawnUnder.BackgroundStyle.Class = "";
            this.lblDrawnUnder.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDrawnUnder.Location = new System.Drawing.Point(291, 109);
            this.lblDrawnUnder.Name = "lblDrawnUnder";
            this.lblDrawnUnder.Size = new System.Drawing.Size(75, 23);
            this.lblDrawnUnder.TabIndex = 27;
            this.lblDrawnUnder.Text = "Drawn Under";
            // 
            // txtAccountOf3
            // 
            // 
            // 
            // 
            this.txtAccountOf3.Border.Class = "TextBoxBorder";
            this.txtAccountOf3.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtAccountOf3.Location = new System.Drawing.Point(372, 85);
            this.txtAccountOf3.MaxLength = 100;
            this.txtAccountOf3.Name = "txtAccountOf3";
            this.txtAccountOf3.Size = new System.Drawing.Size(179, 20);
            this.txtAccountOf3.TabIndex = 8;
            // 
            // lblAccountOf
            // 
            // 
            // 
            // 
            this.lblAccountOf.BackgroundStyle.Class = "";
            this.lblAccountOf.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAccountOf.Location = new System.Drawing.Point(291, 7);
            this.lblAccountOf.Name = "lblAccountOf";
            this.lblAccountOf.Size = new System.Drawing.Size(75, 23);
            this.lblAccountOf.TabIndex = 26;
            this.lblAccountOf.Text = "Account of";
            // 
            // lblNetWeight
            // 
            // 
            // 
            // 
            this.lblNetWeight.BackgroundStyle.Class = "";
            this.lblNetWeight.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblNetWeight.Location = new System.Drawing.Point(15, 111);
            this.lblNetWeight.Name = "lblNetWeight";
            this.lblNetWeight.Size = new System.Drawing.Size(75, 23);
            this.lblNetWeight.TabIndex = 24;
            this.lblNetWeight.Text = "Net Weight";
            // 
            // lblGrossWeight
            // 
            // 
            // 
            // 
            this.lblGrossWeight.BackgroundStyle.Class = "";
            this.lblGrossWeight.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblGrossWeight.Location = new System.Drawing.Point(15, 85);
            this.lblGrossWeight.Name = "lblGrossWeight";
            this.lblGrossWeight.Size = new System.Drawing.Size(75, 23);
            this.lblGrossWeight.TabIndex = 23;
            this.lblGrossWeight.Text = "Gross Weight";
            // 
            // txtHSCode
            // 
            // 
            // 
            // 
            this.txtHSCode.Border.Class = "TextBoxBorder";
            this.txtHSCode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtHSCode.Location = new System.Drawing.Point(96, 60);
            this.txtHSCode.MaxLength = 50;
            this.txtHSCode.Name = "txtHSCode";
            this.txtHSCode.Size = new System.Drawing.Size(179, 20);
            this.txtHSCode.TabIndex = 2;
            // 
            // lblHSCode
            // 
            // 
            // 
            // 
            this.lblHSCode.BackgroundStyle.Class = "";
            this.lblHSCode.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblHSCode.Location = new System.Drawing.Point(15, 59);
            this.lblHSCode.Name = "lblHSCode";
            this.lblHSCode.Size = new System.Drawing.Size(75, 23);
            this.lblHSCode.TabIndex = 22;
            this.lblHSCode.Text = "H.S.Code";
            // 
            // lblInvoiceNo
            // 
            // 
            // 
            // 
            this.lblInvoiceNo.BackgroundStyle.Class = "";
            this.lblInvoiceNo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblInvoiceNo.Location = new System.Drawing.Point(15, 33);
            this.lblInvoiceNo.Name = "lblInvoiceNo";
            this.lblInvoiceNo.Size = new System.Drawing.Size(75, 23);
            this.lblInvoiceNo.TabIndex = 21;
            this.lblInvoiceNo.Text = "Invoice No";
            // 
            // dtpDate
            // 
            // 
            // 
            // 
            this.dtpDate.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtpDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtpDate.ButtonDropDown.Visible = true;
            this.dtpDate.CustomFormat = "dd MMM yyyy";
            this.dtpDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.dtpDate.IsPopupCalendarOpen = false;
            this.dtpDate.Location = new System.Drawing.Point(96, 8);
            // 
            // 
            // 
            this.dtpDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.dtpDate.MonthCalendar.BackgroundStyle.Class = "";
            this.dtpDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpDate.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtpDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtpDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtpDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtpDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtpDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtpDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.dtpDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpDate.MonthCalendar.DisplayMonth = new System.DateTime(2014, 2, 1, 0, 0, 0, 0);
            this.dtpDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dtpDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtpDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtpDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.dtpDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpDate.MonthCalendar.TodayButtonVisible = true;
            this.dtpDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(106, 20);
            this.dtpDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtpDate.TabIndex = 0;
            // 
            // lblDate
            // 
            // 
            // 
            // 
            this.lblDate.BackgroundStyle.Class = "";
            this.lblDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDate.Location = new System.Drawing.Point(15, 7);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(75, 23);
            this.lblDate.TabIndex = 20;
            this.lblDate.Text = "Date";
            // 
            // pnlBottom
            // 
            this.pnlBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottom.Controls.Add(this.lblCurrencyValue);
            this.pnlBottom.Controls.Add(this.lblCurrency);
            this.pnlBottom.Controls.Add(this.lblAmountInWordsValue);
            this.pnlBottom.Controls.Add(this.txtCFR);
            this.pnlBottom.Controls.Add(this.txtDeclaration);
            this.pnlBottom.Controls.Add(this.lblDeclaration);
            this.pnlBottom.Controls.Add(this.txtNetTotal);
            this.pnlBottom.Controls.Add(this.lblNetTotal);
            this.pnlBottom.Controls.Add(this.txtGrossTotal);
            this.pnlBottom.Controls.Add(this.lblGrossTotal);
            this.pnlBottom.Controls.Add(this.lblLCPercentage);
            this.pnlBottom.Controls.Add(this.diLCPercentage);
            this.pnlBottom.Controls.Add(this.txtTotalUOM);
            this.pnlBottom.Controls.Add(this.lblValue);
            this.pnlBottom.Controls.Add(this.lblTotalUOM);
            this.pnlBottom.Controls.Add(this.diTotalCFR);
            this.pnlBottom.Controls.Add(this.lblTotalCFR);
            this.pnlBottom.Controls.Add(this.diFreightCharge);
            this.pnlBottom.Controls.Add(this.lblFreightCharge);
            this.pnlBottom.Controls.Add(this.diFOBValue);
            this.pnlBottom.Controls.Add(this.lblFOBValue);
            this.pnlBottom.Controls.Add(this.lblAmountInWords);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 324);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1043, 131);
            this.pnlBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottom.Style.GradientAngle = 90;
            this.pnlBottom.TabIndex = 2;
            // 
            // lblCurrencyValue
            // 
            // 
            // 
            // 
            this.lblCurrencyValue.BackgroundStyle.Class = "";
            this.lblCurrencyValue.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCurrencyValue.Location = new System.Drawing.Point(674, 32);
            this.lblCurrencyValue.Name = "lblCurrencyValue";
            this.lblCurrencyValue.Size = new System.Drawing.Size(88, 23);
            this.lblCurrencyValue.TabIndex = 21;
            this.lblCurrencyValue.Text = "Currency";
            // 
            // lblCurrency
            // 
            // 
            // 
            // 
            this.lblCurrency.BackgroundStyle.Class = "";
            this.lblCurrency.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCurrency.Location = new System.Drawing.Point(572, 32);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(99, 23);
            this.lblCurrency.TabIndex = 20;
            this.lblCurrency.Text = "Currency";
            // 
            // lblAmountInWordsValue
            // 
            // 
            // 
            // 
            this.lblAmountInWordsValue.BackgroundStyle.Class = "";
            this.lblAmountInWordsValue.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAmountInWordsValue.Location = new System.Drawing.Point(674, 63);
            this.lblAmountInWordsValue.Name = "lblAmountInWordsValue";
            this.lblAmountInWordsValue.Size = new System.Drawing.Size(354, 65);
            this.lblAmountInWordsValue.TabIndex = 19;
            this.lblAmountInWordsValue.Text = "Amount In Words";
            this.lblAmountInWordsValue.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblAmountInWordsValue.WordWrap = true;
            // 
            // txtCFR
            // 
            // 
            // 
            // 
            this.txtCFR.Border.Class = "TextBoxBorder";
            this.txtCFR.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCFR.Location = new System.Drawing.Point(96, 59);
            this.txtCFR.MaxLength = 50;
            this.txtCFR.Name = "txtCFR";
            this.txtCFR.Size = new System.Drawing.Size(179, 20);
            this.txtCFR.TabIndex = 6;
            // 
            // txtDeclaration
            // 
            // 
            // 
            // 
            this.txtDeclaration.Border.Class = "TextBoxBorder";
            this.txtDeclaration.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDeclaration.Location = new System.Drawing.Point(96, 85);
            this.txtDeclaration.MaxLength = 500;
            this.txtDeclaration.Multiline = true;
            this.txtDeclaration.Name = "txtDeclaration";
            this.txtDeclaration.Size = new System.Drawing.Size(449, 43);
            this.txtDeclaration.TabIndex = 8;
            // 
            // lblDeclaration
            // 
            // 
            // 
            // 
            this.lblDeclaration.BackgroundStyle.Class = "";
            this.lblDeclaration.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDeclaration.Location = new System.Drawing.Point(15, 82);
            this.lblDeclaration.Name = "lblDeclaration";
            this.lblDeclaration.Size = new System.Drawing.Size(111, 23);
            this.lblDeclaration.TabIndex = 12;
            this.lblDeclaration.Text = "Declaration";
            // 
            // txtNetTotal
            // 
            this.txtNetTotal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtNetTotal.Border.Class = "TextBoxBorder";
            this.txtNetTotal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtNetTotal.Location = new System.Drawing.Point(849, 35);
            this.txtNetTotal.Name = "txtNetTotal";
            this.txtNetTotal.ReadOnly = true;
            this.txtNetTotal.Size = new System.Drawing.Size(179, 20);
            this.txtNetTotal.TabIndex = 3;
            this.txtNetTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNetTotal.TextChanged += new System.EventHandler(this.txtNetTotal_TextChanged);
            // 
            // lblNetTotal
            // 
            // 
            // 
            // 
            this.lblNetTotal.BackgroundStyle.Class = "";
            this.lblNetTotal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblNetTotal.Location = new System.Drawing.Point(768, 34);
            this.lblNetTotal.Name = "lblNetTotal";
            this.lblNetTotal.Size = new System.Drawing.Size(54, 23);
            this.lblNetTotal.TabIndex = 17;
            this.lblNetTotal.Text = "Net Total";
            // 
            // txtGrossTotal
            // 
            this.txtGrossTotal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtGrossTotal.Border.Class = "TextBoxBorder";
            this.txtGrossTotal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGrossTotal.Location = new System.Drawing.Point(849, 9);
            this.txtGrossTotal.Name = "txtGrossTotal";
            this.txtGrossTotal.ReadOnly = true;
            this.txtGrossTotal.Size = new System.Drawing.Size(179, 20);
            this.txtGrossTotal.TabIndex = 2;
            this.txtGrossTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblGrossTotal
            // 
            // 
            // 
            // 
            this.lblGrossTotal.BackgroundStyle.Class = "";
            this.lblGrossTotal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblGrossTotal.Location = new System.Drawing.Point(768, 8);
            this.lblGrossTotal.Name = "lblGrossTotal";
            this.lblGrossTotal.Size = new System.Drawing.Size(75, 23);
            this.lblGrossTotal.TabIndex = 16;
            this.lblGrossTotal.Text = "Gross Total";
            // 
            // lblLCPercentage
            // 
            // 
            // 
            // 
            this.lblLCPercentage.BackgroundStyle.Class = "";
            this.lblLCPercentage.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblLCPercentage.Location = new System.Drawing.Point(503, 6);
            this.lblLCPercentage.Name = "lblLCPercentage";
            this.lblLCPercentage.Size = new System.Drawing.Size(186, 23);
            this.lblLCPercentage.TabIndex = 15;
            this.lblLCPercentage.Text = "percentage of L/C value pay at sight";
            // 
            // diLCPercentage
            // 
            // 
            // 
            // 
            this.diLCPercentage.BackgroundStyle.Class = "DateTimeInputBackground";
            this.diLCPercentage.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.diLCPercentage.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.diLCPercentage.Increment = 1;
            this.diLCPercentage.Location = new System.Drawing.Point(372, 7);
            this.diLCPercentage.MaxValue = 100;
            this.diLCPercentage.MinValue = 0;
            this.diLCPercentage.Name = "diLCPercentage";
            this.diLCPercentage.ShowUpDown = true;
            this.diLCPercentage.Size = new System.Drawing.Size(125, 20);
            this.diLCPercentage.TabIndex = 1;
            this.diLCPercentage.ValueChanged += new System.EventHandler(this.diLCPercentage_ValueChanged);
            // 
            // txtTotalUOM
            // 
            this.txtTotalUOM.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTotalUOM.Border.Class = "TextBoxBorder";
            this.txtTotalUOM.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTotalUOM.Location = new System.Drawing.Point(150, 7);
            this.txtTotalUOM.MaxLength = 15;
            this.txtTotalUOM.Name = "txtTotalUOM";
            this.txtTotalUOM.ReadOnly = true;
            this.txtTotalUOM.Size = new System.Drawing.Size(125, 20);
            this.txtTotalUOM.TabIndex = 0;
            // 
            // lblValue
            // 
            // 
            // 
            // 
            this.lblValue.BackgroundStyle.Class = "";
            this.lblValue.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblValue.Location = new System.Drawing.Point(291, 58);
            this.lblValue.Name = "lblValue";
            this.lblValue.Size = new System.Drawing.Size(40, 23);
            this.lblValue.TabIndex = 14;
            this.lblValue.Text = "Value";
            // 
            // lblTotalUOM
            // 
            // 
            // 
            // 
            this.lblTotalUOM.BackgroundStyle.Class = "";
            this.lblTotalUOM.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTotalUOM.Location = new System.Drawing.Point(15, 6);
            this.lblTotalUOM.Name = "lblTotalUOM";
            this.lblTotalUOM.Size = new System.Drawing.Size(129, 23);
            this.lblTotalUOM.TabIndex = 9;
            this.lblTotalUOM.Text = "Total Rolls";
            // 
            // diTotalCFR
            // 
            // 
            // 
            // 
            this.diTotalCFR.BackgroundStyle.Class = "DateTimeInputBackground";
            this.diTotalCFR.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.diTotalCFR.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.diTotalCFR.Increment = 1;
            this.diTotalCFR.Location = new System.Drawing.Point(372, 59);
            this.diTotalCFR.MaxValue = 999999999999;
            this.diTotalCFR.MinValue = 0;
            this.diTotalCFR.Name = "diTotalCFR";
            this.diTotalCFR.ShowUpDown = true;
            this.diTotalCFR.Size = new System.Drawing.Size(125, 20);
            this.diTotalCFR.TabIndex = 7;
            // 
            // lblTotalCFR
            // 
            // 
            // 
            // 
            this.lblTotalCFR.BackgroundStyle.Class = "";
            this.lblTotalCFR.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTotalCFR.Location = new System.Drawing.Point(15, 58);
            this.lblTotalCFR.Name = "lblTotalCFR";
            this.lblTotalCFR.Size = new System.Drawing.Size(61, 23);
            this.lblTotalCFR.TabIndex = 11;
            this.lblTotalCFR.Text = "Total CFR";
            // 
            // diFreightCharge
            // 
            // 
            // 
            // 
            this.diFreightCharge.BackgroundStyle.Class = "DateTimeInputBackground";
            this.diFreightCharge.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.diFreightCharge.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.diFreightCharge.Increment = 1;
            this.diFreightCharge.Location = new System.Drawing.Point(372, 33);
            this.diFreightCharge.MaxValue = 999999999999;
            this.diFreightCharge.MinValue = 0;
            this.diFreightCharge.Name = "diFreightCharge";
            this.diFreightCharge.ShowUpDown = true;
            this.diFreightCharge.Size = new System.Drawing.Size(125, 20);
            this.diFreightCharge.TabIndex = 5;
            // 
            // lblFreightCharge
            // 
            // 
            // 
            // 
            this.lblFreightCharge.BackgroundStyle.Class = "";
            this.lblFreightCharge.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFreightCharge.Location = new System.Drawing.Point(291, 32);
            this.lblFreightCharge.Name = "lblFreightCharge";
            this.lblFreightCharge.Size = new System.Drawing.Size(89, 23);
            this.lblFreightCharge.TabIndex = 13;
            this.lblFreightCharge.Text = "Freight Charges";
            // 
            // diFOBValue
            // 
            // 
            // 
            // 
            this.diFOBValue.BackgroundStyle.Class = "DateTimeInputBackground";
            this.diFOBValue.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.diFOBValue.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.diFOBValue.Increment = 1;
            this.diFOBValue.Location = new System.Drawing.Point(150, 33);
            this.diFOBValue.MaxValue = 999999999999;
            this.diFOBValue.MinValue = 0;
            this.diFOBValue.Name = "diFOBValue";
            this.diFOBValue.ShowUpDown = true;
            this.diFOBValue.Size = new System.Drawing.Size(125, 20);
            this.diFOBValue.TabIndex = 4;
            // 
            // lblFOBValue
            // 
            // 
            // 
            // 
            this.lblFOBValue.BackgroundStyle.Class = "";
            this.lblFOBValue.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFOBValue.Location = new System.Drawing.Point(15, 32);
            this.lblFOBValue.Name = "lblFOBValue";
            this.lblFOBValue.Size = new System.Drawing.Size(129, 23);
            this.lblFOBValue.TabIndex = 10;
            this.lblFOBValue.Text = "FOB Value of the Goods";
            // 
            // lblAmountInWords
            // 
            // 
            // 
            // 
            this.lblAmountInWords.BackgroundStyle.Class = "";
            this.lblAmountInWords.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAmountInWords.Location = new System.Drawing.Point(572, 58);
            this.lblAmountInWords.Name = "lblAmountInWords";
            this.lblAmountInWords.Size = new System.Drawing.Size(99, 23);
            this.lblAmountInWords.TabIndex = 18;
            this.lblAmountInWords.Text = "Amount In Words";
            // 
            // dgvDetails
            // 
            this.dgvDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Item,
            this.NoofRolls,
            this.Roll,
            this.Quantity,
            this.UOM,
            this.UnitPrice,
            this.EachCartoon,
            this.Total});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDetails.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDetails.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvDetails.Location = new System.Drawing.Point(0, 219);
            this.dgvDetails.Name = "dgvDetails";
            this.dgvDetails.Size = new System.Drawing.Size(1043, 105);
            this.dgvDetails.TabIndex = 1;
            this.dgvDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetails_CellValueChanged);
            this.dgvDetails.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvDetails_RowsAdded);
            this.dgvDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvDetails_CurrentCellDirtyStateChanged);
            this.dgvDetails.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvDetails_RowsRemoved);
            // 
            // Item
            // 
            this.Item.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Item.DataPropertyName = "Item";
            this.Item.HeaderText = "Description";
            this.Item.MaxInputLength = 500;
            this.Item.Name = "Item";
            // 
            // NoofRolls
            // 
            this.NoofRolls.DataPropertyName = "NoOfRolls";
            this.NoofRolls.HeaderText = "No. of Rolls";
            this.NoofRolls.MaxInputLength = 50;
            this.NoofRolls.Name = "NoofRolls";
            // 
            // Roll
            // 
            // 
            // 
            // 
            this.Roll.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.Roll.BackgroundStyle.Class = "DataGridViewNumericBorder";
            this.Roll.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Roll.BackgroundStyle.TextColor = System.Drawing.SystemColors.ControlText;
            this.Roll.DataPropertyName = "Roll";
            this.Roll.HeaderText = "Roll";
            this.Roll.Increment = 1;
            this.Roll.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            this.Roll.MaxInputLength = 15;
            this.Roll.Name = "Roll";
            this.Roll.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Quantity
            // 
            // 
            // 
            // 
            this.Quantity.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.Quantity.BackgroundStyle.Class = "DataGridViewNumericBorder";
            this.Quantity.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Quantity.BackgroundStyle.TextColor = System.Drawing.SystemColors.ControlText;
            this.Quantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle1;
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.Increment = 1;
            this.Quantity.MaxInputLength = 15;
            this.Quantity.Name = "Quantity";
            this.Quantity.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // UOM
            // 
            this.UOM.DataPropertyName = "UOM";
            this.UOM.HeaderText = "UOM";
            this.UOM.MaxInputLength = 50;
            this.UOM.Name = "UOM";
            // 
            // UnitPrice
            // 
            // 
            // 
            // 
            this.UnitPrice.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.UnitPrice.BackgroundStyle.Class = "DataGridViewNumericBorder";
            this.UnitPrice.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.UnitPrice.BackgroundStyle.TextColor = System.Drawing.SystemColors.ControlText;
            this.UnitPrice.DataPropertyName = "UnitPrice";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.UnitPrice.DefaultCellStyle = dataGridViewCellStyle2;
            this.UnitPrice.HeaderText = "Unit Price";
            this.UnitPrice.Increment = 1;
            this.UnitPrice.MaxInputLength = 15;
            this.UnitPrice.Name = "UnitPrice";
            this.UnitPrice.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // EachCartoon
            // 
            this.EachCartoon.DataPropertyName = "EachCartoon";
            this.EachCartoon.HeaderText = "Each Carton";
            this.EachCartoon.MaxInputLength = 2000;
            this.EachCartoon.Name = "EachCartoon";
            this.EachCartoon.Width = 200;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Total.DefaultCellStyle = dataGridViewCellStyle3;
            this.Total.HeaderText = "Total";
            this.Total.MaxInputLength = 15;
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.Width = 125;
            // 
            // bStatus
            // 
            this.bStatus.AntiAlias = true;
            this.bStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bStatus.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lblStatus});
            this.bStatus.Location = new System.Drawing.Point(0, 455);
            this.bStatus.Name = "bStatus";
            this.bStatus.Size = new System.Drawing.Size(1043, 19);
            this.bStatus.Stretch = true;
            this.bStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bStatus.TabIndex = 20;
            this.bStatus.TabStop = false;
            this.bStatus.Text = "bStatusBar";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            // 
            // tmrStatus
            // 
            this.tmrStatus.Interval = 3000;
            this.tmrStatus.Tick += new System.EventHandler(this.tmrStatus_Tick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Roll";
            this.dataGridViewTextBoxColumn1.HeaderText = "Roll";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 500;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Item";
            this.dataGridViewTextBoxColumn2.HeaderText = "Description";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 50;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Quantity";
            this.dataGridViewTextBoxColumn3.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 50;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "UOM";
            this.dataGridViewTextBoxColumn4.HeaderText = "UOM";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 2000;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 200;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "UnitPrice";
            this.dataGridViewTextBoxColumn5.HeaderText = "Unit Price";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 15;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 125;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "EachCartoon";
            this.dataGridViewTextBoxColumn6.HeaderText = "Each Carton";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 200;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Total";
            this.dataGridViewTextBoxColumn7.HeaderText = "Total";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 125;
            // 
            // FrmPackingList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 474);
            this.Controls.Add(this.dgvDetails);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.bStatus);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.bMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmPackingList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Packing List/Commercial Invoice";
            this.Load += new System.EventHandler(this.FrmPackingList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bMenu)).EndInit();
            this.pnlTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtpDate)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.diLCPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diTotalCFR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diFreightCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diFOBValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bStatus)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Bar bMenu;
        private DevComponents.DotNetBar.ButtonItem btnSave;
        private DevComponents.DotNetBar.ButtonItem btnDelete;
        private DevComponents.DotNetBar.ButtonItem btnPackingList;
        private DevComponents.DotNetBar.ButtonItem btnCommercialInvoice;
        private DevComponents.DotNetBar.PanelEx pnlTop;
        private DevComponents.DotNetBar.PanelEx pnlBottom;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvDetails;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtpDate;
        private DevComponents.DotNetBar.LabelX lblDate;
        private DevComponents.DotNetBar.LabelX lblInvoiceNo;
        private DevComponents.DotNetBar.LabelX lblAccountOf;
        private DevComponents.DotNetBar.LabelX lblNetWeight;
        private DevComponents.DotNetBar.LabelX lblGrossWeight;
        private DevComponents.DotNetBar.Controls.TextBoxX txtHSCode;
        private DevComponents.DotNetBar.LabelX lblHSCode;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGoodsDescription;
        private DevComponents.DotNetBar.LabelX lblDescriptionofGoods;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDrawnUnder3;
        private DevComponents.DotNetBar.LabelX lblDrawnUnder;
        private DevComponents.DotNetBar.Controls.TextBoxX txtAccountOf3;
        private DevComponents.DotNetBar.Controls.TextBoxX txtAddress;
        private DevComponents.DotNetBar.LabelX lblAddress;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboNationality;
        private DevComponents.DotNetBar.LabelX lblNationality;
        private DevComponents.DotNetBar.LabelX lblManufaturer;
        private DevComponents.DotNetBar.Controls.TextBoxX txtManufacturer;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTradeMark;
        private DevComponents.DotNetBar.LabelX lblTradeMark;
        private DevComponents.DotNetBar.Controls.TextBoxX txtShippingMarks;
        private DevComponents.DotNetBar.LabelX lblShippingMarks;
        private DevComponents.Editors.DoubleInput diFOBValue;
        private DevComponents.DotNetBar.LabelX lblFOBValue;
        private DevComponents.DotNetBar.LabelX lblAmountInWords;
        private DevComponents.Editors.DoubleInput diFreightCharge;
        private DevComponents.DotNetBar.LabelX lblFreightCharge;
        private DevComponents.Editors.DoubleInput diTotalCFR;
        private DevComponents.DotNetBar.LabelX lblTotalCFR;
        private DevComponents.DotNetBar.LabelX lblUOM;
        private DevComponents.DotNetBar.LabelX lblTotalUOM;
        private DevComponents.DotNetBar.LabelX lblValue;
        private DevComponents.DotNetBar.LabelX lblLCPercentage;
        private DevComponents.Editors.DoubleInput diLCPercentage;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTotalUOM;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDeclaration;
        private DevComponents.DotNetBar.LabelX lblDeclaration;
        private DevComponents.DotNetBar.Controls.TextBoxX txtNetTotal;
        private DevComponents.DotNetBar.LabelX lblNetTotal;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGrossTotal;
        private DevComponents.DotNetBar.LabelX lblGrossTotal;
        private DevComponents.DotNetBar.ButtonX btnNationality;
        private DevComponents.DotNetBar.Controls.TextBoxX txtUOM;
        private DevComponents.DotNetBar.Controls.TextBoxX txtInvoiceNo;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCFR;
        private DevComponents.DotNetBar.LabelX lblAmountInWordsValue;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTelephone;
        private DevComponents.DotNetBar.LabelX lblTelephone;
        private DevComponents.DotNetBar.Controls.TextBoxX txtNetWeight;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGrossWeight;
        private DevComponents.DotNetBar.Controls.TextBoxX txtAccountOf2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDrawnUnder2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDrawnUnder1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtAccountOf1;
        private DevComponents.DotNetBar.Bar bStatus;
        private DevComponents.DotNetBar.LabelItem lblStatus;
        private System.Windows.Forms.Timer tmrStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Item;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoofRolls;
        private DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn Roll;
        private DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOM;
        private DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn UnitPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn EachCartoon;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private DevComponents.DotNetBar.LabelX lblCurrencyValue;
        private DevComponents.DotNetBar.LabelX lblCurrency;
    }
}