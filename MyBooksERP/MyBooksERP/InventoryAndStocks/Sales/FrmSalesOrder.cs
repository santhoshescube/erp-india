using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data;
using System.Data.SqlClient;

using System.Collections.Generic;

using System.Text;
using Microsoft.VisualBasic;

namespace MyBooksERP
{
    /// <summary>
    /// Summary description for frmDocument.
    /// </summary>
    public class FrmSalesOrder : DevComponents.DotNetBar.Office2007Form
    {
        #region Designer

        #region Controls Declaration
        public Command CommandZoom;
        private DockContainerItem dockContainerItem1;
        private ExpandableSplitter expandableSplitterLeft;
        private PanelEx PanelLeft;
        private IContainer components;
        private DotNetBarManager dotNetBarManager1;
        private DockSite dockSite4;
        private DockSite dockSite1;
        private DockSite dockSite2;
        private DockSite dockSite3;
        private DockSite dockSite5;
        private DockSite dockSite6;
        private Bar bar1;
        private DockSite dockSite8;
        private PanelEx panelEx1;
        private Bar PurchaseOrderBindingNavigator;
        private ButtonItem BindingNavigatorAddNewItem;
        private ExpandableSplitter expandableSplitterTop;
        private PanelEx panelTop;
        private ExpandablePanel expandablePanel1;
        private LabelX LblSCountStatus;
        private PanelEx panelBottom;
        private PanelEx panelGridBottom;
        private PanelEx panelLeftTop;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvSalesDisplay;
        private Label lblSCompany;
        private Label lblSCustomer;
        private Label lblSStatus;
        private ClsInnerGridBar dgvSales;
        private ButtonItem BindingNavigatorSaveItem;
        private ButtonItem BindingNavigatorClearItem;
        private ButtonItem BindingNavigatorDeleteItem;
        private ButtonX btnSRefresh;
        private DevComponents.DotNetBar.Controls.TextBoxX lblSSalesNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSStatus;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSCustomer;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSCompany;
        internal Button btnTContextmenu;
        private Label LblType;
        private System.Windows.Forms.DateTimePicker dtpDueDate;
        private System.Windows.Forms.DateTimePicker dtpOrderDate;
        private Label lblVendorAddress;
        private Label lblStatus;
        private Label lblDueDate;
        private Label lblDate;
        private Label lblOrderNumber;
        private Label lblCustomer;
        internal ContextMenuStrip CMSVendorAddress;
        internal Timer TmrSales;
        internal Timer TmrFocus;
        internal ErrorProvider ErrSales;
        private ButtonItem BtnPrint;
        private ButtonItem BtnEmail;
        private ButtonX btnCustomer;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCustomer;
        public DevComponents.DotNetBar.Controls.TextBoxX txtVendorAddress;
        private DevComponents.DotNetBar.Controls.TextBoxX txtOrderNo;
        private Label lblDescription;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDescription;
        private ImageList ImgSales;
        DataGridViewComboBoxEditingControl combo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCurrency;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboOrderNumbers;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboOrderType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private Label lblOrderType;
        private Label lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboPaymentTerms;
        private DevComponents.DotNetBar.Controls.TextBoxX txtRemarks;
        private Label lblRemarks;
        private Label lblPaymentTerms;
        private ButtonItem btnActions;
        private ButtonItem btnExpense;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private DataGridViewTextBoxColumn clmItemCode;
        private DataGridViewTextBoxColumn clmItemName;
        private DataGridViewTextBoxColumn clmQuantity;
        private DataGridViewTextBoxColumn clmQtyReceived;
        private DataGridViewTextBoxColumn clmBatchNumber;
        private DataGridViewTextBoxColumn clmRate;
        private DataGridViewTextBoxColumn clmTotal;
        private DataGridViewTextBoxColumn clmStatus;
        private DataGridViewTextBoxColumn clmOrderDetailID;
        private DataGridViewTextBoxColumn clmSalesOrderID;
        private DataGridViewTextBoxColumn clmItemID;
        private ButtonItem btnApprove;
        ClsNotificationNew mObjNotification;
        private Label lblCurrency;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private Label lblStatusLabel;
        private ButtonItem btnReject;
        private ButtonItem btnDocuments;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private ButtonItem bnSubmitForApproval;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSExecutive;
        private Label lblExecutive;
        private PanelEx panelEx2;
        private Label lblTotalAmount;
        private Label lblDiscount;
        private Label lblExpenseAmount;
        private Label lblSubtotal;
        private Label lblCurrencyAmnt;
        private DevComponents.DotNetBar.Controls.TextBoxX txtExchangeCurrencyRate;
        private ButtonX btnAddExpense;
        private DevComponents.DotNetBar.Controls.TextBoxX txtExpenseAmount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDiscount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSubtotal;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDiscount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTotalAmount;
        private ButtonX btnAddressChange;
        private BackgroundWorker bckgrndWrkrSendSms;
        private Label lblAdvancePayment;
        private DevComponents.DotNetBar.Controls.TextBoxX txtAdvPayment;
        private Label lblSalesNo;
        private System.Windows.Forms.DateTimePicker dtpSTo;
        private System.Windows.Forms.DateTimePicker dtpSFrom;
        private Label lblTo;
        private Label lblFrom;
        private ButtonItem btnSuggest;
        private ButtonItem btnDeny;
        private Panel pnlBottom;
        private DevComponents.DotNetBar.Controls.WarningBox lblSalesOrderstatus;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedDateValue;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedByText;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private ButtonItem BtnHelp;
        private DevComponents.DotNetBar.TabControl tcGeneral;
        private TabControlPanel tabControlPanel3;
        private TabItem tiGeneral;
        private TabControlPanel tabControlPanel4;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvSuggestions;
        private DataGridViewTextBoxColumn UserID;
        private DataGridViewTextBoxColumn UserName;
        private DataGridViewTextBoxColumn Status;
        private DataGridViewTextBoxColumn VerifiedDate;
        private DataGridViewTextBoxColumn Comment;
        private TabItem tiSuggestions;
        private DevComponents.DotNetBar.TabControl tcSales;
        private TabControlPanel tabControlPanel1;
        private TabItem tpItemDetails;
        private Label lblAmountInWords;
        private Label lblAmountIn;
        private LinkLabel lnkLabel;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private ContextMenuStrip CntxtHistory;
        private ToolStripMenuItem ShowItemHistory;
        private ToolStripMenuItem ShowSaleRate;
        private LabelX lblAccount;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboAccount;
        private CheckedListBox cblSaleQuotationNo;
        private Label lbllpo;
        private TextBox txtLpoNo;
        private ToolStripMenuItem showGroupItemDetails;
        private DevComponents.Editors.ComboItem cboPercentage;
        private DevComponents.Editors.ComboItem cboAmount;
        private DataGridViewTextBoxColumn ItemCode;
        private DataGridViewTextBoxColumn ItemName;
        private DataGridViewTextBoxColumn BatchNo;
        private DataGridViewTextBoxColumn OrderedQty;
        private DataGridViewTextBoxColumn InvoicedQty;
        private DataGridViewTextBoxColumn Quantity;
        private DataGridViewComboBoxColumn Uom;
        private DataGridViewTextBoxColumn Rate;
        private DataGridViewTextBoxColumn GrandAmount;
        private DataGridViewComboBoxColumn Discount;
        private DataGridViewTextBoxColumn DiscountAmount;
        private DataGridViewTextBoxColumn NetAmount;
        private DataGridViewTextBoxColumn SalesOrderID;
        private DataGridViewTextBoxColumn ItemID;
        private DataGridViewTextBoxColumn BatchID;
        private DataGridViewCheckBoxColumn IsGroup;
        private DataGridViewTextBoxColumn QtyAvailable;
        private DataGridViewTextBoxColumn IsAutofilled;
        private DataGridViewTextBoxColumn IsGroupItem;
        private DataGridViewTextBoxColumn DiscAmount;
        private DataGridViewTextBoxColumn ProductTypeID;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboTaxScheme;
        private Label label1;
        private Label label2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTaxAmount;
        private ButtonItem BindingNavigatorCancelItem;

        #endregion

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSalesOrder));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            this.CommandZoom = new DevComponents.DotNetBar.Command(this.components);
            this.dockContainerItem1 = new DevComponents.DotNetBar.DockContainerItem();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvSalesDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelLeftTop = new DevComponents.DotNetBar.PanelEx();
            this.lnkLabel = new System.Windows.Forms.LinkLabel();
            this.lblSalesNo = new System.Windows.Forms.Label();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cboSExecutive = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblExecutive = new System.Windows.Forms.Label();
            this.btnSRefresh = new DevComponents.DotNetBar.ButtonX();
            this.lblSSalesNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboSStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboSCustomer = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboSCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSCompany = new System.Windows.Forms.Label();
            this.lblSCustomer = new System.Windows.Forms.Label();
            this.lblSStatus = new System.Windows.Forms.Label();
            this.LblSCountStatus = new DevComponents.DotNetBar.LabelX();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.dotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.dockSite4 = new DevComponents.DotNetBar.DockSite();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.dockSite2 = new DevComponents.DotNetBar.DockSite();
            this.dockSite8 = new DevComponents.DotNetBar.DockSite();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.dockSite6 = new DevComponents.DotNetBar.DockSite();
            this.dockSite3 = new DevComponents.DotNetBar.DockSite();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.panelBottom = new DevComponents.DotNetBar.PanelEx();
            this.tcSales = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvSales = new ClsInnerGridBar();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvoicedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Uom = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GrandAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Discount = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.DiscountAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesOrderID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsGroup = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.QtyAvailable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsAutofilled = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsGroupItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpItemDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.panelGridBottom = new DevComponents.DotNetBar.PanelEx();
            this.txtLpoNo = new System.Windows.Forms.TextBox();
            this.lbllpo = new System.Windows.Forms.Label();
            this.cboAccount = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblAmountIn = new System.Windows.Forms.Label();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.lblCreatedByText = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblCreatedDateValue = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblSalesOrderstatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTaxAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblAdvancePayment = new System.Windows.Forms.Label();
            this.txtAdvPayment = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.lblDiscount = new System.Windows.Forms.Label();
            this.lblExpenseAmount = new System.Windows.Forms.Label();
            this.lblSubtotal = new System.Windows.Forms.Label();
            this.lblCurrencyAmnt = new System.Windows.Forms.Label();
            this.txtExchangeCurrencyRate = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnAddExpense = new DevComponents.DotNetBar.ButtonX();
            this.txtExpenseAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtDiscount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtSubtotal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboDiscount = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboPercentage = new DevComponents.Editors.ComboItem();
            this.cboAmount = new DevComponents.Editors.ComboItem();
            this.txtTotalAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtRemarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.cboPaymentTerms = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblPaymentTerms = new System.Windows.Forms.Label();
            this.lblAmountInWords = new System.Windows.Forms.Label();
            this.lblAccount = new DevComponents.DotNetBar.LabelX();
            this.expandableSplitterTop = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.tcGeneral = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.cboTaxScheme = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.label1 = new System.Windows.Forms.Label();
            this.cblSaleQuotationNo = new System.Windows.Forms.CheckedListBox();
            this.btnAddressChange = new DevComponents.DotNetBar.ButtonX();
            this.lblVendorAddress = new System.Windows.Forms.Label();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.lblCompany = new System.Windows.Forms.Label();
            this.lblStatusLabel = new System.Windows.Forms.Label();
            this.cboCustomer = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnCustomer = new DevComponents.DotNetBar.ButtonX();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.txtVendorAddress = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnTContextmenu = new System.Windows.Forms.Button();
            this.cboCurrency = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtOrderNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboOrderNumbers = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.dtpDueDate = new System.Windows.Forms.DateTimePicker();
            this.cboOrderType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtDescription = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblOrderNumber = new System.Windows.Forms.Label();
            this.dtpOrderDate = new System.Windows.Forms.DateTimePicker();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblOrderType = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblDueDate = new System.Windows.Forms.Label();
            this.tiGeneral = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel4 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvSuggestions = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.UserID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VerifiedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiSuggestions = new DevComponents.DotNetBar.TabItem(this.components);
            this.PurchaseOrderBindingNavigator = new DevComponents.DotNetBar.Bar();
            this.BindingNavigatorAddNewItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorSaveItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorClearItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorDeleteItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorCancelItem = new DevComponents.DotNetBar.ButtonItem();
            this.btnActions = new DevComponents.DotNetBar.ButtonItem();
            this.btnExpense = new DevComponents.DotNetBar.ButtonItem();
            this.bnSubmitForApproval = new DevComponents.DotNetBar.ButtonItem();
            this.btnApprove = new DevComponents.DotNetBar.ButtonItem();
            this.btnReject = new DevComponents.DotNetBar.ButtonItem();
            this.btnSuggest = new DevComponents.DotNetBar.ButtonItem();
            this.btnDeny = new DevComponents.DotNetBar.ButtonItem();
            this.btnDocuments = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.BtnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.BtnHelp = new DevComponents.DotNetBar.ButtonItem();
            this.CMSVendorAddress = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TmrSales = new System.Windows.Forms.Timer(this.components);
            this.TmrFocus = new System.Windows.Forms.Timer(this.components);
            this.ErrSales = new System.Windows.Forms.ErrorProvider(this.components);
            this.ImgSales = new System.Windows.Forms.ImageList(this.components);
            this.bckgrndWrkrSendSms = new System.ComponentModel.BackgroundWorker();
            this.CntxtHistory = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ShowItemHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowSaleRate = new System.Windows.Forms.ToolStripMenuItem();
            this.showGroupItemDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmQtyReceived = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmBatchNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmOrderDetailID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmSalesOrderID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesDisplay)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.panelEx1.SuspendLayout();
            this.panelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcSales)).BeginInit();
            this.tcSales.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSales)).BeginInit();
            this.panelGridBottom.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.panelEx2.SuspendLayout();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).BeginInit();
            this.tcGeneral.SuspendLayout();
            this.tabControlPanel3.SuspendLayout();
            this.tabControlPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSuggestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrSales)).BeginInit();
            this.CntxtHistory.SuspendLayout();
            this.SuspendLayout();
            // 
            // CommandZoom
            // 
            this.CommandZoom.Name = "CommandZoom";
            // 
            // dockContainerItem1
            // 
            this.dockContainerItem1.Name = "dockContainerItem1";
            this.dockContainerItem1.Text = "dockContainerItem1";
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.dgvSalesDisplay);
            this.PanelLeft.Controls.Add(this.expandablePanel1);
            this.PanelLeft.Controls.Add(this.LblSCountStatus);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(200, 544);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 9;
            this.PanelLeft.Text = "panelEx1";
            this.PanelLeft.Visible = false;
            // 
            // dgvSalesDisplay
            // 
            this.dgvSalesDisplay.AllowUserToAddRows = false;
            this.dgvSalesDisplay.AllowUserToDeleteRows = false;
            this.dgvSalesDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSalesDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSalesDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSalesDisplay.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvSalesDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSalesDisplay.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSalesDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSalesDisplay.Location = new System.Drawing.Point(0, 226);
            this.dgvSalesDisplay.Name = "dgvSalesDisplay";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSalesDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvSalesDisplay.RowHeadersVisible = false;
            this.dgvSalesDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSalesDisplay.Size = new System.Drawing.Size(200, 292);
            this.dgvSalesDisplay.TabIndex = 109;
            this.dgvSalesDisplay.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSalesDisplay_CellDoubleClick);
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expandablePanel1.Controls.Add(this.panelLeftTop);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(200, 226);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 111;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Search";
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelLeftTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelLeftTop.Controls.Add(this.lnkLabel);
            this.panelLeftTop.Controls.Add(this.lblSalesNo);
            this.panelLeftTop.Controls.Add(this.dtpSTo);
            this.panelLeftTop.Controls.Add(this.dtpSFrom);
            this.panelLeftTop.Controls.Add(this.lblTo);
            this.panelLeftTop.Controls.Add(this.lblFrom);
            this.panelLeftTop.Controls.Add(this.cboSExecutive);
            this.panelLeftTop.Controls.Add(this.lblExecutive);
            this.panelLeftTop.Controls.Add(this.btnSRefresh);
            this.panelLeftTop.Controls.Add(this.lblSSalesNo);
            this.panelLeftTop.Controls.Add(this.cboSStatus);
            this.panelLeftTop.Controls.Add(this.cboSCustomer);
            this.panelLeftTop.Controls.Add(this.cboSCompany);
            this.panelLeftTop.Controls.Add(this.lblSCompany);
            this.panelLeftTop.Controls.Add(this.lblSCustomer);
            this.panelLeftTop.Controls.Add(this.lblSStatus);
            this.panelLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftTop.Location = new System.Drawing.Point(0, 26);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(200, 200);
            this.panelLeftTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelLeftTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelLeftTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelLeftTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelLeftTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelLeftTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelLeftTop.Style.GradientAngle = 90;
            this.panelLeftTop.TabIndex = 108;
            // 
            // lnkLabel
            // 
            this.lnkLabel.AutoSize = true;
            this.lnkLabel.Location = new System.Drawing.Point(3, 181);
            this.lnkLabel.Name = "lnkLabel";
            this.lnkLabel.Size = new System.Drawing.Size(87, 13);
            this.lnkLabel.TabIndex = 258;
            this.lnkLabel.TabStop = true;
            this.lnkLabel.Text = "Advance Search";
            this.lnkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLabel_LinkClicked);
            // 
            // lblSalesNo
            // 
            this.lblSalesNo.AutoSize = true;
            this.lblSalesNo.Location = new System.Drawing.Point(3, 151);
            this.lblSalesNo.Name = "lblSalesNo";
            this.lblSalesNo.Size = new System.Drawing.Size(70, 13);
            this.lblSalesNo.TabIndex = 251;
            this.lblSalesNo.Text = "Quotation No";
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(74, 125);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.Size = new System.Drawing.Size(122, 20);
            this.dtpSTo.TabIndex = 250;
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(74, 102);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.Size = new System.Drawing.Size(122, 20);
            this.dtpSFrom.TabIndex = 249;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(3, 128);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 248;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(3, 105);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 247;
            this.lblFrom.Text = "From";
            // 
            // cboSExecutive
            // 
            this.cboSExecutive.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSExecutive.DisplayMember = "Text";
            this.cboSExecutive.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSExecutive.DropDownHeight = 75;
            this.cboSExecutive.FormattingEnabled = true;
            this.cboSExecutive.IntegralHeight = false;
            this.cboSExecutive.ItemHeight = 14;
            this.cboSExecutive.Location = new System.Drawing.Point(74, 33);
            this.cboSExecutive.Name = "cboSExecutive";
            this.cboSExecutive.Size = new System.Drawing.Size(122, 20);
            this.cboSExecutive.TabIndex = 33;
            // 
            // lblExecutive
            // 
            this.lblExecutive.AutoSize = true;
            this.lblExecutive.Location = new System.Drawing.Point(3, 36);
            this.lblExecutive.Name = "lblExecutive";
            this.lblExecutive.Size = new System.Drawing.Size(54, 13);
            this.lblExecutive.TabIndex = 126;
            this.lblExecutive.Text = "Executive";
            // 
            // btnSRefresh
            // 
            this.btnSRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSRefresh.Location = new System.Drawing.Point(125, 171);
            this.btnSRefresh.Name = "btnSRefresh";
            this.btnSRefresh.Size = new System.Drawing.Size(71, 23);
            this.btnSRefresh.TabIndex = 36;
            this.btnSRefresh.Text = "Refresh";
            this.btnSRefresh.Click += new System.EventHandler(this.btnSRefresh_Click);
            // 
            // lblSSalesNo
            // 
            this.lblSSalesNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblSSalesNo.Border.Class = "TextBoxBorder";
            this.lblSSalesNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSSalesNo.Location = new System.Drawing.Point(74, 148);
            this.lblSSalesNo.MaxLength = 20;
            this.lblSSalesNo.Name = "lblSSalesNo";
            this.lblSSalesNo.Size = new System.Drawing.Size(122, 20);
            this.lblSSalesNo.TabIndex = 31;
            // 
            // cboSStatus
            // 
            this.cboSStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSStatus.DisplayMember = "Text";
            this.cboSStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSStatus.DropDownHeight = 75;
            this.cboSStatus.FormattingEnabled = true;
            this.cboSStatus.IntegralHeight = false;
            this.cboSStatus.ItemHeight = 14;
            this.cboSStatus.Location = new System.Drawing.Point(74, 79);
            this.cboSStatus.Name = "cboSStatus";
            this.cboSStatus.Size = new System.Drawing.Size(122, 20);
            this.cboSStatus.TabIndex = 35;
            // 
            // cboSCustomer
            // 
            this.cboSCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSCustomer.DisplayMember = "Text";
            this.cboSCustomer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSCustomer.DropDownHeight = 75;
            this.cboSCustomer.FormattingEnabled = true;
            this.cboSCustomer.IntegralHeight = false;
            this.cboSCustomer.ItemHeight = 14;
            this.cboSCustomer.Location = new System.Drawing.Point(74, 56);
            this.cboSCustomer.Name = "cboSCustomer";
            this.cboSCustomer.Size = new System.Drawing.Size(122, 20);
            this.cboSCustomer.TabIndex = 34;
            this.cboSCustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // cboSCompany
            // 
            this.cboSCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSCompany.DisplayMember = "Text";
            this.cboSCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSCompany.DropDownHeight = 75;
            this.cboSCompany.FormattingEnabled = true;
            this.cboSCompany.IntegralHeight = false;
            this.cboSCompany.ItemHeight = 14;
            this.cboSCompany.Location = new System.Drawing.Point(74, 10);
            this.cboSCompany.Name = "cboSCompany";
            this.cboSCompany.Size = new System.Drawing.Size(122, 20);
            this.cboSCompany.TabIndex = 32;
            this.cboSCompany.SelectedIndexChanged += new System.EventHandler(this.cboSCompany_SelectedIndexChanged);
            this.cboSCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblSCompany
            // 
            this.lblSCompany.AutoSize = true;
            this.lblSCompany.Location = new System.Drawing.Point(3, 13);
            this.lblSCompany.Name = "lblSCompany";
            this.lblSCompany.Size = new System.Drawing.Size(51, 13);
            this.lblSCompany.TabIndex = 109;
            this.lblSCompany.Text = "Company";
            // 
            // lblSCustomer
            // 
            this.lblSCustomer.AutoSize = true;
            this.lblSCustomer.Location = new System.Drawing.Point(3, 59);
            this.lblSCustomer.Name = "lblSCustomer";
            this.lblSCustomer.Size = new System.Drawing.Size(51, 13);
            this.lblSCustomer.TabIndex = 108;
            this.lblSCustomer.Text = "Customer";
            // 
            // lblSStatus
            // 
            this.lblSStatus.AutoSize = true;
            this.lblSStatus.Location = new System.Drawing.Point(3, 82);
            this.lblSStatus.Name = "lblSStatus";
            this.lblSStatus.Size = new System.Drawing.Size(37, 13);
            this.lblSStatus.TabIndex = 107;
            this.lblSStatus.Text = "Status";
            // 
            // LblSCountStatus
            // 
            // 
            // 
            // 
            this.LblSCountStatus.BackgroundStyle.Class = "";
            this.LblSCountStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblSCountStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LblSCountStatus.Location = new System.Drawing.Point(0, 518);
            this.LblSCountStatus.Name = "LblSCountStatus";
            this.LblSCountStatus.Size = new System.Drawing.Size(200, 26);
            this.LblSCountStatus.TabIndex = 0;
            this.LblSCountStatus.Text = "...";
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.Expanded = false;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(0, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 544);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 10;
            this.expandableSplitterLeft.TabStop = false;
            this.expandableSplitterLeft.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitterLeft_ExpandedChanged);
            // 
            // dotNetBarManager1
            // 
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.dotNetBarManager1.BottomDockSite = this.dockSite4;
            this.dotNetBarManager1.EnableFullSizeDock = false;
            this.dotNetBarManager1.LeftDockSite = this.dockSite1;
            this.dotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.dotNetBarManager1.ParentForm = this;
            this.dotNetBarManager1.RightDockSite = this.dockSite2;
            this.dotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.dotNetBarManager1.ToolbarBottomDockSite = this.dockSite8;
            this.dotNetBarManager1.ToolbarLeftDockSite = this.dockSite5;
            this.dotNetBarManager1.ToolbarRightDockSite = this.dockSite6;
            this.dotNetBarManager1.TopDockSite = this.dockSite3;
            // 
            // dockSite4
            // 
            this.dockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite4.Location = new System.Drawing.Point(0, 544);
            this.dockSite4.Name = "dockSite4";
            this.dockSite4.Size = new System.Drawing.Size(1284, 0);
            this.dockSite4.TabIndex = 18;
            this.dockSite4.TabStop = false;
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(3, 0);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 544);
            this.dockSite1.TabIndex = 15;
            this.dockSite1.TabStop = false;
            // 
            // dockSite2
            // 
            this.dockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite2.Location = new System.Drawing.Point(1284, 0);
            this.dockSite2.Name = "dockSite2";
            this.dockSite2.Size = new System.Drawing.Size(0, 544);
            this.dockSite2.TabIndex = 16;
            this.dockSite2.TabStop = false;
            // 
            // dockSite8
            // 
            this.dockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite8.Location = new System.Drawing.Point(0, 544);
            this.dockSite8.Name = "dockSite8";
            this.dockSite8.Size = new System.Drawing.Size(1284, 0);
            this.dockSite8.TabIndex = 22;
            this.dockSite8.TabStop = false;
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(0, 0);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 544);
            this.dockSite5.TabIndex = 19;
            this.dockSite5.TabStop = false;
            // 
            // dockSite6
            // 
            this.dockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite6.Location = new System.Drawing.Point(1284, 0);
            this.dockSite6.Name = "dockSite6";
            this.dockSite6.Size = new System.Drawing.Size(0, 544);
            this.dockSite6.TabIndex = 20;
            this.dockSite6.TabStop = false;
            // 
            // dockSite3
            // 
            this.dockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite3.Location = new System.Drawing.Point(0, 0);
            this.dockSite3.Name = "dockSite3";
            this.dockSite3.Size = new System.Drawing.Size(1284, 0);
            this.dockSite3.TabIndex = 17;
            this.dockSite3.TabStop = false;
            // 
            // bar1
            // 
            this.bar1.AccessibleDescription = "DotNetBar Bar (bar1)";
            this.bar1.AccessibleName = "DotNetBar Bar";
            this.bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.bar1.Location = new System.Drawing.Point(0, 0);
            this.bar1.MenuBar = true;
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(36, 24);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.bar1.TabIndex = 0;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx1.Controls.Add(this.panelBottom);
            this.panelEx1.Controls.Add(this.expandableSplitterTop);
            this.panelEx1.Controls.Add(this.panelTop);
            this.panelEx1.Controls.Add(this.PurchaseOrderBindingNavigator);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(3, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(1281, 544);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 23;
            this.panelEx1.Text = "panelEx1";
            // 
            // panelBottom
            // 
            this.panelBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelBottom.Controls.Add(this.tcSales);
            this.panelBottom.Controls.Add(this.panelGridBottom);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBottom.Location = new System.Drawing.Point(0, 155);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1281, 389);
            this.panelBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelBottom.Style.GradientAngle = 90;
            this.panelBottom.TabIndex = 1;
            this.panelBottom.Text = "panelEx3";
            // 
            // tcSales
            // 
            this.tcSales.BackColor = System.Drawing.Color.Transparent;
            this.tcSales.CanReorderTabs = true;
            this.tcSales.Controls.Add(this.tabControlPanel1);
            this.tcSales.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcSales.Location = new System.Drawing.Point(0, 0);
            this.tcSales.Name = "tcSales";
            this.tcSales.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcSales.SelectedTabIndex = 0;
            this.tcSales.Size = new System.Drawing.Size(1281, 253);
            this.tcSales.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcSales.TabIndex = 14;
            this.tcSales.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcSales.Tabs.Add(this.tpItemDetails);
            this.tcSales.TabStop = false;
            this.tcSales.Text = "tabControl1";
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.dgvSales);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1281, 231);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 15;
            this.tabControlPanel1.TabItem = this.tpItemDetails;
            // 
            // dgvSales
            // 
            this.dgvSales.AddNewRow = false;
            this.dgvSales.AlphaNumericCols = new int[0];
            this.dgvSales.BackgroundColor = System.Drawing.Color.White;
            this.dgvSales.CapsLockCols = new int[0];
            this.dgvSales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSales.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.BatchNo,
            this.OrderedQty,
            this.InvoicedQty,
            this.Quantity,
            this.Uom,
            this.Rate,
            this.GrandAmount,
            this.Discount,
            this.DiscountAmount,
            this.NetAmount,
            this.SalesOrderID,
            this.ItemID,
            this.BatchID,
            this.IsGroup,
            this.QtyAvailable,
            this.IsAutofilled,
            this.IsGroupItem,
            this.DiscAmount,
            this.ProductTypeID});
            this.dgvSales.DecimalCols = new int[0];
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSales.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvSales.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSales.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvSales.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSales.HasSlNo = false;
            this.dgvSales.LastRowIndex = 0;
            this.dgvSales.Location = new System.Drawing.Point(1, 1);
            this.dgvSales.Name = "dgvSales";
            this.dgvSales.NegativeValueCols = new int[0];
            this.dgvSales.NumericCols = new int[0];
            this.dgvSales.RowHeadersWidth = 50;
            this.dgvSales.Size = new System.Drawing.Size(1279, 229);
            this.dgvSales.TabIndex = 0;
            this.dgvSales.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSales_CellValueChanged);
            this.dgvSales.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvSales_UserDeletingRow);
            this.dgvSales.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvSales_CellMouseClick);
            this.dgvSales.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvSales_CellBeginEdit);
            this.dgvSales.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvSales_RowsAdded);
            this.dgvSales.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSales_CellEndEdit);
            this.dgvSales.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSales_CellClick);
            this.dgvSales.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvSales_Textbox_TextChanged);
            this.dgvSales.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvSales_EditingControlShowing);
            this.dgvSales.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvSales_CurrentCellDirtyStateChanged);
            this.dgvSales.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSales_CellEnter);
            this.dgvSales.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvSales_RowsRemoved);
            // 
            // ItemCode
            // 
            this.ItemCode.HeaderText = "Item Code";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ItemCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemCode.Width = 150;
            // 
            // ItemName
            // 
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemName.Width = 207;
            // 
            // BatchNo
            // 
            this.BatchNo.HeaderText = "BatchNo";
            this.BatchNo.Name = "BatchNo";
            this.BatchNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BatchNo.Visible = false;
            this.BatchNo.Width = 125;
            // 
            // OrderedQty
            // 
            this.OrderedQty.HeaderText = "OrderedQty";
            this.OrderedQty.Name = "OrderedQty";
            this.OrderedQty.Visible = false;
            // 
            // InvoicedQty
            // 
            this.InvoicedQty.HeaderText = "InvoicedQty";
            this.InvoicedQty.Name = "InvoicedQty";
            this.InvoicedQty.Visible = false;
            // 
            // Quantity
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle4.Format = "N3";
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle4;
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.MaxInputLength = 10;
            this.Quantity.Name = "Quantity";
            this.Quantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Uom
            // 
            this.Uom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Uom.HeaderText = "UOM";
            this.Uom.Name = "Uom";
            this.Uom.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Uom.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Rate
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle5.Format = "N2";
            this.Rate.DefaultCellStyle = dataGridViewCellStyle5;
            this.Rate.HeaderText = "Rate";
            this.Rate.MaxInputLength = 10;
            this.Rate.Name = "Rate";
            this.Rate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // GrandAmount
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.GrandAmount.DefaultCellStyle = dataGridViewCellStyle6;
            this.GrandAmount.HeaderText = "Grand Amount";
            this.GrandAmount.Name = "GrandAmount";
            // 
            // Discount
            // 
            this.Discount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Discount.HeaderText = "Discount";
            this.Discount.Items.AddRange(new object[] {
            "%",
            "Amt"});
            this.Discount.Name = "Discount";
            this.Discount.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // DiscountAmount
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.DiscountAmount.DefaultCellStyle = dataGridViewCellStyle7;
            this.DiscountAmount.HeaderText = "Disc. Value";
            this.DiscountAmount.MaxInputLength = 10;
            this.DiscountAmount.Name = "DiscountAmount";
            // 
            // NetAmount
            // 
            this.NetAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle8.Format = "N2";
            this.NetAmount.DefaultCellStyle = dataGridViewCellStyle8;
            this.NetAmount.HeaderText = "Net Amount";
            this.NetAmount.MaxInputLength = 10;
            this.NetAmount.MinimumWidth = 100;
            this.NetAmount.Name = "NetAmount";
            this.NetAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // SalesOrderID
            // 
            dataGridViewCellStyle9.Format = "N0";
            dataGridViewCellStyle9.NullValue = "0";
            this.SalesOrderID.DefaultCellStyle = dataGridViewCellStyle9;
            this.SalesOrderID.HeaderText = "SalesOrderID";
            this.SalesOrderID.Name = "SalesOrderID";
            this.SalesOrderID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SalesOrderID.Visible = false;
            // 
            // ItemID
            // 
            dataGridViewCellStyle10.Format = "N0";
            dataGridViewCellStyle10.NullValue = "0";
            this.ItemID.DefaultCellStyle = dataGridViewCellStyle10;
            this.ItemID.HeaderText = "ItemID";
            this.ItemID.Name = "ItemID";
            this.ItemID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemID.Visible = false;
            // 
            // BatchID
            // 
            this.BatchID.HeaderText = "BatchID";
            this.BatchID.Name = "BatchID";
            this.BatchID.Visible = false;
            // 
            // IsGroup
            // 
            this.IsGroup.HeaderText = "IsGroup";
            this.IsGroup.Name = "IsGroup";
            this.IsGroup.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsGroup.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.IsGroup.Visible = false;
            // 
            // QtyAvailable
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.QtyAvailable.DefaultCellStyle = dataGridViewCellStyle11;
            this.QtyAvailable.HeaderText = "QtyAvailable";
            this.QtyAvailable.Name = "QtyAvailable";
            this.QtyAvailable.Visible = false;
            // 
            // IsAutofilled
            // 
            this.IsAutofilled.HeaderText = "IsAutofilled";
            this.IsAutofilled.Name = "IsAutofilled";
            this.IsAutofilled.Visible = false;
            // 
            // IsGroupItem
            // 
            this.IsGroupItem.HeaderText = "IsGroupItem";
            this.IsGroupItem.Name = "IsGroupItem";
            this.IsGroupItem.Visible = false;
            // 
            // DiscAmount
            // 
            this.DiscAmount.DataPropertyName = "DiscAmount";
            this.DiscAmount.HeaderText = "DiscAmount";
            this.DiscAmount.Name = "DiscAmount";
            this.DiscAmount.Visible = false;
            // 
            // ProductTypeID
            // 
            this.ProductTypeID.HeaderText = "ProductTypeID";
            this.ProductTypeID.Name = "ProductTypeID";
            this.ProductTypeID.Visible = false;
            // 
            // tpItemDetails
            // 
            this.tpItemDetails.AttachedControl = this.tabControlPanel1;
            this.tpItemDetails.Name = "tpItemDetails";
            this.tpItemDetails.Text = "Item Details";
            // 
            // panelGridBottom
            // 
            this.panelGridBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelGridBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelGridBottom.Controls.Add(this.txtLpoNo);
            this.panelGridBottom.Controls.Add(this.lbllpo);
            this.panelGridBottom.Controls.Add(this.cboAccount);
            this.panelGridBottom.Controls.Add(this.lblAmountIn);
            this.panelGridBottom.Controls.Add(this.pnlBottom);
            this.panelGridBottom.Controls.Add(this.panelEx2);
            this.panelGridBottom.Controls.Add(this.txtRemarks);
            this.panelGridBottom.Controls.Add(this.lblRemarks);
            this.panelGridBottom.Controls.Add(this.cboPaymentTerms);
            this.panelGridBottom.Controls.Add(this.lblPaymentTerms);
            this.panelGridBottom.Controls.Add(this.lblAmountInWords);
            this.panelGridBottom.Controls.Add(this.lblAccount);
            this.panelGridBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGridBottom.Location = new System.Drawing.Point(0, 253);
            this.panelGridBottom.Name = "panelGridBottom";
            this.panelGridBottom.Size = new System.Drawing.Size(1281, 136);
            this.panelGridBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelGridBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelGridBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelGridBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelGridBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelGridBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelGridBottom.Style.GradientAngle = 90;
            this.panelGridBottom.TabIndex = 25;
            // 
            // txtLpoNo
            // 
            this.txtLpoNo.Location = new System.Drawing.Point(545, 7);
            this.txtLpoNo.MaxLength = 50;
            this.txtLpoNo.Name = "txtLpoNo";
            this.txtLpoNo.Size = new System.Drawing.Size(145, 20);
            this.txtLpoNo.TabIndex = 3;
            // 
            // lbllpo
            // 
            this.lbllpo.AutoSize = true;
            this.lbllpo.Location = new System.Drawing.Point(496, 10);
            this.lbllpo.Name = "lbllpo";
            this.lbllpo.Size = new System.Drawing.Size(48, 13);
            this.lbllpo.TabIndex = 250;
            this.lbllpo.Text = "LPO No.";
            // 
            // cboAccount
            // 
            this.cboAccount.DisplayMember = "Text";
            this.cboAccount.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboAccount.FormattingEnabled = true;
            this.cboAccount.ItemHeight = 14;
            this.cboAccount.Location = new System.Drawing.Point(324, 7);
            this.cboAccount.Name = "cboAccount";
            this.cboAccount.Size = new System.Drawing.Size(145, 20);
            this.cboAccount.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboAccount.TabIndex = 2;
            this.cboAccount.Visible = false;
            // 
            // lblAmountIn
            // 
            this.lblAmountIn.AutoSize = true;
            this.lblAmountIn.Location = new System.Drawing.Point(8, 84);
            this.lblAmountIn.Name = "lblAmountIn";
            this.lblAmountIn.Size = new System.Drawing.Size(89, 13);
            this.lblAmountIn.TabIndex = 246;
            this.lblAmountIn.Text = "Amount In Words";
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.lblCreatedByText);
            this.pnlBottom.Controls.Add(this.lblCreatedDateValue);
            this.pnlBottom.Controls.Add(this.lblSalesOrderstatus);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 110);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1281, 26);
            this.pnlBottom.TabIndex = 245;
            // 
            // lblCreatedByText
            // 
            this.lblCreatedByText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedByText.CloseButtonVisible = false;
            this.lblCreatedByText.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblCreatedByText.Location = new System.Drawing.Point(899, 0);
            this.lblCreatedByText.Name = "lblCreatedByText";
            this.lblCreatedByText.OptionsButtonVisible = false;
            this.lblCreatedByText.Size = new System.Drawing.Size(380, 24);
            this.lblCreatedByText.TabIndex = 258;
            this.lblCreatedByText.Text = "Created By";
            // 
            // lblCreatedDateValue
            // 
            this.lblCreatedDateValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedDateValue.CloseButtonVisible = false;
            this.lblCreatedDateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCreatedDateValue.Location = new System.Drawing.Point(393, 0);
            this.lblCreatedDateValue.Name = "lblCreatedDateValue";
            this.lblCreatedDateValue.OptionsButtonVisible = false;
            this.lblCreatedDateValue.Size = new System.Drawing.Size(886, 24);
            this.lblCreatedDateValue.TabIndex = 256;
            this.lblCreatedDateValue.Text = "Created Date";
            // 
            // lblSalesOrderstatus
            // 
            this.lblSalesOrderstatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblSalesOrderstatus.CloseButtonVisible = false;
            this.lblSalesOrderstatus.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblSalesOrderstatus.Image = ((System.Drawing.Image)(resources.GetObject("lblSalesOrderstatus.Image")));
            this.lblSalesOrderstatus.Location = new System.Drawing.Point(0, 0);
            this.lblSalesOrderstatus.Name = "lblSalesOrderstatus";
            this.lblSalesOrderstatus.OptionsButtonVisible = false;
            this.lblSalesOrderstatus.Size = new System.Drawing.Size(393, 24);
            this.lblSalesOrderstatus.TabIndex = 20;
            // 
            // panelEx2
            // 
            this.panelEx2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.panelEx2.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx2.Controls.Add(this.label2);
            this.panelEx2.Controls.Add(this.txtTaxAmount);
            this.panelEx2.Controls.Add(this.lblAdvancePayment);
            this.panelEx2.Controls.Add(this.txtAdvPayment);
            this.panelEx2.Controls.Add(this.lblTotalAmount);
            this.panelEx2.Controls.Add(this.lblDiscount);
            this.panelEx2.Controls.Add(this.lblExpenseAmount);
            this.panelEx2.Controls.Add(this.lblSubtotal);
            this.panelEx2.Controls.Add(this.lblCurrencyAmnt);
            this.panelEx2.Controls.Add(this.txtExchangeCurrencyRate);
            this.panelEx2.Controls.Add(this.btnAddExpense);
            this.panelEx2.Controls.Add(this.txtExpenseAmount);
            this.panelEx2.Controls.Add(this.txtDiscount);
            this.panelEx2.Controls.Add(this.txtSubtotal);
            this.panelEx2.Controls.Add(this.cboDiscount);
            this.panelEx2.Controls.Add(this.txtTotalAmount);
            this.panelEx2.Location = new System.Drawing.Point(892, 5);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(389, 106);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 241;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(197, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 9);
            this.label2.TabIndex = 305;
            this.label2.Text = "Tax Amount";
            // 
            // txtTaxAmount
            // 
            this.txtTaxAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTaxAmount.Border.Class = "TextBoxBorder";
            this.txtTaxAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTaxAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxAmount.Location = new System.Drawing.Point(196, 46);
            this.txtTaxAmount.Name = "txtTaxAmount";
            this.txtTaxAmount.ReadOnly = true;
            this.txtTaxAmount.Size = new System.Drawing.Size(185, 20);
            this.txtTaxAmount.TabIndex = 304;
            this.txtTaxAmount.TabStop = false;
            this.txtTaxAmount.Text = "0.00";
            this.txtTaxAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblAdvancePayment
            // 
            this.lblAdvancePayment.AutoSize = true;
            this.lblAdvancePayment.BackColor = System.Drawing.Color.White;
            this.lblAdvancePayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdvancePayment.Location = new System.Drawing.Point(9, 12);
            this.lblAdvancePayment.Name = "lblAdvancePayment";
            this.lblAdvancePayment.Size = new System.Drawing.Size(66, 9);
            this.lblAdvancePayment.TabIndex = 303;
            this.lblAdvancePayment.Text = "Advance Payment";
            this.lblAdvancePayment.Visible = false;
            // 
            // txtAdvPayment
            // 
            this.txtAdvPayment.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtAdvPayment.Border.Class = "TextBoxBorder";
            this.txtAdvPayment.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtAdvPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdvPayment.Location = new System.Drawing.Point(8, 2);
            this.txtAdvPayment.MaxLength = 20;
            this.txtAdvPayment.Name = "txtAdvPayment";
            this.txtAdvPayment.ReadOnly = true;
            this.txtAdvPayment.Size = new System.Drawing.Size(185, 20);
            this.txtAdvPayment.TabIndex = 0;
            this.txtAdvPayment.TabStop = false;
            this.txtAdvPayment.Text = "0.00";
            this.txtAdvPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAdvPayment.Visible = false;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.BackColor = System.Drawing.Color.White;
            this.lblTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(197, 92);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(45, 9);
            this.lblTotalAmount.TabIndex = 253;
            this.lblTotalAmount.Text = "Net Amount";
            // 
            // lblDiscount
            // 
            this.lblDiscount.AutoSize = true;
            this.lblDiscount.BackColor = System.Drawing.Color.White;
            this.lblDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscount.Location = new System.Drawing.Point(91, 55);
            this.lblDiscount.Name = "lblDiscount";
            this.lblDiscount.Size = new System.Drawing.Size(35, 9);
            this.lblDiscount.TabIndex = 251;
            this.lblDiscount.Text = "Discount";
            this.lblDiscount.Visible = false;
            // 
            // lblExpenseAmount
            // 
            this.lblExpenseAmount.AutoSize = true;
            this.lblExpenseAmount.BackColor = System.Drawing.Color.White;
            this.lblExpenseAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpenseAmount.Location = new System.Drawing.Point(197, 34);
            this.lblExpenseAmount.Name = "lblExpenseAmount";
            this.lblExpenseAmount.Size = new System.Drawing.Size(38, 9);
            this.lblExpenseAmount.TabIndex = 254;
            this.lblExpenseAmount.Text = "Expenses";
            // 
            // lblSubtotal
            // 
            this.lblSubtotal.AutoSize = true;
            this.lblSubtotal.BackColor = System.Drawing.Color.White;
            this.lblSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubtotal.Location = new System.Drawing.Point(197, 12);
            this.lblSubtotal.Name = "lblSubtotal";
            this.lblSubtotal.Size = new System.Drawing.Size(37, 9);
            this.lblSubtotal.TabIndex = 250;
            this.lblSubtotal.Text = "Sub Total";
            // 
            // lblCurrencyAmnt
            // 
            this.lblCurrencyAmnt.AutoSize = true;
            this.lblCurrencyAmnt.BackColor = System.Drawing.Color.White;
            this.lblCurrencyAmnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrencyAmnt.Location = new System.Drawing.Point(9, 92);
            this.lblCurrencyAmnt.Name = "lblCurrencyAmnt";
            this.lblCurrencyAmnt.Size = new System.Drawing.Size(39, 9);
            this.lblCurrencyAmnt.TabIndex = 240;
            this.lblCurrencyAmnt.Text = "Amount in";
            // 
            // txtExchangeCurrencyRate
            // 
            this.txtExchangeCurrencyRate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtExchangeCurrencyRate.Border.Class = "TextBoxBorder";
            this.txtExchangeCurrencyRate.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtExchangeCurrencyRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExchangeCurrencyRate.Location = new System.Drawing.Point(8, 67);
            this.txtExchangeCurrencyRate.Name = "txtExchangeCurrencyRate";
            this.txtExchangeCurrencyRate.ReadOnly = true;
            this.txtExchangeCurrencyRate.Size = new System.Drawing.Size(185, 35);
            this.txtExchangeCurrencyRate.TabIndex = 2;
            this.txtExchangeCurrencyRate.TabStop = false;
            this.txtExchangeCurrencyRate.Text = "0.00";
            this.txtExchangeCurrencyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnAddExpense
            // 
            this.btnAddExpense.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddExpense.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddExpense.Location = new System.Drawing.Point(164, 24);
            this.btnAddExpense.Name = "btnAddExpense";
            this.btnAddExpense.Size = new System.Drawing.Size(29, 20);
            this.btnAddExpense.TabIndex = 18;
            this.btnAddExpense.Text = "...";
            this.btnAddExpense.Click += new System.EventHandler(this.btnAddExpense_Click);
            // 
            // txtExpenseAmount
            // 
            this.txtExpenseAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtExpenseAmount.Border.Class = "TextBoxBorder";
            this.txtExpenseAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtExpenseAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExpenseAmount.Location = new System.Drawing.Point(196, 24);
            this.txtExpenseAmount.Name = "txtExpenseAmount";
            this.txtExpenseAmount.ReadOnly = true;
            this.txtExpenseAmount.Size = new System.Drawing.Size(185, 20);
            this.txtExpenseAmount.TabIndex = 4;
            this.txtExpenseAmount.TabStop = false;
            this.txtExpenseAmount.Text = "0.00";
            this.txtExpenseAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtDiscount
            // 
            this.txtDiscount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDiscount.Border.Class = "TextBoxBorder";
            this.txtDiscount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscount.Location = new System.Drawing.Point(89, 46);
            this.txtDiscount.MaxLength = 17;
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(104, 20);
            this.txtDiscount.TabIndex = 5;
            this.txtDiscount.TabStop = false;
            this.txtDiscount.Text = "0.00";
            this.txtDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscount.Visible = false;
            this.txtDiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDiscount_KeyPress);
            this.txtDiscount.TextChanged += new System.EventHandler(this.txtDiscount_TextChanged);
            // 
            // txtSubtotal
            // 
            this.txtSubtotal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtSubtotal.Border.Class = "TextBoxBorder";
            this.txtSubtotal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubtotal.Location = new System.Drawing.Point(196, 2);
            this.txtSubtotal.Name = "txtSubtotal";
            this.txtSubtotal.ReadOnly = true;
            this.txtSubtotal.Size = new System.Drawing.Size(185, 20);
            this.txtSubtotal.TabIndex = 3;
            this.txtSubtotal.TabStop = false;
            this.txtSubtotal.Text = "0.00";
            this.txtSubtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSubtotal.TextChanged += new System.EventHandler(this.txtSubtotal_TextChanged);
            // 
            // cboDiscount
            // 
            this.cboDiscount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDiscount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDiscount.DisplayMember = "Text";
            this.cboDiscount.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDiscount.DropDownHeight = 75;
            this.cboDiscount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDiscount.FormattingEnabled = true;
            this.cboDiscount.IntegralHeight = false;
            this.cboDiscount.ItemHeight = 14;
            this.cboDiscount.Items.AddRange(new object[] {
            this.cboPercentage,
            this.cboAmount});
            this.cboDiscount.Location = new System.Drawing.Point(8, 46);
            this.cboDiscount.Name = "cboDiscount";
            this.cboDiscount.Size = new System.Drawing.Size(81, 20);
            this.cboDiscount.TabIndex = 1;
            this.cboDiscount.Visible = false;
            this.cboDiscount.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDiscount.WatermarkText = "Select Discount";
            this.cboDiscount.SelectedIndexChanged += new System.EventHandler(this.cboDiscount_SelectedIndexChanged);
            // 
            // cboPercentage
            // 
            this.cboPercentage.Text = "%";
            // 
            // cboAmount
            // 
            this.cboAmount.Text = "Amount";
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTotalAmount.Border.Class = "TextBoxBorder";
            this.txtTotalAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(196, 67);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.Size = new System.Drawing.Size(185, 35);
            this.txtTotalAmount.TabIndex = 6;
            this.txtTotalAmount.TabStop = false;
            this.txtTotalAmount.Text = "0.00";
            this.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalAmount.TextChanged += new System.EventHandler(this.txtTotalAmount_TextChanged);
            // 
            // txtRemarks
            // 
            // 
            // 
            // 
            this.txtRemarks.Border.Class = "TextBoxBorder";
            this.txtRemarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtRemarks.Location = new System.Drawing.Point(102, 31);
            this.txtRemarks.MaxLength = 2000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(367, 50);
            this.txtRemarks.TabIndex = 1;
            this.txtRemarks.TextChanged += new System.EventHandler(this.TxtCbo_ValueChanged);
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(8, 35);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 237;
            this.lblRemarks.Text = "Remarks";
            // 
            // cboPaymentTerms
            // 
            this.cboPaymentTerms.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboPaymentTerms.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboPaymentTerms.DisplayMember = "Text";
            this.cboPaymentTerms.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboPaymentTerms.DropDownHeight = 75;
            this.cboPaymentTerms.FormattingEnabled = true;
            this.cboPaymentTerms.IntegralHeight = false;
            this.cboPaymentTerms.ItemHeight = 14;
            this.cboPaymentTerms.Location = new System.Drawing.Point(102, 7);
            this.cboPaymentTerms.Name = "cboPaymentTerms";
            this.cboPaymentTerms.Size = new System.Drawing.Size(145, 20);
            this.cboPaymentTerms.TabIndex = 0;
            this.cboPaymentTerms.SelectedIndexChanged += new System.EventHandler(this.cboPaymentTerms_SelectedIndexChanged);
            this.cboPaymentTerms.SelectedValueChanged += new System.EventHandler(this.TxtCbo_ValueChanged);
            this.cboPaymentTerms.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblPaymentTerms
            // 
            this.lblPaymentTerms.AutoSize = true;
            this.lblPaymentTerms.Location = new System.Drawing.Point(8, 10);
            this.lblPaymentTerms.Name = "lblPaymentTerms";
            this.lblPaymentTerms.Size = new System.Drawing.Size(80, 13);
            this.lblPaymentTerms.TabIndex = 236;
            this.lblPaymentTerms.Text = "Payment Terms";
            // 
            // lblAmountInWords
            // 
            this.lblAmountInWords.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountInWords.Location = new System.Drawing.Point(101, 84);
            this.lblAmountInWords.Name = "lblAmountInWords";
            this.lblAmountInWords.Size = new System.Drawing.Size(785, 23);
            this.lblAmountInWords.TabIndex = 247;
            // 
            // lblAccount
            // 
            // 
            // 
            // 
            this.lblAccount.BackgroundStyle.Class = "";
            this.lblAccount.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAccount.Location = new System.Drawing.Point(266, 7);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(82, 20);
            this.lblAccount.TabIndex = 249;
            this.lblAccount.Text = "Account";
            this.lblAccount.Visible = false;
            // 
            // expandableSplitterTop
            // 
            this.expandableSplitterTop.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterTop.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitterTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitterTop.ExpandableControl = this.panelTop;
            this.expandableSplitterTop.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterTop.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterTop.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterTop.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterTop.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.Location = new System.Drawing.Point(0, 152);
            this.expandableSplitterTop.Name = "expandableSplitterTop";
            this.expandableSplitterTop.Size = new System.Drawing.Size(1281, 3);
            this.expandableSplitterTop.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterTop.TabIndex = 16;
            this.expandableSplitterTop.TabStop = false;
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelTop.Controls.Add(this.tcGeneral);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1281, 127);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 0;
            // 
            // tcGeneral
            // 
            this.tcGeneral.BackColor = System.Drawing.Color.Transparent;
            this.tcGeneral.CanReorderTabs = true;
            this.tcGeneral.Controls.Add(this.tabControlPanel3);
            this.tcGeneral.Controls.Add(this.tabControlPanel4);
            this.tcGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcGeneral.Location = new System.Drawing.Point(0, 0);
            this.tcGeneral.Name = "tcGeneral";
            this.tcGeneral.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcGeneral.SelectedTabIndex = 0;
            this.tcGeneral.Size = new System.Drawing.Size(1281, 127);
            this.tcGeneral.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcGeneral.TabIndex = 0;
            this.tcGeneral.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcGeneral.Tabs.Add(this.tiGeneral);
            this.tcGeneral.Tabs.Add(this.tiSuggestions);
            this.tcGeneral.TabStop = false;
            this.tcGeneral.Text = "tabControl1";
            this.tcGeneral.SelectedTabChanged += new DevComponents.DotNetBar.TabStrip.SelectedTabChangedEventHandler(this.tcGeneral_SelectedTabChanged);
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel3.Controls.Add(this.cboTaxScheme);
            this.tabControlPanel3.Controls.Add(this.label1);
            this.tabControlPanel3.Controls.Add(this.cblSaleQuotationNo);
            this.tabControlPanel3.Controls.Add(this.btnAddressChange);
            this.tabControlPanel3.Controls.Add(this.lblVendorAddress);
            this.tabControlPanel3.Controls.Add(this.lblCustomer);
            this.tabControlPanel3.Controls.Add(this.lblCompany);
            this.tabControlPanel3.Controls.Add(this.lblStatusLabel);
            this.tabControlPanel3.Controls.Add(this.cboCustomer);
            this.tabControlPanel3.Controls.Add(this.btnCustomer);
            this.tabControlPanel3.Controls.Add(this.lblCurrency);
            this.tabControlPanel3.Controls.Add(this.txtVendorAddress);
            this.tabControlPanel3.Controls.Add(this.cboCompany);
            this.tabControlPanel3.Controls.Add(this.btnTContextmenu);
            this.tabControlPanel3.Controls.Add(this.cboCurrency);
            this.tabControlPanel3.Controls.Add(this.txtOrderNo);
            this.tabControlPanel3.Controls.Add(this.cboOrderNumbers);
            this.tabControlPanel3.Controls.Add(this.dtpDueDate);
            this.tabControlPanel3.Controls.Add(this.cboOrderType);
            this.tabControlPanel3.Controls.Add(this.txtDescription);
            this.tabControlPanel3.Controls.Add(this.lblOrderNumber);
            this.tabControlPanel3.Controls.Add(this.dtpOrderDate);
            this.tabControlPanel3.Controls.Add(this.lblDate);
            this.tabControlPanel3.Controls.Add(this.lblDescription);
            this.tabControlPanel3.Controls.Add(this.lblOrderType);
            this.tabControlPanel3.Controls.Add(this.lblStatus);
            this.tabControlPanel3.Controls.Add(this.lblDueDate);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(1281, 105);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 0;
            this.tabControlPanel3.TabItem = this.tiGeneral;
            // 
            // cboTaxScheme
            // 
            this.cboTaxScheme.DisplayMember = "Text";
            this.cboTaxScheme.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboTaxScheme.DropDownHeight = 75;
            this.cboTaxScheme.FormattingEnabled = true;
            this.cboTaxScheme.IntegralHeight = false;
            this.cboTaxScheme.ItemHeight = 14;
            this.cboTaxScheme.Location = new System.Drawing.Point(970, 8);
            this.cboTaxScheme.Name = "cboTaxScheme";
            this.cboTaxScheme.Size = new System.Drawing.Size(115, 20);
            this.cboTaxScheme.TabIndex = 258;
            this.cboTaxScheme.Tag = "DoNotChange";
            this.cboTaxScheme.SelectedIndexChanged += new System.EventHandler(this.cboTaxScheme_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(903, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 259;
            this.label1.Text = "TaxScheme";
            // 
            // cblSaleQuotationNo
            // 
            this.cblSaleQuotationNo.CheckOnClick = true;
            this.cblSaleQuotationNo.FormattingEnabled = true;
            this.cblSaleQuotationNo.Location = new System.Drawing.Point(496, 30);
            this.cblSaleQuotationNo.Name = "cblSaleQuotationNo";
            this.cblSaleQuotationNo.Size = new System.Drawing.Size(191, 64);
            this.cblSaleQuotationNo.TabIndex = 6;
            this.cblSaleQuotationNo.SelectedIndexChanged += new System.EventHandler(this.cblSaleQuotationNo_SelectedIndexChanged);
            // 
            // btnAddressChange
            // 
            this.btnAddressChange.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddressChange.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddressChange.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnAddressChange.Location = new System.Drawing.Point(242, 53);
            this.btnAddressChange.Name = "btnAddressChange";
            this.btnAddressChange.Size = new System.Drawing.Size(29, 20);
            this.btnAddressChange.TabIndex = 8;
            this.btnAddressChange.Text = "6";
            this.btnAddressChange.Click += new System.EventHandler(this.btnAddressChange_Click);
            // 
            // lblVendorAddress
            // 
            this.lblVendorAddress.AutoSize = true;
            this.lblVendorAddress.BackColor = System.Drawing.Color.Transparent;
            this.lblVendorAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F);
            this.ErrSales.SetIconAlignment(this.lblVendorAddress, System.Windows.Forms.ErrorIconAlignment.BottomLeft);
            this.lblVendorAddress.Location = new System.Drawing.Point(66, 53);
            this.lblVendorAddress.Name = "lblVendorAddress";
            this.lblVendorAddress.Size = new System.Drawing.Size(33, 9);
            this.lblVendorAddress.TabIndex = 194;
            this.lblVendorAddress.Text = "Address";
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.BackColor = System.Drawing.Color.Transparent;
            this.lblCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomer.Location = new System.Drawing.Point(9, 33);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(51, 13);
            this.lblCustomer.TabIndex = 184;
            this.lblCustomer.Text = "Customer";
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.BackColor = System.Drawing.Color.Transparent;
            this.lblCompany.Location = new System.Drawing.Point(9, 7);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 237;
            this.lblCompany.Text = "Company";
            // 
            // lblStatusLabel
            // 
            this.lblStatusLabel.AutoSize = true;
            this.lblStatusLabel.BackColor = System.Drawing.Color.Transparent;
            this.lblStatusLabel.Location = new System.Drawing.Point(1104, 8);
            this.lblStatusLabel.Name = "lblStatusLabel";
            this.lblStatusLabel.Size = new System.Drawing.Size(37, 13);
            this.lblStatusLabel.TabIndex = 252;
            this.lblStatusLabel.Text = "Status";
            // 
            // cboCustomer
            // 
            this.cboCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCustomer.DisplayMember = "Text";
            this.cboCustomer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCustomer.DropDownHeight = 75;
            this.cboCustomer.FormattingEnabled = true;
            this.cboCustomer.IntegralHeight = false;
            this.cboCustomer.ItemHeight = 14;
            this.cboCustomer.Location = new System.Drawing.Point(68, 30);
            this.cboCustomer.Name = "cboCustomer";
            this.cboCustomer.Size = new System.Drawing.Size(171, 20);
            this.cboCustomer.TabIndex = 1;
            this.cboCustomer.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCustomer.WatermarkText = "Select Customer";
            this.cboCustomer.SelectedIndexChanged += new System.EventHandler(this.cboCustomer_SelectedIndexChanged);
            this.cboCustomer.SelectedValueChanged += new System.EventHandler(this.TxtCbo_ValueChanged);
            this.cboCustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // btnCustomer
            // 
            this.btnCustomer.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCustomer.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCustomer.Location = new System.Drawing.Point(242, 30);
            this.btnCustomer.Name = "btnCustomer";
            this.btnCustomer.Size = new System.Drawing.Size(29, 20);
            this.btnCustomer.TabIndex = 7;
            this.btnCustomer.Text = "....";
            this.btnCustomer.Click += new System.EventHandler(this.btnCustomer_Click);
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrency.Location = new System.Drawing.Point(285, 33);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(49, 13);
            this.lblCurrency.TabIndex = 251;
            this.lblCurrency.Text = "Currency";
            // 
            // txtVendorAddress
            // 
            // 
            // 
            // 
            this.txtVendorAddress.Border.Class = "TextBoxBorder";
            this.txtVendorAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtVendorAddress.Location = new System.Drawing.Point(68, 53);
            this.txtVendorAddress.MaxLength = 200;
            this.txtVendorAddress.Multiline = true;
            this.txtVendorAddress.Name = "txtVendorAddress";
            this.txtVendorAddress.ReadOnly = true;
            this.txtVendorAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtVendorAddress.Size = new System.Drawing.Size(171, 51);
            this.txtVendorAddress.TabIndex = 3;
            this.txtVendorAddress.TabStop = false;
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 75;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(68, 4);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(203, 20);
            this.cboCompany.TabIndex = 0;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.SelectedValueChanged += new System.EventHandler(this.cboCompany_SelectedValueChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // btnTContextmenu
            // 
            this.btnTContextmenu.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnTContextmenu.Location = new System.Drawing.Point(185, 81);
            this.btnTContextmenu.Name = "btnTContextmenu";
            this.btnTContextmenu.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.btnTContextmenu.Size = new System.Drawing.Size(29, 23);
            this.btnTContextmenu.TabIndex = 8;
            this.btnTContextmenu.Text = "6";
            this.btnTContextmenu.UseVisualStyleBackColor = true;
            this.btnTContextmenu.Visible = false;
            this.btnTContextmenu.Click += new System.EventHandler(this.btnTContextmenu_Click);
            // 
            // cboCurrency
            // 
            this.cboCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCurrency.DisplayMember = "Text";
            this.cboCurrency.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCurrency.DropDownHeight = 75;
            this.cboCurrency.Enabled = false;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.IntegralHeight = false;
            this.cboCurrency.ItemHeight = 14;
            this.cboCurrency.Location = new System.Drawing.Point(360, 30);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(114, 20);
            this.cboCurrency.TabIndex = 5;
            this.cboCurrency.TabStop = false;
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            this.cboCurrency.SelectedValueChanged += new System.EventHandler(this.cboCurrency_SelectedValueChanged);
            this.cboCurrency.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtOrderNo.Border.Class = "TextBoxBorder";
            this.txtOrderNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtOrderNo.Location = new System.Drawing.Point(786, 8);
            this.txtOrderNo.MaxLength = 20;
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(112, 20);
            this.txtOrderNo.TabIndex = 7;
            this.txtOrderNo.TabStop = false;
            this.txtOrderNo.TextChanged += new System.EventHandler(this.TxtCbo_ValueChanged);
            // 
            // cboOrderNumbers
            // 
            this.cboOrderNumbers.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOrderNumbers.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOrderNumbers.DisplayMember = "Text";
            this.cboOrderNumbers.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboOrderNumbers.DropDownHeight = 75;
            this.cboOrderNumbers.FormattingEnabled = true;
            this.cboOrderNumbers.IntegralHeight = false;
            this.cboOrderNumbers.ItemHeight = 14;
            this.cboOrderNumbers.Location = new System.Drawing.Point(496, 4);
            this.cboOrderNumbers.Name = "cboOrderNumbers";
            this.cboOrderNumbers.Size = new System.Drawing.Size(111, 20);
            this.cboOrderNumbers.TabIndex = 4;
            this.cboOrderNumbers.SelectedIndexChanged += new System.EventHandler(this.cboOrderNumbers_SelectedIndexChanged);
            this.cboOrderNumbers.SelectedValueChanged += new System.EventHandler(this.TxtCbo_ValueChanged);
            this.cboOrderNumbers.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // dtpDueDate
            // 
            this.dtpDueDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDueDate.Location = new System.Drawing.Point(786, 58);
            this.dtpDueDate.Name = "dtpDueDate";
            this.dtpDueDate.Size = new System.Drawing.Size(112, 20);
            this.dtpDueDate.TabIndex = 9;
            this.dtpDueDate.ValueChanged += new System.EventHandler(this.TxtCbo_ValueChanged);
            // 
            // cboOrderType
            // 
            this.cboOrderType.DisplayMember = "Text";
            this.cboOrderType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboOrderType.DropDownHeight = 75;
            this.cboOrderType.FormattingEnabled = true;
            this.cboOrderType.IntegralHeight = false;
            this.cboOrderType.ItemHeight = 14;
            this.cboOrderType.Location = new System.Drawing.Point(360, 4);
            this.cboOrderType.Name = "cboOrderType";
            this.cboOrderType.Size = new System.Drawing.Size(114, 20);
            this.cboOrderType.TabIndex = 4;
            this.cboOrderType.Tag = "DoNotChange";
            this.cboOrderType.SelectedIndexChanged += new System.EventHandler(this.cboOrderType_SelectedIndexChanged);
            this.cboOrderType.SelectedValueChanged += new System.EventHandler(this.TxtCbo_ValueChanged);
            // 
            // txtDescription
            // 
            // 
            // 
            // 
            this.txtDescription.Border.Class = "TextBoxBorder";
            this.txtDescription.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDescription.Location = new System.Drawing.Point(1151, 31);
            this.txtDescription.MaxLength = 200;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(128, 51);
            this.txtDescription.TabIndex = 10;
            this.txtDescription.TabStop = false;
            this.txtDescription.Visible = false;
            this.txtDescription.TextChanged += new System.EventHandler(this.TxtCbo_ValueChanged);
            // 
            // lblOrderNumber
            // 
            this.lblOrderNumber.AutoSize = true;
            this.lblOrderNumber.BackColor = System.Drawing.Color.Transparent;
            this.lblOrderNumber.Location = new System.Drawing.Point(710, 12);
            this.lblOrderNumber.Name = "lblOrderNumber";
            this.lblOrderNumber.Size = new System.Drawing.Size(73, 13);
            this.lblOrderNumber.TabIndex = 185;
            this.lblOrderNumber.Text = "Order Number";
            // 
            // dtpOrderDate
            // 
            this.dtpOrderDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOrderDate.Location = new System.Drawing.Point(786, 33);
            this.dtpOrderDate.Name = "dtpOrderDate";
            this.dtpOrderDate.Size = new System.Drawing.Size(112, 20);
            this.dtpOrderDate.TabIndex = 8;
            this.dtpOrderDate.ValueChanged += new System.EventHandler(this.dtpOrderDate_ValueChanged);
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Location = new System.Drawing.Point(710, 37);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 188;
            this.lblDate.Text = "Date";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblDescription.Location = new System.Drawing.Point(1088, 30);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 13);
            this.lblDescription.TabIndex = 236;
            this.lblDescription.Text = "Description";
            this.lblDescription.Visible = false;
            // 
            // lblOrderType
            // 
            this.lblOrderType.AutoSize = true;
            this.lblOrderType.BackColor = System.Drawing.Color.Transparent;
            this.lblOrderType.Location = new System.Drawing.Point(285, 7);
            this.lblOrderType.Name = "lblOrderType";
            this.lblOrderType.Size = new System.Drawing.Size(69, 13);
            this.lblOrderType.TabIndex = 239;
            this.lblOrderType.Text = "Invoice Type";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(1169, 5);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(43, 20);
            this.lblStatus.TabIndex = 14;
            this.lblStatus.Text = "New";
            // 
            // lblDueDate
            // 
            this.lblDueDate.AutoSize = true;
            this.lblDueDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDueDate.Location = new System.Drawing.Point(710, 62);
            this.lblDueDate.Name = "lblDueDate";
            this.lblDueDate.Size = new System.Drawing.Size(53, 13);
            this.lblDueDate.TabIndex = 189;
            this.lblDueDate.Text = "Due Date";
            // 
            // tiGeneral
            // 
            this.tiGeneral.AttachedControl = this.tabControlPanel3;
            this.tiGeneral.Name = "tiGeneral";
            this.tiGeneral.Text = "General";
            // 
            // tabControlPanel4
            // 
            this.tabControlPanel4.Controls.Add(this.dgvSuggestions);
            this.tabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel4.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel4.Name = "tabControlPanel4";
            this.tabControlPanel4.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel4.Size = new System.Drawing.Size(1281, 105);
            this.tabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel4.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel4.Style.GradientAngle = 90;
            this.tabControlPanel4.TabIndex = 2;
            this.tabControlPanel4.TabItem = this.tiSuggestions;
            // 
            // dgvSuggestions
            // 
            this.dgvSuggestions.AllowUserToDeleteRows = false;
            this.dgvSuggestions.BackgroundColor = System.Drawing.Color.White;
            this.dgvSuggestions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSuggestions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UserID,
            this.UserName,
            this.Status,
            this.VerifiedDate,
            this.Comment});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSuggestions.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgvSuggestions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSuggestions.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSuggestions.Location = new System.Drawing.Point(1, 1);
            this.dgvSuggestions.Name = "dgvSuggestions";
            this.dgvSuggestions.Size = new System.Drawing.Size(1279, 103);
            this.dgvSuggestions.TabIndex = 0;
            this.dgvSuggestions.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSuggestions_CellEndEdit);
            // 
            // UserID
            // 
            this.UserID.HeaderText = "UserID";
            this.UserID.Name = "UserID";
            this.UserID.Visible = false;
            // 
            // UserName
            // 
            this.UserName.HeaderText = "User Name";
            this.UserName.Name = "UserName";
            this.UserName.ReadOnly = true;
            this.UserName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.UserName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.UserName.Width = 250;
            // 
            // Status
            // 
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // VerifiedDate
            // 
            this.VerifiedDate.HeaderText = "Verified Date";
            this.VerifiedDate.Name = "VerifiedDate";
            this.VerifiedDate.ReadOnly = true;
            this.VerifiedDate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.VerifiedDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Comment
            // 
            this.Comment.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Comment.HeaderText = "Comment";
            this.Comment.Name = "Comment";
            this.Comment.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Comment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tiSuggestions
            // 
            this.tiSuggestions.AttachedControl = this.tabControlPanel4;
            this.tiSuggestions.Name = "tiSuggestions";
            this.tiSuggestions.Text = "Suggestions";
            this.tiSuggestions.Visible = false;
            // 
            // PurchaseOrderBindingNavigator
            // 
            this.PurchaseOrderBindingNavigator.AccessibleDescription = "DotNetBar Bar (PurchaseOrderBindingNavigator)";
            this.PurchaseOrderBindingNavigator.AccessibleName = "DotNetBar Bar";
            this.PurchaseOrderBindingNavigator.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.PurchaseOrderBindingNavigator.Dock = System.Windows.Forms.DockStyle.Top;
            this.PurchaseOrderBindingNavigator.DockLine = 1;
            this.PurchaseOrderBindingNavigator.DockOffset = 73;
            this.PurchaseOrderBindingNavigator.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.PurchaseOrderBindingNavigator.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.PurchaseOrderBindingNavigator.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorClearItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorCancelItem,
            this.btnActions,
            this.BtnPrint,
            this.BtnEmail,
            this.BtnHelp});
            this.PurchaseOrderBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.PurchaseOrderBindingNavigator.Name = "PurchaseOrderBindingNavigator";
            this.PurchaseOrderBindingNavigator.Size = new System.Drawing.Size(1281, 25);
            this.PurchaseOrderBindingNavigator.Stretch = true;
            this.PurchaseOrderBindingNavigator.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PurchaseOrderBindingNavigator.TabIndex = 12;
            this.PurchaseOrderBindingNavigator.TabStop = false;
            this.PurchaseOrderBindingNavigator.Text = "bar2";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlN);
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.Tooltip = "Add New Information";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlS);
            this.BindingNavigatorSaveItem.Text = "&Save";
            this.BindingNavigatorSaveItem.Tooltip = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorClearItem
            // 
            this.BindingNavigatorClearItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorClearItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorClearItem.Image")));
            this.BindingNavigatorClearItem.Name = "BindingNavigatorClearItem";
            this.BindingNavigatorClearItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlR);
            this.BindingNavigatorClearItem.Text = "Clear";
            this.BindingNavigatorClearItem.Tooltip = "Clear";
            this.BindingNavigatorClearItem.Visible = false;
            this.BindingNavigatorClearItem.Click += new System.EventHandler(this.BindingNavigatorClearItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlDel);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Tooltip = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorCancelItem
            // 
            this.BindingNavigatorCancelItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorCancelItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorCancelItem.Image")));
            this.BindingNavigatorCancelItem.Name = "BindingNavigatorCancelItem";
            this.BindingNavigatorCancelItem.Text = "Cancel";
            this.BindingNavigatorCancelItem.Tooltip = "Cancel";
            this.BindingNavigatorCancelItem.Click += new System.EventHandler(this.BindingNavigatorCancelItem_Click);
            // 
            // btnActions
            // 
            this.btnActions.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnActions.Image = ((System.Drawing.Image)(resources.GetObject("btnActions.Image")));
            this.btnActions.Name = "btnActions";
            this.btnActions.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnExpense,
            this.bnSubmitForApproval,
            this.btnApprove,
            this.btnReject,
            this.btnSuggest,
            this.btnDeny,
            this.btnDocuments});
            this.btnActions.Text = "Actions";
            this.btnActions.Tooltip = "Actions";
            this.btnActions.PopupOpen += new DevComponents.DotNetBar.DotNetBarManager.PopupOpenEventHandler(this.btnActions_PopupOpen);
            // 
            // btnExpense
            // 
            this.btnExpense.Image = ((System.Drawing.Image)(resources.GetObject("btnExpense.Image")));
            this.btnExpense.Name = "btnExpense";
            this.btnExpense.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.btnExpense.Text = "Expense";
            this.btnExpense.Tooltip = "Expense";
            this.btnExpense.Click += new System.EventHandler(this.btnExpense_Click);
            // 
            // bnSubmitForApproval
            // 
            this.bnSubmitForApproval.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnSubmitForApproval.Image = ((System.Drawing.Image)(resources.GetObject("bnSubmitForApproval.Image")));
            this.bnSubmitForApproval.Name = "bnSubmitForApproval";
            this.bnSubmitForApproval.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.bnSubmitForApproval.Text = "Submit For Approval";
            this.bnSubmitForApproval.Tooltip = "Submit For Approval";
            this.bnSubmitForApproval.Visible = false;
            this.bnSubmitForApproval.Click += new System.EventHandler(this.bnSubmitForApproval_Click);
            // 
            // btnApprove
            // 
            this.btnApprove.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnApprove.Image = ((System.Drawing.Image)(resources.GetObject("btnApprove.Image")));
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.btnApprove.Text = "Approve";
            this.btnApprove.Tooltip = "Approve";
            this.btnApprove.Visible = false;
            this.btnApprove.Click += new System.EventHandler(this.btnApprove_Click);
            // 
            // btnReject
            // 
            this.btnReject.Image = ((System.Drawing.Image)(resources.GetObject("btnReject.Image")));
            this.btnReject.Name = "btnReject";
            this.btnReject.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlR);
            this.btnReject.Text = "Reject";
            this.btnReject.Visible = false;
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // btnSuggest
            // 
            this.btnSuggest.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSuggest.Image = ((System.Drawing.Image)(resources.GetObject("btnSuggest.Image")));
            this.btnSuggest.Name = "btnSuggest";
            this.btnSuggest.Text = "Suggest";
            this.btnSuggest.Tooltip = "Suggest";
            this.btnSuggest.Click += new System.EventHandler(this.btnSuggest_Click);
            // 
            // btnDeny
            // 
            this.btnDeny.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDeny.Image = ((System.Drawing.Image)(resources.GetObject("btnDeny.Image")));
            this.btnDeny.Name = "btnDeny";
            this.btnDeny.Text = "Deny";
            this.btnDeny.Tooltip = "Deny";
            this.btnDeny.Click += new System.EventHandler(this.btnDeny_Click);
            // 
            // btnDocuments
            // 
            this.btnDocuments.Image = ((System.Drawing.Image)(resources.GetObject("btnDocuments.Image")));
            this.btnDocuments.Name = "btnDocuments";
            this.btnDocuments.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlD);
            this.btnDocuments.Text = "Documents";
            this.btnDocuments.Tooltip = "Documents";
            this.btnDocuments.Click += new System.EventHandler(this.btnDocuments_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // BtnHelp
            // 
            this.BtnHelp.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Tooltip = "Help";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // CMSVendorAddress
            // 
            this.CMSVendorAddress.Name = "ContextMenuStrip1";
            this.CMSVendorAddress.Size = new System.Drawing.Size(61, 4);
            // 
            // TmrSales
            // 
            this.TmrSales.Interval = 2000;
            // 
            // ErrSales
            // 
            this.ErrSales.ContainerControl = this;
            this.ErrSales.RightToLeft = true;
            // 
            // ImgSales
            // 
            this.ImgSales.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgSales.ImageStream")));
            this.ImgSales.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgSales.Images.SetKeyName(0, "Purchase Indent.ICO");
            this.ImgSales.Images.SetKeyName(1, "Purchase Order.ico");
            this.ImgSales.Images.SetKeyName(2, "Purchase Invoice.ico");
            this.ImgSales.Images.SetKeyName(3, "GRN.ICO");
            this.ImgSales.Images.SetKeyName(4, "Purchase Order Return.ico");
            this.ImgSales.Images.SetKeyName(5, "Add.png");
            this.ImgSales.Images.SetKeyName(6, "save.PNG");
            // 
            // CntxtHistory
            // 
            this.CntxtHistory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ShowItemHistory,
            this.ShowSaleRate,
            this.showGroupItemDetails});
            this.CntxtHistory.Name = "CntxtHistory";
            this.CntxtHistory.Size = new System.Drawing.Size(195, 70);
            // 
            // ShowItemHistory
            // 
            this.ShowItemHistory.Name = "ShowItemHistory";
            this.ShowItemHistory.Size = new System.Drawing.Size(194, 22);
            this.ShowItemHistory.Text = "Show Item History";
            this.ShowItemHistory.Click += new System.EventHandler(this.ShowItemHistory_Click);
            // 
            // ShowSaleRate
            // 
            this.ShowSaleRate.Name = "ShowSaleRate";
            this.ShowSaleRate.Size = new System.Drawing.Size(194, 22);
            this.ShowSaleRate.Text = "Show Sale Rates";
            this.ShowSaleRate.Visible = false;
            this.ShowSaleRate.Click += new System.EventHandler(this.ShowSaleRate_Click);
            // 
            // showGroupItemDetails
            // 
            this.showGroupItemDetails.Name = "showGroupItemDetails";
            this.showGroupItemDetails.Size = new System.Drawing.Size(194, 22);
            this.showGroupItemDetails.Text = "showGroupItemDetails";
            this.showGroupItemDetails.Click += new System.EventHandler(this.showGroupItemDetails_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "User Name";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 120;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Status";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 85;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Verified Date";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 95;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Comment";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 217;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.HeaderText = "Comment";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Width = 150;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "Item Name";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Width = 180;
            // 
            // dataGridViewTextBoxColumn14
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn14.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn14.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn15.HeaderText = "Return Quantity";
            this.dataGridViewTextBoxColumn15.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn16.HeaderText = "Batch Number";
            this.dataGridViewTextBoxColumn16.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Visible = false;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewTextBoxColumn17.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn17.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn18
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn18.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewTextBoxColumn18.HeaderText = "Total";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn18.Visible = false;
            // 
            // dataGridViewTextBoxColumn19
            // 
            dataGridViewCellStyle19.Format = "N0";
            dataGridViewCellStyle19.NullValue = "0";
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn19.HeaderText = "Status";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn19.Visible = false;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.HeaderText = "OrderDetailID";
            this.dataGridViewTextBoxColumn20.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn20.Visible = false;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.HeaderText = "PurchaseOrderID";
            this.dataGridViewTextBoxColumn21.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn21.Visible = false;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn22.Visible = false;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.HeaderText = "SelectedDiscountID";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.Visible = false;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.HeaderText = "BatchID";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.Visible = false;
            // 
            // clmItemCode
            // 
            this.clmItemCode.HeaderText = "Item Code";
            this.clmItemCode.Name = "clmItemCode";
            this.clmItemCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmItemCode.Width = 150;
            // 
            // clmItemName
            // 
            this.clmItemName.HeaderText = "Item Name";
            this.clmItemName.Name = "clmItemName";
            this.clmItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmItemName.Width = 180;
            // 
            // clmQuantity
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmQuantity.DefaultCellStyle = dataGridViewCellStyle20;
            this.clmQuantity.HeaderText = "Quantity";
            this.clmQuantity.MaxInputLength = 6;
            this.clmQuantity.Name = "clmQuantity";
            this.clmQuantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmQtyReceived
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmQtyReceived.DefaultCellStyle = dataGridViewCellStyle21;
            this.clmQtyReceived.HeaderText = "QtyReceived";
            this.clmQtyReceived.MaxInputLength = 6;
            this.clmQtyReceived.Name = "clmQtyReceived";
            this.clmQtyReceived.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmBatchNumber
            // 
            this.clmBatchNumber.HeaderText = "Batch Number";
            this.clmBatchNumber.Name = "clmBatchNumber";
            this.clmBatchNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmBatchNumber.Visible = false;
            // 
            // clmRate
            // 
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmRate.DefaultCellStyle = dataGridViewCellStyle22;
            this.clmRate.HeaderText = "Rate";
            this.clmRate.MaxInputLength = 6;
            this.clmRate.Name = "clmRate";
            this.clmRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmTotal
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmTotal.DefaultCellStyle = dataGridViewCellStyle23;
            this.clmTotal.HeaderText = "Total";
            this.clmTotal.Name = "clmTotal";
            this.clmTotal.ReadOnly = true;
            this.clmTotal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmStatus
            // 
            this.clmStatus.HeaderText = "Status";
            this.clmStatus.Name = "clmStatus";
            this.clmStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmOrderDetailID
            // 
            this.clmOrderDetailID.HeaderText = "OrderDetailID";
            this.clmOrderDetailID.Name = "clmOrderDetailID";
            this.clmOrderDetailID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmOrderDetailID.Visible = false;
            // 
            // clmSalesOrderID
            // 
            this.clmSalesOrderID.HeaderText = "SalesOrderID";
            this.clmSalesOrderID.Name = "clmSalesOrderID";
            this.clmSalesOrderID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmSalesOrderID.Visible = false;
            // 
            // clmItemID
            // 
            this.clmItemID.HeaderText = "ItemID";
            this.clmItemID.Name = "clmItemID";
            this.clmItemID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmItemID.Visible = false;
            // 
            // FrmSalesOrder
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.ClientSize = new System.Drawing.Size(1284, 544);
            this.Controls.Add(this.panelEx1);
            this.Controls.Add(this.dockSite2);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.expandableSplitterLeft);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.dockSite3);
            this.Controls.Add(this.dockSite4);
            this.Controls.Add(this.dockSite5);
            this.Controls.Add(this.dockSite6);
            this.Controls.Add(this.dockSite8);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmSalesOrder";
            this.Text = "Sales Order";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmSales_Load);
            this.Shown += new System.EventHandler(this.FrmSalesNew_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSales_FormClosing);
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesDisplay)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.panelEx1.ResumeLayout(false);
            this.panelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcSales)).EndInit();
            this.tcSales.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSales)).EndInit();
            this.panelGridBottom.ResumeLayout(false);
            this.panelGridBottom.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            this.panelEx2.ResumeLayout(false);
            this.panelEx2.PerformLayout();
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).EndInit();
            this.tcGeneral.ResumeLayout(false);
            this.tabControlPanel3.ResumeLayout(false);
            this.tabControlPanel3.PerformLayout();
            this.tabControlPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSuggestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrSales)).EndInit();
            this.CntxtHistory.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #endregion

        #region Declaration

        public FrmMain objFrmTradingMain;
        int MintTempMenuID = 0, MintTempCompany = 0, MintTempEmployee = 0; // For Permission Related (Please don't use this Variable)
        string MstrTempTableName = ""; // For Permission Related (Please don't use this Variable)
        string strTableItemDetails = "InvItemDetails";
        string strTableSalesInvoice = "InvSalesInvoiceMaster";
        //string strTableReceiptAndPayment = "InvReceiptAndPayment";
        string strTableReceiptAndPayment = "AccReceiptAndPaymentMaster Payments inner join AccReceiptAndPaymentDetails RPD on  Payments.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID";
        string strTableSalesQuotation = "InvSalesQuotationMaster";
        string strTableSalesOrder = "InvSalesOrderMaster";
        string strTableUserMaster = "UserMaster";
        string strTableVerificationHistory = "InvVerificationHistory";
        string strTableInvItemIssueMaster = "InvItemIssueMaster ";

        private bool MblnQtyChanged = false;
        private bool MblnChangeStatus;                    //Check state of the page
        private bool MblnAddStatus;                       //Add/Update mode 
        private bool MblnViewPermission = false;
        private bool MblnAddPermission = false;
        private bool MblnUpdatePermission = false;
        private bool MblnDeletePermission = false;
        private bool MblnAddUpdatePermission = false;
        private bool MblnCancelPermission = false;
        private bool MblnSearchFlag = false;
        private bool MblnApproveFlag = false;
        private bool MblnRejectFlag = false;
        private bool MblnSubmitForApprovalFlag = false;
        private bool MblnIsFromCancellation = false;
        private bool MblnIsValueChanged = false;
        public bool PblnIsFromApproval { get; set; }
        private bool MblnIsFromClear = false;
        private bool MblnIsEditable = true;
        private bool mlbnReopen = true;
        private int MintFromForm;                         // From Indent 1,From Order 2 , From Invoice 3
        private int MintVendorAddID = 0;
        private int MintShipAddID = 0;
        private int MintCurrencyID = 0;

        private int intSmsType;
        private int intCustomerId;
        public Int64 PintInvoiceID = 0; // for just showing particular details no permissions
        private Int64 MintSalesID = 0; // for approving or rejecting
        public Int64 lngSalesID = 0; //For filling details with permissions
        int MintExchangeCurrencyScale = 2;
        int MintBaseCurrencyScale = 2;
        public int PintApprovalPermission = 0;
        public string strCondition1;
        private DataTable MaMessageArr;                  // Error Message display
        private DataTable MaStatusMessage;

        private string MstrMessageCommon;
        private MessageBoxIcon MmessageIcon;

        ClsLogWriter MobjLogs;
        clsBLLSTSalesOrder MobjclsBLLSTSales;
        clsBLLPermissionSettings MobjClsBLLPermissionSettings;
        clsBLLCommonUtility MobjClsCommonUtility;
        public long lngReferenceID = 0;

        private int SItemID;
        private int SIsGroup;

        // For Delivery Note
        private Int64 MlngDeliveryID;
        #endregion

        #region Constructor

        public FrmSalesOrder(int iFromForm, Int64 iInvoiceID)
        {
            InitializeComponent();

            MblnIsValueChanged = true;
            MintFromForm = iFromForm;          // Form Type Identification
            MobjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotificationNew();
            MmessageIcon = MessageBoxIcon.Information;
            MobjclsBLLSTSales = new clsBLLSTSalesOrder();
            MobjClsCommonUtility = new clsBLLCommonUtility();
            MobjClsBLLPermissionSettings = new clsBLLPermissionSettings();
            this.Load += new EventHandler(FrmSalesNew_Load);
            LoadInitials();
            MintSalesID = iInvoiceID;
        }

        public FrmSalesOrder(long intDeliveryID)
        {
            InitializeComponent();
            MintFromForm = 2;          // Form Type Identification

            MobjclsBLLSTSales = new clsBLLSTSalesOrder();
            MobjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotificationNew();
            MmessageIcon = MessageBoxIcon.Information;

            MobjClsCommonUtility = new clsBLLCommonUtility();
            MobjClsBLLPermissionSettings = new clsBLLPermissionSettings();
            ClearControls();
            MlngDeliveryID = intDeliveryID;
            LoadInitials();
        }
        #endregion

        #region Functions

        private void LoadInitials()
        {
            switch (MintFromForm)
            {
                case 1: // Sales Quotation
                    this.Icon = ((System.Drawing.Icon)(Properties.Resources.Sales_quotation1));
                    MintTempMenuID = (int)eMenuID.SalesQuotation;
                    MintTempCompany = (int)ControlOperationType.SalQuoCompany;
                    MintTempEmployee = (int)ControlOperationType.SalQuoEmployee;
                    MstrTempTableName = strTableSalesQuotation;
                    break;
                case 2: // Sales Order
                    this.Icon = ((System.Drawing.Icon)(Properties.Resources.sales_order1));
                    MintTempMenuID = (int)eMenuID.SalesOrder;
                    MintTempCompany = (int)ControlOperationType.SalOrdCompany;
                    MintTempEmployee = (int)ControlOperationType.SalOrdEmployee;
                    MstrTempTableName = strTableSalesOrder;
                    break;
                case 3: // Sales Invoice
                    this.Icon = ((System.Drawing.Icon)(Properties.Resources.Sales_invoice1));
                    MintTempMenuID = (int)eMenuID.SalesInvoice;
                    MintTempCompany = (int)ControlOperationType.SalInvCompany;
                    MintTempEmployee = (int)ControlOperationType.SalInvEmployee;
                    MstrTempTableName = strTableSalesInvoice;
                    break;
            }
        }

        private void SetEnableDisable(bool blnIsEnabled)
        {
            cboCompany.Enabled = blnIsEnabled;
            cboOrderNumbers.Enabled = cboOrderType.Enabled = blnIsEnabled;
            dtpOrderDate.Enabled = dtpDueDate.Enabled = cboCustomer.Enabled = btnAddressChange.Enabled = blnIsEnabled;
            cboPaymentTerms.Enabled = blnIsEnabled;
            btnCustomer.Enabled = blnIsEnabled;
            btnAddExpense.Enabled = blnIsEnabled;
            txtLpoNo.Enabled = blnIsEnabled;
            dgvSales.ReadOnly = !blnIsEnabled;
            cblSaleQuotationNo.Enabled = blnIsEnabled;
            if (!dgvSales.ReadOnly)
                NetAmount.ReadOnly = GrandAmount.ReadOnly = true;
            //   Rate.ReadOnly = NetAmount.ReadOnly = GrandAmount.ReadOnly = true;

            BindingNavigatorClearItem.Enabled = blnIsEnabled;
            cboDiscount.Enabled = txtExpenseAmount.Enabled = blnIsEnabled;
            txtRemarks.ReadOnly = !blnIsEnabled;
            txtExchangeCurrencyRate.Enabled = txtDiscount.Enabled = txtTotalAmount.Enabled = txtSubtotal.Enabled = blnIsEnabled;
        }

        private void SetControlsVisibility()
        {
            cboOrderType.Tag = "";

            if (MintFromForm == 2)  //Sales Order
            {
                lblSalesNo.Text = "Order No";
                lblOrderType.Text = "Order Type";

                btnApprove.Visible = false;
                btnReject.Visible = false;
                btnSuggest.Visible = btnDeny.Visible = tiSuggestions.Visible = false;

                if (MintSalesID != 0)
                {
                    btnActions.Enabled = true;
                    if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderRejected)
                    {
                        DataTable dtVerificationHistoryDetails = GetVerificationHistoryStatus(false);
                        if (dtVerificationHistoryDetails.Rows.Count > 0)
                        {
                            if ((MintFromForm == 1 && dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.SQuotationSuggested) || (MintFromForm == 2 && dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.SOrderSuggested))
                            {
                                btnSuggest.Visible = false;
                                btnDeny.Visible = true;
                                tiSuggestions.Visible = true;
                            }
                            else
                            {
                                btnSuggest.Visible = true;
                                btnDeny.Visible = false;
                                tiSuggestions.Visible = true;
                                
                            }
                        }
                        else
                        {
                            if (PintApprovalPermission == (int)ApprovePermission.Suggest || PintApprovalPermission == (int)ApprovePermission.Both)
                            {
                                tiSuggestions.Visible = true;
                                btnDeny.Visible = btnSuggest.Visible = true;
                            }
                        }
                        if (PintApprovalPermission == (int)ApprovePermission.Approve || PintApprovalPermission == (int)ApprovePermission.Both)
                            btnApprove.Visible = true;
                    }
                    else if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderApproved)
                    {
                        btnReject.Visible = true;
                    }
                    else if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderSubmittedForApproval)
                    {
                        if (PintApprovalPermission == (int)ApprovePermission.Suggest || PintApprovalPermission == (int)ApprovePermission.Both)
                        {
                            DataTable dtVerificationHistoryDetails = GetVerificationHistoryStatus(false);
                            if (dtVerificationHistoryDetails.Rows.Count > 0)
                            {
                                if ((MintFromForm == 1 && dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.SQuotationSuggested) || (MintFromForm == 2 && dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.SOrderSuggested))
                                {
                                    btnSuggest.Visible = false;
                                    btnDeny.Visible = true;
                                    tiSuggestions.Visible = true;
                                }
                                else
                                {
                                    btnSuggest.Visible = true;
                                    btnDeny.Visible = false;
                                    tiSuggestions.Visible = true;
                                }
                            }
                        }
                        if (PintApprovalPermission == (int)ApprovePermission.Approve || PintApprovalPermission == (int)ApprovePermission.Both)
                            btnApprove.Visible = btnReject.Visible = true;
                    }
                    btnExpense.Visible = true;
                    btnDocuments.Visible = true;
                    SetEnableDisable(false);
                }
                else if (PintInvoiceID != 0)
                {
                    btnActions.Enabled = true;
                    btnApprove.Visible = false;
                    btnReject.Visible = false;
                    bnSubmitForApproval.Visible = false;
                    tiSuggestions.Visible = btnSuggest.Visible = btnDeny.Visible = false;

                    btnExpense.Visible = true;
                    SetEnableDisable(false);
                }

                lblOrderNumber.Text = "Order Number";


                txtAdvPayment.Visible = true;
                lblAdvancePayment.Visible = true;
                lblAdvancePayment.BringToFront();
            }

        }

        private void ArrangeControls()
        {
            if (MintFromForm == 4)
            {
                lblCurrency.Left = lblCompany.Left;
                lblCurrency.Top = lblCompany.Top + lblCompany.Height + 13;

                cboCurrency.Left = cboCompany.Left;
                cboCurrency.Top = cboCompany.Top + cboCompany.Height + 6;
            }
        }

        private void LoadMessage()
        {
            switch (MintFromForm)
            {
                case 1://Sales Quotation
                    MaMessageArr = new DataTable();
                    MaStatusMessage = new DataTable();
                    MaMessageArr = mObjNotification.FillMessageArray(28, ClsCommonSettings.ProductID);
                    break;
                case 2://Sales Order
                    MaMessageArr = new DataTable();
                    MaStatusMessage = new DataTable();
                    MaMessageArr = mObjNotification.FillMessageArray(28, ClsCommonSettings.ProductID);
                    break;
                case 3://Sales Invoice
                    MaMessageArr = new DataTable();
                    MaStatusMessage = new DataTable();
                    MaMessageArr = mObjNotification.FillMessageArray(28, ClsCommonSettings.ProductID);
                    break;
                case 4:
                    MaMessageArr = new DataTable();
                    MaStatusMessage = new DataTable();
                    MaMessageArr = mObjNotification.FillMessageArray(28, ClsCommonSettings.ProductID);
                    break;
                case 5:
                    MaMessageArr = new DataTable();
                    MaStatusMessage = new DataTable();
                    MaMessageArr = mObjNotification.FillMessageArray(28, ClsCommonSettings.ProductID);
                    break;
            }
        }

        private bool LoadCombos(int intFillType)
        {
            try
            {
                DataTable datCombos = new DataTable();
                DataTable dtControlsPermissions = new DataTable();

                //if (MintFromForm == 1)
                //    dtControlsPermissions = MobjClsBLLPermissionSettings.GetControlsPermissions(ClsCommonSettings.RoleID, (Int32)MenuID.SalesQuotation, ClsCommonSettings.CompanyID);
                //else if (MintFromForm == 2)
                //    dtControlsPermissions = MobjClsBLLPermissionSettings.GetControlsPermissions(ClsCommonSettings.RoleID, (Int32)MenuID.SalesOrder, ClsCommonSettings.CompanyID);
                //else if (MintFromForm == 3)
                //    dtControlsPermissions = MobjClsBLLPermissionSettings.GetControlsPermissions(ClsCommonSettings.RoleID, (Int32)MenuID.SalesInvoice, ClsCommonSettings.CompanyID);
                //else if (MintFromForm == 4)
                //    dtControlsPermissions = MobjClsBLLPermissionSettings.GetControlsPermissions(ClsCommonSettings.RoleID, (Int32)MenuID.CreditNote, ClsCommonSettings.CompanyID);
                //else
                //    dtControlsPermissions = MobjClsBLLPermissionSettings.GetControlsPermissions(ClsCommonSettings.RoleID, (Int32)MenuID.POS, ClsCommonSettings.CompanyID);

                //MblnCancelPermission = false;

                //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                //    MblnCancelPermission = true;
                //else
                //{
                //    dtControlsPermissions.DefaultView.RowFilter = "ControlName = 'BindingNavigatorCancelItem'";
                //    if (dtControlsPermissions.DefaultView.ToTable().Rows.Count > 0)
                //        MblnCancelPermission = Convert.ToBoolean(dtControlsPermissions.DefaultView.ToTable().Rows[0]["IsVisible"]);
                //}

                if (intFillType == 0 || intFillType == 1)
                {
                    datCombos = null;
                    //  if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", " CompanyID= " + ClsCommonSettings.LoginCompanyID });
                    //else
                    //{
                    //    try
                    //    {
                    //        datCombos = MobjclsBLLSTSales.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, MintTempCompany);
                    //    }
                    //    catch { }
                    //}
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;

                    datCombos = null;
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", " CompanyID= " + ClsCommonSettings.LoginCompanyID });
    
                    cboSCompany.ValueMember = "CompanyID";
                    cboSCompany.DisplayMember = "CompanyName";
                    cboSCompany.DataSource = datCombos;
                    cboCompany.SelectedValue = cboSCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;
                }
                if (intFillType == 0 || intFillType == 2)
                {
                    datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "distinct V.VendorID,V.VendorName", "InvVendorInformation V INNER JOIN InvVendorCompanyDetails VC ON V.VendorID = VC.VendorID ", "VendorTypeID=" + (int)VendorType.Customer + " AND VC.CompanyID = " + ClsCommonSettings.CompanyID + " And StatusID = " + (int)VendorStatus.CustomerActive });
                    cboCustomer.ValueMember = "VendorID";
                    cboCustomer.DisplayMember = "VendorName";
                    cboCustomer.DataSource = datCombos;
                }
                if (intFillType == 0 || intFillType == 3)
                {
                    datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "PaymentTermsID,TermsName", "InvPaymentTerms", "" });
                    cboPaymentTerms.ValueMember = "PaymentTermsID";
                    cboPaymentTerms.DisplayMember = "TermsName";
                    cboPaymentTerms.DataSource = datCombos;
                }
                //if (intFillType == 0 || intFillType == 5)
                //{

                //    if (MblnIsEditable)
                //    {
                //        decimal decMaxSlab = 0;
                //        datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "IsNull(Max(MinSlab),0) as MinSlab", "InvDiscountReference", "DiscountMode in ('N') And IsSaleType = 'True' And IsActive = 1 and IsDiscountForAmount=1 And (IsPeriodApplicable = 0 or (IsPeriodApplicable = 1 And '" + dtpOrderDate.Value.ToString("dd-MMM-yyyy") + "' >= convert(datetime,convert(varchar,PeriodFrom,106)) And '" + dtpOrderDate.Value.ToString("dd-MMM-yyyy") + "' <= convert(datetime,convert(varchar,PeriodTo,106)))) And (IsSlabApplicable = 0 Or (IsSlabApplicable = 1 And " + txtSubtotal.Text.ToDecimal() + " >= MinSlab)) And CurrencyID = " + cboCurrency.SelectedValue.ToInt32() + " " });
                //        if (datCombos.Rows.Count > 0)
                //            decMaxSlab = datCombos.Rows[0]["MinSlab"].ToDecimal();
                //        datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "DiscountID,DiscountShortName", "InvDiscountReference", "(DiscountMode in ('N') And IsSaleType = 'True' And IsActive = 1 and IsDiscountForAmount=1 And (IsPeriodApplicable = 0 or (IsPeriodApplicable = 1 And '" + dtpOrderDate.Value.ToString("dd-MMM-yyyy") + "' >= convert(datetime,convert(varchar,PeriodFrom,106)) And '" + dtpOrderDate.Value.ToString("dd-MMM-yyyy") + "' <= convert(datetime,convert(varchar,PeriodTo,106)))) And (IsSlabApplicable = 0 Or (IsSlabApplicable = 1 And " + txtSubtotal.Text.ToDecimal() + " >= MinSlab) And MinSlab >= " + decMaxSlab + " And CurrencyID = " + cboCurrency.SelectedValue.ToInt32() + ") And case PercentOrAmount When 0 Then CurrencyID Else " + cboCurrency.SelectedValue.ToInt32() + " End = " + cboCurrency.SelectedValue.ToInt32() + ") Or (IsSaleType = 1 And IsPredefined = 1)", "DiscountID", "DiscountShortName" });
                //    }
                //    else
                //    {
                //        datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "DiscountID,DiscountShortName", "InvDiscountReference", "(DiscountMode in ('N') And IsSaleType = 'True' And IsActive = 1 and IsDiscountForAmount=1)  Or (IsSaleType = 1 And IsPredefined = 1) ", "DiscountID", "DiscountShortName" });
                //    }

                //    cboDiscount.ValueMember = "DiscountID";
                //    cboDiscount.DisplayMember = "DiscountShortName";

                //    DataTable datCustomerWiseDiscounts = new DataTable();
                //    if (MblnIsEditable)
                //    {
                //        datCustomerWiseDiscounts = MobjclsBLLSTSales.GetCustomerWiseDiscounts(cboCustomer.SelectedValue.ToInt32(), txtSubtotal.Text.ToDecimal(), dtpOrderDate.Value.ToString("dd-MMM-yyyy"), cboCurrency.SelectedValue.ToInt32());
                //    }
                //    else
                //    {
                //        datCustomerWiseDiscounts = MobjclsBLLSTSales.GetCustomerWiseDiscounts(cboCustomer.SelectedValue.ToInt32(), txtSubtotal.Text.ToDecimal(), "", cboCurrency.SelectedValue.ToInt32());
                //    }
                //    foreach (DataRow drNew in datCustomerWiseDiscounts.Rows)
                //    {
                //        DataRow drNew1 = datCombos.NewRow();
                //        drNew1["DiscountID"] = drNew["DiscountID"];
                //        drNew1["DiscountShortName"] = drNew["DiscountShortName"];
                //        datCombos.Rows.Add(drNew1);
                //    }
                //    DataRow dr = datCombos.NewRow();
                //    dr["DiscountID"] = 0;
                //    dr["DiscountShortName"] = "No Discount";
                //    datCombos.Rows.InsertAt(dr, 0);
                //    int intTag = cboDiscount.SelectedValue.ToInt32();
                //    string strDiscountAmount = txtDiscount.Text;
                //    cboDiscount.DataSource = datCombos;
                //    cboDiscount.SelectedValue = intTag;

                //    if (intTag == (int)PredefinedDiscounts.DefaultSalesDiscount)
                //        txtDiscount.Text = strDiscountAmount;
                //}
                if (intFillType == 0 || intFillType == 6)
                {
                    datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "distinct CR.CurrencyID,CR.CurrencyName", "CurrencyReference CR INNER JOIN CurrencyDetails CD ON CR.CurrencyID=CD.CurrencyID", "CD.CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) });
                    cboCurrency.ValueMember = "CurrencyID";
                    cboCurrency.DisplayMember = "CurrencyName";
                    cboCurrency.DataSource = datCombos;
                }
                if (intFillType == 0 || intFillType == 12)
                {
                    datCombos = null;
                    datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "AccountID,AccountName", "AccAccountMaster", "AccountGroupID=33" }); // For Tax Scheme
                    DataRow dr = datCombos.NewRow();
                    dr[0] = 0;
                    dr[1] = "None";
                    datCombos.Rows.InsertAt(dr, 0);
                    cboTaxScheme.ValueMember = "AccountID";
                    cboTaxScheme.DisplayMember = "AccountName";
                    cboTaxScheme.DataSource = datCombos;
                }
                if (intFillType == 0 || intFillType == 7)
                {
                    if (MintFromForm == 1)
                    {
                        datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "OperationTypeID=" + (int)OperationType.SalesQuotation });
                        cboSStatus.ValueMember = "StatusID";
                        cboSStatus.DisplayMember = "Status";
                        DataTable dat = datCombos.Copy();
                        DataRow dr = dat.NewRow();
                        dr["StatusID"] = -2;
                        dr["Status"] = "ALL";
                        dat.Rows.InsertAt(dr, 0);
                        cboSStatus.DataSource = dat;

                        datCombos = null;
                        //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                        //{
                        datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "OrderTypeID,OrderType", "CommonOrderTypeReference", "OperationTypeID=" + (int)OperationType.SalesQuotation });
                        //}
                        //else
                        //{
                        //try
                        //{
                        //    datCombos = MobjclsBLLSTSales.GetOperationTypeByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.SalOrdType);
                        //}
                        //catch { }
                        //}
                        cboOrderType.ValueMember = "OrderTypeID";
                        cboOrderType.DisplayMember = "OrderType";
                        
                        cboOrderType.DataSource = datCombos;

                    }
                    else if (MintFromForm == 2)
                    {
                        datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "OperationTypeID=" + (int)OperationType.SalesOrder });
                        cboSStatus.ValueMember = "StatusID";
                        cboSStatus.DisplayMember = "Status";

                        DataTable dat = datCombos.Copy();
                        DataRow dr = dat.NewRow();
                        dr["StatusID"] = -2;
                        dr["Status"] = "ALL";
                        dat.Rows.InsertAt(dr, 0);
                        cboSStatus.DataSource = dat;

                        datCombos = null;
                        //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                        //{
                        datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "OrderTypeID,OrderType", "CommonOrderTypeReference", "OperationTypeID=" + (int)OperationType.SalesOrder });
                        //}
                        //else
                        //{
                        //    try
                        //    {
                        //        datCombos = MobjclsBLLSTSales.GetOperationTypeByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.SalOrdType);
                        //    }
                        //    catch { }
                        //}
                        cboOrderType.ValueMember = "OrderTypeID";
                        cboOrderType.DisplayMember = "OrderType";
                        DataTable dt = MobjclsBLLSTSales.FillCombos(new string[] { "ConfigurationID,ConfigurationItem", "ConfigurationMaster", "ConfigurationItem='" + ConfigurationItem.SalesproEnabled + "' and ConfigurationValue = 'YES'" });
                        if (dt.Rows.Count > 0)
                        {
                            DataRow drOrderType = datCombos.NewRow();
                            drOrderType["OrderTypeID"] = -1;
                            drOrderType["OrderType"] = "From SalesPro";
                            datCombos.Rows.Add(drOrderType);
                        }
                        cboOrderType.DataSource = datCombos;
                    }
                    else if (MintFromForm == 3)
                    {
                        //datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "OperationTypeID="+(int)OperationType.SalesInvoice+"" });
                        //cboSStatus.ValueMember = "StatusID";
                        //cboSStatus.DisplayMember = "Status";

                        //DataTable dat = datCombos.Copy();
                        //DataRow dr = dat.NewRow();
                        //dr["StatusID"] = -2;
                        //dr["Status"] = "ALL";
                        //dat.Rows.InsertAt(dr, 0);
                        //cboSStatus.DataSource = dat;
                        //datCombos = null;
                        datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "OrderTypeID,OrderType", "CommonOrderTypeReference", "OperationTypeID=" + (int)OperationType.SalesInvoice });

                        cboOrderType.ValueMember = "OrderTypeID";
                        cboOrderType.DisplayMember = "OrderType";
                        cboOrderType.DataSource = datCombos;
                        //cboOrderType.SelectedValue = (int)OperationOrderType.SalesInvoiceDirect;

                    }
                }
                if (intFillType == 0 || intFillType == 8)
                {

                }
                if (intFillType == 0 || intFillType == 9)
                {
                    datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", "VendorTypeID=" + (int)VendorType.Customer + "" });
                    DataRow dr = datCombos.NewRow();
                    dr["VendorID"] = -2;
                    dr["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(dr, 0);
                    cboSCustomer.ValueMember = "VendorID";
                    cboSCustomer.DisplayMember = "VendorName";
                    cboSCustomer.DataSource = datCombos;
                }
                if (intFillType == 10)
                {
                    datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "AccountID,AccountName ", "AccAccountMaster", "AccountGroupID=" + (int)AccountGroups.SalesAccounts + "" });

                    cboAccount.ValueMember = "AccountID";
                    cboAccount.DisplayMember = "AccountName";
                    cboAccount.DataSource = datCombos;
                }
                if (intFillType == 0 || intFillType == 11)
                {
                    datCombos = null;
                    datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "AccountID,AccountName", "AccAccountMaster", "AccountGroupID=33" }); // For Tax Scheme
                    DataRow dr = datCombos.NewRow();
                    dr[0] = 0;
                    dr[1] = "None";
                    datCombos.Rows.InsertAt(dr, 0);
                    cboTaxScheme.ValueMember = "AccountID";
                    cboTaxScheme.DisplayMember = "AccountName";
                    cboTaxScheme.DataSource = datCombos;
                }


                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadCombos() " + ex.Message);
                MobjLogs.WriteLog("Error in LoadCombos() " + ex.Message, 2);
            }
            return false;
        }

        private bool LoadOrderNumbers()
        {
            try
            {
                DataTable datCombos = new DataTable();
                string strCondition = "";

                if (MintFromForm == 2)
                {
                    if (MblnAddStatus)
                    {
                        if ((int)cboOrderType.SelectedValue == (int)OperationOrderType.SOTFromQuotation)
                        {

                            if (ClsCommonSettings.SQApproval)
                                strCondition = "StatusID = " + (int)OperationStatusType.SQuotationApproved + " AND CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue);
                            else

                                strCondition = "StatusID = " + (int)OperationStatusType.SQuotationSubmitted + " AND CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue);


                            datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "SalesQuotationID,SalesQuotationNo", strTableSalesQuotation, strCondition });
                            cboOrderNumbers.ValueMember = "SalesQuotationID";
                            cboOrderNumbers.DisplayMember = "SalesQuotationNo";
                            cboOrderNumbers.DataSource = datCombos;
                        }


                        if ((int)cboOrderType.SelectedValue == (int)OperationOrderType.SOTFromDeliveryNote)
                        {
                            datCombos = MobjclsBLLSTSales.FillDeliveryNote(cboCompany.SelectedValue.ToInt64(), cboCustomer.SelectedValue.ToInt64(), (Int64)OperationOrderType.DNOTDirect);
                            ((ListBox)cblSaleQuotationNo).DataSource = datCombos;
                            ((ListBox)cblSaleQuotationNo).ValueMember = "ItemIssueID";
                            ((ListBox)cblSaleQuotationNo).DisplayMember = "ItemIssueNo";
                        }
                        if ((int)cboOrderType.SelectedValue == (int)OperationOrderType.SOTFromSalesPro)
                        {
                            datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "QuoteId as SalesQuotationID,QuoteNo as SalesQuotationNo", "QuoteMaster", "Status = 0 order by QuoteNo" });
                            cboOrderNumbers.ValueMember = "SalesQuotationID";
                            cboOrderNumbers.DisplayMember = "SalesQuotationNo";
                            cboOrderNumbers.DataSource = datCombos;
                            
                        }
                    }
                    else
                    {
                        cboOrderNumbers.ValueMember = "SalesQuotationID";
                        cboOrderNumbers.DisplayMember = "SalesQuotationNo";
                        cboOrderNumbers.DataSource = MobjclsBLLSTSales.FillCombos(new string[] { "SalesQuotationID,SalesQuotationNo", "InvSalesQuotationMaster", "" });
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadOrderNumbers() " + ex.Message);
                MobjLogs.WriteLog("Error in LoadOrderNumbers() " + ex.Message, 2);
            }
            return false;
        }

        private DataTable FillDataGridCombo(int intItemID, int intVendorID, bool blnIsGroup)
        {
            try
            {
                DataTable datCombos = new DataTable();
                if (!blnIsGroup)
                {
                    datCombos = MobjclsBLLSTSales.GetItemWiseUOMs(intItemID);
                    Uom.ValueMember = "UOMID";
                    Uom.DisplayMember = "UOM";
                    Uom.DataSource = datCombos;
                }
                else
                {
                    datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "UOMID,Code as UOM", "InvUOMReference", "UOMID = " + (int)PredefinedUOMs.Numbers });
                    Uom.ValueMember = "UOMID";
                    Uom.DisplayMember = "UOM";
                    Uom.DataSource = datCombos;
                }

                return datCombos;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FillDataGridCombo() " + ex.Message);
                MobjLogs.WriteLog("Error in FillDataGridCombo() " + ex.Message, 2);
            }
            return null;
        }

        private DataTable FillDiscountColumn(int intRowIndex, bool blnIgnoreDate)
        {
            decimal decQty, decRate = 0;
            int intItemID = 0;
            intItemID = dgvSales[ItemID.Index, intRowIndex].Value.ToInt32();
            int intDiscountID = dgvSales[Discount.Index, intRowIndex].Tag.ToInt32();

            decQty = dgvSales[Quantity.Index, intRowIndex].Value.ToDecimal();
            DataTable datUomConversions = MobjclsBLLSTSales.GetUomConversionValues(dgvSales["Uom", intRowIndex].Tag.ToInt32(), intItemID);
            if (datUomConversions.Rows.Count > 0)
            {
                if (datUomConversions.Rows[0]["ConvertionFactorID"].ToInt32() == 2)
                    decQty = decQty * datUomConversions.Rows[0]["ConvertionValue"].ToDecimal();
                else
                    decQty = decQty / datUomConversions.Rows[0]["ConvertionValue"].ToDecimal();
            }
            decRate = dgvSales[Rate.Index, intRowIndex].Value.ToDecimal() * dgvSales[Quantity.Index, intRowIndex].Value.ToDecimal();


            DataTable dtItemCustomerDiscounts = new DataTable();
            if (blnIgnoreDate)
                dtItemCustomerDiscounts = MobjclsBLLSTSales.GetItemAndCustomerWiseDiscounts(intItemID, cboCustomer.SelectedValue.ToInt32(), decQty, decRate, "", cboCurrency.SelectedValue.ToInt32());
            else
                dtItemCustomerDiscounts = MobjclsBLLSTSales.GetItemAndCustomerWiseDiscounts(intItemID, cboCustomer.SelectedValue.ToInt32(), decQty, decRate, dtpOrderDate.Value.ToString("dd-MMM-yyyy"), cboCurrency.SelectedValue.ToInt32());
            Discount.DataSource = null;
            Discount.ValueMember = "DiscountID";
            Discount.DisplayMember = "DiscountShortName";
            DataRow dr = dtItemCustomerDiscounts.NewRow();
            dr["DiscountID"] = 0;
            dr["DiscountShortName"] = "No Discount";
            dtItemCustomerDiscounts.Rows.InsertAt(dr, 0);
            Discount.DataSource = dtItemCustomerDiscounts;

            dgvSales[Discount.Index, intRowIndex].Value = intDiscountID;
            dgvSales[Discount.Index, intRowIndex].Tag = intDiscountID;
            dgvSales[Discount.Index, intRowIndex].Value = dgvSales[Discount.Index, intRowIndex].FormattedValue;
            if (string.IsNullOrEmpty(dgvSales[Discount.Index, intRowIndex].Value.ToString()))
            {
                dgvSales[Discount.Index, intRowIndex].Value = 0;
                dgvSales[Discount.Index, intRowIndex].Tag = 0;
                dgvSales[Discount.Index, intRowIndex].Value = dgvSales[Discount.Index, intRowIndex].FormattedValue;
            }
            return dtItemCustomerDiscounts;
        }

        private void SetPermissions()
        {
            try
            {
                // Function for setting permissions
                if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    if (MintFromForm == 1)
                        MobjClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.SalesQuotation, out MblnViewPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                    else if (MintFromForm == 2)
                        MobjClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.SalesOrder, out MblnViewPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                    else if (MintFromForm == 3)
                        MobjClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.SalesInvoice, out MblnViewPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                    if (MblnAddPermission == true || MblnUpdatePermission == true)
                        MblnAddUpdatePermission = true;

                    if (MblnAddPermission == true || MblnUpdatePermission == true || MblnDeletePermission == true)
                        MblnViewPermission = true;
                }
                else
                {
                    MblnAddPermission = MblnAddUpdatePermission = MblnCancelPermission = MblnViewPermission = MblnUpdatePermission = MblnDeletePermission = true;
                }

                MblnCancelPermission = MblnUpdatePermission;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SetPermissions() " + ex.Message);
                MobjLogs.WriteLog("Error in SetPermissions() " + ex.Message, 2);
            }
        }

        private void EnableDisableButtons(bool bEnable)
        {
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;

            if (bEnable)
            {
                BindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
                BindingNavigatorClearItem.Enabled = !bEnable;
                MblnChangeStatus = !bEnable;
            }
            else
            {
                BindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
                BindingNavigatorClearItem.Enabled = bEnable;
                MblnChangeStatus = bEnable;
            }
        }

        private void ClearControls()
        {
            //MlngDeliveryID = 0;
            MobjclsBLLSTSales.clsDTOSTSalesMaster.decExpenseAmount = 0;
            MblnIsFromClear = true;
            MblnSearchFlag = false;
            MblnIsEditable = true;
            MintExchangeCurrencyScale = 2;
            MintBaseCurrencyScale = 2;
            ErrSales.Clear();
            dgvSales.Rows.Clear();
            lblSalesOrderstatus.Text = "";
            dgvSales.ClearSelection();
            UncheckedCheckListBox();
            cboSCompany.SelectedIndex = -1;
            cboSExecutive.SelectedIndex = -1;
            cboSCustomer.SelectedIndex = -1;
            cboSStatus.SelectedIndex = -1;
            cblSaleQuotationNo.Enabled = true;
            cboOrderType.Tag = " ";
            cboCompany.Enabled = cboOrderNumbers.Enabled = cboOrderType.Enabled=cboTaxScheme.Enabled = true;
            cboCustomer.SelectedIndex = -1;
            cboTaxScheme.SelectedValue = 0;
            cboSCompany.Text = cboSExecutive.Text = cboSCustomer.Text = cboSStatus.Text = cboCustomer.Text = "";
            if (MintFromForm != 3)
            {
                cboPaymentTerms.Enabled = true;
                cboCustomer.Enabled = true;
                btnAddressChange.Enabled = true;
                txtOrderNo.Enabled = cboOrderType.Enabled = cboCompany.Enabled = cboOrderNumbers.Enabled = true;
            }
            else
            {
                cboPaymentTerms.Enabled = false;
                cboCustomer.Enabled = true;
                btnAddressChange.Enabled = false;
            }
            EnableDisableButtons(false);
            cboPaymentTerms.SelectedValue = (int)PaymentTerms.Cash;
            cboDiscount.SelectedIndex = -1;
            cboDiscount.Text = "";
            dtpDueDate.Value = dtpOrderDate.Value = ClsCommonSettings.GetServerDate();
            txtLpoNo.Text = "";
            switch (MintFromForm)
            {
                case 2:
                    cboOrderType.SelectedValue = (int)OperationOrderType.SOTDirect;
                    break;
                case 3:
                    cboOrderType.SelectedValue = (int)OperationOrderType.SalesInvoiceDirect;
                    break;
            }

            MobjclsBLLSTSales.clsDTOSTSalesMaster.blnCancelled = false;
            txtRemarks.Text = txtVendorAddress.Text = txtDescription.Text = cboOrderNumbers.Text = "";
            txtExchangeCurrencyRate.Text = txtSubtotal.Text = txtDiscount.Text=txtTaxAmount.Text= txtExpenseAmount.Text = txtTotalAmount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            txtAdvPayment.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            BindingNavigatorCancelItem.Text = "Cancel";
            lblStatus.Text = "New";
            lblStatus.ForeColor = Color.Black;

            btnApprove.Visible = btnReject.Visible = bnSubmitForApproval.Visible = false;
            btnSuggest.Visible = btnDeny.Visible = tiSuggestions.Visible = false;
            cboCompany.Focus();
            MblnIsFromClear = false;
        }

        private void GenerateOrderNo()
        {
            try
            {
                Int64 intOrderNo = 0;
                Int64 intOrderCount = 0;

                switch (MintFromForm)
                {
                    case 1://Sales Quotation
                        intOrderNo = MobjclsBLLSTSales.GetLastQuotationNo(ref intOrderCount, cboCompany.SelectedValue.ToInt32());
                        if (ClsCommonSettings.blnSQAutogenerate)
                            txtOrderNo.ReadOnly = false;
                        else
                            txtOrderNo.ReadOnly = true;
                        txtOrderNo.Text = GetSalesPrefix() + intOrderNo;
                        break;
                    case 2://Sales Order
                        intOrderNo = MobjclsBLLSTSales.GetLastOrderNo(ref intOrderCount, cboCompany.SelectedValue.ToInt32());
                        if (ClsCommonSettings.blnSOAutogenerate)
                            txtOrderNo.ReadOnly = false;
                        else
                            txtOrderNo.ReadOnly = true;
                        txtOrderNo.Text = GetSalesPrefix() + intOrderNo;
                        break;
                    case 3: //Sales Invoice
                        intOrderNo = MobjclsBLLSTSales.GetLastInvoiceNo(ref intOrderCount, cboCompany.SelectedValue.ToInt32());
                        if (ClsCommonSettings.blnSIAutogenerate)
                            txtOrderNo.ReadOnly = false;
                        else
                            txtOrderNo.ReadOnly = true;
                        txtOrderNo.Text = GetSalesPrefix() + intOrderNo;
                        break;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in GenerateOrderNo() " + ex.Message);
                MobjLogs.WriteLog("Error in GenerateOrderNo() " + ex.Message, 2);
            }
        }

        private void AddNew()
        {
            MobjclsBLLSTSales = new clsBLLSTSalesOrder();
            MblnAddStatus = true;
            lblCreatedByText.Text = "Created By : " + ClsCommonSettings.strEmployeeName;
            lblCreatedDateValue.Text = "Created Date : " + ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
            txtDescription.Visible = lblDescription.Visible = false;
            SetEnableDisable(true);
            btnAddExpense.Enabled = false;
            try
            {
                switch (MintFromForm)
                {
                    case 1:
                        MstrMessageCommon = "Add new Sales Quotation";
                        btnActions.Enabled = false;
                        btnAddExpense.Enabled = false;
                        break;
                    case 2:
                        MstrMessageCommon = "Add new Sales Order";
                        if (cboOrderType.SelectedValue == null)
                        {
                            DataTable datTemp = MobjclsBLLSTSales.FillCombos(new string[] { "*", "CommonOrderTypeReference", "OrderTypeID=" + (int)OperationOrderType.SOTDirect });
                            if (datTemp.Rows.Count > 0)
                                cboOrderType.Text = datTemp.Rows[0]["OrderType"].ToString();
                        }
                        cboDiscount.Enabled = true;
                        btnActions.Enabled = false;
                        btnAddExpense.Enabled = false;
                        break;
                    case 3:
                        MstrMessageCommon = "Add new Sales Invoice";
                        if (cboOrderType.SelectedValue == null)
                        {
                            DataTable datTemp = MobjclsBLLSTSales.FillCombos(new string[] { "*", "CommonOrderTypeReference", "OrderTypeID=" + (int)OperationOrderType.SalesInvoiceDirect });
                            if (datTemp.Rows.Count > 0)
                                cboOrderType.Text = datTemp.Rows[0]["OrderType"].ToString();
                        }
                        cboOrderType.SelectedValue = (int)OperationOrderType.SalesInvoiceDirect;

                        btnActions.Enabled = false;
                        btnAddExpense.Enabled = false;
                        break;
                }
                ClearControls();
                GenerateOrderNo();
                DisplaySalesNumbersInSearchGrid();
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrFocus.Enabled = true;
                ErrSales.Clear();
                MblnChangeStatus = false;
                BtnPrint.Enabled = false;
                BtnEmail.Enabled = false;
                BindingNavigatorAddNewItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorClearItem.Enabled = false;
                BindingNavigatorCancelItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                SetOrderTypeComboView();
                cboCompany_SelectedIndexChanged(null, null);
                MblnQtyChanged = false;
                MblnChangeStatus = false;
                cboCompany.Focus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in AddNew() " + ex.Message);
                MobjLogs.WriteLog("Error in AddNew() " + ex.Message, 2);
            }
        }

        private bool DisplaySalesNumbersInSearchGrid()
        {
            try
            {
                DataTable dtSales = new DataTable();
                DataTable dtTemp = null;
                string strFilterCondition = string.Empty;

                if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                    strFilterCondition += " CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue);
                else
                {
                    dtTemp = null;
                    dtTemp = (DataTable)cboSCompany.DataSource;
                    if (dtTemp.Rows.Count > 0)
                    {
                        strFilterCondition += "CompanyID In (";
                        foreach (DataRow dr in dtTemp.Rows)
                        {
                            strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                        }
                        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                        strFilterCondition += ")";
                    }
                }
                if (cboSCustomer.SelectedValue != null && Convert.ToInt32(cboSCustomer.SelectedValue) != -2)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "VendorID = " + Convert.ToInt32(cboSCustomer.SelectedValue);
                }
                if (cboSExecutive.SelectedValue != null && Convert.ToInt32(cboSExecutive.SelectedValue) != -2)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "CreatedBy = " + Convert.ToInt32(cboSExecutive.SelectedValue);
                }
                //else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                //{
                //    dtTemp = null;
                //    dtTemp = (DataTable)cboSExecutive.DataSource;
                //    if (dtTemp.Rows.Count > 0)
                //    {
                //        //DataTable datTempPer = MobjClsBLLPermissionSettings.GetControlPermissions1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, MintTempEmployee);
                //        //if (datTempPer != null)
                //        //{
                //        //    if (datTempPer.Rows.Count > 0)
                //        //    {
                //        //        if (datTempPer.Rows[0]["IsVisible"].ToString() == "True")
                //        //            strFilterCondition += "And EmployeeID In (0,";
                //        //        else
                //        //            strFilterCondition += "And EmployeeID In (";
                //        //    }
                //        //    else
                //        //        strFilterCondition += "And EmployeeID In (";
                //        //}
                //        //else
                //        if (!string.IsNullOrEmpty(strFilterCondition))
                //            strFilterCondition += " And ";
                //        strFilterCondition += " CreatedBy In (";
                //        foreach (DataRow dr in dtTemp.Rows)
                //        {
                //            strFilterCondition += Convert.ToInt32(dr["UserID"]) + ",";
                //        }
                //        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                //        strFilterCondition += ")";
                //    }
                //    else
                //    {
                //        if (!string.IsNullOrEmpty(strFilterCondition))
                //            strFilterCondition += " And ";
                //        strFilterCondition += " CreatedBy = -1";
                //    }
                //}
                if (cboSStatus.SelectedValue != null && Convert.ToInt32(cboSStatus.SelectedValue) != -2)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "StatusID = " + Convert.ToInt32(cboSStatus.SelectedValue);
                }

                if (!string.IsNullOrEmpty(strFilterCondition))
                    strFilterCondition += " And ";
                strFilterCondition += " OrderDate >= '" + dtpSFrom.Value.ToString("dd-MMM-yyyy") + "' And OrderDate <= '" + dtpSTo.Value.ToString("dd-MMM-yyyy") + "'";

                dgvSalesDisplay.DataSource = null;
                switch (MintFromForm)
                {
                    case 1:
                        if (!string.IsNullOrEmpty(lblSSalesNo.Text.Trim()))
                        {
                            strFilterCondition = " QuotationNo = '" + lblSSalesNo.Text.Trim() + "'";
                        }
                        dtSales = MobjclsBLLSTSales.GetQuotationNumbers();
                        dtSales.DefaultView.RowFilter = strFilterCondition;
                        dgvSalesDisplay.DataSource = dtSales.DefaultView.ToTable();
                        LblSCountStatus.Text = "Total Quotations : " + dgvSalesDisplay.Rows.Count;
                        break;
                    case 2:
                        if (!string.IsNullOrEmpty(lblSSalesNo.Text.Trim()))
                        {
                            strFilterCondition = " OrderNo = '" + lblSSalesNo.Text.Trim() + "'";
                        }
                        dtSales = MobjclsBLLSTSales.GetOrderNumbers();
                        dtSales.DefaultView.RowFilter = strFilterCondition;
                        dgvSalesDisplay.DataSource = dtSales.DefaultView.ToTable();
                        LblSCountStatus.Text = "Total Orders : " + dgvSalesDisplay.Rows.Count;
                        break;
                    case 3:
                        if (!string.IsNullOrEmpty(lblSSalesNo.Text.Trim()))
                        {
                            strFilterCondition = " InvoiceNo = '" + lblSSalesNo.Text.Trim() + "'";
                        }
                        dtSales = MobjclsBLLSTSales.GetInvoiceNumbers();
                        dtSales.DefaultView.RowFilter = strFilterCondition;
                        dgvSalesDisplay.DataSource = dtSales.DefaultView.ToTable();
                        LblSCountStatus.Text = "Total Invoices : " + dgvSalesDisplay.Rows.Count;
                        break;
                }

                dgvSalesDisplay.Columns[0].Visible = false;
                dgvSalesDisplay.Columns[2].Visible = false;
                dgvSalesDisplay.Columns[3].Visible = false;
                dgvSalesDisplay.Columns[4].Visible = false;
                dgvSalesDisplay.Columns[5].Visible = false;
                dgvSalesDisplay.Columns[6].Visible = false;
                dgvSalesDisplay.Columns[7].Visible = false;
                dgvSalesDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplaySalesNumbersInSearchGrid() " + ex.Message);
                MobjLogs.WriteLog("Error in DisplaySalesNumbersInSearchGrid() " + ex.Message, 2);
            }
            return false;
        }

        private void Changestatus()
        {
            if (MblnIsValueChanged == false) return;
            MblnChangeStatus = true;

            if (MblnAddStatus && MintSalesID == 0 && PintInvoiceID == 0)
            {
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
            else
            {
                if ((MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SQuotationOpen || MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderOpen || MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SInvoiceOpen || MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SQuotationRejected || MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderRejected) && MintSalesID == 0 && PintInvoiceID == 0)
                    BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                else
                    BindingNavigatorSaveItem.Enabled = false;
            }
            BindingNavigatorClearItem.Enabled = true;
            ErrSales.Clear();
        }

        private bool DisplayInformation(Int64 intSalesID)
        {
            try
            {
                MblnAddStatus = false;
                MblnSearchFlag = true;

                ClearControls();
                DisplaySalesDetails(MintFromForm, intSalesID);
                MblnSearchFlag = false;
                BtnPrint.Enabled = MblnViewPermission;
                BtnEmail.Enabled = MblnViewPermission;
                cboOrderNumbers.Enabled = cboOrderType.Enabled = cboCompany.Enabled = false;
                MblnQtyChanged = false;
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayInformation() " + ex.Message);
                MobjLogs.WriteLog("Error in DisplayInformation() " + ex.Message, 2);
            }
            return false;
        }

        private bool DisplayAddress()
        {
            try
            {
                txtVendorAddress.Text = MobjclsBLLSTSales.GetVendorAddressInformation(MintVendorAddID);
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayAddress() " + ex.Message);
                MobjLogs.WriteLog("Error in DisplayAddress() " + ex.Message, 2);
            }
            return false;
        }

        private void CalculateNetTotal()
        {
            try
            {
                decimal decGrandAmount = 0, decExpenseAmount = 0, decDiscountAmount = 0, decDiscAmount = 0, decNetAmount = 0;
                decimal dTaxAmount = 0, taxValue = 0;
                decGrandAmount = txtSubtotal.Text.ToDecimal();
                decExpenseAmount = txtExpenseAmount.Text.ToDecimal();
                decDiscountAmount = txtDiscount.Text.ToDecimal();

                if (cboDiscount.SelectedIndex.ToInt32() >= 0)
                {
                    if (Convert.ToString(cboDiscount.SelectedItem) == "%")
                        decDiscAmount = decGrandAmount * (decDiscountAmount / 100);
                    else
                        decDiscAmount = decDiscountAmount;
                }


                if (cboTaxScheme.SelectedValue.ToInt32() > 0)
                {
                    taxValue = MobjclsBLLSTSales.getTaxValue(cboTaxScheme.SelectedValue.ToInt32());
                    dTaxAmount = (((decGrandAmount - decDiscountAmount) * taxValue) / 100).ToDecimal();
                }

                decNetAmount = (decGrandAmount + decExpenseAmount + dTaxAmount) - decDiscAmount;
                txtTaxAmount.Text = dTaxAmount.ToString("F" + MintExchangeCurrencyScale);
                txtExpenseAmount.Text = decExpenseAmount.ToString("F" + MintExchangeCurrencyScale);
                txtDiscount.Tag = decDiscAmount;
                txtTotalAmount.Text = decNetAmount.ToString("F" + MintExchangeCurrencyScale);
                CalculateExchangeCurrencyRate();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CalculateNetTotal() " + ex.Message);
                MobjLogs.WriteLog("Error in CalculateNetTotal() " + ex.Message, 2);
            }
        }

        private void CalculateTotalAmt()
        {
            try
            {
                decimal decQty = 0, decRate = 0, decDiscountAmount = 0, decDiscAmount = 0, decGrandAmount = 0, decNetAmount = 0, decSubtotal = 0;
               
                if (dgvSales.RowCount > 0)
                {
                    for (int intICounter = 0; intICounter < dgvSales.RowCount - 1; intICounter++)
                    {
                        decQty = decRate = decDiscountAmount = decGrandAmount = decDiscAmount = decNetAmount = 0;

                        decQty = dgvSales.Rows[intICounter].Cells["Quantity"].Value.ToDecimal();
                        decRate = dgvSales.Rows[intICounter].Cells["Rate"].Value.ToDecimal();
                        decDiscountAmount = dgvSales.Rows[intICounter].Cells["DiscountAmount"].Value.ToDecimal();
                        decGrandAmount = decQty * decRate;

                        if (dgvSales.Rows[intICounter].Cells["Discount"].Value.ToInt32() >= 0)
                        {
                            if (Convert.ToString(dgvSales.Rows[intICounter].Cells["Discount"].Value) == "%")
                                decDiscAmount = decGrandAmount * (decDiscountAmount / 100);
                            else
                                decDiscAmount = decDiscountAmount;
                        }

                        decNetAmount = decGrandAmount - decDiscAmount;
                        dgvSales.Rows[intICounter].Cells["GrandAmount"].Value = decGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                        dgvSales.Rows[intICounter].Cells["DiscAmount"].Value = decDiscAmount.ToString("F" + MintExchangeCurrencyScale);
                        dgvSales.Rows[intICounter].Cells["NetAmount"].Value = decNetAmount.ToString("F" + MintExchangeCurrencyScale);
                        decSubtotal += decNetAmount;
                    }

                    txtSubtotal.Text = decSubtotal.ToString("F" + MintExchangeCurrencyScale);
                    CalculateNetTotal();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CalculateTotalAmt() " + ex.Message);
                MobjLogs.WriteLog("Error in CalculateTotalAmt() " + ex.Message, 2);
            }
        }

        private bool SalesQuotationvalidation()
        {
            ErrSales.Clear();
            // lblSalesOrderstatus.Text = "";
            if (string.IsNullOrEmpty(txtOrderNo.Text.Trim()))
            {
                //MsMessageCommon = "Please select Sales Order";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2038, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation No");
                ErrSales.SetError(txtOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("*", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                txtOrderNo.Focus();
                return false;
            }

            if (cboCompany.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 14, out MmessageIcon);
                ErrSales.SetError(cboCompany, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                cboCompany.Focus();
                return false;
            }

            if (cboCustomer.SelectedIndex == -1)
            {
                // MsMessageCommon = "Please select Customer.";
                if (cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.SOTFromSalesPro)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1951, out MmessageIcon);
                    ErrSales.SetError(cboCustomer, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    cboCustomer.Focus();
                    return false;
                }
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                //MsMessageCommon = "Please select Currency.";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1958, out MmessageIcon);
                ErrSales.SetError(cboCurrency, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                cboCurrency.Focus();
                return false;
            }
            if (dtpOrderDate.Value.Date > ClsCommonSettings.GetServerDate())
            {
                // due date limit
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9006, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation");

                ErrSales.SetError(dtpOrderDate, MstrMessageCommon.Replace("#", "").Trim());
                //mObjNotification.CallErrorMessage(MaMessageArr, 17, MsMessageCaption);
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                dtpOrderDate.Focus();
                return false;
            }
            if (dtpDueDate.Value.Date < dtpOrderDate.Value.Date)
            {

                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9009, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation");

                ErrSales.SetError(dtpDueDate, MstrMessageCommon.Replace("#", "").Trim());
                //mObjNotification.CallErrorMessage(MaMessageArr, 17, MsMessageCaption);
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                dtpDueDate.Focus();
                return false;
            }
            if (ClsCommonSettings.strSQDueDateLimit.ToInt32() != 0 && (dtpDueDate.Value > dtpOrderDate.Value.AddDays(ClsCommonSettings.strSQDueDateLimit.ToDouble())))
            {
                // due date limit
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9005, out MmessageIcon).Replace("*", ClsCommonSettings.strSQDueDateLimit);
                ErrSales.SetError(dtpDueDate, MstrMessageCommon.Replace("#", "").Trim());
                //mObjNotification.CallErrorMessage(MaMessageArr, 17, MsMessageCaption);
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                dtpDueDate.Focus();
                return false;
            }
            if (dgvSales.Rows.Count == 1)
            {
                //MsMessageCommon = "Please enter Quotation Details.";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1953, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                dgvSales.Focus();
                return false;
            }
            if (cboPaymentTerms.SelectedIndex == -1)
            {
                //MsMessageCommon = "Please select Payment Terms.";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1952, out MmessageIcon);
                ErrSales.SetError(cboPaymentTerms, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                cboPaymentTerms.Focus();
                return false;
            }
            if (txtTotalAmount.Text.ToDecimal() < 0)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2056, out MmessageIcon);
                ErrSales.SetError(txtTotalAmount, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                return false;
            }

            if (!txtOrderNo.ReadOnly && MobjclsBLLSTSales.IsSalesNoExists(txtOrderNo.Text, 1, cboCompany.SelectedValue.ToInt32()))
            {
                //MsMessageCommon = "Sales Quotation No already Exsist";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2014, out MmessageIcon);
                ErrSales.SetError(txtOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                txtOrderNo.Focus();
                return false;
            }
            if (dgvSales.Rows.Count > 0)
            {
                if (ValidateQuotationGrid() == false) return false;

                int iTempID = 0;
                iTempID = CheckDuplicationInGrid();
                if (iTempID != -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1954, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    dgvSales.CurrentCell = dgvSales["ItemCode", iTempID];
                    dgvSales.Focus();
                    return false;
                }
            }
            return true;
        }

        private bool ValidateQuotationGrid()
        {
            for (int intICounter = 0; intICounter < dgvSales.RowCount - 1; intICounter++)
            {
                int iRowIndex = dgvSales.Rows[intICounter].Index;

                if (Convert.ToString(dgvSales.Rows[intICounter].Cells["ItemCode"].Value).Trim() == "")
                {
                    //MsMessageCommon = "Please enter Item code.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1955, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales["ItemCode", iRowIndex];
                    return false;
                }
                if (Convert.ToString(dgvSales.Rows[intICounter].Cells["Quantity"].Value).Trim() == "" || Convert.ToDecimal(dgvSales.Rows[intICounter].Cells["Quantity"].Value) == 0)
                {
                    //MsMessageCommon = "Please enter Quantity.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1956, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales["Quantity", iRowIndex];
                    return false;
                }
                if (Convert.ToString(dgvSales.Rows[intICounter].Cells["Rate"].Value).Trim() == "" || Convert.ToDecimal(dgvSales.Rows[intICounter].Cells["Rate"].Value) <= 0)
                {
                    //MsMessageCommon = "Please enter Rate.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1957, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales["Rate", iRowIndex];
                    return false;
                }
                if (Convert.ToInt32(dgvSales.Rows[intICounter].Cells["Uom"].Tag) == 0)
                {
                    //MsMessageCommon = "Please select Uom.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1961, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales["Uom", iRowIndex];
                    return false;
                }
                if (dgvSales.Rows[intICounter].Cells["NetAmount"].Value.ToDecimal() <= 0)
                {
                    //MsMessageCommon = "Please enter NetAmount .";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2068, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales["NetAmount", iRowIndex];
                    return false;
                }
                /* Validation for Sale Rate > CutOff price*/
                decimal decCutoffRate = MobjClsCommonUtility.GetCutOffPrice(dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt32());
                decimal decCurrentSalesRate = MobjclsBLLSTSales.GetItemRateForQuotation(dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt64(), dgvSales.Rows[intICounter].Cells[BatchID.Index].Value.ToInt64(), txtOrderNo.Tag.ToInt64());
                if (decCurrentSalesRate == 0)
                {
                    if (decCutoffRate > (dgvSales.Rows[intICounter].Cells[NetAmount.Index].Value.ToDecimal() / dgvSales.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal()))
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Red;
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2088);
                        lblSalesOrderstatus.Text = MstrMessageCommon;
                        dgvSales.Focus();
                        dgvSales.CurrentCell = dgvSales[Rate.Index, iRowIndex];
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " (" + decCutoffRate.ToString() + ")", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return false;
                    }
                    else
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Black;
                    }
                }
                else
                {
                    if ((dgvSales.Rows[intICounter].Cells[NetAmount.Index].Value.ToDecimal() / dgvSales.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal()) > decCutoffRate || (dgvSales.Rows[intICounter].Cells[NetAmount.Index].Value.ToDecimal() / dgvSales.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal()) == decCurrentSalesRate)
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Black;
                    }
                    else
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Red;
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2088);
                        lblSalesOrderstatus.Text = MstrMessageCommon;
                        dgvSales.Focus();
                        dgvSales.CurrentCell = dgvSales[Rate.Index, iRowIndex];
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " (" + decCutoffRate.ToString() + ")", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return false;
                    }
                }

                if (MblnIsFromCancellation && MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (int)OperationStatusType.SQuotationOpen && dgvSales.Rows[intICounter].Cells["BatchID"].Value.ToInt64() != 0)
                {
                    if (MobjclsBLLSTSales.FillCombos(new string[] { "BatchNo", "InvBatchDetails", "ItemID = " + dgvSales.Rows[intICounter].Cells["ItemID"].Value.ToInt32() + " And BatchID = " + dgvSales.Rows[intICounter].Cells["BatchID"].Value.ToInt64() }).Rows.Count == 0)
                    {
                        //MsMessageCommon = "Details of batch no not exists in the system .";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2078, out MmessageIcon).Replace("ItemName", dgvSales.Rows[intICounter].Cells[ItemName.Index].Value.ToString());
                        MstrMessageCommon = MstrMessageCommon.Replace("*", dgvSales.Rows[intICounter].Cells["BatchNo"].Value.ToString());
                        MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrSales.Enabled = true;
                        dgvSales.Focus();
                        dgvSales.CurrentCell = dgvSales["BatchNo", iRowIndex];
                        return false;
                    }
                }
            }
            return true;
        }

        private bool SalesOrderValidation()
        {
            ErrSales.Clear();
            //  lblSalesOrderstatus.Text = "";

            if (string.IsNullOrEmpty(txtOrderNo.Text.Trim()))
            {
                //MsMessageCommon = "Please select Sales Order";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2038, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("*", "Order No");
                ErrSales.SetError(txtOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("*", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                txtOrderNo.Focus();
                return false;
            }
            if ((int)cboOrderType.SelectedValue == (int)OperationOrderType.SOTFromQuotation && cboOrderNumbers.SelectedIndex == -1)
            {
                //MsMessageCommon = "Please select Sales Quotation";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2017, out MmessageIcon);
                ErrSales.SetError(cboOrderNumbers, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                cboOrderNumbers.Focus();
                return false;
            }
            if (MblnAddStatus)
            {
                if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromDeliveryNote)
                {
                    if (MobjclsBLLSTSales.IsSalesInvoiceExists(MlngDeliveryID))
                    {
                        //MstrMessageCommon = "reference exists";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2106, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
            }

            if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID == 0 && Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.SOTFromQuotation)
            {
                if (ClsCommonSettings.SQApproval)
                {
                    if (MobjclsBLLSTSales.FillCombos(new string[] { "SalesQuotationID", strTableSalesQuotation, "SalesQuotationID = " + Convert.ToInt64(cboOrderNumbers.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.SQuotationApproved + ")" }).Rows.Count == 0)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2046, out MmessageIcon);
                        ErrSales.SetError(cboOrderNumbers, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrSales.Enabled = true;
                        cboOrderNumbers.Focus();
                        return false;
                    }
                }
                else
                {
                    if (MobjclsBLLSTSales.FillCombos(new string[] { "SalesQuotationID", strTableSalesQuotation, "SalesQuotationID = " + Convert.ToInt64(cboOrderNumbers.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.SQuotationSubmitted + ")" }).Rows.Count == 0)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2046, out MmessageIcon).Replace("approved", "Submitted Status");
                        ErrSales.SetError(cboOrderNumbers, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrSales.Enabled = true;
                        cboOrderNumbers.Focus();
                        return false;
                    }
                }
            }
            if (cboCompany.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 14, out MmessageIcon);
                ErrSales.SetError(cboCompany, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                cboCompany.Focus();
                return false;
            }
            if (cboCustomer.SelectedIndex == -1)
            {
                if (cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.SOTFromSalesPro)
                {
                    //MsMessageCommon = "Please select Customer."; ---- 2002
                    //MsMessageCommon = "Please select a active Customer."; ---2113
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2113, out MmessageIcon);

                    ErrSales.SetError(cboCustomer, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    cboCustomer.Focus();
                    return false;
                }
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                //MsMessageCommon = "Please select Currency.";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2030, out MmessageIcon);
                ErrSales.SetError(cboCurrency, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                cboCurrency.Focus();
                return false;
            }
            if (dtpOrderDate.Value.Date > ClsCommonSettings.GetServerDate())
            {
                // due date limit
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9006, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");

                ErrSales.SetError(dtpOrderDate, MstrMessageCommon.Replace("#", "").Trim());
                //mObjNotification.CallErrorMessage(MaMessageArr, 17, MsMessageCaption);
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                dtpOrderDate.Focus();
                return false;
            }
            if (dtpDueDate.Value.Date < dtpOrderDate.Value.Date)
            {

                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9009, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");

                ErrSales.SetError(dtpDueDate, MstrMessageCommon.Replace("#", "").Trim());
                //mObjNotification.CallErrorMessage(MaMessageArr, 17, MsMessageCaption);
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                dtpDueDate.Focus();
                return false;
            }
            if (ClsCommonSettings.strSODueDateLimit.ToInt32() != 0 && (dtpDueDate.Value > dtpOrderDate.Value.AddDays(ClsCommonSettings.strSODueDateLimit.ToDouble())))
            {
                // due date limit
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9005, out MmessageIcon).Replace("*", ClsCommonSettings.strSODueDateLimit);
                ErrSales.SetError(dtpDueDate, MstrMessageCommon.Replace("#", "").Trim());
                //mObjNotification.CallErrorMessage(MaMessageArr, 17, MsMessageCaption);
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                dtpDueDate.Focus();
                return false;
            }

            if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromQuotation && cboOrderNumbers.SelectedValue.ToInt32() != 0)
            {
                DataTable datQuotationDetails = MobjclsBLLSTSales.FillCombos(new string[] { "QuotationDate,DueDate", strTableSalesQuotation, "SalesQuotationID = " + cboOrderNumbers.SelectedValue.ToInt64() });
                if (dtpOrderDate.Value.Date < datQuotationDetails.Rows[0]["QuotationDate"].ToDateTime().Date)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9008, out MmessageIcon).Replace("*", "Order");
                    MstrMessageCommon = MstrMessageCommon.Replace("@", "Quotation");
                    ErrSales.SetError(dtpOrderDate, MstrMessageCommon.Replace("#", "").Trim());
                    //mObjNotification.CallErrorMessage(MaMessageArr, 17, MsMessageCaption);
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    dtpOrderDate.Focus();
                    return false;
                }
                else if (!MblnIsFromCancellation && dtpOrderDate.Value.Date > datQuotationDetails.Rows[0]["DueDate"].ToDateTime().Date)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9007, out MmessageIcon).Replace("*", "Order");
                    MstrMessageCommon = MstrMessageCommon.Replace("@", "Quotation");
                    ErrSales.SetError(dtpOrderDate, MstrMessageCommon.Replace("#", "").Trim());
                    //mObjNotification.CallErrorMessage(MaMessageArr, 17, MsMessageCaption);
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    dtpOrderDate.Focus();
                }
            }
            if (dgvSales.Rows.Count == 1)
            {
                //MsMessageCommon = "Please enter Order Details.";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2010, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                dgvSales.Focus();
                return false;
            }
            if (cboPaymentTerms.SelectedIndex == -1)
            {
                //MsMessageCommon = "Please select Payment Terms.";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2003, out MmessageIcon);
                ErrSales.SetError(cboPaymentTerms, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                cboPaymentTerms.Focus();
                return false;
            }
            if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTDirect)
            {
                if (cboDiscount.SelectedIndex == 1 && txtDiscount.Text.ToDecimal() == 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2111, out MmessageIcon);
                    ErrSales.SetError(txtDiscount, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    return false;
                }
            }
            if (txtTotalAmount.Text.ToDecimal() < 0)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2056, out MmessageIcon);
                ErrSales.SetError(txtTotalAmount, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                return false;
            }
            if (!MblnAddStatus && MobjclsBLLSTSales.CompareAdvancepaymentAndTotAmt(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID, txtTotalAmount.Text.ToDecimal()))
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2057, out MmessageIcon);
                ErrSales.SetError(txtTotalAmount, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                return false;
            }
            if (!MblnAddStatus)
            {
                if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intPaymentTermsID == (int)PaymentTerms.Credit && cboPaymentTerms.SelectedValue.ToInt32() == (int)PaymentTerms.Cash)
                {
                    if (MobjClsCommonUtility.FillCombos(new string[] {"M.ReceiptAndPaymentID","AccReceiptAndPaymentMaster M Inner Join AccReceiptAndPaymentDetails D On M.ReceiptAndPaymentID = D.ReceiptAndPaymentID",
                                                                    "M.OperationTypeID = "+(int)OperationType.SalesOrder+" And D.ReferenceID = "+MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID}).Rows.Count > 0)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2087, out MmessageIcon);
                        ErrSales.SetError(cboPaymentTerms, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrSales.Enabled = true;
                        cboPaymentTerms.Focus();
                        return false;
                    }
                }
            }
            if (!txtOrderNo.ReadOnly && MobjclsBLLSTSales.IsSalesNoExists(txtOrderNo.Text, 2, cboCompany.SelectedValue.ToInt32()))
            {
                //MsMessageCommon = "Sales Order No already Exsist";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2018, out MmessageIcon);
                ErrSales.SetError(txtOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                txtOrderNo.Focus();
                return false;
            }
            if (MblnSubmitForApprovalFlag)
            {
                //DataTable datVendorDetails = MobjClsCommonUtility.FillCombos(new string[] { "IsNull(dbo.fnAccCustomerOpeningBalance(AccountID),0) as Balance, IsNull(CreditLimit,0) as CreditLimit,VendorName", "InvVendorInformation", "VendorID = " + cboCustomer.SelectedValue.ToInt32() });
                //decimal decBalanceAmount = 0;
                //if (datVendorDetails.Rows.Count > 0)
                //{
                //    int intCompanyCurrency = 0;

                //    DataTable datCompanyDetails = MobjClsCommonUtility.FillCombos(new string[] { "CompanyID,ParentID", "CompanyMaster", "CompanyID = " + cboCompany.SelectedValue.ToInt32() });
                //    if (datCompanyDetails.Rows[0]["ParentID"].ToInt32() != 0)
                //    {
                //        intCompanyCurrency = MobjClsCommonUtility.FillCombos(new string[] { "CurrencyId", "CompanyMaster", "CompanyID = " + datCompanyDetails.Rows[0]["ParentID"].ToInt32() }).Rows[0]["CurrencyId"].ToInt32();
                //    }
                //    else
                //        intCompanyCurrency = cboCurrency.SelectedValue.ToInt32();

                //    DataTable datCurrencyDetails = MobjClsCommonUtility.FillCombos(new string[] { "ExchangeRate", "CurrencyDetails", "CompanyID = " + cboCompany.SelectedValue.ToInt32() + " And CurrencyID = " + intCompanyCurrency + " Order By ExchangeRate desc" });
                //    if (datCurrencyDetails.Rows.Count > 0)
                //    {
                //        decBalanceAmount = datVendorDetails.Rows[0]["Balance"].ToDecimal() * datCurrencyDetails.Rows[0]["ExchangeRate"].ToDecimal();
                //    }

                //}




                string strDocument = MobjclsBLLSTSales.IsMandatoryDocumentsEntered((int)OperationType.SalesOrder, MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID.ToInt32(), cboCompany.SelectedValue.ToInt32(), (int)OperationType.SalesOrder);
                if (!string.IsNullOrEmpty(strDocument))
                {
                    MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 2060, out MmessageIcon).Replace("***", strDocument).Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                strDocument = MobjclsBLLSTSales.IsMandatoryDocumentExpired((int)OperationType.SalesOrder, MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID.ToInt32(), (int)OperationType.SalesOrder, cboCompany.SelectedValue.ToInt32());
                if (!string.IsNullOrEmpty(strDocument))
                {
                    MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 2060, out MmessageIcon).Replace("***", strDocument).Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                //decimal decOldNetAmount = 0;
                //DataTable datInvoiceDetails = MobjClsCommonUtility.FillCombos(new string[] { "NetAmount", "InvSalesOrderMaster", "SalesOrderID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID+" And VendorID = "+cboCustomer.SelectedValue.ToInt32() });
                //if (datInvoiceDetails.Rows.Count > 0)
                //    decOldNetAmount = datInvoiceDetails.Rows[0]["NetAmount"].ToDecimal();
                //if (cboPaymentTerms.SelectedValue.ToInt32() == (int)PaymentTerms.Credit && datVendorDetails.Rows[0]["CreditLimit"].ToDecimal() != 0 && ((decBalanceAmount + decOldNetAmount) - txtTotalAmount.Text.ToDecimal() > (datVendorDetails.Rows[0]["CreditLimit"].ToDecimal() * -1)))
                //{
                //    if(MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 2077, out MmessageIcon).Replace("#", "").Replace("*",datVendorDetails.Rows[0]["VendorName"].ToString()), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question)== DialogResult.No)
                //    return false;
                //}
            }
            else if (MblnApproveFlag && cboPaymentTerms.SelectedValue.ToInt32() == (int)PaymentTerms.Credit)
            {
                //DataTable datVendorDetails = MobjClsCommonUtility.FillCombos(new string[] { "IsNull(dbo.fnAccCustomerOpeningBalance(AccountID),0) as Balance, IsNull(CreditLimit,0) as CreditLimit,VendorName", "InvVendorInformation", "VendorID = " + cboCustomer.SelectedValue.ToInt32() });
                //decimal decBalanceAmount = 0;
                //if (datVendorDetails.Rows.Count > 0)
                //{
                //    int intCompanyCurrency = 0;

                //    DataTable datCompanyDetails = MobjClsCommonUtility.FillCombos(new string[] { "CompanyID,ParentID", "CompanyMaster", "CompanyID = " + cboCompany.SelectedValue.ToInt32() });
                //    if (datCompanyDetails.Rows[0]["ParentID"].ToInt32() != 0)
                //    {
                //        intCompanyCurrency = MobjClsCommonUtility.FillCombos(new string[] { "CurrencyId", "CompanyMaster", "CompanyID = " + datCompanyDetails.Rows[0]["ParentID"].ToInt32() }).Rows[0]["CurrencyId"].ToInt32();
                //    }
                //    else
                //        intCompanyCurrency = cboCurrency.SelectedValue.ToInt32();

                //    DataTable datCurrencyDetails = MobjClsCommonUtility.FillCombos(new string[] { "ExchangeRate", "CurrencyDetails", "CompanyID = " + cboCompany.SelectedValue.ToInt32() + " And CurrencyID = " + intCompanyCurrency + " Order By ExchangeRate desc" });
                //    if (datCurrencyDetails.Rows.Count > 0)
                //    {
                //        decBalanceAmount = datVendorDetails.Rows[0]["Balance"].ToDecimal() * datCurrencyDetails.Rows[0]["ExchangeRate"].ToDecimal();
                //    }

                //}

                //decimal decOldNetAmount = 0;
                //DataTable datInvoiceDetails = MobjClsCommonUtility.FillCombos(new string[] { "NetAmount", "InvSalesOrderMaster", "SalesOrderID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID });
                //if (datInvoiceDetails.Rows.Count > 0)
                //    decOldNetAmount = datInvoiceDetails.Rows[0]["NetAmount"].ToDecimal();
                //if (datVendorDetails.Rows[0]["CreditLimit"].ToDecimal() != 0 && ((decBalanceAmount + decOldNetAmount) - txtTotalAmount.Text.ToDecimal() > (datVendorDetails.Rows[0]["CreditLimit"].ToDecimal() * -1)))
                //{
                //    if (MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 2077, out MmessageIcon).Replace("#", "").Replace("*", datVendorDetails.Rows[0]["VendorName"].ToString()), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                //        return false;
                //}
            }
            if (dgvSales.Rows.Count > 0)
            {
                if (ValidateOrderGrid() == false) return false;

                int iTempID = 0;
                iTempID = CheckDuplicationInGrid();
                if (iTempID != -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1954, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    dgvSales.CurrentCell = dgvSales["ItemCode", iTempID];
                    dgvSales.Focus();
                    return false;
                }

                if (ValidateCutOffPriceinOrderGrid() == false) return false;
            }
            //------------------------------------------------ Bottom Panel---------------------------------------------------------------------------
            return true;
        }

        private bool ValidateCutOffPriceinInvoiceGrid()
        {

            for (int intICounter = 0; intICounter < dgvSales.RowCount - 1; intICounter++)
            {
                int iRowIndex = dgvSales.Rows[intICounter].Index;

                decimal decCutoffRate = MobjClsCommonUtility.GetCutOffPrice(dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt32());
                decimal decCurrentSalesRate = MobjclsBLLSTSales.GetItemRateForInvoice(dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt64(), dgvSales.Rows[intICounter].Cells[BatchID.Index].Value.ToInt64(), txtOrderNo.Tag.ToInt64());


                if (decCurrentSalesRate == 0)
                {
                    if (decCutoffRate > (dgvSales.Rows[intICounter].Cells[NetAmount.Index].Value.ToDecimal() / dgvSales.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal()))
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Red;
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2088);
                        lblSalesOrderstatus.Text = MstrMessageCommon;
                        dgvSales.Focus();
                        dgvSales.CurrentCell = dgvSales[Rate.Index, iRowIndex];
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " (" + decCutoffRate.ToString() + ")", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return false;
                    }
                    else
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Black;
                    }
                }
                else
                {
                    if ((dgvSales.Rows[intICounter].Cells[NetAmount.Index].Value.ToDecimal() / dgvSales.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal()) > decCutoffRate /*|| (dgvSales.Rows[intICounter].Cells[NetAmount.Index].Value.ToDecimal() / dgvSales.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal()) == decCurrentSalesRate*/)
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Black;
                    }
                    else
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Red;
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2088);
                        lblSalesOrderstatus.Text = MstrMessageCommon;
                        dgvSales.Focus();
                        dgvSales.CurrentCell = dgvSales[Rate.Index, iRowIndex];
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " (" + decCutoffRate.ToString() + ")", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return false;
                    }
                }
            }
            return true;
        }

        private bool ValidateCutOffPriceinOrderGrid()
        {

            for (int intICounter = 0; intICounter < dgvSales.RowCount - 1; intICounter++)
            {
                int iRowIndex = dgvSales.Rows[intICounter].Index;

                int intItemBaseUomID = MobjClsCommonUtility.GetBaseUomByItemID(dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt32());
                int intItemCurrentUomID = dgvSales.Rows[intICounter].Cells[Uom.Index].Tag.ToInt32();
                int intQuantity = dgvSales.Rows[intICounter].Cells[Quantity.Index].Value.ToInt32();
                decimal decUomConversionFactor,decConversionRate = 0;
                decimal decCutoffRate = MobjClsCommonUtility.GetCutOffPrice(dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt32());
                if (intItemBaseUomID != intItemCurrentUomID)
                {
                    DataTable dtconversion = MobjClsCommonUtility.ConvertRateFromBaseUOMtoOtherUOM(intItemBaseUomID, intItemCurrentUomID);
                    if (dtconversion.Rows.Count > 0)
                    {
                        decUomConversionFactor = dtconversion.Rows[0][0].ToDecimal(); //MobjClsCommonUtility.ConvertQtyToBaseUnitQty(intItemCurrentUomID, dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt32(), intQuantity, 0);
                        decConversionRate = dtconversion.Rows[0][1].ToDecimal();
                        if (decUomConversionFactor == 1)
                        {
                            decCutoffRate = decCutoffRate / decConversionRate;
                        }
                        else
                        {
                            decCutoffRate = decCutoffRate * decConversionRate;
                        }
                    }
                    //decUomConversionFactor = MobjClsCommonUtility.ConvertQtyToBaseUnitQty(intItemCurrentUomID, dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt32(), intQuantity, 0);
                    //decCutoffRate = decCutoffRate * decUomConversionFactor;
                    decCutoffRate=System.Math.Round(decCutoffRate,3);
                }
 
                decimal decCurrentSalesRate = MobjclsBLLSTSales.GetItemRateForSalesOrder(dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt64(), dgvSales.Rows[intICounter].Cells[BatchID.Index].Value.ToInt64(), txtOrderNo.Tag.ToInt64());

                if (decCurrentSalesRate == 0)
                {
                    if (decCutoffRate > (dgvSales.Rows[intICounter].Cells[NetAmount.Index].Value.ToDecimal() / dgvSales.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal()))
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Red;
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2088);
                        lblSalesOrderstatus.Text = MstrMessageCommon;
                        dgvSales.Focus();
                        dgvSales.CurrentCell = dgvSales[Rate.Index, iRowIndex];
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " (" + decCutoffRate.ToString() + ")", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return false;
                    }
                    else
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Black;
                    }
                }
                else
                {
                    if ((dgvSales.Rows[intICounter].Cells[NetAmount.Index].Value.ToDecimal() / dgvSales.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal()) > decCutoffRate || (dgvSales.Rows[intICounter].Cells[NetAmount.Index].Value.ToDecimal() / dgvSales.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal()) == decCurrentSalesRate)
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Black;
                    }
                    else
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Red;
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2088);
                        lblSalesOrderstatus.Text = MstrMessageCommon;
                        dgvSales.Focus();
                        dgvSales.CurrentCell = dgvSales[Rate.Index, iRowIndex];
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " (" + decCutoffRate.ToString() + ")", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return false;
                    }
                }
            }
            return true;
        }

        private bool ValidateOrderGrid()
        {
            for (int intICounter = 0; intICounter < dgvSales.RowCount - 1; intICounter++)
            {
                int iRowIndex = dgvSales.Rows[intICounter].Index;

                if (Convert.ToString(dgvSales.Rows[intICounter].Cells["ItemCode"].Value).Trim() == "")
                {
                    //MsMessageCommon = "Please enter Item code.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2006, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales[ItemCode.Index, iRowIndex];
                    return false;
                }
                if (Convert.ToString(dgvSales.Rows[intICounter].Cells["Quantity"].Value).Trim() == "" || Convert.ToDecimal(dgvSales.Rows[intICounter].Cells["Quantity"].Value) == 0)
                {
                    //MsMessageCommon = "Please enter Quantity.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2007, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales[Quantity.Index, iRowIndex];
                    return false;
                }
                if (Convert.ToString(dgvSales.Rows[intICounter].Cells["Rate"].Value).Trim() == "" || Convert.ToDecimal(dgvSales.Rows[intICounter].Cells["Rate"].Value) <= 0)
                {
                    //MsMessageCommon = "Please enter Rate.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2008, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales[Rate.Index, iRowIndex];
                    return false;
                }
                //if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromDeliveryNote)
                //{
                //    if (dgvSales.Rows[intICounter].Cells["IsGroup"].Value.ToInt32() == 0)
                //    {
                //        if (MobjclsBLLSTSales.CheckWheatherPurchaseInvoiceExists(dgvSales.Rows[intICounter].Cells["ItemID"].Value.ToInt32(), dgvSales.Rows[intICounter].Cells["BatchID"].Value.ToInt32()))
                //        {
                //            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2112, out MmessageIcon).Replace("*", dgvSales.Rows[intICounter].Cells[ItemName.Index].Value.ToString());
                //            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //            lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //            TmrSales.Enabled = true;
                //            dgvSales.Focus();
                //            dgvSales.CurrentCell = dgvSales["ItemName", iRowIndex];
                //            return false;
                //        }
                //    }
                //}
                if (Convert.ToInt32(dgvSales.Rows[intICounter].Cells["Uom"].Tag) == 0)
                {
                    //MsMessageCommon = "Please select Uom.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2033, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales["Uom", iRowIndex];
                    return false;
                }
                if (dgvSales.Rows[intICounter].Cells["NetAmount"].Value.ToDecimal() <= 0)
                {
                    //MsMessageCommon = "Please enter NetAmount .";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2068, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales["NetAmount", iRowIndex];
                    return false;
                }
                /* Validation for Sale Rate > CutOff price*/
                if ((cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTDirect) && Convert.ToBoolean(dgvSales[IsGroup.Index, intICounter].Value) == true && MblnAddStatus)
                {
                    if (dgvSales.Rows[intICounter].Cells["Quantity"].Value.ToDecimal() > dgvSales.Rows[intICounter].Cells["QtyAvailable"].Value.ToDecimal())
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2063, out MmessageIcon).Replace("#", "");
                        if (MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MmessageIcon) == DialogResult.No)
                        {
                            dgvSales.Focus();
                            dgvSales.CurrentCell = dgvSales["Quantity", intICounter];
                            return false;
                        }
                    }
                }
                if (MblnIsFromCancellation && MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (int)OperationStatusType.SOrderOpen && dgvSales.Rows[intICounter].Cells["BatchID"].Value.ToInt64() != 0)
                {
                    if (MobjclsBLLSTSales.FillCombos(new string[] { "BatchNo", "InvBatchDetails", "ItemID = " + dgvSales.Rows[intICounter].Cells["ItemID"].Value.ToInt32() + " And BatchID = " + dgvSales.Rows[intICounter].Cells["BatchID"].Value.ToInt64() }).Rows.Count == 0)
                    {
                        //MsMessageCommon = "Details of batch no not exists in the system .";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2078, out MmessageIcon).Replace("ItemName", dgvSales.Rows[intICounter].Cells[ItemName.Index].Value.ToString());
                        MstrMessageCommon = MstrMessageCommon.Replace("*", dgvSales.Rows[intICounter].Cells["BatchNo"].Value.ToString());
                        MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrSales.Enabled = true;
                        dgvSales.Focus();
                        dgvSales.CurrentCell = dgvSales["BatchNo", iRowIndex];
                        return false;
                    }
                }
                if (MintFromForm == 3)
                {
                    if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SInvoiceCancelled)
                    {

                        if (MobjclsBLLSTSales.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt64() }).Rows[0]["CostingMethodID"].ToInt32() == (int)CostingMethodReference.Batch)
                        {
                            if (MobjClsCommonUtility.GetStockQuantity(dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt32(), dgvSales.Rows[intICounter].Cells[BatchID.Index].Value.ToInt64(), cboCompany.SelectedValue.ToInt32(), ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")) - MobjclsBLLSTSales.GetSalesInvoiceReservedQty(dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt64(), dgvSales.Rows[intICounter].Cells[BatchID.Index].Value.ToInt64(), MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID, cboCompany.SelectedValue.ToInt32()) < MobjClsCommonUtility.ConvertQtyToBaseUnitQty(dgvSales.Rows[intICounter].Cells[Uom.Index].Tag.ToInt32(), dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt32(), dgvSales.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal(), 1))
                            {
                                //MsMessageCommon = "Out of stock.";
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2081, out MmessageIcon).Replace("*", dgvSales.Rows[intICounter].Cells[ItemName.Index].Value.ToString() + "(" + dgvSales.Rows[intICounter].Cells["BatchNo"].Value.ToString() + ")");
                                //MstrMessageCommon = MstrMessageCommon.Replace("*", dgvSales.Rows[intICounter].Cells["BatchNo"].Value.ToString());
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrSales.Enabled = true;
                                dgvSales.Focus();
                                dgvSales.CurrentCell = dgvSales["ItemName", intICounter];
                                return false;
                            }
                        }
                        else
                        {
                            if (MobjClsCommonUtility.GetStockQuantity(dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt32(), 0, cboCompany.SelectedValue.ToInt32(), ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")) - MobjclsBLLSTSales.GetSalesInvoiceReservedQty(dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt64(), 0, MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID, cboCompany.SelectedValue.ToInt32()) < MobjClsCommonUtility.ConvertQtyToBaseUnitQty(dgvSales.Rows[intICounter].Cells[Uom.Index].Tag.ToInt32(), dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt32(), dgvSales.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal(), 1))
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2081, out MmessageIcon).Replace("*", dgvSales.Rows[intICounter].Cells[ItemName.Index].Value.ToString());
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrSales.Enabled = true;
                                dgvSales.Focus();
                                dgvSales.CurrentCell = dgvSales["ItemName", iRowIndex];
                                return false;
                            }

                        }
                    }
                }
            }
            if (MintFromForm == 2 && (MblnAddStatus || (!MblnAddStatus && MblnQtyChanged)))
            {
                for (int intICounter = 0; intICounter < dgvSales.Rows.Count; intICounter++)
                {
                    if (dgvSales.Rows[intICounter].Cells[ProductTypeID.Index].Value.ToInt32() == 1)
                    {
                        if (dgvSales["ItemID", intICounter].Value != null && Convert.ToBoolean(dgvSales[IsGroup.Index, intICounter].Value) == false)
                        {
                            DataTable dtDatasource = MobjclsBLLSTSales.GetDataForItemSelection(Convert.ToInt32(cboCompany.SelectedValue), Convert.ToInt32(cboCurrency.SelectedValue), MintFromForm);
                            if (MobjclsBLLSTSales.FillCombos(new string[] { "CostingMethodID", strTableItemDetails, "ItemID = " + dgvSales["ItemID", intICounter].Value.ToInt32() }).Rows[0]["CostingMethodID"].ToInt32() == (int)CostingMethodReference.Batch)
                            {
                                dtDatasource.DefaultView.RowFilter = "ItemID = " + Convert.ToInt32(dgvSales["ItemID", intICounter].Value) + " And BatchID = " + dgvSales["BatchID", intICounter].Value.ToInt64();
                            }
                            else
                                dtDatasource.DefaultView.RowFilter = "ItemID = " + Convert.ToInt32(dgvSales["ItemID", intICounter].Value);

                            decimal decQuantity = 0;
                            decQuantity = Convert.ToDecimal(dgvSales["Quantity", intICounter].Value);


                            DataTable dtGetUomDetails = MobjclsBLLSTSales.GetUomConversionValues(Convert.ToInt32(dgvSales["UOM", intICounter].Tag), Convert.ToInt32(dgvSales["ItemID", intICounter].Value));
                            if (dtGetUomDetails.Rows.Count > 0)
                            {
                                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                if (intConversionFactor == 1)
                                    decQuantity = decQuantity / decConversionValue;
                                else if (intConversionFactor == 2)
                                    decQuantity = decQuantity * decConversionValue;
                            }
                            //for (int intJCounter = intICounter + 1; intJCounter < dgvSales.Rows.Count; intJCounter++)
                            //{
                            //    if (dgvSales["ItemID", intJCounter].Value != null && dgvSales["UOM", intJCounter].Tag != null && dgvSales["Quantity", intJCounter].Value != null && (Convert.ToInt32(dgvSales["ItemID", intICounter].Value) == Convert.ToInt32(dgvSales["ItemID", intJCounter].Value)) && (Convert.ToInt32(dgvSales["ItemID", intICounter].Value) == Convert.ToInt32(dgvSales["ItemID", intJCounter].Value))
                            //    {
                            //        dtGetUomDetails = MobjclsBLLSTSales.GetUomConversionValues(Convert.ToInt32(dgvSales["UOM", intJCounter].Tag), Convert.ToInt32(dgvSales["ItemID", intICounter].Value));
                            //        decimal decQty = Convert.ToDecimal(dgvSales["Quantity", intJCounter].Value);
                            //        if (dtGetUomDetails.Rows.Count > 0)
                            //        {
                            //            int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                            //            decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                            //            if (intConversionFactor == 1)
                            //                decQty = decQty / decConversionValue;
                            //            else if (intConversionFactor == 2)
                            //                decQty = decQty * decConversionValue;
                            //        }
                            //        decQuantity += decQty;
                            //    }
                            //}

                            if (cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.SOTFromDeliveryNote)
                            {

                                if (dtDatasource.DefaultView.ToTable().Rows.Count > 0)
                                {
                                    if (decQuantity > (dtDatasource.DefaultView.ToTable().Rows[0]["QtyAvailable"]).ToDecimal())
                                    {
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2063, out MmessageIcon).Replace("#", "");
                                        if (MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MmessageIcon) == DialogResult.No)
                                        {
                                            dgvSales.Focus();
                                            dgvSales.CurrentCell = dgvSales["Quantity", intICounter];
                                            return false;
                                        }
                                        else
                                            return true;
                                    }
                                }
                                else
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2063, out MmessageIcon).Replace("#", "");
                                    if (MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MmessageIcon) == DialogResult.No)
                                    {
                                        dgvSales.Focus();
                                        dgvSales.CurrentCell = dgvSales["Quantity", intICounter];
                                        return false;
                                    }
                                    else
                                        return true;
                                }
                            }
                        }
                    }
                }
            }
           
            return true;
        }

        private bool FillSalesOrderParameters()
        {
            //Filling Master Parameters
            
            if (MblnAddStatus == true)
                MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID = 0;
            else
                MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID = Convert.ToInt64(txtOrderNo.Tag);

            MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID = cboOrderNumbers.SelectedValue.ToInt64();
            MobjclsBLLSTSales.clsDTOSTSalesMaster.strSalesOrderNo = txtOrderNo.Text.Replace("'", "�").Trim();
            MobjclsBLLSTSales.clsDTOSTSalesMaster.strOrderDate = dtpOrderDate.Value.ToString("dd-MMM-yyyy");

            MobjclsBLLSTSales.clsDTOSTSalesMaster.intOrderTypeID = Convert.ToInt32(cboOrderType.SelectedValue);
            MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);   
            MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID = MintVendorAddID.ToInt32();
         

            if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromSalesPro)
            {
                DataTable dtven = MobjclsBLLSTSales.SaveVendorFromSalesPro();
                if (dtven.Rows.Count > 0)
                {
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID = dtven.Rows[0]["VendorID"].ToInt32();
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID = dtven.Rows[0]["VendorAddressID"].ToInt32();
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.intOrderTypeID = 12;
                }
               
            }
            MobjclsBLLSTSales.clsDTOSTSalesMaster.strDueDate = dtpDueDate.Value.ToString("dd-MMM-yyyy");
            MobjclsBLLSTSales.clsDTOSTSalesMaster.intPaymentTermsID = Convert.ToInt32(cboPaymentTerms.SelectedValue);
            
            MobjclsBLLSTSales.clsDTOSTSalesMaster.intTaxSchemeID = Convert.ToInt32(cboTaxScheme.SelectedValue);
            MobjclsBLLSTSales.clsDTOSTSalesMaster.decTaxAmount =txtTaxAmount.Text.ToDecimal();
            
            MobjclsBLLSTSales.clsDTOSTSalesMaster.strRemarks = txtRemarks.Text.Trim();
            MobjclsBLLSTSales.clsDTOSTSalesMaster.intEmployeeID = ClsCommonSettings.intEmployeeID;
            MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID = Convert.ToInt32(cboCurrency.SelectedValue);
            MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandAmount = Convert.ToDecimal(txtSubtotal.Text);
            MobjclsBLLSTSales.clsDTOSTSalesMaster.decNetAmount = Convert.ToDecimal(txtTotalAmount.Text);
            MobjclsBLLSTSales.clsDTOSTSalesMaster.decNetAmountRounded = Math.Ceiling(MobjclsBLLSTSales.clsDTOSTSalesMaster.decNetAmount);
            MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            MobjclsBLLSTSales.clsDTOSTSalesMaster.strCondition = strCondition1;
            MobjclsBLLSTSales.clsDTOSTSalesMaster.lopno = txtLpoNo.Text.Trim();
            
            if (cboDiscount.SelectedItem != null)
            {
                if (cboDiscount.SelectedItem.ToString() == "%")
                {

                    MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountPercentage = txtDiscount.Text.ToDecimal();
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountAmount = (txtDiscount.Text.ToDecimal() / 100) * Convert.ToDecimal(txtSubtotal.Text);
                     MobjclsBLLSTSales.clsDTOSTSalesMaster.blnIsDiscountPercentage = true;
                }
                else
                {
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountAmount = txtDiscount.Text.ToDecimal();
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.blnIsDiscountPercentage = false;
                }
            }






            if (MblnAddStatus == true)
            {
                MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SOrderOpen; // Status ID 13 for Sales order open
                MobjclsBLLSTSales.clsDTOSTSalesMaster.intCreatedBy = ClsCommonSettings.UserID;
                MobjclsBLLSTSales.clsDTOSTSalesMaster.strCreatedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
            }
            else if (MblnSubmitForApprovalFlag == true)
            {
                if (ClsCommonSettings.SOApproval)
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SOrderSubmittedForApproval;
                else
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SOrderSubmitted;
                // MobjclsBLLSTSales.clsDTOSTSalesMaster.intApprovedBy = ClsCommonSettings.UserID;
                //MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                MobjclsBLLSTSales.clsDTOSTSalesMaster.intApprovedBy = ClsCommonSettings.UserID;
                MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
            }
            else if (MblnRejectFlag == true)
            {
                MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SOrderRejected;
                MobjclsBLLSTSales.clsDTOSTSalesMaster.intApprovedBy = ClsCommonSettings.UserID;
                MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                MobjclsBLLSTSales.clsDTOSTSalesMaster.strVerificationDescription = txtDescription.Text.Trim();
            }
            else if (MblnApproveFlag == true)
            {
                MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SOrderApproved;
                MobjclsBLLSTSales.clsDTOSTSalesMaster.intApprovedBy = ClsCommonSettings.UserID;
                MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
            }
            MobjclsBLLSTSales.clsDTOSTSalesMaster.decExpenseAmount = txtExpenseAmount.Text.ToDecimal();
            FillSalesOrderDetParameters();

            return true;
        }

        private bool FillSalesOrderDetParameters()
        {
            //Filling Detail Parameters

            MobjclsBLLSTSales.clsDTOSTSalesMaster.IlstClsDTOSTSalesDetails = new List<clsDTOSTSalesOrderDetails>();

            for (int intICounter = 0; intICounter < dgvSales.Rows.Count; intICounter++)
            {
                if (dgvSales["ItemID", intICounter].Value != null && dgvSales["Quantity", intICounter].Value != null)
                {
                    clsDTOSTSalesOrderDetails objclsDTOSTSalesDetails = new clsDTOSTSalesOrderDetails();
                    objclsDTOSTSalesDetails.intItemID = Convert.ToInt32(dgvSales["ItemID", intICounter].Value);
                    objclsDTOSTSalesDetails.intBatchID = dgvSales["BatchID", intICounter].Value.ToInt64();
                    objclsDTOSTSalesDetails.blnIsGroup = Convert.ToBoolean(dgvSales["IsGroup", intICounter].Value);

                    objclsDTOSTSalesDetails.decQuantity = Convert.ToDecimal(dgvSales["Quantity", intICounter].Value);
                    objclsDTOSTSalesDetails.intUOMID = Convert.ToInt32(dgvSales["UOM", intICounter].Tag);
                    objclsDTOSTSalesDetails.decRate = Convert.ToDecimal(dgvSales["Rate", intICounter].Value);
                    objclsDTOSTSalesDetails.intSalesQuotationID = Convert.ToInt64(cboOrderNumbers.SelectedValue);

                    if (dgvSales.Rows[intICounter].Cells["Discount"].Value.ToInt32() >= 0)
                    {
                        if (Convert.ToString(dgvSales.Rows[intICounter].Cells["Discount"].Value) == "%")
                        {
                            objclsDTOSTSalesDetails.blnIsDiscountPercentage = true;
                            objclsDTOSTSalesDetails.decDiscountPercentage = Convert.ToDecimal(dgvSales["DiscountAmount", intICounter].Value);
                            objclsDTOSTSalesDetails.decDiscountAmount = Convert.ToDecimal(dgvSales["DiscAmount", intICounter].Value);
                        }
                        else
                        {
                            objclsDTOSTSalesDetails.blnIsDiscountPercentage = false;
                            objclsDTOSTSalesDetails.decDiscountPercentage = 0;
                            objclsDTOSTSalesDetails.decDiscountAmount = Convert.ToDecimal(dgvSales["DiscountAmount", intICounter].Value);
                        }
                    }

                    objclsDTOSTSalesDetails.decNetAmount = Convert.ToDecimal(dgvSales["NetAmount", intICounter].Value);
                    objclsDTOSTSalesDetails.decGrandAmount = Convert.ToDecimal(dgvSales["GrandAmount", intICounter].Value);

                    MobjclsBLLSTSales.clsDTOSTSalesMaster.IlstClsDTOSTSalesDetails.Add(objclsDTOSTSalesDetails);
                }
            }
            return true;
        }

        private bool SaveSalesOrders()
        {
            if (!MblnIsFromCancellation)
            {
                if (MblnAddStatus == true)
                    //MsMessageCommon = "Do you wish to save new Sales Orders?";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1, out  MmessageIcon);
                else if (MblnSubmitForApprovalFlag)
                {
                    //MsMessageCommon = "Are you sure you want to submit this Order for Approval?";
                    if (ClsCommonSettings.SOApproval)
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2020, out MmessageIcon);
                    else
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9011, out MmessageIcon).Replace("*", "Order");
                }
                else if (MblnApproveFlag)
                    //MsMessageCommon = "Are you sure you want to Approval this Sales Order?";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2023, out  MmessageIcon);
                else if (MblnRejectFlag)
                    //MsMessageCommon = "Are you sure you want to Reject this Sales Order?";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2023, out  MmessageIcon);
                else
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;
            }
            if (MblnAddStatus && txtOrderNo.ReadOnly && MobjclsBLLSTSales.IsSalesNoExists(txtOrderNo.Text, MintFromForm, cboCompany.SelectedValue.ToInt32()))
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2061, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return false;
                else
                    GenerateOrderNo();
            }
            FillSalesOrderParameters();

            if (MobjclsBLLSTSales.SaveSalesOrder(MblnAddStatus))
            {
                clsBLLAlertMoment objclsBLLAlertMoment = new clsBLLAlertMoment();
                //Sales order alert
                if (MblnAddStatus)
                {
                    objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.SaleOrderCreated;
                    objFrmTradingMain.tmrAlert.Enabled = true;
                }
                else if (MblnSubmitForApprovalFlag)
                {
                    objclsBLLAlertMoment.DeleteAlert(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID.ToInt32(), (int)AlertSettingsTypes.SaleOrderCreated);
                    objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.SaleOrderSubmitted;
                    objFrmTradingMain.tmrAlert.Enabled = true;
                }
                else if (MblnApproveFlag)
                {
                    objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.SaleOrderApproved;
                    objFrmTradingMain.tmrAlert.Enabled = true;
                }
                else if (MblnRejectFlag)
                {
                    objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.SaleOrderRejected;
                    objFrmTradingMain.tmrAlert.Enabled = true;
                }

                if (!MblnAddStatus && !MblnSubmitForApprovalFlag && !MblnApproveFlag && !MblnRejectFlag)
                {
                    objclsBLLAlertMoment.DeleteAlert(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID.ToInt32(), (int)AlertSettingsTypes.SaleOrderCreated);
                    objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.SaleOrderCreated;
                    objFrmTradingMain.tmrAlert.Enabled = true;
                }

                txtOrderNo.Tag = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID;
                MobjLogs.WriteLog("Saved successfully:SaveCurrency()  " + this.Name + "", 0);
                return true;
            }
            return false;
        }

        private bool SalesInvoiceValidation()
        {
            ErrSales.Clear();
            // lblSalesOrderstatus.Text = "";


            if (string.IsNullOrEmpty(txtOrderNo.Text.Trim()))
            {
                //MsMessageCommon = "Please select Sales Order";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2038, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("*", "Invoice No");
                ErrSales.SetError(txtOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("*", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                txtOrderNo.Focus();
                return false;
            }
            if (cboOrderType.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2090, out MmessageIcon);
                ErrSales.SetError(cboOrderType, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                cboOrderType.Focus();
                return false;
            }

            //if (MblnIsFromCancellation)
            
                //if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID == 0 && ((Int32)cboOrderType.SelectedValue == (int)OperationOrderType.SalesInvoiceFromOrder))
                //{
                //    if (ClsCommonSettings.SOApproval)
                //    {
                //        if (MobjclsBLLSTSales.FillCombos(new string[] { "SalesOrderID", strTableSalesOrder, "SalesOrderID = " + Convert.ToInt64(cboOrderNumbers.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.SOrderApproved + ")" }).Rows.Count == 0)
                //        {
                //            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2053, out MmessageIcon);
                //            ErrSales.SetError(cboOrderNumbers, MstrMessageCommon.Replace("#", "").Trim());
                //            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //            lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //            TmrSales.Enabled = true;
                //            cboOrderNumbers.Focus();
                //            return false;
                //        }
                //    }
                //    else
                //    {
                //        if (MobjclsBLLSTSales.FillCombos(new string[] { "SalesOrderID", strTableSalesOrder, "SalesOrderID = " + Convert.ToInt64(cboOrderNumbers.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.SOrderSubmitted + ")" }).Rows.Count == 0)
                //        {
                //            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2053, out MmessageIcon).Replace("approved", "submitted");
                //            ErrSales.SetError(cboOrderNumbers, MstrMessageCommon.Replace("#", "").Trim());
                //            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //            lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //            TmrSales.Enabled = true;
                //            cboOrderNumbers.Focus();
                //            return false;
                //        }
                //    }
                //}
            
            if ((Int32)cboOrderType.SelectedValue == (int)OperationOrderType.SalesInvoiceFromOrder)
            {
                if (cboOrderNumbers.SelectedIndex == -1)
                {
                    //MsMessageCommon = "Please select Sales Order";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2021, out MmessageIcon);
                    ErrSales.SetError(cboOrderNumbers, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    cboOrderNumbers.Focus();
                    return false;
                }
                if (cboOrderNumbers.SelectedValue.ToInt32() != 0)
                {
                    DataTable datOrderDetails = MobjclsBLLSTSales.FillCombos(new string[] { "OrderDate,DueDate", strTableSalesOrder, "SalesOrderID = " + cboOrderNumbers.SelectedValue.ToInt64() });
                    if (dtpOrderDate.Value.Date < datOrderDetails.Rows[0]["OrderDate"].ToDateTime().Date)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9008, out MmessageIcon).Replace("*", "Invoice");
                        MstrMessageCommon = MstrMessageCommon.Replace("@", "Order");
                        ErrSales.SetError(dtpOrderDate, MstrMessageCommon.Replace("#", "").Trim());
                        //mObjNotification.CallErrorMessage(MaMessageArr, 17, MsMessageCaption);
                        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrSales.Enabled = true;
                        dtpOrderDate.Focus();
                        return false;
                    }
                    else if (!MblnIsFromCancellation && dtpOrderDate.Value.Date > datOrderDetails.Rows[0]["DueDate"].ToDateTime().Date)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9007, out MmessageIcon).Replace("*", "Invoice");
                        MstrMessageCommon = MstrMessageCommon.Replace("@", "Order");
                        ErrSales.SetError(dtpOrderDate, MstrMessageCommon.Replace("#", "").Trim());
                        //mObjNotification.CallErrorMessage(MaMessageArr, 17, MsMessageCaption);
                        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrSales.Enabled = true;
                        dtpOrderDate.Focus();
                    }
                }
            }
            if (cboCompany.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 14, out MmessageIcon);
                ErrSales.SetError(cboCompany, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                cboCompany.Focus();
                return false;
            }
            if (cboCustomer.SelectedIndex == -1)
            {
                //MsMessageCommon = "Please select Customer.";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2002, out MmessageIcon);

                ErrSales.SetError(cboCustomer, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                cboCustomer.Focus();
                return false;
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                //MsMessageCommon = "Please select Currency.";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2030, out MmessageIcon);
                ErrSales.SetError(cboCurrency, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                cboCurrency.Focus();
                return false;
            }
            if (dtpOrderDate.Value.Date > ClsCommonSettings.GetServerDate())
            {
                // due date limit
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9006, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("*", "Invoice");

                ErrSales.SetError(dtpOrderDate, MstrMessageCommon.Replace("#", "").Trim());
                //mObjNotification.CallErrorMessage(MaMessageArr, 17, MsMessageCaption);
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                dtpOrderDate.Focus();
                return false;
            }
            if (dtpDueDate.Value.Date < dtpOrderDate.Value.Date)
            {

                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9009, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("*", "Invoice");
                ErrSales.SetError(dtpDueDate, MstrMessageCommon.Replace("#", "").Trim());
                //mObjNotification.CallErrorMessage(MaMessageArr, 17, MsMessageCaption);
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                dtpDueDate.Focus();
                return false;
            }
            if (ClsCommonSettings.strSIDueDateLimit.ToInt32() != 0 && (dtpDueDate.Value > dtpOrderDate.Value.AddDays(ClsCommonSettings.strSIDueDateLimit.ToDouble())))
            {
                // due date limit
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9005, out MmessageIcon).Replace("*", ClsCommonSettings.strSIDueDateLimit);
                ErrSales.SetError(dtpDueDate, MstrMessageCommon.Replace("#", "").Trim());
                //mObjNotification.CallErrorMessage(MaMessageArr, 17, MsMessageCaption);
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                dtpDueDate.Focus();
                return false;
            }


            if (dgvSales.Rows.Count == 1)
            {
                //MsMessageCommon = "Please enter Invoice Details.";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2010, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                dgvSales.Focus();
                return false;
            }
            if (cboPaymentTerms.SelectedIndex == -1)
            {
                //MsMessageCommon = "Please select Payment Terms.";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2003, out MmessageIcon);
                ErrSales.SetError(cboPaymentTerms, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                cboPaymentTerms.Focus();
                return false;
            }
            if (txtTotalAmount.Text.ToDecimal() < 0)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2056, out MmessageIcon);
                ErrSales.SetError(txtTotalAmount, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                return false;
            }
            if (!txtOrderNo.ReadOnly && MobjclsBLLSTSales.IsSalesNoExists(txtOrderNo.Text, 3, cboCompany.SelectedValue.ToInt32()))
            {
                //MsMessageCommon = "Sales Invoice No already Exsist";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2022, out MmessageIcon);
                ErrSales.SetError(txtOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                txtOrderNo.Focus();
                return false;
            }
            MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);

            if (cboPaymentTerms.SelectedValue.ToInt32() == (int)PaymentTerms.Credit && MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SInvoiceCancelled)
            {
                //if (!MblnRejectFlag && !MobjclsBLLSTSales.GetCompanyAccount(Convert.ToInt32(TransactionTypes.CreditSale)))
                //{
                //    //MsMessageCommon = "Please create a Sales account for the selected company.";
                //    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2034, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmrSales.Enabled = true;
                //    return false;
                //}

                DataTable datVendorDetails = MobjClsCommonUtility.FillCombos(new string[] { "IsNull(dbo.fnAccCustomerOpeningBalance(AccountID),0) as Balance, IsNull(CreditLimit,0) as CreditLimit,VendorName", "InvVendorInformation", "VendorID = " + cboCustomer.SelectedValue.ToInt32() });
                decimal decBalanceAmount = 0;
                if (datVendorDetails.Rows.Count > 0)
                {
                    int intCompanyCurrency = 0;

                    DataTable datCompanyDetails = MobjClsCommonUtility.FillCombos(new string[] { "CompanyID,ParentID", "CompanyMaster", "CompanyID = " + cboCompany.SelectedValue.ToInt32() });
                    if (datCompanyDetails.Rows[0]["ParentID"].ToInt32() != 0)
                    {
                        intCompanyCurrency = MobjClsCommonUtility.FillCombos(new string[] { "CurrencyId", "CompanyMaster", "CompanyID = " + datCompanyDetails.Rows[0]["ParentID"].ToInt32() }).Rows[0]["CurrencyId"].ToInt32();
                    }
                    else
                        intCompanyCurrency = cboCurrency.SelectedValue.ToInt32();

                    DataTable datCurrencyDetails = MobjClsCommonUtility.FillCombos(new string[] { "ExchangeRate", "CurrencyDetails", "CompanyID = " + cboCompany.SelectedValue.ToInt32() + " And CurrencyID = " + intCompanyCurrency + " Order By ExchangeRate desc" });
                    if (datCurrencyDetails.Rows.Count > 0)
                    {
                        decBalanceAmount = datVendorDetails.Rows[0]["Balance"].ToDecimal() * datCurrencyDetails.Rows[0]["ExchangeRate"].ToDecimal();
                    }

                }

                //decimal decOldNetAmount = 0;
                //DataTable datInvoiceDetails = MobjClsCommonUtility.FillCombos(new string[] { "NetAmount", "InvSalesOrderMaster", "SalesOrderID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID });
                //if (datInvoiceDetails.Rows.Count > 0)
                //    decOldNetAmount = datInvoiceDetails.Rows[0]["NetAmount"].ToDecimal();
                if (datVendorDetails.Rows[0]["CreditLimit"].ToDecimal() != 0 && ((decBalanceAmount) + txtTotalAmount.Text.ToDecimal() > (datVendorDetails.Rows[0]["CreditLimit"].ToDecimal())))
                {
                    if (MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 2077, out MmessageIcon).Replace("#", "").Replace("*", datVendorDetails.Rows[0]["VendorName"].ToString()), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                }
            }
            else
            {
                //if (!MblnRejectFlag && !MobjclsBLLSTSales.GetCompanyAccount(Convert.ToInt32(TransactionTypes.CashSale)) && MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SInvoiceCancelled)
                //{
                //    //MsMessageCommon = "Please create a Sales account for the selected company.";
                //    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2079, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmrSales.Enabled = true;
                //    return false;
                //}
            }
            if (cboAccount.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2089, out MmessageIcon);
                ErrSales.SetError(cboAccount, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                cboAccount.Focus();
                return false;
            }
            if (dgvSales.Rows.Count > 0)
            {
                if (ValidateOrderGrid() == false) return false;

                int iTempID = 0;
                iTempID = CheckDuplicationInGrid();
                if (iTempID != -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1954, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    dgvSales.CurrentCell = dgvSales["ItemCode", iTempID];
                    dgvSales.Focus();
                    return false;
                }
            }

            if (dgvSales.Rows.Count > 0)
            {
                if (ValidateCutOffPriceinInvoiceGrid() == false) return false;
            }
            return true;
        }

        private bool ValidateInvoiceGrid()
        {
            for (int intICounter = 0; intICounter < dgvSales.RowCount - 1; intICounter++)
            {
                int iRowIndex = dgvSales.Rows[intICounter].Index;

                if (Convert.ToString(dgvSales.Rows[intICounter].Cells["ItemCode"].Value).Trim() == "")
                {
                    //MsMessageCommon = "Please enter Item code.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2006, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales[ItemCode.Index, iRowIndex];
                    return false;
                }
                if (Convert.ToString(dgvSales.Rows[intICounter].Cells["Quantity"].Value).Trim() == "" || Convert.ToDecimal(dgvSales.Rows[intICounter].Cells["Quantity"].Value) == 0)
                {
                    //MsMessageCommon = "Please enter Quantity.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2007, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales[Quantity.Index, iRowIndex];
                    return false;
                }
                if (Convert.ToString(dgvSales.Rows[intICounter].Cells["Rate"].Value).Trim() == "" || Convert.ToDecimal(dgvSales.Rows[intICounter].Cells["Rate"].Value) <= 0)
                {
                    //MsMessageCommon = "Please enter Rate.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2008, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales[Rate.Index, iRowIndex];
                    return false;
                }
                if (Convert.ToInt32(dgvSales.Rows[intICounter].Cells["Uom"].Tag) == 0)
                {
                    //MsMessageCommon = "Please select Uom.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2033, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales["Uom", iRowIndex];
                    return false;
                }

                if (Convert.ToInt32(dgvSales.Rows[intICounter].Cells["Discount"].Tag) == 0)
                {
                    //MsMessageCommon = "Please select Discount.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2031, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales["Discount", iRowIndex];
                    return false;
                }
                if (dgvSales.Rows[intICounter].Cells["NetAmount"].Value.ToDecimal() <= 0)
                {
                    //MsMessageCommon = "Please enter NetAmount .";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2068, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    dgvSales.Focus();
                    dgvSales.CurrentCell = dgvSales["NetAmount", iRowIndex];
                    return false;
                }
                /* Validation for Sale Rate > CutOff price*/
                decimal decCutoffRate = MobjClsCommonUtility.GetCutOffPrice(dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt32());
                decimal decCurrentSalesRate = MobjclsBLLSTSales.GetItemRateForInvoice(dgvSales.Rows[intICounter].Cells[ItemID.Index].Value.ToInt64(), dgvSales.Rows[intICounter].Cells[BatchID.Index].Value.ToInt64(), txtOrderNo.Tag.ToInt64());
                if (decCurrentSalesRate == 0)
                {
                    if (decCutoffRate > (dgvSales.Rows[intICounter].Cells[NetAmount.Index].Value.ToDecimal() / dgvSales.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal()))
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Red;
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2088);
                        lblSalesOrderstatus.Text = MstrMessageCommon;
                        dgvSales.Focus();
                        dgvSales.CurrentCell = dgvSales[Rate.Index, iRowIndex];
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " (" + decCutoffRate.ToString() + ")", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return false;
                    }
                    else
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Black;
                    }
                }
                else
                {
                    if (dgvSales.Rows[intICounter].Cells[Rate.Index].Value.ToDecimal() > decCutoffRate || dgvSales.Rows[intICounter].Cells[Rate.Index].Value.ToDecimal() == decCurrentSalesRate)
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Black;
                    }
                    else
                    {
                        dgvSales.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Red;
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2088);
                        lblSalesOrderstatus.Text = MstrMessageCommon;
                        dgvSales.Focus();
                        dgvSales.CurrentCell = dgvSales[Rate.Index, iRowIndex];
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " (" + decCutoffRate.ToString() + ")", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return false;
                    }
                }
              
                if (MblnIsFromCancellation && MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (int)OperationStatusType.SInvoiceOpen && dgvSales.Rows[intICounter].Cells["BatchID"].Value.ToInt64() != 0)
                {
                    if (MobjclsBLLSTSales.FillCombos(new string[] { "BatchNo", "InvBatchDetails", "ItemID = " + dgvSales.Rows[intICounter].Cells["ItemID"].Value.ToInt32() + " And BatchID = " + dgvSales.Rows[intICounter].Cells["BatchID"].Value.ToInt64() }).Rows.Count == 0)
                    {
                        //MsMessageCommon = "Details of batch no not exists in the system .";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2078, out MmessageIcon).Replace("ItemName", dgvSales.Rows[intICounter].Cells[ItemName.Index].Value.ToString());
                        MstrMessageCommon = MstrMessageCommon.Replace("*", dgvSales.Rows[intICounter].Cells["BatchNo"].Value.ToString());
                        MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrSales.Enabled = true;
                        dgvSales.Focus();
                        dgvSales.CurrentCell = dgvSales["BatchNo", iRowIndex];
                        return false;
                    }
                }

                if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (int)OperationStatusType.SInvoiceOpen)
                {
                    if (MobjclsBLLSTSales.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + dgvSales.Rows[iRowIndex].Cells[ItemID.Index].Value.ToInt64() }).Rows[0]["CostingMethodID"].ToInt32() == (int)CostingMethodReference.Batch)
                    {
                        if (MobjClsCommonUtility.GetStockQuantity(dgvSales.Rows[iRowIndex].Cells[ItemID.Index].Value.ToInt32(), dgvSales.Rows[iRowIndex].Cells[BatchID.Index].Value.ToInt64(), cboCompany.SelectedValue.ToInt32(), ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")) - MobjclsBLLSTSales.GetSalesInvoiceReservedQty(dgvSales.Rows[iRowIndex].Cells[ItemID.Index].Value.ToInt64(), dgvSales.Rows[iRowIndex].Cells[BatchID.Index].Value.ToInt64(), MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID, cboCompany.SelectedValue.ToInt32()) < MobjClsCommonUtility.ConvertQtyToBaseUnitQty(dgvSales.Rows[iRowIndex].Cells[Uom.Index].Tag.ToInt32(), dgvSales.Rows[iRowIndex].Cells[ItemID.Index].Value.ToInt32(), dgvSales.Rows[iRowIndex].Cells[Quantity.Index].Value.ToDecimal(), 1))
                        {
                            //MsMessageCommon = "Out of stock.";
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2081, out MmessageIcon).Replace("*", dgvSales.Rows[intICounter].Cells[ItemName.Index].Value.ToString() + "(" + dgvSales.Rows[intICounter].Cells["BatchNo"].Value.ToString() + ")");
                            //MstrMessageCommon = MstrMessageCommon.Replace("*", dgvSales.Rows[intICounter].Cells["BatchNo"].Value.ToString());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrSales.Enabled = true;
                            dgvSales.Focus();
                            dgvSales.CurrentCell = dgvSales["ItemName", iRowIndex];
                            return false;
                        }
                    }
                    else
                    {
                        if (MobjClsCommonUtility.GetStockQuantity(dgvSales.Rows[iRowIndex].Cells[ItemID.Index].Value.ToInt32(), 0, cboCompany.SelectedValue.ToInt32(), ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")) - MobjclsBLLSTSales.GetSalesInvoiceReservedQty(dgvSales.Rows[iRowIndex].Cells[ItemID.Index].Value.ToInt64(), 0, MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID, cboCompany.SelectedValue.ToInt32()) < MobjClsCommonUtility.ConvertQtyToBaseUnitQty(dgvSales.Rows[iRowIndex].Cells[Uom.Index].Tag.ToInt32(), dgvSales.Rows[iRowIndex].Cells[ItemID.Index].Value.ToInt32(), dgvSales.Rows[iRowIndex].Cells[Quantity.Index].Value.ToDecimal(), 1))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2081, out MmessageIcon).Replace("*", dgvSales.Rows[intICounter].Cells[ItemName.Index].Value.ToString());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrSales.Enabled = true;
                            dgvSales.Focus();
                            dgvSales.CurrentCell = dgvSales["ItemName", iRowIndex];
                            return false;
                        }
                    }

                }
            }
            return true;
        }

        private bool SaveSalesDetails()
        {
            dgvSales.EndEdit();

            if (SalesOrderValidation())
            {
                if (SaveSalesOrders())
                {
                    if (MblnIsFromCancellation && mlbnReopen == false)
                    {

                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2096, out MmessageIcon).Replace("#", "");
                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        TmrSales.Enabled = true;
                        return true;
                    }
                    if (MblnIsFromCancellation && mlbnReopen == true)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2097, out MmessageIcon).Replace("#", "");
                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        TmrSales.Enabled = true;
                        return true;
                    }
                    else
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon).Replace("#", "");
                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        TmrSales.Enabled = true;
                        return true;
                    }
                }
                else
                    return false;
            }
            else
                return false;

            return true;
        }

        private void SetOrderTypeComboView()
        {
            try
            {
                if ((Int32)cboOrderType.SelectedValue == (int)OperationOrderType.SOTFromQuotation || (Int32)cboOrderType.SelectedValue == (int)OperationOrderType.SOTFromSalesPro)
                {
                    cblSaleQuotationNo.Visible = false;
                    cboOrderNumbers.Visible = true;
                    cboOrderNumbers.Visible = true;
                    LoadOrderNumbers();
                    btnActions.Enabled = true;
                    if (MblnAddStatus)
                    {
                        btnDocuments.Visible = false;
                    }
                    else
                    {
                        btnDocuments.Visible = true;
                    }
                    btnAddExpense.Enabled = true;

                    // cboCurrency.Enabled = false;
                    cboCustomer.Enabled = false;
                    //btnCustomer.Enabled = false;
                    cboDiscount.Enabled = false;
                    cboPaymentTerms.Enabled = false;
                }
                else if ((int)cboOrderType.SelectedValue == (int)OperationOrderType.SOTDirect)
                {
                    cblSaleQuotationNo.Visible = false;
                    cboOrderNumbers.Visible = false;
                    btnActions.Enabled = true;
                    cboDiscount.Enabled = true;
                    btnAddExpense.Enabled = false;
                }
                else
                {
                    cblSaleQuotationNo.Visible = true;
                    cboOrderNumbers.Visible = false;
                    cboOrderNumbers.DataSource = null;
                    cboOrderNumbers.Visible = false;
                    if (MblnAddStatus)
                    {
                        btnActions.Enabled = false;
                        btnAddExpense.Enabled = false; 
                    }
                    else
                    {
                        btnAddExpense.Enabled = true;
                        btnActions.Enabled = true;
                    }
                    //cboCurrency.Enabled = true;
                    cboCustomer.Enabled = true;
                    //   btnCustomer.Enabled = true;
                    cboDiscount.Enabled = true;
                    cboPaymentTerms.Enabled = true;
                }
            }





            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SetOrderTypeComboView() " + ex.Message);
                MobjLogs.WriteLog("Error in SetOrderTypeComboView() " + ex.Message, 2);
            }
        }

        private string GetSalesPrefix()
        {
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue));

            switch (MintFromForm)
            {
                case 1:
                    return ClsCommonSettings.strSQPrefix;
                case 2:
                    return ClsCommonSettings.strSOPrefix;
                case 3:
                    return ClsCommonSettings.strSIPrefix;
            }
            return "";
        }

        public void RefreshForm()
        {
            btnSRefresh_Click(null, null);
        }

        private void SetSalesStatus()
        {
            bnSubmitForApproval.Text = bnSubmitForApproval.Tooltip = "Submitted For Approval";
            txtDescription.Visible = lblDescription.Visible = false;
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SNew)
            {
                //if (MintFromForm != 3)
                //{
                SetEnableDisable(true);
                switch (MintFromForm)
                {
                    case 2: if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromQuotation)
                        {
                            MblnIsEditable = false;
                        }
                        else
                        {
                            MblnIsEditable = true;
                        }
                        break;
                    case 3: if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SalesInvoiceFromOrder)
                        {
                            MblnIsEditable = false;
                        }
                        else
                        {
                            MblnIsEditable = true;
                        }
                        break;
                }



                //}
                //else
                //{
                //    MblnIsEditable = false;
                //    SetEnableDisable(false);
                //    cboCompany.Enabled = true;
                //    cboOrderNumbers.Enabled = true;
                //    dtpOrderDate.Enabled = true;
                //    dtpDueDate.Enabled = true;
                //    txtOrderNo.Enabled = true;
                //    btnAddExpense.Enabled = true;
                //}
                lblStatus.ForeColor = Color.Black;
                lblStatus.Text = "New";
                BindingNavigatorAddNewItem.Enabled = false;
                BindingNavigatorCancelItem.Enabled = false;
                BindingNavigatorCancelItem.Text = "Cancel";
                BindingNavigatorDeleteItem.Enabled = false;

                btnApprove.Visible = btnReject.Visible = false;
                bnSubmitForApproval.Visible = false;
                btnSuggest.Visible = btnDeny.Visible = tiSuggestions.Visible = false;

                txtDescription.Visible = lblDescription.Visible = false;

            }
            else if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SQuotationCancelled ||
                MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderCancelled ||
                MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SInvoiceCancelled)
            {
                lblStatus.ForeColor = Color.Red;
                lblStatus.Text = "Cancelled";
                BindingNavigatorCancelItem.Enabled = true;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorCancelItem.Text = "Re Open";
                btnApprove.Visible = btnReject.Visible = bnSubmitForApproval.Visible = false;
                tiSuggestions.Visible = btnSuggest.Visible = btnDeny.Visible = false;
                SetEnableDisable(false);
                btnAddExpense.Enabled = true;
                dgvSales.ReadOnly = true;
                //if (MintFromForm != 3)
                //{
                //    if (MintFromForm == 2 && cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromQuotation)
                //        MblnIsEditable = false;
                //    else
                //        MblnIsEditable = true;
                //}
                //else
                //{
                //    MblnIsEditable = false;
                //}
                switch (MintFromForm)
                {
                    case 2: if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromQuotation)
                        {
                            MblnIsEditable = false;
                        }
                        else
                        {
                            MblnIsEditable = true;
                        }
                        break;
                    case 3: if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SalesInvoiceFromOrder)
                        {
                            MblnIsEditable = false;
                        }
                        else
                        {
                            MblnIsEditable = true;
                        }
                        break;
                }
                txtDescription.Visible = lblDescription.Visible = true;
            }
            else if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SQuotationOpen ||
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderOpen ||
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SInvoiceOpen)
            {
                lblStatus.ForeColor = Color.Brown;
                lblStatus.Text = "Opened";
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
                BindingNavigatorCancelItem.Text = "Cancel";
                btnActions.Enabled = true;
                if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SInvoiceOpen)
                    bnSubmitForApproval.Visible = false;
                else
                {
                    if ((MintFromForm == 1 && ClsCommonSettings.SQApproval) || (MintFromForm == 2 && ClsCommonSettings.SOApproval))
                    {
                        bnSubmitForApproval.Text = bnSubmitForApproval.Tooltip = "Submit For Approval";
                        bnSubmitForApproval.Visible = MblnUpdatePermission;
                    }
                    else
                    {
                        bnSubmitForApproval.Visible = MblnUpdatePermission;
                        bnSubmitForApproval.Text = bnSubmitForApproval.Tooltip = "Submit";
                    }
                }
                btnApprove.Visible = false;
                btnReject.Visible = false;
                btnDeny.Visible = btnSuggest.Visible = tiSuggestions.Visible = false;
                SetEnableDisable(true);
                btnAddExpense.Enabled = true;
                switch (MintFromForm)
                {
                    case 2: if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromQuotation)
                        {
                            MblnIsEditable = false;
                        }
                        else
                        {
                            MblnIsEditable = true;
                        }
                        break;
                    case 3: if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SalesInvoiceFromOrder)
                        {
                            MblnIsEditable = false;
                            cboAccount.Enabled = false;
                        }
                        else
                        {
                            MblnIsEditable = true;
                            cboAccount.Enabled = true;
                        }
                        break;
                }
                //if (MintFromForm != 3)
                //{
                //    if (MintFromForm == 2 && cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromQuotation)
                //        MblnIsEditable = false;
                //    else
                //        MblnIsEditable = true;
                //}
                //else
                //{
                //    MblnIsEditable = false;
                //}
                txtDescription.Visible = lblDescription.Visible = false;
            }
            else if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SQuotationClosed ||
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderClosed ||
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SInvoiceClosed)
            {
                lblStatus.ForeColor = Color.Purple;
                lblStatus.Text = "Closed";
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorCancelItem.Text = "Cancel";
                BindingNavigatorCancelItem.Enabled = false;
                btnApprove.Visible = btnReject.Visible = bnSubmitForApproval.Visible = false;
                tiSuggestions.Visible = btnSuggest.Visible = btnDeny.Visible = false;
                SetEnableDisable(false);
                //btnAddExpense.Enabled = true;
                lblSalesOrderstatus.Text = "Closed items cann't be Deleted.";
                MblnIsEditable = false;
            }
            else if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderSubmittedForApproval || MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SQuotationSubmittedForApproval)
            {
                lblStatus.ForeColor = Color.BlueViolet;
                lblStatus.Text = "Submitted For Approval";
                BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
                BindingNavigatorCancelItem.Text = "Cancel";
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                bnSubmitForApproval.Visible = false;
                if (PblnIsFromApproval)
                {
                    // btnApprove.Visible = btnReject.Visible = true;
                    if (PintApprovalPermission == (int)ApprovePermission.Suggest || PintApprovalPermission == (int)ApprovePermission.Both)
                    {
                        btnSuggest.Visible = btnDeny.Visible = true;
                        tiSuggestions.Visible = true;
                    }
                    if (PintApprovalPermission == (int)ApprovePermission.Approve || PintApprovalPermission == (int)ApprovePermission.Both)
                        btnApprove.Visible = btnReject.Visible = true;
                }
                else
                {
                    btnApprove.Visible = btnReject.Visible = false;
                    btnSuggest.Visible = btnDeny.Visible = tiSuggestions.Visible = false;
                }
                SetEnableDisable(false);
                btnAddExpense.Enabled = true;
                MblnIsEditable = false;
                txtDescription.Visible = lblDescription.Visible = false;
            }
            else if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderSubmitted || MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SQuotationSubmitted)
            {
                lblStatus.ForeColor = Color.BlueViolet;
                lblStatus.Text = "Submitted";
                BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
                BindingNavigatorCancelItem.Text = "Cancel";
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                bnSubmitForApproval.Visible = false;

                btnApprove.Visible = btnReject.Visible = false;
                btnSuggest.Visible = btnDeny.Visible = tiSuggestions.Visible = false;
                SetEnableDisable(false);
                btnAddExpense.Enabled = true;
                MblnIsEditable = false;
                txtDescription.Visible = lblDescription.Visible = false;
                if (MobjclsBLLSTSales.CheckSalesOrderExists(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID))
                {
                    BindingNavigatorCancelItem.Enabled = false;
                }
            }
            else if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SQuotationRejected ||
               MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderRejected)
            {
                lblStatus.ForeColor = Color.Red;
                lblStatus.Text = "Rejected";
                BindingNavigatorCancelItem.Text = "Cancel";
                BindingNavigatorCancelItem.Enabled = true;
                btnActions.Enabled = true;
                btnApprove.Visible = false;
                btnReject.Visible = false;
                btnDeny.Visible = btnSuggest.Visible = tiSuggestions.Visible = false;
                if (MintSalesID == 0 && PintInvoiceID == 0)
                {
                    if ((MintFromForm == 1 && ClsCommonSettings.SQApproval) || (MintFromForm == 2 && ClsCommonSettings.SOApproval))
                    {
                        bnSubmitForApproval.Visible = MblnUpdatePermission;
                        bnSubmitForApproval.Text = bnSubmitForApproval.Tooltip = "Submit For Approval";
                    }
                    else
                    {
                        bnSubmitForApproval.Visible = MblnUpdatePermission;
                        bnSubmitForApproval.Text = bnSubmitForApproval.Tooltip = "Submit";
                    }
                }
                BindingNavigatorSaveItem.Enabled = true;
                SetEnableDisable(true);
                btnAddExpense.Enabled = true;
                if (MintFromForm != 3)
                {
                    if (MintFromForm == 2 && cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromQuotation)
                        MblnIsEditable = false;
                    else
                        MblnIsEditable = true;
                }
                else
                {
                    MblnIsEditable = false;
                }
                txtDescription.Visible = lblDescription.Visible = true;
            }
            else if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SQuotationApproved ||
                MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderApproved)
            {
                lblStatus.ForeColor = Color.Green;
                lblStatus.Text = "Approved";
                BindingNavigatorCancelItem.Text = "Cancel";
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorCancelItem.Enabled = false;
                btnApprove.Visible = false;
                btnReject.Visible = false;
                bnSubmitForApproval.Visible = false;
                tiSuggestions.Visible = btnSuggest.Visible = btnDeny.Visible = false;
                btnActions.Enabled = true;
                if (PblnIsFromApproval)
                {
                    btnReject.Visible = true;
                }
                SetEnableDisable(false);
                btnAddExpense.Enabled = true;
                lblSalesOrderstatus.Text = "Approved items cann't be Deleted.";
                MblnIsEditable = false;
                txtDescription.Visible = lblDescription.Visible = true;
            }
            else if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SDelivered)
            {
                lblStatus.ForeColor = Color.Purple;
                lblStatus.Text = "Delivered";
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorCancelItem.Text = "Cancel";
                BindingNavigatorCancelItem.Enabled = false;
                btnActions.Enabled = true;
                bnSubmitForApproval.Visible = btnApprove.Visible = btnReject.Visible = false;
                btnSuggest.Visible = btnDeny.Visible = tiSuggestions.Visible = false;
                lblSalesOrderstatus.Text = "Delivered items cann't be Deleted.";
                SetEnableDisable(false);
                btnAddExpense.Enabled = true;
                MblnIsEditable = false;
                txtDescription.Visible = lblDescription.Visible = false;
            }

            if (MintSalesID != 0)
            {
                MblnIsEditable = false;
                bool blnValid = false;
                if (MintFromForm == 1)
                {
                    if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID == MintSalesID)
                    {
                        blnValid = true;
                    }
                }
                else if (MintFromForm == 2)
                {
                    if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID == MintSalesID)
                    {
                        blnValid = true;
                    }
                }
                if (blnValid)
                {
                    if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (int)OperationStatusType.SQuotationSubmittedForApproval || MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (int)OperationStatusType.SOrderSubmittedForApproval)
                    {
                        DataTable dtVerificationHistoryDetails = GetVerificationHistoryStatus(false);
                        if (dtVerificationHistoryDetails.Rows.Count > 0)
                        {
                            if ((MintFromForm == 1 && dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.SQuotationSuggested) || (MintFromForm == 2 && dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.SOrderSuggested))
                            {
                                btnSuggest.Visible = false;
                                btnDeny.Visible = true;
                            }
                            else
                            {
                                btnSuggest.Visible = true;
                                btnDeny.Visible = false;
                            }
                        }
                        else
                        {
                            if (PintApprovalPermission == (int)ApprovePermission.Suggest || PintApprovalPermission == (int)ApprovePermission.Both)
                            {
                                btnSuggest.Visible = btnDeny.Visible = true;
                                tiSuggestions.Visible = true;
                            }
                        }
                        if (PintApprovalPermission == (int)ApprovePermission.Approve || PintApprovalPermission == (int)ApprovePermission.Both)
                            btnApprove.Visible = btnReject.Visible = true;
                        // btnApprove.Visible = btnReject.Visible = true;
                    }
                    else if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (int)OperationStatusType.SOrderRejected || MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (int)OperationStatusType.SQuotationRejected)
                    {
                        //btnApprove.Visible = true;
                        DataTable dtVerificationHistoryDetails = GetVerificationHistoryStatus(false);
                        if (dtVerificationHistoryDetails.Rows.Count > 0)
                        {
                            if ((MintFromForm == 1 && dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.SQuotationSuggested) || (MintFromForm == 2 && dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.SOrderSuggested))
                            {
                                btnSuggest.Visible = false;
                                btnDeny.Visible = true;
                                tiSuggestions.Visible = true;
                            }
                            else
                            {
                                btnSuggest.Visible = true;
                                btnDeny.Visible = false;
                                tiSuggestions.Visible = true;
                            }
                        }
                        else
                        {
                            if (PintApprovalPermission == (int)ApprovePermission.Suggest || PintApprovalPermission == (int)ApprovePermission.Both)
                            {
                                tiSuggestions.Visible = true;
                                btnDeny.Visible = btnSuggest.Visible = true;
                            }
                        }
                        if (PintApprovalPermission == (int)ApprovePermission.Approve || PintApprovalPermission == (int)ApprovePermission.Both)
                            btnApprove.Visible = true;
                    }
                    else if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (int)OperationStatusType.SOrderApproved || MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (int)OperationStatusType.SQuotationApproved)
                    {
                        // btnReject.Visible = true;
                        btnDeny.Visible = btnSuggest.Visible = false;
                        tiSuggestions.Visible = false;
                        if (PintApprovalPermission == (int)ApprovePermission.Approve || PintApprovalPermission == (int)ApprovePermission.Both)
                            btnReject.Visible = true;
                    }
                }
                else
                {
                    btnApprove.Visible = btnReject.Visible = false;
                    tiSuggestions.Visible = btnSuggest.Visible = btnDeny.Visible = false;
                }
                SetEnableDisable(false);
                bnSubmitForApproval.Visible = BindingNavigatorSaveItem.Enabled = BindingNavigatorCancelItem.Enabled = BindingNavigatorClearItem.Enabled = BindingNavigatorDeleteItem.Enabled = BindingNavigatorAddNewItem.Enabled = false;
                btnAddExpense.Enabled = true;
            }
            else if (PintInvoiceID != 0)
            {
                MblnIsEditable = false;
                SetEnableDisable(false);
                bnSubmitForApproval.Visible = btnApprove.Visible = btnReject.Visible = BindingNavigatorSaveItem.Enabled = BindingNavigatorCancelItem.Enabled = BindingNavigatorClearItem.Enabled = BindingNavigatorDeleteItem.Enabled = BindingNavigatorAddNewItem.Enabled = false;
                btnSuggest.Visible = btnDeny.Visible = tiSuggestions.Visible = false;
                btnAddExpense.Enabled = true;
            }
        }

        private DataTable GetVerificationHistoryStatus(bool blnAllUsers)
        {
            string strCondition = "";

            switch (MintFromForm)
            {
                case 1:
                    strCondition += "VH.OperationTypeID = " + (int)OperationType.SalesQuotation + " And VH.ReferenceID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID + " And VH.StatusID In (" + (int)OperationStatusType.SQuotationSuggested + "," + (int)OperationStatusType.SQuotationDenied + ")";
                    if (!blnAllUsers)
                        strCondition += " And VH.VerifiedBy = " + ClsCommonSettings.UserID;
                    break;
                case 2:
                    strCondition += "VH.OperationTypeID = " + (int)OperationType.SalesOrder + " And VH.ReferenceID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID + " And VH.StatusID In (" + (int)OperationStatusType.SOrderSuggested + "," + (int)OperationStatusType.SOrderDenied + ")";
                    if (!blnAllUsers)
                        strCondition += " And VH.VerifiedBy = " + ClsCommonSettings.UserID;
                    break;
            }
            return MobjclsBLLSTSales.FillCombos(new string[] { "VH.StatusID,Convert(varchar(10),VH.VerifiedDate,103) as VerifiedDate,UM.UserName,UM.UserID,ST.Status as Status,VH.Remarks as Comment", strTableVerificationHistory + " VH Inner Join " + strTableUserMaster + " UM On UM.UserID = VH.VerifiedBy Inner Join CommonStatusReference ST On ST.StatusID = VH.StatusID", strCondition });
        }


        private void CancelSales()
        {
            try
            {
                int intStatusID = 0;
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2055);
                if (MintFromForm == 1)
                {
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation");
                    DataTable datQuotation = MobjclsBLLSTSales.FillCombos(new string[] { "StatusID", strTableSalesQuotation, "SalesQuotationID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID });
                    if (datQuotation.Rows.Count > 0)
                        intStatusID = datQuotation.Rows[0]["StatusID"].ToInt32();
                }
                else if (MintFromForm == 2)
                {
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");
                    DataTable datOrder = MobjclsBLLSTSales.FillCombos(new string[] { "StatusID", strTableSalesOrder, "SalesOrderID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID });
                    if (datOrder.Rows.Count > 0)
                        intStatusID = datOrder.Rows[0]["StatusID"].ToInt32();
                }

                if (MintFromForm != 3 && intStatusID != (int)OperationStatusType.SOrderOpen && intStatusID != (int)OperationStatusType.SOrderSubmittedForApproval && intStatusID != (int)OperationStatusType.SOrderSubmitted && intStatusID != (int)OperationStatusType.SOrderRejected && intStatusID != (int)OperationStatusType.SQuotationOpen && intStatusID != (int)OperationStatusType.SQuotationSubmittedForApproval && intStatusID != (int)OperationStatusType.SQuotationSubmitted && intStatusID != (int)OperationStatusType.SQuotationRejected)
                {
                    MessageBox.Show(MstrMessageCommon.Replace("*", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
              if (Convert.ToInt32(cboPaymentTerms.SelectedValue) == (int)PaymentTerms.Credit)
                {
                    if (MobjclsBLLSTSales.FillCombos(new string[] { "Payments.ReceiptAndPaymentID", strTableReceiptAndPayment, "Payments.OperationTypeID = " + (int)OperationType.SalesOrder + " And RPD.ReferenceID =" + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID + " And Payments.ReceiptAndPaymentTypeID = " + (int)PaymentTypes.AdvancePayment + "" }).Rows.Count > 0)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2115, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrSales.Enabled = true;
                        return;
                    }
                }
              if (MobjclsBLLSTSales.CheckSalesOrderExists(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID))
              {
                  MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2116, out MmessageIcon).Replace("approved", "submitted");
                  ErrSales.SetError(cboOrderNumbers, MstrMessageCommon.Replace("#", "").Trim());
                  MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                  lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                  TmrSales.Enabled = true;
                  return;

              }
                using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                {
                    objFrmCancellationDetails.ShowDialog();
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.strCancellationDate = objFrmCancellationDetails.PDtCancellationDate.ToString("dd-MMM-yyyy");
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.strDescription = objFrmCancellationDetails.PStrDescription;
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.intCancelledBy = ClsCommonSettings.UserID;
                    if (!objFrmCancellationDetails.PBlnIsCanelled)
                    {
                        switch (MintFromForm)
                        {
                            case 1:
                                MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SQuotationCancelled;
                                break;
                            case 2:
                                MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SOrderCancelled;
                                break;
                            case 3:
                                MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SInvoiceCancelled;
                                break;
                        }
                        txtDescription.Text = objFrmCancellationDetails.PStrDescription;
                        lblStatus.ForeColor = Color.Red;
                        lblStatus.Text = "Cancelled";
                        MobjclsBLLSTSales.clsDTOSTSalesMaster.blnCancelled = true;
                        MblnIsFromCancellation = true;
                        if (SaveSalesDetails())
                        {
                            clsBLLAlertMoment objclsBLLAlertMoment = new clsBLLAlertMoment();
                            switch (MintFromForm)
                            {
                                case 1:
                                    objclsBLLAlertMoment.DeleteAlert(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID.ToInt32(), (int)AlertSettingsTypes.SalesQuotationSubmitted);
                                    objclsBLLAlertMoment.DeleteAlert(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID.ToInt32(), (int)AlertSettingsTypes.SalesQuotationApproved);
                                    break;
                                case 2:
                                    objclsBLLAlertMoment.DeleteAlert(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID.ToInt32(), (int)AlertSettingsTypes.SaleOrderSubmitted);
                                    objclsBLLAlertMoment.DeleteAlert(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID.ToInt32(), (int)AlertSettingsTypes.SaleOrderApproved);
                                    objclsBLLAlertMoment.DeleteAlert(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID.ToInt32(), (int)AlertSettingsTypes.SaleOrderRejected);
                                    objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.SaleOrderCreated;
                                    objFrmTradingMain.tmrAlert.Enabled = true;
                                    break;
                                default:
                                    break;
                            }
                            SetSalesStatus();
                            MblnIsFromCancellation = false;
                            BindingNavigatorCancelItem.Text = "Re Open";
                            BindingNavigatorSaveItem.Enabled = false;
                            //AddNew();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CancelSales() " + ex.Message);
                MobjLogs.WriteLog("Error in CancelSales() " + ex.Message, 2);
            }
        }

        private void ReopenSales()
        {
            try
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    switch (MintFromForm)
                    {
                        case 1:
                            MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SQuotationOpen;
                            break;
                        case 2:
                            if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.SOTFromQuotation)
                            {
                                if (ClsCommonSettings.SQApproval)
                                {
                                    if (MobjclsBLLSTSales.FillCombos(new string[] { "SalesQuotationID", strTableSalesQuotation, "SalesQuotationID = " + Convert.ToInt64(cboOrderNumbers.SelectedValue) + " And StatusID = " + (Int32)OperationStatusType.SQuotationApproved }).Rows.Count == 0)
                                    {
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2046, out MmessageIcon);
                                        ErrSales.SetError(cboOrderNumbers, MstrMessageCommon.Replace("#", "").Trim());
                                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                        TmrSales.Enabled = true;
                                        cboOrderNumbers.Focus();
                                        return;
                                    }
                                }
                                else
                                {
                                    if (MobjclsBLLSTSales.FillCombos(new string[] { "SalesQuotationID", strTableSalesQuotation, "SalesQuotationID = " + Convert.ToInt64(cboOrderNumbers.SelectedValue) + " And StatusID = " + (Int32)OperationStatusType.SQuotationSubmitted }).Rows.Count == 0)
                                    {
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2046, out MmessageIcon).Replace("approved", "Submitted Status");
                                        ErrSales.SetError(cboOrderNumbers, MstrMessageCommon.Replace("#", "").Trim());
                                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                        TmrSales.Enabled = true;
                                        cboOrderNumbers.Focus();
                                        return;
                                    }
                                }
                               
                            }
                            if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.SOTFromDeliveryNote)
                            {
                                if (MobjclsBLLSTSales.CheckWhetherSalesInvoiceExists(strCondition1))
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2116, out MmessageIcon).Replace("approved", "submitted");
                                    ErrSales.SetError(cboOrderNumbers, MstrMessageCommon.Replace("#", "").Trim());
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrSales.Enabled = true;
                                    return;

                                }
                                if (MobjclsBLLSTSales.CheckWhetherSOExists(strCondition1))
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2116, out MmessageIcon).Replace("approved", "submitted");
                                    ErrSales.SetError(cboOrderNumbers, MstrMessageCommon.Replace("#", "").Trim());
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrSales.Enabled = true;
                                    return;

                                }
                            }
                            MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SOrderOpen;

                            if ((MobjclsBLLSTSales.FillCombos(new string[] { "VendorID ", "InvVendorInformation", "VendorID =" + MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID + " and StatusID = " + (int)VendorStatus.CustomerBlocked })).Rows.Count > 0)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2114, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrSales.Enabled = true;
                                return;
                            }
                            break;
                        
                    }
                    txtDescription.Text = "";

                    MobjclsBLLSTSales.clsDTOSTSalesMaster.IlstClsDTOSTSalesDetails = new List<clsDTOSTSalesOrderDetails>();
                    lblStatus.ForeColor = Color.Brown;
                    lblStatus.Text = "Opened";
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.blnCancelled = false;
                    MblnIsFromCancellation = true;
                    if (SaveSalesDetails())
                    {
                        objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.SaleOrderCreated;
                        objFrmTradingMain.tmrAlert.Enabled = true;
                        MblnIsFromCancellation = false;
                        SetSalesStatus();
                        BindingNavigatorCancelItem.Text = "Cancel";
                        BindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
                        BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                        //AddNew();
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in ReopenSales() " + ex.Message);
                MobjLogs.WriteLog("Error in ReopenSales() " + ex.Message, 2);
            }
        }

        private bool ValidateCompanyAccount()
        {

            if (cboPaymentTerms.SelectedValue.ToInt32() == (int)PaymentTerms.Credit)
            {
                if (!MobjclsBLLSTSales.GetCompanyAccount(Convert.ToInt32(TransactionTypes.CreditSale)))
                {
                    //MsMessageCommon = "Please create a Sales account for the selected company.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2034, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    return false;
                }
            }
            else
            {
                if (!MobjclsBLLSTSales.GetCompanyAccount(Convert.ToInt32(TransactionTypes.CashSale)))
                {
                    //MsMessageCommon = "Please create a Sales account for the selected company.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2079, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    return false;
                }
            }

            //if (!MobjclsBLLSTSales.GetCompanyAccount(Convert.ToInt32(TransactionTypes.CreditSale)))
            //{
            //    //MsMessageCommon = "Please create a Sales account for the selected company.";
            //    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2034, out MmessageIcon);
            //    MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            //    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            //    TmrSales.Enabled = true;
            //    return false;
            //}
            return true;
        }

        private void AddExpense()
        {
            try
            {
                using (frmExpense objfrmExpense = new frmExpense(false))
                {
                    objfrmExpense.intModuleID = 2;
                    bool blnCalculateExpense = true;
                    int intOperationTypeID = 0;
                    long lngReferenceID = 0;
                    switch (MintFromForm)
                    {
                        case 1:
                            intOperationTypeID = Convert.ToInt32(OperationType.SalesQuotation);
                            lngReferenceID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID;
                            if (MintSalesID != 0 || PintInvoiceID != 0)
                                objfrmExpense.PBlnIsEditable = false;
                            break;
                        case 2:
                            if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.SOTFromQuotation && Convert.ToInt64(cboOrderNumbers.SelectedValue) != 0 && MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID == 0)
                            {
                                objfrmExpense.PBlnIsEditable = false;
                                blnCalculateExpense = false;
                                intOperationTypeID = Convert.ToInt32(OperationType.SalesQuotation);
                                lngReferenceID = Convert.ToInt64(cboOrderNumbers.SelectedValue);
                            }
                            else
                            {
                                intOperationTypeID = Convert.ToInt32(OperationType.SalesOrder);
                                lngReferenceID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID;
                                if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.SOTFromQuotation || MintSalesID != 0 || PintInvoiceID != 0)
                                    objfrmExpense.PBlnIsEditable = false;
                                else
                                    objfrmExpense.PBlnIsEditable = true;
                            }
                            break;
                        case 3:

                            if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID == 0)
                            {
                                blnCalculateExpense = false;
                                objfrmExpense.PBlnIsEditable = false;
                                intOperationTypeID = Convert.ToInt32(OperationType.SalesOrder);
                                lngReferenceID = Convert.ToInt64(cboOrderNumbers.SelectedValue);
                            }
                            else
                            {
                                intOperationTypeID = Convert.ToInt32(OperationType.SalesInvoice);
                                lngReferenceID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID;
                                objfrmExpense.PBlnIsEditable = false;
                            }
                            break;
                    }

                    objfrmExpense.PintCompanyID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID;
                    objfrmExpense.PintOperationID = lngReferenceID;
                    objfrmExpense.PintOperationType = intOperationTypeID;

                    objfrmExpense.ShowDialog();
                    if (blnCalculateExpense)
                        txtExpenseAmount.Text = objfrmExpense.GetExpenseAmount(intOperationTypeID, lngReferenceID, 0).ToString("F" + MintExchangeCurrencyScale);
                    CalculateNetTotal();
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandAmount = Convert.ToDecimal(txtSubtotal.Text);
                   
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountAmt  = Convert.ToDecimal(txtDiscount.Text);
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.decNetAmount = Convert.ToDecimal(txtTotalAmount.Text);
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.decNetAmountRounded = Math.Ceiling(MobjclsBLLSTSales.clsDTOSTSalesMaster.decNetAmount);
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.decExpenseAmount = txtExpenseAmount.Text.ToDecimal();
                    MobjclsBLLSTSales.UpdateNetAmount(MintFromForm);
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in AddExpense() " + ex.Message);
                MobjLogs.WriteLog("Error in AddExpense() " + ex.Message, 2);
            }
        }

        private void ShowDocumentMaster()
        {
            using (frmDocumentMaster objfrmDocumentMaster = new frmDocumentMaster())
            {

                int intOperationTypeID = 0;//,intStatusID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID;
                //  string strOperationNo = txtOrderNo.Text.Trim();
                long lngReferenceID = 0;
                if (MintFromForm == 1)
                {
                    intOperationTypeID = (int)OperationType.SalesQuotation;
                    lngReferenceID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID;
                    if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SQuotationOpen && MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SQuotationRejected && MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SQuotationCancelled)
                        objfrmDocumentMaster.MblnEditable = false;
                }
                else if (MintFromForm == 2)
                {
                    intOperationTypeID = (int)OperationType.SalesOrder;
                    lngReferenceID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID;

                    if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SOrderOpen && MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SOrderRejected && MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SOrderCancelled)
                        objfrmDocumentMaster.MblnEditable = false;
                }
                else if (MintFromForm == 3)
                {
                    intOperationTypeID = (int)OperationType.SalesInvoice;
                    lngReferenceID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID;
                    if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SInvoiceOpen && MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SInvoiceCancelled)
                        objfrmDocumentMaster.MblnEditable = false;
                }

                objfrmDocumentMaster.PintOperationType = intOperationTypeID;
                objfrmDocumentMaster.PlngOperationID = lngReferenceID;
                objfrmDocumentMaster.PstrOperationNo = txtOrderNo.Text.Trim();
                objfrmDocumentMaster.PintOperationStatus = MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID;
                if (PintInvoiceID != 0 || MintSalesID != 0)
                    objfrmDocumentMaster.MblnEditable = false;
                objfrmDocumentMaster.sStatus = lblStatus.Text;
                objfrmDocumentMaster.FlagWhere = 2;
                objfrmDocumentMaster.ShowDialog();
            }
        }

        private void ShowPrint()
        {
            FrmReportviewer ObjViewer = new FrmReportviewer();
            ObjViewer.PsFormName = this.Text;
            switch (MintFromForm)
            {
                case 1:
                    ObjViewer.PiRecId = Convert.ToInt64(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID);
                    ObjViewer.PiFormID = (int)FormID.SalesQuotation;
                    break;
                case 2:
                    ObjViewer.PiRecId = Convert.ToInt64(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID);
                    ObjViewer.PiFormID = (int)FormID.SalesOrder;
                    break;
                case 3:
                    ObjViewer.PiRecId = Convert.ToInt64(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID);
                    ObjViewer.PiFormID = (int)FormID.SalesInvoice;
                    break;
            }
            ObjViewer.ShowDialog();
        }

        private void ShowEmailPopUp()
        {
            using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
            {
                switch (MintFromForm)
                {
                    case 1:
                        ObjEmailPopUp.MsSubject = "Sales Quotation Information";
                        ObjEmailPopUp.EmailFormType = EmailFormID.SalesQuotation;
                        ObjEmailPopUp.EmailSource = MobjclsBLLSTSales.GetSalesQuotationReport();
                        break;

                    case 2:
                        ObjEmailPopUp.MsSubject = "Sales Order Information";
                        ObjEmailPopUp.EmailFormType = EmailFormID.SalesOrder;
                        ObjEmailPopUp.EmailSource = MobjclsBLLSTSales.GetSalesOrderReport();
                        break;

                    case 3:
                        ObjEmailPopUp.MsSubject = "Sales Invoice Information";
                        ObjEmailPopUp.EmailFormType = EmailFormID.SalesInvoice;
                        ObjEmailPopUp.EmailSource = MobjclsBLLSTSales.GetSalesInvoiceReport();
                        break;
                }
                ObjEmailPopUp.ShowDialog();
            }
        }

        private int CheckDuplicationInGrid()
        {
            int intItemID = 0; bool blnIsGroup = false;
            long lngBatchID = 0;
            int RowIndexTemp = 0;
            bool blnCheckBatchID = false;

            foreach (DataGridViewRow rowValue in dgvSales.Rows)
            {
                if (rowValue.Cells["ItemID"].Value != null)
                {
                    intItemID = Convert.ToInt32(rowValue.Cells["ItemID"].Value);
                    blnIsGroup = Convert.ToBoolean(rowValue.Cells["IsGroup"].Value);
                    lngBatchID = rowValue.Cells["BatchID"].Value.ToInt64();
                    RowIndexTemp = rowValue.Index;
                    foreach (DataGridViewRow row in dgvSales.Rows)
                    {
                        if (RowIndexTemp != row.Index)
                        {
                            if (row.Cells["ItemID"].Value != null && row.Cells["BatchID"].Value.ToInt64() != 0)
                            {
                                if ((Convert.ToInt32(row.Cells["ItemID"].Value) == intItemID) && (row.Cells["BatchID"].Value.ToInt64() == lngBatchID))
                                    return row.Index;
                            }
                            else if (row.Cells["ItemID"].Value != null)
                            {
                                if ((Convert.ToInt32(row.Cells["ItemID"].Value) == intItemID) && Convert.ToBoolean(row.Cells["IsGroup"].Value) == blnIsGroup)
                                    return row.Index;
                            }
                        }
                    }
                }
            }
            return -1;
        }

        private void CalculateExchangeCurrencyRate()
        {
            try
            {
                int intCompanyID = 0, intCurrencyID = 0;

                intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                intCurrencyID = Convert.ToInt32(cboCurrency.SelectedValue);

                if (MobjclsBLLSTSales.GetExchangeCurrencyRate(intCompanyID, intCurrencyID))
                {
                    txtExchangeCurrencyRate.Text = Convert.ToDecimal(MobjclsBLLSTSales.clsDTOSTSalesMaster.decExchangeCurrencyRate * Convert.ToDecimal(txtTotalAmount.Text)).ToString("F" + MintBaseCurrencyScale);
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CalculateExchangeCurrencyRate() " + ex.Message);
                MobjLogs.WriteLog("Error in CalculateExchangeCurrencyRate() " + ex.Message, 2);
            }
        }

        private void SendSaleOrderApproveSMS(long lngVendorID)
        {
            StringBuilder sbrMessage;
            clsSendmail clsSendSMSMail;
            string strToAddress = "", strMobileNo = "";

            try
            {
                this.MobjclsBLLSTSales.GetMailSetting();
                if (this.MobjclsBLLSTSales.objclsDTOMailSetting == null) return;

                strMobileNo = this.MobjclsBLLSTSales.GetVendorMobileNo(lngVendorID);

                strToAddress = strMobileNo + "@" + this.MobjclsBLLSTSales.objclsDTOMailSetting.IncomingServer;
                sbrMessage = new StringBuilder();
                sbrMessage.Append("Your Sale Order Approved.");
                sbrMessage.Append("Sale Order No : " + this.txtOrderNo.Text);
                sbrMessage.Append("Sale Order Date : " + this.dtpOrderDate.Value.ToShortDateString());

                clsSendSMSMail = new clsSendmail();
                if (!clsSendSMSMail.IsInternet_Connected())
                    return;

                clsSendSMSMail.SendSmsMail(this.MobjclsBLLSTSales.objclsDTOMailSetting.UserName,
                                            this.MobjclsBLLSTSales.objclsDTOMailSetting.PassWord,
                                            this.MobjclsBLLSTSales.objclsDTOMailSetting.OutgoingServer,
                                            this.MobjclsBLLSTSales.objclsDTOMailSetting.PortNumber,
                                            this.MobjclsBLLSTSales.objclsDTOMailSetting.UserName,
                                            strToAddress, "Sale Order Confirmation",
                                            sbrMessage.ToString(),
                                            this.MobjclsBLLSTSales.objclsDTOMailSetting.EnableSsl);
            }
            catch (Exception Ex)
            {
                MobjLogs.WriteLog("Error on filling sending sms:SendSMS " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on SendSMS() " + Ex.Message.ToString());
            }
        }

        private void SendSaleOrderRejectSMS(long lngVendorID)
        {
            StringBuilder sbrMessage;
            clsSendmail clsSendSMSMail;
            string strToAddress = "", strMobileNo = "";

            try
            {
                this.MobjclsBLLSTSales.GetMailSetting();
                if (this.MobjclsBLLSTSales.objclsDTOMailSetting == null) return;

                strMobileNo = this.MobjclsBLLSTSales.GetVendorMobileNo(lngVendorID);

                strToAddress = strMobileNo + "@" + this.MobjclsBLLSTSales.objclsDTOMailSetting.IncomingServer;
                sbrMessage = new StringBuilder();
                sbrMessage.Append("Your Sale Order Rejected.");
                sbrMessage.Append("Sale Order No : " + this.txtOrderNo.Text);
                sbrMessage.Append("Sale Order Date : " + this.dtpOrderDate.Value.ToShortDateString());

                clsSendSMSMail = new clsSendmail();
                if (!clsSendSMSMail.IsInternet_Connected())
                    return;

                clsSendSMSMail.SendSmsMail(this.MobjclsBLLSTSales.objclsDTOMailSetting.UserName,
                                            this.MobjclsBLLSTSales.objclsDTOMailSetting.PassWord,
                                            this.MobjclsBLLSTSales.objclsDTOMailSetting.OutgoingServer,
                                            this.MobjclsBLLSTSales.objclsDTOMailSetting.PortNumber,
                                            this.MobjclsBLLSTSales.objclsDTOMailSetting.UserName,
                                            strToAddress, "Sale Order Confirmation",
                                            sbrMessage.ToString(),
                                            this.MobjclsBLLSTSales.objclsDTOMailSetting.EnableSsl);
            }
            catch (Exception Ex)
            {
                MobjLogs.WriteLog("Error on filling sending sms:SendSMS " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on SendSMS() " + Ex.Message.ToString());
            }
        }


        private void DisplaySalesDetDetails(int intDisplayMode, Int64 intQuotationID)
        {
            try
            {
                dgvSales.Rows.Clear();
                dgvSales.RowCount = 1;
                DataTable datSalesDetail = null;

                switch (intDisplayMode)
                {
                    case 1:
                        if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromSalesPro)
                            datSalesDetail = MobjclsBLLSTSales.GetSalesProQuotationDetDetails(intQuotationID);
                    else

                        datSalesDetail = MobjclsBLLSTSales.GetQuotationDetDetails(intQuotationID);

                        //  DataTable datJobOrderDetails = MobjclsBLLSTSales.FillCombos(new string[] { "ItemID,IsNull(BatchID,0) as BatchID,IsFromStore", "PrdJobOrderProducts", "JobOrderID = " + cboOrderNumbers.SelectedValue.ToInt64() });

                        if (dgvSales.Rows.Count > 0)
                        {
                            for (int intICounter = 0; intICounter < datSalesDetail.Rows.Count; intICounter++)
                            {
                                int intItemID = Convert.ToInt32(datSalesDetail.Rows[intICounter]["ItemID"]);
                                int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                                FillDataGridCombo(intItemID, intVendorID, Convert.ToBoolean(datSalesDetail.Rows[intICounter]["IsGroup"]));

                                dgvSales.RowCount = dgvSales.RowCount + 1;
                                dgvSales.Rows[intICounter].Cells["IsGroup"].Value = Convert.ToInt32(datSalesDetail.Rows[intICounter]["IsGroup"]);
                                dgvSales.Rows[intICounter].Cells["ItemID"].Value = datSalesDetail.Rows[intICounter]["ItemID"];
                                dgvSales.Rows[intICounter].Cells["BatchID"].Value = datSalesDetail.Rows[intICounter]["BatchID"];
                                dgvSales.Rows[intICounter].Cells["BatchNo"].Value = datSalesDetail.Rows[intICounter]["BatchNo"];
                                dgvSales.Rows[intICounter].Cells["ItemCode"].Value = datSalesDetail.Rows[intICounter]["ItemCode"];
                                dgvSales.Rows[intICounter].Cells["ItemName"].Value = datSalesDetail.Rows[intICounter]["Description"];
                                dgvSales.Rows[intICounter].Cells["Uom"].Value = datSalesDetail.Rows[intICounter]["UOMID"];
                                dgvSales.Rows[intICounter].Cells["Uom"].Tag = datSalesDetail.Rows[intICounter]["UOMID"];
                                dgvSales.Rows[intICounter].Cells["Uom"].Value = dgvSales.Rows[intICounter].Cells["Uom"].FormattedValue;

                                int intUomScale = 0;
                                if (dgvSales[Uom.Index, intICounter].Tag.ToInt32() != 0)
                                    intUomScale = MobjclsBLLSTSales.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvSales[Uom.Index, intICounter].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                                dgvSales.Rows[intICounter].Cells["Quantity"].Value = datSalesDetail.Rows[intICounter]["Quantity"].ToDecimal().ToString("F" + intUomScale);
                                dgvSales.Rows[intICounter].Cells["Rate"].Value = datSalesDetail.Rows[intICounter]["Rate"];
                                dgvSales.Rows[intICounter].Cells["GrandAmount"].Value = datSalesDetail.Rows[intICounter]["GrandAmount"];
                                dgvSales.Rows[intICounter].Cells["NetAmount"].Value = datSalesDetail.Rows[intICounter]["NetAmount"];

                                if (datSalesDetail.Rows[intICounter]["IsDiscountPercentage"] != DBNull.Value)
                                {
                                    if (datSalesDetail.Rows[intICounter]["IsDiscountPercentage"].ToBoolean())
                                    {
                                        dgvSales.Rows[intICounter].Cells["Discount"].Value = "%";
                                        dgvSales.Rows[intICounter].Cells["DiscountAmount"].Value = datSalesDetail.Rows[intICounter]["DiscountPercentage"].ToDecimal();
                                        dgvSales.Rows[intICounter].Cells["DiscAmount"].Value = datSalesDetail.Rows[intICounter]["DiscountAmount"].ToDecimal();
                                    }
                                    else
                                    {
                                        dgvSales.Rows[intICounter].Cells["Discount"].Value = "Amt";
                                        dgvSales.Rows[intICounter].Cells["DiscountAmount"].Value = datSalesDetail.Rows[intICounter]["DiscountAmount"].ToDecimal();
                                        dgvSales.Rows[intICounter].Cells["DiscAmount"].Value = datSalesDetail.Rows[intICounter]["DiscountAmount"].ToDecimal();
                                    }
                                }                                
                            }
                        }
                        break;
                    case 2:
                        datSalesDetail = MobjclsBLLSTSales.GetOrderDetDetails(intQuotationID, 3);

                        if (dgvSales.Rows.Count > 0)
                        {
                            for (int intICounter = 0; intICounter < datSalesDetail.Rows.Count; intICounter++)
                            {
                                Int32 intItemID = Convert.ToInt32(datSalesDetail.Rows[intICounter]["ItemID"]);
                                int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                                FillDataGridCombo(intItemID, intVendorID, Convert.ToBoolean(datSalesDetail.Rows[intICounter]["IsGroup"]));

                                dgvSales.RowCount = dgvSales.RowCount + 1;
                                dgvSales.Rows[intICounter].Cells["ItemID"].Value = datSalesDetail.Rows[intICounter]["ItemID"];
                                dgvSales.Rows[intICounter].Cells["IsGroup"].Value = Convert.ToInt32(datSalesDetail.Rows[intICounter]["IsGroup"]);
                                dgvSales.Rows[intICounter].Cells["ItemCode"].Value = datSalesDetail.Rows[intICounter]["ItemCode"];
                                dgvSales.Rows[intICounter].Cells["ItemName"].Value = datSalesDetail.Rows[intICounter]["Description"];
                                dgvSales.Rows[intICounter].Cells["BatchID"].Value = datSalesDetail.Rows[intICounter]["BatchID"];
                                dgvSales.Rows[intICounter].Cells["BatchNo"].Value = datSalesDetail.Rows[intICounter]["BatchNo"];
                                dgvSales.Rows[intICounter].Cells["Uom"].Value = datSalesDetail.Rows[intICounter]["UOMID"];
                                dgvSales.Rows[intICounter].Cells["Uom"].Tag = datSalesDetail.Rows[intICounter]["UOMID"];
                                dgvSales.Rows[intICounter].Cells["Uom"].Value = dgvSales.Rows[intICounter].Cells["Uom"].FormattedValue;

                                int intUomScale = 0;
                                if (dgvSales[Uom.Index, intICounter].Tag.ToInt32() != 0)
                                    intUomScale = MobjclsBLLSTSales.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvSales[Uom.Index, intICounter].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                                dgvSales.Rows[intICounter].Cells["Quantity"].Value = datSalesDetail.Rows[intICounter]["Quantity"].ToDecimal().ToString("F" + intUomScale);
                                dgvSales.Rows[intICounter].Cells["Rate"].Value = datSalesDetail.Rows[intICounter]["Rate"];
                                dgvSales.Rows[intICounter].Cells["GrandAmount"].Value = datSalesDetail.Rows[intICounter]["GrandAmount"];
                                dgvSales.Rows[intICounter].Cells["NetAmount"].Value = datSalesDetail.Rows[intICounter]["NetAmount"];

                                if (datSalesDetail.Rows[intICounter]["IsDiscountPercentage"] != DBNull.Value)
                                {
                                    if (Convert.ToBoolean(datSalesDetail.Rows[intICounter]["IsDiscountPercentage"]))
                                    {
                                        dgvSales.Rows[intICounter].Cells["Discount"].Value = "%";
                                        dgvSales.Rows[intICounter].Cells["DiscountAmount"].Value = datSalesDetail.Rows[intICounter]["DiscountPercentage"].ToDecimal();
                                        dgvSales.Rows[intICounter].Cells["DiscAmount"].Value = datSalesDetail.Rows[intICounter]["DiscountAmount"].ToDecimal();
                                    }
                                    else
                                    {
                                        dgvSales.Rows[intICounter].Cells["Discount"].Value = "Amt";
                                        dgvSales.Rows[intICounter].Cells["DiscountAmount"].Value = datSalesDetail.Rows[intICounter]["DiscountAmount"].ToDecimal();
                                        dgvSales.Rows[intICounter].Cells["DiscAmount"].Value = datSalesDetail.Rows[intICounter]["DiscountAmount"].ToDecimal();
                                    }
                                }
                            }
                        }
                        break;
                    case 3:

                        datSalesDetail = MobjclsBLLSTSales.GetSalesInvoiceDetDetails(intQuotationID, Convert.ToInt32(cboOrderType.SelectedValue));

                        if (dgvSales.Rows.Count > 0)
                        {
                            for (int intICounter = 0; intICounter < datSalesDetail.Rows.Count; intICounter++)
                            {
                                Int32 intItemID = Convert.ToInt32(datSalesDetail.Rows[intICounter]["ItemID"]);
                                int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                                FillDataGridCombo(intItemID, intVendorID, Convert.ToBoolean(datSalesDetail.Rows[intICounter]["IsGroup"]));

                                dgvSales.RowCount = dgvSales.RowCount + 1;
                                dgvSales.Rows[intICounter].Cells["ItemID"].Value = datSalesDetail.Rows[intICounter]["ItemID"];
                                dgvSales.Rows[intICounter].Cells["IsGroup"].Value = Convert.ToInt32(datSalesDetail.Rows[intICounter]["IsGroup"]);
                                dgvSales.Rows[intICounter].Cells["ItemCode"].Value = datSalesDetail.Rows[intICounter]["ItemCode"];
                                dgvSales.Rows[intICounter].Cells["ItemName"].Value = datSalesDetail.Rows[intICounter]["Description"];
                                dgvSales.Rows[intICounter].Cells["BatchID"].Value = datSalesDetail.Rows[intICounter]["BatchID"];
                                dgvSales.Rows[intICounter].Cells["BatchNo"].Value = datSalesDetail.Rows[intICounter]["BatchNo"];
                                dgvSales.Rows[intICounter].Cells["Uom"].Value = datSalesDetail.Rows[intICounter]["UOMID"];
                                dgvSales.Rows[intICounter].Cells["Uom"].Tag = datSalesDetail.Rows[intICounter]["UOMID"];
                                dgvSales.Rows[intICounter].Cells["Uom"].Value = dgvSales.Rows[intICounter].Cells["Uom"].FormattedValue;

                                int intUomScale = 0;
                                if (dgvSales[Uom.Index, intICounter].Tag.ToInt32() != 0)
                                    intUomScale = MobjclsBLLSTSales.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvSales[Uom.Index, intICounter].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                                dgvSales.Rows[intICounter].Cells["Quantity"].Value = datSalesDetail.Rows[intICounter]["Quantity"].ToDecimal().ToString("F" + intUomScale);

                                dgvSales.Rows[intICounter].Cells["Rate"].Value = datSalesDetail.Rows[intICounter]["Rate"];
                                dgvSales.Rows[intICounter].Cells["OrderedQty"].Value = datSalesDetail.Rows[intICounter]["OrderedQty"];
                                dgvSales.Rows[intICounter].Cells["InvoicedQty"].Value = datSalesDetail.Rows[intICounter]["InvoicedQty"];
                                dgvSales.Rows[intICounter].Cells["GrandAmount"].Value = datSalesDetail.Rows[intICounter]["GrandAmount"];
                                dgvSales.Rows[intICounter].Cells["NetAmount"].Value = datSalesDetail.Rows[intICounter]["NetAmount"];

                                if (datSalesDetail.Rows[intICounter]["IsDiscountPercentage"] != DBNull.Value)
                                {
                                    if (datSalesDetail.Rows[intICounter]["IsDiscountPercentage"].ToBoolean())
                                    {
                                        dgvSales.Rows[intICounter].Cells["Discount"].Value = "%";
                                        dgvSales.Rows[intICounter].Cells["DiscountAmount"].Value = datSalesDetail.Rows[intICounter]["DiscountPercentage"].ToDecimal();
                                        dgvSales.Rows[intICounter].Cells["DiscAmount"].Value = datSalesDetail.Rows[intICounter]["DiscountAmount"].ToDecimal();
                                    }
                                    else
                                    {
                                        dgvSales.Rows[intICounter].Cells["Discount"].Value = "Amount";
                                        dgvSales.Rows[intICounter].Cells["DiscountAmount"].Value = datSalesDetail.Rows[intICounter]["DiscountAmount"].ToDecimal();
                                        dgvSales.Rows[intICounter].Cells["DiscAmount"].Value = datSalesDetail.Rows[intICounter]["DiscountAmount"].ToDecimal();
                                    }
                                }
                            }
                        }
                        break;
                    case 4:
                        datSalesDetail = MobjclsBLLSTSales.GetJobOrderItemDetails(intQuotationID);

                        if (dgvSales.Rows.Count > 0)
                        {
                            for (int intICounter = 0; intICounter < datSalesDetail.Rows.Count; intICounter++)
                            {
                                int intItemID = Convert.ToInt32(datSalesDetail.Rows[intICounter]["ItemID"]);
                                int intVendorID = Convert.ToInt32(datSalesDetail.Rows[intICounter]["VendorID"]);
                                cboCustomer.SelectedValue = intVendorID;
                                cboDiscount.SelectedValue = (int)PredefinedDiscounts.DefaultSalesDiscount;
                                txtDiscount.Text = datSalesDetail.Rows[intICounter]["Discount"].ToStringCustom();

                                dtpOrderDate.Value = ClsCommonSettings.GetServerDate();
                                dtpDueDate.Value = datSalesDetail.Rows[intICounter]["ExpectedDeliveryDate"].ToDateTime();

                                MintVendorAddID = Convert.ToInt32(datSalesDetail.Rows[intICounter]["VendorAddID"]);
                                FillDataGridCombo(intItemID, intVendorID, true);

                                dgvSales.RowCount = dgvSales.RowCount + 1;
                                dgvSales.Rows[intICounter].Cells["ItemID"].Value = datSalesDetail.Rows[intICounter]["ItemID"];
                                dgvSales.Rows[intICounter].Cells["IsGroup"].Value = false;
                                dgvSales.Rows[intICounter].Cells["ItemCode"].Value = datSalesDetail.Rows[intICounter]["ItemCode"];
                                dgvSales.Rows[intICounter].Cells["ItemName"].Value = datSalesDetail.Rows[intICounter]["Description"];
                                dgvSales.Rows[intICounter].Cells[BatchID.Index].Value = datSalesDetail.Rows[intICounter]["BatchID"];
                                dgvSales.Rows[intICounter].Cells[BatchNo.Index].Value = datSalesDetail.Rows[intICounter]["BatchNo"];

                                //  if (datSalesDetail.Rows[intICounter]["IsFromStore"].ToBoolean())
                                //   dgvSales.Rows[intICounter].Cells[IsFromStore.Index].Value = 1;
                                // else

                                dgvSales.Rows[intICounter].Cells[IsAutofilled.Index].Value = 1;

                                dgvSales.Rows[intICounter].Cells["Uom"].Value = datSalesDetail.Rows[intICounter]["UOMID"];
                                dgvSales.Rows[intICounter].Cells["Uom"].Tag = datSalesDetail.Rows[intICounter]["UOMID"];
                                dgvSales.Rows[intICounter].Cells["Uom"].Value = dgvSales.Rows[intICounter].Cells["Uom"].FormattedValue;

                                int intUomScale = 0;
                                if (dgvSales[Uom.Index, intICounter].Tag.ToInt32() != 0)
                                    intUomScale = MobjclsBLLSTSales.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvSales[Uom.Index, intICounter].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                                dgvSales.Rows[intICounter].Cells["Quantity"].Value = datSalesDetail.Rows[intICounter]["Quantity"].ToDecimal().ToString("F" + intUomScale);

                                dgvSales.Rows[intICounter].Cells["Rate"].Value = datSalesDetail.Rows[intICounter]["Rate"];
                                dgvSales.Rows[intICounter].Cells["GrandAmount"].Value = datSalesDetail.Rows[intICounter]["Amount"];
                                dgvSales.Rows[intICounter].Cells["NetAmount"].Value = datSalesDetail.Rows[intICounter]["Amount"];

                                FillDiscountColumn(intICounter, true);
                            }


                        }
                        break;
                }
                MblnSearchFlag = false;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplaySalesDetDetails() " + ex.Message);
                MobjLogs.WriteLog("Error in DisplaySalesDetDetails() " + ex.Message, 2);
            }
        }




        //private void DisplaySalesDetDetails(int intDisplayMode, Int64 intQuotationID)
        //{
        //    try
        //    {
        //        dgvSales.Rows.Clear();
        //        dgvSales.RowCount = 1;
        //        DataTable datSalesDetail = null;

        //                datSalesDetail = MobjclsBLLSTSales.GetOrderDetDetails(intQuotationID, 3);

        //                if (dgvSales.Rows.Count > 0)
        //                {
        //                    for (int intICounter = 0; intICounter < datSalesDetail.Rows.Count; intICounter++)
        //                    {
        //                        Int32 intItemID = Convert.ToInt32(datSalesDetail.Rows[intICounter]["ItemID"]);
        //                        int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
        //                        FillDataGridCombo(intItemID, intVendorID, Convert.ToBoolean(datSalesDetail.Rows[intICounter]["IsGroup"]));

        //                        dgvSales.RowCount = dgvSales.RowCount + 1;
        //                        dgvSales.Rows[intICounter].Cells["ItemID"].Value = datSalesDetail.Rows[intICounter]["ItemID"];
        //                        dgvSales.Rows[intICounter].Cells["IsGroup"].Value = Convert.ToInt32(datSalesDetail.Rows[intICounter]["IsGroup"]);
        //                        dgvSales.Rows[intICounter].Cells["ItemCode"].Value = datSalesDetail.Rows[intICounter]["ItemCode"];
        //                        dgvSales.Rows[intICounter].Cells["ItemName"].Value = datSalesDetail.Rows[intICounter]["Description"];
        //                        // dgvSales.Rows[intICounter].Cells["Quantity"].Value = datSalesDetail.Rows[intICounter]["Quantity"];
        //                        dgvSales.Rows[intICounter].Cells["BatchID"].Value = datSalesDetail.Rows[intICounter]["BatchID"];
        //                        dgvSales.Rows[intICounter].Cells["BatchNo"].Value = datSalesDetail.Rows[intICounter]["BatchNo"];
        //                        dgvSales.Rows[intICounter].Cells["Uom"].Value = datSalesDetail.Rows[intICounter]["UOMID"];
        //                        dgvSales.Rows[intICounter].Cells["Uom"].Tag = datSalesDetail.Rows[intICounter]["UOMID"];
        //                        dgvSales.Rows[intICounter].Cells["Uom"].Value = dgvSales.Rows[intICounter].Cells["Uom"].FormattedValue;
        //                        int intUomScale = 0;
        //                        if (dgvSales[Uom.Index, intICounter].Tag.ToInt32() != 0)
        //                            intUomScale = MobjclsBLLSTSales.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvSales[Uom.Index, intICounter].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

        //                        dgvSales.Rows[intICounter].Cells["Quantity"].Value = datSalesDetail.Rows[intICounter]["Quantity"].ToDecimal().ToString("F" + intUomScale);
        //                        dgvSales.Rows[intICounter].Cells["Rate"].Value = datSalesDetail.Rows[intICounter]["Rate"];
        //                        dgvSales.Rows[intICounter].Cells["GrandAmount"].Value = datSalesDetail.Rows[intICounter]["GrandAmount"];
        //                        dgvSales.Rows[intICounter].Cells["NetAmount"].Value = datSalesDetail.Rows[intICounter]["NetAmount"];

        //                        //FillDiscountColumn(intICounter, true);


        //                        if (datSalesDetail.Rows[intICounter]["IsDiscountPercentage"] != DBNull.Value)
        //                        {
        //                            dgvSales.Rows[intICounter].Cells["DiscountAmt"].Value = (Convert.ToBoolean(datSalesDetail.Rows[intICounter]["IsDiscountPercentage"]) ? datSalesDetail.Rows[intICounter]["DiscountPercentage"].ToDecimal() / 100 * datSalesDetail.Rows[intICounter]["GrandAmount"].ToDecimal() : datSalesDetail.Rows[intICounter]["DiscountAmount"].ToDecimal()); ;
        //                            dgvSales.Rows[intICounter].Cells["DiscountAmount"].Value = (Convert.ToBoolean(datSalesDetail.Rows[intICounter]["IsDiscountPercentage"]) ? datSalesDetail.Rows[intICounter]["DiscountPercentage"].ToDecimal() : datSalesDetail.Rows[intICounter]["DiscountAmount"].ToDecimal()); ;
        //                            dgvSales.Rows[intICounter].Cells["Discount"].Value = (Convert.ToBoolean(datSalesDetail.Rows[intICounter]["IsDiscountPercentage"]) ? "%" : "Amt");
        //                        }

                             
        //                    }
        //                }
                        
        //        MblnSearchFlag = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ClsCommonSettings.ShowErrorMess)
        //            MessageBox.Show("Error in DisplaySalesDetDetails() " + ex.Message);
        //        MobjLogs.WriteLog("Error in DisplaySalesDetDetails() " + ex.Message, 2);
        //    }
        //}










        //private bool DisplaySalesOrderDetails(int intDisplayMode, Int64 intOrderID)
        //{
        //    try
        //    {
        //        switch (intDisplayMode)
        //        {
        //            case 1:
        //                if (MobjclsBLLSTSales.GetQuotationMasterDetails(intOrderID))
        //                {
        //                    cboOrderType.Tag = "DoNotChange";

        //                    txtOrderNo.Tag = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID;
        //                    cboCompany.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID;
        //                    if (cboCompany.SelectedValue == null)
        //                    {
        //                        DataTable datTemp = MobjclsBLLSTSales.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID });
        //                        if (datTemp.Rows.Count > 0)
        //                            cboCompany.Text = datTemp.Rows[0]["Name"].ToString();
        //                    }
        //                    cboCustomer.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID;
        //                    if (cboCustomer.SelectedValue == null)
        //                        cboCustomer.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVendorName;

        //                    MintVendorAddID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID;

        //                    lblVendorAddress.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strAddressName + " Address";

        //                    cboPaymentTerms.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intPaymentTermsID;
        //                    cboDiscount.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intGrandDiscountID;
        //                    MintCurrencyID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID;
        //                    cboCurrency.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID;
        //                    txtOrderNo.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strSalesQuotationNo;


        //                    dtpOrderDate.Value = Convert.ToDateTime(MobjclsBLLSTSales.clsDTOSTSalesMaster.strQuotationDate);

        //                    txtRemarks.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strRemarks;
        //                    txtExpenseAmount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decExpenseAmount.ToString("F" + MintExchangeCurrencyScale);
        //                    txtDiscount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountAmount.ToString("F" + MintExchangeCurrencyScale);
        //                    txtSubtotal.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandAmount.ToString("F" + MintExchangeCurrencyScale);
        //                    txtTotalAmount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decNetAmount.ToString("F" + MintExchangeCurrencyScale);
        //                    lblCreatedByText.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strCreatedBy;
        //                    dtpCreatedDate.Value = MobjclsBLLSTSales.clsDTOSTSalesMaster.strCreatedDate.ToDateTime();

        //                    DisplayAddress();
        //                    DisplaySalesDetDetails(intDisplayMode, intOrderID);

        //                    SetSalesStatus();
        //                    cboOrderType.Tag = "";
        //                    BindingNavigatorSaveItem.Enabled = false;
        //                    return true;
        //                }
        //                break;

        //            case 2:
        //                if (MobjclsBLLSTSales.GetOrderMasterDetails(intOrderID))
        //                {
        //                    cboOrderType.Tag = "DoNotChange";
        //                    txtOrderNo.Tag = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID;
        //                    cboCompany.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID;
        //                    if (cboCompany.SelectedValue == null)
        //                    {
        //                        DataTable datTemp = MobjclsBLLSTSales.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID });
        //                        if (datTemp.Rows.Count > 0)
        //                            cboCompany.Text = datTemp.Rows[0]["Name"].ToString();
        //                    }
        //                    cboCustomer.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID;
        //                    if (cboCustomer.SelectedValue == null)
        //                        cboCustomer.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVendorName;
        //                    AddToAddressMenu();
        //                    MintVendorAddID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID;
        //                    lblVendorAddress.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strAddressName + " Address";

        //                    cboPaymentTerms.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intPaymentTermsID;


        //                    cboOrderType.SelectedValue = Convert.ToInt32(MobjclsBLLSTSales.clsDTOSTSalesMaster.intOrderTypeID);
        //                    if (cboOrderType.SelectedValue == null)
        //                    {
        //                        DataTable datTemp = MobjclsBLLSTSales.FillCombos(new string[] { "*", "CommonOrderTypeReference", "OrderTypeID=" + Convert.ToInt32(MobjclsBLLSTSales.clsDTOSTSalesMaster.intOrderTypeID) });
        //                        if (datTemp.Rows.Count > 0)
        //                            cboOrderType.Text = datTemp.Rows[0]["Description"].ToString();
        //                    }

        //                    SetOrderTypeComboView();

        //                    cboOrderNumbers.SelectedValue = Convert.ToInt64(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID);
        //                    cboDiscount.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intGrandDiscountID;
        //                    MintCurrencyID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID;
        //                    cboCurrency.SelectedValue = MintCurrencyID;
        //                    MintShipAddID = Convert.ToInt32(MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID);

        //                    txtOrderNo.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strSalesOrderNo;

        //                    dtpOrderDate.Value = Convert.ToDateTime(MobjclsBLLSTSales.clsDTOSTSalesMaster.strOrderDate);


        //                    txtRemarks.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strRemarks;

        //                    if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderCancelled)
        //                    {
        //                        txtDescription.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strDescription;
        //                        dtpCancelledDate.Value = Convert.ToDateTime(MobjclsBLLSTSales.clsDTOSTSalesMaster.strCancellationDate);
        //                        lblCancelledByText.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strCancelledBy;
        //                    }
        //                  //  lblCancelledDate.Text = "Cancelled Date";
        //                    if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderRejected)
        //                    {
        //                        txtDescription.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVerificationDescription;
        //                        dtpCancelledDate.Value = Convert.ToDateTime(MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedDate);
        //                        lblCancelledByText.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedBy;
        //                    //    lblCancelledDate.Text = "Rejected Date";
        //                    }
        //                    else if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderApproved)
        //                    {
        //                        txtDescription.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVerificationDescription;
        //                        dtpCancelledDate.Value = Convert.ToDateTime(MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedDate);
        //                        lblCancelledByText.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedBy;
        //                     //   lblCancelledDate.Text = "Approved Date";
        //                    }
        //                    lblCreatedByText.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strCreatedBy;
        //                    dtpCreatedDate.Value = MobjclsBLLSTSales.clsDTOSTSalesMaster.strCreatedDate.ToDateTime();
        //                    DisplayAddress();
        //                    DisplaySalesDetDetails(intDisplayMode, intOrderID);

        //                    txtExpenseAmount.Text = MobjclsBLLSTSales.GetExpenseAmount().ToString("F" + MintExchangeCurrencyScale);
        //                    CalculateTotalAmt();
        //                    CalculateNetTotal();
        //                    BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
        //                    SetSalesStatus();
        //                    btnDocuments.Visible = true;
        //                    cboOrderType.Tag = "";
        //                    txtAdvPayment.Text = MobjclsBLLSTSales.GetAdvancePayment(Convert.ToInt64(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID)).ToString("F" + MintExchangeCurrencyScale);
        //                }
        //                break;
        //            case 3:
        //                if (MobjclsBLLSTSales.GetOrderMasterDetails(intOrderID))
        //                {
        //                    cboOrderType.Tag = "DoNotChange";
        //                    cboCompany.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID;
        //                    if (cboCompany.SelectedValue == null)
        //                    {
        //                        DataTable datTemp = MobjclsBLLSTSales.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID });
        //                        if (datTemp.Rows.Count > 0)
        //                            cboCompany.Text = datTemp.Rows[0]["Name"].ToString();
        //                    }
        //                    LoadOrderNumbers();
        //                    cboOrderNumbers.SelectedValue = intOrderID;
        //                    txtOrderNo.Tag = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID;

        //                    cboCustomer.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID;
        //                    if (cboCustomer.SelectedValue == null)
        //                        cboCustomer.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVendorName;
        //                    MintVendorAddID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID;



        //                    lblVendorAddress.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strAddressName + " Address";

        //                    cboPaymentTerms.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intPaymentTermsID;
        //                    cboDiscount.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intGrandDiscountID;
        //                    MintCurrencyID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID;
        //                    cboCurrency.SelectedValue = MintCurrencyID;
        //                    MintShipAddID = Convert.ToInt32(MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID);

        //                    GenerateOrderNo();

        //                    dtpOrderDate.Value = Convert.ToDateTime(MobjclsBLLSTSales.clsDTOSTSalesMaster.strOrderDate);


        //                    MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SNew;
        //                    lblCreatedByText.Text = ClsCommonSettings.strEmployeeName;
        //                    dtpCreatedDate.Value = ClsCommonSettings.GetServerDate();

        //                    txtRemarks.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strRemarks;

        //                    SetSalesStatus();

        //                    DisplayAddress();
        //                    DisplaySalesDetDetails(2, intOrderID);

        //                    txtExpenseAmount.Text = MobjclsBLLSTSales.GetExpenseAmount().ToString("F" + MintExchangeCurrencyScale);
        //                    CalculateTotalAmt();
        //                    CalculateNetTotal();

        //                    cboOrderType.Tag = "";
        //                    BindingNavigatorSaveItem.Enabled = MblnAddPermission;
        //                    dgvSales.ReadOnly = true;
        //                    btnActions.Enabled = true;
        //                    btnAddExpense.Enabled = true;
        //                    cboDiscount.Enabled = false;
        //                    txtAdvPayment.Text = MobjclsBLLSTSales.GetAdvancePayment(Convert.ToInt64(cboOrderNumbers.SelectedValue)).ToString("F" + MintExchangeCurrencyScale);
        //                    return true;
        //                }
        //                break;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ClsCommonSettings.ShowErrorMess)
        //            MessageBox.Show("Error in DisplaySalesOrderDetails() " + ex.Message);
        //        MobjLogs.WriteLog("Error in DisplaySalesOrderDetails() " + ex.Message, 2);
        //    }
        //    return false;
        //}

        //private bool DisplaySalesQuotationDetails(Int64 intQuotationID)
        //{
        //    try
        //    {
        //        if (MobjclsBLLSTSales.GetQuotationMasterDetails(intQuotationID))
        //        {
        //            cboCustomer.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID;
        //            if (cboCustomer.SelectedValue == null)
        //                cboCustomer.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVendorName;
        //            cboCustomer.Enabled = false;
        //            btnCustomer.Enabled = false;
        //            btnAddressChange.Enabled = false;
        //            // cboCurrency.Enabled = false;
        //            cboDiscount.Enabled = false;
        //            cboPaymentTerms.Enabled = false;

        //            MintVendorAddID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID;

        //            lblVendorAddress.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strAddressName + " Address";

        //            cboPaymentTerms.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intPaymentTermsID;
        //            cboDiscount.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intGrandDiscountID;
        //            MintCurrencyID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID;
        //            cboCurrency.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID;


        //            dtpOrderDate.Value = Convert.ToDateTime(MobjclsBLLSTSales.clsDTOSTSalesMaster.strQuotationDate);

        //            dtpDueDate.Value = Convert.ToDateTime(MobjclsBLLSTSales.clsDTOSTSalesMaster.strDueDate);

        //            lblCreatedByText.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strCreatedBy;
        //            dtpCreatedDate.Value = MobjclsBLLSTSales.clsDTOSTSalesMaster.strCreatedDate.ToDateTime();
        //            txtExpenseAmount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decExpenseAmount.ToString("F" + MintExchangeCurrencyScale);
        //            DisplayAddress();
        //            DisplaySalesDetDetails(1, intQuotationID);
        //            return true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ClsCommonSettings.ShowErrorMess)
        //            MessageBox.Show("Error in DisplaySalesQuotationDetails() " + ex.Message);
        //        MobjLogs.WriteLog("Error in DisplaySalesQuotationDetails() " + ex.Message, 2);
        //    }
        //    return false;
        //}

        private bool DisplaySalesDetails(int intDisplayMode, Int64 intOrderID)
        {
            try
            {
                switch (intDisplayMode)
                {
                    case 1:
                        if (MobjclsBLLSTSales.GetQuotationMasterDetails(intOrderID))
                        {
                            txtOrderNo.Tag = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID;
                            cboCompany.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID;
                            clsBLLLogin mobjLogin = new clsBLLLogin();
                            mobjLogin.GetCompanySettings(cboCompany.SelectedValue.ToInt32());
                            if (cboCompany.SelectedValue == null)
                            {
                                DataTable datTemp = MobjclsBLLSTSales.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID });
                                if (datTemp.Rows.Count > 0)
                                    cboCompany.Text = datTemp.Rows[0]["CompanyName"].ToString();
                            }
                            cboCustomer.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID;
                            if (cboCustomer.SelectedValue == null)
                                cboCustomer.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVendorName;
                            AddToAddressMenu();
                            MintVendorAddID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID;
                            lblVendorAddress.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strAddressName + " Address";

                            cboPaymentTerms.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intPaymentTermsID;

                            cboOrderType.SelectedValue = Convert.ToInt32(MobjclsBLLSTSales.clsDTOSTSalesMaster.intOrderTypeID);
                            if (cboOrderType.SelectedValue == null)
                            {
                                DataTable datTemp = MobjclsBLLSTSales.FillCombos(new string[] { "*", "CommonOrderTypeReference", "OrderTypeID=" + Convert.ToInt32(MobjclsBLLSTSales.clsDTOSTSalesMaster.intOrderTypeID) });
                                if (datTemp.Rows.Count > 0)
                                    cboOrderType.Text = datTemp.Rows[0]["OrderType"].ToString();
                            }
                            SetOrderTypeComboView();
                            MintCurrencyID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID;
                            cboCurrency.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID;
                            txtOrderNo.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strSalesQuotationNo;


                            dtpOrderDate.Value = Convert.ToDateTime(MobjclsBLLSTSales.clsDTOSTSalesMaster.strQuotationDate);


                            dtpDueDate.Value = Convert.ToDateTime(MobjclsBLLSTSales.clsDTOSTSalesMaster.strDueDate);

                            txtRemarks.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strRemarks;
                            //  lblCancelledDate.Text = "Cancelled Date";

                            lblCreatedByText.Text = "     Created By : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strCreatedBy;
                            lblCreatedDateValue.Text = "     Created Date : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strCreatedDate.ToDateTime().ToString("dd-MMM-yyyy");

                            if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SQuotationCancelled)
                            {
                                txtDescription.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strDescription;

                                lblCreatedByText.Text += "     |     Cancelled By : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strCancelledBy;
                                lblCreatedDateValue.Text += "     |     Cancelled Date : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strCancellationDate.ToDateTime().ToString("dd-MMM-yyyy");
                            }

                            if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SQuotationRejected)
                            {
                                txtDescription.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVerificationDescription;
                                //lblCancelledDate.Text = "Rejected Date";

                                lblCreatedByText.Text += "     |     Rejected By : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedBy;
                                lblCreatedDateValue.Text += "     |     Rejected Date : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedDate.ToDateTime().ToString("dd-MMM-yyyy");
                            }
                            else if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SQuotationApproved)
                            {
                                txtDescription.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVerificationDescription;
                                // lblCancelledDate.Text = "Approved Date";

                                lblCreatedByText.Text += "     |     Approved By : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedBy;
                                lblCreatedDateValue.Text += "     |     Approved Date : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedDate.ToDateTime().ToString("dd-MMM-yyyy");
                            }

                            DisplayAddress();
                            DisplaySalesDetDetails(intDisplayMode, intOrderID);

                            txtExpenseAmount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decExpenseAmount.ToString("F" + MintExchangeCurrencyScale);
                            CalculateTotalAmt();
                            CalculateNetTotal();
                            LoadCombos(5);

                            cboDiscount.SelectedIndex = (MobjclsBLLSTSales.clsDTOSTSalesMaster.blnGrandDiscount.ToBoolean() ? 0 : 1);
                            if (Convert.ToString(cboDiscount.SelectedItem) == "%")
                                txtDiscount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountPercent.ToString();
                            else
                                txtDiscount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountAmt.ToString();
                            txtTotalAmount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decNetAmount.ToString();
                            
                            
                            
                            //cboDiscount.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intGrandDiscountID;
                            //txtDiscount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountAmount.ToString("F" + MintExchangeCurrencyScale);

                            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;

                            btnAddExpense.Enabled = true;
                            btnActions.Enabled = true;
                            btnDocuments.Visible = true;
                            btnExpense.Visible = true;

                            SetSalesStatus();

                        }
                        break;
                    case 2:
                        if (MobjclsBLLSTSales.GetOrderMasterDetails(intOrderID))
                        {
                            DataTable datCombos = new DataTable();
                            cboOrderType.Tag = "DoNotChange";
                            txtOrderNo.Tag = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID;
       
                            cboCompany.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID;
                            clsBLLLogin mobjLogin = new clsBLLLogin();
                            mobjLogin.GetCompanySettings(cboCompany.SelectedValue.ToInt32());
                            if (cboCompany.SelectedValue == null)
                            {
                                DataTable datTemp = MobjclsBLLSTSales.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID });
                                if (datTemp.Rows.Count > 0)
                                    cboCompany.Text = datTemp.Rows[0]["CompanyName"].ToString();
                            }
                            DataTable datCombo = MobjclsBLLSTSales.FillCombos(new string[] { "distinct V.VendorID,V.VendorName+' - '+V.VendorCode as VendorName", "InvVendorInformation V INNER JOIN InvVendorCompanyDetails VC ON V.VendorID = VC.VendorID ", "VendorTypeID=" + (int)VendorType.Customer + " AND VC.CompanyID = " + ClsCommonSettings.CompanyID + " And StatusID = " + (int)VendorStatus.CustomerActive + " UNION SELECT VendorID,VendorName+' - '+VendorCode as VendorName FROM InvVendorInformation WHERE VendorID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID });

                           // DataTable datCombo = MobjclsBLLSTSales.FillCombos(new string[] { "VendorID,VendorName+' - '+VendorCode as VendorName", "InvVendorInformation", "VendorTypeID=" + (int)VendorType.Customer + " And StatusID = " + (int)VendorStatus.CustomerActive + " UNION SELECT VendorID,VendorName+' - '+VendorCode as VendorName FROM InvVendorInformation WHERE VendorID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID });
                            cboCustomer.ValueMember = "VendorID";
                            cboCustomer.DisplayMember = "VendorName";
                            cboCustomer.DataSource = datCombo;

                            cboTaxScheme.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intTaxSchemeID;
                            cboCustomer.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID;
                            if (cboCustomer.SelectedValue == null)
                                cboCustomer.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVendorName;
                            AddToAddressMenu();
                            MintVendorAddID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID;
                            lblVendorAddress.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strAddressName + " Address";
                            cboPaymentTerms.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intPaymentTermsID;
                            txtLpoNo.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.lopno;
                            cboOrderType.SelectedValue = Convert.ToInt32(MobjclsBLLSTSales.clsDTOSTSalesMaster.intOrderTypeID);
                            if (cboOrderType.SelectedValue == null)
                            {
                                DataTable datTemp = MobjclsBLLSTSales.FillCombos(new string[] { "*", "CommonOrderTypeReference", "OrderTypeID=" + Convert.ToInt32(MobjclsBLLSTSales.clsDTOSTSalesMaster.intOrderTypeID) });
                                if (datTemp.Rows.Count > 0)
                                    cboOrderType.Text = datTemp.Rows[0]["OrderType"].ToString();
                            }

                            SetOrderTypeComboView();

                            dtpOrderDate.Value = Convert.ToDateTime(MobjclsBLLSTSales.clsDTOSTSalesMaster.strOrderDate);
                            dtpDueDate.Value = Convert.ToDateTime(MobjclsBLLSTSales.clsDTOSTSalesMaster.strDueDate);

                            LoadOrderNumbers();
                            cboOrderNumbers.SelectedValue = Convert.ToInt64(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID);
                            if ((Int32)cboOrderType.SelectedValue == (int)OperationOrderType.SOTFromDeliveryNote)
                            {
                                FillDNoteInUpdateMode();
                                DataTable dtCombo = MobjclsBLLSTSales.GetItemIssueIDFromSalesOrder(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID);
                                //DataTable datCombos = MobjclsBLLSTSales.FillDeliveryNoteDetails(cboCompany.SelectedValue.ToInt64(), txtOrderNo.Tag.ToInt64(), cboCustomer.SelectedValue.ToInt64(), (Int64)OperationOrderType.DNOTDirect);
                                DataColumn[] keys = new DataColumn[1];
                                keys[0] = dtCombo.Columns[0];
                                dtCombo.PrimaryKey = keys;
                                for (int i = 0; i < cblSaleQuotationNo.Items.Count; i++)
                                {
                                    if (dtCombo.Rows.Contains(((System.Data.DataRowView)(cblSaleQuotationNo.Items[i])).Row.ItemArray[0]))
                                    {
                                        cblSaleQuotationNo.SetItemChecked(i, true);
                                    }
                                    else
                                    {
                                        cblSaleQuotationNo.SetItemChecked(i, false);
                                    }

                                }

                                cblSaleQuotationNo_SelectedIndexChanged(null, null);
                                MblnIsEditable = false;

                            }

                            MintCurrencyID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID;
                            cboCurrency.SelectedValue = MintCurrencyID;
                            MintShipAddID = Convert.ToInt32(MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID);

                            txtOrderNo.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strSalesOrderNo;
                            dtpOrderDate.Value = Convert.ToDateTime(MobjclsBLLSTSales.clsDTOSTSalesMaster.strOrderDate);
                            txtRemarks.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strRemarks;

                            lblCreatedByText.Text = "     Created By : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strCreatedBy;
                            lblCreatedDateValue.Text = "     Created Date : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strCreatedDate.ToDateTime().ToString("dd-MMM-yyyy");

                            if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderCancelled)
                            {
                                txtDescription.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strDescription;

                                lblCreatedByText.Text += "     |     Cancelled By : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strCancelledBy;
                                lblCreatedDateValue.Text += "     |     Cancelled Date : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strCancellationDate.ToDateTime().ToString("dd-MMM-yyyy");
                            }
                            // lblCancelledDate.Text = "Cancelled Date";
                            if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderRejected)
                            {
                                txtDescription.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVerificationDescription;
                                //  lblCancelledDate.Text = "Rejected Date";

                                lblCreatedByText.Text += "     |     Rejected By : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedBy;
                                lblCreatedDateValue.Text += "     |     Rejected Date : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedDate.ToDateTime().ToString("dd-MMM-yyyy");
                            }
                            else if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SOrderApproved)
                            {
                                txtDescription.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVerificationDescription;
                                //lblCancelledDate.Text = "Approved Date";

                                lblCreatedByText.Text += "     |     Approved By : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedBy;
                                lblCreatedDateValue.Text += "     |     Approved Date : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedDate.ToDateTime().ToString("dd-MMM-yyyy");
                            }

                            DisplayAddress();
                            DisplaySalesDetDetails(intDisplayMode, intOrderID);

                            txtExpenseAmount.Text = MobjclsBLLSTSales.GetExpenseAmount().ToString("F" + MintExchangeCurrencyScale);
                            txtTaxAmount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decTaxAmount.ToString("F" + MintExchangeCurrencyScale);
                           
                            CalculateTotalAmt();
                            CalculateNetTotal();
                            LoadCombos(5);

                            if (MobjclsBLLSTSales.clsDTOSTSalesMaster.blnIsDiscountAllowed)
                            {
                                if (MobjclsBLLSTSales.clsDTOSTSalesMaster.blnIsDiscountPercentage)
                                {
                                    cboDiscount.SelectedIndex = 0; // Percentage
                                    txtDiscount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountPercentage.ToString("F" + MintExchangeCurrencyScale);
                                    txtDiscount.Tag = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountAmount.ToString("F" + MintExchangeCurrencyScale);
                                }
                                else
                                {
                                    cboDiscount.SelectedIndex = 1; // Amount
                                    txtDiscount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountAmount.ToString("F" + MintExchangeCurrencyScale);
                                    txtDiscount.Tag = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountAmount.ToString("F" + MintExchangeCurrencyScale);
                                }
                            }
                            else
                            {
                                cboDiscount.SelectedIndex = -1; // None
                                txtDiscount.Text = 0.ToString("F" + MintExchangeCurrencyScale);
                                txtDiscount.Tag = 0;
                            }

                            txtTotalAmount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decNetAmount.ToString();
                            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                            SetSalesStatus();

                            if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intOrderTypeID == (int)OperationOrderType.SOTFromQuotation)
                            {
                               
                                cboOrderNumbers.Visible = true;
                                LoadOrderNumbers();
                                cboOrderNumbers.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID;
                                btnActions.Enabled = true;
                                if (MblnAddStatus)
                                {
                                    btnDocuments.Visible = false;
                                }
                                else
                                {
                                    btnDocuments.Visible = true;
                                }
                                btnAddExpense.Enabled = true;


                                cboCustomer.Enabled = false;

                                cboDiscount.Enabled = false;
                                cboPaymentTerms.Enabled = false;

                            }
                            else
                            {
                                cboOrderNumbers.Visible = false;
                                btnDocuments.Visible = true;
                                cboOrderType.Tag = "";
                            }

                            txtAdvPayment.Text = MobjclsBLLSTSales.GetAdvancePayment(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID).ToString("F" + MintExchangeCurrencyScale);
                            if (MobjClsCommonUtility.FillCombos(new string[] { "IsNull(Sum(RPD.Amount),0) as TotalAmount", strTableReceiptAndPayment, "Payments.OperationTypeID = " + (int)OperationType.SalesOrder + " And RPD.ReferenceID = " + cboOrderNumbers.SelectedValue.ToInt64() + " And Payments.ReceiptAndPaymentTypeID  <> " + (int)PaymentTypes.DirectExpense }).Rows[0]["TotalAmount"].ToDecimal() != 0)
                                cboPaymentTerms.Enabled = false;
                        }
                        break;
                    case 3:
                        if (MobjclsBLLSTSales.GetInvoiceMasterDetails(intOrderID))
                        {
                            cboOrderType.Tag = "DoNotChange";
                            txtOrderNo.Tag = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID;
                            cboCompany.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID;
                            clsBLLLogin mobjLogin = new clsBLLLogin();
                            mobjLogin.GetCompanySettings(cboCompany.SelectedValue.ToInt32());
                            if (cboCompany.SelectedValue == null)
                            {
                                DataTable datTemp = MobjclsBLLSTSales.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID });
                                if (datTemp.Rows.Count > 0)
                                    cboCompany.Text = datTemp.Rows[0]["CompanyName"].ToString();
                            }
                            cboCustomer.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID;
                            if (cboCustomer.SelectedValue == null)
                                cboCustomer.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVendorName;
                            AddToAddressMenu();
                            MintVendorAddID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID;
                            lblVendorAddress.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strAddressName + " Address";
                            cboPaymentTerms.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intPaymentTermsID;
                            //added by hima,when order type added
                            cboOrderType.SelectedValue = Convert.ToInt32(MobjclsBLLSTSales.clsDTOSTSalesMaster.intOrderTypeID);
                            if (cboOrderType.SelectedValue == null)
                            {
                                DataTable datTemp = MobjclsBLLSTSales.FillCombos(new string[] { "*", "CommonOrderTypeReference", "OrderTypeID=" + Convert.ToInt32(MobjclsBLLSTSales.clsDTOSTSalesMaster.intOrderTypeID) });
                                if (datTemp.Rows.Count > 0)
                                    cboOrderType.Text = datTemp.Rows[0]["OrderType"].ToString();
                            }
                            cboAccount.SelectedValue = Convert.ToInt32(MobjclsBLLSTSales.clsDTOSTSalesMaster.intAccountID);
                            if (cboAccount.SelectedValue == null)
                            {

                                DataTable datTemp = MobjclsBLLSTSales.FillCombos(new string[] { "AccountID,AccountName ", "AccAccountMaster", "AccountGroupID=" + (int)AccountGroups.SalesAccounts + "" });

                                if (datTemp.Rows.Count > 0)
                                    cboAccount.Text = datTemp.Rows[0]["AccountName"].ToString();
                            }
                            //
                            SetOrderTypeComboView();

                            MintCurrencyID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID;
                            cboCurrency.SelectedValue = MintCurrencyID;
                            txtOrderNo.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strSalesInvoiceNo;


                            dtpOrderDate.Value = Convert.ToDateTime(MobjclsBLLSTSales.clsDTOSTSalesMaster.strInvoiceDate);
                            dtpDueDate.Value = MobjclsBLLSTSales.clsDTOSTSalesMaster.strDueDate.ToDateTime();

                            txtRemarks.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strRemarks;
                            cboOrderNumbers.SelectedValue = Convert.ToInt64(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID);

                            lblCreatedByText.Text = "     Created By : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strCreatedBy;
                            lblCreatedDateValue.Text = "     Created Date : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strCreatedDate.ToDateTime().ToString("dd-MMM-yyyy");

                            if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID == (Int32)OperationStatusType.SInvoiceCancelled)
                            {
                                txtDescription.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strDescription;

                                lblCreatedByText.Text += "     |     Cancelled By : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strCancelledBy;
                                lblCreatedDateValue.Text += "     |     Cancelled Date : " + MobjclsBLLSTSales.clsDTOSTSalesMaster.strCancellationDate.ToDateTime().ToString("dd-MMM-yyyy");
                            }

                            DisplayAddress();
                            DisplaySalesDetDetails(intDisplayMode, intOrderID);

                            txtExpenseAmount.Text = MobjclsBLLSTSales.GetExpenseAmount().ToString("F" + MintExchangeCurrencyScale);
                            CalculateTotalAmt();
                            CalculateNetTotal();
                            LoadCombos(5);
                            cboDiscount.SelectedIndex = (MobjclsBLLSTSales.clsDTOSTSalesMaster.blnGrandDiscount.ToBoolean() ? 0 : 1);
                            if (Convert.ToString(cboDiscount.SelectedItem) == "%")
                                txtDiscount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountPercent.ToString();
                            else
                                txtDiscount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountAmt.ToString();
                            txtTotalAmount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decNetAmount.ToString();
                            
                            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                            SetSalesStatus();
                            //added by hima for order type
                            if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intOrderTypeID == (int)OperationOrderType.SalesInvoiceFromOrder)
                            {
                               
                                cboOrderNumbers.Visible = true;
                                LoadOrderNumbers();
                                cboOrderNumbers.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID;
                                txtAdvPayment.Text = MobjclsBLLSTSales.GetAdvancePayment(Convert.ToInt64(cboOrderNumbers.SelectedValue)).ToString("F" + MintExchangeCurrencyScale);

                                btnActions.Enabled = true;
                                if (MblnAddStatus)
                                {
                                    btnDocuments.Visible = false;
                                }
                                else
                                {
                                    btnDocuments.Visible = true;
                                }
                                btnAddExpense.Enabled = true;

                                // cboCurrency.Enabled = false;
                                //   btnCustomer.Enabled = false;
                                cboDiscount.Enabled = false;
                                cboPaymentTerms.Enabled = false;

                            }
                            cboCustomer.Enabled = false;

                            cboOrderType.Tag = "";
                            //SetEnableDisable(false);
                            dtpOrderDate.Enabled = true;
                            dtpDueDate.Enabled = true;
                        }
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplaySalesDetails() " + ex.Message);
                MobjLogs.WriteLog("Error in DisplaySalesDetails() " + ex.Message, 2);
            }
            return false;
        }

        private bool AddToAddressMenu()
        {
            try
            {
                CMSVendorAddress.Items.Clear();
                //CMSVendorAddress.Items.Add("Permanent");
                DataTable datAddMenu = MobjclsBLLSTSales.DtGetAddressName();

                for (int intICounter = 0; intICounter <= datAddMenu.Rows.Count - 1; intICounter++)
                {
                    ToolStripMenuItem tItem = new ToolStripMenuItem();
                    tItem.Tag = Convert.ToString(datAddMenu.Rows[intICounter][0]);
                    tItem.Text = Convert.ToString(datAddMenu.Rows[intICounter][1]);
                    CMSVendorAddress.Items.Add(tItem);
                }

                lblVendorAddress.Text = CMSVendorAddress.Items[0].Text + " Address";
                MintVendorAddID = Convert.ToInt32(CMSVendorAddress.Items[0].Tag);

                CMSVendorAddress.ItemClicked -= new ToolStripItemClickedEventHandler(this.MenuItem_Click);
                CMSVendorAddress.ItemClicked += new ToolStripItemClickedEventHandler(this.MenuItem_Click);
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in AddToAddressMenu() " + ex.Message);
                MobjLogs.WriteLog("Error in AddToAddressMenu() " + ex.Message, 2);
            }
            return false;
        }

        #endregion

        #region Events

        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        void FrmSalesNew_Load(object sender, EventArgs e)
        {
            bckgrndWrkrSendSms.DoWork += new DoWorkEventHandler(bckgrndWrkrSendSms_DoWork);
            bckgrndWrkrSendSms.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bckgrndWrkrSendSms_RunWorkerCompleted);
            bckgrndWrkrSendSms.WorkerSupportsCancellation = true;
        }

        /* Created By Rajesh.R
         * Created on 32-06-2011 */

        #region SendSms--Background Worker Thread
        void bckgrndWrkrSendSms_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                if (intCustomerId != 0)
                {
                    if (intSmsType == 1)
                        this.SendSaleOrderApproveSMS(intCustomerId);
                    else if (intSmsType == 2)
                        this.SendSaleOrderRejectSMS(intCustomerId);
                }
            }
        }

        void bckgrndWrkrSendSms_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (bckgrndWrkrSendSms.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                else

                    if (intCustomerId != 0)
                    {
                        if (intSmsType == 1) //SmsType =1 SaleOrder Approval ;=2 SaleOrder Reject
                            this.SendSaleOrderApproveSMS(intCustomerId);
                        else if (intSmsType == 2)
                            this.SendSaleOrderRejectSMS(intCustomerId);
                    }
            }
            catch
            {

            }
        }
        #endregion

        private void BtnItem_Click(object sender, EventArgs e)
        {
            CallProducts();
        }

        private void CallProducts()
        {
            frmItemMaster objItemMaster1 = null;

            try
            {
                objItemMaster1 = new frmItemMaster(ClsCommonSettings.PblnIsTaxable);
                objItemMaster1.Text = "Product Master " + (ClsMainSettings.objFrmMain.MdiChildren.Length + 1);
                objItemMaster1.MdiParent = ClsMainSettings.objFrmMain;
                objItemMaster1.WindowState = FormWindowState.Maximized;
                objItemMaster1.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objItemMaster1.Dispose();
                System.GC.Collect();
                CallProducts();
            }
        }

        private void BtnUom_Click(object sender, EventArgs e)
        {
            using (FrmUnitOfMeasurement objUoM = new FrmUnitOfMeasurement())
            {
                objUoM.ShowDialog();
            }
        }

        private void FrmSales_Load(object sender, EventArgs e)
        {
            try
            {
                SetPermissions();
                dtpSFrom.Value = ClsCommonSettings.GetServerDate();
                dtpSTo.Value = ClsCommonSettings.GetServerDate();
                ArrangeControls();
                MblnAddStatus = true;
                LoadMessage();

                cboOrderType.Tag = "DoNotChange";
                LoadCombos(0);//0 for All filling all Combos
                cboOrderType.Tag = "";


                if (MintSalesID != 0)
                {
                    MblnAddStatus = false;
                    SetControlsVisibility();
                    DisplaySalesDetails(MintFromForm, MintSalesID);
                    cboCompany.Enabled = cboOrderType.Enabled = cboOrderNumbers.Enabled = MblnQtyChanged = false;
                }
                else if (PintInvoiceID != 0)
                {
                    MblnAddStatus = false;
                    SetControlsVisibility();
                    DisplaySalesDetails(MintFromForm, PintInvoiceID);
                    cboCompany.Enabled = cboOrderType.Enabled = cboOrderNumbers.Enabled = MblnQtyChanged = false;
                }
                else if (lngSalesID != 0)
                {
                    MblnAddStatus = false;
                    SetControlsVisibility();
                    DisplaySalesDetails(MintFromForm, lngSalesID);
                    cboCompany.Enabled = cboOrderType.Enabled = cboOrderNumbers.Enabled = MblnQtyChanged = false;
                }
                else if (MlngDeliveryID > 0)
                {
                    BindingNavigatorCancelItem.Enabled = false;
                    BindingNavigatorDeleteItem.Enabled = false;
                    BtnEmail.Enabled = false;
                    BtnPrint.Enabled = false;
                    BindingNavigatorSaveItem.Enabled = true;
                    MblnAddStatus = true;
                    SetControlsVisibility();
                    DisplayDirectDelivery();
                    

                    
                }
                else
                {
                    SetControlsVisibility();
                    AddNew();

                    if (lngReferenceID > 0)
                    {
                        if (MintFromForm == 2)
                            cboOrderType.SelectedValue = (int)OperationOrderType.SOTFromQuotation;

                        cboOrderNumbers.SelectedValue = lngReferenceID;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in AddToAddressMenu() " + ex.Message);
                MobjLogs.WriteLog("Error in AddToAddressMenu() " + ex.Message, 2);
            }
        }

        private void TxtCbo_ValueChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void ComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void TextboxNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvSales.CurrentCell != null && (dgvSales.CurrentCell.OwningColumn.Name == "Quantity" || dgvSales.CurrentCell.OwningColumn.Name == "Rate"))
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (((TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true; 
                }
            }
        }

        private void MenuItem_Click(System.Object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(cboCustomer.SelectedValue) > 0)
                {
                    lblVendorAddress.Text = e.ClickedItem.Text + " Address";
                    MintVendorAddID = Convert.ToInt32(e.ClickedItem.Tag);
                    DisplayAddress();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in MenuItem_Click() " + ex.Message);
                MobjLogs.WriteLog("Error in MenuItem_Click() " + ex.Message, 2);
            }
        }

        private void dgvSales_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvSales.CurrentCell.ColumnIndex == 0 || dgvSales.CurrentCell.ColumnIndex == 1)
                {
                    for (int i = 0; i < dgvSales.Columns.Count; i++)
                        dgvSales.Rows[dgvSales.CurrentCell.RowIndex].Cells[i].Value = null;
                }

                dgvSales.PServerName = ClsCommonSettings.ServerName;
                string[] First = null;
                string[] second = null;
              
                First = new string[] { "IsGroup", "ItemCode", "ItemName", "BatchID", "BatchNo", "ItemID", "QtyAvailable","IsGroupItem","ProductTypeID", };//first grid 
                second = new string[] { "IsGroup", "ItemCode", "Description", "BatchID", "BatchNo", "ItemID", "QtyAvailable", "IsGroupItem", "ProductTypeID", };//inner grid
                
                dgvSales.aryFirstGridParam = First;
                dgvSales.arySecondGridParam = second;
                dgvSales.PiFocusIndex = Quantity.Index;
                dgvSales.iGridWidth = 550;
                dgvSales.bBothScrollBar = true;
                dgvSales.ColumnsToHide = new string[] { "ItemID", "IsGroup", "BatchID", "BatchNo", "ProductTypeID", "QtyAvailable" };

                if (dgvSales.CurrentCell.ColumnIndex == 0)
                {
                    dgvSales.CurrentCell.Value = dgvSales.TextBoxText;
                    dgvSales.field = "ItemCode";
                }
                else if (dgvSales.CurrentCell.ColumnIndex == 1)
                {
                    dgvSales.CurrentCell.Value = dgvSales.TextBoxText;
                    dgvSales.field = "Description";
                }

                DataTable dtDatasource = MobjclsBLLSTSales.GetDataForItemSelection(ClsCommonSettings.CompanyID, Convert.ToInt32(cboCurrency.SelectedValue), MintFromForm);
                
                string strFilterString = dgvSales.field + " Like '" + "%" + dgvSales.TextBoxText.ToUpper() + "%'";
                dtDatasource.DefaultView.RowFilter = strFilterString;
                DataTable datTemp = dtDatasource.DefaultView.ToTable();
                datTemp.DefaultView.Sort = dgvSales.field;
                dgvSales.dtDataSource = datTemp.DefaultView.ToTable();                
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvSales_Textbox_TextChanged() " + ex.Message);
                MobjLogs.WriteLog("Error in dgvSales_Textbox_TextChanged() " + ex.Message, 2);
            }
        }

        private void dgvSales_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (MblnSearchFlag == true) return;
                if (dgvSales.CurrentCell == null) return;

                if (e.ColumnIndex >= 0 && e.RowIndex >= 0 && (e.ColumnIndex == Quantity.Index || e.ColumnIndex == Rate.Index))
                {
                    //if (e.ColumnIndex == Quantity.Index)
                    //{
                    //    int intDecimalPart = 0;
                    //    if (dgvSales.CurrentCell.Value != null && !string.IsNullOrEmpty(dgvSales.CurrentCell.Value.ToString()) && dgvSales.CurrentCell.Value.ToString().Contains("."))
                    //        intDecimalPart = dgvSales.CurrentCell.Value.ToString().Split('.')[1].Length;
                    //    int intUomScale = 0;
                    //    if (dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    //        intUomScale = MobjclsBLLSTSales.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                    //    if (intDecimalPart > intUomScale)
                    //    {
                    //        dgvSales.CurrentCell.Value = Math.Round(dgvSales.CurrentCell.Value.ToDecimal(), intUomScale).ToString();
                    //        dgvSales.EditingControl.Text = dgvSales.CurrentCell.Value.ToString();
                    //    }
                    //}

                    if (dgvSales.Columns[e.ColumnIndex].Name == "Quantity")
                        MblnQtyChanged = true;
                    decimal decValue = 0;
                    try
                    {
                        decValue = Convert.ToDecimal(dgvSales[e.ColumnIndex, e.RowIndex].Value);
                    }
                    catch
                    {
                        dgvSales[e.ColumnIndex, e.RowIndex].Value = 0;
                    }
                   // FillDiscountColumn(e.RowIndex, true);
                }

                Changestatus();



                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    DataTable datSalesRate = new DataTable();

                    if (e.ColumnIndex == ItemID.Index)
                    {


                        int intUOMID = 0;
                        Int32 intItemID = Convert.ToInt32(dgvSales.Rows[e.RowIndex].Cells["ItemID"].Value);
                        int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);

                        if (intItemID != 0)
                        {
                            datSalesRate = FillDataGridCombo(intItemID, intVendorID, Convert.ToBoolean(dgvSales.Rows[e.RowIndex].Cells["IsGroup"].Value));
                            if (datSalesRate.Rows.Count !=0)
                            {
                                if (Convert.ToBoolean(dgvSales.Rows[e.RowIndex].Cells["IsGroup"].Value) == false)
                                {
                                    DataRow[] dtrSalesRate = datSalesRate.Select("ItemID=" + intItemID);
                                    FillDataGridCombo(intItemID, intVendorID, false);
                                    DataTable datDefaultUom = MobjclsBLLSTSales.FillCombos(new string[] { "DefaultSalesUomID as UomID", "InvItemMaster", "ItemID = " + intItemID + "" });
                                    intUOMID = Convert.ToInt32(datDefaultUom.Rows[0]["UomID"]);
                                    dgvSales[Rate.Index, e.RowIndex].Value = MobjClsCommonUtility.GetSaleRate(intItemID, intUOMID, dgvSales.Rows[e.RowIndex].Cells["BatchID"].Value.ToInt64(), cboCompany.SelectedValue.ToInt32());
                                }
                                else
                                {
                                    intUOMID = (int)PredefinedUOMs.Numbers;
                                    DataTable datProductGrpDetails = MobjclsBLLSTSales.FillCombos(new string[] { "SaleAmount", "InvItemGroupMaster", "ItemGroupID = " + intItemID });
                                    dgvSales[Rate.Index, e.RowIndex].Value = Convert.ToDecimal(datProductGrpDetails.Rows[0]["SaleAmount"]);
                                }
                            }
                            dgvSales.Rows[e.RowIndex].Cells["Uom"].Tag = intUOMID;
                            dgvSales.Rows[e.RowIndex].Cells["Uom"].Value = intUOMID;
                            dgvSales.Rows[e.RowIndex].Cells["Uom"].Value = dgvSales.Rows[e.RowIndex].Cells["Uom"].FormattedValue;

                            //FillDiscountColumn(e.RowIndex, false);
                        }
                        int intUomScale = 0;
                        if (dgvSales[Uom.Index, e.RowIndex].Tag.ToInt32() != 0)
                            intUomScale = MobjclsBLLSTSales.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvSales[Uom.Index, e.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                        dgvSales[Quantity.Index, e.RowIndex].Value = Convert.ToDecimal(1).ToString("F" + intUomScale);
                        CalculateTotalAmt();
                    }
                    else if (e.ColumnIndex == Rate.Index)
                    {
                        Int32 intItemID = Convert.ToInt32(dgvSales.Rows[e.RowIndex].Cells["ItemID"].Value);
                        int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                        int intUOMID = Convert.ToInt32(dgvSales.Rows[e.RowIndex].Cells["Uom"].Tag);

                        if (intItemID != 0 && MblnAddStatus)
                        {
                            //datSalesRate = FillDataGridCombo(intItemID, intVendorID, Convert.ToBoolean(dgvSales.Rows[e.RowIndex].Cells["IsGroup"].Value));
                            //DataRow[] dtrSalesRate = datSalesRate.Select("UOMID=" + intUOMID);

                            //if (dtrSalesRate.Length > 0)
                            //{
                            //    MobjclsBLLSTSales.GetExchangeCurrencyRate(Convert.ToInt32(cboCompany.SelectedValue), Convert.ToInt32(cboCurrency.SelectedValue));
                            //   // dgvSales.Rows[e.RowIndex].Cells["Rate"].Value = MobjClsCommonUtility.GetSaleRate(//(Convert.ToDecimal(dtrSalesRate[0].ItemArray[5].ToString()) / MobjclsBLLSTSales.clsDTOSTSalesMaster.decExchangeCurrencyRate).ToString("F" + MintExchangeCurrencyScale);
                            //}
                        }
                       // FillDiscountColumn(e.RowIndex, true);
                        if (dgvSales.Rows[e.RowIndex].Cells["Rate"].Value.ToDecimal() == -1 && dgvSales.Rows[e.RowIndex].Cells[ItemID.Index].Value.ToInt32() != 0)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2076, out MmessageIcon).Replace("#", "").Replace("*", dgvSales.Rows[e.RowIndex].Cells[ItemName.Index].Value.ToString()).Replace("@", cboCurrency.Text);
                            MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        }
                    }
                    else if (dgvSales.Columns[e.ColumnIndex].Name == "IsGroup")
                    {
                        Int32 intItemID = Convert.ToInt32(dgvSales.Rows[e.RowIndex].Cells["ItemID"].Value);
                        int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);

                        if (intItemID != 0)
                        {
                            if (Convert.ToBoolean(dgvSales["IsGroup", e.RowIndex].Value) == true)
                            {
                                datSalesRate = FillDataGridCombo(intItemID, intVendorID, Convert.ToBoolean(dgvSales.Rows[e.RowIndex].Cells["IsGroup"].Value));

                                dgvSales.Rows[e.RowIndex].Cells["Uom"].Tag = (int)PredefinedUOMs.Numbers;
                                dgvSales.Rows[e.RowIndex].Cells["Uom"].Value = (int)PredefinedUOMs.Numbers;
                                dgvSales.Rows[e.RowIndex].Cells["Uom"].Value = dgvSales.Rows[e.RowIndex].Cells["Uom"].FormattedValue;
                            }
                            //FillDiscountColumn(e.RowIndex, false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvSales_CellValueChanged() " + ex.Message);
                MobjLogs.WriteLog("Error in dgvSales_CellValueChanged() " + ex.Message, 2);
            }
        }

        private void btnCustomer_Click(object sender, EventArgs e)
        {
            int intVendor = Convert.ToInt32(cboCustomer.SelectedValue);
            using (FrmVendor objVendor = new FrmVendor(3))
            {
                objVendor.PintVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                objVendor.ShowDialog();
                if (MblnAddStatus)
                {
                    LoadCombos(2);
                }
                else
                {
                    DataTable datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "distinct V.VendorID,V.VendorName+' - '+V.VendorCode as VendorName", "InvVendorInformation V INNER JOIN InvVendorCompanyDetails VC ON V.VendorID = VC.VendorID ", "VendorTypeID=" + (int)VendorType.Customer + " AND VC.CompanyID = " + ClsCommonSettings.CompanyID + " And StatusID = " + (int)VendorStatus.CustomerActive + " UNION SELECT VendorID,VendorName+' - '+VendorCode as VendorName FROM InvVendorInformation WHERE VendorID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID });

                   // DataTable datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "VendorID,VendorName+' - '+VendorCode as VendorName", "InvVendorInformation", "VendorTypeID=" + (int)VendorType.Customer + " And StatusID = " + (int)VendorStatus.CustomerActive + " UNION SELECT VendorID,VendorName+' - '+VendorCode as VendorName FROM InvVendorInformation WHERE VendorID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID });
                    cboCustomer.ValueMember = "VendorID";
                    cboCustomer.DisplayMember = "VendorName";
                    cboCustomer.DataSource = datCombos; 
                }
                if (objVendor.PintVendorID != 0 && cboCustomer.Enabled)
                    cboCustomer.SelectedValue = objVendor.PintVendorID;
                else
                    cboCustomer.SelectedValue = intVendor;
            }
        }

        private void cboCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCustomer.SelectedValue != null)
            {
                LoadCombos(5);
                if (MblnAddStatus)
                {
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                }
                else
                {
                    cboCustomer.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID;
                }
                AddToAddressMenu();
                DisplayAddress();
                Changestatus();
                if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromDeliveryNote)
                DisplayDNote();
                //foreach (DataGridViewRow dr in dgvSales.Rows)
                //{
                //    FillDiscountColumn(dr.Index, false);
                //}
                CalculateTotalAmt();
                CalculateNetTotal();
            }
            else
            {
                CMSVendorAddress.Items.Clear();
            }
        }

        private void btnTContextmenu_Click(object sender, EventArgs e)
        {
            CMSVendorAddress.Show(btnTContextmenu, btnTContextmenu.PointToClient(System.Windows.Forms.Cursor.Position));
        }

        private void dgvSales_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //if ((((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex) == 2 ||
            //    (((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex) == 3 ||
            //    (((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex) == 6 ||
            //    (((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex) == 7)
            //{
            //    e.Control.KeyPress += this.TextboxNumeric_KeyPress;
            //}
            //else
            //{
            dgvSales.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
            // }

            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                ((DataGridViewComboBoxEditingControl)e.Control).BackColor = Color.White;
            }
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvSales.CurrentCell.OwningColumn.Index == Quantity.Index || dgvSales.CurrentCell.OwningColumn.Index == Rate.Index)
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
                int intUomScale = 0;
                if (dgvSales.CurrentCell.OwningColumn.Index == Quantity.Index)
                {
                    if (dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                        intUomScale = MobjclsBLLSTSales.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                }
                else
                    intUomScale = MintExchangeCurrencyScale;

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    if (intUomScale == 0)
                    {
                        if (e.KeyChar == '.')
                            e.Handled = true;
                    }
                    else
                    {
                        int dotIndex = -1;
                        if (txt.Text.Contains("."))
                        {
                            dotIndex = txt.Text.IndexOf('.');
                        }
                        if (e.KeyChar == '.')
                        {
                            if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            {
                                e.Handled = true;
                            }
                        }
                        else
                        {
                            if (char.IsDigit(e.KeyChar))
                            {
                                if (dotIndex != -1 && txt.SelectionStart > dotIndex)
                                {
                                    string[] splitText = txt.Text.Split('.');
                                    if (splitText.Length == 2)
                                    {
                                        if (splitText[1].Length - txt.SelectedText.Length >= intUomScale)
                                            e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
                //int intDecimalPart = 0;
                //if (dgvSales.EditingControl != null && !string.IsNullOrEmpty(dgvSales.EditingControl.Text) && dgvSales.EditingControl.Text.Contains("."))
                //    intDecimalPart = dgvSales.EditingControl.Text.Split('.')[1].Length;
                //int intUomScale = 0;
                //if (dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                //    intUomScale = MobjclsBLLSTSales.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                //if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                //{
                //    e.Handled = true;
                //}
                //if (e.KeyChar.ToString() == "." && intUomScale == 0)
                //{
                //    e.Handled = true;
                //}

                //if (e.KeyChar!=08 && e.KeyChar !=13 && dgvSales.EditingControl != null && !string.IsNullOrEmpty(dgvSales.EditingControl.Text) && (dgvSales.EditingControl.Text.Contains(".") && ((e.KeyChar == 46) || intDecimalPart == intUomScale)))//checking more than one "."
                //{
                //    e.Handled = true;
                //}
            }
            if (dgvSales.CurrentCell != null && dgvSales.CurrentCell.OwningColumn.Name == "ItemCode" || dgvSales.CurrentCell.OwningColumn.Name == "ItemName")
            {
                if (e.KeyChar.ToString() == "'")
                {
                    e.Handled = true;
                }
            }
        }

        private void dgvSales_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                if (cboCompany.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 14, out MmessageIcon);
                    ErrSales.SetError(cboCompany, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    cboCompany.Focus();
                    e.Cancel = true;
                    return;
                }
                else if (cboCurrency.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2030, out MmessageIcon);
                    ErrSales.SetError(cboCurrency, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSales.Enabled = true;
                    cboCurrency.Focus();
                    e.Cancel = true;
                    return;
                }
                else if (cboCustomer.SelectedIndex == -1)
                {
                    if (cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.SOTFromSalesPro)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1951, out MmessageIcon);
                        ErrSales.SetError(cboCustomer, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrSales.Enabled = true;
                        cboCustomer.Focus();
                        e.Cancel = true;
                        return;
                    }
                }

                //if (e.ColumnIndex == BatchNo.Index || e.ColumnIndex == Rate.Index)
                if (e.ColumnIndex == BatchNo.Index)
                {
                    e.Cancel = true;
                    return;
                }

                switch (MintFromForm)
                {
                    case 1:
                        break;
                    case 2:
                        if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromQuotation)
                        {
                        //    e.Cancel = true;
                        //    return;
                        }
                        if (e.ColumnIndex != ItemCode.Index && e.ColumnIndex != ItemName.Index)
                        {
                            if (Convert.ToString(dgvSales.CurrentRow.Cells["ItemCode"].Value).Trim() == "")
                            {
                                //MsMessageCommon = "Please enter Item code.";
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2006, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                dgvSales.Focus();
                                e.Cancel = true;
                                return;
                            }
                        }
                        if (e.ColumnIndex == Uom.Index)
                        {
                            DataTable datSalesRate = new DataTable();

                            Int32 intItemID = Convert.ToInt32(dgvSales.Rows[e.RowIndex].Cells["ItemID"].Value);
                            int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                            int intUOMID = Convert.ToInt32(dgvSales.Rows[e.RowIndex].Cells["Uom"].Tag);

                            if (intItemID != 0)
                            {
                                datSalesRate = FillDataGridCombo(intItemID, intVendorID, Convert.ToBoolean(dgvSales.Rows[e.RowIndex].Cells["IsGroup"].Value));
                            }
                        }
                        //else if (e.ColumnIndex == Discount.Index)
                        //{
                        //    if (Convert.ToBoolean(dgvSales.Rows[e.RowIndex].Cells[IsGroup.Index].Value) == false)
                        //    {
                        //        int intItemID = Convert.ToInt32(dgvSales.Rows[e.RowIndex].Cells["ItemID"].Value);
                        //        if (intItemID != 0)
                        //        {
                        //            FillDiscountColumn(e.RowIndex, false);
                        //        }
                        //    }
                        //    else
                        //        e.Cancel = true;
                        //}


                        //if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromDeliveryNote)
                        //{
                        //    if (e.ColumnIndex == 7 || e.ColumnIndex == 9 || e.ColumnIndex == 10)
                        //    {
                        //        dgvSales.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = false;
                        //        e.Cancel = false;
                        //    }
                        //}

                        break;
                    case 3:
                        break;
                }

                //if (e.ColumnIndex == DiscountAmount.Index)
                //{
                //    if (dgvSales.Rows[e.RowIndex].Cells[Discount.Index].Tag.ToInt32() == (int)PredefinedDiscounts.DefaultSalesDiscount && !Discount.ReadOnly)
                //        e.Cancel = false;
                //    else
                //        e.Cancel = true;
                //}
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {

            try
            {
                if (SaveSalesDetails())
                {
                    //  AddNew();
                    long lngSalesID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID;
                    if (MintFromForm == 2)
                        lngSalesID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID;
                    else if (MintFromForm == 3)
                        lngSalesID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID;

                    DisplaySalesNumbersInSearchGrid();
                    DisplayInformation(lngSalesID);

                    cboCompany.Enabled = false;
                    cboOrderType.Enabled = false;
                    cboOrderNumbers.Enabled = false;
                    MblnQtyChanged = false;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorSaveItem_Click() " + ex.Message);
                MobjLogs.WriteLog("Error in BindingNavigatorSaveItem_Click() " + ex.Message, 2);
            }
        }

        private void cboDiscount_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cboDiscount.Enabled)
            //{
            //    if (cboDiscount.SelectedValue.ToInt32() == (int)PredefinedDiscounts.DefaultSalesDiscount)
            //    {
            //        txtDiscount.ReadOnly = false;
            //    }
            //    else
            //        txtDiscount.ReadOnly = true;
            //}

            CalculateNetTotal();
        }

        private void dgvSalesDisplay_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvSalesDisplay.Rows.Count > 0)
            {
                if (Convert.ToInt64(dgvSalesDisplay.CurrentRow.Cells[0].Value) != 0)
                {
                    DisplayInformation(Convert.ToInt64(dgvSalesDisplay.CurrentRow.Cells[0].Value));
                    cboCompany.Enabled = false;
                    cboOrderType.Enabled = false;
                    cboOrderNumbers.Enabled = false;
                    cboTaxScheme.Enabled = false;
                    MblnQtyChanged = false;

                    string strSalesNo = "";
                    switch (MintFromForm)
                    {
                        case 1:
                            strSalesNo += " Quotation No ";
                            break;
                        case 2:
                            strSalesNo += " Order No ";
                            break;
                        case 3:
                            strSalesNo += " Invoice No ";
                            break;
                    }
                    strSalesNo += dgvSalesDisplay.CurrentRow.Cells[1].Value.ToString();
                    lblSalesOrderstatus.Text = "Details of " + strSalesNo;
                }
            }
        }

        private void dgvSales_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    if (e.ColumnIndex == Uom.Index)
                    {
                        dgvSales.CurrentRow.Cells["Uom"].Tag = dgvSales.CurrentRow.Cells["Uom"].Value;
                        dgvSales.CurrentRow.Cells["Uom"].Value = dgvSales.CurrentRow.Cells["Uom"].FormattedValue;

                        int intUomScale = 0;
                        if (dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                            intUomScale = MobjclsBLLSTSales.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvSales[Uom.Index, dgvSales.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                        dgvSales.CurrentRow.Cells["Quantity"].Value = dgvSales.CurrentRow.Cells["Quantity"].Value.ToDecimal().ToString("F" + intUomScale);

                        Int32 intItemID = Convert.ToInt32(dgvSales.Rows[e.RowIndex].Cells["ItemID"].Value);
                        int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                        int intUOMID = Convert.ToInt32(dgvSales.Rows[e.RowIndex].Cells["Uom"].Tag);

                        if (intItemID != 0)//&& MblnAddStatus)
                        {
                            //   DataTable  datSalesRate = FillDataGridCombo(intItemID, intVendorID, Convert.ToBoolean(dgvSales.Rows[e.RowIndex].Cells["IsGroup"].Value));
                            //  DataRow[] dtrSalesRate = datSalesRate.Select("UOMID=" + intUOMID);

                            //  if (dtrSalesRate.Length > 0)
                            //  {
                            //  MobjclsBLLSTSales.GetExchangeCurrencyRate(Convert.ToInt32(cboCompany.SelectedValue), Convert.ToInt32(cboCurrency.SelectedValue));
                            if (Convert.ToBoolean(dgvSales[IsGroup.Index, e.RowIndex].Value) == false)
                                dgvSales.Rows[e.RowIndex].Cells["Rate"].Value = MobjClsCommonUtility.GetSaleRate(intItemID, intUOMID, dgvSales.CurrentRow.Cells["BatchID"].Value.ToInt64(), cboCompany.SelectedValue.ToInt32()); //(Convert.ToDecimal(dtrSalesRate[0].ItemArray[5].ToString()) / MobjclsBLLSTSales.clsDTOSTSalesMaster.decExchangeCurrencyRate).ToString("F" + MintExchangeCurrencyScale);
                            // }
                        }
                        //FillDiscountColumn(e.RowIndex, true);

                        CalculateTotalAmt();
                    }
                    else if (e.ColumnIndex == Discount.Index)
                    {
                        dgvSales.CurrentRow.Cells["Discount"].Tag = dgvSales.CurrentRow.Cells["Discount"].Value;
                        dgvSales.CurrentRow.Cells["Discount"].Value = dgvSales.CurrentRow.Cells["Discount"].FormattedValue;

                        //if (dgvSales.CurrentRow.Cells["Discount"].Tag.ToInt32() == (int)PredefinedDiscounts.DefaultSalesDiscount)
                        //    dgvSales.CurrentRow.Cells["DiscountAmount"].ReadOnly = false;
                        //else
                        //    dgvSales.CurrentRow.Cells["DiscountAmount"].ReadOnly = true;

                        CalculateTotalAmt();
                    }
                    else if (e.ColumnIndex == Quantity.Index || e.ColumnIndex == Discount.Index || e.ColumnIndex == DiscountAmount.Index)
                    {
                        CalculateTotalAmt();
                    }
                    else if (e.ColumnIndex == Rate.Index)
                    {
                        CalculateTotalAmt();
                        if (MobjClsCommonUtility.GetPurchaseRate(dgvSales.Rows[e.RowIndex].Cells[ItemID.Index].Value.ToInt32(), dgvSales.Rows[e.RowIndex].Cells[Uom.Index].Tag.ToInt32(), dgvSales.Rows[e.RowIndex].Cells[BatchID.Index].Value.ToInt64(), cboCompany.SelectedValue.ToInt32()) > dgvSales.Rows[e.RowIndex].Cells[Rate.Index].Value.ToDecimal())
                        {
                            dgvSales.Rows[e.RowIndex].Cells[Rate.Index].Style.ForeColor = Color.Red;
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2086);
                            lblSalesOrderstatus.Text = MstrMessageCommon;
                        }
                        else
                        {
                            dgvSales.Rows[e.RowIndex].Cells[Rate.Index].Style.ForeColor = Color.Black;
                            foreach (DataGridViewRow dr in dgvSales.Rows)
                            {
                                lblSalesOrderstatus.Text = "";
                                if (dr.Index != e.RowIndex)
                                {
                                    if (dr.Cells[Rate.Index].Style.ForeColor == Color.Red)
                                    {
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2086);
                                        lblSalesOrderstatus.Text = MstrMessageCommon;
                                        return;
                                    }
                                }
                            }
                            //lblSalesOrderstatus.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvSales_CellEndEdit() " + ex.Message);
                MobjLogs.WriteLog("Error in dgvSales_CellEndEdit() " + ex.Message, 2);
            }
        }

        private void cboOrderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvSales.Rows.Clear();
            txtAdvPayment.Text = txtExpenseAmount.Text = new decimal(0).ToString("F" + MintExchangeCurrencyScale);
            CalculateTotalAmt();
            CalculateNetTotal();
            if (cboCompany.SelectedIndex == -1) return;

            if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTDirect)
            {
                cboCustomer.Enabled = true;
                btnCustomer.Enabled = true;

                ItemCode.ReadOnly = false;
                ItemName.ReadOnly = false;
                BatchNo.ReadOnly = false;
                Quantity.ReadOnly = false;
                Uom.ReadOnly = false;
                NetAmount.ReadOnly = true;
                GrandAmount.ReadOnly = true;
                Rate.ReadOnly = false;
                Discount.ReadOnly = false;
                DiscountAmount.ReadOnly = false;
                cboPaymentTerms.Enabled = true;
                txtLpoNo.ReadOnly = false;
            }
            if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromQuotation)
            {
                btnCustomer.Enabled = false;

                ItemCode.ReadOnly = true;
                ItemName.ReadOnly = true;
                BatchNo.ReadOnly = true;
                Quantity.ReadOnly = false;
                Uom.ReadOnly = false;
                NetAmount.ReadOnly = true;
                GrandAmount.ReadOnly = true;
                Rate.ReadOnly = false;
                Discount.ReadOnly = false;
                DiscountAmount.ReadOnly = false;
                cboPaymentTerms.Enabled = false;
                txtLpoNo.ReadOnly = false;

            }
            if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromDeliveryNote)
            {
                btnCustomer.Enabled = true;
                ItemCode.ReadOnly = true;
                ItemName.ReadOnly = true;
                BatchNo.ReadOnly = true;
                Quantity.ReadOnly = true;
                Uom.ReadOnly = true;
                NetAmount.ReadOnly = true;
                GrandAmount.ReadOnly = true;
                Rate.ReadOnly = false;
                Discount.ReadOnly = false;
                DiscountAmount.ReadOnly = false;
                cboPaymentTerms.Enabled = true;
                txtLpoNo.ReadOnly = true;
                //dgvSales.ReadOnly = true;
            }
            if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromSalesPro)
            {
                btnCustomer.Enabled = false;

                ItemCode.ReadOnly = true;
                ItemName.ReadOnly = true;
                BatchNo.ReadOnly = true;
                Quantity.ReadOnly = false;
                Uom.ReadOnly = false;
                NetAmount.ReadOnly = true;
                GrandAmount.ReadOnly = true;
                Rate.ReadOnly = false;
                Discount.ReadOnly = false;
                DiscountAmount.ReadOnly = false;
                cboPaymentTerms.Enabled = false;
                txtLpoNo.ReadOnly = false;
                cblSaleQuotationNo.Visible = false;
                cboOrderType.Visible = true;
            }
            SetOrderTypeComboView();
            LoadOrderNumbers();

        }

        private void cboOrderNumbers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboOrderType.Tag.ToString() == "DoNotChange") return;
                if (cboOrderNumbers.SelectedIndex == -1) return;
                if (!MblnIsFromClear && MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID == 0 && Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.SOTFromQuotation)
                {
                    if (MobjclsBLLSTSales.FillCombos(new string[] { "SalesQuotationID", strTableSalesQuotation, "SalesQuotationID = " + Convert.ToInt64(cboOrderNumbers.SelectedValue) + " And StatusID In( " + (Int32)OperationStatusType.SQuotationApproved + "," + (int)OperationStatusType.SQuotationSubmitted + ")" }).Rows.Count == 0)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2046, out MmessageIcon);
                        ErrSales.SetError(cboOrderNumbers, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrSales.Enabled = true;
                        cboOrderNumbers.Focus();
                        return;
                    }
                }

                if (Convert.ToInt64(cboOrderNumbers.SelectedValue) != 0)
                {
                    if (MintFromForm == 1)
                    {
                        DisplaySalesDetDetails(4, Convert.ToInt64(cboOrderNumbers.SelectedValue));
                        DisplayAddress();
                        CalculateTotalAmt();
                        CalculateNetTotal();

                        cboCustomer.Enabled = false;
                    }
                    else if (MintFromForm == 2)
                    {
                        if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.SOTFromSalesPro)
                        {
                            if (MobjclsBLLSTSales.GetSalesProQuotationMasterDetails(Convert.ToInt64(cboOrderNumbers.SelectedValue)))
                            {
                                cboCustomer.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID;

                                if (cboCustomer.SelectedValue == null)
                                    cboCustomer.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVendorName;

                                cboCustomer.Enabled = btnAddressChange.Enabled = cboDiscount.Enabled = cboPaymentTerms.Enabled = false;

                                MintVendorAddID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID;
                                lblVendorAddress.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strAddressName + " Address";
                                cboPaymentTerms.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intPaymentTermsID;

                                cboDiscount.SelectedIndex = (MobjclsBLLSTSales.clsDTOSTSalesMaster.blnGrandDiscount.ToBoolean() ? 0 : 1);

                                if (Convert.ToString(cboDiscount.SelectedItem) == "%")
                                    txtDiscount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountPercent.ToString();
                                else
                                    txtDiscount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountAmt.ToString();
                                txtTotalAmount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decNetAmount.ToString();



                                //cboDiscount.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intGrandDiscountID;
                                MintCurrencyID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID;
                                cboCurrency.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID;

                                dtpOrderDate.Value = ClsCommonSettings.GetServerDate();
                                dtpDueDate.Value = ClsCommonSettings.GetServerDate();

                                txtExpenseAmount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decExpenseAmount.ToString("F" + MintExchangeCurrencyScale);
                                DisplayAddress();
                                DisplaySalesDetDetails(1, Convert.ToInt64(cboOrderNumbers.SelectedValue));

                                CalculateTotalAmt();
                                //txtDiscount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountAmt.ToString("F" + MintExchangeCurrencyScale);
                                CalculateNetTotal();
                                dgvSales.ReadOnly = false;


                            }
                        }
                        else
                        {
                            if (MobjclsBLLSTSales.GetQuotationMasterDetails(Convert.ToInt64(cboOrderNumbers.SelectedValue)))
                            {
                                cboCustomer.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID;

                                if (cboCustomer.SelectedValue == null)
                                    cboCustomer.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVendorName;

                                cboCustomer.Enabled = btnAddressChange.Enabled = cboDiscount.Enabled = cboPaymentTerms.Enabled = false;

                                MintVendorAddID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID;
                                lblVendorAddress.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strAddressName + " Address";
                                cboPaymentTerms.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intPaymentTermsID;

                                cboDiscount.SelectedIndex = (MobjclsBLLSTSales.clsDTOSTSalesMaster.blnGrandDiscount.ToBoolean() ? 0 : 1);

                                if (Convert.ToString(cboDiscount.SelectedItem) == "%")
                                    txtDiscount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountPercent.ToString();
                                else
                                    txtDiscount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountAmt.ToString("F" + MintExchangeCurrencyScale);
                                txtTotalAmount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decNetAmount.ToString();

                                cboTaxScheme.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intTaxSchemeID;
                                txtTaxAmount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decTaxAmount.ToString();

                                //cboDiscount.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intGrandDiscountID;
                                MintCurrencyID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID;
                                cboCurrency.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID;

                                dtpOrderDate.Value = ClsCommonSettings.GetServerDate();
                                dtpDueDate.Value = ClsCommonSettings.GetServerDate();

                                txtExpenseAmount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decExpenseAmount.ToString("F" + MintExchangeCurrencyScale);
                                DisplayAddress();
                                DisplaySalesDetDetails(1, Convert.ToInt64(cboOrderNumbers.SelectedValue));

                                CalculateTotalAmt();
                                //txtDiscount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountAmt.ToString("F" + MintExchangeCurrencyScale);
                                CalculateNetTotal();
                                dgvSales.ReadOnly = false;


                            }
                        }
                    }
                    //else if (MintFromForm == 3)
                    //{
                    //    //   DisplaySalesOrderDetails(MintFromForm, Convert.ToInt64(cboOrderNumbers.SelectedValue));
                    //    if (MobjclsBLLSTSales.GetOrderMasterDetails(Convert.ToInt64(cboOrderNumbers.SelectedValue)))
                    //    {
                    //        cboOrderType.Tag = "DoNotChange";
                    //        cboCompany.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID;

                    //        if (cboCompany.SelectedValue == null)
                    //        {
                    //            DataTable datTemp = MobjclsBLLSTSales.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intCompanyID });
                    //            if (datTemp.Rows.Count > 0)
                    //                cboCompany.Text = datTemp.Rows[0]["Name"].ToString();
                    //        }
                    //        txtOrderNo.Tag = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID;
                    //        cboCustomer.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorID;

                    //        if (cboCustomer.SelectedValue == null)
                    //            cboCustomer.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVendorName;

                    //        MintVendorAddID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID;
                    //        lblVendorAddress.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strAddressName + " Address";
                    //        cboPaymentTerms.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intPaymentTermsID;
                    //        cboDiscount.SelectedValue = MobjclsBLLSTSales.clsDTOSTSalesMaster.intGrandDiscountID;

                    //        MintCurrencyID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intCurrencyID;
                    //        cboCurrency.SelectedValue = MintCurrencyID;
                    //        MintShipAddID = Convert.ToInt32(MobjclsBLLSTSales.clsDTOSTSalesMaster.intVendorAddID);

                    //        GenerateOrderNo();

                    //        dtpOrderDate.Value = ClsCommonSettings.GetServerDate();
                    //        dtpDueDate.Value = ClsCommonSettings.GetServerDate();


                    //        MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SNew;

                    //        txtRemarks.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strRemarks;

                    //        DisplayAddress();
                    //        DisplaySalesDetDetails(2, Convert.ToInt64(cboOrderNumbers.SelectedValue));

                    //        txtExpenseAmount.Text = MobjclsBLLSTSales.GetExpenseAmount().ToString("F" + MintExchangeCurrencyScale);
                    //        CalculateTotalAmt();
                    //        txtDiscount.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.decGrandDiscountAmount.ToString("F" + MintExchangeCurrencyScale);
                    //        CalculateNetTotal();

                    //        cboOrderType.Tag = "";
                    //        BindingNavigatorSaveItem.Enabled = MblnAddPermission;

                    //        dgvSales.ReadOnly = false;
                    //        btnActions.Enabled = false;
                    //        btnAddExpense.Enabled = false;
                    //        cboDiscount.Enabled = false;
                    //        txtAdvPayment.Text = MobjclsBLLSTSales.GetAdvancePayment(Convert.ToInt64(cboOrderNumbers.SelectedValue)).ToString("F" + MintExchangeCurrencyScale);
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in cboOrderNumbers_SelectedIndexChanged() " + ex.Message);
                MobjLogs.WriteLog("Error in cboOrderNumbers_SelectedIndexChanged() " + ex.Message, 2);
            }
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            int intBaseCurrencyID = 0;
            cboOrderNumbers.SelectedIndex = -1;
            dgvSales.Rows.Clear();
            LoadCombos(6);
            LoadCombos(7);

            if (MblnAddStatus)
                GenerateOrderNo();
            if (Convert.ToInt32(cboCompany.SelectedValue) != 0)
            {
                lblCurrencyAmnt.Text = "Amount in " + MobjclsBLLSTSales.GetCompanyCurrency(Convert.ToInt32(cboCompany.SelectedValue), out MintBaseCurrencyScale, out intBaseCurrencyID);
            }
            else
            {
                lblCurrencyAmnt.Text = "Amount in company currency";
            }
            cboCurrency.SelectedValue = intBaseCurrencyID;
            ClearControls();
            cboOrderType_SelectedIndexChanged(sender, e);
            cboPaymentTerms_SelectedIndexChanged(null, null);

        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void BindingNavigatorCancelItem_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64(txtOrderNo.Tag) > 0)
            {
                if (BindingNavigatorCancelItem.Text.ToUpper() == "CANCEL")
                {
                    mlbnReopen = false;
                  CancelSales();
              
                }
                else
                {
                    mlbnReopen = true;
                    //if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.SOTFromDeliveryNote)
                    //{
                    //    for (int intICounter = 0; intICounter < dgvSales.RowCount - 1; intICounter++)
                    //    {
                    //        int iRowIndex = dgvSales.Rows[intICounter].Index;
                    //        if (dgvSales.Rows[intICounter].Cells["InvoicedQty"].Value.ToDecimal() >= dgvSales.Rows[intICounter].Cells["OrderedQty"].Value.ToDecimal())
                    //        {
                    //            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2101, out MmessageIcon);
                    //            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    //            dgvSales.Focus();
                    //            dgvSales.CurrentCell = dgvSales[Quantity.Index, iRowIndex];
                    //            return;

                    //        }
                    //    }
                    //}
                    ReopenSales();
                }
                long lngSalesID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID;
                if (MintFromForm == 2)
                    lngSalesID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID;
                else if (MintFromForm == 3)
                    lngSalesID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID;

                DisplayInformation(lngSalesID);

            }
        }

        private void btnSRefresh_Click(object sender, EventArgs e)
        {
            DisplaySalesNumbersInSearchGrid();
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            
        }

        private void btnAddExpense_Click(object sender, EventArgs e)
        {
            if ((btnActions.Enabled && txtOrderNo.Tag != null && Convert.ToString(txtOrderNo.Tag) != "" && Convert.ToString(txtOrderNo.Tag) != "0") || txtExpenseAmount.Text.ToDecimal() != 0)
            {
                AddExpense();
            }
        }

        private void btnExpense_Click(object sender, EventArgs e)
        {
            btnAddExpense_Click(sender, e);
        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            if (txtOrderNo.Tag != null && Convert.ToString(txtOrderNo.Tag) != "" && Convert.ToString(txtOrderNo.Tag) != "0")
            {
                int intReferenceID = txtOrderNo.Tag.ToInt32();
                clsBLLAlertMoment objclsBLLAlertMoment = new clsBLLAlertMoment();

                switch (MintFromForm)
                {
                    case 1:
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2016, out MmessageIcon);
                        break;
                    case 2:
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2036, out MmessageIcon);
                        break;
                    case 3:
                        break;
                }
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                {
                    objFrmCancellationDetails.Text = "Rejection Details";
                    objFrmCancellationDetails.lblCancelledDate.Text = "Rejected Date";
                    objFrmCancellationDetails.ShowDialog();
                    txtDescription.Text = objFrmCancellationDetails.PStrDescription;
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.intApprovedBy = ClsCommonSettings.UserID;
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.strApprovedDate = objFrmCancellationDetails.PDtCancellationDate.ToString("dd-MMM-yyyy");
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.strVerificationDescription = txtDescription.Text.Trim();
                    MobjclsBLLSTSales.clsDTOSTSalesMaster.intVerificationCriteriaID = objFrmCancellationDetails.PintVerificationCriteria;

                    MblnAddStatus = false;
                    MblnRejectFlag = true;
                    if (MintFromForm == 1)
                    {
                        MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (int)OperationStatusType.SQuotationRejected;
                        BindingNavigatorSaveItem_Click(sender, e);
                    }
                    else if (MintFromForm == 2)
                    {
                        MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (int)OperationStatusType.SOrderRejected;
                        MobjclsBLLSTSales.RejectSalesOrder();
                        // AddNew();

                        objclsBLLAlertMoment.DeleteAlert(intReferenceID, (int)AlertSettingsTypes.SaleOrderSubmitted);
                        objclsBLLAlertMoment.DeleteAlert(intReferenceID, (int)AlertSettingsTypes.SaleOrderApproved);

                        //Sales Order Rejected Alert
                        objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.SaleOrderRejected;
                        objFrmTradingMain.tmrAlert.Enabled = true;

                        DisplayInformation(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID);
                    }
                    MblnRejectFlag = false;
                    BindingNavigatorSaveItem.Enabled = false;
                }
            }
        }

        private void btnDocuments_Click(object sender, EventArgs e)
        {
            if (txtOrderNo.Tag != null && Convert.ToString(txtOrderNo.Tag) != "" && Convert.ToString(txtOrderNo.Tag) != "0")
            {
                ShowDocumentMaster();
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            if (MobjclsBLLSTSales.FillCombos(new string[] { "SalesOrderID", "InvSalesOrderMaster", "SalesOrderID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID.ToInt64() }).Rows.Count > 0)
            {
                ShowPrint();

            }
            else
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2110, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                dgvSales.Focus();
                return;
            }
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            ShowEmailPopUp();
        }

        private void txtExpenseAmount_TextChanged(object sender, EventArgs e)
        {
            decimal decExpenseAmt = 0, decTotalAmt = 0, decNetlAmt = 0;

            decExpenseAmt = Convert.ToDecimal(txtExpenseAmount.Text);
            decTotalAmt = Convert.ToDecimal(txtTotalAmount.Text);
            decNetlAmt = decExpenseAmt + decTotalAmt;

            MobjclsBLLSTSales.clsDTOSTSalesMaster.decNetAmount = decNetlAmt;
            MobjclsBLLSTSales.clsDTOSTSalesMaster.decNetAmountRounded = decNetlAmt;

            txtTotalAmount.Text = decNetlAmt.ToString("F" + MintExchangeCurrencyScale);
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            int intReferenceID = txtOrderNo.Tag.ToInt32();
            clsBLLAlertMoment objclsBLLAlertMoment = new clsBLLAlertMoment();
            try
            {
                if (Convert.ToInt64(txtOrderNo.Tag) > 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4268, out MmessageIcon);

                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;

                    switch (MintFromForm)
                    {
                        case 1:
                            MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID = Convert.ToInt64(txtOrderNo.Tag);

                            if (MobjClsCommonUtility.FillCombos(new string[] { "1", "InvSalesOrderMaster SOM InvSalesOrderReferenceDetails SORD ON SORD.SalesOrderID = SOM.SalesOrderID  ", "SOM.OrderTypeID = " + (int)OperationOrderType.SOTFromQuotation + " And SORD.ReferenceID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID }).Rows.Count > 0)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9001, out MmessageIcon).Replace("#", "").Replace("*", "Sales Quotation");
                                MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }

                            if (MobjclsBLLSTSales.DeleteSalesQuotation())
                            {
                                //deletes Alert
                                //objclsBLLAlertMoment.DeleteAlert(intReferenceID, (int)AlertSettingsTypes.SalesQuotationSubmitted);
                                //objclsBLLAlertMoment.DeleteAlert(intReferenceID, (int)AlertSettingsTypes.SalesQuotationApproved);
                                //objclsBLLAlertMoment.DeleteAlert(intReferenceID, (int)AlertSettingsTypes.SalesQuotationCancelled);
                                ////submitted Alert
                                //objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.SalesQuotationSubmitted;
                                //objFrmTradingMain.tmrAlert.Enabled = true;

                                //lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                //TmrFocus.Enabled = true;
                                AddNew();
                            }
                            else
                            {
                                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrFocus.Enabled = true;
                            }
                            break;
                        case 2:
                            MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID = Convert.ToInt64(txtOrderNo.Tag);

                            if (MobjClsCommonUtility.FillCombos(new string[] { "1", "InvSalesInvoiceMaster SIM INNER JOIN InvSalesInvoiceReferenceDetails SIRD ON SIRD.SalesInvoiceID = SIM.SalesInvoiceID ", "SIRD.ReferenceID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID +" And SIM.OrderTypeID = 15" }).Rows.Count > 0)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9001, out MmessageIcon).Replace("#", "").Replace("*", "Sales Order");
                                MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }

                            if (MobjclsBLLSTSales.FillCombos(new string[] { "Payments.ReceiptAndPaymentID", strTableReceiptAndPayment, "Payments.OperationTypeID = " + (int)OperationType.SalesOrder + " And RPD.ReferenceID =" + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID }).Rows.Count > 0)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2052, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrSales.Enabled = true;
                                return;
                            }

                            if (MobjclsBLLSTSales.CheckSalesOrderExists(MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID))
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2116, out MmessageIcon).Replace("approved", "submitted");
                                    ErrSales.SetError(cboOrderNumbers, MstrMessageCommon.Replace("#", "").Trim());
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrSales.Enabled = true;
                                    return;

                                }
                           
                            if (MobjclsBLLSTSales.DeleteSalesOrder())
                            {
                                objclsBLLAlertMoment.DeleteAlert(intReferenceID, (int)AlertSettingsTypes.SaleOrderCreated);
                                objclsBLLAlertMoment.DeleteAlert(intReferenceID, (int)AlertSettingsTypes.SaleOrderSubmitted);
                                objclsBLLAlertMoment.DeleteAlert(intReferenceID, (int)AlertSettingsTypes.SaleOrderApproved);
                                objclsBLLAlertMoment.DeleteAlert(intReferenceID, (int)AlertSettingsTypes.SaleOrderRejected);

                                objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.SaleOrderCreated;
                                objFrmTradingMain.tmrAlert.Enabled = true;

                                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrFocus.Enabled = true;
                                AddNew();
                            }
                            else
                            {
                                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrFocus.Enabled = true;
                            }
                            break;
                        case 3:
                            MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID = Convert.ToInt64(txtOrderNo.Tag);
                            if (Convert.ToInt32(cboPaymentTerms.SelectedValue) == (int)PaymentTerms.Credit)
                            {
                                if (MobjclsBLLSTSales.FillCombos(new string[] { "Payments.ReceiptAndPaymentID", strTableReceiptAndPayment, "Payments.OperationTypeID = " + (int)OperationType.SalesInvoice + " And RPD.ReferenceID =" + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID }).Rows.Count > 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2050, out MmessageIcon);
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrSales.Enabled = true;
                                    return;
                                }
                            }
                            else
                            {
                                if (MobjclsBLLSTSales.FillCombos(new string[] { "Payments.ReceiptAndPaymentID", strTableReceiptAndPayment, "Payments.OperationTypeID = " + (int)OperationType.SalesInvoice + " And RPD.ReferenceID =" + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID + " And Payments.ReceiptAndPaymentTypeID = " + (int)PaymentTypes.DirectExpense + "" }).Rows.Count > 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2050, out MmessageIcon);
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrSales.Enabled = true;
                                    return;
                                }

                            }
                            if (MobjclsBLLSTSales.FillCombos(new string[] { "ExpenseID", "InvExpenseMaster", "OperationTypeID = " + (int)OperationType.SalesInvoice + " And ReferenceID =" + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID + " And IsDirectExpense = 0" }).Rows.Count > 0)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 997, out MmessageIcon).Replace("*", "Invoice"); ;
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrSales.Enabled = true;
                                return;
                            }

                            if (MobjclsBLLSTSales.FillCombos(new string[] { "StatusID", strTableSalesInvoice, "SalesInvoiceID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID }).Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.SDelivered)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2065, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                            if (MobjclsBLLSTSales.DeleteSalesInvoice())
                            {
                                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrFocus.Enabled = true;
                                AddNew();
                            }
                            else
                            {
                                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrFocus.Enabled = true;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorDeleteItem_Click() " + ex.Message);
                MobjLogs.WriteLog("Error in BindingNavigatorDeleteItem_Click() " + ex.Message, 2);
            }
        }

        private void cboCompany_SelectedValueChanged(object sender, EventArgs e)
        {
            Changestatus();
            LoadCombos(6); // Filltype 6 for Currency combo filling
            LoadCombos(7);
        }

        private void dgvSales_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvSales.IsCurrentCellDirty)
            {
                if (dgvSales.CurrentCell != null)
                    dgvSales.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void BindingNavigatorClearItem_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void FrmSalesNew_Shown(object sender, EventArgs e)
        {
            cboCompany.Focus();
        }

        private void dgvSales_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            CalculateTotalAmt();
            CalculateNetTotal();
            MobjClsCommonUtility.SetSerialNo(dgvSales, e.RowIndex, false);
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus();
            
            MobjclsBLLSTSales.GetExchangeCurrencyRate(Convert.ToInt32(cboCompany.SelectedValue), Convert.ToInt32(cboCurrency.SelectedValue));
            foreach (DataGridViewRow dr in dgvSales.Rows)
            {
                if (dr.Cells["ItemID"].Value != null && dr.Cells["ItemID"].Value.ToDecimal() != 0)
                {
                    //FillDiscountColumn(dr.Index, false);
                    //DataTable dtItemDetails = new DataTable();
                    //  if (dr.Cells["IsGroup"].Value != null && Convert.ToBoolean(dr.Cells["IsGroup"].Value) == true)
                    //      dtItemDetails = MobjclsBLLSTSales.FillCombos(new string[] { "SaleAmount as SaleRate", "STItemGroupMaster", "ItemGroupID = " + dr.Cells["ItemID"].Value.ToInt32() });
                    //   else
                    //      dtItemDetails = MobjclsBLLSTSales.FillCombos(new string[] { "SaleRate", strTableItemDetails, "ItemID = " + dr.Cells["ItemID"].Value.ToInt32() });
                    // if (dtItemDetails.Rows.Count > 0)
                    //  {
                    //decimal decRate = dtItemDetails.Rows[0]["SaleRate"].ToDecimal();
                    int intUomID = dr.Cells["Uom"].Tag.ToInt32();
                    int intItemID = dr.Cells["ItemID"].Value.ToInt32();

                    //  DataTable datSalesRate = FillDataGridCombo(intItemID, Convert.ToInt32(cboCustomer.SelectedValue), Convert.ToBoolean(dgvSales.Rows[dr.Index].Cells["IsGroup"].Value));
                    //   DataRow[] dtrSalesRate = datSalesRate.Select("UOMID=" + intUomID);

                    //   if (dtrSalesRate.Length > 0)
                    //   {
                    //       decRate = (Convert.ToDecimal(dtrSalesRate[0].ItemArray[5].ToString()) / MobjclsBLLSTSales.clsDTOSTSalesMaster.decExchangeCurrencyRate).ToString("F" + MintExchangeCurrencyScale).ToDecimal();
                    //   }

                    dr.Cells["Rate"].Value = MobjClsCommonUtility.GetSaleRate(intItemID, intUomID, dr.Cells["BatchID"].Value.ToInt64(), cboCompany.SelectedValue.ToInt32());
                    // }
                }
            }
           
            LoadCombos(5);
            int intBaseCurrencyID = 0;
            string strTemp = MobjclsBLLSTSales.GetCompanyCurrency(Convert.ToInt32(cboCompany.SelectedValue), out MintBaseCurrencyScale, out intBaseCurrencyID);
            cboCurrency.SelectedValue = intBaseCurrencyID;
        }

        private void cboCurrency_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cboCurrency.SelectedValue != null)
            {
                DataTable datCurrency = MobjclsBLLSTSales.FillCombos(new string[] { "CurrencyID,Scale", "CurrencyReference", "CurrencyID = " + Convert.ToInt32(cboCurrency.SelectedValue) });
                if (datCurrency.Rows.Count > 0)
                    MintExchangeCurrencyScale = Convert.ToInt32(datCurrency.Rows[0]["Scale"]);
            }
            CalculateTotalAmt();
            CalculateNetTotal();
            CalculateExchangeCurrencyRate();
        }

        private void bnSubmitForApproval_Click(object sender, EventArgs e)
        {
            if (txtOrderNo.Tag != null && Convert.ToString(txtOrderNo.Tag) != "" && Convert.ToString(txtOrderNo.Tag) != "0")
            {
                if (MintFromForm == 2 && cboPaymentTerms.SelectedValue.ToInt32() == (int)PaymentTerms.Credit)
                {
                    decimal decAdvanceLimit = MobjClsCommonUtility.FillCombos(new string[] { "AdvanceLimit", "InvVendorInformation", "VendorID = " + cboCustomer.SelectedValue.ToInt32() }).Rows[0]["AdvanceLimit"].ToDecimal();
                    decAdvanceLimit = txtTotalAmount.Text.ToDecimal() * (decAdvanceLimit / 100);

                    decimal decPaidAmount = 0;
                    if (decAdvanceLimit > 0)
                    {
                        DataTable datReceiptDetails = MobjClsCommonUtility.FillCombos(new string[] { "IsNull(Sum(Amount),0) as TotalAmount", strTableReceiptAndPayment, "Payments.OperationTypeID = " + (int)OperationType.SalesOrder + " And RPD.ReferenceID = " + MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID + " And Payments.ReceiptAndPaymentTypeID  = " + (int)PaymentTypes.AdvancePayment });
                        if (datReceiptDetails.Rows.Count > 0)
                        {
                            decPaidAmount = datReceiptDetails.Rows[0]["TotalAmount"].ToDecimal();
                        }
                    }

                    if (decPaidAmount < decAdvanceLimit)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2083, out MmessageIcon).Replace("#", "");
                        MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

                MblnSubmitForApprovalFlag = true;
                BindingNavigatorSaveItem_Click(sender, e);
                MblnSubmitForApprovalFlag = false;
            }
        }

        private void btnAddressChange_Click(object sender, EventArgs e)
        {
            CMSVendorAddress.Show(btnTContextmenu, btnTContextmenu.PointToClient(System.Windows.Forms.Cursor.Position));
        }

        private void cboSCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable datCombos = null; //bool blnIsEnabled = false;
            string strSearchCondition = "";
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                strSearchCondition = "UM.RoleID Not In (1,2)";
            }
            if (!string.IsNullOrEmpty(strSearchCondition))
                strSearchCondition = strSearchCondition + " AND ";
            strSearchCondition = strSearchCondition + "UM.UserID <> 1";

            //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
            //{
            if (cboSCompany.SelectedValue == null || Convert.ToInt32(cboSCompany.SelectedValue) == -2)
            {
                //DataTable datCompany = (DataTable)cboSCompany.DataSource;
                //string strFilterCondition = "";
                //if (datCompany.Rows.Count > 0)
                //{
                //    strFilterCondition = "CompanyID In (";
                //    foreach (DataRow dr in datCompany.Rows)
                //        strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                //    strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                //    strFilterCondition += ")";
                //}
                //datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN " + strTableUserMaster + " UM " +
                //  " ON EM.EmployeeID=UM.EmployeeID INNER JOIN " + MstrTempTableName + " IM ON UM.UserID=IM.CreatedBy", "IM." + strFilterCondition });
                datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "UM.UserID,IsNull(EM.FirstName,UM.UserName) as UserName", "UserMaster UM Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID", strSearchCondition });
            }
            else
            {
                //datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN " + strTableUserMaster + " UM " +
                //    " ON EM.EmployeeID=UM.EmployeeID INNER JOIN " + MstrTempTableName + " IM ON UM.UserID=IM.CreatedBy", "IM.CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue) });
                if (!string.IsNullOrEmpty(strSearchCondition))
                    strSearchCondition = " And " + strSearchCondition;
                datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "UM.UserID,IsNull(EM.FirstName,UM.UserName) as UserName", "UserMaster UM Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID ", "Case IsNull(EM.EmployeeID,0) When 0 Then " + cboSCompany.SelectedValue.ToInt32() + " Else EM.CompanyID  End = " + cboSCompany.SelectedValue.ToInt32() + "" + strSearchCondition });
            }
            //}
            //else
            //{
            //    //if (cboSCompany.SelectedValue == null || Convert.ToInt32(cboSCompany.SelectedValue) == -2)
            //    //{
            //    //    datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.FirstName","EmployeeMaster EM INNER JOIN " + strTableUserMaster + " UM " +
            //    //        " ON EM.EmployeeID=UM.EmployeeID INNER JOIN " + MstrTempTableName + " IM ON UM.UserID=IM.CreatedBy", "" +
            //    //        " IM.CompanyID IN(SELECT CompanyID FROM STRoleFIeldsDetails WHERE RoleID=" + ClsCommonSettings.RoleID + 
            //    //        " AND ControlID=" + MintTempEmployee + " AND IsVisible=1)" });
            //    //}
            //    //else
            //    //{
            //    //    datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.FirstName","EmployeeMaster EM INNER JOIN " + strTableUserMaster + " UM " +
            //    //        " ON EM.EmployeeID=UM.EmployeeID INNER JOIN " + MstrTempTableName + " IM ON UM.UserID=IM.CreatedBy", "" +
            //    //        " IM.CompanyID IN(SELECT CompanyID FROM STRoleFIeldsDetails WHERE RoleID=" + ClsCommonSettings.RoleID + 
            //    //        " AND ControlID=" + MintTempEmployee + " AND IsVisible=1 AND CompanyID=" + Convert.ToInt32(cboSCompany.SelectedValue) + ")" });
            //    //}
            //    try
            //    {
            //        DataTable datTemp = MobjclsBLLSTSales.FillCombos(new string[] { "IsVisible", "STRoleFieldsDetails", "" + 
            //            "RoleID=" + ClsCommonSettings.RoleID + " AND CompanyID=" + ClsCommonSettings.CompanyID + " " +
            //            "AND ControlID=" + MintTempEmployee + " AND IsVisible=1" });
            //        if (datTemp != null)
            //            if (datTemp.Rows.Count > 0)
            //                datCombos = MobjclsBLLSTSales.GetEmployeeByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, MintTempCompany);

            //        if (datCombos == null)
            //            datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN " + strTableUserMaster + " UM " +
            //                            " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
            //        else
            //        {
            //            if (datCombos.Rows.Count == 0)
            //                datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN " + strTableUserMaster + " UM " +
            //                            " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
            //        }
            //    }
            //    catch { }
            //}
            cboSExecutive.ValueMember = "UserID";
            cboSExecutive.DisplayMember = "UserName";
            cboSExecutive.DataSource = datCombos;
        }

        private void btnAccountSettings_Click(object sender, EventArgs e)
        {
            using (FrmAccountSettings objFrmAccountSettings = new FrmAccountSettings())
            {
                objFrmAccountSettings.ShowDialog();
            }
        }

        private void dtpOrderDate_ValueChanged(object sender, EventArgs e)
        {
            Changestatus();
            if (MblnIsEditable)
            {
                //foreach (DataGridViewRow dr in dgvSales.Rows)
                //{
                //    if (dr.Cells["ItemID"].Value.ToInt32() != 0)
                //    {
                //        FillDiscountColumn(dr.Index, false);
                //    }
                //}
                txtSubtotal_TextChanged(null, null);
            }
        }

        private void dgvSales_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (dgvSales.ReadOnly)
                e.Cancel = true;
            else if (MintFromForm == 2)
            {
                if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.SOTFromQuotation)
                {
                    e.Cancel = true;
                }
            }
        }

        private void dgvSales_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == ItemCode.Index) || (e.ColumnIndex == ItemName.Index))
            {
                dgvSales.PiColumnIndex = e.ColumnIndex;
            }
        }

        private void btnCustomerHistory_Click(object sender, EventArgs e)
        {
            if (cboCustomer.SelectedIndex == -1)
            {
                //MsMessageCommon = "Please select Customer.";
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2002, out MmessageIcon);
                ErrSales.SetError(cboCustomer, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblSalesOrderstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrSales.Enabled = true;
                cboCustomer.Focus();
                return;
            }
            if (cboCustomer.SelectedIndex != -1)
            {
                using (FrmVendorHistory objFrmVendorHistory = new FrmVendorHistory(3, cboCustomer.SelectedValue.ToInt32()))
                {
                    objFrmVendorHistory.ShowDialog();
                }
            }
        }

        private void txtSubtotal_TextChanged(object sender, EventArgs e)
        {
            int intDiscountTypeID = cboDiscount.SelectedValue.ToInt32();
            LoadCombos(5);
            cboDiscount.SelectedValue = intDiscountTypeID;
        }

        #endregion

        private void btnShowComments_Click(object sender, EventArgs e)
        {
            dgvSuggestions.Focus();
            DataTable datVerification = new DataTable();
            if (PintApprovalPermission == (int)ApprovePermission.Approve || PintApprovalPermission == (int)ApprovePermission.Both)
                datVerification = GetVerificationHistoryStatus(true);
            else
                datVerification = GetVerificationHistoryStatus(false);
            dgvSuggestions.Rows.Clear();
            for (int i = 0; i < datVerification.Rows.Count; i++)
            {
                dgvSuggestions.Rows.Add();
                dgvSuggestions[UserName.Index, i].Value = datVerification.Rows[i]["UserName"];
                dgvSuggestions[Status.Index, i].Value = datVerification.Rows[i]["Status"];
                dgvSuggestions[VerifiedDate.Index, i].Value = datVerification.Rows[i]["VerifiedDate"];
                dgvSuggestions[Comment.Index, i].Value = datVerification.Rows[i]["Comment"];
            }
        }

        private void btnSuggest_Click(object sender, EventArgs e)
        {
            try
            {
                switch (MintFromForm)
                {
                    case 1:

                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2073, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation");
                        break;
                    case 2:
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2073, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");
                        break;
                }

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {

                    using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                    {
                        objFrmCancellationDetails.Text = "Suggestion Details";
                        objFrmCancellationDetails.lblCancelledDate.Text = "Suggested Date";
                        objFrmCancellationDetails.ShowDialog();
                        MobjclsBLLSTSales.clsDTOSTSalesMaster.strDescription = objFrmCancellationDetails.PStrDescription;
                        int intOperationTypeID = 0;
                        if (!objFrmCancellationDetails.PBlnIsCanelled)
                        {
                            if (MintFromForm == 1)
                            {
                                MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SQuotationSuggested;
                                intOperationTypeID = (int)OperationType.SalesQuotation;
                            }
                            else if (MintFromForm == 2)
                            {
                                MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SOrderSuggested;
                                intOperationTypeID = (int)OperationType.SalesOrder;
                            }
                            MobjclsBLLSTSales.clsDTOSTSalesMaster.intCancelledBy = ClsCommonSettings.UserID;
                            MobjclsBLLSTSales.clsDTOSTSalesMaster.strCancellationDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");

                            if (MobjclsBLLSTSales.Suggest(intOperationTypeID))
                            {
                                AddNew();
                            }
                            else
                            {
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnSuggest_Click() " + ex.Message);

                MobjLogs.WriteLog("Error in btnSuggest_Click() " + ex.Message, 2);
            }
        }

        private void btnDeny_Click(object sender, EventArgs e)
        {
            try
            {
                switch (MintFromForm)
                {
                    case 1:

                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2074, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation");
                        break;
                    case 2:
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2074, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");
                        break;
                }

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {

                    using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                    {
                        objFrmCancellationDetails.Text = "Suggestion Details";
                        objFrmCancellationDetails.lblCancelledDate.Text = "Suggested Date";
                        objFrmCancellationDetails.ShowDialog();
                        MobjclsBLLSTSales.clsDTOSTSalesMaster.strDescription = objFrmCancellationDetails.PStrDescription;
                        int intOperationTypeID = 0;
                        if (!objFrmCancellationDetails.PBlnIsCanelled)
                        {
                            if (MintFromForm == 1)
                            {
                                MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SQuotationDenied;
                                intOperationTypeID = (int)OperationType.SalesQuotation;
                            }
                            else if (MintFromForm == 2)
                            {
                                MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID = (Int32)OperationStatusType.SOrderDenied;
                                intOperationTypeID = (int)OperationType.SalesOrder;
                            }
                            MobjclsBLLSTSales.clsDTOSTSalesMaster.intCancelledBy = ClsCommonSettings.UserID;
                            MobjclsBLLSTSales.clsDTOSTSalesMaster.strCancellationDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");

                            if (MobjclsBLLSTSales.Suggest(intOperationTypeID))
                            {
                                AddNew();
                            }
                            else
                            {
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnSuggest_Click() " + ex.Message);

                MobjLogs.WriteLog("Error in btnSuggest_Click() " + ex.Message, 2);
            }
        }

        private void expandableSplitterLeft_ExpandedChanged(object sender, ExpandedChangeEventArgs e)
        {
            if (expandableSplitterLeft.Expanded)
            {
                if (dgvSalesDisplay.Columns.Count > 0)
                    dgvSalesDisplay.Columns[0].Visible = false;
            }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();

            switch (MintFromForm)
            {
                case 1: // Sales Quotation
                    objHelp.strFormName = "Salesquotation";

                    break;
                case 2: // Sales Order
                    objHelp.strFormName = "SalesOrder";
                    break;
                case 3: // Sales Invoice
                    objHelp.strFormName = "Salesinvoice";
                    break;
            }

            objHelp.ShowDialog();
            objHelp = null;
        }

        private void tcGeneral_SelectedTabChanged(object sender, TabStripTabChangedEventArgs e)
        {
            if (tcGeneral.SelectedTab == tiSuggestions)
            {
                DataTable datVerification = new DataTable();
                if (PintApprovalPermission == (int)ApprovePermission.Approve || PintApprovalPermission == (int)ApprovePermission.Both)
                    datVerification = GetVerificationHistoryStatus(true);
                else
                    datVerification = GetVerificationHistoryStatus(false);
                dgvSuggestions.Rows.Clear();
                for (int i = 0; i < datVerification.Rows.Count; i++)
                {
                    dgvSuggestions.Rows.Add();
                    dgvSuggestions[UserID.Index, i].Value = datVerification.Rows[i]["UserID"];
                    dgvSuggestions[UserName.Index, i].Value = datVerification.Rows[i]["UserName"];
                    dgvSuggestions[Status.Index, i].Value = datVerification.Rows[i]["Status"];
                    dgvSuggestions[VerifiedDate.Index, i].Value = datVerification.Rows[i]["VerifiedDate"];
                    dgvSuggestions[Comment.Index, i].Value = datVerification.Rows[i]["Comment"];
                    if (datVerification.Rows[i]["UserID"].ToInt32() != ClsCommonSettings.UserID)
                        dgvSuggestions[Comment.Index, i].ReadOnly = true;
                }
            }
        }

        private void dgvSuggestions_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == Comment.Index)
            {
                if (dgvSuggestions.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                {
                    long lngReferenceID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID;
                    if (MintFromForm == 2)
                        lngReferenceID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID;
                    MobjclsBLLSTSales.UpdationVerificationTableRemarks(MintFromForm, lngReferenceID, dgvSuggestions.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
            }
        }

        private void txtTotalAmount_TextChanged(object sender, EventArgs e)
        {
            string strAmount = txtTotalAmount.Text;
            int intCurrency = Convert.ToInt32(cboCurrency.SelectedValue);
            lblAmountInWords.Text = MobjClsCommonUtility.ConvertToWord(strAmount, intCurrency);
        }

        private void lnkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DataTable datAdvanceSearchedData = new DataTable();
            switch (MintFromForm)
            {

                case 1: // Sales Quotation
                    using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.SalesQuotation))
                    {
                        objFrmSearchForm.ShowDialog();
                        if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                        {
                            datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                            datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                            dgvSalesDisplay.DataSource = datAdvanceSearchedData;
                            dgvSalesDisplay.Columns["ID"].Visible = false;
                            dgvSalesDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        }
                    }
                    break;
                case 2: // Sales Order
                    using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.SalesOrder))
                    {
                        objFrmSearchForm.ShowDialog();
                        if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                        {
                            datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                            datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                            dgvSalesDisplay.DataSource = datAdvanceSearchedData;
                            dgvSalesDisplay.Columns["ID"].Visible = false;
                            dgvSalesDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        }
                    }
                    break;
                case 3: // Sales Invoice

                    using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.SalesInvoice))
                    {
                        objFrmSearchForm.ShowDialog();
                        if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                        {
                            datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                            datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                            dgvSalesDisplay.DataSource = datAdvanceSearchedData;
                            dgvSalesDisplay.Columns["ID"].Visible = false;
                            dgvSalesDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        }
                    }
                    break;
            }

        }

        private void FrmSales_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.MdiFormClosing)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4214, out MmessageIcon).Replace("#", "").Trim();
                if (!MblnChangeStatus || MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                    DialogResult.Yes)
                {

                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void txtDiscount_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
            CalculateNetTotal();
        }

        private void dgvSales_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjClsCommonUtility.SetSerialNo(dgvSales, e.RowIndex, false);
        }

       

        private void btnActions_PopupOpen(object sender, PopupOpenEventArgs e)
        {
            int intTempOperationTypeID = 0;
            long lngReferenceID = 0;
            bool blnTempEditable = true;

            if (MintFromForm == 1)
            {
                intTempOperationTypeID = (int)OperationType.SalesQuotation;
                lngReferenceID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesQuotationID;
                if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SQuotationOpen && MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SQuotationRejected && MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SQuotationCancelled)
                    blnTempEditable = false;
            }
            else if (MintFromForm == 2)
            {
                intTempOperationTypeID = (int)OperationType.SalesOrder;
                lngReferenceID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesOrderID;

                if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SOrderOpen && MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SOrderRejected && MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SOrderCancelled)
                    blnTempEditable = false;
            }
            else if (MintFromForm == 3)
            {
                intTempOperationTypeID = (int)OperationType.SalesInvoice;
                lngReferenceID = MobjclsBLLSTSales.clsDTOSTSalesMaster.intSalesInvoiceID;
                if (MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SInvoiceOpen && MobjclsBLLSTSales.clsDTOSTSalesMaster.intStatusID != (int)OperationStatusType.SInvoiceCancelled)
                    blnTempEditable = false;
            }


            bool blnShowDocumentForm = clsUtilities.IsDocumentExists(intTempOperationTypeID, lngReferenceID);
            if (blnShowDocumentForm == false)
            {
                if (blnTempEditable)
                    blnShowDocumentForm = true;
            }

            if (blnShowDocumentForm == false)
                btnDocuments.Enabled = false;
            else
                btnDocuments.Enabled = true;
        }

        private void ShowItemHistory_Click(object sender, EventArgs e)
        {
            using (FrmCustomerSaleHistory objFrmCustomerSaleHistory = new FrmCustomerSaleHistory())
            {
                objFrmCustomerSaleHistory.intCompanyID = this.cboCompany.SelectedValue.ToInt32();
                objFrmCustomerSaleHistory.intCustomerID = this.cboCustomer.SelectedValue.ToInt32();
                objFrmCustomerSaleHistory.intItemID = SItemID;
                objFrmCustomerSaleHistory.intIsGroup = SIsGroup;
                objFrmCustomerSaleHistory.ShowDialog();
            }
        }

        private void dgvSales_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (MintFromForm == 1 || MintFromForm == 2 || MintFromForm == 3)
            {
                if (e.RowIndex >= 0)
                {
                    int intCurrentColumn = dgvSales.CurrentCell.ColumnIndex;

                    if (this.cboCompany.SelectedIndex >= 0 && cboCustomer.SelectedIndex >= 0)
                    {
                        if (intCurrentColumn == 0 || intCurrentColumn == 1)
                        {
                            if (e.Button == MouseButtons.Right)
                            {
                                if (dgvSales.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt32() > 0)
                                {
                                    SItemID = dgvSales.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt32();
                                    SIsGroup = Convert.ToBoolean(dgvSales.Rows[e.RowIndex].Cells["IsGroup"].Value) == true ? 1 : 0;
                                    CntxtHistory.Items["showGroupItemDetails"].Visible = SIsGroup == 1 ? true : false;
                                    this.CntxtHistory.Show(this.dgvSales, this.dgvSales.PointToClient(Cursor.Position));
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ShowSaleRate_Click(object sender, EventArgs e)
        {
            using (FrmSaleRateHistory objFrmSaleRateHistory = new FrmSaleRateHistory())
            {
                objFrmSaleRateHistory.ItemID = SItemID;
                objFrmSaleRateHistory.CompanyID = this.cboCompany.SelectedValue.ToInt32();
                objFrmSaleRateHistory.ShowDialog();
            }
        }

        private void cboPaymentTerms_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedIndex != -1)
            {
                int intCompanyAcc = cboCompany.SelectedValue.ToInt32();
                LoadCombos(10);
                if (cboPaymentTerms.SelectedIndex != -1)
                {
                    switch (MintFromForm)
                    {
                        case 3:
                            if (cboPaymentTerms.SelectedValue.ToInt32() == (int)PaymentTerms.Credit)
                            {

                                cboAccount.SelectedValue = MobjclsBLLSTSales.GetCompanyAccountID(Convert.ToInt32(TransactionTypes.CreditSale), intCompanyAcc);
                            }
                            else
                            {
                                cboAccount.SelectedValue = MobjclsBLLSTSales.GetCompanyAccountID(Convert.ToInt32(TransactionTypes.CashSale), intCompanyAcc);
                            }
                            break;
                    }
                }
            }
        }

        private void cblSaleQuotationNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplayDNote();
        }


        private void DisplayDeliveryNoteDetDetails(string strCondition)
        {
            dgvSales.Rows.Clear();
            dgvSales.RowCount = 1;
            DataTable datSalesDetail = null;
            decimal salesRate = 0;

            DataSet dsSales = MobjclsBLLSTSales.FillGridAganistDelivertNote(strCondition, Convert.ToInt32(cboCompany.SelectedValue));
            datSalesDetail = dsSales.Tables[0];
            if (dsSales.Tables[1].Rows.Count > 0)
            txtLpoNo.Text = Convert.ToString(dsSales.Tables[1].Rows[0][0]);
           // datSalesDetail = MobjclsBLLSTSales.FillGridAganistDelivertNote(strCondition, Convert.ToInt32(cboCompany.SelectedValue));


            if (dgvSales.Rows.Count > 0)
            {
                for (int intICounter = 0; intICounter < datSalesDetail.Rows.Count; intICounter++)
                {
                    int intItemID = Convert.ToInt32(datSalesDetail.Rows[intICounter]["ItemID"]);
                    int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                    FillDataGridCombo(intItemID, intVendorID, Convert.ToBoolean(datSalesDetail.Rows[intICounter]["IsGroup"]));

                    dgvSales.RowCount = dgvSales.RowCount + 1;
                    dgvSales.Rows[intICounter].Cells["ItemID"].Value = datSalesDetail.Rows[intICounter]["ItemID"];
                    dgvSales.Rows[intICounter].Cells["BatchID"].Value = datSalesDetail.Rows[intICounter]["BatchID"];
                    dgvSales.Rows[intICounter].Cells["BatchNo"].Value = datSalesDetail.Rows[intICounter]["BatchNo"];
                    dgvSales.Rows[intICounter].Cells["IsGroup"].Value = Convert.ToInt32(datSalesDetail.Rows[intICounter]["IsGroup"]);
                    dgvSales.Rows[intICounter].Cells["ItemCode"].Value = datSalesDetail.Rows[intICounter]["Code"];
                    dgvSales.Rows[intICounter].Cells["ItemName"].Value = datSalesDetail.Rows[intICounter]["ItemName"];
                    // dgvSales.Rows[intICounter].Cells["Quantity"].Value = datSalesDetail.Rows[intICounter]["Quantity"];

                   
                    int intUomScale = 0;
                    if (dgvSales[Uom.Index, intICounter].Tag.ToInt32() != 0)
                        intUomScale = MobjclsBLLSTSales.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvSales[Uom.Index, intICounter].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                    dgvSales.Rows[intICounter].Cells["Quantity"].Value = datSalesDetail.Rows[intICounter]["Quantity"].ToDecimal().ToString("F" + intUomScale);
                    if (datSalesDetail.Rows[intICounter]["Rate"].ToDecimal() > 0)
                    {
                        dgvSales.Rows[intICounter].Cells["Rate"].Value = datSalesDetail.Rows[intICounter]["Rate"];
                    }
                    else
                    {
                        salesRate = MobjclsBLLSTSales.GetSalesRateFormDeliveryNote(strCondition, cboCompany.SelectedValue.ToInt32(), dgvSales.Rows[intICounter].Cells["ItemID"].Value.ToInt32(), dgvSales.Rows[intICounter].Cells["BatchID"].Value.ToInt32());
                        dgvSales.Rows[intICounter].Cells["Rate"].Value = salesRate;
                    }
                    dgvSales.Rows[intICounter].Cells["GrandAmount"].Value = ((dgvSales.Rows[intICounter].Cells["Quantity"].Value.ToDecimal()) * (dgvSales.Rows[intICounter].Cells["Rate"].Value.ToDecimal()));
                    dgvSales.Rows[intICounter].Cells["NetAmount"].Value = dgvSales.Rows[intICounter].Cells["GrandAmount"].Value.ToDecimal();
                    //txtLpoNo.Text = datSalesDetail.Rows[intICounter]["LPONumber"].ToString();

                    //FillDiscountColumn(intICounter, true);
                    //dgvSales.Rows[intICounter].Cells["Discount"].Tag = datSalesDetail.Rows[intICounter]["DiscountID"];
                    //dgvSales.Rows[intICounter].Cells["Discount"].Value = datSalesDetail.Rows[intICounter]["Discount"];
                    //dgvSales.Rows[intICounter].Cells["DiscountAmount"].Value = datSalesDetail.Rows[intICounter]["DiscountAmount"];


                }
            }

        }

        private void DisplayDirectDelivery()
        {
            clsBLLDirectDelivery ObjBLLDirectDelivery = new clsBLLDirectDelivery();
            ObjBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID = MlngDeliveryID;
            DataTable dtTemp = ObjBLLDirectDelivery.DisplayItemIssueMasterInfoForSales();
            if (dtTemp != null && dtTemp.Rows.Count > 0)
            {
                cboCompany.SelectedValue = dtTemp.Rows[0]["CompanyID"].ToString();
                cboCustomer.SelectedValue = dtTemp.Rows[0]["VendorID"].ToString();
                cboOrderType.SelectedValue = (Int32)OperationOrderType.SOTFromDeliveryNote;
                
                ((ListBox)(cblSaleQuotationNo)).DataSource = dtTemp;
                ((ListBox)(cblSaleQuotationNo)).ValueMember = "ItemIssueID";
                ((ListBox)(cblSaleQuotationNo)).DisplayMember = "ItemIssueNo";
                for (int i = 0; i < cblSaleQuotationNo.Items.Count; i++)
                {
                    if (MlngDeliveryID == (((System.Data.DataRowView)(cblSaleQuotationNo.Items[i])).Row.ItemArray[0]).ToInt64())
                    {
                        cblSaleQuotationNo.SetItemChecked(i, true);
                    }
                    else
                    {
                        cblSaleQuotationNo.SetItemChecked(i, false);
                    }
                    cblSaleQuotationNo_SelectedIndexChanged(null, null);
                }
                cboOrderType.Enabled = false;
                cboCompany.Enabled = false;
                cboCustomer.Enabled = false;
                txtVendorAddress.Enabled = false;
                cblSaleQuotationNo.Enabled = false;
                cboCurrency.Enabled = false;
                btnCustomer.Enabled = false;
                btnAddressChange.Enabled = false;

            }
        }

        private void txtDiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in txtDiscountAmount_KeyPress() " + ex.Message);
                MobjLogs.WriteLog("Error in txtDiscountAmount_KeyPress() " + ex.Message, 2);
            }
        }

        public void DisplayDNote()
        {

            DataTable datCombos = new DataTable();
            string strCondition = "";
            if (MlngDeliveryID == 0)
            {
                //strCondition = "OrderTypeID = " + (int)OperationOrderType.DNOTDirect + " AND CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + " AND VendorID = " + Convert.ToInt32(cboCustomer.SelectedValue);
                //datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "ItemIssueID,ItemIssueNo", strTableInvItemIssueMaster, strCondition });
                if (MblnAddStatus)
                {
                    datCombos = MobjclsBLLSTSales.FillDeliveryNote(cboCompany.SelectedValue.ToInt64(), cboCustomer.SelectedValue.ToInt64(), (Int64)OperationOrderType.DNOTDirect);
                }
                else
                {
                    datCombos = MobjclsBLLSTSales.FillDeliveryNoteDetails(cboCompany.SelectedValue.ToInt64(), txtOrderNo.Tag.ToInt64(), cboCustomer.SelectedValue.ToInt64(), (Int64)OperationOrderType.DNOTDirect);
                }
                ((ListBox)cblSaleQuotationNo).DataSource = datCombos;
                ((ListBox)cblSaleQuotationNo).ValueMember = "ItemIssueID";
                ((ListBox)cblSaleQuotationNo).DisplayMember = "ItemIssueNo";
            }
            strCondition = string.Empty;

            strCondition = strCondition + "IIM.ItemIssueID IN(";

            foreach (DataRowView rowView in cblSaleQuotationNo.CheckedItems)
            {
                strCondition = strCondition + (rowView["ItemIssueID"]).ToString() + ",";
                //salesorderno[i] = (rowView["SalesQuotationID"]).ToInt32();
                //i++;
               
            }
            strCondition = strCondition.Remove(strCondition.Length - 1, 1);
            strCondition = strCondition + ")";
            strCondition1 = strCondition;
            if (cblSaleQuotationNo.CheckedItems.Count > 0)
            {
                DisplayDeliveryNoteDetDetails(strCondition);
                CalculateTotalAmt();
                CalculateNetTotal();
            }
            else
            {
                dgvSales.Rows.Clear();
            }
        }
        public void UncheckedCheckListBox()
        {
            foreach (int i in cblSaleQuotationNo.CheckedIndices)
            {
              cblSaleQuotationNo.SetItemCheckState(i, CheckState.Unchecked);
            }
        }

        public void FillDNoteInUpdateMode()
        {
            DataTable datCombos = new DataTable();

            datCombos = MobjclsBLLSTSales.FillDeliveryNoteDetails(cboCompany.SelectedValue.ToInt64(), txtOrderNo.Tag.ToInt64(), cboCustomer.SelectedValue.ToInt64(), (Int64)OperationOrderType.DNOTDirect);

            ((ListBox)cblSaleQuotationNo).DataSource = datCombos;
            ((ListBox)cblSaleQuotationNo).ValueMember = "ItemIssueID";
            ((ListBox)cblSaleQuotationNo).DisplayMember = "ItemIssueNo";
        }

      
        private void showGroupItemDetails_Click(object sender, EventArgs e)
        {
            frmItemGroupMaster objfrmItemGroupMaster = new frmItemGroupMaster();
            objfrmItemGroupMaster.PintProductGroupID = dgvSales.Rows[dgvSales.CurrentRow.Index].Cells["ItemID"].Value.ToInt32();
            objfrmItemGroupMaster.ShowDialog();
        }

        private void dgvSales_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (cboCustomer.SelectedIndex >= 0 && e.RowIndex >= 0)
            //{
            //    if (e.ColumnIndex == dgvSales.Columns["ItemCode"].Index || e.ColumnIndex == dgvSales.Columns["ItemName"].Index)
            //    {
            //        DataTable dtDatasource = MobjclsBLLSTSales.GetDataForItemSelection(ClsCommonSettings.CompanyID, Convert.ToInt32(cboCurrency.SelectedValue), MintFromForm);

            //        using (frmItemSelection objItemSelection = new frmItemSelection(dtDatasource))
            //        {
            //            objItemSelection.ShowDialog();
            //        }
            //    }
            //}
        }

        private void cboTaxScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalculateNetTotal();
        }
    }
}
