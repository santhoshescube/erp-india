﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

using System.Text;

namespace MyBooksERP
{
    public partial class FrmPOS : DevComponents.DotNetBar.Office2007Form
    {
        private bool MblnChangeStatus;                    //Check state of the page
        private bool MblnAddStatus;                       //Add/Update mode 
        private bool MblnViewPermission = false;
        private bool MblnAddPermission = false;
        private bool MblnUpdatePermission = false;
        private bool MblnDeletePermission = false;
        private bool MblnAddUpdatePermission = false;
        private bool MblnCancelPermission = false;
        private bool MblnPrintEmailPermission = false;

        public clsDTOPosProductList  ProductList{get ;set; }
        private int MintExchangeCurrencyScale = 2;
        public long PlngReferenceID;

        ClsLogWriter MobjLogs;
        clsBLLSTSales MobjclsBLLSTSales;
        clsBLLPermissionSettings MobjClsBLLPermissionSettings;
        ClsNotificationNew mObjNotification;
        private MessageBoxIcon MmessageIcon;
        DataTable datMessages;
        string MstrCommonMessage;                   // for setting error message
        private clsBLLCommonUtility MobjclsBLLCommonUtility;

        private int SItemID;
        private int SIsGroup;

        clsBLLPOS MobjclsBLLPOS;
        public DataLayer objclsConnection { get; set; }

        public FrmPOS()
        {
            InitializeComponent();
            MobjclsBLLCommonUtility = new clsBLLCommonUtility();
        }

        private void FrmPointofSale_Load(object sender, EventArgs e)
        {
            try
            {
                tmrCreatedDateTime.Start();
                MobjclsBLLPOS = new clsBLLPOS();
                MobjLogs = new ClsLogWriter(Application.StartupPath);
                mObjNotification = new ClsNotificationNew();
                MmessageIcon = MessageBoxIcon.Information;
                MobjClsBLLPermissionSettings = new clsBLLPermissionSettings();
                SetPermissions();
                LoadMessage();
                LoadCombos(0);
                ClearSearchBoxControls();
                AddMode();
                wbCreatedBy.Text = "Cashier : " + ClsCommonSettings.strEmployeeName;
                clearMasterControls();
                cboPaymentMode.SelectedValue = 1;
                ClearControlVisibility();
                if(ProductList!=null)
                {
                      PosFromProduct();
                }
                if (cboPaymentMode.Items.Count > 0)
                    cboPaymentMode.SelectedIndex = 0;

                if (PlngReferenceID > 0)
                {
                    MobjclsBLLPOS.clsDTOPOS.intPOSID = Int32.Parse(PlngReferenceID.ToString());
                    EditMode();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FrmPointofSale_Load() " + ex.Message);
                MobjLogs.WriteLog("Error in FrmPointofSale_Load() " + ex.Message, 2);
            }
            
        }

        public void PosFromProduct()
        {
            try
            {

                fillMastersDynamically(ProductList.CompanyID, 0, ProductList.CustomerId, 0, 0);
                DataTable dtDatasource = MobjclsBLLPOS.GetDataForItemSelection(cboCompany.SelectedValue.ToInt32(), MobjclsBLLPOS.GetCurrencyID(cboCompany.SelectedValue.ToInt32()), cboWarehouse.SelectedValue.ToInt32());
                foreach (clsProductListTemp pRow in ProductList.ProductList)
                {
                    //dtDatasource.DefaultView.RowFilter = "ItemID = " + (int)pRow.ProductID + "";
                    decimal OrderedQuantity = pRow.Quantity;
                    decimal ActualQuantity = 0;
                    decimal CurrentQuantity = 0;
                    foreach (DataRow dr in dtDatasource.Select("ItemID = " + (int)pRow.ProductID + ""))
                    {
                        CurrentQuantity = 0;
                        if (dr["QtyAvailable"].ToDecimal() >= OrderedQuantity - ActualQuantity)
                        {
                            CurrentQuantity = OrderedQuantity - ActualQuantity;
                        }
                        else
                        {
                            CurrentQuantity = dr["QtyAvailable"].ToDecimal();
                        }
                        FillGridDynamically(cboCompany.SelectedValue.ToInt32(), dr["ItemID"].ToInt32(), dr["BatchID"].ToInt32(), CurrentQuantity);
                        ActualQuantity = ActualQuantity + CurrentQuantity;
                        if (OrderedQuantity == ActualQuantity)
                        { break; }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in PosFromProduct() " + ex.Message);
                MobjLogs.WriteLog("Error in PosFromProduct() " + ex.Message, 2);
            }
        }

        private void clearMasterControls()
        {
            if (ClsCommonSettings.CompanyID <= 0)
                ClsCommonSettings.CompanyID=MobjclsBLLCommonUtility.GetCompanyID();
            
            cboCompany.SelectedValue = ClsCommonSettings.CompanyID;
            cboCounter.SelectedIndex = -1;
            cboWarehouse.SelectedIndex = -1;
            cboPaymentMode.SelectedIndex = -1;
            cboCustomer.SelectedIndex = 0;
            txtBillNumber.Text = getBillNo();
            dtpPOSDate.Value = ClsCommonSettings.GetServerDateTime();
        }
        private void BtnHelp_Click(object sender, EventArgs e)
        {
            try
            {
                Help();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnHelp_Click() " + ex.Message);
                MobjLogs.WriteLog("Error in BtnHelp_Click() " + ex.Message, 2);
            }
        }
        private void SetPermissions()
        {
            try
            {
                 //Function for setting permissions
                clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
                if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    MobjClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.POS, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                    DataTable dtControlsPermissions = new DataTable();
                    dtControlsPermissions = MobjClsBLLPermissionSettings.GetControlsPermissions(ClsCommonSettings.RoleID, (Int32)eMenuID.POS, (int)ClsCommonSettings.CompanyID);
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //    MblnCancelPermission = true;
                    //else
                    //{
                    //    dtControlsPermissions.DefaultView.RowFilter = "ControlName = 'BindingNavigatorCancelItem'";
                    //    if (dtControlsPermissions.DefaultView.ToTable().Rows.Count > 0)
                    //        MblnCancelPermission = Convert.ToBoolean(dtControlsPermissions.DefaultView.ToTable().Rows[0]["IsVisible"]);//"IsEnabled"]);
                    //}
                    MblnCancelPermission = MblnUpdatePermission;
                }
                else
                {
                    MblnCancelPermission=MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

                   
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SetPermissions() " + ex.Message);
                MobjLogs.WriteLog("Error in SetPermissions() " + ex.Message, 2);
            }
        }
        private void ChangeStatus()
        {
            if (MobjclsBLLPOS.clsDTOPOS.blnAddMode==true)
            {
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnNew.Enabled = MblnAddPermission;
              
            }
            else
            {
                BindingNavigatorAddNewItem.Enabled = btnNew.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled  = MblnUpdatePermission;
            }
            BindingNavigatorClearItem.Enabled = true;

        }
        private void LoadMessage()
        {
            try
            {
                //Loads error messages
                datMessages = new DataTable();
                datMessages = mObjNotification.FillMessageArray((int)FormID.PointOfSales, (int)ClsCommonSettings.ProductID);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadMessage() " + ex.Message);
                MobjLogs.WriteLog("Error in Load Message() " + ex.Message, 2);
            }
        }
        private void LoadCombos(int intFillType)
        {
            try
            {
                //bool blnIsEnabled = false;
                DataTable datCombos = new DataTable();
                DataTable dtControlsPermissions = new DataTable();
                if (intFillType == 0)
                {
                    datCombos = null;
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                        datCombos = MobjclsBLLPOS.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                    //else
                    //    datCombos = MobjclsBLLPOS.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.POSCompany);
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;
                }
                if (intFillType == 0 || intFillType == 7)
                {

                    datCombos = MobjclsBLLPOS.FillCombos(new string[] { "SalesCounterID,SalesCounterName", "InvSalesCounterReference", "" });
                    cboCounter.ValueMember = "SalesCounterID";
                    cboCounter.DisplayMember = "SalesCounterName";
                    cboCounter.DataSource = datCombos;
                }
                if (intFillType == 0 || intFillType == 2)
                {
                    datCombos = MobjclsBLLPOS.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", "VendorTypeID="+(int)VendorType.Customer+" And StatusID = " + (int)VendorStatus.CustomerActive });
                    cboCustomer.ValueMember = "VendorID";
                    cboCustomer.DisplayMember = "VendorName";
                    DataRow dr = datCombos.NewRow();
                    dr["VendorID"] = 0;
                    dr["VendorName"] = "General Customer";
                    datCombos.Rows.InsertAt(dr, 0);
                    cboCustomer.DataSource = datCombos;
                }
                if (intFillType == 0 || intFillType == 3)
                {
                    //datCombos = MobjclsBLLPOS.FillCombos(new string[] { "PaymentTermsID,TermsName", "STPaymentTerms", "" });
                    //cboPaymentMode.ValueMember = "PaymentTermsID";
                    //cboPaymentMode.DisplayMember = "TermsName";
                    //cboPaymentMode.DataSource = datCombos;
                    datCombos = MobjclsBLLPOS.FillCombos(new string[] { "TransactionTypeID,TransactionType", "TransactionTypeReference", "TransactionTypeID=2" });
                    cboPaymentMode.ValueMember = "TransactionTypeID";
                    cboPaymentMode.DisplayMember = "TransactionType";
                    cboPaymentMode.DataSource = datCombos;
                }
                if (intFillType == 4)
                {
                    //datCombos = MobjclsBLLPOS.FillCombos(new string[] { "DiscountTypeID,DiscountShortName", "STDiscountTypeReference", "DiscountMode in ('N') And IsSaleType = 'True' and DiscountForAmount=1" });
                    DataTable datCurrencyID = MobjclsBLLPOS.FillCombos(new string[] { "CurrencyId", "CompanyMaster", "CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue.ToInt32()) });
                    if (datCurrencyID.Rows.Count > 0)
                    {
                        datCombos = MobjclsBLLPOS.GetDiscount(datCurrencyID.Rows[0]["CurrencyId"].ToInt32(), txtSubTotal.Text.ToDecimal(), dtpPOSDate.Value, cboCustomer.SelectedValue.ToInt32());
                        cboDiscountScheme.ValueMember = "DiscountID";
                        cboDiscountScheme.DisplayMember = "DiscountShortName";

                        //DataTable datCustomerWiseDiscounts = new DataTable();
                        //if (MblnIsEditable)
                        //{
                        //    datCustomerWiseDiscounts = MobjclsBLLSTSales.GetCustomerWiseDiscounts(cboCustomer.SelectedValue.ToInt32(), txtSubtotal.Text.ToDecimal(), dtpOrderDate.Value.ToString("dd-MMM-yyyy"));
                        //}
                        //else
                        //{
                        //    datCustomerWiseDiscounts = MobjclsBLLSTSales.GetCustomerWiseDiscounts(cboCustomer.SelectedValue.ToInt32(), txtSubtotal.Text.ToDecimal(), "");
                        //}
                        //foreach (DataRow drNew in datCustomerWiseDiscounts.Rows)
                        //{
                        //    DataRow drNew1 = datCombos.NewRow();
                        //    drNew1["DiscountTypeID"] = drNew["DiscountTypeID"];
                        //    drNew1["DiscountShortName"] = drNew["DiscountShortName"];
                        //    datCombos.Rows.Add(drNew1);
                        //}
                        DataRow dr = datCombos.NewRow();
                        dr["DiscountID"] = 0;
                        dr["DiscountShortName"] = "No Discount";
                        datCombos.Rows.InsertAt(dr, 0);
                        cboDiscountScheme.DataSource = datCombos;
                    }
                }
                //if (intFillType == 0 || intFillType == 5)
                //{
                //    datCombos = MobjclsBLLPOS.FillCombos(new string[] { "DiscountTypeID,DiscountShortName", "STDiscountTypeReference", "DiscountMode in ('N') And IsSaleType = 'True' and DiscountForAmount=1" });
                //    cboDiscountScheme.ValueMember = "DiscountTypeID";
                //    cboDiscountScheme.DisplayMember = "DiscountShortName";

                //    //DataTable datCustomerWiseDiscounts = new DataTable();
                //    //if (MblnIsEditable)
                //    //{
                //    //    datCustomerWiseDiscounts = MobjclsBLLSTSales.GetCustomerWiseDiscounts(cboCustomer.SelectedValue.ToInt32(), txtSubtotal.Text.ToDecimal(), dtpOrderDate.Value.ToString("dd-MMM-yyyy"));
                //    //}
                //    //else
                //    //{
                //    //    datCustomerWiseDiscounts = MobjclsBLLSTSales.GetCustomerWiseDiscounts(cboCustomer.SelectedValue.ToInt32(), txtSubtotal.Text.ToDecimal(), "");
                //    //}
                //    //foreach (DataRow drNew in datCustomerWiseDiscounts.Rows)
                //    //{
                //    //    DataRow drNew1 = datCombos.NewRow();
                //    //    drNew1["DiscountTypeID"] = drNew["DiscountTypeID"];
                //    //    drNew1["DiscountShortName"] = drNew["DiscountShortName"];
                //    //    datCombos.Rows.Add(drNew1);
                //    //}
                //    //DataRow dr = datCombos.NewRow();
                //    //dr["DiscountTypeID"] = 0;
                //    //dr["DiscountShortName"] = "No Discount";
                //    //datCombos.Rows.InsertAt(dr, 0);
                //    cboDiscountScheme.DataSource = datCombos;
                //}


                if (intFillType == 0)
                {
                    datCombos = null;
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    datCombos = MobjclsBLLPOS.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                    //else
                    //{
                    //    datCombos = MobjclsBLLPOS.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.POSCompany);
                    //    if (datCombos == null)
                    //        datCombos = MobjclsBLLPOS.FillCombos(new string[] { "CompanyID,Name", "CompanyMaster", "CompanyID = " + ClsCommonSettings.CompanyID });
                    //    else
                    //    {
                    //        if (datCombos.Rows.Count == 0)
                    //            datCombos = MobjclsBLLPOS.FillCombos(new string[] { "CompanyID,Name", "CompanyMaster", "CompanyID = " + ClsCommonSettings.CompanyID });
                    //    }
                    //}
                    cboCompanySearch.ValueMember = "CompanyID";
                    cboCompanySearch.DisplayMember = "CompanyName";
                    cboCompanySearch.DataSource = datCombos;
                }
                if (intFillType == 0)
                {
                    datCombos = MobjclsBLLPOS.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", "VendorTypeID="+(int)VendorType.Customer+" And VendorID in (Select VendorID from InvPOSMaster )And StatusID = " + (int)VendorStatus.CustomerActive });
                    cboCustomerSearch.ValueMember = "VendorID";
                    cboCustomerSearch.DisplayMember = "VendorName";
                    cboCustomerSearch.DataSource = datCombos;
                }
                if (intFillType == 0)
                {
                    datCombos = null;
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //{
                        //datCombos = MobjclsBLLPOS.FillCombos(new string[] { "EM.EmployeeID,isnull(EM.[EmployeeFullName],UM.[UserName]) as [EmployeeFullName]", "[UserMaster]  UM left join [EmployeeMaster] EM on EM.[EmployeeId]=UM.[EmployeeId]", "UM.UserID in(Select CreatedBy from STPOSMaster)" });
                        if (cboCompanySearch.SelectedValue == null || Convert.ToInt32(cboCompanySearch.SelectedValue) == -2)
                        {
                            DataTable datCompany = (DataTable)cboCompanySearch.DataSource;
                            string strFilterCondition = "";
                            if (datCompany.Rows.Count > 0)
                            {
                                strFilterCondition = "CompanyID In (";
                                foreach (DataRow dr in datCompany.Rows)
                                    strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                                strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                                strFilterCondition += ")";
                            }
                            datCombos = MobjclsBLLPOS.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
                        " ON EM.EmployeeID=UM.EmployeeID INNER JOIN InvPOSMaster IM ON UM.UserID=IM.CreatedBy", "IM." + strFilterCondition });
                        }
                        else
                        {
                            datCombos = MobjclsBLLPOS.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
                        " ON EM.EmployeeID=UM.EmployeeID INNER JOIN InvPOSMaster IM ON UM.UserID=IM.CreatedBy", "IM.CompanyID = " + Convert.ToInt32(cboCompanySearch.SelectedValue) });
                        }
                    //}
                    //else
                    //{
                    //    try
                    //    {
                    //        DataTable datTemp = MobjclsBLLPOS.FillCombos(new string[] { "IsVisible", "STRoleFieldsDetails", "" + 
                    //    "RoleID=" + ClsCommonSettings.RoleID + " AND CompanyID=" + ClsCommonSettings.CompanyID + " " +
                    //    "AND ControlID=" + (int)ControlOperationType.POSEmployee + " AND IsVisible=1" });
                    //        if (datTemp != null)
                    //            if (datTemp.Rows.Count > 0)
                    //            {
                    //                datCombos = MobjclsBLLPOS.GetEmployeeByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.POSCompany);
                    //            }
                    //        if (datCombos == null)
                    //            datCombos = MobjclsBLLPOS.FillCombos(new string[] { "DISTINCT EM.EmployeeID,isnull(EM.[EmployeeFullName],UM.[UserName]) as [EmployeeFullName]","EmployeeMaster EM INNER JOIN UserMaster UM " +
                    //                    " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
                    //        else
                    //        {
                    //            if (datCombos.Rows.Count == 0)
                    //                datCombos = MobjclsBLLPOS.FillCombos(new string[] { "DISTINCT EM.EmployeeID,isnull(EM.[EmployeeFullName],UM.[UserName]) as [EmployeeFullName]","EmployeeMaster EM INNER JOIN UserMaster UM " +
                    //                    " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
                    //        }
                    //    }
                    //    catch { }
                    //}
                    cboCashierSearch.ValueMember = "EmployeeID";
                    cboCashierSearch.DisplayMember = "EmployeeFullName";
                    cboCashierSearch.DataSource = datCombos;
                }
                if (intFillType == 0)
                {
                    datCombos = MobjclsBLLPOS.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "StatusID in (" + (int)OperationStatusType.SPOSDeliverd + "," + (int)OperationStatusType.SPOSCancelled + ")" });
                    cboStatusSearch.ValueMember = "StatusID";
                    cboStatusSearch.DisplayMember = "Status";
                    cboStatusSearch.DataSource = datCombos;
                }
                if (intFillType == 0)
                {

                    datCombos = MobjclsBLLPOS.FillCombos(new string[] { "SalesCounterID,SalesCounterName", "InvSalesCounterReference", "  SalesCounterID in (Select SalesCounterID from InvPOSMaster) " });
                    cboCounterSearch.ValueMember = "SalesCounterID";
                    cboCounterSearch.DisplayMember = "SalesCounterName";
                    cboCounterSearch.DataSource = datCombos;
                }

                if (intFillType == 0 || intFillType == 6)
                {
                    //datCombos = null;
                    //datCombos = MobjclsBLLPOS.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", " (CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + " Or CompanyID = (Select ParentID From CompanyMaster Where CompanyID= " + Convert.ToInt32(cboCompany.SelectedValue) + ") Or " +
                    //" CompanyID in(Select CM2.CompanyID From CompanyMaster CM1 Inner Join CompanyMaster CM2 ON CM2.ParentID = CM1.ParentID Where CM1.CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + " AND CM1.ParentID!=0) Or " +
                    //" CompanyID in(Select CompanyID From CompanyMaster Where ParentID = " + Convert.ToInt32(cboCompany.SelectedValue) + ")) And IsActive=1" });
                    //cboWarehouse.ValueMember = "WarehouseID";
                    //cboWarehouse.DisplayMember = "WarehouseName";
                    //cboWarehouse.DataSource = datCombos;
                    datCombos = null;
                    datCombos = MobjclsBLLPOS.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", " CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + " And IsActive=1" });
                    cboWarehouse.ValueMember = "WarehouseID";
                    cboWarehouse.DisplayMember = "WarehouseName";
                    cboWarehouse.DataSource = datCombos;

                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadCombos() " + ex.Message);
                MobjLogs.WriteLog("Error in LoadCombos() " + ex.Message, 2);
            }
        }
      

        private void dgvPOS_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch { }
        }


        private void dgvPOS_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvPOS.CurrentCell.ColumnIndex == 0 || dgvPOS.CurrentCell.ColumnIndex == 1)
                {
                    for (int i = 0; i < dgvPOS.Columns.Count; i++)
                        dgvPOS.Rows[dgvPOS.CurrentCell.RowIndex].Cells[i].Value = null;
                }
                dgvPOS.PServerName = ClsCommonSettings.ServerName;
                string[] First = null;
                string[] second = null;
                //if (MintFromForm == 1)
                //{
                    //First = new string[] { "IsGroup", "ItemCode", "ItemName", "ItemID" };//first grid 
                    //second = new string[] { "IsGroup", "ItemCode", "Description", "ItemID" };//inner grid
                //}
                //else
                //{
                First = new string[] { "IsGroup", "ItemCode", "ItemName", "BatchID", "BatchNo", "ItemID", "QtyAvailable", "Uom" ,"IsGroupItem"};//first grid 
                second = new string[] { "IsGroup", "ItemCode", "Description", "BatchID", "BatchNo", "ItemID", "QtyAvailable", "UOM", "IsGroupItem" };//inner grid

                //}
                dgvPOS.aryFirstGridParam = First;
                dgvPOS.arySecondGridParam = second;
                dgvPOS.PiFocusIndex = Quantity.Index;
                dgvPOS.iGridWidth = 500;
                dgvPOS.pnlLeft = expandableSplitterLeftPOS.Location.X + 5;
                dgvPOS.pnlTop = PurchaseOrderBindingNavigator.Height + 10;
                dgvPOS.bBothScrollBar = true;
                //dgvPOS.ColumnsToHide = new string[] { "ItemID", "IsGroup", "BatchID" };
                dgvPOS.ColumnsToHide = new string[] { "ItemID", "IsGroup", "BatchID" , "BatchNo"};

              DataTable dtDatasource = MobjclsBLLPOS.GetDataForItemSelection(Convert.ToInt32(cboCompany.SelectedValue),MobjclsBLLPOS.GetCurrencyID(cboCompany.SelectedValue.ToInt32()),cboWarehouse.SelectedValue.ToInt32());

                if (dgvPOS.CurrentCell.ColumnIndex == 0)
                {
                    dgvPOS.CurrentCell.Value = dgvPOS.TextBoxText;
                    dgvPOS.field = "ItemCode";
                }
                else if (dgvPOS.CurrentCell.ColumnIndex == 1)
                {
                    dgvPOS.CurrentCell.Value = dgvPOS.TextBoxText;
                    dgvPOS.field = "Description";
                }
                string strFilterString = dgvPOS.field + " Like '%" + dgvPOS.TextBoxText + "%'";
                dtDatasource.DefaultView.RowFilter = strFilterString;
                DataTable datTemp = dtDatasource.DefaultView.ToTable();
                datTemp.DefaultView.Sort = dgvPOS.field;
                dgvPOS.dtDataSource = datTemp.DefaultView.ToTable();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvPOS_Textbox_TextChanged() " + ex.Message);
                MobjLogs.WriteLog("Error in dgvPOS_Textbox_TextChanged() " + ex.Message, 2);
            }

        }

        private void btnVendor_Click(object sender, EventArgs e)
        {
            try
            {
                int intVendor = Convert.ToInt32(cboCustomer.SelectedValue);
                using (FrmVendor objVendor = new FrmVendor(3))
                {
                    objVendor.PintVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                    objVendor.ShowDialog();
                    LoadCombos(2);
                    if (objVendor.PintVendorID != 0)
                        cboCustomer.SelectedValue = objVendor.PintVendorID;
                    else
                        cboCustomer.SelectedValue = intVendor;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnVendor_Click() " + ex.Message);
                MobjLogs.WriteLog("Error in btnVendor_Click() " + ex.Message, 2);
            }

        }

        private void dgvPOS_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                ChangeStatus();
                if (dgvPOS.RowCount > 0)
                {

                    if (e.ColumnIndex == ItemCode.Index || e.ColumnIndex == ItemName.Index)
                    {
                        if (!ValidateForm(2))
                        {
                            e.Cancel = true;
                            return;
                        }
                        if (e.RowIndex > 1)
                        {
                            int intICounter = e.RowIndex - 1;
                            for (int intJCounter = 0; intJCounter < intICounter - 1; intJCounter++)
                            {
                                if (dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToDecimal() == dgvPOS.Rows[intJCounter].Cells["ItemID"].Value.ToDecimal() && dgvPOS.Rows[intICounter].Cells["BatchID"].Value.ToDecimal() == dgvPOS.Rows[intJCounter].Cells["BatchID"].Value.ToDecimal())
                                {
                                    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9581, out MmessageIcon);
                                    ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                                    TmrPOS.Enabled = true;
                                    dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["BatchNo"];
                                    //dgvPOS.BeginEdit(true);
                                    e.Cancel = true;
                                    return;
                                }
                            }
                        }
                        if (e.RowIndex > 0)
                        {
                            int intICounter = e.RowIndex - 1;

                            if (dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToDecimal() <= 0)
                            {
                                MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9575, out MmessageIcon);
                                ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                                TmrPOS.Enabled = true;
                                dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["ItemCode"];
                                // dgvPOS.BeginEdit(true);
                                e.Cancel = true;
                                return;
                            }
                            if (dgvPOS.Rows[intICounter].Cells["Quantity"].Value.ToDecimal() <= 0)
                            {
                                MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9592, out MmessageIcon);
                                ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                                TmrPOS.Enabled = true;
                                dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["Quantity"];
                                // dgvPOS.BeginEdit(true);
                                e.Cancel = true;
                                return;
                            }
                            if (dgvPOS.Rows[intICounter].Cells["UOM"].Tag.ToInt32() <= 0)
                            {
                                MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9578, out MmessageIcon);
                                ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                                TmrPOS.Enabled = true;
                                dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["UOM"];
                                // dgvPOS.BeginEdit(true);
                                e.Cancel = true;
                                return;
                            }
                            if (dgvPOS.Rows[intICounter].Cells["Rate"].Value.ToDecimal() <= 0)
                            {
                                MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9577, out MmessageIcon);
                                ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                                TmrPOS.Enabled = true;
                                dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["Rate"];
                                // dgvPOS.BeginEdit(true);
                                e.Cancel = true;
                                return;
                            }
                            if (dgvPOS.Rows[intICounter].Cells["DiscountAmount"].Value.ToDecimal() < 0)
                            {
                                MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9579, out MmessageIcon);
                                ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                                TmrPOS.Enabled = true;
                                dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["Discount"];
                                // dgvPOS.BeginEdit(true);
                                e.Cancel = true;
                                return;
                            }
                          
                            DataTable datQtyAvailable = MobjclsBLLPOS.GetQuantityAvailable(dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToInt32(), cboWarehouse.SelectedValue.ToInt32(), dgvPOS.Rows[intICounter].Cells["BatchID"].Value.ToInt32(), MobjclsBLLPOS.clsDTOPOS.intPOSID);
                            if (datQtyAvailable.Rows.Count > 0)
                            {
                                decimal decQuantity = 0;
                                decimal decAvailQuantity = 0;
                                decQuantity = dgvPOS.Rows[intICounter].Cells["Quantity"].Value.ToDecimal();
                                decAvailQuantity = datQtyAvailable.Rows[0]["QtyAvailable"].ToString().ToDecimal();
                                DataTable dtGetUomDetails = MobjclsBLLPOS.GetUomConversionValues(dgvPOS["UOM", intICounter].Tag.ToInt32(), dgvPOS["ItemID", intICounter].Value.ToInt32());
                                if (dtGetUomDetails.Rows.Count > 0)
                                {
                                    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                    if (intConversionFactor == 1)
                                    {
                                        decQuantity = decQuantity / decConversionValue;
                                        decAvailQuantity = decAvailQuantity * decConversionValue;
                                    }
                                    else if (intConversionFactor == 2)
                                    {
                                        decQuantity = decQuantity * decConversionValue;
                                        decAvailQuantity = decAvailQuantity / decConversionValue;
                                    }
                                }
                                if (datQtyAvailable.Rows[0]["QtyAvailable"].ToString().ToDecimal() < decQuantity)
                                {
                                    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9576, out MmessageIcon);
                                    ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                                    TmrPOS.Enabled = true;
                                    dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["Quantity"];
                                    dgvPOS.Rows[intICounter].Cells["Quantity"].Value = Math.Round(decAvailQuantity, 2);
                                    //dgvPOS.BeginEdit(true);
                                    e.Cancel = true;
                                    return;
                                }
                            }

                        }




                    }
                }
                if (e.ColumnIndex != ItemCode.Index && e.ColumnIndex != ItemName.Index)
                {
                    if (Convert.ToString(dgvPOS.CurrentRow.Cells["ItemCode"].Value).Trim() == "")
                    {

                        e.Cancel = true;
                        return;
                    }
                }

                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    if (e.ColumnIndex == Uom.Index)
                    {
                        DataTable datSalesRate = new DataTable();

                        Int32 intItemID = Convert.ToInt32(dgvPOS.Rows[e.RowIndex].Cells["ItemID"].Value);
                        int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                        int intUOMID = Convert.ToInt32(dgvPOS.Rows[e.RowIndex].Cells["Uom"].Tag);

                        if (intItemID != 0)
                        {
                            datSalesRate = FillDataGridCombo(intItemID, intVendorID, Convert.ToBoolean(dgvPOS.Rows[e.RowIndex].Cells["IsGroup"].Value));
                        }
                    }
                    else if (e.ColumnIndex == Discount.Index)
                    {
                        Int32 intItemID = Convert.ToInt32(dgvPOS.Rows[e.RowIndex].Cells["ItemID"].Value);
                        if (intItemID != 0)
                        {
                            FillDiscountColumn(e.RowIndex, false);
                        }
                    }
                }
                //if (e.ColumnIndex == Discount.Index)
                //{
                //    if (Convert.ToBoolean(dgvPOS.Rows[e.RowIndex].Cells[IsGroup.Index].Value) == false)
                //    {
                //        Int32 intItemID = Convert.ToInt32(dgvPOS.Rows[e.RowIndex].Cells["ItemID"].Value);
                //        if (intItemID != 0)
                //        {
                //            FillDiscountColumn(e.RowIndex, false);
                //        }
                //    }
                //    else
                //        e.Cancel = true;
                //}
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvPOS_CellBeginEdit() " + ex.Message);
                MobjLogs.WriteLog("Error in dgvPOS_CellBeginEdit() " + ex.Message, 2);
            }
        }
        private DataTable FillDataGridCombo(int intItemID, int intVendorID, bool blnIsGroup)
        {
            try
            {
                DataTable datCombos = new DataTable();
                if (!blnIsGroup)
                {
                    datCombos = MobjclsBLLPOS.GetItemWiseUOMs(intItemID);
                    Uom.ValueMember = "UOMID";
                    Uom.DisplayMember = "UOM";
                    Uom.DataSource = datCombos;
                }
                else
                {
                    datCombos = MobjclsBLLPOS.FillCombos(new string[] { "UOMID,Code as UOM", "InvUOMReference", "UOMID = " + (int)PredefinedUOMs.Numbers });
                    Uom.ValueMember = "UOMID";
                    Uom.DisplayMember = "UOM";
                    Uom.DataSource = datCombos;
                }

                return datCombos;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FillDataGridCombo() " + ex.Message);
                MobjLogs.WriteLog("Error in FillDataGridCombo() " + ex.Message, 2);
            }
            return null;
        }

        private void dgvPOS_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    if (e.ColumnIndex == Uom.Index)
                    {
                        dgvPOS.CurrentRow.Cells["Uom"].Tag = dgvPOS.CurrentRow.Cells["Uom"].Value;
                        dgvPOS.CurrentRow.Cells["Uom"].Value = dgvPOS.CurrentRow.Cells["Uom"].FormattedValue;

                        int intUomScale = 0;
                        if (dgvPOS[Uom.Index, dgvPOS.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                            intUomScale = MobjclsBLLPOS.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPOS[Uom.Index, dgvPOS.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                        dgvPOS.CurrentRow.Cells["Quantity"].Value = dgvPOS.CurrentRow.Cells["Quantity"].Value.ToDecimal().ToString("F" + intUomScale);

                        CalculateTotalAmt();
                    }
                    else if (e.ColumnIndex == Discount.Index)
                    {
                        if (dgvPOS.CurrentRow.Cells["Discount"].Value.ToInt32() == (int)PredefinedDiscounts.DefaultSalesDiscount)
                        {
                            dgvPOS.CurrentRow.Cells["DiscountAmount"].ReadOnly = false;
                        }
                        else
                        { 
                            dgvPOS.CurrentRow.Cells["DiscountAmount"].ReadOnly = true;
                        }
                        dgvPOS.CurrentRow.Cells["Discount"].Tag = dgvPOS.CurrentRow.Cells["Discount"].Value;
                        dgvPOS.CurrentRow.Cells["Discount"].Value = dgvPOS.CurrentRow.Cells["Discount"].FormattedValue;
                        dgvPOS.CurrentRow.Cells["DiscountAmount"].Value = "0.00";

                        CalculateTotalAmt();
                    }
                    else if (e.ColumnIndex == Quantity.Index || e.ColumnIndex == DiscountAmount.Index)
                    {
                        CalculateTotalAmt();
                    }
                    else if (e.ColumnIndex == Rate.Index)
                    {
                        CalculateTotalAmt();
                        if (MobjclsBLLCommonUtility.GetPurchaseRate(dgvPOS.Rows[e.RowIndex].Cells[ItemID.Index].Value.ToInt32(), dgvPOS.Rows[e.RowIndex].Cells[Uom.Index].Tag.ToInt32(), dgvPOS.Rows[e.RowIndex].Cells[BatchID.Index].Value.ToInt64(), cboCompany.SelectedValue.ToInt32()) > dgvPOS.Rows[e.RowIndex].Cells[Rate.Index].Value.ToDecimal())
                        {
                            dgvPOS.Rows[e.RowIndex].Cells[Rate.Index].Style.ForeColor = Color.Red;
                            MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9595);
                            lblPOSstatus.Text = MstrCommonMessage;
                        }
                        else
                        {
                            dgvPOS.Rows[e.RowIndex].Cells[Rate.Index].Style.ForeColor = Color.Black;
                            foreach (DataGridViewRow dr in dgvPOS.Rows)
                            {
                                lblPOSstatus.Text = "";
                                if (dr.Index != e.RowIndex)
                                {
                                    if (dr.Cells[Rate.Index].Style.ForeColor == Color.Red)
                                    {
                                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9595);
                                        lblPOSstatus.Text = MstrCommonMessage;
                                        return;
                                    }
                                }
                            }
                            //lblSalesOrderstatus.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvPOS_CellEndEdit() " + ex.Message);
                MobjLogs.WriteLog("Error in dgvPOS_CellEndEdit() " + ex.Message, 2);
            }
        }

        private void dgvPOS_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    if (e.ColumnIndex >= 0 && e.RowIndex >= 0 && (e.ColumnIndex == Quantity.Index || e.ColumnIndex == Rate.Index))
                    {
                        //if (e.ColumnIndex == Quantity.Index)
                        //{
                        //    int intDecimalPart = 0;
                        //    if (dgvPOS.CurrentCell.Value != null && !string.IsNullOrEmpty(dgvPOS.CurrentCell.Value.ToString()) && dgvPOS.CurrentCell.Value.ToString().Contains("."))
                        //        intDecimalPart = dgvPOS.CurrentCell.Value.ToString().Split('.')[1].Length;
                        //    int intUomScale = 0;
                        //    if (dgvPOS[Uom.Index, dgvPOS.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                        //        intUomScale = MobjclsBLLSTSales.FillCombos(new string[] { "Scale", "STUOMReference", "UOMID = " + dgvPOS[Uom.Index, dgvPOS.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                        //    if (intDecimalPart > intUomScale)
                        //    {
                        //        dgvPOS.CurrentCell.Value = Math.Round(dgvPOS.CurrentCell.Value.ToDecimal(), intUomScale).ToString();
                        //        dgvPOS.EditingControl.Text = dgvPOS.CurrentCell.Value.ToString();
                        //    }
                        //}

                        //if (dgvPOS.Columns[e.ColumnIndex].Name == "Quantity")
                        //   // MblnQtyChanged = true;
                        decimal decValue = 0;
                        try
                        {
                            decValue = Convert.ToDecimal(dgvPOS[e.ColumnIndex, e.RowIndex].Value);
                        }
                        catch
                        {
                            dgvPOS[e.ColumnIndex, e.RowIndex].Value = 0;
                        }
                        FillDiscountColumn(e.RowIndex, true);
                        //CalculateTotalAmt();
                    }



                    DataTable datSalesRate = new DataTable();
                    if (e.ColumnIndex == ItemID.Index)
                    {


                        int intUOMID = 0;
                        Int32 intItemID = Convert.ToInt32(dgvPOS.Rows[e.RowIndex].Cells["ItemID"].Value);
                        Int32 intBatchID = Convert.ToInt32(dgvPOS.Rows[e.RowIndex].Cells["BatchID"].Value.ToInt32());
                        int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                        int intCompanyID = cboCompany.SelectedValue.ToInt32();

                        if (intItemID != 0)
                        {
                            datSalesRate = FillDataGridCombo(intItemID, intVendorID, Convert.ToBoolean(dgvPOS.Rows[e.RowIndex].Cells["IsGroup"].Value));

                            if (Convert.ToBoolean(dgvPOS.Rows[e.RowIndex].Cells["IsGroup"].Value) == false)
                            {
                                DataRow[] dtrSalesRate = datSalesRate.Select("ItemID=" + intItemID);
                                FillDataGridCombo(intItemID, intVendorID, false);
                                DataTable datDefaultUom = MobjclsBLLPOS.FillCombos(new string[] { "DefaultSalesUomID as UomID", "InvItemMaster", "ItemID = " + intItemID + "" });
                                intUOMID = Convert.ToInt32(datDefaultUom.Rows[0]["UomID"]);
                                // dgvPOS[Rate.Index, e.RowIndex].Value = dtrSalesRate[0]["SaleRate"].ToDecimal();
                                dgvPOS[Rate.Index, e.RowIndex].Value = MobjclsBLLPOS.GetRate(intUOMID, intItemID, intCompanyID, intBatchID).ToDecimal().ToString("F" + MintExchangeCurrencyScale); ;
                                // double abc = MobjclsBLLPOS.GetRate(intUOMID, intItemID, intCompanyID,intBatchID);
                            }
                            else
                            {
                                intUOMID = (int)PredefinedUOMs.Numbers;
                                DataTable datProductGrpDetails = MobjclsBLLPOS.FillCombos(new string[] { "SaleAmount", "InvItemGroupMaster", "ItemGroupID = " + intItemID });
                                dgvPOS[Rate.Index, e.RowIndex].Value = Convert.ToDecimal(datProductGrpDetails.Rows[0]["SaleAmount"]);
                            }

                            dgvPOS.Rows[e.RowIndex].Cells["Uom"].Tag = intUOMID;
                            dgvPOS.Rows[e.RowIndex].Cells["Uom"].Value = intUOMID;
                            dgvPOS.Rows[e.RowIndex].Cells["Uom"].Value = dgvPOS.Rows[e.RowIndex].Cells["Uom"].FormattedValue;

                            FillDiscountColumn(e.RowIndex, false);
                        }
                        int intUomScale = 0;
                        if (dgvPOS[Uom.Index, e.RowIndex].Tag.ToInt32() != 0)
                            intUomScale = MobjclsBLLPOS.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPOS[Uom.Index, e.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                        dgvPOS[Quantity.Index, e.RowIndex].Value = Convert.ToDecimal(1).ToString("F" + intUomScale);
                        //CalculateTotalAmt();
                    }
                    if (e.ColumnIndex == Uom.Index)
                    {
                        int intUOMID = 0;
                        Int32 intItemID = Convert.ToInt32(dgvPOS.Rows[e.RowIndex].Cells["ItemID"].Value);
                        Int32 intBatchID = Convert.ToInt32(dgvPOS.Rows[e.RowIndex].Cells["BatchID"].Value.ToInt32());
                        int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                        int intCompanyID = cboCompany.SelectedValue.ToInt32();

                        if (Convert.ToBoolean(dgvPOS.Rows[e.RowIndex].Cells["IsGroup"].Value) == false)
                        {
                            datSalesRate = FillDataGridCombo(intItemID, intVendorID, Convert.ToBoolean(dgvPOS.Rows[e.RowIndex].Cells["IsGroup"].Value));
                            DataRow[] dtrSalesRate = datSalesRate.Select("ItemID=" + intItemID);
                            FillDataGridCombo(intItemID, intVendorID, false);
                            DataTable datDefaultUom = MobjclsBLLPOS.FillCombos(new string[] { "DefaultSalesUomID as UomID", "InvItemMaster", "ItemID = " + intItemID + "" });
                            //intUOMID = Convert.ToInt32(datDefaultUom.Rows[0]["UomID"]);
                            intUOMID = dgvPOS.Rows[e.RowIndex].Cells["UOM"].Tag.ToInt32();
                            // dgvPOS[Rate.Index, e.RowIndex].Value = dtrSalesRate[0]["SaleRate"].ToDecimal();
                            dgvPOS[Rate.Index, e.RowIndex].Value = MobjclsBLLPOS.GetRate(intUOMID, intItemID, intCompanyID, intBatchID).ToDecimal().ToString("F" + MintExchangeCurrencyScale); ;
                            // double abc = MobjclsBLLPOS.GetRate(intUOMID, intItemID, intCompanyID, intBatchID);
                        }
                        else
                        {
                            intUOMID = (int)PredefinedUOMs.Numbers;
                            DataTable datProductGrpDetails = MobjclsBLLPOS.FillCombos(new string[] { "SaleAmount", "InvItemGroupMaster", "ItemGroupID = " + intItemID });
                            dgvPOS[Rate.Index, e.RowIndex].Value = Convert.ToDecimal(datProductGrpDetails.Rows[0]["SaleAmount"]);
                        }
                        //dgvPOS.Rows[e.RowIndex].Cells["Uom"].Tag = intUOMID;
                        //dgvPOS.Rows[e.RowIndex].Cells["Uom"].Value = intUOMID;
                       // dgvPOS.Rows[e.RowIndex].Cells["Uom"].Value = dgvPOS.Rows[e.RowIndex].Cells["Uom"].FormattedValue;
                    }
                    CalculateTotalAmt();
                    //if (dgvPOS.Rows[dgvPOS.Rows.Count - 1].Cells["ItemID"].Value.ToInt32() <= 0 && e.RowIndex==dgvPOS.Rows.Count - 1)
                    //{
                    //  //  dgvPOS.Rows[dgvPOS.Rows.Count - 1].Cells["Quantity"].Value = "";
                    //    decimal jj;
                    //}
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvPOS_CellValueChanged() " + ex.Message);
                MobjLogs.WriteLog("Error in dgvPOS_CellValueChanged() " + ex.Message, 2);
            }
        }

        private void dgvPOS_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if ((e.ColumnIndex == ItemCode.Index) || (e.ColumnIndex == ItemName.Index))
                {
                    dgvPOS.PiColumnIndex = e.ColumnIndex;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvPOS_CellEnter() " + ex.Message);
                MobjLogs.WriteLog("Error in dgvPOS_CellEnter() " + ex.Message, 2);
            }
        }

        private void btnNewItem_Click(object sender, EventArgs e)
        {

            try
            {
                BeginNextItem();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnNewItem_Click() " + ex.Message);
                MobjLogs.WriteLog("Error in btnNewItem_Click() " + ex.Message, 2);
            }

        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            try
            {
                if (keyData == Keys.F1)
                {
                    Help();
                    return true;
                }
                if (keyData == Keys.F2)
                {

                    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9583, out MmessageIcon);
                    lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                    TmrPOS.Enabled = true;

                    btnNewItem.Focus();
                    BeginNextItem();
                    return true;
                }
                if (keyData == Keys.F8)
                {
                    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9585, out MmessageIcon);
                    lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                    TmrPOS.Enabled = true;
                    btnRemove.Focus();
                    DeleteLastItem();
                    return true;
                }
                if (keyData == Keys.F9)
                {
                    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9586, out MmessageIcon);
                    lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                    TmrPOS.Enabled = true;
                    btnRemove.Focus();
                    DeleteFirstItem();
                    return true;
                }
                if (keyData == Keys.F3)
                {
                    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9584, out MmessageIcon);
                    lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                    TmrPOS.Enabled = true;
                    //btnRemove.Focus();
                    DeleteCurrentItem();
                    return true;
                }
                if (keyData == Keys.F5)
                {
                    if (btnNew.Enabled == true)
                    {
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9588, out MmessageIcon);
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                        btnNew.Focus();
                        AddMode();
                    }
                    return true;
                }
                if (keyData == Keys.F6)
                {
                    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9590, out MmessageIcon);
                    lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                    TmrPOS.Enabled = true;
                    txtNetAmount.Focus();
                    txtNetAmount.SelectAll();
                    return true;
                }
                if (keyData == Keys.F7)
                {
                    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9591, out MmessageIcon);
                    lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                    TmrPOS.Enabled = true;
                    txtTendered.Focus();
                    txtTendered.SelectAll();
                    return true;
                }
                if (keyData == Keys.Escape)
                {
                    //MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9587, out MmessageIcon);
                    //lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                    //TmrPOS.Enabled = true;
                    //clearMasterControls();
                    //cboCompany.Focus();
                    return true;
                }
                if (keyData == Keys.F11)
                {
                    if (expandableSplitterLeftPOS.Expanded)
                    {
                        expandableSplitterLeftPOS.Expanded = false;
                    }
                    else
                    {
                        expandableSplitterLeftPOS.Expanded = true;
                    }
                    return true;
                }
                if (keyData == Keys.F12)
                {
                    if (BindingNavigatorSaveItem.Enabled == true)
                    {
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9589, out MmessageIcon);
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                        SavePOS();
                    }
                    return true;
                }
                return base.ProcessCmdKey(ref msg, keyData);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in ProcessCmdKey() " + ex.Message);
                MobjLogs.WriteLog("Error in ProcessCmdKey() " + ex.Message, 2);
                return base.ProcessCmdKey(ref msg, keyData);
            }
        }

        private void BeginNextItem()
        {
            if (!ValidateForm(0))
            {
                return;
            }
            dgvPOS.Focus();
            dgvPOS.CurrentCell = dgvPOS.Rows[(dgvPOS.Rows.Count - 1)].Cells[0];
            dgvPOS.BeginEdit(true);
            dgvPOS.Rows[(dgvPOS.Rows.Count - 1)].Cells[0].Value = " ";
            dgvPOS.EndEdit();
            dgvPOS.BeginEdit(true);
            SendKeys.Send("{BS}");
           Application.DoEvents();
            //dgvPOS.EndEdit();
            dgvPOS.BeginEdit(true);
            //SendKeys.Send("{DOWN}");
        }
        private void DeleteLastItem()
        {                
            if (dgvPOS.Rows.Count > 1)
            dgvPOS.Rows.RemoveAt(dgvPOS.Rows.Count - 2);
        }
        private void DeleteCurrentItem()
        {

            if (dgvPOS.Rows.Count > 1 && dgvPOS.CurrentRow.Index != dgvPOS.Rows.Count - 1)
            {
                dgvPOS.CurrentCell = dgvPOS.Rows[dgvPOS.CurrentRow.Index].Cells["Quantity"];
                dgvPOS.Rows.Remove(dgvPOS.CurrentRow);
            }
        }
        private void DeleteFirstItem()
        {
            if (dgvPOS.Rows.Count > 1)
                dgvPOS.Rows.RemoveAt(0);
        }
        private void ClearControls()
        {
            dgvPOS.Rows.Clear();
            txtNetAmount.Text = "0.00";
            txtTendered.Text = "0.00";
            txtChange.Text = "0.00";
            txtRemarks.Text = null;
            txtTotal.Text = "0.00";
            txtSubTotal.Text = "0.00";
            txtExpense.Text = "0.00";
            txtDiscountAmount.Text = "0.00";
            if(cboDiscountScheme.DataSource!=null)
            cboDiscountScheme.SelectedIndex = 0;
            txtBillNumber.Text = getBillNo();
            dtpDueDate.MaxDate = ClsCommonSettings.GetServerDate().AddMinutes(1439);
            dtpPOSDate.MaxDate = ClsCommonSettings.GetServerDate().AddMinutes(1439);
            dtpSToSearch.MaxDate = ClsCommonSettings.GetServerDate().AddMinutes(1439);
            dtpSFromSearch.MaxDate = ClsCommonSettings.GetServerDate().AddMinutes(1439);
            dtpPOSDate.Value = ClsCommonSettings.GetServerDateTime();
            MobjclsBLLPOS.clsDTOPOS.lstclsDTOPOSItemGroupDetails = null;
        }
        private void ClearSearchBoxControls()
        {
            cboCompanySearch.SelectedIndex = -1; 
            cboCustomerSearch.SelectedIndex = -1;
            cboCounterSearch.SelectedIndex = -1;
            cboCashierSearch.SelectedIndex = -1;
            cboStatusSearch.SelectedIndex = -1;
            txtBillNoSearch.Text = "";
            dtpSFromSearch.Value = ClsCommonSettings.GetServerDate();
            dtpSToSearch.Value = ClsCommonSettings.GetServerDate();
        }
        private void Help()
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Pointofsale";
            objHelp.ShowDialog();
            objHelp = null;
        }
        private Boolean ValidateForm(int intMode)
        {
            try
            {
                if (intMode == 0 || intMode == 1 || intMode == 2)
                {
                    if (cboCompany.SelectedIndex == -1)
                    {
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 14, out MmessageIcon);
                        ErrSales.SetError(cboCompany, MstrCommonMessage.Replace("#", "").Trim());
                        //tmrMessage.Enabled = true;
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        //tmrMessage.Enabled = false;
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                        cboCompany.Focus();
                        return false;
                    }

                    //if (cboCustomer.SelectedIndex == -1)
                    //{
                    //    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9570, out MmessageIcon);
                    //    ErrSales.SetError(cboCustomer, MstrCommonMessage.Replace("#", "").Trim());
                    //    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    //    lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                    //    TmrPOS.Enabled = true;
                    //    cboCustomer.Focus();
                    //    return false;
                    //}

                    if (cboCounter.SelectedIndex == -1)
                    {
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9571, out MmessageIcon);
                        ErrSales.SetError(cboCounter, MstrCommonMessage.Replace("#", "").Trim());
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                        cboCounter.Focus();
                        return false;
                    }

                    if (cboWarehouse.SelectedIndex == -1)
                    {
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9573, out MmessageIcon);
                        ErrSales.SetError(cboWarehouse, MstrCommonMessage.Replace("#", "").Trim());
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                        cboWarehouse.Focus();
                        return false;
                    }
                }
                if (intMode == 0 || intMode == 1)
                {
                    DataTable dtItemDetails = new DataTable();
                    dtItemDetails.Columns.Add("ItemID");
                    dtItemDetails.Columns.Add("ItemQty");

                    DataTable dtItemGroupReferenceDetails = new DataTable();
                    dtItemGroupReferenceDetails.Columns.Add("ItemID");
                    dtItemGroupReferenceDetails.Columns.Add("ItemQty");;

                    for (int intICounter = 0; intICounter < dgvPOS.RowCount - 1; intICounter++)
                    
                    {
                        if (dgvPOS.Rows[intICounter].Cells["IsGroup"].Value.ToBoolean() == false)
                        {
                            DataRow drItem = dtItemDetails.NewRow();
                            drItem["ItemID"] = dgvPOS.Rows[intICounter].Cells["ItemID"].Value;
                            drItem["ItemQty"] = dgvPOS.Rows[intICounter].Cells["Quantity"].Value;
                            dtItemDetails.Rows.Add(drItem);
                        }
                        if (dgvPOS.Rows[intICounter].Cells["IsGroup"].Value.ToBoolean() == true)
                        {
                            DataRow drItemGroup = dtItemGroupReferenceDetails.NewRow();
                            drItemGroup["ItemID"] = dgvPOS.Rows[intICounter].Cells["ItemID"].Value;
                            drItemGroup["ItemQty"] = dgvPOS.Rows[intICounter].Cells["Quantity"].Value;
                            dtItemGroupReferenceDetails.Rows.Add(drItemGroup);
                        }

                        for (int intJCounter = intICounter + 1; intJCounter < dgvPOS.RowCount - 1; intJCounter++)
                        {
                            DataTable datCostingMethod = MobjclsBLLPOS.FillCombos(new string[] { "CostingMethodID", "InvCostingAndPricingDetails", "ItemID = " + dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToDecimal() + "" });
                                if (datCostingMethod.Rows.Count > 0 && datCostingMethod != null)
                                {
                                    int intCostingMethodID = datCostingMethod.Rows[0]["CostingMethodID"].ToInt32();
                                    if (intCostingMethodID == (int)CostingMethodReference.Batch)
                                    {
                                        if (dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToDecimal() == dgvPOS.Rows[intJCounter].Cells["ItemID"].Value.ToDecimal() && dgvPOS.Rows[intICounter].Cells["BatchID"].Value.ToDecimal() == dgvPOS.Rows[intJCounter].Cells["BatchID"].Value.ToDecimal())
                                        {
                                            MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9581, out MmessageIcon);
                                            ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                            lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                                            TmrPOS.Enabled = true;
                                            //dgvPOS.CurrentCell = dgvPOS.Rows[intJCounter].Cells["BatchNo"];
                                            //dgvPOS.BeginEdit(true);
                                            return false;
                                        }
                                    }
                                    else
                                    {
                                        if (dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToDecimal() == dgvPOS.Rows[intJCounter].Cells["ItemID"].Value.ToDecimal() && dgvPOS.Rows[intICounter].Cells["IsGroup"].Value.ToBoolean() == true && dgvPOS.Rows[intJCounter].Cells["IsGroup"].Value.ToBoolean() == true)
                                        {
                                            MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9581, out MmessageIcon);
                                            ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                            lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                                            TmrPOS.Enabled = true;
                                            dgvPOS.CurrentCell = dgvPOS.Rows[intJCounter].Cells["ItemName"];
                                            //dgvPOS.BeginEdit(true);
                                            return false;
                                        }
                                  
                                    }
                                }
                                if (dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToDecimal() == dgvPOS.Rows[intJCounter].Cells["ItemID"].Value.ToDecimal() && dgvPOS.Rows[intICounter].Cells["IsGroup"].Value.ToBoolean() == false && dgvPOS.Rows[intJCounter].Cells["IsGroup"].Value.ToBoolean() == false)
                                {
                                    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9581, out MmessageIcon);
                                    ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                                    TmrPOS.Enabled = true;
                                    dgvPOS.CurrentCell = dgvPOS.Rows[intJCounter].Cells["ItemName"];
                                    //dgvPOS.BeginEdit(true);
                                    return false;
                                }
                          

                        }
                        if (dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToDecimal() <= 0)
                        {
                            MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9575, out MmessageIcon);
                            ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                            TmrPOS.Enabled = true;
                            dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["ItemCode"];
                            dgvPOS.BeginEdit(true);
                            return false;
                        }
                        if (dgvPOS.Rows[intICounter].Cells["Quantity"].Value.ToDecimal() <= 0)
                        {
                            MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9592, out MmessageIcon);
                            ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                            TmrPOS.Enabled = true;
                            dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["Quantity"];
                            dgvPOS.BeginEdit(true);
                            return false;
                        }
                        if (dgvPOS.Rows[intICounter].Cells["UOM"].Tag.ToInt32() <= 0)
                        {
                            MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9578, out MmessageIcon);
                            ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                            TmrPOS.Enabled = true;
                            dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["UOM"];
                            dgvPOS.BeginEdit(true);
                            return false;
                        }
                        if (dgvPOS.Rows[intICounter].Cells["Rate"].Value.ToDecimal() <= 0)
                        {
                            MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9577, out MmessageIcon);
                            ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                            TmrPOS.Enabled = true;
                            dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["Rate"];
                            // dgvPOS.BeginEdit(true);
                            return false;
                        }
                        if (dgvPOS.Rows[intICounter].Cells["DiscountAmount"].Value.ToDecimal() < 0)
                        {
                            MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9579, out MmessageIcon);
                            ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                            TmrPOS.Enabled = true;
                            dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["Discount"];
                            // dgvPOS.BeginEdit(true);
                            return false;
                        }

                        if (dgvPOS.Rows[intICounter].Cells["Total"].Value.ToDecimal() <= 0)
                        {
                            MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9577, out MmessageIcon);
                            ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                            TmrPOS.Enabled = true;
                            dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["Total"];
                            // dgvPOS.BeginEdit(true);
                            return false;
                        }
                        //if (MobjclsBLLPOS.GetClosingStockValidation(cboCompany.SelectedValue.ToInt32(), MobjclsBLLPOS.GetCurrencyID(cboCompany.SelectedValue.ToInt32()), dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToInt32(), dgvPOS.Rows[intICounter].Cells["BatchID"].Value.ToInt32(), dgvPOS.Rows[intICounter].Cells["Quantity"].Value.ToDecimal(),dgvPOS.Rows[intICounter].Cells["IsGroup"].Value.ToBoolean()))
                        //{
                        //    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 2301, out MmessageIcon);
                        //    ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                        //    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        //    lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        //    TmrPOS.Enabled = true;
                        //    return false;
                        //}
   
                        DataTable datQtyAvailable = MobjclsBLLPOS.GetQuantityAvailable(dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToInt32(), cboWarehouse.SelectedValue.ToInt32(), dgvPOS.Rows[intICounter].Cells["BatchID"].Value.ToInt32(), MobjclsBLLPOS.clsDTOPOS.intPOSID);
                        if (datQtyAvailable.Rows.Count > 0)
                        {
                            decimal decQuantity = 0;
                            decimal decAvailQuantity = 0;
                            decQuantity = dgvPOS.Rows[intICounter].Cells["Quantity"].Value.ToDecimal();
                            foreach (DataRow dr in datQtyAvailable.Rows)
                            {
                                decAvailQuantity = decAvailQuantity + Convert.ToInt32(dr["QtyAvailable"]).ToDecimal(); 
                            }
                            DataTable dtGetUomDetails = MobjclsBLLPOS.GetUomConversionValues(dgvPOS["UOM", intICounter].Tag.ToInt32(), dgvPOS["ItemID", intICounter].Value.ToInt32());
                            if (dtGetUomDetails.Rows.Count > 0)
                            {
                                int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                if (intConversionFactor == 1)
                                {
                                    decQuantity = decQuantity / decConversionValue;
                                    decAvailQuantity = decAvailQuantity * decConversionValue;
                                }
                                else if (intConversionFactor == 2)
                                {
                                    decQuantity = decQuantity * decConversionValue;
                                    decAvailQuantity = decAvailQuantity / decConversionValue;
                                }
                            }
                            if (decAvailQuantity < decQuantity)
                            {
                                MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9576, out MmessageIcon);
                                ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                                TmrPOS.Enabled = true;
                                dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["Quantity"];
                                dgvPOS.Rows[intICounter].Cells["Quantity"].Value = Math.Round(decAvailQuantity, 2);
                                //dgvPOS.BeginEdit(true);
                                return false;
                            }
                            //if (decAvailQuantity < decQuantity)
                            //{
                            //    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9576, out MmessageIcon);
                            //    ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                            //    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            //    lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                            //    TmrPOS.Enabled = true;
                            //    dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["Quantity"];
                            //    dgvPOS.Rows[intICounter].Cells["Quantity"].Value = Math.Round(decAvailQuantity, 2);
                            //    //dgvPOS.BeginEdit(true);
                            //    return false;
                            //}
                    }
                        //DataSet dsQtyAvailable = MobjclsBLLPOS.GetAllQuantityAvailable(dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToInt32(), cboWarehouse.SelectedValue.ToInt32(), dgvPOS.Rows[intICounter].Cells["BatchID"].Value.ToInt32(), MobjclsBLLPOS.clsDTOPOS.intPOSID);
                      
                    }
                 
                    if (dtItemGroupReferenceDetails.Rows.Count > 0 && dtItemDetails.Rows.Count>0)
                    {
                        foreach (DataRow drItemGroup in dtItemGroupReferenceDetails.Rows)
                        {
                            DataTable dtGroupItemDetails = MobjclsBLLPOS.GetGroupItemDetails(drItemGroup["ItemID"].ToInt32());

                            //decimal value = new clsBLLSTSalesInvoice().GetStockQuantity(dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToInt32(), 0, cboCompany.SelectedValue.ToInt32(), ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy"));
                            foreach (DataRow dr in dtGroupItemDetails.Rows)
                            {
                                decimal ItemQty = 0;
                                decimal UsedQuantity = 0;
                                foreach (DataRow drItem in dtItemDetails.Rows)
                                {
                                    if (dr["ItemID"].ToInt32() == drItem["ItemID"].ToInt32())
                                    {
                                        ItemQty = Convert.ToDecimal(drItem["ItemQty"]);
                                        if (!MobjclsBLLPOS.clsDTOPOS.blnAddMode)
                                            UsedQuantity = MobjclsBLLPOS.GetUsedQuantity(dr["ItemID"].ToInt32(), cboWarehouse.SelectedValue.ToInt32());

                                    }
                                }
                                decimal value = new clsBLLDirectDelivery().GetStockQuantity(Convert.ToInt32(dr["ItemID"]), 0, cboCompany.SelectedValue.ToInt32(), cboWarehouse.SelectedValue.ToInt32());
                                if (!MobjclsBLLPOS.clsDTOPOS.blnAddMode)
                                    value = value + UsedQuantity;
                                decimal TotalQty =  Convert.ToDecimal(drItemGroup["ItemQty"]) * Convert.ToDecimal(dr["GroupItemQuantity"]);
                                if (TotalQty + ItemQty > value)
                                {
                                    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9576, out MmessageIcon);
                                    ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                                    TmrPOS.Enabled = true;
                                   // dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["Quantity"];
                                    // dgvPOS.Rows[intICounter].Cells["Quantity"].Value = Math.Round(decAvailQuantity, 2);
                                    //dgvPOS.BeginEdit(true);
                                    return false;
                                }

                            }
                        }
                    }
                }
                if (intMode == 1)
                {
                    if (txtBillNumber.Text.Trim() == "")
                    {
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9572, out MmessageIcon);
                        ErrSales.SetError(txtBillNumber, MstrCommonMessage.Replace("#", "").Trim());
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                        txtBillNumber.Focus();
                        return false;
                    }
                    if (dgvPOS.RowCount <= 1)
                    {
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9575, out MmessageIcon);
                        ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                        BeginNextItem();
                        return false;
                    }
                    if (txtNetAmount.Text.ToDecimal() <= 0)
                    {
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9574, out MmessageIcon);
                        ErrSales.SetError(txtNetAmount, MstrCommonMessage.Replace("#", "").Trim());
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                        txtNetAmount.Focus();
                        return false;
                    }
                    if (cboPaymentMode.SelectedIndex == -1)
                    {
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 22, out MmessageIcon);
                        ErrSales.SetError(cboPaymentMode, MstrCommonMessage.Replace("#", "").Trim());
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                        cboPaymentMode.Focus();
                        return false;
                    }
                    if (txtChange.Text.ToDecimal() < 0)
                    {
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9593, out MmessageIcon);
                        ErrSales.SetError(txtChange, MstrCommonMessage.Replace("#", "").Trim());
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                        txtChange.Focus();
                        txtChange.SelectAll();
                        return false;
                    }
                   
                    if (!MobjclsBLLPOS.GetCompanyAccount(Convert.ToInt32(TransactionTypes.POS),cboCompany.SelectedValue.ToInt32()))
                    {
                        //MsMessageCommon = "Please create a Cash Sale Account for the selected company.";
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9594, out MmessageIcon);
                        ErrSales.SetError(cboCompany, MstrCommonMessage.Replace("#", "").Trim());
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                        cboCompany.Focus();
                        return false;
                    }
                    if (dtpPOSDate.Value >ClsCommonSettings.GetServerDateTime())
                    {
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 2293, out MmessageIcon);
                        ErrSales.SetError(dtpPOSDate, MstrCommonMessage.Replace("#", "").Trim());
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                        dtpPOSDate.Focus();
                        return false;
                    }

                }
                /* Validation for Sale Rate > CutOff Price */
                if (intMode == 1 || intMode == 2)
                {
                    for (int intICounter = 0; intICounter < dgvPOS.RowCount - 1; intICounter++)
                    {
                        if (dgvPOS.Rows[intICounter].Cells[IsGroup.Index].Value.ToBoolean()==false)
                        {
                        decimal decCutoffRate = MobjclsBLLCommonUtility.GetCutOffPrice(dgvPOS.Rows[intICounter].Cells[ItemID.Index].Value.ToInt32());
                        decimal decCurrentSalesRate = MobjclsBLLPOS.GetItemRateForPOS(dgvPOS.Rows[intICounter].Cells[ItemID.Index].Value.ToInt64(), dgvPOS.Rows[intICounter].Cells[BatchID.Index].Value.ToInt64(), txtBillNumber.Tag.ToInt64());
                        if (decCurrentSalesRate == 0 && dgvPOS.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal() > 0 )
                        {
                            if (decCutoffRate > (dgvPOS.Rows[intICounter].Cells[Total.Index].Value.ToDecimal() / dgvPOS.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal()))
                            {
                                dgvPOS.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Red;
                                MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9596);
                                lblPOSstatus.Text = MstrCommonMessage;
                                dgvPOS.Focus();
                                dgvPOS.CurrentCell = dgvPOS[Rate.Index, intICounter];
                                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " (" + decCutoffRate.ToString() + ")", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                return false;
                            }
                            else
                            {
                                dgvPOS.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Black;
                            }
                        }
                        else
                        {
                            if ( dgvPOS.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal() > 0)
                            {
                                if ((dgvPOS.Rows[intICounter].Cells[Total.Index].Value.ToDecimal() / dgvPOS.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal()) > decCutoffRate || (dgvPOS.Rows[intICounter].Cells[Total.Index].Value.ToDecimal() / dgvPOS.Rows[intICounter].Cells[Quantity.Index].Value.ToDecimal()) == decCurrentSalesRate)
                                {
                                    dgvPOS.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Black;
                                }
                                else
                                {
                                    dgvPOS.Rows[intICounter].Cells[Rate.Index].Style.ForeColor = Color.Red;
                                    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9596);
                                    lblPOSstatus.Text = MstrCommonMessage;
                                    dgvPOS.Focus();
                                    dgvPOS.CurrentCell = dgvPOS[Rate.Index, intICounter];
                                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " (" + decCutoffRate.ToString() + ")", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    return false;
                                }
                            }
                        }
                    }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in ValidateForm() " + ex.Message);
                MobjLogs.WriteLog("Error in ValidateForm() " + ex.Message, 2);
                return false;
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                SavePOS();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnSave_Click() " + ex.Message);
                MobjLogs.WriteLog("Error in btnSave_Click() " + ex.Message, 2);
            }
        }
        private void SavePOS()
        {
            try
            {
                if (!ValidateForm(1))
                {
                    return;
                }
                btnNewItem.Focus();
                //if (!MobjclsBLLPOS.clsDTOPOS.blnAddMode)
                //{
                //    new clsDALPOS().DeleteItemSummary(MobjclsBLLPOS.clsDTOPOS.intPOSID);
                //    MobjclsBLLPOS.DeletePOS();
                //   // MobjclsBLLPOS.DeleteAllPOSDetails();
                //}


                if (MobjclsBLLPOS.clsDTOPOS.blnAddMode)
                {
                    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 1, out MmessageIcon).Replace("#", "").Trim();
                    if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                        DialogResult.No)
                    {
                        return;
                    }
                    fillMasterParameters();
                    fillDetailsParameters();
                    int IntSaved = MobjclsBLLPOS.insertPOS();
                    if (IntSaved == 1)
                    {
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 2, out MmessageIcon);
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                        AddMode();
                    }
                    else if (IntSaved == 3)
                    {
                        //MessageBox.Show("POS Number Repeating");
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9582, out MmessageIcon);
                        if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            txtBillNumber.Text = getBillNo();
                            SavePOS();
                        }
                    }
                    else
                    {
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9016, out MmessageIcon);
                        ErrSales.SetError(btnSave, MstrCommonMessage.Replace("#", "").Trim());
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                    }
                }
                else
                {
                    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 3, out MmessageIcon).Replace("#", "").Trim();
                    if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                        DialogResult.No)
                    {
                        return;
                    }
                    if (!MobjclsBLLPOS.clsDTOPOS.blnAddMode)
                    {
                        new clsDALPOS().DeleteItemSummary(MobjclsBLLPOS.clsDTOPOS.intPOSID);
                        //MobjclsBLLPOS.DeletePOS();
                        MobjclsBLLPOS.DeleteAllPOSDetails();
                    }
                    fillMasterParameters();
                    fillDetailsParameters();
                    if (MobjclsBLLPOS.UpdatePOS() == 1)
                    {
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 21, out MmessageIcon);
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                        AddMode();
                    }
                    else
                    {
                        MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9018, out MmessageIcon);
                        ErrSales.SetError(btnSave, MstrCommonMessage.Replace("#", "").Trim());
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        TmrPOS.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SavePOS() " + ex.Message);
                MobjLogs.WriteLog("Error in SavePOS() " + ex.Message, 2);
            }
        }
        public void fillMasterParameters()
        {
            try
            {
                if (MobjclsBLLPOS.clsDTOPOS.blnAddMode)
                {
                    MobjclsBLLPOS.clsDTOPOS.intPOSID = (MobjclsBLLPOS.GetPOSID() + 1);
                }
                MobjclsBLLPOS.clsDTOPOS.strPOSNo = txtBillNumber.Text;
                MobjclsBLLPOS.clsDTOPOS.dtePOSDate = dtpPOSDate.Value;
                MobjclsBLLPOS.clsDTOPOS.intVendorID = cboCustomer.SelectedValue.ToInt32();
                MobjclsBLLPOS.clsDTOPOS.dteDueDate = dtpDueDate.Value;
                MobjclsBLLPOS.clsDTOPOS.intVendorAddID = 0;
                MobjclsBLLPOS.clsDTOPOS.intPaymentTermID = cboPaymentMode.SelectedValue.ToInt32();
                MobjclsBLLPOS.clsDTOPOS.strRemarks = txtRemarks.Text;
                MobjclsBLLPOS.clsDTOPOS.intGrandDiscountID = cboDiscountScheme.SelectedValue.ToInt32();
                MobjclsBLLPOS.clsDTOPOS.intCurrencyID = MobjclsBLLPOS.GetCurrencyID(cboCompany.SelectedValue.ToInt32());
                MobjclsBLLPOS.clsDTOPOS.decExpenseAmount = txtExpense.Text.ToDecimal();
                MobjclsBLLPOS.clsDTOPOS.decGrandDiscountAmount = txtDiscountAmount.Text.ToDecimal();
                MobjclsBLLPOS.clsDTOPOS.decGrandAmount = txtSubTotal.Text.ToDecimal();
                MobjclsBLLPOS.clsDTOPOS.decNetAmount = txtTotal.Text.ToDecimal();
                MobjclsBLLPOS.clsDTOPOS.decNetRounded = txtNetAmount.Text.ToDecimal();
                MobjclsBLLPOS.clsDTOPOS.intCreatedBy = ClsCommonSettings.UserID;
                MobjclsBLLPOS.clsDTOPOS.dteCreatedDate = ClsCommonSettings.GetServerDateTime();
                MobjclsBLLPOS.clsDTOPOS.intCompanyID = cboCompany.SelectedValue.ToInt32();
                MobjclsBLLPOS.clsDTOPOS.intSalesCounterID = cboCounter.SelectedValue.ToInt32();
                MobjclsBLLPOS.clsDTOPOS.decTendered = txtTendered.Text.ToDecimal();
                MobjclsBLLPOS.clsDTOPOS.decChange = txtChange.Text.ToDecimal();
                MobjclsBLLPOS.clsDTOPOS.intWareHouseID = cboWarehouse.SelectedValue.ToInt32();
                MobjclsBLLPOS.clsDTOPOS.intDebitHeadID = (Int32)Accounts.CashAccount ;
                MobjclsBLLPOS.clsDTOPOS.intOperationModID = (Int32)TransactionTypes.CashSale;
                if (MobjclsBLLPOS.clsDTOPOS.blnAddMode == true)
                {
                    MobjclsBLLPOS.clsDTOPOS.intStatusID = (int)OperationStatusType.SPOSDeliverd;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in fillMasterParameters() " + ex.Message);
                MobjLogs.WriteLog("Error in fillMasterParameters() " + ex.Message, 2);
            }
        }
        public void fillDetailsParameters()
        {
            try
            {
                
                decimal TotalQtyBatchless = 0;
                decimal AvailbleQtyinBatch = 0;
                

                DataTable dtBatchlessItems = new DataTable();
                dtBatchlessItems.Columns.Add("ItemID");
                dtBatchlessItems.Columns.Add("BatchID");
                dtBatchlessItems.Columns.Add("AvailableQty");


                DataTable datUsedQtyBatchWise = new DataTable();
                datUsedQtyBatchWise.Columns.Add("ItemID");
                datUsedQtyBatchWise.Columns.Add("BatchID");
                datUsedQtyBatchWise.Columns.Add("TakenQuantity", typeof(decimal));
                datUsedQtyBatchWise.Columns.Add("UOMID");


                MobjclsBLLPOS.clsDTOPOS.lstclsDTOPOSDetails = new List<clsDTOPOSDetails>();
                Int32 intCount = dgvPOS.Rows.Count - 1;
                for (int intICounter = 0; intICounter < intCount; intICounter++)
                {


                    if (dgvPOS.Rows[intICounter].Cells["BatchID"].Value.ToInt32() != 0 && Convert.ToBoolean(dgvPOS.Rows[intICounter].Cells["IsGroup"].Value) != true)
                    {
                        clsDTOPOSDetails objclsDTOPOSDetails = new clsDTOPOSDetails();
                        objclsDTOPOSDetails.intItemID = dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToInt32();
                        objclsDTOPOSDetails.blnIsGroup = Convert.ToBoolean(dgvPOS.Rows[intICounter].Cells["IsGroup"].Value.ToInt32());
                        objclsDTOPOSDetails.decQuantity = dgvPOS.Rows[intICounter].Cells["Quantity"].Value.ToDecimal();
                        objclsDTOPOSDetails.intUOMID = dgvPOS.Rows[intICounter].Cells["Uom"].Tag.ToInt32();
                        objclsDTOPOSDetails.intBatchID = dgvPOS.Rows[intICounter].Cells["BatchID"].Value.ToInt32();
                        objclsDTOPOSDetails.decRate = dgvPOS.Rows[intICounter].Cells["Rate"].Value.ToDecimal();
                        objclsDTOPOSDetails.intDiscountID = dgvPOS.Rows[intICounter].Cells["Discount"].Tag.ToInt32();
                        objclsDTOPOSDetails.decDiscountAmount = dgvPOS.Rows[intICounter].Cells["DiscountAmount"].Value.ToDecimal();
                        objclsDTOPOSDetails.decGrandAmount = dgvPOS.Rows[intICounter].Cells["txtColGrandAmount"].Value.ToDecimal();
                        objclsDTOPOSDetails.decNetAmount = dgvPOS.Rows[intICounter].Cells["Total"].Value.ToDecimal();
                        objclsDTOPOSDetails.blnDeliveryRequired = Convert.ToBoolean(0);
                        objclsDTOPOSDetails.intStatusID = 1;
                        MobjclsBLLPOS.clsDTOPOS.lstclsDTOPOSDetails.Add(objclsDTOPOSDetails);
                    }
                    else if (dgvPOS.Rows[intICounter].Cells["BatchID"].Value.ToInt32() == 0 && Convert.ToBoolean(dgvPOS.Rows[intICounter].Cells["IsGroup"].Value) != true)
                    {

                        decimal QtySelected = 0;
                        dtBatchlessItems = MobjclsBLLPOS.BatchLessItems(dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToInt32(), cboWarehouse.SelectedValue.ToInt32());

                        TotalQtyBatchless = Convert.ToDecimal(dgvPOS.Rows[intICounter].Cells["Quantity"].Value.ToDecimal());

                        foreach (DataRow drBatchLess in dtBatchlessItems.Rows)
                        {
                            if (dgvPOS.Rows[intICounter].Cells["Quantity"].Value.ToDecimal() == QtySelected)
                            {
                                break;
                            }

                            decimal decTakenQuantity = 0;

                            if (datUsedQtyBatchWise != null && datUsedQtyBatchWise.Rows.Count > 0)
                            {
                                decTakenQuantity = datUsedQtyBatchWise.Compute("SUM(TakenQuantity)", "ItemID=" + Convert.ToInt32(dgvPOS.Rows[intICounter].Cells["ItemID"].Value) + " AND BatchID=" + Convert.ToInt32(drBatchLess["BatchID"]) + "").ToDecimal();
                            }
                            //if AvailableQty not equal to 0
                            if ((Convert.ToDecimal(drBatchLess["AvailableQty"]) - decTakenQuantity) > 0)
                            {
                                //get remaining available qty
                                drBatchLess["AvailableQty"] = (Convert.ToDecimal(drBatchLess["AvailableQty"]) - decTakenQuantity);

                                clsDTOPOSDetails objclsDTOPOSDetails = new clsDTOPOSDetails();
                                objclsDTOPOSDetails.intItemID = dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToInt32();
                                objclsDTOPOSDetails.blnIsGroup = Convert.ToBoolean(dgvPOS.Rows[intICounter].Cells["IsGroup"].Value.ToInt32());
                                //objclsDTOPOSDetails.decQuantity = dgvPOS.Rows[intICounter].Cells["Quantity"].Value.ToDecimal();
                                objclsDTOPOSDetails.intUOMID = dgvPOS.Rows[intICounter].Cells["Uom"].Tag.ToInt32();
                                objclsDTOPOSDetails.intBatchID = Convert.ToInt32(drBatchLess["BatchID"]);
                                objclsDTOPOSDetails.decRate = dgvPOS.Rows[intICounter].Cells["Rate"].Value.ToDecimal();
                                objclsDTOPOSDetails.intDiscountID = dgvPOS.Rows[intICounter].Cells["Discount"].Tag.ToInt32();
                                objclsDTOPOSDetails.decDiscountAmount = dgvPOS.Rows[intICounter].Cells["DiscountAmount"].Value.ToDecimal();
                                objclsDTOPOSDetails.decGrandAmount = dgvPOS.Rows[intICounter].Cells["txtColGrandAmount"].Value.ToDecimal();
                                objclsDTOPOSDetails.decNetAmount = dgvPOS.Rows[intICounter].Cells["Total"].Value.ToDecimal();
                                objclsDTOPOSDetails.blnDeliveryRequired = Convert.ToBoolean(0);
                                objclsDTOPOSDetails.intStatusID = 1;
                   
                                AvailbleQtyinBatch = MobjclsBLLCommonUtility.ConvertBaseUnitQtyToOtherQty( dgvPOS.Rows[intICounter].Cells["Uom"].Tag.ToInt32(),dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToInt32(),Convert.ToDecimal(drBatchLess["AvailableQty"]),2);


                                if (AvailbleQtyinBatch < TotalQtyBatchless)
                                {

                                    QtySelected = QtySelected + AvailbleQtyinBatch;
                                    TotalQtyBatchless = TotalQtyBatchless - AvailbleQtyinBatch;
                                    objclsDTOPOSDetails.decQuantity = AvailbleQtyinBatch;
                                }
                                else
                                {
                                    QtySelected = QtySelected + TotalQtyBatchless;

                                    objclsDTOPOSDetails.decQuantity = TotalQtyBatchless;
                                }

                                MobjclsBLLPOS.clsDTOPOS.lstclsDTOPOSDetails.Add(objclsDTOPOSDetails);

                                //Add selected itemid,batchid and quantity from each batch to the new datatable
                                datUsedQtyBatchWise.Rows.Add();
                                datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["ItemID"] = dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToInt32();
                                datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["BatchID"] = Convert.ToInt32(drBatchLess["BatchID"]);
                                datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["TakenQuantity"] = objclsDTOPOSDetails.decQuantity;
                                datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["UOMID"] = dgvPOS.Rows[intICounter].Cells["Uom"].Tag.ToInt32();
                            }
                        }
                    }
                    else if (dgvPOS.Rows[intICounter].Cells["BatchID"].Value.ToInt32() == 0 && Convert.ToBoolean(dgvPOS.Rows[intICounter].Cells["IsGroup"].Value) == true)
                    {

                            clsDTOPOSDetails objclsDTOPOSDetails = new clsDTOPOSDetails();
                            objclsDTOPOSDetails.intItemID = dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToInt32();
                            objclsDTOPOSDetails.blnIsGroup = Convert.ToBoolean(dgvPOS.Rows[intICounter].Cells["IsGroup"].Value.ToBoolean());
                            objclsDTOPOSDetails.decQuantity = dgvPOS.Rows[intICounter].Cells["Quantity"].Value.ToDecimal();
                            objclsDTOPOSDetails.intUOMID = dgvPOS.Rows[intICounter].Cells["Uom"].Tag.ToInt32();
                            objclsDTOPOSDetails.intBatchID = dgvPOS.Rows[intICounter].Cells["BatchID"].Value.ToInt32();
                            objclsDTOPOSDetails.decRate = dgvPOS.Rows[intICounter].Cells["Rate"].Value.ToDecimal();
                            objclsDTOPOSDetails.intDiscountID = dgvPOS.Rows[intICounter].Cells["Discount"].Tag.ToInt32();
                            objclsDTOPOSDetails.decDiscountAmount = dgvPOS.Rows[intICounter].Cells["DiscountAmount"].Value.ToDecimal();
                            objclsDTOPOSDetails.decGrandAmount = dgvPOS.Rows[intICounter].Cells["txtColGrandAmount"].Value.ToDecimal();
                            objclsDTOPOSDetails.decNetAmount = dgvPOS.Rows[intICounter].Cells["Total"].Value.ToDecimal();
                            objclsDTOPOSDetails.blnDeliveryRequired = Convert.ToBoolean(0);
                            objclsDTOPOSDetails.intStatusID = 1;

                            if (objclsDTOPOSDetails.blnIsGroup)
                            {
                                DataTable dtGroupDetailsI = MobjclsBLLPOS.GetGroupDetails(objclsDTOPOSDetails.intItemID);
                                //MDtItemGroupIssueDetails.DefaultView.RowFilter = "ItemGroupID = " + objDtoItemIssueDetails.intItemID + " And ReferenceNo = " + MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intReferenceID + " And ReferenceSerialNo = " + objDtoItemIssueDetails.intReferenceSerialNo;
                                MobjclsBLLPOS.clsDTOPOS.lstclsDTOPOSItemGroupDetails = new List<clsDTOPOSItemGroupDetails>();

                                int intSerialNo = 1;
                                foreach (DataRow drRow in dtGroupDetailsI.Rows)
                                {
                                   
                                    clsDTOPOSItemGroupDetails objItemIssueDetails = new clsDTOPOSItemGroupDetails();                     
                                   
                                    objItemIssueDetails.intItemID = Convert.ToInt32(drRow["ItemID"]);
                                    dtBatchlessItems = MobjclsBLLPOS.BatchLessItems(Convert.ToInt32(drRow["ItemID"]), cboWarehouse.SelectedValue.ToInt32());

                                    TotalQtyBatchless = Convert.ToDecimal(drRow["Quantity"])* dgvPOS.Rows[intICounter].Cells["Quantity"].Value.ToDecimal();
                                    decimal QtySelectedGroupItem = 0;

                                    foreach (DataRow drBatchLess in dtBatchlessItems.Rows)
                                    {
                                        if ((Convert.ToDecimal(drRow["Quantity"]) * dgvPOS.Rows[intICounter].Cells["Quantity"].Value.ToDecimal()) == QtySelectedGroupItem)
                                        {
                                            break;
                                        }

                                        decimal decTakenQuantityG = 0;
                                        if (datUsedQtyBatchWise != null && datUsedQtyBatchWise.Rows.Count > 0)
                                        {
                                            //Total Qty FROM EACH  Item and Batch;decTakenQuantity
                                            decTakenQuantityG = datUsedQtyBatchWise.Compute("SUM(TakenQuantity)", "ItemID=" + Convert.ToInt32(drBatchLess["ItemID"]) + " AND BatchID=" + Convert.ToInt32(drBatchLess["BatchID"]) + "").ToDecimal();

                                        }

                                        //if AvailableQty not equal to 0
                                        if ((Convert.ToDecimal(drBatchLess["AvailableQty"]) - decTakenQuantityG) > 0)
                                        {
                                            objItemIssueDetails = new clsDTOPOSItemGroupDetails();
                                            //clsDTOBatchlessItemIssueDetails objBatchless = new clsDTOBatchlessItemIssueDetails();
                                            objItemIssueDetails.intItemID = Convert.ToInt32(drBatchLess["ItemID"]);

                                            objItemIssueDetails.intUOMID = Convert.ToInt32(drRow["UOMID"]);



                                            objItemIssueDetails.intBatchID = Convert.ToInt32(drBatchLess["BatchID"]);

                                            AvailbleQtyinBatch = Convert.ToDecimal(drBatchLess["AvailableQty"]);


                                            if (AvailbleQtyinBatch < TotalQtyBatchless)
                                            {

                                                QtySelectedGroupItem = QtySelectedGroupItem + AvailbleQtyinBatch;
                                                TotalQtyBatchless = TotalQtyBatchless - AvailbleQtyinBatch;
                                                objItemIssueDetails.decQuantity = (AvailbleQtyinBatch);
                                            }
                                            else
                                            {
                                                QtySelectedGroupItem = QtySelectedGroupItem + TotalQtyBatchless;

                                                //objItemIssueDetails.decQuantity = (TotalQtyBatchless) * dgvPOS.Rows[intICounter].Cells["Quantity"].Value.ToDecimal();
                                                objItemIssueDetails.decQuantity = (TotalQtyBatchless);

                                            }

                                            objItemIssueDetails.intSerialNo = intSerialNo++;
                                            objItemIssueDetails.intPOSDetailsSerialNo = objclsDTOPOSDetails.intItemID;


                                            MobjclsBLLPOS.clsDTOPOS.lstclsDTOPOSItemGroupDetails.Add(objItemIssueDetails);

                                            //Add selected itemid,batchid and quantity from each batch to the new datatable
                                            datUsedQtyBatchWise.Rows.Add();
                                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["ItemID"] = Convert.ToInt32(drBatchLess["ItemID"]);
                                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["BatchID"] = Convert.ToInt32(drBatchLess["BatchID"]);
                                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["TakenQuantity"] = objItemIssueDetails.decQuantity;
                                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["UOMID"] = Convert.ToInt32(drRow["UOMID"]);


                                        }
                                    }

                                }
                            }


                            MobjclsBLLPOS.clsDTOPOS.lstclsDTOPOSDetails.Add(objclsDTOPOSDetails);
                        }
                    

                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in fillDetailsParameters() " + ex.Message);
                MobjLogs.WriteLog("Error in fillDetailsParameters() " + ex.Message, 2);
            }
        }
        
        private DataTable FillDiscountColumn(int intRowIndex, bool blnIgnoreDate)
        {
            try
            {
                decimal decQty, decRate = 0;
                int intItemID = 0;
                int intDiscountID = dgvPOS[Discount.Index, intRowIndex].Tag.ToInt32();
                decRate = dgvPOS[Rate.Index, intRowIndex].Value.ToDecimal();
                intItemID = dgvPOS[ItemID.Index, intRowIndex].Value.ToInt32();
                decQty = dgvPOS[Quantity.Index, intRowIndex].Value.ToDecimal();
                DataTable datUomConversions = MobjclsBLLPOS.GetUomConversionValues(dgvPOS["Uom", intRowIndex].Tag.ToInt32(), intItemID);
                if (datUomConversions.Rows.Count > 0)
                {
                    if (datUomConversions.Rows[0]["ConvertionFactorID"].ToInt32() == 2)
                        decQty = decQty * datUomConversions.Rows[0]["ConvertionValue"].ToDecimal();
                    else
                        decQty = decQty / datUomConversions.Rows[0]["ConvertionValue"].ToDecimal();
                }


                DataTable dtItemCustomerDiscounts = new DataTable();
                DataTable datCurrencyID = MobjclsBLLPOS.FillCombos(new string[] { "CurrencyId", "CompanyMaster", "CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue.ToInt32()) });
                int intCurrencyIDDiscount = 0;
                if (datCurrencyID.Rows.Count > 0)
                {
                    intCurrencyIDDiscount = datCurrencyID.Rows[0]["CurrencyId"].ToInt32();
                }
                //if (blnIgnoreDate)
                //    dtItemCustomerDiscounts = MobjclsBLLPOS.GetItemAndCustomerWiseDiscounts(intItemID, cboCustomer.SelectedValue.ToInt32(), decQty, decRate, "", intCurrencyIDDiscount);
                //else
                dtItemCustomerDiscounts = MobjclsBLLPOS.GetItemAndCustomerWiseDiscounts(intItemID, cboCustomer.SelectedValue.ToInt32(), decQty, decRate, dtpPOSDate.Value.ToString("dd-MMM-yyyy"), intCurrencyIDDiscount);
                Discount.DataSource = null;
                Discount.ValueMember = "DiscountID";
                Discount.DisplayMember = "DiscountShortName";
                DataRow dr = dtItemCustomerDiscounts.NewRow();
                dr["DiscountID"] = 0;
                dr["DiscountShortName"] = "No Discount";
                dtItemCustomerDiscounts.Rows.InsertAt(dr, 0);
                Discount.DataSource = dtItemCustomerDiscounts;

                dgvPOS[Discount.Index, intRowIndex].Value = intDiscountID;
                dgvPOS[Discount.Index, intRowIndex].Tag = intDiscountID;
                dgvPOS[Discount.Index, intRowIndex].Value = dgvPOS[Discount.Index, intRowIndex].FormattedValue;
                if (string.IsNullOrEmpty(dgvPOS[Discount.Index, intRowIndex].Value.ToString()))
                {
                    dgvPOS[Discount.Index, intRowIndex].Value = 0;
                    dgvPOS[Discount.Index, intRowIndex].Tag = 0;
                    dgvPOS[Discount.Index, intRowIndex].Value = dgvPOS[Discount.Index, intRowIndex].FormattedValue;
                }
                return dtItemCustomerDiscounts;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FillDiscountColumn() " + ex.Message);
                MobjLogs.WriteLog("Error in FillDiscountColumn() " + ex.Message, 2);
                return null;
            }
        }

        private void CalculateTotalAmt()
        {
            try
            {
                decimal decDirectRate = 0, decRate = 0, decAmount = 0, decTotal = 0, decSubtotal = 0, decQty = 0;
                decimal decDiscountAmt = 0;

                if (dgvPOS.RowCount > 0)
                {
                    for (int intICounter = 0; intICounter < dgvPOS.RowCount - 1; intICounter++)
                    {
                        int intDiscountID = 0, intItemID = 0, intVendorID = 0;
                        decQty = decAmount = decDirectRate = decRate = 0;

                        intItemID = dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToInt32();
                        intDiscountID = dgvPOS.Rows[intICounter].Cells["Discount"].Tag.ToInt32();
                        intVendorID = cboCustomer.SelectedValue.ToInt32();

                        decQty = dgvPOS.Rows[intICounter].Cells["Quantity"].Value.ToDecimal();

                        decDirectRate = dgvPOS.Rows[intICounter].Cells["Rate"].Value.ToDecimal();

                        if (decDirectRate > 0 && intItemID > 0  )
                        {
                            if (intDiscountID > 0)
                            {
                                if (intDiscountID != (int)PredefinedDiscounts.DefaultSalesDiscount)
                                {
                                    MobjclsBLLPOS.GetItemDiscountAmount(9, intItemID, intDiscountID, intVendorID);

                                    if (MobjclsBLLPOS.clsDTOPOS.blnDiscountForAmount)
                                    {
                                        decSubtotal = decAmount = (decQty * decDirectRate);

                                        if (MobjclsBLLPOS.clsDTOPOS.blnPercentCalculation)
                                            decAmount = decAmount - (decAmount * MobjclsBLLPOS.clsDTOPOS.decDiscountValue) / 100;
                                        else
                                            decAmount = decAmount - MobjclsBLLPOS.clsDTOPOS.decDiscountValue;
                                    }
                                    else
                                    {
                                        decSubtotal = (decQty * decDirectRate);

                                        if (MobjclsBLLPOS.clsDTOPOS.blnPercentCalculation)
                                            decRate = decDirectRate - (decDirectRate * MobjclsBLLPOS.clsDTOPOS.decDiscountValue) / 100;
                                        else
                                            decRate = decDirectRate - MobjclsBLLPOS.clsDTOPOS.decDiscountValue;

                                        decAmount = (decQty * decRate);
                                    }
                                }
                                else
                                {
                                    decSubtotal = decAmount = (decQty * decDirectRate);
                                    MobjclsBLLPOS.clsDTOPOS.decDiscountValue = dgvPOS.Rows[intICounter].Cells[DiscountAmount.Index].Value.ToDecimal();
                                    decAmount = decAmount - MobjclsBLLPOS.clsDTOPOS.decDiscountValue;
                                }
                            }
                            else
                            {
                                
                                decSubtotal = decAmount = (decQty * decDirectRate);
                            }

                            if (decAmount < 0)
                            {
                                //MsMessageCommon = "Discount exceeds the item amount.";
                                //MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2013, out MmessageIcon);
                                //MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                //  dgvPOS.Rows[intICounter].Cells["DiscountAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                                // dgvPOS.Rows[intICounter].Cells["NetAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                                //return;
                            }

                            decDiscountAmt = decSubtotal - decAmount;

                            dgvPOS.Rows[intICounter].Cells["txtColGrandAmount"].Value = decSubtotal.ToString("F" + MintExchangeCurrencyScale);
                            dgvPOS.Rows[intICounter].Cells["DiscountAmount"].Value = decDiscountAmt.ToString("F" + MintExchangeCurrencyScale);
                            dgvPOS.Rows[intICounter].Cells["Total"].Value = decAmount.ToString("F" + MintExchangeCurrencyScale);
                            decTotal += decAmount;
                        }
                    }
                    txtSubTotal.Text = decTotal.ToString("F" + MintExchangeCurrencyScale);
                    CalculateNetTotal();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CalculateTotalAmt() " + ex.Message);
                MobjLogs.WriteLog("Error in CalculateTotalAmt() " + ex.Message, 2);
            }
        }
        private void CalculateNetTotal()
        {
            try
            {
                decimal decAmount = 0, decSubtotal = 0, decNetAmt = 0;
                decimal decDiscountAmt = 0, decExpenseAmt = 0;

                int intDiscountID = cboDiscountScheme.SelectedValue.ToInt32();
                int intVendorID = cboCustomer.SelectedValue.ToInt32();

                decSubtotal = decAmount = txtSubTotal.Text.ToDecimal();

                if (intDiscountID > 0)
                {
                    if (intDiscountID != (int)PredefinedDiscounts.DefaultSalesDiscount)
                    {
                        MobjclsBLLPOS.GetItemDiscountAmount(9, 0, intDiscountID, intVendorID);

                        // if (MobjclsBLLSTSales.clsDTOSTSalesMaster.blnDiscountForAmount)
                        //{
                        if (MobjclsBLLPOS.clsDTOPOS.blnPercentCalculation)
                            decAmount = decAmount - (decAmount * MobjclsBLLPOS.clsDTOPOS.decDiscountValue) / 100;
                        else
                            decAmount = decAmount - MobjclsBLLPOS.clsDTOPOS.decDiscountValue;
                        // }

                    }
                    else
                    {
                        MobjclsBLLPOS.clsDTOPOS.decDiscountValue = txtDiscountAmount.Text.ToDecimal();
                        decAmount = decAmount - MobjclsBLLPOS.clsDTOPOS.decDiscountValue;
                    }
                }

                if (decAmount < 0)
                {
                    //MsMessageCommon = "Discount exceeds the item amount.";
                    //MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2013, out MmessageIcon);
                    //MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                }

                decDiscountAmt = decSubtotal - decAmount;
                decExpenseAmt = Convert.ToDecimal(txtExpense.Text.ToDouble());
                if (intDiscountID != (int)PredefinedDiscounts.DefaultSalesDiscount)
                {
                    txtDiscountAmount.Text = decDiscountAmt.ToString("F" + MintExchangeCurrencyScale);
                }
               // MobjclsBLLPOS.clsDTOPOS.decGrandDiscountAmount = decDiscountAmt;

                decNetAmt = decAmount + decExpenseAmt;
                txtTotal.Text = decNetAmt.ToString("F" + MintExchangeCurrencyScale);
                txtExpense.Text = Convert.ToDecimal(txtExpense.Text).ToString("F" + MintExchangeCurrencyScale);
                txtNetAmount.Text = txtTotal.Text;
                //txtTendered.Text = "0.00";
                //txtChange.Text = "0.00";
               // CalculateExchangeCurrencyRate();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CalculateNetTotal() " + ex.Message);
                MobjLogs.WriteLog("Error in CalculateNetTotal() " + ex.Message, 2);
            }
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (cboCurrency.SelectedValue != null)
                //{
                //    //DataTable datCurrency = MobjclsBLLSTSales.FillCombos(new string[] { "CurrencyID,Scale", "CurrencyReference", "CurrencyID = " + Convert.ToInt32(cboCurrency.SelectedValue) });
                //    DataTable datCurrency = MobjclsBLLPOS.FillCombos(new string[] { "CurrencyID,Scale", "CurrencyReference", "CurrencyID = 37 " });

                //    if (datCurrency.Rows.Count > 0)
                //        MintExchangeCurrencyScale = Convert.ToInt32(datCurrency.Rows[0]["Scale"]);
                //}
                //CalculateTotalAmt();
                //CalculateNetTotal();
                //CalculateExchangeCurrencyRate();
                DataTable datCurrencyID = MobjclsBLLPOS.FillCombos(new string[] { "CurrencyId", "CompanyMaster", "CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) });
                if (datCurrencyID.Rows.Count > 0)
                {
                    int intCurrencyID = datCurrencyID.Rows[0][0].ToString().ToInt32();

                    DataTable datCurrency = MobjclsBLLPOS.FillCombos(new string[] { "CurrencyID,Scale", "CurrencyReference", "CurrencyID =  " + intCurrencyID });
                    if (datCurrency.Rows.Count > 0)
                    {
                        MintExchangeCurrencyScale = Convert.ToInt32(datCurrency.Rows[0]["Scale"]);
                    }
                }

                txtBillNumber.Text = getBillNo();
                LoadCombos(6);
                //dgvPOS.Rows.Clear();
                ClearControls();
                cboWarehouse.Text = null;
                ChangeStatus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in cboCompany_SelectedIndexChanged() " + ex.Message);
                MobjLogs.WriteLog("Error in cboCompany_SelectedIndexChanged() " + ex.Message, 2);
            }
        }
        private string getBillNo()
        {
            try
            {
                if (cboCompany.SelectedValue.ToInt32() > 0)
                {
                    clsBLLLogin objClsBLLLogin = new clsBLLLogin();
                    DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue.ToInt32()));

                    //dtCompanySettingsInfo.DefaultView.RowFilter = "ConfigurationItem = 'Item Issue' And SubItem = 'Prefix'";
                    //if (dtCompanySettingsInfo.DefaultView.ToTable().Rows.Count > 0)
                    //    return dtCompanySettingsInfo.DefaultView.ToTable().Rows[0]["ConfigurationValue"].ToString();
                    //return "";
                    MobjclsBLLPOS.clsDTOPOS.intSLNO = (MobjclsBLLPOS.GetPOSNo(cboCompany.SelectedValue.ToInt32()) + 1);
                    return (ClsCommonSettings.strPOSPrefix + (MobjclsBLLPOS.clsDTOPOS.intSLNO).ToString());
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in getBillNo() " + ex.Message);
                MobjLogs.WriteLog("Error in getBillNo() " + ex.Message, 2);
                return "";
            }
        }

        private void dgvPOS_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            SetSerialNo();
            //dgvPOS.Rows[dgvPOS.Rows.Count - 1].Cells["Quantity"].Value = DBNull.Value;

        }

        private void dgvPOS_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            SetSerialNo();
            CalculateTotalAmt();
        }
        private void SetSerialNo()
        {
            try
            {
                int intRowNumber = 1;
                foreach (DataGridViewRow row in dgvPOS.Rows)
                {
                    if (row.Index < dgvPOS.Rows.Count - 1)
                    {
                        row.HeaderCell.Value = intRowNumber.ToString();
                        intRowNumber++;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SetSerialNo() " + ex.Message);
                MobjLogs.WriteLog("Error in SetSerialNo() " + ex.Message, 2);
            }
        }

        private void txtTendered_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtChange.Text = (txtTendered.Text.ToDouble() - txtNetAmount.Text.ToDouble()).ToDouble().ToString("F" + MintExchangeCurrencyScale);
                ChangeStatus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in txtTendered_TextChanged() " + ex.Message);
                MobjLogs.WriteLog("Error in txtTendered_TextChanged() " + ex.Message, 2);
            }
        }
        private void txtTendered_Enter(object sender, EventArgs e)
        {
            try
            {
                if (MobjclsBLLPOS.clsDTOPOS.blnAddMode == true && txtTendered.Text.ToDecimal() == 0)
                    txtTendered.Text = txtNetAmount.Text.ToDecimal().ToString();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in txtTendered_Enter() " + ex.Message);
                MobjLogs.WriteLog("Error in txtTendered_Enter() " + ex.Message, 2);
            }
           
        }

        private void txtNetAmount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int intCurrencyID1 = MobjclsBLLPOS.GetCurrencyID(cboCompany.SelectedValue.ToInt32());
                lblAmountInWords.Text = MobjclsBLLCommonUtility.ConvertToWord(Convert.ToString(txtNetAmount.Text), intCurrencyID1);

                txtChange.Text = (txtTendered.Text.ToDouble() - txtNetAmount.Text.ToDouble()).ToDouble().ToString("F" + MintExchangeCurrencyScale);
                ChangeStatus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in txtNetAmount_TextChanged() " + ex.Message);
                MobjLogs.WriteLog("Error in txtNetAmount_TextChanged() " + ex.Message, 2);
            }

        }

        private void txtTendered_Click(object sender, EventArgs e)
        {
            txtTendered.SelectAll();
        }

        private void txtNetAmount_Click(object sender, EventArgs e)
        {
            txtNetAmount.SelectAll();
        }

        private void cboDiscountScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboDiscountScheme.SelectedIndex != -1 && cboDiscountScheme.SelectedValue.ToInt32() != (int)PredefinedDiscounts.DefaultSalesDiscount)
                {   txtDiscountAmount.Text = "0.00";
                    txtDiscountAmount.ReadOnly = true;
                }
                else
                {
                     txtDiscountAmount.Text = "0.00";
                    txtDiscountAmount.ReadOnly = false;
                }
                CalculateNetTotal();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in cboDiscountScheme_SelectedIndexChanged() " + ex.Message);
                MobjLogs.WriteLog("Error in cboDiscountScheme_SelectedIndexChanged() " + ex.Message, 2);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteCurrentItem();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnRemove_Click() " + ex.Message);
                MobjLogs.WriteLog("Error in btnRemove_Click() " + ex.Message, 2);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            AddMode();
          
        }
        private void AddMode()
        {
            try
            {
                ClearControls();
                MobjclsBLLPOS.clsDTOPOS.intPOSID = 0;
                SearchedItem();
                //dgvPOS.CurrentCell = dgvPOS.Rows[(dgvPOS.Rows.Count - 1)].Cells[0];
                //dgvPOS.BeginEdit(true);
                wbStatus.Text = "Status : New";
                if (MobjclsBLLPOS.clsDTOPOS.blnAddMode == false)
                {
                    MobjclsBLLPOS.clsDTOPOS.blnAddMode = true;
                    clearMasterControls();
                }
                BindingNavigatorCancelItem.Text = "Cancel";
                lblCancelReason.Visible = false;
                txtCancelReason.Visible = false;
                panelTop.Enabled = true;
                panelBottom.Enabled = true;
                panelGridBottom.Enabled = true;
                ClearControlVisibility();

                DataTable datTemp = (DataTable)cboCompany.DataSource;
                if (datTemp == null)
                    cboCompany.Text = "";
                else if (datTemp.Rows.Count == 0)
                    cboCompany.Text = "";

                if (cboPaymentMode.Items.Count > 0)
                    cboPaymentMode.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in AddMode() " + ex.Message);
                MobjLogs.WriteLog("Error in AddMode() " + ex.Message, 2);
            }
        }
        private void ClearControlVisibility()
        {
                    BindingNavigatorSaveItem.Enabled = false;
                    btnSave.Enabled = false;
                    btnNew.Enabled = false;
                    BindingNavigatorAddNewItem.Enabled = false;
                    BindingNavigatorDeleteItem.Enabled = false;
                    BtnPrint.Enabled = false;
                    BtnEmail.Enabled = false;
                    BindingNavigatorClearItem.Enabled = false;
                    BindingNavigatorCancelItem.Enabled= false;


                if (MobjclsBLLPOS.clsDTOPOS.blnAddMode == false)
                {
                    BindingNavigatorAddNewItem.Enabled = btnNew.Enabled = MblnAddPermission;               
                }
           
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            
            SearchedItem();
            
        }
        private void SearchedItem()
        {
            try
            {
                //MobjclsBLLPOS.clsDTOPOS.strPOSNo = txtBillNoSearch.Text.Trim();
                //MobjclsBLLPOS.clsDTOPOS.dtePOSFromDate = dtpSFromSearch.Value;
                //MobjclsBLLPOS.clsDTOPOS.dtePOSToDate = dtpSToSearch.Value;
                //MobjclsBLLPOS.clsDTOPOS.intVendorID = cboCustomerSearch.SelectedValue.ToInt32();
                //MobjclsBLLPOS.clsDTOPOS.intCreatedBy = cboCashierSearch.SelectedValue.ToInt32();
                //MobjclsBLLPOS.clsDTOPOS.intCompanyID = cboCompanySearch.SelectedValue.ToInt32();
                //MobjclsBLLPOS.clsDTOPOS.intSearchStatusID = cboStatusSearch.SelectedValue.ToInt32();
                //MobjclsBLLPOS.clsDTOPOS.intSalesCounterID = cboCounterSearch.SelectedValue.ToInt32();
                //DataTable datBillNo = new DataTable();
                //datBillNo = MobjclsBLLPOS.SearchPOSNos();
                
                DataTable datPermittedCompanies = new DataTable();
                //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                datPermittedCompanies = MobjclsBLLPOS.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                //else
                //    datPermittedCompanies = MobjclsBLLPOS.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.POSCompany);

                string strFilterCondition = "";
                //     strFilterCondition += "CompanyID In (0,";
                //    if (datPermittedCompanies.Rows.Count > 0)
                //    {
                //        foreach (DataRow dr in datPermittedCompanies.Rows)
                //        {
                //            strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                //        }                    
                //    }
                //    strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                //    strFilterCondition += ")";

                if (cboCompanySearch.SelectedValue != null && Convert.ToInt32(cboCompanySearch.SelectedValue) != -2)
                    strFilterCondition += " CompanyID = " + Convert.ToInt32(cboCompanySearch.SelectedValue);
                else
                {
                    DataTable datTemp = (DataTable)cboCompanySearch.DataSource;
                    if (datTemp.Rows.Count > 0)
                    {
                        strFilterCondition += "CompanyID In (";
                        foreach (DataRow dr in datTemp.Rows)
                        {
                            strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                        }
                        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                        strFilterCondition += ")";
                    }
                }

                if (cboCashierSearch.SelectedValue != null && Convert.ToInt32(cboCashierSearch.SelectedValue) != -2)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    //if(cboCashierSearch.SelectedIndex!=-1)
                    strFilterCondition += "EmployeeID = " + Convert.ToInt32(cboCashierSearch.SelectedValue);
                }
                //else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                //{
                //    DataTable datTemp = (DataTable)cboCashierSearch.DataSource;
                //    if (datTemp.Rows.Count > 0)
                //    {
                //        DataTable datTempPer = MobjClsBLLPermissionSettings.GetControlPermissions1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.POSEmployee);
                //        if (datTempPer != null)
                //        {
                //            if (datTempPer.Rows.Count > 0)
                //            {
                //                if (datTempPer.Rows[0]["IsVisible"].ToString() == "True")
                //                    strFilterCondition += "And EmployeeID In (0,";
                //                else
                //                    strFilterCondition += "And EmployeeID In (";
                //            }
                //            else
                //                strFilterCondition += "And EmployeeID In (";
                //        }
                //        else
                //            strFilterCondition += "And EmployeeID In (";
                //        foreach (DataRow dr in datTemp.Rows)
                //        {
                //            strFilterCondition += Convert.ToInt32(dr["EmployeeID"]) + ",";
                //        }
                //        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                //        strFilterCondition += ")";
                //    }
                //}
                if (cboCustomerSearch.SelectedValue != null && Convert.ToInt32(cboCustomerSearch.SelectedValue) != -2)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "VendorID = " + Convert.ToInt32(cboCustomerSearch.SelectedValue);
                }
                if (cboStatusSearch.SelectedValue != null && Convert.ToInt32(cboStatusSearch.SelectedValue) != -2)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "StatusID = " + Convert.ToInt32(cboStatusSearch.SelectedValue);
                }
                if (cboCounterSearch.SelectedValue != null && Convert.ToInt32(cboCounterSearch.SelectedValue) != -2)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "SalesCounterID = " + Convert.ToInt32(cboCounterSearch.SelectedValue);
                }
                if (!string.IsNullOrEmpty(strFilterCondition))
                    strFilterCondition += " And ";
                strFilterCondition += " convert(datetime,convert(varchar,POSDate,106),106) BETWEEN '" + dtpSFromSearch.Value.ToString("dd-MMM-yyyy") + 
                    "' And '" + dtpSToSearch.Value.ToString("dd-MMM-yyyy") + "'";

                if (!string.IsNullOrEmpty(txtBillNoSearch.Text.Trim()))
                {
                    strFilterCondition = " [POSNo] = '" + txtBillNoSearch.Text.Trim() + "'";
                }
                                
                //datBillNo.DefaultView.RowFilter = strFilterCondition;
                DataTable datBillNo = new DataTable();
                datBillNo = MobjclsBLLPOS.SearchPOSNos(strFilterCondition);

                datBillNo.Columns.Remove("EmployeeID");
                datBillNo.Columns.Remove("VendorID");
                datBillNo.Columns.Remove("StatusID");
                datBillNo.Columns.Remove("SalesCounterID");
                datBillNo.Columns.Remove("POSDate");
                DgvBillNo.DataSource = datBillNo.DefaultView.ToTable();
                DgvBillNo.Columns["POSID"].Visible = false;
                DgvBillNo.Columns["CompanyID"].Visible = false;
                LblSCountStatus.Text = "Total POS : " + DgvBillNo.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SearchedItem() " + ex.Message);
                MobjLogs.WriteLog("Error in SearchedItem() " + ex.Message, 2);
            }
        }

        private void DgvBillNo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DgvBillNo.Rows.Count > 0)
                {
                    MobjclsBLLPOS.clsDTOPOS.intPOSID = DgvBillNo.Rows[DgvBillNo.CurrentRow.Index].Cells["POSID"].Value.ToInt32();
                    EditMode();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DgvBillNo_CellClick() " + ex.Message);
                MobjLogs.WriteLog("Error in DgvBillNo_CellClick() " + ex.Message, 2);
            }

        }
      private void EditMode()
        {
            try
            {
                DisplayPOS();
                MobjclsBLLPOS.clsDTOPOS.blnAddMode = false;
                ClearControlVisibility();
                BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
                if (MobjclsBLLPOS.clsDTOPOS.intStatusID == (int)OperationStatusType.SPOSCancelled)
                {
                    BindingNavigatorCancelItem.Text = "ReOpen";
                    lblCancelReason.Visible = true;
                    txtCancelReason.Visible = true;
                }
                else
                {
                    BindingNavigatorCancelItem.Text = "Cancel";
                    lblCancelReason.Visible = false;
                    txtCancelReason.Visible = false;
                }

            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in EditMode() " + ex.Message);
                MobjLogs.WriteLog("Error in EditMode() " + ex.Message, 2);
            }

        }
       private void DisplayPOS()
      {
          try
          {
              ClearControls();
              DataTable datPOS = MobjclsBLLPOS.GetPOS();
              if (datPOS.Rows.Count > 0)
              {
                  cboCompany.SelectedValue = datPOS.Rows[0]["CompanyID"].ToInt32();
                  if (cboCompany.SelectedValue == null)
                  {
                      DataTable datTemp = MobjclsBLLPOS.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + datPOS.Rows[0]["CompanyID"].ToInt32() });
                      if (datTemp.Rows.Count > 0)
                          cboCompany.Text = datTemp.Rows[0]["CompanyName"].ToString();
                  }
                  cboCustomer.SelectedValue = datPOS.Rows[0]["VendorID"].ToInt32();
                  dtpPOSDate.Value = datPOS.Rows[0]["POSDate"].ToDateTime();
                  cboCustomer.Text = datPOS.Rows[0]["VendorName"].ToString();
                  dtpDueDate.Value = datPOS.Rows[0]["DueDate"].ToDateTime();
                  cboPaymentMode.SelectedValue = datPOS.Rows[0]["PaymentTermsID"].ToInt32();
                  txtRemarks.Text = datPOS.Rows[0]["Remarks"].ToString();
                  txtExpense.Text = datPOS.Rows[0]["ExpenseAmount"].ToString();
                  txtSubTotal.Text = datPOS.Rows[0]["GrandAmount"].ToString();
                  txtTotal.Text = datPOS.Rows[0]["NetAmount"].ToString();
                  wbCreatedBy.Text = "Cashier : " + datPOS.Rows[0]["CreatedBy"].ToString();
                  wbCreatedDate.Text = "Created Date : " + datPOS.Rows[0]["CreatedDate"].ToDateTime().ToString("dd-MMM-yyyy  hh:mm:ss tt");
                  wbStatus.Text = "Status : " + datPOS.Rows[0]["POSStatus"].ToString(); ;
                  cboCounter.SelectedValue = datPOS.Rows[0]["SalesCounterID"].ToInt32();
                  cboWarehouse.SelectedValue = datPOS.Rows[0]["WareHouseID"].ToInt32();
                  txtBillNumber.Text = datPOS.Rows[0]["POSNo"].ToString();
                  txtBillNumber.Tag = datPOS.Rows[0]["POSID"].ToInt64();
                  MobjclsBLLPOS.clsDTOPOS.intStatusID = datPOS.Rows[0]["StatusID"].ToInt32();

                  for (int intICounter = 0; intICounter < datPOS.Rows.Count; intICounter++)
                  {
                      dgvPOS.Rows.Add();
                      dgvPOS.Rows[intICounter].Cells["IsGroup"].Value = datPOS.Rows[intICounter]["IsGroup"].ToBoolean();
                      dgvPOS.Rows[intICounter].Cells["ItemID"].Value = datPOS.Rows[intICounter]["ItemID"].ToInt32();
                      dgvPOS.Rows[intICounter].Cells["ItemCode"].Value = datPOS.Rows[intICounter]["Code"].ToString();
                      dgvPOS.Rows[intICounter].Cells["ItemName"].Value = datPOS.Rows[intICounter]["ItemName"].ToString();
                      dgvPOS.Rows[intICounter].Cells["Quantity"].Value = Math.Round(datPOS.Rows[intICounter]["Quantity"].ToDecimal(), 2);
                      dgvPOS.Rows[intICounter].Cells["Uom"].Value = datPOS.Rows[intICounter]["UOMID"].ToInt32();
                      dgvPOS.Rows[intICounter].Cells["Uom"].Tag = dgvPOS.Rows[intICounter].Cells["Uom"].Value;
                      dgvPOS.Rows[intICounter].Cells["Uom"].Value = dgvPOS.Rows[intICounter].Cells["Uom"].FormattedValue;
                      dgvPOS.Rows[intICounter].Cells["BatchID"].Value = datPOS.Rows[intICounter]["BatchID"].ToInt32();
                      dgvPOS.Rows[intICounter].Cells["BatchNo"].Value = datPOS.Rows[intICounter]["BatchNo"].ToString();
                      dgvPOS.Rows[intICounter].Cells["Rate"].Value = datPOS.Rows[intICounter]["Rate"].ToDecimal();
                      dgvPOS.Rows[intICounter].Cells["Discount"].Value = datPOS.Rows[intICounter]["DiscountID"].ToInt32();
                      dgvPOS.Rows[intICounter].Cells["Discount"].Tag = dgvPOS.Rows[intICounter].Cells["Discount"].Value;
                      dgvPOS.Rows[intICounter].Cells["Discount"].Value = dgvPOS.Rows[intICounter].Cells["Discount"].FormattedValue;
                      dgvPOS.Rows[intICounter].Cells["DiscountAmount"].Value = datPOS.Rows[intICounter]["DiscountAmount"].ToDecimal();
                      dgvPOS.Rows[intICounter].Cells["txtColGrandAmount"].Value = datPOS.Rows[intICounter]["GrandAmount"].ToDecimal();
                      dgvPOS.Rows[intICounter].Cells["Total"].Value = datPOS.Rows[intICounter]["NetAmount"].ToDecimal();

                  }
                  //txtNetAmount.Text = (datPOS.Rows[0]["NetAmountRounded"].ToString().ToDecimal()).ToString("N2");
                  cboDiscountScheme.SelectedValue = datPOS.Rows[0]["GrandDiscountID"].ToInt32();
                  //txtDiscountAmount.Text =  Math.Round(datPOS.Rows[0]["GrandDiscountAmount"].ToDecimal(), 2).ToString();"F" + MintExchangeCurrencyScale
                  //txtNetAmount.Text = Math.Round(datPOS.Rows[0]["NetAmountRounded"].ToDecimal(), 2).ToString();
                  //txtTendered.Text = Math.Round(datPOS.Rows[0]["TenderAmount"].ToDecimal(), 2).ToString();
                  //txtChange.Text = Math.Round(datPOS.Rows[0]["Change"].ToDecimal(), 2).ToString();
                  txtDiscountAmount.Text =  datPOS.Rows[0]["GrandDiscountAmount"].ToDecimal().ToString("F" + MintExchangeCurrencyScale);
                  txtNetAmount.Text = datPOS.Rows[0]["NetAmountRounded"].ToDecimal().ToString("F" + MintExchangeCurrencyScale);
                  txtTendered.Text = datPOS.Rows[0]["TenderAmount"].ToDecimal().ToString("F" + MintExchangeCurrencyScale);
                  txtChange.Text = datPOS.Rows[0]["Change"].ToDecimal().ToString("F" + MintExchangeCurrencyScale);
                  txtCancelReason.Text = datPOS.Rows[0]["CancelReason"].ToString();
                  if ((datPOS.Rows[0]["VendorStatusID"].ToInt32() == (int)VendorStatus.CustomerBlocked) || (MobjclsBLLPOS.clsDTOPOS.intStatusID == (int)OperationStatusType.SPOSCancelled))
                  {
                      //panelEx1.Enabled = false;
                      panelTop.Enabled = false;
                      panelBottom.Enabled = false;
                      panelGridBottom.Enabled = false;
                  }
                  else
                  {
                      //panelEx1.Enabled = true;
                      panelTop.Enabled = true;
                      panelBottom.Enabled = true;
                      panelGridBottom.Enabled = true;
                  }
                  lblPOSstatus.Text = "Details of " + txtBillNumber.Text + " Displayed";
                  TmrPOS.Enabled = true;
              }
          }
          catch (Exception ex)
          {
              if (ClsCommonSettings.ShowErrorMess)
                  MessageBox.Show("Error in DisplayPOS() " + ex.Message);
              MobjLogs.WriteLog("Error in DisplayPOS() " + ex.Message, 2);
          }
        }

       private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
       {
           try
           {
               MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 13, out MmessageIcon).Replace("#", "").Trim();
               if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                   DialogResult.No)
               {
                   return;
               }
               MobjclsBLLPOS.clsDTOPOS.intCreatedBy = ClsCommonSettings.UserID;
               MobjclsBLLPOS.clsDTOPOS.intOperationModID = (int)OperationType.POS;
               if (MobjclsBLLPOS.DeletePOS())
               {
                   MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 4, out MmessageIcon);
                   MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                   lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                   TmrPOS.Enabled = true;
                   AddMode();
               }
               else
               {
                   MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9017, out MmessageIcon);
                   MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                   lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                   TmrPOS.Enabled = true;
               }
           }
           catch (Exception ex)
           {
               if (ClsCommonSettings.ShowErrorMess)
                   MessageBox.Show("Error in BindingNavigatorDeleteItem_Click() " + ex.Message);
               MobjLogs.WriteLog("Error in BindingNavigatorDeleteItem_Click() " + ex.Message, 2);
           }
       }

       private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
       {
           AddMode();
       }

       private void BindingNavigatorClearItem_Click(object sender, EventArgs e)
       {
           //ClearControls();
           clearMasterControls();
       }

       private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
       {
           SavePOS();
       }

       private void expandableSplitterLeftPOS_ExpandedChanged(object sender, ExpandedChangeEventArgs e)
       {
           try
           {
               if (expandableSplitterLeftPOS.Expanded == false)
               {
                   ClearSearchBoxControls();
                   dgvPOS.Focus();
               }
               else
               {
                   SearchedItem();
                   DgvBillNo.Focus();
               }
           }
           catch (Exception ex)
           {
               if (ClsCommonSettings.ShowErrorMess)
                   MessageBox.Show("Error in expandableSplitterLeftPOS_ExpandedChanged() " + ex.Message);
               MobjLogs.WriteLog("Error in expandableSplitterLeftPOS_ExpandedChanged() " + ex.Message, 2);
           }
       }

       private void tmrCreatedDateTime_Tick(object sender, EventArgs e)
       {
           try
           {
               if (MobjclsBLLPOS.clsDTOPOS.blnAddMode)
                   wbCreatedDate.Text = "Created Date : " + ClsCommonSettings.GetServerDateTime().ToString("dd-MMM-yyyy  hh:mm:ss tt");
           }
           catch (Exception ex)
           {
               if (ClsCommonSettings.ShowErrorMess)
                   MessageBox.Show("Error in tmrCreatedDateTime_Tick() " + ex.Message);
               MobjLogs.WriteLog("Error in tmrCreatedDateTime_Tick() " + ex.Message, 2);
           }
       }

       private void BindingNavigatorCancelItem_Click(object sender, EventArgs e)
       {
           try
           {
               DisplayPOS();
               if (MobjclsBLLPOS.clsDTOPOS.intStatusID == (int)OperationStatusType.SPOSCancelled)
               {
                   fillMasterParameters();
                   //if (!ValidateForm(1))
                   //{
                   //    return;
                   //}
                   for (int intICounter = 0; intICounter < dgvPOS.RowCount - 1; intICounter++)
                   {
                       DataTable datQtyAvailable = MobjclsBLLPOS.GetQuantityAvailable(dgvPOS.Rows[intICounter].Cells["ItemID"].Value.ToInt32(), cboWarehouse.SelectedValue.ToInt32(), dgvPOS.Rows[intICounter].Cells["BatchID"].Value.ToInt32(), MobjclsBLLPOS.clsDTOPOS.intPOSID);
                       if (datQtyAvailable.Rows.Count > 0)
                       {
                           decimal decQuantity = 0;
                           decimal decAvailQuantity = 0;
                           decQuantity = dgvPOS.Rows[intICounter].Cells["Quantity"].Value.ToDecimal();
                           foreach (DataRow dr in datQtyAvailable.Rows)
                           {
                               decAvailQuantity = decAvailQuantity + Convert.ToInt32(dr["QtyAvailable"]).ToDecimal();
                           }
                           DataTable dtGetUomDetails = MobjclsBLLPOS.GetUomConversionValues(dgvPOS["UOM", intICounter].Tag.ToInt32(), dgvPOS["ItemID", intICounter].Value.ToInt32());
                           if (dtGetUomDetails.Rows.Count > 0)
                           {
                               int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                               decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                               if (intConversionFactor == 1)
                                   decQuantity = decQuantity / decConversionValue;
                               else if (intConversionFactor == 2)
                                   decQuantity = decQuantity * decConversionValue;
                           }

                           //if (datQtyAvailable.Rows[0]["QtyAvailable"].ToString().ToDecimal() < (decQuantity * 2))
                           //{
                           //    MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9576, out MmessageIcon);
                           //    ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                           //    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                           //    lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                           //    TmrPOS.Enabled = true;
                           //    dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["Quantity"];
                           //    //dgvPOS.Rows[intICounter].Cells["Quantity"].Value = datQtyAvailable.Rows[0]["QtyAvailable"].ToString().ToDecimal();
                           //    //dgvPOS.BeginEdit(true);
                           //    return;
                           //}
                           if (decAvailQuantity < (decQuantity * 2))
                           {
                               MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9576, out MmessageIcon);
                               ErrSales.SetError(dgvPOS, MstrCommonMessage.Replace("#", "").Trim());
                               MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                               lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                               TmrPOS.Enabled = true;
                               dgvPOS.CurrentCell = dgvPOS.Rows[intICounter].Cells["Quantity"];
                               //dgvPOS.Rows[intICounter].Cells["Quantity"].Value = datQtyAvailable.Rows[0]["QtyAvailable"].ToString().ToDecimal();
                               //dgvPOS.BeginEdit(true);
                               return;
                           }
                       }
                   }




                   MobjclsBLLPOS.clsDTOPOS.intStatusID = (Int32)OperationStatusType.SPOSDeliverd;
                   MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 34, out MmessageIcon).Replace("#", "").Trim();
                   if (MessageBox.Show(MstrCommonMessage.Replace("***", "POS"), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                       DialogResult.No)
                   {
                       EditMode();
                       return;
                   }
                   MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9020, out MmessageIcon);
                   // BindingNavigatorCancelItem.Text = "Cancel";
               }
               else
               {

                   MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 33, out MmessageIcon).Replace("#", "").Trim();
                   if (MessageBox.Show(MstrCommonMessage.Replace("***", "POS"), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                       DialogResult.No)
                   {
                       EditMode();
                       return;
                   }
                   fillMasterParameters();
                   MobjclsBLLPOS.clsDTOPOS.intStatusID = (Int32)OperationStatusType.SPOSCancelled;
                   using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                   {
                       objFrmCancellationDetails.ShowDialog();
                       if (objFrmCancellationDetails.PBlnIsCanelled)
                       {
                           EditMode();
                           return;
                       }
                       MobjclsBLLPOS.clsDTOPOS.strRemarks = objFrmCancellationDetails.PStrDescription;
                   }
             
                   MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 9019, out MmessageIcon);
                   //BindingNavigatorCancelItem.Text = "ReOpen";

               }
               MobjclsBLLPOS.clsDTOPOS.intOperationModID = (int)OperationType.POS;//Fill POS OperationModeID
               MobjclsBLLPOS.ChangePOSStatus();
               MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
               lblPOSstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
               TmrPOS.Enabled = true;
               AddMode();
           }
           catch (Exception ex)
           {
               if (ClsCommonSettings.ShowErrorMess)
                   MessageBox.Show("Error in BindingNavigatorCancelItem_Click() " + ex.Message);
               MobjLogs.WriteLog("Error in BindingNavigatorCancelItem_Click() " + ex.Message, 2);
           }
       }

       private void TmrPOS_Tick(object sender, EventArgs e)
       {
           TmrPOS.Enabled = false;
           lblPOSstatus.Text = "";
           ErrSales.Clear();
       }

       private void tmrMessage_Tick(object sender, EventArgs e)
       
       {
           try
           {
               SendKeys.Send("{ENTER}");
               tmrMessage.Enabled = false;
           }
           catch (Exception ex)
           {
               if (ClsCommonSettings.ShowErrorMess)
                   MessageBox.Show("Error in tmrMessage_Tick() " + ex.Message);
               MobjLogs.WriteLog("Error in tmrMessage_Tick() " + ex.Message, 2);
           }
       }

       private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
       {
           cboCompany.DroppedDown = false;
       }

       private void cboCounter_KeyPress(object sender, KeyPressEventArgs e)
       {
           cboCounter.DroppedDown = false;
       }

       private void cboCustomer_KeyPress(object sender, KeyPressEventArgs e)
       {
           cboCustomer.DroppedDown = false;
       }

       private void cboWarehouse_KeyPress(object sender, KeyPressEventArgs e)
       {
           cboWarehouse.DroppedDown = false;
       }

       private void cboPaymentMode_KeyPress(object sender, KeyPressEventArgs e)
       {
           cboPaymentMode.DroppedDown = false;
       }

       private void cboDiscountScheme_KeyPress(object sender, KeyPressEventArgs e)
       {
           cboDiscountScheme.DroppedDown = false;
       }

       private void cboWarehouse_SelectedIndexChanged(object sender, EventArgs e)
       {
           try
           {
               dgvPOS.Rows.Clear();
               ChangeStatus();
           }
           catch (Exception ex)
           {
               if (ClsCommonSettings.ShowErrorMess)
                   MessageBox.Show("Error in cboWarehouse_SelectedIndexChanged() " + ex.Message);
               MobjLogs.WriteLog("Error in cboWarehouse_SelectedIndexChanged() " + ex.Message, 2);
           }
       }

       private void txtNetAmount_KeyPress(object sender, KeyPressEventArgs e)
       {
           try
           {
               if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
               {
                   e.Handled = true;
               }
               if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
               {
                   e.Handled = true;
               }
           }
           catch (Exception ex)
           {
               if (ClsCommonSettings.ShowErrorMess)
                   MessageBox.Show("Error in txtNetAmount_KeyPress() " + ex.Message);
               MobjLogs.WriteLog("Error in txtNetAmount_KeyPress() " + ex.Message, 2);
           }
       }

       private void txtTendered_KeyPress(object sender, KeyPressEventArgs e)
       {
           try
           {
               if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
               {
                   e.Handled = true;
               }
               if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
               {
                   e.Handled = true;
               }
           }
           catch (Exception ex)
           {
               if (ClsCommonSettings.ShowErrorMess)
                   MessageBox.Show("Error in txtTendered_KeyPress() " + ex.Message);
               MobjLogs.WriteLog("Error in txtTendered_KeyPress() " + ex.Message, 2);
           }
       }

       private void cboDiscountScheme_Enter(object sender, EventArgs e)
       {
           if (!ValidateForm(0))
           {
               return;
           }
       }

       private void txtSubTotal_TextChanged(object sender, EventArgs e)
       {
           LoadCombos(4);
       }

       private void cboCustomer_SelectedIndexChanged(object sender, EventArgs e)
       {
           try
           {
               //ClearControls();
               ChangeStatus();
               LoadCombos(4);
               // FillDiscountColumn();
               CalculateTotalAmt();
           }
           catch (Exception ex)
           {
               if (ClsCommonSettings.ShowErrorMess)
                   MessageBox.Show("Error in cboCustomer_SelectedIndexChanged() " + ex.Message);
               MobjLogs.WriteLog("Error in cboCustomer_SelectedIndexChanged() " + ex.Message, 2);
           }
       }

       private void BtnPrint_Click(object sender, EventArgs e)
       {
           try
           {
               FrmReportviewer ObjViewer = new FrmReportviewer();
               ObjViewer.PsFormName = this.Text;
               ObjViewer.PiRecId = MobjclsBLLPOS.clsDTOPOS.intPOSID;
               ObjViewer.PiFormID = (int)FormID.PointOfSales;
               ObjViewer.ShowDialog();
           }
           catch (Exception ex)
           {
               if (ClsCommonSettings.ShowErrorMess)
                   MessageBox.Show("Error in BtnPrint_Click() " + ex.Message);
               MobjLogs.WriteLog("Error in BtnPrint_Click() " + ex.Message, 2);
           }
       }

       private void BtnEmail_Click(object sender, EventArgs e)
       {
           try
           {
               using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
               {
                   ObjEmailPopUp.MsSubject = "Point of Sale";
                   ObjEmailPopUp.EmailFormType = EmailFormID.PointofSale;
                   ObjEmailPopUp.EmailSource = MobjclsBLLPOS.GetEmailPOSinfo();
                   ObjEmailPopUp.ShowDialog();
               }
           }
           catch (Exception ex)
           {
               if (ClsCommonSettings.ShowErrorMess)
                   MessageBox.Show("Error in BtnEmail_Click() " + ex.Message);
               MobjLogs.WriteLog("Error in BtnEmail_Click() " + ex.Message, 2);
           }
       }

       private void cboCounter_SelectedIndexChanged(object sender, EventArgs e)
       {
           ChangeStatus();
       }

       private void txtBillNumber_TextChanged(object sender, EventArgs e)
       {
           ChangeStatus();
       }

       private void dtpPOSDate_ValueChanged(object sender, EventArgs e)
       {
           ChangeStatus();
       }

       private void chkDelivered_CheckedChanged(object sender, EventArgs e)
       {
           ChangeStatus();
       }

       private void dtpDueDate_ValueChanged(object sender, EventArgs e)
       {
           ChangeStatus();
       }

       private void cboPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
       {    
           ChangeStatus();

       }

       private void txtRemarks_TextChanged(object sender, EventArgs e)
       {
           ChangeStatus();
       }

       private void txtExpense_TextChanged(object sender, EventArgs e)
       {
           ChangeStatus();
       }

       private void txtDiscountAmount_TextChanged(object sender, EventArgs e)
       {
           
           try
           {
               ChangeStatus();
               CalculateNetTotal();
           }
           catch (Exception ex)
           {
               if (ClsCommonSettings.ShowErrorMess)
                   MessageBox.Show("Error in cboDiscountScheme_SelectedIndexChanged() " + ex.Message);
               MobjLogs.WriteLog("Error in cboDiscountScheme_SelectedIndexChanged() " + ex.Message, 2);
           }
       }

       private void FrmPOS_FormClosing(object sender, FormClosingEventArgs e)
       {
           if (e.CloseReason != CloseReason.MdiFormClosing)
           {
               try
               {
                   if (btnSave.Enabled == true)
                   {
                       MstrCommonMessage = mObjNotification.GetErrorMessage(datMessages, 4269, out MmessageIcon);
                       if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                               DialogResult.No)
                       {
                           e.Cancel = true;
                       }
                   }
               }
               catch (Exception ex)
               {
                   if (ClsCommonSettings.ShowErrorMess)
                       MessageBox.Show("Error in FrmPOS_FormClosing() " + ex.Message);
                   MobjLogs.WriteLog("Error in FrmPOS_FormClosing() " + ex.Message, 2);
               }
           }
       }

       private void dgvPOS_CurrentCellDirtyStateChanged(object sender, EventArgs e)
       {
           try
           {
               if (dgvPOS.IsCurrentCellDirty)
               {
                   if (dgvPOS.CurrentCell != null)
                       dgvPOS.CommitEdit(DataGridViewDataErrorContexts.Commit);
               }
           }
           catch (Exception ex)
           {
               if (ClsCommonSettings.ShowErrorMess)
                   MessageBox.Show("Error in dgvPOS_CurrentCellDirtyStateChanged() " + ex.Message);
               MobjLogs.WriteLog("Error in dgvPOS_CurrentCellDirtyStateChanged() " + ex.Message, 2);
           }

       }

       private void btnCounterReference_Click(object sender, EventArgs e)
       {
           try
           {

               FrmCommonRef objCommon = new FrmCommonRef("Counter", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "SalesCounterID,SalesCounterName As Counter", "InvSalesCounterReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intCounter = Convert.ToInt32(cboCounter.SelectedValue);
                LoadCombos(7);
                if (objCommon.NewID != 0)
                {
                    cboCounter.SelectedValue = objCommon.NewID;
                }
                else
                {
                    cboCounter.SelectedValue = intCounter;
                    if (intCounter == 0)
                    {
                        cboCounter.Text = "";
                        //cboCounter.DataSource = null;
                    }
                }

            }
           catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnCounterReference_Click() " + ex.Message);
                MobjLogs.WriteLog("Error in btnCounterReference_Click() " + ex.Message, 2);
            }
       }

       private void txtCancelReason_TextChanged(object sender, EventArgs e)
       {
           ChangeStatus();
       }

       private void DgvBillNo_CurrentCellDirtyStateChanged(object sender, EventArgs e)
       {
           try
           {
           }
           catch { }
       }

       private void cboCounterSearch_SelectedIndexChanged(object sender, EventArgs e)
       {
           
       }

       private void cboCompanySearch_SelectedIndexChanged(object sender, EventArgs e)
       {
           DataTable datCombos = new DataTable();
           //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
           //{
               //datCombos = MobjclsBLLPOS.FillCombos(new string[] { "EM.EmployeeID,isnull(EM.[EmployeeFullName],UM.[UserName]) as [EmployeeFullName]", "[UserMaster]  UM left join [EmployeeMaster] EM on EM.[EmployeeId]=UM.[EmployeeId]", "UM.UserID in(Select CreatedBy from STPOSMaster)" });
               if (cboCompanySearch.SelectedValue == null || Convert.ToInt32(cboCompanySearch.SelectedValue) == -2)
               {
                   DataTable datCompany = (DataTable)cboCompanySearch.DataSource;
                   string strFilterCondition = "";
                   if (datCompany.Rows.Count > 0)
                   {
                       strFilterCondition = "CompanyID In (";
                       foreach (DataRow dr in datCompany.Rows)
                           strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                       strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                       strFilterCondition += ")";
                   }
                   datCombos = MobjclsBLLPOS.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
                        " ON EM.EmployeeID=UM.EmployeeID INNER JOIN InvPOSMaster IM ON UM.UserID=IM.CreatedBy", "IM." + strFilterCondition });
               }
               else
               {
                   //datCombos = MobjclsBLLPOS.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
                   //     " ON EM.EmployeeID=UM.EmployeeID INNER JOIN InvPOSMaster IM ON UM.UserID=IM.CreatedBy", "IM.CompanyID = " + Convert.ToInt32(cboCompanySearch.SelectedValue) });
                   datCombos = MobjclsBLLPOS.FillCombos(new string[] {  "UM.UserID,IsNull(EM.FirstName,UM.UserName) as UserName", "UserMaster UM Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID", "EM.CompanyID = " + Convert.ToInt32(cboCompanySearch.SelectedValue) });

               }
           //}
           //else
           //{
           //    try
           //    {
           //        DataTable datTemp = MobjclsBLLPOS.FillCombos(new string[] { "IsVisible", "STRoleFieldsDetails", "" + 
           //             "RoleID=" + ClsCommonSettings.RoleID + " AND CompanyID=" + ClsCommonSettings.CompanyID + " " +
           //             "AND ControlID=" + (int)ControlOperationType.POSEmployee + " AND IsVisible=1" });
           //        if (datTemp != null)
           //            if (datTemp.Rows.Count > 0)
           //            {
           //                datCombos = MobjclsBLLPOS.GetEmployeeByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.POSCompany);
           //            }
           //        if (datCombos == null)
           //            datCombos = MobjclsBLLPOS.FillCombos(new string[] { "DISTINCT EM.EmployeeID,isnull(EM.[EmployeeFullName],UM.[UserName]) as [EmployeeFullName]","EmployeeMaster EM INNER JOIN UserMaster UM " +
           //                             " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
           //        else
           //        {
           //            if (datCombos.Rows.Count == 0)
           //                datCombos = MobjclsBLLPOS.FillCombos(new string[] { "DISTINCT EM.EmployeeID,isnull(EM.[EmployeeFullName],UM.[UserName]) as [EmployeeFullName]","EmployeeMaster EM INNER JOIN UserMaster UM " +
           //                             " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
           //        }
           //    }
           //    catch { }
           //}
           cboCashierSearch.ValueMember = "EmployeeID";
           cboCashierSearch.DisplayMember = "EmployeeFullName";
           cboCashierSearch.DataSource = datCombos;
       }

       private void lnkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
       {
           DataTable datAdvanceSearchedData= new DataTable();
           using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.POS))
           {
               objFrmSearchForm.ShowDialog();
               if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
               {
                   datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                   datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                   datAdvanceSearchedData.Columns["ID"].ColumnName = "POSID";
                   DgvBillNo.DataSource = datAdvanceSearchedData;
                   DgvBillNo.Columns["POSID"].Visible = false;
                   DgvBillNo.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
               }
           }
       }

       private void DgvBillNo_KeyPress(object sender, KeyPressEventArgs e)
       {
           if (e.KeyChar == (int)Keys.Space)
           {
               DgvBillNo_CellClick(null, null);
           }
       }

       private void txtDiscountAmount_KeyPress(object sender, KeyPressEventArgs e)
       {
           try
           {
               if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
               {
                   e.Handled = true;
               }
               if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
               {
                   e.Handled = true;
               }
           }
           catch (Exception ex)
           {
               if (ClsCommonSettings.ShowErrorMess)
                   MessageBox.Show("Error in txtDiscountAmount_KeyPress() " + ex.Message);
               MobjLogs.WriteLog("Error in txtDiscountAmount_KeyPress() " + ex.Message, 2);
           }
       }
       private void FillGridDynamically(int intCompanyID, int intItemID, int intBatchID, decimal decQuantity)
       {
           try
           {
               DataTable datItemBarcode = MobjclsBLLPOS.GetItemForBarCode(intCompanyID, intItemID, intBatchID);
               for (int intJCounter = 0; intJCounter < dgvPOS.Rows.Count - 1; intJCounter++)
               {
                   if (dgvPOS.Rows[intJCounter].Cells["ItemID"].Value.ToDecimal() == intItemID && dgvPOS.Rows[intJCounter].Cells["BatchID"].Value.ToDecimal() == intBatchID)
                   {
                       dgvPOS.Rows[intJCounter].Cells["Quantity"].Value = dgvPOS.Rows[intJCounter].Cells["Quantity"].Value.ToDecimal() + decQuantity;
                       return;
                   }
               }
               dgvPOS.Rows.Add();
               Int32 intICounter = dgvPOS.Rows.Count - 2;
               dgvPOS.Rows[intICounter].Cells["ItemID"].Value = datItemBarcode.Rows[0]["ItemID"].ToInt32();
               dgvPOS.Rows[intICounter].Cells["ItemCode"].Value = datItemBarcode.Rows[0]["ItemCode"].ToString();
               dgvPOS.Rows[intICounter].Cells["ItemName"].Value = datItemBarcode.Rows[0]["Description"].ToString();
               dgvPOS.Rows[intICounter].Cells["Rate"].Value = datItemBarcode.Rows[0]["Rate"].ToDecimal().ToString("F" + MintExchangeCurrencyScale);
               //dgvPOS.Rows[intICounter].Cells["ExpiryDate"].Value = datItemBarcode.Rows[0]["ExpiryDate"].ToString();
               dgvPOS.Rows[intICounter].Cells["QtyAvailable"].Value = datItemBarcode.Rows[0]["QtyAvailable"].ToString();
               dgvPOS.Rows[intICounter].Cells["BatchNo"].Value = datItemBarcode.Rows[0]["BatchNo"].ToString();
               dgvPOS.Rows[intICounter].Cells["IsGroup"].Value = datItemBarcode.Rows[0]["IsGroup"].ToInt32();
               dgvPOS.Rows[intICounter].Cells["Quantity"].Value = decQuantity;
               //dgvPOS.Rows[intICounter].Cells["Uom"].Tag.ToInt32();
               dgvPOS.Rows[intICounter].Cells["BatchID"].Value = datItemBarcode.Rows[0]["BatchID"].ToInt32();
               //dgvPOS.Rows[intICounter].Cells["Discount"].Tag.ToInt32();
               //dgvPOS.Rows[intICounter].Cells["DiscountAmount"].Value.ToDecimal();
               //dgvPOS.Rows[intICounter].Cells["txtColGrandAmount"].Value.ToDecimal();
               //dgvPOS.Rows[intICounter].Cells["Total"].Value.ToDecimal();

           }
           catch (Exception ex)
           {
               if (ClsCommonSettings.ShowErrorMess)
                   MessageBox.Show("Error in FillGridDynamically() " + ex.Message);
               MobjLogs.WriteLog("Error in FillGridDynamically() " + ex.Message, 2);
           }
       }
       private void fillMastersDynamically(int intCompanyID,int intWareHouseID, int intCustomerID, int intCounterID, int intPaymentMode)
       {
            cboCompany.SelectedValue=intCompanyID;
            //cboCounter.SelectedValue=intCounterID;
            cboCounter.SelectedIndex = 0;
            //cboWarehouse.SelectedValue=intWareHouseID;
            cboWarehouse.SelectedIndex = 0;
            //cboPaymentMode.SelectedValue=intPaymentMode;
            cboPaymentMode.SelectedIndex = 0;
            cboCustomer.SelectedValue=intCustomerID;
       }

       private void ShowItemHistory_Click(object sender, EventArgs e)
       {
           using (FrmCustomerSaleHistory objFrmCustomerSaleHistory = new FrmCustomerSaleHistory())
           {
               objFrmCustomerSaleHistory.intCompanyID = this.cboCompany.SelectedValue.ToInt32();
               objFrmCustomerSaleHistory.intCustomerID = this.cboCustomer.SelectedValue.ToInt32();
               objFrmCustomerSaleHistory.intItemID = SItemID;
               objFrmCustomerSaleHistory.intIsGroup = SIsGroup;
               objFrmCustomerSaleHistory.ShowDialog();
           }
       }

       private void dgvPOS_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
       {
           if (e.RowIndex >= 0)
           {
               int intCurrentColumn = dgvPOS.CurrentCell.ColumnIndex;

               if (this.cboCompany.SelectedIndex >= 0 && cboCustomer.SelectedValue.ToInt32() > 0)
               {
                   if (intCurrentColumn == 0 || intCurrentColumn == 1)
                   {
                       if (e.Button == MouseButtons.Right)
                       {
                           if (dgvPOS.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt32() > 0)
                           {
                               SItemID = dgvPOS.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt32();
                               SIsGroup = Convert.ToBoolean(dgvPOS.Rows[e.RowIndex].Cells["IsGroup"].Value) == true ? 1 : 0;
                               CntxtHistory.Items["showGroupItemDetails"].Visible = SIsGroup == 1 ? true : false;
                               this.CntxtHistory.Show(this.dgvPOS, this.dgvPOS.PointToClient(Cursor.Position));
                           }
                       }
                   }
               }
           }
       }

       private void ShowSaleRateHistory_Click(object sender, EventArgs e)
       {
           using (FrmSaleRateHistory objFrmSaleRateHistory = new FrmSaleRateHistory())
           {
               objFrmSaleRateHistory.ItemID = SItemID;
               objFrmSaleRateHistory.CompanyID = this.cboCompany.SelectedValue.ToInt32();
               objFrmSaleRateHistory.ShowDialog();
           }
       }

       private void showGroupItemDetails_Click(object sender, EventArgs e)
       {

           frmItemGroupMaster objfrmItemGroupMaster = new frmItemGroupMaster();
           objfrmItemGroupMaster.PintProductGroupID = dgvPOS.Rows[dgvPOS.CurrentRow.Index].Cells["ItemID"].Value.ToInt32();
           objfrmItemGroupMaster.ShowDialog();
       }

      
    }
}
