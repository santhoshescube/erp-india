using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel; 
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data;
using System.Data.SqlClient;
 
using System.Collections.Generic;


using System.Text;

namespace MyBooksERP
{
    /// <summary>
    /// Summary description for frmDocument.
    /// </summary>
    public class FrmDebitNote : DevComponents.DotNetBar.Office2007Form
    {
        #region Controls Declaration
        public Command CommandZoom;
        private DockContainerItem dockContainerItem1;
        private ExpandableSplitter expandableSplitterLeft;
        private PanelEx PanelLeft;
        private IContainer components;
        private clsBLLCommonUtility MobjclsBLLCommonUtility;

        private DotNetBarManager dotNetBarManager1;
        private DockSite dockSite4;
        private DockSite dockSite1;
        private DockSite dockSite2;
        private DockSite dockSite3;
        private DockSite dockSite5;
        private DockSite dockSite6;
        private Bar bar1;
        private DockSite dockSite8;
        private PanelEx panelEx1;
        private Bar PurchaseOrderBindingNavigator;
        private ButtonItem BindingNavigatorAddNewItem;
        private ExpandableSplitter expandableSplitterTop;
        private ExpandablePanel expandablePanel1;
        private LabelX lblPurchaseReturnCount;
        private PanelEx panelBottom;
        private PanelEx panelLeftTop;
        private Label LblScompany;
        private Label LblSCustomer;
        private Label LblSStatus;
        private ClsInnerGridBar dgvPurchase;
        private ButtonItem BindingNavigatorSaveItem;
        private ButtonItem BindingNavigatorClearItem;
        private ButtonItem BindingNavigatorDeleteItem;
        private ButtonX BtnSRefresh;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtSsearch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboSStatus;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboSSupplier;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboSCompany;
        internal Timer TmrPurchaseReturn;
        internal Timer TmrFocus;
        internal ErrorProvider ErrPurchaseReturn;
        private ButtonItem BtnPrint;
        private ButtonItem BtnEmail;
        private ImageList ImgPurchaseReturn;

        //private bool MbViewPermission = false;
        private bool MbAddPermission = false;
        private bool MbUpdatePermission = false;
        private bool MbDeletePermission = false;
        private bool MbCancelPermission = false;
        private bool MbPrintEmailPermission = false;
        //private bool MBlnIsFromCancellation = false;
        private bool MBlnEditMode = false;
        private bool blnCalculate = true;
        DataTable dtItemDetails = new DataTable();
        private string MstrMessageCommon;
        private bool MBlnCanShow=true;

        private DataTable datMessages;                  // Error Message display
        private DataTable MaStatusMessage;

        private MessageBoxIcon MmessageIcon;

        ClsLogWriter MobjClsLogWriter;
        clsBLLPurchaseReturnMaster MobjclsBLLPurchaseReturn;
        //clsBLLPermissionSettings objClsBLLPermissionSettings;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private DataGridViewTextBoxColumn clmItemCode;
        private DataGridViewTextBoxColumn clmItemName;
        private DataGridViewTextBoxColumn clmQuantity;
        private DataGridViewTextBoxColumn clmQtyReceived;
        private DataGridViewTextBoxColumn clmBatchNumber;
        private DataGridViewTextBoxColumn clmRate;
        private DataGridViewTextBoxColumn clmTotal;
        private DataGridViewTextBoxColumn clmStatus;
        private DataGridViewTextBoxColumn clmOrderDetailID;
        private DataGridViewTextBoxColumn clmSalesOrderID;
        private DataGridViewTextBoxColumn clmItemID;
        ClsNotificationNew MObjClsNotification;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSExecutive;
        private Label lblExecutive;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private DateTimePicker dtpSTo;
        private DateTimePicker dtpSFrom;
        private Label lblTo;
        private Label lblFrom;
        private Label lblPurchaseReturnNo;
        private DevComponents.DotNetBar.Controls.DataGridViewX DgvSOrderDisplay;
        private ButtonItem btnSupplierHistory;
        private Panel pnlBottom;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedByText;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedDateValue;
        private DevComponents.DotNetBar.Controls.WarningBox lblPurchaseReturnStatus;
        private PanelEx panelTop;
        private Label lblCustomer;
        private Label lblVendorAddress;
        private Label lblStatusLabel;
        private Label lblCurrency;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboInvoiceNo;
        private Label lblInvoiceNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCurrency;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private Label lblCompany;
        private Label lblDescription;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDescription;
        private DevComponents.DotNetBar.Controls.TextBoxX txtReturnNo;
        public DevComponents.DotNetBar.Controls.TextBoxX txtVendorAddress;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSupplier;
        private Label lblStatus;
        private Label lblDate;
        private Label lblReturnNo;
        private DevComponents.DotNetBar.TabControl tcPurchaseReturn;
        private TabControlPanel tabControlPanel2;
        private TabItem tiLocationDetails;
        private TabControlPanel tabControlPanel1;
        private TabItem tiItemDetails;
        private ClsInnerGridBar dgvLocationDetails;
        private ButtonItem BindingNavigatorCancelItem;

        int MintBaseCurrencyScale = 2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboWarehouse;
        private ButtonItem btnPickList;
        private ButtonItem btnPickListEmail;
        private DevComponents.DotNetBar.TabControl tcGeneral;
        private TabControlPanel tabControlPanel3;
        private TabItem tiGeneral;
        private TabItem tiSuggestions;
        private PanelEx panelGridBottom;
        private Label lblAmountInWords;
        private Label lblAmountIn;
        private PanelEx panelEx2;
        private Label lblCompanyCurrencyAmount;
        private Label lblGrandReturnAmount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGrandReturnAmount;
        private Label lblTotalAmount;
        private Label lblSubtotal;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSubtotal;
        private DevComponents.DotNetBar.Controls.TextBoxX txtExchangeCurrencyRate;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTotalAmount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtRemarks;
        private Label lblPaymentRemarks;
        private DataGridViewTextBoxColumn LItemCode;
        private DataGridViewTextBoxColumn LItemName;
        private DataGridViewTextBoxColumn LItemID;
        private DataGridViewTextBoxColumn LBatchID;
        private DataGridViewTextBoxColumn LBatchNo;
        private DataGridViewTextBoxColumn LQuantity;
        private DataGridViewComboBoxColumn LUOM;
        private DataGridViewTextBoxColumn LUOMID;
        private DataGridViewComboBoxColumn LLocation;
        private DataGridViewComboBoxColumn LRow;
        private DataGridViewComboBoxColumn LBlock;
        private DataGridViewComboBoxColumn LLot;
        private LinkLabel lnkLabel;
        private ButtonX btnAccount;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboAccount;
        private Label lblAccount;
        private DateTimePicker dtpReturnDate;
        int MintExchangeCurrencyScale = 2;
        private Label lblDiscount;
        private LabelX lblDiscountAmount;
        private LabelX lblExpenseAmount;
        private Label label1;
        private DataGridViewTextBoxColumn ItemCode;
        private DataGridViewTextBoxColumn ItemName;
        private DataGridViewTextBoxColumn ItemID;
        private DataGridViewTextBoxColumn ReferenceSerialNo;
        private DataGridViewTextBoxColumn BatchID;
        private DataGridViewTextBoxColumn BatchNo;
        private DataGridViewTextBoxColumn InvoicedQuantity;
        private DataGridViewTextBoxColumn ReceivedQty;
        private DataGridViewTextBoxColumn ExtraQuantity;
        private DataGridViewTextBoxColumn ReturnedQty;
        private DataGridViewTextBoxColumn TempExtraQty;
        private DataGridViewTextBoxColumn InvQty;
        private DataGridViewTextBoxColumn InvUomID;
        private DataGridViewTextBoxColumn InvRate;
        private DataGridViewTextBoxColumn PurchaseRate;
        private DataGridViewTextBoxColumn ReturnQuantity;
        private DataGridViewComboBoxColumn Uom;
        private DataGridViewTextBoxColumn Rate;
        private DataGridViewTextBoxColumn GrandAmount;
        private DataGridViewComboBoxColumn Discount;
        private DataGridViewTextBoxColumn DiscountAmount;
        private DataGridViewTextBoxColumn NetAmount;
        private DataGridViewTextBoxColumn ReturnAmount;
        private DataGridViewComboBoxColumn Reason;
        private Label label2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboTaxScheme;
        private LabelX lblTaxAmount;
        private Label label3;

        public long PlngReferenceID;

        #endregion

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDebitNote));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            this.CommandZoom = new DevComponents.DotNetBar.Command(this.components);
            this.dockContainerItem1 = new DevComponents.DotNetBar.DockContainerItem();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.DgvSOrderDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelLeftTop = new DevComponents.DotNetBar.PanelEx();
            this.lnkLabel = new System.Windows.Forms.LinkLabel();
            this.TxtSsearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblPurchaseReturnNo = new System.Windows.Forms.Label();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cboSExecutive = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblExecutive = new System.Windows.Forms.Label();
            this.BtnSRefresh = new DevComponents.DotNetBar.ButtonX();
            this.CboSStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboSSupplier = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboSCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.LblScompany = new System.Windows.Forms.Label();
            this.LblSCustomer = new System.Windows.Forms.Label();
            this.LblSStatus = new System.Windows.Forms.Label();
            this.lblPurchaseReturnCount = new DevComponents.DotNetBar.LabelX();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.dotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.dockSite4 = new DevComponents.DotNetBar.DockSite();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.dockSite2 = new DevComponents.DotNetBar.DockSite();
            this.dockSite8 = new DevComponents.DotNetBar.DockSite();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.dockSite6 = new DevComponents.DotNetBar.DockSite();
            this.dockSite3 = new DevComponents.DotNetBar.DockSite();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.lblCreatedByText = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblCreatedDateValue = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblPurchaseReturnStatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.panelBottom = new DevComponents.DotNetBar.PanelEx();
            this.tcPurchaseReturn = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvPurchase = new ClsInnerGridBar();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReferenceSerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvoicedQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceivedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExtraQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TempExtraQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvUomID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PurchaseRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Uom = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GrandAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Discount = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.DiscountAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Reason = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tiItemDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvLocationDetails = new ClsInnerGridBar();
            this.LItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LBatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LBatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LUOM = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LUOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LLocation = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LRow = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LBlock = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LLot = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cboWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.tiLocationDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.panelGridBottom = new DevComponents.DotNetBar.PanelEx();
            this.btnAccount = new DevComponents.DotNetBar.ButtonX();
            this.cboAccount = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblAccount = new System.Windows.Forms.Label();
            this.lblAmountIn = new System.Windows.Forms.Label();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.lblTaxAmount = new DevComponents.DotNetBar.LabelX();
            this.label3 = new System.Windows.Forms.Label();
            this.lblExpenseAmount = new DevComponents.DotNetBar.LabelX();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDiscountAmount = new DevComponents.DotNetBar.LabelX();
            this.lblDiscount = new System.Windows.Forms.Label();
            this.lblCompanyCurrencyAmount = new System.Windows.Forms.Label();
            this.lblGrandReturnAmount = new System.Windows.Forms.Label();
            this.txtGrandReturnAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.lblSubtotal = new System.Windows.Forms.Label();
            this.txtSubtotal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtExchangeCurrencyRate = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtTotalAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtRemarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblPaymentRemarks = new System.Windows.Forms.Label();
            this.lblAmountInWords = new System.Windows.Forms.Label();
            this.expandableSplitterTop = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.tcGeneral = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.cboTaxScheme = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.dtpReturnDate = new System.Windows.Forms.DateTimePicker();
            this.lblCompany = new System.Windows.Forms.Label();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.lblReturnNo = new System.Windows.Forms.Label();
            this.lblVendorAddress = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblStatusLabel = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.cboInvoiceNo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboSupplier = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblInvoiceNo = new System.Windows.Forms.Label();
            this.txtVendorAddress = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboCurrency = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtReturnNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtDescription = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblDescription = new System.Windows.Forms.Label();
            this.tiGeneral = new DevComponents.DotNetBar.TabItem(this.components);
            this.PurchaseOrderBindingNavigator = new DevComponents.DotNetBar.Bar();
            this.BindingNavigatorAddNewItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorSaveItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorClearItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorDeleteItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorCancelItem = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.BtnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.btnPickList = new DevComponents.DotNetBar.ButtonItem();
            this.btnPickListEmail = new DevComponents.DotNetBar.ButtonItem();
            this.btnSupplierHistory = new DevComponents.DotNetBar.ButtonItem();
            this.tiSuggestions = new DevComponents.DotNetBar.TabItem(this.components);
            this.TmrPurchaseReturn = new System.Windows.Forms.Timer(this.components);
            this.TmrFocus = new System.Windows.Forms.Timer(this.components);
            this.ErrPurchaseReturn = new System.Windows.Forms.ErrorProvider(this.components);
            this.ImgPurchaseReturn = new System.Windows.Forms.ImageList(this.components);
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmQtyReceived = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmBatchNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmOrderDetailID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmSalesOrderID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvSOrderDisplay)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.panelEx1.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.panelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcPurchaseReturn)).BeginInit();
            this.tcPurchaseReturn.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchase)).BeginInit();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocationDetails)).BeginInit();
            this.panelGridBottom.SuspendLayout();
            this.panelEx2.SuspendLayout();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).BeginInit();
            this.tcGeneral.SuspendLayout();
            this.tabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrPurchaseReturn)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandZoom
            // 
            this.CommandZoom.Name = "CommandZoom";
            // 
            // dockContainerItem1
            // 
            this.dockContainerItem1.Name = "dockContainerItem1";
            this.dockContainerItem1.Text = "dockContainerItem1";
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.DgvSOrderDisplay);
            this.PanelLeft.Controls.Add(this.expandablePanel1);
            this.PanelLeft.Controls.Add(this.lblPurchaseReturnCount);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(200, 544);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 9;
            this.PanelLeft.Text = "panelEx1";
            this.PanelLeft.Visible = false;
            // 
            // DgvSOrderDisplay
            // 
            this.DgvSOrderDisplay.AllowUserToAddRows = false;
            this.DgvSOrderDisplay.AllowUserToDeleteRows = false;
            this.DgvSOrderDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvSOrderDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DgvSOrderDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvSOrderDisplay.DefaultCellStyle = dataGridViewCellStyle2;
            this.DgvSOrderDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvSOrderDisplay.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DgvSOrderDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvSOrderDisplay.Location = new System.Drawing.Point(0, 225);
            this.DgvSOrderDisplay.Name = "DgvSOrderDisplay";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvSOrderDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DgvSOrderDisplay.RowHeadersVisible = false;
            this.DgvSOrderDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvSOrderDisplay.Size = new System.Drawing.Size(200, 293);
            this.DgvSOrderDisplay.TabIndex = 28;
            this.DgvSOrderDisplay.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvSOrderDisplay_CellDoubleClick);
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expandablePanel1.Controls.Add(this.panelLeftTop);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(200, 225);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 111;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Search";
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelLeftTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelLeftTop.Controls.Add(this.lnkLabel);
            this.panelLeftTop.Controls.Add(this.TxtSsearch);
            this.panelLeftTop.Controls.Add(this.lblPurchaseReturnNo);
            this.panelLeftTop.Controls.Add(this.dtpSTo);
            this.panelLeftTop.Controls.Add(this.dtpSFrom);
            this.panelLeftTop.Controls.Add(this.lblTo);
            this.panelLeftTop.Controls.Add(this.lblFrom);
            this.panelLeftTop.Controls.Add(this.cboSExecutive);
            this.panelLeftTop.Controls.Add(this.lblExecutive);
            this.panelLeftTop.Controls.Add(this.BtnSRefresh);
            this.panelLeftTop.Controls.Add(this.CboSStatus);
            this.panelLeftTop.Controls.Add(this.CboSSupplier);
            this.panelLeftTop.Controls.Add(this.CboSCompany);
            this.panelLeftTop.Controls.Add(this.LblScompany);
            this.panelLeftTop.Controls.Add(this.LblSCustomer);
            this.panelLeftTop.Controls.Add(this.LblSStatus);
            this.panelLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftTop.Location = new System.Drawing.Point(0, 26);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(200, 198);
            this.panelLeftTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelLeftTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelLeftTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelLeftTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelLeftTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelLeftTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelLeftTop.Style.GradientAngle = 90;
            this.panelLeftTop.TabIndex = 20;
            // 
            // lnkLabel
            // 
            this.lnkLabel.AutoSize = true;
            this.lnkLabel.Location = new System.Drawing.Point(3, 178);
            this.lnkLabel.Name = "lnkLabel";
            this.lnkLabel.Size = new System.Drawing.Size(87, 13);
            this.lnkLabel.TabIndex = 261;
            this.lnkLabel.TabStop = true;
            this.lnkLabel.Text = "Advance Search";
            this.lnkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLabel_LinkClicked);
            // 
            // TxtSsearch
            // 
            this.TxtSsearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.TxtSsearch.Border.Class = "TextBoxBorder";
            this.TxtSsearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtSsearch.Location = new System.Drawing.Point(58, 143);
            this.TxtSsearch.MaxLength = 20;
            this.TxtSsearch.Name = "TxtSsearch";
            this.TxtSsearch.Size = new System.Drawing.Size(136, 20);
            this.TxtSsearch.TabIndex = 27;
            this.TxtSsearch.WatermarkText = "Purchase Return No";
            // 
            // lblPurchaseReturnNo
            // 
            this.lblPurchaseReturnNo.AutoSize = true;
            this.lblPurchaseReturnNo.Location = new System.Drawing.Point(2, 147);
            this.lblPurchaseReturnNo.Name = "lblPurchaseReturnNo";
            this.lblPurchaseReturnNo.Size = new System.Drawing.Size(56, 13);
            this.lblPurchaseReturnNo.TabIndex = 260;
            this.lblPurchaseReturnNo.Text = "Return No";
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(58, 121);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.Size = new System.Drawing.Size(136, 20);
            this.dtpSTo.TabIndex = 26;
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(58, 99);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.Size = new System.Drawing.Size(136, 20);
            this.dtpSFrom.TabIndex = 25;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(2, 122);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 257;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(2, 99);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 256;
            this.lblFrom.Text = "From";
            // 
            // cboSExecutive
            // 
            this.cboSExecutive.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSExecutive.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSExecutive.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSExecutive.DisplayMember = "Text";
            this.cboSExecutive.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSExecutive.DropDownHeight = 75;
            this.cboSExecutive.FormattingEnabled = true;
            this.cboSExecutive.IntegralHeight = false;
            this.cboSExecutive.ItemHeight = 14;
            this.cboSExecutive.Location = new System.Drawing.Point(58, 33);
            this.cboSExecutive.Name = "cboSExecutive";
            this.cboSExecutive.Size = new System.Drawing.Size(136, 20);
            this.cboSExecutive.TabIndex = 22;
            // 
            // lblExecutive
            // 
            this.lblExecutive.AutoSize = true;
            this.lblExecutive.Location = new System.Drawing.Point(2, 37);
            this.lblExecutive.Name = "lblExecutive";
            this.lblExecutive.Size = new System.Drawing.Size(54, 13);
            this.lblExecutive.TabIndex = 126;
            this.lblExecutive.Text = "Executive";
            // 
            // BtnSRefresh
            // 
            this.BtnSRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnSRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BtnSRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnSRefresh.Location = new System.Drawing.Point(123, 168);
            this.BtnSRefresh.Name = "BtnSRefresh";
            this.BtnSRefresh.Size = new System.Drawing.Size(71, 23);
            this.BtnSRefresh.TabIndex = 28;
            this.BtnSRefresh.Text = "Refresh";
            this.BtnSRefresh.Click += new System.EventHandler(this.BtnSRefresh_Click);
            // 
            // CboSStatus
            // 
            this.CboSStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CboSStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSStatus.DisplayMember = "Text";
            this.CboSStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboSStatus.DropDownHeight = 75;
            this.CboSStatus.FormattingEnabled = true;
            this.CboSStatus.IntegralHeight = false;
            this.CboSStatus.ItemHeight = 14;
            this.CboSStatus.Location = new System.Drawing.Point(58, 77);
            this.CboSStatus.Name = "CboSStatus";
            this.CboSStatus.Size = new System.Drawing.Size(136, 20);
            this.CboSStatus.TabIndex = 24;
            // 
            // CboSSupplier
            // 
            this.CboSSupplier.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CboSSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSSupplier.DisplayMember = "Text";
            this.CboSSupplier.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboSSupplier.DropDownHeight = 75;
            this.CboSSupplier.FormattingEnabled = true;
            this.CboSSupplier.IntegralHeight = false;
            this.CboSSupplier.ItemHeight = 14;
            this.CboSSupplier.Location = new System.Drawing.Point(58, 55);
            this.CboSSupplier.Name = "CboSSupplier";
            this.CboSSupplier.Size = new System.Drawing.Size(136, 20);
            this.CboSSupplier.TabIndex = 23;
            this.CboSSupplier.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // CboSCompany
            // 
            this.CboSCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CboSCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSCompany.DisplayMember = "Text";
            this.CboSCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboSCompany.DropDownHeight = 75;
            this.CboSCompany.FormattingEnabled = true;
            this.CboSCompany.IntegralHeight = false;
            this.CboSCompany.ItemHeight = 14;
            this.CboSCompany.Location = new System.Drawing.Point(58, 11);
            this.CboSCompany.Name = "CboSCompany";
            this.CboSCompany.Size = new System.Drawing.Size(136, 20);
            this.CboSCompany.TabIndex = 21;
            this.CboSCompany.SelectedIndexChanged += new System.EventHandler(this.CboSCompany_SelectedIndexChanged);
            this.CboSCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // LblScompany
            // 
            this.LblScompany.AutoSize = true;
            this.LblScompany.Location = new System.Drawing.Point(2, 15);
            this.LblScompany.Name = "LblScompany";
            this.LblScompany.Size = new System.Drawing.Size(51, 13);
            this.LblScompany.TabIndex = 109;
            this.LblScompany.Text = "Company";
            // 
            // LblSCustomer
            // 
            this.LblSCustomer.AutoSize = true;
            this.LblSCustomer.Location = new System.Drawing.Point(2, 57);
            this.LblSCustomer.Name = "LblSCustomer";
            this.LblSCustomer.Size = new System.Drawing.Size(45, 13);
            this.LblSCustomer.TabIndex = 108;
            this.LblSCustomer.Text = "Supplier";
            // 
            // LblSStatus
            // 
            this.LblSStatus.AutoSize = true;
            this.LblSStatus.Location = new System.Drawing.Point(2, 76);
            this.LblSStatus.Name = "LblSStatus";
            this.LblSStatus.Size = new System.Drawing.Size(37, 13);
            this.LblSStatus.TabIndex = 107;
            this.LblSStatus.Text = "Status";
            // 
            // lblPurchaseReturnCount
            // 
            // 
            // 
            // 
            this.lblPurchaseReturnCount.BackgroundStyle.Class = "";
            this.lblPurchaseReturnCount.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPurchaseReturnCount.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblPurchaseReturnCount.Location = new System.Drawing.Point(0, 518);
            this.lblPurchaseReturnCount.Name = "lblPurchaseReturnCount";
            this.lblPurchaseReturnCount.Size = new System.Drawing.Size(200, 26);
            this.lblPurchaseReturnCount.TabIndex = 0;
            this.lblPurchaseReturnCount.Text = "...";
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.Expanded = false;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(0, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 544);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 10;
            this.expandableSplitterLeft.TabStop = false;
            this.expandableSplitterLeft.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitterLeft_ExpandedChanged);
            // 
            // dotNetBarManager1
            // 
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.dotNetBarManager1.BottomDockSite = this.dockSite4;
            this.dotNetBarManager1.EnableFullSizeDock = false;
            this.dotNetBarManager1.LeftDockSite = this.dockSite1;
            this.dotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.dotNetBarManager1.ParentForm = this;
            this.dotNetBarManager1.RightDockSite = this.dockSite2;
            this.dotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.dotNetBarManager1.ToolbarBottomDockSite = this.dockSite8;
            this.dotNetBarManager1.ToolbarLeftDockSite = this.dockSite5;
            this.dotNetBarManager1.ToolbarRightDockSite = this.dockSite6;
            this.dotNetBarManager1.TopDockSite = this.dockSite3;
            // 
            // dockSite4
            // 
            this.dockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite4.Location = new System.Drawing.Point(0, 544);
            this.dockSite4.Name = "dockSite4";
            this.dockSite4.Size = new System.Drawing.Size(1284, 0);
            this.dockSite4.TabIndex = 18;
            this.dockSite4.TabStop = false;
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(3, 0);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 544);
            this.dockSite1.TabIndex = 15;
            this.dockSite1.TabStop = false;
            // 
            // dockSite2
            // 
            this.dockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite2.Location = new System.Drawing.Point(1284, 0);
            this.dockSite2.Name = "dockSite2";
            this.dockSite2.Size = new System.Drawing.Size(0, 544);
            this.dockSite2.TabIndex = 16;
            this.dockSite2.TabStop = false;
            // 
            // dockSite8
            // 
            this.dockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite8.Location = new System.Drawing.Point(0, 544);
            this.dockSite8.Name = "dockSite8";
            this.dockSite8.Size = new System.Drawing.Size(1284, 0);
            this.dockSite8.TabIndex = 22;
            this.dockSite8.TabStop = false;
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(0, 0);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 544);
            this.dockSite5.TabIndex = 19;
            this.dockSite5.TabStop = false;
            // 
            // dockSite6
            // 
            this.dockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite6.Location = new System.Drawing.Point(1284, 0);
            this.dockSite6.Name = "dockSite6";
            this.dockSite6.Size = new System.Drawing.Size(0, 544);
            this.dockSite6.TabIndex = 20;
            this.dockSite6.TabStop = false;
            // 
            // dockSite3
            // 
            this.dockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite3.Location = new System.Drawing.Point(0, 0);
            this.dockSite3.Name = "dockSite3";
            this.dockSite3.Size = new System.Drawing.Size(1284, 0);
            this.dockSite3.TabIndex = 17;
            this.dockSite3.TabStop = false;
            // 
            // bar1
            // 
            this.bar1.AccessibleDescription = "DotNetBar Bar (bar1)";
            this.bar1.AccessibleName = "DotNetBar Bar";
            this.bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.bar1.Location = new System.Drawing.Point(0, 0);
            this.bar1.MenuBar = true;
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(36, 24);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.bar1.TabIndex = 0;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx1.Controls.Add(this.pnlBottom);
            this.panelEx1.Controls.Add(this.panelBottom);
            this.panelEx1.Controls.Add(this.expandableSplitterTop);
            this.panelEx1.Controls.Add(this.panelTop);
            this.panelEx1.Controls.Add(this.PurchaseOrderBindingNavigator);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(3, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(1281, 544);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 23;
            this.panelEx1.Text = "panelEx1";
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.lblCreatedByText);
            this.pnlBottom.Controls.Add(this.lblCreatedDateValue);
            this.pnlBottom.Controls.Add(this.lblPurchaseReturnStatus);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 518);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1281, 26);
            this.pnlBottom.TabIndex = 246;
            // 
            // lblCreatedByText
            // 
            this.lblCreatedByText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedByText.CloseButtonVisible = false;
            this.lblCreatedByText.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblCreatedByText.Location = new System.Drawing.Point(899, 0);
            this.lblCreatedByText.Name = "lblCreatedByText";
            this.lblCreatedByText.OptionsButtonVisible = false;
            this.lblCreatedByText.Size = new System.Drawing.Size(380, 24);
            this.lblCreatedByText.TabIndex = 258;
            this.lblCreatedByText.Text = "Created By";
            // 
            // lblCreatedDateValue
            // 
            this.lblCreatedDateValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedDateValue.CloseButtonVisible = false;
            this.lblCreatedDateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCreatedDateValue.Location = new System.Drawing.Point(393, 0);
            this.lblCreatedDateValue.Name = "lblCreatedDateValue";
            this.lblCreatedDateValue.OptionsButtonVisible = false;
            this.lblCreatedDateValue.Size = new System.Drawing.Size(886, 24);
            this.lblCreatedDateValue.TabIndex = 256;
            this.lblCreatedDateValue.Text = "Created Date";
            // 
            // lblPurchaseReturnStatus
            // 
            this.lblPurchaseReturnStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblPurchaseReturnStatus.CloseButtonVisible = false;
            this.lblPurchaseReturnStatus.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPurchaseReturnStatus.Image = ((System.Drawing.Image)(resources.GetObject("lblPurchaseReturnStatus.Image")));
            this.lblPurchaseReturnStatus.Location = new System.Drawing.Point(0, 0);
            this.lblPurchaseReturnStatus.Name = "lblPurchaseReturnStatus";
            this.lblPurchaseReturnStatus.OptionsButtonVisible = false;
            this.lblPurchaseReturnStatus.Size = new System.Drawing.Size(393, 24);
            this.lblPurchaseReturnStatus.TabIndex = 20;
            // 
            // panelBottom
            // 
            this.panelBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelBottom.Controls.Add(this.tcPurchaseReturn);
            this.panelBottom.Controls.Add(this.panelGridBottom);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBottom.Location = new System.Drawing.Point(0, 150);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1281, 394);
            this.panelBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelBottom.Style.GradientAngle = 90;
            this.panelBottom.TabIndex = 1;
            this.panelBottom.Text = "panelEx3";
            // 
            // tcPurchaseReturn
            // 
            this.tcPurchaseReturn.BackColor = System.Drawing.Color.Transparent;
            this.tcPurchaseReturn.CanReorderTabs = true;
            this.tcPurchaseReturn.Controls.Add(this.tabControlPanel1);
            this.tcPurchaseReturn.Controls.Add(this.tabControlPanel2);
            this.tcPurchaseReturn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcPurchaseReturn.Location = new System.Drawing.Point(0, 0);
            this.tcPurchaseReturn.Name = "tcPurchaseReturn";
            this.tcPurchaseReturn.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcPurchaseReturn.SelectedTabIndex = 0;
            this.tcPurchaseReturn.Size = new System.Drawing.Size(1281, 269);
            this.tcPurchaseReturn.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcPurchaseReturn.TabIndex = 10;
            this.tcPurchaseReturn.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcPurchaseReturn.Tabs.Add(this.tiItemDetails);
            this.tcPurchaseReturn.Tabs.Add(this.tiLocationDetails);
            this.tcPurchaseReturn.TabStop = false;
            this.tcPurchaseReturn.Text = "tabControl1";
            this.tcPurchaseReturn.SelectedTabChanged += new DevComponents.DotNetBar.TabStrip.SelectedTabChangedEventHandler(this.tcPurchase_SelectedTabChanged);
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.dgvPurchase);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1281, 247);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 11;
            this.tabControlPanel1.TabItem = this.tiItemDetails;
            // 
            // dgvPurchase
            // 
            this.dgvPurchase.AddNewRow = false;
            this.dgvPurchase.AlphaNumericCols = new int[0];
            this.dgvPurchase.BackgroundColor = System.Drawing.Color.White;
            this.dgvPurchase.CapsLockCols = new int[0];
            this.dgvPurchase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPurchase.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.ItemID,
            this.ReferenceSerialNo,
            this.BatchID,
            this.BatchNo,
            this.InvoicedQuantity,
            this.ReceivedQty,
            this.ExtraQuantity,
            this.ReturnedQty,
            this.TempExtraQty,
            this.InvQty,
            this.InvUomID,
            this.InvRate,
            this.PurchaseRate,
            this.ReturnQuantity,
            this.Uom,
            this.Rate,
            this.GrandAmount,
            this.Discount,
            this.DiscountAmount,
            this.NetAmount,
            this.ReturnAmount,
            this.Reason});
            this.dgvPurchase.DecimalCols = new int[0];
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPurchase.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvPurchase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPurchase.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvPurchase.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvPurchase.HasSlNo = false;
            this.dgvPurchase.LastRowIndex = 0;
            this.dgvPurchase.Location = new System.Drawing.Point(1, 1);
            this.dgvPurchase.Name = "dgvPurchase";
            this.dgvPurchase.NegativeValueCols = new int[0];
            this.dgvPurchase.NumericCols = new int[0];
            this.dgvPurchase.RowHeadersWidth = 50;
            this.dgvPurchase.Size = new System.Drawing.Size(1279, 245);
            this.dgvPurchase.TabIndex = 12;
            this.dgvPurchase.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPurchase_CellValueChanged);
            this.dgvPurchase.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvPurchase_CellBeginEdit);
            this.dgvPurchase.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvPurchase_RowsAdded);
            this.dgvPurchase.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPurchase_CellEndEdit);
            this.dgvPurchase.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvPurchase_Textbox_TextChanged);
            this.dgvPurchase.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvPurchase_EditingControlShowing);
            this.dgvPurchase.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPurchase_CellEnter);
            this.dgvPurchase.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvPurchase_RowsRemoved);
            // 
            // ItemCode
            // 
            this.ItemCode.HeaderText = "Item Code";
            this.ItemCode.MaxInputLength = 20;
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemCode.Width = 65;
            // 
            // ItemName
            // 
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.MaxInputLength = 200;
            this.ItemName.MinimumWidth = 100;
            this.ItemName.Name = "ItemName";
            this.ItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemName.Width = 150;
            // 
            // ItemID
            // 
            dataGridViewCellStyle4.Format = "N0";
            dataGridViewCellStyle4.NullValue = "0";
            this.ItemID.DefaultCellStyle = dataGridViewCellStyle4;
            this.ItemID.HeaderText = "ItemID";
            this.ItemID.Name = "ItemID";
            this.ItemID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemID.Visible = false;
            // 
            // ReferenceSerialNo
            // 
            this.ReferenceSerialNo.HeaderText = "ReferenceSerialNo";
            this.ReferenceSerialNo.Name = "ReferenceSerialNo";
            this.ReferenceSerialNo.Visible = false;
            // 
            // BatchID
            // 
            this.BatchID.HeaderText = "BatchID";
            this.BatchID.Name = "BatchID";
            this.BatchID.Visible = false;
            // 
            // BatchNo
            // 
            this.BatchNo.HeaderText = "BatchNo";
            this.BatchNo.MaxInputLength = 50;
            this.BatchNo.Name = "BatchNo";
            this.BatchNo.ReadOnly = true;
            this.BatchNo.Visible = false;
            // 
            // InvoicedQuantity
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle5.Format = "N2";
            this.InvoicedQuantity.DefaultCellStyle = dataGridViewCellStyle5;
            this.InvoicedQuantity.FillWeight = 75F;
            this.InvoicedQuantity.HeaderText = "Invoiced Qty";
            this.InvoicedQuantity.MaxInputLength = 10;
            this.InvoicedQuantity.Name = "InvoicedQuantity";
            this.InvoicedQuantity.ReadOnly = true;
            this.InvoicedQuantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.InvoicedQuantity.Width = 70;
            // 
            // ReceivedQty
            // 
            this.ReceivedQty.FillWeight = 75F;
            this.ReceivedQty.HeaderText = "Received Qty";
            this.ReceivedQty.MaxInputLength = 10;
            this.ReceivedQty.Name = "ReceivedQty";
            this.ReceivedQty.ReadOnly = true;
            this.ReceivedQty.Width = 70;
            // 
            // ExtraQuantity
            // 
            this.ExtraQuantity.HeaderText = "Extra Qty";
            this.ExtraQuantity.MaxInputLength = 10;
            this.ExtraQuantity.Name = "ExtraQuantity";
            this.ExtraQuantity.ReadOnly = true;
            // 
            // ReturnedQty
            // 
            this.ReturnedQty.HeaderText = "Returned Qty";
            this.ReturnedQty.MaxInputLength = 10;
            this.ReturnedQty.Name = "ReturnedQty";
            this.ReturnedQty.ReadOnly = true;
            this.ReturnedQty.Width = 70;
            // 
            // TempExtraQty
            // 
            this.TempExtraQty.HeaderText = "TempExtraQty";
            this.TempExtraQty.MaxInputLength = 10;
            this.TempExtraQty.Name = "TempExtraQty";
            this.TempExtraQty.Visible = false;
            // 
            // InvQty
            // 
            this.InvQty.HeaderText = "InvQty";
            this.InvQty.MaxInputLength = 10;
            this.InvQty.Name = "InvQty";
            this.InvQty.Visible = false;
            // 
            // InvUomID
            // 
            this.InvUomID.HeaderText = "InvUomID";
            this.InvUomID.Name = "InvUomID";
            this.InvUomID.Visible = false;
            // 
            // InvRate
            // 
            this.InvRate.HeaderText = "InvRate";
            this.InvRate.MaxInputLength = 18;
            this.InvRate.Name = "InvRate";
            this.InvRate.Visible = false;
            // 
            // PurchaseRate
            // 
            this.PurchaseRate.HeaderText = "PurchaseRate";
            this.PurchaseRate.MaxInputLength = 18;
            this.PurchaseRate.Name = "PurchaseRate";
            this.PurchaseRate.Visible = false;
            // 
            // ReturnQuantity
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle6.Format = "N3";
            this.ReturnQuantity.DefaultCellStyle = dataGridViewCellStyle6;
            this.ReturnQuantity.FillWeight = 75F;
            this.ReturnQuantity.HeaderText = "Quantity";
            this.ReturnQuantity.MaxInputLength = 10;
            this.ReturnQuantity.Name = "ReturnQuantity";
            this.ReturnQuantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ReturnQuantity.Width = 75;
            // 
            // Uom
            // 
            this.Uom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Uom.HeaderText = "UOM";
            this.Uom.Name = "Uom";
            this.Uom.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Uom.Width = 60;
            // 
            // Rate
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle7.Format = "N3";
            this.Rate.DefaultCellStyle = dataGridViewCellStyle7;
            this.Rate.HeaderText = "Rate";
            this.Rate.MaxInputLength = 10;
            this.Rate.Name = "Rate";
            this.Rate.ReadOnly = true;
            this.Rate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Rate.Width = 80;
            // 
            // GrandAmount
            // 
            this.GrandAmount.HeaderText = "Grand Amount";
            this.GrandAmount.MaxInputLength = 10;
            this.GrandAmount.Name = "GrandAmount";
            this.GrandAmount.Width = 80;
            // 
            // Discount
            // 
            this.Discount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Discount.HeaderText = "Discount";
            this.Discount.Name = "Discount";
            this.Discount.ReadOnly = true;
            this.Discount.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Discount.Visible = false;
            // 
            // DiscountAmount
            // 
            this.DiscountAmount.FillWeight = 75F;
            this.DiscountAmount.HeaderText = "Discount Amount";
            this.DiscountAmount.MaxInputLength = 10;
            this.DiscountAmount.Name = "DiscountAmount";
            this.DiscountAmount.ReadOnly = true;
            this.DiscountAmount.Width = 75;
            // 
            // NetAmount
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle8.Format = "N3";
            this.NetAmount.DefaultCellStyle = dataGridViewCellStyle8;
            this.NetAmount.HeaderText = "Net Amount";
            this.NetAmount.MaxInputLength = 13;
            this.NetAmount.Name = "NetAmount";
            this.NetAmount.ReadOnly = true;
            this.NetAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.NetAmount.Width = 90;
            // 
            // ReturnAmount
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N3";
            this.ReturnAmount.DefaultCellStyle = dataGridViewCellStyle9;
            this.ReturnAmount.FillWeight = 95F;
            this.ReturnAmount.HeaderText = "Return Amount";
            this.ReturnAmount.MaxInputLength = 13;
            this.ReturnAmount.Name = "ReturnAmount";
            this.ReturnAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ReturnAmount.Width = 90;
            // 
            // Reason
            // 
            this.Reason.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Reason.FillWeight = 75F;
            this.Reason.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Reason.HeaderText = "Reason";
            this.Reason.MinimumWidth = 75;
            this.Reason.Name = "Reason";
            // 
            // tiItemDetails
            // 
            this.tiItemDetails.AttachedControl = this.tabControlPanel1;
            this.tiItemDetails.Name = "tiItemDetails";
            this.tiItemDetails.Text = "Item Details";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.dgvLocationDetails);
            this.tabControlPanel2.Controls.Add(this.cboWarehouse);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(1281, 247);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            this.tabControlPanel2.TabItem = this.tiLocationDetails;
            // 
            // dgvLocationDetails
            // 
            this.dgvLocationDetails.AddNewRow = false;
            this.dgvLocationDetails.AlphaNumericCols = new int[0];
            this.dgvLocationDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvLocationDetails.CapsLockCols = new int[0];
            this.dgvLocationDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocationDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LItemCode,
            this.LItemName,
            this.LItemID,
            this.LBatchID,
            this.LBatchNo,
            this.LQuantity,
            this.LUOM,
            this.LUOMID,
            this.LLocation,
            this.LRow,
            this.LBlock,
            this.LLot});
            this.dgvLocationDetails.DecimalCols = new int[0];
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLocationDetails.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvLocationDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLocationDetails.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvLocationDetails.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvLocationDetails.HasSlNo = false;
            this.dgvLocationDetails.LastRowIndex = 0;
            this.dgvLocationDetails.Location = new System.Drawing.Point(1, 1);
            this.dgvLocationDetails.Name = "dgvLocationDetails";
            this.dgvLocationDetails.NegativeValueCols = new int[0];
            this.dgvLocationDetails.NumericCols = new int[0];
            this.dgvLocationDetails.RowHeadersWidth = 30;
            this.dgvLocationDetails.Size = new System.Drawing.Size(1279, 245);
            this.dgvLocationDetails.TabIndex = 1;
            this.dgvLocationDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLocationDetails_CellValueChanged);
            this.dgvLocationDetails.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvLocationDetails_UserDeletingRow);
            this.dgvLocationDetails.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvLocationDetails_CellBeginEdit);
            this.dgvLocationDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLocationDetails_CellEndEdit);
            this.dgvLocationDetails.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvLocationDetails_Textbox_TextChanged);
            this.dgvLocationDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvLocationDetails_EditingControlShowing);
            this.dgvLocationDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvLocationDetails_CurrentCellDirtyStateChanged);
            this.dgvLocationDetails.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLocationDetails_CellEnter);
            // 
            // LItemCode
            // 
            this.LItemCode.HeaderText = "Item Code";
            this.LItemCode.Name = "LItemCode";
            this.LItemCode.Width = 60;
            // 
            // LItemName
            // 
            this.LItemName.HeaderText = "Item Name";
            this.LItemName.Name = "LItemName";
            this.LItemName.Width = 250;
            // 
            // LItemID
            // 
            this.LItemID.HeaderText = "ItemID";
            this.LItemID.Name = "LItemID";
            this.LItemID.Visible = false;
            // 
            // LBatchID
            // 
            this.LBatchID.HeaderText = "BatchID";
            this.LBatchID.Name = "LBatchID";
            this.LBatchID.Visible = false;
            // 
            // LBatchNo
            // 
            this.LBatchNo.HeaderText = "Batch No";
            this.LBatchNo.Name = "LBatchNo";
            this.LBatchNo.ReadOnly = true;
            this.LBatchNo.Width = 130;
            // 
            // LQuantity
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.LQuantity.DefaultCellStyle = dataGridViewCellStyle11;
            this.LQuantity.HeaderText = "Quantity";
            this.LQuantity.Name = "LQuantity";
            // 
            // LUOM
            // 
            this.LUOM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LUOM.HeaderText = "UOM";
            this.LUOM.Name = "LUOM";
            this.LUOM.ReadOnly = true;
            this.LUOM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LUOM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.LUOM.Width = 60;
            // 
            // LUOMID
            // 
            this.LUOMID.HeaderText = "UOMID";
            this.LUOMID.Name = "LUOMID";
            this.LUOMID.Visible = false;
            // 
            // LLocation
            // 
            this.LLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LLocation.HeaderText = "Location";
            this.LLocation.Name = "LLocation";
            this.LLocation.Width = 125;
            // 
            // LRow
            // 
            this.LRow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LRow.HeaderText = "Row";
            this.LRow.Name = "LRow";
            this.LRow.Width = 125;
            // 
            // LBlock
            // 
            this.LBlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LBlock.HeaderText = "Block";
            this.LBlock.Name = "LBlock";
            this.LBlock.Width = 125;
            // 
            // LLot
            // 
            this.LLot.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LLot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LLot.HeaderText = "Lot";
            this.LLot.Name = "LLot";
            // 
            // cboWarehouse
            // 
            this.cboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWarehouse.DisplayMember = "Text";
            this.cboWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWarehouse.DropDownHeight = 75;
            this.cboWarehouse.FormattingEnabled = true;
            this.cboWarehouse.IntegralHeight = false;
            this.cboWarehouse.ItemHeight = 14;
            this.cboWarehouse.Location = new System.Drawing.Point(417, 19);
            this.cboWarehouse.Name = "cboWarehouse";
            this.cboWarehouse.Size = new System.Drawing.Size(223, 20);
            this.cboWarehouse.TabIndex = 259;
            this.cboWarehouse.Visible = false;
            // 
            // tiLocationDetails
            // 
            this.tiLocationDetails.AttachedControl = this.tabControlPanel2;
            this.tiLocationDetails.Name = "tiLocationDetails";
            this.tiLocationDetails.Text = "Location Details";
            // 
            // panelGridBottom
            // 
            this.panelGridBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelGridBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelGridBottom.Controls.Add(this.btnAccount);
            this.panelGridBottom.Controls.Add(this.cboAccount);
            this.panelGridBottom.Controls.Add(this.lblAccount);
            this.panelGridBottom.Controls.Add(this.lblAmountIn);
            this.panelGridBottom.Controls.Add(this.panelEx2);
            this.panelGridBottom.Controls.Add(this.txtRemarks);
            this.panelGridBottom.Controls.Add(this.lblPaymentRemarks);
            this.panelGridBottom.Controls.Add(this.lblAmountInWords);
            this.panelGridBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGridBottom.Location = new System.Drawing.Point(0, 269);
            this.panelGridBottom.Name = "panelGridBottom";
            this.panelGridBottom.Size = new System.Drawing.Size(1281, 125);
            this.panelGridBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelGridBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelGridBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelGridBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelGridBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelGridBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelGridBottom.Style.GradientAngle = 90;
            this.panelGridBottom.TabIndex = 13;
            // 
            // btnAccount
            // 
            this.btnAccount.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAccount.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAccount.Location = new System.Drawing.Point(680, 6);
            this.btnAccount.Name = "btnAccount";
            this.btnAccount.Size = new System.Drawing.Size(25, 20);
            this.btnAccount.TabIndex = 297;
            this.btnAccount.Text = "...";
            this.btnAccount.Click += new System.EventHandler(this.btnAccount_Click);
            // 
            // cboAccount
            // 
            this.cboAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAccount.DisplayMember = "Text";
            this.cboAccount.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboAccount.DropDownHeight = 75;
            this.cboAccount.FormattingEnabled = true;
            this.cboAccount.IntegralHeight = false;
            this.cboAccount.ItemHeight = 14;
            this.cboAccount.Location = new System.Drawing.Point(528, 6);
            this.cboAccount.Name = "cboAccount";
            this.cboAccount.Size = new System.Drawing.Size(146, 20);
            this.cboAccount.TabIndex = 296;
            this.cboAccount.SelectedIndexChanged += new System.EventHandler(this.cboAccount_SelectedIndexChanged);
            // 
            // lblAccount
            // 
            this.lblAccount.AutoSize = true;
            this.lblAccount.Location = new System.Drawing.Point(475, 10);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(47, 13);
            this.lblAccount.TabIndex = 295;
            this.lblAccount.Text = "Account";
            // 
            // lblAmountIn
            // 
            this.lblAmountIn.AutoSize = true;
            this.lblAmountIn.Location = new System.Drawing.Point(9, 64);
            this.lblAmountIn.Name = "lblAmountIn";
            this.lblAmountIn.Size = new System.Drawing.Size(92, 13);
            this.lblAmountIn.TabIndex = 247;
            this.lblAmountIn.Text = "Amount In Words:";
            // 
            // panelEx2
            // 
            this.panelEx2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.panelEx2.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx2.Controls.Add(this.lblTaxAmount);
            this.panelEx2.Controls.Add(this.label3);
            this.panelEx2.Controls.Add(this.lblExpenseAmount);
            this.panelEx2.Controls.Add(this.label1);
            this.panelEx2.Controls.Add(this.lblDiscountAmount);
            this.panelEx2.Controls.Add(this.lblDiscount);
            this.panelEx2.Controls.Add(this.lblCompanyCurrencyAmount);
            this.panelEx2.Controls.Add(this.lblGrandReturnAmount);
            this.panelEx2.Controls.Add(this.txtGrandReturnAmount);
            this.panelEx2.Controls.Add(this.lblTotalAmount);
            this.panelEx2.Controls.Add(this.lblSubtotal);
            this.panelEx2.Controls.Add(this.txtSubtotal);
            this.panelEx2.Controls.Add(this.txtExchangeCurrencyRate);
            this.panelEx2.Controls.Add(this.txtTotalAmount);
            this.panelEx2.Location = new System.Drawing.Point(709, 3);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(569, 91);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 241;
            // 
            // lblTaxAmount
            // 
            // 
            // 
            // 
            this.lblTaxAmount.BackgroundStyle.Class = "";
            this.lblTaxAmount.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTaxAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaxAmount.ForeColor = System.Drawing.Color.Black;
            this.lblTaxAmount.Location = new System.Drawing.Point(284, 28);
            this.lblTaxAmount.Name = "lblTaxAmount";
            this.lblTaxAmount.Size = new System.Drawing.Size(92, 17);
            this.lblTaxAmount.TabIndex = 310;
            this.lblTaxAmount.Text = "0.00";
            this.lblTaxAmount.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(196, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 15);
            this.label3.TabIndex = 309;
            this.label3.Text = "Tax Amount";
            // 
            // lblExpenseAmount
            // 
            // 
            // 
            // 
            this.lblExpenseAmount.BackgroundStyle.Class = "";
            this.lblExpenseAmount.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblExpenseAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpenseAmount.ForeColor = System.Drawing.Color.Black;
            this.lblExpenseAmount.Location = new System.Drawing.Point(269, 8);
            this.lblExpenseAmount.Name = "lblExpenseAmount";
            this.lblExpenseAmount.Size = new System.Drawing.Size(106, 17);
            this.lblExpenseAmount.TabIndex = 308;
            this.lblExpenseAmount.Text = "0.00";
            this.lblExpenseAmount.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblExpenseAmount.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(197, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 15);
            this.label1.TabIndex = 307;
            this.label1.Text = "Expense";
            this.label1.Visible = false;
            // 
            // lblDiscountAmount
            // 
            // 
            // 
            // 
            this.lblDiscountAmount.BackgroundStyle.Class = "";
            this.lblDiscountAmount.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDiscountAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscountAmount.ForeColor = System.Drawing.Color.Black;
            this.lblDiscountAmount.Location = new System.Drawing.Point(82, 7);
            this.lblDiscountAmount.Name = "lblDiscountAmount";
            this.lblDiscountAmount.Size = new System.Drawing.Size(106, 17);
            this.lblDiscountAmount.TabIndex = 306;
            this.lblDiscountAmount.Text = "0.00";
            this.lblDiscountAmount.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblDiscountAmount.Visible = false;
            // 
            // lblDiscount
            // 
            this.lblDiscount.AutoSize = true;
            this.lblDiscount.BackColor = System.Drawing.Color.Transparent;
            this.lblDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.lblDiscount.Location = new System.Drawing.Point(13, 7);
            this.lblDiscount.Name = "lblDiscount";
            this.lblDiscount.Size = new System.Drawing.Size(63, 15);
            this.lblDiscount.TabIndex = 305;
            this.lblDiscount.Text = "Discount";
            this.lblDiscount.Visible = false;
            // 
            // lblCompanyCurrencyAmount
            // 
            this.lblCompanyCurrencyAmount.AutoSize = true;
            this.lblCompanyCurrencyAmount.BackColor = System.Drawing.Color.White;
            this.lblCompanyCurrencyAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompanyCurrencyAmount.Location = new System.Drawing.Point(4, 75);
            this.lblCompanyCurrencyAmount.Name = "lblCompanyCurrencyAmount";
            this.lblCompanyCurrencyAmount.Size = new System.Drawing.Size(104, 9);
            this.lblCompanyCurrencyAmount.TabIndex = 303;
            this.lblCompanyCurrencyAmount.Text = "Amount in company currency";
            // 
            // lblGrandReturnAmount
            // 
            this.lblGrandReturnAmount.AutoSize = true;
            this.lblGrandReturnAmount.BackColor = System.Drawing.Color.White;
            this.lblGrandReturnAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrandReturnAmount.Location = new System.Drawing.Point(197, 75);
            this.lblGrandReturnAmount.Name = "lblGrandReturnAmount";
            this.lblGrandReturnAmount.Size = new System.Drawing.Size(56, 9);
            this.lblGrandReturnAmount.TabIndex = 255;
            this.lblGrandReturnAmount.Text = "Return Amount";
            // 
            // txtGrandReturnAmount
            // 
            this.txtGrandReturnAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtGrandReturnAmount.Border.Class = "TextBoxBorder";
            this.txtGrandReturnAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGrandReturnAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrandReturnAmount.Location = new System.Drawing.Point(192, 49);
            this.txtGrandReturnAmount.MaxLength = 13;
            this.txtGrandReturnAmount.Multiline = true;
            this.txtGrandReturnAmount.Name = "txtGrandReturnAmount";
            this.txtGrandReturnAmount.Size = new System.Drawing.Size(185, 38);
            this.txtGrandReturnAmount.TabIndex = 18;
            this.txtGrandReturnAmount.TabStop = false;
            this.txtGrandReturnAmount.Text = "0.00";
            this.txtGrandReturnAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGrandReturnAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGrandReturnAmount_KeyPress);
            this.txtGrandReturnAmount.TextChanged += new System.EventHandler(this.txtGrandReturnAmount_TextChanged);
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.BackColor = System.Drawing.Color.White;
            this.lblTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(384, 75);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(45, 9);
            this.lblTotalAmount.TabIndex = 253;
            this.lblTotalAmount.Text = "Net Amount";
            // 
            // lblSubtotal
            // 
            this.lblSubtotal.AutoSize = true;
            this.lblSubtotal.BackColor = System.Drawing.Color.White;
            this.lblSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubtotal.Location = new System.Drawing.Point(382, 33);
            this.lblSubtotal.Name = "lblSubtotal";
            this.lblSubtotal.Size = new System.Drawing.Size(37, 9);
            this.lblSubtotal.TabIndex = 250;
            this.lblSubtotal.Text = "Sub Total";
            // 
            // txtSubtotal
            // 
            this.txtSubtotal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtSubtotal.Border.Class = "TextBoxBorder";
            this.txtSubtotal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.txtSubtotal.Location = new System.Drawing.Point(381, 8);
            this.txtSubtotal.Multiline = true;
            this.txtSubtotal.Name = "txtSubtotal";
            this.txtSubtotal.ReadOnly = true;
            this.txtSubtotal.Size = new System.Drawing.Size(185, 41);
            this.txtSubtotal.TabIndex = 14;
            this.txtSubtotal.TabStop = false;
            this.txtSubtotal.Text = "0.00";
            this.txtSubtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtExchangeCurrencyRate
            // 
            this.txtExchangeCurrencyRate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtExchangeCurrencyRate.Border.Class = "TextBoxBorder";
            this.txtExchangeCurrencyRate.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtExchangeCurrencyRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExchangeCurrencyRate.Location = new System.Drawing.Point(3, 49);
            this.txtExchangeCurrencyRate.Multiline = true;
            this.txtExchangeCurrencyRate.Name = "txtExchangeCurrencyRate";
            this.txtExchangeCurrencyRate.ReadOnly = true;
            this.txtExchangeCurrencyRate.Size = new System.Drawing.Size(185, 38);
            this.txtExchangeCurrencyRate.TabIndex = 17;
            this.txtExchangeCurrencyRate.TabStop = false;
            this.txtExchangeCurrencyRate.Text = "0.00";
            this.txtExchangeCurrencyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTotalAmount.Border.Class = "TextBoxBorder";
            this.txtTotalAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(381, 49);
            this.txtTotalAmount.MaxLength = 15;
            this.txtTotalAmount.Multiline = true;
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.Size = new System.Drawing.Size(185, 38);
            this.txtTotalAmount.TabIndex = 19;
            this.txtTotalAmount.TabStop = false;
            this.txtTotalAmount.Text = "0.00";
            this.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtRemarks
            // 
            // 
            // 
            // 
            this.txtRemarks.Border.Class = "TextBoxBorder";
            this.txtRemarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtRemarks.Location = new System.Drawing.Point(107, 6);
            this.txtRemarks.MaxLength = 1000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(353, 49);
            this.txtRemarks.TabIndex = 13;
            this.txtRemarks.TextChanged += new System.EventHandler(this.Changestatus);
            // 
            // lblPaymentRemarks
            // 
            this.lblPaymentRemarks.AutoSize = true;
            this.lblPaymentRemarks.Location = new System.Drawing.Point(9, 17);
            this.lblPaymentRemarks.Name = "lblPaymentRemarks";
            this.lblPaymentRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblPaymentRemarks.TabIndex = 237;
            this.lblPaymentRemarks.Text = "Remarks";
            // 
            // lblAmountInWords
            // 
            this.lblAmountInWords.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountInWords.Location = new System.Drawing.Point(107, 64);
            this.lblAmountInWords.Name = "lblAmountInWords";
            this.lblAmountInWords.Size = new System.Drawing.Size(615, 27);
            this.lblAmountInWords.TabIndex = 248;
            // 
            // expandableSplitterTop
            // 
            this.expandableSplitterTop.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterTop.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitterTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitterTop.ExpandableControl = this.panelTop;
            this.expandableSplitterTop.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterTop.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterTop.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterTop.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterTop.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.Location = new System.Drawing.Point(0, 147);
            this.expandableSplitterTop.Name = "expandableSplitterTop";
            this.expandableSplitterTop.Size = new System.Drawing.Size(1281, 3);
            this.expandableSplitterTop.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterTop.TabIndex = 16;
            this.expandableSplitterTop.TabStop = false;
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelTop.Controls.Add(this.tcGeneral);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1281, 122);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 0;
            // 
            // tcGeneral
            // 
            this.tcGeneral.BackColor = System.Drawing.Color.Transparent;
            this.tcGeneral.CanReorderTabs = true;
            this.tcGeneral.Controls.Add(this.tabControlPanel3);
            this.tcGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcGeneral.Location = new System.Drawing.Point(0, 0);
            this.tcGeneral.Name = "tcGeneral";
            this.tcGeneral.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcGeneral.SelectedTabIndex = 0;
            this.tcGeneral.Size = new System.Drawing.Size(1281, 122);
            this.tcGeneral.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcGeneral.TabIndex = 0;
            this.tcGeneral.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcGeneral.Tabs.Add(this.tiGeneral);
            this.tcGeneral.TabStop = false;
            this.tcGeneral.Text = "tabControl1";
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel3.Controls.Add(this.label2);
            this.tabControlPanel3.Controls.Add(this.cboTaxScheme);
            this.tabControlPanel3.Controls.Add(this.dtpReturnDate);
            this.tabControlPanel3.Controls.Add(this.lblCompany);
            this.tabControlPanel3.Controls.Add(this.lblCustomer);
            this.tabControlPanel3.Controls.Add(this.lblReturnNo);
            this.tabControlPanel3.Controls.Add(this.lblVendorAddress);
            this.tabControlPanel3.Controls.Add(this.lblDate);
            this.tabControlPanel3.Controls.Add(this.lblStatusLabel);
            this.tabControlPanel3.Controls.Add(this.lblStatus);
            this.tabControlPanel3.Controls.Add(this.lblCurrency);
            this.tabControlPanel3.Controls.Add(this.cboInvoiceNo);
            this.tabControlPanel3.Controls.Add(this.cboSupplier);
            this.tabControlPanel3.Controls.Add(this.lblInvoiceNo);
            this.tabControlPanel3.Controls.Add(this.txtVendorAddress);
            this.tabControlPanel3.Controls.Add(this.cboCurrency);
            this.tabControlPanel3.Controls.Add(this.txtReturnNo);
            this.tabControlPanel3.Controls.Add(this.cboCompany);
            this.tabControlPanel3.Controls.Add(this.txtDescription);
            this.tabControlPanel3.Controls.Add(this.lblDescription);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(1281, 100);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 1;
            this.tabControlPanel3.TabItem = this.tiGeneral;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(577, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 257;
            this.label2.Text = "TaxScheme";
            // 
            // cboTaxScheme
            // 
            this.cboTaxScheme.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboTaxScheme.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTaxScheme.DisplayMember = "Text";
            this.cboTaxScheme.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboTaxScheme.DropDownHeight = 75;
            this.cboTaxScheme.Enabled = false;
            this.cboTaxScheme.FormattingEnabled = true;
            this.cboTaxScheme.IntegralHeight = false;
            this.cboTaxScheme.ItemHeight = 14;
            this.cboTaxScheme.Location = new System.Drawing.Point(645, 8);
            this.cboTaxScheme.Name = "cboTaxScheme";
            this.cboTaxScheme.Size = new System.Drawing.Size(119, 20);
            this.cboTaxScheme.TabIndex = 256;
            this.cboTaxScheme.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTaxScheme.WatermarkText = "Select Customer";
            this.cboTaxScheme.SelectedIndexChanged += new System.EventHandler(this.cboTaxScheme_SelectedIndexChanged);
            // 
            // dtpReturnDate
            // 
            this.dtpReturnDate.CustomFormat = "dd MMM yyyy";
            this.dtpReturnDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpReturnDate.Location = new System.Drawing.Point(426, 53);
            this.dtpReturnDate.Name = "dtpReturnDate";
            this.dtpReturnDate.Size = new System.Drawing.Size(132, 20);
            this.dtpReturnDate.TabIndex = 255;
            this.dtpReturnDate.ValueChanged += new System.EventHandler(this.dtpReturnDate_ValueChanged);
            this.dtpReturnDate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtpReturnDate_KeyPress);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.BackColor = System.Drawing.Color.Transparent;
            this.lblCompany.Location = new System.Drawing.Point(15, 11);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 237;
            this.lblCompany.Text = "Company";
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.BackColor = System.Drawing.Color.Transparent;
            this.lblCustomer.Location = new System.Drawing.Point(15, 34);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(45, 13);
            this.lblCustomer.TabIndex = 254;
            this.lblCustomer.Text = "Supplier";
            // 
            // lblReturnNo
            // 
            this.lblReturnNo.AutoSize = true;
            this.lblReturnNo.BackColor = System.Drawing.Color.Transparent;
            this.lblReturnNo.Location = new System.Drawing.Point(351, 11);
            this.lblReturnNo.Name = "lblReturnNo";
            this.lblReturnNo.Size = new System.Drawing.Size(56, 13);
            this.lblReturnNo.TabIndex = 185;
            this.lblReturnNo.Text = "Return No";
            // 
            // lblVendorAddress
            // 
            this.lblVendorAddress.AutoSize = true;
            this.lblVendorAddress.BackColor = System.Drawing.SystemColors.Control;
            this.lblVendorAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendorAddress.Location = new System.Drawing.Point(87, 55);
            this.lblVendorAddress.Name = "lblVendorAddress";
            this.lblVendorAddress.Size = new System.Drawing.Size(33, 9);
            this.lblVendorAddress.TabIndex = 194;
            this.lblVendorAddress.Text = "Address";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Location = new System.Drawing.Point(351, 52);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 188;
            this.lblDate.Text = "Date";
            // 
            // lblStatusLabel
            // 
            this.lblStatusLabel.AutoSize = true;
            this.lblStatusLabel.BackColor = System.Drawing.Color.Transparent;
            this.lblStatusLabel.Location = new System.Drawing.Point(780, 9);
            this.lblStatusLabel.Name = "lblStatusLabel";
            this.lblStatusLabel.Size = new System.Drawing.Size(37, 13);
            this.lblStatusLabel.TabIndex = 252;
            this.lblStatusLabel.Text = "Status";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(860, 6);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(43, 20);
            this.lblStatus.TabIndex = 14;
            this.lblStatus.Text = "New";
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrency.Location = new System.Drawing.Point(351, 73);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(49, 13);
            this.lblCurrency.TabIndex = 251;
            this.lblCurrency.Text = "Currency";
            // 
            // cboInvoiceNo
            // 
            this.cboInvoiceNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboInvoiceNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboInvoiceNo.DisplayMember = "Text";
            this.cboInvoiceNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboInvoiceNo.DropDownHeight = 75;
            this.cboInvoiceNo.FormattingEnabled = true;
            this.cboInvoiceNo.IntegralHeight = false;
            this.cboInvoiceNo.ItemHeight = 14;
            this.cboInvoiceNo.Location = new System.Drawing.Point(426, 30);
            this.cboInvoiceNo.Name = "cboInvoiceNo";
            this.cboInvoiceNo.Size = new System.Drawing.Size(132, 20);
            this.cboInvoiceNo.TabIndex = 6;
            this.cboInvoiceNo.SelectedIndexChanged += new System.EventHandler(this.cboInvoiceNo_SelectedIndexChanged);
            // 
            // cboSupplier
            // 
            this.cboSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSupplier.DisplayMember = "Text";
            this.cboSupplier.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSupplier.DropDownHeight = 75;
            this.cboSupplier.FormattingEnabled = true;
            this.cboSupplier.IntegralHeight = false;
            this.cboSupplier.ItemHeight = 14;
            this.cboSupplier.Location = new System.Drawing.Point(86, 31);
            this.cboSupplier.Name = "cboSupplier";
            this.cboSupplier.Size = new System.Drawing.Size(223, 20);
            this.cboSupplier.TabIndex = 3;
            this.cboSupplier.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSupplier.WatermarkText = "Select Customer";
            this.cboSupplier.SelectedIndexChanged += new System.EventHandler(this.cboSupplier_SelectedIndexChanged);
            this.cboSupplier.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblInvoiceNo
            // 
            this.lblInvoiceNo.AutoSize = true;
            this.lblInvoiceNo.BackColor = System.Drawing.Color.Transparent;
            this.lblInvoiceNo.Location = new System.Drawing.Point(351, 32);
            this.lblInvoiceNo.Name = "lblInvoiceNo";
            this.lblInvoiceNo.Size = new System.Drawing.Size(59, 13);
            this.lblInvoiceNo.TabIndex = 249;
            this.lblInvoiceNo.Text = "Invoice No";
            // 
            // txtVendorAddress
            // 
            // 
            // 
            // 
            this.txtVendorAddress.Border.Class = "TextBoxBorder";
            this.txtVendorAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtVendorAddress.Location = new System.Drawing.Point(86, 53);
            this.txtVendorAddress.MaxLength = 20;
            this.txtVendorAddress.Multiline = true;
            this.txtVendorAddress.Name = "txtVendorAddress";
            this.txtVendorAddress.ReadOnly = true;
            this.txtVendorAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtVendorAddress.Size = new System.Drawing.Size(223, 43);
            this.txtVendorAddress.TabIndex = 4;
            this.txtVendorAddress.TabStop = false;
            // 
            // cboCurrency
            // 
            this.cboCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCurrency.DisplayMember = "Text";
            this.cboCurrency.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCurrency.DropDownHeight = 75;
            this.cboCurrency.Enabled = false;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.IntegralHeight = false;
            this.cboCurrency.ItemHeight = 14;
            this.cboCurrency.Location = new System.Drawing.Point(426, 75);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(132, 20);
            this.cboCurrency.TabIndex = 8;
            this.cboCurrency.TabStop = false;
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            this.cboCurrency.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // txtReturnNo
            // 
            this.txtReturnNo.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtReturnNo.Border.Class = "TextBoxBorder";
            this.txtReturnNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtReturnNo.Location = new System.Drawing.Point(426, 7);
            this.txtReturnNo.MaxLength = 20;
            this.txtReturnNo.Name = "txtReturnNo";
            this.txtReturnNo.ReadOnly = true;
            this.txtReturnNo.Size = new System.Drawing.Size(132, 20);
            this.txtReturnNo.TabIndex = 5;
            this.txtReturnNo.TabStop = false;
            this.txtReturnNo.TextChanged += new System.EventHandler(this.Changestatus);
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 75;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(86, 8);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(223, 20);
            this.cboCompany.TabIndex = 2;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // txtDescription
            // 
            // 
            // 
            // 
            this.txtDescription.Border.Class = "TextBoxBorder";
            this.txtDescription.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDescription.Enabled = false;
            this.txtDescription.Location = new System.Drawing.Point(862, 32);
            this.txtDescription.MaxLength = 20;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(260, 60);
            this.txtDescription.TabIndex = 9;
            this.txtDescription.TabStop = false;
            this.txtDescription.Visible = false;
            this.txtDescription.TextChanged += new System.EventHandler(this.Changestatus);
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblDescription.Location = new System.Drawing.Point(780, 32);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 13);
            this.lblDescription.TabIndex = 236;
            this.lblDescription.Text = "Description";
            this.lblDescription.Visible = false;
            // 
            // tiGeneral
            // 
            this.tiGeneral.AttachedControl = this.tabControlPanel3;
            this.tiGeneral.Name = "tiGeneral";
            this.tiGeneral.Text = "General";
            // 
            // PurchaseOrderBindingNavigator
            // 
            this.PurchaseOrderBindingNavigator.AccessibleDescription = "DotNetBar Bar (PurchaseOrderBindingNavigator)";
            this.PurchaseOrderBindingNavigator.AccessibleName = "DotNetBar Bar";
            this.PurchaseOrderBindingNavigator.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.PurchaseOrderBindingNavigator.Dock = System.Windows.Forms.DockStyle.Top;
            this.PurchaseOrderBindingNavigator.DockLine = 1;
            this.PurchaseOrderBindingNavigator.DockOffset = 73;
            this.PurchaseOrderBindingNavigator.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.PurchaseOrderBindingNavigator.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.PurchaseOrderBindingNavigator.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorClearItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorCancelItem,
            this.BtnPrint,
            this.BtnEmail,
            this.btnPickList,
            this.btnPickListEmail,
            this.btnSupplierHistory});
            this.PurchaseOrderBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.PurchaseOrderBindingNavigator.Name = "PurchaseOrderBindingNavigator";
            this.PurchaseOrderBindingNavigator.Size = new System.Drawing.Size(1281, 25);
            this.PurchaseOrderBindingNavigator.Stretch = true;
            this.PurchaseOrderBindingNavigator.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PurchaseOrderBindingNavigator.TabIndex = 12;
            this.PurchaseOrderBindingNavigator.TabStop = false;
            this.PurchaseOrderBindingNavigator.Text = "bar2";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlEnter);
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.Tooltip = "Add New Information";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Text = "&Save";
            this.BindingNavigatorSaveItem.Tooltip = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorClearItem
            // 
            this.BindingNavigatorClearItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorClearItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorClearItem.Image")));
            this.BindingNavigatorClearItem.Name = "BindingNavigatorClearItem";
            this.BindingNavigatorClearItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.BindingNavigatorClearItem.Text = "Clear";
            this.BindingNavigatorClearItem.Tooltip = "Clear";
            this.BindingNavigatorClearItem.Visible = false;
            this.BindingNavigatorClearItem.Click += new System.EventHandler(this.BindingNavigatorClearItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Tooltip = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorCancelItem
            // 
            this.BindingNavigatorCancelItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorCancelItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorCancelItem.Image")));
            this.BindingNavigatorCancelItem.Name = "BindingNavigatorCancelItem";
            this.BindingNavigatorCancelItem.Text = "Cancel";
            this.BindingNavigatorCancelItem.Tooltip = "Cancel";
            this.BindingNavigatorCancelItem.Click += new System.EventHandler(this.BindingNavigatorCancelItem_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // btnPickList
            // 
            this.btnPickList.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPickList.Image = ((System.Drawing.Image)(resources.GetObject("btnPickList.Image")));
            this.btnPickList.Name = "btnPickList";
            this.btnPickList.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.btnPickList.Text = "Pick List";
            this.btnPickList.Click += new System.EventHandler(this.btnPickList_Click);
            // 
            // btnPickListEmail
            // 
            this.btnPickListEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPickListEmail.Image = ((System.Drawing.Image)(resources.GetObject("btnPickListEmail.Image")));
            this.btnPickListEmail.Name = "btnPickListEmail";
            this.btnPickListEmail.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.btnPickListEmail.Text = "Pick List Email";
            this.btnPickListEmail.Click += new System.EventHandler(this.btnPickListEmail_Click);
            // 
            // btnSupplierHistory
            // 
            this.btnSupplierHistory.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSupplierHistory.Image = ((System.Drawing.Image)(resources.GetObject("btnSupplierHistory.Image")));
            this.btnSupplierHistory.Name = "btnSupplierHistory";
            this.btnSupplierHistory.Text = "SupplierHistory";
            this.btnSupplierHistory.Visible = false;
            this.btnSupplierHistory.Click += new System.EventHandler(this.btnSupplierHistory_Click);
            // 
            // tiSuggestions
            // 
            this.tiSuggestions.Name = "tiSuggestions";
            this.tiSuggestions.Text = "Suggestions";
            this.tiSuggestions.Visible = false;
            // 
            // TmrPurchaseReturn
            // 
            this.TmrPurchaseReturn.Interval = 2000;
            this.TmrPurchaseReturn.Tick += new System.EventHandler(this.TmrPurchaseReturn_Tick);
            // 
            // ErrPurchaseReturn
            // 
            this.ErrPurchaseReturn.ContainerControl = this;
            this.ErrPurchaseReturn.RightToLeft = true;
            // 
            // ImgPurchaseReturn
            // 
            this.ImgPurchaseReturn.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgPurchaseReturn.ImageStream")));
            this.ImgPurchaseReturn.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgPurchaseReturn.Images.SetKeyName(0, "Purchase Indent.ICO");
            this.ImgPurchaseReturn.Images.SetKeyName(1, "Purchase Order.ico");
            this.ImgPurchaseReturn.Images.SetKeyName(2, "Purchase Invoice.ico");
            this.ImgPurchaseReturn.Images.SetKeyName(3, "GRN.ICO");
            this.ImgPurchaseReturn.Images.SetKeyName(4, "Purchase Order Return.ico");
            this.ImgPurchaseReturn.Images.SetKeyName(5, "Add.png");
            this.ImgPurchaseReturn.Images.SetKeyName(6, "save.PNG");
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Visible = false;
            this.dataGridViewTextBoxColumn12.Width = 150;
            // 
            // dataGridViewTextBoxColumn13
            // 
            dataGridViewCellStyle13.Format = "N0";
            dataGridViewCellStyle13.NullValue = "0";
            this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn13.HeaderText = "Item Name";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Visible = false;
            this.dataGridViewTextBoxColumn13.Width = 180;
            // 
            // dataGridViewTextBoxColumn14
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn14.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn14.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Visible = false;
            this.dataGridViewTextBoxColumn14.Width = 65;
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn15.HeaderText = "Return Quantity";
            this.dataGridViewTextBoxColumn15.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Visible = false;
            this.dataGridViewTextBoxColumn15.Width = 190;
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn16.HeaderText = "Batch Number";
            this.dataGridViewTextBoxColumn16.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Visible = false;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewTextBoxColumn17.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn17.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn18
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn18.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewTextBoxColumn18.HeaderText = "Total";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn18.Visible = false;
            this.dataGridViewTextBoxColumn18.Width = 80;
            // 
            // dataGridViewTextBoxColumn19
            // 
            dataGridViewCellStyle19.Format = "N0";
            dataGridViewCellStyle19.NullValue = "0";
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn19.HeaderText = "Status";
            this.dataGridViewTextBoxColumn19.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn19.Visible = false;
            this.dataGridViewTextBoxColumn19.Width = 80;
            // 
            // dataGridViewTextBoxColumn20
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle20.Format = "N2";
            dataGridViewCellStyle20.NullValue = "0";
            this.dataGridViewTextBoxColumn20.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn20.HeaderText = "OrderDetailID";
            this.dataGridViewTextBoxColumn20.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn20.Visible = false;
            this.dataGridViewTextBoxColumn20.Width = 80;
            // 
            // dataGridViewTextBoxColumn21
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle21.Format = "N2";
            dataGridViewCellStyle21.NullValue = "0";
            this.dataGridViewTextBoxColumn21.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewTextBoxColumn21.HeaderText = "PurchaseOrderID";
            this.dataGridViewTextBoxColumn21.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn21.Visible = false;
            this.dataGridViewTextBoxColumn21.Width = 80;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn22.Visible = false;
            // 
            // dataGridViewTextBoxColumn23
            // 
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle22.Format = "N2";
            dataGridViewCellStyle22.NullValue = "0";
            this.dataGridViewTextBoxColumn23.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewTextBoxColumn23.HeaderText = "SelectedDiscountID";
            this.dataGridViewTextBoxColumn23.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn23.Visible = false;
            this.dataGridViewTextBoxColumn23.Width = 80;
            // 
            // dataGridViewTextBoxColumn24
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle23.Format = "N4";
            dataGridViewCellStyle23.NullValue = "0";
            this.dataGridViewTextBoxColumn24.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewTextBoxColumn24.HeaderText = "BatchID";
            this.dataGridViewTextBoxColumn24.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn24.Visible = false;
            this.dataGridViewTextBoxColumn24.Width = 80;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle24.Format = "N0";
            dataGridViewCellStyle24.NullValue = "0";
            this.dataGridViewTextBoxColumn25.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewTextBoxColumn25.HeaderText = "ReturnAmount";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn25.Visible = false;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.HeaderText = "DiscountAmount";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Visible = false;
            this.dataGridViewTextBoxColumn26.Width = 80;
            // 
            // dataGridViewTextBoxColumn27
            // 
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle25.Format = "N4";
            dataGridViewCellStyle25.NullValue = "0";
            this.dataGridViewTextBoxColumn27.DefaultCellStyle = dataGridViewCellStyle25;
            this.dataGridViewTextBoxColumn27.HeaderText = "Total";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn27.Width = 80;
            // 
            // dataGridViewTextBoxColumn28
            // 
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle26.Format = "N0";
            dataGridViewCellStyle26.NullValue = "0";
            this.dataGridViewTextBoxColumn28.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn28.FillWeight = 80F;
            this.dataGridViewTextBoxColumn28.HeaderText = "Return Amt";
            this.dataGridViewTextBoxColumn28.MaxInputLength = 15;
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.HeaderText = "WarehouseID";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.Visible = false;
            // 
            // clmItemCode
            // 
            this.clmItemCode.HeaderText = "Item Code";
            this.clmItemCode.Name = "clmItemCode";
            this.clmItemCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmItemCode.Width = 150;
            // 
            // clmItemName
            // 
            this.clmItemName.HeaderText = "Item Name";
            this.clmItemName.Name = "clmItemName";
            this.clmItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmItemName.Width = 180;
            // 
            // clmQuantity
            // 
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmQuantity.DefaultCellStyle = dataGridViewCellStyle27;
            this.clmQuantity.HeaderText = "Quantity";
            this.clmQuantity.MaxInputLength = 6;
            this.clmQuantity.Name = "clmQuantity";
            this.clmQuantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmQtyReceived
            // 
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmQtyReceived.DefaultCellStyle = dataGridViewCellStyle28;
            this.clmQtyReceived.HeaderText = "QtyReceived";
            this.clmQtyReceived.MaxInputLength = 6;
            this.clmQtyReceived.Name = "clmQtyReceived";
            this.clmQtyReceived.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmBatchNumber
            // 
            this.clmBatchNumber.HeaderText = "Batch Number";
            this.clmBatchNumber.Name = "clmBatchNumber";
            this.clmBatchNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmBatchNumber.Visible = false;
            // 
            // clmRate
            // 
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmRate.DefaultCellStyle = dataGridViewCellStyle29;
            this.clmRate.HeaderText = "Rate";
            this.clmRate.MaxInputLength = 6;
            this.clmRate.Name = "clmRate";
            this.clmRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmTotal
            // 
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.clmTotal.DefaultCellStyle = dataGridViewCellStyle30;
            this.clmTotal.HeaderText = "Total";
            this.clmTotal.Name = "clmTotal";
            this.clmTotal.ReadOnly = true;
            this.clmTotal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmStatus
            // 
            this.clmStatus.HeaderText = "Status";
            this.clmStatus.Name = "clmStatus";
            this.clmStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmOrderDetailID
            // 
            this.clmOrderDetailID.HeaderText = "OrderDetailID";
            this.clmOrderDetailID.Name = "clmOrderDetailID";
            this.clmOrderDetailID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmOrderDetailID.Visible = false;
            // 
            // clmSalesOrderID
            // 
            this.clmSalesOrderID.HeaderText = "SalesOrderID";
            this.clmSalesOrderID.Name = "clmSalesOrderID";
            this.clmSalesOrderID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmSalesOrderID.Visible = false;
            // 
            // clmItemID
            // 
            this.clmItemID.HeaderText = "ItemID";
            this.clmItemID.Name = "clmItemID";
            this.clmItemID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmItemID.Visible = false;
            // 
            // FrmDebitNote
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.ClientSize = new System.Drawing.Size(1284, 544);
            this.Controls.Add(this.panelEx1);
            this.Controls.Add(this.dockSite2);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.expandableSplitterLeft);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.dockSite3);
            this.Controls.Add(this.dockSite4);
            this.Controls.Add(this.dockSite5);
            this.Controls.Add(this.dockSite6);
            this.Controls.Add(this.dockSite8);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmDebitNote";
            this.Text = "Debit Note";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmPurchaseReturn_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmDebitNote_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmPurchaseReturn_KeyDown);
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvSOrderDisplay)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.panelEx1.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            this.panelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcPurchaseReturn)).EndInit();
            this.tcPurchaseReturn.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchase)).EndInit();
            this.tabControlPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocationDetails)).EndInit();
            this.panelGridBottom.ResumeLayout(false);
            this.panelGridBottom.PerformLayout();
            this.panelEx2.ResumeLayout(false);
            this.panelEx2.PerformLayout();
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).EndInit();
            this.tcGeneral.ResumeLayout(false);
            this.tabControlPanel3.ResumeLayout(false);
            this.tabControlPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrPurchaseReturn)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        public FrmDebitNote()
        {
            InitializeComponent();

            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotificationNew();
            MmessageIcon = MessageBoxIcon.Information;
            MobjclsBLLPurchaseReturn = new clsBLLPurchaseReturnMaster();
            //objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            MobjclsBLLCommonUtility = new clsBLLCommonUtility();
        }


        private void LoadMessage()
        {
            datMessages = new DataTable();
            MaStatusMessage = new DataTable();
            datMessages = MObjClsNotification.FillMessageArray((int)FormID.Purchase, 9);
        }

        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private bool LoadCombos(int intFillType)
        {
            try
            {
                //bool blnIsEnabled = false;
                DataTable datCombos = new DataTable();
               

                if (intFillType == 0 || intFillType == 1)
                {
                    datCombos = null;

                    datCombos = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", " CompanyID= " + ClsCommonSettings.LoginCompanyID.ToString() });
                    CboSCompany.ValueMember = "CompanyID";
                    CboSCompany.DisplayMember = "CompanyName";
                    CboSCompany.DataSource = datCombos;

                    datCombos = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", " CompanyID= " + ClsCommonSettings.LoginCompanyID.ToString() });
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;
                    cboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID.ToString();
                    cboCompany.Enabled = false;
                }
                if (intFillType == 0 || intFillType == 2)
                {
                    if (MBlnEditMode)
                    {
                        datCombos = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "distinct V.VendorID,V.VendorName", "InvVendorInformation V INNER JOIN InvVendorCompanyDetails VC ON V.VendorID = VC.VendorID ", " V.VendorTypeID = " + (int)VendorType.Supplier + " AND VC.CompanyID = " + ClsCommonSettings.CompanyID + "  and V.StatusID=" + (int)VendorStatus.SupplierActive + " UNION SELECT VendorID,VendorName FROM InvVendorInformation WHERE VendorID =" + MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intVendorID });
                    }
                    else
                    {
                        datCombos = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "distinct V.VendorID,V.VendorName", "InvVendorInformation V INNER JOIN InvVendorCompanyDetails VC ON V.VendorID = VC.VendorID ", " V.VendorTypeID = " + (int)VendorType.Supplier + " AND VC.CompanyID = " + ClsCommonSettings.CompanyID + "  and V.StatusID=" + (int)VendorStatus.SupplierActive });
                    } 
                    cboSupplier.ValueMember = "VendorID";
                    cboSupplier.DisplayMember = "VendorName";
                    cboSupplier.DataSource = datCombos;
                }
                if (intFillType == 0 || intFillType == 3)
                {
                    datCombos = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", "VendorTypeID=2" });
                    CboSSupplier.ValueMember = "VendorID";
                    CboSSupplier.DisplayMember = "VendorName";
                    DataRow dr = datCombos.NewRow();
                    dr["VendorID"] = -2;
                    dr["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(dr, 0); 
                    CboSSupplier.DataSource = datCombos;
                }
                
                if (intFillType == 0 || intFillType == 6)
                {
                    datCombos = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "distinct CR.CurrencyID,CR.CurrencyName as Description", "CurrencyReference CR INNER JOIN CurrencyDetails CD ON CR.CurrencyID=CD.CurrencyID", "CD.CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) });
                    cboCurrency.ValueMember = "CurrencyID";
                    cboCurrency.DisplayMember = "Description";
                    cboCurrency.DataSource = datCombos;

                    if (MBlnEditMode)
                    {
                        datCombos = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", "CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + " And IsActive = 1 UNION SELECT WarehouseID,WarehouseName FROM InvWarehouse WHERE WarehouseID =" + MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intWarehouseID });
                    }
                    else
                    {
                        datCombos = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", "CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + " And IsActive = 1" });
                    } 
                    cboWarehouse.ValueMember = "WarehouseID";
                    cboWarehouse.DisplayMember = "WarehouseName";
                    cboWarehouse.DataSource = datCombos;
                }
                if (intFillType == 0 || intFillType == 7)
                {
                    //DataTable dtDiscount = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "DiscountID as DiscountTypeID,DiscountShortName", "InvDiscountReference", "" });
                    //Discount.ValueMember = "DiscountTypeID";
                    //Discount.DisplayMember = "DiscountShortName";
                    //Discount.DataSource = dtDiscount;
                    //DataRow dr = dtDiscount.NewRow();
                    //dr["DiscountTypeID"] = 0;
                    //dr["DiscountShortName"] = "No Discount";
                    //dtDiscount.Rows.InsertAt(dr, 0);
                    //Discount.DataSource = dtDiscount;

                    datCombos = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "ReasonID,Reason as Description", "InvReasonReference", "" });
                    Reason.ValueMember = "ReasonID";
                    Reason.DisplayMember = "Description";
                    Reason.DataSource = datCombos;
                }
                if(intFillType ==0 || intFillType == 8)
                {
                    datCombos=null;
                    datCombos = MobjclsBLLPurchaseReturn.FillCombos(new string[] {"StatusID,Status as Description","CommonStatusReference","StatusID In("+(Int32)OperationStatusType.PReturnOpened+","+(Int32)OperationStatusType.PReturnCancelled+")"});
                    CboSStatus.ValueMember = "StatusID";
                    CboSStatus.DisplayMember = "Description";
                    DataRow dr = datCombos.NewRow();
                    dr["StatusID"] = -2;
                    dr["Description"] = "ALL";
                    datCombos.Rows.InsertAt(dr, 0); 
                    CboSStatus.DataSource = datCombos;

                }
                if (intFillType == 0 || intFillType == 9)
                {
                    datCombos = null;
                    cboInvoiceNo.Text = string.Empty;
                    datCombos = MobjclsBLLPurchaseReturn.GetPurchaseInvoiceNo(cboCompany.SelectedValue.ToInt32(), cboSupplier.SelectedValue.ToInt32());
                    cboInvoiceNo.ValueMember = "PurchaseInvoiceID";
                    cboInvoiceNo.DisplayMember = "PurchaseInvoiceNo";
                    cboInvoiceNo.DataSource = datCombos;
                    cboInvoiceNo.SelectedIndex = -1;

                }
                if (intFillType == 0 || intFillType == 10)
                {

                    datCombos = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "AccountID,AccountName", "AccAccountMaster", "AccountGroupID=33" }); // For Tax Scheme
                    DataRow dr = datCombos.NewRow();
                    dr[0] = 0;
                    dr[1] = "None";
                    datCombos.Rows.InsertAt(dr, 0);
                    cboTaxScheme.ValueMember = "AccountID";
                    cboTaxScheme.DisplayMember = "AccountName";
                    cboTaxScheme.DataSource = datCombos;
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadCombos() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in LoadCombos() " + ex.Message, 2);
            }
            return false;
        }

        private void SetPermissions()
        {
            try
            {
                 //Function for setting permissions
                clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
                if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.DebitNote, out MbPrintEmailPermission, out MbAddPermission, out MbUpdatePermission, out MbDeletePermission);
                }
                else
                    MbAddPermission = MbCancelPermission = MbPrintEmailPermission = MbUpdatePermission = MbDeletePermission = true;
                MbCancelPermission = MbUpdatePermission;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SetPermissions() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in SetPermissions() " + ex.Message, 2);
            }
        }


        private void ClearControls()
        {

            ErrPurchaseReturn.Clear();
            dgvPurchase.Rows.Clear();
            dgvPurchase.ReadOnly = false;
            dgvPurchase.ClearSelection();
            dgvLocationDetails.Rows.Clear();
            CboSCompany.SelectedIndex = -1;
            CboSSupplier.SelectedIndex = -1;
            cboSExecutive.SelectedIndex = -1;
            cboSExecutive.Text = "";
            CboSStatus.SelectedIndex = -1;
            cboTaxScheme.SelectedValue = 0;

            if (ClsCommonSettings.CompanyID <= 0)
                ClsCommonSettings.CompanyID = MobjclsBLLCommonUtility.GetCompanyID();

            cboCompany.SelectedValue = ClsCommonSettings.CompanyID;
            cboSupplier.SelectedIndex = -1;
            cboInvoiceNo.SelectedIndex = -1;
            cboCurrency.SelectedIndex = -1;

            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.blnIsCancelled = false;

            txtRemarks.Text = txtVendorAddress.Text = txtDescription.Text = "";
            txtSubtotal.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            lblTaxAmount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            txtTotalAmount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            lblDiscountAmount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            lblExpenseAmount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            txtGrandReturnAmount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            txtExchangeCurrencyRate.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            dtpReturnDate.Value = ClsCommonSettings.GetServerDate();

            BindingNavigatorCancelItem.Text = "Cancel";
            lblStatus.Text = "New";
            lblStatus.ForeColor = Color.Black;
            //MBlnIsFromCancellation = false;
            cboCompany.Enabled = cboSupplier.Enabled = cboInvoiceNo.Enabled = true;
            cboCompany.Focus();
            MBlnEditMode = false;

            Uom.ReadOnly = true;
            BatchNo.ReadOnly = true;
            InvoicedQuantity.ReadOnly = true;
            ReceivedQty.ReadOnly = true;
            ExtraQuantity.ReadOnly = true;
            ReturnedQty.ReadOnly = true;
            Rate.ReadOnly = true;
            GrandAmount.ReadOnly = true;
            Discount.ReadOnly = true;
            DiscountAmount.ReadOnly = true;
            NetAmount.ReadOnly = true;
      
        }

        private void GeneratePurchaseReturnNo()
        {
            try
            {
              txtReturnNo.Text = GetPurchaseReturnPrefix() + (MobjclsBLLPurchaseReturn.GetLastReturnNo(cboCompany.SelectedValue.ToInt32())) ;
              lblPurchaseReturnCount.Text = "Total Purchase Returns :" + MobjclsBLLPurchaseReturn.GetLastReturnNo(cboCompany.SelectedValue.ToInt32());
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in GenerateOrderNo() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in GenerateOrderNo() " + ex.Message, 2);
            }
        }

      

        private void AddNew()
        {
            try
            {
                dtItemDetails = new DataTable();

                dtItemDetails.Columns.Add("ItemCode");
                dtItemDetails.Columns.Add("ItemName");
                dtItemDetails.Columns.Add("ItemID");
                dtItemDetails.Columns.Add("BatchID");
                dtItemDetails.Columns.Add("BatchNo");
                dtItemDetails.Columns.Add("UOMID");
                MstrMessageCommon = "Add new Debit Note";
                MobjclsBLLPurchaseReturn = new clsBLLPurchaseReturnMaster();
                ClearControls();
                GeneratePurchaseReturnNo();
                DisplayPurchaseReturnNumbersInSearchGrid();
                lblStatus.Text = "New";

                lblPurchaseReturnStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrFocus.Enabled = true;

                ErrPurchaseReturn.Clear();
                BindingNavigatorAddNewItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorClearItem.Enabled = true;
                BindingNavigatorCancelItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                btnPickList.Enabled = false;
                btnPickListEmail.Enabled = false;
                BtnPrint.Enabled = false;
                BtnEmail.Enabled = false;
                MBlnEditMode = false;
                txtDescription.Visible = lblDescription.Visible = false;
                lblCreatedByText.Text = " Created By : " + ClsCommonSettings.strEmployeeName;
                lblCreatedDateValue.Text = " Created Date : " + ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                tcPurchaseReturn.SelectedTab = tiItemDetails;
                MBlnCanShow = true;
                cboCompany.Focus();
                LoadCombos(2);
                LoadCombos(6);
               
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in AddNew() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in AddNew() " + ex.Message, 2);
            }
        }

        private string GetPurchaseReturnPrefix()
        {
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue));
            return ClsCommonSettings.PRPrefix;
        }

        private bool DisplayPurchaseReturnNumbersInSearchGrid()
        {
            try
            {
                DataTable dtPurchase = new DataTable();
                DataTable dtTemp = null;
                string strFilterCondition = string.Empty;

                if (CboSCompany.SelectedValue != null && Convert.ToInt32(CboSCompany.SelectedValue) !=-2)
                    strFilterCondition += " CompanyID = " + Convert.ToInt32(CboSCompany.SelectedValue);
                else
                {
                    dtTemp = null;
                    dtTemp = (DataTable)CboSCompany.DataSource;
                    if (dtTemp.Rows.Count > 0)
                    {
                        strFilterCondition += "CompanyID In (";
                        foreach (DataRow dr in dtTemp.Rows)
                        {
                            strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                        }
                        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                        strFilterCondition += ")";
                    }
                }
                if (CboSSupplier.SelectedValue != null && Convert.ToInt32(CboSSupplier.SelectedValue) !=-2)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "VendorID = " + Convert.ToInt32(CboSSupplier.SelectedValue);
                }
                else
                {
                    dtTemp = null;
                    dtTemp = (DataTable)CboSSupplier.DataSource;
                    if (dtTemp.Rows.Count > 0)
                    {
                        strFilterCondition += " And VendorID In (";
                        foreach (DataRow dr in dtTemp.Rows)
                        {
                            strFilterCondition += Convert.ToInt32(dr["VendorID"]) + ",";
                        }
                        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                        strFilterCondition += ")";
                    }
                }
                if (cboSExecutive.SelectedValue != null && Convert.ToInt32(cboSExecutive.SelectedValue)!=-2)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "CreatedBy = " + Convert.ToInt32(cboSExecutive.SelectedValue);
                }
                //else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                //{
                //    //if (cboSExecutive.SelectedValue != null && Convert.ToInt32(cboSExecutive.SelectedValue) != -2)
                //    //{
                //        dtTemp = null;
                //        dtTemp = (DataTable)cboSExecutive.DataSource;
                //        if (dtTemp.Rows.Count > 0)
                //        {
                //            //clsBLLPermissionSettings MobjClsBLLPermissionSettings = new clsBLLPermissionSettings();
                //            //DataTable datTempPer = MobjClsBLLPermissionSettings.GetControlPermissions1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.DelNoteEmployee);
                //            if (datTempPer != null)
                //            {
                //                if (datTempPer.Rows.Count > 0)
                //                {
                //                    if (datTempPer.Rows[0]["IsVisible"].ToString() == "True")
                //                        strFilterCondition += "And EmployeeID In (0,";
                //                    else
                //                        strFilterCondition += "And EmployeeID In (";
                //                }
                //                else
                //                    strFilterCondition += "And EmployeeID In (";
                //            }
                //            else
                //                strFilterCondition += "And EmployeeID In (";
                //            foreach (DataRow dr in dtTemp.Rows)
                //            {
                //                strFilterCondition += Convert.ToInt32(dr["EmployeeID"]) + ",";
                //            }
                //            strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                //            strFilterCondition += ")";
                //        }
                //    //}
                //}
                if (CboSStatus.SelectedValue != null && Convert.ToInt32(CboSStatus.SelectedValue) !=-2)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "StatusID = " + Convert.ToInt32(CboSStatus.SelectedValue);
                }
                if (!string.IsNullOrEmpty(strFilterCondition))
                    strFilterCondition += " And ";
                strFilterCondition += " ReturnDate >= '" + dtpSFrom.Value.ToString("dd-MMM-yyyy") + "' And ReturnDate <= '" + dtpSTo.Value.ToString("dd-MMM-yyyy") + "'";
                DgvSOrderDisplay.DataSource = null;
                        if (!string.IsNullOrEmpty(TxtSsearch.Text.Trim()))
                        {
                            if (!string.IsNullOrEmpty(strFilterCondition))
                                strFilterCondition += " And ";
                            strFilterCondition += " ReturnNo = '" + TxtSsearch.Text.Trim() + "'";
                        }
                        dtPurchase = MobjclsBLLPurchaseReturn.GetReturnNumbers();
                        dtPurchase.DefaultView.RowFilter = strFilterCondition;
                        DgvSOrderDisplay.DataSource = dtPurchase.DefaultView.ToTable();

                DgvSOrderDisplay.Columns[0].Visible = false;
                DgvSOrderDisplay.Columns[2].Visible = false;
                DgvSOrderDisplay.Columns[3].Visible = false;
                DgvSOrderDisplay.Columns[4].Visible = false;
                DgvSOrderDisplay.Columns[5].Visible = false;
                DgvSOrderDisplay.Columns[6].Visible = false;
                DgvSOrderDisplay.Columns[7].Visible = false;
                DgvSOrderDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                lblPurchaseReturnCount.Text = "Total Purchase Returns : " + DgvSOrderDisplay.Rows.Count;
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayPurchaseReturnNumbersInSearchGrid() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in DisplayPurchaseReturnNumbersInSearchGrid() " + ex.Message, 2);
            }
            return false;
        }

        private void Changestatus(object sender, EventArgs e)
        {
            if (MBlnEditMode)
            {
                BindingNavigatorAddNewItem.Enabled = MbAddPermission;
                if (MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intStatusID == (Int32)OperationStatusType.PReturnCancelled)
                {
                    BindingNavigatorSaveItem.Enabled = false;
                }
                else
                {
                    BindingNavigatorSaveItem.Enabled = MbUpdatePermission;
                }
            }
            else
            {
                BindingNavigatorSaveItem.Enabled = MbAddPermission;
               
            }
            ErrPurchaseReturn.Clear();
        }
     
        private void CalculateNetTotal()
        {
            try
            {
                double dSubTotal = 0;
                double dNetAmount = 0;
                double dTaxAmount = 0;
                decimal taxValue = 0;
                dSubTotal = txtSubtotal.Text.ToDouble();
                double dBTotal = dSubTotal;
               
                if (dNetAmount < 0)
                        dNetAmount = 0;
                if (cboTaxScheme.SelectedValue.ToInt32() > 0)
                {
                    taxValue = MobjclsBLLPurchaseReturn.getTaxValue(cboTaxScheme.SelectedValue.ToInt32());
                    dTaxAmount = ((dSubTotal.ToDecimal() * taxValue) / 100).ToDouble();
                }
                dNetAmount = dSubTotal + dTaxAmount;
                lblTaxAmount.Text = dTaxAmount.ToString("F" + MintExchangeCurrencyScale);
                txtTotalAmount.Text = dNetAmount.ToString("F" + MintExchangeCurrencyScale);
                txtGrandReturnAmount.Text = dNetAmount.ToString("F" + MintExchangeCurrencyScale);
                CalculateExchangeCurrencyRate();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CalculateBNetNotal() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in CalculateBNetTotal() " + ex.Message, 2);
            }
        }

       

        private void CalculateExchangeCurrencyRate()
        {
            int intCompanyID = 0, intCurrencyID = 0;

            intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            intCurrencyID = Convert.ToInt32(cboCurrency.SelectedValue);
            decimal decExchangeCurrencyRate = MobjclsBLLPurchaseReturn.GetExchangeCurrencyRate(intCompanyID, intCurrencyID);

            if (txtGrandReturnAmount.Text != string.Empty && txtGrandReturnAmount.Text != ".")
            {
                if (decExchangeCurrencyRate > 0)
                {
                    txtExchangeCurrencyRate.Text = Convert.ToDecimal(decExchangeCurrencyRate * Convert.ToDecimal(txtGrandReturnAmount.Text)).ToString("F" + MintBaseCurrencyScale);
                }
                else
                {
                    txtExchangeCurrencyRate.Text = txtGrandReturnAmount.Text.ToDecimal().ToString("F" + MintExchangeCurrencyScale);

                }
            }
        }
        private void CalcuateGridTotal()
        {
            
            try
            {
                    double dGrandAmount = 0;
                    if (dgvPurchase.RowCount > 0)
                    {
                        int iGridRowCount = dgvPurchase.RowCount;
                        iGridRowCount = iGridRowCount - 1;

                        for (int i = 0; i < iGridRowCount; i++)
                        {
                            double dGridNetAmount = 0;
                            if (dgvPurchase.Rows[i].Cells["ItemID"].Value.ToInt32() != 0)
                            {
                                int iItemID = dgvPurchase.Rows[i].Cells["ItemID"].Value.ToInt32();
                                double dQty = 0;
                                double decDiscountAmount = dgvPurchase.Rows[i].Cells["DiscountAmount"].Value.ToDouble();
                                dQty = dgvPurchase.Rows[i].Cells["ReturnQuantity"].Value.ToDouble();
                                double dDirectRate = dgvPurchase.Rows[i].Cells["Rate"].Value.ToDouble();
                                double dGridGrandAmount = dQty * dDirectRate;
                                if (dQty > 0 && dDirectRate > 0)
                                {
                                    dGridNetAmount = (dDirectRate * dQty) - (decDiscountAmount);
                                }

                                dgvPurchase.Rows[i].Cells["GrandAmount"].Value = dGridGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                                dgvPurchase.Rows[i].Cells["NetAmount"].Value = dGridNetAmount.ToString("F" + MintExchangeCurrencyScale);
                                dgvPurchase.Rows[i].Cells["ReturnAmount"].Value = dGridNetAmount.ToString("F" + MintExchangeCurrencyScale);
                                dGrandAmount += dGridNetAmount;
                            }
                            else
                            {
                                dGrandAmount += 0;
                                dgvPurchase.Rows[i].Cells["GrandAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                                dgvPurchase.Rows[i].Cells["NetAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                                dgvPurchase.Rows[i].Cells["ReturnAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                                dgvPurchase.Rows[i].Cells["DiscountAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                            }
                        }
                    }

                    txtSubtotal.Text = dGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                    CalculateNetTotal();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CalculateTotalAmt() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in CalculateTotalAmt() " + ex.Message, 2);
            }
        }
        private void CalculateTotalAmt()
        {
            try
            {
                    double dGrandAmount = 0;
                    if (dgvPurchase.RowCount > 0)
                    {
                        int iGridRowCount = dgvPurchase.RowCount;
                        iGridRowCount = iGridRowCount - 1;

                        for (int i = 0; i < iGridRowCount; i++)
                        {
                            double dGridNetAmount = 0;
                            if (dgvPurchase.Rows[i].Cells["ItemID"].Value.ToInt32() != 0)
                            {
                                int iItemID = dgvPurchase.Rows[i].Cells["ItemID"].Value.ToInt32();
                                int iDiscountID = dgvPurchase.Rows[i].Cells["Discount"].Value.ToInt32();
                                double dQty = 0;
                                double dRate = 0, decDiscountAmount = 0;
                                dQty = dgvPurchase.Rows[i].Cells["ReturnQuantity"].Value.ToDouble();
                                double dDirectRate = dgvPurchase.Rows[i].Cells["Rate"].Value.ToDouble();
                                double dGridGrandAmount = dQty * dDirectRate;
                                if (dQty > 0 && dDirectRate > 0)
                                {
                                    bool blnIsDiscountForAmount = MobjclsBLLPurchaseReturn.IsDiscountForAmount(iDiscountID);
                                    if (!blnIsDiscountForAmount)
                                    {
                                        if (iDiscountID > 0 && dDirectRate > 0 && iItemID > 0)
                                            dRate = MobjclsBLLPurchaseReturn.GetItemDiscountAmount(3, iItemID, iDiscountID, dDirectRate);
                                        else
                                            dRate = dDirectRate;
                                        decDiscountAmount = dDirectRate - dRate;
                                        decDiscountAmount = decDiscountAmount * dQty;
                                        dGridNetAmount = dRate * dQty;
                                    }
                                    else
                                    {
                                        dGridNetAmount = dDirectRate * dQty;
                                        if (iDiscountID > 0 && dDirectRate > 0 && iItemID > 0)
                                            dGridNetAmount = MobjclsBLLPurchaseReturn.GetItemDiscountAmount(3, iItemID, iDiscountID, dGridNetAmount);
                                        decDiscountAmount = (dDirectRate * dQty) - dGridNetAmount;
                                    }
                                }

                                // if (dGridNetAmount >= 0)
                                // {
                                dgvPurchase.Rows[i].Cells["GrandAmount"].Value = dGridGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                                dgvPurchase.Rows[i].Cells["NetAmount"].Value = dGridNetAmount.ToString("F" + MintExchangeCurrencyScale);
                                dgvPurchase.Rows[i].Cells["ReturnAmount"].Value = dGridNetAmount.ToString("F" + MintExchangeCurrencyScale);
                                //dgvPurchase.Rows[i].Cells["DiscountAmount"].Value = decDiscountAmount.ToString("F" + MintExchangeCurrencyScale);
                                dGrandAmount += dGridNetAmount;
                                //  }
                                //else
                                //{
                                //    dgvPurchase.Rows[i].Cells["GrandAmount"].Value = dGridGrandAmount.ToString("F"+MintExchangeCurrencyScale);
                                //    dgvPurchase.Rows[i].Cells["NetAmount"].Value = dGridNetAmount.ToString("F"+MintExchangeCurrencyScale);
                                //    dgvPurchase.Rows[i].Cells["DiscountAmount"].Value = decDiscountAmount.ToString("F"+MintExchangeCurrencyScale);
                                //    TxtBSubtotal.Text = dGrandAmount.ToString("F"+MintExchangeCurrencyScale);
                                //    TxtBTotalAmount.Text = dGrandAmount.ToString("F"+MintExchangeCurrencyScale);
                                //    MsMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 947, out MmessageIcon);
                                //    MessageBox.Show(MsMessageCommon.Replace("#", ""));
                                //}
                            }
                            else
                            {
                                dGrandAmount += 0;
                                dgvPurchase.Rows[i].Cells["GrandAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                                dgvPurchase.Rows[i].Cells["NetAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                                dgvPurchase.Rows[i].Cells["ReturnAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                                dgvPurchase.Rows[i].Cells["DiscountAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                            }
                        }
                    }

                    txtSubtotal.Text = dGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                    CalculateNetTotal();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CalculateTotalAmt() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in CalculateTotalAmt() " + ex.Message, 2);
            }
        }

       

        private void BtnUom_Click(object sender, EventArgs e)
        {
            using (FrmUnitOfMeasurement objUoM = new FrmUnitOfMeasurement())
            {
                objUoM.ShowDialog();
            }
        }

        private void FillUOMColumn(int intItemID)
        {
            DataTable dtItemUOMs = MobjclsBLLPurchaseReturn.GetItemUOMs(intItemID);
            Uom.DataSource = null;
            Uom.ValueMember = "UOMID";
            Uom.DisplayMember = "ShortName";
            Uom.DataSource = dtItemUOMs;
        }

        private void DisplayPurchaseReturnDetDetails(long lngPurchaseReturnID)
        {
            try
            {
                dgvPurchase.Rows.Clear();
                dgvPurchase.RowCount = 1;
                DataTable datPurchaseDetail = null;

                        datPurchaseDetail = MobjclsBLLPurchaseReturn.GetReturnDetDetails(lngPurchaseReturnID);

                        if (dgvPurchase.Rows.Count > 0)
                        {
                            dtItemDetails.Rows.Clear();

                            for (int intICounter = 0; intICounter < datPurchaseDetail.Rows.Count; intICounter++)
                            {
                                int intItemID = Convert.ToInt32(datPurchaseDetail.Rows[intICounter]["ItemID"]);
                                int intVendorID = Convert.ToInt32(cboSupplier.SelectedValue);
                                FillUOMColumn(intItemID);

                                dgvPurchase.RowCount = dgvPurchase.RowCount + 1;
                                dgvPurchase.Rows[intICounter].Cells["ReferenceSerialNo"].Value = datPurchaseDetail.Rows[intICounter]["ReferenceSerialNo"];
                                dgvPurchase.Rows[intICounter].Cells["ItemID"].Value = datPurchaseDetail.Rows[intICounter]["ItemID"];
                                dgvPurchase.Rows[intICounter].Cells["ItemCode"].Value = datPurchaseDetail.Rows[intICounter]["Code"];
                                dgvPurchase.Rows[intICounter].Cells["ItemName"].Value = datPurchaseDetail.Rows[intICounter]["ItemName"];
                                dgvPurchase.Rows[intICounter].Cells["BatchID"].Value = datPurchaseDetail.Rows[intICounter]["BatchID"];
                                dgvPurchase.Rows[intICounter].Cells["BatchNo"].Value = datPurchaseDetail.Rows[intICounter]["BatchNo"];
                                dgvPurchase.Rows[intICounter].Cells["InvoicedQuantity"].Value = datPurchaseDetail.Rows[intICounter]["InvoicedQuantity"];
                                dgvPurchase.Rows[intICounter].Cells["ExtraQuantity"].Value = datPurchaseDetail.Rows[intICounter]["ExtraQuantity"];
                                dgvPurchase.Rows[intICounter].Cells["ReceivedQty"].Value = datPurchaseDetail.Rows[intICounter]["ReceivedQuantity"];

                                dgvPurchase.Rows[intICounter].Cells["PurchaseRate"].Value = datPurchaseDetail.Rows[intICounter]["PurchaseRate"];


                                dgvPurchase.Rows[intICounter].Cells["InvRate"].Value = datPurchaseDetail.Rows[intICounter]["InvRate"];
                                dgvPurchase.Rows[intICounter].Cells["InvUomID"].Value = datPurchaseDetail.Rows[intICounter]["InvUomID"];
                                dgvPurchase.Rows[intICounter].Cells["InvQty"].Value = datPurchaseDetail.Rows[intICounter]["InvQty"];
                                dgvPurchase.Rows[intICounter].Cells["ReturnQuantity"].Value = datPurchaseDetail.Rows[intICounter]["ReturnQuantity"];
                                dgvPurchase.Rows[intICounter].Cells["TempExtraQty"].Value = datPurchaseDetail.Rows[intICounter]["ExtraQty"];

                                dgvPurchase.Rows[intICounter].Cells["Rate"].Value = datPurchaseDetail.Rows[intICounter]["Rate"];
                                dgvPurchase.Rows[intICounter].Cells["Uom"].Tag = datPurchaseDetail.Rows[intICounter]["UOMID"];
                                dgvPurchase.Rows[intICounter].Cells["Uom"].Value = datPurchaseDetail.Rows[intICounter]["UOMID"];
                                dgvPurchase.Rows[intICounter].Cells["Uom"].Value = dgvPurchase.Rows[intICounter].Cells["Uom"].FormattedValue;                                
                                dgvPurchase.Rows[intICounter].Cells["DiscountAmount"].Value = datPurchaseDetail.Rows[intICounter]["DiscountAmount"];
                                dgvPurchase.Rows[intICounter].Cells["NetAmount"].Value = datPurchaseDetail.Rows[intICounter]["NetAmount"];
                                dgvPurchase.Rows[intICounter].Cells["ReturnAmount"].Value = datPurchaseDetail.Rows[intICounter]["ReturnAmount"];
                                dgvPurchase.Rows[intICounter].Cells["GrandAmount"].Value = datPurchaseDetail.Rows[intICounter]["GrandAmount"];

                                dgvPurchase.Rows[intICounter].Cells["Reason"].Value = datPurchaseDetail.Rows[intICounter]["ReasonID"];
                                //dgvPurchase.Rows[intICounter].Cells["ReturnedQty"].Value = MobjclsBLLPurchaseReturn.GetAlreadyReturnedQty(cboInvoiceNo.SelectedValue.ToInt64(), datPurchaseDetail.Rows[intICounter]["ReferenceSerialNo"].ToInt32());
                                DataRow drItemDetailsRow = dtItemDetails.NewRow();
                                drItemDetailsRow["ItemID"] = datPurchaseDetail.Rows[intICounter]["ItemID"].ToInt32();
                                drItemDetailsRow["ItemCode"] = datPurchaseDetail.Rows[intICounter]["Code"].ToString();
                                drItemDetailsRow["ItemName"] = datPurchaseDetail.Rows[intICounter]["ItemName"].ToString();
                                drItemDetailsRow["BatchID"] = datPurchaseDetail.Rows[intICounter]["BatchID"].ToInt64();
                                drItemDetailsRow["BatchNo"] = datPurchaseDetail.Rows[intICounter]["BatchNo"].ToString();
                                drItemDetailsRow["UomID"] = datPurchaseDetail.Rows[intICounter]["UOMID"].ToInt32();
                                dtItemDetails.Rows.Add(drItemDetailsRow);
                                LoadCombos(7);
                                //if (datPurchaseDetail.Rows[intICounter]["DiscountID"] != DBNull.Value && Convert.ToInt32(datPurchaseDetail.Rows[intICounter]["DiscountID"]) != 0)
                                //{
                                //    dgvPurchase.Rows[intICounter].Cells["Discount"].Tag = datPurchaseDetail.Rows[intICounter]["DiscountID"];
                                //    dgvPurchase.Rows[intICounter].Cells["Discount"].Value = datPurchaseDetail.Rows[intICounter]["DiscountID"];
                                //}
                                dgvPurchase.Rows[intICounter].Cells["DiscountAmount"].Value = datPurchaseDetail.Rows[intICounter]["DiscountAmount"];

                            }
                            //CalculateTotalAmt();
                            CalculateNetTotal();
                        }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayPurchaseDetDetails() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in DisplayPurchaseDetDetails() " + ex.Message, 2);
            }
        }

        private bool DisplayPurchaseReturnMasterDetails(long intOrderID)
        {
            try
            {
                        if (MobjclsBLLPurchaseReturn.GetPurchaseReturnMasterDetails(intOrderID))
                        {
                            blnCalculate = false;
                            MBlnEditMode = true;
                            txtReturnNo.Tag = MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intPurchaseReturnID;
                            cboCompany.SelectedValue = MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intCompanyID;
                            if (cboCompany.SelectedValue == null)
                            {
                                DataTable datTemp = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intCompanyID });
                                if (datTemp.Rows.Count > 0)
                                    cboCompany.Text = datTemp.Rows[0]["Name"].ToString();
                            }
                            LoadCombos(2);
                            LoadCombos(6);
                            cboWarehouse.SelectedValue = MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intWarehouseID;
                            cboSupplier.SelectedValue = MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intVendorID;
                            txtReturnNo.Text = MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.strPurchaseReturnNo;
                            DataTable dtTemp = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "PurchaseInvoiceID,PurchaseInvoiceNo", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intPurchaseInvoiceID });
                            cboInvoiceNo.ValueMember = "PurchaseInvoiceID";
                            cboInvoiceNo.DisplayMember = "PurchaseInvocieNo";
                            cboInvoiceNo.DataSource = dtTemp;
                            cboInvoiceNo.SelectedValue = MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intPurchaseInvoiceID;
                            //txtVendorAddress.Text = MobjclsBLLPurchaseReturn.GetVendorAddress(MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intVendorAddID);
                            dtpReturnDate.Value = Convert.ToDateTime(MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.strReturnDate);
                            cboCurrency.SelectedValue = MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intCurrencyID;
                            txtRemarks.Text = MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.strRemarks;
                            cboAccount.SelectedValue = MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intAccountID;
                            cboTaxScheme.SelectedValue=MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intTaxSchemeID;
                            DisplayPurchaseReturnDetDetails(intOrderID);
                            //DisplayLocationDetails();
                            lblTaxAmount.Text = MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.decTaxAmount.ToString("F" + MintExchangeCurrencyScale);
                            txtSubtotal.Text = MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.decGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                            txtTotalAmount.Text = MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.decNetAmount.ToString("F" + MintExchangeCurrencyScale);
                            txtGrandReturnAmount.Text = MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.decGrandReturnAmount.ToString("F" + MintExchangeCurrencyScale);

                            lblCreatedByText.Text = " Created By : " + MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.strCreatedBy;
                            lblCreatedDateValue.Text = " Created Date : " + MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.strCreatedDate.ToDateTime().ToString("dd-MMM-yyyy");

                            BindingNavigatorSaveItem.Enabled = MbUpdatePermission;
                            BindingNavigatorAddNewItem.Enabled = MbAddPermission;
                            BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
                            if (MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intStatusID == (Int32)OperationStatusType.PReturnOpened)
                            {
                                lblStatus.ForeColor = Color.Brown;
                                lblStatus.Text = "Open";
                                BindingNavigatorCancelItem.Text = "Cancel";
                                BindingNavigatorCancelItem.Enabled = MbCancelPermission;
                                txtDescription.Visible = lblDescription.Visible = false;
                                dgvPurchase.ReadOnly = false;
                            }
                            else
                            {
                                lblStatus.ForeColor = Color.Red;
                                lblStatus.Text = "Cancelled";
                                BindingNavigatorCancelItem.Text = "ReOpen";
                                BindingNavigatorCancelItem.Enabled = MbCancelPermission;
                                BindingNavigatorSaveItem.Enabled = false;
                                txtDescription.Text = MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.strCancellationDescription;
                                txtDescription.Visible = lblDescription.Visible = true;
                                lblCreatedByText.Text += "     |    Cancelled By : " + MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.strCancelledBy;
                                lblCreatedDateValue.Text += "     |   Cancelled Date : " + MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.strCancellationDate.ToDateTime().ToString("dd-MMM-yyyy");
                                dgvPurchase.ReadOnly = true;
                            }
                            cboCompany.Enabled = cboSupplier.Enabled = cboInvoiceNo.Enabled = false;
                            blnCalculate = true;
                            return true;
                        }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayReturnMasterDetails() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in DisplayPurchaseReturnMasterDetails() " + ex.Message, 2);
            }
            return false;
        }


        private void FrmPurchaseReturn_Load(object sender, EventArgs e)
        {
            try
            {
                SetPermissions();
                LoadMessage();
                LoadCombos(0);
                AddNew();

                if (!ClsCommonSettings.PblnIsLocationWise)
                {
                    tiLocationDetails.Visible = false;
                    btnPickList.Visible = false;
                    btnPickListEmail.Visible = false;
                }
                if (PlngReferenceID > 0)
                {
                    DisplayPurchaseReturnMasterDetails(PlngReferenceID);
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FrmPurchaseReturn_Load() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in FrmPurchaseReturn_Load() " + ex.Message, 2);
            }
        }



        private void dgvPurchase_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                    dgvPurchase.PServerName = ClsCommonSettings.ServerName;
                    string[] First = { "ItemCode", "ItemName", "ItemID", "ReferenceSerialNo", "BatchID", "BatchNo", "InvoicedQuantity", "ReceivedQty", "ExtraQuantity", "InvQty", "TempExtraQty", "InvUomID", "InvRate", "PurchaseRate" ,"Discount","DiscountAmount"};//first grid 
                    string[] second = { "ItemCode", "ItemName", "ItemID", "ReferenceSerialNo", "BatchID", "BatchNo", "InvoicedQuantity", "ReceivedQuantity", "ExtraQuantity", "InvQty", "ExtraQty", "InvUomID", "InvRate","PurchaseRate", "Discount", "DiscountAmount" };//inner grid
                    dgvPurchase.aryFirstGridParam = First;
                    dgvPurchase.arySecondGridParam = second;
                    dgvPurchase.PiFocusIndex = ReturnQuantity.Index;
                    dgvPurchase.iGridWidth = 500;
                    dgvPurchase.bBothScrollBar = true;
                    dgvPurchase.ColumnsToHide = new string[] { "ItemID", "ReferenceSerialNo", "BatchID", "BatchNo","InvQty", "ExtraQty", "ReceivedQty", "WarehouseID", "InvRate", "PurchaseRate","InvUomID", "Discount" };
                    

                    
                    DataTable dtItemSelectionTable = MobjclsBLLPurchaseReturn.GetPurchaseInvoiceItemDetails(Convert.ToInt32(cboInvoiceNo.SelectedValue));
                    string strFilterCondition = "";
                    if (dgvPurchase.CurrentCell.OwningColumn.Index == 0)
                    {
                        dgvPurchase.field = "ItemCode";
                        strFilterCondition = " ItemCode Like '%" + ((dgvPurchase.TextBoxText)) + "%'";
                    }
                    else
                    {
                        dgvPurchase.field = "ItemName";
                        strFilterCondition = " ItemName Like '%" + ((dgvPurchase.TextBoxText)) + "%'";
                    }

                    //List<int> lstSelectedReferenceSerialNos = new List<int>();
                    //foreach (DataGridViewRow dr in dgvPurchase.Rows)
                    //{
                    //    if (dr.Cells["ReferenceSerialNo"].Value != null && Convert.ToInt32(dr.Cells["ReferenceSerialNo"].Value) > 0)
                    //    {
                    //        lstSelectedReferenceSerialNos.Add(Convert.ToInt32(dr.Cells["ReferenceSerialNo"].Value));
                    //    }
                    //}

                    //if (lstSelectedReferenceSerialNos.Count > 0)
                    //{
                    //    if (!string.IsNullOrEmpty(strFilterCondition))
                    //        strFilterCondition += " And ";
                    //    strFilterCondition += "ReferenceSerialNo Not In (";
                    //    foreach (int intReferenceSerialNo in lstSelectedReferenceSerialNos)
                    //    {
                    //        strFilterCondition += intReferenceSerialNo + ",";
                    //    }
                    //    strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                    //    strFilterCondition += ")";
                    //}

                    dtItemSelectionTable.DefaultView.RowFilter = strFilterCondition;
                    DataTable datTemp = dtItemSelectionTable.DefaultView.ToTable();
                    datTemp.DefaultView.Sort = dgvPurchase.field;
                    dgvPurchase.dtDataSource = datTemp.DefaultView.ToTable();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvPurchase_Textbox_TextChanged() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvPurchase_Textbox_TextChanged() " + ex.Message, 2);
            }
        }
        private bool ValidateFields()
        {
            try
            {               
                // Validating datas
                if (dgvPurchase.CurrentCell != null)
                    dgvPurchase.CurrentCell = dgvPurchase["ItemCode", dgvPurchase.CurrentRow.Index];
                bool blnValid = true;
                Control cntrlFocus = null;
                ErrPurchaseReturn.Clear();
                DataTable dtInvoice = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "InvoiceDate", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + Convert.ToInt32(cboInvoiceNo.SelectedValue) });

                if (cboCompany.SelectedValue == null)
                {
                    //Please select Company
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 14, out MmessageIcon);
                    cntrlFocus = cboCompany;
                    blnValid = false;
                }
                else if (cboSupplier.SelectedValue == null)
                {
                    //Please select customer
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 908, out MmessageIcon);
                    cntrlFocus = cboSupplier;
                    blnValid = false;
                }

                else if (cboInvoiceNo.SelectedValue == null)
                {
                    //Please select purchase invoice
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 921, out MmessageIcon);
                    cntrlFocus = cboInvoiceNo;
                    blnValid = false;
                }
                else if (MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intPurchaseReturnID == 0 && (MobjclsBLLPurchaseReturn.FillCombos(new string[] { "PurchaseInvoiceID", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + Convert.ToInt32(cboInvoiceNo.SelectedValue) + " And StatusID <> " + (Int32)OperationStatusType.PInvoiceCancelled }).Rows.Count == 0))
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 921, out MmessageIcon);
                    //MstrMessageCommon = MstrMessageCommon.Replace("*", "PurchaseInvoice");
                    cntrlFocus = cboInvoiceNo;
                    blnValid = false;
                }
                else if (cboCurrency.SelectedValue == null)
                {
                    //Please select currency
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 939, out MmessageIcon);
                    cntrlFocus = cboCurrency;
                    blnValid = false;
                }

                else if (dtpReturnDate.Value.Date > ClsCommonSettings.GetServerDate())
                {
                    // return date cannot be greater than current date
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 9006, out MmessageIcon).Replace("*","Return");
                    cntrlFocus = dtpReturnDate;
                    blnValid = false;
                }

                else if (dtInvoice != null && dtInvoice.Rows.Count > 0 && dtInvoice.Rows[0]["InvoiceDate"] != null && (dtpReturnDate.Value.Date < Convert.ToDateTime(dtInvoice.Rows[0]["InvoiceDate"])))
                {
                    txtRemarks.Focus();
                    dtpReturnDate.Focus();
                    // return date cannot be greater than current date
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages,2277, out MmessageIcon);
                    TmrPurchaseReturn.Enabled = true;
                    cntrlFocus = dtpReturnDate;
                    dtpReturnDate.Focus();
                    blnValid = false;
                }
                else if (cboAccount.SelectedValue == null)
                {
                    //Please select Account 

                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 921, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("Invoice", "Account");
                    cntrlFocus = cboAccount;
                    blnValid = false;
                }
                else if (dgvPurchase.Rows.Count == 1)
                {
                    //Please select items
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 901, out MmessageIcon);
                    cntrlFocus = dgvPurchase;
                    blnValid = false;
                }
                else if (!MBlnEditMode && MobjclsBLLPurchaseReturn.IsPurchaseReturnExists(cboCompany.SelectedValue.ToInt32(), txtReturnNo.Text.Trim()))
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 973, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Return");
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    else
                        GeneratePurchaseReturnNo();
                }
                else if ((txtGrandReturnAmount.Text == string.Empty || txtGrandReturnAmount.Text.Trim() == "."))
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 2296, out MmessageIcon);
                    cntrlFocus = txtGrandReturnAmount;
                    blnValid = false;
                }
                else if (Convert.ToDecimal(txtGrandReturnAmount.Text) <= 0)
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 2296, out MmessageIcon);
                    cntrlFocus = txtGrandReturnAmount;
                    blnValid = false;
                }
                
                
                if (!blnValid)
                {
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErrPurchaseReturn.SetError(cntrlFocus, MstrMessageCommon.Replace("#", "").Trim());
                    lblPurchaseReturnStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                    TmrPurchaseReturn.Enabled = true;
                    tcPurchaseReturn.SelectedTab = tiItemDetails;
                    cntrlFocus.Focus();
                }
                else if (!ValidateGrid())
                {
                    blnValid = false;
                }
                else if (ClsCommonSettings.PblnIsLocationWise && (!ValidateLocationGrid() || !CheckLocationDuplication()))
                {
                    blnValid = false;
                }
                return blnValid;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in ValidateFields() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in ValidateFields() " + ex.Message, 2);
                return false;
            }
        }

        private int CheckDuplicationInGrid()
        {
            int intItemID = 0;
            long lngBatchID = 0;
            int RowIndexTemp = 0;

            foreach (DataGridViewRow rowValue in dgvPurchase.Rows)
            {
                 int intCostingMethodID = GetCostingMethod(rowValue.Cells["ItemID"].Value.ToInt32());

                 if (intCostingMethodID == (Int32)CostingMethodReference.Batch)
                 {
                     if (rowValue.Cells[ItemID.Index].Value != null && rowValue.Cells[BatchID.Index].Value != null)
                     {
                         intItemID = Convert.ToInt32(rowValue.Cells[ItemID.Index].Value);
                         lngBatchID = rowValue.Cells[BatchID.Index].Value.ToInt64();
                         RowIndexTemp = rowValue.Index;
                         foreach (DataGridViewRow row in dgvPurchase.Rows)
                         {
                             if (RowIndexTemp != row.Index)
                             {
                                 if (row.Cells[ItemID.Index].Value != null && Convert.ToInt32(row.Cells[BatchID.Index].Value) > 0)
                                 {
                                     if ((Convert.ToInt32(row.Cells[ItemID.Index].Value) == intItemID) && (row.Cells[BatchID.Index].Value.ToInt64() == lngBatchID))
                                         return row.Index;
                                 }
                             }
                         }
                     }
                 }
                 else
                 {
                     if (rowValue.Cells[ItemID.Index].Value != null && rowValue.Cells[BatchID.Index].Value != null)
                     {
                         intItemID = Convert.ToInt32(rowValue.Cells[ItemID.Index].Value);
                         RowIndexTemp = rowValue.Index;
                         foreach (DataGridViewRow row in dgvPurchase.Rows)
                         {
                             if (RowIndexTemp != row.Index)
                             {
                                 if (row.Cells[ItemID.Index].Value != null)
                                 {
                                     if ((Convert.ToInt32(row.Cells[ItemID.Index].Value)) == intItemID)
                                         return row.Index;
                                 }
                             }
                         }
                     }
                 }

            }
            return -1;
        }

        private int GetCostingMethod(int intItemID)
        {
            int intCostingMethodID = 0;
            DataTable dtCostingMethod = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + intItemID });
            if (dtCostingMethod != null && dtCostingMethod.Rows.Count > 0)
            {
                intCostingMethodID = dtCostingMethod.Rows[0]["CostingMethodID"].ToInt32();
            }
            return intCostingMethodID;
        }
        private bool ValidateCompanyAccount()
        {
            DataTable datTemp = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "*", "AccountSettings", "CompanyID=" + cboCompany.SelectedValue.ToString() + " AND OperationModID=" + (int)TransactionTypes.PurchaseReturn });
            if (datTemp == null)
            {
                //MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 990, out MmessageIcon);
                //MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //lblPurchaseReturnStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //TmrPurchaseReturn.Enabled = true;
                return false;
            }
            else
            {
                if (datTemp.Rows.Count == 0)
                {
                    //MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 990, out MmessageIcon);
                    //MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    //lblPurchaseReturnStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    //TmrPurchaseReturn.Enabled = true;
                    return false;
                }
            }
            return true;
        }

        private decimal ConvertValueToBaseUnitValue( int intUOMID,int intItemID,decimal decValue)
        {
            DataTable dtUomDetails = MobjclsBLLPurchaseReturn.GetUomConversionValues(intUOMID, intItemID);
            if (dtUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                    decValue = decValue / decConversionValue;
                else if (intConversionFactor == 2)
                    decValue = decValue * decConversionValue;
            }
            return decValue;
        }

        private bool ValidateGrid()
        {
            bool blnIsValid = true;
            int intItemCount = 0, intRowIndex = 0;
            string strColumnName = "";

            for (int i = 0; i < dgvPurchase.Rows.Count; i++)
            {
                if (dgvPurchase["ReferenceSerialNo", i].Value != null)
                {
                    if (Convert.ToString(dgvPurchase.Rows[i].Cells["ItemCode"].Value).Trim() == "")
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 903, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        intRowIndex = i;
                        strColumnName = "ItemCode";
                        blnIsValid = false;
                        return false;
                    }
                    if (Convert.ToString(dgvPurchase.Rows[i].Cells["ItemName"].Value).Trim() == "")
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 915, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        intRowIndex = i;
                        strColumnName = "ItemCode";
                        blnIsValid = false;
                        return false;
                    }
                    if (dgvPurchase.Rows.Count > 0)
                    {
                        int iTempID = 0;
                        iTempID = CheckDuplicationInGrid();
                        if (iTempID != -1)
                        {
                            MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 902, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            intRowIndex = i;
                            strColumnName = "ItemCode";
                            blnIsValid = false;
                            return false;
                        }

                    }


                    if (dgvPurchase["ReturnQuantity", i].Value == null || Convert.ToDecimal(dgvPurchase["ReturnQuantity", i].Value) == 0)
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 986, out MmessageIcon);
                        intRowIndex = i;
                        strColumnName = "ReturnQuantity";
                        blnIsValid = false;
                    }
                    else if (dgvPurchase["Uom", i].Tag == null)
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 917, out MmessageIcon);
                        intRowIndex = i;
                        strColumnName = "Uom";
                        blnIsValid = false;
                    }
                    else if (dgvPurchase["Reason", i].Value == null || Convert.ToInt32(dgvPurchase["Reason",i].Value)==0)
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 987, out MmessageIcon);
                        intRowIndex = i;
                        strColumnName = "Reason";
                        blnIsValid = false;
                    }
                    else if (dgvPurchase["ReturnAmount", i].Value == null || Convert.ToDecimal(dgvPurchase["ReturnAmount", i].Value) == 0)
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 988, out MmessageIcon);
                        intRowIndex = i;
                        strColumnName = "ReturnAmount";
                        blnIsValid = false;
                    }
                    else
                    {
                        int intInvUom = 0, intUom = 0;
                        decimal decReceivedQty = 0, decRetQty = 0;
                        decReceivedQty = MobjclsBLLPurchaseReturn.GetInvoiceReceivedQty(cboInvoiceNo.SelectedValue.ToInt64(), dgvPurchase["ItemID", i].Value.ToInt32(), dgvPurchase["BatchID", i].Value.ToInt64());
                        intInvUom = Convert.ToInt32(dgvPurchase["InvUomID", i].Value);
                        decRetQty = Convert.ToDecimal(dgvPurchase["ReturnQuantity", i].Value);
                        intUom = Convert.ToInt32(dgvPurchase["Uom", i].Tag);

                        decRetQty = ConvertValueToBaseUnitValue(intUom, Convert.ToInt32(dgvPurchase["ItemID", i].Value),decRetQty);

                        decReceivedQty = ConvertValueToBaseUnitValue(intInvUom, Convert.ToInt32(dgvPurchase["ItemID", i].Value),decReceivedQty);

                        DataTable datPurchaseReturnDetails = new DataTable();
                        if(!MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.blnIsCancelled)
                            datPurchaseReturnDetails = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "PR.ReferenceSerialNo,PD.ItemID,PR.BatchID,PR.ReturnQuantity,PR.UOMID", "InvPurchaseReturnDetails PR Inner Join InvPurchaseReturnMaster On InvPurchaseReturnMaster.PurchaseReturnID = PR.PurchaseReturnID Inner Join InvPurchaseInvoiceDetail PD On PD.PurchaseInvoiceID = InvPurchaseReturnMaster.PurchaseInvoiceID And PD.SerialNo = PR.ReferenceSerialNo", "InvPurchaseReturnMaster.PurchaseInvoiceID = " + Convert.ToInt64(cboInvoiceNo.SelectedValue) + " And InvPurchaseReturnMaster.StatusID = " + (Int32)OperationStatusType.PReturnOpened });
                        else
                            datPurchaseReturnDetails = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "PR.ReferenceSerialNo,PD.ItemID,PR.BatchID,PR.ReturnQuantity,PR.UOMID", "InvPurchaseReturnDetails PR Inner Join InvPurchaseReturnMaster On InvPurchaseReturnMaster.PurchaseReturnID = PR.PurchaseReturnID Inner Join InvPurchaseInvoiceDetail PD On PD.PurchaseInvoiceID = InvPurchaseReturnMaster.PurchaseInvoiceID And PD.SerialNo = PR.ReferenceSerialNo", "InvPurchaseReturnMaster.PurchaseInvoiceID = " + Convert.ToInt64(cboInvoiceNo.SelectedValue) + " And InvPurchaseReturnMaster.StatusID = " + (Int32)OperationStatusType.PReturnOpened + " And InvPurchaseReturnMaster.PurchaseReturnID <> " + MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intPurchaseReturnID });
                        datPurchaseReturnDetails.DefaultView.RowFilter = "ItemID = " + Convert.ToInt32(dgvPurchase["ItemID", i].Value) + " And BatchID = "+dgvPurchase["BatchID",i].Value.ToInt64();
                        decimal decAlreadyRetQty = 0;
                        foreach (DataRow dr in datPurchaseReturnDetails.DefaultView.ToTable().Rows)
                        {
                            decimal decRQty = Convert.ToDecimal(dr["ReturnQuantity"]);
                            decRQty = ConvertValueToBaseUnitValue(Convert.ToInt32(dr["UOMID"]), Convert.ToInt32(dgvPurchase["ItemID", i].Value),decRQty);
                            decAlreadyRetQty += decRQty;
                        }

                        decimal decOldQty = 0;
                        DataTable datOldPurchaseReturnDetails = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "PR.ReferenceSerialNo,PR.ReturnQuantity,PR.UOMID,PD.ItemID,PR.BatchID", "InvPurchaseReturnDetails PR Inner Join InvPurchaseReturnMaster On InvPurchaseReturnMaster.PurchaseReturnID = PR.PurchaseReturnID Inner Join InvPurchaseInvoiceDetail PD On PD.PurchaseInvoiceID = InvPurchaseReturnMaster.PurchaseInvoiceID And PD.SerialNo = PR.ReferenceSerialNo ", "PR.PurchaseReturnID = " + MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intPurchaseReturnID + " And ReferenceSerialNo = " + Convert.ToInt32(dgvPurchase["ReferenceSerialNo", i].Value) + " And InvPurchaseReturnMaster.StatusID <> " + (Int32)OperationStatusType.PReturnCancelled });

                        datOldPurchaseReturnDetails.DefaultView.RowFilter = "ItemID = " + Convert.ToInt32(dgvPurchase["ItemID", i].Value) + " And BatchID = " + dgvPurchase["BatchID", i].Value.ToInt64();
                        if (datOldPurchaseReturnDetails.DefaultView.ToTable().Rows.Count > 0)
                            decOldQty = ConvertValueToBaseUnitValue(Convert.ToInt32(datOldPurchaseReturnDetails.DefaultView.ToTable().Rows[0]["UOMID"]), Convert.ToInt32(dgvPurchase["ItemID", i].Value),Convert.ToDecimal(datOldPurchaseReturnDetails.DefaultView.ToTable().Rows[0]["ReturnQuantity"]));
                        if ((decReceivedQty - decAlreadyRetQty) + decOldQty < decRetQty)
                        {
                            if (lblStatus.Text == "Cancelled")
                            {
                                MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 2284, out MmessageIcon);
                                intRowIndex = i;
                                strColumnName = "ReturnQuantity";
                                blnIsValid = false;
                            }
                            else
                            {
                                MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 984, out MmessageIcon);
                                intRowIndex = i;
                                strColumnName = "ReturnQuantity";
                                blnIsValid = false;
                            }
                        }
                        if (blnIsValid)
                        {
                            if (!ValidateStockQuantity(dgvPurchase[ItemID.Index, i].Value.ToInt32(), dgvPurchase[BatchID.Index, i].Value.ToInt64(), decRetQty - decOldQty))
                            {
                                MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 994, out MmessageIcon).Replace("@",dgvPurchase[ItemName.Index,i].Value.ToString());
                                intRowIndex = i;
                                strColumnName = "ReturnQuantity";
                                blnIsValid = false;
                            }
                        }
                    }
                    intItemCount++;
                }
            }
            if (intItemCount == 0)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 901, out MmessageIcon);
                intRowIndex = 0;
                strColumnName = "ItemCode";
                blnIsValid = false;
            }
            if (blnIsValid)
            {

            }

            if (!blnIsValid)
            {
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrPurchaseReturn.SetError(dgvPurchase, MstrMessageCommon.Replace("#", "").Trim());
                lblPurchaseReturnStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                TmrPurchaseReturn.Enabled = true;
                tcPurchaseReturn.SelectedTab = tiItemDetails;
                dgvPurchase.CurrentCell = dgvPurchase[strColumnName, intRowIndex];
                dgvPurchase.Focus();
            }
            return blnIsValid;
        }

        private void dgvPurchase_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >=0 && e.ColumnIndex >=0)
            {
            try
            {
                if (e.ColumnIndex == ItemID.Index)
                {
                    dgvPurchase[ReturnQuantity.Index, e.RowIndex].Value = Convert.ToDecimal(1).ToString("F" + MintExchangeCurrencyScale);
                    //dgvPurchase[ReturnedQty.Index, e.RowIndex].Value = MobjclsBLLPurchaseReturn.GetAlreadyReturnedQty(cboInvoiceNo.SelectedValue.ToInt32(), dgvPurchase[ReferenceSerialNo.Index, e.RowIndex].Value.ToInt32());
                }
                else if (e.ColumnIndex == ReferenceSerialNo.Index)
                {
                    //dgvPurchase[ReturnedQty.Index, e.RowIndex].Value = MobjclsBLLPurchaseReturn.GetAlreadyReturnedQty(cboInvoiceNo.SelectedValue.ToInt32(), dgvPurchase[ReferenceSerialNo.Index, e.RowIndex].Value.ToInt32());
                }
                else if (dgvPurchase.Columns[e.ColumnIndex].Name == "ReturnQuantity" || dgvPurchase.Columns[e.ColumnIndex].Name == "Rate")
                {
                    decimal decValue = 0;
                    try
                    {
                        decValue = Convert.ToDecimal(dgvPurchase[e.ColumnIndex, e.RowIndex].Value);
                    }
                    catch
                    {
                        dgvPurchase[e.ColumnIndex, e.RowIndex].Value = 0;
                    }
                    CalcuateGridTotal();
                    CalculateNetTotal();
                }
                else if (e.ColumnIndex == BatchID.Index)
                {
                    dgvPurchase[ReturnedQty.Index, e.RowIndex].Value = MobjclsBLLPurchaseReturn.GetReturnedQty(Convert.ToInt64(cboInvoiceNo.SelectedValue), dgvPurchase[BatchID.Index, e.RowIndex].Value.ToInt64(),dgvPurchase[ReferenceSerialNo.Index,e.RowIndex].Value.ToInt32());

                }
                else if (dgvPurchase.Columns[e.ColumnIndex].Name == "InvUomID")
                {
                    FillUOMColumn(Convert.ToInt32(dgvPurchase["ItemID", e.RowIndex].Value));
                    dgvPurchase["Uom", e.RowIndex].Tag = dgvPurchase["InvUomID", e.RowIndex].Value;
                    dgvPurchase["Uom", e.RowIndex].Value = dgvPurchase["InvUomID", e.RowIndex].Value;
                    dgvPurchase["Uom", e.RowIndex].Value = dgvPurchase["Uom", e.RowIndex].FormattedValue;
                }
                else if (dgvPurchase.Columns[e.ColumnIndex].Name == "InvRate")
                {
                    dgvPurchase["Rate", e.RowIndex].Value = dgvPurchase["InvRate", e.RowIndex].Value ;
                }
                else if (dgvPurchase.Columns[e.ColumnIndex].Name == "Uom")
                {
                    if (dgvPurchase["Uom", e.RowIndex].Value != null)
                    {

                        int intUom = 0, intInvUom = 0;
                        decimal decRate = 0, decInvRate = 0;
                        decInvRate = Convert.ToDecimal(dgvPurchase["InvRate", e.RowIndex].Value);
                        intInvUom = Convert.ToInt32(dgvPurchase["InvUomID", e.RowIndex].Value);
                        intUom = Convert.ToInt32(dgvPurchase["Uom", e.RowIndex].Tag);

                        DataTable dtUomDetails = MobjclsBLLPurchaseReturn.GetUomConversionValues(intInvUom, Convert.ToInt32(dgvPurchase["ItemID", e.RowIndex].Value));
                        if (dtUomDetails.Rows.Count > 0)
                        {
                            int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                            decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                            if (intConversionFactor == 1)
                                decInvRate = decInvRate * decConversionValue;
                            else if (intConversionFactor == 2)
                                decInvRate = decInvRate / decConversionValue;
                        }

                        dtUomDetails = MobjclsBLLPurchaseReturn.GetUomConversionValues(intUom, Convert.ToInt32(dgvPurchase["ItemID", e.RowIndex].Value));
                        decRate = decInvRate;
                        if (dtUomDetails.Rows.Count > 0)
                        {
                            int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                            decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                            if (intConversionFactor == 1)
                                decRate = decInvRate / decConversionValue;
                            else if (intConversionFactor == 2)
                                decRate = decInvRate * decConversionValue;
                        }
                        dgvPurchase["Rate", e.RowIndex].Value = decRate;
                    }
                }
                else if (dgvPurchase.Columns[e.ColumnIndex].Name == "ReturnAmount")
                {
                    decimal decTotalAmount = 0;
                    foreach (DataGridViewRow dr in dgvPurchase.Rows)
                    {
                        if (dr.Cells["ReturnAmount"].Value != null)
                            decTotalAmount += Convert.ToDecimal(dr.Cells["ReturnAmount"].Value);
                    }
                    txtSubtotal.Text = decTotalAmount.ToString("0.000");
                    CalculateNetTotal();
                }
                else if (e.ColumnIndex == Discount.Index)
                {
                    CalcuateGridTotal();
                    CalculateNetTotal();
                }
                if (dgvPurchase.Columns[e.ColumnIndex].Name == "DiscountAmount" || dgvPurchase.Columns[e.ColumnIndex].Name == "ReturnQuantity")
                {
                    if(!MBlnEditMode)
                    {
                        dgvPurchase.Rows[e.RowIndex].Cells["Discount"].Tag = dgvPurchase.Rows[e.RowIndex].Cells["Discount"].Value;
                    }
                    decimal decDiscountAmount = MobjclsBLLPurchaseReturn.GetDiscountAmount(Convert.ToInt64(cboInvoiceNo.SelectedValue), Convert.ToInt32(dgvPurchase.Rows[e.RowIndex].Cells["ItemID"].Value), Convert.ToInt64(dgvPurchase.Rows[e.RowIndex].Cells["BatchID"].Value));
                    dgvPurchase.Rows[e.RowIndex].Cells["DiscountAmount"].Value = (decDiscountAmount * (Convert.ToDecimal(dgvPurchase.Rows[e.RowIndex].Cells["ReturnQuantity"].Value))).ToString("F" + MintExchangeCurrencyScale);
                    CalcuateGridTotal();
                    CalculateNetTotal();
                }

                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    if (e.ColumnIndex == Uom.Index || e.ColumnIndex == ReturnQuantity.Index || e.ColumnIndex == Rate.Index)
                    {
                        if (dgvPurchase.Rows[e.RowIndex].Cells["Discount"].Value == null)
                        {
                            LoadCombos(7);
                            dgvPurchase.Rows[e.RowIndex].Cells["Discount"].Tag = 0;
                            dgvPurchase.Rows[e.RowIndex].Cells["Discount"].Value = 0;
                            dgvPurchase.Rows[e.RowIndex].Cells["Discount"].Value = dgvPurchase.Rows[e.RowIndex].Cells["Discount"].FormattedValue;
                        }
                    }

                }
                Changestatus(null,null);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvPurchase_CellValueChanged() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvPurchase_CellValueChanged() " + ex.Message, 2);
            }
        }
        }


        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (SavePurchaseReturn())
                {
                    AddNew();
                }
                    
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorSaveItem_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in BindingNavigatorSaveItem_Click() " + ex.Message, 2);
            }
        }

        private bool SavePurchaseReturn()
        {
            
            if(ValidateFields())
            {
                if (MBlnCanShow)
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, (MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intPurchaseReturnID == 0 ? 1 : 3), out MmessageIcon);

                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                        MessageBoxIcon.Information) == DialogResult.No)
                        return false;
                    else
                    {
                        if (!MBlnEditMode && MobjclsBLLPurchaseReturn.IsPurchaseReturnExists(cboCompany.SelectedValue.ToInt32(), txtReturnNo.Text.Trim()))
                        {
                            MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 973, out MmessageIcon);
                            MstrMessageCommon = MstrMessageCommon.Replace("*", "Return");
                            if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                                return false;
                            else
                                GeneratePurchaseReturnNo();
                        }
                    }
                }
                FillMasterParameters();
                FillDetParameters();
                if (MobjclsBLLPurchaseReturn.SavePurchaseReturnMaster())
                {
                    if (!MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.blnIsCancelled)
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 2, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    }
                    lblPurchaseReturnStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchaseReturn.Enabled = true;
                    return true;

                }
            }
            return false;
        }

        private void FillMasterParameters()
        {
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intPurchaseInvoiceID = Convert.ToInt32(cboInvoiceNo.SelectedValue);
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.strPurchaseReturnNo = txtReturnNo.Text.Trim();
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.strReturnDate = dtpReturnDate.Value.ToString("dd-MMM-yyyy");
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.strRemarks = txtRemarks.Text.Trim();
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intCurrencyID = Convert.ToInt32(cboCurrency.SelectedValue);
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intGrandDiscountID = 0;
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.decGrandDiscountAmount = 0;
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.decNetAmount = Convert.ToDecimal(txtTotalAmount.Text.Trim());
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.decGrandReturnAmount = Convert.ToDecimal(txtGrandReturnAmount.Text.Trim());
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.decGrandAmount = Convert.ToDecimal(txtSubtotal.Text.Trim());
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intCreatedBy = ClsCommonSettings.UserID;
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.strCreatedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intWarehouseID = Convert.ToInt32(cboWarehouse.SelectedValue);
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intDebitHeadID = (int)Accounts.CashAccount;
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intCreditHeadID = 0;
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intTaxSchemeID = Convert.ToInt32(cboTaxScheme.SelectedValue);
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.decTaxAmount = Convert.ToDecimal(lblTaxAmount.Text);
           
           
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intAccountID = cboAccount.SelectedValue.ToInt32();
            if (MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.blnIsCancelled)
            {
                MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intStatusID = (Int32)OperationStatusType.PReturnCancelled;
            }
            else
            {
                MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intStatusID = (Int32)OperationStatusType.PReturnOpened;
            }
        }

        private void FillDetParameters()
        {
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.lstPurchaseReturnDetails = new List<clsDTOPurchaseReturnDetails>();
            for (int i = 0; i < dgvPurchase.Rows.Count; i++)
            {
                if (dgvPurchase["ReferenceSerialNo", i].Value != null && Convert.ToInt32(dgvPurchase["ReferenceSerialNo", i].Value) > 0)
                {
                    clsDTOPurchaseReturnDetails objClsDtoPurchaseReturnDetails = new clsDTOPurchaseReturnDetails();
                    objClsDtoPurchaseReturnDetails.intPurchaseReturnID = Convert.ToInt64(cboInvoiceNo.SelectedValue);
                    objClsDtoPurchaseReturnDetails.intReferenceSerialNo = Convert.ToInt32(dgvPurchase["ReferenceSerialNo", i].Value);
                    objClsDtoPurchaseReturnDetails.lngBatchID = Convert.ToInt64(dgvPurchase["BatchID", i].Value);
                    objClsDtoPurchaseReturnDetails.decReturnQuantity = Convert.ToDecimal(dgvPurchase["ReturnQuantity", i].Value);
                    objClsDtoPurchaseReturnDetails.intReasonID = Convert.ToInt32(dgvPurchase["Reason", i].Value);
                    objClsDtoPurchaseReturnDetails.intUOMID = Convert.ToInt32(dgvPurchase["Uom", i].Tag);
                    if(dgvPurchase["Discount",i].Value !=DBNull.Value)
                    objClsDtoPurchaseReturnDetails.intDiscountID = Convert.ToInt32(dgvPurchase["Discount", i].Tag);
                    objClsDtoPurchaseReturnDetails.decDiscountAmount = dgvPurchase["DiscountAmount", i].Value.ToDecimal();
                    objClsDtoPurchaseReturnDetails.decNetAmount = Convert.ToDecimal(dgvPurchase["NetAmount", i].Value);
                    objClsDtoPurchaseReturnDetails.decReturnAmount = Convert.ToDecimal(dgvPurchase["ReturnAmount", i].Value);
                    objClsDtoPurchaseReturnDetails.decGrandAmount = Convert.ToDecimal(dgvPurchase["GrandAmount", i].Value);
                    objClsDtoPurchaseReturnDetails.decRate = Convert.ToDecimal(dgvPurchase["Rate", i].Value);
                    objClsDtoPurchaseReturnDetails.intItemID = Convert.ToInt32(dgvPurchase["ItemID", i].Value);

                    MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.lstPurchaseReturnDetails.Add(objClsDtoPurchaseReturnDetails);
                }
            }
        }

        private void DgvSOrderDisplay_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (DgvSOrderDisplay.Rows.Count > 0)
            {
                if (Convert.ToInt32(DgvSOrderDisplay.CurrentRow.Cells[0].Value) != 0)
                {
                    ClearControls();
                    if (ClsCommonSettings.PblnIsLocationWise)
                    {
                        btnPickList.Enabled = MbPrintEmailPermission;
                        btnPickListEmail.Enabled = MbPrintEmailPermission;
                    }
                    MBlnEditMode = true;
                    BtnPrint.Enabled = MbPrintEmailPermission;
                    BtnEmail.Enabled = MbPrintEmailPermission;
                    DisplayPurchaseReturnMasterDetails(Convert.ToInt32(DgvSOrderDisplay.CurrentRow.Cells[0].Value));
                    BindingNavigatorSaveItem.Enabled = false;
                    MBlnCanShow = true;
                }
            }
        }


        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void BindingNavigatorCancelItem_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64(txtReturnNo.Tag) > 0)
            {
                if (BindingNavigatorCancelItem.Text.ToUpper() == "CANCEL")
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 33, out MmessageIcon).Replace("***","Purchase Return");
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                        {
                            objFrmCancellationDetails.ShowDialog();
                            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.strCancellationDate = objFrmCancellationDetails.PDtCancellationDate.ToString("dd-MMM-yyyy");
                            txtDescription.Text = objFrmCancellationDetails.PStrDescription;
                            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.strCancellationDescription = objFrmCancellationDetails.PStrDescription;

                            if (!objFrmCancellationDetails.PBlnIsCanelled)
                            {
                                MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.blnIsCancelled = true;
                                MBlnCanShow = false;
                                if (SavePurchaseReturn())
                                {
                                    AddNew();
                                    //BindingNavigatorCancelItem.Text = "Re Open";
                                    //BindingNavigatorSaveItem.Enabled = false;
                                    //BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
                                    //lblStatus.Text = "Cancelled";
                                }
                            }
                            else
                            {
                                txtDescription.Text = "";
                                MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.blnIsCancelled = false;
                            }
                        }
                    }
                }
                else
                {
                    MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.blnIsCancelled = false;
                     MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 34, out MmessageIcon).Replace("***","Purchase Return");
                     if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                     {
                         MBlnCanShow = false;
                         if (SavePurchaseReturn())
                             AddNew();
                     }
                }
            }
        }



        private void BtnSRefresh_Click(object sender, EventArgs e)
        {
            DisplayPurchaseReturnNumbersInSearchGrid();
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Clear Controls
            dgvPurchase.Rows.Clear();
            lblDiscountAmount.Text = Convert.ToDecimal(0).ToString("F" + MintBaseCurrencyScale);
            lblExpenseAmount.Text = Convert.ToDecimal(0).ToString("F" + MintBaseCurrencyScale);
            txtSubtotal.Text = Convert.ToDecimal(0).ToString("F" + MintBaseCurrencyScale);
            txtTotalAmount.Text = Convert.ToDecimal(0).ToString("F" + MintBaseCurrencyScale);
            txtGrandReturnAmount.Text = Convert.ToDecimal(0).ToString("F" + MintBaseCurrencyScale);
            txtExchangeCurrencyRate.Text = Convert.ToDecimal(0).ToString("F" + MintBaseCurrencyScale);

            Changestatus(sender, e);

            LoadCombos(6);
            LoadCombos(9);
            GetAccount();
            if (MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intPurchaseReturnID == 0)
                GeneratePurchaseReturnNo();
            lblCompanyCurrencyAmount.Text = "Amount in " + MobjclsBLLPurchaseReturn.GetCompanyCurrency(Convert.ToInt32(cboCompany.SelectedValue), out MintBaseCurrencyScale);
            Changestatus(sender, e);
        }
        public void GetAccount()
        {
            DataTable datAccount = new DataTable();
            datAccount = new clsBLLAccountSettings().FillCombos(new string[] { "AccountID,AccountName", "AccAccountMaster", "AccountGroupID=" + (int)AccountGroups.PurchaseAccounts +" AND AccountID <>"+(int)DefaultAccount.PurchaseAc });
            cboAccount.ValueMember = "AccountID";
            cboAccount.DisplayMember = "AccountName";
            cboAccount.DataSource = datAccount;
            cboAccount.SelectedValue =  new clsBLLPurchase().GetCompanyAccountID(Convert.ToInt32(TransactionTypes.PurchaseReturn), cboCompany.SelectedValue.ToInt32());

        }
        private void cboSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboInvoiceNo.DataSource = null;
            if (cboSupplier.SelectedValue != null)
            {
                LoadCombos(9);
                string strVendorAddress = "";
                txtVendorAddress.Text = MobjclsBLLPurchaseReturn.GetVendorAddress(Convert.ToInt32(cboSupplier.SelectedValue), true,out strVendorAddress);
                lblVendorAddress.Text = strVendorAddress;
            }
            else
            {
                txtVendorAddress.Text = "";
                lblVendorAddress.Text = "Address";
            }
            Changestatus(sender, e);
        }

        private void cboInvoiceNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvPurchase.Rows.Clear();
            if (cboInvoiceNo.SelectedValue != null)
            {
                if (MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intPurchaseReturnID == 0)
                {
                    if (MobjclsBLLPurchaseReturn.FillCombos(new string[] { "PurchaseInvoiceID", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + Convert.ToInt32(cboInvoiceNo.SelectedValue) + " And StatusID <> " + (Int32)OperationStatusType.PInvoiceCancelled }).Rows.Count == 0)
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 921, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("*", "PurchaseInvoice");
                        ErrPurchaseReturn.SetError(cboInvoiceNo, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchaseReturnStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchaseReturn.Enabled = true;
                        cboInvoiceNo.Focus();
                        return;
                    }
                }
                DataTable datInvoiceDetails = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "*", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + cboInvoiceNo.SelectedValue.ToInt64() });
                cboWarehouse.SelectedValue = datInvoiceDetails.Rows[0]["WarehouseID"].ToInt32();
               
                if (datInvoiceDetails.Rows[0]["GrandDiscountAmount"] != DBNull.Value)
                    lblDiscountAmount.Text = Convert.ToDecimal(datInvoiceDetails.Rows[0]["GrandDiscountAmount"]).ToString("F" + MintBaseCurrencyScale);
                else
                    lblDiscountAmount.Text = Convert.ToDecimal(0).ToString("F" + MintBaseCurrencyScale);

                if (datInvoiceDetails.Rows[0]["ExpenseAmount"] != DBNull.Value)
                    lblExpenseAmount.Text = Convert.ToDecimal(datInvoiceDetails.Rows[0]["ExpenseAmount"]).ToString("F" + MintBaseCurrencyScale);
                else
                    lblExpenseAmount.Text = Convert.ToDecimal(0).ToString("F" + MintBaseCurrencyScale);

                cboCurrency.SelectedValue = Convert.ToInt32(datInvoiceDetails.Rows[0]["CurrencyID"]);
                LoadCombos(10);
                cboTaxScheme.SelectedValue =datInvoiceDetails.Rows[0]["TaxSchemeID"]==DBNull.Value? 0 : Convert.ToInt32(datInvoiceDetails.Rows[0]["TaxSchemeID"]);
                string strVendorAddress = "";
                txtVendorAddress.Text = MobjclsBLLPurchaseReturn.GetVendorAddress(Convert.ToInt32(datInvoiceDetails.Rows[0]["VendorAddID"]), false, out strVendorAddress);
                lblVendorAddress.Text = strVendorAddress;
            }
            else
            {
                txtVendorAddress.Text = "";
                lblVendorAddress.Text = "Address";
            }
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 4268, out MmessageIcon);
                
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                if (MobjclsBLLPurchaseReturn.DeletePurchaseReturnMaster())
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 4, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchaseReturnStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrFocus.Enabled = true;
                    AddNew();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorDeleteItem_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in BindingNavigatorDeleteItem_Click() " + ex.Message, 2);
            }
        }

        private void dgvPurchase_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 || e.ColumnIndex == 1)
            {
                dgvPurchase.PiColumnIndex = e.ColumnIndex;
            }
        }

        private void dgvPurchase_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvPurchase["ItemID", e.RowIndex].Value != null)
                {
                    if (dgvPurchase.Columns[e.ColumnIndex].Name == "Uom")
                    {
                        if (dgvPurchase.CurrentRow.Cells["Uom"].Value != null)
                        {
                            dgvPurchase.CurrentRow.Cells["Uom"].Tag = dgvPurchase.CurrentRow.Cells["Uom"].Value;
                            dgvPurchase.CurrentRow.Cells["Uom"].Value = dgvPurchase.CurrentRow.Cells["Uom"].FormattedValue;

                            int intUom = 0,intInvUom=0;
                            decimal decRate = 0,decInvRate=0;
                            decInvRate = Convert.ToDecimal(dgvPurchase["InvRate", e.RowIndex].Value);
                            intInvUom = Convert.ToInt32(dgvPurchase["InvUomID", e.RowIndex].Value);
                            intUom = Convert.ToInt32(dgvPurchase["Uom", e.RowIndex].Tag);

                            DataTable dtUomDetails = MobjclsBLLPurchaseReturn.GetUomConversionValues(intInvUom, Convert.ToInt32(dgvPurchase["ItemID", e.RowIndex].Value));
                            if (dtUomDetails.Rows.Count > 0)
                            {
                                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                                if (intConversionFactor == 1)
                                    decInvRate = decInvRate * decConversionValue;
                                else if (intConversionFactor == 2)
                                    decInvRate = decInvRate / decConversionValue;
                            }

                            dtUomDetails = MobjclsBLLPurchaseReturn.GetUomConversionValues(intUom, Convert.ToInt32(dgvPurchase["ItemID", e.RowIndex].Value));
                            decRate = decInvRate;
                            if (dtUomDetails.Rows.Count > 0)
                            {
                                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                                if (intConversionFactor == 1)
                                    decRate = decInvRate / decConversionValue;
                                else if (intConversionFactor == 2)
                                    decRate = decInvRate * decConversionValue;
                            }
                            dgvPurchase["Rate", e.RowIndex].Value = decRate;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvPurchase_CellEndEdit() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvPurchase_CellEndEdit() " + ex.Message, 2);
            }
        }

        private void dgvPurchase_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                   int iRowIndex = -1;
                   dgvPurchase.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    iRowIndex = dgvPurchase.CurrentCell.RowIndex;
                    if (e.ColumnIndex == ReturnQuantity.Index || e.ColumnIndex == ExtraQuantity.Index || e.ColumnIndex == Uom.Index || e.ColumnIndex == Rate.Index || e.ColumnIndex == Discount.Index)
                    {
                        if (Convert.ToString(dgvPurchase.CurrentRow.Cells[0].Value).Trim() == "")
                        {
                            MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 915, out MmessageIcon);
                            ErrPurchaseReturn.SetError(dgvPurchase, MstrMessageCommon.Replace("#", "").Trim());
                            e.Cancel = true;
                        }
                    }

                    if (e.ColumnIndex == Uom.Index || e.ColumnIndex == Discount.Index)
                    {
                        if (dgvPurchase.CurrentRow.Cells["ItemID"].Value != null)
                        {
                            int iItemID = Convert.ToInt32(dgvPurchase.CurrentRow.Cells["ItemID"].Value);
                            int tag = -1;
                            if (e.ColumnIndex == Uom.Index)
                            {
                                if (dgvPurchase.CurrentRow.Cells["Uom"].Tag != null)
                                {
                                    tag = Convert.ToInt32(dgvPurchase.CurrentRow.Cells["Uom"].Tag);
                                }
                                FillUOMColumn(iItemID);
                                if (tag != -1)
                                    dgvPurchase.CurrentRow.Cells["Uom"].Tag = tag;
                            }
                            else if (e.ColumnIndex == Discount.Index)
                            {
                                if (dgvPurchase.CurrentRow.Cells["Discount"].Tag != null)
                                {
                                    tag = Convert.ToInt32(dgvPurchase.CurrentRow.Cells["Discount"].Tag);
                                }
                                LoadCombos(7);
                                if (tag != -1)
                                    dgvPurchase.CurrentRow.Cells["Discount"].Tag = tag;
                            }
                        }
                        else
                        {
                            Uom.DataSource = null;
                            Discount.DataSource = null;
                        }
                    }
               
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvPurchase_CellBeginEdit() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvPurchase_CellBeginEdit() " + ex.Message, 2);
            }
        }

        private void dgvPurchase_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvPurchase.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
            dgvPurchase.CellLeave += new DataGridViewCellEventHandler(dgvPurchase_CellLeave);
        }

        void dgvPurchase_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvPurchase.CurrentCell.OwningColumn.Name == "ReturnQuantity" || dgvPurchase.CurrentCell.OwningColumn.Name == "ReturnAmount")
            {
                decimal decValue = 0;
                try
                {
                    decValue = Convert.ToDecimal(dgvPurchase[e.ColumnIndex, e.RowIndex].Value);
                }
                catch
                {
                    dgvPurchase[e.ColumnIndex, e.RowIndex].Value = 0;
                }
            } 
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvPurchase.CurrentCell.OwningColumn.Name == "ReturnQuantity" || dgvPurchase.CurrentCell.OwningColumn.Name =="ReturnAmount")
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (!string.IsNullOrEmpty(dgvPurchase.EditingControl.Text) && dgvPurchase.EditingControl.Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true;
                }
            }
        }

        private void BindingNavigatorClearItem_Click(object sender, EventArgs e)
        {
            ClearControls();
        }
           //case 4:
           //             ObjEmailPopUp.MsSubject = "Sales Return Information";
           //             ObjEmailPopUp.EmailFormType = EmailFormID.SalesReturn;
           //             ObjEmailPopUp.EmailSource = MobjclsBLLSTSales.GetSalesReturnReport();
           //             break;

       
        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if ( Convert.ToInt32(MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intPurchaseReturnID) > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PblnPickList = false;
                    ObjViewer.PiRecId = Convert.ToInt32(MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intPurchaseReturnID);
                    ObjViewer.PiFormID = (int)FormID.DebitNote;
                   // ObjViewer.strAmountInWord = lblAmountInWords.Text;
                    ObjViewer.ShowDialog();
                    
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form FrmPurchaseReturn:LoadReport " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnPrint_Click " + Ex.Message.ToString());
            }

        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Purchase Return Information";
                    ObjEmailPopUp.EmailFormType = EmailFormID.DebitNote;
                    ObjEmailPopUp.EmailSource = MobjclsBLLPurchaseReturn.GetDebitNoteReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on Email:BtnEmail_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnEmail_Click " + Ex.Message.ToString());
            }
        }

        private void CboSCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable datCombos = null; bool blnIsEnabled = false;

            if (CboSCompany.SelectedValue == null || Convert.ToInt32(CboSCompany.SelectedValue) == -2)
            {
                DataTable datCompany = (DataTable)CboSCompany.DataSource;
                string strFilterCondition = "";
                //datCompany.Rows.RemoveAt(0);
                //datCompany.AcceptChanges();
                if (datCompany.Rows.Count > 0)
                {
                    strFilterCondition = "CompanyID In (";
                    foreach (DataRow dr in datCompany.Rows)
                        strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                    strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                    strFilterCondition += ")";
                }
                strFilterCondition = strFilterCondition + " AND UM.UserID <> 1 ";
                datCombos = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "UM.UserID,IsNull(EM.FirstName,UM.UserName) as UserName", "UserMaster UM Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID", strFilterCondition });

            }
            else
                datCombos = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "UM.UserID,IsNull(EM.FirstName,UM.UserName) as UserName", "UserMaster UM Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID ", " UM.UserID <> 1 AND Case IsNull(EM.EmployeeID,0) When 0 Then " + CboSCompany.SelectedValue.ToInt32() + " Else EM.CompanyID  End = " + CboSCompany.SelectedValue.ToInt32() + ""  });
            cboSExecutive.ValueMember = "UserID";
            cboSExecutive.DisplayMember = "UserName";
            cboSExecutive.DataSource = datCombos;
        }

        public void RefreshForm()
        {
            //cboCompany_SelectedIndexChanged(null, null);
            BtnSRefresh_Click(null, null);
        }

        private void FrmPurchaseReturn_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Alt | Keys.S:
                       BindingNavigatorSaveItem_Click(sender, new EventArgs());//save
                        break;
                    case Keys.Alt | Keys.R:
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
           
                }
            }
            catch (Exception)
            {
            }
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCurrency.SelectedValue != null)
            {
                DataTable datCurrency = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "CurrencyID,Scale", "CurrencyReference", "CurrencyID = " + Convert.ToInt32(cboCurrency.SelectedValue) });
                if (datCurrency.Rows.Count > 0)
                    MintExchangeCurrencyScale = Convert.ToInt32(datCurrency.Rows[0]["Scale"]);
            }
            Changestatus(sender, e);
        }

        private bool FillItemLocationDetails()
        {

            int iTempID = 0;
            iTempID = CheckDuplicationInGrid();
            if (iTempID != -1)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 31, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblPurchaseReturnStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrFocus.Enabled = true;
                tcPurchaseReturn.SelectedTab = tiItemDetails;
                dgvPurchase.CurrentCell = dgvPurchase["ItemCode", iTempID];
                dgvPurchase.Focus();
                return false;
            }
                //for (int i = 0; i < dgvLocationDetails.Rows.Count; i++)
                //{

                //    if (dgvLocationDetails.Rows[i].Cells["LItemID"].Value != null && dgvLocationDetails.Rows[i].Cells["LBatchNo"].Value != null)
                //    {
                //        bool blnExists = false;
                //        foreach (DataGridViewRow dr in dgvPurchase.Rows)
                //        {
                //            if (dr.Cells["ItemID"].Value != null && dr.Cells["BatchID"].Value != null)
                //            {
                //                if (dr.Cells["ItemID"].Value.ToInt32() == dgvLocationDetails.Rows[i].Cells["LItemID"].Value.ToInt32() && dr.Cells["BatchID"].Value.ToInt64() == dgvLocationDetails.Rows[i].Cells["LBatchID"].Value.ToInt64())
                //                {
                //                    blnExists = true;
                //                    break;
                //                }
                //            }
                //        }
                //        if (!blnExists)
                //        {
                //            dgvLocationDetails.Rows.RemoveAt(i);
                //            i--;
                //        }
                //    }
                //}
            dgvLocationDetails.Rows.Clear();

                bool blnIsValid = true;
                //foreach (DataGridViewRow dr in dgvPurchase.Rows)
                //{
                //    if (dr.Cells[ItemID.Index].Value.ToInt32() != 0 && dr.Cells[BatchID.Index].Value != null)
                //    {
                //        decimal decQuantity = dr.Cells[ReturnQuantity.Index].Value.ToDecimal();
                //        decimal decLocationQuantity = 0;
                //        foreach (DataGridViewRow drRow in dgvLocationDetails.Rows)
                //        {
                //            if (dr.Cells[ItemID.Index].Value.ToInt32() == drRow.Cells[LItemID.Index].Value.ToInt32() && Convert.ToString(dr.Cells[BatchID.Index].Value) == Convert.ToString(drRow.Cells[LBatchID.Index].Value))
                //                decLocationQuantity += drRow.Cells[LQuantity.Index].Value.ToDecimal();
                //        }
                //        if (decQuantity != decLocationQuantity)
                //        {
                //            blnIsValid = false;
                //            break;
                //        }
                //    }
                //}
                //if (!blnIsValid)
                //{
                    if (dgvLocationDetails.Rows.Count == 1)
                    {
                        dgvLocationDetails.Rows.Clear(); dtItemDetails.Rows.Clear();
                        foreach (DataGridViewRow drItemRow in dgvPurchase.Rows)
                        {

                            if (drItemRow.Cells["ItemID"].Value != null && drItemRow.Cells["BatchID"].Value != null)
                            {

                                DataRow drItemDetailsRow = dtItemDetails.NewRow();
                                drItemDetailsRow["ItemID"] = drItemRow.Cells["ItemID"].Value.ToInt32();
                                drItemDetailsRow["ItemCode"] = drItemRow.Cells["ItemCode"].Value.ToString();
                                drItemDetailsRow["ItemName"] = drItemRow.Cells["ItemName"].Value.ToString();
                                drItemDetailsRow["BatchID"] = drItemRow.Cells["BatchID"].Value.ToInt64();
                                drItemDetailsRow["BatchNo"] = drItemRow.Cells["BatchNo"].Value.ToString();
                                drItemDetailsRow["UomID"] = drItemRow.Cells["Uom"].Tag.ToInt32();
                                dtItemDetails.Rows.Add(drItemDetailsRow);

                                // dgvLocationDetails.Rows.Add();
                                dgvLocationDetails.RowCount = dgvLocationDetails.RowCount + 1;
                                int intRowIndex = dgvLocationDetails.RowCount - 2;
                                dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value = drItemRow.Cells["ItemID"].Value.ToInt32();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LItemCode"].Value = drItemRow.Cells["ItemCode"].Value.ToString();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LItemName"].Value = drItemRow.Cells["ItemName"].Value.ToString();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LBatchID"].Value = drItemRow.Cells["BatchID"].Value.ToInt64();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LBatchNo"].Value = drItemRow.Cells["BatchNo"].Value.ToString();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LQuantity"].Value = drItemRow.Cells["ReturnQuantity"].Value.ToDecimal();

                                DataTable dtItemUOMs = MobjclsBLLPurchaseReturn.GetItemUOMs(drItemRow.Cells["ItemID"].Value.ToInt32());
                                LUOM.DataSource = null;
                                LUOM.ValueMember = "UOMID";
                                LUOM.DisplayMember = "ShortName";
                                LUOM.DataSource = dtItemUOMs;
                                dgvLocationDetails.Rows[intRowIndex].Cells["LUOMID"].Value = drItemRow.Cells["UOM"].Tag.ToInt32();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = drItemRow.Cells["UOM"].Tag.ToInt32();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Tag = drItemRow.Cells["UOM"].Tag.ToInt32();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].FormattedValue;

                                DataRow dr = MobjclsBLLPurchaseReturn.GetItemDefaultLocation(drItemRow.Cells["ItemID"].Value.ToInt32(), cboWarehouse.SelectedValue.ToInt32());

                                FillLocationGridCombo(LLocation.Index, intRowIndex);
                                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dr["LocationID"].ToInt32();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag = dr["LocationID"].ToInt32();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].FormattedValue;

                                FillLocationGridCombo(LRow.Index, intRowIndex);
                                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dr["RowID"].ToInt32();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag = dr["RowID"].ToInt32();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].FormattedValue;

                                FillLocationGridCombo(LBlock.Index, intRowIndex);
                                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dr["BlockID"].ToInt32();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag = dr["BlockID"].ToInt32();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].FormattedValue;

                                FillLocationGridCombo(LLot.Index, intRowIndex);
                                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dr["LotID"].ToInt32();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag = dr["LotID"].ToInt32();
                                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].FormattedValue;

                                clsDTOPurchaseReturnLocationDetails objLocationDetails = new clsDTOPurchaseReturnLocationDetails();
                                objLocationDetails.intItemID = dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value.ToInt32();
                                objLocationDetails.lngBatchID = dgvLocationDetails.Rows[intRowIndex].Cells["LBatchID"].Value.ToInt64();
                                objLocationDetails.intLocationID = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag.ToInt32();
                                objLocationDetails.intRowID = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag.ToInt32();
                                objLocationDetails.intBlockID = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag.ToInt32();
                                objLocationDetails.intLotID = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag.ToInt32();
                                MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intCompanyID = cboCompany.SelectedValue.ToInt32();
                                MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intWarehouseID = cboWarehouse.SelectedValue.ToInt32();

                                decimal decStockQuantity = MobjclsBLLPurchaseReturn.GetItemLocationQuantity(objLocationDetails);
                                DataTable dtUomDetails = new DataTable();
                                dtUomDetails = MobjclsBLLPurchaseReturn.GetUomConversionValues(dgvLocationDetails.Rows[intRowIndex].Cells["LUom"].Tag.ToInt32(), dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value.ToInt32());
                                if (dtUomDetails.Rows.Count > 0)
                                {
                                    int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                                    decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                                    if (intConversionFactor == 1)
                                        decStockQuantity = decStockQuantity * decConversionValue;
                                    else if (intConversionFactor == 2)
                                        decStockQuantity = decStockQuantity / decConversionValue;
                                }
                                // dgvLocationDetails.Rows[intRowIndex].Cells["LAvailableQuantity"].Value = decStockQuantity;

                                //dgvLocationDetails.Rows.Add();
                            }
                        }
                    }
                //    else
                //    {
                //        dtItemDetails.Rows.Clear();
                //        foreach (DataGridViewRow drItemRow in dgvPurchase.Rows)
                //        {
                //            bool blnIsExists = false;
                //            decimal decLocationQty = 0;
                //            if (drItemRow.Cells["ItemID"].Value != null && drItemRow.Cells["BatchID"].Value != null)
                //            {
                //                DataRow drItemDetailsRow = dtItemDetails.NewRow();
                //                drItemDetailsRow["ItemID"] = drItemRow.Cells["ItemID"].Value.ToInt32();
                //                drItemDetailsRow["ItemCode"] = drItemRow.Cells["ItemCode"].Value.ToString();
                //                drItemDetailsRow["ItemName"] = drItemRow.Cells["ItemName"].Value.ToString();
                //                drItemDetailsRow["BatchID"] = drItemRow.Cells["BatchID"].Value.ToInt64();
                //                drItemDetailsRow["BatchNo"] = drItemRow.Cells["BatchNo"].Value.ToString();
                //                drItemDetailsRow["UOMID"] = drItemRow.Cells["Uom"].Tag.ToInt32();
                //                dtItemDetails.Rows.Add(drItemDetailsRow);

                //                foreach (DataGridViewRow drLocationRow in dgvLocationDetails.Rows)
                //                {
                //                    if (drLocationRow.Cells["LItemID"].Value != null && drLocationRow.Cells["LBatchID"].Value != null)
                //                    {
                //                        if (drItemRow.Cells["ItemID"].Value.ToInt32() == drLocationRow.Cells["LItemID"].Value.ToInt32() && drItemRow.Cells["BatchID"].Value.ToInt64() == drLocationRow.Cells["LBatchID"].Value.ToInt32())
                //                        {
                //                            blnIsExists = true;
                //                            decLocationQty += drLocationRow.Cells["LQuantity"].Value.ToDecimal();
                //                            drLocationRow.Cells["LUOMID"].Value = drItemRow.Cells["UOM"].Tag.ToInt32();
                //                            drLocationRow.Cells["LUOM"].Value = drItemRow.Cells["UOM"].Tag.ToInt32();
                //                            drLocationRow.Cells["LUOM"].Tag = drItemRow.Cells["UOM"].Tag.ToInt32();
                //                            drLocationRow.Cells["LUOM"].Value = drLocationRow.Cells["LUOM"].FormattedValue;
                //                        }
                //                    }
                //                }
                //                if (!blnIsExists)
                //                {
                //                    //  dgvLocationDetails.Rows.Add();
                //                    dgvLocationDetails.RowCount = dgvLocationDetails.RowCount + 1;
                //                    int intRowIndex = dgvLocationDetails.RowCount - 2;
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value = drItemRow.Cells["ItemID"].Value.ToInt32();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LItemCode"].Value = drItemRow.Cells["ItemCode"].Value.ToString();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LItemName"].Value = drItemRow.Cells["ItemName"].Value.ToString();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LBatchID"].Value = drItemRow.Cells["BatchID"].Value.ToInt32();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LBatchNo"].Value = drItemRow.Cells["BatchNo"].Value.ToString();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LQuantity"].Value = drItemRow.Cells["ReturnQuantity"].Value.ToDecimal();

                //                    DataTable dtItemUOMs = MobjclsBLLPurchaseReturn.GetItemUOMs(drItemRow.Cells["ItemID"].Value.ToInt32());
                //                    LUOM.DataSource = null;
                //                    LUOM.ValueMember = "UOMID";
                //                    LUOM.DisplayMember = "ShortName";
                //                    LUOM.DataSource = dtItemUOMs;
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LUOMID"].Value = drItemRow.Cells["UOM"].Tag.ToInt32();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = drItemRow.Cells["UOM"].Tag.ToInt32();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Tag = drItemRow.Cells["UOM"].Tag.ToInt32();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].FormattedValue;

                //                    DataRow dr = MobjclsBLLPurchaseReturn.GetItemDefaultLocation(drItemRow.Cells["ItemID"].Value.ToInt32(), cboWarehouse.SelectedValue.ToInt32());

                //                    FillLocationGridCombo(LLocation.Index, intRowIndex);
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dr["LocationID"].ToInt32();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag = dr["LocationID"].ToInt32();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].FormattedValue;

                //                    FillLocationGridCombo(LRow.Index, intRowIndex);
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dr["RowID"].ToInt32();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag = dr["RowID"].ToInt32();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].FormattedValue;

                //                    FillLocationGridCombo(LBlock.Index, intRowIndex);
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dr["BlockID"].ToInt32();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag = dr["BlockID"].ToInt32();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].FormattedValue;

                //                    FillLocationGridCombo(LLocation.Index, intRowIndex);
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dr["LotID"].ToInt32();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag = dr["LotID"].ToInt32();
                //                    dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].FormattedValue;

                //                    clsDTOPurchaseReturnLocationDetails objLocationDetails = new clsDTOPurchaseReturnLocationDetails();
                //                    objLocationDetails.intItemID = dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value.ToInt32();
                //                    objLocationDetails.lngBatchID = dgvLocationDetails.Rows[intRowIndex].Cells["LBatchID"].Value.ToInt64();
                //                    objLocationDetails.intLocationID = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag.ToInt32();
                //                    objLocationDetails.intRowID = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag.ToInt32();
                //                    objLocationDetails.intBlockID = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag.ToInt32();
                //                    objLocationDetails.intLotID = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag.ToInt32();
                //                    MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intCompanyID = cboCompany.SelectedValue.ToInt32();
                //                    MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intWarehouseID = cboWarehouse.SelectedValue.ToInt32();

                //                    decimal decStockQuantity = MobjclsBLLPurchaseReturn.GetItemLocationQuantity(objLocationDetails);
                //                    DataTable dtUomDetails = new DataTable();
                //                    dtUomDetails = MobjclsBLLPurchaseReturn.GetUomConversionValues(dgvLocationDetails.Rows[intRowIndex].Cells["LUom"].Tag.ToInt32(), dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value.ToInt32());
                //                    if (dtUomDetails.Rows.Count > 0)
                //                    {
                //                        int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                //                        decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                //                        if (intConversionFactor == 1)
                //                            decStockQuantity = decStockQuantity * decConversionValue;
                //                        else if (intConversionFactor == 2)
                //                            decStockQuantity = decStockQuantity / decConversionValue;
                //                    }
                //                    //dgvLocationDetails.Rows[intRowIndex].Cells["LAvailableQuantity"].Value = decStockQuantity;

                //                    //dgvLocationDetails.Rows.Add();
                //                }
                //                else
                //                {
                //                    foreach (DataGridViewRow drLocationRow in dgvLocationDetails.Rows)
                //                    {
                //                        if (drLocationRow.Cells["LItemID"].Value != null && drLocationRow.Cells["LBatchID"].Value != null)
                //                        {
                //                            if (drItemRow.Cells["ItemID"].Value.ToInt32() == drLocationRow.Cells["LItemID"].Value.ToInt32() && drItemRow.Cells["BatchID"].Value.ToInt64() == drLocationRow.Cells["LBatchID"].Value.ToInt64())
                //                            {
                //                                if (drItemRow.Cells["ReturnQuantity"].Value.ToDecimal() - (decLocationQty - drLocationRow.Cells["LQuantity"].Value.ToDecimal()) <= 0)
                //                                    drLocationRow.Cells["LQuantity"].Value = 0;
                //                                else
                //                                    drLocationRow.Cells["LQuantity"].Value = drItemRow.Cells["ReturnQuantity"].Value.ToDecimal() - (decLocationQty - drLocationRow.Cells["LQuantity"].Value.ToDecimal());
                //                                break;
                //                            }
                //                        }
                //                    }
                //                }
                //            }
                //        }

                       
                //    }
                //}
                return true;
        }

        private bool FillLocationGridCombo(int inColumnIndex, int intRowIndex)
        {
            try
            {
                DataTable dtLocation = new DataTable();
                int intTag = 0;
                if (inColumnIndex == LLocation.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32();
                    dtLocation = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "Distinct LocationID,Location", "InvWarehouseDetails", "WarehouseID=" + cboWarehouse.SelectedValue.ToString() });

                    LLocation.DataSource = null;
                    LLocation.ValueMember = "LocationID";
                    LLocation.DisplayMember = "Location";
                    LLocation.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].FormattedValue;

                }
                else if (inColumnIndex == LRow.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag.ToInt32();
                    dtLocation = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "Distinct  RowID, RowNumber", "InvWarehouseDetails", "WarehouseID=" + cboWarehouse.SelectedValue.ToString() + " And LocationID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32() });
                    LRow.DataSource = null;
                    LRow.ValueMember = "RowID";
                    LRow.DisplayMember = "RowNumber";
                    LRow.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].FormattedValue;
                }
                else if (inColumnIndex == LBlock.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag.ToInt32();
                    dtLocation = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "Distinct BlockID,BlockNumber", "InvWarehouseDetails", " WarehouseID=" + cboWarehouse.SelectedValue.ToString() + " And LocationID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32() + " And RowID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag.ToInt32() });
                    LBlock.DataSource = null;
                    LBlock.ValueMember = "BlockID";
                    LBlock.DisplayMember = "BlockNumber";
                    LBlock.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].FormattedValue;
                }
                else if (inColumnIndex == LLot.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Tag.ToInt32();

                    dtLocation = MobjclsBLLPurchaseReturn.FillCombos(new string[] { " Distinct LotID, LotNumber", "InvWarehouseDetails", "WarehouseID=" + cboWarehouse.SelectedValue.ToString() + " And LocationID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32() + " And RowID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag.ToInt32() + " And BlockID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag.ToInt32() });
                    LLot.DataSource = null;
                    LLot.ValueMember = "LotID";
                    LLot.DisplayMember = "LotNumber";
                    LLot.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].FormattedValue;
                }

                return (dtLocation.Rows.Count > 0);

            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on FillLocationGridCombo(): " + this.Name + " " + Ex.Message.ToString(), 2);
                return false;
            }
        }

        private bool CheckLocationDuplication()
        {
            for (int intICounter = 0; intICounter < dgvLocationDetails.Rows.Count; intICounter++)
            {
                for (int intJCounter = intICounter + 1; intJCounter < dgvLocationDetails.Rows.Count; intJCounter++)
                {
                    if (dgvLocationDetails["LItemID", intICounter].Value.ToInt32() == dgvLocationDetails["LItemID", intJCounter].Value.ToInt32() &&
                          (dgvLocationDetails["LBatchID", intICounter].Value != null && dgvLocationDetails["LBatchID", intJCounter].Value != null && dgvLocationDetails["LBatchID", intICounter].Value.ToInt64() == dgvLocationDetails["LBatchID", intJCounter].Value.ToInt64()) &&
                          dgvLocationDetails["LLocation", intICounter].Tag.ToInt32() == dgvLocationDetails["LLocation", intJCounter].Tag.ToInt32() &&
                          dgvLocationDetails["LRow", intICounter].Tag.ToInt32() == dgvLocationDetails["LRow", intJCounter].Tag.ToInt32() &&
                          dgvLocationDetails["LBlock", intICounter].Tag.ToInt32() == dgvLocationDetails["LBlock", intJCounter].Tag.ToInt32() &&
                          dgvLocationDetails["LLot", intICounter].Tag.ToInt32() == dgvLocationDetails["LLot", intJCounter].Tag.ToInt32())
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 4235, out MmessageIcon).Replace("*", dgvLocationDetails["LItemName", intICounter].Value.ToString()).Replace("@", dgvLocationDetails["LBatchNo", intICounter].Value.ToString()).Replace("#", "");
                        MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        tcPurchaseReturn.SelectedTab = tiLocationDetails;
                        dgvLocationDetails.Focus();
                        dgvLocationDetails.CurrentCell = dgvLocationDetails["LLocation", intJCounter];
                        return false;
                    }
                }
            }
            return true;
        }

        private void dgvLocationDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == LLocation.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
            else if (e.ColumnIndex == LBlock.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
            else if (e.ColumnIndex == LRow.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
            else if (e.ColumnIndex == LLot.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
        }

        private void dgvLocationDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                dgvLocationDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                int intTag = 0;

                if (e.RowIndex >= 0 && e.ColumnIndex == LLocation.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LLocation"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LLocation"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LLocation"].Tag = dgvLocationDetails.CurrentRow.Cells["LLocation"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LLocation"].Value = dgvLocationDetails.CurrentRow.Cells["LLocation"].FormattedValue;
                        LRow.DataSource = null;
                        LBlock.DataSource = null;
                        LLot.DataSource = null;
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Value = "";
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = "";
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = "";
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LLocation"].Value = dgvLocationDetails.CurrentRow.Cells["LLocation"].FormattedValue;
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex == LRow.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LRow"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LRow"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Tag = dgvLocationDetails.CurrentRow.Cells["LRow"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Value = dgvLocationDetails.CurrentRow.Cells["LRow"].FormattedValue;

                        LBlock.DataSource = null;
                        LLot.DataSource = null;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = "";
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = "";
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Value = dgvLocationDetails.CurrentRow.Cells["LRow"].FormattedValue;
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex == LBlock.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LBlock"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag = dgvLocationDetails.CurrentRow.Cells["LBlock"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = dgvLocationDetails.CurrentRow.Cells["LBlock"].FormattedValue;
                        LLot.DataSource = null;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = "";
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = dgvLocationDetails.CurrentRow.Cells["LBlock"].FormattedValue;
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex == LLot.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LLot"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LLot"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = dgvLocationDetails.CurrentRow.Cells["LLot"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = dgvLocationDetails.CurrentRow.Cells["LLot"].FormattedValue;
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = dgvLocationDetails.CurrentRow.Cells["LLot"].FormattedValue;

                    clsDTOPurchaseReturnLocationDetails objLocationDetails = new clsDTOPurchaseReturnLocationDetails();
                    objLocationDetails.intItemID = dgvLocationDetails.CurrentRow.Cells["LItemID"].Value.ToInt32();
                    objLocationDetails.lngBatchID = dgvLocationDetails.CurrentRow.Cells["LBatchID"].Value.ToInt64();
                    objLocationDetails.intLocationID = dgvLocationDetails.CurrentRow.Cells["LLocation"].Tag.ToInt32();
                    objLocationDetails.intRowID = dgvLocationDetails.CurrentRow.Cells["LRow"].Tag.ToInt32();
                    objLocationDetails.intBlockID = dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag.ToInt32();
                    objLocationDetails.intLotID = dgvLocationDetails.CurrentRow.Cells["LLot"].Tag.ToInt32();
                    MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intCompanyID = cboCompany.SelectedValue.ToInt32();
                    MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intWarehouseID = cboWarehouse.SelectedValue.ToInt32();

                    decimal decStockQuantity = MobjclsBLLPurchaseReturn.GetItemLocationQuantity(objLocationDetails);
                    DataTable dtUomDetails = new DataTable();
                    dtUomDetails = MobjclsBLLPurchaseReturn.GetUomConversionValues(dgvLocationDetails.CurrentRow.Cells["LUom"].Tag.ToInt32(), dgvLocationDetails.CurrentRow.Cells["LItemID"].Value.ToInt32());
                    if (dtUomDetails.Rows.Count > 0)
                    {
                        int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                        decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                        if (intConversionFactor == 1)
                            decStockQuantity = decStockQuantity * decConversionValue;
                        else if (intConversionFactor == 2)
                            decStockQuantity = decStockQuantity / decConversionValue;
                    }
                    dgvLocationDetails.CurrentRow.Cells["LAvailableQuantity"].Value = decStockQuantity;
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        private void dgvLocationDetails_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dgvLocationDetails.PServerName = ClsCommonSettings.ServerName;
                string[] First = { "LItemCode", "LItemName", "LItemID", "LBatchNo", "LBatchID", "LUOMID" };//first grid 
                string[] second = { "ItemCode", "ItemName", "ItemID", "BatchNo", "BatchID", "UOMID" };//inner grid
                dgvLocationDetails.aryFirstGridParam = First;
                dgvLocationDetails.arySecondGridParam = second;
                dgvLocationDetails.PiFocusIndex = LQuantity.Index;
                dgvLocationDetails.bBothScrollBar = true;
                dgvLocationDetails.pnlLeft = expandableSplitterLeft.Location.X + 5;
                dgvLocationDetails.pnlTop = expandableSplitterTop.Location.Y + PurchaseOrderBindingNavigator.Height + 10;
                dgvLocationDetails.ColumnsToHide = new string[] { "ItemID", "UOMID", "BatchID" };
                if (dgvLocationDetails.CurrentCell.ColumnIndex == LItemCode.Index)
                {
                    dgvLocationDetails.field = "ItemCode";
                    dgvLocationDetails.CurrentCell.Value = dgvLocationDetails.TextBoxText;
                }
                if (dgvLocationDetails.CurrentCell.ColumnIndex == LItemName.Index)
                {
                    // dgvPurchase.PiColumnIndex = 1;
                    dgvLocationDetails.field = "ItemName";
                    dgvLocationDetails.CurrentCell.Value = dgvLocationDetails.TextBoxText;
                }
                string strFilterString = dgvLocationDetails.field + " Like '" + dgvLocationDetails.TextBoxText + "%'";
                dtItemDetails.DefaultView.RowFilter = strFilterString;
                dgvLocationDetails.dtDataSource = dtItemDetails.DefaultView.ToTable();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvItemGrid_TextBoxChangedEvent() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvItemGrid_TextBoxChangedEvent() " + ex.Message, 2);
            }
        }

        private void dgvLocationDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvLocationDetails.EditingControl.KeyPress += new KeyPressEventHandler(dgvLocationEditingControl_KeyPress);
        }

        void dgvLocationEditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvLocationDetails.CurrentCell.OwningColumn.Index == LQuantity.Index)
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
                int intUomScale = 0;
                if (dgvLocationDetails[LUOM.Index, dgvLocationDetails.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    intUomScale = MobjclsBLLPurchaseReturn.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvLocationDetails[LUOM.Index, dgvLocationDetails.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    if (intUomScale == 0)
                    {
                        if (e.KeyChar == '.')
                            e.Handled = true;
                    }
                    else
                    {
                        int dotIndex = -1;
                        if (txt.Text.Contains("."))
                        {
                            dotIndex = txt.Text.IndexOf('.');
                        }
                        if (e.KeyChar == '.')
                        {
                            if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            {
                                e.Handled = true;
                            }
                        }
                        else
                        {
                            if (char.IsDigit(e.KeyChar))
                            {
                                if (dotIndex != -1 && txt.SelectionStart > dotIndex)
                                {
                                    string[] splitText = txt.Text.Split('.');
                                    if (splitText.Length == 2)
                                    {
                                        if (splitText[1].Length - txt.SelectedText.Length >= intUomScale)
                                            e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void dgvLocationDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvLocationDetails.IsCurrentCellDirty)
            {
                if (dgvLocationDetails.CurrentCell != null)
                    dgvLocationDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
        }
        private void DisplayLocationDetails()
        {
            DataTable datLocationDetails = new DataTable();
            dgvLocationDetails.Rows.Clear();

            datLocationDetails = MobjclsBLLPurchaseReturn.GetPurchaseReturnLocationDetails();

            for (int intRowIndex = 0; intRowIndex < datLocationDetails.Rows.Count; intRowIndex++)
            {
                dgvLocationDetails.RowCount = dgvLocationDetails.RowCount + 1;
                dgvLocationDetails["LItemID", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["ItemID"];
                dgvLocationDetails["LItemCode", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["Code"];
                dgvLocationDetails["LItemName", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["ItemName"];
                dgvLocationDetails["LBatchNo", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["BatchNo"];
                dgvLocationDetails["LBatchID", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["BatchID"];
                dgvLocationDetails["LQuantity", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["Quantity"];

                DataTable dtItemUOMs = MobjclsBLLPurchaseReturn.GetItemUOMs(datLocationDetails.Rows[intRowIndex]["ItemID"].ToInt32());
                LUOM.DataSource = null;
                LUOM.ValueMember = "UOMID";
                LUOM.DisplayMember = "ShortName";
                LUOM.DataSource = dtItemUOMs;
                dgvLocationDetails.Rows[intRowIndex].Cells["LUOMID"].Value = datLocationDetails.Rows[intRowIndex]["UOMID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = datLocationDetails.Rows[intRowIndex]["UOMID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Tag = datLocationDetails.Rows[intRowIndex]["UOMID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].FormattedValue;

                FillLocationGridCombo(LLocation.Index, intRowIndex);
                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = datLocationDetails.Rows[intRowIndex]["LocationID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag = datLocationDetails.Rows[intRowIndex]["LocationID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].FormattedValue;

                FillLocationGridCombo(LRow.Index, intRowIndex);
                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = datLocationDetails.Rows[intRowIndex]["RowID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag = datLocationDetails.Rows[intRowIndex]["RowID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].FormattedValue;

                FillLocationGridCombo(LBlock.Index, intRowIndex);
                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = datLocationDetails.Rows[intRowIndex]["BlockID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag = datLocationDetails.Rows[intRowIndex]["BlockID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].FormattedValue;

                FillLocationGridCombo(LLot.Index, intRowIndex);
                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = datLocationDetails.Rows[intRowIndex]["LotID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag = datLocationDetails.Rows[intRowIndex]["LotID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].FormattedValue;

                clsDTOPurchaseReturnLocationDetails objLocationDetails = new clsDTOPurchaseReturnLocationDetails();
                objLocationDetails.intItemID = dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value.ToInt32();
                objLocationDetails.lngBatchID = dgvLocationDetails.Rows[intRowIndex].Cells["LBatchID"].Value.ToInt64();
                objLocationDetails.intLocationID = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag.ToInt32();
                objLocationDetails.intRowID = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag.ToInt32();
                objLocationDetails.intBlockID = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag.ToInt32();
                objLocationDetails.intLotID = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag.ToInt32();
                MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intCompanyID = cboCompany.SelectedValue.ToInt32();
                MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intWarehouseID = cboWarehouse.SelectedValue.ToInt32();
                decimal decStockQuantity = MobjclsBLLPurchaseReturn.GetItemLocationQuantity(objLocationDetails);
                DataTable dtUomDetails = new DataTable();
                dtUomDetails = MobjclsBLLPurchaseReturn.GetUomConversionValues(dgvLocationDetails.Rows[intRowIndex].Cells["LUom"].Tag.ToInt32(), dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value.ToInt32());
                if (dtUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                        decStockQuantity = decStockQuantity * decConversionValue;
                    else if (intConversionFactor == 2)
                        decStockQuantity = decStockQuantity / decConversionValue;
                }
             //   dgvLocationDetails.Rows[intRowIndex].Cells["LAvailableQuantity"].Value = decStockQuantity;
            }
        }

        private void FillLocationDetailsParameters()
        {
            if (dgvLocationDetails.CurrentCell != null)
            {
                dgvLocationDetails.CurrentCell = dgvLocationDetails[LItemCode.Index, dgvLocationDetails.CurrentCell.RowIndex];
            }
            MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.lstLocationDetails = new List<clsDTOPurchaseReturnLocationDetails>();
            foreach (DataGridViewRow dr in dgvLocationDetails.Rows)
            {
                if (dr.Cells["LItemID"].Value != null && dr.Cells["LBatchID"].Value != null)
                {
                    clsDTOPurchaseReturnLocationDetails objLocationDetails = new clsDTOPurchaseReturnLocationDetails();
                    objLocationDetails.intItemID = dr.Cells["LItemID"].Value.ToInt32();
                    objLocationDetails.lngBatchID = dr.Cells["LBatchID"].Value.ToInt64();
                    objLocationDetails.strBatchNo = dr.Cells["LBatchNo"].Value.ToString();
                    objLocationDetails.decQuantity = dr.Cells["LQuantity"].Value.ToDecimal();
                    objLocationDetails.intUOMID = dr.Cells["LUOM"].Tag.ToInt32();
                    objLocationDetails.intLocationID = dr.Cells["LLocation"].Tag.ToInt32();
                    objLocationDetails.intRowID = dr.Cells["LRow"].Tag.ToInt32();
                    objLocationDetails.intBlockID = dr.Cells["LBlock"].Tag.ToInt32();
                    objLocationDetails.intLotID = dr.Cells["LLot"].Tag.ToInt32();
                    MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.lstLocationDetails.Add(objLocationDetails);
                }
            }
        }
        private void dgvLocationDetails_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == LItemCode.Index) || (e.ColumnIndex == LItemName.Index))
            {
                dgvLocationDetails.PiColumnIndex = e.ColumnIndex;
            }
        }

        private void dgvLocationDetails_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {

        }

        private void dgvLocationDetails_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            bool blnValid = false;
            if (dgvLocationDetails.Rows[e.Row.Index].Cells["LItemID"].Value.ToInt32() != 0 && dgvLocationDetails.Rows[e.Row.Index].Cells["LBatchID"].Value != null)
            {
                foreach (DataGridViewRow dr in dgvLocationDetails.Rows)
                {

                    if (dr.Cells["LItemID"].Value.ToInt32() != 0 && dr.Cells["LBatchID"].Value != null)
                    {
                        if (dr.Index != e.Row.Index && dr.Cells["LItemID"].Value.ToInt32() == dgvLocationDetails.Rows[e.Row.Index].Cells["LItemID"].Value.ToInt32() && dr.Cells["LBatchID"].Value.ToInt64() == dgvLocationDetails.Rows[e.Row.Index].Cells["LBatchID"].Value.ToInt64())
                        {
                            blnValid = true;
                            break;
                        }
                    }
                }
            }
            else
            {
                blnValid = true;
            }
            if (!blnValid)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 929, out MmessageIcon).Replace("#", "");
                MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                e.Cancel = true;
            }
        }
   
        private bool ValidateStockQuantity(int intItemID, long intBatchID, decimal decQuantity)
        {
            decimal decStockQuantity = MobjclsBLLPurchaseReturn.GetStockQuantity(intItemID, intBatchID, Convert.ToInt32(cboCompany.SelectedValue.ToInt32()), Convert.ToInt32(cboWarehouse.SelectedValue));
            if (decQuantity > decStockQuantity)
            {
                return false;
            }
            return true;
        }

        private bool ValidateLocationQuantity()
        {
            DataTable datLocationDetails = new DataTable();
            bool blnValid = true;
            datLocationDetails = MobjclsBLLPurchaseReturn.GetPurchaseReturnLocationDetails();

            foreach (DataGridViewRow dr in dgvLocationDetails.Rows)
            {
                if (dr.Cells["LItemID"].Value.ToInt32() != 0 && dr.Cells["LBatchID"].Value.ToInt64() != 0)
                {
                    clsDTOPurchaseReturnLocationDetails objLocationDetails = new clsDTOPurchaseReturnLocationDetails();
                    objLocationDetails.intItemID = dr.Cells["LItemID"].Value.ToInt32();
                    objLocationDetails.lngBatchID = dr.Cells["LBatchID"].Value.ToInt64();
                    objLocationDetails.intLocationID = dr.Cells["LLocation"].Tag.ToInt32();
                    objLocationDetails.intRowID = dr.Cells["LRow"].Tag.ToInt32();
                    objLocationDetails.intBlockID = dr.Cells["LBlock"].Tag.ToInt32();
                    objLocationDetails.intLotID = dr.Cells["LLot"].Tag.ToInt32();

                    decimal decAvailableQty = MobjclsBLLPurchaseReturn.GetItemLocationQuantity(objLocationDetails);
                    DataTable dtGetUomDetails = MobjclsBLLPurchaseReturn.GetUomConversionValues(dr.Cells["LUOM"].Tag.ToInt32(), dr.Cells["LItemID"].Value.ToInt32());
                    datLocationDetails.DefaultView.RowFilter = "ItemID = " + objLocationDetails.intItemID + " And BatchID = " + objLocationDetails.lngBatchID + " And LocationID = " + objLocationDetails.intLocationID + " And RowID = " + objLocationDetails.intRowID + " And BlockID = " + objLocationDetails.intBlockID + " And LotID = " + objLocationDetails.intLotID;
                    decimal decOldQuantity = 0, decCurrentQty = 0;
                    if (datLocationDetails.DefaultView.ToTable().Rows.Count > 0)
                        decOldQuantity = datLocationDetails.DefaultView.ToTable().Rows[0]["Quantity"].ToDecimal();
                    decCurrentQty = dr.Cells["LQuantity"].Value.ToDecimal();
                    if (dtGetUomDetails.Rows.Count > 0)
                    {
                        int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                        decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                        if (intConversionFactor == 1)
                        {
                            decOldQuantity = decOldQuantity / decConversionValue;
                            decCurrentQty = decCurrentQty / decConversionValue;
                        }
                        else if (intConversionFactor == 2)
                        {
                            decOldQuantity = decOldQuantity * decConversionValue;
                            decCurrentQty = decCurrentQty * decConversionValue;
                        }
                    }

                    if ((decAvailableQty + decOldQuantity) - decCurrentQty < 0)
                    {
                        blnValid = false;
                    }
                    if (!blnValid)
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 985, out MmessageIcon).Replace("#", "").Replace("@", dr.Cells["LItemName"].Value.ToString());
                        MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        tcPurchaseReturn.SelectedTab = tiLocationDetails;
                        dgvLocationDetails.Focus();
                        break;
                    }
                }
            }
            return blnValid;
        }
      

        private void tcPurchase_SelectedTabChanged(object sender, TabStripTabChangedEventArgs e)
        {
            if (tcPurchaseReturn.SelectedTab == tiLocationDetails)
            {
                FillItemLocationDetails();
            }
        }

        private void dgvLocationDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (e.ColumnIndex == LUOMID.Index)
                {
                    DataTable dtItemUOMs = MobjclsBLLPurchaseReturn.GetItemUOMs(dgvLocationDetails["LItemID", e.RowIndex].Value.ToInt32());
                    LUOM.DataSource = null;
                    LUOM.ValueMember = "UOMID";
                    LUOM.DisplayMember = "ShortName";
                    LUOM.DataSource = dtItemUOMs;
                    dgvLocationDetails.Rows[e.RowIndex].Cells["LUOM"].Value = dgvLocationDetails["LUOMID", e.RowIndex].Value.ToInt32();
                    dgvLocationDetails.Rows[e.RowIndex].Cells["LUOM"].Tag = dgvLocationDetails["LUOMID", e.RowIndex].Value.ToInt32();
                    dgvLocationDetails.Rows[e.RowIndex].Cells["LUOM"].Value = dgvLocationDetails.Rows[e.RowIndex].Cells["LUOM"].FormattedValue;
                }
            }
            Changestatus(sender, e);
        }
        private bool ValidateLocationGrid()
        {
            bool blnValid = true;
            int intColumnIndex = 0;
            foreach (DataGridViewRow drItemRow in dgvPurchase.Rows)
            {
                decimal decLocationQty = 0;
                foreach (DataGridViewRow drLocationRow in dgvLocationDetails.Rows)
                {
                    if (drLocationRow.Cells["LItemID"].Value.ToInt32() != 0)
                    {
                        if (drLocationRow.Cells["LLocation"].Tag.ToInt32() == 0)
                        {
                            MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 924, out MmessageIcon);
                            intColumnIndex = LLocation.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LRow"].Tag.ToInt32() == 0)
                        {
                            MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 931, out MmessageIcon);
                            intColumnIndex = LRow.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LBlock"].Tag.ToInt32() == 0)
                        {
                            MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 932, out MmessageIcon);
                            intColumnIndex = LBlock.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LLot"].Tag.ToInt32() == 0)
                        {
                            MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 930, out MmessageIcon);
                            intColumnIndex = LLot.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LQuantity"].Value.ToDecimal() <= 0)
                        {
                            MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 928, out MmessageIcon);
                            intColumnIndex = LQuantity.Index;
                            blnValid = false;
                        }
                        if (!blnValid)
                        {
                            MessageBox.Show(MstrMessageCommon.Replace("#",""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            TmrPurchaseReturn.Enabled = true;
                            lblPurchaseReturnStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1); 
                            tcPurchaseReturn.SelectedTab = tiLocationDetails;
                            dgvLocationDetails.Focus();
                            dgvLocationDetails.CurrentCell = dgvLocationDetails[intColumnIndex, drLocationRow.Index];
                            break;
                        }
                    }
                    if (blnValid && drLocationRow.Cells["LItemID"].Value.ToInt32() != 0 && drLocationRow.Cells["LBatchID"].Value != null)
                    {
                        if (drLocationRow.Cells["LItemID"].Value.ToInt32() == drItemRow.Cells["ItemID"].Value.ToInt32() && drLocationRow.Cells["LBatchID"].Value.ToString() == drItemRow.Cells["BatchID"].Value.ToString())
                            decLocationQty += drLocationRow.Cells["LQuantity"].Value.ToDecimal();
                    }
                }
                if (!blnValid) break;
                if (blnValid && drItemRow.Cells["ReturnQuantity"].Value.ToDecimal() != decLocationQty)
                {
                    blnValid = false;
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 933, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#",""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    TmrPurchaseReturn.Enabled = true;
                    lblPurchaseReturnStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1); ;
                    tcPurchaseReturn.SelectedTab = tiLocationDetails;
                    dgvLocationDetails.Focus();
                    dgvLocationDetails.CurrentCell = dgvLocationDetails[intColumnIndex, drItemRow.Index];
                    break;
                }
            }
            if (ClsCommonSettings.PblnIsLocationWise && blnValid && !ValidateLocationQuantity())
            {
                blnValid = false;
              
            }
            return blnValid;
        }


        private void txtGrandReturnAmount_TextChanged(object sender, EventArgs e)
        {
            ErrPurchaseReturn.Clear();
            Changestatus(null, null);
            CalculateExchangeCurrencyRate();

            if(txtGrandReturnAmount.Text != string.Empty && txtGrandReturnAmount.Text != ".")
            lblAmountInWords.Text = MobjclsBLLCommonUtility.ConvertToWord(Convert.ToString(Convert.ToDecimal(txtGrandReturnAmount.Text)), Convert.ToInt32(cboCurrency.SelectedValue));
        }

        private void expandableSplitterLeft_ExpandedChanged(object sender, ExpandedChangeEventArgs e)
        {
            if (expandableSplitterLeft.Expanded)
                Reason.AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
            else
                Reason.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            if (expandableSplitterLeft.Expanded)
            {
                if (DgvSOrderDisplay.Columns.Count > 0)
                    DgvSOrderDisplay.Columns[0].Visible = false;
            }
        }

        private void btnPickList_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intPurchaseReturnID) > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PblnPickList = true;
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = Convert.ToInt32(MobjclsBLLPurchaseReturn.PobjClsDTOPurchaseReturnMaster.intPurchaseReturnID);
                    ObjViewer.PiFormID = (int)FormID.DebitNote;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form FrmPurchaseReturn:LoadReport " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnPickList_Click " + Ex.Message.ToString());
            }
        }

        private void btnPickListEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Debit Note Location Details";
                    ObjEmailPopUp.EmailFormType = EmailFormID.PickList;
                    ObjEmailPopUp.EmailSource = MobjclsBLLPurchaseReturn.DisplayDebitNoteLocationReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on Email:btnPickListEmail_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnPickListEmail_Click " + Ex.Message.ToString());
            }
        }

        private void btnSupplierHistory_Click(object sender, EventArgs e)
        {
            if (cboSupplier.SelectedIndex == -1)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 908, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                cboSupplier.Focus();
                return;
            }
            using (FrmVendorHistory objFrmVendorHistory = new FrmVendorHistory(2, cboSupplier.SelectedValue.ToInt32()))
            {
                objFrmVendorHistory.ShowDialog();
            }
        }

        private void FrmDebitNote_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.MdiFormClosing)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(datMessages, 4214, out MmessageIcon).Replace("#", "").Trim();
                if (MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                    DialogResult.Yes)
                {
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void lnkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DataTable datAdvanceSearchedData = new DataTable();
            using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.DebitNote))
            {
                objFrmSearchForm.ShowDialog();
                if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                {
                    datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                    datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                    DgvSOrderDisplay.DataSource = datAdvanceSearchedData;
                    DgvSOrderDisplay.Columns["ID"].Visible = false;
                    DgvSOrderDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

        private void txtGrandReturnAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in txtGrandReturnAmount_KeyPress() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in txtGrandReturnAmount_KeyPress() " + ex.Message, 2);
            }
        }

        private void dgvPurchase_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjclsBLLCommonUtility.SetSerialNo(dgvPurchase, e.RowIndex, false);
        }

        private void dgvPurchase_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjclsBLLCommonUtility.SetSerialNo(dgvPurchase, e.RowIndex, false);
        }

        private void cboAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus(sender,e);
        }

        private void btnAccount_Click(object sender, EventArgs e)
        {
            using (FrmChartOfAccounts objChartOfAccounts = new FrmChartOfAccounts())
            {
                try
                {
                    // user will only be able to create account head or group under the account group that we set here
                    objChartOfAccounts.PermittedAccountGroup = AccountGroups.PurchaseAccounts;
                    if (this.cboAccount.SelectedIndex != -1)
                    {
                        objChartOfAccounts.SelectedAccount = (Accounts)this.cboAccount.SelectedValue.ToInt32();
                    }

                    objChartOfAccounts.MbCalledFromAccountSettings = true;
                }
                catch
                {
                }
                objChartOfAccounts.ShowDialog();
            }
            int intComboID = cboAccount.SelectedValue.ToInt32();
            GetAccount();
            cboAccount.SelectedValue = intComboID;        
        }

        private void TmrPurchaseReturn_Tick(object sender, EventArgs e)
        {
            ErrPurchaseReturn.Clear();
            TmrPurchaseReturn.Enabled = false;
        }

        private void dtpReturnDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            Changestatus(sender, e);
        }

        private void dtpReturnDate_ValueChanged(object sender, EventArgs e)
        {
            Changestatus(sender, e);
        }

        private void cboTaxScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalculateNetTotal();
        }
    }
}