﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmCancellationDetails : DevComponents.DotNetBar.Office2007Form
    {
        public DateTime PDtCancellationDate { get; set; }
        public string PStrDescription { get; set; }
        public bool PBlnIsCanelled { get; set; }
        public int PintVerificationCriteria { get; set; }
        private bool MblnShowVerificationCriteria = false;

        public FrmCancellationDetails(bool blnShowVerificationCriteria)
        {
            InitializeComponent();
            MblnShowVerificationCriteria = blnShowVerificationCriteria;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            PDtCancellationDate = ClsCommonSettings.GetServerDate();
            PStrDescription = null;
            PBlnIsCanelled = true;
            PintVerificationCriteria = 0;
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            PStrDescription = txtDescription.Text.Trim();
            PDtCancellationDate = dtpCancelledDate.Value;
            PBlnIsCanelled = false;
            if (MblnShowVerificationCriteria)
                PintVerificationCriteria = cboVerificationCriteria.SelectedValue.ToInt32();
            else
                PintVerificationCriteria = 0;
            this.Close();
        }

        private void FrmCancellationDetails_Load(object sender, EventArgs e)
        {
            PBlnIsCanelled = true;

            if (MblnShowVerificationCriteria)
                lblVerificationCriteria.Visible = cboVerificationCriteria.Visible  = btnVerificationCriteria.Visible= true ;
            else
            {
                groupPanel1.Location = new Point(12, 16);
                groupPanel1.Height = groupPanel1.Height + 26;
                txtDescription.Height = txtDescription.Height + 26;
            }

            LoadCombo();
        }

        private void LoadCombo()
        {
            ClsCommonUtility objClsCommon = new ClsCommonUtility();
            DataTable datCombos = objClsCommon.FillCombos(new string[] { "VerificationCriteriaID,VerificationCriteria", "InvVerificationCriteriaReference", "" });
            cboVerificationCriteria.ValueMember = "VerificationCriteriaID";
            cboVerificationCriteria.DisplayMember = "VerificationCriteria";
            cboVerificationCriteria.DataSource = datCombos;
        }

        private void FrmCancellationDetails_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (PBlnIsCanelled)
            {
                PDtCancellationDate = ClsCommonSettings.GetServerDate();
                PStrDescription = null;
                PintVerificationCriteria = 0;
            }
        }

        private void FrmCancellationDetails_Shown(object sender, EventArgs e)
        {
            txtDescription.Focus();
        }

        private void FrmCancellationDetails_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                   
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnVerificationCriteria_Click(object sender, EventArgs e)
        {
            FrmCommonRef objCommon = new FrmCommonRef("Verification Criteria", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "VerificationCriteriaID,VerificationCriteria", "InvVerificationCriteriaReference", "");
            objCommon.ShowDialog();
            objCommon.Dispose();
            LoadCombo();
        }
    }
}
