using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data;
using System.Data.SqlClient;

using Microsoft.ReportingServices.ReportRendering;
using System.Collections.Generic;


/* 
================================================= 
   Author:		<Author,,Midhun>
   Create date: <Create Date,,22 Feb 2011>
   Description:	<Description,,Purchase Indent Form>
================================================
*/
namespace MyBooksERP
{
    public class FrmPurchaseIndent : DevComponents.DotNetBar.Office2007Form
    {

        # region Designer

        public Command cmdCommandZoom;
        private DockContainerItem dockContainerItem1;
        private ExpandableSplitter expandableSplitterLeft;
        private PanelEx PanelLeft;
        private IContainer components;
        private DotNetBarManager dotNetBarManager1;
        private DockSite dockSite4;
        private DockSite dockSite1;
        private DockSite dockSite2;
        private DockSite dockSite3;
        private DockSite dockSite5;
        private DockSite dockSite6;
        private Bar bar1;
        private DockSite dockSite8;
        private PanelEx panelEx1;
        private Bar PurchaseOrderBindingNavigator;
        private ButtonItem BindingNavigatorAddNewItem;
        private ExpandableSplitter expandableSplitterTop;
        private PanelEx panelTop;
        private ExpandablePanel expandablePanel1;
        private LabelX lblIndentCount;
        private PanelEx panelBottom;
        private PanelEx panelGridBottom;
        private Label lblRemarks;
        private PanelEx panelLeftTop;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvPurchaseIndentDisplay;
        private Label LblScompany;
        private ClsInnerGridBar dgvPurchaseIndent;
        private ButtonItem BindingNavigatorSaveItem;
        private ButtonItem BindingNavigatorClearItem;
        private ButtonItem BindingNavigatorDeleteItem;
        private ButtonItem BtnItem;
        private ButtonItem BtnUom;
        private ButtonX btnSRefresh;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSIndentNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSCompany;
        private Label lblCompany;
        private System.Windows.Forms.DateTimePicker dtpDueDate;
        private System.Windows.Forms.DateTimePicker dtpIndentDate;
        private Label lblDueDate;
        private Label lblIndentDate;
        private Label lblIndentNo;
        internal Timer tmrPurchaseIndent;
        internal Timer tmrFocus;
        internal ErrorProvider errPurchaseIndent;
        private ButtonItem BtnPrint;
        private ButtonItem BtnEmail;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private Label lblDepartment;
        private ImageList imgPurchaseIndent;
        private DevComponents.DotNetBar.Controls.TextBoxX txtRemarks;
        private DevComponents.DotNetBar.Controls.TextBoxX txtIndentNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDepartment;
        private ButtonX btnDepartment;
        private ComboBox cb;
        private clsBLLPurchaseIndent MobjClsBLLPurchaseIndent;//Object Of clsBllPurchaseIndent
        private Label lblStatus;
        private Label lblDescription;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSStatus;
        private Label lblSStatus;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSDepartment;
        private Label lblSDepartment;
        private Label lblStatusText;
        private ButtonItem BindingNavigatorCancelItem;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSExecutive;
        private Label lblSExecutive;
        private System.Windows.Forms.DateTimePicker dtpSFrom;
        private Label lblTo;
        private Label lblFrom;
        private System.Windows.Forms.DateTimePicker dtpSTo;
        private DataGridViewTextBoxColumn ItemCode;
        private DataGridViewTextBoxColumn ItemName;
        private DataGridViewTextBoxColumn Quantity;
        private DataGridViewComboBoxColumn Uom;
        private DataGridViewTextBoxColumn ItemID;
        private ButtonItem BtnHelp;
        private Panel pnlBottom;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedByText;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedDateValue;
        private DevComponents.DotNetBar.Controls.WarningBox lblPurchaseIndentStatus;
        private DevComponents.DotNetBar.TabControl tcGeneral;
        private TabControlPanel tabControlPanel3;
        private TabItem tiGeneral;
        private TabItem tiSuggestions;
        private DevComponents.DotNetBar.TabControl tcPurchaseIndent;
        private TabControlPanel tabControlPanel1;
        private TabItem tpItemDetails;
        private Label lblSIndentNo;
        private LinkLabel lnkLabel;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDescription;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPurchaseIndent));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cmdCommandZoom = new DevComponents.DotNetBar.Command(this.components);
            this.dockContainerItem1 = new DevComponents.DotNetBar.DockContainerItem();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvPurchaseIndentDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelLeftTop = new DevComponents.DotNetBar.PanelEx();
            this.lnkLabel = new System.Windows.Forms.LinkLabel();
            this.lblSIndentNo = new System.Windows.Forms.Label();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cboSExecutive = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSExecutive = new System.Windows.Forms.Label();
            this.cboSStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSStatus = new System.Windows.Forms.Label();
            this.cboSDepartment = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSDepartment = new System.Windows.Forms.Label();
            this.btnSRefresh = new DevComponents.DotNetBar.ButtonX();
            this.txtSIndentNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboSCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.LblScompany = new System.Windows.Forms.Label();
            this.lblIndentCount = new DevComponents.DotNetBar.LabelX();
            this.lblIndentNo = new System.Windows.Forms.Label();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.dotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.dockSite4 = new DevComponents.DotNetBar.DockSite();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.dockSite2 = new DevComponents.DotNetBar.DockSite();
            this.dockSite8 = new DevComponents.DotNetBar.DockSite();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.dockSite6 = new DevComponents.DotNetBar.DockSite();
            this.dockSite3 = new DevComponents.DotNetBar.DockSite();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.panelBottom = new DevComponents.DotNetBar.PanelEx();
            this.tcPurchaseIndent = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvPurchaseIndent = new ClsInnerGridBar();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Uom = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpItemDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.panelGridBottom = new DevComponents.DotNetBar.PanelEx();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.lblCreatedByText = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblCreatedDateValue = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblPurchaseIndentStatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.txtRemarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.expandableSplitterTop = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.tcGeneral = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.lblCompany = new System.Windows.Forms.Label();
            this.lblStatusText = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblIndentDate = new System.Windows.Forms.Label();
            this.txtDescription = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblDueDate = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.dtpIndentDate = new System.Windows.Forms.DateTimePicker();
            this.btnDepartment = new DevComponents.DotNetBar.ButtonX();
            this.dtpDueDate = new System.Windows.Forms.DateTimePicker();
            this.txtIndentNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblDepartment = new System.Windows.Forms.Label();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboDepartment = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.tiGeneral = new DevComponents.DotNetBar.TabItem(this.components);
            this.PurchaseOrderBindingNavigator = new DevComponents.DotNetBar.Bar();
            this.BindingNavigatorAddNewItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorSaveItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorClearItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorDeleteItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorCancelItem = new DevComponents.DotNetBar.ButtonItem();
            this.BtnItem = new DevComponents.DotNetBar.ButtonItem();
            this.BtnUom = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.BtnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.BtnHelp = new DevComponents.DotNetBar.ButtonItem();
            this.tmrPurchaseIndent = new System.Windows.Forms.Timer(this.components);
            this.tmrFocus = new System.Windows.Forms.Timer(this.components);
            this.errPurchaseIndent = new System.Windows.Forms.ErrorProvider(this.components);
            this.imgPurchaseIndent = new System.Windows.Forms.ImageList(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiSuggestions = new DevComponents.DotNetBar.TabItem(this.components);
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchaseIndentDisplay)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.panelEx1.SuspendLayout();
            this.panelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcPurchaseIndent)).BeginInit();
            this.tcPurchaseIndent.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchaseIndent)).BeginInit();
            this.panelGridBottom.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).BeginInit();
            this.tcGeneral.SuspendLayout();
            this.tabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errPurchaseIndent)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdCommandZoom
            // 
            this.cmdCommandZoom.Name = "cmdCommandZoom";
            // 
            // dockContainerItem1
            // 
            this.dockContainerItem1.Name = "dockContainerItem1";
            this.dockContainerItem1.Text = "dockContainerItem1";
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.dgvPurchaseIndentDisplay);
            this.PanelLeft.Controls.Add(this.expandablePanel1);
            this.PanelLeft.Controls.Add(this.lblIndentCount);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(200, 514);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 9;
            this.PanelLeft.Text = "panelEx1";
            this.PanelLeft.Visible = false;
            // 
            // dgvPurchaseIndentDisplay
            // 
            this.dgvPurchaseIndentDisplay.AllowUserToAddRows = false;
            this.dgvPurchaseIndentDisplay.AllowUserToDeleteRows = false;
            this.dgvPurchaseIndentDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPurchaseIndentDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPurchaseIndentDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPurchaseIndentDisplay.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPurchaseIndentDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPurchaseIndentDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvPurchaseIndentDisplay.Location = new System.Drawing.Point(0, 227);
            this.dgvPurchaseIndentDisplay.Name = "dgvPurchaseIndentDisplay";
            this.dgvPurchaseIndentDisplay.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPurchaseIndentDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvPurchaseIndentDisplay.RowHeadersVisible = false;
            this.dgvPurchaseIndentDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPurchaseIndentDisplay.Size = new System.Drawing.Size(200, 261);
            this.dgvPurchaseIndentDisplay.TabIndex = 20;
            this.dgvPurchaseIndentDisplay.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPurchaseIndentDisplay_CellDoubleClick);
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expandablePanel1.Controls.Add(this.panelLeftTop);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(200, 227);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 17;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Search";
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelLeftTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelLeftTop.Controls.Add(this.lnkLabel);
            this.panelLeftTop.Controls.Add(this.lblSIndentNo);
            this.panelLeftTop.Controls.Add(this.dtpSTo);
            this.panelLeftTop.Controls.Add(this.dtpSFrom);
            this.panelLeftTop.Controls.Add(this.lblTo);
            this.panelLeftTop.Controls.Add(this.lblFrom);
            this.panelLeftTop.Controls.Add(this.cboSExecutive);
            this.panelLeftTop.Controls.Add(this.lblSExecutive);
            this.panelLeftTop.Controls.Add(this.cboSStatus);
            this.panelLeftTop.Controls.Add(this.lblSStatus);
            this.panelLeftTop.Controls.Add(this.cboSDepartment);
            this.panelLeftTop.Controls.Add(this.lblSDepartment);
            this.panelLeftTop.Controls.Add(this.btnSRefresh);
            this.panelLeftTop.Controls.Add(this.txtSIndentNo);
            this.panelLeftTop.Controls.Add(this.cboSCompany);
            this.panelLeftTop.Controls.Add(this.LblScompany);
            this.panelLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftTop.Location = new System.Drawing.Point(0, 26);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(200, 200);
            this.panelLeftTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelLeftTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelLeftTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelLeftTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelLeftTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelLeftTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelLeftTop.Style.GradientAngle = 90;
            this.panelLeftTop.TabIndex = 108;
            // 
            // lnkLabel
            // 
            this.lnkLabel.AutoSize = true;
            this.lnkLabel.Location = new System.Drawing.Point(3, 181);
            this.lnkLabel.Name = "lnkLabel";
            this.lnkLabel.Size = new System.Drawing.Size(87, 13);
            this.lnkLabel.TabIndex = 259;
            this.lnkLabel.TabStop = true;
            this.lnkLabel.Text = "Advance Search";
            this.lnkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLabel_LinkClicked);
            // 
            // lblSIndentNo
            // 
            this.lblSIndentNo.AutoSize = true;
            this.lblSIndentNo.BackColor = System.Drawing.Color.Transparent;
            this.lblSIndentNo.Location = new System.Drawing.Point(3, 150);
            this.lblSIndentNo.Name = "lblSIndentNo";
            this.lblSIndentNo.Size = new System.Drawing.Size(54, 13);
            this.lblSIndentNo.TabIndex = 257;
            this.lblSIndentNo.Text = "Indent No";
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(62, 125);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.Size = new System.Drawing.Size(135, 20);
            this.dtpSTo.TabIndex = 17;
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(62, 102);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.Size = new System.Drawing.Size(135, 20);
            this.dtpSFrom.TabIndex = 16;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(3, 128);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 130;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(3, 105);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 129;
            this.lblFrom.Text = "From";
            // 
            // cboSExecutive
            // 
            this.cboSExecutive.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSExecutive.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSExecutive.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSExecutive.DisplayMember = "Text";
            this.cboSExecutive.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSExecutive.DropDownHeight = 75;
            this.cboSExecutive.FormattingEnabled = true;
            this.cboSExecutive.IntegralHeight = false;
            this.cboSExecutive.ItemHeight = 14;
            this.cboSExecutive.Location = new System.Drawing.Point(62, 33);
            this.cboSExecutive.Name = "cboSExecutive";
            this.cboSExecutive.Size = new System.Drawing.Size(135, 20);
            this.cboSExecutive.TabIndex = 13;
            this.cboSExecutive.SelectedIndexChanged += new System.EventHandler(this.cboSExecutive_SelectedIndexChanged);
            // 
            // lblSExecutive
            // 
            this.lblSExecutive.AutoSize = true;
            this.lblSExecutive.Location = new System.Drawing.Point(3, 36);
            this.lblSExecutive.Name = "lblSExecutive";
            this.lblSExecutive.Size = new System.Drawing.Size(54, 13);
            this.lblSExecutive.TabIndex = 128;
            this.lblSExecutive.Text = "Executive";
            // 
            // cboSStatus
            // 
            this.cboSStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSStatus.DisplayMember = "Text";
            this.cboSStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSStatus.DropDownHeight = 75;
            this.cboSStatus.FormattingEnabled = true;
            this.cboSStatus.IntegralHeight = false;
            this.cboSStatus.ItemHeight = 14;
            this.cboSStatus.Location = new System.Drawing.Point(62, 79);
            this.cboSStatus.Name = "cboSStatus";
            this.cboSStatus.Size = new System.Drawing.Size(135, 20);
            this.cboSStatus.TabIndex = 15;
            // 
            // lblSStatus
            // 
            this.lblSStatus.AutoSize = true;
            this.lblSStatus.Location = new System.Drawing.Point(3, 82);
            this.lblSStatus.Name = "lblSStatus";
            this.lblSStatus.Size = new System.Drawing.Size(37, 13);
            this.lblSStatus.TabIndex = 125;
            this.lblSStatus.Text = "Status";
            // 
            // cboSDepartment
            // 
            this.cboSDepartment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSDepartment.DisplayMember = "Text";
            this.cboSDepartment.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSDepartment.DropDownHeight = 75;
            this.cboSDepartment.FormattingEnabled = true;
            this.cboSDepartment.IntegralHeight = false;
            this.cboSDepartment.ItemHeight = 14;
            this.cboSDepartment.Location = new System.Drawing.Point(62, 56);
            this.cboSDepartment.Name = "cboSDepartment";
            this.cboSDepartment.Size = new System.Drawing.Size(135, 20);
            this.cboSDepartment.TabIndex = 14;
            // 
            // lblSDepartment
            // 
            this.lblSDepartment.AutoSize = true;
            this.lblSDepartment.Location = new System.Drawing.Point(3, 59);
            this.lblSDepartment.Name = "lblSDepartment";
            this.lblSDepartment.Size = new System.Drawing.Size(62, 13);
            this.lblSDepartment.TabIndex = 123;
            this.lblSDepartment.Text = "Department";
            // 
            // btnSRefresh
            // 
            this.btnSRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSRefresh.Location = new System.Drawing.Point(126, 171);
            this.btnSRefresh.Name = "btnSRefresh";
            this.btnSRefresh.Size = new System.Drawing.Size(71, 23);
            this.btnSRefresh.TabIndex = 19;
            this.btnSRefresh.Text = "Refresh";
            this.btnSRefresh.Click += new System.EventHandler(this.btnSRefresh_Click);
            // 
            // txtSIndentNo
            // 
            this.txtSIndentNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtSIndentNo.Border.Class = "TextBoxBorder";
            this.txtSIndentNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSIndentNo.Location = new System.Drawing.Point(62, 148);
            this.txtSIndentNo.MaxLength = 20;
            this.txtSIndentNo.Name = "txtSIndentNo";
            this.txtSIndentNo.Size = new System.Drawing.Size(135, 20);
            this.txtSIndentNo.TabIndex = 18;
            this.txtSIndentNo.WatermarkImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtSIndentNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSIndentNo_KeyPress);
            // 
            // cboSCompany
            // 
            this.cboSCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSCompany.DisplayMember = "Text";
            this.cboSCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSCompany.DropDownHeight = 75;
            this.cboSCompany.FormattingEnabled = true;
            this.cboSCompany.IntegralHeight = false;
            this.cboSCompany.ItemHeight = 14;
            this.cboSCompany.Location = new System.Drawing.Point(62, 10);
            this.cboSCompany.Name = "cboSCompany";
            this.cboSCompany.Size = new System.Drawing.Size(135, 20);
            this.cboSCompany.TabIndex = 12;
            this.cboSCompany.SelectedIndexChanged += new System.EventHandler(this.cboSCompany_SelectedIndexChanged);
            // 
            // LblScompany
            // 
            this.LblScompany.AutoSize = true;
            this.LblScompany.Location = new System.Drawing.Point(3, 13);
            this.LblScompany.Name = "LblScompany";
            this.LblScompany.Size = new System.Drawing.Size(51, 13);
            this.LblScompany.TabIndex = 109;
            this.LblScompany.Text = "Company";
            // 
            // lblIndentCount
            // 
            // 
            // 
            // 
            this.lblIndentCount.BackgroundStyle.Class = "";
            this.lblIndentCount.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblIndentCount.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblIndentCount.Location = new System.Drawing.Point(0, 488);
            this.lblIndentCount.Name = "lblIndentCount";
            this.lblIndentCount.Size = new System.Drawing.Size(200, 26);
            this.lblIndentCount.TabIndex = 110;
            this.lblIndentCount.Text = "...";
            // 
            // lblIndentNo
            // 
            this.lblIndentNo.AutoSize = true;
            this.lblIndentNo.BackColor = System.Drawing.Color.Transparent;
            this.lblIndentNo.Location = new System.Drawing.Point(338, 9);
            this.lblIndentNo.Name = "lblIndentNo";
            this.lblIndentNo.Size = new System.Drawing.Size(77, 13);
            this.lblIndentNo.TabIndex = 185;
            this.lblIndentNo.Text = "Indent Number";
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.Expanded = false;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(0, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 514);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 10;
            this.expandableSplitterLeft.TabStop = false;
            this.expandableSplitterLeft.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitterLeft_ExpandedChanged);
            // 
            // dotNetBarManager1
            // 
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.dotNetBarManager1.BottomDockSite = this.dockSite4;
            this.dotNetBarManager1.EnableFullSizeDock = false;
            this.dotNetBarManager1.LeftDockSite = this.dockSite1;
            this.dotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.dotNetBarManager1.ParentForm = this;
            this.dotNetBarManager1.RightDockSite = this.dockSite2;
            this.dotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.dotNetBarManager1.ToolbarBottomDockSite = this.dockSite8;
            this.dotNetBarManager1.ToolbarLeftDockSite = this.dockSite5;
            this.dotNetBarManager1.ToolbarRightDockSite = this.dockSite6;
            this.dotNetBarManager1.TopDockSite = this.dockSite3;
            // 
            // dockSite4
            // 
            this.dockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite4.Location = new System.Drawing.Point(0, 514);
            this.dockSite4.Name = "dockSite4";
            this.dockSite4.Size = new System.Drawing.Size(1303, 0);
            this.dockSite4.TabIndex = 18;
            this.dockSite4.TabStop = false;
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(3, 0);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 514);
            this.dockSite1.TabIndex = 15;
            this.dockSite1.TabStop = false;
            // 
            // dockSite2
            // 
            this.dockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite2.Location = new System.Drawing.Point(1303, 0);
            this.dockSite2.Name = "dockSite2";
            this.dockSite2.Size = new System.Drawing.Size(0, 514);
            this.dockSite2.TabIndex = 16;
            this.dockSite2.TabStop = false;
            // 
            // dockSite8
            // 
            this.dockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite8.Location = new System.Drawing.Point(0, 514);
            this.dockSite8.Name = "dockSite8";
            this.dockSite8.Size = new System.Drawing.Size(1303, 0);
            this.dockSite8.TabIndex = 22;
            this.dockSite8.TabStop = false;
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(0, 0);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 514);
            this.dockSite5.TabIndex = 19;
            this.dockSite5.TabStop = false;
            // 
            // dockSite6
            // 
            this.dockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite6.Location = new System.Drawing.Point(1303, 0);
            this.dockSite6.Name = "dockSite6";
            this.dockSite6.Size = new System.Drawing.Size(0, 514);
            this.dockSite6.TabIndex = 20;
            this.dockSite6.TabStop = false;
            // 
            // dockSite3
            // 
            this.dockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite3.Location = new System.Drawing.Point(0, 0);
            this.dockSite3.Name = "dockSite3";
            this.dockSite3.Size = new System.Drawing.Size(1303, 0);
            this.dockSite3.TabIndex = 17;
            this.dockSite3.TabStop = false;
            // 
            // bar1
            // 
            this.bar1.AccessibleDescription = "DotNetBar Bar (bar1)";
            this.bar1.AccessibleName = "DotNetBar Bar";
            this.bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.bar1.Location = new System.Drawing.Point(0, 0);
            this.bar1.MenuBar = true;
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(36, 24);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.bar1.TabIndex = 0;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx1.Controls.Add(this.panelBottom);
            this.panelEx1.Controls.Add(this.expandableSplitterTop);
            this.panelEx1.Controls.Add(this.panelTop);
            this.panelEx1.Controls.Add(this.PurchaseOrderBindingNavigator);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(3, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(1300, 514);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 23;
            this.panelEx1.Text = "panelEx1";
            // 
            // panelBottom
            // 
            this.panelBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelBottom.Controls.Add(this.tcPurchaseIndent);
            this.panelBottom.Controls.Add(this.panelGridBottom);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBottom.Location = new System.Drawing.Point(0, 130);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1300, 384);
            this.panelBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelBottom.Style.GradientAngle = 90;
            this.panelBottom.TabIndex = 18;
            this.panelBottom.Text = "panelEx3";
            // 
            // tcPurchaseIndent
            // 
            this.tcPurchaseIndent.BackColor = System.Drawing.Color.Transparent;
            this.tcPurchaseIndent.CanReorderTabs = true;
            this.tcPurchaseIndent.Controls.Add(this.tabControlPanel1);
            this.tcPurchaseIndent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcPurchaseIndent.Location = new System.Drawing.Point(0, 0);
            this.tcPurchaseIndent.Name = "tcPurchaseIndent";
            this.tcPurchaseIndent.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcPurchaseIndent.SelectedTabIndex = 0;
            this.tcPurchaseIndent.Size = new System.Drawing.Size(1300, 265);
            this.tcPurchaseIndent.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcPurchaseIndent.TabIndex = 8;
            this.tcPurchaseIndent.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcPurchaseIndent.Tabs.Add(this.tpItemDetails);
            this.tcPurchaseIndent.TabStop = false;
            this.tcPurchaseIndent.Text = "tabControl1";
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.dgvPurchaseIndent);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1300, 243);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 9;
            this.tabControlPanel1.TabItem = this.tpItemDetails;
            // 
            // dgvPurchaseIndent
            // 
            this.dgvPurchaseIndent.AddNewRow = false;
            this.dgvPurchaseIndent.AlphaNumericCols = new int[0];
            this.dgvPurchaseIndent.BackgroundColor = System.Drawing.Color.White;
            this.dgvPurchaseIndent.CapsLockCols = new int[0];
            this.dgvPurchaseIndent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPurchaseIndent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.Quantity,
            this.Uom,
            this.ItemID});
            this.dgvPurchaseIndent.DecimalCols = new int[0];
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPurchaseIndent.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvPurchaseIndent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPurchaseIndent.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvPurchaseIndent.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvPurchaseIndent.HasSlNo = false;
            this.dgvPurchaseIndent.LastRowIndex = 0;
            this.dgvPurchaseIndent.Location = new System.Drawing.Point(1, 1);
            this.dgvPurchaseIndent.Name = "dgvPurchaseIndent";
            this.dgvPurchaseIndent.NegativeValueCols = new int[0];
            this.dgvPurchaseIndent.NumericCols = new int[0];
            this.dgvPurchaseIndent.Size = new System.Drawing.Size(1298, 241);
            this.dgvPurchaseIndent.TabIndex = 10;
            this.dgvPurchaseIndent.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPurchaseIndent_CellValueChanged);
            this.dgvPurchaseIndent.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvPurchaseIndent_CellBeginEdit);
            this.dgvPurchaseIndent.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPurchaseIndent_CellEndEdit);
            this.dgvPurchaseIndent.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvPurchaseIndent_Textbox_TextChanged);
            this.dgvPurchaseIndent.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvPurchaseIndent_EditingControlShowing);
            this.dgvPurchaseIndent.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvPurchaseIndent_CurrentCellDirtyStateChanged);
            this.dgvPurchaseIndent.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPurchaseIndent_CellEnter);
            // 
            // ItemCode
            // 
            this.ItemCode.HeaderText = "Item Code";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemCode.Width = 150;
            // 
            // ItemName
            // 
            this.ItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Quantity
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle4;
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.MaxInputLength = 10;
            this.Quantity.Name = "Quantity";
            this.Quantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Quantity.Width = 125;
            // 
            // Uom
            // 
            this.Uom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Uom.HeaderText = "UOM";
            this.Uom.Name = "Uom";
            this.Uom.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ItemID
            // 
            this.ItemID.HeaderText = "ItemID";
            this.ItemID.Name = "ItemID";
            this.ItemID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemID.Visible = false;
            // 
            // tpItemDetails
            // 
            this.tpItemDetails.AttachedControl = this.tabControlPanel1;
            this.tpItemDetails.Name = "tpItemDetails";
            this.tpItemDetails.Text = "Item Details";
            // 
            // panelGridBottom
            // 
            this.panelGridBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelGridBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelGridBottom.Controls.Add(this.pnlBottom);
            this.panelGridBottom.Controls.Add(this.txtRemarks);
            this.panelGridBottom.Controls.Add(this.lblRemarks);
            this.panelGridBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGridBottom.Location = new System.Drawing.Point(0, 265);
            this.panelGridBottom.Name = "panelGridBottom";
            this.panelGridBottom.Size = new System.Drawing.Size(1300, 119);
            this.panelGridBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelGridBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelGridBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelGridBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelGridBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelGridBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelGridBottom.Style.GradientAngle = 90;
            this.panelGridBottom.TabIndex = 106;
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.lblCreatedByText);
            this.pnlBottom.Controls.Add(this.lblCreatedDateValue);
            this.pnlBottom.Controls.Add(this.lblPurchaseIndentStatus);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 93);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1300, 26);
            this.pnlBottom.TabIndex = 246;
            // 
            // lblCreatedByText
            // 
            this.lblCreatedByText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedByText.CloseButtonVisible = false;
            this.lblCreatedByText.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblCreatedByText.Location = new System.Drawing.Point(918, 0);
            this.lblCreatedByText.Name = "lblCreatedByText";
            this.lblCreatedByText.OptionsButtonVisible = false;
            this.lblCreatedByText.Size = new System.Drawing.Size(380, 24);
            this.lblCreatedByText.TabIndex = 258;
            this.lblCreatedByText.Text = "Created By";
            // 
            // lblCreatedDateValue
            // 
            this.lblCreatedDateValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedDateValue.CloseButtonVisible = false;
            this.lblCreatedDateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCreatedDateValue.Location = new System.Drawing.Point(393, 0);
            this.lblCreatedDateValue.Name = "lblCreatedDateValue";
            this.lblCreatedDateValue.OptionsButtonVisible = false;
            this.lblCreatedDateValue.Size = new System.Drawing.Size(905, 24);
            this.lblCreatedDateValue.TabIndex = 256;
            this.lblCreatedDateValue.Text = "Created Date";
            // 
            // lblPurchaseIndentStatus
            // 
            this.lblPurchaseIndentStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblPurchaseIndentStatus.CloseButtonVisible = false;
            this.lblPurchaseIndentStatus.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPurchaseIndentStatus.Image = ((System.Drawing.Image)(resources.GetObject("lblPurchaseIndentStatus.Image")));
            this.lblPurchaseIndentStatus.Location = new System.Drawing.Point(0, 0);
            this.lblPurchaseIndentStatus.Name = "lblPurchaseIndentStatus";
            this.lblPurchaseIndentStatus.OptionsButtonVisible = false;
            this.lblPurchaseIndentStatus.Size = new System.Drawing.Size(393, 24);
            this.lblPurchaseIndentStatus.TabIndex = 20;
            // 
            // txtRemarks
            // 
            // 
            // 
            // 
            this.txtRemarks.Border.Class = "TextBoxBorder";
            this.txtRemarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtRemarks.Location = new System.Drawing.Point(78, 17);
            this.txtRemarks.MaxLength = 2000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(343, 67);
            this.txtRemarks.TabIndex = 11;
            this.txtRemarks.TextChanged += new System.EventHandler(this.Text_Changed);
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(12, 19);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 236;
            this.lblRemarks.Text = "Remarks";
            // 
            // expandableSplitterTop
            // 
            this.expandableSplitterTop.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterTop.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitterTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitterTop.ExpandableControl = this.panelTop;
            this.expandableSplitterTop.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterTop.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterTop.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterTop.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterTop.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.Location = new System.Drawing.Point(0, 127);
            this.expandableSplitterTop.Name = "expandableSplitterTop";
            this.expandableSplitterTop.Size = new System.Drawing.Size(1300, 3);
            this.expandableSplitterTop.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterTop.TabIndex = 1;
            this.expandableSplitterTop.TabStop = false;
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelTop.Controls.Add(this.tcGeneral);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1300, 102);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 14;
            // 
            // tcGeneral
            // 
            this.tcGeneral.BackColor = System.Drawing.Color.Transparent;
            this.tcGeneral.CanReorderTabs = true;
            this.tcGeneral.Controls.Add(this.tabControlPanel3);
            this.tcGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcGeneral.Location = new System.Drawing.Point(0, 0);
            this.tcGeneral.Name = "tcGeneral";
            this.tcGeneral.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcGeneral.SelectedTabIndex = 0;
            this.tcGeneral.Size = new System.Drawing.Size(1300, 102);
            this.tcGeneral.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcGeneral.TabIndex = 0;
            this.tcGeneral.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcGeneral.Tabs.Add(this.tiGeneral);
            this.tcGeneral.TabStop = false;
            this.tcGeneral.Text = "tabControl1";
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel3.Controls.Add(this.lblCompany);
            this.tabControlPanel3.Controls.Add(this.lblStatusText);
            this.tabControlPanel3.Controls.Add(this.lblIndentNo);
            this.tabControlPanel3.Controls.Add(this.lblDescription);
            this.tabControlPanel3.Controls.Add(this.lblIndentDate);
            this.tabControlPanel3.Controls.Add(this.txtDescription);
            this.tabControlPanel3.Controls.Add(this.lblDueDate);
            this.tabControlPanel3.Controls.Add(this.lblStatus);
            this.tabControlPanel3.Controls.Add(this.dtpIndentDate);
            this.tabControlPanel3.Controls.Add(this.btnDepartment);
            this.tabControlPanel3.Controls.Add(this.dtpDueDate);
            this.tabControlPanel3.Controls.Add(this.txtIndentNo);
            this.tabControlPanel3.Controls.Add(this.lblDepartment);
            this.tabControlPanel3.Controls.Add(this.cboCompany);
            this.tabControlPanel3.Controls.Add(this.cboDepartment);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(1300, 80);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 1;
            this.tabControlPanel3.TabItem = this.tiGeneral;
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.BackColor = System.Drawing.Color.Transparent;
            this.lblCompany.Location = new System.Drawing.Point(21, 7);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 200;
            this.lblCompany.Text = "Company";
            // 
            // lblStatusText
            // 
            this.lblStatusText.AutoSize = true;
            this.lblStatusText.BackColor = System.Drawing.Color.Transparent;
            this.lblStatusText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatusText.Location = new System.Drawing.Point(636, 5);
            this.lblStatusText.Name = "lblStatusText";
            this.lblStatusText.Size = new System.Drawing.Size(56, 20);
            this.lblStatusText.TabIndex = 256;
            this.lblStatusText.Text = "Status";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblDescription.Location = new System.Drawing.Point(570, 33);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 13);
            this.lblDescription.TabIndex = 240;
            this.lblDescription.Text = "Description";
            // 
            // lblIndentDate
            // 
            this.lblIndentDate.AutoSize = true;
            this.lblIndentDate.BackColor = System.Drawing.Color.Transparent;
            this.lblIndentDate.Location = new System.Drawing.Point(338, 33);
            this.lblIndentDate.Name = "lblIndentDate";
            this.lblIndentDate.Size = new System.Drawing.Size(30, 13);
            this.lblIndentDate.TabIndex = 188;
            this.lblIndentDate.Text = "Date";
            // 
            // txtDescription
            // 
            // 
            // 
            // 
            this.txtDescription.Border.Class = "TextBoxBorder";
            this.txtDescription.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDescription.Location = new System.Drawing.Point(640, 30);
            this.txtDescription.MaxLength = 200;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(317, 44);
            this.txtDescription.TabIndex = 7;
            this.txtDescription.TabStop = false;
            // 
            // lblDueDate
            // 
            this.lblDueDate.AutoSize = true;
            this.lblDueDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDueDate.Location = new System.Drawing.Point(338, 56);
            this.lblDueDate.Name = "lblDueDate";
            this.lblDueDate.Size = new System.Drawing.Size(53, 13);
            this.lblDueDate.TabIndex = 189;
            this.lblDueDate.Text = "Due Date";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Location = new System.Drawing.Point(570, 10);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 208;
            this.lblStatus.Text = "Status";
            // 
            // dtpIndentDate
            // 
            this.dtpIndentDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpIndentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpIndentDate.Location = new System.Drawing.Point(442, 31);
            this.dtpIndentDate.Name = "dtpIndentDate";
            this.dtpIndentDate.Size = new System.Drawing.Size(101, 20);
            this.dtpIndentDate.TabIndex = 5;
            this.dtpIndentDate.ValueChanged += new System.EventHandler(this.dtpIndentDate_ValueChanged);
            // 
            // btnDepartment
            // 
            this.btnDepartment.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDepartment.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDepartment.Location = new System.Drawing.Point(294, 31);
            this.btnDepartment.Name = "btnDepartment";
            this.btnDepartment.Size = new System.Drawing.Size(25, 20);
            this.btnDepartment.TabIndex = 3;
            this.btnDepartment.Text = "...";
            this.btnDepartment.Click += new System.EventHandler(this.btnDepartment_Click);
            // 
            // dtpDueDate
            // 
            this.dtpDueDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDueDate.Location = new System.Drawing.Point(442, 54);
            this.dtpDueDate.Name = "dtpDueDate";
            this.dtpDueDate.Size = new System.Drawing.Size(101, 20);
            this.dtpDueDate.TabIndex = 6;
            this.dtpDueDate.ValueChanged += new System.EventHandler(this.Text_Changed);
            // 
            // txtIndentNo
            // 
            this.txtIndentNo.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtIndentNo.Border.Class = "TextBoxBorder";
            this.txtIndentNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtIndentNo.Location = new System.Drawing.Point(442, 8);
            this.txtIndentNo.MaxLength = 20;
            this.txtIndentNo.Name = "txtIndentNo";
            this.txtIndentNo.Size = new System.Drawing.Size(101, 20);
            this.txtIndentNo.TabIndex = 4;
            this.txtIndentNo.TabStop = false;
            this.txtIndentNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIndentNo_KeyPress);
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            this.lblDepartment.BackColor = System.Drawing.Color.Transparent;
            this.lblDepartment.Location = new System.Drawing.Point(21, 33);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(62, 13);
            this.lblDepartment.TabIndex = 202;
            this.lblDepartment.Text = "Department";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 75;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(118, 8);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(171, 20);
            this.cboCompany.TabIndex = 1;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // cboDepartment
            // 
            this.cboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDepartment.DisplayMember = "Text";
            this.cboDepartment.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDepartment.DropDownHeight = 75;
            this.cboDepartment.FormattingEnabled = true;
            this.cboDepartment.IntegralHeight = false;
            this.cboDepartment.ItemHeight = 14;
            this.cboDepartment.Location = new System.Drawing.Point(118, 31);
            this.cboDepartment.Name = "cboDepartment";
            this.cboDepartment.Size = new System.Drawing.Size(171, 20);
            this.cboDepartment.TabIndex = 2;
            this.cboDepartment.SelectedIndexChanged += new System.EventHandler(this.cboDepartment_SelectedIndexChanged);
            this.cboDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // tiGeneral
            // 
            this.tiGeneral.AttachedControl = this.tabControlPanel3;
            this.tiGeneral.Name = "tiGeneral";
            this.tiGeneral.Text = "General";
            // 
            // PurchaseOrderBindingNavigator
            // 
            this.PurchaseOrderBindingNavigator.AccessibleDescription = "DotNetBar Bar (PurchaseOrderBindingNavigator)";
            this.PurchaseOrderBindingNavigator.AccessibleName = "DotNetBar Bar";
            this.PurchaseOrderBindingNavigator.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.PurchaseOrderBindingNavigator.Dock = System.Windows.Forms.DockStyle.Top;
            this.PurchaseOrderBindingNavigator.DockLine = 1;
            this.PurchaseOrderBindingNavigator.DockOffset = 73;
            this.PurchaseOrderBindingNavigator.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.PurchaseOrderBindingNavigator.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.PurchaseOrderBindingNavigator.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorClearItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorCancelItem,
            this.BtnItem,
            this.BtnUom,
            this.BtnPrint,
            this.BtnEmail,
            this.BtnHelp});
            this.PurchaseOrderBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.PurchaseOrderBindingNavigator.Name = "PurchaseOrderBindingNavigator";
            this.PurchaseOrderBindingNavigator.Size = new System.Drawing.Size(1300, 25);
            this.PurchaseOrderBindingNavigator.Stretch = true;
            this.PurchaseOrderBindingNavigator.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PurchaseOrderBindingNavigator.TabIndex = 12;
            this.PurchaseOrderBindingNavigator.TabStop = false;
            this.PurchaseOrderBindingNavigator.Text = "bar2";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorAddNewItem.Image = global::MyBooksERP.Properties.Resources.Add;
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlEnter);
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.Tooltip = "Add New Information";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorSaveItem.Image = global::MyBooksERP.Properties.Resources.save;
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Tooltip = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.PurchaseOrderBindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorClearItem
            // 
            this.BindingNavigatorClearItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorClearItem.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.BindingNavigatorClearItem.Name = "BindingNavigatorClearItem";
            this.BindingNavigatorClearItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.BindingNavigatorClearItem.Text = "Clear";
            this.BindingNavigatorClearItem.Tooltip = "Clear";
            this.BindingNavigatorClearItem.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorDeleteItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Tooltip = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorCancelItem
            // 
            this.BindingNavigatorCancelItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorCancelItem.Image = global::MyBooksERP.Properties.Resources.Cancel;
            this.BindingNavigatorCancelItem.Name = "BindingNavigatorCancelItem";
            this.BindingNavigatorCancelItem.Text = "Cancel";
            this.BindingNavigatorCancelItem.Tooltip = "Cancel";
            this.BindingNavigatorCancelItem.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // BtnItem
            // 
            this.BtnItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnItem.Image = global::MyBooksERP.Properties.Resources.Product2;
            this.BtnItem.Name = "BtnItem";
            this.BtnItem.Text = "Product";
            this.BtnItem.Tooltip = "Product";
            this.BtnItem.Click += new System.EventHandler(this.BtnItem_Click);
            // 
            // BtnUom
            // 
            this.BtnUom.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnUom.Icon = ((System.Drawing.Icon)(resources.GetObject("BtnUom.Icon")));
            this.BtnUom.Name = "BtnUom";
            this.BtnUom.Text = "Uom";
            this.BtnUom.Tooltip = "Unit Of Measurement";
            this.BtnUom.Click += new System.EventHandler(this.BtnUom_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // BtnHelp
            // 
            this.BtnHelp.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnHelp.Image = global::MyBooksERP.Properties.Resources.help;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // tmrPurchaseIndent
            // 
            this.tmrPurchaseIndent.Enabled = true;
            this.tmrPurchaseIndent.Interval = 2000;
            this.tmrPurchaseIndent.Tick += new System.EventHandler(this.tmrPurchaseIndent_Tick);
            // 
            // errPurchaseIndent
            // 
            this.errPurchaseIndent.ContainerControl = this;
            this.errPurchaseIndent.RightToLeft = true;
            // 
            // imgPurchaseIndent
            // 
            this.imgPurchaseIndent.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgPurchaseIndent.ImageStream")));
            this.imgPurchaseIndent.TransparentColor = System.Drawing.Color.Transparent;
            this.imgPurchaseIndent.Images.SetKeyName(0, "Purchase Indent.ICO");
            this.imgPurchaseIndent.Images.SetKeyName(1, "Purchase Order.ico");
            this.imgPurchaseIndent.Images.SetKeyName(2, "Purchase Invoice.ico");
            this.imgPurchaseIndent.Images.SetKeyName(3, "GRN.ICO");
            this.imgPurchaseIndent.Images.SetKeyName(4, "Purchase Order Return.ico");
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Item Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn3.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn4.HeaderText = "QtyReceived";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Batch Number";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn6.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn6.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn7.HeaderText = "Total";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Status";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "OrderDetailID";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "PurchaseOrderID";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Visible = false;
            // 
            // tiSuggestions
            // 
            this.tiSuggestions.Name = "tiSuggestions";
            this.tiSuggestions.Text = "Suggestions";
            this.tiSuggestions.Visible = false;
            // 
            // FrmPurchaseIndent
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.ClientSize = new System.Drawing.Size(1303, 514);
            this.Controls.Add(this.panelEx1);
            this.Controls.Add(this.dockSite2);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.expandableSplitterLeft);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.dockSite3);
            this.Controls.Add(this.dockSite4);
            this.Controls.Add(this.dockSite5);
            this.Controls.Add(this.dockSite6);
            this.Controls.Add(this.dockSite8);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmPurchaseIndent";
            this.Text = "Purchase Indent";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmPurchase_Load);
            this.Shown += new System.EventHandler(this.FrmPurchaseIndent_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPurchaseIndent_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmPurchaseIndent_KeyDown);
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchaseIndentDisplay)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.panelEx1.ResumeLayout(false);
            this.panelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcPurchaseIndent)).EndInit();
            this.tcPurchaseIndent.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchaseIndent)).EndInit();
            this.panelGridBottom.ResumeLayout(false);
            this.panelGridBottom.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).EndInit();
            this.tcGeneral.ResumeLayout(false);
            this.tabControlPanel3.ResumeLayout(false);
            this.tabControlPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errPurchaseIndent)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        #endregion

        #region Declarations

        public bool blnDocumentChanged = false;
        public string strFileName = "";
        private bool MblnChangeStatus;                    //Check state of the page
        private bool MblnAddStatus;                       //Status for Addition/Updation mode 
        private bool MblnPrintEmailPermission = true;          // Set View Permission
        private bool MblnAddPermission = true;           //Set Add Permission
        private bool MblnUpdatePermission = true;        //Set Update Permission
        private bool MblnDeletePermission = true;        //Set Delete Permission
        private bool MblnAddUpdatePermission = true;     //Set Add Update Permission
        private bool MblnCancelPermission = true;       // Set Cancel Permission
        private bool MblnSearchFlag = false;               //  Flag for identifying SearchMode /Addition Mode
        private bool MBlnIsFromCancellation = false;      // Set To Check Save Is From Button Cancellation
        private string MstrMessageCommon;                 //  variable for assigning message
        private int MintCompanyId;
        private MessageBoxIcon MmessageIcon;
        private ArrayList MaMessageArr;                 // Error Message display
        private ArrayList MaStatusMessage;              //Status Message Display
        ClsLogWriter mObjLogs;                          //  Object for Class Clslogs
        ClsNotification mObjNotification;               //  Object for Class ClsNotification

        #endregion

        #region Constructor

        public FrmPurchaseIndent()
        {
            //Constructor
            InitializeComponent();
            MobjClsBLLPurchaseIndent = new clsBLLPurchaseIndent();
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
        }

        #endregion

        #region Functions

        private bool Save()
        {
            try
            {
                //Save Information
                if (PurchaseIndentvalidation())
                {
                    if (SavePurchaseIndent())
                    {
                        MblnAddStatus = false;
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        tmrPurchaseIndent.Enabled = true;
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on Save() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Save()" + ex.Message.ToString());
                return false;
            }
        }

        private void LoadMessage()
        {
            try
            {
                //Loading Message Function
                MaMessageArr = new ArrayList();
                MaStatusMessage = new ArrayList();
                MaMessageArr = mObjNotification.FillMessageArray((int)FormID.Purchase, 4);
                MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.Purchase, 4);
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on LoadMessage() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadMessage()" + ex.Message.ToString());
            }
        }

        private void SetPermissions()
        {
            try
            {// Function for setting permissions
                clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
                if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.PurchaseIndent, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                    if (MblnAddPermission == true || MblnUpdatePermission == true)
                        MblnAddUpdatePermission = true;
                }
                else
                    MblnAddPermission = MblnAddUpdatePermission = MblnCancelPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

                //BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                //BindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
                //BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    DataTable DtPermission = objClsBLLPermissionSettings.GetMenuPermissions(ClsCommonSettings.RoleID);
                    if (DtPermission.Rows.Count == 0)
                    {
                        BtnItem.Enabled = BtnUom.Enabled = false;
                    }
                    else
                    {
                        DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Products).ToString();
                        BtnItem.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);
                        DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.UOM).ToString();
                        BtnUom.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);
                    }
                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on SetPermissions() " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on SetPermissions()" + Ex.Message.ToString());
            }
        }

        private void AddNewPurchaseIndent() //Itype=1 from seraching ,itype=0 from clear
        {
            try
            {
                // Add New PurchaseIndent
                MobjClsBLLPurchaseIndent = new clsBLLPurchaseIndent();
                MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intStatusID = 0;

                ClearControls();
                GenerateIndentNo();

                DisplayAllNoInSearchGrid();

                MblnAddStatus = true;
                MstrMessageCommon = "Add new Purchase Indent";
                lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrFocus.Enabled = true;
                errPurchaseIndent.Clear();
                MblnChangeStatus = false;

                BindingNavigatorCancelItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                MBlnIsFromCancellation = false;
                BtnEmail.Enabled = false;
                BtnPrint.Enabled = false;
                lblCreatedDateValue.Text = "Created Date :"+ ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                lblCreatedByText.Text = "Created By :" + ClsCommonSettings.strEmployeeName;
                cboCompany.Focus();
                BindingNavigatorAddNewItem.Enabled = false;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on AddNewPurchaseIndent() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewPurchaseIndent()" + ex.Message.ToString());
            }
        }

        private string GetPurchaseIndentPrefix()
        {
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue));

            return ClsCommonSettings.PIPrefix;
        }

        private void ClearControls()
        {
            // Clear Controls Function
            dgvPurchaseIndent.Enabled = true;
            panelTop.Enabled = true;
            MblnSearchFlag = false;
            errPurchaseIndent.Clear();
            txtIndentNo.Enabled = true;
            cboCompany.Enabled = true;

            cboSCompany.SelectedIndex = -1;
            cboSDepartment.SelectedIndex = -1;
            cboSExecutive.SelectedIndex = -1;
            cboSStatus.SelectedIndex = -1;
            txtSIndentNo.Text = "";

            cboCompany.SelectedValue = ClsCommonSettings.CompanyID;
            dtpIndentDate.Value = ClsCommonSettings.GetServerDate();
            dtpDueDate.Value = ClsCommonSettings.GetServerDate();

            dgvPurchaseIndent.Rows.Clear();
            dgvPurchaseIndent.ClearSelection();

            txtRemarks.Text = "";
            SetStatusLabelText();
            txtDescription.Clear();
        }

        private void GenerateIndentNo()
        {
            try
            {
                // Generate Purchase Indent No
                txtIndentNo.Text = GetPurchaseIndentPrefix() + (MobjClsBLLPurchaseIndent.GetLastIndentNo(1, cboCompany.SelectedValue.ToInt32()) + 1).ToString();
                if (ClsCommonSettings.PIAutogenerate)
                    txtIndentNo.ReadOnly = false;
                else
                    txtIndentNo.ReadOnly = true;
                lblIndentCount.Text = "Total Indents " + MobjClsBLLPurchaseIndent.GetLastIndentNo(2, cboCompany.SelectedValue.ToInt32());
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on GenerateIndentNo() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on GenerateIndentNo()" + ex.Message.ToString());
            }
        }

        private bool PurchaseIndentvalidation()
        {
            try
            {
                // Purchase Indent Validation
                errPurchaseIndent.Clear();
                lblPurchaseIndentStatus.Text = "";

                if (string.IsNullOrEmpty(txtIndentNo.Text.Trim()))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 942, out MmessageIcon);
                    errPurchaseIndent.SetError(txtIndentNo, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrPurchaseIndent.Enabled = true;
                    txtIndentNo.Focus();
                    return false;
                }
                if (!txtIndentNo.ReadOnly && MobjClsBLLPurchaseIndent.IsPurchaseIndentNoExists(txtIndentNo.Text, cboCompany.SelectedValue.ToInt32()))
                {
                    //MstrMessageCommon = "This Indent No Already Exists";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 943, out MmessageIcon);
                    errPurchaseIndent.SetError(txtIndentNo, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrPurchaseIndent.Enabled = true;
                    txtIndentNo.Focus();
                    return false;
                }
                if (cboCompany.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 14, out MmessageIcon);
                    errPurchaseIndent.SetError(cboCompany, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrPurchaseIndent.Enabled = true;
                    cboCompany.Focus();
                    return false;
                }
                if (dtpIndentDate.Value.Date > ClsCommonSettings.GetServerDate())
                {
                    // due date limit
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9006, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Indent");

                    errPurchaseIndent.SetError(dtpIndentDate, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrPurchaseIndent.Enabled = true;
                    dtpIndentDate.Focus();
                    return false;
                }
                if (dtpDueDate.Value.Date < dtpIndentDate.Value.Date)
                {

                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9009, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Indent");

                    errPurchaseIndent.SetError(dtpDueDate, MstrMessageCommon.Replace("#", "").Trim());
                    //mObjNotification.CallErrorMessage(MaMessageArr, 17, MsMessageCaption);
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrPurchaseIndent.Enabled = true;
                    dtpDueDate.Focus();
                    return false;
                }
                if (ClsCommonSettings.PIDueDateLimit.ToInt32() != 0 && (dtpDueDate.Value > dtpIndentDate.Value.AddDays(ClsCommonSettings.PIDueDateLimit.ToDouble())))
                {
                    // due date limit
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9005, out MmessageIcon).Replace("*", ClsCommonSettings.PIDueDateLimit);
                    errPurchaseIndent.SetError(dtpDueDate, MstrMessageCommon.Replace("#", "").Trim());
                    //mObjNotification.CallErrorMessage(MaMessageArr, 17, MsMessageCaption);
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrPurchaseIndent.Enabled = true;
                    dtpDueDate.Focus();
                    return false;
                }
                if (cboDepartment.SelectedIndex == -1)
                {
                    //MstrMessageCommon = "Please select Department";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 17, out MmessageIcon);
                    errPurchaseIndent.SetError(cboDepartment, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrPurchaseIndent.Enabled = true;
                    cboDepartment.Focus();
                    return false;
                }

                if (dgvPurchaseIndent.Rows.Count == 1)
                {
                    //MstrMessageCommon = "Please enter Indent Details.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 901, out MmessageIcon);
                    errPurchaseIndent.SetError(dgvPurchaseIndent, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrPurchaseIndent.Enabled = true;
                    dgvPurchaseIndent.Focus();
                    return false;
                }
                //------------------------------------------------ Middle Panel---------------------------------------------------------------------------
                if (ValidatePurchaseIndentGrid() == false) return false;

                if (dgvPurchaseIndent.Rows.Count > 0)
                {
                    int iTempID = 0;
                    iTempID = CheckDuplicationInGrid();

                    if (iTempID != -1)
                    {
                        //MstrMessageCommon = "Please check duplication of values.";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 902, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        tmrPurchaseIndent.Enabled = true;
                        dgvPurchaseIndent.Focus();
                        dgvPurchaseIndent.CurrentCell = dgvPurchaseIndent["ItemCode", iTempID];
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on PurchaseIndentValidation() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on PurchaseIndentValidation()" + ex.Message.ToString());
                return false;
            }
        }

        private int CheckDuplicationInGrid()
        {
            // Function For Checking Duplication In Grid
            string SearchValuefirst = "";
            int RowIndexTemp = 0;

            foreach (DataGridViewRow rowValue in dgvPurchaseIndent.Rows)
            {
                if (rowValue.Cells[ItemID.Index].Value != null)
                {
                    SearchValuefirst = Convert.ToString(rowValue.Cells[ItemID.Index].Value);
                    RowIndexTemp = rowValue.Index;
                    foreach (DataGridViewRow row in dgvPurchaseIndent.Rows)
                    {
                        if (RowIndexTemp != row.Index)
                        {
                            if (row.Cells[ItemID.Index].Value != null)
                            {
                                if ((Convert.ToString(row.Cells[ItemID.Index].Value).Trim() == Convert.ToString(SearchValuefirst).Trim()))
                                    return row.Index;
                            }
                        }
                    }
                }
            }
            return -1;
        }

        private bool ValidatePurchaseIndentGrid()
        {
            try
            {
                // Function For Grid Validation
                for (int i = 0; i < dgvPurchaseIndent.RowCount - 1; i++)
                {
                    int iRowIndex = dgvPurchaseIndent.Rows[i].Index;
                    if (dgvPurchaseIndent.Rows[i].Cells["ItemID"].Value.ToInt32() == 0)
                    {
                        //MstrMessageCommon = "Please enter Item code.";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 903, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        tmrPurchaseIndent.Enabled = true;
                        dgvPurchaseIndent.Focus();
                        dgvPurchaseIndent.CurrentCell = dgvPurchaseIndent[0, iRowIndex];
                        return false;
                    }
                    if (Convert.ToString(dgvPurchaseIndent.Rows[i].Cells["ItemCode"].Value).Trim() == "")
                    {
                        //MstrMessageCommon = "Please enter Item code.";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 903, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        tmrPurchaseIndent.Enabled = true;
                        dgvPurchaseIndent.Focus();
                        dgvPurchaseIndent.CurrentCell = dgvPurchaseIndent[0, iRowIndex];
                        return false;
                    }
                    if (Convert.ToString(dgvPurchaseIndent.Rows[i].Cells["ItemName"].Value).Trim() == "")
                    {
                        //MsMessageCommon = "Please enter Item Name.";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 915, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        tmrPurchaseIndent.Enabled = true;
                        dgvPurchaseIndent.Focus();
                        dgvPurchaseIndent.CurrentCell = dgvPurchaseIndent[0, iRowIndex];
                        return false;
                    }
                    if (Convert.ToString(dgvPurchaseIndent.Rows[i].Cells["Quantity"].Value).Trim() == "" || Convert.ToString(dgvPurchaseIndent.Rows[i].Cells["Quantity"].Value).Trim() == "." || Convert.ToDecimal(dgvPurchaseIndent.Rows[i].Cells["Quantity"].Value) == 0)
                    {
                        //MsMessageCommon = "Please enter Quantity.";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 916, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        tmrPurchaseIndent.Enabled = true;
                        dgvPurchaseIndent.Focus();
                        dgvPurchaseIndent.CurrentCell = dgvPurchaseIndent[2, iRowIndex];
                        return false;
                    }
                    if (Convert.ToInt32(dgvPurchaseIndent.Rows[i].Cells["Uom"].Tag) == 0)
                    {
                        //MstrMessageCommon = "Please select Uom.";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 917, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        tmrPurchaseIndent.Enabled = true;
                        dgvPurchaseIndent.Focus();
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on CheckPurchaseIndentGridValidate() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on CheckPurchaseIndentGridValidate()" + ex.Message.ToString());
                return false;
            }
        }

        private bool SavePurchaseIndent()
        {
            try
            {
                //Save Purchase Indent
                if (!MBlnIsFromCancellation)
                {
                    if (MblnAddStatus == true)

                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 907, out MmessageIcon);
                    else
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);

                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return false;
                }
                if (MblnAddStatus && txtIndentNo.ReadOnly && MobjClsBLLPurchaseIndent.IsPurchaseIndentNoExists((txtIndentNo.Text), cboCompany.SelectedValue.ToInt32()))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 973, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Indent");
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    else
                        GenerateIndentNo();
                }
                FillParameters();
                FillDetailParameters();

                if (MobjClsBLLPurchaseIndent.SavePurchaseIndent(MblnAddStatus))
                {
                    txtIndentNo.Tag = MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intPurchaseIndentID;
                    mObjLogs.WriteLog("Saved successfully:  " + this.Name + "", 0);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on SavePurchaseIndent() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on SavePurchaseIndent()" + ex.Message.ToString());
                return false;
            }
        }

        private void FillParameters()
        {
            //Filling Parameters

            if (MblnAddStatus == true)
            {
                MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intPurchaseIndentID = 0;
            }
            else
            {
                MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intPurchaseIndentID = Convert.ToInt64(txtIndentNo.Tag);
            }

            MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intDepartmentID = Convert.ToInt32(cboDepartment.SelectedValue);
            MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strPurchaseIndentNo = txtIndentNo.Text.Replace("'", "�").Trim();
            MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strIndentDate = dtpIndentDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strDueDate = dtpDueDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strRemarks = txtRemarks.Text.Trim();
            MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intCreatedBy = ClsCommonSettings.UserID;
            MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strCreatedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
            if (MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intStatusID == 0)
                MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intStatusID = (int)OperationStatusType.PIndentOpened;
            if (MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intStatusID == (Int32)OperationStatusType.PIndentCancelled)
                MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.blnCancelled = true;
            else
                MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.blnCancelled = false;
            MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strDescription = txtDescription.Text.Trim();
        }

        private void FillDetailParameters()
        {
            //Filling Detail Parameters
            if (dgvPurchaseIndent.CurrentRow != null)
                dgvPurchaseIndent.CurrentCell = dgvPurchaseIndent["ItemCode", dgvPurchaseIndent.CurrentRow.Index];
            MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.lstDTOPurchaseIndentDetailsCollection = new List<clsDTOPurchaseIndentDetails>();

            for (int i = 0; i < dgvPurchaseIndent.Rows.Count; i++)
            {
                if (dgvPurchaseIndent["ItemID", i].Value != null && dgvPurchaseIndent["Quantity", i].Value != null)
                {
                    clsDTOPurchaseIndentDetails objClsPurchaseIndentDetails = new clsDTOPurchaseIndentDetails();
                    objClsPurchaseIndentDetails.intItemID = Convert.ToInt32(dgvPurchaseIndent["ItemID", i].Value);
                    objClsPurchaseIndentDetails.intUOMID = Convert.ToInt32(dgvPurchaseIndent["UOM", i].Tag);
                    objClsPurchaseIndentDetails.decQuantity = Convert.ToDecimal(dgvPurchaseIndent["Quantity", i].Value);
                    MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.lstDTOPurchaseIndentDetailsCollection.Add(objClsPurchaseIndentDetails);
                }
            }
        }

        private void Changestatus()
        {
            //Changing status
            MblnChangeStatus = true;
            if (MblnAddStatus)
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            else
            {
                if (MobjClsBLLPurchaseIndent != null && MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intStatusID == (Int32)OperationStatusType.PIndentOpened)
                    BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                else
                    BindingNavigatorSaveItem.Enabled = false;

                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            }


            errPurchaseIndent.Clear();
        }

        private bool DisplayAllNoInSearchGrid()
        {
            try
            {
                //Filling Search Grid
                DataTable dtIndents = MobjClsBLLPurchaseIndent.GetIndentNumbers();
                DataTable dtTemp = null;
                string strFilterCondition = string.Empty;
                if (!string.IsNullOrEmpty(txtSIndentNo.Text.Trim()))
                {
                    strFilterCondition = "IndentNo = '" + txtSIndentNo.Text.Trim() + "'";
                }
                else
                {
                    if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                        strFilterCondition += " CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue);
                    else
                    {
                        dtTemp = null;
                        dtTemp = (DataTable)cboSCompany.DataSource;
                        if (dtTemp.Rows.Count > 0)
                        {
                            strFilterCondition += "CompanyID In (";
                            foreach (DataRow dr in dtTemp.Rows)
                            {
                                strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                            }
                            strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                            strFilterCondition += ")";
                        }
                    }
                    if (cboSDepartment.SelectedValue != null && Convert.ToInt32(cboSDepartment.SelectedValue) != -2)
                    {
                        if (!string.IsNullOrEmpty(strFilterCondition))
                            strFilterCondition += " And ";
                        strFilterCondition += "DepartmentID = " + Convert.ToInt32(cboSDepartment.SelectedValue);
                    }
                    else
                    {
                        dtTemp = null;
                        dtTemp = (DataTable)cboSDepartment.DataSource;
                        if (dtTemp.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(strFilterCondition))
                                strFilterCondition += " AND ";
                            strFilterCondition += "DepartmentID In (";
                            foreach (DataRow dr in dtTemp.Rows)
                            {
                                strFilterCondition += Convert.ToInt32(dr["DepartmentID"]) + ",";
                            }
                            strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                            strFilterCondition += ")";
                        }
                    }

                    if (cboSExecutive.SelectedValue != null && Convert.ToInt32(cboSExecutive.SelectedValue) != -2)
                    {
                        if (!string.IsNullOrEmpty(strFilterCondition))
                            strFilterCondition += " And ";
                        strFilterCondition += "EmployeeID = " + Convert.ToInt32(cboSExecutive.SelectedValue);
                    }
                    else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                    {
                        //if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                        //{
                            dtTemp = null;
                            dtTemp = (DataTable)cboSExecutive.DataSource;
                            if (dtTemp.Rows.Count > 1)
                            {
                                clsBLLPermissionSettings MobjClsBLLPermissionSettings = new clsBLLPermissionSettings();
                                DataTable datTempPer = MobjClsBLLPermissionSettings.GetControlPermissions1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.PurIndEmployee);
                                if (datTempPer != null)
                                {
                                    if (datTempPer.Rows.Count > 0)
                                    {
                                        if (datTempPer.Rows[0]["IsVisible"].ToString() == "True")
                                            strFilterCondition += "And EmployeeID In (0,";
                                        else
                                            strFilterCondition += "And EmployeeID In (";
                                    }
                                    else
                                        strFilterCondition += "And EmployeeID In (";
                                }
                                else
                                    strFilterCondition += "And EmployeeID In (";
                                foreach (DataRow dr in dtTemp.Rows)
                                {
                                    strFilterCondition += Convert.ToInt32(dr["EmployeeID"]) + ",";
                                }
                                strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                                strFilterCondition += ")";
                            }
                        //}
                    }
                    if (cboSStatus.SelectedValue != null && Convert.ToInt32(cboSStatus.SelectedValue) != -2)
                    {
                        if (!string.IsNullOrEmpty(strFilterCondition))
                            strFilterCondition += " And ";
                        strFilterCondition += "StatusID = " + Convert.ToInt32(cboSStatus.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += " CreatedDate >= '" + dtpSFrom.Value.ToString("dd-MMM-yyyy") + "' And CreatedDate <= '" + dtpSTo.Value.ToString("dd-MMM-yyyy") + "' ";

                }
                dtIndents.DefaultView.RowFilter = strFilterCondition;

                dgvPurchaseIndentDisplay.DataSource = null;
                dgvPurchaseIndentDisplay.DataSource = dtIndents.DefaultView.ToTable();
                dgvPurchaseIndentDisplay.Columns["PurchaseIndentID"].Visible = false;
                dgvPurchaseIndentDisplay.Columns["StatusID"].Visible = false;
                dgvPurchaseIndentDisplay.Columns["DepartmentID"].Visible = false;
                dgvPurchaseIndentDisplay.Columns["CompanyID"].Visible = false;
                dgvPurchaseIndentDisplay.Columns["EmployeeID"].Visible = false;
                dgvPurchaseIndentDisplay.Columns["CreatedDate"].Visible = false;
                dgvPurchaseIndentDisplay.Columns["IndentNo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                lblIndentCount.Text = "Total Indents " + dgvPurchaseIndentDisplay.Rows.Count.ToString();
                return true;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on DisplayAllNoInSearchGrid() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DisplayAllNoInSearchGrid()" + ex.Message.ToString());
                return false;
            }
        }

        private bool DisplayIndentInfo(Int64 intIndentID)
        {
            try
            {
                //Displaying Indent Information

                if (MobjClsBLLPurchaseIndent.DisplayPurchaseIndentInfo(intIndentID))
                {
                    BindingNavigatorAddNewItem.Enabled = MblnAddPermission;

                    txtIndentNo.Tag = MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intPurchaseIndentID;
                    cboCompany.SelectedValue = MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intCompanyID;
                    if (cboCompany.SelectedValue == null)
                    {
                        DataTable datTemp = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intCompanyID });
                        if (datTemp.Rows.Count > 0)
                            cboCompany.Text = datTemp.Rows[0]["Name"].ToString();
                    }
                    cboCompany.Enabled = false;
                    // txtIndentNo.Enabled = false;
                    cboDepartment.SelectedValue = MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intDepartmentID;
                    txtIndentNo.Text = MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strPurchaseIndentNo;
                    dtpIndentDate.Value = Convert.ToDateTime(MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strIndentDate);
                    dtpDueDate.Value = Convert.ToDateTime(MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strDueDate);
                    txtRemarks.Text = MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strRemarks;
                    
                    lblCreatedByText.Text = " Created By :"+ MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strEmployeeName;
                    lblCreatedDateValue.Text = "Created Date :"+ MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strCreatedDate.ToDateTime().ToString("dd-MMM-yyyy");

                    DisplayIndentDetail(intIndentID);
                                        
                    if (MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intStatusID == (Int32)OperationStatusType.PIndentOpened)
                    {
                        BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
                        BindingNavigatorCancelItem.Text = "Cancel";
                        BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                        BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                    }
                    else if (MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intStatusID == (Int32)OperationStatusType.PIndentCancelled)
                    {
                        BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
                        BindingNavigatorCancelItem.Text = "Re Open";
                        BindingNavigatorSaveItem.Enabled = false;
                        BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                        //lblPurchaseIndentStatus.Text = " Cancelled Indents cannot be edited..";

                        txtDescription.Text = MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strDescription;
                        lblCreatedDateValue.Text += "         | Cancelled Date :"+ Convert.ToDateTime(MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strCancellationDate).ToString("dd-MMM-yyyy");
                        lblCreatedByText.Text += "         | Cancelled By :"+ MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strCancelledBy;
                    }
                    else
                    {
                        BindingNavigatorDeleteItem.Enabled = false;
                        BindingNavigatorCancelItem.Enabled = false;
                        BindingNavigatorSaveItem.Enabled = false;
                    }
                    if (MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intStatusID == (int)OperationStatusType.PIndentCancelled)
                    {
                        
                    }
                    SetStatusLabelText();
                    lblPurchaseIndentStatus.Text = "";
                }
                return true;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on DisplayIndentInfo() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DisplayIndentInfo()" + ex.Message.ToString());
                return false;
            }
        }

        private void DisplayIndentDetail(Int64 iOrderID)
        {
            try
            {
                //Display Indent Details
                MblnSearchFlag = true;
                dgvPurchaseIndent.Rows.Clear();
                DataTable DtPurOrderDetail = null;
                DtPurOrderDetail = MobjClsBLLPurchaseIndent.DisplayPurchaseIndentDetail(iOrderID);

                if (dgvPurchaseIndent.Rows.Count > 0)
                {
                    LoadCombos(4, null);
                    for (int i = 0; i < DtPurOrderDetail.Rows.Count; i++)
                    {
                        dgvPurchaseIndent.RowCount = dgvPurchaseIndent.RowCount + 1;
                        dgvPurchaseIndent.Rows[i].Cells["ItemCode"].Value = DtPurOrderDetail.Rows[i]["ItemCode"];
                        dgvPurchaseIndent.Rows[i].Cells["ItemName"].Value = DtPurOrderDetail.Rows[i]["Itemname"];
                        
                        dgvPurchaseIndent.Rows[i].Cells["ItemID"].Value = DtPurOrderDetail.Rows[i]["ItemID"];

                        FillUOMColumn(Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]));
                        dgvPurchaseIndent.Rows[i].Cells["Uom"].Value = DtPurOrderDetail.Rows[i]["UOMID"];
                        dgvPurchaseIndent.Rows[i].Cells["Uom"].Tag = DtPurOrderDetail.Rows[i]["UOMID"];
                        dgvPurchaseIndent.Rows[i].Cells["Uom"].Value = dgvPurchaseIndent.Rows[i].Cells["Uom"].FormattedValue;

                        int intUomScale = 0;
                        if (dgvPurchaseIndent[Uom.Index, i].Tag.ToInt32() != 0)
                            intUomScale = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "Scale", "STUOMReference", "UOMID = " + dgvPurchaseIndent[Uom.Index, i].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                        dgvPurchaseIndent.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["Quantity"].ToDecimal().ToString("F"+intUomScale);
                    }
                }
                DtPurOrderDetail = null;
                MblnSearchFlag = false;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on DisplayIndentDetail() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DisplayIndentDetails()" + ex.Message.ToString());
            }
        }

        private bool LoadCombos(int intType, string condition)
        {
            // function for loading combo
            // 0 - For Loading All Combo
            // 1 - For Loading CboCompanyName
            // 2 - For Loading CboSearchCompany
            // 3 - For Loading CboDepartment
            // 4 - For Loading CboStatus
            // 5 - For Loading cbosDepartment
            // 6 - For Loading cboSStatus
            // 7 - For Loading UOM ComboBox Column
            if (string.IsNullOrEmpty(condition))
                condition = string.Empty;
            else
                condition = " And " + condition;

            try
            {
                clsBLLPermissionSettings objBllPermissionSettings = new clsBLLPermissionSettings();
                DataTable dtControlsPermissions = objBllPermissionSettings.GetControlsPermissions(ClsCommonSettings.RoleID, (Int32)eMenuID.PurchaseIndent, ClsCommonSettings.CompanyID);

                MblnCancelPermission = false;
                if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    MblnCancelPermission = true;
                else
                {
                    dtControlsPermissions.DefaultView.RowFilter = "ControlName = 'BindingNavigatorCancelItem'";
                    if (dtControlsPermissions.DefaultView.ToTable().Rows.Count > 0)
                        MblnCancelPermission = Convert.ToBoolean(dtControlsPermissions.DefaultView.ToTable().Rows[0]["IsVisible"]);//"IsEnabled"]);]);
                }
                DataTable datCombos = new DataTable();
                //bool blnIsEnabled = false;

                if (intType == 0 || intType == 1)
                {
                    datCombos = null;
                    if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                        datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "CompanyID,Name", "CompanyMaster", "" });
                    else
                        datCombos = MobjClsBLLPurchaseIndent.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.PurIndCompany);

                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "Name";
                    cboCompany.DataSource = datCombos;
                }

                if (intType == 0 || intType == 2)
                {
                    datCombos = null;
                    if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                        datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "CompanyID,Name", "CompanyMaster", "" });
                    else
                    {
                        datCombos = MobjClsBLLPurchaseIndent.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.PurIndCompany);
                        if (datCombos == null)
                            datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "CompanyID,Name", "CompanyMaster", "CompanyID = " + ClsCommonSettings.CompanyID });
                        else
                        {
                            if (datCombos.Rows.Count == 0)
                                datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "CompanyID,Name", "CompanyMaster", "CompanyID = " + ClsCommonSettings.CompanyID });
                        }
                    }
                    cboSCompany.ValueMember = "CompanyID";
                    cboSCompany.DisplayMember = "Name";
                    cboSCompany.DataSource = datCombos;
                }

                if (intType == 0 || intType == 3)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "DepartmentID,Description", "DepartmentReference", "" });
                    cboDepartment.ValueMember = "DepartmentID";
                    cboDepartment.DisplayMember = "Description";
                    cboDepartment.DataSource = datCombos;
                }

                if (intType == 0 || intType == 4)
                {
                    //datCombos = null;
                    //datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "StatusID,Description", "STCommonStatusReference", "StatusTypeID = 1", "StatusID", "Description" });
                    //CboTStatus.ValueMember = "StatusID";
                    //CboTStatus.DisplayMember = "Description";
                    //CboTStatus.DataSource = datCombos;
                }

                if (intType == 0 || intType == 5)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "DepartmentID,Description", "DepartmentReference", "" });
                    cboSDepartment.ValueMember = "DepartmentID";
                    cboSDepartment.DisplayMember = "Description";
                    cboSDepartment.DataSource = datCombos;
                }

                if (intType == 0 || intType == 6)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "StatusID,Description", "STCommonStatusReference", "StatusTypeID = 1", "StatusID", "Description" });
                    DataRow dr = datCombos.NewRow();
                    dr["StatusID"] = -2;
                    dr["Description"] = "ALL";
                    datCombos.Rows.InsertAt(dr, 0);
                    cboSStatus.ValueMember = "StatusID";
                    cboSStatus.DisplayMember = "Description";
                    cboSStatus.DataSource = datCombos;
                }
                if (intType == 0 || intType == 7)
                {
                    //        datCombos = null;
                    //        dtControlsPermissions.DefaultView.RowFilter = "ControlName = 'cboSExecutive'";
                    //        if (dtControlsPermissions.DefaultView.ToTable().Rows.Count > 0)
                    //            blnIsEnabled = Convert.ToBoolean(dtControlsPermissions.DefaultView.ToTable().Rows[0]["IsVisible"]);//"IsEnabled"]);]);
                    //        if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2 || blnIsEnabled)
                    //            datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "EmployeeID,FirstName", "EmployeeMaster",""});
                    //        else
                    //            datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "EmployeeID,FirstName", "EmployeeMaster", "EmployeeID = " + ClsCommonSettings.intEmployeeID + ""});
                    //        cboSExecutive.ValueMember = "EmployeeID";
                    //        cboSExecutive.DisplayMember = "FirstName";
                    //        cboSExecutive.DataSource = datCombos;
                }

                mObjLogs.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        private void SetStatusLabelText()
        {
            lblDescription.Visible = false;
            txtDescription.Visible = false;
            switch (MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intStatusID)
            {
                case 0:
                    lblStatusText.Text = "New";
                    lblStatusText.ForeColor = Color.Black;
                    break;
                //case (int)OperationStatusType.PIndentCancelled:
                //    lblStatusText.Text = "Cancelled";
                //    lblStatusText.ForeColor = Color.Red;
                //    lblDescription.Visible = true;
                //    txtDescription.Visible = true;
                //    break;
                //case (int)OperationStatusType.PIndentOpened:
                //    lblStatusText.Text = "Opened";
                //    lblStatusText.ForeColor = Color.Brown;
                //    break;
                //case (int)OperationStatusType.PIndentClosed:
                //    lblStatusText.Text = "Closed";
                //    lblStatusText.ForeColor = Color.Purple;
                //    break;
            }
        }

        private DataTable FillUOMColumn(Int32 intItemID)
        {
            try
            {
                DataTable dtItemUOMs = MobjClsBLLPurchaseIndent.GetItemUOMs(intItemID);
                Uom.DataSource = null;
                Uom.ValueMember = "UOMID";
                Uom.DisplayMember = "ShortName";
                Uom.DataSource = dtItemUOMs;
                return dtItemUOMs;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on FillUomColumn() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FillUomColumn()" + ex.Message.ToString());
            }
            return null;
        }

        public void RefreshForm()
        {
            btnSRefresh_Click(null, null);
        }

        #endregion

        #region Events

        private void FrmPurchase_Load(object sender, EventArgs e)
        {
            // Load Function
            try
            {
                LoadCombos(0, null);
                LoadMessage();
                SetPermissions();
                AddNewPurchaseIndent();
                expandableSplitterLeft.Expanded = true;
                expandableSplitterLeft.Expanded = false;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on FrmPurchaseIndentLoad() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmPurchaseIndentLoad()" + ex.Message.ToString());
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            //Add New Function
            AddNewPurchaseIndent();
        }

        private void PurchaseOrderBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            //Save Function
            try
            {
                txtIndentNo.Focus();

                if (Save())
                {
                    AddNewPurchaseIndent();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorSaveItem_Click() " + ex.Message);
                mObjLogs.WriteLog("Error in BindingNavigatorSaveItem_Click() " + ex.Message, 2);
            }

        }

        private void dgvPurchaseIndent_Textbox_TextChanged(object sender, EventArgs e)
        {
            //Item Selection Event 
            try
            {
                if (dgvPurchaseIndent.CurrentCell.ColumnIndex == 0 || dgvPurchaseIndent.CurrentCell.ColumnIndex == 1)
                {
                    for (int i = 0; i < dgvPurchaseIndent.Columns.Count; i++)
                    {
                        dgvPurchaseIndent.Rows[dgvPurchaseIndent.CurrentCell.RowIndex].Cells[i].Value = null;
                        dgvPurchaseIndent.Rows[dgvPurchaseIndent.CurrentCell.RowIndex].Cells[i].Tag = null;
                    }
                }
                dgvPurchaseIndent.PServerName = ClsCommonSettings.ServerName;
                string[] First = { "ItemCode", "ItemName", "ItemID" };//first grid 
                string[] second = { "ItemCode", "Description", "ItemID" };//inner grid
                dgvPurchaseIndent.aryFirstGridParam = First;
                dgvPurchaseIndent.arySecondGridParam = second;
                dgvPurchaseIndent.PiFocusIndex = Quantity.Index;
                dgvPurchaseIndent.iGridWidth = 350;
                dgvPurchaseIndent.bBothScrollBar = true;
                dgvPurchaseIndent.pnlLeft = expandableSplitterLeft.Location.X + 5;
                dgvPurchaseIndent.pnlTop = expandableSplitterTop.Location.Y + PurchaseOrderBindingNavigator.Height + 10;
                dgvPurchaseIndent.ColumnsToHide = new string[] { "ItemID" };
                if (dgvPurchaseIndent.CurrentCell.ColumnIndex == ItemCode.Index)
                {
                    dgvPurchaseIndent.field = "ItemCode";
                    dgvPurchaseIndent.CurrentCell.Value = dgvPurchaseIndent.TextBoxText;
                }
                else if (dgvPurchaseIndent.CurrentCell.ColumnIndex == ItemName.Index)
                {
                    // dgvPurchaseIndent.PiColumnIndex = 1;
                    dgvPurchaseIndent.field = "Description";
                    dgvPurchaseIndent.CurrentCell.Value = dgvPurchaseIndent.TextBoxText;
                }
                int intParentID = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "ParentID", "CompanyMaster", "CompanyID = " + cboCompany.SelectedValue.ToInt32() }).Rows[0]["ParentID"].ToInt32();
                DataTable datCompany = new DataTable();
                if (intParentID == 0)
                    datCompany = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "CompanyID", "CompanyMaster", "(CompanyID = " + cboCompany.SelectedValue.ToInt32() + " Or ParentID = " + cboCompany.SelectedValue.ToInt32() + ")" });
                else
                    datCompany = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "CompanyID", "CompanyMaster", "(CompanyID = " + intParentID + " Or ParentID = " + intParentID + ")" });
                if (datCompany.Rows.Count > 0)
                {
                    DataRow dr = datCompany.NewRow();
                    dr["CompanyID"] = 0;
                    datCompany.Rows.InsertAt(dr, 0);
                }

                dgvPurchaseIndent.PsQuery = "select [ItemCode] as ItemCode,[Description] as Description,[ItemID] as ItemID from STItemmaster where " + dgvPurchaseIndent.field + " like  '" + ((dgvPurchaseIndent.TextBoxText)) + "%' And ItemStatusID In (" + (int)OperationStatusType.ProductActive + ")";
                dgvPurchaseIndent.PsQuery += " And IsNull(CompanyID,0) In (";
                foreach (DataRow dr in datCompany.Rows)
                    dgvPurchaseIndent.PsQuery += dr["CompanyID"].ToInt32() + ",";
                dgvPurchaseIndent.PsQuery = dgvPurchaseIndent.PsQuery.Remove(dgvPurchaseIndent.PsQuery.Length - 1);
                dgvPurchaseIndent.PsQuery += ")";
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on dgvPurchaseIndentTextBox_TextChanged() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on dgvPurchaseIndentTextBox_TextChanged()" + ex.Message.ToString());
            }
        }

        private void CancelToolStripButton_Click(object sender, EventArgs e)
        {
            //Clear Controls
            ClearControls();
        }

        private void dgvPurchaseIndent_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == ItemCode.Index) || (e.ColumnIndex == ItemName.Index))
            {
                dgvPurchaseIndent.PiColumnIndex = e.ColumnIndex;
            }
        }

        private void cb_KeyPress(object sender, KeyPressEventArgs e)
        {
            cb.DroppedDown = false;
        }

        private void dgvPurchaseIndent_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvPurchaseIndent.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
            dgvPurchaseIndent.CellLeave += new DataGridViewCellEventHandler(dgvPurchaseIndent_CellLeave);
        }

        void dgvPurchaseIndent_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvPurchaseIndent.CurrentCell.OwningColumn.Name == "Quantity")
            {
                decimal decValue = 0;
                try
                {
                    decValue = Convert.ToDecimal(dgvPurchaseIndent[e.ColumnIndex, e.RowIndex].Value);
                }
                catch
                {
                    dgvPurchaseIndent[e.ColumnIndex, e.RowIndex].Value = 0;
                }
            }
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvPurchaseIndent.CurrentCell.OwningColumn.Name == "Quantity")
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
                int intUomScale = 0;
                    if (dgvPurchaseIndent[Uom.Index, dgvPurchaseIndent.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                        intUomScale = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "Scale", "STUOMReference", "UOMID = " + dgvPurchaseIndent[Uom.Index, dgvPurchaseIndent.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
             
                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    if (intUomScale == 0)
                    {
                        if (e.KeyChar == '.')
                            e.Handled = true;
                    }
                    else
                    {
                        int dotIndex = -1;
                        if (txt.Text.Contains("."))
                        {
                            dotIndex = txt.Text.IndexOf('.');
                        }
                        if (e.KeyChar == '.')
                        {
                            if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            {
                                e.Handled = true;
                            }
                        }
                        else
                        {
                            if (char.IsDigit(e.KeyChar))
                            {
                                if (dotIndex != -1 && txt.SelectionStart > dotIndex)
                                {
                                    string[] splitText = txt.Text.Split('.');
                                    if (splitText.Length == 2)
                                    {
                                        if (splitText[1].Length - txt.SelectedText.Length >= intUomScale)
                                            e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }

                //int intDecimalPart = 0;
                //if (dgvPurchaseIndent.EditingControl != null && !string.IsNullOrEmpty(dgvPurchaseIndent.EditingControl.Text) && dgvPurchaseIndent.EditingControl.Text.Contains("."))
                //{
                    
                //    int intDotIndex = dgvPurchaseIndent.EditingControl.Text.IndexOf('.');
                //    if(intDotIndex >= 0 && txt.SelectionStart>intDotIndex)
                //    intDecimalPart = dgvPurchaseIndent.EditingControl.Text.Split('.')[1].Length;
                //}
                //  int intUomScale = 0;
                //    if (dgvPurchaseIndent[Uom.Index, dgvPurchaseIndent.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                //        intUomScale = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "Scale", "STUOMReference", "UOMID = " + dgvPurchaseIndent[Uom.Index, dgvPurchaseIndent.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                //if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                //{
                //    e.Handled = true;
                //}

                //if (e.KeyChar.ToString() == "." && (intUomScale == 0 || (string.IsNullOrEmpty(txt.SelectedText) && dgvPurchaseIndent.EditingControl.Text.Length - txt.SelectionStart >intUomScale)))
                //{
                //    e.Handled = true;
                //}

                //if (string.IsNullOrEmpty(txt.SelectedText) && e.KeyChar != 08 && e.KeyChar !=13 && dgvPurchaseIndent.EditingControl != null && !string.IsNullOrEmpty(dgvPurchaseIndent.EditingControl.Text) && (dgvPurchaseIndent.EditingControl.Text.Contains(".") && ((e.KeyChar == 46) || intDecimalPart == intUomScale)))//checking more than one "."
                //{
                //    e.Handled = true;
                //}
                
            }
            else if (dgvPurchaseIndent.CurrentCell.OwningColumn.Name == "ItemCode" || dgvPurchaseIndent.CurrentCell.OwningColumn.Name == "ItemName")
            {
                if (e.KeyChar.ToString() == "'")
                {
                    e.Handled = true;
                }
            }
        }

        private void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Text Box Keypress Event
            if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
            {
                e.Handled = true;
            }
            if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            {
                e.Handled = true;
            }
        }

        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void dgvPurchaseIndent_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (MblnSearchFlag == true) return;

                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    //if (e.ColumnIndex == Quantity.Index)
                    //{
                    //    int intDecimalPart = 0;
                    //    if (dgvPurchaseIndent.CurrentCell.Value != null && !string.IsNullOrEmpty(dgvPurchaseIndent.CurrentCell.Value.ToString()) && dgvPurchaseIndent.CurrentCell.Value.ToString().Contains("."))
                    //        intDecimalPart = dgvPurchaseIndent.CurrentCell.Value.ToString().Split('.')[1].Length;
                    //    int intUomScale = 0;
                    //    if (dgvPurchaseIndent[Uom.Index, dgvPurchaseIndent.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    //        intUomScale = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "Scale", "STUOMReference", "UOMID = " + dgvPurchaseIndent[Uom.Index, dgvPurchaseIndent.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                    //    if (intDecimalPart > intUomScale)
                    //    {
                    //        dgvPurchaseIndent.CurrentCell.Value = Math.Round(dgvPurchaseIndent.CurrentCell.Value.ToDecimal(), intUomScale).ToString();
                    //        dgvPurchaseIndent.EditingControl.Text = dgvPurchaseIndent.CurrentCell.Value.ToString();
                    //    }
                    //}
                }
                if (e.ColumnIndex == ItemID.Index && e.RowIndex >= 0)
                {
                    if (dgvPurchaseIndent["ItemCode", e.RowIndex].Value != null)
                    {

                        if (dgvPurchaseIndent.CurrentRow.Cells["ItemID"].Value != null)
                        {
                            Int32 iItemID = Convert.ToInt32(dgvPurchaseIndent.CurrentRow.Cells["ItemID"].Value);
                            int tag = -1;
                            if (dgvPurchaseIndent.CurrentRow.Cells["Uom"].Tag != null)
                            {
                                tag = Convert.ToInt32(dgvPurchaseIndent.CurrentRow.Cells["Uom"].Tag);
                            }
                            FillUOMColumn(iItemID);
                            if (tag == -1)
                            {
                                DataTable datDefaultUom = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "DefaultPurchaseUomID as UomID", "STItemMaster", "ItemID = " + iItemID + "" });
                                tag = Convert.ToInt32(datDefaultUom.Rows[0]["UomID"]);
                            }
                            dgvPurchaseIndent.CurrentRow.Cells["Uom"].Value = tag;
                            dgvPurchaseIndent.CurrentRow.Cells["Uom"].Tag = tag;
                            dgvPurchaseIndent.CurrentRow.Cells["Uom"].Value = dgvPurchaseIndent.CurrentRow.Cells["Uom"].FormattedValue;
                        }
                        else
                            Uom.DataSource = null;
                    }
                    else
                    {
                        Uom.DataSource = null;
                    }
                }
               
                Changestatus();
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on dgvPurchaseIndent_CellValueChanged() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on dgvPurchaseIndent_CellValueChanged()" + ex.Message.ToString());
            }
        }

        private void dgvPurchaseIndent_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvPurchaseIndent.IsCurrentCellDirty)
            {
                if (dgvPurchaseIndent.CurrentCell != null)
                    dgvPurchaseIndent.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dgvPurchaseIndent_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                //Cell Begin Edit
                int iRowIndex = 0;
                //----------------------------------------------------------------------------------------------------------------
                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    dgvPurchaseIndent.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    iRowIndex = dgvPurchaseIndent.CurrentCell.RowIndex;

                    //------------------------------------------------------------UOM----------------------------------------------------------
                    if (e.ColumnIndex == Uom.Index)
                    {
                        if (dgvPurchaseIndent.CurrentRow.Cells["ItemID"].Value != null)
                        {
                            int iItemID = Convert.ToInt32(dgvPurchaseIndent.CurrentRow.Cells["ItemID"].Value);
                            int tag = -1;
                            if (dgvPurchaseIndent.CurrentRow.Cells["Uom"].Tag != null)
                            {
                                tag = Convert.ToInt32(dgvPurchaseIndent.CurrentRow.Cells["Uom"].Tag);
                            }
                            FillUOMColumn(iItemID);
                            if (tag != -1)
                                dgvPurchaseIndent.CurrentRow.Cells["Uom"].Tag = tag;
                        }
                        else
                        {
                            Uom.DataSource = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on dgvPurchaseIndent_CellBeginEdit() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on dgvPurchaseIndent_CellBeginEdit()" + ex.Message.ToString());
            }
        }

        private void btnTContextmenu_Click(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void dgvPurchaseIndent_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    //------------------------------------------------------------UOM----------------------------------------------------------
                    if (e.ColumnIndex == Uom.Index)
                    {
                        if (dgvPurchaseIndent.CurrentRow != null && dgvPurchaseIndent.CurrentRow.Cells["Uom"].Value != null)
                        {
                            dgvPurchaseIndent.CurrentRow.Cells["Uom"].Tag = dgvPurchaseIndent.CurrentRow.Cells["Uom"].Value;
                            dgvPurchaseIndent.CurrentRow.Cells["Uom"].Value = dgvPurchaseIndent.CurrentRow.Cells["Uom"].FormattedValue;

                          //  int intDecimalPart = 0;
                          //  if (dgvPurchaseIndent.CurrentRow.Cells["Quantity"].Value != null && !string.IsNullOrEmpty(dgvPurchaseIndent.CurrentRow.Cells["Quantity"].Value.ToString()) && dgvPurchaseIndent.CurrentRow.Cells["Quantity"].Value.ToString().Contains("."))
                         //       intDecimalPart = dgvPurchaseIndent.CurrentRow.Cells["Quantity"].Value.ToString().Split('.')[1].Length;
                            int intUomScale = 0;
                            if (dgvPurchaseIndent[Uom.Index, dgvPurchaseIndent.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                                intUomScale = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "Scale", "STUOMReference", "UOMID = " + dgvPurchaseIndent[Uom.Index, dgvPurchaseIndent.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                         //   if (intDecimalPart > intUomScale)
                         //   {
                                dgvPurchaseIndent.CurrentRow.Cells["Quantity"].Value = dgvPurchaseIndent.CurrentRow.Cells["Quantity"].Value.ToDecimal().ToString("F" + intUomScale);
                         //   }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on dgvPurchaseIndent_CellEndEdit() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on dgvPurchaseIndent_CellEndEdit()" + ex.Message.ToString());
            }
        }

        private void Text_Changed(object sendeer, EventArgs e)
        {
            Changestatus();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            //Purchase Indent Deletion
            try
            {

                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 936, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                if (Convert.ToInt32(txtIndentNo.Tag) > 0)
                {
                    MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intPurchaseIndentID = Convert.ToInt32(txtIndentNo.Tag);

                    if (MobjClsBLLPurchaseIndent.DeletePurchaseIndent())
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                        lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        tmrFocus.Enabled = true;
                        AddNewPurchaseIndent();
                    }
                    else
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 933, out MmessageIcon);
                        //mObjNotification.CallErrorMessage(MaMessageArr, 933, MsMessageCaption);
                        lblPurchaseIndentStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        tmrFocus.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorDeleteItem_Click() " + ex.Message);
                mObjLogs.WriteLog("Error in BindingNavigatorDeleteItem_Click() " + ex.Message, 2);
            }
        }

        private void btnDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                //Department Editing
                FrmCommonRef objFrmCommonRef = new FrmCommonRef("Department", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "DepartmentID,Predefined, Description As Department", "DepartmentReference", "");
                objFrmCommonRef.ShowDialog();
                int intComboID = cboDepartment.SelectedValue.ToInt32();
                LoadCombos(3, null);
                LoadCombos(5, null);
                if (objFrmCommonRef.NewID != 0)
                    cboDepartment.SelectedValue = objFrmCommonRef.NewID;
                else
                    cboDepartment.SelectedValue = intComboID;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on btnDepartment_Click() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnDepartment_Click()" + ex.Message.ToString());
            }
        }

        private void FrmPurchaseIndent_Shown(object sender, EventArgs e)
        {
            cboCompany.Focus();
        }

        private void btnSRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                DisplayAllNoInSearchGrid();
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on btnSRefresh_Click() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnSRefresh_Click()" + ex.Message.ToString());
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                //function for loading report
                if (MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intPurchaseIndentID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intPurchaseIndentID;
                    ObjViewer.PiFormID = (int)FormID.PurchaseIndent;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BtnPrint_Click() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnPrint_Click()" + ex.Message.ToString());
            }
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup objEmailPopUp = new FrmEmailPopup())
                {
                    objEmailPopUp.MsSubject = "Purchase Indent Information";
                    objEmailPopUp.EmailFormType = EmailFormID.PurchaseIndent;
                    objEmailPopUp.EmailSource = MobjClsBLLPurchaseIndent.GetPurchaseIndentReport();
                    objEmailPopUp.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BtnEmail_Click() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnEmail_Click()" + ex.Message.ToString());
            }
        }

        private void BtnUom_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmUnitOfMeasurement objUoM = new FrmUnitOfMeasurement())
                {
                    objUoM.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BtnUom_Click() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnUom_Click()" + ex.Message.ToString());
            }
        }

        private void BtnItem_Click(object sender, EventArgs e)
        {
            try
            {
                frmItemMaster objItemMaster = new frmItemMaster(true);
                objItemMaster.Text = "Product Master " + (ClsMainSettings.objFrmMain.MdiChildren.Length + 1);
                objItemMaster.MdiParent = ClsMainSettings.objFrmMain;
                objItemMaster.WindowState = FormWindowState.Maximized;
                objItemMaster.Show();
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BtnItem_CLick() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnItem_Click()" + ex.Message.ToString());
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                if (BindingNavigatorCancelItem.Text.ToUpper() == "CANCEL")
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 955, out MmessageIcon).Replace("***", "Indent");

                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                        {
                            objFrmCancellationDetails.ShowDialog();
                            MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.strCancellationDate  = objFrmCancellationDetails.PDtCancellationDate.ToString("dd-MMM-yyyy");
                            txtDescription.Text = objFrmCancellationDetails.PStrDescription;
                            MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intCancelledBy = ClsCommonSettings.UserID;
                            if (!objFrmCancellationDetails.PBlnIsCanelled)
                            {
                                DisplayIndentInfo(Convert.ToInt32(MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intPurchaseIndentID));
                                MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intStatusID = (int)OperationStatusType.PIndentCancelled;
                                MBlnIsFromCancellation = true;
                                SavePurchaseIndent();
                                txtDescription.Text = objFrmCancellationDetails.PStrDescription;
                                MBlnIsFromCancellation = false;
                                AddNewPurchaseIndent();
                            }
                        }
                    }
                }
                else
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 956, out MmessageIcon).Replace("***", "Indent");

                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intStatusID = (Int32)OperationStatusType.PIndentOpened;
                        MBlnIsFromCancellation = true;
                        SavePurchaseIndent();
                        MBlnIsFromCancellation = false;
                        AddNewPurchaseIndent();
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorCancelItem_Click() " + ex.Message);
                mObjLogs.WriteLog("Error in BindingNavigatorCancelItem_Click() " + ex.Message, 2);
            }
        }

        private void txtSIndentNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\'')
                e.Handled = true;
        }

        private void FrmPurchaseIndent_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.MdiFormClosing)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4214, out MmessageIcon).Replace("#", "").Trim();
                if (!MblnChangeStatus || MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                    DialogResult.Yes)
                {

                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void tmrPurchaseIndent_Tick(object sender, EventArgs e)
        {
            errPurchaseIndent.Clear();
            lblPurchaseIndentStatus.Text = "";
            tmrPurchaseIndent.Enabled = false;
        }

        private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Company Selection Index Changed
            Changestatus();
            if (MobjClsBLLPurchaseIndent.PobjClsDTOPurchaseIndent.intPurchaseIndentID == 0)
                GenerateIndentNo();

            int intCurrentCompanyID = 0;
            DataTable datCompany = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "CompanyID,ParentID", "CompanyMaster", "CompanyID = " + cboCompany.SelectedValue.ToInt32() });
            if (datCompany.Rows[0]["ParentID"].ToInt32() == 0)
                intCurrentCompanyID = datCompany.Rows[0]["CompanyID"].ToInt32();
            else
                intCurrentCompanyID = datCompany.Rows[0]["ParentID"].ToInt32();
            if (intCurrentCompanyID != MintCompanyId)
                dgvPurchaseIndent.Rows.Clear();
            MintCompanyId = intCurrentCompanyID;
        }

        private void dgvPurchaseIndentDisplay_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //Search Grid Cell Click
            if (dgvPurchaseIndentDisplay.Rows.Count > 0)
            {
                if (Convert.ToInt64(dgvPurchaseIndentDisplay.CurrentRow.Cells[0].Value) != 0)
                {
                    MblnSearchFlag = true;
                    MblnAddStatus = false;
                    DisplayIndentInfo(Convert.ToInt64(dgvPurchaseIndentDisplay.CurrentRow.Cells[0].Value));
                    BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                    BtnPrint.Enabled = MblnPrintEmailPermission;
                    BtnEmail.Enabled = MblnPrintEmailPermission;
                    MblnSearchFlag = false;
                    lblPurchaseIndentStatus.Text = "Details of Indent No " + dgvPurchaseIndentDisplay.CurrentRow.Cells[1].Value.ToString();
                }
            }
        }

        private void dtpIndentDate_ValueChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtIndentNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetterOrDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
                e.Handled = true;
            else
                e.Handled = false;
        }

        private void cboSExecutive_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsBLLPermissionSettings objBllPermissionSettings = new clsBLLPermissionSettings();
            DataTable dtControlsPermissions = objBllPermissionSettings.GetControlsPermissions(ClsCommonSettings.RoleID, (Int32)eMenuID.PurchaseIndent, ClsCommonSettings.CompanyID);
            DataTable datCombos = null; bool blnIsEnabled = false;
            dtControlsPermissions.DefaultView.RowFilter = "ControlName = 'cboDepartment'";
            if (dtControlsPermissions.DefaultView.ToTable().Rows.Count > 0)
                blnIsEnabled = Convert.ToBoolean(dtControlsPermissions.DefaultView.ToTable().Rows[0]["IsVisible"]);//"IsEnabled"]);]);
            if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2 || blnIsEnabled)
            {
                if (Convert.ToInt32(cboSExecutive.SelectedValue) == 0 || Convert.ToInt32(cboSExecutive.SelectedValue) == -2)
                {
                    string strFilterString = "";
                    DataTable datEmployees = (DataTable)cboSExecutive.DataSource;
                    //datEmployees.Rows[0].Delete();
                    //datEmployees.AcceptChanges();
                    //datEmployees.Be
                    if (datEmployees.Rows.Count > 0)
                    {
                        strFilterString = "EmployeeID In (";
                        foreach (DataRow dr in datEmployees.Rows)
                        {
                            strFilterString += Convert.ToInt32(dr["EmployeeID"]) + ",";
                        }
                        strFilterString = strFilterString.Remove(strFilterString.Length - 1);
                        strFilterString += ")";
                    }
                    if (!string.IsNullOrEmpty(strFilterString))
                        datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "DepartmentID,Description", "DepartmentReference", "" });// "DepartmentID In (Select DepartmentID  From EmployeeMaster Where " + strFilterString + ")" });
                }
                else
                    datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "DepartmentID,Description", "DepartmentReference", "" });//"DepartmentID In (Select DepartmentID From EmployeeMaster Where EmployeeID = " + Convert.ToInt32(cboSExecutive.SelectedValue) + ")" });
            }
            else
                datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "DepartmentID,Description", "DepartmentReference", "" });//"DepartmentID = " + ClsCommonSettings.intDepartmentID + "", "DepartmentID", "Description" });

            DataRow drNewRow = datCombos.NewRow();
            drNewRow["DepartmentID"] = -2;
            drNewRow["Description"] = "ALL";
            datCombos.Rows.InsertAt(drNewRow, 0);
            cboSDepartment.ValueMember = "DepartmentID";
            cboSDepartment.DisplayMember = "Description";
            cboSDepartment.DataSource = datCombos;
        }

        private void cboSCompany_SelectedIndexChanged(object sender, EventArgs e)
        {

            DataTable datCombos = null; //bool blnIsEnabled = false;
            if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
            {
                if (cboSCompany.SelectedValue == null || Convert.ToInt32(cboSCompany.SelectedValue) == -2)
                {
                    DataTable datCompany = (DataTable)cboSCompany.DataSource;
                    string strFilterCondition = "";
                    //datCompany.Rows.RemoveAt(0);
                    //datCompany.AcceptChanges();
                    if (datCompany.Rows.Count > 0)
                    {
                        strFilterCondition = "CompanyID In (";
                        foreach (DataRow dr in datCompany.Rows)
                            strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                        strFilterCondition += ")";
                    }
                    //datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "EmployeeID,FirstName", "EmployeeMaster", strFilterCondition });
                    datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
                        " ON EM.EmployeeID=UM.EmployeeID INNER JOIN STPurchaseIndentMaster IM ON UM.UserID=IM.CreatedBy", "IM." + strFilterCondition });
                }
                else
                {
                    //datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "EmployeeID,FirstName", "EmployeeMaster", "CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue) });
                    datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
                        " ON EM.EmployeeID=UM.EmployeeID INNER JOIN STPurchaseIndentMaster IM ON UM.UserID=IM.CreatedBy", "IM.CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue) });
                }
            }
            else
            {
                try
                {
                    DataTable datTemp = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "IsVisible", "STRoleFieldsDetails", "" + 
                        "RoleID=" + ClsCommonSettings.RoleID + " AND CompanyID=" + ClsCommonSettings.CompanyID + " " +
                        "AND ControlID=" + (int)ControlOperationType.PurIndEmployee + " AND IsVisible=1" });
                    if (datTemp != null)
                        if (datTemp.Rows.Count > 0)
                            datCombos = MobjClsBLLPurchaseIndent.GetEmployeeByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.PurIndCompany);

                    if (datCombos == null)
                        datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
                                        " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
                    else
                    {
                        if (datCombos.Rows.Count == 0)
                            datCombos = MobjClsBLLPurchaseIndent.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
                                        " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
                    }
                }
                catch { }
            }            
            cboSExecutive.ValueMember = "EmployeeID";
            cboSExecutive.DisplayMember = "EmployeeFullName";
            cboSExecutive.DataSource = datCombos;
        }

        private void FrmPurchaseIndent_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Alt | Keys.S:
                        PurchaseOrderBindingNavigatorSaveItem_Click(sender, new EventArgs());//save
                        break;
                    case Keys.Control | Keys.R:
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//Remove
                        break;


                }
            }
            catch (Exception)
            {
            }
        }

        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        #endregion

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "PurchaseIndent";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void expandableSplitterLeft_ExpandedChanged(object sender, ExpandedChangeEventArgs e)
        {
            if (expandableSplitterLeft.Expanded)
            {
                if (dgvPurchaseIndentDisplay.Columns.Count > 0)
                    dgvPurchaseIndentDisplay.Columns[0].Visible = false;
            }
        }

        private void lnkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        { 
            DataTable datAdvanceSearchedData= new DataTable();
            using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.PurchaseIndent))
            {
                objFrmSearchForm.ShowDialog();
                if (objFrmSearchForm.datSerachedData!=null&&objFrmSearchForm.datSerachedData.Rows.Count > 0)
                {
                    datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                    datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                    dgvPurchaseIndentDisplay.DataSource = datAdvanceSearchedData;
                    dgvPurchaseIndentDisplay.Columns["ID"].Visible = false;
                    dgvPurchaseIndentDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

    }
}
