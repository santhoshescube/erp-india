using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data;
using System.Data.SqlClient;


using Microsoft.ReportingServices.ReportRendering;
using System.Collections.Generic;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,23 Feb 2011>
   Description:	<Description,,Purchase Form>
================================================
*/
namespace MyBooksERP
{
    /// <summary>
    /// Summary description for frmDocument.
    /// </summary>
    public class FrmDirectGRN : DevComponents.DotNetBar.Office2007Form
    {

        #region Designer

        ComboBox cb;
        public Command CommandZoom;
        private DockContainerItem dockContainerItem1;
        private ExpandableSplitter expandableSplitterLeft;
        private PanelEx PanelLeft;
        private IContainer components;
        private DotNetBarManager dotNetBarManager1;
        private DockSite dockSite4;
        private DockSite dockSite1;
        private DockSite dockSite2;
        private DockSite dockSite3;
        private DockSite dockSite5;
        private DockSite dockSite6;
        private Bar bar1;
        private DockSite dockSite8;
        private PanelEx panelEx1;
        private Bar PurchaseOrderBindingNavigator;
        private ButtonItem BindingNavigatorAddNewItem;
        private ExpandableSplitter expandableSplitterTop;
        private PanelEx panelTop;
        private ExpandablePanel expandablePanel1;
        private LabelX lblPurchaseCount;
        private PanelEx panelBottom;
        private PanelEx panelLeftTop;
        private Label lblSCompany;
        private Label lblSSupplier;
        private Label LblSStatus;
        private ClsInnerGridBar dgvGRN;
        private ButtonItem BindingNavigatorSaveItem;
        private ButtonItem BindingNavigatorClearItem;
        private ButtonItem BindingNavigatorDeleteItem;
        private ButtonItem BtnUom;
        private ButtonX btnSRefresh;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSGRNNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSStatus;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSSupplier;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSCompany;
        private Label lblWarehouse;
        private Label lblCompany;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private Label lblStatus;
        private Label lblDate;
        private Label lblPurchaseNo;
        internal ContextMenuStrip CMSVendorAddress;
        internal Timer TmrPurchase;
        internal Timer TmrFocus;
        internal ErrorProvider ErrPurchase;
        private ButtonItem BtnPrint;
        private ButtonItem BtnEmail;
        private ButtonX btnWarehouse;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private Label lblDescription;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDescription;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private ImageList ImgPurchase;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private ButtonItem BindingNavigatorCancelItem;
        private Label lblStatusText;
        private ButtonX BtnTVenderAddress;
        private ButtonX btnSupplier;
        private System.Windows.Forms.TextBox txtSupplierAddress;
        private Label lblSupplierAddressName;
        private Label lblSupplier;
        private ButtonItem btnActions;
        private DemoClsDataGridview.CalendarColumn calendarColumn1;
        Random random = new Random();
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSupplier;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGRNNo;
        private ButtonItem btnPurchaseInvoice;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSExecutive;
        private Label lblExecutive;
        private ButtonX btnAddressChange;

        public Int64 intPurchaseIDToLoad = 0;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvGRNDisplay;
        private System.Windows.Forms.DateTimePicker dtpSTo;
        private System.Windows.Forms.DateTimePicker dtpSFrom;
        private Label lblSGRNNo;
        private Label lblTo;
        private Label lblFrom;
        private ButtonItem btnPurchaseOrder;
        private DevComponents.DotNetBar.TabControl tcPurchase;
        private TabItem tpLocationDetails;
        private TabControlPanel tabControlPanel1;
        private TabItem tpItemDetails;
        private DevComponents.DotNetBar.TabControl tcGeneral;
        private TabControlPanel tabControlPanel3;
        private TabItem tiGeneral;
        private TabItem tiSuggestions;
        private Panel pnlBottom;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedByText;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedDateValue;
        private DevComponents.DotNetBar.Controls.WarningBox lblPurchasestatus;
        private ButtonItem BtnHelp;
        private LinkLabel lnkLabel;
        private TabControlPanel tabControlPanel2;
        private PanelEx panelEx2;
        private ClsInnerGridBar dgvLocationDetails;
        private DataGridViewComboBoxColumn LLot;
        private DataGridViewComboBoxColumn LBlock;
        private DataGridViewComboBoxColumn LRow;
        private DataGridViewComboBoxColumn LLocation;
        private DataGridViewTextBoxColumn LUOMID;
        private DataGridViewComboBoxColumn LUOM;
        private DataGridViewTextBoxColumn LQuantity;
        private DataGridViewTextBoxColumn LBatchNo;
        private DataGridViewTextBoxColumn LBatchID;
        private DataGridViewTextBoxColumn LItemID;
        private DataGridViewTextBoxColumn LItemName;
        private DataGridViewTextBoxColumn LItemCode;
        private PanelEx panelGridBottom;
        private DevComponents.DotNetBar.Controls.TextBoxX txtRemarks;
        private Label lblRemarks;
        internal Button btnTContextmenu;
        private Label lblAmtinWrds;
        private Label lblAmountinWords;
        private PanelEx pnlAmount;
        private Label lblTotalAmount;
        private Label lblCompanyCurrencyAmount;
        private Label lblSubTotal;
        private DevComponents.DotNetBar.Controls.TextBoxX txtExchangeCurrencyRate;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSubTotal;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTotalAmount;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCurrency;
        private Label lblCurrency;
        private DataGridViewTextBoxColumn ItemID;
        private DataGridViewTextBoxColumn ItemCode;
        private DataGridViewTextBoxColumn ItemName;
        private DataGridViewTextBoxColumn Batchnumber;
        private DataGridViewTextBoxColumn Quantity;
        private DataGridViewTextBoxColumn ExtraQty;
        private DataGridViewComboBoxColumn Uom;
        private DataGridViewTextBoxColumn Rate;
        private DataGridViewTextBoxColumn NetAmount;
        private DemoClsDataGridview.CalendarColumn ExpiryDate;
        private DataGridViewTextBoxColumn SlNo;
        private DataGridViewTextBoxColumn BatchID;

        public bool blnDisableFunctionalities { get; set; }
        public bool blnIsFrmApproval;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDirectGRN));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            this.CommandZoom = new DevComponents.DotNetBar.Command(this.components);
            this.dockContainerItem1 = new DevComponents.DotNetBar.DockContainerItem();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvGRNDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelLeftTop = new DevComponents.DotNetBar.PanelEx();
            this.lnkLabel = new System.Windows.Forms.LinkLabel();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblSGRNNo = new System.Windows.Forms.Label();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cboSExecutive = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblExecutive = new System.Windows.Forms.Label();
            this.btnSRefresh = new DevComponents.DotNetBar.ButtonX();
            this.txtSGRNNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboSStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboSSupplier = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboSCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSCompany = new System.Windows.Forms.Label();
            this.lblSSupplier = new System.Windows.Forms.Label();
            this.LblSStatus = new System.Windows.Forms.Label();
            this.lblPurchaseCount = new DevComponents.DotNetBar.LabelX();
            this.tcPurchase = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvGRN = new ClsInnerGridBar();
            this.ItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Batchnumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExtraQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Uom = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpiryDate = new DemoClsDataGridview.CalendarColumn();
            this.SlNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnTVenderAddress = new DevComponents.DotNetBar.ButtonX();
            this.tpItemDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.tpLocationDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.dotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.dockSite4 = new DevComponents.DotNetBar.DockSite();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.dockSite2 = new DevComponents.DotNetBar.DockSite();
            this.dockSite8 = new DevComponents.DotNetBar.DockSite();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.dockSite6 = new DevComponents.DotNetBar.DockSite();
            this.dockSite3 = new DevComponents.DotNetBar.DockSite();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.panelBottom = new DevComponents.DotNetBar.PanelEx();
            this.panelGridBottom = new DevComponents.DotNetBar.PanelEx();
            this.pnlAmount = new DevComponents.DotNetBar.PanelEx();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.lblCompanyCurrencyAmount = new System.Windows.Forms.Label();
            this.lblSubTotal = new System.Windows.Forms.Label();
            this.txtExchangeCurrencyRate = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtSubTotal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtTotalAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblAmountinWords = new System.Windows.Forms.Label();
            this.lblAmtinWrds = new System.Windows.Forms.Label();
            this.txtRemarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.expandableSplitterTop = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.tcGeneral = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.cboCurrency = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.btnTContextmenu = new System.Windows.Forms.Button();
            this.lblPurchaseNo = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnAddressChange = new DevComponents.DotNetBar.ButtonX();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.lblSupplierAddressName = new System.Windows.Forms.Label();
            this.lblCompany = new System.Windows.Forms.Label();
            this.txtGRNNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboSupplier = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.btnWarehouse = new DevComponents.DotNetBar.ButtonX();
            this.txtDescription = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblDescription = new System.Windows.Forms.Label();
            this.btnSupplier = new DevComponents.DotNetBar.ButtonX();
            this.lblStatusText = new System.Windows.Forms.Label();
            this.txtSupplierAddress = new System.Windows.Forms.TextBox();
            this.lblSupplier = new System.Windows.Forms.Label();
            this.tiGeneral = new DevComponents.DotNetBar.TabItem(this.components);
            this.PurchaseOrderBindingNavigator = new DevComponents.DotNetBar.Bar();
            this.BindingNavigatorAddNewItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorSaveItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorClearItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorDeleteItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorCancelItem = new DevComponents.DotNetBar.ButtonItem();
            this.btnActions = new DevComponents.DotNetBar.ButtonItem();
            this.btnPurchaseOrder = new DevComponents.DotNetBar.ButtonItem();
            this.btnPurchaseInvoice = new DevComponents.DotNetBar.ButtonItem();
            this.BtnUom = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.BtnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.BtnHelp = new DevComponents.DotNetBar.ButtonItem();
            this.tiSuggestions = new DevComponents.DotNetBar.TabItem(this.components);
            this.CMSVendorAddress = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TmrPurchase = new System.Windows.Forms.Timer(this.components);
            this.TmrFocus = new System.Windows.Forms.Timer(this.components);
            this.ErrPurchase = new System.Windows.Forms.ErrorProvider(this.components);
            this.ImgPurchase = new System.Windows.Forms.ImageList(this.components);
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.lblCreatedByText = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblCreatedDateValue = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblPurchasestatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.LLot = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LBlock = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LRow = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LLocation = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LUOM = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calendarColumn1 = new DemoClsDataGridview.CalendarColumn();
            this.dgvLocationDetails = new ClsInnerGridBar();
            this.LUOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LBatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LBatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGRNDisplay)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcPurchase)).BeginInit();
            this.tcPurchase.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGRN)).BeginInit();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.panelEx1.SuspendLayout();
            this.panelBottom.SuspendLayout();
            this.panelGridBottom.SuspendLayout();
            this.pnlAmount.SuspendLayout();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).BeginInit();
            this.tcGeneral.SuspendLayout();
            this.tabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrPurchase)).BeginInit();
            this.pnlBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocationDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandZoom
            // 
            this.CommandZoom.Name = "CommandZoom";
            // 
            // dockContainerItem1
            // 
            this.dockContainerItem1.Name = "dockContainerItem1";
            this.dockContainerItem1.Text = "dockContainerItem1";
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.dgvGRNDisplay);
            this.PanelLeft.Controls.Add(this.expandablePanel1);
            this.PanelLeft.Controls.Add(this.lblPurchaseCount);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(200, 572);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 104;
            this.PanelLeft.Text = "panelEx1";
            this.PanelLeft.Visible = false;
            // 
            // dgvGRNDisplay
            // 
            this.dgvGRNDisplay.AllowUserToAddRows = false;
            this.dgvGRNDisplay.AllowUserToDeleteRows = false;
            this.dgvGRNDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvGRNDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvGRNDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGRNDisplay.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvGRNDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGRNDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvGRNDisplay.Location = new System.Drawing.Point(0, 226);
            this.dgvGRNDisplay.Name = "dgvGRNDisplay";
            this.dgvGRNDisplay.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvGRNDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvGRNDisplay.RowHeadersVisible = false;
            this.dgvGRNDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGRNDisplay.Size = new System.Drawing.Size(200, 320);
            this.dgvGRNDisplay.TabIndex = 47;
            this.dgvGRNDisplay.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGRNDisplay_CellDoubleClick);
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expandablePanel1.Controls.Add(this.panelLeftTop);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(200, 226);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 101;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Search";
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelLeftTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelLeftTop.Controls.Add(this.lnkLabel);
            this.panelLeftTop.Controls.Add(this.dtpSTo);
            this.panelLeftTop.Controls.Add(this.dtpSFrom);
            this.panelLeftTop.Controls.Add(this.lblSGRNNo);
            this.panelLeftTop.Controls.Add(this.lblTo);
            this.panelLeftTop.Controls.Add(this.lblFrom);
            this.panelLeftTop.Controls.Add(this.cboSExecutive);
            this.panelLeftTop.Controls.Add(this.lblExecutive);
            this.panelLeftTop.Controls.Add(this.btnSRefresh);
            this.panelLeftTop.Controls.Add(this.txtSGRNNo);
            this.panelLeftTop.Controls.Add(this.cboSStatus);
            this.panelLeftTop.Controls.Add(this.cboSSupplier);
            this.panelLeftTop.Controls.Add(this.cboSCompany);
            this.panelLeftTop.Controls.Add(this.lblSCompany);
            this.panelLeftTop.Controls.Add(this.lblSSupplier);
            this.panelLeftTop.Controls.Add(this.LblSStatus);
            this.panelLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftTop.Location = new System.Drawing.Point(0, 26);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(200, 200);
            this.panelLeftTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelLeftTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelLeftTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelLeftTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelLeftTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelLeftTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelLeftTop.Style.GradientAngle = 90;
            this.panelLeftTop.TabIndex = 38;
            // 
            // lnkLabel
            // 
            this.lnkLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lnkLabel.AutoSize = true;
            this.lnkLabel.Location = new System.Drawing.Point(1, 181);
            this.lnkLabel.Name = "lnkLabel";
            this.lnkLabel.Size = new System.Drawing.Size(87, 13);
            this.lnkLabel.TabIndex = 259;
            this.lnkLabel.TabStop = true;
            this.lnkLabel.Text = "Advance Search";
            this.lnkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLabel_LinkClicked);
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(72, 125);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.Size = new System.Drawing.Size(122, 20);
            this.dtpSTo.TabIndex = 44;
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(72, 102);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.Size = new System.Drawing.Size(122, 20);
            this.dtpSFrom.TabIndex = 43;
            // 
            // lblSGRNNo
            // 
            this.lblSGRNNo.AutoSize = true;
            this.lblSGRNNo.Location = new System.Drawing.Point(1, 151);
            this.lblSGRNNo.Name = "lblSGRNNo";
            this.lblSGRNNo.Size = new System.Drawing.Size(48, 13);
            this.lblSGRNNo.TabIndex = 245;
            this.lblSGRNNo.Text = "GRN No";
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(1, 128);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 244;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(1, 105);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 243;
            this.lblFrom.Text = "From";
            // 
            // cboSExecutive
            // 
            this.cboSExecutive.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSExecutive.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSExecutive.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSExecutive.DisplayMember = "Text";
            this.cboSExecutive.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSExecutive.DropDownHeight = 75;
            this.cboSExecutive.FormattingEnabled = true;
            this.cboSExecutive.IntegralHeight = false;
            this.cboSExecutive.ItemHeight = 14;
            this.cboSExecutive.Location = new System.Drawing.Point(72, 33);
            this.cboSExecutive.Name = "cboSExecutive";
            this.cboSExecutive.Size = new System.Drawing.Size(122, 20);
            this.cboSExecutive.TabIndex = 40;
            this.cboSExecutive.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboSExecutive_KeyPress);
            // 
            // lblExecutive
            // 
            this.lblExecutive.AutoSize = true;
            this.lblExecutive.Location = new System.Drawing.Point(1, 36);
            this.lblExecutive.Name = "lblExecutive";
            this.lblExecutive.Size = new System.Drawing.Size(54, 13);
            this.lblExecutive.TabIndex = 124;
            this.lblExecutive.Text = "Executive";
            // 
            // btnSRefresh
            // 
            this.btnSRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSRefresh.Location = new System.Drawing.Point(123, 173);
            this.btnSRefresh.Name = "btnSRefresh";
            this.btnSRefresh.Size = new System.Drawing.Size(71, 23);
            this.btnSRefresh.TabIndex = 46;
            this.btnSRefresh.Text = "Refresh";
            this.btnSRefresh.Click += new System.EventHandler(this.btnSRefresh_Click);
            // 
            // txtSGRNNo
            // 
            this.txtSGRNNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtSGRNNo.Border.Class = "TextBoxBorder";
            this.txtSGRNNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSGRNNo.Location = new System.Drawing.Point(72, 148);
            this.txtSGRNNo.MaxLength = 20;
            this.txtSGRNNo.Name = "txtSGRNNo";
            this.txtSGRNNo.Size = new System.Drawing.Size(122, 20);
            this.txtSGRNNo.TabIndex = 45;
            this.txtSGRNNo.WatermarkEnabled = false;
            this.txtSGRNNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtSsearch_KeyPress);
            // 
            // cboSStatus
            // 
            this.cboSStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSStatus.DisplayMember = "Text";
            this.cboSStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSStatus.DropDownHeight = 75;
            this.cboSStatus.FormattingEnabled = true;
            this.cboSStatus.IntegralHeight = false;
            this.cboSStatus.ItemHeight = 14;
            this.cboSStatus.Location = new System.Drawing.Point(72, 79);
            this.cboSStatus.Name = "cboSStatus";
            this.cboSStatus.Size = new System.Drawing.Size(122, 20);
            this.cboSStatus.TabIndex = 42;
            this.cboSStatus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboSStatus_KeyPress);
            // 
            // cboSSupplier
            // 
            this.cboSSupplier.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSSupplier.DisplayMember = "Text";
            this.cboSSupplier.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSSupplier.DropDownHeight = 75;
            this.cboSSupplier.FormattingEnabled = true;
            this.cboSSupplier.IntegralHeight = false;
            this.cboSSupplier.ItemHeight = 14;
            this.cboSSupplier.Location = new System.Drawing.Point(72, 56);
            this.cboSSupplier.Name = "cboSSupplier";
            this.cboSSupplier.Size = new System.Drawing.Size(122, 20);
            this.cboSSupplier.TabIndex = 41;
            this.cboSSupplier.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboSSupplier_KeyPress);
            // 
            // cboSCompany
            // 
            this.cboSCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSCompany.DisplayMember = "Text";
            this.cboSCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSCompany.DropDownHeight = 75;
            this.cboSCompany.FormattingEnabled = true;
            this.cboSCompany.IntegralHeight = false;
            this.cboSCompany.ItemHeight = 14;
            this.cboSCompany.Location = new System.Drawing.Point(72, 10);
            this.cboSCompany.Name = "cboSCompany";
            this.cboSCompany.Size = new System.Drawing.Size(122, 20);
            this.cboSCompany.TabIndex = 39;
            this.cboSCompany.SelectedIndexChanged += new System.EventHandler(this.cboSCompany_SelectedIndexChanged);
            this.cboSCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboSCompany_KeyPress);
            // 
            // lblSCompany
            // 
            this.lblSCompany.AutoSize = true;
            this.lblSCompany.Location = new System.Drawing.Point(1, 13);
            this.lblSCompany.Name = "lblSCompany";
            this.lblSCompany.Size = new System.Drawing.Size(51, 13);
            this.lblSCompany.TabIndex = 109;
            this.lblSCompany.Text = "Company";
            // 
            // lblSSupplier
            // 
            this.lblSSupplier.AutoSize = true;
            this.lblSSupplier.Location = new System.Drawing.Point(1, 59);
            this.lblSSupplier.Name = "lblSSupplier";
            this.lblSSupplier.Size = new System.Drawing.Size(45, 13);
            this.lblSSupplier.TabIndex = 108;
            this.lblSSupplier.Text = "Supplier";
            // 
            // LblSStatus
            // 
            this.LblSStatus.AutoSize = true;
            this.LblSStatus.Location = new System.Drawing.Point(1, 82);
            this.LblSStatus.Name = "LblSStatus";
            this.LblSStatus.Size = new System.Drawing.Size(37, 13);
            this.LblSStatus.TabIndex = 107;
            this.LblSStatus.Text = "Status";
            // 
            // lblPurchaseCount
            // 
            // 
            // 
            // 
            this.lblPurchaseCount.BackgroundStyle.Class = "";
            this.lblPurchaseCount.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPurchaseCount.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblPurchaseCount.Location = new System.Drawing.Point(0, 546);
            this.lblPurchaseCount.Name = "lblPurchaseCount";
            this.lblPurchaseCount.Size = new System.Drawing.Size(200, 26);
            this.lblPurchaseCount.TabIndex = 105;
            this.lblPurchaseCount.Text = "...";
            // 
            // tcPurchase
            // 
            this.tcPurchase.BackColor = System.Drawing.Color.Transparent;
            this.tcPurchase.CanReorderTabs = true;
            this.tcPurchase.Controls.Add(this.tabControlPanel1);
            this.tcPurchase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcPurchase.Location = new System.Drawing.Point(0, 0);
            this.tcPurchase.Name = "tcPurchase";
            this.tcPurchase.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcPurchase.SelectedTabIndex = 0;
            this.tcPurchase.Size = new System.Drawing.Size(1273, 275);
            this.tcPurchase.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcPurchase.TabIndex = 18;
            this.tcPurchase.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcPurchase.Tabs.Add(this.tpItemDetails);
            this.tcPurchase.TabStop = false;
            this.tcPurchase.Text = "tabControl1";
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.dgvGRN);
            this.tabControlPanel1.Controls.Add(this.BtnTVenderAddress);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1273, 253);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 19;
            this.tabControlPanel1.TabItem = this.tpItemDetails;
            // 
            // dgvGRN
            // 
            this.dgvGRN.AddNewRow = false;
            this.dgvGRN.AlphaNumericCols = new int[0];
            this.dgvGRN.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvGRN.BackgroundColor = System.Drawing.Color.White;
            this.dgvGRN.CapsLockCols = new int[0];
            this.dgvGRN.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGRN.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemID,
            this.ItemCode,
            this.ItemName,
            this.Batchnumber,
            this.Quantity,
            this.ExtraQty,
            this.Uom,
            this.Rate,
            this.NetAmount,
            this.ExpiryDate,
            this.SlNo,
            this.BatchID});
            this.dgvGRN.DecimalCols = new int[0];
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGRN.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvGRN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGRN.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvGRN.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvGRN.HasSlNo = false;
            this.dgvGRN.LastRowIndex = 0;
            this.dgvGRN.Location = new System.Drawing.Point(1, 1);
            this.dgvGRN.Name = "dgvGRN";
            this.dgvGRN.NegativeValueCols = new int[0];
            this.dgvGRN.NumericCols = new int[0];
            this.dgvGRN.RowHeadersWidth = 50;
            this.dgvGRN.Size = new System.Drawing.Size(1271, 251);
            this.dgvGRN.TabIndex = 20;
            this.dgvGRN.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGRN_CellValueChanged);
            this.dgvGRN.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvGRN_CellBeginEdit);
            this.dgvGRN.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvGRN_RowsAdded);
            this.dgvGRN.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGRN_CellEndEdit);
            this.dgvGRN.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvGRN_Textbox_TextChanged);
            this.dgvGRN.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvGRN_EditingControlShowing);
            this.dgvGRN.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvGRN_CurrentCellDirtyStateChanged);
            this.dgvGRN.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGRN_CellEnter);
            this.dgvGRN.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvGRN_RowsRemoved);
            // 
            // ItemID
            // 
            this.ItemID.HeaderText = "ItemID";
            this.ItemID.Name = "ItemID";
            this.ItemID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemID.Visible = false;
            // 
            // ItemCode
            // 
            this.ItemCode.FillWeight = 19.72055F;
            this.ItemCode.HeaderText = "Item Code";
            this.ItemCode.MaxInputLength = 20;
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ItemName
            // 
            this.ItemName.FillWeight = 55.21755F;
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.MaxInputLength = 200;
            this.ItemName.Name = "ItemName";
            this.ItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Batchnumber
            // 
            this.Batchnumber.FillWeight = 34.00933F;
            this.Batchnumber.HeaderText = "Batch No";
            this.Batchnumber.MaxInputLength = 50;
            this.Batchnumber.Name = "Batchnumber";
            this.Batchnumber.ReadOnly = true;
            this.Batchnumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Batchnumber.Visible = false;
            // 
            // Quantity
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle4.Format = "N3";
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle4;
            this.Quantity.FillWeight = 25F;
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.MaxInputLength = 10;
            this.Quantity.Name = "Quantity";
            this.Quantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ExtraQty
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.ExtraQty.DefaultCellStyle = dataGridViewCellStyle5;
            this.ExtraQty.FillWeight = 25F;
            this.ExtraQty.HeaderText = "Extra Quantity";
            this.ExtraQty.MaxInputLength = 10;
            this.ExtraQty.Name = "ExtraQty";
            // 
            // Uom
            // 
            this.Uom.FillWeight = 23.66466F;
            this.Uom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Uom.HeaderText = "UOM";
            this.Uom.Name = "Uom";
            this.Uom.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Rate
            // 
            this.Rate.FillWeight = 25F;
            this.Rate.HeaderText = "Rate";
            this.Rate.MaxInputLength = 10;
            this.Rate.Name = "Rate";
            this.Rate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // NetAmount
            // 
            this.NetAmount.FillWeight = 30F;
            this.NetAmount.HeaderText = "NetAmount";
            this.NetAmount.MaxInputLength = 13;
            this.NetAmount.Name = "NetAmount";
            this.NetAmount.ReadOnly = true;
            this.NetAmount.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ExpiryDate
            // 
            this.ExpiryDate.FillWeight = 19.72055F;
            this.ExpiryDate.HeaderText = "Expiry Date";
            this.ExpiryDate.Name = "ExpiryDate";
            this.ExpiryDate.Visible = false;
            // 
            // SlNo
            // 
            this.SlNo.HeaderText = "SlNo";
            this.SlNo.Name = "SlNo";
            this.SlNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SlNo.Visible = false;
            // 
            // BatchID
            // 
            this.BatchID.HeaderText = "BatchID";
            this.BatchID.Name = "BatchID";
            this.BatchID.Visible = false;
            // 
            // BtnTVenderAddress
            // 
            this.BtnTVenderAddress.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnTVenderAddress.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnTVenderAddress.Location = new System.Drawing.Point(399, 84);
            this.BtnTVenderAddress.Name = "BtnTVenderAddress";
            this.BtnTVenderAddress.Size = new System.Drawing.Size(24, 23);
            this.BtnTVenderAddress.TabIndex = 10;
            this.BtnTVenderAddress.TabStop = false;
            this.BtnTVenderAddress.Text = "Permanent";
            this.BtnTVenderAddress.Visible = false;
            // 
            // tpItemDetails
            // 
            this.tpItemDetails.AttachedControl = this.tabControlPanel1;
            this.tpItemDetails.Name = "tpItemDetails";
            this.tpItemDetails.Text = "Item Details";
            // 
            // tpLocationDetails
            // 
            this.tpLocationDetails.AttachedControl = this.tabControlPanel2;
            this.tpLocationDetails.Name = "tpLocationDetails";
            this.tpLocationDetails.Text = "Location Details";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.panelEx2);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(1273, 253);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            this.tabControlPanel2.TabItem = this.tpLocationDetails;
            // 
            // panelEx2
            // 
            this.panelEx2.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx2.Location = new System.Drawing.Point(1, 1);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(1271, 251);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 1;
            this.panelEx2.Text = "panelEx2";
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.Expanded = false;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(0, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 572);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 119;
            this.expandableSplitterLeft.TabStop = false;
            this.expandableSplitterLeft.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitterLeft_ExpandedChanged);
            // 
            // dotNetBarManager1
            // 
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.dotNetBarManager1.BottomDockSite = this.dockSite4;
            this.dotNetBarManager1.EnableFullSizeDock = false;
            this.dotNetBarManager1.LeftDockSite = this.dockSite1;
            this.dotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.dotNetBarManager1.ParentForm = this;
            this.dotNetBarManager1.RightDockSite = this.dockSite2;
            this.dotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.dotNetBarManager1.ToolbarBottomDockSite = this.dockSite8;
            this.dotNetBarManager1.ToolbarLeftDockSite = this.dockSite5;
            this.dotNetBarManager1.ToolbarRightDockSite = this.dockSite6;
            this.dotNetBarManager1.TopDockSite = this.dockSite3;
            // 
            // dockSite4
            // 
            this.dockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite4.Location = new System.Drawing.Point(0, 572);
            this.dockSite4.Name = "dockSite4";
            this.dockSite4.Size = new System.Drawing.Size(1276, 0);
            this.dockSite4.TabIndex = 114;
            this.dockSite4.TabStop = false;
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(3, 0);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 572);
            this.dockSite1.TabIndex = 111;
            this.dockSite1.TabStop = false;
            // 
            // dockSite2
            // 
            this.dockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite2.Location = new System.Drawing.Point(1276, 0);
            this.dockSite2.Name = "dockSite2";
            this.dockSite2.Size = new System.Drawing.Size(0, 572);
            this.dockSite2.TabIndex = 112;
            this.dockSite2.TabStop = false;
            // 
            // dockSite8
            // 
            this.dockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite8.Location = new System.Drawing.Point(0, 572);
            this.dockSite8.Name = "dockSite8";
            this.dockSite8.Size = new System.Drawing.Size(1276, 0);
            this.dockSite8.TabIndex = 118;
            this.dockSite8.TabStop = false;
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(0, 0);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 572);
            this.dockSite5.TabIndex = 115;
            this.dockSite5.TabStop = false;
            // 
            // dockSite6
            // 
            this.dockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite6.Location = new System.Drawing.Point(1276, 0);
            this.dockSite6.Name = "dockSite6";
            this.dockSite6.Size = new System.Drawing.Size(0, 572);
            this.dockSite6.TabIndex = 116;
            this.dockSite6.TabStop = false;
            // 
            // dockSite3
            // 
            this.dockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite3.Location = new System.Drawing.Point(0, 0);
            this.dockSite3.Name = "dockSite3";
            this.dockSite3.Size = new System.Drawing.Size(1276, 0);
            this.dockSite3.TabIndex = 113;
            this.dockSite3.TabStop = false;
            // 
            // bar1
            // 
            this.bar1.AccessibleDescription = "DotNetBar Bar (bar1)";
            this.bar1.AccessibleName = "DotNetBar Bar";
            this.bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.bar1.Location = new System.Drawing.Point(0, 0);
            this.bar1.MenuBar = true;
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(36, 24);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.bar1.TabIndex = 0;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx1.Controls.Add(this.panelBottom);
            this.panelEx1.Controls.Add(this.expandableSplitterTop);
            this.panelEx1.Controls.Add(this.panelTop);
            this.panelEx1.Controls.Add(this.PurchaseOrderBindingNavigator);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(3, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(1273, 572);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 100;
            this.panelEx1.Text = "panelEx1";
            // 
            // panelBottom
            // 
            this.panelBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelBottom.Controls.Add(this.tcPurchase);
            this.panelBottom.Controls.Add(this.panelGridBottom);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBottom.Location = new System.Drawing.Point(0, 161);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1273, 411);
            this.panelBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelBottom.Style.GradientAngle = 90;
            this.panelBottom.TabIndex = 103;
            this.panelBottom.Text = "panelEx3";
            // 
            // panelGridBottom
            // 
            this.panelGridBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelGridBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelGridBottom.Controls.Add(this.pnlAmount);
            this.panelGridBottom.Controls.Add(this.lblAmountinWords);
            this.panelGridBottom.Controls.Add(this.lblAmtinWrds);
            this.panelGridBottom.Controls.Add(this.txtRemarks);
            this.panelGridBottom.Controls.Add(this.lblRemarks);
            this.panelGridBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGridBottom.Location = new System.Drawing.Point(0, 275);
            this.panelGridBottom.Name = "panelGridBottom";
            this.panelGridBottom.Size = new System.Drawing.Size(1273, 136);
            this.panelGridBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelGridBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelGridBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelGridBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelGridBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelGridBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelGridBottom.Style.GradientAngle = 90;
            this.panelGridBottom.TabIndex = 102;
            // 
            // pnlAmount
            // 
            this.pnlAmount.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlAmount.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.pnlAmount.Controls.Add(this.lblTotalAmount);
            this.pnlAmount.Controls.Add(this.lblCompanyCurrencyAmount);
            this.pnlAmount.Controls.Add(this.lblSubTotal);
            this.pnlAmount.Controls.Add(this.txtExchangeCurrencyRate);
            this.pnlAmount.Controls.Add(this.txtSubTotal);
            this.pnlAmount.Controls.Add(this.txtTotalAmount);
            this.pnlAmount.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlAmount.Location = new System.Drawing.Point(838, 0);
            this.pnlAmount.Name = "pnlAmount";
            this.pnlAmount.Size = new System.Drawing.Size(435, 136);
            this.pnlAmount.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlAmount.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlAmount.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlAmount.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlAmount.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlAmount.Style.GradientAngle = 90;
            this.pnlAmount.TabIndex = 306;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.BackColor = System.Drawing.Color.White;
            this.lblTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(217, 96);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(45, 9);
            this.lblTotalAmount.TabIndex = 299;
            this.lblTotalAmount.Text = "Net Amount";
            // 
            // lblCompanyCurrencyAmount
            // 
            this.lblCompanyCurrencyAmount.AutoSize = true;
            this.lblCompanyCurrencyAmount.BackColor = System.Drawing.Color.White;
            this.lblCompanyCurrencyAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompanyCurrencyAmount.Location = new System.Drawing.Point(7, 95);
            this.lblCompanyCurrencyAmount.Name = "lblCompanyCurrencyAmount";
            this.lblCompanyCurrencyAmount.Size = new System.Drawing.Size(104, 9);
            this.lblCompanyCurrencyAmount.TabIndex = 302;
            this.lblCompanyCurrencyAmount.Text = "Amount in company currency";
            // 
            // lblSubTotal
            // 
            this.lblSubTotal.AutoSize = true;
            this.lblSubTotal.BackColor = System.Drawing.Color.White;
            this.lblSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubTotal.Location = new System.Drawing.Point(217, 54);
            this.lblSubTotal.Name = "lblSubTotal";
            this.lblSubTotal.Size = new System.Drawing.Size(37, 9);
            this.lblSubTotal.TabIndex = 296;
            this.lblSubTotal.Text = "Sub Total";
            // 
            // txtExchangeCurrencyRate
            // 
            this.txtExchangeCurrencyRate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtExchangeCurrencyRate.Border.Class = "TextBoxBorder";
            this.txtExchangeCurrencyRate.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtExchangeCurrencyRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExchangeCurrencyRate.Location = new System.Drawing.Point(5, 67);
            this.txtExchangeCurrencyRate.Name = "txtExchangeCurrencyRate";
            this.txtExchangeCurrencyRate.ReadOnly = true;
            this.txtExchangeCurrencyRate.Size = new System.Drawing.Size(204, 38);
            this.txtExchangeCurrencyRate.TabIndex = 36;
            this.txtExchangeCurrencyRate.TabStop = false;
            this.txtExchangeCurrencyRate.Text = "0.00";
            this.txtExchangeCurrencyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtSubTotal.Border.Class = "TextBoxBorder";
            this.txtSubTotal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubTotal.Location = new System.Drawing.Point(215, 45);
            this.txtSubTotal.MaxLength = 20;
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.ReadOnly = true;
            this.txtSubTotal.Size = new System.Drawing.Size(194, 20);
            this.txtSubTotal.TabIndex = 32;
            this.txtSubTotal.TabStop = false;
            this.txtSubTotal.Text = "0.00";
            this.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTotalAmount.Border.Class = "TextBoxBorder";
            this.txtTotalAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(215, 69);
            this.txtTotalAmount.MaxLength = 24;
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.Size = new System.Drawing.Size(194, 38);
            this.txtTotalAmount.TabIndex = 37;
            this.txtTotalAmount.TabStop = false;
            this.txtTotalAmount.Text = "0.00";
            this.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblAmountinWords
            // 
            this.lblAmountinWords.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountinWords.Location = new System.Drawing.Point(109, 85);
            this.lblAmountinWords.Name = "lblAmountinWords";
            this.lblAmountinWords.Size = new System.Drawing.Size(638, 16);
            this.lblAmountinWords.TabIndex = 305;
            // 
            // lblAmtinWrds
            // 
            this.lblAmtinWrds.AutoSize = true;
            this.lblAmtinWrds.Location = new System.Drawing.Point(15, 85);
            this.lblAmtinWrds.Name = "lblAmtinWrds";
            this.lblAmtinWrds.Size = new System.Drawing.Size(88, 13);
            this.lblAmtinWrds.TabIndex = 294;
            this.lblAmtinWrds.Text = "Amount in Words";
            // 
            // txtRemarks
            // 
            // 
            // 
            // 
            this.txtRemarks.Border.Class = "TextBoxBorder";
            this.txtRemarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtRemarks.Location = new System.Drawing.Point(109, 21);
            this.txtRemarks.MaxLength = 1000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(297, 50);
            this.txtRemarks.TabIndex = 22;
            this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(15, 21);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 36;
            this.lblRemarks.Text = "Remarks";
            // 
            // expandableSplitterTop
            // 
            this.expandableSplitterTop.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterTop.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitterTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitterTop.ExpandableControl = this.panelTop;
            this.expandableSplitterTop.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterTop.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterTop.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterTop.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterTop.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.Location = new System.Drawing.Point(0, 158);
            this.expandableSplitterTop.Name = "expandableSplitterTop";
            this.expandableSplitterTop.Size = new System.Drawing.Size(1273, 3);
            this.expandableSplitterTop.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterTop.TabIndex = 120;
            this.expandableSplitterTop.TabStop = false;
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelTop.Controls.Add(this.tcGeneral);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1273, 133);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 0;
            // 
            // tcGeneral
            // 
            this.tcGeneral.BackColor = System.Drawing.Color.Transparent;
            this.tcGeneral.CanReorderTabs = true;
            this.tcGeneral.Controls.Add(this.tabControlPanel3);
            this.tcGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcGeneral.Location = new System.Drawing.Point(0, 0);
            this.tcGeneral.Name = "tcGeneral";
            this.tcGeneral.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcGeneral.SelectedTabIndex = 0;
            this.tcGeneral.Size = new System.Drawing.Size(1273, 133);
            this.tcGeneral.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcGeneral.TabIndex = 0;
            this.tcGeneral.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcGeneral.Tabs.Add(this.tiGeneral);
            this.tcGeneral.TabStop = false;
            this.tcGeneral.Text = "tabControl1";
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel3.Controls.Add(this.cboCurrency);
            this.tabControlPanel3.Controls.Add(this.lblCurrency);
            this.tabControlPanel3.Controls.Add(this.btnTContextmenu);
            this.tabControlPanel3.Controls.Add(this.lblPurchaseNo);
            this.tabControlPanel3.Controls.Add(this.lblDate);
            this.tabControlPanel3.Controls.Add(this.lblStatus);
            this.tabControlPanel3.Controls.Add(this.btnAddressChange);
            this.tabControlPanel3.Controls.Add(this.dtpDate);
            this.tabControlPanel3.Controls.Add(this.lblSupplierAddressName);
            this.tabControlPanel3.Controls.Add(this.lblCompany);
            this.tabControlPanel3.Controls.Add(this.txtGRNNo);
            this.tabControlPanel3.Controls.Add(this.cboSupplier);
            this.tabControlPanel3.Controls.Add(this.cboWarehouse);
            this.tabControlPanel3.Controls.Add(this.lblWarehouse);
            this.tabControlPanel3.Controls.Add(this.btnWarehouse);
            this.tabControlPanel3.Controls.Add(this.txtDescription);
            this.tabControlPanel3.Controls.Add(this.cboCompany);
            this.tabControlPanel3.Controls.Add(this.lblDescription);
            this.tabControlPanel3.Controls.Add(this.btnSupplier);
            this.tabControlPanel3.Controls.Add(this.lblStatusText);
            this.tabControlPanel3.Controls.Add(this.txtSupplierAddress);
            this.tabControlPanel3.Controls.Add(this.lblSupplier);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(1273, 111);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 1;
            this.tabControlPanel3.TabItem = this.tiGeneral;
            // 
            // cboCurrency
            // 
            this.cboCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCurrency.DisplayMember = "Text";
            this.cboCurrency.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCurrency.DropDownHeight = 134;
            this.cboCurrency.Enabled = false;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.IntegralHeight = false;
            this.cboCurrency.ItemHeight = 14;
            this.cboCurrency.Location = new System.Drawing.Point(81, 60);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(190, 20);
            this.cboCurrency.TabIndex = 256;
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrency.Location = new System.Drawing.Point(15, 62);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(49, 13);
            this.lblCurrency.TabIndex = 257;
            this.lblCurrency.Text = "Currency";
            // 
            // btnTContextmenu
            // 
            this.btnTContextmenu.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnTContextmenu.Location = new System.Drawing.Point(626, 44);
            this.btnTContextmenu.Name = "btnTContextmenu";
            this.btnTContextmenu.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.btnTContextmenu.Size = new System.Drawing.Size(20, 23);
            this.btnTContextmenu.TabIndex = 255;
            this.btnTContextmenu.Text = "6";
            this.btnTContextmenu.UseVisualStyleBackColor = true;
            this.btnTContextmenu.Visible = false;
            this.btnTContextmenu.Click += new System.EventHandler(this.btnTContextmenu_Click);
            // 
            // lblPurchaseNo
            // 
            this.lblPurchaseNo.AutoSize = true;
            this.lblPurchaseNo.BackColor = System.Drawing.Color.Transparent;
            this.lblPurchaseNo.Location = new System.Drawing.Point(606, 11);
            this.lblPurchaseNo.Name = "lblPurchaseNo";
            this.lblPurchaseNo.Size = new System.Drawing.Size(71, 13);
            this.lblPurchaseNo.TabIndex = 13;
            this.lblPurchaseNo.Text = "GRN Number";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Location = new System.Drawing.Point(606, 36);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 188;
            this.lblDate.Text = "Date";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Location = new System.Drawing.Point(825, 12);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 191;
            this.lblStatus.Text = "Status";
            // 
            // btnAddressChange
            // 
            this.btnAddressChange.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddressChange.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddressChange.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnAddressChange.Location = new System.Drawing.Point(550, 34);
            this.btnAddressChange.Name = "btnAddressChange";
            this.btnAddressChange.Size = new System.Drawing.Size(25, 20);
            this.btnAddressChange.TabIndex = 12;
            this.btnAddressChange.Text = "6";
            this.btnAddressChange.Click += new System.EventHandler(this.btnAddressChange_Click);
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(691, 33);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(103, 20);
            this.dtpDate.TabIndex = 14;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // lblSupplierAddressName
            // 
            this.lblSupplierAddressName.AutoSize = true;
            this.lblSupplierAddressName.BackColor = System.Drawing.Color.White;
            this.lblSupplierAddressName.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSupplierAddressName.Location = new System.Drawing.Point(361, 38);
            this.lblSupplierAddressName.Name = "lblSupplierAddressName";
            this.lblSupplierAddressName.Size = new System.Drawing.Size(33, 9);
            this.lblSupplierAddressName.TabIndex = 254;
            this.lblSupplierAddressName.Text = "Address";
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.BackColor = System.Drawing.Color.Transparent;
            this.lblCompany.Location = new System.Drawing.Point(15, 13);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 200;
            this.lblCompany.Text = "Company";
            // 
            // txtGRNNo
            // 
            this.txtGRNNo.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtGRNNo.Border.Class = "TextBoxBorder";
            this.txtGRNNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGRNNo.Location = new System.Drawing.Point(691, 8);
            this.txtGRNNo.MaxLength = 20;
            this.txtGRNNo.Name = "txtGRNNo";
            this.txtGRNNo.Size = new System.Drawing.Size(103, 20);
            this.txtGRNNo.TabIndex = 13;
            this.txtGRNNo.TabStop = false;
            this.txtGRNNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPurchaseNo_KeyPress);
            // 
            // cboSupplier
            // 
            this.cboSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSupplier.DisplayMember = "Text";
            this.cboSupplier.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSupplier.DropDownHeight = 134;
            this.cboSupplier.FormattingEnabled = true;
            this.cboSupplier.IntegralHeight = false;
            this.cboSupplier.ItemHeight = 14;
            this.cboSupplier.Location = new System.Drawing.Point(355, 9);
            this.cboSupplier.Name = "cboSupplier";
            this.cboSupplier.Size = new System.Drawing.Size(190, 20);
            this.cboSupplier.TabIndex = 9;
            this.cboSupplier.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSupplier.WatermarkText = "Select Supplier";
            this.cboSupplier.SelectedIndexChanged += new System.EventHandler(this.cboSupplier_SelectedIndexChanged);
            this.cboSupplier.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // cboWarehouse
            // 
            this.cboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWarehouse.DisplayMember = "Text";
            this.cboWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWarehouse.DropDownHeight = 134;
            this.cboWarehouse.FormattingEnabled = true;
            this.cboWarehouse.IntegralHeight = false;
            this.cboWarehouse.ItemHeight = 14;
            this.cboWarehouse.Location = new System.Drawing.Point(81, 34);
            this.cboWarehouse.Name = "cboWarehouse";
            this.cboWarehouse.Size = new System.Drawing.Size(190, 20);
            this.cboWarehouse.TabIndex = 5;
            this.cboWarehouse.SelectedIndexChanged += new System.EventHandler(this.cboWarehouse_SelectedIndexChanged);
            this.cboWarehouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.BackColor = System.Drawing.Color.Transparent;
            this.lblWarehouse.Location = new System.Drawing.Point(15, 36);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(62, 13);
            this.lblWarehouse.TabIndex = 203;
            this.lblWarehouse.Text = "Warehouse";
            // 
            // btnWarehouse
            // 
            this.btnWarehouse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnWarehouse.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnWarehouse.Location = new System.Drawing.Point(273, 36);
            this.btnWarehouse.Name = "btnWarehouse";
            this.btnWarehouse.Size = new System.Drawing.Size(25, 20);
            this.btnWarehouse.TabIndex = 6;
            this.btnWarehouse.Text = "...";
            this.btnWarehouse.Visible = false;
            this.btnWarehouse.Click += new System.EventHandler(this.btnWarehouse_Click);
            // 
            // txtDescription
            // 
            // 
            // 
            // 
            this.txtDescription.Border.Class = "TextBoxBorder";
            this.txtDescription.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDescription.Location = new System.Drawing.Point(893, 36);
            this.txtDescription.MaxLength = 200;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(200, 45);
            this.txtDescription.TabIndex = 17;
            this.txtDescription.TabStop = false;
            this.txtDescription.Visible = false;
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(81, 9);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(190, 20);
            this.cboCompany.TabIndex = 2;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblDescription.Location = new System.Drawing.Point(825, 38);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 13);
            this.lblDescription.TabIndex = 244;
            this.lblDescription.Text = "Description";
            this.lblDescription.Visible = false;
            // 
            // btnSupplier
            // 
            this.btnSupplier.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSupplier.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSupplier.Location = new System.Drawing.Point(550, 9);
            this.btnSupplier.Name = "btnSupplier";
            this.btnSupplier.Size = new System.Drawing.Size(25, 20);
            this.btnSupplier.TabIndex = 10;
            this.btnSupplier.Text = "...";
            this.btnSupplier.Click += new System.EventHandler(this.btnSupplier_Click);
            // 
            // lblStatusText
            // 
            this.lblStatusText.AutoSize = true;
            this.lblStatusText.BackColor = System.Drawing.Color.Transparent;
            this.lblStatusText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.lblStatusText.Location = new System.Drawing.Point(893, 8);
            this.lblStatusText.Name = "lblStatusText";
            this.lblStatusText.Size = new System.Drawing.Size(62, 20);
            this.lblStatusText.TabIndex = 19;
            this.lblStatusText.Text = "Status";
            // 
            // txtSupplierAddress
            // 
            this.txtSupplierAddress.BackColor = System.Drawing.Color.White;
            this.txtSupplierAddress.Location = new System.Drawing.Point(355, 34);
            this.txtSupplierAddress.Multiline = true;
            this.txtSupplierAddress.Name = "txtSupplierAddress";
            this.txtSupplierAddress.ReadOnly = true;
            this.txtSupplierAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtSupplierAddress.Size = new System.Drawing.Size(190, 70);
            this.txtSupplierAddress.TabIndex = 11;
            this.txtSupplierAddress.TabStop = false;
            this.txtSupplierAddress.TextChanged += new System.EventHandler(this.TxtTVendorAddress_TextChanged);
            // 
            // lblSupplier
            // 
            this.lblSupplier.AutoSize = true;
            this.lblSupplier.BackColor = System.Drawing.Color.Transparent;
            this.lblSupplier.Location = new System.Drawing.Point(303, 13);
            this.lblSupplier.Name = "lblSupplier";
            this.lblSupplier.Size = new System.Drawing.Size(45, 13);
            this.lblSupplier.TabIndex = 252;
            this.lblSupplier.Text = "Supplier";
            // 
            // tiGeneral
            // 
            this.tiGeneral.AttachedControl = this.tabControlPanel3;
            this.tiGeneral.Name = "tiGeneral";
            this.tiGeneral.Text = "General";
            // 
            // PurchaseOrderBindingNavigator
            // 
            this.PurchaseOrderBindingNavigator.AccessibleDescription = "DotNetBar Bar (PurchaseOrderBindingNavigator)";
            this.PurchaseOrderBindingNavigator.AccessibleName = "DotNetBar Bar";
            this.PurchaseOrderBindingNavigator.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.PurchaseOrderBindingNavigator.Dock = System.Windows.Forms.DockStyle.Top;
            this.PurchaseOrderBindingNavigator.DockLine = 1;
            this.PurchaseOrderBindingNavigator.DockOffset = 73;
            this.PurchaseOrderBindingNavigator.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.PurchaseOrderBindingNavigator.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.PurchaseOrderBindingNavigator.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorClearItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorCancelItem,
            this.btnActions,
            this.BtnUom,
            this.BtnPrint,
            this.BtnEmail,
            this.BtnHelp});
            this.PurchaseOrderBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.PurchaseOrderBindingNavigator.Name = "PurchaseOrderBindingNavigator";
            this.PurchaseOrderBindingNavigator.Size = new System.Drawing.Size(1273, 25);
            this.PurchaseOrderBindingNavigator.Stretch = true;
            this.PurchaseOrderBindingNavigator.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PurchaseOrderBindingNavigator.TabIndex = 12;
            this.PurchaseOrderBindingNavigator.TabStop = false;
            this.PurchaseOrderBindingNavigator.Text = "bar2";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.Tooltip = "Add New Information";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Tooltip = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorClearItem
            // 
            this.BindingNavigatorClearItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorClearItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorClearItem.Image")));
            this.BindingNavigatorClearItem.Name = "BindingNavigatorClearItem";
            this.BindingNavigatorClearItem.Text = "Clear";
            this.BindingNavigatorClearItem.Tooltip = "Clear";
            this.BindingNavigatorClearItem.Visible = false;
            this.BindingNavigatorClearItem.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Tooltip = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorCancelItem
            // 
            this.BindingNavigatorCancelItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorCancelItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorCancelItem.Image")));
            this.BindingNavigatorCancelItem.Name = "BindingNavigatorCancelItem";
            this.BindingNavigatorCancelItem.Text = "Cancel";
            this.BindingNavigatorCancelItem.Tooltip = "Cancel";
            this.BindingNavigatorCancelItem.Click += new System.EventHandler(this.BindingNavigatorCancelItem_Click);
            // 
            // btnActions
            // 
            this.btnActions.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnActions.Image = ((System.Drawing.Image)(resources.GetObject("btnActions.Image")));
            this.btnActions.Name = "btnActions";
            this.btnActions.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnPurchaseOrder,
            this.btnPurchaseInvoice});
            this.btnActions.Text = "Actions";
            this.btnActions.Tooltip = "Actions";
            // 
            // btnPurchaseOrder
            // 
            this.btnPurchaseOrder.Image = ((System.Drawing.Image)(resources.GetObject("btnPurchaseOrder.Image")));
            this.btnPurchaseOrder.Name = "btnPurchaseOrder";
            this.btnPurchaseOrder.Text = "Create Purchase Order";
            this.btnPurchaseOrder.Tooltip = "Create Purchase Order";
            this.btnPurchaseOrder.Click += new System.EventHandler(this.btnPurchaseOrder_Click);
            // 
            // btnPurchaseInvoice
            // 
            this.btnPurchaseInvoice.Image = ((System.Drawing.Image)(resources.GetObject("btnPurchaseInvoice.Image")));
            this.btnPurchaseInvoice.Name = "btnPurchaseInvoice";
            this.btnPurchaseInvoice.Text = "Create Purchase Invoice";
            this.btnPurchaseInvoice.Tooltip = "Create Purchase Invoice";
            this.btnPurchaseInvoice.Click += new System.EventHandler(this.btnPurchaseInvoice_Click);
            // 
            // BtnUom
            // 
            this.BtnUom.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnUom.Icon = ((System.Drawing.Icon)(resources.GetObject("BtnUom.Icon")));
            this.BtnUom.Name = "BtnUom";
            this.BtnUom.Text = "Uom";
            this.BtnUom.Tooltip = "Unit Of Measurement";
            this.BtnUom.Click += new System.EventHandler(this.BtnUom_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // BtnHelp
            // 
            this.BtnHelp.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Tooltip = "Help";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // tiSuggestions
            // 
            this.tiSuggestions.Name = "tiSuggestions";
            this.tiSuggestions.Text = "Suggestions";
            this.tiSuggestions.Visible = false;
            // 
            // CMSVendorAddress
            // 
            this.CMSVendorAddress.Name = "ContextMenuStrip1";
            this.CMSVendorAddress.Size = new System.Drawing.Size(61, 4);
            // 
            // TmrPurchase
            // 
            this.TmrPurchase.Interval = 2000;
            this.TmrPurchase.Tick += new System.EventHandler(this.TmrPurchaseOrder_Tick);
            // 
            // TmrFocus
            // 
            this.TmrFocus.Tick += new System.EventHandler(this.TmrFocus_Tick);
            // 
            // ErrPurchase
            // 
            this.ErrPurchase.ContainerControl = this;
            this.ErrPurchase.RightToLeft = true;
            // 
            // ImgPurchase
            // 
            this.ImgPurchase.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgPurchase.ImageStream")));
            this.ImgPurchase.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgPurchase.Images.SetKeyName(0, "Purchase Indent.ICO");
            this.ImgPurchase.Images.SetKeyName(1, "Purchase Order.ico");
            this.ImgPurchase.Images.SetKeyName(2, "Purchase Invoice.ico");
            this.ImgPurchase.Images.SetKeyName(3, "GRN.ICO");
            this.ImgPurchase.Images.SetKeyName(4, "Purchase Order Return.ico");
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.lblCreatedByText);
            this.pnlBottom.Controls.Add(this.lblCreatedDateValue);
            this.pnlBottom.Controls.Add(this.lblPurchasestatus);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(3, 546);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1273, 26);
            this.pnlBottom.TabIndex = 291;
            // 
            // lblCreatedByText
            // 
            this.lblCreatedByText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedByText.CloseButtonVisible = false;
            this.lblCreatedByText.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblCreatedByText.Location = new System.Drawing.Point(891, 0);
            this.lblCreatedByText.Name = "lblCreatedByText";
            this.lblCreatedByText.OptionsButtonVisible = false;
            this.lblCreatedByText.Size = new System.Drawing.Size(380, 24);
            this.lblCreatedByText.TabIndex = 258;
            this.lblCreatedByText.Text = "Created By";
            // 
            // lblCreatedDateValue
            // 
            this.lblCreatedDateValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedDateValue.CloseButtonVisible = false;
            this.lblCreatedDateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCreatedDateValue.Location = new System.Drawing.Point(393, 0);
            this.lblCreatedDateValue.Name = "lblCreatedDateValue";
            this.lblCreatedDateValue.OptionsButtonVisible = false;
            this.lblCreatedDateValue.Size = new System.Drawing.Size(878, 24);
            this.lblCreatedDateValue.TabIndex = 256;
            this.lblCreatedDateValue.Text = "Created Date";
            // 
            // lblPurchasestatus
            // 
            this.lblPurchasestatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblPurchasestatus.CloseButtonVisible = false;
            this.lblPurchasestatus.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPurchasestatus.Image = ((System.Drawing.Image)(resources.GetObject("lblPurchasestatus.Image")));
            this.lblPurchasestatus.Location = new System.Drawing.Point(0, 0);
            this.lblPurchasestatus.Name = "lblPurchasestatus";
            this.lblPurchasestatus.OptionsButtonVisible = false;
            this.lblPurchasestatus.Size = new System.Drawing.Size(393, 24);
            this.lblPurchasestatus.TabIndex = 20;
            // 
            // LLot
            // 
            this.LLot.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LLot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LLot.HeaderText = "Lot";
            this.LLot.Name = "LLot";
            // 
            // LBlock
            // 
            this.LBlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LBlock.HeaderText = "Block";
            this.LBlock.Name = "LBlock";
            this.LBlock.Width = 125;
            // 
            // LRow
            // 
            this.LRow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LRow.HeaderText = "Row";
            this.LRow.Name = "LRow";
            this.LRow.Width = 125;
            // 
            // LLocation
            // 
            this.LLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LLocation.HeaderText = "Location";
            this.LLocation.Name = "LLocation";
            this.LLocation.Width = 125;
            // 
            // LUOM
            // 
            this.LUOM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LUOM.HeaderText = "UOM";
            this.LUOM.Name = "LUOM";
            this.LUOM.ReadOnly = true;
            this.LUOM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LUOM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.LUOM.Width = 60;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Item Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 180;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn3.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 180;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn4.HeaderText = "QtyReceived";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Visible = false;
            this.dataGridViewTextBoxColumn4.Width = 217;
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn5.HeaderText = "Batch Number";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn6.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn6.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn7.HeaderText = "Total";
            this.dataGridViewTextBoxColumn7.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn8.HeaderText = "Status";
            this.dataGridViewTextBoxColumn8.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn9.HeaderText = "OrderDetailID";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // dataGridViewTextBoxColumn10
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn10.HeaderText = "PurchaseOrderID";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Visible = false;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn12.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn12.HeaderText = "PurchaseOrderID";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Visible = false;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "BatchID";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "OrderQuantity";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Visible = false;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "BatchID";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.Visible = false;
            // 
            // calendarColumn1
            // 
            this.calendarColumn1.HeaderText = "ExpiryDate";
            this.calendarColumn1.Name = "calendarColumn1";
            this.calendarColumn1.Visible = false;
            // 
            // dgvLocationDetails
            // 
            this.dgvLocationDetails.AddNewRow = false;
            this.dgvLocationDetails.AlphaNumericCols = new int[0];
            this.dgvLocationDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvLocationDetails.CapsLockCols = new int[0];
            this.dgvLocationDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocationDetails.DecimalCols = new int[0];
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLocationDetails.DefaultCellStyle = dataGridViewCellStyle16;
            this.dgvLocationDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLocationDetails.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvLocationDetails.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvLocationDetails.HasSlNo = false;
            this.dgvLocationDetails.LastRowIndex = 0;
            this.dgvLocationDetails.Location = new System.Drawing.Point(0, 0);
            this.dgvLocationDetails.Name = "dgvLocationDetails";
            this.dgvLocationDetails.NegativeValueCols = new int[0];
            this.dgvLocationDetails.NumericCols = new int[0];
            this.dgvLocationDetails.RowHeadersWidth = 30;
            this.dgvLocationDetails.Size = new System.Drawing.Size(1271, 251);
            this.dgvLocationDetails.TabIndex = 0;
            // 
            // LUOMID
            // 
            this.LUOMID.HeaderText = "UOMID";
            this.LUOMID.Name = "LUOMID";
            this.LUOMID.Visible = false;
            // 
            // LQuantity
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.LQuantity.DefaultCellStyle = dataGridViewCellStyle17;
            this.LQuantity.HeaderText = "Quantity";
            this.LQuantity.Name = "LQuantity";
            // 
            // LBatchNo
            // 
            this.LBatchNo.HeaderText = "Batch No";
            this.LBatchNo.Name = "LBatchNo";
            this.LBatchNo.ReadOnly = true;
            this.LBatchNo.Width = 130;
            // 
            // LBatchID
            // 
            this.LBatchID.HeaderText = "BatchID";
            this.LBatchID.Name = "LBatchID";
            this.LBatchID.Visible = false;
            // 
            // LItemID
            // 
            this.LItemID.HeaderText = "ItemID";
            this.LItemID.Name = "LItemID";
            this.LItemID.Visible = false;
            // 
            // LItemName
            // 
            this.LItemName.HeaderText = "Item Name";
            this.LItemName.Name = "LItemName";
            this.LItemName.Width = 250;
            // 
            // LItemCode
            // 
            this.LItemCode.HeaderText = "Item Code";
            this.LItemCode.Name = "LItemCode";
            this.LItemCode.Width = 60;
            // 
            // FrmDirectGRN
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.ClientSize = new System.Drawing.Size(1276, 572);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.panelEx1);
            this.Controls.Add(this.dockSite2);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.expandableSplitterLeft);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.dockSite3);
            this.Controls.Add(this.dockSite4);
            this.Controls.Add(this.dockSite5);
            this.Controls.Add(this.dockSite6);
            this.Controls.Add(this.dockSite8);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmDirectGRN";
            this.Text = "Direct GRN";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmDirectGRN_Load);
            this.Shown += new System.EventHandler(this.FrmDirectGRN_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmDirectGRN_FormClosing);
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGRNDisplay)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcPurchase)).EndInit();
            this.tcPurchase.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGRN)).EndInit();
            this.tabControlPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.panelEx1.ResumeLayout(false);
            this.panelBottom.ResumeLayout(false);
            this.panelGridBottom.ResumeLayout(false);
            this.panelGridBottom.PerformLayout();
            this.pnlAmount.ResumeLayout(false);
            this.pnlAmount.PerformLayout();
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).EndInit();
            this.tcGeneral.ResumeLayout(false);
            this.tabControlPanel3.ResumeLayout(false);
            this.tabControlPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrPurchase)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocationDetails)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>

        #endregion

        #region Declarations

        public FrmMain objFrmTradingMain;

        private bool MblnPrintEmailPermission = false;
        private bool MblnAddPermission = false;
        private bool MblnUpdatePermission = false;
        private bool MblnDeletePermission = false;
        private bool MblnAddUpdatePermission = false;
        private bool MblnCancelPermission = false;
        private bool MblnIsFromCancellation = false;
        private bool MblnPIPermission = false;
        private bool MblnPOPermission = false;
        private string MstrMessageCommon;                 //  variable for assigning message
        private ArrayList MaMessageArr;                 // Error Message display
        private ArrayList MaStatusMessage;
        private MessageBoxIcon MmessageIcon;
        ClsLogWriter MObjLogs;                          //  Object for Class Clslogs
        ClsNotification mObjNotification;               //  Object for Class ClsNotification

        clsBLLDirectGRN MobjclsBLLDirectGRN;
        clsBLLCommonUtility MobjclsBLLCommonUtility;
        private bool MblnCanShow;
        private bool MblnChangeStatus;
        bool blnAddMode = true;
        int MintComboID;
        int MintVendorAddID;

        int MUOMID;
        int MintBaseCurrencyScale;
        #endregion

        #region Constructor

        public FrmDirectGRN(int intFormType)
        {
            InitializeComponent();
            MobjclsBLLDirectGRN = new clsBLLDirectGRN();
            mObjNotification = new ClsNotification();
            MobjclsBLLCommonUtility = new clsBLLCommonUtility();
        }

        #endregion

        #region Functions


        private void LoadMessage()
        {
            try
            {
                MaMessageArr = new ArrayList();
                MaStatusMessage = new ArrayList();
                MaMessageArr = mObjNotification.FillMessageArray((int)FormID.Purchase, ClsCommonSettings.ProductID);
                MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.Purchase, ClsCommonSettings.ProductID);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadMessage() " + ex.Message);
                MObjLogs.WriteLog("Error in LoadMessage() " + ex.Message, 2);
            }
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.GRN, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                DataTable DtPermission = objClsBLLPermissionSettings.GetMenuPermissions(ClsCommonSettings.RoleID);
                if (DtPermission.Rows.Count == 0)
                {
                    BtnUom.Enabled = MblnPIPermission = MblnPOPermission = false;
                }
                else
                {
                    DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.UOM).ToString();
                    BtnUom.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);

                    DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PurchaseOrder).ToString();
                    MblnPOPermission = (DtPermission.DefaultView.ToTable().Rows.Count > 0);

                    DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PurchaseInvoice).ToString();
                    MblnPIPermission = (DtPermission.DefaultView.ToTable().Rows.Count > 0);
                }
            }
            else
            {
                MblnAddPermission = MblnAddUpdatePermission = MblnCancelPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
                MblnPIPermission = MblnPOPermission = true;
                BtnUom.Enabled = true;
            }
            MblnCancelPermission = MblnUpdatePermission;
        }

        private bool IsPurchaseOrderExists()
        {
            if (MobjclsBLLDirectGRN.IsPurchaseOrderExists())
            {
                return true;
            }
            else
                return false;
        }

        private bool IsPurchaseInvoiceExists()
        {
            if (MobjclsBLLDirectGRN.IsPurchaseInvoiceExists())
            {
                return true;
            }
            else
                return false;
        }

        private void LoadCombos(int intType)
        {

            DataTable datCombos = null;

            if (intType == 0 || intType == 1)
            {
                datCombos = null;
                datCombos = MobjclsBLLDirectGRN.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", " CompanyID= " + ClsCommonSettings.LoginCompanyID });
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.DataSource = datCombos;
                cboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;
            }
            if (intType == 0 || intType == 2)
            {
                datCombos = null;
                datCombos = MobjclsBLLDirectGRN.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", " CompanyID= " + ClsCommonSettings.LoginCompanyID });
                cboSCompany.ValueMember = "CompanyID";
                cboSCompany.DisplayMember = "CompanyName";
                cboSCompany.DataSource = datCombos;
            }
            if (intType == 0 || intType == 3)
            {
                datCombos = null;
                if (!blnAddMode)
                {
                    datCombos = MobjclsBLLDirectGRN.FillCombos(new string[] { "distinct V.VendorID,V.VendorName", "InvVendorInformation V INNER JOIN InvVendorCompanyDetails VC ON V.VendorID = VC.VendorID ", " V.VendorTypeID = " + (int)VendorType.Supplier + " AND VC.CompanyID = " + ClsCommonSettings.CompanyID + "  and V.StatusID=" + (int)VendorStatus.SupplierActive + " UNION SELECT VendorID,VendorName FROM InvVendorInformation WHERE VendorID =" + MobjclsBLLDirectGRN.PobjclsDTOGRN.intVendorID });
                }
                else
                {
                    datCombos = MobjclsBLLDirectGRN.FillCombos(new string[] { "distinct V.VendorID,V.VendorName", "InvVendorInformation V INNER JOIN InvVendorCompanyDetails VC ON V.VendorID = VC.VendorID ", " V.VendorTypeID = " + (int)VendorType.Supplier + " AND VC.CompanyID = " + ClsCommonSettings.CompanyID + "  and V.StatusID=" + (int)VendorStatus.SupplierActive + "" });
                    //datCombos = MobjclsBLLDirectGRN.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", " VendorTypeID = " + (int)VendorType.Supplier + " and StatusID=" + (int)VendorStatus.SupplierActive });
                }
                cboSupplier.ValueMember = "VendorID";
                cboSupplier.DisplayMember = "VendorName";
                cboSupplier.DataSource = datCombos;
            }

            if (intType == 0 || intType == 4)
            {
                datCombos = null;
                datCombos = MobjclsBLLDirectGRN.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", " VendorTypeID = " + (int)VendorType.Supplier + " and StatusID=" + (int)VendorStatus.SupplierActive + "" });
                DataRow dr = datCombos.NewRow();
                dr["VendorID"] = 0;
                dr["VendorName"] = "ALL";
                datCombos.Rows.InsertAt(dr, 0);
                cboSSupplier.ValueMember = "VendorID";
                cboSSupplier.DisplayMember = "VendorName";
                cboSSupplier.DataSource = datCombos;
            }

            if (intType == 0 || intType == 5)
            {
                datCombos = null;
                cboWarehouse.Text = string.Empty;
                if (!blnAddMode)
                {
                    datCombos = MobjclsBLLDirectGRN.FillCombos(new string[] { "Distinct InvWarehouse.WarehouseID,WarehouseName", "" +
                        "InvWarehouse INNER JOIN InvWarehouseCompanyDetails WC ON InvWarehouse.WarehouseID=WC.WarehouseID", "" +
                        "WC.CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + " And IsActive = 1 UNION " +
                        "SELECT WarehouseID,WarehouseName FROM InvWarehouse WHERE WarehouseID = " + MobjclsBLLDirectGRN.PobjclsDTOGRN.intWarehouseID });
                }
                else
                {
                    datCombos = MobjclsBLLDirectGRN.FillCombos(new string[] { "Distinct InvWarehouse.WarehouseID,WarehouseName", "" +
                        "InvWarehouse INNER JOIN InvWarehouseCompanyDetails WC ON InvWarehouse.WarehouseID=WC.WarehouseID", "" +
                        "WC.CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + " And IsActive = 1" });
                }
                cboWarehouse.ValueMember = "WarehouseID";
                cboWarehouse.DisplayMember = "WarehouseName";
                cboWarehouse.DataSource = datCombos;
            }
            if (intType == 0 || intType == 6)
            {
                datCombos = null;
                string strCondition = "OperationTypeID = "+(Int32)OperationType.GRN;
                datCombos = MobjclsBLLDirectGRN.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", strCondition });
                DataRow dr = datCombos.NewRow();
                dr["StatusID"] = 0;
                dr["Status"] = "ALL";
                datCombos.Rows.InsertAt(dr, 0);
                cboSStatus.ValueMember = "StatusID";
                cboSStatus.DisplayMember = "Status";
                cboSStatus.DataSource = datCombos;
            }
            if (intType == 0 || intType == 7)
            {
                string strSearchCondition = "";
                strSearchCondition = "UM.UserID <> 1";
                if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    strSearchCondition = strSearchCondition + " AND UM.RoleID Not In (1,2)";
                }
                datCombos = MobjclsBLLDirectGRN.FillCombos(new string[] { "UM.UserID,IsNull(EM.FirstName,UM.UserName) as UserName", "UserMaster UM Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID", strSearchCondition });
                cboSExecutive.ValueMember = "UserID";
                cboSExecutive.DisplayMember = "UserName";
                cboSExecutive.DataSource = datCombos;
            }
            if (intType == 0 || intType == 8)
            {
                string strSearchCondition = "";
                strSearchCondition = "UM.UserID <> 1";
                if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    strSearchCondition = strSearchCondition +" AND UM.RoleID Not In (1,2)";
                }
                if (!string.IsNullOrEmpty(strSearchCondition))
                    strSearchCondition = " And " + strSearchCondition;
                datCombos = MobjclsBLLDirectGRN.FillCombos(new string[] { "UM.UserID,IsNull(EM.FirstName,UM.UserName) as UserName", "UserMaster UM Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID ", "Case IsNull(EM.EmployeeID,0) When 0 Then " + cboSCompany.SelectedValue.ToInt32() + " Else EM.CompanyID  End = " + cboSCompany.SelectedValue.ToInt32() + "" + strSearchCondition });
                cboSExecutive.ValueMember = "UserID";
                cboSExecutive.DisplayMember = "UserName";
                cboSExecutive.DataSource = datCombos;
            }
            if (intType == 0 || intType == 9)
            {
                datCombos = null;
                cboCurrency.SelectedText = "";
                cboCurrency.Text = string.Empty;
                datCombos = MobjclsBLLDirectGRN.FillCombos(new string[] { "distinct CR.CurrencyID,CR.CurrencyName", "CurrencyReference CR INNER JOIN CurrencyDetails CD ON CR.CurrencyID=CD.CurrencyID INNER JOIN InvVendorInformation VI   on VI.CurrencyID = CR.CurrencyID ", "CD.CompanyID=" + cboCompany.SelectedValue.ToInt32() + "and  (  VI.VendorID =" + cboSupplier.SelectedValue.ToInt32() + " or " + cboSupplier.SelectedValue.ToInt32() + " = 0 )" });
                cboCurrency.ValueMember = "CurrencyID";
                cboCurrency.DisplayMember = "CurrencyName";
                cboCurrency.DataSource = datCombos;
                
            }


        }

        //private void Changestatus()
        //{
        //    MblnChangeStatus = true;
        //    if (blnAddMode)
        //    {
        //        BindingNavigatorSaveItem.Enabled = MblnAddPermission;
        //        BtnPrint.Enabled = BtnEmail.Enabled = false;
        //    }
        //    else
        //    {
        //        if(!MobjclsBLLDirectGRN.PobjclsDTOGRN.blnCancelled)
        //        BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
        //        BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
        //        BtnPrint.Enabled = MblnPrintEmailPermission;
        //        BtnEmail.Enabled = MblnPrintEmailPermission;
        //    }

        //    ErrPurchase.Clear();
        //}
        private bool AddToAddressMenu(int iVendorID)
        {
            try
            {
                int i;

                CMSVendorAddress.Items.Clear();
                DataTable dtAddMenu = DtGetAddressName(iVendorID);

                for (i = 0; i <= dtAddMenu.Rows.Count - 1; i++)
                {
                    ToolStripMenuItem tItem = new ToolStripMenuItem();
                    tItem.Tag = Convert.ToString(dtAddMenu.Rows[i][0]);
                    tItem.Text = Convert.ToString(dtAddMenu.Rows[i][1]);
                    CMSVendorAddress.Items.Add(tItem);
                }
                CMSVendorAddress.ItemClicked -= new ToolStripItemClickedEventHandler(this.MenuItem_Click);
                CMSVendorAddress.ItemClicked += new ToolStripItemClickedEventHandler(this.MenuItem_Click);
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in AddToAddressMenu() " + ex.Message);
                MObjLogs.WriteLog("Error in AddToAddressMenu() " + ex.Message, 2);
                return false;
            }
        }

        public DataTable DtGetAddressName(int iVendorID)
        {
            try
            {
                DataTable sRecordValues;
                sRecordValues = MobjclsBLLDirectGRN.FillCombos(new string[] { "VendorAddID,AddressName", "InvVendorAddress", "VendorID = " + iVendorID });
                return sRecordValues;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DtGetAddressName() " + ex.Message);
                MObjLogs.WriteLog("Error in DtGetAddressName() " + ex.Message, 2);
                return null;
            }
        }

        private bool DisplayAddress(int iVendorID)
        {
            try
            {
                txtSupplierAddress.Text = MobjclsBLLDirectGRN.DisplayAddressInformation(MintVendorAddID);
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayAddress() " + ex.Message);
                MObjLogs.WriteLog("Error in DisplayAddress() " + ex.Message, 2);
                return false;
            }
        }

        private void GenerateOrderNo()
        {
            try
            {
                txtGRNNo.Text = GetGRNPrefix() + (MobjclsBLLDirectGRN.GetLastGRNNo(true, cboCompany.SelectedValue.ToInt32()) + 1).ToString();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in GenerateOrderNo() " + ex.Message);
                MObjLogs.WriteLog("Error in GenerateOrderNo() " + ex.Message, 2);
            }
        }
        private string GetGRNPrefix()
        {

            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue));

            if (ClsCommonSettings.PGRNAutogenerate)
                txtGRNNo.ReadOnly = false;
            else
                txtGRNNo.ReadOnly = true;
            return ClsCommonSettings.PGRNPrefix;
            
        }

        private void ClearControls()
        {

            dtpSFrom.Value = ClsCommonSettings.GetServerDate();
            dtpSTo.Value = ClsCommonSettings.GetServerDate();
            dtpDate.Value = ClsCommonSettings.GetServerDate();

            if (ClsCommonSettings.CompanyID <= 0)
                ClsCommonSettings.CompanyID = MobjclsBLLCommonUtility.GetCompanyID();
            txtSupplierAddress.Text = string.Empty;
            LoadCombos(0);
            cboCompany.SelectedValue = ClsCommonSettings.CompanyID;
            cboWarehouse.SelectedValue = "-1";
            cboSupplier.SelectedValue = "-1";
            txtSupplierAddress.Text = string.Empty;
            GenerateOrderNo();
            txtDescription.Text = string.Empty;
            txtRemarks.Text = string.Empty;
            dgvGRN.Rows.Clear();
            blnAddMode = true;
            MintVendorAddID = 0;
            MblnChangeStatus = false;
            if (blnAddMode)
            {
                lblStatusText.ForeColor = Color.Black;
                lblStatusText.Text = "New";
            }
            txtSubTotal.Text = Convert.ToDecimal(0).ToString("F" + MintBaseCurrencyScale);
            txtExchangeCurrencyRate.Text = Convert.ToDecimal(0).ToString("F" + MintBaseCurrencyScale);
            txtTotalAmount.Text = Convert.ToDecimal(0).ToString("F" + MintBaseCurrencyScale);

            MblnCanShow = true;
            BindingNavigatorCancelItem.Text = "Cancel";
            btnActions.Enabled = false;
            ChangeStatus();
            txtGRNNo.ReadOnly = true;
            cboSCompany.SelectedValue = "-1";
            cboSExecutive.SelectedValue = "-1";
            cboSSupplier.SelectedValue = "-1";
            cboSStatus.SelectedValue = "-1";
            txtSGRNNo.Text = string.Empty;
            lblDescription.Visible = false;
            txtDescription.Visible = false;
            dgvGRN.ReadOnly = false;
            MobjclsBLLDirectGRN = new clsBLLDirectGRN();

        }
        private void DisplayAllNoInSearchGrid()
        {
            try
            {
                DataTable dtTemp = null;
                string strFilterCondition = string.Empty;
                if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != 0)
                    strFilterCondition += " CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue);
                else
                {
                    dtTemp = null;
                    dtTemp = (DataTable)cboSCompany.DataSource;
                    if (dtTemp.Rows.Count > 0)
                    {
                        strFilterCondition += "CompanyID In (";
                        foreach (DataRow dr in dtTemp.Rows)
                        {
                            strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                        }
                        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                        strFilterCondition += ")";
                    }
                }
                if (cboSSupplier.SelectedValue != null && Convert.ToInt32(cboSSupplier.SelectedValue) != 0)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "VendorID = " + Convert.ToInt32(cboSSupplier.SelectedValue);
                }
                if (cboSExecutive.SelectedValue != null && Convert.ToInt32(cboSExecutive.SelectedValue) != 0)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "CreatedBy = " + Convert.ToInt32(cboSExecutive.SelectedValue);
                }
                if (cboSStatus.SelectedValue != null && Convert.ToInt32(cboSStatus.SelectedValue) != 0)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "StatusID = " + Convert.ToInt32(cboSStatus.SelectedValue);
                }

                if (!string.IsNullOrEmpty(strFilterCondition))
                    strFilterCondition += " And ";
                strFilterCondition += " GRNDate >= '" + dtpSFrom.Value.ToString("dd-MMM-yyyy") + "' And GRNDate <= '" + dtpSTo.Value.ToString("dd-MMM-yyyy") + "'";

                if (!string.IsNullOrEmpty(txtSGRNNo.Text.Trim()))
                {
                    strFilterCondition = " GRNNo = '" + txtSGRNNo.Text.Trim() + "'";
                }

                DataTable dtGRN = MobjclsBLLDirectGRN.GetGRNNos();
                dtGRN.DefaultView.RowFilter = strFilterCondition;
                dgvGRNDisplay.DataSource = null;
                dgvGRNDisplay.DataSource = dtGRN.DefaultView.ToTable();
                dgvGRNDisplay.Columns["GRNID"].Visible = false;
                dgvGRNDisplay.Columns["CompanyID"].Visible = false;
                dgvGRNDisplay.Columns["VendorID"].Visible = false;
                dgvGRNDisplay.Columns["StatusID"].Visible = false;
                dgvGRNDisplay.Columns["EmployeeID"].Visible = false;
                dgvGRNDisplay.Columns["GRNDate"].Visible = false;
                dgvGRNDisplay.Columns["CreatedBy"].Visible = false;
                dgvGRNDisplay.Columns["GRNNo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                lblPurchaseCount.Text = "Total GRNs " + dgvGRNDisplay.Rows.Count;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayAllNoInSearchGrid() " + ex.Message);
                MObjLogs.WriteLog("Error in DisplayAllNoInSearchGrid() " + ex.Message, 2);
            }
        }

        public void RefreshForm()
        {
            btnSRefresh_Click(null, null);
        }

        private void FillComboColumn(int intItemID,int intRowIndex)
        {
            try
            {
                DataTable dtItemUOMs = MobjclsBLLDirectGRN.GetItemUOMs(intItemID);
                Uom.DataSource = null;
                Uom.ValueMember = "UOMID";
                Uom.DisplayMember = "ShortName";
                Uom.DataSource = dtItemUOMs;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FillComboColumn() " + ex.Message);
                MObjLogs.WriteLog("Error in FillComboColumn() " + ex.Message, 2);
            }
        }

        private void FillMasterParameters()
        {
            if (blnAddMode)
            {
                MobjclsBLLDirectGRN.PobjclsDTOGRN.blnCancelled = false;
                MobjclsBLLDirectGRN.PobjclsDTOGRN.lngGRNID = 0;
                MobjclsBLLDirectGRN.PobjclsDTOGRN.intStatusID = (Int32)OperationStatusType.GRNOpened;
            }
            else
            {
                MobjclsBLLDirectGRN.PobjclsDTOGRN.intStatusID = lblStatusText.Tag.ToInt32();
            }

            MobjclsBLLDirectGRN.PobjclsDTOGRN.strGRNNo = txtGRNNo.Text.Trim();
            MobjclsBLLDirectGRN.PobjclsDTOGRN.strGRNDate = dtpDate.Value.ToString("dd-MMM-yyyy");
            MobjclsBLLDirectGRN.PobjclsDTOGRN.intVendorID = cboSupplier.SelectedValue.ToInt32();
            MobjclsBLLDirectGRN.PobjclsDTOGRN.intVendorAddID = MintVendorAddID;
            MobjclsBLLDirectGRN.PobjclsDTOGRN.strRemarks = txtRemarks.Text.Trim();
            MobjclsBLLDirectGRN.PobjclsDTOGRN.intCompanyID = cboCompany.SelectedValue.ToInt32();
            MobjclsBLLDirectGRN.PobjclsDTOGRN.intWarehouseID = cboWarehouse.SelectedValue.ToInt32();
            MobjclsBLLDirectGRN.PobjclsDTOGRN.strDueDate = dtpDate.Value.ToString("dd-MMM-yyyy"); 
            MobjclsBLLDirectGRN.PobjclsDTOGRN.strCreatedDate = ClsCommonSettings.GetServerDate().ToString();
            MobjclsBLLDirectGRN.PobjclsDTOGRN.intCreatedBy = ClsCommonSettings.UserID;
            MobjclsBLLDirectGRN.PobjclsDTOGRN.intTempWarehouseID = MobjclsBLLDirectGRN.PobjclsDTOGRN.intWarehouseID;
            MobjclsBLLDirectGRN.PobjclsDTOGRN.intCurrencyID = cboCurrency.SelectedValue.ToInt32();
            if (MobjclsBLLDirectGRN.PobjclsDTOGRN.intStatusID == (Int32)OperationStatusType.GRNCancelled)
            {
                MobjclsBLLDirectGRN.PobjclsDTOGRN.blnCancelled = true;
                MobjclsBLLDirectGRN.PobjclsDTOGRN.strDescription = txtDescription.Text.Trim();
            }
            else
            {
                MobjclsBLLDirectGRN.PobjclsDTOGRN.blnCancelled = false;
            }
            MobjclsBLLDirectGRN.PobjclsDTOGRN.decNetAmount = Convert.ToDecimal(txtTotalAmount.Text);
        }
        private void FillDetailParameters()
        {
            MobjclsBLLDirectGRN.PobjclsDTOGRN.lstclsDTOGRNDetailsCollection = new List<clsDTOGRNDetails>();
            for (int iCounter = 0; iCounter < dgvGRN.Rows.Count; iCounter++)
            {
                if (dgvGRN["ItemID", iCounter].Value != null && dgvGRN["Quantity", iCounter].Value != null)
                {
                    clsDTOGRNDetails objclsDTOGRNDetails = new clsDTOGRNDetails();
                    objclsDTOGRNDetails.intItemID = dgvGRN["ItemID", iCounter].Value.ToInt32();
                    objclsDTOGRNDetails.decQuantity = dgvGRN["Quantity", iCounter].Value.ToDecimal();
                    objclsDTOGRNDetails.decExtraQuantity = dgvGRN["ExtraQty", iCounter].Value.ToDecimal();
                    objclsDTOGRNDetails.intUOMID = dgvGRN["Uom", iCounter].Tag.ToInt32();
                    if (dgvGRN["ExpiryDate", iCounter].Value != null)
                        objclsDTOGRNDetails.strExpiryDate = Convert.ToDateTime(dgvGRN["ExpiryDate", iCounter].Value).ToString("dd-MMM-yyyy");
                    objclsDTOGRNDetails.strBatchNumber = dgvGRN["Batchnumber", iCounter].Value.ToString();
                    objclsDTOGRNDetails.intBatchID = dgvGRN["Batchnumber", iCounter].Tag.ToInt32();
                    if (dgvGRN["Rate", iCounter].Value != null)
                        objclsDTOGRNDetails.decPurchaseRate = dgvGRN["Rate", iCounter].Value.ToDecimal();

                    if (dgvGRN["NetAmount", iCounter].Value != null)
                        objclsDTOGRNDetails.decNetAmount = dgvGRN["NetAmount", iCounter].Value.ToDecimal();

                    MobjclsBLLDirectGRN.PobjclsDTOGRN.lstclsDTOGRNDetailsCollection.Add(objclsDTOGRNDetails);
                }
            }
        }

       
        private void ChangeStatus()
        {

            ErrPurchase.Clear();
            if (blnAddMode)
            {
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorCancelItem.Enabled = false;
                BindingNavigatorAddNewItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = true;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
                BtnPrint.Enabled = BtnEmail.Enabled = false;
            }
            else 
            {
                BtnPrint.Enabled = BtnEmail.Enabled = true;
                BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
                BindingNavigatorAddNewItem.Enabled = true;
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;

                if (IsPurchaseOrderExists())
                {
                    btnPurchaseOrder.Enabled = true;
                    btnPurchaseOrder.Enabled = MblnPOPermission;
                }
                else
                    btnPurchaseOrder.Enabled = false;

                if (IsPurchaseInvoiceExists())
                {
                    btnPurchaseInvoice.Enabled = true;
                    btnPurchaseInvoice.Enabled = MblnPIPermission;
                }
                else
                    btnPurchaseInvoice.Enabled = false;


                if (!MobjclsBLLDirectGRN.PobjclsDTOGRN.blnCancelled)
                {
                    btnActions.Enabled = true;
                    if (MobjclsBLLDirectGRN.IsInvoiceOrOrderExists() && (MobjclsBLLDirectGRN.IsReferenceExists()))
                    {
                        BindingNavigatorCancelItem.Enabled = true;
                        BindingNavigatorDeleteItem.Enabled = true;
                        BindingNavigatorSaveItem.Enabled = true;
                        BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                        BindingNavigatorCancelItem.Enabled = MblnUpdatePermission;
                        BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                        dgvGRN.ReadOnly = false;
                    }
                    else
                    {
                        BindingNavigatorSaveItem.Enabled = false;
                        BindingNavigatorCancelItem.Enabled = false;
                        BindingNavigatorDeleteItem.Enabled = false;
                        dgvGRN.ReadOnly = true;
                    }
                }
                else
                {
                    btnActions.Enabled = false;
                    BindingNavigatorSaveItem.Enabled = false;
                    BindingNavigatorCancelItem.Enabled = true;
                    BindingNavigatorDeleteItem.Enabled = true;
                    BindingNavigatorCancelItem.Enabled = MblnUpdatePermission;
                    BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                    dgvGRN.ReadOnly = true;
                }
            }
            //Changestatus();
        }

        private void DisplayPurchaseGRN()
        {
            blnAddMode = false;
            DataTable dtTemp = MobjclsBLLDirectGRN.DisplayPurchaseGRN(MobjclsBLLDirectGRN.PobjclsDTOGRN.lngGRNID);
            if (dtTemp != null && dtTemp.Rows.Count > 0)
            {
                cboCompany.SelectedValue = dtTemp.Rows[0]["CompanyID"].ToString();
                MobjclsBLLDirectGRN.PobjclsDTOGRN.intWarehouseID = Convert.ToInt32(dtTemp.Rows[0]["WarehouseID"]);
                LoadCombos(5);
                cboWarehouse.SelectedValue = dtTemp.Rows[0]["WarehouseID"].ToString();
                txtGRNNo.Text = dtTemp.Rows[0]["GRNNo"].ToString();
                txtGRNNo.Tag = dtTemp.Rows[0]["GRNID"].ToString();
                dtpDate.Text = dtTemp.Rows[0]["GRNDate"].ToString();
                MobjclsBLLDirectGRN.PobjclsDTOGRN.intVendorID = Convert.ToInt32(dtTemp.Rows[0]["VendorID"]);
                LoadCombos(3);
                cboSupplier.SelectedValue = dtTemp.Rows[0]["VendorID"].ToString();
                lblSupplierAddressName.Text = dtTemp.Rows[0]["AddressName"].ToString();
                lblSupplierAddressName.Tag = dtTemp.Rows[0]["VendorAddID"].ToString();
                if (dtTemp.Rows[0]["CurrencyID"] != DBNull.Value)
                    cboCurrency.SelectedValue = dtTemp.Rows[0]["CurrencyID"].ToString();
                txtRemarks.Text = dtTemp.Rows[0]["Remarks"].ToString();
                lblCreatedByText.Text = "Created By :" + dtTemp.Rows[0]["EmployeeName"].ToString();
                lblCreatedDateValue.Text = "Created Date :" + (Convert.ToDateTime(dtTemp.Rows[0]["CreatedDate"])).ToString("dd-MMM-yyyy");
                lblCreatedByText.Tag = dtTemp.Rows[0]["CreatedBy"].ToString();
                lblPurchasestatus.Text = "Details of GRN No " + txtGRNNo.Text.Trim();

                MobjclsBLLDirectGRN.PobjclsDTOGRN.blnCancelled = false;

                if (dtTemp.Rows[0]["StatusID"].ToInt32() == (Int32)OperationStatusType.GRNOpened)
                {
                    lblStatusText.ForeColor = Color.Brown;
                    lblStatusText.Text = "Opened";
                    btnActions.Enabled = true;
                }
                else if (dtTemp.Rows[0]["StatusID"].ToInt32() == (Int32)OperationStatusType.GRNCancelled)
                {
                    lblStatusText.ForeColor = Color.Red;
                    lblStatusText.Text = "Cancelled";
                    btnActions.Enabled = false;
                    MobjclsBLLDirectGRN.PobjclsDTOGRN.blnCancelled = true;
                }
                else if (dtTemp.Rows[0]["StatusID"].ToInt32() == (Int32)OperationStatusType.GRNPartiallyInvoiced)
                {
                    lblStatusText.ForeColor = Color.Green;
                    lblStatusText.Text = "Partially Invoiced";
                    btnActions.Enabled = false;
                }
                else if (dtTemp.Rows[0]["StatusID"].ToInt32() == (Int32)OperationStatusType.GRNFullyInvoiced)
                {
                    lblStatusText.ForeColor = Color.Green;
                    lblStatusText.Text = "Invoiced";
                    btnActions.Enabled = false;
                }
                else if (dtTemp.Rows[0]["StatusID"].ToInt32() == (Int32)OperationStatusType.GRNOrdered)
                {
                    lblStatusText.ForeColor = Color.Violet;
                    lblStatusText.Text = "Ordered";
                    btnActions.Enabled = false;
                }

                lblStatusText.Tag = dtTemp.Rows[0]["StatusID"].ToString();
                if (dtTemp.Rows[0]["StatusID"].ToInt32() == (Int32)OperationStatusType.GRNCancelled)
                {
                    BindingNavigatorCancelItem.Text = "Re Open";
                    dtTemp = MobjclsBLLDirectGRN.DisplayCancellationDetails(MobjclsBLLDirectGRN.PobjclsDTOGRN.lngGRNID);
                    if (dtTemp != null && dtTemp.Rows.Count > 0)
                    {
                        lblDescription.Visible = true;
                        txtDescription.Visible = true;
                        txtDescription.Text = dtTemp.Rows[0]["Description"].ToString();
                        lblCreatedByText.Text += "                 | Cancelled By :" + dtTemp.Rows[0]["CancelledByName"].ToString();
                        lblCreatedDateValue.Text += "                  | Cancelled Date :" + (Convert.ToDateTime(dtTemp.Rows[0]["Date"])).ToString("dd-MMM-yyyy");
                    }
                }
                else
                {
                    lblDescription.Visible = false;
                    txtDescription.Visible = false;
                    BindingNavigatorCancelItem.Text = "Cancel";
                }


                ChangeStatus();

                dgvGRN.Rows.Clear();

                dtTemp = MobjclsBLLDirectGRN.DisplayGRNDetail(MobjclsBLLDirectGRN.PobjclsDTOGRN.lngGRNID);
                if (dtTemp != null)
                {
                    for (int i = 0; i < dtTemp.Rows.Count; i++)
                    {
                        dgvGRN.RowCount = dgvGRN.RowCount + 1;
                        dgvGRN.Rows[i].Cells["ItemCode"].Value = dtTemp.Rows[i]["ItemCode"];
                        dgvGRN.Rows[i].Cells["ItemName"].Value = dtTemp.Rows[i]["Itemname"];
                        dgvGRN.Rows[i].Cells["ItemID"].Value = dtTemp.Rows[i]["ItemID"];

                        FillComboColumn(Convert.ToInt32(dtTemp.Rows[i]["ItemID"]), i);

                        dgvGRN.Rows[i].Cells["Uom"].Value = dtTemp.Rows[i]["UOMID"];
                        dgvGRN.Rows[i].Cells["Uom"].Tag = dtTemp.Rows[i]["UOMID"];
                        dgvGRN.Rows[i].Cells["Uom"].Value = dgvGRN.Rows[i].Cells["Uom"].FormattedValue;

                        int intUomScale = 0;
                        if (dgvGRN[Uom.Index, i].Tag.ToInt32() != 0)
                            intUomScale = MobjclsBLLDirectGRN.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvGRN[Uom.Index, i].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                        dgvGRN.Rows[i].Cells["Quantity"].Value = dtTemp.Rows[i]["ReceivedQuantity"].ToDecimal().ToString("F" + intUomScale);
                        dgvGRN.Rows[i].Cells["ExtraQty"].Value = dtTemp.Rows[i]["ExtraQuantity"].ToDecimal().ToString("F" + intUomScale); ;
                        if (dtTemp.Rows[i]["ExpiryDate"] != DBNull.Value)
                            dgvGRN.Rows[i].Cells["ExpiryDate"].Value = dtTemp.Rows[i]["ExpiryDate"];
                        dgvGRN.Rows[i].Cells["Batchnumber"].Value = dtTemp.Rows[i]["BatchNo"];
                        dgvGRN.Rows[i].Cells["Batchnumber"].Tag = dtTemp.Rows[i]["BatchID"];
                        dgvGRN.Rows[i].Cells["Rate"].Value = dtTemp.Rows[i]["PurchaseRate"];
                        dgvGRN.Rows[i].Cells["NetAmount"].Value = dtTemp.Rows[i]["NetAmount"];
                        CalculateTotalAmt();
                    }
                }
            }

        }
        private void DeletePurchaseGRN()
        {
            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 936, out MmessageIcon);
            if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                return;

            FillMasterParameters();
            FillDetailParameters();
            if (MobjclsBLLDirectGRN.DeleteGRN(Convert.ToInt64(txtGRNNo.Tag)))
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrFocus.Enabled = true;
                ClearControls();
                DisplayAllNoInSearchGrid();
            }
            else
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 934, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrFocus.Enabled = true;
            }
        }
        private int CheckDuplicationInGrid()
        {
            string SearchValuefirst = "";
            int RowIndexTemp = 0;
            foreach (DataGridViewRow rowValue in dgvGRN.Rows)
            {
                if (rowValue.Cells[ItemID.Index].Value != null)
                {
                    SearchValuefirst = Convert.ToString(rowValue.Cells[ItemID.Index].Value);
                    RowIndexTemp = rowValue.Index;
                    foreach (DataGridViewRow row in dgvGRN.Rows)
                    {
                        if (RowIndexTemp != row.Index)
                        {
                            if (row.Cells[ItemID.Index].Value != null)
                            {
                                if ((Convert.ToString(row.Cells[ItemID.Index].Value).Trim() == Convert.ToString(SearchValuefirst).Trim()))
                                    return row.Index;
                            }
                        }
                    }
                }
            }
            return -1;
        }
        private bool ValidateGrid()
        {
            int iGridRowCount = dgvGRN.RowCount - 1;

            for (int i = 0; i < iGridRowCount; i++)
            {
                int iRowIndex = dgvGRN.Rows[i].Index;

                if (!blnAddMode)
                {
                    if (!IsPurchaseInvoiceExists() && !IsPurchaseOrderExists())
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2226, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("*", "Updated");
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        tcPurchase.SelectedTab = tpItemDetails;
                        return false;
                    }
                }

                if (Convert.ToString(dgvGRN.Rows[i].Cells["ItemCode"].Value).Trim() == "")
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 903, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    dgvGRN.Focus();
                    dgvGRN.CurrentCell = dgvGRN["ItemCode", iRowIndex];
                    return false;
                }
                if (Convert.ToString(dgvGRN.Rows[i].Cells["ItemName"].Value).Trim() == "")
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 915, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    dgvGRN.Focus();
                    dgvGRN.CurrentCell = dgvGRN["ItemName", iRowIndex];
                    return false;
                }
                if (dgvGRN.Rows.Count > 0)
                {
                    int iTempID = 0;
                    iTempID = CheckDuplicationInGrid();
                    if (iTempID != -1)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 902, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        dgvGRN.CurrentCell = dgvGRN["ItemCode", iTempID];
                        dgvGRN.Focus();
                        return false;
                    }

                }

                if (Convert.ToString(dgvGRN.Rows[i].Cells["Quantity"].Value).Trim() == "" || Convert.ToString(dgvGRN.Rows[i].Cells["Quantity"].Value).Trim() == "." || Convert.ToDecimal(dgvGRN.Rows[i].Cells["Quantity"].Value) == 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 916, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    dgvGRN.Focus();
                    dgvGRN.CurrentCell = dgvGRN["Quantity", iRowIndex];
                    return false;
                }

                DataTable dtCostingMethod = MobjclsBLLDirectGRN.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + Convert.ToInt32(dgvGRN.Rows[i].Cells["ItemID"].Value) });
                if (dtCostingMethod != null && dtCostingMethod.Rows.Count > 0)
                {
                    if (dtCostingMethod.Rows[0]["CostingMethodID"].ToInt32() == (Int32)CostingMethodReference.Batch)
                    {
                        if (dgvGRN.Rows[i].Cells["Batchnumber"].Value == null || string.IsNullOrEmpty(dgvGRN.Rows[i].Cells["Batchnumber"].Value.ToString()))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 949, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            dgvGRN.Focus();
                            dgvGRN.CurrentCell = dgvGRN["Batchnumber", iRowIndex];
                            return false;
                        }
                        if (!IsValidBatch(dgvGRN.Rows[i].Cells["Batchnumber"].Value.ToString(), dgvGRN.Rows[i].Cells["ItemID"].Value.ToInt32()))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 979, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            dgvGRN.Focus();
                            dgvGRN.CurrentCell = dgvGRN["Batchnumber", iRowIndex];
                            return false;
                        }

                    }
                }
               
                if (dgvGRN.Rows[i].Cells["ExpiryDate"].Value != null) 
                {
                    DateTime dtExpiryDate = new DateTime();
                    try
                    {
                        dtExpiryDate = Convert.ToDateTime(dgvGRN.Rows[i].Cells["ExpiryDate"].Value);
                    }
                    catch
                    {

                    }
                    if (dtExpiryDate < dtpDate.Value)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 959, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("*", "GRN");
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        tcPurchase.SelectedTab = tpItemDetails;
                        dgvGRN.Focus();
                        dgvGRN.CurrentCell = dgvGRN["ExpiryDate", iRowIndex];
                        return false;
                    }
                }
                if (Convert.ToBoolean(MobjclsBLLDirectGRN.FillCombos(new string[] { "ExpiryDateMandatory", "InvItemMaster", "ItemID = " + dgvGRN.Rows[i].Cells["ItemID"].Value.ToInt32() }).Rows[0]["ExpiryDateMandatory"]) == true && (dgvGRN.Rows[i].Cells["ExpiryDate"].Value == DBNull.Value || dgvGRN.Rows[i].Cells["ExpiryDate"].Value == null))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 991, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    dgvGRN.Focus();
                    dgvGRN.CurrentCell = dgvGRN["ExpiryDate", iRowIndex];
                    return false;
                }
                if (Convert.ToInt32(dgvGRN.Rows[i].Cells["Uom"].Tag) == 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 917, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    dgvGRN.Focus();
                    dgvGRN.CurrentCell = dgvGRN["Uom", iRowIndex];
                    return false;
                }

            }
            return true;
        }

        private bool IsValidBatch(string strBatchNo, int intItemID)
        {
            DataTable dtBatchDetails = MobjclsBLLDirectGRN.FillCombos(new string[] { "BatchID,ItemID", "InvBatchDetails", "BatchNo = '" + strBatchNo + "' And ItemID <> " + intItemID });
            if (dtBatchDetails.Rows.Count > 0)
            {
                return false;
            }
            return true;
        }
        private bool ValidatePurchaseGRN()
        {
            if (cboCompany.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 14, out MmessageIcon);
                ErrPurchase.SetError(cboCompany, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrPurchase.Enabled = true;
                tcPurchase.SelectedTab = tpItemDetails;
                cboCompany.Focus();
                return false;
            }

            if (cboWarehouse.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 922, out MmessageIcon);
                ErrPurchase.SetError(cboWarehouse, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrPurchase.Enabled = true;
                tcPurchase.SelectedTab = tpItemDetails;
                cboWarehouse.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtGRNNo.Text.Trim()))
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 942, out MmessageIcon);
                ErrPurchase.SetError(txtGRNNo, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrPurchase.Enabled = true;
                tcPurchase.SelectedTab = tpItemDetails;
                txtGRNNo.Focus();
                return false;
            }

            if (cboSupplier.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 908, out MmessageIcon);
                ErrPurchase.SetError(cboSupplier, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrPurchase.Enabled = true;
                tcPurchase.SelectedTab = tpItemDetails;
                cboSupplier.Focus();
                return false;
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 939, out MmessageIcon);
                ErrPurchase.SetError(cboCurrency, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrPurchase.Enabled = true;
                tcPurchase.SelectedTab = tpItemDetails;
                cboCurrency.Focus();
                return false;
            }
            if (blnAddMode && txtGRNNo.ReadOnly && MobjclsBLLDirectGRN.IsGRNNoExists(txtGRNNo.Text, cboCompany.SelectedValue.ToInt32()))
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 973, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("*", "GRN");
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return false;
                else
                    GenerateOrderNo();
            }
            txtRemarks.Focus();
            dtpDate.Focus();
            if (dtpDate.Value.Date > ClsCommonSettings.GetServerDate())
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9006, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("*", "GRN");
                ErrPurchase.SetError(dtpDate, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrPurchase.Enabled = true;
                tcPurchase.SelectedTab = tpItemDetails;
                dtpDate.Focus();
                return false;
            }
            if (dgvGRN.Rows.Count == 1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 901, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrPurchase.Enabled = true;
                dgvGRN.Focus();
                return false;
            }
            if (!blnAddMode)
            {
                if (!MobjclsBLLDirectGRN.IsInvoiceOrOrderExists() || !MobjclsBLLDirectGRN.IsReferenceExists())
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2302, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    return false;
                }
            }
            return true;
        }

        
        private bool SavePurchaseGRN()
        {
            if (blnAddMode == true)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 925, out MmessageIcon);
            }
            else
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
            }
            if (MblnCanShow)
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;
                else
                {
                    if (!ValidateOnSave())
                        return false;
                }

            FillMasterParameters();
            FillDetailParameters();
            if (MobjclsBLLDirectGRN.SavePurchaseGRN())
            {
                return true;
            }
            return false;
        }

        private bool ValidateOnSave()
        {
            if (blnAddMode && txtGRNNo.ReadOnly && MobjclsBLLDirectGRN.IsGRNNoExists(txtGRNNo.Text, cboCompany.SelectedValue.ToInt32()))
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 973, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("*", "GRN");
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return false;
                else
                    GenerateOrderNo();
            }

            if (!blnAddMode)
            { 
                if (!IsPurchaseInvoiceExists() || !IsPurchaseOrderExists())
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2226, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Updated");
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    return false;
                }
            }

            return true;
        }
        private void CancelPurchaseGRN()
        {
            try
            {
                FillMasterParameters();
                FillDetailParameters();
                if (BindingNavigatorCancelItem.Text.ToUpper() == "CANCEL")
                {
                   
                    int intOldStatusID = MobjclsBLLDirectGRN.PobjclsDTOGRN.intStatusID;
                    MobjclsBLLDirectGRN.PobjclsDTOGRN.blnIsCancel = true;
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 955, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("***", "GRN");
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        btnActions.Enabled = false;
                        using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                        {
                            MblnCanShow = false;
                            objFrmCancellationDetails.ShowDialog();
                            MobjclsBLLDirectGRN.PobjclsDTOGRN.strCancellationDate = objFrmCancellationDetails.PDtCancellationDate.ToString("dd-MMM-yyyy");
                            txtDescription.Text = objFrmCancellationDetails.PStrDescription;
                            MobjclsBLLDirectGRN.PobjclsDTOGRN.intCancelledBy = ClsCommonSettings.UserID;
                            if (!objFrmCancellationDetails.PBlnIsCanelled)
                            {
                                BindingNavigatorSaveItem.Enabled = false;
                                DisplayPurchaseGRN();
                                MobjclsBLLDirectGRN.PobjclsDTOGRN.intStatusID = (Int32)OperationStatusType.GRNCancelled;
                                lblStatusText.Tag = MobjclsBLLDirectGRN.PobjclsDTOGRN.intStatusID;
                                MblnIsFromCancellation = true;

                                if (SavePurchaseGRN())
                                {
                                    DisplayPurchaseGRN();
                                }
                                else
                                {
                                    MobjclsBLLDirectGRN.PobjclsDTOGRN.intStatusID = intOldStatusID;
                                }
                                MblnIsFromCancellation = false;
                            }
                        }
                    }
                }
                else
                {
                    MobjclsBLLDirectGRN.PobjclsDTOGRN.blnIsCancel = false;
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 956, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("***", "GRN");

                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        MblnCanShow = false;
                        MobjclsBLLDirectGRN.PobjclsDTOGRN.intStatusID = (int)OperationStatusType.GRNOpened;
                        MblnIsFromCancellation = true;
                        DisplayPurchaseGRN();
                        BindingNavigatorSaveItem.Enabled = false;
                        lblStatusText.Tag = MobjclsBLLDirectGRN.PobjclsDTOGRN.intStatusID;
                        if (SavePurchaseGRN())
                        {
                            DisplayPurchaseGRN();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorCancelItem_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in BindingNavigatorCancelItem_Click() " + ex.Message, 2);
            }
        }

        private void CalculateTotalAmt()
        {
            decimal decQty = 0;
            decimal decRate = 0;
            decimal decNetAmount = 0;

            int iGridRowCount = dgvGRN.RowCount;
            iGridRowCount = iGridRowCount - 1;
            for(int iCounter = 0; iCounter < iGridRowCount;iCounter ++)
            {
                decRate = decQty = 0;
                decQty = Convert.ToDecimal(dgvGRN.Rows[iCounter].Cells[Quantity.Index].Value);
                if (dgvGRN.Rows[iCounter].Cells[Rate.Index].Value != null)
                    decRate = Convert.ToDecimal(dgvGRN.Rows[iCounter].Cells[Rate.Index].Value);


                dgvGRN.Rows[iCounter].Cells[NetAmount.Index].Value = (decQty * decRate).ToString("F" + MintBaseCurrencyScale);
                if(dgvGRN.Rows[iCounter].Cells[NetAmount.Index].Value != null)
                decNetAmount = decNetAmount + Convert.ToDecimal(dgvGRN.Rows[iCounter].Cells[NetAmount.Index].Value);
            }
            txtTotalAmount.Text = decNetAmount.ToString("F" + MintBaseCurrencyScale);
            txtSubTotal.Text = decNetAmount.ToString("F" + MintBaseCurrencyScale);
            CalculateExchangeCurrencyRate();
        }

        private void CalculateExchangeCurrencyRate()
        {
            int intCompanyID = 0, intCurrencyID = 0;

            intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            intCurrencyID = Convert.ToInt32(cboCurrency.SelectedValue);
           
            if (MobjclsBLLDirectGRN.GetExchangeCurrencyRate(intCompanyID, intCurrencyID))
            {
                txtExchangeCurrencyRate.Text = Convert.ToDecimal(MobjclsBLLDirectGRN.PobjclsDTOGRN.decExchangeCurrencyRate * Convert.ToDecimal(txtTotalAmount.Text)).ToString("F" + MintBaseCurrencyScale);
            }
            string strAmount = txtTotalAmount.Text;

            lblAmountinWords.Text = new clsBLLCommonUtility().ConvertToWord(strAmount, intCurrencyID);
        }

        private void GetCurrencyScale()
        {
            MintBaseCurrencyScale = ClsCommonUtility.GetCurrencyScaleByCompany(cboCompany.SelectedValue.ToInt32());

        }


        #endregion

        #region Events

        private void FrmDirectGRN_Load(object sender, EventArgs e)
        {
            try
            {
                SetPermissions();
                ClearControls();
                LoadMessage();
                cboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FrmPurchaseLoad() " + ex.Message);
                MObjLogs.WriteLog("Error in FrmPurchaseLoad() " + ex.Message, 2);
            }
        }

       
        private void btnSRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                DisplayAllNoInSearchGrid();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnSRefreshClick() " + ex.Message);
                MObjLogs.WriteLog("Error in btnSRefreshClick() " + ex.Message, 2);
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            ClearControls();
            LoadCombos(3);
            LoadCombos(5);
        }

        private void cboSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblSupplierAddressName.Text = "";
                if(cboSupplier.SelectedValue.ToInt32() > 0)
                {
                    LoadCombos(9);
                    AddToAddressMenu(Convert.ToInt32(cboSupplier.SelectedValue));
                    BtnTVenderAddress.Text = CMSVendorAddress.Items[0].Text;
                    lblSupplierAddressName.Text = CMSVendorAddress.Items[0].Text + " Address";
                    MintVendorAddID = Convert.ToInt32(CMSVendorAddress.Items[0].Tag);
                    DisplayAddress(Convert.ToInt32(cboSupplier.SelectedValue));
                    ChangeStatus();

                }
                    
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in cboSupplierSelectedIndexChanged() " + ex.Message);
                MObjLogs.WriteLog("Error in cboSupplier_SelectedIndexChanged() " + ex.Message, 2);
            }
        }


        private void CancelToolStripButton_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

       
        private void cb_KeyPress(object sender, KeyPressEventArgs e)
        {
            cb.DroppedDown = false;
        }


        private void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
            {
                e.Handled = true;
            }
            if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            {
                e.Handled = true;
            }

        }

        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void TmrFocus_Tick(object sender, EventArgs e)
        {
            cboCompany.Focus();
            TmrFocus.Enabled = false;
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            int intBaseCurrencyScale;
            int intCurrencyID;
            LoadCombos(5);
            GenerateOrderNo();
            GetCurrencyScale();
            dgvGRN.Rows.Clear();
            ChangeStatus();
            if (Convert.ToInt32(cboCompany.SelectedValue) > 0)
            {
                lblCompanyCurrencyAmount.Text = "Amount in " + new ClsCommonUtility().GetCompanyCurrency(Convert.ToInt32(cboCompany.SelectedValue), out intBaseCurrencyScale,out intCurrencyID);
            }
            else
            {
                lblCompanyCurrencyAmount.Text = "Amount in company currency";
            }

        }

        private void btnSupplier_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(cboSupplier.SelectedValue);
                int iVendorCurID = MobjclsBLLDirectGRN.GetVendorRecordID(MintComboID);
                using (FrmVendor objVendor = new FrmVendor(2))
                {
                    objVendor.PintVendorID = MintComboID;
                    objVendor.ShowDialog();
                    LoadCombos(3);
                    if (objVendor.PintVendorID != 0 && cboSupplier.Enabled)
                        cboSupplier.SelectedValue = objVendor.PintVendorID;
                    else
                        cboSupplier.SelectedValue = MintComboID;
                }
                
                if (Convert.ToInt32(cboSupplier.SelectedValue) > 0)
                    AddToAddressMenu(Convert.ToInt32(cboSupplier.SelectedValue));

                ChangeStatus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnSupplier_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in btnSupplier_Click() " + ex.Message, 2);
            }
        }

        private void BtnUom_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmUnitOfMeasurement objUoM = new FrmUnitOfMeasurement())
                {
                    objUoM.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnUom_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in BtnUom_Click() " + ex.Message, 2);
            }
        }

        private void TxtTVendorAddress_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();

        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }
       

        private void btnWarehouse_Click(object sender, EventArgs e)
        {

            try
            {
                MintComboID = Convert.ToInt32(cboWarehouse.SelectedValue);
                using (FrmWarehouse objCreateWarehouse = new FrmWarehouse())
                {
                    objCreateWarehouse.PintWareHouse = Convert.ToInt32(cboWarehouse.SelectedValue);
                    objCreateWarehouse.ShowDialog();
                    LoadCombos(5);
                    if (objCreateWarehouse.PintWareHouse != 0)
                        cboWarehouse.SelectedValue = objCreateWarehouse.PintWareHouse;
                    else
                        cboWarehouse.SelectedValue = MintComboID;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnWarehouse_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in btnWarehouse_Click() " + ex.Message, 2);
            }
            
        }

        private void cboSupplier_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboSupplier.DroppedDown = false;
        }

      
        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (!MobjclsBLLDirectGRN.PobjclsDTOGRN.blnCancelled)
            {
                if (MobjclsBLLDirectGRN.IsInvoiceOrOrderExists() && MobjclsBLLDirectGRN.IsReferenceExists())
                {
                    DeletePurchaseGRN();
                }
                else
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2226, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Deleted");
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    return;
                }
            }
            else
                DeletePurchaseGRN();
        }


        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup objEmailPopUp = new FrmEmailPopup())
                {
                    objEmailPopUp.MsSubject = " Direct GRN Information";
                    objEmailPopUp.EmailFormType = EmailFormID.DirectGRN;
                    objEmailPopUp.EmailSource = MobjclsBLLDirectGRN.GetGRNReport();
                    objEmailPopUp.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnEmail_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in BtnEmail_Click() " + ex.Message, 2);
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (MobjclsBLLDirectGRN.PobjclsDTOGRN.lngGRNID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MobjclsBLLDirectGRN.PobjclsDTOGRN.lngGRNID;
                    ObjViewer.PiFormID = (int)FormID.DirectGRN;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnPrint_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in BtnPrint_Click() " + ex.Message, 2);
            }
        }

        private void BindingNavigatorCancelItem_Click(object sender, EventArgs e)
        {
            if (BindingNavigatorCancelItem.Text == "Cancel")
            {
                if (MobjclsBLLDirectGRN.IsInvoiceOrOrderExists() && MobjclsBLLDirectGRN.IsReferenceExists())
                {
                    CancelPurchaseGRN();
                }
                else
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2226, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Cancelled");
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    return;
                }
            }
            else
                CancelPurchaseGRN();
        }




        private void TxtSsearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\'')
                e.Handled = true;
        }


        private void TmrPurchaseOrder_Tick(object sender, EventArgs e)
        {
            ErrPurchase.Clear();
            lblPurchasestatus.Text = "";
            TmrPurchase.Enabled = false;
        }


        private void btnAddressChange_Click(object sender, EventArgs e)
        {
            ChangeStatus();
            CMSVendorAddress.Show(btnTContextmenu, btnTContextmenu.PointToClient(System.Windows.Forms.Cursor.Position));
        }

        private void txtPurchaseNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetterOrDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
                e.Handled = true;
            else
                e.Handled = false;
        }

        private void cboSCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboSCompany.SelectedValue == null || Convert.ToInt32(cboSCompany.SelectedValue) == 0)
            {
                LoadCombos(7);
            }
            else
            {
                LoadCombos(8);
            }
        }
     

        private void btnAccountSettings_Click(object sender, EventArgs e)
        {
            using (FrmAccountSettings objFrmAccountSettings = new FrmAccountSettings())
            {
                objFrmAccountSettings.ShowDialog();
            }
        }

        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        #endregion

        private void MenuItem_Click(System.Object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(cboSupplier.SelectedValue) > 0)
                {
                    BtnTVenderAddress.Text = e.ClickedItem.Text;
                    lblSupplierAddressName.Text = e.ClickedItem.Text + " Address";
                    MintVendorAddID = Convert.ToInt32(e.ClickedItem.Tag);
                    DisplayAddress(Convert.ToInt32(cboSupplier.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in MenuItem_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in MenuItem_Click() " + ex.Message, 2);
            }
        }

     
        private void expandableSplitterLeft_ExpandedChanged(object sender, ExpandedChangeEventArgs e)
        {
            if (expandableSplitterLeft.Expanded)
            {
                if (dgvGRNDisplay.Columns.Count > 0)
                    dgvGRNDisplay.Columns[0].Visible = false;
            }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "GRN";
            objHelp.ShowDialog();
            objHelp = null;
        }


        private void lnkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DataTable datAdvanceSearchedData = new DataTable();
            using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.GRN))
            {
                objFrmSearchForm.ShowDialog();
                if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                {
                    datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                    datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                    dgvGRNDisplay.DataSource = datAdvanceSearchedData;
                    dgvGRNDisplay.Columns["ID"].Visible = false;
                    dgvGRNDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

        private void dgvGRN_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {

                dgvGRN.EndEdit();

                if (e.ColumnIndex == Uom.Index)
                {
                    dgvGRN.CurrentRow.Cells["Uom"].Tag = dgvGRN.CurrentRow.Cells["Uom"].Value;
                    dgvGRN.CurrentRow.Cells["Uom"].Value = dgvGRN.CurrentRow.Cells["Uom"].FormattedValue;
                }
            }
            if (e.ColumnIndex == Quantity.Index || e.ColumnIndex == Rate.Index)
            {
                CalculateTotalAmt();
            }

        }


        private void dgvGRN_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == ItemCode.Index) || (e.ColumnIndex == ItemName.Index))
            {
                dgvGRN.PiColumnIndex = e.ColumnIndex;
            }
        }

        private void dgvGRN_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {

            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                if (cboCompany.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 14, out MmessageIcon);
                    ErrPurchase.SetError(cboCompany, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    cboCompany.Focus();
                    e.Cancel = true;
                    return ;
                }

                if (cboWarehouse.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 922, out MmessageIcon);
                    ErrPurchase.SetError(cboWarehouse, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    cboWarehouse.Focus();
                    e.Cancel = true;
                    return ;
                }
                if (string.IsNullOrEmpty(txtGRNNo.Text.Trim()))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 942, out MmessageIcon);
                    ErrPurchase.SetError(txtGRNNo, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    txtGRNNo.Focus();
                    e.Cancel = true;
                    return ;
                }

                if (cboSupplier.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 908, out MmessageIcon);
                    ErrPurchase.SetError(cboSupplier, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    cboSupplier.Focus();
                    e.Cancel = true;
                    return ;
                }

                if (e.ColumnIndex == Batchnumber.Index)
                {
                    e.Cancel = true;
                    return;
                }
               
            }
            dgvGRN.CommitEdit(DataGridViewDataErrorContexts.Commit);

            if (e.ColumnIndex == Uom.Index )
            {
                if (dgvGRN.CurrentRow.Cells["ItemID"].Value != null)
                {
                    int iItemID = Convert.ToInt32(dgvGRN.CurrentRow.Cells["ItemID"].Value);
                    int tag = -1;
                    if (e.ColumnIndex == Uom.Index)
                    {
                        if (dgvGRN.CurrentRow.Cells["Uom"].Tag != null)
                        {
                            tag = Convert.ToInt32(dgvGRN.CurrentRow.Cells["Uom"].Tag);
                        }
                        FillComboColumn(iItemID, 1);
                        if (tag != -1)
                            dgvGRN.CurrentRow.Cells["Uom"].Tag = tag;
                    }
                }
                else
                {
                    Uom.DataSource = null;
                }

            }
        }

        private void dgvGRN_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {

                if (dgvGRN.Columns[e.ColumnIndex].Name == "ItemID")
                {
                    if (dgvGRN.Rows[e.RowIndex].Cells["ItemID"].Value != null)
                    {
                        int intItemID = dgvGRN.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt32();

                        if (intItemID > 0)
                        {
                            if (blnAddMode)
                            {
                                dgvGRN["Quantity", e.RowIndex].Value =  1.ToString("F"+ MintBaseCurrencyScale);
                                dgvGRN["ExtraQty", e.RowIndex].Value = 0.ToString("F"+ MintBaseCurrencyScale);
                                dgvGRN["Rate", e.RowIndex].Value = 0.ToString("F" + MintBaseCurrencyScale);
                                dgvGRN["NetAmount", e.RowIndex].Value = 0.ToString("F" + MintBaseCurrencyScale);
                            }

                            int tag = -1;
                            if (dgvGRN.Rows[e.RowIndex].Cells["Uom"].Tag != null)
                            {
                                tag = Convert.ToInt32(dgvGRN.Rows[e.RowIndex].Cells["Uom"].Tag);
                            }
                            FillComboColumn(intItemID, 1);
                            if (tag == -1)
                            {
                                DataTable datDefaultUom = MobjclsBLLDirectGRN.FillCombos(new string[] { "DefaultPurchaseUomID as UomID", "InvItemMaster", "ItemID = " + intItemID + "" });
                                tag = Convert.ToInt32(datDefaultUom.Rows[0]["UomID"]);
                            }
                            dgvGRN.Rows[e.RowIndex].Cells["Uom"].Value = tag;
                            dgvGRN.Rows[e.RowIndex].Cells["Uom"].Tag = tag;
                            dgvGRN.Rows[e.RowIndex].Cells["Uom"].Value = dgvGRN.Rows[e.RowIndex].Cells["Uom"].FormattedValue;

                            if (dgvGRN.Rows[e.RowIndex].Cells["ItemID"].Value != null && dgvGRN.Rows[e.RowIndex].Cells["Batchnumber"].Tag.ToInt32() == 0)
                            {
                                intItemID = Convert.ToInt32(dgvGRN.Rows[e.RowIndex].Cells["ItemID"].Value);
                                DataTable dtCostingMethod = MobjclsBLLDirectGRN.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + intItemID });
                                if (dtCostingMethod != null && dtCostingMethod.Rows.Count > 0)
                                {
                                    if (dtCostingMethod.Rows[0]["CostingMethodID"].ToInt32() == (Int32)CostingMethodReference.Batch)
                                    {
                                        dgvGRN["Batchnumber", e.RowIndex].Value = dgvGRN["ItemCode", e.RowIndex].Value.ToStringCustom() + ClsCommonSettings.GetServerDate().ToString("dd/MMM/yyyy") + random.Next(10000).ToString();
                                    }
                                    else
                                    {
                                        dgvGRN["Batchnumber", e.RowIndex].Value = string.Empty;
                                    }
                                }

                            }

                        }
                    }
                }
                ChangeStatus();
            }
        }


        private void dgvGRN_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvGRN.IsCurrentCellDirty)
            {
                if (dgvGRN.CurrentCell != null)
                    dgvGRN.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
        }

        private void dgvGRN_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            this.dgvGRN.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (dgvGRN.CurrentCell.OwningColumn.Name == "Quantity" || dgvGRN.CurrentCell.OwningColumn.Name == "ExtraQty")
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
                int intUomScale = 0;
                if (dgvGRN[Uom.Index, dgvGRN.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    intUomScale = MobjclsBLLDirectGRN.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvGRN[Uom.Index, dgvGRN.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    if (intUomScale == 0)
                    {
                        if (e.KeyChar == '.')
                            e.Handled = true;
                    }
                    else
                    {
                        int dotIndex = -1;
                        if (txt.Text.Contains("."))
                        {
                            dotIndex = txt.Text.IndexOf('.');
                        }
                        if (e.KeyChar == '.')
                        {
                            if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            {
                                e.Handled = true;
                            }
                        }
                        else
                        {
                            if (char.IsDigit(e.KeyChar))
                            {
                                if (dotIndex != -1 && txt.SelectionStart > dotIndex)
                                {
                                    string[] splitText = txt.Text.Split('.');
                                    if (splitText.Length == 2)
                                    {
                                        if (splitText[1].Length - txt.SelectedText.Length >= intUomScale)
                                            e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (dgvGRN.CurrentCell.OwningColumn.Name == "Rate")
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (dgvGRN.EditingControl != null && !string.IsNullOrEmpty(dgvGRN.EditingControl.Text) && dgvGRN.EditingControl.Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true;
                }
            }
            else if (dgvGRN.CurrentCell.OwningColumn.Name == "ItemCode" || dgvGRN.CurrentCell.OwningColumn.Name == "ItemName")
            {
                if (e.KeyChar.ToString() == "'")
                {
                    e.Handled = true;
                }
            }
        }

        private void dgvGRN_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjclsBLLCommonUtility.SetSerialNo(dgvGRN, e.RowIndex, false);
        }
       
        private void dgvGRN_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjclsBLLCommonUtility.SetSerialNo(dgvGRN, e.RowIndex, false);
        }


        private void dgvGRN_Textbox_TextChanged(object sender, EventArgs e)
        {
            dgvGRN.PServerName = ClsCommonSettings.ServerName;
            string[] First = { "ItemCode", "ItemName", "ItemID" };//first grid 
            string[] second = { "ItemCode", "Description", "ItemID" };//inner grid
            dgvGRN.ColumnsToHide = new string[] { "ItemID" };


            dgvGRN.aryFirstGridParam = First;
            dgvGRN.arySecondGridParam = second;
            dgvGRN.PiFocusIndex = Quantity.Index;
            dgvGRN.iGridWidth = 350;
            dgvGRN.bBothScrollBar = true;
            dgvGRN.pnlLeft = expandableSplitterLeft.Location.X + 5;
            dgvGRN.pnlTop = expandableSplitterTop.Location.Y + PurchaseOrderBindingNavigator.Height + 10;

            if (dgvGRN.CurrentCell.ColumnIndex == ItemCode.Index)
            {
                dgvGRN.field = "Code";
                dgvGRN.CurrentCell.Value = dgvGRN.TextBoxText;
            }
            if (dgvGRN.CurrentCell.ColumnIndex == ItemName.Index)
            {
                dgvGRN.field = "ItemName";
                dgvGRN.CurrentCell.Value = dgvGRN.TextBoxText;
            }

            dgvGRN.PsQuery = "select IM.[Code] as ItemCode,IM.[ItemName] as Description,IM.[ItemID] as ItemID from InvItemMaster IM INNER JOIN InvItemDetails ID ON IM.ItemID = ID.ItemID where Upper(IM." + dgvGRN.field + ") like '%" + ((dgvGRN.TextBoxText.ToUpper())) + "%'  And IM.StatusID In( " + (int)OperationStatusType.ProductActive + ")";
            dgvGRN.PsQuery += " And IsNull(CompanyID,0) In (0," + ClsCommonSettings.CompanyID + ") AND ID.ProductTypeID <> 3 Order By CHARINDEX('" + dgvGRN.TextBoxText.ToUpper() + "',UPPER(ItemName))";

        }

    
        private void dgvGRNDisplay_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ClearControls();
            blnAddMode = false;
            if (dgvGRNDisplay.CurrentRow != null)
            {
                MobjclsBLLDirectGRN.PobjclsDTOGRN.lngGRNID = dgvGRNDisplay.CurrentRow.Cells[0].Value.ToInt64();
                DisplayPurchaseGRN();
            }
        }


        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (ValidatePurchaseGRN() && ValidateGrid())
            {
                if (SavePurchaseGRN())
                {
                    long lngGRNID = MobjclsBLLDirectGRN.PobjclsDTOGRN.lngGRNID;
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ClearControls();
                    MobjclsBLLDirectGRN.PobjclsDTOGRN.lngGRNID = lngGRNID;
                    DisplayPurchaseGRN();
                    DisplayAllNoInSearchGrid();
                }
            }
        }

        private void btnPurchaseOrder_Click(object sender, EventArgs e)
        {
            if (!IsPurchaseOrderExists())
            {
                MstrMessageCommon = " Order Or Invoice Exists For " + txtGRNNo.Text.Trim();
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrPurchase.Enabled = true;
                tcPurchase.SelectedTab = tpItemDetails;
                return ;
            }
            else
            {
                FrmPurchaseOrder objFrmPurchaseOrder = new FrmPurchaseOrder(MobjclsBLLDirectGRN.PobjclsDTOGRN.lngGRNID);
                objFrmPurchaseOrder.WindowState = FormWindowState.Maximized;
                objFrmPurchaseOrder.MdiParent = this.ParentForm;
                objFrmPurchaseOrder.Show();
            }
          
        }

       
        private void txtRemarks_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void FrmDirectGRN_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.MdiFormClosing)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4214, out MmessageIcon).Replace("#", "").Trim();
                if (MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void FrmDirectGRN_Shown(object sender, EventArgs e)
        {
            cboCompany.Focus();
        }

        private void cboWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
            dgvGRN.Rows.Clear();
        }

        private void btnPurchaseInvoice_Click(object sender, EventArgs e)
        {
            if (!IsPurchaseInvoiceExists())
            {
                MstrMessageCommon = " Order Or Invoice Exists For " + txtGRNNo.Text.Trim();
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrPurchase.Enabled = true;
                tcPurchase.SelectedTab = tpItemDetails;
                return ;
            }
            else
            {
                FrmPurchaseInvoice objFrmPurchaseInvoice = new FrmPurchaseInvoice(MobjclsBLLDirectGRN.PobjclsDTOGRN.lngGRNID);
                objFrmPurchaseInvoice.WindowState = FormWindowState.Maximized;
                objFrmPurchaseInvoice.MdiParent = this.ParentForm;
                objFrmPurchaseInvoice.Show();
            }
        }

        private void cboSExecutive_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboSExecutive.DroppedDown = false;
        }

        private void cboSStatus_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboSStatus.DroppedDown = false;
        }

        private void cboSSupplier_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboSSupplier.DroppedDown = false;
        }

        private void cboSCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboSCompany.DroppedDown = false;
        }

        private void btnTContextmenu_Click(object sender, EventArgs e)
        {
            ChangeStatus();
            CMSVendorAddress.Show(btnTContextmenu, btnTContextmenu.PointToClient(System.Windows.Forms.Cursor.Position));
        }

       
    }
}
