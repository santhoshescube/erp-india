﻿namespace MyBooksERP
{
    partial class FrmCancellationDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCancellationDetails));
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.dtpCancelledDate = new System.Windows.Forms.DateTimePicker();
            this.txtDescription = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblCancelledDate = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.cboVerificationCriteria = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblVerificationCriteria = new DevComponents.DotNetBar.LabelX();
            this.btnVerificationCriteria = new System.Windows.Forms.Button();
            this.groupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(224, 151);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(301, 151);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupPanel1
            // 
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.dtpCancelledDate);
            this.groupPanel1.Controls.Add(this.txtDescription);
            this.groupPanel1.Controls.Add(this.lblCancelledDate);
            this.groupPanel1.Controls.Add(this.lblDescription);
            this.groupPanel1.Location = new System.Drawing.Point(12, 42);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(364, 103);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.Class = "";
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.Class = "";
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.Class = "";
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 251;
            this.groupPanel1.Text = "Please Enter Description";
            // 
            // dtpCancelledDate
            // 
            this.dtpCancelledDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpCancelledDate.Enabled = false;
            this.dtpCancelledDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCancelledDate.Location = new System.Drawing.Point(127, 25);
            this.dtpCancelledDate.Name = "dtpCancelledDate";
            this.dtpCancelledDate.Size = new System.Drawing.Size(111, 20);
            this.dtpCancelledDate.TabIndex = 1;
            this.dtpCancelledDate.Visible = false;
            // 
            // txtDescription
            // 
            // 
            // 
            // 
            this.txtDescription.Border.Class = "TextBoxBorder";
            this.txtDescription.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDescription.Location = new System.Drawing.Point(3, 3);
            this.txtDescription.MaxLength = 300;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(352, 76);
            this.txtDescription.TabIndex = 0;
            // 
            // lblCancelledDate
            // 
            this.lblCancelledDate.AutoSize = true;
            this.lblCancelledDate.Location = new System.Drawing.Point(41, 28);
            this.lblCancelledDate.Name = "lblCancelledDate";
            this.lblCancelledDate.Size = new System.Drawing.Size(80, 13);
            this.lblCancelledDate.TabIndex = 251;
            this.lblCancelledDate.Text = "Cancelled Date";
            this.lblCancelledDate.Visible = false;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(41, 43);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 13);
            this.lblDescription.TabIndex = 252;
            this.lblDescription.Text = "Description";
            this.lblDescription.Visible = false;
            // 
            // cboVerificationCriteria
            // 
            this.cboVerificationCriteria.DisplayMember = "Text";
            this.cboVerificationCriteria.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboVerificationCriteria.FormattingEnabled = true;
            this.cboVerificationCriteria.ItemHeight = 14;
            this.cboVerificationCriteria.Location = new System.Drawing.Point(133, 16);
            this.cboVerificationCriteria.Name = "cboVerificationCriteria";
            this.cboVerificationCriteria.Size = new System.Drawing.Size(216, 20);
            this.cboVerificationCriteria.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboVerificationCriteria.TabIndex = 252;
            this.cboVerificationCriteria.Visible = false;
            // 
            // lblVerificationCriteria
            // 
            // 
            // 
            // 
            this.lblVerificationCriteria.BackgroundStyle.Class = "";
            this.lblVerificationCriteria.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblVerificationCriteria.Location = new System.Drawing.Point(18, 15);
            this.lblVerificationCriteria.Name = "lblVerificationCriteria";
            this.lblVerificationCriteria.Size = new System.Drawing.Size(104, 23);
            this.lblVerificationCriteria.TabIndex = 253;
            this.lblVerificationCriteria.Text = "Verification Criteria";
            this.lblVerificationCriteria.Visible = false;
            // 
            // btnVerificationCriteria
            // 
            this.btnVerificationCriteria.Location = new System.Drawing.Point(351, 16);
            this.btnVerificationCriteria.Name = "btnVerificationCriteria";
            this.btnVerificationCriteria.Size = new System.Drawing.Size(25, 20);
            this.btnVerificationCriteria.TabIndex = 254;
            this.btnVerificationCriteria.Text = "...";
            this.btnVerificationCriteria.UseVisualStyleBackColor = true;
            this.btnVerificationCriteria.Visible = false;
            this.btnVerificationCriteria.Click += new System.EventHandler(this.btnVerificationCriteria_Click);
            // 
            // FrmCancellationDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 180);
            this.Controls.Add(this.btnVerificationCriteria);
            this.Controls.Add(this.lblVerificationCriteria);
            this.Controls.Add(this.cboVerificationCriteria);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCancellationDetails";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cancellation Details";
            this.Load += new System.EventHandler(this.FrmCancellationDetails_Load);
            this.Shown += new System.EventHandler(this.FrmCancellationDetails_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCancellationDetails_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmCancellationDetails_KeyDown);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        public System.Windows.Forms.DateTimePicker dtpCancelledDate;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDescription;
        public System.Windows.Forms.Label lblCancelledDate;
        private System.Windows.Forms.Label lblDescription;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboVerificationCriteria;
        private DevComponents.DotNetBar.LabelX lblVerificationCriteria;
        private System.Windows.Forms.Button btnVerificationCriteria;
    }
}