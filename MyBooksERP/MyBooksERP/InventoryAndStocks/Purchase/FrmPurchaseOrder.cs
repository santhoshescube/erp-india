using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data;
using System.Data.SqlClient;


using Microsoft.ReportingServices.ReportRendering;
using System.Collections.Generic;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,23 Feb 2011>
   Description:	<Description,,Purchase Form>
================================================
*/
namespace MyBooksERP
{
    /// <summary>
    /// Summary description for frmDocument.
    /// </summary>
    public class FrmPurchaseOrder : DevComponents.DotNetBar.Office2007Form
    {

        #region Designer

        ComboBox cb;
        public Command CommandZoom;
        private DockContainerItem dockContainerItem1;
        private ExpandableSplitter expandableSplitterLeft;
        private PanelEx PanelLeft;
        private IContainer components;
        private DotNetBarManager dotNetBarManager1;
        private DockSite dockSite4;
        private DockSite dockSite1;
        private DockSite dockSite2;
        private DockSite dockSite3;
        private DockSite dockSite5;
        private DockSite dockSite6;
        private Bar bar1;
        private DockSite dockSite8;
        private PanelEx panelEx1;
        private Bar PurchaseOrderBindingNavigator;
        private ButtonItem BindingNavigatorAddNewItem;
        private ExpandableSplitter expandableSplitterTop;
        private PanelEx panelTop;
        private ExpandablePanel expandablePanel1;
        private LabelX lblPurchaseCount;
        private PanelEx panelBottom;
        private PanelEx panelGridBottom;
        private Label lblRemarks;
        private Label lblPaymentTerms;
        private PanelEx panelLeftTop;
        private Label lblSCompany;
        private Label lblSSupplier;
        private Label LblSStatus;
        private ClsInnerGridBar dgvPurchase;
        private ButtonItem BindingNavigatorSaveItem;
        private ButtonItem BindingNavigatorClearItem;
        private ButtonItem BindingNavigatorDeleteItem;
        private ButtonItem BtnItem;
        private ButtonItem BtnDiscount;
        private ButtonItem BtnTax;
        private ButtonItem BtnUom;
        private ButtonX btnSRefresh;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSPurchaseNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSStatus;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSSupplier;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSCompany;
        private Label lblWarehouse;
        private System.Windows.Forms.CheckBox ChkTGNR;
        private Label lblSupplierBillNo;
        private Label lblOrderType;
        private Label lblCompany;
        private System.Windows.Forms.DateTimePicker dtpDueDate;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private Label lblStatus;
        private Label lblDueDate;
        private Label lblDate;
        private Label lblPurchaseNo;
        internal ContextMenuStrip CMSVendorAddress;
        internal Timer TmrPurchase;
        internal Timer TmrFocus;
        internal ErrorProvider ErrPurchase;
        private ButtonItem BtnPrint;
        private ButtonItem BtnEmail;
        private ButtonX btnWarehouse;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private ButtonX btnCurrency;
        private Label lblCurrency;
        private Label lblDescription;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDescription;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private ImageList ImgPurchase;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private ButtonItem BindingNavigatorCancelItem;
        private Label lblStatusText;
        private ButtonX BtnTVenderAddress;
        private ButtonX btnSupplier;
        private System.Windows.Forms.TextBox txtSupplierAddress;
        internal Button btnTContextmenu;
        private Label lblSupplierAddressName;
        private Label lblSupplier;
        private ButtonItem btnActions;
        private DemoClsDataGridview.CalendarColumn calendarColumn1;
        Random random = new Random();
        private ButtonItem btnApprove;
        private ButtonItem btnReject;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboOrderType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboOrderNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCurrency;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSupplier;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSupplierBillNo;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPurchaseNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboPaymentTerms;
        private DevComponents.DotNetBar.Controls.TextBoxX txtRemarks;
        private ButtonItem bnExpense;
        private ButtonItem btnDocuments;
        private ButtonItem bnSubmitForApproval;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSExecutive;
        private Label lblExecutive;
        private ButtonX btnAddressChange;
        private ButtonItem btnAccountSettings;

        public Int64 intPurchaseIDToLoad = 0;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvPurchaseDisplay;
        private System.Windows.Forms.DateTimePicker dtpSTo;
        private System.Windows.Forms.DateTimePicker dtpSFrom;
        private Label lblSPurchaseNo;
        private Label lblTo;
        private Label lblFrom;
        private System.Windows.Forms.DateTimePicker dtpClearenceDate;
        private Label lblLeadTime;
        private Label lblClearenceDate;
        private Label lblTransShipmentDate;
        private System.Windows.Forms.DateTimePicker dtpTrnsShipmentDate;
        private Label lblProductionDate;
        private System.Windows.Forms.DateTimePicker dtpProductionDate;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboIncoTerms;
        private Label lblIncoTerms;
        private NumericTextBox txtLeadTime;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboContainerType;
        private Label lblContainerType;
        private Label lblNoOfContainers;
        private NumericTextBox txtNoOfContainers;
        private ButtonX btnContainerType;
        private ButtonX btnIncoTerms;
        private Label lblCriteria;
        private Label lblCriteriaText;
        private ButtonItem btnSupplierHistory;
        private PanelEx pnlAmount;
        private Label LblBDiscount;
        private Label lblTotalAmount;
        private Label lblCompanyCurrencyAmount;
        private Label lblExpenseAmount;
        private Label lblSubTotal;
        private Label lblAdvancePayment;
        private DevComponents.DotNetBar.Controls.TextBoxX txtExchangeCurrencyRate;
        private DevComponents.DotNetBar.Controls.TextBoxX txtAdvPayment;
        private ButtonX btnAddExpense;
        private DevComponents.DotNetBar.Controls.TextBoxX txtExpenseAmount;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDiscount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDiscount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSubTotal;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTotalAmount;
        private ButtonItem btnSuggest;
        private ButtonItem btnDeny;
        private ButtonItem btnShowComments;
        private DevComponents.DotNetBar.TabControl tcPurchase;
        private TabControlPanel tabControlPanel2;
        private ClsInnerGridBar dgvLocationDetails;
        private TabItem tpLocationDetails;
        private TabControlPanel tabControlPanel1;
        private TabItem tpItemDetails;
        private PanelEx panelEx2;
        private DevComponents.DotNetBar.TabControl tcGeneral;
        private TabControlPanel tabControlPanel3;
        private TabItem tiGeneral;
        private TabControlPanel tabControlPanel4;
        private TabItem tiSuggestions;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvSuggestions;
        private Panel pnlBottom;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedByText;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedDateValue;
        private DevComponents.DotNetBar.Controls.WarningBox lblPurchasestatus;
        private DataGridViewTextBoxColumn UserID;
        private DataGridViewTextBoxColumn UserName;
        private DataGridViewTextBoxColumn Status;
        private DataGridViewTextBoxColumn VerifiedDate;
        private DataGridViewTextBoxColumn Comment;
        private ButtonItem btnPickList;
        private ButtonItem btnPickListEmail;
        private ButtonItem BtnHelp;
        private Label lblAmtinWrds;
        private Label lblAmountinWords;
        private DataGridViewTextBoxColumn LItemCode;
        private DataGridViewTextBoxColumn LItemName;
        private DataGridViewTextBoxColumn LItemID;
        private DataGridViewTextBoxColumn LBatchID;
        private DataGridViewTextBoxColumn LBatchNo;
        private DataGridViewTextBoxColumn LQuantity;
        private DataGridViewComboBoxColumn LUOM;
        private DataGridViewTextBoxColumn LUOMID;
        private DataGridViewComboBoxColumn LLocation;
        private DataGridViewComboBoxColumn LRow;
        private DataGridViewComboBoxColumn LBlock;
        private DataGridViewComboBoxColumn LLot;
        private LinkLabel lnkLabel;
        private Label lblOrderNo1;
        private Label lblOrder1;
        private Label lblOrderNo;
        private Label lblOrder;
        private ContextMenuStrip CntxtHistory;
        private ToolStripMenuItem ShowItemHistory;
        private Label lblPurchaseAccount;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboPurchaseAccount;
        private ButtonX btnAccount;
        private CheckedListBox clbGRN;
        private Label lblGRNWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboGRNWarehouse;
        private ButtonX btnGRNWarehouse;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DataGridViewTextBoxColumn ItemID;
        private DataGridViewTextBoxColumn ItemCode;
        private DataGridViewTextBoxColumn ItemName;
        private DataGridViewTextBoxColumn Batchnumber;
        private DataGridViewTextBoxColumn OrderedQty;
        private DataGridViewTextBoxColumn UsedQty;
        private DataGridViewTextBoxColumn ReceivedQty;
        private DataGridViewTextBoxColumn Quantity;
        private DataGridViewTextBoxColumn ExtraQty;
        private DataGridViewComboBoxColumn Uom;
        private DemoClsDataGridview.CalendarColumn ExpiryDate;
        private DataGridViewTextBoxColumn Rate;
        private DataGridViewTextBoxColumn PurchaseRate;
        private DataGridViewTextBoxColumn GrandAmount;
        private DataGridViewComboBoxColumn Discount;
        private DataGridViewTextBoxColumn DiscountAmount;
        private DataGridViewTextBoxColumn NetAmount;
        private DataGridViewTextBoxColumn SlNo;
        private DataGridViewTextBoxColumn BatchID;
        private DataGridViewTextBoxColumn IsAutofilled;
        private DataGridViewTextBoxColumn DiscountAmt;
        private Label label1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboTaxScheme;
        private Label label2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTaxAmount;

        public bool blnDisableFunctionalities { get; set; }
        public bool blnIsFrmApproval;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPurchaseOrder));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            this.CommandZoom = new DevComponents.DotNetBar.Command(this.components);
            this.dockContainerItem1 = new DevComponents.DotNetBar.DockContainerItem();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvPurchaseDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelLeftTop = new DevComponents.DotNetBar.PanelEx();
            this.lnkLabel = new System.Windows.Forms.LinkLabel();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblSPurchaseNo = new System.Windows.Forms.Label();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cboSExecutive = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblExecutive = new System.Windows.Forms.Label();
            this.btnSRefresh = new DevComponents.DotNetBar.ButtonX();
            this.txtSPurchaseNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboSStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboSSupplier = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboSCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSCompany = new System.Windows.Forms.Label();
            this.lblSSupplier = new System.Windows.Forms.Label();
            this.LblSStatus = new System.Windows.Forms.Label();
            this.lblPurchaseCount = new DevComponents.DotNetBar.LabelX();
            this.tcPurchase = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvPurchase = new ClsInnerGridBar();
            this.ItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Batchnumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UsedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceivedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExtraQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Uom = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ExpiryDate = new DemoClsDataGridview.CalendarColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PurchaseRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GrandAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Discount = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.DiscountAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SlNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsAutofilled = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscountAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnTVenderAddress = new DevComponents.DotNetBar.ButtonX();
            this.tpItemDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.dgvLocationDetails = new ClsInnerGridBar();
            this.LItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LBatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LBatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LUOM = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LUOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LLocation = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LRow = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LBlock = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LLot = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tpLocationDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.dotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.dockSite4 = new DevComponents.DotNetBar.DockSite();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.dockSite2 = new DevComponents.DotNetBar.DockSite();
            this.dockSite8 = new DevComponents.DotNetBar.DockSite();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.dockSite6 = new DevComponents.DotNetBar.DockSite();
            this.dockSite3 = new DevComponents.DotNetBar.DockSite();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.panelBottom = new DevComponents.DotNetBar.PanelEx();
            this.panelGridBottom = new DevComponents.DotNetBar.PanelEx();
            this.btnAccount = new DevComponents.DotNetBar.ButtonX();
            this.cboPurchaseAccount = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblPurchaseAccount = new System.Windows.Forms.Label();
            this.lblAmtinWrds = new System.Windows.Forms.Label();
            this.pnlAmount = new DevComponents.DotNetBar.PanelEx();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTaxAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.LblBDiscount = new System.Windows.Forms.Label();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.lblCompanyCurrencyAmount = new System.Windows.Forms.Label();
            this.lblExpenseAmount = new System.Windows.Forms.Label();
            this.lblSubTotal = new System.Windows.Forms.Label();
            this.lblAdvancePayment = new System.Windows.Forms.Label();
            this.txtExchangeCurrencyRate = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtAdvPayment = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnAddExpense = new DevComponents.DotNetBar.ButtonX();
            this.txtExpenseAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboDiscount = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.txtDiscount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtSubTotal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtTotalAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtRemarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.lblClearenceDate = new System.Windows.Forms.Label();
            this.txtLeadTime = new NumericTextBox();
            this.dtpTrnsShipmentDate = new System.Windows.Forms.DateTimePicker();
            this.dtpClearenceDate = new System.Windows.Forms.DateTimePicker();
            this.dtpProductionDate = new System.Windows.Forms.DateTimePicker();
            this.cboIncoTerms = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblIncoTerms = new System.Windows.Forms.Label();
            this.lblProductionDate = new System.Windows.Forms.Label();
            this.lblTransShipmentDate = new System.Windows.Forms.Label();
            this.lblLeadTime = new System.Windows.Forms.Label();
            this.lblNoOfContainers = new System.Windows.Forms.Label();
            this.txtNoOfContainers = new NumericTextBox();
            this.cboContainerType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblContainerType = new System.Windows.Forms.Label();
            this.btnContainerType = new DevComponents.DotNetBar.ButtonX();
            this.btnIncoTerms = new DevComponents.DotNetBar.ButtonX();
            this.cboPaymentTerms = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblPaymentTerms = new System.Windows.Forms.Label();
            this.lblAmountinWords = new System.Windows.Forms.Label();
            this.expandableSplitterTop = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.tcGeneral = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.cboTaxScheme = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnGRNWarehouse = new DevComponents.DotNetBar.ButtonX();
            this.lblGRNWarehouse = new System.Windows.Forms.Label();
            this.cboGRNWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.clbGRN = new System.Windows.Forms.CheckedListBox();
            this.lblOrderNo1 = new System.Windows.Forms.Label();
            this.lblOrder1 = new System.Windows.Forms.Label();
            this.lblOrderNo = new System.Windows.Forms.Label();
            this.lblOrder = new System.Windows.Forms.Label();
            this.lblPurchaseNo = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblCriteriaText = new System.Windows.Forms.Label();
            this.lblDueDate = new System.Windows.Forms.Label();
            this.lblCriteria = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnAddressChange = new DevComponents.DotNetBar.ButtonX();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.lblSupplierAddressName = new System.Windows.Forms.Label();
            this.dtpDueDate = new System.Windows.Forms.DateTimePicker();
            this.txtSupplierBillNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblCompany = new System.Windows.Forms.Label();
            this.txtPurchaseNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblOrderType = new System.Windows.Forms.Label();
            this.cboSupplier = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSupplierBillNo = new System.Windows.Forms.Label();
            this.cboCurrency = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ChkTGNR = new System.Windows.Forms.CheckBox();
            this.cboWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.cboOrderNo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnWarehouse = new DevComponents.DotNetBar.ButtonX();
            this.cboOrderType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtDescription = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnCurrency = new DevComponents.DotNetBar.ButtonX();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.btnSupplier = new DevComponents.DotNetBar.ButtonX();
            this.lblStatusText = new System.Windows.Forms.Label();
            this.txtSupplierAddress = new System.Windows.Forms.TextBox();
            this.lblSupplier = new System.Windows.Forms.Label();
            this.btnTContextmenu = new System.Windows.Forms.Button();
            this.tiGeneral = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel4 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvSuggestions = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.UserID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VerifiedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiSuggestions = new DevComponents.DotNetBar.TabItem(this.components);
            this.PurchaseOrderBindingNavigator = new DevComponents.DotNetBar.Bar();
            this.BindingNavigatorAddNewItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorSaveItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorClearItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorDeleteItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorCancelItem = new DevComponents.DotNetBar.ButtonItem();
            this.btnActions = new DevComponents.DotNetBar.ButtonItem();
            this.bnExpense = new DevComponents.DotNetBar.ButtonItem();
            this.bnSubmitForApproval = new DevComponents.DotNetBar.ButtonItem();
            this.btnApprove = new DevComponents.DotNetBar.ButtonItem();
            this.btnReject = new DevComponents.DotNetBar.ButtonItem();
            this.btnSuggest = new DevComponents.DotNetBar.ButtonItem();
            this.btnDeny = new DevComponents.DotNetBar.ButtonItem();
            this.btnShowComments = new DevComponents.DotNetBar.ButtonItem();
            this.btnDocuments = new DevComponents.DotNetBar.ButtonItem();
            this.BtnItem = new DevComponents.DotNetBar.ButtonItem();
            this.BtnDiscount = new DevComponents.DotNetBar.ButtonItem();
            this.BtnTax = new DevComponents.DotNetBar.ButtonItem();
            this.BtnUom = new DevComponents.DotNetBar.ButtonItem();
            this.btnAccountSettings = new DevComponents.DotNetBar.ButtonItem();
            this.btnSupplierHistory = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.BtnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.btnPickList = new DevComponents.DotNetBar.ButtonItem();
            this.btnPickListEmail = new DevComponents.DotNetBar.ButtonItem();
            this.BtnHelp = new DevComponents.DotNetBar.ButtonItem();
            this.CMSVendorAddress = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TmrPurchase = new System.Windows.Forms.Timer(this.components);
            this.TmrFocus = new System.Windows.Forms.Timer(this.components);
            this.ErrPurchase = new System.Windows.Forms.ErrorProvider(this.components);
            this.ImgPurchase = new System.Windows.Forms.ImageList(this.components);
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.lblCreatedByText = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblCreatedDateValue = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblPurchasestatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.CntxtHistory = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ShowItemHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calendarColumn1 = new DemoClsDataGridview.CalendarColumn();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchaseDisplay)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcPurchase)).BeginInit();
            this.tcPurchase.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchase)).BeginInit();
            this.tabControlPanel2.SuspendLayout();
            this.panelEx2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocationDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.panelEx1.SuspendLayout();
            this.panelBottom.SuspendLayout();
            this.panelGridBottom.SuspendLayout();
            this.pnlAmount.SuspendLayout();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).BeginInit();
            this.tcGeneral.SuspendLayout();
            this.tabControlPanel3.SuspendLayout();
            this.tabControlPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSuggestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrPurchase)).BeginInit();
            this.pnlBottom.SuspendLayout();
            this.CntxtHistory.SuspendLayout();
            this.SuspendLayout();
            // 
            // CommandZoom
            // 
            this.CommandZoom.Name = "CommandZoom";
            // 
            // dockContainerItem1
            // 
            this.dockContainerItem1.Name = "dockContainerItem1";
            this.dockContainerItem1.Text = "dockContainerItem1";
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.dgvPurchaseDisplay);
            this.PanelLeft.Controls.Add(this.expandablePanel1);
            this.PanelLeft.Controls.Add(this.lblPurchaseCount);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(200, 572);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 104;
            this.PanelLeft.Text = "panelEx1";
            this.PanelLeft.Visible = false;
            // 
            // dgvPurchaseDisplay
            // 
            this.dgvPurchaseDisplay.AllowUserToAddRows = false;
            this.dgvPurchaseDisplay.AllowUserToDeleteRows = false;
            this.dgvPurchaseDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPurchaseDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPurchaseDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPurchaseDisplay.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPurchaseDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPurchaseDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvPurchaseDisplay.Location = new System.Drawing.Point(0, 226);
            this.dgvPurchaseDisplay.Name = "dgvPurchaseDisplay";
            this.dgvPurchaseDisplay.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPurchaseDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvPurchaseDisplay.RowHeadersVisible = false;
            this.dgvPurchaseDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPurchaseDisplay.Size = new System.Drawing.Size(200, 320);
            this.dgvPurchaseDisplay.TabIndex = 47;
            this.dgvPurchaseDisplay.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPurchaseDisplay_CellDoubleClick);
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expandablePanel1.Controls.Add(this.panelLeftTop);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(200, 226);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 101;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Search";
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelLeftTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelLeftTop.Controls.Add(this.lnkLabel);
            this.panelLeftTop.Controls.Add(this.dtpSTo);
            this.panelLeftTop.Controls.Add(this.dtpSFrom);
            this.panelLeftTop.Controls.Add(this.lblSPurchaseNo);
            this.panelLeftTop.Controls.Add(this.lblTo);
            this.panelLeftTop.Controls.Add(this.lblFrom);
            this.panelLeftTop.Controls.Add(this.cboSExecutive);
            this.panelLeftTop.Controls.Add(this.lblExecutive);
            this.panelLeftTop.Controls.Add(this.btnSRefresh);
            this.panelLeftTop.Controls.Add(this.txtSPurchaseNo);
            this.panelLeftTop.Controls.Add(this.cboSStatus);
            this.panelLeftTop.Controls.Add(this.cboSSupplier);
            this.panelLeftTop.Controls.Add(this.cboSCompany);
            this.panelLeftTop.Controls.Add(this.lblSCompany);
            this.panelLeftTop.Controls.Add(this.lblSSupplier);
            this.panelLeftTop.Controls.Add(this.LblSStatus);
            this.panelLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftTop.Location = new System.Drawing.Point(0, 26);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(200, 200);
            this.panelLeftTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelLeftTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelLeftTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelLeftTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelLeftTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelLeftTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelLeftTop.Style.GradientAngle = 90;
            this.panelLeftTop.TabIndex = 38;
            // 
            // lnkLabel
            // 
            this.lnkLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lnkLabel.AutoSize = true;
            this.lnkLabel.Location = new System.Drawing.Point(1, 181);
            this.lnkLabel.Name = "lnkLabel";
            this.lnkLabel.Size = new System.Drawing.Size(87, 13);
            this.lnkLabel.TabIndex = 259;
            this.lnkLabel.TabStop = true;
            this.lnkLabel.Text = "Advance Search";
            this.lnkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLabel_LinkClicked);
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(72, 125);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.Size = new System.Drawing.Size(122, 20);
            this.dtpSTo.TabIndex = 44;
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(72, 102);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.Size = new System.Drawing.Size(122, 20);
            this.dtpSFrom.TabIndex = 43;
            // 
            // lblSPurchaseNo
            // 
            this.lblSPurchaseNo.AutoSize = true;
            this.lblSPurchaseNo.Location = new System.Drawing.Point(1, 151);
            this.lblSPurchaseNo.Name = "lblSPurchaseNo";
            this.lblSPurchaseNo.Size = new System.Drawing.Size(70, 13);
            this.lblSPurchaseNo.TabIndex = 245;
            this.lblSPurchaseNo.Text = "Quotation No";
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(1, 128);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 244;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(1, 105);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 243;
            this.lblFrom.Text = "From";
            // 
            // cboSExecutive
            // 
            this.cboSExecutive.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSExecutive.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSExecutive.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSExecutive.DisplayMember = "Text";
            this.cboSExecutive.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSExecutive.DropDownHeight = 75;
            this.cboSExecutive.FormattingEnabled = true;
            this.cboSExecutive.IntegralHeight = false;
            this.cboSExecutive.ItemHeight = 14;
            this.cboSExecutive.Location = new System.Drawing.Point(72, 33);
            this.cboSExecutive.Name = "cboSExecutive";
            this.cboSExecutive.Size = new System.Drawing.Size(122, 20);
            this.cboSExecutive.TabIndex = 40;
            // 
            // lblExecutive
            // 
            this.lblExecutive.AutoSize = true;
            this.lblExecutive.Location = new System.Drawing.Point(1, 36);
            this.lblExecutive.Name = "lblExecutive";
            this.lblExecutive.Size = new System.Drawing.Size(54, 13);
            this.lblExecutive.TabIndex = 124;
            this.lblExecutive.Text = "Executive";
            // 
            // btnSRefresh
            // 
            this.btnSRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSRefresh.Location = new System.Drawing.Point(123, 173);
            this.btnSRefresh.Name = "btnSRefresh";
            this.btnSRefresh.Size = new System.Drawing.Size(71, 23);
            this.btnSRefresh.TabIndex = 46;
            this.btnSRefresh.Text = "Refresh";
            this.btnSRefresh.Click += new System.EventHandler(this.btnSRefresh_Click);
            // 
            // txtSPurchaseNo
            // 
            this.txtSPurchaseNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtSPurchaseNo.Border.Class = "TextBoxBorder";
            this.txtSPurchaseNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSPurchaseNo.Location = new System.Drawing.Point(72, 148);
            this.txtSPurchaseNo.MaxLength = 20;
            this.txtSPurchaseNo.Name = "txtSPurchaseNo";
            this.txtSPurchaseNo.Size = new System.Drawing.Size(122, 20);
            this.txtSPurchaseNo.TabIndex = 45;
            this.txtSPurchaseNo.WatermarkEnabled = false;
            this.txtSPurchaseNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtSsearch_KeyPress);
            // 
            // cboSStatus
            // 
            this.cboSStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSStatus.DisplayMember = "Text";
            this.cboSStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSStatus.DropDownHeight = 75;
            this.cboSStatus.FormattingEnabled = true;
            this.cboSStatus.IntegralHeight = false;
            this.cboSStatus.ItemHeight = 14;
            this.cboSStatus.Location = new System.Drawing.Point(72, 79);
            this.cboSStatus.Name = "cboSStatus";
            this.cboSStatus.Size = new System.Drawing.Size(122, 20);
            this.cboSStatus.TabIndex = 42;
            // 
            // cboSSupplier
            // 
            this.cboSSupplier.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSSupplier.DisplayMember = "Text";
            this.cboSSupplier.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSSupplier.DropDownHeight = 75;
            this.cboSSupplier.FormattingEnabled = true;
            this.cboSSupplier.IntegralHeight = false;
            this.cboSSupplier.ItemHeight = 14;
            this.cboSSupplier.Location = new System.Drawing.Point(72, 56);
            this.cboSSupplier.Name = "cboSSupplier";
            this.cboSSupplier.Size = new System.Drawing.Size(122, 20);
            this.cboSSupplier.TabIndex = 41;
            // 
            // cboSCompany
            // 
            this.cboSCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSCompany.DisplayMember = "Text";
            this.cboSCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSCompany.DropDownHeight = 75;
            this.cboSCompany.FormattingEnabled = true;
            this.cboSCompany.IntegralHeight = false;
            this.cboSCompany.ItemHeight = 14;
            this.cboSCompany.Location = new System.Drawing.Point(72, 10);
            this.cboSCompany.Name = "cboSCompany";
            this.cboSCompany.Size = new System.Drawing.Size(122, 20);
            this.cboSCompany.TabIndex = 39;
            this.cboSCompany.SelectedIndexChanged += new System.EventHandler(this.cboSCompany_SelectedIndexChanged);
            // 
            // lblSCompany
            // 
            this.lblSCompany.AutoSize = true;
            this.lblSCompany.Location = new System.Drawing.Point(1, 13);
            this.lblSCompany.Name = "lblSCompany";
            this.lblSCompany.Size = new System.Drawing.Size(51, 13);
            this.lblSCompany.TabIndex = 109;
            this.lblSCompany.Text = "Company";
            // 
            // lblSSupplier
            // 
            this.lblSSupplier.AutoSize = true;
            this.lblSSupplier.Location = new System.Drawing.Point(1, 59);
            this.lblSSupplier.Name = "lblSSupplier";
            this.lblSSupplier.Size = new System.Drawing.Size(45, 13);
            this.lblSSupplier.TabIndex = 108;
            this.lblSSupplier.Text = "Supplier";
            // 
            // LblSStatus
            // 
            this.LblSStatus.AutoSize = true;
            this.LblSStatus.Location = new System.Drawing.Point(1, 82);
            this.LblSStatus.Name = "LblSStatus";
            this.LblSStatus.Size = new System.Drawing.Size(37, 13);
            this.LblSStatus.TabIndex = 107;
            this.LblSStatus.Text = "Status";
            // 
            // lblPurchaseCount
            // 
            // 
            // 
            // 
            this.lblPurchaseCount.BackgroundStyle.Class = "";
            this.lblPurchaseCount.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPurchaseCount.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblPurchaseCount.Location = new System.Drawing.Point(0, 546);
            this.lblPurchaseCount.Name = "lblPurchaseCount";
            this.lblPurchaseCount.Size = new System.Drawing.Size(200, 26);
            this.lblPurchaseCount.TabIndex = 105;
            this.lblPurchaseCount.Text = "...";
            // 
            // tcPurchase
            // 
            this.tcPurchase.BackColor = System.Drawing.Color.Transparent;
            this.tcPurchase.CanReorderTabs = true;
            this.tcPurchase.Controls.Add(this.tabControlPanel1);
            this.tcPurchase.Controls.Add(this.tabControlPanel2);
            this.tcPurchase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcPurchase.Location = new System.Drawing.Point(0, 0);
            this.tcPurchase.Name = "tcPurchase";
            this.tcPurchase.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcPurchase.SelectedTabIndex = 0;
            this.tcPurchase.Size = new System.Drawing.Size(1273, 275);
            this.tcPurchase.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcPurchase.TabIndex = 18;
            this.tcPurchase.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcPurchase.Tabs.Add(this.tpItemDetails);
            this.tcPurchase.Tabs.Add(this.tpLocationDetails);
            this.tcPurchase.TabStop = false;
            this.tcPurchase.Text = "tabControl1";
            this.tcPurchase.SelectedTabChanged += new DevComponents.DotNetBar.TabStrip.SelectedTabChangedEventHandler(this.tcPurchase_SelectedTabChanged);
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.dgvPurchase);
            this.tabControlPanel1.Controls.Add(this.BtnTVenderAddress);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1273, 253);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 19;
            this.tabControlPanel1.TabItem = this.tpItemDetails;
            // 
            // dgvPurchase
            // 
            this.dgvPurchase.AddNewRow = false;
            this.dgvPurchase.AlphaNumericCols = new int[0];
            this.dgvPurchase.BackgroundColor = System.Drawing.Color.White;
            this.dgvPurchase.CapsLockCols = new int[0];
            this.dgvPurchase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPurchase.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemID,
            this.ItemCode,
            this.ItemName,
            this.Batchnumber,
            this.OrderedQty,
            this.UsedQty,
            this.ReceivedQty,
            this.Quantity,
            this.ExtraQty,
            this.Uom,
            this.ExpiryDate,
            this.Rate,
            this.PurchaseRate,
            this.GrandAmount,
            this.Discount,
            this.DiscountAmount,
            this.NetAmount,
            this.SlNo,
            this.BatchID,
            this.IsAutofilled,
            this.DiscountAmt});
            this.dgvPurchase.DecimalCols = new int[0];
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPurchase.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgvPurchase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPurchase.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvPurchase.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvPurchase.HasSlNo = false;
            this.dgvPurchase.LastRowIndex = 0;
            this.dgvPurchase.Location = new System.Drawing.Point(1, 1);
            this.dgvPurchase.Name = "dgvPurchase";
            this.dgvPurchase.NegativeValueCols = new int[0];
            this.dgvPurchase.NumericCols = new int[0];
            this.dgvPurchase.RowHeadersWidth = 50;
            this.dgvPurchase.Size = new System.Drawing.Size(1271, 251);
            this.dgvPurchase.TabIndex = 20;
            this.dgvPurchase.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPurchase_CellValueChanged);
            this.dgvPurchase.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvPurchase_Scroll);
            this.dgvPurchase.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvPurchase_UserDeletingRow);
            this.dgvPurchase.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPurchase_CellMouseClick);
            this.dgvPurchase.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvPurchase_CellBeginEdit);
            this.dgvPurchase.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvPurchase_UserDeletedRow);
            this.dgvPurchase.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvPurchase_RowsAdded);
            this.dgvPurchase.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPurchase_CellEndEdit);
            this.dgvPurchase.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvPurchase_Textbox_TextChanged);
            this.dgvPurchase.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvPurchase_EditingControlShowing);
            this.dgvPurchase.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvPurchase_CurrentCellDirtyStateChanged);
            this.dgvPurchase.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPurchase_CellEnter);
            this.dgvPurchase.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvPurchase_RowsRemoved);
            // 
            // ItemID
            // 
            this.ItemID.HeaderText = "ItemID";
            this.ItemID.Name = "ItemID";
            this.ItemID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemID.Visible = false;
            // 
            // ItemCode
            // 
            this.ItemCode.HeaderText = "Item Code";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemCode.Width = 60;
            // 
            // ItemName
            // 
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemName.Width = 350;
            // 
            // Batchnumber
            // 
            this.Batchnumber.HeaderText = "Batch No";
            this.Batchnumber.MaxInputLength = 25;
            this.Batchnumber.Name = "Batchnumber";
            this.Batchnumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Batchnumber.Visible = false;
            // 
            // OrderedQty
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.OrderedQty.DefaultCellStyle = dataGridViewCellStyle4;
            this.OrderedQty.HeaderText = "Ordered Qty";
            this.OrderedQty.MaxInputLength = 10;
            this.OrderedQty.Name = "OrderedQty";
            this.OrderedQty.ReadOnly = true;
            this.OrderedQty.Visible = false;
            this.OrderedQty.Width = 70;
            // 
            // UsedQty
            // 
            this.UsedQty.HeaderText = "Used Qty";
            this.UsedQty.Name = "UsedQty";
            this.UsedQty.ReadOnly = true;
            this.UsedQty.Visible = false;
            this.UsedQty.Width = 70;
            // 
            // ReceivedQty
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.ReceivedQty.DefaultCellStyle = dataGridViewCellStyle5;
            this.ReceivedQty.HeaderText = "Received Qty";
            this.ReceivedQty.MaxInputLength = 10;
            this.ReceivedQty.Name = "ReceivedQty";
            this.ReceivedQty.ReadOnly = true;
            this.ReceivedQty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ReceivedQty.Visible = false;
            this.ReceivedQty.Width = 70;
            // 
            // Quantity
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle6.Format = "N3";
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle6;
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.MaxInputLength = 10;
            this.Quantity.Name = "Quantity";
            this.Quantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Quantity.Width = 70;
            // 
            // ExtraQty
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.ExtraQty.DefaultCellStyle = dataGridViewCellStyle7;
            this.ExtraQty.HeaderText = "Extra Qty";
            this.ExtraQty.MaxInputLength = 10;
            this.ExtraQty.Name = "ExtraQty";
            this.ExtraQty.Visible = false;
            this.ExtraQty.Width = 70;
            // 
            // Uom
            // 
            this.Uom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Uom.HeaderText = "UOM";
            this.Uom.Name = "Uom";
            this.Uom.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Uom.Width = 60;
            // 
            // ExpiryDate
            // 
            this.ExpiryDate.HeaderText = "Expiry Date";
            this.ExpiryDate.Name = "ExpiryDate";
            this.ExpiryDate.Visible = false;
            // 
            // Rate
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle8.Format = "N2";
            this.Rate.DefaultCellStyle = dataGridViewCellStyle8;
            this.Rate.HeaderText = "Rate";
            this.Rate.MaxInputLength = 10;
            this.Rate.Name = "Rate";
            this.Rate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PurchaseRate
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle9.Format = "N2";
            this.PurchaseRate.DefaultCellStyle = dataGridViewCellStyle9;
            this.PurchaseRate.HeaderText = "Purchase Rate";
            this.PurchaseRate.MaxInputLength = 10;
            this.PurchaseRate.Name = "PurchaseRate";
            this.PurchaseRate.ReadOnly = true;
            this.PurchaseRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PurchaseRate.Visible = false;
            // 
            // GrandAmount
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.GrandAmount.DefaultCellStyle = dataGridViewCellStyle10;
            this.GrandAmount.HeaderText = "Grand Amount";
            this.GrandAmount.MaxInputLength = 15;
            this.GrandAmount.Name = "GrandAmount";
            this.GrandAmount.ReadOnly = true;
            this.GrandAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Discount
            // 
            this.Discount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Discount.HeaderText = "Discount Type";
            this.Discount.Items.AddRange(new object[] {
            "%",
            "Amt"});
            this.Discount.Name = "Discount";
            this.Discount.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // DiscountAmount
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.DiscountAmount.DefaultCellStyle = dataGridViewCellStyle11;
            this.DiscountAmount.HeaderText = "Discount";
            this.DiscountAmount.MaxInputLength = 10;
            this.DiscountAmount.Name = "DiscountAmount";
            this.DiscountAmount.Width = 80;
            // 
            // NetAmount
            // 
            this.NetAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.NetAmount.DefaultCellStyle = dataGridViewCellStyle12;
            this.NetAmount.HeaderText = "Net Amount";
            this.NetAmount.Name = "NetAmount";
            this.NetAmount.ReadOnly = true;
            // 
            // SlNo
            // 
            this.SlNo.HeaderText = "SlNo";
            this.SlNo.Name = "SlNo";
            this.SlNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SlNo.Visible = false;
            // 
            // BatchID
            // 
            this.BatchID.HeaderText = "BatchID";
            this.BatchID.Name = "BatchID";
            this.BatchID.Visible = false;
            // 
            // IsAutofilled
            // 
            this.IsAutofilled.HeaderText = "IsAutoFilled";
            this.IsAutofilled.Name = "IsAutofilled";
            this.IsAutofilled.Visible = false;
            // 
            // DiscountAmt
            // 
            this.DiscountAmt.HeaderText = "DiscountAmt";
            this.DiscountAmt.Name = "DiscountAmt";
            this.DiscountAmt.Visible = false;
            // 
            // BtnTVenderAddress
            // 
            this.BtnTVenderAddress.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnTVenderAddress.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnTVenderAddress.Location = new System.Drawing.Point(399, 84);
            this.BtnTVenderAddress.Name = "BtnTVenderAddress";
            this.BtnTVenderAddress.Size = new System.Drawing.Size(24, 23);
            this.BtnTVenderAddress.TabIndex = 10;
            this.BtnTVenderAddress.TabStop = false;
            this.BtnTVenderAddress.Text = "Permanent";
            this.BtnTVenderAddress.Visible = false;
            this.BtnTVenderAddress.Click += new System.EventHandler(this.BtnTVenderAddress_Click);
            // 
            // tpItemDetails
            // 
            this.tpItemDetails.AttachedControl = this.tabControlPanel1;
            this.tpItemDetails.Name = "tpItemDetails";
            this.tpItemDetails.Text = "Item Details";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.panelEx2);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(1273, 253);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            this.tabControlPanel2.TabItem = this.tpLocationDetails;
            // 
            // panelEx2
            // 
            this.panelEx2.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx2.Controls.Add(this.dgvLocationDetails);
            this.panelEx2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx2.Location = new System.Drawing.Point(1, 1);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(1271, 251);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 1;
            this.panelEx2.Text = "panelEx2";
            // 
            // dgvLocationDetails
            // 
            this.dgvLocationDetails.AddNewRow = false;
            this.dgvLocationDetails.AlphaNumericCols = new int[0];
            this.dgvLocationDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvLocationDetails.CapsLockCols = new int[0];
            this.dgvLocationDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocationDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LItemCode,
            this.LItemName,
            this.LItemID,
            this.LBatchID,
            this.LBatchNo,
            this.LQuantity,
            this.LUOM,
            this.LUOMID,
            this.LLocation,
            this.LRow,
            this.LBlock,
            this.LLot});
            this.dgvLocationDetails.DecimalCols = new int[0];
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLocationDetails.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvLocationDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLocationDetails.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvLocationDetails.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvLocationDetails.HasSlNo = false;
            this.dgvLocationDetails.LastRowIndex = 0;
            this.dgvLocationDetails.Location = new System.Drawing.Point(0, 0);
            this.dgvLocationDetails.Name = "dgvLocationDetails";
            this.dgvLocationDetails.NegativeValueCols = new int[0];
            this.dgvLocationDetails.NumericCols = new int[0];
            this.dgvLocationDetails.RowHeadersWidth = 30;
            this.dgvLocationDetails.Size = new System.Drawing.Size(1271, 251);
            this.dgvLocationDetails.TabIndex = 0;
            this.dgvLocationDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLocationDetails_CellValueChanged);
            this.dgvLocationDetails.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvLocationDetails_UserDeletingRow);
            this.dgvLocationDetails.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvLocationDetails_CellBeginEdit);
            this.dgvLocationDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLocationDetails_CellEndEdit);
            this.dgvLocationDetails.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvLocationDetails_Textbox_TextChanged);
            this.dgvLocationDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvLocationDetails_EditingControlShowing);
            this.dgvLocationDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvLocationDetails_CurrentCellDirtyStateChanged);
            this.dgvLocationDetails.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLocationDetails_CellEnter);
            // 
            // LItemCode
            // 
            this.LItemCode.HeaderText = "Item Code";
            this.LItemCode.Name = "LItemCode";
            this.LItemCode.Width = 60;
            // 
            // LItemName
            // 
            this.LItemName.HeaderText = "Item Name";
            this.LItemName.Name = "LItemName";
            this.LItemName.Width = 250;
            // 
            // LItemID
            // 
            this.LItemID.HeaderText = "ItemID";
            this.LItemID.Name = "LItemID";
            this.LItemID.Visible = false;
            // 
            // LBatchID
            // 
            this.LBatchID.HeaderText = "BatchID";
            this.LBatchID.Name = "LBatchID";
            this.LBatchID.Visible = false;
            // 
            // LBatchNo
            // 
            this.LBatchNo.HeaderText = "Batch No";
            this.LBatchNo.Name = "LBatchNo";
            this.LBatchNo.ReadOnly = true;
            this.LBatchNo.Width = 130;
            // 
            // LQuantity
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.LQuantity.DefaultCellStyle = dataGridViewCellStyle14;
            this.LQuantity.HeaderText = "Quantity";
            this.LQuantity.Name = "LQuantity";
            // 
            // LUOM
            // 
            this.LUOM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LUOM.HeaderText = "UOM";
            this.LUOM.Name = "LUOM";
            this.LUOM.ReadOnly = true;
            this.LUOM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LUOM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.LUOM.Width = 60;
            // 
            // LUOMID
            // 
            this.LUOMID.HeaderText = "UOMID";
            this.LUOMID.Name = "LUOMID";
            this.LUOMID.Visible = false;
            // 
            // LLocation
            // 
            this.LLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LLocation.HeaderText = "Location";
            this.LLocation.Name = "LLocation";
            this.LLocation.Width = 125;
            // 
            // LRow
            // 
            this.LRow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LRow.HeaderText = "Row";
            this.LRow.Name = "LRow";
            this.LRow.Width = 125;
            // 
            // LBlock
            // 
            this.LBlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LBlock.HeaderText = "Block";
            this.LBlock.Name = "LBlock";
            this.LBlock.Width = 125;
            // 
            // LLot
            // 
            this.LLot.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LLot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LLot.HeaderText = "Lot";
            this.LLot.Name = "LLot";
            // 
            // tpLocationDetails
            // 
            this.tpLocationDetails.AttachedControl = this.tabControlPanel2;
            this.tpLocationDetails.Name = "tpLocationDetails";
            this.tpLocationDetails.Text = "Location Details";
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.Expanded = false;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(179)))), ((int)(((byte)(219)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(0, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 572);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 119;
            this.expandableSplitterLeft.TabStop = false;
            this.expandableSplitterLeft.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitterLeft_ExpandedChanged);
            // 
            // dotNetBarManager1
            // 
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.dotNetBarManager1.BottomDockSite = this.dockSite4;
            this.dotNetBarManager1.EnableFullSizeDock = false;
            this.dotNetBarManager1.LeftDockSite = this.dockSite1;
            this.dotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.dotNetBarManager1.ParentForm = this;
            this.dotNetBarManager1.RightDockSite = this.dockSite2;
            this.dotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.dotNetBarManager1.ToolbarBottomDockSite = this.dockSite8;
            this.dotNetBarManager1.ToolbarLeftDockSite = this.dockSite5;
            this.dotNetBarManager1.ToolbarRightDockSite = this.dockSite6;
            this.dotNetBarManager1.TopDockSite = this.dockSite3;
            // 
            // dockSite4
            // 
            this.dockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite4.Location = new System.Drawing.Point(0, 572);
            this.dockSite4.Name = "dockSite4";
            this.dockSite4.Size = new System.Drawing.Size(1276, 0);
            this.dockSite4.TabIndex = 114;
            this.dockSite4.TabStop = false;
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(3, 0);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 572);
            this.dockSite1.TabIndex = 111;
            this.dockSite1.TabStop = false;
            // 
            // dockSite2
            // 
            this.dockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite2.Location = new System.Drawing.Point(1276, 0);
            this.dockSite2.Name = "dockSite2";
            this.dockSite2.Size = new System.Drawing.Size(0, 572);
            this.dockSite2.TabIndex = 112;
            this.dockSite2.TabStop = false;
            // 
            // dockSite8
            // 
            this.dockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite8.Location = new System.Drawing.Point(0, 572);
            this.dockSite8.Name = "dockSite8";
            this.dockSite8.Size = new System.Drawing.Size(1276, 0);
            this.dockSite8.TabIndex = 118;
            this.dockSite8.TabStop = false;
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(0, 0);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 572);
            this.dockSite5.TabIndex = 115;
            this.dockSite5.TabStop = false;
            // 
            // dockSite6
            // 
            this.dockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite6.Location = new System.Drawing.Point(1276, 0);
            this.dockSite6.Name = "dockSite6";
            this.dockSite6.Size = new System.Drawing.Size(0, 572);
            this.dockSite6.TabIndex = 116;
            this.dockSite6.TabStop = false;
            // 
            // dockSite3
            // 
            this.dockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite3.Location = new System.Drawing.Point(0, 0);
            this.dockSite3.Name = "dockSite3";
            this.dockSite3.Size = new System.Drawing.Size(1276, 0);
            this.dockSite3.TabIndex = 113;
            this.dockSite3.TabStop = false;
            // 
            // bar1
            // 
            this.bar1.AccessibleDescription = "DotNetBar Bar (bar1)";
            this.bar1.AccessibleName = "DotNetBar Bar";
            this.bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.bar1.Location = new System.Drawing.Point(0, 0);
            this.bar1.MenuBar = true;
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(36, 24);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.bar1.TabIndex = 0;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx1.Controls.Add(this.panelBottom);
            this.panelEx1.Controls.Add(this.expandableSplitterTop);
            this.panelEx1.Controls.Add(this.panelTop);
            this.panelEx1.Controls.Add(this.PurchaseOrderBindingNavigator);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(3, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(1273, 572);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 100;
            this.panelEx1.Text = "panelEx1";
            // 
            // panelBottom
            // 
            this.panelBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelBottom.Controls.Add(this.tcPurchase);
            this.panelBottom.Controls.Add(this.panelGridBottom);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBottom.Location = new System.Drawing.Point(0, 161);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1273, 411);
            this.panelBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelBottom.Style.GradientAngle = 90;
            this.panelBottom.TabIndex = 103;
            this.panelBottom.Text = "panelEx3";
            // 
            // panelGridBottom
            // 
            this.panelGridBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelGridBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelGridBottom.Controls.Add(this.btnAccount);
            this.panelGridBottom.Controls.Add(this.cboPurchaseAccount);
            this.panelGridBottom.Controls.Add(this.lblPurchaseAccount);
            this.panelGridBottom.Controls.Add(this.lblAmtinWrds);
            this.panelGridBottom.Controls.Add(this.pnlAmount);
            this.panelGridBottom.Controls.Add(this.txtRemarks);
            this.panelGridBottom.Controls.Add(this.lblRemarks);
            this.panelGridBottom.Controls.Add(this.lblClearenceDate);
            this.panelGridBottom.Controls.Add(this.txtLeadTime);
            this.panelGridBottom.Controls.Add(this.dtpTrnsShipmentDate);
            this.panelGridBottom.Controls.Add(this.dtpClearenceDate);
            this.panelGridBottom.Controls.Add(this.dtpProductionDate);
            this.panelGridBottom.Controls.Add(this.cboIncoTerms);
            this.panelGridBottom.Controls.Add(this.lblIncoTerms);
            this.panelGridBottom.Controls.Add(this.lblProductionDate);
            this.panelGridBottom.Controls.Add(this.lblTransShipmentDate);
            this.panelGridBottom.Controls.Add(this.lblLeadTime);
            this.panelGridBottom.Controls.Add(this.lblNoOfContainers);
            this.panelGridBottom.Controls.Add(this.txtNoOfContainers);
            this.panelGridBottom.Controls.Add(this.cboContainerType);
            this.panelGridBottom.Controls.Add(this.lblContainerType);
            this.panelGridBottom.Controls.Add(this.btnContainerType);
            this.panelGridBottom.Controls.Add(this.btnIncoTerms);
            this.panelGridBottom.Controls.Add(this.cboPaymentTerms);
            this.panelGridBottom.Controls.Add(this.lblPaymentTerms);
            this.panelGridBottom.Controls.Add(this.lblAmountinWords);
            this.panelGridBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGridBottom.Location = new System.Drawing.Point(0, 275);
            this.panelGridBottom.Name = "panelGridBottom";
            this.panelGridBottom.Size = new System.Drawing.Size(1273, 136);
            this.panelGridBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelGridBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelGridBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelGridBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelGridBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelGridBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelGridBottom.Style.GradientAngle = 90;
            this.panelGridBottom.TabIndex = 102;
            this.panelGridBottom.Click += new System.EventHandler(this.panelGridBottom_Click);
            // 
            // btnAccount
            // 
            this.btnAccount.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAccount.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAccount.Location = new System.Drawing.Point(515, 9);
            this.btnAccount.Name = "btnAccount";
            this.btnAccount.Size = new System.Drawing.Size(25, 20);
            this.btnAccount.TabIndex = 294;
            this.btnAccount.Text = "...";
            this.btnAccount.Click += new System.EventHandler(this.btnAccount_Click);
            // 
            // cboPurchaseAccount
            // 
            this.cboPurchaseAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboPurchaseAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboPurchaseAccount.DisplayMember = "Text";
            this.cboPurchaseAccount.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboPurchaseAccount.DropDownHeight = 75;
            this.cboPurchaseAccount.FormattingEnabled = true;
            this.cboPurchaseAccount.IntegralHeight = false;
            this.cboPurchaseAccount.ItemHeight = 14;
            this.cboPurchaseAccount.Location = new System.Drawing.Point(363, 9);
            this.cboPurchaseAccount.Name = "cboPurchaseAccount";
            this.cboPurchaseAccount.Size = new System.Drawing.Size(146, 20);
            this.cboPurchaseAccount.TabIndex = 293;
            // 
            // lblPurchaseAccount
            // 
            this.lblPurchaseAccount.AutoSize = true;
            this.lblPurchaseAccount.Location = new System.Drawing.Point(256, 13);
            this.lblPurchaseAccount.Name = "lblPurchaseAccount";
            this.lblPurchaseAccount.Size = new System.Drawing.Size(95, 13);
            this.lblPurchaseAccount.TabIndex = 292;
            this.lblPurchaseAccount.Text = "Purchase Account";
            // 
            // lblAmtinWrds
            // 
            this.lblAmtinWrds.AutoSize = true;
            this.lblAmtinWrds.Location = new System.Drawing.Point(9, 87);
            this.lblAmtinWrds.Name = "lblAmtinWrds";
            this.lblAmtinWrds.Size = new System.Drawing.Size(88, 13);
            this.lblAmtinWrds.TabIndex = 290;
            this.lblAmtinWrds.Text = "Amount in Words";
            // 
            // pnlAmount
            // 
            this.pnlAmount.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlAmount.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.pnlAmount.Controls.Add(this.label2);
            this.pnlAmount.Controls.Add(this.txtTaxAmount);
            this.pnlAmount.Controls.Add(this.LblBDiscount);
            this.pnlAmount.Controls.Add(this.lblTotalAmount);
            this.pnlAmount.Controls.Add(this.lblCompanyCurrencyAmount);
            this.pnlAmount.Controls.Add(this.lblExpenseAmount);
            this.pnlAmount.Controls.Add(this.lblSubTotal);
            this.pnlAmount.Controls.Add(this.lblAdvancePayment);
            this.pnlAmount.Controls.Add(this.txtExchangeCurrencyRate);
            this.pnlAmount.Controls.Add(this.txtAdvPayment);
            this.pnlAmount.Controls.Add(this.btnAddExpense);
            this.pnlAmount.Controls.Add(this.txtExpenseAmount);
            this.pnlAmount.Controls.Add(this.cboDiscount);
            this.pnlAmount.Controls.Add(this.txtDiscount);
            this.pnlAmount.Controls.Add(this.txtSubTotal);
            this.pnlAmount.Controls.Add(this.txtTotalAmount);
            this.pnlAmount.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlAmount.Location = new System.Drawing.Point(891, 0);
            this.pnlAmount.Name = "pnlAmount";
            this.pnlAmount.Size = new System.Drawing.Size(382, 136);
            this.pnlAmount.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlAmount.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlAmount.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlAmount.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlAmount.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlAmount.Style.GradientAngle = 90;
            this.pnlAmount.TabIndex = 286;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(193, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 9);
            this.label2.TabIndex = 304;
            this.label2.Text = "Tax Amount";
            // 
            // txtTaxAmount
            // 
            this.txtTaxAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTaxAmount.Border.Class = "TextBoxBorder";
            this.txtTaxAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTaxAmount.Enabled = false;
            this.txtTaxAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxAmount.Location = new System.Drawing.Point(192, 48);
            this.txtTaxAmount.Name = "txtTaxAmount";
            this.txtTaxAmount.ReadOnly = true;
            this.txtTaxAmount.Size = new System.Drawing.Size(185, 20);
            this.txtTaxAmount.TabIndex = 303;
            this.txtTaxAmount.TabStop = false;
            this.txtTaxAmount.Text = "0.00";
            this.txtTaxAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LblBDiscount
            // 
            this.LblBDiscount.AutoSize = true;
            this.LblBDiscount.BackColor = System.Drawing.Color.White;
            this.LblBDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBDiscount.Location = new System.Drawing.Point(92, 57);
            this.LblBDiscount.Name = "LblBDiscount";
            this.LblBDiscount.Size = new System.Drawing.Size(35, 9);
            this.LblBDiscount.TabIndex = 297;
            this.LblBDiscount.Text = "Discount";
            this.LblBDiscount.Visible = false;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.BackColor = System.Drawing.Color.White;
            this.lblTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(193, 95);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(45, 9);
            this.lblTotalAmount.TabIndex = 299;
            this.lblTotalAmount.Text = "Net Amount";
            // 
            // lblCompanyCurrencyAmount
            // 
            this.lblCompanyCurrencyAmount.AutoSize = true;
            this.lblCompanyCurrencyAmount.BackColor = System.Drawing.Color.White;
            this.lblCompanyCurrencyAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompanyCurrencyAmount.Location = new System.Drawing.Point(6, 95);
            this.lblCompanyCurrencyAmount.Name = "lblCompanyCurrencyAmount";
            this.lblCompanyCurrencyAmount.Size = new System.Drawing.Size(104, 9);
            this.lblCompanyCurrencyAmount.TabIndex = 302;
            this.lblCompanyCurrencyAmount.Text = "Amount in company currency";
            // 
            // lblExpenseAmount
            // 
            this.lblExpenseAmount.AutoSize = true;
            this.lblExpenseAmount.BackColor = System.Drawing.Color.White;
            this.lblExpenseAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpenseAmount.Location = new System.Drawing.Point(193, 37);
            this.lblExpenseAmount.Name = "lblExpenseAmount";
            this.lblExpenseAmount.Size = new System.Drawing.Size(38, 9);
            this.lblExpenseAmount.TabIndex = 300;
            this.lblExpenseAmount.Text = "Expenses";
            // 
            // lblSubTotal
            // 
            this.lblSubTotal.AutoSize = true;
            this.lblSubTotal.BackColor = System.Drawing.Color.White;
            this.lblSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubTotal.Location = new System.Drawing.Point(193, 16);
            this.lblSubTotal.Name = "lblSubTotal";
            this.lblSubTotal.Size = new System.Drawing.Size(37, 9);
            this.lblSubTotal.TabIndex = 296;
            this.lblSubTotal.Text = "Sub Total";
            // 
            // lblAdvancePayment
            // 
            this.lblAdvancePayment.AutoSize = true;
            this.lblAdvancePayment.BackColor = System.Drawing.Color.White;
            this.lblAdvancePayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdvancePayment.Location = new System.Drawing.Point(6, 15);
            this.lblAdvancePayment.Name = "lblAdvancePayment";
            this.lblAdvancePayment.Size = new System.Drawing.Size(66, 9);
            this.lblAdvancePayment.TabIndex = 301;
            this.lblAdvancePayment.Text = "Advance Payment";
            this.lblAdvancePayment.Visible = false;
            // 
            // txtExchangeCurrencyRate
            // 
            this.txtExchangeCurrencyRate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtExchangeCurrencyRate.Border.Class = "TextBoxBorder";
            this.txtExchangeCurrencyRate.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtExchangeCurrencyRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExchangeCurrencyRate.Location = new System.Drawing.Point(5, 69);
            this.txtExchangeCurrencyRate.Name = "txtExchangeCurrencyRate";
            this.txtExchangeCurrencyRate.ReadOnly = true;
            this.txtExchangeCurrencyRate.Size = new System.Drawing.Size(185, 38);
            this.txtExchangeCurrencyRate.TabIndex = 36;
            this.txtExchangeCurrencyRate.TabStop = false;
            this.txtExchangeCurrencyRate.Text = "0.00";
            this.txtExchangeCurrencyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAdvPayment
            // 
            this.txtAdvPayment.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtAdvPayment.Border.Class = "TextBoxBorder";
            this.txtAdvPayment.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtAdvPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdvPayment.Location = new System.Drawing.Point(5, 6);
            this.txtAdvPayment.MaxLength = 20;
            this.txtAdvPayment.Name = "txtAdvPayment";
            this.txtAdvPayment.ReadOnly = true;
            this.txtAdvPayment.Size = new System.Drawing.Size(185, 20);
            this.txtAdvPayment.TabIndex = 31;
            this.txtAdvPayment.TabStop = false;
            this.txtAdvPayment.Text = "0.00";
            this.txtAdvPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAdvPayment.Visible = false;
            // 
            // btnAddExpense
            // 
            this.btnAddExpense.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddExpense.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddExpense.Enabled = false;
            this.btnAddExpense.Location = new System.Drawing.Point(165, 27);
            this.btnAddExpense.Name = "btnAddExpense";
            this.btnAddExpense.Size = new System.Drawing.Size(25, 20);
            this.btnAddExpense.TabIndex = 37;
            this.btnAddExpense.Text = "...";
            this.btnAddExpense.Click += new System.EventHandler(this.btnAddExpense_Click);
            // 
            // txtExpenseAmount
            // 
            this.txtExpenseAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtExpenseAmount.Border.Class = "TextBoxBorder";
            this.txtExpenseAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtExpenseAmount.Enabled = false;
            this.txtExpenseAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExpenseAmount.Location = new System.Drawing.Point(192, 27);
            this.txtExpenseAmount.Name = "txtExpenseAmount";
            this.txtExpenseAmount.ReadOnly = true;
            this.txtExpenseAmount.Size = new System.Drawing.Size(185, 20);
            this.txtExpenseAmount.TabIndex = 33;
            this.txtExpenseAmount.TabStop = false;
            this.txtExpenseAmount.Text = "0.00";
            this.txtExpenseAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cboDiscount
            // 
            this.cboDiscount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDiscount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDiscount.DisplayMember = "Text";
            this.cboDiscount.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDiscount.DropDownHeight = 75;
            this.cboDiscount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDiscount.FormattingEnabled = true;
            this.cboDiscount.IntegralHeight = false;
            this.cboDiscount.ItemHeight = 14;
            this.cboDiscount.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2});
            this.cboDiscount.Location = new System.Drawing.Point(5, 48);
            this.cboDiscount.Name = "cboDiscount";
            this.cboDiscount.Size = new System.Drawing.Size(81, 20);
            this.cboDiscount.TabIndex = 34;
            this.cboDiscount.Visible = false;
            this.cboDiscount.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDiscount.WatermarkText = "Select Discount";
            this.cboDiscount.SelectedIndexChanged += new System.EventHandler(this.cboDiscount_SelectedIndexChanged);
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "%";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "Amt";
            // 
            // txtDiscount
            // 
            this.txtDiscount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDiscount.Border.Class = "TextBoxBorder";
            this.txtDiscount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscount.Location = new System.Drawing.Point(85, 48);
            this.txtDiscount.MaxLength = 14;
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(107, 20);
            this.txtDiscount.TabIndex = 35;
            this.txtDiscount.TabStop = false;
            this.txtDiscount.Text = "0.00";
            this.txtDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscount.Visible = false;
            this.txtDiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDiscount_KeyPress);
            this.txtDiscount.TextChanged += new System.EventHandler(this.txtDiscount_TextChanged);
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtSubTotal.Border.Class = "TextBoxBorder";
            this.txtSubTotal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubTotal.Location = new System.Drawing.Point(192, 6);
            this.txtSubTotal.MaxLength = 20;
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.ReadOnly = true;
            this.txtSubTotal.Size = new System.Drawing.Size(185, 20);
            this.txtSubTotal.TabIndex = 32;
            this.txtSubTotal.TabStop = false;
            this.txtSubTotal.Text = "0.00";
            this.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSubTotal.TextChanged += new System.EventHandler(this.TxtBSubtotal_TextChanged);
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTotalAmount.Border.Class = "TextBoxBorder";
            this.txtTotalAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(192, 69);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.Size = new System.Drawing.Size(185, 38);
            this.txtTotalAmount.TabIndex = 37;
            this.txtTotalAmount.TabStop = false;
            this.txtTotalAmount.Text = "0.00";
            this.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalAmount.TextChanged += new System.EventHandler(this.txtTotalAmount_TextChanged);
            // 
            // txtRemarks
            // 
            // 
            // 
            // 
            this.txtRemarks.Border.Class = "TextBoxBorder";
            this.txtRemarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtRemarks.Location = new System.Drawing.Point(100, 35);
            this.txtRemarks.MaxLength = 1000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(297, 45);
            this.txtRemarks.TabIndex = 22;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(9, 38);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 36;
            this.lblRemarks.Text = "Remarks";
            // 
            // lblClearenceDate
            // 
            this.lblClearenceDate.AutoSize = true;
            this.lblClearenceDate.Location = new System.Drawing.Point(765, 36);
            this.lblClearenceDate.Name = "lblClearenceDate";
            this.lblClearenceDate.Size = new System.Drawing.Size(81, 13);
            this.lblClearenceDate.TabIndex = 266;
            this.lblClearenceDate.Text = "Clearence Date";
            this.lblClearenceDate.Visible = false;
            // 
            // txtLeadTime
            // 
            this.txtLeadTime.DecimalPlaces = 4;
            this.txtLeadTime.Location = new System.Drawing.Point(471, 67);
            this.txtLeadTime.MaxLength = 12;
            this.txtLeadTime.Name = "txtLeadTime";
            this.txtLeadTime.ShortcutsEnabled = false;
            this.txtLeadTime.Size = new System.Drawing.Size(119, 20);
            this.txtLeadTime.TabIndex = 24;
            this.txtLeadTime.Text = "0.0000";
            this.txtLeadTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLeadTime.Visible = false;
            // 
            // dtpTrnsShipmentDate
            // 
            this.dtpTrnsShipmentDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpTrnsShipmentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTrnsShipmentDate.Location = new System.Drawing.Point(675, 37);
            this.dtpTrnsShipmentDate.Name = "dtpTrnsShipmentDate";
            this.dtpTrnsShipmentDate.Size = new System.Drawing.Size(84, 20);
            this.dtpTrnsShipmentDate.TabIndex = 29;
            this.dtpTrnsShipmentDate.Visible = false;
            // 
            // dtpClearenceDate
            // 
            this.dtpClearenceDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpClearenceDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpClearenceDate.Location = new System.Drawing.Point(859, 37);
            this.dtpClearenceDate.Name = "dtpClearenceDate";
            this.dtpClearenceDate.Size = new System.Drawing.Size(84, 20);
            this.dtpClearenceDate.TabIndex = 30;
            this.dtpClearenceDate.Visible = false;
            // 
            // dtpProductionDate
            // 
            this.dtpProductionDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpProductionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpProductionDate.Location = new System.Drawing.Point(675, 10);
            this.dtpProductionDate.Name = "dtpProductionDate";
            this.dtpProductionDate.Size = new System.Drawing.Size(84, 20);
            this.dtpProductionDate.TabIndex = 28;
            this.dtpProductionDate.Visible = false;
            // 
            // cboIncoTerms
            // 
            this.cboIncoTerms.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboIncoTerms.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboIncoTerms.DisplayMember = "Text";
            this.cboIncoTerms.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboIncoTerms.DropDownHeight = 75;
            this.cboIncoTerms.FormattingEnabled = true;
            this.cboIncoTerms.IntegralHeight = false;
            this.cboIncoTerms.ItemHeight = 14;
            this.cboIncoTerms.Location = new System.Drawing.Point(437, 35);
            this.cboIncoTerms.Name = "cboIncoTerms";
            this.cboIncoTerms.Size = new System.Drawing.Size(119, 20);
            this.cboIncoTerms.TabIndex = 22;
            this.cboIncoTerms.Visible = false;
            // 
            // lblIncoTerms
            // 
            this.lblIncoTerms.AutoSize = true;
            this.lblIncoTerms.Location = new System.Drawing.Point(391, 36);
            this.lblIncoTerms.Name = "lblIncoTerms";
            this.lblIncoTerms.Size = new System.Drawing.Size(60, 13);
            this.lblIncoTerms.TabIndex = 270;
            this.lblIncoTerms.Text = "Inco Terms";
            this.lblIncoTerms.Visible = false;
            // 
            // lblProductionDate
            // 
            this.lblProductionDate.AutoSize = true;
            this.lblProductionDate.Location = new System.Drawing.Point(585, 13);
            this.lblProductionDate.Name = "lblProductionDate";
            this.lblProductionDate.Size = new System.Drawing.Size(84, 13);
            this.lblProductionDate.TabIndex = 262;
            this.lblProductionDate.Text = "Production Date";
            this.lblProductionDate.Visible = false;
            // 
            // lblTransShipmentDate
            // 
            this.lblTransShipmentDate.AutoSize = true;
            this.lblTransShipmentDate.Location = new System.Drawing.Point(585, 40);
            this.lblTransShipmentDate.Name = "lblTransShipmentDate";
            this.lblTransShipmentDate.Size = new System.Drawing.Size(63, 13);
            this.lblTransShipmentDate.TabIndex = 263;
            this.lblTransShipmentDate.Text = "Trans. Date";
            this.lblTransShipmentDate.Visible = false;
            // 
            // lblLeadTime
            // 
            this.lblLeadTime.AutoSize = true;
            this.lblLeadTime.Location = new System.Drawing.Point(403, 67);
            this.lblLeadTime.Name = "lblLeadTime";
            this.lblLeadTime.Size = new System.Drawing.Size(57, 13);
            this.lblLeadTime.TabIndex = 260;
            this.lblLeadTime.Text = "Lead Time";
            this.lblLeadTime.Visible = false;
            // 
            // lblNoOfContainers
            // 
            this.lblNoOfContainers.AutoSize = true;
            this.lblNoOfContainers.Location = new System.Drawing.Point(765, 13);
            this.lblNoOfContainers.Name = "lblNoOfContainers";
            this.lblNoOfContainers.Size = new System.Drawing.Size(88, 13);
            this.lblNoOfContainers.TabIndex = 289;
            this.lblNoOfContainers.Text = "No Of Containers";
            this.lblNoOfContainers.Visible = false;
            // 
            // txtNoOfContainers
            // 
            this.txtNoOfContainers.DecimalPlaces = 0;
            this.txtNoOfContainers.Location = new System.Drawing.Point(859, 10);
            this.txtNoOfContainers.MaxLength = 12;
            this.txtNoOfContainers.Name = "txtNoOfContainers";
            this.txtNoOfContainers.ShortcutsEnabled = false;
            this.txtNoOfContainers.Size = new System.Drawing.Size(56, 20);
            this.txtNoOfContainers.TabIndex = 27;
            this.txtNoOfContainers.Text = "0";
            this.txtNoOfContainers.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNoOfContainers.Visible = false;
            // 
            // cboContainerType
            // 
            this.cboContainerType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboContainerType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboContainerType.DisplayMember = "Text";
            this.cboContainerType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboContainerType.DropDownHeight = 75;
            this.cboContainerType.FormattingEnabled = true;
            this.cboContainerType.IntegralHeight = false;
            this.cboContainerType.ItemHeight = 14;
            this.cboContainerType.Location = new System.Drawing.Point(694, 63);
            this.cboContainerType.Name = "cboContainerType";
            this.cboContainerType.Size = new System.Drawing.Size(119, 20);
            this.cboContainerType.TabIndex = 25;
            this.cboContainerType.Visible = false;
            // 
            // lblContainerType
            // 
            this.lblContainerType.AutoSize = true;
            this.lblContainerType.Location = new System.Drawing.Point(601, 67);
            this.lblContainerType.Name = "lblContainerType";
            this.lblContainerType.Size = new System.Drawing.Size(79, 13);
            this.lblContainerType.TabIndex = 287;
            this.lblContainerType.Text = "Container Type";
            this.lblContainerType.Visible = false;
            // 
            // btnContainerType
            // 
            this.btnContainerType.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnContainerType.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnContainerType.Location = new System.Drawing.Point(819, 63);
            this.btnContainerType.Name = "btnContainerType";
            this.btnContainerType.Size = new System.Drawing.Size(25, 20);
            this.btnContainerType.TabIndex = 26;
            this.btnContainerType.Text = "...";
            this.btnContainerType.Visible = false;
            this.btnContainerType.Click += new System.EventHandler(this.btnContainerType_Click);
            // 
            // btnIncoTerms
            // 
            this.btnIncoTerms.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnIncoTerms.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnIncoTerms.Location = new System.Drawing.Point(565, 36);
            this.btnIncoTerms.Name = "btnIncoTerms";
            this.btnIncoTerms.Size = new System.Drawing.Size(25, 20);
            this.btnIncoTerms.TabIndex = 23;
            this.btnIncoTerms.Text = "...";
            this.btnIncoTerms.Visible = false;
            this.btnIncoTerms.Click += new System.EventHandler(this.btnIncoTerms_Click);
            // 
            // cboPaymentTerms
            // 
            this.cboPaymentTerms.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboPaymentTerms.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboPaymentTerms.DisplayMember = "Text";
            this.cboPaymentTerms.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboPaymentTerms.DropDownHeight = 75;
            this.cboPaymentTerms.Enabled = false;
            this.cboPaymentTerms.FormattingEnabled = true;
            this.cboPaymentTerms.IntegralHeight = false;
            this.cboPaymentTerms.ItemHeight = 14;
            this.cboPaymentTerms.Location = new System.Drawing.Point(100, 9);
            this.cboPaymentTerms.Name = "cboPaymentTerms";
            this.cboPaymentTerms.Size = new System.Drawing.Size(146, 20);
            this.cboPaymentTerms.TabIndex = 21;
            this.cboPaymentTerms.SelectedIndexChanged += new System.EventHandler(this.cboPaymentTerms_SelectedIndexChanged);
            // 
            // lblPaymentTerms
            // 
            this.lblPaymentTerms.AutoSize = true;
            this.lblPaymentTerms.Location = new System.Drawing.Point(9, 13);
            this.lblPaymentTerms.Name = "lblPaymentTerms";
            this.lblPaymentTerms.Size = new System.Drawing.Size(80, 13);
            this.lblPaymentTerms.TabIndex = 31;
            this.lblPaymentTerms.Text = "Payment Terms";
            // 
            // lblAmountinWords
            // 
            this.lblAmountinWords.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountinWords.Location = new System.Drawing.Point(100, 87);
            this.lblAmountinWords.Name = "lblAmountinWords";
            this.lblAmountinWords.Size = new System.Drawing.Size(859, 16);
            this.lblAmountinWords.TabIndex = 291;
            // 
            // expandableSplitterTop
            // 
            this.expandableSplitterTop.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterTop.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitterTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitterTop.ExpandableControl = this.panelTop;
            this.expandableSplitterTop.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.expandableSplitterTop.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(179)))), ((int)(((byte)(219)))));
            this.expandableSplitterTop.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterTop.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterTop.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.Location = new System.Drawing.Point(0, 158);
            this.expandableSplitterTop.Name = "expandableSplitterTop";
            this.expandableSplitterTop.Size = new System.Drawing.Size(1273, 3);
            this.expandableSplitterTop.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterTop.TabIndex = 120;
            this.expandableSplitterTop.TabStop = false;
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelTop.Controls.Add(this.tcGeneral);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1273, 133);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 0;
            // 
            // tcGeneral
            // 
            this.tcGeneral.BackColor = System.Drawing.Color.Transparent;
            this.tcGeneral.CanReorderTabs = true;
            this.tcGeneral.Controls.Add(this.tabControlPanel3);
            this.tcGeneral.Controls.Add(this.tabControlPanel4);
            this.tcGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcGeneral.Location = new System.Drawing.Point(0, 0);
            this.tcGeneral.Name = "tcGeneral";
            this.tcGeneral.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcGeneral.SelectedTabIndex = 0;
            this.tcGeneral.Size = new System.Drawing.Size(1273, 133);
            this.tcGeneral.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcGeneral.TabIndex = 0;
            this.tcGeneral.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcGeneral.Tabs.Add(this.tiGeneral);
            this.tcGeneral.Tabs.Add(this.tiSuggestions);
            this.tcGeneral.TabStop = false;
            this.tcGeneral.Text = "tabControl1";
            this.tcGeneral.SelectedTabChanged += new DevComponents.DotNetBar.TabStrip.SelectedTabChangedEventHandler(this.tcGeneral_SelectedTabChanged);
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel3.Controls.Add(this.label1);
            this.tabControlPanel3.Controls.Add(this.cboTaxScheme);
            this.tabControlPanel3.Controls.Add(this.btnGRNWarehouse);
            this.tabControlPanel3.Controls.Add(this.lblGRNWarehouse);
            this.tabControlPanel3.Controls.Add(this.cboGRNWarehouse);
            this.tabControlPanel3.Controls.Add(this.clbGRN);
            this.tabControlPanel3.Controls.Add(this.lblOrderNo1);
            this.tabControlPanel3.Controls.Add(this.lblOrder1);
            this.tabControlPanel3.Controls.Add(this.lblOrderNo);
            this.tabControlPanel3.Controls.Add(this.lblOrder);
            this.tabControlPanel3.Controls.Add(this.lblPurchaseNo);
            this.tabControlPanel3.Controls.Add(this.lblDate);
            this.tabControlPanel3.Controls.Add(this.lblCriteriaText);
            this.tabControlPanel3.Controls.Add(this.lblDueDate);
            this.tabControlPanel3.Controls.Add(this.lblCriteria);
            this.tabControlPanel3.Controls.Add(this.lblStatus);
            this.tabControlPanel3.Controls.Add(this.btnAddressChange);
            this.tabControlPanel3.Controls.Add(this.dtpDate);
            this.tabControlPanel3.Controls.Add(this.lblSupplierAddressName);
            this.tabControlPanel3.Controls.Add(this.dtpDueDate);
            this.tabControlPanel3.Controls.Add(this.txtSupplierBillNo);
            this.tabControlPanel3.Controls.Add(this.lblCompany);
            this.tabControlPanel3.Controls.Add(this.txtPurchaseNo);
            this.tabControlPanel3.Controls.Add(this.lblOrderType);
            this.tabControlPanel3.Controls.Add(this.cboSupplier);
            this.tabControlPanel3.Controls.Add(this.lblSupplierBillNo);
            this.tabControlPanel3.Controls.Add(this.cboCurrency);
            this.tabControlPanel3.Controls.Add(this.ChkTGNR);
            this.tabControlPanel3.Controls.Add(this.cboWarehouse);
            this.tabControlPanel3.Controls.Add(this.lblWarehouse);
            this.tabControlPanel3.Controls.Add(this.cboOrderNo);
            this.tabControlPanel3.Controls.Add(this.btnWarehouse);
            this.tabControlPanel3.Controls.Add(this.cboOrderType);
            this.tabControlPanel3.Controls.Add(this.txtDescription);
            this.tabControlPanel3.Controls.Add(this.cboCompany);
            this.tabControlPanel3.Controls.Add(this.btnCurrency);
            this.tabControlPanel3.Controls.Add(this.lblCurrency);
            this.tabControlPanel3.Controls.Add(this.lblDescription);
            this.tabControlPanel3.Controls.Add(this.btnSupplier);
            this.tabControlPanel3.Controls.Add(this.lblStatusText);
            this.tabControlPanel3.Controls.Add(this.txtSupplierAddress);
            this.tabControlPanel3.Controls.Add(this.lblSupplier);
            this.tabControlPanel3.Controls.Add(this.btnTContextmenu);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(1273, 111);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 1;
            this.tabControlPanel3.TabItem = this.tiGeneral;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(893, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 273;
            this.label1.Text = "TaxScheme";
            // 
            // cboTaxScheme
            // 
            this.cboTaxScheme.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboTaxScheme.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTaxScheme.DisplayMember = "Text";
            this.cboTaxScheme.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboTaxScheme.DropDownHeight = 75;
            this.cboTaxScheme.FormattingEnabled = true;
            this.cboTaxScheme.IntegralHeight = false;
            this.cboTaxScheme.ItemHeight = 14;
            this.cboTaxScheme.Location = new System.Drawing.Point(963, 8);
            this.cboTaxScheme.Name = "cboTaxScheme";
            this.cboTaxScheme.Size = new System.Drawing.Size(118, 20);
            this.cboTaxScheme.TabIndex = 272;
            this.cboTaxScheme.SelectedIndexChanged += new System.EventHandler(this.cboTaxScheme_SelectedIndexChanged);
            // 
            // btnGRNWarehouse
            // 
            this.btnGRNWarehouse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnGRNWarehouse.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnGRNWarehouse.Location = new System.Drawing.Point(301, 84);
            this.btnGRNWarehouse.Name = "btnGRNWarehouse";
            this.btnGRNWarehouse.Size = new System.Drawing.Size(25, 20);
            this.btnGRNWarehouse.TabIndex = 271;
            this.btnGRNWarehouse.Text = "...";
            this.btnGRNWarehouse.Visible = false;
            this.btnGRNWarehouse.Click += new System.EventHandler(this.btnGRNWarehouse_Click);
            // 
            // lblGRNWarehouse
            // 
            this.lblGRNWarehouse.AutoSize = true;
            this.lblGRNWarehouse.BackColor = System.Drawing.Color.Transparent;
            this.lblGRNWarehouse.Location = new System.Drawing.Point(14, 86);
            this.lblGRNWarehouse.Name = "lblGRNWarehouse";
            this.lblGRNWarehouse.Size = new System.Drawing.Size(89, 13);
            this.lblGRNWarehouse.TabIndex = 270;
            this.lblGRNWarehouse.Text = "GRN Warehouse";
            this.lblGRNWarehouse.Visible = false;
            // 
            // cboGRNWarehouse
            // 
            this.cboGRNWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboGRNWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboGRNWarehouse.DisplayMember = "Text";
            this.cboGRNWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboGRNWarehouse.DropDownHeight = 75;
            this.cboGRNWarehouse.FormattingEnabled = true;
            this.cboGRNWarehouse.IntegralHeight = false;
            this.cboGRNWarehouse.ItemHeight = 14;
            this.cboGRNWarehouse.Location = new System.Drawing.Point(108, 84);
            this.cboGRNWarehouse.Name = "cboGRNWarehouse";
            this.cboGRNWarehouse.Size = new System.Drawing.Size(190, 20);
            this.cboGRNWarehouse.TabIndex = 269;
            this.cboGRNWarehouse.Visible = false;
            this.cboGRNWarehouse.SelectedIndexChanged += new System.EventHandler(this.cboGRNWarehouse_SelectedIndexChanged);
            // 
            // clbGRN
            // 
            this.clbGRN.CheckOnClick = true;
            this.clbGRN.FormattingEnabled = true;
            this.clbGRN.Location = new System.Drawing.Point(550, 11);
            this.clbGRN.Name = "clbGRN";
            this.clbGRN.Size = new System.Drawing.Size(147, 94);
            this.clbGRN.TabIndex = 268;
            this.clbGRN.SelectedIndexChanged += new System.EventHandler(this.clbGRN_SelectedIndexChanged);
            // 
            // lblOrderNo1
            // 
            this.lblOrderNo1.AutoSize = true;
            this.lblOrderNo1.BackColor = System.Drawing.Color.Transparent;
            this.lblOrderNo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderNo1.Location = new System.Drawing.Point(1251, 53);
            this.lblOrderNo1.Name = "lblOrderNo1";
            this.lblOrderNo1.Size = new System.Drawing.Size(75, 17);
            this.lblOrderNo1.TabIndex = 267;
            this.lblOrderNo1.Text = "Order No";
            // 
            // lblOrder1
            // 
            this.lblOrder1.AutoSize = true;
            this.lblOrder1.BackColor = System.Drawing.Color.Transparent;
            this.lblOrder1.Location = new System.Drawing.Point(1212, 54);
            this.lblOrder1.Name = "lblOrder1";
            this.lblOrder1.Size = new System.Drawing.Size(33, 13);
            this.lblOrder1.TabIndex = 266;
            this.lblOrder1.Text = "Order";
            // 
            // lblOrderNo
            // 
            this.lblOrderNo.AutoSize = true;
            this.lblOrderNo.BackColor = System.Drawing.Color.Transparent;
            this.lblOrderNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderNo.Location = new System.Drawing.Point(1251, 15);
            this.lblOrderNo.Name = "lblOrderNo";
            this.lblOrderNo.Size = new System.Drawing.Size(75, 17);
            this.lblOrderNo.TabIndex = 265;
            this.lblOrderNo.Text = "Order No";
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.BackColor = System.Drawing.Color.Transparent;
            this.lblOrder.Location = new System.Drawing.Point(1220, 16);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(33, 13);
            this.lblOrder.TabIndex = 264;
            this.lblOrder.Text = "Order";
            // 
            // lblPurchaseNo
            // 
            this.lblPurchaseNo.AutoSize = true;
            this.lblPurchaseNo.BackColor = System.Drawing.Color.Transparent;
            this.lblPurchaseNo.Location = new System.Drawing.Point(703, 11);
            this.lblPurchaseNo.Name = "lblPurchaseNo";
            this.lblPurchaseNo.Size = new System.Drawing.Size(73, 13);
            this.lblPurchaseNo.TabIndex = 13;
            this.lblPurchaseNo.Text = "Order Number";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Location = new System.Drawing.Point(703, 36);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 188;
            this.lblDate.Text = "Date";
            // 
            // lblCriteriaText
            // 
            this.lblCriteriaText.AutoSize = true;
            this.lblCriteriaText.BackColor = System.Drawing.Color.Transparent;
            this.lblCriteriaText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCriteriaText.Location = new System.Drawing.Point(1121, 37);
            this.lblCriteriaText.Name = "lblCriteriaText";
            this.lblCriteriaText.Size = new System.Drawing.Size(39, 13);
            this.lblCriteriaText.TabIndex = 259;
            this.lblCriteriaText.Text = "Criteria";
            this.lblCriteriaText.Visible = false;
            // 
            // lblDueDate
            // 
            this.lblDueDate.AutoSize = true;
            this.lblDueDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDueDate.Location = new System.Drawing.Point(703, 61);
            this.lblDueDate.Name = "lblDueDate";
            this.lblDueDate.Size = new System.Drawing.Size(53, 13);
            this.lblDueDate.TabIndex = 189;
            this.lblDueDate.Text = "Due Date";
            // 
            // lblCriteria
            // 
            this.lblCriteria.AutoSize = true;
            this.lblCriteria.BackColor = System.Drawing.Color.Transparent;
            this.lblCriteria.Location = new System.Drawing.Point(1053, 37);
            this.lblCriteria.Name = "lblCriteria";
            this.lblCriteria.Size = new System.Drawing.Size(39, 13);
            this.lblCriteria.TabIndex = 258;
            this.lblCriteria.Text = "Criteria";
            this.lblCriteria.Visible = false;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Location = new System.Drawing.Point(1089, 13);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 191;
            this.lblStatus.Text = "Status";
            // 
            // btnAddressChange
            // 
            this.btnAddressChange.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddressChange.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddressChange.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnAddressChange.Location = new System.Drawing.Point(518, 34);
            this.btnAddressChange.Name = "btnAddressChange";
            this.btnAddressChange.Size = new System.Drawing.Size(25, 20);
            this.btnAddressChange.TabIndex = 12;
            this.btnAddressChange.Text = "6";
            this.btnAddressChange.Click += new System.EventHandler(this.btnAddressChange_Click);
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(781, 33);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(103, 20);
            this.dtpDate.TabIndex = 14;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // lblSupplierAddressName
            // 
            this.lblSupplierAddressName.AutoSize = true;
            this.lblSupplierAddressName.BackColor = System.Drawing.Color.White;
            this.lblSupplierAddressName.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSupplierAddressName.Location = new System.Drawing.Point(346, 35);
            this.lblSupplierAddressName.Name = "lblSupplierAddressName";
            this.lblSupplierAddressName.Size = new System.Drawing.Size(33, 9);
            this.lblSupplierAddressName.TabIndex = 254;
            this.lblSupplierAddressName.Text = "Address";
            // 
            // dtpDueDate
            // 
            this.dtpDueDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDueDate.Location = new System.Drawing.Point(781, 58);
            this.dtpDueDate.Name = "dtpDueDate";
            this.dtpDueDate.Size = new System.Drawing.Size(103, 20);
            this.dtpDueDate.TabIndex = 15;
            this.dtpDueDate.ValueChanged += new System.EventHandler(this.dtpDueDate_ValueChanged);
            // 
            // txtSupplierBillNo
            // 
            this.txtSupplierBillNo.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtSupplierBillNo.Border.Class = "TextBoxBorder";
            this.txtSupplierBillNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSupplierBillNo.Location = new System.Drawing.Point(781, 83);
            this.txtSupplierBillNo.MaxLength = 20;
            this.txtSupplierBillNo.Name = "txtSupplierBillNo";
            this.txtSupplierBillNo.Size = new System.Drawing.Size(103, 20);
            this.txtSupplierBillNo.TabIndex = 16;
            this.txtSupplierBillNo.Visible = false;
            this.txtSupplierBillNo.TextChanged += new System.EventHandler(this.txtSupplierBillNo_TextChanged);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.BackColor = System.Drawing.Color.Transparent;
            this.lblCompany.Location = new System.Drawing.Point(24, 13);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 200;
            this.lblCompany.Text = "Company";
            // 
            // txtPurchaseNo
            // 
            this.txtPurchaseNo.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtPurchaseNo.Border.Class = "TextBoxBorder";
            this.txtPurchaseNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPurchaseNo.Location = new System.Drawing.Point(780, 8);
            this.txtPurchaseNo.MaxLength = 20;
            this.txtPurchaseNo.Name = "txtPurchaseNo";
            this.txtPurchaseNo.Size = new System.Drawing.Size(103, 20);
            this.txtPurchaseNo.TabIndex = 13;
            this.txtPurchaseNo.TabStop = false;
            this.txtPurchaseNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPurchaseNo_KeyPress);
            // 
            // lblOrderType
            // 
            this.lblOrderType.AutoSize = true;
            this.lblOrderType.BackColor = System.Drawing.Color.Transparent;
            this.lblOrderType.Location = new System.Drawing.Point(24, 37);
            this.lblOrderType.Name = "lblOrderType";
            this.lblOrderType.Size = new System.Drawing.Size(60, 13);
            this.lblOrderType.TabIndex = 201;
            this.lblOrderType.Text = "Order Type";
            this.lblOrderType.Visible = false;
            // 
            // cboSupplier
            // 
            this.cboSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSupplier.DisplayMember = "Text";
            this.cboSupplier.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSupplier.DropDownHeight = 75;
            this.cboSupplier.FormattingEnabled = true;
            this.cboSupplier.IntegralHeight = false;
            this.cboSupplier.ItemHeight = 14;
            this.cboSupplier.Location = new System.Drawing.Point(345, 9);
            this.cboSupplier.Name = "cboSupplier";
            this.cboSupplier.Size = new System.Drawing.Size(164, 20);
            this.cboSupplier.TabIndex = 9;
            this.cboSupplier.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSupplier.WatermarkText = "Select Supplier";
            this.cboSupplier.SelectedIndexChanged += new System.EventHandler(this.cboSupplier_SelectedIndexChanged);
            this.cboSupplier.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblSupplierBillNo
            // 
            this.lblSupplierBillNo.AutoSize = true;
            this.lblSupplierBillNo.BackColor = System.Drawing.Color.Transparent;
            this.lblSupplierBillNo.Location = new System.Drawing.Point(703, 86);
            this.lblSupplierBillNo.Name = "lblSupplierBillNo";
            this.lblSupplierBillNo.Size = new System.Drawing.Size(75, 13);
            this.lblSupplierBillNo.TabIndex = 202;
            this.lblSupplierBillNo.Text = "Supplier BillNo";
            this.lblSupplierBillNo.Visible = false;
            // 
            // cboCurrency
            // 
            this.cboCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCurrency.DisplayMember = "Text";
            this.cboCurrency.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCurrency.DropDownHeight = 75;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.IntegralHeight = false;
            this.cboCurrency.ItemHeight = 14;
            this.cboCurrency.Location = new System.Drawing.Point(108, 61);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(190, 20);
            this.cboCurrency.TabIndex = 7;
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            this.cboCurrency.SelectedValueChanged += new System.EventHandler(this.cboCurrency_SelectedValueChanged);
            this.cboCurrency.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // ChkTGNR
            // 
            this.ChkTGNR.BackColor = System.Drawing.Color.Transparent;
            this.ChkTGNR.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.ChkTGNR.Checked = true;
            this.ChkTGNR.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkTGNR.Location = new System.Drawing.Point(784, 83);
            this.ChkTGNR.Name = "ChkTGNR";
            this.ChkTGNR.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ChkTGNR.Size = new System.Drawing.Size(98, 19);
            this.ChkTGNR.TabIndex = 21;
            this.ChkTGNR.Text = "GRN Required          ";
            this.ChkTGNR.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.ChkTGNR.UseVisualStyleBackColor = false;
            this.ChkTGNR.Visible = false;
            this.ChkTGNR.CheckedChanged += new System.EventHandler(this.ChkTGNR_CheckedChanged);
            // 
            // cboWarehouse
            // 
            this.cboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWarehouse.DisplayMember = "Text";
            this.cboWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWarehouse.DropDownHeight = 75;
            this.cboWarehouse.FormattingEnabled = true;
            this.cboWarehouse.IntegralHeight = false;
            this.cboWarehouse.ItemHeight = 14;
            this.cboWarehouse.Location = new System.Drawing.Point(1215, 34);
            this.cboWarehouse.Name = "cboWarehouse";
            this.cboWarehouse.Size = new System.Drawing.Size(61, 20);
            this.cboWarehouse.TabIndex = 5;
            this.cboWarehouse.Visible = false;
            this.cboWarehouse.SelectedIndexChanged += new System.EventHandler(this.cboWarehouse_SelectedIndexChanged);
            this.cboWarehouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.BackColor = System.Drawing.Color.Transparent;
            this.lblWarehouse.Location = new System.Drawing.Point(1160, 37);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(62, 13);
            this.lblWarehouse.TabIndex = 203;
            this.lblWarehouse.Text = "Warehouse";
            this.lblWarehouse.Visible = false;
            // 
            // cboOrderNo
            // 
            this.cboOrderNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOrderNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOrderNo.DisplayMember = "Text";
            this.cboOrderNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboOrderNo.DropDownHeight = 75;
            this.cboOrderNo.FormattingEnabled = true;
            this.cboOrderNo.IntegralHeight = false;
            this.cboOrderNo.ItemHeight = 14;
            this.cboOrderNo.Location = new System.Drawing.Point(219, 34);
            this.cboOrderNo.Name = "cboOrderNo";
            this.cboOrderNo.Size = new System.Drawing.Size(79, 20);
            this.cboOrderNo.TabIndex = 4;
            this.cboOrderNo.Visible = false;
            this.cboOrderNo.SelectedIndexChanged += new System.EventHandler(this.cboOrderNo_SelectedIndexChanged);
            this.cboOrderNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // btnWarehouse
            // 
            this.btnWarehouse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnWarehouse.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnWarehouse.Location = new System.Drawing.Point(1302, 34);
            this.btnWarehouse.Name = "btnWarehouse";
            this.btnWarehouse.Size = new System.Drawing.Size(26, 20);
            this.btnWarehouse.TabIndex = 6;
            this.btnWarehouse.Text = "...";
            this.btnWarehouse.Visible = false;
            this.btnWarehouse.Click += new System.EventHandler(this.btnWarehouse_Click);
            // 
            // cboOrderType
            // 
            this.cboOrderType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOrderType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOrderType.DisplayMember = "Text";
            this.cboOrderType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboOrderType.DropDownHeight = 75;
            this.cboOrderType.FormattingEnabled = true;
            this.cboOrderType.IntegralHeight = false;
            this.cboOrderType.ItemHeight = 14;
            this.cboOrderType.Location = new System.Drawing.Point(108, 33);
            this.cboOrderType.Name = "cboOrderType";
            this.cboOrderType.Size = new System.Drawing.Size(104, 20);
            this.cboOrderType.TabIndex = 3;
            this.cboOrderType.Visible = false;
            this.cboOrderType.SelectedIndexChanged += new System.EventHandler(this.cboOrderType_SelectedIndexChanged);
            this.cboOrderType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // txtDescription
            // 
            // 
            // 
            // 
            this.txtDescription.Border.Class = "TextBoxBorder";
            this.txtDescription.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDescription.Location = new System.Drawing.Point(1056, 58);
            this.txtDescription.MaxLength = 200;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(157, 53);
            this.txtDescription.TabIndex = 17;
            this.txtDescription.TabStop = false;
            this.txtDescription.Visible = false;
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 75;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(108, 9);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(190, 20);
            this.cboCompany.TabIndex = 2;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // btnCurrency
            // 
            this.btnCurrency.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCurrency.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCurrency.Location = new System.Drawing.Point(301, 61);
            this.btnCurrency.Name = "btnCurrency";
            this.btnCurrency.Size = new System.Drawing.Size(25, 20);
            this.btnCurrency.TabIndex = 8;
            this.btnCurrency.Text = "...";
            this.btnCurrency.Visible = false;
            this.btnCurrency.Click += new System.EventHandler(this.btnCurrency_Click);
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrency.Location = new System.Drawing.Point(24, 61);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(49, 13);
            this.lblCurrency.TabIndex = 52;
            this.lblCurrency.Text = "Currency";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblDescription.Location = new System.Drawing.Point(1053, 63);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 13);
            this.lblDescription.TabIndex = 244;
            this.lblDescription.Text = "Description";
            this.lblDescription.Visible = false;
            // 
            // btnSupplier
            // 
            this.btnSupplier.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSupplier.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSupplier.Location = new System.Drawing.Point(518, 9);
            this.btnSupplier.Name = "btnSupplier";
            this.btnSupplier.Size = new System.Drawing.Size(25, 20);
            this.btnSupplier.TabIndex = 10;
            this.btnSupplier.Text = "...";
            this.btnSupplier.Click += new System.EventHandler(this.btnSupplier_Click);
            // 
            // lblStatusText
            // 
            this.lblStatusText.AutoSize = true;
            this.lblStatusText.BackColor = System.Drawing.Color.Transparent;
            this.lblStatusText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.lblStatusText.Location = new System.Drawing.Point(1153, 9);
            this.lblStatusText.Name = "lblStatusText";
            this.lblStatusText.Size = new System.Drawing.Size(62, 20);
            this.lblStatusText.TabIndex = 19;
            this.lblStatusText.Text = "Status";
            // 
            // txtSupplierAddress
            // 
            this.txtSupplierAddress.BackColor = System.Drawing.Color.White;
            this.txtSupplierAddress.Location = new System.Drawing.Point(345, 34);
            this.txtSupplierAddress.Multiline = true;
            this.txtSupplierAddress.Name = "txtSupplierAddress";
            this.txtSupplierAddress.ReadOnly = true;
            this.txtSupplierAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtSupplierAddress.Size = new System.Drawing.Size(164, 70);
            this.txtSupplierAddress.TabIndex = 11;
            this.txtSupplierAddress.TabStop = false;
            this.txtSupplierAddress.TextChanged += new System.EventHandler(this.TxtTVendorAddress_TextChanged);
            // 
            // lblSupplier
            // 
            this.lblSupplier.AutoSize = true;
            this.lblSupplier.BackColor = System.Drawing.Color.Transparent;
            this.lblSupplier.Location = new System.Drawing.Point(300, 13);
            this.lblSupplier.Name = "lblSupplier";
            this.lblSupplier.Size = new System.Drawing.Size(45, 13);
            this.lblSupplier.TabIndex = 252;
            this.lblSupplier.Text = "Supplier";
            // 
            // btnTContextmenu
            // 
            this.btnTContextmenu.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnTContextmenu.Location = new System.Drawing.Point(345, 72);
            this.btnTContextmenu.Name = "btnTContextmenu";
            this.btnTContextmenu.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.btnTContextmenu.Size = new System.Drawing.Size(20, 23);
            this.btnTContextmenu.TabIndex = 11;
            this.btnTContextmenu.Text = "6";
            this.btnTContextmenu.UseVisualStyleBackColor = true;
            this.btnTContextmenu.Visible = false;
            this.btnTContextmenu.Click += new System.EventHandler(this.btnTContextmenu_Click);
            // 
            // tiGeneral
            // 
            this.tiGeneral.AttachedControl = this.tabControlPanel3;
            this.tiGeneral.Name = "tiGeneral";
            this.tiGeneral.Text = "General";
            // 
            // tabControlPanel4
            // 
            this.tabControlPanel4.Controls.Add(this.dgvSuggestions);
            this.tabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel4.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel4.Name = "tabControlPanel4";
            this.tabControlPanel4.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel4.Size = new System.Drawing.Size(1273, 111);
            this.tabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel4.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel4.Style.GradientAngle = 90;
            this.tabControlPanel4.TabIndex = 2;
            this.tabControlPanel4.TabItem = this.tiSuggestions;
            // 
            // dgvSuggestions
            // 
            this.dgvSuggestions.AllowUserToDeleteRows = false;
            this.dgvSuggestions.BackgroundColor = System.Drawing.Color.White;
            this.dgvSuggestions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSuggestions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UserID,
            this.UserName,
            this.Status,
            this.VerifiedDate,
            this.Comment});
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSuggestions.DefaultCellStyle = dataGridViewCellStyle16;
            this.dgvSuggestions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSuggestions.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSuggestions.Location = new System.Drawing.Point(1, 1);
            this.dgvSuggestions.Name = "dgvSuggestions";
            this.dgvSuggestions.Size = new System.Drawing.Size(1271, 109);
            this.dgvSuggestions.TabIndex = 0;
            this.dgvSuggestions.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSuggestions_CellEndEdit);
            // 
            // UserID
            // 
            this.UserID.HeaderText = "UserID";
            this.UserID.Name = "UserID";
            this.UserID.Visible = false;
            // 
            // UserName
            // 
            this.UserName.HeaderText = "User Name";
            this.UserName.Name = "UserName";
            this.UserName.ReadOnly = true;
            this.UserName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.UserName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.UserName.Width = 250;
            // 
            // Status
            // 
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // VerifiedDate
            // 
            this.VerifiedDate.HeaderText = "Verified Date";
            this.VerifiedDate.Name = "VerifiedDate";
            this.VerifiedDate.ReadOnly = true;
            this.VerifiedDate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.VerifiedDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Comment
            // 
            this.Comment.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Comment.HeaderText = "Comment";
            this.Comment.Name = "Comment";
            this.Comment.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Comment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tiSuggestions
            // 
            this.tiSuggestions.AttachedControl = this.tabControlPanel4;
            this.tiSuggestions.Name = "tiSuggestions";
            this.tiSuggestions.Text = "Suggestions";
            this.tiSuggestions.Visible = false;
            // 
            // PurchaseOrderBindingNavigator
            // 
            this.PurchaseOrderBindingNavigator.AccessibleDescription = "DotNetBar Bar (PurchaseOrderBindingNavigator)";
            this.PurchaseOrderBindingNavigator.AccessibleName = "DotNetBar Bar";
            this.PurchaseOrderBindingNavigator.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.PurchaseOrderBindingNavigator.Dock = System.Windows.Forms.DockStyle.Top;
            this.PurchaseOrderBindingNavigator.DockLine = 1;
            this.PurchaseOrderBindingNavigator.DockOffset = 73;
            this.PurchaseOrderBindingNavigator.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.PurchaseOrderBindingNavigator.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.PurchaseOrderBindingNavigator.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorClearItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorCancelItem,
            this.btnActions,
            this.BtnItem,
            this.BtnDiscount,
            this.BtnTax,
            this.BtnUom,
            this.btnAccountSettings,
            this.btnSupplierHistory,
            this.BtnPrint,
            this.BtnEmail,
            this.btnPickList,
            this.btnPickListEmail,
            this.BtnHelp});
            this.PurchaseOrderBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.PurchaseOrderBindingNavigator.Name = "PurchaseOrderBindingNavigator";
            this.PurchaseOrderBindingNavigator.Size = new System.Drawing.Size(1273, 25);
            this.PurchaseOrderBindingNavigator.Stretch = true;
            this.PurchaseOrderBindingNavigator.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PurchaseOrderBindingNavigator.TabIndex = 12;
            this.PurchaseOrderBindingNavigator.TabStop = false;
            this.PurchaseOrderBindingNavigator.Text = "bar2";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.Tooltip = "Add New Information";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Tooltip = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.PurchaseOrderBindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorClearItem
            // 
            this.BindingNavigatorClearItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorClearItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorClearItem.Image")));
            this.BindingNavigatorClearItem.Name = "BindingNavigatorClearItem";
            this.BindingNavigatorClearItem.Text = "Clear";
            this.BindingNavigatorClearItem.Tooltip = "Clear";
            this.BindingNavigatorClearItem.Visible = false;
            this.BindingNavigatorClearItem.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Tooltip = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorCancelItem
            // 
            this.BindingNavigatorCancelItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorCancelItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorCancelItem.Image")));
            this.BindingNavigatorCancelItem.Name = "BindingNavigatorCancelItem";
            this.BindingNavigatorCancelItem.Text = "Cancel";
            this.BindingNavigatorCancelItem.Tooltip = "Cancel";
            this.BindingNavigatorCancelItem.Click += new System.EventHandler(this.BindingNavigatorCancelItem_Click);
            // 
            // btnActions
            // 
            this.btnActions.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnActions.Image = ((System.Drawing.Image)(resources.GetObject("btnActions.Image")));
            this.btnActions.Name = "btnActions";
            this.btnActions.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.bnExpense,
            this.bnSubmitForApproval,
            this.btnApprove,
            this.btnReject,
            this.btnSuggest,
            this.btnDeny,
            this.btnShowComments,
            this.btnDocuments});
            this.btnActions.Text = "Actions";
            this.btnActions.Tooltip = "Actions";
            this.btnActions.PopupOpen += new DevComponents.DotNetBar.DotNetBarManager.PopupOpenEventHandler(this.btnActions_PopupOpen);
            // 
            // bnExpense
            // 
            this.bnExpense.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnExpense.Image = ((System.Drawing.Image)(resources.GetObject("bnExpense.Image")));
            this.bnExpense.Name = "bnExpense";
            this.bnExpense.Text = "Expenses";
            this.bnExpense.Tooltip = "Expenses";
            this.bnExpense.Click += new System.EventHandler(this.bnExpense_Click);
            // 
            // bnSubmitForApproval
            // 
            this.bnSubmitForApproval.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnSubmitForApproval.Image = ((System.Drawing.Image)(resources.GetObject("bnSubmitForApproval.Image")));
            this.bnSubmitForApproval.Name = "bnSubmitForApproval";
            this.bnSubmitForApproval.Text = "Submit For Approval";
            this.bnSubmitForApproval.Tooltip = "Submit For Approval";
            this.bnSubmitForApproval.Visible = false;
            this.bnSubmitForApproval.Click += new System.EventHandler(this.bnSubmitForApproval_Click);
            // 
            // btnApprove
            // 
            this.btnApprove.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnApprove.Image = ((System.Drawing.Image)(resources.GetObject("btnApprove.Image")));
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Text = "Approve";
            this.btnApprove.Tooltip = "Approve";
            this.btnApprove.Click += new System.EventHandler(this.btnApprove_Click);
            // 
            // btnReject
            // 
            this.btnReject.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnReject.Image = ((System.Drawing.Image)(resources.GetObject("btnReject.Image")));
            this.btnReject.Name = "btnReject";
            this.btnReject.Text = "Reject";
            this.btnReject.Tooltip = "Reject";
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // btnSuggest
            // 
            this.btnSuggest.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSuggest.Image = ((System.Drawing.Image)(resources.GetObject("btnSuggest.Image")));
            this.btnSuggest.Name = "btnSuggest";
            this.btnSuggest.Text = "Suggest";
            this.btnSuggest.Tooltip = "Suggest";
            this.btnSuggest.Click += new System.EventHandler(this.btnSuggest_Click);
            // 
            // btnDeny
            // 
            this.btnDeny.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDeny.Image = ((System.Drawing.Image)(resources.GetObject("btnDeny.Image")));
            this.btnDeny.Name = "btnDeny";
            this.btnDeny.Text = "Deny";
            this.btnDeny.Tooltip = "Deny";
            this.btnDeny.Click += new System.EventHandler(this.btnDeny_Click);
            // 
            // btnShowComments
            // 
            this.btnShowComments.Image = ((System.Drawing.Image)(resources.GetObject("btnShowComments.Image")));
            this.btnShowComments.Name = "btnShowComments";
            this.btnShowComments.Text = "Show Comments";
            this.btnShowComments.Tooltip = "Show Comments";
            this.btnShowComments.Visible = false;
            this.btnShowComments.Click += new System.EventHandler(this.btnShowComments_Click);
            // 
            // btnDocuments
            // 
            this.btnDocuments.Image = global::MyBooksERP.Properties.Resources.document_register1;
            this.btnDocuments.Name = "btnDocuments";
            this.btnDocuments.Text = "Documents";
            this.btnDocuments.Tooltip = "Documents";
            this.btnDocuments.Click += new System.EventHandler(this.btnDocuments_Click);
            // 
            // BtnItem
            // 
            this.BtnItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnItem.Image = ((System.Drawing.Image)(resources.GetObject("BtnItem.Image")));
            this.BtnItem.Name = "BtnItem";
            this.BtnItem.Text = "Product";
            this.BtnItem.Tooltip = "Product";
            this.BtnItem.Visible = false;
            this.BtnItem.Click += new System.EventHandler(this.BtnItem_Click);
            // 
            // BtnDiscount
            // 
            this.BtnDiscount.Name = "BtnDiscount";
            // 
            // BtnTax
            // 
            this.BtnTax.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnTax.Icon = ((System.Drawing.Icon)(resources.GetObject("BtnTax.Icon")));
            this.BtnTax.Name = "BtnTax";
            this.BtnTax.Text = "Tax";
            this.BtnTax.Tooltip = "Tax";
            this.BtnTax.Visible = false;
            // 
            // BtnUom
            // 
            this.BtnUom.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnUom.Icon = ((System.Drawing.Icon)(resources.GetObject("BtnUom.Icon")));
            this.BtnUom.Name = "BtnUom";
            this.BtnUom.Text = "Uom";
            this.BtnUom.Tooltip = "Unit Of Measurement";
            this.BtnUom.Click += new System.EventHandler(this.BtnUom_Click);
            // 
            // btnAccountSettings
            // 
            this.btnAccountSettings.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAccountSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnAccountSettings.Image")));
            this.btnAccountSettings.Name = "btnAccountSettings";
            this.btnAccountSettings.Text = "Account";
            this.btnAccountSettings.Tooltip = "Account Settings";
            this.btnAccountSettings.Visible = false;
            this.btnAccountSettings.Click += new System.EventHandler(this.btnAccountSettings_Click);
            // 
            // btnSupplierHistory
            // 
            this.btnSupplierHistory.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSupplierHistory.Image = ((System.Drawing.Image)(resources.GetObject("btnSupplierHistory.Image")));
            this.btnSupplierHistory.Name = "btnSupplierHistory";
            this.btnSupplierHistory.Text = "Supplier History";
            this.btnSupplierHistory.Tooltip = "Supplier History";
            this.btnSupplierHistory.Visible = false;
            this.btnSupplierHistory.Click += new System.EventHandler(this.btnSupplierHistory_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // btnPickList
            // 
            this.btnPickList.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPickList.Image = ((System.Drawing.Image)(resources.GetObject("btnPickList.Image")));
            this.btnPickList.Name = "btnPickList";
            this.btnPickList.Text = "Pick List";
            this.btnPickList.Visible = false;
            this.btnPickList.Click += new System.EventHandler(this.btnPickList_Click);
            // 
            // btnPickListEmail
            // 
            this.btnPickListEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPickListEmail.Image = ((System.Drawing.Image)(resources.GetObject("btnPickListEmail.Image")));
            this.btnPickListEmail.Name = "btnPickListEmail";
            this.btnPickListEmail.Text = "Pick List Email";
            this.btnPickListEmail.Visible = false;
            this.btnPickListEmail.Click += new System.EventHandler(this.btnPickListEmail_Click);
            // 
            // BtnHelp
            // 
            this.BtnHelp.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Tooltip = "Help";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // CMSVendorAddress
            // 
            this.CMSVendorAddress.Name = "ContextMenuStrip1";
            this.CMSVendorAddress.Size = new System.Drawing.Size(61, 4);
            // 
            // TmrPurchase
            // 
            this.TmrPurchase.Interval = 2000;
            this.TmrPurchase.Tick += new System.EventHandler(this.TmrPurchaseOrder_Tick);
            // 
            // TmrFocus
            // 
            this.TmrFocus.Tick += new System.EventHandler(this.TmrFocus_Tick);
            // 
            // ErrPurchase
            // 
            this.ErrPurchase.ContainerControl = this;
            this.ErrPurchase.RightToLeft = true;
            // 
            // ImgPurchase
            // 
            this.ImgPurchase.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgPurchase.ImageStream")));
            this.ImgPurchase.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgPurchase.Images.SetKeyName(0, "Purchase Indent.ICO");
            this.ImgPurchase.Images.SetKeyName(1, "Purchase Order.ico");
            this.ImgPurchase.Images.SetKeyName(2, "Purchase Invoice.ico");
            this.ImgPurchase.Images.SetKeyName(3, "GRN.ICO");
            this.ImgPurchase.Images.SetKeyName(4, "Purchase Order Return.ico");
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.lblCreatedByText);
            this.pnlBottom.Controls.Add(this.lblCreatedDateValue);
            this.pnlBottom.Controls.Add(this.lblPurchasestatus);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(3, 546);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1273, 26);
            this.pnlBottom.TabIndex = 291;
            // 
            // lblCreatedByText
            // 
            this.lblCreatedByText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedByText.CloseButtonVisible = false;
            this.lblCreatedByText.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblCreatedByText.Location = new System.Drawing.Point(891, 0);
            this.lblCreatedByText.Name = "lblCreatedByText";
            this.lblCreatedByText.OptionsButtonVisible = false;
            this.lblCreatedByText.Size = new System.Drawing.Size(380, 24);
            this.lblCreatedByText.TabIndex = 258;
            this.lblCreatedByText.Text = "Created By";
            // 
            // lblCreatedDateValue
            // 
            this.lblCreatedDateValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedDateValue.CloseButtonVisible = false;
            this.lblCreatedDateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCreatedDateValue.Location = new System.Drawing.Point(393, 0);
            this.lblCreatedDateValue.Name = "lblCreatedDateValue";
            this.lblCreatedDateValue.OptionsButtonVisible = false;
            this.lblCreatedDateValue.Size = new System.Drawing.Size(878, 24);
            this.lblCreatedDateValue.TabIndex = 256;
            this.lblCreatedDateValue.Text = "Created Date";
            // 
            // lblPurchasestatus
            // 
            this.lblPurchasestatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblPurchasestatus.CloseButtonVisible = false;
            this.lblPurchasestatus.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPurchasestatus.Image = ((System.Drawing.Image)(resources.GetObject("lblPurchasestatus.Image")));
            this.lblPurchasestatus.Location = new System.Drawing.Point(0, 0);
            this.lblPurchasestatus.Name = "lblPurchasestatus";
            this.lblPurchasestatus.OptionsButtonVisible = false;
            this.lblPurchasestatus.Size = new System.Drawing.Size(393, 24);
            this.lblPurchasestatus.TabIndex = 20;
            // 
            // CntxtHistory
            // 
            this.CntxtHistory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ShowItemHistory});
            this.CntxtHistory.Name = "CntxtHistory";
            this.CntxtHistory.Size = new System.Drawing.Size(172, 26);
            // 
            // ShowItemHistory
            // 
            this.ShowItemHistory.Name = "ShowItemHistory";
            this.ShowItemHistory.Size = new System.Drawing.Size(171, 22);
            this.ShowItemHistory.Text = "Show Item History";
            this.ShowItemHistory.Click += new System.EventHandler(this.ShowItemHistory_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Item Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 180;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewTextBoxColumn3.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 180;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewTextBoxColumn4.HeaderText = "QtyReceived";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Visible = false;
            this.dataGridViewTextBoxColumn4.Width = 217;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn5.HeaderText = "Batch Number";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn6.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn6.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewTextBoxColumn7.HeaderText = "Total";
            this.dataGridViewTextBoxColumn7.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewTextBoxColumn8.HeaderText = "Status";
            this.dataGridViewTextBoxColumn8.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewTextBoxColumn9.HeaderText = "OrderDetailID";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // dataGridViewTextBoxColumn10
            // 
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewTextBoxColumn10.HeaderText = "PurchaseOrderID";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Visible = false;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn12.DefaultCellStyle = dataGridViewCellStyle25;
            this.dataGridViewTextBoxColumn12.HeaderText = "PurchaseOrderID";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Visible = false;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "BatchID";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "OrderQuantity";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Visible = false;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "BatchID";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.Visible = false;
            // 
            // calendarColumn1
            // 
            this.calendarColumn1.HeaderText = "ExpiryDate";
            this.calendarColumn1.Name = "calendarColumn1";
            this.calendarColumn1.Visible = false;
            // 
            // FrmPurchaseOrder
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.ClientSize = new System.Drawing.Size(1276, 572);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.panelEx1);
            this.Controls.Add(this.dockSite2);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.expandableSplitterLeft);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.dockSite3);
            this.Controls.Add(this.dockSite4);
            this.Controls.Add(this.dockSite5);
            this.Controls.Add(this.dockSite6);
            this.Controls.Add(this.dockSite8);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPurchaseOrder";
            this.Text = "Purchase Order";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmPurchase_Load);
            this.Shown += new System.EventHandler(this.FrmPurchase_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPurchase_FormClosing);
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchaseDisplay)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcPurchase)).EndInit();
            this.tcPurchase.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchase)).EndInit();
            this.tabControlPanel2.ResumeLayout(false);
            this.panelEx2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocationDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.panelEx1.ResumeLayout(false);
            this.panelBottom.ResumeLayout(false);
            this.panelGridBottom.ResumeLayout(false);
            this.panelGridBottom.PerformLayout();
            this.pnlAmount.ResumeLayout(false);
            this.pnlAmount.PerformLayout();
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).EndInit();
            this.tcGeneral.ResumeLayout(false);
            this.tabControlPanel3.ResumeLayout(false);
            this.tabControlPanel3.PerformLayout();
            this.tabControlPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSuggestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrPurchase)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            this.CntxtHistory.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>

        #endregion

        #region Declarations

        public FrmMain objFrmTradingMain;
        public bool MblnDocumentChanged = false;
        public string MstrFileName = "";
        private MessageBoxIcon MmessageIcon;
        private bool MblnChangeStatus;                    //Check state of the page
        private bool MblnAddStatus;                       //Status for Addition/Updation mode 
        private bool MblnPrintEmailPermission = false;
        private bool MblnAddPermission = false;
        private bool MblnUpdatePermission = false;
        private bool MblnDeletePermission = false;
        private bool MblnAddUpdatePermission = false;
        private bool MblnCancelPermission = false;
        private bool MblnIsFromCancellation = false;
        private bool MblnIsFromSubmitForApproval = false;
        private bool MblnCanShow;
        private int MintFromForm;                         //  From Indent 1,From Order 2 , From Invoice 3, From GRN 4
        private int MintComboID = 0;                      //  setting comboboxvalue to this variable when clicking reference button.
        private int MintCnt = 0;                          //  Vendor Address
        private int MintVendorAddID = 0;                  //  VendorAddressID when selecting contextmenu
        private int MintPurOrderCompanyID = 0;            //  Purchase Ordered CompanyID/Purchase Invoiced CompanyID
        private bool MblnIsFromClear = false;
        private bool MblnIsEditable = true;
        private string MstrMessageCommon;                 //  variable for assigning message
        private ArrayList MaMessageArr;                 // Error Message display
        private ArrayList MaStatusMessage;

        clsBLLPurchaseOrder MobjClsBLLPurchase;
        ClsLogWriter MObjLogs;                          //  Object for Class Clslogs
        ClsNotification mObjNotification;               //  Object for Class ClsNotification
        clsBLLCommonUtility MobjclsBLLCommonUtility;    //  Object for Class clsBLLCommonUtility 
        DataTable dtTempDetails = new DataTable();
        int MintBaseCurrencyScale = 2;
        int MintExchangeCurrencyScale = 2;
        Boolean blnIsCancel = false, blnIsUpdate = false;
        int intTempMenuID = 0, intTempCompany = 0, intTempEmployee = 0; // For Permission Related (Please don't use this Variable)
        string strTempTableName = ""; // For Permission Related (Please don't use this Variable)
        public int PintApprovePermission = 0;
        DataTable dtItemDetails = new DataTable();
        public int PintOperationTypeID = 0;  // for identifying material return ( 0 - For Normal ,else specify corresponding operationtypeid)

        public long lngReferenceID = 0;

        private bool blnIsFromDisplay = false;

        private bool blnPurchaseOrder = true;

        public string strCondition = "";

        private int SItemID;

        private long MlngGRNID = 0;

        #endregion

        #region Constructor

        public FrmPurchaseOrder(int intFormType)
        {
            InitializeComponent();

            MobjClsBLLPurchase = new clsBLLPurchaseOrder();
            MObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MobjclsBLLCommonUtility = new clsBLLCommonUtility();
            MintFromForm = intFormType;          // Form Identification
            LoadInitials();
        }

        public FrmPurchaseOrder(long lngGRNID)
        {
            InitializeComponent();
            MobjClsBLLPurchase = new clsBLLPurchaseOrder();
            MObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MobjclsBLLCommonUtility = new clsBLLCommonUtility();
            MintFromForm = 2;
            MlngGRNID = lngGRNID;
            LoadInitials();
        }

      
        #endregion

        #region Functions

        private void LoadInitials()
        {
            switch (MintFromForm)
            {
                case 1:
                    this.Icon = ((System.Drawing.Icon)(Properties.Resources.Purchase_Quotation1));
                    break;
                case 2:
                    this.Icon = ((System.Drawing.Icon)(Properties.Resources.Purchase_Order1));
                    break;
                case 3:
                    this.Icon = ((System.Drawing.Icon)(Properties.Resources.GRN1));
                    break;
                case 4:
                    this.Icon = ((System.Drawing.Icon)(Properties.Resources.Purchase_Invoice1));
                    break;
            }
        }

        private void DisplayPurchaseGRN()
        {
            DataTable dtTemp = new clsBLLDirectGRN().DisplayPurchaseGRN(MlngGRNID);
            if (dtTemp != null && dtTemp.Rows.Count > 0)
            {
                cboCompany.SelectedValue = dtTemp.Rows[0]["CompanyID"].ToString();
                cboSupplier.SelectedValue = dtTemp.Rows[0]["VendorID"].ToString();
                cboGRNWarehouse.SelectedValue = dtTemp.Rows[0]["WarehouseID"].ToString();
                cboOrderType.SelectedValue = 25;
                ((ListBox)(clbGRN)).DataSource = dtTemp;
                ((ListBox)(clbGRN)).ValueMember = "GRNID";
                ((ListBox)(clbGRN)).DisplayMember = "GRNNo";
                for (int i = 0; i < clbGRN.Items.Count; i++)
                {
                    if (MlngGRNID == (((System.Data.DataRowView)(clbGRN.Items[i])).Row.ItemArray[0]).ToInt64())
                    {
                        clbGRN.SetItemChecked(i, true);
                    }
                    else
                    {
                        clbGRN.SetItemChecked(i, false);
                    }
                    clbGRN_SelectedIndexChanged(null, null);
                }
                cboOrderType.Enabled = false;
                ChkTGNR.Checked = true;
                cboCompany.Enabled=cboTaxScheme.Enabled = false;
                cboSupplier.Enabled = false;
                txtSupplierAddress.Enabled = false;
                clbGRN.Enabled = false;
                cboCurrency.Enabled = false;
                btnSupplier.Enabled = false;
                btnAddressChange.Enabled = false;
                cboGRNWarehouse.Enabled = false;
            }
        }

        private bool AddToAddressMenu(int iVendorID)
        {
            try
            {
                int i;

                CMSVendorAddress.Items.Clear();
                DataTable dtAddMenu = DtGetAddressName(iVendorID);

                for (i = 0; i <= dtAddMenu.Rows.Count - 1; i++)
                {
                    ToolStripMenuItem tItem = new ToolStripMenuItem();
                    tItem.Tag = Convert.ToString(dtAddMenu.Rows[i][0]);
                    tItem.Text = Convert.ToString(dtAddMenu.Rows[i][1]);
                    CMSVendorAddress.Items.Add(tItem);
                }
                CMSVendorAddress.ItemClicked -= new ToolStripItemClickedEventHandler(this.MenuItem_Click);
                CMSVendorAddress.ItemClicked += new ToolStripItemClickedEventHandler(this.MenuItem_Click);
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in AddToAddressMenu() " + ex.Message);
                MObjLogs.WriteLog("Error in AddToAddressMenu() " + ex.Message, 2);
                return false;
            }
        }

        private void MenuItem_Click(System.Object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(cboSupplier.SelectedValue) > 0)
                {
                    BtnTVenderAddress.Text = e.ClickedItem.Text;
                    lblSupplierAddressName.Text = e.ClickedItem.Text + " Address";
                    MintVendorAddID = Convert.ToInt32(e.ClickedItem.Tag);
                    DisplayAddress(Convert.ToInt32(cboSupplier.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in MenuItem_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in MenuItem_Click() " + ex.Message, 2);
            }
        }

        public DataTable DtGetAddressName(int iVendorID)
        {
            try
            {
                DataTable sRecordValues;
                sRecordValues = MobjClsBLLPurchase.FillCombos(new string[] { "VendorAddID,AddressName", "InvVendorAddress", "VendorID = " + iVendorID });
                return sRecordValues;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DtGetAddressName() " + ex.Message);
                MObjLogs.WriteLog("Error in DtGetAddressName() " + ex.Message, 2);
                return null;
            }
        }

        private int GetCustomerAccountID(int intVendorID)
        {
            try
            {
                DataTable dtVendorDetails = MobjClsBLLPurchase.FillCombos(new string[] { "AccountID", "InvVendorInformation", "VendorID = " + intVendorID });
                int intAccountID = 0;
                if (dtVendorDetails.Rows.Count > 0)
                {
                    if (dtVendorDetails.Rows[0]["AccountID"] != DBNull.Value)
                        intAccountID = Convert.ToInt32(dtVendorDetails.Rows[0]["AccountID"]);
                }
                return intAccountID;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in GetCustomerAccountID() " + ex.Message);
                MObjLogs.WriteLog("Error in GetCustomerAccountID() " + ex.Message, 2);
                return 0;
            }
        }

        private void FillParameters()
        {
            MobjClsBLLPurchase.PobjClsDTOPurchase.strPurchaseNo = txtPurchaseNo.Text.Trim();
            MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            MobjClsBLLPurchase.PobjClsDTOPurchase.strDate = dtpDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID = Convert.ToInt32(cboSupplier.SelectedValue);
            MobjClsBLLPurchase.PobjClsDTOPurchase.strDueDate = dtpDueDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLPurchase.PobjClsDTOPurchase.strRemarks = txtRemarks.Text.Trim();
            //MobjClsBLLPurchase.PobjClsDTOPurchase.intGrandDiscountID = Convert.ToInt32(cboDiscount.SelectedValue);

            //if (MobjClsBLLPurchase.PobjClsDTOPurchase.intGrandDiscountID > 0)
            //    MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountAmount = txtDiscount.Text.ToDecimal();
            //else
            //    MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountAmount = 0;
            if (cboDiscount.SelectedItem != null)
            {
                if (cboDiscount.SelectedItem.ToString() == "%")
                {
                    MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountPercent = txtDiscount.Text.ToDecimal();
                    MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountAmt = (txtDiscount.Text.ToDecimal() / 100) * Convert.ToDecimal(txtSubTotal.Text);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.blnGrandDiscount = true;
                }
                else
                {
                    MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountAmt = txtDiscount.Text.ToDecimal();
                    MobjClsBLLPurchase.PobjClsDTOPurchase.blnGrandDiscount = false;
                }
            }
            MobjClsBLLPurchase.PobjClsDTOPurchase.decExpenseAmount = 0;
            MobjClsBLLPurchase.PobjClsDTOPurchase.intTaxSchemeID = cboTaxScheme.SelectedValue.ToInt32();
            MobjClsBLLPurchase.PobjClsDTOPurchase.decTaxAmount = txtTaxAmount.Text.ToDecimal();
            MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandAmount = Convert.ToDecimal(txtSubTotal.Text);
            MobjClsBLLPurchase.PobjClsDTOPurchase.decNetAmount = Convert.ToDecimal(txtTotalAmount.Text);
            MobjClsBLLPurchase.PobjClsDTOPurchase.decAdvAmount = Convert.ToDecimal(txtAdvPayment.Text);
            MobjClsBLLPurchase.PobjClsDTOPurchase.decNetAmountRounded = Math.Ceiling(MobjClsBLLPurchase.PobjClsDTOPurchase.decNetAmount);
            if (MblnAddStatus)
            {
                MobjClsBLLPurchase.PobjClsDTOPurchase.intCreatedBy = ClsCommonSettings.UserID;
                MobjClsBLLPurchase.PobjClsDTOPurchase.strCreatedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
            }
            MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            MobjClsBLLPurchase.PobjClsDTOPurchase.intCurrencyID = Convert.ToInt32(cboCurrency.SelectedValue);
            if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == 0)
            {
                switch (MintFromForm)
                {
                    case 1:
                        MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (int)OperationStatusType.PQuotationOpened;
                        break;
                    case 2:
                        MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (int)OperationStatusType.POrderOpened;
                        break;
                    case 3:
                        MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (int)OperationStatusType.GRNOpened;
                        break;
                    case 4:
                        MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (int)OperationStatusType.PInvoiceOpened;
                        break;
                }

            }

            //   MobjClsBLLPurchase.PobjClsDTOPurchase.intDebitHeadID = (Int32)ClsCommonSettings.PintSIDebitHeadID;
            //if (MobjClsBLLPurchase.PobjClsDTOPurchase.intDebitHeadID == 0)
            //{
            //    DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "AccountSettings", "CompanyID=" + cboCompany.SelectedValue.ToString() + " AND OperationModID=" + (int)TransactionTypes.CreditPurchase });
            //    if (datTemp.Rows.Count > 0)
            //        try { MobjClsBLLPurchase.PobjClsDTOPurchase.intDebitHeadID = Convert.ToInt32(datTemp.Rows[0]["AccountID"].ToString()); }
            //        catch { }
            //}
            //  MobjClsBLLPurchase.PobjClsDTOPurchase.intCreditHeadID = GetCustomerAccountID(Convert.ToInt32(cboSupplier.SelectedValue));
            MobjClsBLLPurchase.PobjClsDTOPurchase.strAccountDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");

            switch (MintFromForm)
            {
                case 1:
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorAddID = MintVendorAddID;
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType = Convert.ToInt32(cboOrderType.SelectedValue);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intReferenceID = Convert.ToInt64(cboOrderNo.SelectedValue);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intPaymentTermsID = Convert.ToInt32(cboPaymentTerms.SelectedValue);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intEmployeeID = ClsCommonSettings.intEmployeeID;
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intIncoTerms = Convert.ToInt32(cboIncoTerms.SelectedValue);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.decLeadTime = Convert.ToDecimal(txtLeadTime.Text);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.strProductionDate = dtpProductionDate.Value.ToString("dd-MMM-yyyy");
                    MobjClsBLLPurchase.PobjClsDTOPurchase.strTranShipmentDate = dtpTrnsShipmentDate.Value.ToString("dd-MMM-yyyy");
                    MobjClsBLLPurchase.PobjClsDTOPurchase.strClearenceDate = dtpClearenceDate.Value.ToString("dd-MMM-yyyy");
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intContainerTypeID = cboContainerType.SelectedValue.ToInt32();
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intNoOfContainers = txtNoOfContainers.Text.ToInt32();
                    break;
                case 2:
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorAddID = MintVendorAddID;
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intPaymentTermsID = Convert.ToInt32(cboPaymentTerms.SelectedValue);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType = Convert.ToInt32(cboOrderType.SelectedValue);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intReferenceID = Convert.ToInt64(cboOrderNo.SelectedValue);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.blnGRNRequired = ChkTGNR.Checked;

                    MobjClsBLLPurchase.PobjClsDTOPurchase.intIncoTerms = Convert.ToInt32(cboIncoTerms.SelectedValue);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.decLeadTime = Convert.ToDecimal(txtLeadTime.Text);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.strProductionDate = dtpProductionDate.Value.ToString("dd-MMM-yyyy");
                    MobjClsBLLPurchase.PobjClsDTOPurchase.strTranShipmentDate = dtpTrnsShipmentDate.Value.ToString("dd-MMM-yyyy");
                    MobjClsBLLPurchase.PobjClsDTOPurchase.strClearenceDate = dtpClearenceDate.Value.ToString("dd-MMM-yyyy");
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intContainerTypeID = cboContainerType.SelectedValue.ToInt32();
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intNoOfContainers = txtNoOfContainers.Text.ToInt32();

                    break;
                case 3:
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorAddID = MintVendorAddID;
                    //  MobjClsBLLPurchase.PobjClsDTOPurchase.intPaymentTermsID = Convert.ToInt32(cboPaymentTerms.SelectedValue);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType = Convert.ToInt32(cboOrderType.SelectedValue);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intReferenceID = Convert.ToInt64(cboOrderNo.SelectedValue);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intWarehouseID = Convert.ToInt32(cboWarehouse.SelectedValue);
                    break;
                case 4:
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorAddID = MintVendorAddID;
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intPaymentTermsID = Convert.ToInt32(cboPaymentTerms.SelectedValue);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType = Convert.ToInt32(cboOrderType.SelectedValue);
                    if (cboOrderNo.SelectedValue != null)
                        MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseOrderID = Convert.ToInt64(cboOrderNo.SelectedValue);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.strPurchaseBillNo = txtSupplierBillNo.Text.Trim();
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intWarehouseID = Convert.ToInt32(cboWarehouse.SelectedValue);
                    MobjClsBLLPurchase.PobjClsDTOPurchase.decVoucherAmount = Convert.ToDecimal(txtTotalAmount.Text) - Convert.ToDecimal(txtAdvPayment.Text);
                    if (MobjClsBLLPurchase.PobjClsDTOPurchase.decVoucherAmount < 0)
                        MobjClsBLLPurchase.PobjClsDTOPurchase.decVoucherAmount = 0;
                    MobjClsBLLPurchase.PobjClsDTOPurchase.blnGRNRequired = ChkTGNR.Checked;
                    if (cboPurchaseAccount.SelectedIndex != -1)
                        MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseAccountID = cboPurchaseAccount.SelectedValue.ToInt32();
                    else
                        MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseAccountID = 0;
                    //   if (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.PInvoiceDirect)
                    // MobjClsBLLPurchase.PobjClsDTOPurchase.blnGRNRequired = true;
                    break;
            }
            if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationCancelled || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderCancelled || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.GRNCancelled || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PInvoiceCancelled)
            {
                MobjClsBLLPurchase.PobjClsDTOPurchase.blnCancelled = true;
                MobjClsBLLPurchase.PobjClsDTOPurchase.strDescription = txtDescription.Text.Trim();
            }
            else
            {
                MobjClsBLLPurchase.PobjClsDTOPurchase.blnCancelled = false;
            }

            if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderRejected || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationRejected || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderApproved || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationApproved)
            {
                MobjClsBLLPurchase.PobjClsDTOPurchase.strVerifcationDescription = txtDescription.Text.Trim();
            }
            else
            {
                MobjClsBLLPurchase.PobjClsDTOPurchase.strVerifcationDescription = "";
            }
            MobjClsBLLPurchase.PobjClsDTOPurchase.decExpenseAmount = txtExpenseAmount.Text.ToDecimal();
        }

        private void FillDetailParameters()
        {
            if (dgvPurchase.CurrentRow != null)
                dgvPurchase.CurrentCell = dgvPurchase["ItemCode", dgvPurchase.CurrentRow.Index];
            MobjClsBLLPurchase.PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection = new System.Collections.Generic.List<clsDTOPurchaseDetails>();
            for (int i = 0; i < dgvPurchase.Rows.Count; i++)
            {
                if (dgvPurchase["ItemID", i].Value != null && dgvPurchase["Quantity", i].Value != null)
                {
                    clsDTOPurchaseDetails objClsDtoPurchaseDetails = new clsDTOPurchaseDetails();
                    objClsDtoPurchaseDetails.intItemID = Convert.ToInt32(dgvPurchase["ItemID", i].Value);
                    objClsDtoPurchaseDetails.decQuantity = Convert.ToDecimal(dgvPurchase["Quantity", i].Value);
                    objClsDtoPurchaseDetails.decOldReceivedQuantity = Convert.ToDecimal(dgvPurchase["ReceivedQty", i].Value);
                    if (dgvPurchase["OrderedQty", i].Value.ToDecimal() != 0)
                        objClsDtoPurchaseDetails.decOrderQuantity = Convert.ToDecimal(dgvPurchase["OrderedQty", i].Value);
                    if (dgvPurchase["ExtraQty", i].Value != null)
                        objClsDtoPurchaseDetails.decExtraQuantity = Convert.ToDecimal(dgvPurchase["ExtraQty", i].Value);
                    objClsDtoPurchaseDetails.intUOMID = Convert.ToInt32(dgvPurchase["UOM", i].Tag);
                    objClsDtoPurchaseDetails.decRate = Convert.ToDecimal(dgvPurchase["Rate", i].Value);
                    if (dgvPurchase["Discount", i].Value != null)
                    {
                        if (dgvPurchase["Discount", i].Value.ToString() == "%")
                        {
                            objClsDtoPurchaseDetails.decDiscountAmt = dgvPurchase["DiscountAmt", i].Value.ToDecimal();
                            objClsDtoPurchaseDetails.decDiscountPercent = dgvPurchase["DiscountAmount", i].Value.ToDecimal();
                            objClsDtoPurchaseDetails.blnDiscount = true;
                        }
                        else
                        {
                            objClsDtoPurchaseDetails.decDiscountAmt = dgvPurchase["DiscountAmount", i].Value.ToDecimal();
                            objClsDtoPurchaseDetails.blnDiscount = false;
                        }
                    }
                    //if (dgvPurchase["DiscountAmount", i].Value != DBNull.Value)
                    //{
                    //    objClsDtoPurchaseDetails.decDiscountAmount = Convert.ToDecimal(dgvPurchase["DiscountAmount", i].Value);
                    //}
                    objClsDtoPurchaseDetails.decGrandAmount = Convert.ToDecimal(dgvPurchase["GrandAmount", i].Value);
                    objClsDtoPurchaseDetails.decNetAmount = Convert.ToDecimal(dgvPurchase["NetAmount", i].Value);
                    if (dgvPurchase["Batchnumber", i].Value != null)
                        objClsDtoPurchaseDetails.strBatchNumber = Convert.ToString(dgvPurchase["Batchnumber", i].Value);

                    objClsDtoPurchaseDetails.strItemCode = Convert.ToString(dgvPurchase["ItemCode", i].Value);
                    objClsDtoPurchaseDetails.strItemName = Convert.ToString(dgvPurchase["ItemName", i].Value);
                    objClsDtoPurchaseDetails.decPurchaseRate = dgvPurchase["PurchaseRate", i].Value.ToDecimal();

                    if (dgvPurchase["ExpiryDate", i].Value != null)
                        objClsDtoPurchaseDetails.strExpiryDate = Convert.ToDateTime(dgvPurchase["ExpiryDate", i].Value).ToString("dd-MMM-yyyy");

                    MobjClsBLLPurchase.PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection.Add(objClsDtoPurchaseDetails);
                }
            }
        }
        /// <summary>
        /// Shows quotation number, if purchase invoice from Order has quotation
        /// </summary>
        private void ShowQuotationNo()
        {
            DataTable datQuotation = MobjClsBLLPurchase.FillCombos(new string[] { "PQM.PurchaseQuotationNo ", "InvPurchaseQuotationMaster  PQM left join InvPurchaseOrderMaster POM ON PQM.PurchaseQuotationID = POM.ReferenceID", "POM.PurchaseOrderID = " + Convert.ToInt64(cboOrderNo.SelectedValue) });
            if (datQuotation.Rows.Count > 0)
            {
                lblOrder.Visible = true;
                lblOrderNo.Visible = true;
                lblOrder.Text = "Quotation No";
                lblOrderNo.Text = datQuotation.Rows[0][0].ToString();
                lblOrder.Top = lblDate.Top;
                lblOrderNo.Top = lblOrder.Top - 1;
                lblOrder.Left = lblStatus.Left;
                lblOrderNo.Left = lblStatusText.Left;
            }
            else
            {
                lblOrder.Visible = false;
                lblOrderNo.Visible = false;
            }
        }
        /// <summary>
        /// Show Order No and Quotation No for invoice in GRN
        /// </summary>
        private void ShowNoForGRN()
        {
            DataTable datOrderNo = MobjClsBLLPurchase.FillCombos(new string[] { "POM.PurchaseOrderNo", "InvPurchaseOrderMaster POM left join InvPurchaseInvoiceMaster PIM ON POM.PurchaseOrderID = PIM.PurchaseOrderID", "PIM.PurchaseInvoiceID = " + Convert.ToInt64(cboOrderNo.SelectedValue) });
            if (datOrderNo.Rows.Count > 0)
            {
                lblOrder.Visible = true;
                lblOrderNo.Visible = true;
                lblOrder.Text = "Order No";
                lblOrderNo.Text = datOrderNo.Rows[0][0].ToString();
                lblOrder.Top = lblDate.Top;
                lblOrderNo.Top = lblOrder.Top - 1;
                lblOrder.Left = lblStatus.Left;
                lblOrderNo.Left = lblStatusText.Left;
            }
            else
            {
                lblOrder.Visible = false;
                lblOrderNo.Visible = false;
            }
            DataTable datQtnNo = MobjClsBLLPurchase.FillCombos(new string[] { "PQM.PurchaseQuotationNo", "InvPurchaseQuotationMaster  PQM left join InvPurchaseOrderMaster POM ON PQM.PurchaseQuotationID = POM.ReferenceID", "POM.PurchaseOrderID = ISNULL((SELECT POM.PurchaseOrderID From InvPurchaseOrderMaster POM left join InvPurchaseInvoiceMaster PIM ON POM.PurchaseOrderID = PIM.PurchaseOrderID WHERE PIM.PurchaseInvoiceID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + "),0)" });
            if (datQtnNo.Rows.Count > 0)
            {
                lblOrder1.Visible = true;
                lblOrderNo1.Visible = true;
                lblOrder1.Text = "Quotation No";
                lblOrderNo1.Text = datQtnNo.Rows[0][0].ToString();
                lblOrder1.Top = lblDate.Top + 25;
                lblOrderNo1.Top = lblOrder.Top - 1 + 25;
                lblOrder1.Left = lblStatus.Left;
                lblOrderNo1.Left = lblStatusText.Left;
            }
            else
            {
                lblOrder1.Visible = false;
                lblOrderNo1.Visible = false;
            }
        }
        private bool SavePurchaseQuotation()
        {
            try
            {
                if (!MblnIsFromCancellation)
                {
                    if (MblnAddStatus == true)
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 937, out MmessageIcon);
                    else
                    {
                        if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationSubmittedForApproval || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationSubmitted)
                        {
                            if (ClsCommonSettings.PQApproval)
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 952, out MmessageIcon).Replace("***", "quotation");
                            else
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9011, out MmessageIcon).Replace("*", "Quotation");
                        }
                        else
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
                    }

                    if (MblnCanShow)
                    {
                        if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                            return false;
                    }
                }

                if (MblnAddStatus && txtPurchaseNo.ReadOnly && MobjClsBLLPurchase.IsPurchaseNoExists(txtPurchaseNo.Text, MintFromForm, cboCompany.SelectedValue.ToInt32()))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 973, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation");
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    else
                        GenerateOrderNo();
                }
        

                if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.PQuotationFromRFQ && MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseQuotationID", "InvPurchaseQuotationMaster", "QuotationType = " + (int)OperationOrderType.PQuotationFromRFQ + " And ReferenceID = " + cboOrderNo.SelectedValue.ToInt32() + " And VendorID = " + cboSupplier.SelectedValue.ToInt32() + " And PurchaseQuotationID <> " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID }).Rows.Count > 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 976, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                FillParameters();
                FillDetailParameters();

                if (MobjClsBLLPurchase.SavePurchaseQuotation(MblnAddStatus))
                {
                    txtPurchaseNo.Tag = MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID;
                    MObjLogs.WriteLog("Saved successfully:SaveCurrency()  " + this.Name + "", 0);
                    return true;

                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SavePurchaseQuotation() " + ex.Message);
                MObjLogs.WriteLog("Error in SavePurchaseQuotation() " + ex.Message, 2);
                return false;
            }
        }

        private bool SavePurchase(int iFrom)
        {
            try
            {
                switch (iFrom)
                {
                    case 1:
                        if (PurchaseQuotationvalidation())
                        {
                            if (SavePurchaseQuotation())
                            {
                                MblnAddStatus = false;
                                if (!MblnIsFromCancellation && !blnIsFrmApproval && !MblnIsFromSubmitForApproval)
                                {

                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                }
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                return true;

                            }
                        }
                        return false;
                    case 2:
                        if (Purchasevalidation())
                        {
                            if (SavePurchaseOrder())
                            {
                                MblnAddStatus = false;
                                if (!MblnIsFromCancellation && !blnIsFrmApproval && !MblnIsFromSubmitForApproval)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                }
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                return true;

                            }
                        }
                        return false;
                    case 3:

                        if (FillItemLocationDetails() && Purchasevalidation())
                        {
                            if (SavePurchaseGRN())
                            {
                                MblnAddStatus = false;
                                if (!MblnIsFromCancellation && !blnIsFrmApproval && !MblnIsFromSubmitForApproval)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                }
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                return true;
                            }
                        }
                        return false;

                    case 4:


                        if (FillItemLocationDetails() && Purchasevalidation())
                        {
                            if (SavePurchaseInvoice())
                            {
                                MblnAddStatus = false;
                                if (!MblnIsFromCancellation && !blnIsFrmApproval && !MblnIsFromSubmitForApproval)
                                {

                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                }
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                return true;
                            }
                        }
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SavePurchase() " + ex.Message);
                MObjLogs.WriteLog("Error in SavePurchase() " + ex.Message, 2);
                return false;
            }
        }

        private void LoadMessage()
        {
            try
            {
                MaMessageArr = new ArrayList();
                MaStatusMessage = new ArrayList();
                MaMessageArr = mObjNotification.FillMessageArray((int)FormID.Purchase, ClsCommonSettings.ProductID);
                MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.Purchase, ClsCommonSettings.ProductID);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadMessage() " + ex.Message);
                MObjLogs.WriteLog("Error in LoadMessage() " + ex.Message, 2);
            }
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                switch (MintFromForm)
                {
                    case 1: objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.PurchaseQuotation, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                        break;
                    case 2: objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.PurchaseOrder, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                        break;
                    case 3:

                        objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.GRN, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                        break;
                    case 4: objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.PurchaseInvoice, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                        break;
                }

                //if (MblnAddPermission == true || MblnUpdatePermission == true)
                //    MblnAddUpdatePermission = true;

                DataTable DtPermission = objClsBLLPermissionSettings.GetMenuPermissions(ClsCommonSettings.RoleID);
                if (DtPermission.Rows.Count == 0)
                {
                    BtnItem.Enabled = btnAccountSettings.Enabled = BtnUom.Enabled = BtnTax.Enabled = BtnDiscount.Enabled = false;
                }
                else
                {
                    DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.RawMaterials).ToString();
                    BtnItem.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);

                    DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AccountSettings).ToString();
                    btnAccountSettings.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);

                    DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.UOM).ToString();
                    BtnUom.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);

                    //DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Tax).ToString();
                    //BtnTax.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);

                    DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Discounts).ToString();
                    BtnDiscount.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);
                }
                //DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "RoleDetails", "RoleID=" + ClsCommonSettings.RoleID + " " +
                //        "AND CompanyID=" + ClsCommonSettings.CompanyID + " AND MenuID=" + (int)MenuID.VendorHistory + " AND IsView=1"});
                //if (datTemp != null)
                //{
                //    if (datTemp.Rows.Count > 0)
                //        btnSupplierHistory.Enabled = true;
                //    else
                //        btnSupplierHistory.Enabled = false;
                //}
                //else
                //    btnSupplierHistory.Enabled = false;
            }
            else
            {
                MblnAddPermission = MblnAddUpdatePermission = MblnCancelPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
                //  btnSupplierHistory.Enabled = true;
            }
            MblnCancelPermission = MblnUpdatePermission;
            //  BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            //  BindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
            //  BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;            
        }

        private void AddNewPurchase()
        {
            try
            {
                MobjClsBLLPurchase = new clsBLLPurchaseOrder();

                dtItemDetails = new DataTable();

                dtItemDetails.Columns.Add("ItemCode");
                dtItemDetails.Columns.Add("ItemName");
                dtItemDetails.Columns.Add("ItemID");
                dtItemDetails.Columns.Add("BatchNo");
                dtItemDetails.Columns.Add("UOMID");


                lblDescription.Visible = false;
                txtDescription.Visible = false;

                MintBaseCurrencyScale = 2;
                MintExchangeCurrencyScale = 2;
                MblnAddStatus = true;
                txtPurchaseNo.Tag = 0;
                txtPurchaseNo.Text = "";
                ClearControls();
                MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = 0;
                SetStatusLabelText();
                if (MintFromForm == 1)
                    btnActions.Enabled = false;
                btnApprove.Visible = false;
                btnReject.Visible = false;
                bnSubmitForApproval.Visible = false;
                MblnIsFromSubmitForApproval = false;
                btnSuggest.Visible = btnDeny.Visible = false;
                //btnShowComments.Visible = false;
                tiSuggestions.Visible = false;
                GenerateOrderNo();

                DisplayAllNoInSearchGrid();

                switch (MintFromForm)
                {
                    case 1:
                        MstrMessageCommon = "Add new Purchase Quotation";
                        btnActions.Enabled = false;
                        btnAddExpense.Enabled = false;
                        break;
                    case 2:
                        MstrMessageCommon = "Add new Purchase Order";
                        ChkTGNR.Checked = true;
                        break;
                    case 3:
                        if (PintOperationTypeID == 0)
                            MstrMessageCommon = "Add new GRN";
                        else
                            MstrMessageCommon = "Add new material return";
                        btnActions.Enabled = false;
                        break;
                    case 4:
                        MstrMessageCommon = "Add new Purchase Invoice";
                        ChkTGNR.Checked = true;
                        break;
                    case 5:
                        MstrMessageCommon = "Add new Purchase Return";
                        break;
                }

                btnDocuments.Visible = false;
                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrFocus.Enabled = true;
                ErrPurchase.Clear();
                MblnChangeStatus = false;
                BtnPrint.Enabled = false;
                BtnEmail.Enabled = false;
                btnPickList.Enabled = false;
                btnPickListEmail.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                MblnIsFromCancellation = false;
                BindingNavigatorCancelItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorClearItem.Enabled = false;
                cboTaxScheme.Enabled = true;
                dgvPurchase.Columns["ItemCode"].ReadOnly = dgvPurchase.Columns["ItemName"].ReadOnly = false;
                dgvPurchase.Rows.Clear();
                dgvLocationDetails.Rows.Clear();
                lblCreatedByText.Text = "Created By :" + ClsCommonSettings.strEmployeeName;
                lblCreatedDateValue.Text = "Created Date : " + ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                if (MintFromForm != 1)
                    cboOrderType.Focus();
                else
                    cboCurrency.Focus();
                cboOrderType_SelectedIndexChanged(null, null);
                cboOrderNo_SelectedIndexChanged(null, null);
                BindingNavigatorAddNewItem.Enabled = false;

                clbGRN.Enabled = true;
                cboWarehouse.Enabled = true;
                //cboDiscount.SelectedIndex = -1;
                //txtDiscount.Text = "";

                if (MintFromForm != 3)
                    SetControlsVisibility(true);
                tcPurchase.SelectedTab = tpItemDetails;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in AddNewPurchase() " + ex.Message);
                MObjLogs.WriteLog("Error in AddNewPurchase() " + ex.Message, 2);
            }
        }

        private void ClearControls()
        {
            dgvPurchase.Rows.Clear();
            dgvLocationDetails.Rows.Clear();
            dgvPurchase.ClearSelection();
            dgvLocationDetails.Rows.Clear();

            cboIncoTerms.SelectedIndex = -1;
            MblnIsEditable = true;
            txtLeadTime.Text = "0";
            dtpProductionDate.Value = ClsCommonSettings.GetServerDate();
            dtpTrnsShipmentDate.Value = ClsCommonSettings.GetServerDate();
            dtpClearenceDate.Value = ClsCommonSettings.GetServerDate();
            txtNoOfContainers.Text = "0";
            cboContainerType.SelectedIndex = -1;

            MobjClsBLLPurchase.PobjClsDTOPurchase.decExpenseAmount = 0;
            MblnIsFromClear = true;
            ChkTGNR.Checked = false;
            dgvPurchase.ReadOnly = false;
            GrandAmount.ReadOnly = NetAmount.ReadOnly = PurchaseRate.ReadOnly = true;
            panelTop.Enabled = true;
            ErrPurchase.Clear();
            lblPurchasestatus.Text = "";
            cboOrderType.Enabled = true;
            cboOrderNo.Enabled = true;
            lblCriteria.Visible = false;
            lblCriteriaText.Visible = false;
            cboOrderNo.Enabled = false;

            if (MintFromForm != 3)
            {
                cboSupplier.Enabled = true;
                cboCurrency.Enabled = true;
                // btnSupplier.Enabled = true;
                dtpDate.Enabled = true;
                dgvPurchase.ReadOnly = false;
                GrandAmount.ReadOnly = NetAmount.ReadOnly = PurchaseRate.ReadOnly = true;
                btnAddressChange.Enabled = true;
                dtpDueDate.Enabled = true;
            }
            if (MintFromForm != 4)
            {
                lblPurchaseAccount.Visible = false;
                cboPurchaseAccount.Visible = false;
                btnAccount.Visible = false;
            }
            dtpDate.Enabled = true;
            btnTContextmenu.Enabled = true;
            BtnTVenderAddress.Enabled = true;
            MblnCanShow = true;
            cboCompany.Enabled = true;

            txtSPurchaseNo.Text = "";
            cboSCompany.SelectedIndex = -1;
            cboSExecutive.SelectedIndex = -1;
            cboSSupplier.SelectedIndex = -1;
            cboSStatus.SelectedIndex = -1;

            if (ClsCommonSettings.CompanyID <= 0)
                ClsCommonSettings.CompanyID = MobjclsBLLCommonUtility.GetCompanyID();

            cboCompany.SelectedValue = ClsCommonSettings.CompanyID;
            BtnTVenderAddress.Text = "";
            txtSupplierAddress.Clear();
            CMSVendorAddress.Items.Clear();

            cboSupplier.SelectedIndex = -1;
            txtSupplierAddress.Text = "";
            cboPaymentTerms.SelectedValue = (int)PaymentTerms.Cash;
            cboCurrency.SelectedIndex = -1;
            txtPurchaseNo.Enabled = true;
            cboWarehouse.SelectedIndex = -1;
            cboDiscount.SelectedIndex = -1;
            cboTaxScheme.SelectedValue = 0;
            dtpDate.Value = ClsCommonSettings.GetServerDate();
            dtpDueDate.Value = ClsCommonSettings.GetServerDate();

            txtSupplierBillNo.Text = "";


            txtDescription.Clear();
            txtRemarks.Text = "";
            txtTaxAmount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            cboDiscount.SelectedIndex = -1;
            txtSubTotal.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            txtDiscount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            txtTotalAmount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            txtExpenseAmount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            txtAdvPayment.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            txtExchangeCurrencyRate.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            cboGRNWarehouse.Enabled = true;
            switch (MintFromForm)
            {
                case 1:

                    cboOrderType.SelectedValue = (Int32)OperationOrderType.PQuotationDirect;
                    if (cboOrderType.SelectedValue == null)
                    {
                        DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "CommonOrderTypeReference", "OrderTypeID=" + (Int32)OperationOrderType.PQuotationDirect });
                        if (datTemp.Rows.Count > 0)
                            cboOrderType.Text = datTemp.Rows[0]["OrderType"].ToString();
                    }
                    btnDocuments.Visible = false;
                    break;
                case 2:

                    cboOrderType.SelectedValue = (Int32)OperationOrderType.POrderDirect;
                    if (cboOrderType.SelectedValue == null)
                    {
                        DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "CommonOrderTypeReference", "OrderTypeID=" + (Int32)OperationOrderType.POrderDirect });
                        if (datTemp.Rows.Count > 0)
                            cboOrderType.Text = datTemp.Rows[0]["OrderType"].ToString();
                    }
                    btnActions.Enabled = false;
                    btnDocuments.Visible = false;
                    btnAddExpense.Enabled = false;
                    ChkTGNR.Enabled = true;
                    break;
                case 3:

                    cboOrderType.SelectedValue = (Int32)OperationOrderType.GRNFromInvoice;
                    if (cboOrderType.SelectedValue == null)
                    {
                        DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "CommonOrderTypeReference", "OrderTypeID In (" + (Int32)OperationOrderType.GRNFromInvoice + "," + (int)OperationOrderType.GRNFromTransfer + ")" });
                        if (datTemp.Rows.Count > 0)
                            cboOrderType.Text = datTemp.Rows[0]["OrderType"].ToString();
                    }

                    break;
                case 4:
                    btnDocuments.Visible = false;
                    cboOrderType.SelectedValue = (Int32)OperationOrderType.PInvoiceDirect;
                    if (cboOrderType.SelectedValue == null)
                    {
                        DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "CommonOrderTypeReference", "OrderTypeID=" + (Int32)OperationOrderType.PInvoiceDirect });
                        if (datTemp.Rows.Count > 0)
                            cboOrderType.Text = datTemp.Rows[0]["OrderType"].ToString();
                    }
                    ChkTGNR.Enabled = true;
                    break;
            }
            cboOrderNo.SelectedIndex = -1;
            dtTempDetails = new DataTable();
            cboCompany.Focus();
            MblnIsFromClear = false;

            tcPurchase.SelectedTab = tpItemDetails;
        }

        private void GenerateOrderNo()
        {
            try
            {
                switch (MintFromForm)
                {

                    case 1:
                        txtPurchaseNo.Text = GetPurchasePrefix() + (MobjClsBLLPurchase.GetLastPurchaseNo(1, true, cboCompany.SelectedValue.ToInt32()) + 1).ToString();
                        lblPurchaseCount.Text = "Total Quotations " + MobjClsBLLPurchase.GetLastPurchaseNo(1, false, cboCompany.SelectedValue.ToInt32());
                        break;
                    case 2:
                        txtPurchaseNo.Text = GetPurchasePrefix() + (MobjClsBLLPurchase.GetLastPurchaseNo(2, true, cboCompany.SelectedValue.ToInt32()) + 1).ToString();
                        lblPurchaseCount.Text = "Total Orders " + MobjClsBLLPurchase.GetLastPurchaseNo(2, false, cboCompany.SelectedValue.ToInt32());
                        break;
                    case 3:
                        if (PintOperationTypeID == 0)
                        {
                            txtPurchaseNo.Text = GetPurchasePrefix() + (MobjClsBLLPurchase.GetLastPurchaseNo(3, true, cboCompany.SelectedValue.ToInt32()) + 1).ToString();
                            lblPurchaseCount.Text = "Total GRNs " + MobjClsBLLPurchase.GetLastPurchaseNo(3, false, cboCompany.SelectedValue.ToInt32());
                        }
                        else
                        {
                            txtPurchaseNo.Text = GetPurchasePrefix() + (MobjClsBLLPurchase.GetLastPurchaseNo(5, true, cboCompany.SelectedValue.ToInt32()) + 1).ToString();
                            lblPurchaseCount.Text = "Total Material Returns " + MobjClsBLLPurchase.GetLastPurchaseNo(5, false, cboCompany.SelectedValue.ToInt32());
                        }
                        break;
                    case 4:
                        txtPurchaseNo.Text = GetPurchasePrefix() + (MobjClsBLLPurchase.GetLastPurchaseNo(4, true, cboCompany.SelectedValue.ToInt32()) + 1).ToString();
                        lblPurchaseCount.Text = "Total Invoices " + MobjClsBLLPurchase.GetLastPurchaseNo(4, false, cboCompany.SelectedValue.ToInt32());
                        break;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in GenerateOrderNo() " + ex.Message);
                MObjLogs.WriteLog("Error in GenerateOrderNo() " + ex.Message, 2);
            }
        }

        private void CalculateTotalAmt()
        {
            try
            {
                if (MintFromForm == 1 || MintFromForm == 2 || MintFromForm == 4)
                {
                    double dGrandAmount = 0;
                    if (dgvPurchase.RowCount > 0)
                    {
                        int iGridRowCount = dgvPurchase.RowCount;
                        iGridRowCount = iGridRowCount - 1;

                        for (int i = 0; i < iGridRowCount; i++)
                        {
                            double dGridNetAmount = 0;
                            if (dgvPurchase.Rows[i].Cells["ItemID"].Value.ToInt32() != 0)
                            {
                                int iItemID = dgvPurchase.Rows[i].Cells["ItemID"].Value.ToInt32();
                                //int iDiscountID = dgvPurchase.Rows[i].Cells["Discount"].Tag.ToInt32();
                                double dQty = 0;
                                double dRate = 0, decDiscountAmount = 0;

                                dQty = dgvPurchase.Rows[i].Cells["Quantity"].Value.ToDouble();
                                double dDirectRate = dgvPurchase.Rows[i].Cells["Rate"].Value.ToDouble();
                                double dGridGrandAmount = dQty * dDirectRate;
                                //if (dQty > 0 && dDirectRate > 0 && iDiscountID != (int)PredefinedDiscounts.DefaultPurchaseDiscount)
                                //{
                                //    bool blnIsDiscountForAmount = MobjClsBLLPurchase.IsDiscountForAmount(iDiscountID);
                                //    if (!blnIsDiscountForAmount)
                                //    {
                                //        if (iDiscountID > 0 && dDirectRate > 0 && iItemID > 0)
                                //            dRate = MobjClsBLLPurchase.GetItemDiscountAmount(3, iItemID, iDiscountID, dDirectRate);
                                //        else
                                //            dRate = dDirectRate;
                                //        decDiscountAmount = dDirectRate - dRate;
                                //        decDiscountAmount = decDiscountAmount * dQty;
                                //        dGridNetAmount = dRate * dQty;
                                //        //    dGridNetAmount = dDirectRate * dQty;
                                //    }
                                //    else
                                //    {
                                //        dGridNetAmount = dDirectRate * dQty;
                                //        if (iDiscountID > 0 && dDirectRate > 0 && iItemID > 0)
                                //            dGridNetAmount = MobjClsBLLPurchase.GetItemDiscountAmount(3, iItemID, iDiscountID, dGridNetAmount);
                                //        decDiscountAmount = (dDirectRate * dQty) - dGridNetAmount;
                                //    }
                                //}
                                //else
                                //{
                                decDiscountAmount = dgvPurchase.Rows[i].Cells[DiscountAmount.Index].Value.ToDouble();
                                if (Convert.ToString(dgvPurchase.Rows[i].Cells["Discount"].Value) == "%")
                                {
                                    decDiscountAmount = dGridGrandAmount * (decDiscountAmount / 100);
                                }
                                    
                                    dGridNetAmount = dGridGrandAmount - decDiscountAmount;
                                //}

                                //if (dGridNetAmount >= 0)
                                //   {
                                dgvPurchase.Rows[i].Cells["GrandAmount"].Value = dGridGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                                dgvPurchase.Rows[i].Cells["NetAmount"].Value = dGridNetAmount.ToString("F" + MintExchangeCurrencyScale);
                                if (dgvPurchase.Rows[i].Cells["NetAmount"].Value.ToDecimal() > 0)
                                    dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = (dgvPurchase.Rows[i].Cells["NetAmount"].Value.ToDecimal() / dgvPurchase.Rows[i].Cells["Quantity"].Value.ToDecimal()).ToString("F" + MintExchangeCurrencyScale);
                                else
                                    dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = 0;
                                if (cboDiscount.SelectedValue.ToInt32() != 0 && cboDiscount.SelectedValue.ToInt32() != (int)PredefinedDiscounts.DefaultPurchaseDiscount)
                                    dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = Convert.ToDecimal(MobjClsBLLPurchase.GetItemDiscountAmount(4, 0, cboDiscount.SelectedValue.ToInt32(), dgvPurchase.Rows[i].Cells["PurchaseRate"].Value.ToDouble())).ToString("F" + MintExchangeCurrencyScale);

                                dgvPurchase.Rows[i].Cells["DiscountAmt"].Value = decDiscountAmount.ToString("F" + MintExchangeCurrencyScale);
                                dGrandAmount += dGridNetAmount;
                                //   }
                                //   else
                                //  {
                                //    dgvPurchase.Rows[i].Cells["GrandAmount"].Value = dGridGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                                //    dgvPurchase.Rows[i].Cells["NetAmount"].Value = dGridNetAmount.ToString("F" + MintExchangeCurrencyScale);
                                //    dgvPurchase.Rows[i].Cells["DiscountAmount"].Value = decDiscountAmount.ToString("F" + MintExchangeCurrencyScale);
                                //    TxtBSubtotal.Text = dGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                                //    TxtBTotalAmount.Text = dGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                                //    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 947, out MmessageIcon);
                                //    MessageBox.Show(MsMessageCommon.Replace("#", ""));
                                //}
                            }
                            else
                            {
                                dGrandAmount += 0;
                                dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = dgvPurchase.Rows[i].Cells["Rate"].Value;
                                dgvPurchase.Rows[i].Cells["GrandAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                                dgvPurchase.Rows[i].Cells["NetAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                                dgvPurchase.Rows[i].Cells["DiscountAmt"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                            }
                        }
                    }

                    txtSubTotal.Text = dGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                    CalculateNetTotal();
                    for (int i = 0; i < dgvPurchase.Rows.Count; i++)
                    {
                        if (dgvPurchase.Rows[i].Cells["ItemID"].Value != null && dgvPurchase.Rows[i].Cells["Quantity"].Value.ToDecimal() > 0)
                        {
                            dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = (dgvPurchase.Rows[i].Cells["NetAmount"].Value.ToDecimal() / dgvPurchase.Rows[i].Cells["Quantity"].Value.ToDecimal()).ToString("F" + MintExchangeCurrencyScale);
                            if (cboDiscount.SelectedValue.ToInt32() != 0 && cboDiscount.SelectedValue.ToInt32() != (int)PredefinedDiscounts.DefaultPurchaseDiscount)
                                dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = Convert.ToDecimal(MobjClsBLLPurchase.GetItemDiscountAmount(4, 0, cboDiscount.SelectedValue.ToInt32(), dgvPurchase.Rows[i].Cells["PurchaseRate"].Value.ToDouble())).ToString("F" + MintExchangeCurrencyScale);
                            else if (cboDiscount.SelectedValue.ToInt32() == (int)PredefinedDiscounts.DefaultPurchaseDiscount && txtSubTotal.Text.ToDecimal()>0)
                            {
                                decimal decDiscountPercent = (txtDiscount.Text.ToDecimal() / txtSubTotal.Text.ToDecimal()) * 100;
                                decimal decItemNetAmount = dgvPurchase.Rows[i].Cells["NetAmount"].Value.ToDecimal() - (dgvPurchase.Rows[i].Cells["NetAmount"].Value.ToDecimal() * (decDiscountPercent / 100));
                                dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = (decItemNetAmount / dgvPurchase.Rows[i].Cells["Quantity"].Value.ToDecimal()).ToString("F" + MintExchangeCurrencyScale);
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CalculateTotalAmt() " + ex.Message);
                MObjLogs.WriteLog("Error in CalculateTotalAmt() " + ex.Message, 2);
            }
        }

        private bool IsValidBatch(string strBatchNo, int intItemID)
        {
            DataTable dtBatchDetails = MobjClsBLLPurchase.FillCombos(new string[] { "BatchID,ItemID", "InvBatchDetails", "BatchNo = '" + strBatchNo + "' And ItemID <> " + intItemID });
            if (dtBatchDetails.Rows.Count > 0)
            {
                //if (Convert.ToInt64(dtBatchDetails.Rows[0]["ItemID"]) != intItemID)
                return false;
            }
            return true;
        }

        private bool Purchasevalidation()
        {
            try
            {
                ErrPurchase.Clear();
                lblPurchasestatus.Text = "";

                if (cboCompany.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 14, out MmessageIcon);
                    ErrPurchase.SetError(cboCompany, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    cboCompany.Focus();
                    return false;
                }

                if (string.IsNullOrEmpty(txtPurchaseNo.Text.Trim()))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 942, out MmessageIcon);
                    ErrPurchase.SetError(txtPurchaseNo, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    txtPurchaseNo.Focus();
                    return false;
                }

                //if (!txtPurchaseNo.ReadOnly && MobjClsBLLPurchase.IsPurchaseNoExists(txtPurchaseNo.Text, MintFromForm, cboCompany.SelectedValue.ToInt32()))
                //{
                //    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 943, out MmessageIcon);
                //    ErrPurchase.SetError(txtPurchaseNo, MstrMessageCommon.Replace("#", "").Trim());
                //    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmrPurchase.Enabled = true;
                //    tcPurchase.SelectedTab = tpItemDetails;
                //    txtPurchaseNo.Focus();
                //    return false;
                //}

                if (MintFromForm != 3 && cboCurrency.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 939, out MmessageIcon);
                    ErrPurchase.SetError(cboCurrency, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    cboCurrency.Focus();
                    return false;
                }
                if (!(MintFromForm == 3 && (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromTransfer)) && cboSupplier.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 908, out MmessageIcon);
                    ErrPurchase.SetError(cboSupplier, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    cboSupplier.Focus();
                    return false;
                }

                //if (!(MintFromForm == 3 && cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromTransfer) && txtSupplierAddress.Text == "")
                //{
                //    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 909, out MmessageIcon);
                //    ErrPurchase.SetError(txtSupplierAddress, MstrMessageCommon.Replace("#", "").Trim());
                //    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmrPurchase.Enabled = true;
                //    tcPurchase.SelectedTab = tpItemDetails;
                //    txtSupplierAddress.Focus();
                //    return false;
                //}


                switch (MintFromForm)
                {
                    case 2:
                        if (cboOrderType.SelectedIndex == -1)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 912, out MmessageIcon);
                            ErrPurchase.SetError(cboOrderType, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            cboOrderType.Focus();
                            return false;
                        }

                        //if (!MblnAddStatus && MobjClsBLLPurchase.FillCombos(new string[] { "ReceiptAndPaymentID", "AccReceiptAndPayment", "OperationTypeID = " + (int)OperationType.PurchaseOrder + " And ReferenceID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID }).Rows.Count > 0)
                        //{
                        if (!MblnAddStatus && MobjClsBLLPurchase.FillCombos(new string[] { "PM.ReceiptAndPaymentID", "AccReceiptAndPaymentMaster As PM Inner Join AccReceiptAndPaymentDetails PD ON PM.ReceiptAndPaymentID=PD.ReceiptAndPaymentID", "PM.OperationTypeID = " + (int)OperationType.PurchaseOrder + " And PD.ReferenceID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID }).Rows.Count > 0)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 965, out MmessageIcon);
                            ErrPurchase.SetError(cboOrderType, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            cboOrderType.Focus();
                            return false;
                        }
                        if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.POrderFromGRN)
                        {
                            if (MobjClsBLLPurchase.IsPurchaseInvoiceExists(strCondition))
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2280, out MmessageIcon);
                                MstrMessageCommon = MstrMessageCommon.Replace("*", "Updated");
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                tcPurchase.SelectedTab = tpItemDetails;
                                return false;
                            }
                        }

                        //if (MblnAddStatus && Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.POrderFromIndent)
                        //{
                        //    if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseIndentID", "STPurchaseIndentMaster", "PurchaseIndentID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID = " + (Int32)OperationStatusType.PIndentOpened }).Rows.Count == 0)
                        //    {
                        //        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 960, out MmessageIcon);
                        //        MstrMessageCommon = MstrMessageCommon.Replace("*", "Purchase Indent");
                        //        ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                        //        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        //        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        //        TmrPurchase.Enabled = true;
                        //        cboOrderNo.Focus();
                        //        return false;
                        //    }
                        //}
                        if (MblnAddStatus && Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.POrderFromQuotation)
                        {
                            if (ClsCommonSettings.PQApproval)
                            {
                                if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseQuotationID", "InvPurchaseQuotationMaster", "PurchaseQuotationID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.PQuotationApproved + ")" }).Rows.Count == 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 961, out MmessageIcon);
                                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation");
                                    ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrPurchase.Enabled = true;
                                    cboOrderNo.Focus();
                                    return false;
                                }
                            }
                            else
                            {
                                if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseQuotationID", "InvPurchaseQuotationMaster", "PurchaseQuotationID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.PQuotationSubmitted + ")" }).Rows.Count == 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 961, out MmessageIcon);
                                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation").Replace("approved", "submitted");
                                    ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrPurchase.Enabled = true;
                                    cboOrderNo.Focus();
                                    return false;
                                }
                            }
                        }
                        if (dtpDate.Value.Date > ClsCommonSettings.GetServerDate())
                        {
                            // due date limit
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9006, out MmessageIcon);
                            MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");

                            ErrPurchase.SetError(dtpDate, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            dtpDate.Focus();
                            return false;
                        }
                        if (dtpDueDate.Value.Date < dtpDate.Value.Date)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9009, out MmessageIcon);
                            MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");
                            ErrPurchase.SetError(dtpDueDate, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            dtpDueDate.Focus();
                            return false;
                        }
                        if (ClsCommonSettings.PODueDateLimit.ToInt32() != 0 && (dtpDueDate.Value > dtpDate.Value.AddDays(ClsCommonSettings.PODueDateLimit.ToDouble())))
                        {
                            // due date limit
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9005, out MmessageIcon).Replace("*", ClsCommonSettings.PODueDateLimit);
                            ErrPurchase.SetError(dtpDueDate, MstrMessageCommon.Replace("#", "").Trim());
                            //mObjNotification.CallErrorMessage(MaMessageArr, 17, ClsCommonSettings.MessageCaption);
                            MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            dtpDueDate.Focus();
                            return false;
                        }
                        if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.POrderFromQuotation && cboOrderNo.SelectedValue.ToInt32() != 0)
                        {
                            DataTable datQuotationDetails = MobjClsBLLPurchase.FillCombos(new string[] { "QuotationDate,DueDate", "InvPurchaseQuotationMaster", "PurchaseQuotationID = " + cboOrderNo.SelectedValue.ToInt64() });
                            if (dtpDate.Value.Date < datQuotationDetails.Rows[0]["QuotationDate"].ToDateTime().Date)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9008, out MmessageIcon).Replace("*", "Order");
                                MstrMessageCommon = MstrMessageCommon.Replace("@", "Quotation");
                                ErrPurchase.SetError(dtpDate, MstrMessageCommon.Replace("#", "").Trim());
                                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                dtpDate.Focus();
                                return false;
                            }
                            else if (!MblnIsFromCancellation && dtpDate.Value.Date > datQuotationDetails.Rows[0]["DueDate"].ToDateTime().Date)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9007, out MmessageIcon).Replace("*", "Order");
                                MstrMessageCommon = MstrMessageCommon.Replace("@", "Quotation");
                                ErrPurchase.SetError(dtpDate, MstrMessageCommon.Replace("#", "").Trim());
                                //mObjNotification.CallErrorMessage(MaMessageArr, 17, ClsCommonSettings.MessageCaption);
                                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                dtpDate.Focus();
                            }
                        }
                        //if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.POrderDirect)
                        //{
                        //    if (cboDiscount.SelectedIndex == 1 && txtDiscount.Text.ToDecimal() == 0)
                        //    {
                        //        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2287, out MmessageIcon);
                        //        MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        //        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        //        TmrPurchase.Enabled = true;
                        //        tcPurchase.SelectedTab = tpItemDetails;
                        //        cboDiscount.Focus();
                        //        return false;
                        //    }
                        //}
                        if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.POrderFromGRN)
                        {
                            bool result=new clsBLLPurchaseInvoice().ISWarehouseActive(cboGRNWarehouse.SelectedValue.ToInt32());
                            if (result)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2290, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                tcPurchase.SelectedTab = tpItemDetails;
                                cboWarehouse.Focus();
                                return false;
                            }
                        }


                        break;
                    case 3:

                        if (cboOrderType.SelectedIndex == -1)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 912, out MmessageIcon);
                            ErrPurchase.SetError(cboOrderType, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            cboOrderType.Focus();
                            return false;
                        }
                        if (MblnAddStatus)
                        {
                            if (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.GRNFromInvoice)
                            {
                                if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseInvoiceID", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID = " + (Int32)OperationStatusType.PInvoiceOpened }).Rows.Count == 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 960, out MmessageIcon);
                                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Invoice");
                                    ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrPurchase.Enabled = true;
                                    tcPurchase.SelectedTab = tpItemDetails;
                                    cboOrderNo.Focus();
                                    return false;
                                }
                            }
                            //if (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.GRNFromOrder)
                            //{
                            //    if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseOrderID", "InvPurchaseOrderMaster", "PurchaseOrderID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID = " + (Int32)OperationStatusType.PInvoiceOpened }).Rows.Count == 0)
                            //    {
                            //        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 960, out MmessageIcon);
                            //        MstrMessageCommon = MstrMessageCommon.Replace("*", "Invoice");
                            //        ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                            //        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            //        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            //        TmrPurchase.Enabled = true;
                            //        tcPurchase.SelectedTab = tpItemDetails;
                            //        cboOrderNo.Focus();
                            //        return false;
                            //    }
                            //}
                            //else if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromTransfer)
                            //{
                            //    if (MobjClsBLLPurchase.FillCombos(new string[] { "StockTransferID,StockTransferNo", "InvStockTransferMaster", "ReferenceID In (Select WarehouseID From InvWarehouse Where CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + ") and StatusID = " + Convert.ToInt32(OperationStatusType.STIssued) + " And OrderTypeID In (" + (int)OperationOrderType.STWareHouseToWareHouseTransfer + "," + (int)OperationOrderType.STWareHouseToWareHouseTransferDemo + ") And StockTransferID = " + cboOrderNo.SelectedValue.ToInt64() }).Rows.Count == 0)
                            //    {
                            //        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 982, out MmessageIcon);
                            //        MstrMessageCommon = MstrMessageCommon.Replace("*", "Invoice");
                            //        ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                            //        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            //        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            //        TmrPurchase.Enabled = true;
                            //        tcPurchase.SelectedTab = tpItemDetails;
                            //        cboOrderNo.Focus();
                            //        return false;
                            //    }
                            //}
                        }
                        if (cboWarehouse.SelectedIndex == -1)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 922, out MmessageIcon);
                            ErrPurchase.SetError(cboWarehouse, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            cboWarehouse.Focus();
                            return false;
                        }
                        if (dtpDate.Value.Date > ClsCommonSettings.GetServerDate())
                        {
                            // due date limit
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9006, out MmessageIcon);
                            MstrMessageCommon = MstrMessageCommon.Replace("*", "GRN");
                            ErrPurchase.SetError(dtpDate, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            dtpDate.Focus();
                            return false;
                        }
                        if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromInvoice && cboOrderNo.SelectedValue.ToInt32() != 0)
                        {
                            DataTable datInvoiceDetails = MobjClsBLLPurchase.FillCombos(new string[] { "InvoiceDate", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + cboOrderNo.SelectedValue.ToInt64() });
                            if (dtpDate.Value.Date < datInvoiceDetails.Rows[0]["InvoiceDate"].ToDateTime().Date)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9008, out MmessageIcon).Replace("*", "GRN");
                                MstrMessageCommon = MstrMessageCommon.Replace("@", "Invoice");
                                ErrPurchase.SetError(dtpDate, MstrMessageCommon.Replace("#", "").Trim());
                                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                tcPurchase.SelectedTab = tpItemDetails;
                                dtpDate.Focus();
                                return false;
                            }
                        }
                        //else if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromTransfer)
                        //{
                        //    DataTable datTransferDetails = MobjClsBLLPurchase.FillCombos(new string[] { "OrderDate", "InvStockTransferMaster", "StockTransferID = " + cboOrderNo.SelectedValue.ToInt64() });
                        //    if (dtpDate.Value.Date < datTransferDetails.Rows[0]["OrderDate"].ToDateTime().Date)
                        //    {
                        //        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9008, out MmessageIcon).Replace("*", "GRN");
                        //        MstrMessageCommon = MstrMessageCommon.Replace("@", "Transfer");
                        //        ErrPurchase.SetError(dtpDate, MstrMessageCommon.Replace("#", "").Trim());
                        //        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        //        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        //        TmrPurchase.Enabled = true;
                        //        tcPurchase.SelectedTab = tpItemDetails;
                        //        dtpDate.Focus();
                        //        return false;
                        //    }
                        //}
                        if (!MblnAddStatus && IsStockAdjustmentDone())
                        {
                            return false;
                        }

                        if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromInvoice)
                        {
                            if (!MblnAddStatus && MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseReturnID", "InvPurchaseReturnMaster", "PurchaseInvoiceID = " + cboOrderNo.SelectedValue.ToInt64() }).Rows.Count > 0)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 989, out MmessageIcon).Replace("*", "GRN");

                                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                tcPurchase.SelectedTab = tpItemDetails;
                                return false;
                            }
                        }

                        break;

                    case 4:
                        if (!MblnAddStatus && cboPaymentTerms.SelectedValue.ToInt32() == (int)PaymentTerms.Credit && MobjClsBLLPurchase.FillCombos(new string[] { "PM.ReceiptAndPaymentID", "AccReceiptAndPaymentMaster PM Inner Join AccReceiptAndPaymentDetails PD ON PM.ReceiptAndPaymentID=PD.ReceiptAndPaymentID", "PM.OperationTypeID = " + (int)OperationType.PurchaseInvoice + " And PD.ReferenceID =" + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID }).Rows.Count > 0)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 965, out MmessageIcon);
                            ErrPurchase.SetError(cboOrderType, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            cboOrderType.Focus();
                            return false;
                        }
                        if (cboOrderType.SelectedIndex == -1)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 912, out MmessageIcon);
                            ErrPurchase.SetError(cboOrderType, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            cboOrderType.Focus();
                            return false;
                        }
                        if (MblnAddStatus && Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.PInvoiceFromOrder)
                        {
                            if (ClsCommonSettings.POApproval)
                            {
                                if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseOrderID", "InvPurchaseOrderMaster", "PurchaseOrderID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.POrderApproved + ")" }).Rows.Count == 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 961, out MmessageIcon);
                                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");
                                    ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrPurchase.Enabled = true;
                                    tcPurchase.SelectedTab = tpItemDetails;
                                    cboOrderNo.Focus();
                                    return false;
                                }
                            }
                            else
                            {
                                if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseOrderID", "InvPurchaseOrderMaster", "PurchaseOrderID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.POrderSubmitted + ")" }).Rows.Count == 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 961, out MmessageIcon);
                                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Order").Replace("approved", "submitted");
                                    ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrPurchase.Enabled = true;
                                    tcPurchase.SelectedTab = tpItemDetails;
                                    cboOrderNo.Focus();
                                    return false;
                                }
                            }
                        }
                        if (cboWarehouse.SelectedIndex == -1)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 922, out MmessageIcon);
                            ErrPurchase.SetError(cboWarehouse, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            cboWarehouse.Focus();
                            return false;
                        }
                        if (cboOrderType.SelectedIndex == 1)
                        {
                            //string strSupDocuments = MobjClsBLLPurchase.IsSupplierDocumentsValid((int)OperationType.Suppliers, cboSupplier.SelectedValue.ToInt32(), cboCompany.SelectedValue.ToInt32());
                            //if (!string.IsNullOrEmpty(strSupDocuments))
                            //{
                            //    MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 977, out MmessageIcon).Replace("***", strSupDocuments).Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //    return false;
                            //}
                            //string strDocumentExpired = MobjClsBLLPurchase.IsMandatoryDocumentExpired((int)OperationType.Suppliers, cboSupplier.SelectedValue.ToInt32(), cboCompany.SelectedValue.ToInt32());
                            //if (!string.IsNullOrEmpty(strDocumentExpired))
                            //{
                            //    MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 978, out MmessageIcon).Replace("***", strDocumentExpired).Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //    return false;
                            //}
                        }
                        if (string.IsNullOrEmpty(txtSupplierBillNo.Text.Trim()))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 926, out MmessageIcon);
                            ErrPurchase.SetError(txtSupplierBillNo, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            txtSupplierBillNo.Focus();
                            return false;
                        }

                        if (MobjClsBLLPurchase.IsPurchaseBillNoExists(txtSupplierBillNo.Text.Trim(), MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 958, out MmessageIcon);
                            ErrPurchase.SetError(txtSupplierBillNo, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            txtSupplierBillNo.Focus();
                            return false;
                        }
                        if (dtpDate.Value.Date > ClsCommonSettings.GetServerDate())
                        {
                            // due date limit
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9006, out MmessageIcon);
                            MstrMessageCommon = MstrMessageCommon.Replace("*", "Invoice");
                            ErrPurchase.SetError(dtpDate, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            dtpDate.Focus();
                            return false;
                        }
                        if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.PInvoiceFromOrder && cboOrderNo.SelectedValue.ToInt32() != 0)
                        {
                            DataTable datOrderDetails = MobjClsBLLPurchase.FillCombos(new string[] { "OrderDate,DueDate", "InvPurchaseOrderMaster", "PurchaseOrderID = " + cboOrderNo.SelectedValue.ToInt64() });
                            if (dtpDate.Value.Date < datOrderDetails.Rows[0]["OrderDate"].ToDateTime().Date)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9008, out MmessageIcon).Replace("*", "Invoice");
                                MstrMessageCommon = MstrMessageCommon.Replace("@", "Order");
                                ErrPurchase.SetError(dtpDate, MstrMessageCommon.Replace("#", "").Trim());
                                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                tcPurchase.SelectedTab = tpItemDetails;
                                dtpDate.Focus();
                                return false;
                            }
                            else if (!MblnIsFromCancellation && dtpDate.Value.Date > datOrderDetails.Rows[0]["DueDate"].ToDateTime().Date)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9007, out MmessageIcon).Replace("*", "Invoice");
                                MstrMessageCommon = MstrMessageCommon.Replace("@", "Order");
                                ErrPurchase.SetError(dtpDate, MstrMessageCommon.Replace("#", "").Trim());
                                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                tcPurchase.SelectedTab = tpItemDetails;
                                dtpDate.Focus();
                            }
                        }
                        MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                        if (cboPurchaseAccount.SelectedIndex == -1)
                        {
                            //MsMessageCommon = "Please Select Purchase Account";
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2222, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            cboPurchaseAccount.Focus();
                            return false;

                            //if (cboPaymentTerms.SelectedValue.ToInt32() == (int)PaymentTerms.Credit)
                            //{
                            //    if (!MobjClsBLLPurchase.GetCompanyAccount(Convert.ToInt32(TransactionTypes.CreditPurchase)))
                            //    {
                            //        //MsMessageCommon = "Please create a Purchase account for the selected company.";
                            //        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 951, out MmessageIcon);
                            //        MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            //        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            //        TmrPurchase.Enabled = true;
                            //        return false;
                            //    }
                            //}
                            //else
                            //{
                            //    if (!MobjClsBLLPurchase.GetCompanyAccount(Convert.ToInt32(TransactionTypes.CashPurchase)))
                            //    {
                            //        //MsMessageCommon = "Please create a Purchase account for the selected company.";
                            //        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 993, out MmessageIcon);
                            //        MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            //        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            //        TmrPurchase.Enabled = true;
                            //        return false;
                            //    }
                            //}

                        }
                        clsBLLJournalVoucher MobjclsBLLJournalVoucher = new clsBLLJournalVoucher();
                        if (((MobjclsBLLJournalVoucher.getAccountBalance((int)Accounts.CashAccount, cboCompany.SelectedValue.ToInt32())) - Convert.ToDecimal(txtExchangeCurrencyRate.Text)) < 0 && !MblnIsFromCancellation)
                        {
                            //MsMessageCommon = "There is no Enough cash balance";

                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2223, out MmessageIcon);
                            if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                                return false;

                        }
                        if (!MblnAddStatus && IsStockAdjustmentDone())
                        {
                            return false;
                        }
                        if (!MblnAddStatus && MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseReturnID", "InvPurchaseReturnMaster", "PurchaseInvoiceID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID }).Rows.Count > 0)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 989, out MmessageIcon).Replace("*", "Invoice");

                            MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            return false;
                        }
                        break;
                }

                if (MintFromForm != 3 && cboPaymentTerms.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 938, out MmessageIcon);
                    ErrPurchase.SetError(cboPaymentTerms, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    tcPurchase.SelectedTab = tpItemDetails;
                    cboPaymentTerms.Focus();
                    return false;
                }
                //if ((MintFromForm == 3 && cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromTransfer && dgvPurchase.RowCount == 0) && dgvPurchase.RowCount == 1)
                //{
                //    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 913, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmrPurchase.Enabled = true;
                //    tcPurchase.SelectedTab = tpItemDetails;
                //    dgvPurchase.Focus();
                //    return false;
                //}

                if (MintFromForm != 3)
                {


                    if (dgvPurchase.Rows.Count == 1)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 901, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        dgvPurchase.Focus();
                        return false;
                    }
                }
                else if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromInvoice)
                {
                    if (dgvPurchase.Rows.Count == 1)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 901, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        dgvPurchase.Focus();
                        return false;
                    }
                }

                if (ValidatePurchaseGrid() == false) return false;

                if (dgvPurchase.Rows.Count > 0)
                {
                    int iTempID = 0;
                    iTempID = CheckDuplicationInGrid();
                    if (iTempID != -1)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 902, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        dgvPurchase.CurrentCell = dgvPurchase["ItemCode", iTempID];
                        dgvPurchase.Focus();
                        return false;
                    }

                }

                if (MintFromForm == 3 || (MintFromForm == 4 && !ChkTGNR.Checked))
                {
                    if (ClsCommonSettings.PblnIsLocationWise)
                    {
                        if (!ValidateLocationGrid()) return false;

                        if (!CheckLocationDuplication()) return false;

                        if (!MblnAddStatus)
                            if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PInvoiceCancelled || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.GRNCancelled)
                            {
                                if (!ValidateLocationQuantity(true)) return false;
                            }
                            else if (!MblnIsFromCancellation)
                                if (!ValidateLocationQuantity(false)) return false;
                    }
                }

                if (MintFromForm != 3 && txtTotalAmount.Text.ToDecimal() <= 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 970, out MmessageIcon);
                    ErrPurchase.SetError(cboDiscount, MstrMessageCommon.Replace("#", "").Trim());
                    //mObjNotification.CallErrorMessage(MaMessageArr, 909, ClsCommonSettings.MessageCaption);
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    cboDiscount.Focus();
                    return false;
                }

                //------------------------------------------------ Middle Panel---------------------------------------------------------------------------




                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in PurchaseOrderValidation() " + ex.Message);
                MObjLogs.WriteLog("Error in PurchaseOrderValidation() " + ex.Message, 2);
                return false;
            }
        }

        private bool PurchaseQuotationvalidation()
        {
            try
            {
                ErrPurchase.Clear();
                lblPurchasestatus.Text = "";
                if (cboCompany.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 14, out MmessageIcon);
                    ErrPurchase.SetError(cboCompany, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    cboCompany.Focus();
                    return false;
                }
                if (string.IsNullOrEmpty(txtPurchaseNo.Text.Trim()))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 942, out MmessageIcon);
                    ErrPurchase.SetError(txtPurchaseNo, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    txtPurchaseNo.Focus();
                    return false;
                }
                if (!txtPurchaseNo.ReadOnly && MobjClsBLLPurchase.IsPurchaseNoExists(txtPurchaseNo.Text, 1, cboCompany.SelectedValue.ToInt32()))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 943, out MmessageIcon);
                    ErrPurchase.SetError(txtPurchaseNo, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    txtPurchaseNo.Focus();
                    return false;
                }
                if (cboOrderType.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 912, out MmessageIcon);
                    ErrPurchase.SetError(cboOrderType, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    cboOrderType.Focus();
                    return false;
                }

                //if (MblnAddStatus && Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.PQuotationFromRFQ)
                //{
                //    if (MobjClsBLLPurchase.FillCombos(new string[] { "RFQID", "InvRFQMaster", "RFQID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID In( " + (Int32)RFQStatus.Approved + "," + (int)RFQStatus.Submitted + ")" }).Rows.Count == 0)
                //    {
                //        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 961, out MmessageIcon);
                //        MstrMessageCommon = MstrMessageCommon.Replace("*", "RFQ");
                //        ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                //        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //        TmrPurchase.Enabled = true;
                //        cboOrderNo.Focus();
                //        return false;
                //    }
                //}
                if (cboCurrency.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 939, out MmessageIcon);
                    ErrPurchase.SetError(cboCurrency, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    cboCurrency.Focus();
                    return false;
                }
                if (cboSupplier.SelectedIndex == -1)
                {
                    //MsMessageCommon = "Please select Department";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 908, out MmessageIcon);
                    ErrPurchase.SetError(cboSupplier, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    cboSupplier.Focus();
                    return false;
                }
                if (dtpDate.Value.Date > ClsCommonSettings.GetServerDate())
                {
                    // due date limit
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9006, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation");

                    ErrPurchase.SetError(dtpDate, MstrMessageCommon.Replace("#", "").Trim());
                    //mObjNotification.CallErrorMessage(MaMessageArr, 17, ClsCommonSettings.MessageCaption);
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    dtpDate.Focus();
                    return false;
                }
                if (dtpDueDate.Value.Date < dtpDate.Value.Date)
                {

                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9009, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation");

                    ErrPurchase.SetError(dtpDueDate, MstrMessageCommon.Replace("#", "").Trim());
                    //mObjNotification.CallErrorMessage(MaMessageArr, 17, ClsCommonSettings.MessageCaption);
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    dtpDueDate.Focus();
                    return false;
                }
                if (ClsCommonSettings.PQDueDateLimit.ToInt32() != 0 && (dtpDueDate.Value > dtpDate.Value.AddDays(ClsCommonSettings.PQDueDateLimit.ToDouble())))
                {
                    // due date limit
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9005, out MmessageIcon).Replace("*", ClsCommonSettings.PQDueDateLimit);
                    ErrPurchase.SetError(dtpDueDate, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    dtpDueDate.Focus();
                    return false;
                }
                //if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.PQuotationFromRFQ && cboOrderNo.SelectedValue.ToInt32() != 0)
                //{
                //    DataTable datRFQDetails = MobjClsBLLPurchase.FillCombos(new string[] { "QuotationDate,DueDate", "InvRFQMaster", "RFQID = " + cboOrderNo.SelectedValue.ToInt64() });
                //    if (dtpDate.Value.Date < datRFQDetails.Rows[0]["QuotationDate"].ToDateTime().Date)
                //    {
                //        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9008, out MmessageIcon).Replace("*", "Quotation");
                //        MstrMessageCommon = MstrMessageCommon.Replace("@", "RFQ");
                //        ErrPurchase.SetError(dtpDate, MstrMessageCommon.Replace("#", "").Trim());
                //        //mObjNotification.CallErrorMessage(MaMessageArr, 17, ClsCommonSettings.MessageCaption);
                //        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //        TmrPurchase.Enabled = true;
                //        dtpDate.Focus();
                //        return false;
                //    }
                //    else if (!MblnIsFromCancellation && dtpDate.Value.Date > datRFQDetails.Rows[0]["DueDate"].ToDateTime().Date)
                //    {
                //        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9007, out MmessageIcon).Replace("*", "Quotation");
                //        MstrMessageCommon = MstrMessageCommon.Replace("@", "RFQ");
                //        ErrPurchase.SetError(dtpDate, MstrMessageCommon.Replace("#", "").Trim());
                //        //mObjNotification.CallErrorMessage(MaMessageArr, 17, ClsCommonSettings.MessageCaption);
                //        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //        TmrPurchase.Enabled = true;
                //        dtpDate.Focus();
                //    }
                //}
                if (cboPaymentTerms.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 938, out MmessageIcon);
                    ErrPurchase.SetError(cboPaymentTerms, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    cboPaymentTerms.Focus();
                    return false;
                }

                if (dgvPurchase.Rows.Count == 1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 901, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    dgvPurchase.Focus();
                    return false;
                }

                if (ValidatePurchaseGrid() == false) return false;

                if (txtTotalAmount.Text.ToDecimal() <= 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 970, out MmessageIcon);
                    ErrPurchase.SetError(cboDiscount, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    cboDiscount.Focus();
                    return false;
                }

                if (dgvPurchase.Rows.Count > 0 && tpLocationDetails.Visible == false)
                {
                    int iTempID = 0;
                    iTempID = CheckDuplicationInGrid();
                    if (iTempID != -1)
                    {
                        //MsMessageCommon = "Please check duplication of values.";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 902, out MmessageIcon);
                        ErrPurchase.SetError(dgvPurchase, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        //mObjNotification.CallErrorMessage(MaMessageArr, 902, ClsCommonSettings.MessageCaption);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        dgvPurchase.Focus();
                        dgvPurchase.CurrentCell = dgvPurchase["ItemCode", iTempID];
                        return false;
                    }

                }

                //------------------------------------------------ Bottom Panel---------------------------------------------------------------------------
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in PurchaseQuotationValidation() " + ex.Message);
                MObjLogs.WriteLog("Error in PurchaseQuotationVaildation() " + ex.Message, 2);
                return false;
            }
        }

        private int CheckDuplicationInGrid()
        {
            string SearchValuefirst = "";
            int RowIndexTemp = 0;
            if (MintFromForm == 1 || MintFromForm == 2 || (MintFromForm == 4 && ChkTGNR.Checked))
            {
                foreach (DataGridViewRow rowValue in dgvPurchase.Rows)
                {
                    if (rowValue.Cells[ItemID.Index].Value != null)
                    {
                        SearchValuefirst = Convert.ToString(rowValue.Cells[ItemID.Index].Value);
                        RowIndexTemp = rowValue.Index;
                        foreach (DataGridViewRow row in dgvPurchase.Rows)
                        {
                            if (RowIndexTemp != row.Index)
                            {
                                if (row.Cells[ItemID.Index].Value != null)
                                {
                                    if ((Convert.ToString(row.Cells[ItemID.Index].Value).Trim() == Convert.ToString(SearchValuefirst).Trim()))
                                        return row.Index;
                                }
                            }
                        }
                    }
                }
            }
            if (MintFromForm == 3 || (MintFromForm == 4 && !ChkTGNR.Checked))
            {
                foreach (DataGridViewRow rowValue in dgvPurchase.Rows)
                {
                    if (rowValue.Cells["BatchNumber"].Value != null)
                    {
                        SearchValuefirst = Convert.ToString(rowValue.Cells["BatchNumber"].Value);
                        RowIndexTemp = rowValue.Index;
                        foreach (DataGridViewRow row in dgvPurchase.Rows)
                        {
                            if (RowIndexTemp != row.Index)
                            {
                                if (row.Cells["BatchNumber"].Value != null)
                                {
                                    if ((Convert.ToString(row.Cells["BatchNumber"].Value).Trim() == Convert.ToString(SearchValuefirst).Trim()))
                                        return row.Index;
                                }
                            }
                        }
                    }
                }
            }
            return -1;
        }

        private bool ValidatePurchaseGrid()
        {
            try
            {
                int iGridRowCount = dgvPurchase.RowCount;
                DataTable datOldDetails = new DataTable();


                if (cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.GRNFromTransfer)//&& cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.GRNFromMaterialReturn) 
                    iGridRowCount = iGridRowCount - 1;

                for (int i = 0; i < iGridRowCount; i++)
                {
                    int iRowIndex = dgvPurchase.Rows[i].Index;

                    if (Convert.ToString(dgvPurchase.Rows[i].Cells["ItemCode"].Value).Trim() == "")
                    {
                        //MsMessageCommon = "Please enter Item code.";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 903, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        tcPurchase.SelectedTab = tpItemDetails;
                        dgvPurchase.Focus();
                        dgvPurchase.CurrentCell = dgvPurchase["ItemCode", iRowIndex];
                        return false;
                    }
                    if (Convert.ToString(dgvPurchase.Rows[i].Cells["ItemName"].Value).Trim() == "")
                    {
                        //MsMessageCommon = "Please enter Item Name.";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 915, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        tcPurchase.SelectedTab = tpItemDetails;
                        dgvPurchase.Focus();
                        dgvPurchase.CurrentCell = dgvPurchase["ItemName", iRowIndex];
                        return false;
                    }
                    if (Convert.ToString(dgvPurchase.Rows[i].Cells["Quantity"].Value).Trim() == "" || Convert.ToString(dgvPurchase.Rows[i].Cells["Quantity"].Value).Trim() == "." || Convert.ToDecimal(dgvPurchase.Rows[i].Cells["Quantity"].Value) == 0)
                    {
                        //MsMessageCommon = "Please enter Quantity.";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 916, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        tcPurchase.SelectedTab = tpItemDetails;
                        dgvPurchase.Focus();
                        dgvPurchase.CurrentCell = dgvPurchase["Quantity", iRowIndex];
                        return false;
                    }
                    if (MintFromForm == 3 || MintFromForm == 4)
                    {

                        if ((MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (Int32)OperationStatusType.GRNCancelled && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (Int32)OperationStatusType.PInvoiceCancelled) && (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.PInvoiceFromOrder || Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.GRNFromInvoice || Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.GRNFromOrder))
                        {
                            decimal decQtyReceived = 0;
                            decimal decItemQuantity = 0;
                            if (MintFromForm == 3)
                            {
                                decQtyReceived = MobjClsBLLPurchase.GetReceivedQty(Convert.ToInt32(dgvPurchase.Rows[i].Cells["ItemID"].Value), cboOrderNo.SelectedValue.ToInt64(), true);
                                decItemQuantity = Convert.ToDecimal(dgvPurchase.Rows[i].Cells["Quantity"].Value);
                                for (int intICounter = i + 1; intICounter < dgvPurchase.Rows.Count; intICounter++)
                                {
                                    if (dgvPurchase["ItemID", intICounter].Value.ToInt32() == dgvPurchase["ItemID", i].Value.ToInt32())
                                    {
                                        decItemQuantity += dgvPurchase["Quantity", intICounter].Value.ToDecimal();
                                    }
                                }
                            }
                            else
                            {
                                decQtyReceived = MobjClsBLLPurchase.GetReceivedQty(Convert.ToInt32(dgvPurchase.Rows[i].Cells["ItemID"].Value), MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID, false);
                                decItemQuantity = Convert.ToDecimal(dgvPurchase.Rows[i].Cells["Quantity"].Value);
                                for (int intICounter = i + 1; intICounter < dgvPurchase.Rows.Count; intICounter++)
                                {
                                    if (dgvPurchase["ItemID", intICounter].Value.ToInt32() == dgvPurchase["ItemID", i].Value.ToInt32())
                                    {
                                        decItemQuantity += dgvPurchase["Quantity", intICounter].Value.ToDecimal();
                                    }
                                }
                            }
                            if (dgvPurchase.Rows[i].Cells["ReceivedQty"].Value != null && dgvPurchase.Rows[i].Cells["OrderedQty"].Value != null && Convert.ToDecimal(dgvPurchase.Rows[i].Cells["OrderedQty"].Value) > 0)
                            {
                                dtTempDetails.DefaultView.RowFilter = "ItemID = " + Convert.ToInt32(dgvPurchase["ItemID", i].Value);
                                decimal decOldQty = 0;
                                if (!MblnAddStatus && !MblnIsFromCancellation)
                                {
                                    foreach (DataRow dr in dtTempDetails.DefaultView.ToTable().Rows)
                                    {
                                        decOldQty += Convert.ToDecimal(dr["ReceivedQuantity"]);
                                    }
                                }
                                if (decItemQuantity + decQtyReceived - decOldQty > Convert.ToDecimal(dgvPurchase.Rows[i].Cells["OrderedQty"].Value))
                                {
                                    if (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.GRNFromInvoice)
                                    {

                                        //MsMessageCommon = "Please enter Received Quantity.";
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 935, out MmessageIcon);
                                        if (MintFromForm == 3)
                                            MstrMessageCommon = MstrMessageCommon.Replace("ordered", "Invoiced");
                                        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                        TmrPurchase.Enabled = true;
                                        tcPurchase.SelectedTab = tpItemDetails;
                                        dgvPurchase.Focus();
                                        dgvPurchase.CurrentCell = dgvPurchase["Quantity", iRowIndex];
                                        return false;
                                    }
                                    else
                                    {

                                        //MsMessageCommon = "Total received quantity should not exceed Ordered quantity. Add Excess quantity In Extra quantity field.";
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2224, out MmessageIcon);
                                        if (MintFromForm == 3)
                                            MstrMessageCommon = MstrMessageCommon.Replace("ordered", "Invoiced");
                                        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                        TmrPurchase.Enabled = true;
                                        tcPurchase.SelectedTab = tpItemDetails;
                                        dgvPurchase.Focus();
                                        dgvPurchase.CurrentCell = dgvPurchase["Quantity", iRowIndex];
                                        return false;
                                    }
                                }
                            }
                        }
                        if ((MintFromForm == 3) || ((MintFromForm == 4) && !ChkTGNR.Checked))
                        {
                            if (dgvPurchase.Rows[i].Cells["Batchnumber"].Value == null || string.IsNullOrEmpty(dgvPurchase.Rows[i].Cells["Batchnumber"].Value.ToString()))
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 949, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                tcPurchase.SelectedTab = tpItemDetails;
                                dgvPurchase.Focus();
                                dgvPurchase.CurrentCell = dgvPurchase["Batchnumber", iRowIndex];
                                return false;
                            }
                            if (!IsValidBatch(dgvPurchase.Rows[i].Cells["Batchnumber"].Value.ToString(), dgvPurchase.Rows[i].Cells["ItemID"].Value.ToInt32()))
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 979, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                tcPurchase.SelectedTab = tpItemDetails;
                                dgvPurchase.Focus();
                                dgvPurchase.CurrentCell = dgvPurchase["Batchnumber", iRowIndex];
                                return false;
                            }
                            if (((MintFromForm == 3 && cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromInvoice) || MintFromForm == 4) && Convert.ToBoolean(MobjClsBLLPurchase.FillCombos(new string[] { "ExpiryDateMandatory", "InvItemMaster", "ItemID = " + dgvPurchase.Rows[i].Cells["ItemID"].Value.ToInt32() }).Rows[0]["ExpiryDateMandatory"]) == true && (dgvPurchase.Rows[i].Cells["ExpiryDate"].Value == DBNull.Value || dgvPurchase.Rows[i].Cells["ExpiryDate"].Value == null))
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 991, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                tcPurchase.SelectedTab = tpItemDetails;
                                dgvPurchase.Focus();
                                dgvPurchase.CurrentCell = dgvPurchase["ExpiryDate", iRowIndex];
                                return false;
                            }

                        }
                        if (MintFromForm == 4)
                        {
                            decimal decReceivedQty = MobjClsBLLPurchase.GetReceivedQty(Convert.ToInt32(dgvPurchase.Rows[i].Cells["ItemID"].Value), MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID, true);

                            if (!MblnAddStatus && ChkTGNR.Checked && decReceivedQty > Convert.ToDecimal(dgvPurchase.Rows[i].Cells["Quantity"].Value))
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 962, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                tcPurchase.SelectedTab = tpItemDetails;
                                return false;
                            }

                        }
                        if (dgvPurchase.Rows[i].Cells["ExpiryDate"].Value != null)
                        {
                            DateTime dtExpiryDate = new DateTime();
                            try
                            {
                                dtExpiryDate = Convert.ToDateTime(dgvPurchase.Rows[i].Cells["ExpiryDate"].Value);
                            }
                            catch
                            {

                            }
                            if (dtExpiryDate < dtpDate.Value)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 959, out MmessageIcon);
                                if (MintFromForm == 3)
                                    MstrMessageCommon = MstrMessageCommon.Replace("*", "GRN");
                                else
                                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Invoice");
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                tcPurchase.SelectedTab = tpItemDetails;
                                dgvPurchase.Focus();
                                dgvPurchase.CurrentCell = dgvPurchase["ExpiryDate", iRowIndex];
                                return false;
                            }
                        }
                    }

                    if (Convert.ToInt32(dgvPurchase.Rows[i].Cells["Uom"].Tag) == 0)
                    {
                        //MsMessageCommon = "Please select Uom.";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 917, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        tcPurchase.SelectedTab = tpItemDetails;
                        dgvPurchase.Focus();
                        dgvPurchase.CurrentCell = dgvPurchase["Uom", iRowIndex];
                        return false;
                    }

                    if (MintFromForm != 3 && (Convert.ToString(dgvPurchase.Rows[i].Cells["Rate"].Value).Trim() == "" || Convert.ToDecimal(dgvPurchase.Rows[i].Cells["Rate"].Value) == 0))
                    {
                        //MsMessageCommon = "Please enter rate.";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 918, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        tcPurchase.SelectedTab = tpItemDetails;
                        dgvPurchase.Focus();
                        dgvPurchase.CurrentCell = dgvPurchase["Rate", iRowIndex];
                        dgvPurchase.CurrentCell.DataGridView.BeginEdit(true);
                        return false;
                    }

                    if (MintFromForm != 3 && (Convert.ToString(dgvPurchase.Rows[i].Cells["NetAmount"].Value).Trim() == "" || Convert.ToDecimal(dgvPurchase.Rows[i].Cells["NetAmount"].Value) <= 0))
                    {
                        //MsMessageCommon = "Please enter total amount.";
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 948, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        tcPurchase.SelectedTab = tpItemDetails;
                        dgvPurchase.Focus();
                        dgvPurchase.CurrentCell = dgvPurchase["Rate", iRowIndex];
                        return false;
                    }
                    if (cboOrderType.SelectedValue.ToInt32()==(int)OperationOrderType.POrderFromGRN && MblnIsFromCancellation )
                    {
                        bool IsGRNExists = MobjClsBLLPurchase.IsGRNExists(strCondition,txtPurchaseNo.Tag.ToInt64());
                        if (IsGRNExists)
                        {
                            //MsMessageCommon = "Some reference Occurs.";
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2276, out MmessageIcon);
                            if (MintFromForm == 3)
                                MstrMessageCommon = MstrMessageCommon.Replace("ordered", "Invoiced");
                            MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            dgvPurchase.Focus();
                            return false;
                        }

                    }
                    if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.POrderFromQuotation)
                    {
                        bool result = new clsBLLPurchase().IsValidItem(dgvPurchase.Rows[i].Cells["ItemID"].Value.ToInt32());
                        if (result)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2289, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            dgvPurchase.Focus();
                            dgvPurchase.CurrentCell = dgvPurchase["ItemName", iRowIndex];
                            return false;
                        }
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in ValidatePurchaseGrid() " + ex.Message);
                MObjLogs.WriteLog("Error in ValidatePurchaseGrid() " + ex.Message, 2);
                return false;
            }
        }

        private bool SavePurchaseOrder()
        {
            try
            {
                if (!MblnIsFromCancellation)
                {
                    if (MblnAddStatus == true)

                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 914, out MmessageIcon);
                    else
                    {
                        if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderSubmittedForApproval || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderSubmitted)
                        {
                            if (ClsCommonSettings.POApproval)
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 952, out MmessageIcon).Replace("***", "order");
                            else
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9011, out MmessageIcon).Replace("*", "Order");
                        }
                        else
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
                    }
                    if (MblnCanShow)
                    {
                        if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                            return false;
                        else
                        {
                            if (!ValidateOnSave())
                                return false;
                        }
                    }
                }
                if (MblnAddStatus && !txtPurchaseNo.ReadOnly && MobjClsBLLPurchase.IsPurchaseNoExists(txtPurchaseNo.Text, MintFromForm, cboCompany.SelectedValue.ToInt32()))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 973, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    else
                        GenerateOrderNo();
                }
                if (!MblnAddStatus && !txtPurchaseNo.ReadOnly && MobjClsBLLPurchase.IsPurchaseNoAlreadyExists(cboCompany.SelectedValue.ToInt32(), txtPurchaseNo.Text))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2282, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");
                    ErrPurchase.SetError(txtPurchaseNo, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    txtPurchaseNo.Focus();
                    return false;
                }
                FillParameters();
                FillDetailParameters();

                if (MobjClsBLLPurchase.SavePurchaseOrder(MblnAddStatus, strCondition, MblnIsFromCancellation))
                {
                    if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderApproved)
                    {
                        objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.PurchaseOrderApproved;
                        objFrmTradingMain.tmrAlert.Enabled = true;
                    }
                    txtPurchaseNo.Tag = MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID;
                    MObjLogs.WriteLog("Saved successfully:SaveCurrency()  " + this.Name + "", 0);
                    return true;

                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SavePurchaseOrder() " + ex.Message);
                MObjLogs.WriteLog("Error in SavePurchaseOrder() " + ex.Message, 2);
                return false;
            }
        }
        private bool ValidateOnSave()
        {
            if (MblnAddStatus)
            {
                if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.POrderFromGRN)
                {
                    if (MobjClsBLLPurchase.IsPurchaseInvoiceExists(strCondition))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2280, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("*", "Updated");
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        tcPurchase.SelectedTab = tpItemDetails;
                        return false;
                    }

                    if (new clsBLLPurchaseInvoice().IsPurchaseOrderExists(strCondition))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2279, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("*", "Updated");
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        tcPurchase.SelectedTab = tpItemDetails;
                        return false;
                    }
                 
                }
                if (MblnAddStatus && txtPurchaseNo.ReadOnly && MobjClsBLLPurchase.IsPurchaseNoExists(txtPurchaseNo.Text, MintFromForm, cboCompany.SelectedValue.ToInt32()))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 973, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    else
                        GenerateOrderNo();
                }
            }
            return true;
        }
        private bool SavePurchaseInvoice()
        {
            try
            {
                if (!MblnIsFromCancellation)
                {
                    if (MblnAddStatus == true)
                        if (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.PInvoiceDirect && !ChkTGNR.Checked)
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 971, out MmessageIcon);
                        else
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 906, out MmessageIcon);
                    else
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
                    if (MblnCanShow)
                        if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                            return false;
                }

                if (MblnAddStatus && txtPurchaseNo.ReadOnly && MobjClsBLLPurchase.IsPurchaseNoExists(txtPurchaseNo.Text, MintFromForm, cboCompany.SelectedValue.ToInt32()))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 973, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Invoice");
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    else
                        GenerateOrderNo();
                }

                FillParameters();
                FillDetailParameters();

                if (!ChkTGNR.Checked)
                {
                    FillLocationDetailsParameters();
                }

                if (!MblnAddStatus && (!MobjClsBLLPurchase.PobjClsDTOPurchase.blnGRNRequired))
                {
                    bool blnIsSalesDone = false;
                    string strItemSold = MobjClsBLLPurchase.StockValidation(false, false, out blnIsSalesDone);
                    if (strItemSold != null)
                    {
                        if (!blnIsSalesDone)
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 946, out MmessageIcon).Replace("@", strItemSold);
                        else
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9001, out MmessageIcon).Replace("*", strItemSold);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim());
                        return false;
                    }
                }

                if (MobjClsBLLPurchase.SavePurchaseInvoice(MblnAddStatus))
                {
                    //Creating barcode
                    if (!MobjClsBLLPurchase.PobjClsDTOPurchase.blnGRNRequired)
                    {
                        FrmBarCode objBarcode = new FrmBarCode();
                        foreach (clsDTOPurchaseDetails details in MobjClsBLLPurchase.PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection)
                        {
                            objBarcode.CreateBarCode(0, details.intItemID, details.intBatchID, details.strBatchNumber);
                        }
                        objBarcode.Dispose();
                    }

                    //if (ClsCommonSettings.PblnIsLocationWise)
                    //{
                    //    if (MblnAddStatus && (MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType == (Int32)OperationOrderType.PInvoiceDirect || !MobjClsBLLPurchase.PobjClsDTOPurchase.blnGRNRequired))
                    //    {
                    //        if (MessageBox.Show("Do you want to set Item Locations ?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    //        {
                    //            //using (FrmItemAllocation objFrmItemAllocation = new FrmItemAllocation())
                    //            //{
                    //            //    objFrmItemAllocation.PintType = 2;
                    //            //    objFrmItemAllocation.intReferenceID = MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID;
                    //            //    objFrmItemAllocation.ShowDialog();
                    //            //}
                    //        }
                    //    }
                    //}
                    txtPurchaseNo.Tag = MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID;
                    MObjLogs.WriteLog("Saved successfully:SaveCurrency()  " + this.Name + "", 0);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SavePurchaseInvoice() " + ex.Message);
                MObjLogs.WriteLog("Error in SavePurchaseInvoice() " + ex.Message, 2);
                return false;
            }
        }

        private bool SavePurchaseGRN()
        {
            try
            {
                if (!MblnIsFromCancellation)
                {
                    if (MblnAddStatus == true)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 925, out MmessageIcon);
                        if (PintOperationTypeID != 0)
                            MstrMessageCommon = MstrMessageCommon.Replace("GRN", "Material Return");
                    }
                    else
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
                    if (MblnCanShow)
                        if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                            return false;
                }
                if (MblnAddStatus && txtPurchaseNo.ReadOnly && MobjClsBLLPurchase.IsPurchaseNoExists(txtPurchaseNo.Text, MintFromForm, cboCompany.SelectedValue.ToInt32()))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 973, out MmessageIcon);
                    if (PintOperationTypeID == 0)
                        MstrMessageCommon = MstrMessageCommon.Replace("*", "GRN");
                    else
                        MstrMessageCommon = MstrMessageCommon.Replace("*", "Material Return");

                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    else
                        GenerateOrderNo();
                }
                //if (MblnAddStatus && !MobjClsBLLPurchase.PurchaseNoExists(cboOrderType.SelectedValue.ToInt32(), cboOrderNo.SelectedValue.ToInt32(), cboCompany.SelectedValue.ToInt32()))
                //{
                //    //MsMessageCommon = "Purchase Aready exists*.";
                //    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2225, out MmessageIcon);
                //    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    TmrPurchase.Enabled = true;
                //    tcPurchase.SelectedTab = tpItemDetails;
                //    return false;
                //}
                FillParameters();
                FillDetailParameters();

                FillLocationDetailsParameters();

                if (!MblnAddStatus)
                {
                    bool blnIsSalesDone = false;
                    string strItemSold = string.Empty;
                    // if(cboOrderType.SelectedValue.ToInt32() != (int)PurchaseSettings.GRNItemReturnType)
                    strItemSold = MobjClsBLLPurchase.StockValidation(false, true, out blnIsSalesDone);
                    if (!string.IsNullOrEmpty(strItemSold))
                    {
                        if (!blnIsSalesDone)
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 946, out MmessageIcon).Replace("@", strItemSold);
                        else
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9001, out MmessageIcon).Replace("*", strItemSold);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim());
                        return false;
                    }
                }
                if (MobjClsBLLPurchase.SavePurchaseGRN(MblnAddStatus, MblnIsFromCancellation))
                {
                    //Creating barcode
                    FrmBarCode objBarcode = new FrmBarCode();
                    foreach (clsDTOPurchaseDetails details in MobjClsBLLPurchase.PobjClsDTOPurchase.lstClsDTOPurchaseDetailsCollection)
                    {
                        objBarcode.CreateBarCode(0, details.intItemID, details.intBatchID, details.strBatchNumber);
                    }
                    objBarcode.Dispose();

                    //if (ClsCommonSettings.PblnIsLocationWise)
                    //{
                    //    if (MblnAddStatus && MessageBox.Show("Do you want to set Item Locations ?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    //    {
                    //        //using (FrmItemAllocation objFrmItemAllocation = new FrmItemAllocation())
                    //        //{
                    //        //    objFrmItemAllocation.PintType = 1;
                    //        //    objFrmItemAllocation.intReferenceID = MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID;
                    //        //    objFrmItemAllocation.ShowDialog();
                    //        //}
                    //    }
                    //}
                    txtPurchaseNo.Tag = MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID;
                    MObjLogs.WriteLog("Saved successfully:SaveCurrency()  " + this.Name + "", 0);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SavePurchaseGRN() " + ex.Message);
                MObjLogs.WriteLog("Error in SavePurchaseGRN() " + ex.Message, 2);
                return false;
            }
        }

        private void Changestatus()
        {
            //Changing status
            MblnChangeStatus = true;
            if (MblnAddStatus && intPurchaseIDToLoad == 0)
            {
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
                BindingNavigatorClearItem.Enabled = true;
            }
            else
            {
                if (MobjClsBLLPurchase != null && (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationOpened || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderOpened || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.GRNOpened || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PInvoiceOpened || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationRejected || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderRejected))
                    BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                else
                    BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            }

            ErrPurchase.Clear();
        }

        private bool DisplayAllNoInSearchGrid()
        {
            try
            {
                DataTable dtTemp = null;
                string strFilterCondition = string.Empty;
                if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                    strFilterCondition += " CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue);
                else
                {
                    dtTemp = null;
                    dtTemp = (DataTable)cboSCompany.DataSource;
                    if (dtTemp.Rows.Count > 0)
                    {
                        strFilterCondition += "CompanyID In (";
                        foreach (DataRow dr in dtTemp.Rows)
                        {
                            strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                        }
                        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                        strFilterCondition += ")";
                    }
                }
                if (cboSSupplier.SelectedValue != null && Convert.ToInt32(cboSSupplier.SelectedValue) != -2)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "VendorID = " + Convert.ToInt32(cboSSupplier.SelectedValue);
                }
                if (cboSExecutive.SelectedValue != null && Convert.ToInt32(cboSExecutive.SelectedValue) != -2)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "CreatedBy = " + Convert.ToInt32(cboSExecutive.SelectedValue);
                }
                //else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                //{
                //    //if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                //    //{
                //    dtTemp = null;
                //    dtTemp = (DataTable)cboSExecutive.DataSource;
                //    if (dtTemp.Rows.Count > 0)
                //    {
                //        clsBLLPermissionSettings MobjClsBLLPermissionSettings = new clsBLLPermissionSettings();
                //        //DataTable datTempPer = MobjClsBLLPermissionSettings.GetControlPermissions1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, intTempEmployee);
                //        //if (datTempPer != null)
                //        //{
                //        //    if (datTempPer.Rows.Count > 0)
                //        //    {
                //        //        if (datTempPer.Rows[0]["IsVisible"].ToString() == "True")
                //        //            strFilterCondition += "And EmployeeID In (0,";
                //        //        else
                //        //            strFilterCondition += "And EmployeeID In (";
                //        //    }
                //        //    else
                //        //        strFilterCondition += "And EmployeeID In (";
                //        //}
                //        //else
                //        if (!string.IsNullOrEmpty(strFilterCondition))
                //            strFilterCondition += " And ";

                //        strFilterCondition += "CreatedBy In (";
                //        foreach (DataRow dr in dtTemp.Rows)
                //        {
                //            strFilterCondition += Convert.ToInt32(dr["UserID"]) + ",";
                //        }
                //        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                //        strFilterCondition += ")";
                //    }
                //    else
                //    {
                //        if (!string.IsNullOrEmpty(strFilterCondition))
                //            strFilterCondition += " And ";
                //        strFilterCondition += "CreatedBy = -1";
                //    }
                //    //}
                //}
                if (cboSStatus.SelectedValue != null && Convert.ToInt32(cboSStatus.SelectedValue) != -2)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "StatusID = " + Convert.ToInt32(cboSStatus.SelectedValue);
                }

                if (!string.IsNullOrEmpty(strFilterCondition))
                    strFilterCondition += " And ";
                strFilterCondition += " CreatedDate >= '" + dtpSFrom.Value.ToString("dd-MMM-yyyy") + "' And CreatedDate <= '" + dtpSTo.Value.ToString("dd-MMM-yyyy") + "'";

                switch (MintFromForm)
                {
                    case 1:

                        DataTable dtPurchases = MobjClsBLLPurchase.GetPurchaseNos(1);
                        if (!string.IsNullOrEmpty(txtSPurchaseNo.Text.Trim()))
                        {

                            strFilterCondition = " QuotationNo = '" + txtSPurchaseNo.Text.Trim() + "'";
                        }
                        dtPurchases.DefaultView.RowFilter = strFilterCondition;
                        dgvPurchaseDisplay.DataSource = null;
                        dgvPurchaseDisplay.DataSource = dtPurchases.DefaultView.ToTable();
                        dgvPurchaseDisplay.Columns["PurchaseQuotationID"].Visible = false;
                        dgvPurchaseDisplay.Columns["CompanyID"].Visible = false;
                        dgvPurchaseDisplay.Columns["VendorID"].Visible = false;
                        dgvPurchaseDisplay.Columns["StatusID"].Visible = false;
                        dgvPurchaseDisplay.Columns["EmployeeID"].Visible = false;
                        dgvPurchaseDisplay.Columns["OrderDate"].Visible = false;
                        dgvPurchaseDisplay.Columns["CreatedBy"].Visible = false;
                        dgvPurchaseDisplay.Columns["QuotationNo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        lblPurchaseCount.Text = "Total Quotations " + dgvPurchaseDisplay.Rows.Count;
                        break;
                    case 2:

                        dtPurchases = MobjClsBLLPurchase.GetPurchaseNos(2);
                        if (!string.IsNullOrEmpty(txtSPurchaseNo.Text.Trim()))
                        {
                            strFilterCondition = " OrderNo = '" + txtSPurchaseNo.Text.Trim() + "'";
                        }
                        dtPurchases.DefaultView.RowFilter = strFilterCondition;
                        dgvPurchaseDisplay.DataSource = null;
                        dgvPurchaseDisplay.DataSource = dtPurchases.DefaultView.ToTable();
                        dgvPurchaseDisplay.Columns["PurchaseOrderID"].Visible = false;
                        dgvPurchaseDisplay.Columns["CompanyID"].Visible = false;
                        dgvPurchaseDisplay.Columns["VendorID"].Visible = false;
                        dgvPurchaseDisplay.Columns["StatusID"].Visible = false;
                        dgvPurchaseDisplay.Columns["EmployeeID"].Visible = false;
                        dgvPurchaseDisplay.Columns["CreatedDate"].Visible = false;
                        dgvPurchaseDisplay.Columns["CreatedBy"].Visible = false;
                        dgvPurchaseDisplay.Columns["OrderNo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        lblPurchaseCount.Text = "Total Orders " + dgvPurchaseDisplay.Rows.Count;
                        break;

                    case 3:

                        if (!string.IsNullOrEmpty(txtSPurchaseNo.Text.Trim()))
                        {
                            strFilterCondition = " GRNNo = '" + txtSPurchaseNo.Text.Trim() + "'";
                        }

                        //if (!string.IsNullOrEmpty(strFilterCondition))
                        //    strFilterCondition += " And ";


                        dtPurchases = MobjClsBLLPurchase.GetPurchaseNos(3);
                        dtPurchases.DefaultView.RowFilter = strFilterCondition;
                        dgvPurchaseDisplay.DataSource = null;
                        dgvPurchaseDisplay.DataSource = dtPurchases.DefaultView.ToTable();
                        dgvPurchaseDisplay.Columns["GRNID"].Visible = false;
                        dgvPurchaseDisplay.Columns["CompanyID"].Visible = false;
                        dgvPurchaseDisplay.Columns["VendorID"].Visible = false;
                        dgvPurchaseDisplay.Columns["StatusID"].Visible = false;
                        dgvPurchaseDisplay.Columns["EmployeeID"].Visible = false;
                        dgvPurchaseDisplay.Columns["CreatedDate"].Visible = false;
                        dgvPurchaseDisplay.Columns["CreatedBy"].Visible = false;
                        dgvPurchaseDisplay.Columns["OrderTypeID"].Visible = false;
                        dgvPurchaseDisplay.Columns["GRNNo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                        if (PintOperationTypeID != 0)
                        {
                            dgvPurchaseDisplay.Columns["GRNNo"].HeaderText = "Return No";
                            lblPurchaseCount.Text = "Total Material Returns " + dgvPurchaseDisplay.Rows.Count;
                        }
                        else
                            lblPurchaseCount.Text = "Total GRNs " + dgvPurchaseDisplay.Rows.Count;


                        break;

                    case 4:
                        dtPurchases = MobjClsBLLPurchase.GetPurchaseNos(4);
                        if (!string.IsNullOrEmpty(txtSPurchaseNo.Text.Trim()))
                        {
                            strFilterCondition = " InvoiceNo = '" + txtSPurchaseNo.Text.Trim() + "'";
                        }
                        dtPurchases.DefaultView.RowFilter = strFilterCondition;
                        dgvPurchaseDisplay.DataSource = null;
                        dgvPurchaseDisplay.DataSource = dtPurchases.DefaultView.ToTable();
                        dgvPurchaseDisplay.Columns["PurchaseInvoiceID"].Visible = false;
                        dgvPurchaseDisplay.Columns["CompanyID"].Visible = false;
                        dgvPurchaseDisplay.Columns["VendorID"].Visible = false;
                        dgvPurchaseDisplay.Columns["StatusID"].Visible = false;
                        dgvPurchaseDisplay.Columns["EmployeeID"].Visible = false;
                        dgvPurchaseDisplay.Columns["CreatedDate"].Visible = false;
                        dgvPurchaseDisplay.Columns["CreatedBy"].Visible = false;
                        dgvPurchaseDisplay.Columns["InvoiceNo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        lblPurchaseCount.Text = "Total Invoices " + dgvPurchaseDisplay.Rows.Count;
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayAllNoInSearchGrid() " + ex.Message);
                MObjLogs.WriteLog("Error in DisplayAllNoInSearchGrid() " + ex.Message, 2);
                return false;
            }
        }

        private bool DisplayOrderInfo(Int64 iOrderID)
        {
            this.lblOrder1.Visible = this.lblOrder.Visible = this.lblOrderNo.Visible = this.lblOrderNo1.Visible = false;
            try
            {
                switch (MintFromForm)
                {
                    case 1:
                        if (MobjClsBLLPurchase.DisplayPurchaseQuotationInfo(iOrderID))
                        {
                            btnDocuments.Visible = true;
                            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                            txtPurchaseNo.Tag = MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID;
                            cboCompany.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID;
                            clsBLLLogin mobjClsBllLogin = new clsBLLLogin();
                            mobjClsBllLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue.ToInt32()));
                            if (cboCompany.SelectedValue == null)
                            {
                                DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID });
                                if (datTemp.Rows.Count > 0)
                                    cboCompany.Text = datTemp.Rows[0]["Name"].ToString();
                            }
                            cboCompany.Enabled = false;
                            cboSupplier.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID;
                            if (cboSupplier.SelectedValue == null)
                                cboSupplier.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVendorName.ToString();
                            AddToAddressMenu(MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID);

                            MintVendorAddID = MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorAddID;

                            lblSupplierAddressName.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strAddressName + " Address";
                            DisplayAddress(Convert.ToInt32(cboSupplier.SelectedValue));
                            lblCreatedByText.Text = "Created By :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strEmployeeName;
                            lblCreatedDateValue.Text = " Created Date :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strCreatedDate.ToDateTime().ToString("dd-MMM-yyyy");
                            txtPurchaseNo.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strPurchaseNo;
                            LoadCombos(13, "");
                            cboCurrency.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCurrencyID;

                            if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationCancelled)
                            {
                                lblCreatedByText.Text += "        | Cancelled By :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strCancelledBy;
                                lblCreatedDateValue.Text += "        | Cancelled Date :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strCancellationDate.ToDateTime().ToString("dd-MMM-yyyy");
                                txtDescription.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strDescription;
                            }
                            else if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationRejected)
                            {
                                lblCreatedByText.Text += "        | Rejected By :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strApprovedBy;
                                lblCreatedDateValue.Text += "        | Rejected Date :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strApprovedDate.ToDateTime().ToString("dd-MMM-yyyy");
                                txtDescription.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVerifcationDescription;
                            }
                            else if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationApproved)
                            {
                                lblCreatedByText.Text += "        | Approved By :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strApprovedBy;
                                lblCreatedDateValue.Text += "        | Approved Date :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strApprovedDate.ToDateTime().ToString("dd-MMM-yyyy");
                                txtDescription.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVerifcationDescription;
                            }

                            cboPaymentTerms.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intPaymentTermsID;



                            cboOrderType.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType;
                            if (cboOrderType.SelectedValue == null)
                            {
                                DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "CommonOrderTypeReference", "OrderTypeID=" + MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType });
                                if (datTemp.Rows.Count > 0)
                                    cboOrderType.Text = datTemp.Rows[0]["Description"].ToString();
                            }
                            if (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.PQuotationFromRFQ)
                            {
                                cboOrderNo.Visible = true;
                                cboOrderNo.DataSource = null;
                                DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "RFQID,RFQNo,QuotationDate,DueDate", "InvRFQMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) + "" });
                                cboOrderNo.ValueMember = "RFQID";
                                cboOrderNo.DisplayMember = "RFQNo";
                                cboOrderNo.DataSource = dtCombo;
                                MblnIsEditable = false;
                            }
                            else
                            {
                                cboOrderNo.Visible = false;
                                cboOrderNo.DataSource = null;
                            }

                            dtpDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strDate);
                            dtpDueDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strDueDate);

                            cboOrderNo.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intReferenceID;
                            cboOrderType.Enabled = false;
                            cboOrderNo.Enabled = false;

                            DisplayPurchaseOrderDetail(iOrderID);

                            txtSubTotal.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandAmount.ToString();

                            cboDiscount.Text = (MobjClsBLLPurchase.PobjClsDTOPurchase.blnGrandDiscount.ToBoolean() ? "%" : "Amount");
                            if (cboDiscount.Text == "%")
                                txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountPercent.ToString();
                            else
                                txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountAmt.ToString();

                            txtTotalAmount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decNetAmount.ToString();


                            txtRemarks.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strRemarks;
                            txtExpenseAmount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decExpenseAmount.ToString();
                            CalculateExchangeCurrencyRate();

                            SetPurchaseStatus();
                            lblPurchasestatus.Text = "";

                            //cboIncoTerms.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intIncoTerms;
                            //txtLeadTime.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decLeadTime.ToString();
                            //dtpProductionDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strProductionDate);
                            //dtpTrnsShipmentDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strTranShipmentDate);
                            //dtpClearenceDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strClearenceDate);
                            //cboContainerType.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intContainerTypeID;
                            //txtNoOfContainers.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.intNoOfContainers.ToString();

                            if (!string.IsNullOrEmpty(MobjClsBLLPurchase.PobjClsDTOPurchase.strVerificationCriteria) && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PQuotationApproved)
                            {
                                lblCriteria.Visible = true;
                                lblCriteriaText.Visible = true;
                                lblCriteriaText.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVerificationCriteria;
                                lblDescription.Location = new Point(776, 62);
                                txtDescription.Location = new Point(842, 60);
                                txtDescription.Size = new Size(293, 45);
                            }
                            else
                            {
                                lblDescription.Location = lblCriteria.Location;
                                txtDescription.Location = lblCriteriaText.Location;
                                txtDescription.Size = new Size(293, 67);
                                lblCriteria.Visible = false;
                                lblCriteriaText.Visible = false;
                            }
                        }
                        break;
                    case 2:
                        if (MobjClsBLLPurchase.DisplayPurchaseOrderInfo(iOrderID))
                        {
                            btnDocuments.Visible = true;
                            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                            txtPurchaseNo.Tag = MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID;
                            cboCompany.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID;
                            cboTaxScheme.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intTaxSchemeID;
                            clsBLLLogin mobjClsBllLogin = new clsBLLLogin();
                            mobjClsBllLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue.ToInt32()));
                            if (cboCompany.SelectedValue == null)
                            {
                                DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID });
                                if (datTemp.Rows.Count > 0)
                                    cboCompany.Text = datTemp.Rows[0]["Name"].ToString();
                            }
                            cboCompany.Enabled =cboTaxScheme.Enabled= false;
                            cboSupplier.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID;
                            if (cboSupplier.SelectedValue == null)
                                cboSupplier.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVendorName;
                            AddToAddressMenu(MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID);
                            lblCreatedByText.Text = " Created By :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strEmployeeName;
                            lblCreatedDateValue.Text = "Created Date :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strCreatedDate.ToDateTime().ToString("dd-MMM-yyyy");
                            MintVendorAddID = MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorAddID;
                            lblSupplierAddressName.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strAddressName + " Address";
                            DisplayAddress(Convert.ToInt32(cboSupplier.SelectedValue));
                            cboPaymentTerms.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intPaymentTermsID;
                            string s = MobjClsBLLPurchase.PobjClsDTOPurchase.strRemarks;
                            ChkTGNR.Checked = MobjClsBLLPurchase.PobjClsDTOPurchase.blnGRNRequired;
                            LoadCombos(13, "");
                            cboCurrency.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCurrencyID;
                       
                            if (!string.IsNullOrEmpty(MobjClsBLLPurchase.PobjClsDTOPurchase.strVerificationCriteria) && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.POrderApproved)
                            {
                                lblCriteria.Visible = true;
                                lblCriteriaText.Visible = true;
                                lblCriteriaText.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVerificationCriteria;
                                lblDescription.Location = new Point(776, 62);
                                txtDescription.Location = new Point(842, 60);
                                txtDescription.Size = new Size(293, 45);
                            }
                            else
                            {
                                //lblDescription.Location = lblCriteria.Location;
                                //txtDescription.Location = lblCriteriaText.Location;
                                txtDescription.Size = new Size(180, 67);
                                lblDescription.Location = new Point(899, 28);
                                txtDescription.Location = new Point(899, 42);
                                lblCriteria.Visible = false;
                                lblCriteriaText.Visible = false;
                            }
                            cboOrderType.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType;
                            if ((Int32)cboOrderType.SelectedValue == (Int32)OperationOrderType.POrderFromGRN)
                            {
                                cboGRNWarehouse.SelectedValue = MobjClsBLLPurchase.GetwarehouseID(MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID);
                                cboGRNWarehouse.Enabled = btnGRNWarehouse.Enabled = false;
                            }
                            if (cboOrderType.SelectedValue == null)
                            {
                                DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "CommonOrderTypeReference", "OrderTypeID=" + MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType });
                                if (datTemp.Rows.Count > 0)
                                    cboOrderType.Text = datTemp.Rows[0]["OrderType"].ToString();
                            }
                            //if ((Int32)cboOrderType.SelectedValue == (Int32)OperationOrderType.POrderFromIndent)
                            //{

                            //    cboOrderNo.Visible = true;
                            //    cboOrderNo.DataSource = null;
                            //    DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "purchaseindentID,PurchaseIndentNo", "STPurchaseIndentMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) + "", "purchaseindentID", "PurchaseIndentNo" });
                            //    cboOrderNo.ValueMember = "purchaseindentID";
                            //    cboOrderNo.DisplayMember = "PurchaseIndentNo";
                            //    cboOrderNo.DataSource = dtCombo;

                            //}
                            if ((Int32)cboOrderType.SelectedValue == (Int32)OperationOrderType.POrderFromQuotation)
                            {
                                cboOrderNo.Visible = true;
                                cboOrderNo.DataSource = null;
                                DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseQuotationID,PurchaseQuotationNo,QuotationDate,DueDate", "InvPurchaseQuotationMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) + "", "PurchaseQuotationID", "PurchaseQuotationNo" });
                                cboOrderNo.ValueMember = "PurchaseQuotationID";
                                cboOrderNo.DisplayMember = "PurchaseQuotationNo";
                                cboOrderNo.DataSource = dtCombo;
                                MblnIsEditable = false;
                            }
                            else if ((Int32)cboOrderType.SelectedValue == (Int32)OperationOrderType.POrderFromGRN)
                            {
                                cboSupplier.Enabled = false;
                                DataTable dtCombo = MobjClsBLLPurchase.GetPurchaseOrderReference(MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID);
                                DataColumn[] keys = new DataColumn[1];
                                keys[0] = dtCombo.Columns[0];
                                dtCombo.PrimaryKey = keys;

                                for (int i= 0 ;i<clbGRN.Items.Count;i++)
                                {
                                    if (dtCombo.Rows.Contains(((System.Data.DataRowView)(clbGRN.Items[i])).Row.ItemArray[0]))
                                    {
                                        clbGRN.SetItemChecked(i, true);
                                    }
                                    else
                                    {
                                        clbGRN.SetItemChecked(i, false);
                                    }

                                }
                                clbGRN_SelectedIndexChanged(null, null);
                                MblnIsEditable = false;
                            }
                            else
                            {
                                cboOrderNo.Visible = false;
                                cboOrderNo.DataSource = null;
                            }
                            cboOrderNo.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intReferenceID;
                            cboOrderType.Enabled = false;
                            cboOrderNo.Enabled = false;
                            
                            string s1 = MobjClsBLLPurchase.PobjClsDTOPurchase.strRemarks;
                            txtPurchaseNo.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strPurchaseNo;

                            dtpDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strDate);
                            dtpDueDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strDueDate);
                            dtpDueDate.Enabled = false;


                            if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderCancelled)
                            {
                                lblCreatedByText.Text += "        | Cancelled By :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strCancelledBy;
                                lblCreatedDateValue.Text += "        | Cancelled Date :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strCancellationDate.ToDateTime().ToString("dd-MMM-yyyy");
                                txtDescription.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strDescription;
                            }
                            else if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderRejected)
                            {
                                lblCreatedByText.Text += "        | Rejected By :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strApprovedBy;
                                lblCreatedDateValue.Text += "        | Rejected Date :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strApprovedDate.ToDateTime().ToString("dd-MMM-yyyy");
                                txtDescription.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVerifcationDescription;
                            }
                            else if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderApproved)
                            {
                                lblCreatedByText.Text += "        | Approved By :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strApprovedBy;
                                lblCreatedDateValue.Text += "        | Approved Date :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strApprovedDate.ToDateTime().ToString("dd-MMM-yyyy");
                                txtDescription.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVerifcationDescription;
                            }
                            DisplayPurchaseOrderDetail(iOrderID);
                            //if ((Int32)cboOrderType.SelectedValue == (Int32)OperationOrderType.POrderFromGRN)
                            //{
                      
                            //    clbGRN_SelectedIndexChanged(null, null);

                            //}
                            txtTaxAmount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decTaxAmount.ToString();
                            txtRemarks.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strRemarks;
                            txtSubTotal.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandAmount.ToString();

                            cboDiscount.SelectedIndex = (MobjClsBLLPurchase.PobjClsDTOPurchase.blnGrandDiscount.ToBoolean() ? 0 : 1);
                            if (cboDiscount.SelectedIndex == 0)
                                txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountPercent.ToString();
                            else
                                txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountAmt.ToString();
                            txtTotalAmount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decNetAmount.ToString();
                            txtExpenseAmount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decExpenseAmount.ToString();

                            lblPurchasestatus.Text = "";
                            txtAdvPayment.Text = MobjClsBLLPurchase.GetAdvancePayment(MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID).ToString("F" + MintExchangeCurrencyScale);

                            //cboIncoTerms.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intIncoTerms;
                            //txtLeadTime.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decLeadTime.ToString();
                            //dtpProductionDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strProductionDate);
                            //dtpTrnsShipmentDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strTranShipmentDate);
                            //dtpClearenceDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strClearenceDate);
                            //cboContainerType.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intContainerTypeID;
                            //txtNoOfContainers.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.intNoOfContainers.ToString();
                            SetPurchaseStatus();

                            if (MobjclsBLLCommonUtility.FillCombos(new string[] { "IsNull(Sum(PD.Amount),0) as TotalAmount", "AccReceiptAndPaymentMaster PM Inner Join AccReceiptAndPaymentDetails PD ON PM.ReceiptAndPaymentID=PD.ReceiptAndPaymentID", "PM.OperationTypeID = " + (int)OperationType.PurchaseOrder + " And PD.ReferenceID = " + cboOrderNo.SelectedValue.ToInt64() + " And PM.ReceiptAndPaymentTypeID <> " + (int)PaymentTypes.DirectExpense }).Rows[0]["TotalAmount"].ToDecimal() != 0)
                                cboPaymentTerms.Enabled = false;
                        }
                        break;

                    case 3:
                        if (MobjClsBLLPurchase.DisplayPurchaseGRNInfo(iOrderID))
                        {
                            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                            dgvPurchase.Rows.Clear();
                            dgvLocationDetails.Rows.Clear();
                            txtPurchaseNo.Tag = MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID;
                            cboCompany.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID;
                            clsBLLLogin mobjClsBllLogin = new clsBLLLogin();
                            mobjClsBllLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue.ToInt32()));
                            if (cboCompany.SelectedValue == null)
                            {
                                DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID });
                                if (datTemp.Rows.Count > 0)
                                    cboCompany.Text = datTemp.Rows[0]["Name"].ToString();
                            }
                            cboCompany.Enabled=cboTaxScheme.Enabled = false;
                            cboSupplier.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID;
                            if (cboSupplier.SelectedValue == null)
                                cboSupplier.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVendorName;
                            AddToAddressMenu(MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID);
                            lblCreatedByText.Text = "Created By :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strEmployeeName;
                            lblCreatedDateValue.Text = "Created Date :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strCreatedDate.ToDateTime().ToString("dd-MMM-yyyy");
                            MintVendorAddID = MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorAddID;
                            lblSupplierAddressName.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strAddressName + " Address";
                            DisplayAddress(Convert.ToInt32(cboSupplier.SelectedValue));
                            cboWarehouse.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intWarehouseID;
                            MobjClsBLLPurchase.PobjClsDTOPurchase.intTempWarehouseID = MobjClsBLLPurchase.PobjClsDTOPurchase.intWarehouseID;
                            string s = MobjClsBLLPurchase.PobjClsDTOPurchase.strRemarks;
                            LoadCombos(13, "");

                            cboOrderType.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType;
                            if (cboOrderType.SelectedValue == null)
                            {
                                DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "CommonOrderTypeReference", "OrderTypeID=" + MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType });
                                if (datTemp.Rows.Count > 0)
                                    cboOrderType.Text = datTemp.Rows[0]["OrderType"].ToString();
                            }
                            if ((Int32)cboOrderType.SelectedValue == (Int32)OperationOrderType.GRNFromInvoice)
                            {

                                cboOrderNo.Visible = true;
                                cboOrderNo.DataSource = null;
                                DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseInvoiceID,PurchaseInvoiceNo,InvoiceDate,CurrencyID", "InvPurchaseInvoiceMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) });
                                cboOrderNo.ValueMember = "PurchaseInvoiceID";
                                cboOrderNo.DisplayMember = "PurchaseInvoiceNo";
                                cboOrderNo.DataSource = dtCombo;
                                cboOrderNo.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intReferenceID;
                                dtCombo.DefaultView.RowFilter = "PurchaseInvoiceID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intReferenceID;
                                MobjClsBLLPurchase.PobjClsDTOPurchase.intCurrencyID = dtCombo.DefaultView.ToTable().Rows[0]["CurrencyID"].ToInt32();
                                MblnIsEditable = false;
                                this.ShowNoForGRN();
                            }
                            else if ((Int32)cboOrderType.SelectedValue == (Int32)OperationOrderType.GRNFromOrder)
                            {

                                cboOrderNo.Visible = true;
                                cboOrderNo.DataSource = null;
                                DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseOrderID,PurchaseOrderNo,OrderDate,CurrencyID", "InvPurchaseOrderMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) });
                                cboOrderNo.ValueMember = "PurchaseOrderID";
                                cboOrderNo.DisplayMember = "PurchaseOrderNo";
                                cboOrderNo.DataSource = dtCombo;
                                cboOrderNo.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intReferenceID;
                                dtCombo.DefaultView.RowFilter = "PurchaseOrderID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intReferenceID;
                                MobjClsBLLPurchase.PobjClsDTOPurchase.intCurrencyID = dtCombo.DefaultView.ToTable().Rows[0]["CurrencyID"].ToInt32();
                                MblnIsEditable = false;
                                ShowQuotationNo();
                                //lblOrder.Visible = false;
                                //lblOrder1.Visible = false;
                            }
                            else if ((int)cboOrderType.SelectedValue == (int)OperationOrderType.GRNFromTransfer)
                            {
                                cboOrderNo.Visible = true;
                                cboOrderNo.DataSource = null;
                                DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "StockTransferID,StockTransferNo", "InvStockTransferMaster", "ReferenceID In (Select WarehouseID From InvWarehouse Where CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + ") And OrderTypeID In (" + (int)OperationOrderType.STWareHouseToWareHouseTransfer + "," + (int)OperationOrderType.STWareHouseToWareHouseTransferDemo + ")" });
                                cboOrderNo.ValueMember = "StockTransferID";
                                cboOrderNo.DisplayMember = "StockTransferNo";
                                cboOrderNo.DataSource = dtCombo;
                                cboOrderNo.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intReferenceID;
                                MblnIsEditable = false;
                            }
                            else
                            {
                                cboOrderNo.Visible = true;
                                cboOrderNo.DataSource = null;
                                DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "IIM.ItemIssueID,IIM.ItemIssueNo", "InvItemIssueMaster IIM", "IIM.CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + "" });
                                cboOrderNo.ValueMember = "ItemIssueID";
                                cboOrderNo.DisplayMember = "ItemIssueNo";
                                cboOrderNo.DataSource = dtCombo;
                                cboOrderNo.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intReferenceID;
                                MblnIsEditable = false;
                            }
                            cboOrderNo.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intReferenceID;
                            cboCurrency.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCurrencyID;
                            cboOrderType.Enabled = false;
                            cboOrderNo.Enabled = false;
                            string s1 = MobjClsBLLPurchase.PobjClsDTOPurchase.strRemarks;
                            txtPurchaseNo.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strPurchaseNo;
                            dtpDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strDate);
                            dtpDueDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strDueDate);

                            txtRemarks.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strRemarks;

                            if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.GRNCancelled)
                            {
                                lblCreatedByText.Text += "        | Cancelled By :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strCancelledBy;
                                lblCreatedDateValue.Text += "        | Cancelled Date :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strCancellationDate.ToDateTime().ToString("dd-MMM-yyyy");
                                txtDescription.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strDescription;
                                lblDescription.Location = lblCriteria.Location;
                                txtDescription.Location = lblCriteriaText.Location;


                                if (PintOperationTypeID != 0)
                                {
                                    lblDescription.Left = lblStatus.Left;
                                    txtDescription.Left = lblStatusText.Left;
                                }
                                else
                                    txtDescription.Size = new Size(293, 67);
                            }
                            DisplayPurchaseOrderDetail(iOrderID);
                            DisplayLocationDetails();

                            lblPurchasestatus.Text = "";

                            SetPurchaseStatus();

                            btnActions.Enabled = btnAddExpense.Enabled = false;
                            dtpDate.Enabled = false;
                        }
                        break;
                    case 4:
                        if (MobjClsBLLPurchase.DisplayPurchaseInvoiceInfo(iOrderID))
                        {
                            btnDocuments.Visible = true;
                            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                            dgvPurchase.Rows.Clear();
                            dgvLocationDetails.Rows.Clear();
                            txtPurchaseNo.Tag = MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID;
                            cboCompany.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID;
                            clsBLLLogin mobjClsBllLogin = new clsBLLLogin();
                            mobjClsBllLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue.ToInt32()));
                            if (cboCompany.SelectedValue == null)
                            {
                                DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID });
                                if (datTemp.Rows.Count > 0)
                                    cboCompany.Text = datTemp.Rows[0]["CompanyName"].ToString();
                            }
                            LoadCombos(5, null);
                            cboCompany.Enabled = false;
                            LoadCombos(13, "");
                            cboCurrency.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCurrencyID;
                            if (cboSupplier.SelectedValue == null)
                                cboSupplier.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVendorName;
                            AddToAddressMenu(MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID);
                            lblCreatedByText.Text = " Created By :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strEmployeeName;
                            lblCreatedDateValue.Text = "  Created Date :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strCreatedDate.ToDateTime().ToString("dd-MMM-yyyy");
                            MintVendorAddID = MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorAddID;
                            lblSupplierAddressName.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strAddressName + " Address";
                            DisplayAddress(Convert.ToInt32(cboSupplier.SelectedValue));
                            txtSupplierBillNo.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strPurchaseBillNo;

                            cboPaymentTerms.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intPaymentTermsID;
                            cboWarehouse.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intWarehouseID;
                            MobjClsBLLPurchase.PobjClsDTOPurchase.intTempWarehouseID = MobjClsBLLPurchase.PobjClsDTOPurchase.intWarehouseID;
                            string s = MobjClsBLLPurchase.PobjClsDTOPurchase.strRemarks;
                            cboOrderType.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType;
                            if (cboOrderType.SelectedValue == null)
                            {
                                DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "CommonOrderTypeReference", "OrderTypeID=" + MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType });
                                if (datTemp.Rows.Count > 0)
                                    cboOrderType.Text = datTemp.Rows[0]["OrderType"].ToString();
                            }
                            if ((Int32)cboOrderType.SelectedValue == (Int32)OperationOrderType.PInvoiceFromOrder)
                            {

                                cboOrderNo.Visible = true;
                                cboOrderNo.DataSource = null;
                                DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseOrderID,PurchaseOrderNo,OrderDate,DueDate", "InvPurchaseOrderMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) + "", "PurchaseOrderID", "PurchaseOrderNo" });
                                cboOrderNo.ValueMember = "PurchaseOrderID";
                                cboOrderNo.DisplayMember = "PurchaseOrderNo";
                                cboOrderNo.DataSource = dtCombo;

                                cboOrderNo.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseOrderID;
                                if (!ChkTGNR.Checked)
                                    ExtraQty.Visible = true;
                                else
                                    ExtraQty.Visible = false;
                                ReceivedQty.Visible = true;
                                OrderedQty.Visible = true;
                                MblnIsEditable = false;

                                this.ShowQuotationNo();
                            }
                            else
                            {
                                cboOrderNo.Visible = false;
                                cboOrderNo.DataSource = null;
                                this.lblOrder.Visible = false;
                                this.lblOrderNo.Visible = false;
                            }
                            cboOrderType.Enabled = false;
                            cboOrderNo.Enabled = false;
                            cboOrderNo.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseOrderID;
                            string s1 = MobjClsBLLPurchase.PobjClsDTOPurchase.strRemarks;
                            txtPurchaseNo.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strPurchaseNo;
                            dtpDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strDate);
                            dtpDueDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strDueDate);

                            cboCurrency.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCurrencyID;
                            txtRemarks.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strRemarks;
                            cboPurchaseAccount.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseAccountID;
                            //CboTStatus.Enabled = true;
                            ChkTGNR.Checked = MobjClsBLLPurchase.PobjClsDTOPurchase.blnGRNRequired;

                            if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PInvoiceCancelled)
                            {

                                txtDescription.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strDescription;
                                lblCreatedByText.Text += "        | Cancelled By :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strCancelledBy;
                                lblCreatedDateValue.Text += "        | Cancelled Date :" + MobjClsBLLPurchase.PobjClsDTOPurchase.strCancellationDate.ToDateTime().ToString("dd-MMM-yyyy");
                                lblDescription.Location = lblCriteria.Location;
                                txtDescription.Location = lblCriteriaText.Location;
                                txtDescription.Size = new Size(293, 67);
                            }
                            DisplayPurchaseOrderDetail(iOrderID);
                            if (!ChkTGNR.Checked)
                                DisplayLocationDetails();

                            txtSubTotal.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandAmount.ToString();

                            //cboDiscount.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intGrandDiscountID;
                            //txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountAmount.ToString();
                            cboDiscount.SelectedIndex = (MobjClsBLLPurchase.PobjClsDTOPurchase.blnGrandDiscount.ToBoolean() ? 0 : 1);
                            if (Convert.ToString(cboDiscount.SelectedItem) == "%")
                                txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountPercent.ToString();
                            else
                                txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountAmt.ToString();
                            txtTotalAmount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decNetAmount.ToString();
                            txtExpenseAmount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decExpenseAmount.ToString();
                            txtAdvPayment.Text = MobjClsBLLPurchase.GetAdvancePayment(Convert.ToInt64(cboOrderNo.SelectedValue)).ToString("F" + MintExchangeCurrencyScale);

                            lblPurchasestatus.Text = "";
                            SetPurchaseStatus();
                            ChkTGNR.Enabled = false;
                            btnAddExpense.Enabled = true;
                            dtpDate.Enabled = false;
                            //if (MobjclsBLLCommonUtility.FillCombos(new string[] { "IsNull(Sum(Amount),0) as TotalAmount", "STPayment", "OperationTypeID = "+(int)OperationType.PurchaseInvoice+" And ReferenceID = "+cboOrderNo.SelectedValue.ToInt64()+" PaymentTypeID <> " + (int)PaymentTypes.DirectExpense }).Rows[0]["TotalAmount"].ToDecimal() != 0)
                            cboPaymentTerms.Enabled = false;
                        }
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayOrderInfo() " + ex.Message);
                MObjLogs.WriteLog("Error in DisplayOrderInfo() " + ex.Message, 2);
                return false;
            }
        }

        private void DisplayPurchaseOrderDetail(Int64 iOrderID)
        {
            try
            {
                blnIsFromDisplay = true;
                dgvPurchase.Rows.Clear();
                DataTable DtPurOrderDetail = null;
                switch (MintFromForm)
                {
                    case 1:
                        DtPurOrderDetail = MobjClsBLLPurchase.DisplayPurchaseQuotationDetail(iOrderID);
                        if (DtPurOrderDetail.Rows.Count > 0)
                        {
                            LoadCombos(9, null);

                            for (int i = 0; i < DtPurOrderDetail.Rows.Count; i++)
                            {
                                dgvPurchase.RowCount = dgvPurchase.RowCount + 1;
                                dgvPurchase.Rows[i].Cells["ItemCode"].Value = DtPurOrderDetail.Rows[i]["ItemCode"];
                                dgvPurchase.Rows[i].Cells["ItemName"].Value = DtPurOrderDetail.Rows[i]["Itemname"];
                                //  dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["Quantity"];
                                dgvPurchase.Rows[i].Cells["ItemID"].Value = DtPurOrderDetail.Rows[i]["ItemID"];

                                FillComboColumn(Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]), 1);
                                dgvPurchase.Rows[i].Cells["Uom"].Value = DtPurOrderDetail.Rows[i]["UOMID"];
                                dgvPurchase.Rows[i].Cells["Uom"].Tag = DtPurOrderDetail.Rows[i]["UOMID"];
                                dgvPurchase.Rows[i].Cells["Uom"].Value = dgvPurchase.Rows[i].Cells["Uom"].FormattedValue;

                                int intUomScale = 0;
                                if (dgvPurchase[Uom.Index, i].Tag.ToInt32() != 0)
                                    intUomScale = MobjClsBLLPurchase.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPurchase[Uom.Index, i].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                                dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["Quantity"].ToDecimal().ToString("F" + intUomScale);

                                dgvPurchase.Rows[i].Cells["Rate"].Value = DtPurOrderDetail.Rows[i]["Rate"];
                                dgvPurchase.Rows[i].Cells["GrandAmount"].Value = DtPurOrderDetail.Rows[i]["GrandAmount"];
                                //FillDiscountColumn(i, false);
                                //if (DtPurOrderDetail.Rows[i]["IsDiscountPercentage"] != DBNull.Value)
                                //{
                                //    dgvPurchase.Rows[i].Cells["DiscountAmt"].Value = (Convert.ToBoolean(DtPurOrderDetail.Rows[i]["IsDiscountPercentage"]) ? DtPurOrderDetail.Rows[i]["DiscountPercentage"].ToDecimal() / 100 * DtPurOrderDetail.Rows[i]["GrandAmount"].ToDecimal() : DtPurOrderDetail.Rows[i]["DiscountAmount"].ToDecimal()); ;
                                //    dgvPurchase.Rows[i].Cells["DiscountAmount"].Value = (Convert.ToBoolean(DtPurOrderDetail.Rows[i]["IsDiscountPercentage"]) ? DtPurOrderDetail.Rows[i]["DiscountPercentage"].ToDecimal() : DtPurOrderDetail.Rows[i]["DiscountAmount"].ToDecimal()); ;
                                //    dgvPurchase.Rows[i].Cells["Discount"].Value = (Convert.ToBoolean(DtPurOrderDetail.Rows[i]["IsDiscountPercentage"]) ? "%" : "Amt");
                                //}
                                if (DtPurOrderDetail.Rows[i]["IsDiscountPercentage"] != DBNull.Value)
                                {
                                    dgvPurchase.Rows[i].Cells["DiscountAmt"].Value = (Convert.ToBoolean(DtPurOrderDetail.Rows[i]["IsDiscountPercentage"]) ? DtPurOrderDetail.Rows[i]["DiscountPercentage"].ToDecimal() / 100 * DtPurOrderDetail.Rows[i]["GrandAmount"].ToDecimal() : DtPurOrderDetail.Rows[i]["DiscountAmount"].ToDecimal()); ;
                                    dgvPurchase.Rows[i].Cells["DiscountAmount"].Value = (Convert.ToBoolean(DtPurOrderDetail.Rows[i]["IsDiscountPercentage"]) ? DtPurOrderDetail.Rows[i]["DiscountPercentage"].ToDecimal() : DtPurOrderDetail.Rows[i]["DiscountAmount"].ToDecimal()); ;
                                    dgvPurchase.Rows[i].Cells["Discount"].Value = (Convert.ToBoolean(DtPurOrderDetail.Rows[i]["IsDiscountPercentage"]) ? "%" : "Amt");
                                }
                                
                                dgvPurchase.Rows[i].Cells["NetAmount"].Value = DtPurOrderDetail.Rows[i]["NetAmount"];
                            }
                        }
                        DtPurOrderDetail = null;
                        break;
                    case 2:
                        DtPurOrderDetail = MobjClsBLLPurchase.DisplayPurchaseOrderDetail(iOrderID);
                        if (DtPurOrderDetail.Rows.Count > 0)
                        {
                            LoadCombos(9, null); LoadCombos(12, null);

                            for (int i = 0; i < DtPurOrderDetail.Rows.Count; i++)
                            {
                                dgvPurchase.RowCount = dgvPurchase.RowCount + 1;
                                dgvPurchase.Rows[i].Cells["ItemID"].Value = DtPurOrderDetail.Rows[i]["ItemID"];
                                dgvPurchase.Rows[i].Cells["ItemCode"].Value = DtPurOrderDetail.Rows[i]["ItemCode"];
                                dgvPurchase.Rows[i].Cells["ItemName"].Value = DtPurOrderDetail.Rows[i]["Itemname"];
                                // dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["Quantity"];
                                dgvPurchase.Rows[i].Cells["ItemID"].Value = DtPurOrderDetail.Rows[i]["ItemID"];
                                FillComboColumn(Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]), 1);
                                dgvPurchase.Rows[i].Cells["Uom"].Value = DtPurOrderDetail.Rows[i]["UOMID"];
                                dgvPurchase.Rows[i].Cells["Uom"].Tag = DtPurOrderDetail.Rows[i]["UOMID"];
                                dgvPurchase.Rows[i].Cells["Uom"].Value = dgvPurchase.Rows[i].Cells["Uom"].FormattedValue;

                                int intUomScale = 0;
                                if (dgvPurchase[Uom.Index, i].Tag.ToInt32() != 0)
                                    intUomScale = MobjClsBLLPurchase.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPurchase[Uom.Index, i].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                                dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["Quantity"].ToDecimal().ToString("F" + intUomScale);
                                dgvPurchase.Rows[i].Cells["Rate"].Value = DtPurOrderDetail.Rows[i]["Rate"];
                                dgvPurchase.Rows[i].Cells["GrandAmount"].Value = DtPurOrderDetail.Rows[i]["GrandAmount"];
                                //FillDiscountColumn(i, false);
                                if (DtPurOrderDetail.Rows[i]["IsDiscountPercentage"] != DBNull.Value)
                                {
                                    dgvPurchase.Rows[i].Cells["DiscountAmt"].Value = (Convert.ToBoolean(DtPurOrderDetail.Rows[i]["IsDiscountPercentage"]) ? DtPurOrderDetail.Rows[i]["DiscountPercentage"].ToDecimal() / 100 * DtPurOrderDetail.Rows[i]["GrandAmount"].ToDecimal() : DtPurOrderDetail.Rows[i]["DiscountAmount"].ToDecimal()); ;
                                    dgvPurchase.Rows[i].Cells["DiscountAmount"].Value = (Convert.ToBoolean(DtPurOrderDetail.Rows[i]["IsDiscountPercentage"]) ? DtPurOrderDetail.Rows[i]["DiscountPercentage"].ToDecimal() : DtPurOrderDetail.Rows[i]["DiscountAmount"].ToDecimal()); ;
                                    dgvPurchase.Rows[i].Cells["Discount"].Value = (Convert.ToBoolean(DtPurOrderDetail.Rows[i]["IsDiscountPercentage"]) ? "%" : "Amt");
                                }


                                dgvPurchase.Rows[i].Cells["NetAmount"].Value = DtPurOrderDetail.Rows[i]["NetAmount"];
                            }
                        }
                        if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.POrderFromGRN)
                        {
                            if (lblStatusText.Text == "Cancelled")
                            {
                                clbGRN.Enabled = false;
                            }
                            else
                            {
                                clbGRN.Enabled = true;
                            }
                        }
                        DtPurOrderDetail = null;
                        break;

                    case 3:

                        DtPurOrderDetail = MobjClsBLLPurchase.DisplayPurchaseGRNDetail(iOrderID);
                        dtTempDetails = new DataTable();
                        dtTempDetails = DtPurOrderDetail;
                        DataTable datReferenceDetails = new DataTable();
                        if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromInvoice)
                            datReferenceDetails = MobjClsBLLPurchase.DisplayPurchaseInvoiceDetail(cboOrderNo.SelectedValue.ToInt64());
                        else if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromOrder)
                            datReferenceDetails = MobjClsBLLPurchase.DisplayPurchaseOrderGRNDetail(cboOrderNo.SelectedValue.ToInt64());
                        else if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromTransfer)
                            datReferenceDetails = MobjClsBLLPurchase.GetStockTransferDetails(cboOrderNo.SelectedValue.ToInt64());
                        if (DtPurOrderDetail.Rows.Count > 0)
                        {
                            LoadCombos(9, null); LoadCombos(12, null);
                            dtItemDetails.Rows.Clear();
                            for (int i = 0; i < DtPurOrderDetail.Rows.Count; i++)
                            {

                                dgvPurchase.RowCount = dgvPurchase.RowCount + 1;
                                dgvPurchase.Rows[i].Cells["ItemCode"].Value = DtPurOrderDetail.Rows[i]["ItemCode"];
                                dgvPurchase.Rows[i].Cells["ItemName"].Value = DtPurOrderDetail.Rows[i]["Itemname"];
                                if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromInvoice)
                                {
                                    datReferenceDetails.DefaultView.RowFilter = "ItemID = " + DtPurOrderDetail.Rows[i]["ItemID"].ToInt32();
                                    dgvPurchase.Rows[i].Cells["OrderedQty"].Value = datReferenceDetails.DefaultView.ToTable().Rows[0]["ReceivedQuantity"].ToString();// +" " + datReferenceDetails.Rows[0]["UOM"].ToString(); ;
                                }
                                else if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromOrder)
                                {
                                    datReferenceDetails.DefaultView.RowFilter = "ItemID = " + DtPurOrderDetail.Rows[i]["ItemID"].ToInt32();
                                    dgvPurchase.Rows[i].Cells["OrderedQty"].Value = datReferenceDetails.DefaultView.ToTable().Rows[0]["ReceivedQuantity"].ToString();// +" " + datReferenceDetails.Rows[0]["UOM"].ToString(); ;
                                }
                                else if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromTransfer)
                                {
                                    datReferenceDetails.DefaultView.RowFilter = "ItemID = " + DtPurOrderDetail.Rows[i]["ItemID"].ToInt32() + " And BatchID = " + DtPurOrderDetail.Rows[i]["BatchID"].ToInt64();
                                    dgvPurchase.Rows[i].Cells["OrderedQty"].Value = datReferenceDetails.DefaultView.ToTable().Rows[0]["Quantity"].ToString();// +" " + datReferenceDetails.Rows[0]["UOM"].ToString(); ;
                                }

                                // dgvPurchase.Rows[i].Cells["ExtraQty"].Value = DtPurOrderDetail.Rows[i]["ExtraQuantity"];
                                //  dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["ReceivedQuantity"];
                                if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromInvoice || cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromOrder)
                                    dgvPurchase.Rows[i].Cells["ReceivedQty"].Value = MobjClsBLLPurchase.GetReceivedQty(Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]), Convert.ToInt64(cboOrderNo.SelectedValue), true);
                                //else if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNItemReturnType)
                                //  dgvPurchase.Rows[i].Cells["ReceivedQty"].Value = MobjClsBLLPurchase.GetAlreadyReturnedQty(cboOrderNo.SelectedValue.ToInt64(), DtPurOrderDetail.Rows[i]["ItemID"].ToInt32(), DtPurOrderDetail.Rows[i]["BatchID"].ToInt64()).ToString() +" "+datReferenceDetails.DefaultView.ToTable().Rows[0]["UOM"].ToString();

                                dgvPurchase.Rows[i].Cells["ItemID"].Value = DtPurOrderDetail.Rows[i]["ItemID"];
                                FillComboColumn(Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]), 1);
                                dgvPurchase.Rows[i].Cells["Uom"].Value = DtPurOrderDetail.Rows[i]["UOMID"];
                                dgvPurchase.Rows[i].Cells["Uom"].Tag = DtPurOrderDetail.Rows[i]["UOMID"];
                                dgvPurchase.Rows[i].Cells["Uom"].Value = dgvPurchase.Rows[i].Cells["Uom"].FormattedValue;

                                int intUomScale = 0;
                                if (dgvPurchase[Uom.Index, i].Tag.ToInt32() != 0)
                                    intUomScale = MobjClsBLLPurchase.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPurchase[Uom.Index, i].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                                dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["ReceivedQuantity"].ToDecimal().ToString("F" + intUomScale);
                                dgvPurchase.Rows[i].Cells["ExtraQty"].Value = DtPurOrderDetail.Rows[i]["ExtraQuantity"].ToDecimal().ToString("F" + intUomScale);

                                if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromInvoice)
                                {
                                    dgvPurchase.Rows[i].Cells["Rate"].Value = datReferenceDetails.DefaultView.ToTable().Rows[0]["Rate"].ToDecimal();
                                    dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = datReferenceDetails.DefaultView.ToTable().Rows[0]["PurchaseRate"].ToDecimal();
                                }
                                else if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromOrder)
                                {
                                    dgvPurchase.Rows[i].Cells["Rate"].Value = datReferenceDetails.DefaultView.ToTable().Rows[0]["Rate"].ToDecimal();
                                    dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = datReferenceDetails.DefaultView.ToTable().Rows[0]["PurchaseRate"].ToDecimal();
                                }
                                else if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromTransfer)
                                {
                                    dgvPurchase.Rows[i].Cells["Rate"].Value = datReferenceDetails.DefaultView.ToTable().Rows[0]["Rate"].ToDecimal();
                                    dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = datReferenceDetails.DefaultView.ToTable().Rows[0]["Rate"].ToDecimal();
                                }
                                int iGRNTypeID = Convert.ToInt32(cboOrderType.SelectedValue);



                                dgvPurchase.Rows[i].Cells["ItemID"].Value = DtPurOrderDetail.Rows[i]["ItemID"];
                                dgvPurchase.Rows[i].Cells["Batchnumber"].Value = DtPurOrderDetail.Rows[i]["BatchNo"];
                                dgvPurchase.Rows[i].Cells["BatchID"].Value = DtPurOrderDetail.Rows[i]["BatchID"];
                                if (DtPurOrderDetail.Rows[i]["ExpiryDate"] != DBNull.Value)
                                    dgvPurchase.Rows[i].Cells["ExpiryDate"].Value = Convert.ToDateTime(DtPurOrderDetail.Rows[i]["ExpiryDate"]);

                                dgvPurchase.Rows[i].Cells["Uom"].ReadOnly = true;

                                DataRow drItemDetailsRow = dtItemDetails.NewRow();
                                drItemDetailsRow["ItemID"] = DtPurOrderDetail.Rows[i]["ItemID"].ToInt32();
                                drItemDetailsRow["ItemCode"] = DtPurOrderDetail.Rows[i]["ItemCode"].ToString();
                                drItemDetailsRow["ItemName"] = DtPurOrderDetail.Rows[i]["ItemName"].ToString();
                                drItemDetailsRow["BatchNo"] = DtPurOrderDetail.Rows[i]["BatchNo"].ToString();
                                drItemDetailsRow["UomID"] = DtPurOrderDetail.Rows[i]["UOMID"].ToInt32();
                                dtItemDetails.Rows.Add(drItemDetailsRow);
                            }
                        }
                        DtPurOrderDetail = null;
                        break;
                    case 4:
                        DtPurOrderDetail = MobjClsBLLPurchase.DisplayPurchaseInvoiceDetail(iOrderID);
                        dtTempDetails = new DataTable();
                        dtTempDetails = DtPurOrderDetail;
                        if (DtPurOrderDetail.Rows.Count > 0)
                        {
                            LoadCombos(9, null); LoadCombos(12, null);
                            dtItemDetails.Rows.Clear();
                            for (int i = 0; i < DtPurOrderDetail.Rows.Count; i++)
                            {
                                dgvPurchase.RowCount = dgvPurchase.RowCount + 1;
                                dgvPurchase.Rows[i].Cells["ItemCode"].Value = DtPurOrderDetail.Rows[i]["ItemCode"];
                                dgvPurchase.Rows[i].Cells["ItemName"].Value = DtPurOrderDetail.Rows[i]["Itemname"];
                                dgvPurchase.Rows[i].Cells["OrderedQty"].Value = DtPurOrderDetail.Rows[i]["OrderedQuantity"];
                                //  dgvPurchase.Rows[i].Cells["ExtraQty"].Value = DtPurOrderDetail.Rows[i]["ExtraQuantity"];
                                //   dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["ReceivedQuantity"];
                                if (cboOrderNo.SelectedValue != null)
                                    dgvPurchase.Rows[i].Cells["ReceivedQty"].Value = MobjClsBLLPurchase.GetReceivedQty(Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]), Convert.ToInt64(cboOrderNo.SelectedValue), false);
                                dgvPurchase.Rows[i].Cells["ItemID"].Value = DtPurOrderDetail.Rows[i]["ItemID"];
                                FillComboColumn(Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]), 1);
                                dgvPurchase.Rows[i].Cells["Uom"].Value = DtPurOrderDetail.Rows[i]["UOMID"];
                                dgvPurchase.Rows[i].Cells["Uom"].Tag = DtPurOrderDetail.Rows[i]["UOMID"];
                                dgvPurchase.Rows[i].Cells["Uom"].Value = dgvPurchase.Rows[i].Cells["Uom"].FormattedValue;

                                int intUomScale = 0;
                                if (dgvPurchase[Uom.Index, i].Tag.ToInt32() != 0)
                                    intUomScale = MobjClsBLLPurchase.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPurchase[Uom.Index, i].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                                dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["ReceivedQuantity"].ToDecimal().ToString("F" + intUomScale);
                                dgvPurchase.Rows[i].Cells["ExtraQty"].Value = DtPurOrderDetail.Rows[i]["ExtraQuantity"].ToDecimal().ToString("F" + intUomScale);

                                int iGRNTypeID = Convert.ToInt32(cboOrderType.SelectedValue);
                                dgvPurchase.Rows[i].Cells["Rate"].Value = DtPurOrderDetail.Rows[i]["Rate"];
                                dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = DtPurOrderDetail.Rows[i]["PurchaseRate"];



                                dgvPurchase.Rows[i].Cells["ItemID"].Value = DtPurOrderDetail.Rows[i]["ItemID"];
                                dgvPurchase.Rows[i].Cells["Batchnumber"].Value = DtPurOrderDetail.Rows[i]["BatchNo"];
                                if (DtPurOrderDetail.Rows[i]["ExpiryDate"] != null && DtPurOrderDetail.Rows[i]["ExpiryDate"] != DBNull.Value)
                                    dgvPurchase.Rows[i].Cells["ExpiryDate"].Value = Convert.ToDateTime(DtPurOrderDetail.Rows[i]["ExpiryDate"]);
                                if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.PInvoiceFromOrder)
                                    dgvPurchase.Rows[i].Cells["Uom"].ReadOnly = true;
                                dgvPurchase.Rows[i].Cells["GrandAmount"].Value = DtPurOrderDetail.Rows[i]["GrandAmount"];

                                //FillDiscountColumn(i, false);
                                //if (DtPurOrderDetail.Rows[i]["DiscountID"] != DBNull.Value && Convert.ToInt32(DtPurOrderDetail.Rows[i]["DiscountID"]) != 0)
                                //{
                                //    dgvPurchase.Rows[i].Cells["Discount"].Value = DtPurOrderDetail.Rows[i]["DiscountID"];
                                //    dgvPurchase.Rows[i].Cells["Discount"].Tag = DtPurOrderDetail.Rows[i]["DiscountID"];
                                //    dgvPurchase.Rows[i].Cells["Discount"].Value = dgvPurchase.Rows[i].Cells["Discount"].FormattedValue;
                                //}
                                //dgvPurchase.Rows[i].Cells["DiscountAmount"].Value = DtPurOrderDetail.Rows[i]["DiscountAmount"];
                                if (DtPurOrderDetail.Rows[i]["IsDiscountPercentage"] != DBNull.Value)
                                {
                                    dgvPurchase.Rows[i].Cells["DiscountAmt"].Value = (Convert.ToBoolean(DtPurOrderDetail.Rows[i]["IsDiscountPercentage"]) ? DtPurOrderDetail.Rows[i]["DiscountPercentage"].ToDecimal() / 100 * DtPurOrderDetail.Rows[i]["GrandAmount"].ToDecimal() : DtPurOrderDetail.Rows[i]["DiscountAmount"].ToDecimal()); ;
                                    dgvPurchase.Rows[i].Cells["DiscountAmount"].Value = (Convert.ToBoolean(DtPurOrderDetail.Rows[i]["IsDiscountPercentage"]) ? DtPurOrderDetail.Rows[i]["DiscountPercentage"].ToDecimal() : DtPurOrderDetail.Rows[i]["DiscountAmount"].ToDecimal()); ;
                                    dgvPurchase.Rows[i].Cells["Discount"].Value = (Convert.ToBoolean(DtPurOrderDetail.Rows[i]["IsDiscountPercentage"]) ? "%" : "Amt");
                                }
                                dgvPurchase.Rows[i].Cells["NetAmount"].Value = DtPurOrderDetail.Rows[i]["NetAmount"];
                                if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.PInvoiceFromOrder)
                                    dgvPurchase.Rows[i].Cells[IsAutofilled.Index].Value = 1;

                                DataRow drItemDetailsRow = dtItemDetails.NewRow();
                                drItemDetailsRow["ItemID"] = DtPurOrderDetail.Rows[i]["ItemID"].ToInt32();
                                drItemDetailsRow["ItemCode"] = DtPurOrderDetail.Rows[i]["ItemCode"].ToString();
                                drItemDetailsRow["ItemName"] = DtPurOrderDetail.Rows[i]["ItemName"].ToString();
                                drItemDetailsRow["BatchNo"] = DtPurOrderDetail.Rows[i]["BatchNo"].ToString();
                                drItemDetailsRow["UomID"] = DtPurOrderDetail.Rows[i]["UOMID"].ToInt32();
                                dtItemDetails.Rows.Add(drItemDetailsRow);
                            }
                        }
                        DtPurOrderDetail = null;
                        break;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayPurchaseOrderDetail() " + ex.Message);
                MObjLogs.WriteLog("Error in DisplayPurchaseOrderDetail() " + ex.Message, 2);
            }
            blnIsFromDisplay = false;
        }

        private bool DisplayAddress(int iVendorID)
        {
            try
            {
                txtSupplierAddress.Text = MobjClsBLLPurchase.DisplayAddressInformation(MintVendorAddID);
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayAddress() " + ex.Message);
                MObjLogs.WriteLog("Error in DisplayAddress() " + ex.Message, 2);
                return false;
            }
        }

        private bool IsStockAdjustmentDone()
        {
            DataTable datDetails = new DataTable();
            if (MintFromForm == 3)
            {
                datDetails = MobjClsBLLPurchase.DisplayPurchaseGRNDetail(MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID);
                foreach (DataRow dr in datDetails.Rows)
                {
                    if (MobjClsBLLPurchase.IsStockAdjustmentDone(dr["ItemID"].ToInt32(), dr["BatchID"].ToInt64(), cboWarehouse.SelectedValue.ToInt32()))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 969, out MmessageIcon).Replace("invoice", "GRN");
                        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPurchase.Enabled = true;
                        tcPurchase.SelectedTab = tpItemDetails;
                        dgvPurchase.Focus();
                        dgvPurchase.CurrentCell = dgvPurchase["ItemCode", 0];
                        return true;
                    }
                }
            }
            else if (MintFromForm == 4 && !ChkTGNR.Checked)
            {
                datDetails = MobjClsBLLPurchase.DisplayPurchaseInvoiceDetail(MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID);
                foreach (DataRow dr in datDetails.Rows)
                {
                    if (MobjClsBLLPurchase.IsStockAdjustmentDone(dr["ItemID"].ToInt32(), dr["BatchID"].ToInt64(), cboWarehouse.SelectedValue.ToInt32()))
                    {
                        if (MobjClsBLLPurchase.IsStockAdjustmentDone(dr["ItemID"].ToInt32(), dr["BatchID"].ToInt64(), cboWarehouse.SelectedValue.ToInt32()))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 969, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            dgvPurchase.Focus();
                            dgvPurchase.CurrentCell = dgvPurchase["ItemCode", 0];
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool LoadCombos(int intType, string condition)
        {
            // function for loading combo
            // 0 - For Loading All Combo
            // 1 - For Loading CboCompanyName
            // 2 - For Loading CboSearchCompany
            // 3 - For Loading CboVendor
            // 4 - For Loading CboSearchVendor
            // 5 - For Loading CboWareHouse
            // 6 - For Loading CboPayment Terms
            // 7 - For Loading CboTax
            // 8 - For Loading CboDiscount
            // 9 - For Loading UOM ComboBox Column
            // 10 - For Loading Tax ComboBox Column
            // 11 - For Loading Reason ComboBox Column
            // 12 - For Loading Discount ComboBox Column 
            // 13 - For Loading CboCurrency
            // 14 - For Loading CboEmployee
            // 15 - For Loading CboSStatus
            // 16 - For Loading CboTStatus
            // 17 - For Loading CboCarrier
            // 18 - For Loading OrderType
            // 19 - For Loading Container Type
            // 20 - For Loading Inco Terms

            if (string.IsNullOrEmpty(condition))
                condition = string.Empty;
            else
                condition = " And " + condition;
            try
            {
                clsBLLPermissionSettings objBllPermissionSettings = new clsBLLPermissionSettings();

                DataTable datCombos = new DataTable();
                //bool blnIsEnabled = false;

                if (intType == 0 || intType == 1)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", " CompanyID= " + ClsCommonSettings.LoginCompanyID });
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;
                }
                if (intType == 0 || intType == 2)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", " CompanyID= " + ClsCommonSettings.LoginCompanyID });
                    cboSCompany.ValueMember = "CompanyID";
                    cboSCompany.DisplayMember = "CompanyName";
                    cboSCompany.DataSource = datCombos;
                }
                if (intType == 0 || intType == 3)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "V.VendorID,V.VendorName", "InvVendorInformation V INNER JOIN InvVendorCompanyDetails VC ON V.VendorID = VC.VendorID ", " V.VendorTypeID = " + (int)VendorType.Supplier + " AND VC.CompanyID = " + ClsCommonSettings.CompanyID + "  and V.StatusID=" + (int)VendorStatus.SupplierActive + "" });
                    cboSupplier.ValueMember = "VendorID";
                    cboSupplier.DisplayMember = "VendorName";
                    cboSupplier.DataSource = datCombos;
                }

                if (intType == 0 || intType == 4)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "V.VendorID,V.VendorName", "InvVendorInformation V INNER JOIN InvVendorCompanyDetails VC ON V.VendorID = VC.VendorID ", " V.VendorTypeID = " + (int)VendorType.Supplier + " AND VC.CompanyID = " + ClsCommonSettings.CompanyID + "  and V.StatusID=" + (int)VendorStatus.SupplierActive + "" });
                    DataRow dr = datCombos.NewRow();
                    dr["VendorID"] = -2;
                    dr["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(dr, 0);
                    cboSSupplier.ValueMember = "VendorID";
                    cboSSupplier.DisplayMember = "VendorName";
                    cboSSupplier.DataSource = datCombos;
                }

                if (intType == 0 || intType == 5)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "Distinct InvWarehouse.WarehouseID,WarehouseName", "InvWarehouse INNER JOIN InvWarehouseCompanyDetails WC ON InvWarehouse.WarehouseID=WC.WarehouseID", "WC.CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + " And IsActive = 1" });
                    cboWarehouse.ValueMember = "WarehouseID";
                    cboWarehouse.DisplayMember = "WarehouseName";
                    cboWarehouse.DataSource = datCombos;
                }

                if (intType == 0 || intType == 6)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "PaymentTermsID,TermsName", "InvPaymentTerms", "" });
                    cboPaymentTerms.ValueMember = "PaymentTermsID";
                    cboPaymentTerms.DisplayMember = "TermsName";
                    cboPaymentTerms.DataSource = datCombos;
                    cboPaymentTerms.SelectedValue = (int)PaymentTerms.Cash;
                }

                //if (intType == 0 || intType == 8)
                //{
                //    datCombos = null;
               
                //    if (MblnIsEditable)
                //    {
                //        decimal decMaxSlab = 0;
                //        datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "IsNull(Max(MinSlab),0) as MinSlab", "InvDiscountReference", "DiscountMode in ('N') And IsSaleType = 'False' And IsActive = 1 and IsDiscountForAmount=1 And (IsPeriodApplicable = 0 or (IsPeriodApplicable = 1 And '" + dtpDate.Value.ToString("dd-MMM-yyyy") + "' >= convert(datetime,convert(varchar,PeriodFrom,106)) And '" + dtpDate.Value.ToString("dd-MMM-yyyy") + "' <= convert(datetime,convert(varchar,PeriodTo,106)))) And (IsSlabApplicable = 0 Or (IsSlabApplicable = 1 And " + txtSubTotal.Text.ToDecimal() + " >= MinSlab)) And CurrencyID = " + cboCurrency.SelectedValue.ToInt32() + " " });
                //        if (datCombos.Rows.Count > 0)
                //            decMaxSlab = datCombos.Rows[0]["MinSlab"].ToDecimal();
                //        datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "DiscountID,DiscountShortName", "InvDiscountReference", "(DiscountMode in ('N') And IsSaleType = 'False' And IsActive = 1 and IsDiscountForAmount=1 And (IsPeriodApplicable = 0 or (IsPeriodApplicable = 1 And '" + dtpDate.Value.ToString("dd-MMM-yyyy") + "' >= convert(datetime,convert(varchar,PeriodFrom,106)) And '" + dtpDate.Value.ToString("dd-MMM-yyyy") + "' <= convert(datetime,convert(varchar,PeriodTo,106)))) And (IsSlabApplicable = 0 Or (IsSlabApplicable = 1 And " + txtSubTotal.Text.ToDecimal() + " >= MinSlab) And MinSlab >= " + decMaxSlab + " And CurrencyID = " + cboCurrency.SelectedValue.ToInt32() + ") And case PercentOrAmount When 0 Then CurrencyID Else " + cboCurrency.SelectedValue.ToInt32() + " End = " + cboCurrency.SelectedValue.ToInt32() + ") Or ( IsSaleType = 'False' And IsPredefined = 1)", "DiscountID", "DiscountShortName" });
                //    }
                //    else
                //    {
                //        datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "DiscountID,DiscountShortName", "InvDiscountReference", "(DiscountMode in ('N') And IsSaleType = 'False' And IsActive = 1 and IsDiscountForAmount=1) Or ( IsSaleType = 'False' And IsPredefined = 1) ", "DiscountID", "DiscountShortName" });
                //    }
                //    cboDiscount.ValueMember = "DiscountID";
                //    cboDiscount.DisplayMember = "DiscountShortName";
                //    DataRow dr = datCombos.NewRow();
                //    dr["DiscountID"] = 0;
                //    dr["DiscountShortName"] = "No Discount";
                //    datCombos.Rows.InsertAt(dr, 0);
                //    int intTag = cboDiscount.SelectedValue.ToInt32();
                //    string strDiscountAmount = txtDiscount.Text;

                //    cboDiscount.DataSource = datCombos;
                //    cboDiscount.SelectedValue = intTag;

                //    if (intTag == (int)PredefinedDiscounts.DefaultPurchaseDiscount)
                //        txtDiscount.Text = strDiscountAmount;
                //}

                //if (intType == 0 || intType == 13)
                //{
                //    datCombos = null;
                //    datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "distinct CR.CurrencyID,CR.CurrencyName", "CurrencyReference CR INNER JOIN CurrencyDetails CD ON CR.CurrencyID=CD.CurrencyID", "CD.CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) });
                //    cboCurrency.ValueMember = "CurrencyID";
                //    cboCurrency.DisplayMember = "CurrencyName";
                //    cboCurrency.DataSource = datCombos;
                //}
                if (intType == 0 || intType == 13)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "distinct CR.CurrencyID,CR.CurrencyName", "CurrencyReference CR INNER JOIN CurrencyDetails CD ON CR.CurrencyID=CD.CurrencyID INNER JOIN InvVendorInformation VI   on VI.CurrencyID = CR.CurrencyID ", "CD.CompanyID=" + ClsCommonSettings.CompanyID + "and  (  VI.VendorID =" + cboSupplier.SelectedValue.ToInt32() + " or " + cboSupplier.SelectedValue.ToInt32() + " = 0 )" });
                    cboCurrency.ValueMember = "CurrencyID";
                    cboCurrency.DisplayMember = "CurrencyName";
                    cboCurrency.DataSource = datCombos;
                }
                if (intType == 0 || intType == 15)
                {
                    datCombos = null;
                    string strCondition = "OperationTypeID = ";
                    switch (MintFromForm)
                    {
                        case 1:
                            strCondition += "" + (int)OperationType.PurchaseQuotation + "";
                            break;
                        case 2:
                            strCondition += "" + (int)OperationType.PurchaseOrder + "";
                            break;
                        case 3:
                            strCondition += "" + (int)OperationType.GRN + "";
                            break;
                        case 4:
                            strCondition += "" + (int)OperationType.PurchaseInvoice + "";
                            break;
                    }
                    datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", strCondition });
                    DataRow dr = datCombos.NewRow();
                    dr["StatusID"] = -2;
                    dr["Status"] = "ALL";
                    datCombos.Rows.InsertAt(dr, 0);
                    cboSStatus.ValueMember = "StatusID";
                    cboSStatus.DisplayMember = "Status";
                    cboSStatus.DataSource = datCombos;
                }

                if (intType == 0 || intType == 18)
                {
                    datCombos = null;
                    switch (MintFromForm)
                    {
                        case 1: // Purchase Quotation

                            datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "OrderTypeID,OrderType", "CommonOrderTypeReference", "OperationTypeID = " + (int)OperationType.PurchaseQuotation + "" });

                            cboOrderType.ValueMember = "OrderTypeID";
                            cboOrderType.DisplayMember = "OrderType";
                            cboOrderType.DataSource = datCombos;
                            cboOrderType.SelectedValue = (Int32)OperationOrderType.PQuotationDirect;
                            break;
                        case 2: // Purchase Order

                            datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "OrderTypeID,OrderType", "CommonOrderTypeReference", "OperationTypeID = " + (int)OperationType.PurchaseOrder + "" });

                            cboOrderType.ValueMember = "OrderTypeID";
                            cboOrderType.DisplayMember = "OrderType";
                            cboOrderType.DataSource = datCombos;
                            cboOrderType.SelectedValue = (int)OperationOrderType.POrderDirect;
                            break;
                        case 3: // GRN
                            //cboOrderType.Text = "";

                            datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "OrderTypeID,OrderType", "CommonOrderTypeReference", "OperationTypeID = " + (int)OperationType.GRN + "" });


                            cboOrderType.ValueMember = "OrderTypeID";
                            cboOrderType.DisplayMember = "OrderType";
                            cboOrderType.DataSource = datCombos;
                            cboOrderType.SelectedValue = (int)OperationOrderType.GRNFromInvoice;

                            break;
                        case 4: // Purchase invoice

                            datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "OrderTypeID,OrderType", "CommonOrderTypeReference", "OperationTypeID = " + (int)OperationType.PurchaseInvoice + "" });

                            cboOrderType.ValueMember = "OrderTypeID";
                            cboOrderType.DisplayMember = "OrderType";
                            cboOrderType.DataSource = datCombos;
                            cboOrderType.SelectedValue = (int)OperationOrderType.PInvoiceDirect;
                            break;
                    }

                }
                //if (intType == 0 || intType == 19)
                //{
                //    datCombos = null;
                //    datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "ContainerTypeID,Description", "STContainerTypeReference", "" });
                //    cboContainerType.ValueMember = "ContainerTypeID";
                //    cboContainerType.DisplayMember = "Description";
                //    cboContainerType.DataSource = datCombos;
                //}
                //if (intType == 0 || intType == 20)
                //{
                //    datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "IncoTermsID,Description", "STIncoTermsReference", "" }); // For Inco Terms in PQ
                //    cboIncoTerms.ValueMember = "IncoTermsID";
                //    cboIncoTerms.DisplayMember = "Description";
                //    cboIncoTerms.DataSource = datCombos;
                //}

                if (intType == 0 || intType == 21)
                {

                    datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "AccountID,AccountName", "AccAccountMaster", "AccountGroupID=33" }); // For Tax Scheme
                    DataRow dr = datCombos.NewRow();
                    dr[0] = 0;
                    dr[1] = "None";
                    datCombos.Rows.InsertAt(dr, 0);
                    cboTaxScheme.ValueMember = "AccountID";
                    cboTaxScheme.DisplayMember = "AccountName";
                    cboTaxScheme.DataSource = datCombos;
                }

                MObjLogs.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                MObjLogs.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        private void ArrangeControls()
        {
            //lblOrder, lblOrderNo, lblOrder1, lblOrderNo1  : used for Purchase Invoice and Grn
            //In Purchase invoice, to show Quotation No, if invoiced from any Purchase order with Quotaion
            //In Grn, to show Order No and Quotation No, if invoice has them.
            lblOrder.Visible = false;
            lblOrderNo.Visible = false;
            lblOrder1.Visible = false;
            lblOrderNo1.Visible = false;

            switch (MintFromForm)
            {
                case 1: // Purchase Quotation
                    intTempMenuID = (int)eMenuID.PurchaseQuotation;
                    intTempCompany = (int)ControlOperationType.PurQuoCompany;
                    intTempEmployee = (int)ControlOperationType.PurQuoEmployee;
                    strTempTableName = "InvPurchaseQuotationMaster";
                    txtSupplierAddress.Visible = true;
                    lblOrderType.Visible = true;
                    cboOrderType.Visible = true;
                    cboOrderNo.Visible = true;

                    lblOrderType.Top = lblCompany.Top + 25;
                    cboOrderType.Top = cboCompany.Top + 25;
                    cboOrderNo.Top = cboOrderType.Top;
                    cboCurrency.Top = cboOrderType.Top + 25;
                    lblCurrency.Top = btnCurrency.Top = cboCurrency.Top;

                    lblPurchaseNo.Text = "Quotation No";
                    lblSPurchaseNo.Text = "Quotation No";
                    lblOrderType.Text = "Quotation Type";

                    SetPurchaseGridWidth();
                    btnDocuments.Visible = true;

                    bnExpense.Visible = true;
                    btnAddExpense.Enabled = txtExpenseAmount.Enabled = lblExpenseAmount.Enabled = true;

                    //lblIncoTerms.Visible =  cboIncoTerms.Visible = btnIncoTerms.Visible = true;
                    //lblLeadTime.Visible =  txtLeadTime.Visible = btnContainerType.Visible = true;
                    //lblProductionDate.Visible =  dtpProductionDate.Visible = true;
                    //lblTransShipmentDate.Visible =  dtpTrnsShipmentDate.Visible = true;
                    //lblClearenceDate.Visible =  dtpClearenceDate.Visible = true;
                    //lblContainerType.Visible = cboContainerType.Visible = true;
                    //lblNoOfContainers.Visible = txtNoOfContainers.Visible = true;

                    btnPickList.Visible = false;
                    btnPickListEmail.Visible = false;
                    tpLocationDetails.Visible = false;

                    break;

                case 2: // Purchase Order
                    intTempMenuID = (int)eMenuID.PurchaseOrder;
                    intTempCompany = (int)ControlOperationType.PurOrdCompany;
                    intTempEmployee = (int)ControlOperationType.PurOrdEmployee;
                    strTempTableName = "InvPurchaseOrderMaster";
                    txtSupplierAddress.Visible = true;
                    lblOrderType.Visible = true;
                    cboOrderType.Visible = true;
                    cboOrderNo.Visible = true;
                    ChkTGNR.Visible = true;

                    btnAddExpense.Enabled = txtExpenseAmount.Enabled = lblExpenseAmount.Enabled = true;

                    lblOrderType.Top = lblCompany.Top + 25;
                    cboOrderType.Top = cboCompany.Top + 25;
                    cboOrderNo.Top = cboOrderType.Top;
                    cboCurrency.Top = cboOrderType.Top + 25;
                    lblCurrency.Top = btnCurrency.Top = cboCurrency.Top;

                    lblPurchaseNo.Text = "Order No";
                    lblSPurchaseNo.Text = "Order No";


                    txtAdvPayment.Visible = true;
                    lblAdvancePayment.Visible = true;

                    //lblIncoTerms.Visible =  cboIncoTerms.Visible = true;
                    //lblLeadTime.Visible =  txtLeadTime.Visible = true;
                    //lblProductionDate.Visible =  dtpProductionDate.Visible = true;
                    //lblTransShipmentDate.Visible =  dtpTrnsShipmentDate.Visible = true;
                    //lblClearenceDate.Visible =  dtpClearenceDate.Visible = true;
                    //lblContainerType.Visible = cboContainerType.Visible = true;
                    //lblNoOfContainers.Visible = txtNoOfContainers.Visible = true;
                    //btnIncoTerms.Visible = btnContainerType.Visible = true;

                    btnPickList.Visible = false;
                    btnPickListEmail.Visible = false;
                    tpLocationDetails.Visible = false;


                    //if (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.POrderFromGRN)
                    //{
                    //    dgvPurchase.Columns["Rate"].Visible = false;
                    //    dgvPurchase.Columns["PurchaseRate"].Visible = false;
                    //    dgvPurchase.Columns["Discount"].Visible = false;
                    //    dgvPurchase.Columns["DiscountAmount"].Visible = false;
                    //    dgvPurchase.Columns["GrandAmount"].Visible = false;
                    //    dgvPurchase.Columns["NetAmount"].Visible = false;
                    //}
                    //else
                    //{
                    //    dgvPurchase.Columns["Rate"].Visible = true;
                    //    dgvPurchase.Columns["Discount"].Visible = true;
                    //    dgvPurchase.Columns["DiscountAmount"].Visible = true;
                    //    dgvPurchase.Columns["GrandAmount"].Visible = true;
                    //    dgvPurchase.Columns["NetAmount"].Visible = true;
                    //}

                    break;

                case 3: // Goods Received Note
                    intTempMenuID = (int)eMenuID.GRN;
                    intTempCompany = (int)ControlOperationType.GRNCompany;
                    intTempEmployee = (int)ControlOperationType.GRNEmployee;
                    strTempTableName = "InvGRNMaster";

                    dtpDueDate.Visible = false;
                    lblDueDate.Visible = false;
                    lblAmtinWrds.Visible = false;
                    lblAmountinWords.Visible = false;

                    lblOrderType.Visible = true;
                    cboOrderType.Visible = true;
                    BtnTVenderAddress.Visible = true;
                    lblWarehouse.Visible = true;
                    cboWarehouse.Visible = true;
                    btnWarehouse.Visible = true;

                    cboWarehouse.Enabled = false;
                    btnWarehouse.Enabled = false;
                    cboDiscount.Enabled = false;
                    cboCurrency.Visible = false;
                    lblCurrency.Visible = false;
                    cboSupplier.Enabled = false;
                    cboPaymentTerms.Visible = false;
                    lblPaymentTerms.Visible = false;
                    lblRemarks.Top = lblPaymentTerms.Top;
                    txtRemarks.Top = cboPaymentTerms.Top;

                    if (ClsCommonSettings.PblnIsLocationWise)
                    {
                        tpLocationDetails.Visible = true;
                        //tcPurchase.Controls.Add(dgvPurchase);
                        btnPickList.Visible = true;
                        btnPickListEmail.Visible = true;
                    }
                    else
                    {
                        tpLocationDetails.Visible = false;
                        btnPickList.Visible = false;
                        btnPickListEmail.Visible = false;
                    }
                    //   btnSupplier.Enabled = false;
                    btnAddressChange.Enabled = false;
                    cboSupplier.Enabled = false;


                    lblOrderType.Top = lblCompany.Top + 25;
                    lblWarehouse.Top = lblOrderType.Top + 25;
                    cboOrderType.Top = cboOrderNo.Top = cboCompany.Top + 25;
                    cboWarehouse.Top = btnWarehouse.Top = cboOrderType.Top + 25;
                    cboCurrency.Top = cboWarehouse.Top + 25;
                    lblCurrency.Top = btnCurrency.Top = cboCurrency.Top;

                    txtRemarks.Height = txtRemarks.Bottom - lblPaymentTerms.Top;
                    //lblRemarks.Top = lblPaymentTerms.Top;
                    //txtRemarks.Top = cboPaymentTerms.Top;

                    lblPurchaseNo.Text = "GRN No";
                    lblOrderType.Text = "GRN Type";
                    lblSPurchaseNo.Text = "GRN No";

                    dgvPurchase.Columns["Quantity"].Visible = true;
                    dgvPurchase.Columns["ReceivedQty"].Visible = true;
                    dgvPurchase.Columns["OrderedQty"].Visible = true;
                    dgvPurchase.Columns["ExtraQty"].Visible = true;
                    dgvPurchase.Columns["Batchnumber"].Visible = true;
                    dgvPurchase.Columns["ExpiryDate"].Visible = true;

                    dgvPurchase.Columns["Rate"].Visible = false;
                    dgvPurchase.Columns["PurchaseRate"].Visible = false;
                    dgvPurchase.Columns["Discount"].Visible = false;
                    dgvPurchase.Columns["DiscountAmount"].Visible = false;
                    dgvPurchase.Columns["GrandAmount"].Visible = false;
                    dgvPurchase.Columns["NetAmount"].Visible = false;

                    dgvPurchase.Columns["OrderedQty"].HeaderText = "InvoicedQty";

                    dgvPurchase.Columns["ExpiryDate"].DisplayIndex = dgvPurchase.Columns["Quantity"].DisplayIndex;


                    pnlAmount.Visible = false;

                    bnExpense.Visible = false;



                    break;
                case 4: // Purchase Invoice
                    intTempMenuID = (int)eMenuID.PurchaseInvoice;
                    intTempCompany = (int)ControlOperationType.PurInvCompany;
                    intTempEmployee = (int)ControlOperationType.PurInvEmployee;
                    strTempTableName = "InvPurchaseInvoiceMaster";
                    txtSupplierAddress.Visible = true;
                    lblOrderType.Visible = true;
                    cboOrderType.Visible = true;
                    cboOrderNo.Visible = true;
                    lblWarehouse.Visible = true;
                    cboWarehouse.Visible = true;
                    lblSupplierBillNo.Visible = true;
                    txtSupplierBillNo.Visible = true;
                    btnWarehouse.Visible = true;
                    ChkTGNR.Visible = true;

                    //btnAccountSettings.Visible = true;

                    dtpDueDate.Visible = false;
                    lblDueDate.Visible = false;

                    lblExpenseAmount.Enabled = txtExpenseAmount.Enabled = btnAddExpense.Enabled = true;

                    lblOrderType.Top = lblCompany.Top + 25;
                    lblWarehouse.Top = lblOrderType.Top + 25;
                    cboOrderType.Top = cboOrderNo.Top = cboCompany.Top + 25;
                    cboWarehouse.Top = btnWarehouse.Top = cboOrderType.Top + 25;
                    cboCurrency.Top = cboWarehouse.Top + 25;
                    lblCurrency.Top = btnCurrency.Top = cboCurrency.Top;

                    ChkTGNR.Top = txtSupplierBillNo.Top;
                    ChkTGNR.Left = txtSupplierBillNo.Left;

                    lblSupplierBillNo.Top = lblDueDate.Top;
                    txtSupplierBillNo.Top = dtpDueDate.Top;

                    lblPurchaseNo.Text = "Invoice No";
                    lblOrderType.Text = "Invoice Type";
                    lblSPurchaseNo.Text = "Invoice No";
                    ReceivedQty.HeaderText = "Invoiced Qty";

                    dgvPurchase.Columns["Quantity"].Visible = true;
                    dgvPurchase.Columns["ReceivedQty"].Visible = true;
                    dgvPurchase.Columns["OrderedQty"].Visible = true;
                    dgvPurchase.Columns["ExtraQty"].Visible = true;
                    dgvPurchase.Columns["Batchnumber"].Visible = true;
                    dgvPurchase.Columns["ExpiryDate"].Visible = true;
                    dgvPurchase.Columns["PurchaseRate"].Visible = true;

                    dgvPurchase.Columns["ExpiryDate"].DisplayIndex = dgvPurchase.Columns["Quantity"].DisplayIndex;

                    dgvPurchase.Columns["ItemCode"].Width = 65;
                    dgvPurchase.Columns["ItemName"].Width = 200;
                    dgvPurchase.Columns["Quantity"].Width = 70;
                    dgvPurchase.Columns["ReceivedQty"].Width = 75;
                    dgvPurchase.Columns["OrderedQty"].Width = 70;
                    dgvPurchase.Columns["ExtraQty"].Width = 60;
                    dgvPurchase.Columns["Batchnumber"].Width = 150;
                    dgvPurchase.Columns["Uom"].Width = 75;
                    dgvPurchase.Columns["Rate"].Width = 70;
                    dgvPurchase.Columns["DiscountAmount"].Width = 80;
                    dgvPurchase.Columns["Discount"].Width = 100;
                    dgvPurchase.Columns["GrandAmount"].Width = 80;
                    dgvPurchase.Columns["NetAmount"].Width = 90;

                    txtAdvPayment.Visible = true;
                    lblAdvancePayment.Visible = true;
                    tpLocationDetails.Visible = false;
                    btnPickList.Visible = false;
                    btnPickListEmail.Visible = false;
                    break;

                default:
                    break;
            }
        }

        //private DataTable FillDiscountColumn(int intRowIndex, bool blnIgnoreDate)
        //{
        //    decimal decQty, decRate = 0;
        //    int intItemID = 0;
        //    int intDiscountID = dgvPurchase[Discount.Index, intRowIndex].Tag.ToInt32();

        //    decQty = dgvPurchase[Quantity.Index, intRowIndex].Value.ToDecimal();
        //    DataTable datUomConversions = MobjClsBLLPurchase.GetUomConversionValues(dgvPurchase["Uom", intRowIndex].Tag.ToInt32(), intItemID);
        //    //if (MintFromForm == 3 && cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.GRNFromInvoice)
        //    //    datUomConversions = MobjClsBLLPurchase.GetUomConversionValues(dgvPurchase["Uom", intRowIndex].Tag.ToInt32(), intItemID, 1);
        //    if (datUomConversions.Rows.Count > 0)
        //    {
        //        if (datUomConversions.Rows[0]["ConvertionFactorID"].ToInt32() == 2)
        //            decQty = decQty * datUomConversions.Rows[0]["ConvertionValue"].ToDecimal();
        //        else
        //            decQty = decQty / datUomConversions.Rows[0]["ConvertionValue"].ToDecimal();
        //    }
        //    decRate = dgvPurchase[GrandAmount.Index, intRowIndex].Value.ToDecimal();
        //    intItemID = dgvPurchase[ItemID.Index, intRowIndex].Value.ToInt32();

        //    DataTable dtItemDiscounts = new DataTable();
        //    if (blnIgnoreDate)
        //        dtItemDiscounts = MobjClsBLLPurchase.GetItemDiscountDetails(intItemID, decQty, decRate, "", cboCurrency.SelectedValue.ToInt32());
        //    else
        //        dtItemDiscounts = MobjClsBLLPurchase.GetItemDiscountDetails(intItemID, decQty, decRate, dtpDate.Value.ToString("dd-MMM-yyyy"), cboCurrency.SelectedValue.ToInt32());
        //    Discount.DataSource = null;
        //    Discount.ValueMember = "DiscountID";
        //    Discount.DisplayMember = "DiscountShortName";
        //    DataRow dr = dtItemDiscounts.NewRow();
        //    dr["DiscountID"] = 0;
        //    dr["DiscountShortName"] = "No Discount";
        //    dtItemDiscounts.Rows.InsertAt(dr, 0);
        //    Discount.DataSource = dtItemDiscounts;

        //    dgvPurchase[Discount.Index, intRowIndex].Value = intDiscountID;
        //    dgvPurchase[Discount.Index, intRowIndex].Tag = intDiscountID;
        //    dgvPurchase[Discount.Index, intRowIndex].Value = dgvPurchase[Discount.Index, intRowIndex].FormattedValue;
        //    if (string.IsNullOrEmpty(dgvPurchase[Discount.Index, intRowIndex].Value.ToString()))
        //    {
        //        dgvPurchase[Discount.Index, intRowIndex].Value = 0;
        //        dgvPurchase[Discount.Index, intRowIndex].Tag = 0;
        //        dgvPurchase[Discount.Index, intRowIndex].Value = dgvPurchase[Discount.Index, intRowIndex].FormattedValue;
        //    }
        //    return dtItemDiscounts;

        //    return null;
        //}

        private DataTable FillComboColumn(int intItemID, int intType)
        {
            try
            {
                if (intType == 0 || intType == 1)
                {
                    DataTable dtItemUOMs = MobjClsBLLPurchase.GetItemUOMs(intItemID);
                    //if (MintFromForm == 3 && cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.GRNFromInvoice)
                    //    MobjClsBLLPurchase.GetItemUOMs(intItemID, 1);
                    Uom.DataSource = null;
                    Uom.ValueMember = "UOMID";
                    Uom.DisplayMember = "ShortName";
                    Uom.DataSource = dtItemUOMs;
                    return dtItemUOMs;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FillComboColumn() " + ex.Message);
                MObjLogs.WriteLog("Error in FillComboColumn() " + ex.Message, 2);
            }
            return null;
        }

        private bool IsApprovedDateExeedsQuotationDueDate()
        {
            if (ClsCommonSettings.GetServerDate() > Convert.ToDateTime(dtpDueDate.Value.ToShortDateString()))
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 983, out MmessageIcon).Replace("#", "");
                if (MintFromForm == 1)
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation");
                else
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");

                if (MessageBox.Show(MstrMessageCommon, "MyBooksERP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MblnCanShow = false;
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        private void ShowDocumentMaster()
        {


            using (frmDocumentMaster objfrmDocumentMaster = new frmDocumentMaster())
            {
                if (MintFromForm == 1)
                {
                    objfrmDocumentMaster.PintOperationType = Convert.ToInt32(OperationType.PurchaseQuotation);
                    if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PQuotationOpened && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PQuotationRejected && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PQuotationCancelled)
                        objfrmDocumentMaster.MblnEditable = false;
                }
                else if (MintFromForm == 2)
                {
                    objfrmDocumentMaster.PintOperationType = Convert.ToInt32(OperationType.PurchaseOrder);
                    if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.POrderOpened && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.POrderRejected && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.POrderRejected)
                        objfrmDocumentMaster.MblnEditable = false;
                }
                else if (MintFromForm == 4)
                {
                    objfrmDocumentMaster.PintOperationType = (Int32)OperationType.PurchaseInvoice;
                    if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PInvoiceOpened && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PInvoiceCancelled)
                        objfrmDocumentMaster.MblnEditable = false;
                }

                objfrmDocumentMaster.PlngOperationID = Convert.ToInt64(txtPurchaseNo.Tag);
                objfrmDocumentMaster.PstrOperationNo = txtPurchaseNo.Text.Trim();
                if (intPurchaseIDToLoad != 0)
                    objfrmDocumentMaster.MblnEditable = false;

                //if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PQuotationOpened || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.POrderOpened || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PInvoiceOpened)
                //    objfrmDocumentMaster.ssStatus = "Opened";
                //else if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PQuotationCancelled || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.POrderOpened || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PInvoiceOpened)
                objfrmDocumentMaster.sStatus = lblStatusText.Text;
                objfrmDocumentMaster.PintOperationStatus = MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID;
                //   objfrmDocumentMaster.DocumentID = 1;
                objfrmDocumentMaster.FlagWhere = 2;
                objfrmDocumentMaster.ShowDialog();
            }
        }

        private void SetPurchaseStatus()
        {
            lblDescription.Visible = false;
            txtDescription.Visible = false;
            bnSubmitForApproval.Text = bnSubmitForApproval.Tooltip = "Submit For Approval";
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            btnActions.Enabled = true;
            //lblCriteria.Visible = lblCriteriaText.Visible = false;
            //lblDescription.Location = lblCriteria.Location;
            //txtDescription.Location = lblCriteriaText.Location;

            if (MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID == 0)
            {
                if (MintFromForm != 3)
                    SetControlsVisibility(true);
                else
                {
                    cboCompany.Enabled = true;
                    cboOrderType.Enabled = true;
                    dtpDate.Enabled = true;
                    txtPurchaseNo.Enabled = true;
                    btnAddExpense.Enabled = false;
                }
                lblStatusText.ForeColor = Color.Black;
                lblStatusText.Text = "New";
                BindingNavigatorAddNewItem.Enabled = false;
                BindingNavigatorCancelItem.Enabled = false;
                BindingNavigatorCancelItem.Text = "Cancel";
                BindingNavigatorDeleteItem.Enabled = false;
                btnApprove.Visible = btnReject.Visible = false;
                btnDeny.Visible = btnSuggest.Visible = false;
                //   btnShowComments.Visible = false;
                tiSuggestions.Visible = false;
                bnSubmitForApproval.Visible = false;
                btnActions.Enabled = false;
            }
            else if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationCancelled ||
                MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderCancelled ||
                MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PInvoiceCancelled ||
                MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.GRNCancelled)
            {
                lblStatusText.ForeColor = Color.Red;
                lblStatusText.Text = "Cancelled";
                BindingNavigatorCancelItem.Enabled = true;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorCancelItem.Text = "Re Open";
                btnApprove.Visible = btnReject.Visible = bnSubmitForApproval.Visible = false;
                // btnShowComments.Visible = false;
                btnSuggest.Visible = btnDeny.Visible = false;
                if (MintFromForm != 3)
                    SetControlsVisibility(true);
                btnAddExpense.Enabled = true;

                lblDescription.Visible = true;
                txtDescription.Visible = true;
            }
            else if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationOpened ||
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderOpened ||
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PInvoiceOpened ||
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.GRNOpened)
            {
                lblStatusText.ForeColor = Color.Brown;
                lblStatusText.Text = "Opened";
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
                BindingNavigatorCancelItem.Text = "Cancel";
                btnActions.Enabled = true;
                if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PInvoiceOpened || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.GRNOpened)
                    bnSubmitForApproval.Visible = false;
                else
                {
                    if ((MintFromForm == 1 && ClsCommonSettings.PQApproval) || (MintFromForm == 2 && ClsCommonSettings.POApproval))
                        bnSubmitForApproval.Text = bnSubmitForApproval.Tooltip = "Submit For Approval";
                    else
                        bnSubmitForApproval.Text = bnSubmitForApproval.Tooltip = "Submit";
                    bnSubmitForApproval.Visible = MblnUpdatePermission;
                }
                btnApprove.Visible = false;
                btnReject.Visible = false;
                //btnShowComments.Visible = false;
                tiSuggestions.Visible = false;
                btnDeny.Visible = btnSuggest.Visible = false;
                if (MintFromForm != 3)
                    SetControlsVisibility(true);
                btnAddExpense.Enabled = true;
            }
            else if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationClosed ||
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderClosed ||
                    MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PInvoiceClosed)
            {
                lblStatusText.ForeColor = Color.Purple;
                lblStatusText.Text = "Closed";
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorCancelItem.Text = "Cancel";
                BindingNavigatorCancelItem.Enabled = false;
                btnApprove.Visible = btnReject.Visible = bnSubmitForApproval.Visible = false;
                btnSuggest.Visible = btnDeny.Visible = false;
                tiSuggestions.Visible = false;
                if (MintFromForm != 3)
                    SetControlsVisibility(false);
                btnAddExpense.Enabled = true;
                lblPurchasestatus.Text = "Closed items cann't be Deleted.";
                if (MblnIsEditable)
                    MblnIsEditable = false;
            }
            else if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderSubmittedForApproval || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationSubmittedForApproval)
            {
                lblStatusText.ForeColor = Color.BlueViolet;
                lblStatusText.Text = "Submitted For Approval";
                BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
                BindingNavigatorCancelItem.Text = "Cancel";
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                bnSubmitForApproval.Visible = false;
                if (intPurchaseIDToLoad != 0 && !blnDisableFunctionalities)
                {
                    if (PintApprovePermission == (int)ApprovePermission.Suggest || PintApprovePermission == (int)ApprovePermission.Both)
                    {
                        btnSuggest.Visible = btnDeny.Visible = true;
                        tiSuggestions.Visible = true;
                    }
                    if (PintApprovePermission == (int)ApprovePermission.Approve || PintApprovePermission == (int)ApprovePermission.Both)
                        btnApprove.Visible = btnReject.Visible = true;
                }
                else
                {
                    btnSuggest.Visible = btnDeny.Visible = false;
                    btnApprove.Visible = btnReject.Visible = false;
                    tiSuggestions.Visible = false;
                }
                if (MintFromForm != 3)
                    SetControlsVisibility(false);
                btnAddExpense.Enabled = true;

                if (MblnIsEditable)
                    MblnIsEditable = false;
            }
            else if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderSubmitted || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationSubmitted)
            {
                lblStatusText.ForeColor = Color.BlueViolet;
                lblStatusText.Text = "Submitted";
                BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
                BindingNavigatorCancelItem.Text = "Cancel";
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                bnSubmitForApproval.Visible = false;
                if (MintFromForm != 3)
                    SetControlsVisibility(false);
                btnAddExpense.Enabled = true;

                if (MblnIsEditable)
                    MblnIsEditable = false;
            }
            else if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationRejected ||
               MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderRejected)
            {
                lblStatusText.ForeColor = Color.Red;
                lblStatusText.Text = "Rejected";
                BindingNavigatorCancelItem.Text = "Cancel";
                BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                btnActions.Enabled = true;
                btnApprove.Visible = false;
                btnReject.Visible = false;
                btnDeny.Visible = btnSuggest.Visible = false;
                tiSuggestions.Visible = false;
                if (intPurchaseIDToLoad == 0 && !blnDisableFunctionalities)
                {
                    bnSubmitForApproval.Visible = MblnUpdatePermission;
                    if ((MintFromForm == 1 && ClsCommonSettings.PQApproval) || (MintFromForm == 2 && ClsCommonSettings.POApproval))
                        bnSubmitForApproval.Text = bnSubmitForApproval.Tooltip = "Submit For Approval";
                    else
                        bnSubmitForApproval.Text = bnSubmitForApproval.Tooltip = "Submit";
                }
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                if (MintFromForm != 3)
                    SetControlsVisibility(true);
                btnAddExpense.Enabled = true;

                lblDescription.Visible = true;
                txtDescription.Visible = true;

                if (MblnIsEditable)
                    MblnIsEditable = false;
            }
            else if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationApproved ||
                MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderApproved)
            {
                lblStatusText.ForeColor = Color.Green;
                lblStatusText.Text = "Approved";
                BindingNavigatorCancelItem.Text = "Cancel";
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorCancelItem.Enabled = false;
                btnApprove.Visible = false;
                btnReject.Visible = false;
                bnSubmitForApproval.Visible = false;
                btnSuggest.Visible = btnDeny.Visible = false;
                tiSuggestions.Visible = false;
                btnActions.Enabled = true;
                if (intPurchaseIDToLoad != 0 && !blnDisableFunctionalities)
                {
                    btnReject.Visible = true;
                }
                SetControlsVisibility(false);
                btnAddExpense.Enabled = true;
                lblDescription.Visible = true;
                txtDescription.Visible = true;
                lblPurchasestatus.Text = "Approved items cann't be Deleted.";

                if (MblnIsEditable)
                    MblnIsEditable = false;

                //lblCriteriaText.Visible = lblCriteria.Visible = true;
                //lblDescription.Top = lblDueDate.Top;
                //txtDescription.Top = dtpDueDate.Top;
            }
            //else if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.PQuotationInvalid)
            //{
            //    lblStatusText.ForeColor = Color.Red;
            //    lblStatusText.Text = "Invalid";
            //    BindingNavigatorSaveItem.Enabled = false;
            //    BindingNavigatorDeleteItem.Enabled = false;
            //    BindingNavigatorCancelItem.Text = "Cancel";
            //    BindingNavigatorCancelItem.Enabled = false;
            //    btnApprove.Visible = btnReject.Visible = bnSubmitForApproval.Visible = false;
            //    tiSuggestions.Visible = false;
            //    btnSuggest.Visible = btnDeny.Visible = false;
            //    if (MintFromForm != 3)
            //        SetControlsVisibility(false);
            //    btnAddExpense.Enabled = true;
            //    lblPurchasestatus.Text = "Invalid quotations cann't be Deleted.";

            //    if (MblnIsEditable)
            //        MblnIsEditable = false;
            //}
            if (intPurchaseIDToLoad != 0 && !blnDisableFunctionalities)
            {
                MblnIsEditable = false;
                if (MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID == intPurchaseIDToLoad)
                {
                    //if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PQuotationSubmittedForApproval || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.POrderSubmittedForApproval)
                    //{
                    //    DataTable dtVerificationHistoryDetails = GetVerificationHistoryStatus(false);
                    //    if (dtVerificationHistoryDetails.Rows.Count > 0)
                    //    {
                    //        if ((MintFromForm == 1 && dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.PQuotationSuggested) || (MintFromForm == 2 && dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)PurchaseSettings.OrderSuggested))
                    //        {
                    //            btnSuggest.Visible = false;
                    //            btnDeny.Visible = true;
                    //        }
                    //        else
                    //        {
                    //            btnSuggest.Visible = true;
                    //            btnDeny.Visible = false;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (PintApprovePermission == (int)ApprovePermission.Suggest || PintApprovePermission == (int)ApprovePermission.Both)
                    //        {
                    //            btnSuggest.Visible = btnDeny.Visible = true;
                    //            tiSuggestions.Visible = true;
                    //        }
                    //    }
                    //    if (PintApprovePermission == (int)ApprovePermission.Approve || PintApprovePermission == (int)ApprovePermission.Both)
                    //        btnApprove.Visible = btnReject.Visible = true;
                    //}
                    if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.POrderRejected || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PQuotationRejected)
                    {
                        //DataTable dtVerificationHistoryDetails = GetVerificationHistoryStatus(false);
                        //if (dtVerificationHistoryDetails.Rows.Count > 0)
                        //{
                        //    if ((MintFromForm == 1 && dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.PQuotationSuggested) || (MintFromForm == 2 && dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)PurchaseSettings.OrderSuggested))
                        //    {
                        //        btnSuggest.Visible = false;
                        //        btnDeny.Visible = true;
                        //        tiSuggestions.Visible = true;
                        //    }
                        //    else
                        //    {
                        //        btnSuggest.Visible = true;
                        //        btnDeny.Visible = false;
                        //        tiSuggestions.Visible = true;
                        //    }
                        //}
                        //else
                        //{
                        //    if (PintApprovePermission == (int)ApprovePermission.Suggest || PintApprovePermission == (int)ApprovePermission.Both)
                        //    {
                        //        tiSuggestions.Visible = true;
                        //        btnDeny.Visible = btnSuggest.Visible = true;
                        //    }
                        //}
                        if (PintApprovePermission == (int)ApprovePermission.Approve || PintApprovePermission == (int)ApprovePermission.Both)
                            btnApprove.Visible = true;
                    }
                    else if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.POrderApproved || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PQuotationApproved)
                    {
                        //DataTable dtVerificationHistoryDetails = GetVerificationHistoryStatus(false);
                        // if (dtVerificationHistoryDetails.Rows.Count > 0)
                        // {
                        // if ((MintFromForm == 1 && dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.PQuotationSuggested) || (MintFromForm == 2 && dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)PurchaseSettings.OrderSuggested))
                        // {
                        //   btnSuggest.Visible = false;
                        //    btnDeny.Visible = true;
                        // }
                        // else
                        ///  {
                        //     btnSuggest.Visible = true;
                        //      btnDeny.Visible = false;
                        // }
                        //}
                        // else
                        btnDeny.Visible = btnSuggest.Visible = false;
                        tiSuggestions.Visible = false;
                        //if (PintApprovePermission == (int)ApprovePermission.Suggest || PintApprovePermission == (int)ApprovePermission.Both)
                        //{
                        //    tiSuggestions.Visible = true;
                        //}
                        if (PintApprovePermission == (int)ApprovePermission.Approve || PintApprovePermission == (int)ApprovePermission.Both)
                            btnReject.Visible = true;
                    }
                }
                else
                {
                    btnApprove.Visible = btnReject.Visible = false;
                    btnSuggest.Visible = btnDeny.Visible = false;
                    tiSuggestions.Visible = false;
                }
                SetControlsVisibility(false);
                bnSubmitForApproval.Visible = BindingNavigatorSaveItem.Enabled = BindingNavigatorCancelItem.Enabled = BindingNavigatorClearItem.Enabled = BindingNavigatorDeleteItem.Enabled = BindingNavigatorAddNewItem.Enabled = false;
                btnAddExpense.Enabled = true;
            }
            else if (intPurchaseIDToLoad != 0 && blnDisableFunctionalities)
            {
                MblnIsEditable = false;
                SetControlsVisibility(false);
                btnSuggest.Visible = btnDeny.Visible = bnSubmitForApproval.Visible = btnApprove.Visible = btnReject.Visible = BindingNavigatorSaveItem.Enabled = BindingNavigatorCancelItem.Enabled = BindingNavigatorClearItem.Enabled = BindingNavigatorDeleteItem.Enabled = BindingNavigatorAddNewItem.Enabled = false;
                tiSuggestions.Visible = false;
                btnAddExpense.Enabled = true;
            }
            if (MintFromForm == 2)
            {
                if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.POrderFromQuotation)
                {
                    SetControlVisibilityForBottomPanel(false);
                    cboCurrency.Enabled = false;
                    cboSupplier.Enabled = false;
                    cboPaymentTerms.Enabled = false;
                    //   btnSupplier.Enabled = false;
                    btnAddressChange.Enabled = false;
                    cboDiscount.Enabled = false;

                    dgvPurchase.ReadOnly = false;
                    Quantity.ReadOnly = false;
                    Rate.ReadOnly = false;
                    Discount.ReadOnly=false;
                    ItemCode.ReadOnly = ItemName.ReadOnly = NetAmount.ReadOnly = Uom.ReadOnly = GrandAmount.ReadOnly = true;

                }
                else
                {
                    if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.POrderApproved)
                        SetControlVisibilityForBottomPanel(true);
                    else
                        SetControlVisibilityForBottomPanel(false);
                }
            }
            else if (MintFromForm == 4 && Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.PInvoiceFromOrder)
            {
                cboCurrency.Enabled = false;
                cboSupplier.Enabled = false;
                cboPaymentTerms.Enabled = false;
                cboPurchaseAccount.Enabled = false;
                btnAddressChange.Enabled = false;
                cboDiscount.Enabled = false;
                //  btnSupplier.Enabled = false;
            }

        }

        private DataTable GetVerificationHistoryStatus(bool blnAllUsers)
        {
            string strCondition = "";

            switch (MintFromForm)
            {
                case 1:
                    strCondition += "VH.OperationTypeID = " + (int)OperationType.PurchaseQuotation + " And VH.ReferenceID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID + " And VH.StatusID In (" + (int)OperationStatusType.PQuotationSuggested + "," + (int)OperationStatusType.PQuotationDenied + ")";
                    if (!blnAllUsers)
                        strCondition += " And VH.VerifiedBy = " + ClsCommonSettings.UserID;
                    break;
                case 2:
                    strCondition += "VH.OperationTypeID = " + (int)OperationType.PurchaseOrder + " And VH.ReferenceID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID + " And VH.StatusID In (" + (int)OperationStatusType.POrderSuggested + "," + (int)OperationStatusType.POrderDenied + ")";
                    if (!blnAllUsers)
                        strCondition += " And VH.VerifiedBy = " + ClsCommonSettings.UserID;
                    break;
            }
            return MobjClsBLLPurchase.FillCombos(new string[] { "VH.StatusID,Convert(varchar(10),VH.VerifiedDate,103) as VerifiedDate,UM.UserID,IsNull(EM.FirstName,UM.UserName) as UserName,ST.Description as Status,VH.Remarks as Comment", "STVerificationHistory VH Inner Join UserMaster UM On UM.UserID = VH.VerifiedBy Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID Inner Join STCommonStatusReference ST On ST.StatusID = VH.StatusID", strCondition });
        }

        private void CalculateExchangeCurrencyRate()
        {
            int intCompanyID = 0, intCurrencyID = 0;

            intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            intCurrencyID = Convert.ToInt32(cboCurrency.SelectedValue);

            if (MobjClsBLLPurchase.GetExchangeCurrencyRate(intCompanyID, intCurrencyID))
            {
                txtExchangeCurrencyRate.Text = Convert.ToDecimal(MobjClsBLLPurchase.PobjClsDTOPurchase.decExchangeCurrencyRate * Convert.ToDecimal(txtTotalAmount.Text)).ToString("F" + MintBaseCurrencyScale);
            }
            string strAmount = txtTotalAmount.Text;
            int intCurrency = Convert.ToInt32(cboCurrency.SelectedValue);
            lblAmountinWords.Text = MobjclsBLLCommonUtility.ConvertToWord(strAmount, intCurrency);
        }

        private string GetPurchasePrefix()
        {
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue));
            switch (MintFromForm)
            {
                case 1:
                    if (ClsCommonSettings.PQAutogenerate)
                        txtPurchaseNo.ReadOnly = false;
                    else
                        txtPurchaseNo.ReadOnly = true;
                    return ClsCommonSettings.PQPrefix;
                case 2:
                    if (ClsCommonSettings.POAutogenerate)
                        txtPurchaseNo.ReadOnly = false;
                    else
                        txtPurchaseNo.ReadOnly = true;
                    return ClsCommonSettings.POPrefix;
                case 3:

                    if (ClsCommonSettings.PGRNAutogenerate)
                        txtPurchaseNo.ReadOnly = false;
                    else
                        txtPurchaseNo.ReadOnly = true;
                    return ClsCommonSettings.PGRNPrefix;

                case 4:
                    if (ClsCommonSettings.PINVAutogenerate)
                        txtPurchaseNo.ReadOnly = false;
                    else
                        txtPurchaseNo.ReadOnly = true;
                    return ClsCommonSettings.PINVPrefix;
            }
            return "";
        }

        public void RefreshForm()
        {
            btnSRefresh_Click(null, null);
        }

        private void SetStatusLabelText()
        {
            //lblCancelledDate.Visible = false;
            //lblDescription.Visible = false;
            //dtpCancelledDate.Visible = false;
            //txtDescription.Visible = false;
            //lblCancelledBy.Visible = lblCancelledByText.Visible = false;

            switch (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID)
            {
                case 0:
                    lblStatusText.Text = "New";
                    lblStatusText.ForeColor = Color.Black;
                    break;
                case (int)OperationStatusType.PQuotationCancelled:
                case (int)OperationStatusType.POrderCancelled:
                case (int)OperationStatusType.GRNCancelled:
                case (int)OperationStatusType.PInvoiceCancelled:
                    lblStatusText.Text = "Cancelled";
                    lblStatusText.ForeColor = Color.Red;
                    //lblCancelledDate.Visible = true;
                    //lblDescription.Visible = true;
                    //dtpCancelledDate.Visible = true;
                    //txtDescription.Visible = true;
                    //lblCancelledBy.Visible = lblCancelledByText.Visible = true;
                    break;
                case (int)OperationStatusType.PQuotationOpened:
                case (int)OperationStatusType.POrderOpened:
                case (int)OperationStatusType.GRNOpened:
                case (int)OperationStatusType.PInvoiceOpened:
                    lblStatusText.Text = "Opened";
                    lblStatusText.ForeColor = Color.Brown;
                    break;
                case (int)OperationStatusType.POrderClosed:
                case (int)OperationStatusType.PQuotationClosed:
                case (int)OperationStatusType.PInvoiceClosed:
                    lblStatusText.Text = "Closed";
                    lblStatusText.ForeColor = Color.Purple;
                    break;
                case (int)OperationStatusType.PQuotationSubmittedForApproval:
                case (int)OperationStatusType.POrderSubmittedForApproval:
                    lblStatusText.Text = "Submitted For Approval";
                    lblStatusText.ForeColor = Color.BlueViolet;
                    break;
                //case (int)OperationStatusType.PQuotationSubmitted:
                //case (int)OperationStatusType.POrderSubmitted:
                //    lblStatusText.Text = "Submitted";
                //    lblStatusText.ForeColor = Color.BlueViolet;
                //    break;
                case (int)OperationStatusType.PQuotationApproved:
                case (int)OperationStatusType.POrderApproved:
                    lblStatusText.Text = "Approved";
                    lblStatusText.ForeColor = Color.Green;
                    //lblCancelledDate.Visible = true;
                    //lblDescription.Visible = true;
                    //dtpCancelledDate.Visible = true;
                    //txtDescription.Visible = true;
                    //lblCancelledBy.Visible = lblCancelledByText.Visible = true;
                    break;
                case (int)OperationStatusType.PQuotationRejected:
                case (int)OperationStatusType.POrderRejected:
                    lblStatusText.Text = "Rejected";
                    lblStatusText.ForeColor = Color.Red;
                    //lblCancelledDate.Visible = true;
                    //lblDescription.Visible = true;
                    //dtpCancelledDate.Visible = true;
                    //txtDescription.Visible = true;
                    //lblCancelledBy.Visible = lblCancelledByText.Visible = true;
                    break;
                //case (int)OperationStatusType.PQuotationInvalid:
                //    lblStatusText.Text = "Invalid";
                //    lblStatusText.ForeColor = Color.Red;
                //    break;
            }
        }

        private void ClearBottomControls()
        {
            cboIncoTerms.SelectedIndex = -1;
            txtLeadTime.Text = "";
            cboContainerType.SelectedIndex = -1;
            dtpProductionDate.Value = ClsCommonSettings.GetServerDate();
            dtpTrnsShipmentDate.Value = ClsCommonSettings.GetServerDate();
            dtpClearenceDate.Value = ClsCommonSettings.GetServerDate();
            txtNoOfContainers.Text = "";

        }

        private void SetControlVisibilityForBottomPanel(bool blnIsVisible)
        {
            cboIncoTerms.Enabled = txtLeadTime.Enabled = cboContainerType.Enabled = blnIsVisible;
            dtpProductionDate.Enabled = dtpTrnsShipmentDate.Enabled = dtpClearenceDate.Enabled = txtNoOfContainers.Enabled = blnIsVisible;
            btnIncoTerms.Enabled = btnContainerType.Enabled = blnIsVisible;
        }

        private void SetControlsVisibility(bool blnIsVisible)
        {
            cboCurrency.Enabled = cboPaymentTerms.Enabled = cboSupplier.Enabled = btnTContextmenu.Enabled = blnIsVisible;
            //btnSupplier.Enabled 
            BtnTVenderAddress.Enabled = btnAddressChange.Enabled = blnIsVisible;
            dtpDueDate.Enabled = dtpDate.Enabled = blnIsVisible;
            BindingNavigatorClearItem.Enabled = blnIsVisible;
            ChkTGNR.Enabled = cboDiscount.Enabled = blnIsVisible;
            txtRemarks.ReadOnly = !blnIsVisible;
            cboIncoTerms.Enabled = txtLeadTime.Enabled = cboContainerType.Enabled = blnIsVisible;
            dtpProductionDate.Enabled = dtpTrnsShipmentDate.Enabled = dtpClearenceDate.Enabled = txtNoOfContainers.Enabled = blnIsVisible;
            btnIncoTerms.Enabled = btnContainerType.Enabled = blnIsVisible;

            dgvPurchase.ReadOnly = !blnIsVisible;
            if (!dgvPurchase.ReadOnly)
                GrandAmount.ReadOnly = NetAmount.ReadOnly = PurchaseRate.ReadOnly = true;

            if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.POrderFromGRN && !MblnAddStatus)
                cboSupplier.Enabled = ChkTGNR.Enabled = btnSupplier.Enabled =false;


            if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.POrderFromGRN && !MblnAddStatus && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (Int32)OperationStatusType.POrderOpened)
            {
                dgvPurchase.ReadOnly = false;
                GrandAmount.ReadOnly = NetAmount.ReadOnly = PurchaseRate.ReadOnly = Quantity.ReadOnly = Uom.ReadOnly = ItemCode.ReadOnly = ItemName.ReadOnly = true;
            }

        }

        #endregion

        #region Events

        private void FrmPurchase_Load(object sender, EventArgs e)
        {
            try
            {
                dtpSFrom.Value = ClsCommonSettings.GetServerDate();
                dtpSTo.Value = ClsCommonSettings.GetServerDate();
                ArrangeControls();
                LoadMessage();
                SetPermissions();
                MblnAddStatus = true;
                LoadCombos(0, null);
                AddNewPurchase();

                if (intPurchaseIDToLoad != 0)
                {
                    BindingNavigatorCancelItem.Enabled = false;
                    BindingNavigatorDeleteItem.Enabled = false;
                    BindingNavigatorSaveItem.Enabled = false;
                    MblnAddStatus = false;
                    blnIsFrmApproval = true;
                    DisplayOrderInfo(intPurchaseIDToLoad);
                }
                else if (lngReferenceID != 0)
                {
                    if (MintFromForm == 2)
                        cboOrderType.SelectedValue = (int)OperationOrderType.POrderFromQuotation;
                    else if (MintFromForm == 4)
                        cboOrderType.SelectedValue = (int)OperationOrderType.PInvoiceFromOrder;
                    BindingNavigatorCancelItem.Enabled = false;
                    BindingNavigatorDeleteItem.Enabled = false;
                    BindingNavigatorSaveItem.Enabled = false;
                    MblnAddStatus = false;
                    blnIsFrmApproval = false;
                    DisplayOrderInfo(lngReferenceID);
                }
                else if (MlngGRNID > 0)
                {
                    ClearControls();
                    ClearBottomControls();
                    BindingNavigatorCancelItem.Enabled = false;
                    BindingNavigatorDeleteItem.Enabled = false;
                    BindingNavigatorSaveItem.Enabled = false;
                    MblnAddStatus = true;
                    blnIsFrmApproval = false;
                    DisplayPurchaseGRN();
                }
                cboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FrmPurchaseLoad() " + ex.Message);
                MObjLogs.WriteLog("Error in FrmPurchaseLoad() " + ex.Message, 2);
            }
        }

        private void btnSRefresh_Click(object sender, EventArgs e)
        {
            // Searching With Purchase No
            try
            {
                DisplayAllNoInSearchGrid();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnSRefreshClick() " + ex.Message);
                MObjLogs.WriteLog("Error in btnSRefreshClick() " + ex.Message, 2);
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewPurchase();
        }

        private void cboSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblSupplierAddressName.Text = "";

                if (MintFromForm == 1 || MintFromForm == 2 || MintFromForm == 3 || MintFromForm == 4)
                {
                    if (Convert.ToInt32(cboSupplier.SelectedValue) > 0)
                    {

                        LoadCombos(13, null);
                        lblCompanyCurrencyAmount.Text = "Amount in " + MobjClsBLLPurchase.GetCompanyCurrency(Convert.ToInt32(cboCompany.SelectedValue), out MintBaseCurrencyScale);

                        AddToAddressMenu(Convert.ToInt32(cboSupplier.SelectedValue));
                        BtnTVenderAddress.Text = CMSVendorAddress.Items[0].Text;
                        lblSupplierAddressName.Text = CMSVendorAddress.Items[0].Text + " Address";
                        MintVendorAddID = Convert.ToInt32(CMSVendorAddress.Items[0].Tag);
                        DisplayAddress(Convert.ToInt32(cboSupplier.SelectedValue));


                        if (cboOrderType.SelectedValue.ToInt32() == (Int32)OperationOrderType.POrderFromGRN)
                        cboOrderType_SelectedIndexChanged(null, new EventArgs());
                    }
                    else
                    {
                        CMSVendorAddress.Items.Clear();
                        lblSupplierAddressName.Text = "Permanent Address";
                        txtSupplierAddress.Text = "";
                    }
                }
                Changestatus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in cboSupplierSelectedIndexChanged() " + ex.Message);
                MObjLogs.WriteLog("Error in cboSupplier_SelectedIndexChanged() " + ex.Message, 2);
            }
        }

        private void PurchaseOrderBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                MobjClsBLLPurchase.PobjClsDTOPurchase.blnIsUpdate = true;
                txtPurchaseNo.Focus();
                if (SavePurchase(MintFromForm))
                {
                    // AddNewPurchase();
                    DisplayAllNoInSearchGrid();
                    DisplayOrderInfo(MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID);
                    BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorSaveItem_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in BindingNavigatorSaveItem_Click() " + ex.Message, 2);
            }

        }

        private void dgvPurchase_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //if (dgvPurchase.CurrentCell.ColumnIndex == ItemCode.Index || dgvPurchase.CurrentCell.ColumnIndex == ItemName.Index)
                //{
                //    for (int i = 0; i < dgvPurchase.Columns.Count; i++)
                //    {
                //            dgvPurchase.Rows[dgvPurchase.CurrentCell.RowIndex].Cells[i].Value = null;
                //            dgvPurchase.Rows[dgvPurchase.CurrentCell.RowIndex].Cells[i].Tag = null;
                //    }
                //}
                dgvPurchase.PServerName = ClsCommonSettings.ServerName;
            
                if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.POrderFromGRN)
                {
                    string[]  first = { "ItemCode", "ItemName", "ItemID", "Quantity" };//first grid 
                     string[] Second = { "ItemCode", "Description", "ItemID", "Quantity" };//inner grid
                     dgvPurchase.aryFirstGridParam = first;
                     dgvPurchase.arySecondGridParam = Second;
                }
                else
                {
                    string[] First = { "ItemCode", "ItemName", "ItemID" };//first grid 
                    string[] second = { "ItemCode", "Description", "ItemID" };//inner grid
                    dgvPurchase.aryFirstGridParam = First;
                    dgvPurchase.arySecondGridParam = second;
                }
                dgvPurchase.ColumnsToHide = new string[] { "ItemID" };


              
                dgvPurchase.PiFocusIndex = Quantity.Index;
                dgvPurchase.iGridWidth = 350;
                dgvPurchase.bBothScrollBar = true;
                dgvPurchase.pnlLeft = expandableSplitterLeft.Location.X + 5;
                dgvPurchase.pnlTop = expandableSplitterTop.Location.Y + PurchaseOrderBindingNavigator.Height + 10;

                //if (dgvPurchase.CurrentCell.ColumnIndex == ItemCode.Index)
                //{
                    if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.POrderFromGRN)
                    {
                        dgvPurchase.field = "ItemCode";
                    }
                    else
                    {
                        dgvPurchase.field = "Code";
                    }
                    dgvPurchase.CurrentCell.Value = dgvPurchase.TextBoxText;
                //}
                if (dgvPurchase.CurrentCell.ColumnIndex == ItemName.Index)
                {
                    // dgvPurchase.PiColumnIndex = 1;
                    dgvPurchase.field = "ItemName";
                    dgvPurchase.CurrentCell.Value = dgvPurchase.TextBoxText;
                }

                //int intParentID = MobjClsBLLPurchase.FillCombos(new string[] { "ParentID", "CompanyMaster", "CompanyID = " + cboCompany.SelectedValue.ToInt32() }).Rows[0]["ParentID"].ToInt32();
                //DataTable datCompany = new DataTable();
                //if (intParentID == 0)
                //    datCompany = MobjClsBLLPurchase.FillCombos(new string[] { "CompanyID", "CompanyMaster", "(CompanyID = " + cboCompany.SelectedValue.ToInt32() + " Or ParentID = " + cboCompany.SelectedValue.ToInt32() + ")" });
                //else
                //    datCompany = MobjClsBLLPurchase.FillCombos(new string[] { "CompanyID", "CompanyMaster", "(CompanyID = " + intParentID + " Or ParentID = " + intParentID + ")" });
                //if (datCompany.Rows.Count > 0)
                //{
                //    DataRow dr = datCompany.NewRow();
                //    dr["CompanyID"] = 0;
                //    datCompany.Rows.InsertAt(dr, 0);
                //}
                if (MintFromForm != 3 && MintFromForm != 4)
                {
                    if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.POrderFromGRN)
                    {
                        //DataTable dtDatasource = MobjClsBLLPurchase.GetDataPOrderFromGRN(strCondition);
                        //string strFilterString = dgvPurchase.field + " Like '" + dgvPurchase.TextBoxText.ToUpper() + "%'";
                        //dtDatasource.DefaultView.RowFilter = strFilterString;
                        //DataTable datTemp = dtDatasource.DefaultView.ToTable();
                        //datTemp.DefaultView.Sort = dgvPurchase.field;
                        //dgvPurchase.dtDataSource = datTemp.DefaultView.ToTable();
                    }
                    else
                    {
                        dgvPurchase.PsQuery = "select IM.[Code] as ItemCode,IM.[ItemName] as Description,IM.[ItemID] as ItemID from InvItemMaster IM INNER JOIN InvItemDetails ID ON IM.ItemID = ID.ItemID where Upper(IM." + dgvPurchase.field + ") like  '%" + ((dgvPurchase.TextBoxText.ToUpper())) + "%'  And IM.StatusID In( " + (int)OperationStatusType.ProductActive + ")";
                        dgvPurchase.PsQuery += " And IsNull(CompanyID,0) In (0," + ClsCommonSettings.CompanyID + ") AND ID.ProductTypeID <> 3 Order By CHARINDEX('" + dgvPurchase.TextBoxText.ToUpper() + "',UPPER(ItemName))";
                        //foreach (DataRow dr in datCompany.Rows)
                        //    dgvPurchase.PsQuery += dr["CompanyID"].ToInt32() + ",";
                        //dgvPurchase.PsQuery = dgvPurchase.PsQuery.Remove(dgvPurchase.PsQuery.Length - 1);
                        //dgvPurchase.PsQuery += ")";
                    }
                }
                else if (MintFromForm == 3)
                {
                    if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromInvoice)
                    {
                        dgvPurchase.PsQuery = "Select IM.[Code] as ItemCode,IM.[ItemName] as Description,IM.[ItemID] as ItemID,PD.ReceivedQuantity as InvoicedQty From InvPurchaseInvoiceDetail PD " +
                                             "Inner Join InvItemMaster IM On IM.ItemID = PD.ItemID " +
                                             "Where (Select IsNull(Sum(ReceivedQuantity),0) From InvGRNDetails GD Inner Join InvGRNMaster GM On GM.GRNID = GD.GRNID And GM.ReferenceID = PD.PurchaseInvoiceID And GD.ItemID = PD.ItemID And GM.StatusID <> " + (int)OperationStatusType.GRNCancelled + ") < PD.ReceivedQuantity " +
                                             "And PD.PurchaseInvoiceID = " + cboOrderNo.SelectedValue.ToInt64() + " And Upper(IM." + dgvPurchase.field + ") like  '" + ((dgvPurchase.TextBoxText.ToUpper())) + "%'" +
                                             " Group By IM.Code,IM.ItemName,IM.ItemID,PD.ReceivedQuantity,PD.PurchaseInvoiceID,PD.ItemID Order By " + dgvPurchase.field + "";
                    }
                    else if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromOrder)
                    {
                        dgvPurchase.PsQuery = "Select IM.[Code] as ItemCode,IM.[ItemName] as Description,IM.[ItemID] as ItemID,PD.Quantity as InvoicedQty From InvPurchaseOrderDetail PD " +
                                             "Inner Join InvItemMaster IM On IM.ItemID = PD.ItemID " +
                                             "Where (Select IsNull(Sum(ReceivedQuantity),0) From InvGRNDetails GD Inner Join InvGRNMaster GM On GM.GRNID = GD.GRNID And GM.ReferenceID = PD.PurchaseOrderID And GD.ItemID = PD.ItemID And GM.StatusID <> " + (int)OperationStatusType.GRNCancelled + ") < PD.Quantity " +
                                             "And PD.PurchaseOrderID = " + cboOrderNo.SelectedValue.ToInt64() + " And Upper(IM." + dgvPurchase.field + ") like  '" + ((dgvPurchase.TextBoxText.ToUpper())) + "%'" +
                                             " Group By IM.Code,IM.ItemName,IM.ItemID,PD.Quantity,PD.PurchaseOrderID,PD.ItemID Order By " + dgvPurchase.field + "";
                    }

                }
                else if (MintFromForm == 4)
                {
                    if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.PInvoiceDirect)
                    {
                        dgvPurchase.PsQuery = "select [Code] as ItemCode,[ItemName] as Description,[ItemID] as ItemID from InvItemMaster where Upper(" + dgvPurchase.field + ") like  '" + ((dgvPurchase.TextBoxText.ToUpper())) + "%'  And StatusID In( " + (int)OperationStatusType.ProductActive + ") ";
                        dgvPurchase.PsQuery += " And IsNull(CompanyID,0) In (0," + ClsCommonSettings.CompanyID + ") Order By " + dgvPurchase.field + "";
                        //foreach (DataRow dr in datCompany.Rows)
                        //    dgvPurchase.PsQuery += dr["CompanyID"].ToInt32() + ",";
                        //dgvPurchase.PsQuery = dgvPurchase.PsQuery.Remove(dgvPurchase.PsQuery.Length - 1);
                        //dgvPurchase.PsQuery += ")";
                    }
                    else
                        dgvPurchase.PsQuery = "Select IM.[Code] as ItemCode,IM.[ItemName] as Description,IM.[ItemID] as ItemID,PD.Quantity as OrderedQty From InvPurchaseOrderDetail PD " +
                                             "Inner Join InvItemMaster IM On IM.ItemID = PD.ItemID " +
                                             "Where (Select IsNull(Sum(ReceivedQuantity),0) From InvPurchaseInvoiceDetail PID Inner Join InvPurchaseInvoiceMaster PM On PM.PurchaseInvoiceID = PID.PurchaseInvoiceID And PM.PurchaseOrderID = PD.PurchaseOrderID And PID.ItemID = PD.ItemID And PM.StatusID <> " + (int)OperationStatusType.PInvoiceCancelled + ") < PD.Quantity " +
                                             "And PD.PurchaseOrderID = " + cboOrderNo.SelectedValue.ToInt64() + " And Upper(IM." + dgvPurchase.field + ") like  '" + ((dgvPurchase.TextBoxText.ToUpper())) + "%'" +
                                             "Group By IM.Code,IM.ItemName,IM.ItemID,PD.Quantity,PD.PurchaseOrderID ,PD.ItemID Order By " + dgvPurchase.field + " ";
                }

                if (dgvPurchase.CurrentRow != null)
                    dgvPurchase.CurrentRow.Cells[IsAutofilled.Index].Value = 0;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvItemGrid_TextBoxChangedEvent() " + ex.Message);
                MObjLogs.WriteLog("Error in dgvItemGrid_TextBoxChangedEvent() " + ex.Message, 2);
            }
        }

        private void CancelToolStripButton_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void dgvPurchase_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == ItemCode.Index) || (e.ColumnIndex == ItemName.Index))
            {
                dgvPurchase.PiColumnIndex = e.ColumnIndex;
            }
        }

        private void cb_KeyPress(object sender, KeyPressEventArgs e)
        {
            cb.DroppedDown = false;
        }

        private void dgvPurchase_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            this.dgvPurchase.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (dgvPurchase.CurrentCell.OwningColumn.Name == "Quantity" || dgvPurchase.CurrentCell.OwningColumn.Name == "ExtraQty")
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
                int intUomScale = 0;
                if (dgvPurchase[Uom.Index, dgvPurchase.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    intUomScale = MobjClsBLLPurchase.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPurchase[Uom.Index, dgvPurchase.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    if (intUomScale == 0)
                    {
                        if (e.KeyChar == '.')
                            e.Handled = true;
                    }
                    else
                    {
                        int dotIndex = -1;
                        if (txt.Text.Contains("."))
                        {
                            dotIndex = txt.Text.IndexOf('.');
                        }
                        if (e.KeyChar == '.')
                        {
                            if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            {
                                e.Handled = true;
                            }
                        }
                        else
                        {
                            if (char.IsDigit(e.KeyChar))
                            {
                                if (dotIndex != -1 && txt.SelectionStart > dotIndex)
                                {
                                    string[] splitText = txt.Text.Split('.');
                                    if (splitText.Length == 2)
                                    {
                                        if (splitText[1].Length - txt.SelectedText.Length >= intUomScale)
                                            e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
                //int intDecimalPart = 0;
                //if (dgvPurchase.EditingControl != null && !string.IsNullOrEmpty(dgvPurchase.EditingControl.Text) && dgvPurchase.EditingControl.Text.Contains("."))
                //{
                //    intDecimalPart = dgvPurchase.EditingControl.Text.Split('.')[1].Length;
                //}
                //int intUomScale = 0;
                //if (dgvPurchase[Uom.Index, dgvPurchase.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                //    intUomScale = MobjClsBLLPurchase.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPurchase[Uom.Index, dgvPurchase.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                //if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                //{
                //    e.Handled = true;
                //}
                //if (e.KeyChar.ToString() == "." && intUomScale == 0)
                //{
                //    e.Handled = true;
                //}
                //if (e.KeyChar != 08 && e.KeyChar != 13 && dgvPurchase.EditingControl != null && !string.IsNullOrEmpty(dgvPurchase.EditingControl.Text) && (dgvPurchase.EditingControl.Text.Contains(".") && ((e.KeyChar == 46) || intDecimalPart == intUomScale)))//checking more than one "."
                //{
                //    e.Handled = true;
                //}
            }
            else if (dgvPurchase.CurrentCell.OwningColumn.Name == "Rate")
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (dgvPurchase.EditingControl != null && !string.IsNullOrEmpty(dgvPurchase.EditingControl.Text) && dgvPurchase.EditingControl.Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true;
                }
            }
            else if (dgvPurchase.CurrentCell.OwningColumn.Name == "ItemCode" || dgvPurchase.CurrentCell.OwningColumn.Name == "ItemName")
            {
                if (e.KeyChar.ToString() == "'")
                {
                    e.Handled = true;
                }
            }
        }

        private void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
            {
                e.Handled = true;
            }
            if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            {
                e.Handled = true;
            }

        }

        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void dgvPurchase_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!blnIsFromDisplay)
            {
                try
                {
                    if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                    {
                        if ((dgvPurchase.Columns[e.ColumnIndex].Name == "Quantity" || dgvPurchase.Columns[e.ColumnIndex].Name == "ExtraQty" || dgvPurchase.Columns[e.ColumnIndex].Name == "Rate"))
                        {
                            decimal decValue = 0;
                            try
                            {
                                if (dgvPurchase[e.ColumnIndex, e.RowIndex].Value != null && dgvPurchase[e.ColumnIndex, e.RowIndex].Value.ToString() != string.Empty)
                                decValue = Convert.ToDecimal(dgvPurchase[e.ColumnIndex, e.RowIndex].Value);
                            }
                            catch
                            {
                                dgvPurchase[e.ColumnIndex, e.RowIndex].Value = 0;
                            }
                        }
                        //if (MiFromForm == 3 || MiFromForm == 4)
                        //{
                        if (dgvPurchase.Columns[e.ColumnIndex].Name == "ItemID" && e.RowIndex >= 0)
                        {
                            if (MintFromForm == 3)
                            {
                                if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromInvoice)
                                {
                                    DataTable DtPurInvoiceDetail = MobjClsBLLPurchase.DisplayPurchaseInvoiceDetail(Convert.ToInt64(cboOrderNo.SelectedValue));
                                    DtPurInvoiceDetail.DefaultView.RowFilter = "ItemID = " + dgvPurchase[ItemID.Index, e.RowIndex].Value.ToInt32();
                                    if (DtPurInvoiceDetail.DefaultView.ToTable().Rows.Count > 0)
                                    {
                                        DataRow dr = DtPurInvoiceDetail.DefaultView.ToTable().Rows[0];
                                        decimal decReceivedQty = 0;
                                        decReceivedQty = MobjClsBLLPurchase.GetReceivedQty(Convert.ToInt32(dr["ItemID"]), Convert.ToInt64(cboOrderNo.SelectedValue), true);
                                        if (decReceivedQty < Convert.ToDecimal(dr["ReceivedQuantity"]))
                                        {
                                            dgvPurchase.Rows[e.RowIndex].Cells["OrderedQty"].Value = dr["ReceivedQuantity"];
                                            FillComboColumn(Convert.ToInt32(dr["ItemID"]), 1);
                                            dgvPurchase.Rows[e.RowIndex].Cells["Uom"].Value = dr["UOMID"];
                                            dgvPurchase.Rows[e.RowIndex].Cells["Uom"].Tag = dr["UOMID"];
                                            dgvPurchase.Rows[e.RowIndex].Cells["Uom"].Value = dgvPurchase.Rows[e.RowIndex].Cells["Uom"].FormattedValue;


                                            dgvPurchase.Rows[e.RowIndex].Cells["ReceivedQty"].Value = decReceivedQty;

                                            dgvPurchase.Rows[e.RowIndex].Cells["Uom"].ReadOnly = true;
                                            //  dgvPurchase.Rows[e.RowIndex].Cells["IsAutofilled"].Value = 1;

                                            dgvPurchase.Rows[e.RowIndex].Cells["Rate"].Value = DtPurInvoiceDetail.Rows[0]["Rate"];
                                            dgvPurchase.Rows[e.RowIndex].Cells["PurchaseRate"].Value = DtPurInvoiceDetail.Rows[0]["PurchaseRate"];
                                        }
                                    }
                                }
                                if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromOrder)
                                {
                                    DataTable DtPurInvoiceDetail = MobjClsBLLPurchase.DisplayPurchaseOrderDetail(Convert.ToInt64(cboOrderNo.SelectedValue));
                                    DtPurInvoiceDetail.DefaultView.RowFilter = "ItemID = " + dgvPurchase[ItemID.Index, e.RowIndex].Value.ToInt32();
                                    if (DtPurInvoiceDetail.DefaultView.ToTable().Rows.Count > 0)
                                    {
                                        DataRow dr = DtPurInvoiceDetail.DefaultView.ToTable().Rows[0];
                                        decimal decReceivedQty = 0;
                                        decReceivedQty = MobjClsBLLPurchase.GetReceivedQty(Convert.ToInt32(dr["ItemID"]), Convert.ToInt64(cboOrderNo.SelectedValue), true);
                                        if (decReceivedQty < Convert.ToDecimal(dr["Quantity"]))
                                        {
                                            dgvPurchase.Rows[e.RowIndex].Cells["OrderedQty"].Value = dr["Quantity"];
                                            FillComboColumn(Convert.ToInt32(dr["ItemID"]), 1);
                                            dgvPurchase.Rows[e.RowIndex].Cells["Uom"].Value = dr["UOMID"];
                                            dgvPurchase.Rows[e.RowIndex].Cells["Uom"].Tag = dr["UOMID"];
                                            dgvPurchase.Rows[e.RowIndex].Cells["Uom"].Value = dgvPurchase.Rows[e.RowIndex].Cells["Uom"].FormattedValue;


                                            dgvPurchase.Rows[e.RowIndex].Cells["ReceivedQty"].Value = decReceivedQty;

                                            dgvPurchase.Rows[e.RowIndex].Cells["Uom"].ReadOnly = true;
                                            //  dgvPurchase.Rows[e.RowIndex].Cells["IsAutofilled"].Value = 1;

                                            dgvPurchase.Rows[e.RowIndex].Cells["Rate"].Value = DtPurInvoiceDetail.Rows[0]["Rate"];
                                            dgvPurchase.Rows[e.RowIndex].Cells["PurchaseRate"].Value = DtPurInvoiceDetail.Rows[0]["Rate"];
                                        }
                                    }
                                }

                            }
                            else if (MintFromForm == 4 && cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.PInvoiceFromOrder)
                            {
                                DataTable DtPurInvoiceDetail = MobjClsBLLPurchase.DisplayPurchaseOrderDetail(Convert.ToInt64(cboOrderNo.SelectedValue));
                                DtPurInvoiceDetail.DefaultView.RowFilter = "ItemID = " + dgvPurchase[ItemID.Index, e.RowIndex].Value.ToInt32();
                                DataRow dr = DtPurInvoiceDetail.DefaultView.ToTable().Rows[0];
                                decimal decReceivedQty = 0;
                                decReceivedQty = MobjClsBLLPurchase.GetReceivedQty(Convert.ToInt32(dr["ItemID"]), Convert.ToInt64(cboOrderNo.SelectedValue), false);
                                if (decReceivedQty < Convert.ToDecimal(dr["Quantity"]))
                                {
                                    dgvPurchase.Rows[e.RowIndex].Cells["OrderedQty"].Value = dr["Quantity"];
                                    FillComboColumn(Convert.ToInt32(dr["ItemID"]), 1);
                                    dgvPurchase.Rows[e.RowIndex].Cells["Uom"].Value = dr["UOMID"];
                                    dgvPurchase.Rows[e.RowIndex].Cells["Uom"].Tag = dr["UOMID"];
                                    dgvPurchase.Rows[e.RowIndex].Cells["Uom"].Value = dgvPurchase.Rows[e.RowIndex].Cells["Uom"].FormattedValue;

                                    dgvPurchase.Rows[e.RowIndex].Cells["Rate"].Value = dr["Rate"];
                                    if (dr["IsDiscountPercentage"] != DBNull.Value)
                                    {
                                        dgvPurchase.Rows[e.RowIndex].Cells["DiscountAmt"].Value = (Convert.ToBoolean(dr["IsDiscountPercentage"]) ? dr["DiscountPercentage"].ToDecimal() / 100 * dr["GrandAmount"].ToDecimal() : dr["DiscountAmount"].ToDecimal()); ;
                                        dgvPurchase.Rows[e.RowIndex].Cells["DiscountAmount"].Value = (Convert.ToBoolean(dr["IsDiscountPercentage"]) ? dr["DiscountPercentage"].ToDecimal() : dr["DiscountAmount"].ToDecimal()); ;
                                        dgvPurchase.Rows[e.RowIndex].Cells["Discount"].Value = (Convert.ToBoolean(dr["IsDiscountPercentage"]) ? "%" : "Amt");
                                    }
                                    //FillDiscountColumn(e.RowIndex, true);
                                    //if (dr["DiscountID"] != DBNull.Value && Convert.ToInt32(dr["DiscountID"]) != 0)
                                    //{
                                    //    dgvPurchase.Rows[e.RowIndex].Cells["Discount"].Value = dr["DiscountID"];
                                    //    dgvPurchase.Rows[e.RowIndex].Cells["Discount"].Tag = dr["DiscountID"];
                                    //    dgvPurchase.Rows[e.RowIndex].Cells["Discount"].Value = dgvPurchase.Rows[e.RowIndex].Cells["Discount"].FormattedValue;
                                    //}
                                    //dgvPurchase.Rows[e.RowIndex].Cells["DiscountAmount"].Value = dr["DiscountAmount"];
                                    // dgvPurchase.Rows[e.RowIndex].Cells["GrandAmount"].Value = dr["GrandAmount"];

                                    dgvPurchase.Rows[e.RowIndex].Cells["ReceivedQty"].Value = decReceivedQty;

                                    dgvPurchase.Rows[e.RowIndex].Cells["Uom"].ReadOnly = true;
                                    dgvPurchase.Rows[e.RowIndex].Cells["IsAutofilled"].Value = 1;
                                }
                            }
                            else
                            {
                                if (cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.POrderFromGRN)
                                {
                                    dgvPurchase[Rate.Index, e.RowIndex].Value = 0;
                                    dgvPurchase[PurchaseRate.Index, e.RowIndex].Value = 0;
                                    dgvPurchase[Quantity.Index, e.RowIndex].Value = 0;
                                }


                                CalculateTotalAmt();
                            }

                            if (dgvPurchase["ItemID", e.RowIndex].Value.ToInt32() != 0)
                            {

                                if (dgvPurchase.Rows[e.RowIndex].Cells["ItemID"].Value != null)
                                {
                                    if (MintFromForm == 3 || (MintFromForm == 4 && !ChkTGNR.Checked))
                                        dgvPurchase["Batchnumber", e.RowIndex].Value = dgvPurchase["ItemCode", e.RowIndex].Value.ToStringCustom() + ClsCommonSettings.GetServerDate().ToString("dd/MMM/yyyy") + random.Next(10000).ToString();

                                    int iItemID = dgvPurchase.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt32();
                                    int tag = -1;
                                    if (dgvPurchase.Rows[e.RowIndex].Cells["Uom"].Tag != null)
                                    {
                                        tag = Convert.ToInt32(dgvPurchase.Rows[e.RowIndex].Cells["Uom"].Tag);
                                    }
                                    FillComboColumn(iItemID, 1);
                                    if (tag == -1)
                                    {
                                        DataTable datDefaultUom = MobjClsBLLPurchase.FillCombos(new string[] { "DefaultPurchaseUomID as UomID", "InvItemMaster", "ItemID = " + iItemID + "" });
                                        tag = Convert.ToInt32(datDefaultUom.Rows[0]["UomID"]);
                                    }
                                    dgvPurchase.Rows[e.RowIndex].Cells["Uom"].Value = tag;
                                    dgvPurchase.Rows[e.RowIndex].Cells["Uom"].Tag = tag;
                                    dgvPurchase.Rows[e.RowIndex].Cells["Uom"].Value = dgvPurchase.Rows[e.RowIndex].Cells["Uom"].FormattedValue;


                                    //FillDiscountColumn(e.RowIndex, false);

                                }
                            }
                            else
                            {
                                Uom.DataSource = null;
                                Discount.DataSource = null;
                            }
                            // }
                        }



                        //if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                        //{
                        //    if (MintFromForm == 1 || (MintFromForm == 2 && cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.POrderFromQuotation) || (MintFromForm == 4 && cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.PInvoiceFromOrder))
                        //    {
                        //        if (e.ColumnIndex == Uom.Index || e.ColumnIndex == Quantity.Index || e.ColumnIndex == Rate.Index)
                        //        {
                        //            FillDiscountColumn(e.RowIndex, false);
                        //        }
                        //    }

                        //    //if (e.ColumnIndex == Quantity.Index || e.ColumnIndex == ExtraQty.Index && dgvPurchase.CurrentCell != null)
                        //    //{
                        //    //    int intDecimalPart = 0;
                        //    //    if (dgvPurchase.CurrentCell.Value != null && !string.IsNullOrEmpty(dgvPurchase.CurrentCell.Value.ToString()) && dgvPurchase.CurrentCell.Value.ToString().Contains("."))
                        //    //        intDecimalPart = dgvPurchase.CurrentCell.Value.ToString().Split('.')[1].Length;
                        //    //    int intUomScale = 0;
                        //    //    if (dgvPurchase[Uom.Index, dgvPurchase.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                        //    //        intUomScale = MobjClsBLLPurchase.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPurchase[Uom.Index, dgvPurchase.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                        //    //    if (intDecimalPart > intUomScale)
                        //    //    {
                        //    //        dgvPurchase.CurrentCell.Value = Math.Round(dgvPurchase.CurrentCell.Value.ToDecimal(), intUomScale).ToString();
                        //    //        dgvPurchase.EditingControl.Text = dgvPurchase.CurrentCell.Value.ToString();
                        //    //    }
                        //    //}
                        //}
                        Changestatus();
                    }
                }
                catch (Exception ex)
                {
                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error in dgvPurchase_CellValueChanged() " + ex.Message);
                    MObjLogs.WriteLog("Error in dgvPurchase_CellValuChanged() " + ex.Message, 2);
                }
            }
        }

        private void dgvPurchase_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvPurchase.IsCurrentCellDirty)
            {
                if (dgvPurchase.CurrentCell != null)
                    dgvPurchase.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
        }

        private void dgvPurchase_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                int iRowIndex = 0;
                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    switch (MintFromForm)
                    {
                        case 1:
                            if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.PQuotationFromRFQ)
                            {
                                if (dgvPurchase.Columns[e.ColumnIndex].Name != "Rate" && dgvPurchase.Columns[e.ColumnIndex].Name != "Quantity")
                                {
                                    e.Cancel = true;
                                    return;
                                }                             
                            }
                            break;
                        case 2:
                            if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.POrderFromQuotation)
                            {
                                if (dgvPurchase.Columns[e.ColumnIndex].Name != "Rate" && dgvPurchase.Columns[e.ColumnIndex].Name != "Quantity")
                                {
                                    e.Cancel = true;
                                    return;
                                }
                            }
                            break;
                        case 3:
                            if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromTransfer)
                            {
                                e.Cancel = true;
                                return;
                            }
                            else if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromInvoice)
                            {
                                if (dgvPurchase[IsAutofilled.Index, e.RowIndex].Value != null && dgvPurchase[IsAutofilled.Index, e.RowIndex].Value.ToString() == "1")
                                {
                                    if (dgvPurchase.Columns[e.ColumnIndex].Name != "ExpiryDate" && dgvPurchase.Columns[e.ColumnIndex].Name != "Batchnumber" && dgvPurchase.Columns[e.ColumnIndex].Name != "Quantity" && dgvPurchase.Columns[e.ColumnIndex].Name != "ExtraQty")
                                    {
                                        e.Cancel = true;
                                        return;
                                    }
                                }
                                if (MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID != 0)
                                {
                                    if (dgvPurchase.Columns[e.ColumnIndex].Name != "ExpiryDate" && dgvPurchase.Columns[e.ColumnIndex].Name != "Batchnumber" && dgvPurchase.Columns[e.ColumnIndex].Name != "Quantity" && dgvPurchase.Columns[e.ColumnIndex].Name != "ExtraQty")
                                    {
                                        if (dgvPurchase[OrderedQty.Index, e.RowIndex].Value != null && dgvPurchase[OrderedQty.Index, e.RowIndex].Value.ToDecimal() != 0)
                                        {
                                            e.Cancel = true;
                                            return;
                                        }
                                    }
                                }
                            }
                            //else
                            //{
                            //    if (e.ColumnIndex != Uom.Index && e.ColumnIndex != Quantity.Index)
                            //        e.Cancel = true;
                            //}
                            break;
                        case 4:
                            if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.PInvoiceFromOrder)
                            {
                                if (ChkTGNR.Checked)
                                {
                                    if (dgvPurchase[IsAutofilled.Index, e.RowIndex].Value != null && dgvPurchase[IsAutofilled.Index, e.RowIndex].Value.ToString() == "1")
                                    {
                                        if (dgvPurchase.Columns[e.ColumnIndex].Name != "ExpiryDate" && dgvPurchase.Columns[e.ColumnIndex].Name != "Quantity" && dgvPurchase.Columns[e.ColumnIndex].Name != "ExtraQty" && dgvPurchase.Columns[e.ColumnIndex].Name != "Batchnumber")
                                        {
                                            e.Cancel = true;
                                            return;
                                        }
                                    }
                                }
                                else
                                {
                                    if (dgvPurchase.Columns[e.ColumnIndex].Name != "ExpiryDate" && dgvPurchase.Columns[e.ColumnIndex].Name != "Quantity" && dgvPurchase.Columns[e.ColumnIndex].Name != "ExtraQty" && dgvPurchase.Columns[e.ColumnIndex].Name != "Batchnumber")
                                    {
                                        e.Cancel = true;
                                        return;
                                    }

                                }
                            }
                            break;
                    }

                    dgvPurchase.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    iRowIndex = dgvPurchase.CurrentCell.RowIndex;
                    if (e.ColumnIndex == Quantity.Index || e.ColumnIndex == ExtraQty.Index || e.ColumnIndex == Uom.Index || e.ColumnIndex == Batchnumber.Index || e.ColumnIndex == Rate.Index || e.ColumnIndex == Discount.Index)
                    {
                        if (Convert.ToString(dgvPurchase.CurrentRow.Cells[0].Value).Trim() == "")
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 915, out MmessageIcon);
                            ErrPurchase.SetError(dgvPurchase, MstrMessageCommon.Replace("#", "").Trim());
                            e.Cancel = true;
                        }
                    }

                    if (e.ColumnIndex == Uom.Index || e.ColumnIndex == Discount.Index)
                    {
                        if (dgvPurchase.CurrentRow.Cells["ItemID"].Value != null)
                        {
                            int iItemID = Convert.ToInt32(dgvPurchase.CurrentRow.Cells["ItemID"].Value);
                            int tag = -1;
                            if (e.ColumnIndex == Uom.Index)
                            {
                                if (dgvPurchase.CurrentRow.Cells["Uom"].Tag != null)
                                {
                                    tag = Convert.ToInt32(dgvPurchase.CurrentRow.Cells["Uom"].Tag);
                                }
                                FillComboColumn(iItemID, 1);
                                if (tag != -1)
                                    dgvPurchase.CurrentRow.Cells["Uom"].Tag = tag;
                            }
                            else if (e.ColumnIndex == Discount.Index)
                            {
                                if (dgvPurchase.CurrentRow.Cells["Discount"].Tag != null)
                                {
                                    tag = Convert.ToInt32(dgvPurchase.CurrentRow.Cells["Discount"].Tag);
                                }
                                //if (MintFromForm == 1 || (MintFromForm == 2 && cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.POrderFromQuotation) || (MintFromForm == 4 && cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.PInvoiceFromOrder))
                                //{
                                //    FillDiscountColumn(e.RowIndex, false);
                                //}
                                //else
                                //    FillDiscountColumn(e.RowIndex, true);
                                if (tag != -1)
                                    dgvPurchase.CurrentRow.Cells["Discount"].Tag = tag;
                            }
                        }
                        else
                        {
                            Uom.DataSource = null;
                            Discount.DataSource = null;
                        }

                    }
                    //else if (e.ColumnIndex == DiscountAmount.Index)
                    //{
                    //    if (dgvPurchase.Rows[e.RowIndex].Cells[Discount.Index].Tag.ToInt32() == (int)PredefinedDiscounts.DefaultPurchaseDiscount && !Discount.ReadOnly)
                    //        e.Cancel = false;
                    //    else
                    //        e.Cancel = true;
                    //}
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvPurchase_CellBeginEdit() " + ex.Message);
                MObjLogs.WriteLog("Error in dgvPurchase_CellBeginEdit() " + ex.Message, 2);
            }
        }

        private void cboOrderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.lblOrder.Visible = this.lblOrder1.Visible = this.lblOrderNo.Visible = this.lblOrderNo1.Visible = false;
            try
            {
                switch (MintFromForm)
                {
                    case 1:
                        if (Convert.ToInt32(cboCompany.SelectedValue) > 0 && cboOrderType.SelectedValue != null)
                        {
                            if ((Int32)cboOrderType.SelectedValue == (Int32)OperationOrderType.PQuotationFromRFQ)
                            {
                                cboOrderNo.Visible = true;
                                cboOrderNo.DataSource = null;
                                DataTable dtCombo = null;
                                if (ClsCommonSettings.RFQApproval)
                                    dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "RFQID,RFQNo", "InvRFQMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) + " and InvRFQMaster.StatusID = " + Convert.ToInt32(RFQStatus.Approved) });
                                else
                                    dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "RFQID,RFQNo", "InvRFQMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) + " and InvRFQMaster.StatusID = " + Convert.ToInt32(RFQStatus.Submitted) });
                                cboOrderNo.ValueMember = "RFQID";
                                cboOrderNo.DisplayMember = "RFQNo";
                                cboOrderNo.DataSource = dtCombo;
                            }
                            else
                            {
                                cboOrderNo.Visible = false;
                                cboOrderNo.DataSource = null;
                                dgvPurchase.ReadOnly = false;
                                GrandAmount.ReadOnly = NetAmount.ReadOnly = PurchaseRate.ReadOnly = true;
                            }
                        }
                        break;
                    case 2:
                        if (Convert.ToInt32(cboCompany.SelectedValue) > 0 && cboOrderType.SelectedValue != null)
                        {
                            if ((Int32)cboOrderType.SelectedValue == (Int32)OperationOrderType.POrderFromIndent)
                            {
                                MblnIsEditable = true;
                                cboOrderNo.Visible = true;
                                clbGRN.Visible = false;
                                cboOrderNo.DataSource = null;
                                DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "purchaseindentID,PurchaseIndentNo", "STPurchaseIndentMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) + " and STPurchaseIndentMaster.StatusID = " + Convert.ToInt32(OperationStatusType.PIndentOpened) + "", "purchaseindentID", "PurchaseIndentNo" });
                                cboOrderNo.ValueMember = "purchaseindentID";
                                cboOrderNo.DisplayMember = "PurchaseIndentNo";
                                cboOrderNo.DataSource = dtCombo;
                                lblGRNWarehouse.Visible = btnGRNWarehouse.Visible = cboGRNWarehouse.Visible = false;
                            }
                            else if ((Int32)cboOrderType.SelectedValue == (Int32)OperationOrderType.POrderFromQuotation)
                            {
                                MblnIsEditable = false;
                                clbGRN.Visible = false;
                                ArrangeControls();
                                if (MblnAddStatus)
                                {
                                    btnActions.Enabled = true;
                                    btnAddExpense.Enabled = true;
                                }
                                dtpDueDate.Enabled = false;
                                cboOrderNo.Visible = true;
                                cboOrderNo.DataSource = null;
                                DataTable dtCombo = null;
                                if (ClsCommonSettings.PQApproval)
                                    dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseQuotationID,PurchaseQuotationNo", "InvPurchaseQuotationMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) + " and InvPurchaseQuotationMaster.StatusID = " + Convert.ToInt32(OperationStatusType.PQuotationApproved) + "", "PurchaseQuotationID", "PurchaseQuotationNo" });
                                else
                                    dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseQuotationID,PurchaseQuotationNo", "InvPurchaseQuotationMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) + " and InvPurchaseQuotationMaster.StatusID = " + Convert.ToInt32(OperationStatusType.PQuotationSubmitted) + "", "PurchaseQuotationID", "PurchaseQuotationNo" });
                                cboOrderNo.ValueMember = "PurchaseQuotationID";
                                cboOrderNo.DisplayMember = "PurchaseQuotationNo";
                                cboOrderNo.DataSource = dtCombo;
                                cboCurrency.Enabled = false;
                                cboSupplier.Enabled = false;
                                cboPaymentTerms.Enabled = false;
                                // btnSupplier.Enabled = false;
                                btnAddressChange.Enabled = false;
                                cboDiscount.Enabled = false;
                                lblGRNWarehouse.Visible = btnGRNWarehouse.Visible = cboGRNWarehouse.Visible = false;

                                dgvPurchase.ReadOnly = false;
                                Quantity.ReadOnly = false;
                                Rate.ReadOnly = false;
                                Discount.ReadOnly = false;
                                ItemCode.ReadOnly = ItemName.ReadOnly = NetAmount.ReadOnly =Uom.ReadOnly=GrandAmount.ReadOnly= true;
                             
                            }
                            else if ((Int32)cboOrderType.SelectedValue == (Int32)OperationOrderType.POrderFromGRN)
                            {
                                lblGRNWarehouse.Visible = btnGRNWarehouse.Visible = cboGRNWarehouse.Visible = true;
                              
                                //cboGRNWarehouse.DataSource = null;
                                //DataTable dt = MobjClsBLLPurchase.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) + "", "WarehouseID", "WarehouseName" });
                                //cboGRNWarehouse.ValueMember = "WarehouseID";
                                //cboGRNWarehouse.DisplayMember = "WarehouseName";
                                //cboGRNWarehouse.DataSource = dt;

                                ((ListBox)(clbGRN)).DataSource = null;
                                dgvPurchase.Rows.Clear();
                                dgvPurchase.AllowUserToDeleteRows = false;
       
                                ItemCode.ReadOnly = ItemName.ReadOnly = false;
                                MblnIsEditable = true;
                                clbGRN.Visible = true;
                                cboSupplier.Enabled = false;

                                if (MblnAddStatus)
                                {
                                    btnActions.Enabled = false;
                                    btnAddExpense.Enabled = false;
                                    dtpDueDate.Enabled = true;
                                    dgvPurchase.ReadOnly = false;
                                    txtPurchaseNo.Enabled = true; 
                                    GrandAmount.ReadOnly = NetAmount.ReadOnly = PurchaseRate.ReadOnly = true;
                                    cboSupplier.Enabled = true;
                                    ChkTGNR.Enabled = false;
                                    cboPurchaseAccount.Enabled = true;
                                    //clbGRN.SelectedItems.Clear();
                                }
                                else
                                {
                                    dgvPurchase.Rows.Clear();
                                   // txtPurchaseNo.Enabled = false;
                                    cboSupplier.Enabled = false;
                                    cboDiscount.SelectedIndex = 0;
                                }
                                DataTable dtCombo = null;
                                    //dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "GRNID,GRNNo", "InvGRNMaster", "CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + " And VendorID =" + Convert.ToInt32(cboSupplier.SelectedValue) + "and OrderTypeID = " + (Int32)OperationOrderType.GRNDirect });
                                    dtCombo = MobjClsBLLPurchase.GetGRNNumbers(cboCompany.SelectedValue.ToInt32(), cboSupplier.SelectedValue.ToInt32(), (Int32)OperationOrderType.GRNDirect,MblnAddStatus,cboGRNWarehouse.SelectedValue.ToInt32());

                                ((ListBox)clbGRN).DataSource = dtCombo;
                                ((ListBox)clbGRN).ValueMember = "GRNID";
                                ((ListBox)clbGRN).DisplayMember = "GRNNo";

                                Quantity.ReadOnly = true;
                                Uom.ReadOnly = true;
                                cboOrderNo.Visible = false;
                                cboOrderNo.DataSource = null;
                                cboCurrency.Enabled = true;
                                dgvPurchase.ReadOnly = false;
                             
                                cboPaymentTerms.Enabled = true;
                              
                                btnAddressChange.Enabled = true;
                                cboDiscount.Enabled = true;
                                ClearBottomControls();
                                SetControlVisibilityForBottomPanel(true);
                            }
                            else
                            {
                                dgvPurchase.Rows.Clear();
                                dgvPurchase.AllowUserToDeleteRows = true;
                                cboDiscount.SelectedIndex = 0;
                         
                                ItemCode.ReadOnly = ItemName.ReadOnly = Quantity.ReadOnly =Uom.ReadOnly= false;
                                MblnIsEditable = true;
                                clbGRN.Visible = false;
                                if (MblnAddStatus)
                                {
                                    btnActions.Enabled = false;
                                    btnAddExpense.Enabled = false;
                                    dtpDueDate.Enabled = true;
                                    dgvPurchase.ReadOnly = false;
                                    GrandAmount.ReadOnly = NetAmount.ReadOnly = PurchaseRate.ReadOnly = true;
                                }
                                cboOrderNo.Visible = false;
                                cboOrderNo.DataSource = null;
                                cboCurrency.Enabled = true;
                                cboSupplier.Enabled = true;
                                cboPaymentTerms.Enabled = true;
                                btnAddressChange.Enabled = true;
                                cboDiscount.Enabled = true;
                                ClearBottomControls();
                                SetControlVisibilityForBottomPanel(true);
                                SetPurchaseGridWidth();
                                lblGRNWarehouse.Visible = btnGRNWarehouse.Visible = cboGRNWarehouse.Visible = false;
                            }
                        }
                        break;
                    case 3:
                        if (Convert.ToInt32(cboCompany.SelectedValue) > 0 && cboOrderType.SelectedValue != null)
                        {
                            if ((Int32)cboOrderType.SelectedValue == (Int32)OperationOrderType.GRNFromInvoice)
                            {
                                cboOrderNo.Visible = true;
                                cboOrderNo.DataSource = null;
                                DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseInvoiceID,PurchaseInvoiceNo", "" +
                                    "InvPurchaseInvoiceMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) + " and " +
                                    "InvPurchaseInvoiceMaster.StatusID = " + Convert.ToInt32(OperationStatusType.PInvoiceOpened) + " " + 
                                    "And InvPurchaseInvoiceMaster.IsGRNRequired = 1  AND InvPurchaseInvoiceMaster.PurchaseOrderID NOT IN (" +
                                    "SELECT DISTINCT ReferenceID FROM InvGRNMaster WHERE OrderTypeID = " + (int)OperationOrderType.GRNFromOrder + ")" });
                                cboOrderNo.ValueMember = "PurchaseInvoiceID";
                                cboOrderNo.DisplayMember = "PurchaseInvoiceNo";
                                cboOrderNo.DataSource = dtCombo;
                                MblnIsEditable = false;
                                OrderedQty.HeaderText = "Invoiced Qty";
                                ReceivedQty.HeaderText = "Received Qty";
                                OrderedQty.Visible = ExtraQty.Visible = ReceivedQty.Visible = true;
                                Rate.Visible = PurchaseRate.Visible = false;
                                dgvPurchase.AllowUserToAddRows = true;
                                dgvPurchase.AllowUserToDeleteRows = true;
                                Batchnumber.ReadOnly = false;
                                ExpiryDate.ReadOnly = false;
                                Quantity.ReadOnly = false;
                                Uom.ReadOnly = true;
                                lblWarehouse.Enabled = cboWarehouse.Enabled = btnWarehouse.Visible = btnWarehouse.Enabled = false;

                                OrderedQty.Visible = ReceivedQty.Visible = ExtraQty.Visible = ExpiryDate.Visible = true;
                                SetPurchaseGridWidth();
                            }
                            else if (cboOrderType.SelectedValue.ToInt32() == (Int32)OperationOrderType.GRNFromTransfer)
                            {
                                cboOrderNo.Visible = true;
                                cboOrderNo.DataSource = null;
                                DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "StockTransferID,StockTransferNo", "InvStockTransferMaster", "ReferenceID In (Select WarehouseID From InvWarehouse Where CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + ") and StatusID = " + Convert.ToInt32(OperationStatusType.STIssued) + " And OrderTypeID In (" + (int)OperationOrderType.STWareHouseToWareHouseTransfer + "," + (int)OperationOrderType.STWareHouseToWareHouseTransferDemo + ")" });
                                cboOrderNo.ValueMember = "StockTransferID";
                                cboOrderNo.DisplayMember = "StockTransferNo";
                                cboOrderNo.DataSource = dtCombo;
                                MblnIsEditable = false;

                                OrderedQty.Visible = ExtraQty.Visible = ReceivedQty.Visible = false;


                                Rate.Visible = false;
                                PurchaseRate.Visible = false;
                                dgvPurchase.AllowUserToAddRows = false;
                                dgvPurchase.AllowUserToDeleteRows = false;
                                Batchnumber.ReadOnly = true;
                                ExpiryDate.ReadOnly = true;
                                Quantity.ReadOnly = true;
                                Uom.ReadOnly = true;
                                lblWarehouse.Enabled = cboWarehouse.Enabled = btnWarehouse.Visible = btnWarehouse.Enabled = false;

                                OrderedQty.Visible = ReceivedQty.Visible = ExtraQty.Visible = ExpiryDate.Visible = false;
                                SetPurchaseGridWidth();
                            }
                            else if (cboOrderType.SelectedValue.ToInt32() == (Int32)OperationOrderType.GRNFromOrder)
                            {
                                cboOrderNo.Visible = true;
                                cboOrderNo.DataSource = null;
                                //DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseOrderID,PurchaseOrderNo", "InvPurchaseOrderMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue)
                                //    + " And (InvPurchaseOrderMaster.StatusID = " + Convert.ToInt32(OperationStatusType.POrderApproved) +"OR InvPurchaseOrderMaster.StatusID = "+Convert.ToInt32(OperationStatusType.POrderSubmitted)+")"
                                //    + " and " +
                                //    "InvPurchaseOrderMaster.ReceivedStatus <> " + Convert.ToInt32(OperationStatusType.POReceived)
                                //    + " ", "PurchaseOrderID", "PurchaseOrderNo" 
                                //    + " And InvPurchaseOrderMaster.IsGRNRequired = 1 " 
                                //   });
                                DataTable dtCombo = MobjClsBLLPurchase.GetPurchaseOrder(Convert.ToInt32(cboCompany.SelectedValue));
                                cboOrderNo.ValueMember = "PurchaseOrderID";
                                cboOrderNo.DisplayMember = "PurchaseOrderNo";
                                cboOrderNo.DataSource = dtCombo;
                                MblnIsEditable = false;
                                OrderedQty.HeaderText = "Ordered Qty";
                                ReceivedQty.HeaderText = "Received Qty";
                                OrderedQty.Visible = ExtraQty.Visible = ReceivedQty.Visible = true;
                                Rate.Visible = PurchaseRate.Visible = false;
                                dgvPurchase.AllowUserToAddRows = true;
                                dgvPurchase.AllowUserToDeleteRows = true;
                                Batchnumber.ReadOnly = false;
                                ExpiryDate.ReadOnly = false;
                                Quantity.ReadOnly = false;
                                Uom.ReadOnly = true;
                                lblWarehouse.Enabled = cboWarehouse.Enabled = btnWarehouse.Visible = btnWarehouse.Enabled = true;

                                OrderedQty.Visible = ReceivedQty.Visible = ExtraQty.Visible = ExpiryDate.Visible = true;
                                SetPurchaseGridWidth();
                            }

                        }
                        break;

                    case 4:
                        if (Convert.ToInt32(cboCompany.SelectedValue) > 0 && cboOrderType.SelectedValue != null)
                        {
                            ChkTGNR.Checked = true;
                            if ((Int32)cboOrderType.SelectedValue == (Int32)OperationOrderType.PInvoiceFromOrder)
                            {
                                if (MblnAddStatus)
                                {
                                    btnActions.Enabled = true;
                                    btnAddExpense.Enabled = true;
                                    cboPurchaseAccount.Enabled = true;
                                }
                                cboOrderNo.Visible = true;
                                cboOrderNo.DataSource = null;
                                DataTable dtCombo = null;
                                if (ClsCommonSettings.POApproval)
                                    dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseOrderID,PurchaseOrderNo", "InvPurchaseOrderMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) + " And InvPurchaseOrderMaster.StatusID = " + Convert.ToInt32(OperationStatusType.POrderApproved) + " ", "PurchaseOrderID", "PurchaseOrderNo" });
                                else
                                    dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseOrderID,PurchaseOrderNo", "InvPurchaseOrderMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) + " And InvPurchaseOrderMaster.StatusID = " + Convert.ToInt32(OperationStatusType.POrderSubmitted) + " ", "PurchaseOrderID", "PurchaseOrderNo" });
                                cboOrderNo.ValueMember = "PurchaseOrderID";
                                cboOrderNo.DisplayMember = "PurchaseOrderNo";
                                cboOrderNo.DataSource = dtCombo;

                                OrderedQty.Visible = true;
                                ReceivedQty.Visible = true;

                                ChkTGNR.Enabled = false;
                                cboCurrency.Enabled = false;
                                cboSupplier.Enabled = false;
                                cboPaymentTerms.Enabled = false;
                                //cboPurchaseAccount.Enabled = false;
                                //  btnSupplier.Enabled = false;
                                btnAddressChange.Enabled = false;
                                cboDiscount.Enabled = false;
                                MblnIsEditable = false;
                                SetPurchaseGridWidth();
                            }
                            else
                            {
                                dgvPurchase.Columns["ItemCode"].ReadOnly = dgvPurchase.Columns["ItemName"].ReadOnly = false;
                                dgvPurchase.Columns["Batchnumber"].ReadOnly = false;
                                if (MblnAddStatus)
                                {
                                    btnActions.Enabled = false;
                                    btnAddExpense.Enabled = false;
                                    dgvPurchase.ReadOnly = false;
                                    GrandAmount.ReadOnly = NetAmount.ReadOnly = PurchaseRate.ReadOnly = true;
                                }
                                cboOrderNo.Visible = false;
                                cboOrderNo.DataSource = null;

                                OrderedQty.Visible = false;
                                ReceivedQty.Visible = false;

                                if (MblnAddStatus)
                                    ChkTGNR.Enabled = true;
                                else
                                    ChkTGNR.Enabled = false;

                                cboCurrency.Enabled = true;
                                cboSupplier.Enabled = true;
                                cboPaymentTerms.Enabled = true;
                                cboPurchaseAccount.Enabled = true;
                                btnAddressChange.Enabled = true;
                                cboDiscount.Enabled = true;
                                MblnIsEditable = true;
                                SetPurchaseGridWidth();
                            }
                        }
                        break;
                }
                Changestatus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in cboOrderType_SelectedIndexChanged() " + ex.Message);
                MObjLogs.WriteLog("Error in cboOrderType_SelectedIndexChanged() " + ex.Message, 2);
            }
        }

        private void cboOrderNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (MblnAddStatus == false) return;
                dgvPurchase.Rows.Clear();
                txtAdvPayment.Text = txtExpenseAmount.Text = new decimal(0).ToString("F" + MintExchangeCurrencyScale);
                CalculateTotalAmt();
                CalculateNetTotal();

                switch (MintFromForm)
                {
                    case 1:
                        if (Convert.ToInt64(cboOrderNo.SelectedValue) > 0)
                        {
                            if (!MblnIsFromClear && MblnAddStatus && Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.PQuotationFromRFQ)
                            {
                                if (MobjClsBLLPurchase.FillCombos(new string[] { "RFQID", "InvRFQMaster", "RFQID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID In ( " + (Int32)RFQStatus.Approved + "," + (Int32)RFQStatus.Submitted + ")" }).Rows.Count == 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 961, out MmessageIcon);
                                    MstrMessageCommon = MstrMessageCommon.Replace("*", "RFQ");
                                    ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrPurchase.Enabled = true;
                                    cboOrderNo.Focus();
                                    return;
                                }
                            }

                            if (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.PQuotationFromRFQ && MobjClsBLLPurchase.DisplayRFQInfo(Convert.ToInt64(cboOrderNo.SelectedValue)))
                            {

                                cboCompany.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID;
                                if (cboCompany.SelectedValue == null)
                                {
                                    DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID });
                                    if (datTemp.Rows.Count > 0)
                                        cboCompany.Text = datTemp.Rows[0]["Name"].ToString();
                                }

                                dgvPurchase.Rows.Clear();
                                DataTable DtPurOrderDetail = null;

                                DtPurOrderDetail = MobjClsBLLPurchase.DisplayRFQDetail(Convert.ToInt64(cboOrderNo.SelectedValue));
                                if (dgvPurchase.Rows.Count > 0)
                                {

                                    for (int i = 0; i < DtPurOrderDetail.Rows.Count; i++)
                                    {
                                        dgvPurchase.RowCount = dgvPurchase.RowCount + 1;
                                        dgvPurchase.Rows[i].Cells["ItemCode"].Value = DtPurOrderDetail.Rows[i]["ItemCode"];
                                        dgvPurchase.Rows[i].Cells["ItemName"].Value = DtPurOrderDetail.Rows[i]["Itemname"];
                                        //dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["Quantity"];
                                        int iItemID = Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]);

                                        FillComboColumn(Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]), 1);
                                        dgvPurchase.Rows[i].Cells["Uom"].Value = DtPurOrderDetail.Rows[i]["UOMID"];
                                        dgvPurchase.Rows[i].Cells["Uom"].Tag = DtPurOrderDetail.Rows[i]["UOMID"];
                                        dgvPurchase.Rows[i].Cells["Uom"].Value = dgvPurchase.Rows[i].Cells["Uom"].FormattedValue;

                                        int intUomScale = 0;
                                        if (dgvPurchase[Uom.Index, i].Tag.ToInt32() != 0)
                                            intUomScale = MobjClsBLLPurchase.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPurchase[Uom.Index, i].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                                        dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["Quantity"].ToDecimal().ToString("F" + intUomScale);

                                        dgvPurchase.Rows[i].Cells["ItemID"].Value = DtPurOrderDetail.Rows[i]["ItemID"];
                                        dgvPurchase.Rows[i].Cells["IsAutofilled"].Value = 1;

                                    }
                                }

                                DtPurOrderDetail = null;
                                MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID = 0;
                                SetPurchaseStatus();
                            }
                        }
                        break;

                    case 2:
                        if (Convert.ToInt64(cboOrderNo.SelectedValue) > 0)
                        {
                            if (!MblnIsFromClear && MblnAddStatus && Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.POrderFromIndent)
                            {
                                if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseIndentID", "STPurchaseIndentMaster", "PurchaseIndentID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID = " + (Int32)OperationStatusType.PIndentOpened }).Rows.Count == 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 960, out MmessageIcon);
                                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Purchase Indent");
                                    ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrPurchase.Enabled = true;
                                    cboOrderNo.Focus();
                                    return;
                                }
                            }
                            if (!MblnIsFromClear && MblnAddStatus && Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.POrderFromQuotation)
                            {
                                if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseQuotationID", "InvPurchaseQuotationMaster", "PurchaseQuotationID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.PQuotationApproved + "," + (int)OperationStatusType.PQuotationSubmitted + ")" }).Rows.Count == 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 961, out MmessageIcon);
                                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation");
                                    ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrPurchase.Enabled = true;
                                    cboOrderNo.Focus();
                                    return;
                                }
                            }

                            if (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.POrderFromIndent && MobjClsBLLPurchase.DisplayPurchaseIndentInfo(Convert.ToInt64(cboOrderNo.SelectedValue)))
                            {

                                cboCompany.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID;
                                if (cboCompany.SelectedValue == null)
                                {
                                    DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID });
                                    if (datTemp.Rows.Count > 0)
                                        cboCompany.Text = datTemp.Rows[0]["Name"].ToString();
                                }

                                cboSupplier.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID;
                                if (cboSupplier.SelectedValue == null)
                                    cboSupplier.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVendorName;
                                AddToAddressMenu(MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID);

                                MintVendorAddID = MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorAddID;
                                lblSupplierAddressName.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strAddressName + " Address";
                                DisplayAddress(Convert.ToInt32(cboSupplier.SelectedValue));
                                cboPaymentTerms.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intPaymentTermsID;
                                cboCurrency.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCurrencyID;
                                cboDiscount.SelectedIndex = (MobjClsBLLPurchase.PobjClsDTOPurchase.blnGrandDiscount.ToBoolean() ? 0 : 1);
                                if (Convert.ToString(cboDiscount.SelectedItem) == "%")
                                    txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountPercent.ToString();
                                else
                                    txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountAmt.ToString();
                               // cboDiscount.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intGrandDiscountID;
                                dgvPurchase.Rows.Clear();
                                DataTable DtPurOrderDetail = null;

                                DtPurOrderDetail = MobjClsBLLPurchase.DisplayPurchaseIndentDetail(Convert.ToInt64(cboOrderNo.SelectedValue));
                                if (dgvPurchase.Rows.Count > 0)
                                {

                                    for (int i = 0; i < DtPurOrderDetail.Rows.Count; i++)
                                    {
                                        dgvPurchase.RowCount = dgvPurchase.RowCount + 1;
                                        dgvPurchase.Rows[i].Cells["ItemCode"].Value = DtPurOrderDetail.Rows[i]["ItemCode"];
                                        dgvPurchase.Rows[i].Cells["ItemName"].Value = DtPurOrderDetail.Rows[i]["Itemname"];
                                        dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["Quantity"];
                                        int iItemID = Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]);
                                        FillComboColumn(Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]), 1);
                                        dgvPurchase.Rows[i].Cells["Uom"].Value = DtPurOrderDetail.Rows[i]["UOMID"];
                                        dgvPurchase.Rows[i].Cells["Uom"].Tag = DtPurOrderDetail.Rows[i]["UOMID"];
                                        dgvPurchase.Rows[i].Cells["Uom"].Value = dgvPurchase.Rows[i].Cells["Uom"].FormattedValue;

                                        dgvPurchase.Rows[i].Cells["ItemID"].Value = DtPurOrderDetail.Rows[i]["ItemID"];
                                        dgvPurchase.Rows[i].Cells["IsAutofilled"].Value = 1;

                                    }
                                }

                                DtPurOrderDetail = null;
                                MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID = 0;
                                SetPurchaseStatus();
                            }

                            else if (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.POrderFromQuotation)
                            {
                                int intOldStatusID = MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID;
                                if (MobjClsBLLPurchase.DisplayPurchaseQuotationInfo(Convert.ToInt64(cboOrderNo.SelectedValue)))
                                {
                                    MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = intOldStatusID;
                                    cboCompany.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID;
                                    if (cboCompany.SelectedValue == null)
                                    {
                                        DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID });
                                        if (datTemp.Rows.Count > 0)
                                            cboCompany.Text = datTemp.Rows[0]["CompanyName"].ToString();
                                    }
                                    dtpDueDate.Enabled = false;
                                    cboSupplier.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID;
                                    if (cboSupplier.SelectedValue == null)
                                        cboSupplier.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVendorName;

                                    AddToAddressMenu(MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID);

                                    MintVendorAddID = MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorAddID;
                                    lblSupplierAddressName.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strAddressName + " Address";
                                    DisplayAddress(Convert.ToInt32(cboSupplier.SelectedValue));
                                    cboPaymentTerms.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intPaymentTermsID;
                                    cboCurrency.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCurrencyID;
                                    cboDiscount.SelectedIndex = (MobjClsBLLPurchase.PobjClsDTOPurchase.blnGrandDiscount.ToBoolean() ? 0 : 1);
                                    if (Convert.ToString(cboDiscount.SelectedItem) == "%")
                                        txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountPercent.ToString();
                                    else
                                        txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountAmt.ToString();
                                   // cboDiscount.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intGrandDiscountID;
                                    txtExpenseAmount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decExpenseAmount.ToString("F" + MintExchangeCurrencyScale);
                                    //-----------------
                                    // cboIncoTerms.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intIncoTerms;
                                    //txtLeadTime.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decLeadTime.ToString();
                                    //dtpProductionDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strProductionDate);
                                    //dtpTrnsShipmentDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strTranShipmentDate);
                                    //dtpClearenceDate.Value = Convert.ToDateTime(MobjClsBLLPurchase.PobjClsDTOPurchase.strClearenceDate);
                                    //cboContainerType.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intContainerTypeID;
                                    //txtNoOfContainers.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.intNoOfContainers.ToString();
                                    ////--------------------

                                    //dgvPurchase.Rows.Clear();
                                    DataTable DtPurOrderDetail = null;

                                    DtPurOrderDetail = MobjClsBLLPurchase.DisplayPurchaseQuotationDetail(Convert.ToInt64(cboOrderNo.SelectedValue));
                                    if (dgvPurchase.Rows.Count > 0)
                                    {

                                        for (int i = 0; i < DtPurOrderDetail.Rows.Count; i++)
                                        {
                                            dgvPurchase.RowCount = dgvPurchase.RowCount + 1;
                                            dgvPurchase.Rows[i].Cells["ItemID"].Value = DtPurOrderDetail.Rows[i]["ItemID"];
                                            dgvPurchase.Rows[i].Cells["ItemCode"].Value = DtPurOrderDetail.Rows[i]["ItemCode"];
                                            dgvPurchase.Rows[i].Cells["ItemName"].Value = DtPurOrderDetail.Rows[i]["Itemname"];
                                            //   dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["Quantity"];
                                            int iItemID = Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]);

                                            FillComboColumn(Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]), 1);
                                            dgvPurchase.Rows[i].Cells["Uom"].Value = DtPurOrderDetail.Rows[i]["UOMID"];
                                            dgvPurchase.Rows[i].Cells["Uom"].Tag = DtPurOrderDetail.Rows[i]["UOMID"];
                                            dgvPurchase.Rows[i].Cells["Uom"].Value = dgvPurchase.Rows[i].Cells["Uom"].FormattedValue;

                                            int intUomScale = 0;
                                            if (dgvPurchase[Uom.Index, i].Tag.ToInt32() != 0)
                                                intUomScale = MobjClsBLLPurchase.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPurchase[Uom.Index, i].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                                            dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["Quantity"].ToDecimal().ToString("F" + intUomScale);

                                            dgvPurchase.Rows[i].Cells["Rate"].Value = DtPurOrderDetail.Rows[i]["Rate"];
                                            dgvPurchase.Rows[i].Cells["GrandAmount"].Value = DtPurOrderDetail.Rows[i]["GrandAmount"];
                                            //FillDiscountColumn(i, true);
                                            //if (DtPurOrderDetail.Rows[i]["DiscountID"] != DBNull.Value && Convert.ToInt32(DtPurOrderDetail.Rows[i]["DiscountID"]) != 0)
                                            //{
                                            //    dgvPurchase.Rows[i].Cells["Discount"].Value = DtPurOrderDetail.Rows[i]["DiscountID"];
                                            //    dgvPurchase.Rows[i].Cells["Discount"].Tag = DtPurOrderDetail.Rows[i]["DiscountID"];
                                            //    dgvPurchase.Rows[i].Cells["Discount"].Value = dgvPurchase.Rows[i].Cells["Discount"].FormattedValue;
                                            //}
                                            if (DtPurOrderDetail.Rows[i]["IsDiscountPercentage"] != DBNull.Value)
                                            {
                                                dgvPurchase.Rows[i].Cells["DiscountAmt"].Value = (Convert.ToBoolean(DtPurOrderDetail.Rows[i]["IsDiscountPercentage"]) ? DtPurOrderDetail.Rows[i]["DiscountPercentage"].ToDecimal() / 100 * DtPurOrderDetail.Rows[i]["GrandAmount"].ToDecimal() : DtPurOrderDetail.Rows[i]["DiscountAmount"].ToDecimal()); ;
                                                dgvPurchase.Rows[i].Cells["DiscountAmount"].Value = (Convert.ToBoolean(DtPurOrderDetail.Rows[i]["IsDiscountPercentage"]) ? DtPurOrderDetail.Rows[i]["DiscountPercentage"].ToDecimal() : DtPurOrderDetail.Rows[i]["DiscountAmount"].ToDecimal()); ;
                                                dgvPurchase.Rows[i].Cells["Discount"].Value = (Convert.ToBoolean(DtPurOrderDetail.Rows[i]["IsDiscountPercentage"]) ? "%" : "Amt");
                                            }
                                            //dgvPurchase.Rows[i].Cells["DiscountAmount"].Value = DtPurOrderDetail.Rows[i]["DiscountAmount"];

                                            dgvPurchase.Rows[i].Cells["NetAmount"].Value = DtPurOrderDetail.Rows[i]["NetAmount"];
                                            dgvPurchase.Rows[i].Cells["IsAutofilled"].Value = 1;
                                        }
                                    }
                                    CalculateTotalAmt();
                                    //txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountAmount.ToString("F" + MintExchangeCurrencyScale);

                                    DtPurOrderDetail = null;

                                    CalculateNetTotal();
                                    MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID = 0;
                                    SetPurchaseStatus();
                                    SetControlsVisibility(false);
                                    txtRemarks.ReadOnly = false;
                                    ChkTGNR.Enabled = true;
                                    dtpDueDate.Enabled = dtpDate.Enabled = true;
                                    btnActions.Enabled = true;

                                    if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.POrderFromQuotation)
                                    {
                                        dgvPurchase.ReadOnly = false;
                                        Quantity.ReadOnly = false;
                                        Rate.ReadOnly = false;
                                        Discount.ReadOnly = false;
                                        ItemCode.ReadOnly = ItemName.ReadOnly = NetAmount.ReadOnly = Uom.ReadOnly = GrandAmount.ReadOnly = true;

                                    }
                                }
                            }

                        }
                        else
                        {
                            txtExpenseAmount.Text = "0.00";
                        }
                        break;
                    case 3:
                        if (Convert.ToInt64(cboOrderNo.SelectedValue) > 0)
                        {
                            if (!MblnIsFromClear && MblnAddStatus)
                            {
                                if (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.GRNFromInvoice)
                                {
                                    if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseInvoiceID", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID = " + (Int32)OperationStatusType.PInvoiceOpened }).Rows.Count == 0)
                                    {
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 961, out MmessageIcon);
                                        MstrMessageCommon = MstrMessageCommon.Replace("*", "Invoice");
                                        ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                        TmrPurchase.Enabled = true;
                                        cboOrderNo.Focus();
                                        return;
                                    }
                                }
                                else if (cboOrderType.SelectedValue.ToInt32() == (Int32)OperationOrderType.GRNFromTransfer)
                                {
                                    if (MobjClsBLLPurchase.FillCombos(new string[] { "StockTransferID,StockTransferNo", "InvStockTransferMaster", "ReferenceID In (Select WarehouseID From InvWarehouse Where CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + ") and StatusID = " + Convert.ToInt32(OperationStatusType.STIssued) + " And OrderTypeID In (" + (int)OperationOrderType.STWareHouseToWareHouseTransfer + "," + (int)OperationOrderType.STWareHouseToWareHouseTransferDemo + ") And StockTransferID = " + cboOrderNo.SelectedValue.ToInt64() }).Rows.Count == 0)
                                    {
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 982, out MmessageIcon);
                                        ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                        TmrPurchase.Enabled = true;
                                        cboOrderNo.Focus();
                                        return;
                                    }
                                }
                                else if (cboOrderType.SelectedValue.ToInt32() == (Int32)OperationOrderType.GRNFromOrder)
                                {
                                    if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseOrderID", "InvPurchaseOrderMaster", "PurchaseOrderID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.POrderApproved + "," + (int)OperationStatusType.POrderSubmitted + ")" }).Rows.Count == 0)
                                    {
                                        //MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 961, out MmessageIcon);
                                        //MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");
                                        //ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                        //MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                        //lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                        //TmrPurchase.Enabled = true;
                                        //cboOrderNo.Focus();
                                        //return;
                                    }
                                }

                            }
                            int intOldStatusID = MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID;
                            if (cboOrderType.SelectedValue.ToInt32() == (Int32)OperationOrderType.GRNFromInvoice)
                            {
                                if (MobjClsBLLPurchase.DisplayPurchaseInvoiceInfo(Convert.ToInt64(cboOrderNo.SelectedValue)))
                                {
                                    MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = intOldStatusID;
                                    MintPurOrderCompanyID = MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID;
                                    cboWarehouse.DataSource = null;
                                    DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", "CompanyID=" + MintPurOrderCompanyID + "", "WarehouseID", "WarehouseName" });
                                    cboWarehouse.ValueMember = "WarehouseID";
                                    cboWarehouse.DisplayMember = "WarehouseName";
                                    cboWarehouse.DataSource = dtCombo;
                                    cboWarehouse.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intWarehouseID;
                                    cboCurrency.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCurrencyID;
                                    cboSupplier.SelectedValue = Convert.ToInt32(MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID);
                                    if (cboSupplier.SelectedValue == null)
                                        cboSupplier.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVendorName;
                                    AddToAddressMenu(MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID);

                                    MintVendorAddID = MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorAddID;
                                    lblSupplierAddressName.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strAddressName + " Address";
                                    DisplayAddress(Convert.ToInt32(cboSupplier.SelectedValue));

                                    dgvPurchase.Rows.Clear();
                                    DataTable DtPurInvoiceDetail = null;
                                    DtPurInvoiceDetail = MobjClsBLLPurchase.DisplayPurchaseInvoiceDetail(Convert.ToInt64(cboOrderNo.SelectedValue));
                                    dtTempDetails = new DataTable();
                                    dtTempDetails = DtPurInvoiceDetail;
                                    if (DtPurInvoiceDetail.Rows.Count > 0)
                                    {
                                        LoadCombos(9, null); LoadCombos(12, null); int intRowCount = 0;
                                        for (int i = 0; i < DtPurInvoiceDetail.Rows.Count; i++)
                                        {
                                            decimal decReceivedQty = 0;
                                            decReceivedQty = MobjClsBLLPurchase.GetReceivedQty(Convert.ToInt32(DtPurInvoiceDetail.Rows[i]["ItemID"]), Convert.ToInt64(cboOrderNo.SelectedValue), true);
                                            if (decReceivedQty < Convert.ToDecimal(DtPurInvoiceDetail.Rows[i]["ReceivedQuantity"]))
                                            {
                                                dgvPurchase.RowCount = dgvPurchase.RowCount + 1;
                                                dgvPurchase.Rows[intRowCount].Cells["ItemID"].Value = DtPurInvoiceDetail.Rows[i]["ItemID"];
                                                dgvPurchase.Rows[intRowCount].Cells["ItemCode"].Value = DtPurInvoiceDetail.Rows[i]["ItemCode"];
                                                dgvPurchase.Rows[intRowCount].Cells["ItemName"].Value = DtPurInvoiceDetail.Rows[i]["Itemname"];
                                                //  dgvPurchase.Rows[intRowCount].Cells["OrderedQty"].Value = DtPurInvoiceDetail.Rows[i]["ReceivedQuantity"];
                                                FillComboColumn(Convert.ToInt32(DtPurInvoiceDetail.Rows[i]["ItemID"]), 1);
                                                dgvPurchase.Rows[intRowCount].Cells["Uom"].Value = DtPurInvoiceDetail.Rows[i]["UOMID"];
                                                dgvPurchase.Rows[intRowCount].Cells["Uom"].Tag = DtPurInvoiceDetail.Rows[i]["UOMID"];
                                                dgvPurchase.Rows[intRowCount].Cells["Uom"].Value = dgvPurchase.Rows[intRowCount].Cells["Uom"].FormattedValue;
                                                dgvPurchase.Rows[intRowCount].Cells["Rate"].Value = DtPurInvoiceDetail.Rows[i]["Rate"].ToDecimal();
                                                dgvPurchase.Rows[intRowCount].Cells["PurchaseRate"].Value = DtPurInvoiceDetail.Rows[i]["PurchaseRate"].ToDecimal();


                                                int intUomScale = 0;
                                                if (dgvPurchase[Uom.Index, intRowCount].Tag.ToInt32() != 0)
                                                    intUomScale = MobjClsBLLPurchase.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPurchase[Uom.Index, intRowCount].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                                                dgvPurchase.Rows[intRowCount].Cells["OrderedQty"].Value = DtPurInvoiceDetail.Rows[i]["ReceivedQuantity"].ToDecimal().ToString("F" + intUomScale);
                                                dgvPurchase.Rows[intRowCount].Cells["ReceivedQty"].Value = decReceivedQty.ToString("F" + intUomScale);

                                                string strBatchNo = MobjClsBLLPurchase.GetLastBatchNo(Convert.ToInt32(DtPurInvoiceDetail.Rows[i]["ItemID"]), Convert.ToInt64(cboOrderNo.SelectedValue), true);
                                                if (strBatchNo == null)
                                                    strBatchNo = DtPurInvoiceDetail.Rows[i]["ItemCode"].ToString() + ClsCommonSettings.GetServerDate().ToString("dd/MMM/yyyy") + random.Next(10000).ToString();
                                                dgvPurchase.Rows[intRowCount].Cells["BatchNumber"].Value = strBatchNo;
                                                dgvPurchase.Rows[intRowCount].Cells["Uom"].ReadOnly = true;
                                                dgvPurchase.Rows[intRowCount].Cells["IsAutofilled"].Value = 1;
                                                intRowCount++;
                                            }
                                        }

                                    }
                                    DtPurInvoiceDetail = null;
                                    MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID = 0;
                                }
                                this.ShowNoForGRN();
                            }
                            else if (cboOrderType.SelectedValue.ToInt32() == (Int32)OperationOrderType.GRNFromOrder)
                            {
                                if (MobjClsBLLPurchase.DisplayPurchaseOrderInfo(Convert.ToInt64(cboOrderNo.SelectedValue)))
                                {
                                    MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = intOldStatusID;
                                    MintPurOrderCompanyID = MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID;
                                    cboWarehouse.DataSource = null;
                                    DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", "CompanyID=" + MintPurOrderCompanyID + "", "WarehouseID", "WarehouseName" });
                                    cboWarehouse.ValueMember = "WarehouseID";
                                    cboWarehouse.DisplayMember = "WarehouseName";
                                    cboWarehouse.DataSource = dtCombo;
                                    //cboWarehouse.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intWarehouseID;
                                    cboCurrency.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCurrencyID;
                                    cboSupplier.SelectedValue = Convert.ToInt32(MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID);
                                    if (cboSupplier.SelectedValue == null)
                                        cboSupplier.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVendorName;
                                    AddToAddressMenu(MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID);

                                    MintVendorAddID = MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorAddID;
                                    lblSupplierAddressName.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strAddressName + " Address";
                                    DisplayAddress(Convert.ToInt32(cboSupplier.SelectedValue));

                                    dgvPurchase.Rows.Clear();
                                    DataTable DtPurOrderDetail = null;
                                    DtPurOrderDetail = MobjClsBLLPurchase.DisplayPurchaseOrderGRNDetail(Convert.ToInt64(cboOrderNo.SelectedValue));
                                    dtTempDetails = new DataTable();
                                    dtTempDetails = DtPurOrderDetail;
                                    if (DtPurOrderDetail.Rows.Count > 0)
                                    {
                                        LoadCombos(9, null); LoadCombos(12, null); int intRowCount = 0;
                                        for (int i = 0; i < DtPurOrderDetail.Rows.Count; i++)
                                        {
                                            decimal decReceivedQty = 0;
                                            decReceivedQty = MobjClsBLLPurchase.GetReceivedQty(Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]), Convert.ToInt64(cboOrderNo.SelectedValue), true);
                                            if (decReceivedQty < Convert.ToDecimal(DtPurOrderDetail.Rows[i]["ReceivedQuantity"]))
                                            {
                                                dgvPurchase.RowCount = dgvPurchase.RowCount + 1;
                                                dgvPurchase.Rows[intRowCount].Cells["ItemID"].Value = DtPurOrderDetail.Rows[i]["ItemID"];
                                                dgvPurchase.Rows[intRowCount].Cells["ItemCode"].Value = DtPurOrderDetail.Rows[i]["ItemCode"];
                                                dgvPurchase.Rows[intRowCount].Cells["ItemName"].Value = DtPurOrderDetail.Rows[i]["Itemname"];
                                                //  dgvPurchase.Rows[intRowCount].Cells["OrderedQty"].Value = DtPurInvoiceDetail.Rows[i]["ReceivedQuantity"];
                                                FillComboColumn(Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]), 1);
                                                dgvPurchase.Rows[intRowCount].Cells["Uom"].Value = DtPurOrderDetail.Rows[i]["UOMID"];
                                                dgvPurchase.Rows[intRowCount].Cells["Uom"].Tag = DtPurOrderDetail.Rows[i]["UOMID"];
                                                dgvPurchase.Rows[intRowCount].Cells["Uom"].Value = dgvPurchase.Rows[intRowCount].Cells["Uom"].FormattedValue;
                                                dgvPurchase.Rows[intRowCount].Cells["Rate"].Value = DtPurOrderDetail.Rows[i]["Rate"].ToDecimal();
                                                dgvPurchase.Rows[intRowCount].Cells["PurchaseRate"].Value = DtPurOrderDetail.Rows[i]["PurchaseRate"].ToDecimal();


                                                int intUomScale = 0;
                                                if (dgvPurchase[Uom.Index, intRowCount].Tag.ToInt32() != 0)
                                                    intUomScale = MobjClsBLLPurchase.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPurchase[Uom.Index, intRowCount].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                                                dgvPurchase.Rows[intRowCount].Cells["OrderedQty"].Value = DtPurOrderDetail.Rows[i]["ReceivedQuantity"].ToDecimal().ToString("F" + intUomScale);
                                                dgvPurchase.Rows[intRowCount].Cells["ReceivedQty"].Value = decReceivedQty.ToString("F" + intUomScale);

                                                string strBatchNo = MobjClsBLLPurchase.GetLastBatchNo(Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]), Convert.ToInt64(cboOrderNo.SelectedValue), true);
                                                if (strBatchNo == null)
                                                    strBatchNo = DtPurOrderDetail.Rows[i]["ItemCode"].ToString() + ClsCommonSettings.GetServerDate().ToString("dd/MMM/yyyy") + random.Next(10000).ToString();
                                                dgvPurchase.Rows[intRowCount].Cells["BatchNumber"].Value = strBatchNo;
                                                dgvPurchase.Rows[intRowCount].Cells["Uom"].ReadOnly = true;
                                                dgvPurchase.Rows[intRowCount].Cells["IsAutofilled"].Value = 1;
                                                intRowCount++;
                                            }
                                        }

                                    }
                                    DtPurOrderDetail = null;
                                    MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID = 0;
                                }
                                ShowQuotationNo();
                            }
                            else if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromTransfer)
                            {
                                if (MobjClsBLLPurchase.DisplayStockTransferInfo(Convert.ToInt64(cboOrderNo.SelectedValue)))
                                {
                                    cboWarehouse.DataSource = null;
                                    DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", "CompanyID=" + cboCompany.SelectedValue.ToInt32() + "", "WarehouseID", "WarehouseName" });
                                    cboWarehouse.ValueMember = "WarehouseID";
                                    cboWarehouse.DisplayMember = "WarehouseName";
                                    cboWarehouse.DataSource = dtCombo;
                                    cboWarehouse.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intWarehouseID;
                                    cboSupplier.SelectedValue = Convert.ToInt32(MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID);
                                    if (cboSupplier.SelectedValue == null)
                                        cboSupplier.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVendorName;
                                    AddToAddressMenu(MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID);

                                    MintVendorAddID = MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorAddID;
                                    lblSupplierAddressName.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strAddressName + " Address";
                                    DisplayAddress(Convert.ToInt32(cboSupplier.SelectedValue));

                                    dgvPurchase.Rows.Clear();
                                    DataTable DtTransferDetail = null;
                                    DtTransferDetail = MobjClsBLLPurchase.GetStockTransferDetails(Convert.ToInt64(cboOrderNo.SelectedValue));
                                    dtTempDetails = new DataTable();
                                    dtTempDetails = DtTransferDetail;
                                    for (int intRowCount = 0; intRowCount < DtTransferDetail.Rows.Count; intRowCount++)
                                    {
                                        dgvPurchase.RowCount = dgvPurchase.RowCount + 1;
                                        dgvPurchase.Rows[intRowCount].Cells["ItemID"].Value = DtTransferDetail.Rows[intRowCount]["ItemID"];
                                        dgvPurchase.Rows[intRowCount].Cells["ItemCode"].Value = DtTransferDetail.Rows[intRowCount]["ItemCode"];
                                        dgvPurchase.Rows[intRowCount].Cells["ItemName"].Value = DtTransferDetail.Rows[intRowCount]["Itemname"];
                                        FillComboColumn(Convert.ToInt32(DtTransferDetail.Rows[intRowCount]["ItemID"]), 1);
                                        dgvPurchase.Rows[intRowCount].Cells["Uom"].Value = DtTransferDetail.Rows[intRowCount]["UOMID"];
                                        dgvPurchase.Rows[intRowCount].Cells["Uom"].Tag = DtTransferDetail.Rows[intRowCount]["UOMID"];
                                        dgvPurchase.Rows[intRowCount].Cells["Uom"].Value = dgvPurchase.Rows[intRowCount].Cells["Uom"].FormattedValue;

                                        dgvPurchase.Rows[intRowCount].Cells["Rate"].Value = DtTransferDetail.Rows[intRowCount]["Rate"];
                                        dgvPurchase.Rows[intRowCount].Cells["PurchaseRate"].Value = DtTransferDetail.Rows[intRowCount]["Rate"];
                                        int intUomScale = 0;
                                        if (dgvPurchase[Uom.Index, intRowCount].Tag.ToInt32() != 0)
                                            intUomScale = MobjClsBLLPurchase.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPurchase[Uom.Index, intRowCount].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                                        dgvPurchase.Rows[intRowCount].Cells["BatchNumber"].Value = DtTransferDetail.Rows[intRowCount]["BatchNo"];
                                        dgvPurchase.Rows[intRowCount].Cells["BatchID"].Value = DtTransferDetail.Rows[intRowCount]["BatchID"];
                                        dgvPurchase.Rows[intRowCount].Cells["Uom"].ReadOnly = true;
                                        //  dgvPurchase.Rows[intRowCount].Cells["ExpiryDate"].Value = DtTransferDetail.Rows[intRowCount]["ExpiryDate"].ToDateTime().ToString("dd-MMM-yyyy");
                                        dgvPurchase.Rows[intRowCount].Cells["Quantity"].Value = DtTransferDetail.Rows[intRowCount]["Quantity"];
                                        dgvPurchase.Rows[intRowCount].Cells["IsAutofilled"].Value = 1;
                                    }
                                    DtTransferDetail = null;
                                }
                            }



                        }
                        break;
                    case 4:
                        if (Convert.ToInt64(cboOrderNo.SelectedValue) > 0)
                        {
                            if (!MblnIsFromClear && MblnAddStatus && Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.PInvoiceFromOrder)
                            {
                                if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseOrderID", "InvPurchaseOrderMaster", "PurchaseOrderID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.POrderApproved + "," + (int)OperationStatusType.POrderSubmitted + ")" }).Rows.Count == 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 961, out MmessageIcon);
                                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");
                                    ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrPurchase.Enabled = true;
                                    cboOrderNo.Focus();
                                    return;
                                }
                            }
                            this.ShowQuotationNo();
                            if (Convert.ToInt64(cboOrderNo.SelectedValue) > 0)
                            {
                                int intOldStatusID = MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID;
                                if (MobjClsBLLPurchase.DisplayPurchaseOrderInfo(Convert.ToInt64(cboOrderNo.SelectedValue)))
                                {
                                    MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = intOldStatusID;
                                    MintPurOrderCompanyID = MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID;
                                    cboWarehouse.DataSource = null;
                                    DataTable dtCombo = MobjClsBLLPurchase.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", "CompanyID=" + MintPurOrderCompanyID + "", "WarehouseID", "WarehouseName" });
                                    cboWarehouse.ValueMember = "WarehouseID";
                                    cboWarehouse.DisplayMember = "WarehouseName";
                                    cboWarehouse.DataSource = dtCombo;

                                    cboSupplier.SelectedValue = Convert.ToInt32(MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID);
                                    if (cboSupplier.SelectedValue == null)
                                        cboSupplier.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strVendorName;
                                    AddToAddressMenu(MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID);


                                    MintVendorAddID = MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorAddID;
                                    lblSupplierAddressName.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.strAddressName + " Address";
                                    DisplayAddress(Convert.ToInt32(cboSupplier.SelectedValue));
                                    cboPaymentTerms.SelectedValue = Convert.ToInt32(MobjClsBLLPurchase.PobjClsDTOPurchase.intPaymentTermsID);
                                    cboCurrency.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intCurrencyID;
                                    ChkTGNR.Checked = MobjClsBLLPurchase.PobjClsDTOPurchase.blnGRNRequired;
                                    if (ChkTGNR.Checked)
                                    {
                                        ExpiryDate.Visible = false;
                                        ExtraQty.Visible = false;
                                    }
                                    else
                                    {
                                        ExpiryDate.Visible = true;
                                        ExtraQty.Visible = true;
                                    }
                                    cboDiscount.SelectedIndex = (MobjClsBLLPurchase.PobjClsDTOPurchase.blnGrandDiscount.ToBoolean() ? 0 : 1);
                                    if (Convert.ToString(cboDiscount.SelectedItem) == "%")
                                        txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountPercent.ToString();
                                    else
                                        txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountAmt.ToString();
                                    //cboDiscount.SelectedValue = MobjClsBLLPurchase.PobjClsDTOPurchase.intGrandDiscountID;

                                    if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseInvoiceID", "InvPurchaseInvoiceMaster", "OrderTypeID = " + (int)OperationOrderType.PInvoiceFromOrder + " And PurchaseOrderID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And ExpenseAmount > 0" }).Rows.Count == 0)
                                        txtExpenseAmount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decExpenseAmount.ToString("F" + MintExchangeCurrencyScale);
                                    else
                                        txtExpenseAmount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);

                                    dgvPurchase.Rows.Clear();
                                    DataTable DtPurInvoiceDetail = null;
                                    DtPurInvoiceDetail = MobjClsBLLPurchase.DisplayPurchaseOrderDetail(Convert.ToInt64(cboOrderNo.SelectedValue));
                                    dtTempDetails = new DataTable();
                                    dtTempDetails = DtPurInvoiceDetail;
                                    if (DtPurInvoiceDetail.Rows.Count > 0)
                                    {

                                        LoadCombos(9, null); LoadCombos(12, null); int intRowCount = 0;
                                        for (int i = 0; i < DtPurInvoiceDetail.Rows.Count; i++)
                                        {
                                            decimal decReceivedQty = 0;
                                            decReceivedQty = MobjClsBLLPurchase.GetReceivedQty(Convert.ToInt32(DtPurInvoiceDetail.Rows[i]["ItemID"]), Convert.ToInt64(cboOrderNo.SelectedValue), false);
                                            if (decReceivedQty < Convert.ToDecimal(DtPurInvoiceDetail.Rows[i]["Quantity"]))
                                            {
                                                dgvPurchase.RowCount = dgvPurchase.RowCount + 1;
                                                dgvPurchase.Rows[intRowCount].Cells["ItemID"].Value = DtPurInvoiceDetail.Rows[i]["ItemID"];
                                                dgvPurchase.Rows[intRowCount].Cells["ItemCode"].Value = DtPurInvoiceDetail.Rows[i]["ItemCode"];
                                                dgvPurchase.Rows[intRowCount].Cells["ItemName"].Value = DtPurInvoiceDetail.Rows[i]["Itemname"];
                                                // dgvPurchase.Rows[intRowCount].Cells["OrderedQty"].Value = DtPurInvoiceDetail.Rows[i]["Quantity"];
                                                FillComboColumn(Convert.ToInt32(DtPurInvoiceDetail.Rows[i]["ItemID"]), 1);
                                                dgvPurchase.Rows[intRowCount].Cells["Uom"].Value = DtPurInvoiceDetail.Rows[i]["UOMID"];
                                                dgvPurchase.Rows[intRowCount].Cells["Uom"].Tag = DtPurInvoiceDetail.Rows[i]["UOMID"];
                                                dgvPurchase.Rows[intRowCount].Cells["Uom"].Value = dgvPurchase.Rows[intRowCount].Cells["Uom"].FormattedValue;

                                                int intUomScale = 0;
                                                if (dgvPurchase[Uom.Index, intRowCount].Tag.ToInt32() != 0)
                                                    intUomScale = MobjClsBLLPurchase.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPurchase[Uom.Index, intRowCount].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                                                dgvPurchase.Rows[intRowCount].Cells["Quantity"].Value = DtPurInvoiceDetail.Rows[i]["Quantity"].ToDecimal().ToString("F" + intUomScale);

                                                dgvPurchase.Rows[intRowCount].Cells["Rate"].Value = DtPurInvoiceDetail.Rows[i]["Rate"];

                                                dgvPurchase.Rows[intRowCount].Cells["GrandAmount"].Value = DtPurInvoiceDetail.Rows[i]["GrandAmount"];
                                                //FillDiscountColumn(intRowCount, true);
                                                if (DtPurInvoiceDetail.Rows[intRowCount]["IsDiscountPercentage"] != DBNull.Value)
                                                {
                                                    dgvPurchase.Rows[intRowCount].Cells["DiscountAmt"].Value = (Convert.ToBoolean(DtPurInvoiceDetail.Rows[i]["IsDiscountPercentage"]) ? DtPurInvoiceDetail.Rows[i]["DiscountPercentage"].ToDecimal() / 100 * DtPurInvoiceDetail.Rows[i]["GrandAmount"].ToDecimal() : DtPurInvoiceDetail.Rows[i]["DiscountAmount"].ToDecimal()); ;
                                                    dgvPurchase.Rows[intRowCount].Cells["DiscountAmount"].Value = (Convert.ToBoolean(DtPurInvoiceDetail.Rows[i]["IsDiscountPercentage"]) ? DtPurInvoiceDetail.Rows[i]["DiscountPercentage"].ToDecimal() : DtPurInvoiceDetail.Rows[i]["DiscountAmount"].ToDecimal()); ;
                                                    dgvPurchase.Rows[intRowCount].Cells["Discount"].Value = (Convert.ToBoolean(DtPurInvoiceDetail.Rows[i]["IsDiscountPercentage"]) ? "%" : "Amt");
                                                }
                                                


                                               // dgvPurchase.Rows[intRowCount].Cells["DiscountAmount"].Value = DtPurInvoiceDetail.Rows[i]["DiscountAmount"];

                                                dgvPurchase.Rows[intRowCount].Cells["NetAmount"].Value = DtPurInvoiceDetail.Rows[i]["NetAmount"];

                                                if (cboOrderNo.SelectedValue != null)
                                                    dgvPurchase.Rows[intRowCount].Cells["ReceivedQty"].Value = decReceivedQty.ToString("F" + intUomScale);

                                                string strBatchNo = null;
                                                if (MobjClsBLLPurchase.PobjClsDTOPurchase.blnGRNRequired)
                                                    strBatchNo = MobjClsBLLPurchase.GetLastBatchNo(Convert.ToInt32(DtPurInvoiceDetail.Rows[i]["ItemID"]), Convert.ToInt64(cboOrderNo.SelectedValue), true);
                                                else
                                                    strBatchNo = MobjClsBLLPurchase.GetLastBatchNo(Convert.ToInt32(DtPurInvoiceDetail.Rows[i]["ItemID"]), Convert.ToInt64(cboOrderNo.SelectedValue), false);
                                                if (strBatchNo == null && (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.PInvoiceDirect || !MobjClsBLLPurchase.PobjClsDTOPurchase.blnGRNRequired))
                                                    strBatchNo = DtPurInvoiceDetail.Rows[i]["ItemCode"].ToString() + ClsCommonSettings.GetServerDate().ToString("dd/MMM/yyyy") + random.Next(10000).ToString();
                                                dgvPurchase.Rows[intRowCount].Cells["BatchNumber"].Value = strBatchNo;
                                                dgvPurchase.Rows[intRowCount].Cells["Uom"].ReadOnly = true;
                                                dgvPurchase.Rows[intRowCount].Cells["IsAutofilled"].Value = 1;
                                                intRowCount++;
                                            }
                                        }
                                    }
                                    DtPurInvoiceDetail = null;
                                    CalculateTotalAmt();
                                    //txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountAmount.ToString("F" + MintExchangeCurrencyScale);
                                    txtAdvPayment.Text = MobjClsBLLPurchase.GetAdvancePayment(Convert.ToInt64(cboOrderNo.SelectedValue)).ToString("F" + MintExchangeCurrencyScale);

                                    CalculateNetTotal();
                                }

                            }
                            else
                                txtExpenseAmount.Text = "0.000";
                        }
                        else
                        {
                            this.lblOrder.Visible = false;
                            this.lblOrderNo.Visible = false;
                        }
                        break;
                }

                Changestatus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in cboOrderNo_SelectedIndexChanged() " + ex.Message);
                MObjLogs.WriteLog("Error in cboOrderNo_SelectedIndexChanged() " + ex.Message, 2);
            }
        }

        private void TmrFocus_Tick(object sender, EventArgs e)
        {
            cboCompany.Focus();
            TmrFocus.Enabled = false;
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Changestatus();
                if (MblnAddStatus)
                    GenerateOrderNo();
                if (Convert.ToInt32(cboCompany.SelectedValue) > 0)
                {
                    LoadCombos(13, null);
                    cboCurrency.SelectedIndex = -1;
                    cboCurrency.Text = null;
                    lblCompanyCurrencyAmount.Text = "Amount in " + MobjClsBLLPurchase.GetCompanyCurrency(Convert.ToInt32(cboCompany.SelectedValue), out MintBaseCurrencyScale);
                }
                else
                {
                    cboCurrency.DataSource = null;
                    cboCurrency.SelectedIndex = -1;
                    cboCurrency.Text = null;
                    lblCompanyCurrencyAmount.Text = "Amount in company currency";
                }

                switch (MintFromForm)
                {
                    case 2:
                        ClearBottomControls();
                        SetControlVisibilityForBottomPanel(true);
                        //if ((Int32)cboOrderType.SelectedValue == (Int32)OperationOrderType.POrderFromGRN)
                        //{                        
                            cboGRNWarehouse.DataSource = null;
                            DataTable dt = MobjClsBLLPurchase.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) + "", "WarehouseID", "WarehouseName" });
                            cboGRNWarehouse.ValueMember = "WarehouseID";
                            cboGRNWarehouse.DisplayMember = "WarehouseName";
                            cboGRNWarehouse.DataSource = dt;
                        //}

                        break;
                    case 3:
                    case 4:
                        if (Convert.ToInt32(cboCompany.SelectedValue) > 0)
                        {
                            LoadCombos(5, null);
                            cboWarehouse.SelectedIndex = -1;
                            cboWarehouse.Text = null;
                            cboOrderNo.DataSource = null;
                            cboOrderType.SelectedIndex = -1;
                        }
                        else
                        {
                            cboWarehouse.DataSource = null;
                            cboCurrency.DataSource = null;
                            cboOrderNo.Visible = false;
                            cboOrderNo.DataSource = null;
                            cboWarehouse.SelectedIndex = -1;
                            cboCurrency.SelectedIndex = -1;
                            cboWarehouse.Text = null;
                            cboCurrency.Text = null;
                        }
                        SettingPurchaseAccount();
                        break;
                }
                LoadCombos(18, null);
                dgvPurchase.Rows.Clear();
                dgvLocationDetails.Rows.Clear();
                cboOrderType_SelectedIndexChanged(null, null);

            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in cboCompany_SelectedIndexChanged() " + ex.Message);
                MObjLogs.WriteLog("Error in cboCompany_SelectedIndexChanged() " + ex.Message, 2);
            }
        }

        private void BtnTVenderAddress_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(cboSupplier.SelectedValue) > 0)
                {
                    if (MintCnt < CMSVendorAddress.Items.Count - 1)
                    {
                        MintCnt += 1;
                    }
                    else
                    {
                        MintCnt = 0;
                    }
                    BtnTVenderAddress.Text = CMSVendorAddress.Items[MintCnt].Text;
                    MintVendorAddID = Convert.ToInt32(CMSVendorAddress.Items[MintCnt].Tag);
                    DisplayAddress(Convert.ToInt32(cboSupplier.SelectedValue));
                }
                Changestatus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnSupplierAddress_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in btnSupplierAddress_Click() " + ex.Message, 2);
            }
        }

        private void btnTContextmenu_Click(object sender, EventArgs e)
        {
            Changestatus();
            CMSVendorAddress.Show(btnTContextmenu, btnTContextmenu.PointToClient(System.Windows.Forms.Cursor.Position));
        }

        private void btnSupplier_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(cboSupplier.SelectedValue);
                int iVendorCurID = MobjClsBLLPurchase.GetVendorRecordID(MintComboID);
                using (FrmVendor objVendor = new FrmVendor(2))
                {
                    objVendor.PintVendorID = MintComboID;
                    objVendor.ShowDialog();
                    if (MblnAddStatus)
                    {
                        LoadCombos(3, null);
                    }
                    else
                    {
                        DataTable datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "VendorID,VendorName+' - '+VendorCode as VendorName", "VendorInformation", "VendorTypeID=" + (int)VendorType.Supplier + " And StatusID = " + (int)VendorStatus.Active + " UNION SELECT VendorID,VendorName+' - '+VendorCode as VendorName FROM VendorInformation WHERE VendorID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intVendorID });
                        cboSupplier.ValueMember = "VendorID";
                        cboSupplier.DisplayMember = "VendorName";
                        cboSupplier.DataSource = datCombos;
                    }
                    if (objVendor.PintVendorID != 0 && cboSupplier.Enabled)
                        cboSupplier.SelectedValue = objVendor.PintVendorID;
                    else
                        cboSupplier.SelectedValue = MintComboID;
                }
                if (MintFromForm == 1 || MintFromForm == 2 || MintFromForm == 3 || MintFromForm == 4)
                {
                    if (Convert.ToInt32(cboSupplier.SelectedValue) > 0)
                        AddToAddressMenu(Convert.ToInt32(cboSupplier.SelectedValue));
                }
                Changestatus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnSupplier_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in btnSupplier_Click() " + ex.Message, 2);
            }
        }

        private void BtnUom_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmUnitOfMeasurement objUoM = new FrmUnitOfMeasurement())
                {
                    objUoM.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnUom_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in BtnUom_Click() " + ex.Message, 2);
            }
        }

        private void dgvPurchase_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    dgvPurchase.EndEdit();
                    if (e.ColumnIndex == Uom.Index)
                    {
                        dgvPurchase.CurrentRow.Cells["Uom"].Tag = dgvPurchase.CurrentRow.Cells["Uom"].Value;
                        dgvPurchase.CurrentRow.Cells["Uom"].Value = dgvPurchase.CurrentRow.Cells["Uom"].FormattedValue;


                        // int intDecimalPart = 0;
                        //  if (dgvPurchase.CurrentRow.Cells["Quantity"].Value != null && !string.IsNullOrEmpty(dgvPurchase.CurrentRow.Cells["Quantity"].Value.ToString()) && dgvPurchase.CurrentRow.Cells["Quantity"].Value.ToString().Contains("."))
                        //   intDecimalPart = dgvPurchase.CurrentRow.Cells["Quantity"].Value.ToString().Split('.')[1].Length;
                        int intUomScale = 0;
                        if (dgvPurchase[Uom.Index, dgvPurchase.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                            intUomScale = MobjClsBLLPurchase.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvPurchase[Uom.Index, dgvPurchase.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                        // if (intDecimalPart > intUomScale)
                        // {
                        dgvPurchase.CurrentRow.Cells["Quantity"].Value = dgvPurchase.CurrentRow.Cells["Quantity"].Value.ToDecimal().ToString("F" + intUomScale);
                        dgvPurchase.CurrentRow.Cells[ExtraQty.Index].Value = dgvPurchase.CurrentRow.Cells[ExtraQty.Index].Value.ToDecimal().ToString("F" + intUomScale);
                        // }
                        //FillDiscountColumn(e.RowIndex, false);

                        //if (MintFromForm == 3 && cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromMaterialReturn)
                        //{
                        //    DataTable datTransferDetails = MobjclsBLLCommonUtility.FillCombos(new string[] { "Rate,UOMID", "InvStockTransferDetail", "StockTransferID = " + cboOrderNo.SelectedValue.ToInt64() + " And ItemId = " + dgvPurchase[ItemID.Index, e.RowIndex].Value.ToInt32() + " And BatchID = " + dgvPurchase[BatchID.Index, e.RowIndex].Value.ToInt64() });
                        //    dgvPurchase[Rate.Index, e.RowIndex].Value = MobjclsBLLCommonUtility.ConvertQtyToBaseUnitQty(dgvPurchase[Uom.Index, e.RowIndex].Tag.ToInt32(), dgvPurchase[ItemID.Index, e.RowIndex].Value.ToInt32(), MobjclsBLLCommonUtility.ConvertBaseUnitQtyToOtherQty(datTransferDetails.Rows[0]["UOMID"].ToInt32(), dgvPurchase[ItemID.Index, e.RowIndex].Value.ToInt32(), datTransferDetails.Rows[0]["Rate"].ToDecimal(), 1), 1);
                        //    dgvPurchase[PurchaseRate.Index, e.RowIndex].Value = dgvPurchase[Rate.Index, e.RowIndex].Value;

                        //}

                        if ((MintFromForm == 3 && cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromInvoice) || (MintFromForm == 4 && cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.PInvoiceDirect))
                        {
                            foreach (DataGridViewRow dr in dgvLocationDetails.Rows)
                            {
                                if (dr.Cells[LItemID.Index].Value.ToInt32() == dgvPurchase[ItemID.Index, e.RowIndex].Value.ToInt32() && Convert.ToString(dr.Cells[LBatchNo.Index].Value) == Convert.ToString(dgvPurchase[Batchnumber.Index, e.RowIndex].Value))
                                {
                                    DataTable dtItemUOMs = MobjClsBLLPurchase.GetItemUOMs(dgvPurchase.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt32());
                                    LUOM.DataSource = null;
                                    LUOM.ValueMember = "UOMID";
                                    LUOM.DisplayMember = "ShortName";
                                    LUOM.DataSource = dtItemUOMs;
                                    dr.Cells["LUOMID"].Value = dgvPurchase.Rows[e.RowIndex].Cells["UOM"].Tag.ToInt32();
                                    dr.Cells["LUOM"].Value = dgvPurchase.Rows[e.RowIndex].Cells["UOM"].Tag.ToInt32();
                                    dr.Cells["LUOM"].Tag = dgvPurchase.Rows[e.RowIndex].Cells["UOM"].Tag.ToInt32();
                                    dr.Cells["LUOM"].Value = dr.Cells["LUOM"].FormattedValue;
                                }
                            }
                        }
                    }
                    //if (e.ColumnIndex == Discount.Index)
                    //{
                    //    dgvPurchase.CurrentRow.Cells["Discount"].Tag = dgvPurchase.CurrentRow.Cells["Discount"].Value;
                    //    dgvPurchase.CurrentRow.Cells["Discount"].Value = dgvPurchase.CurrentRow.Cells["Discount"].FormattedValue;

                    //    //if (dgvPurchase.CurrentRow.Cells["Discount"].Tag.ToInt32() == (int)PredefinedDiscounts.DefaultPurchaseDiscount)
                    //    //    dgvPurchase.CurrentRow.Cells["DiscountAmount"].ReadOnly = false;
                    //    //else
                    //    //    dgvPurchase.CurrentRow.Cells["DiscountAmount"].ReadOnly = true;
                    //}
                }
                if (e.ColumnIndex == Quantity.Index || e.ColumnIndex == Rate.Index || e.ColumnIndex == Discount.Index || e.ColumnIndex == DiscountAmount.Index)
                {
                    CalculateTotalAmt();
                    CalculateNetTotal();
                }
                if ((Int32)cboOrderType.SelectedValue != (Int32)OperationOrderType.POrderDirect)
                {
                    dgvPurchase.Rows[dgvPurchase.Rows.Count - 1].ReadOnly = true;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvPurchase_CellEndEdit() " + ex.Message);
                MObjLogs.WriteLog("Error in dgvPurchase_CellEndEdit() " + ex.Message, 2);
            }
        }

        private void CalculateNetTotal()
        {
            try
            {
                double dDiscountAmt = 0;
                double dSubTotal = 0;
                double dNetAmount = 0;
                double dExpenseAmount = 0;
                double dTaxAmount = 0;
                decimal taxValue = 0;

                if (!string.IsNullOrEmpty(txtExpenseAmount.Text))
                    dExpenseAmount = Convert.ToDouble(txtExpenseAmount.Text);
                dSubTotal = txtSubTotal.Text.ToDouble();
               // int intDiscountID = Convert.ToInt32(cboDiscount.SelectedValue);
                //intDiscountID = 2;
                if (MintFromForm == 1 || MintFromForm == 2 || MintFromForm == 4)
                {
                    double dBTotal = dSubTotal;

                    if (cboDiscount.SelectedItem != null)
                    {
                        if (cboDiscount.SelectedItem.ToString() == "%")
                            dDiscountAmt = txtDiscount.Text.ToDouble() / 100 * dSubTotal;
                        else
                            dDiscountAmt = txtDiscount.Text.ToDouble();
                    }
                    else
                    {
                        dDiscountAmt = 0;
                        txtDiscount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                    }

                    //Tax Calculation
                    if (cboTaxScheme.SelectedValue.ToInt32() > 0)
                    {
                        taxValue = MobjClsBLLPurchase.getTaxValue(cboTaxScheme.SelectedValue.ToInt32());
                        dTaxAmount = (((dSubTotal.ToDecimal() - dDiscountAmt.ToDecimal()) * taxValue) / 100).ToDouble();
                    }

                    dNetAmount = (dSubTotal + dExpenseAmount + dTaxAmount) - dDiscountAmt;
                    txtTaxAmount.Text = dTaxAmount.ToString("F" + MintExchangeCurrencyScale);
                    txtTotalAmount.Text = dNetAmount.ToString("F" + MintExchangeCurrencyScale);
                    txtExpenseAmount.Text = Convert.ToDecimal(txtExpenseAmount.Text).ToString("F" + MintExchangeCurrencyScale);
                    CalculateExchangeCurrencyRate();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CalculateBNetNotal() " + ex.Message);
                MObjLogs.WriteLog("Error in CalculateBNetTotal() " + ex.Message, 2);
            }
        }

        private void cboDiscount_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalculateNetTotal();
            //for (int i = 0; i < dgvPurchase.Rows.Count; i++)
            //{
            //    if (dgvPurchase.Rows[i].Cells["ItemID"].Value != null && dgvPurchase.Rows[i].Cells["Quantity"].Value.ToDecimal() >0)
            //    {
            //        dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = (dgvPurchase.Rows[i].Cells["NetAmount"].Value.ToDecimal() / dgvPurchase.Rows[i].Cells["Quantity"].Value.ToDecimal()).ToString("F" + MintExchangeCurrencyScale);
            //        if (cboDiscount.SelectedValue.ToInt32() != 0 && cboDiscount.SelectedValue.ToInt32() != (int)PredefinedDiscounts.DefaultPurchaseDiscount)
            //            dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = Convert.ToDecimal(MobjClsBLLPurchase.GetItemDiscountAmount(4, 0, cboDiscount.SelectedValue.ToInt32(), dgvPurchase.Rows[i].Cells["PurchaseRate"].Value.ToDouble())).ToString("F" + MintExchangeCurrencyScale);
            //        else if (cboDiscount.SelectedValue.ToInt32() == (int)PredefinedDiscounts.DefaultPurchaseDiscount)
            //        {
            //            decimal decDiscountPercent = (txtDiscount.Text.ToDecimal() / txtSubTotal.Text.ToDecimal()) * 100;
            //            decimal decItemNetAmount = dgvPurchase.Rows[i].Cells["NetAmount"].Value.ToDecimal() - (dgvPurchase.Rows[i].Cells["NetAmount"].Value.ToDecimal() * (decDiscountPercent / 100));
            //            dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = (decItemNetAmount / dgvPurchase.Rows[i].Cells["Quantity"].Value.ToDecimal()).ToString("F" + MintExchangeCurrencyScale);
            //        }

            //    }
            //}
            if (cboDiscount.Enabled)
            {
                
                    txtDiscount.ReadOnly = false;
                
            }
            else
                txtDiscount.ReadOnly = true;

            Changestatus();
        }

        private void TxtTVendorAddress_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            Changestatus();
            //if (MintFromForm == 1 || (MintFromForm == 2 && cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.POrderFromQuotation) || MintFromForm == 4 && cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.PInvoiceFromOrder)
            //{
            //    foreach (DataGridViewRow dr in dgvPurchase.Rows)
            //    {
            //        if (dr.Cells["ItemID"].Value.ToInt32() != 0)
            //        {
            //            FillDiscountColumn(dr.Index, false);
            //        }
            //    }
            //    TxtBSubtotal_TextChanged(null, null);
            //}
        }

        private void dtpDueDate_ValueChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtSupplierBillNo_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void ChkTGNR_CheckedChanged(object sender, EventArgs e)
        {
            if (MblnAddStatus)
            {
                if (ChkTGNR.Checked == false)
                {
                    MobjClsBLLPurchase.PobjClsDTOPurchase.blnGRNRequired = false;
                    foreach (DataGridViewRow dr in dgvPurchase.Rows)
                    {
                        if (dr.Cells["ItemID"].Value != null)
                        {
                            string strBatchNo = null;
                            if (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.PInvoiceFromOrder)
                                strBatchNo = MobjClsBLLPurchase.GetLastBatchNo(Convert.ToInt32(dr.Cells["ItemID"].Value), Convert.ToInt64(cboOrderNo.SelectedValue), false);
                            if (string.IsNullOrEmpty(strBatchNo))
                                strBatchNo = dr.Cells["ItemCode"].Value.ToString() + ClsCommonSettings.GetServerDate().ToString("dd/MMM/yyyy") + random.Next(10000).ToString();

                            dgvPurchase.Columns["Batchnumber"].ReadOnly = false;
                            dr.Cells["Batchnumber"].Value = strBatchNo.ToString();
                        }
                    }
                    if (MintFromForm == 4)
                    {
                        ExpiryDate.Visible = true;
                        Batchnumber.Visible = true;
                        ExtraQty.Visible = true;
                        SetPurchaseGridWidth();
                        if (ClsCommonSettings.PblnIsLocationWise)
                        {
                            tpLocationDetails.Visible = true;
                            btnPickList.Visible = true;
                            btnPickListEmail.Visible = true;
                            PurchaseOrderBindingNavigator.Refresh();
                        }
                    }
                }
                else
                {
                    MobjClsBLLPurchase.PobjClsDTOPurchase.blnGRNRequired = true;

                    foreach (DataGridViewRow dr in dgvPurchase.Rows)
                    {
                        dr.Cells["Batchnumber"].Value = "";
                        dr.Cells["Batchnumber"].ReadOnly = true;
                    }

                    if (MintFromForm == 4)
                    {
                        ExpiryDate.Visible = false;
                        Batchnumber.Visible = false;
                        ExtraQty.Visible = false;
                        SetPurchaseGridWidth();
                        tpLocationDetails.Visible = false;
                        btnPickList.Visible = false;
                        btnPickListEmail.Visible = false;
                    }
                }

                Changestatus();
            }
            if (MintFromForm == 4)
            {
                if (ChkTGNR.Checked)
                {
                    ExpiryDate.Visible = false;
                    Batchnumber.Visible = false;
                    ExtraQty.Visible = false;
                    tpLocationDetails.Visible = false;
                    btnPickList.Visible = false;
                    btnPickListEmail.Visible = false;
                    SetPurchaseGridWidth();
                }
                else
                {
                    ExpiryDate.Visible = true;
                    Batchnumber.Visible = true;
                    ExtraQty.Visible = true;
                    SetPurchaseGridWidth();
                    if (ClsCommonSettings.PblnIsLocationWise)
                    {
                        tpLocationDetails.Visible = true;
                        btnPickList.Visible = true;
                        btnPickListEmail.Visible = true;
                        PurchaseOrderBindingNavigator.Refresh();
                    }
                }
            }
        }

        private void CboTPaymentTerms_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void TxtBRemarks_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void btnWarehouse_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(cboWarehouse.SelectedValue);
                using (FrmWarehouse objCreateWarehouse = new FrmWarehouse())
                {
                    objCreateWarehouse.PintWareHouse = Convert.ToInt32(cboWarehouse.SelectedValue);
                    objCreateWarehouse.ShowDialog();
                    LoadCombos(5, null);
                    if (objCreateWarehouse.PintWareHouse != 0)
                        cboWarehouse.SelectedValue = objCreateWarehouse.PintWareHouse;
                    else
                        cboWarehouse.SelectedValue = MintComboID;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnWarehouse_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in btnWarehouse_Click() " + ex.Message, 2);
            }
        }

        private void BtnItem_Click(object sender, EventArgs e)
        {
            CallProducts();
        }
        private void CallProducts()
        {
            frmItemMaster objItemMaster1 = null;

            try
            {
                objItemMaster1 = new frmItemMaster(ClsCommonSettings.PblnIsTaxable);
                objItemMaster1.Text = "Product Master " + (ClsMainSettings.objFrmMain.MdiChildren.Length + 1);
                objItemMaster1.MdiParent = ClsMainSettings.objFrmMain;
                objItemMaster1.WindowState = FormWindowState.Maximized;
                objItemMaster1.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objItemMaster1.Dispose();
                System.GC.Collect();
                CallProducts();
            }
        }
        private void cboSupplier_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboSupplier.DroppedDown = false;
        }

        private void cboOrderType_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboOrderType.DroppedDown = false;
        }

        private void cboOrderNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboOrderNo.DroppedDown = false;
        }

        private void CboTPaymentTerms_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboPaymentTerms.DroppedDown = false;
        }

        private void cboDiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboDiscount.DroppedDown = false;
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MintFromForm == 3 || MintFromForm == 4)
                {
                    if (MintFromForm == 3 && cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromInvoice)
                    {
                        if (!MblnAddStatus && MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseReturnID", "InvPurchaseReturnMaster", "PurchaseInvoiceID = " + cboOrderNo.SelectedValue.ToInt64() }).Rows.Count > 0)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 989, out MmessageIcon).Replace("*", "GRN");

                            MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            return;
                        }
                    }
                    //else if (MintFromForm == 3 && cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromOrder)
                    //{
                    //    if (!MblnAddStatus && MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseReturnID", "InvPurchaseReturnMaster", "PurchaseInvoiceID = " + cboOrderNo.SelectedValue.ToInt64() }).Rows.Count > 0)
                    //    {
                    //        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 989, out MmessageIcon).Replace("*", "Invoice");

                    //        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    //        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    //        TmrPurchase.Enabled = true;
                    //        tcPurchase.SelectedTab = tpItemDetails;
                    //        return;
                    //    }

                    //}
                    else if (MintFromForm == 4)
                    {

                        if (!MblnAddStatus && MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseReturnID", "InvPurchaseReturnMaster", "PurchaseInvoiceID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID }).Rows.Count > 0)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 989, out MmessageIcon).Replace("*", "Invoice");

                            MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            return;
                        }
                    }



                }

                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 936, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                if (Convert.ToInt64(txtPurchaseNo.Tag) > 0)
                {
                    switch (MintFromForm)
                    {
                        case 1:
                            MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID = Convert.ToInt64(txtPurchaseNo.Tag);


                            if (MobjclsBLLCommonUtility.FillCombos(new string[] { "1", "InvPurchaseOrderMaster", "OrderTypeID = " + (int)OperationOrderType.POrderFromQuotation + " And ReferenceID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID }).Rows.Count > 0)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9001, out MmessageIcon).Replace("#", "").Replace("*", "Purchase Quotation");
                                MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }

                            if (MobjClsBLLPurchase.DeletePurchaseQuotation())
                            {
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrFocus.Enabled = true;
                                AddNewPurchase();
                            }
                            else
                            {
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrFocus.Enabled = true;
                            }

                            break;
                        case 2:
                            MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID = Convert.ToInt64(txtPurchaseNo.Tag);

                            //if (MobjclsBLLCommonUtility.FillCombos(new string[] { "1", "InvPurchaseInvoiceMaster", "OrderTypeID = " + (int)OperationOrderType.PInvoiceFromOrder + " And PurchaseOrderID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID }).Rows.Count > 0)
                            //{
                            //    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9001, out MmessageIcon).Replace("#", "").Replace("*", "Purchase Order");
                            //    MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //    return;
                            //}
                            if (!MblnAddStatus && MobjClsBLLPurchase.CheckPurchaseOrderExists(0))
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2267, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1);
                                TmrFocus.Enabled = true;
                                return;
                            }
                            if (MobjClsBLLPurchase.DeletePurchaseOrder(Convert.ToInt64(txtPurchaseNo.Tag),strCondition))
                            {
                                clsBLLAlertMoment objclsBLLAlertMoment = new clsBLLAlertMoment();
                                objclsBLLAlertMoment.DeleteAlert(txtPurchaseNo.Tag.ToInt32(), (int)AlertSettingsTypes.PurchaseOrderApproved);

                                FrmMain objFrmTradingMain = new FrmMain();
                                objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.PurchaseOrderApproved;
                                objFrmTradingMain.tmrAlert.Enabled = true;


                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrFocus.Enabled = true;
                                AddNewPurchase();
                            }
                            else
                            {
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrFocus.Enabled = true;
                            }

                            break;
                        case 3:
                            FillParameters();
                            FillDetailParameters();
                            string strItemSold = "";
                            bool blnIsSalesDone = false;
                            if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.GRNCancelled)
                                strItemSold = MobjClsBLLPurchase.StockValidation(true, true, out blnIsSalesDone);
                            if (!string.IsNullOrEmpty(strItemSold))
                            {
                                if (!blnIsSalesDone)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 946, out MmessageIcon);
                                    MstrMessageCommon = MstrMessageCommon.Replace("@", strItemSold);
                                }
                                else
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9001, out MmessageIcon);
                                    MstrMessageCommon = MstrMessageCommon.Replace("*", strItemSold);
                                }
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                return;
                            }
                            if ((MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.GRNCancelled && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PInvoiceCancelled) && (IsStockAdjustmentDone() || !ValidateLocationQuantity(true)))
                            {
                                return;
                            }
                            if (MobjClsBLLPurchase.DeleteGRN(Convert.ToInt64(txtPurchaseNo.Tag)))
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrFocus.Enabled = true;
                                AddNewPurchase();
                            }
                            else
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 934, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrFocus.Enabled = true;
                            }
                            break;
                        case 4:
                            if (Convert.ToInt32(cboPaymentTerms.SelectedValue) == (int)PaymentTerms.Credit)
                            {
                                if (MobjClsBLLPurchase.FillCombos(new string[] { "PM.ReceiptAndPaymentID", "AccReceiptAndPaymentMaster PM Inner Join AccReceiptAndPaymentDetails PD ON PM.ReceiptAndPaymentID=PD.ReceiptAndPaymentID", "PM.OperationTypeID = " + (int)OperationType.PurchaseInvoice + " And PD.ReferenceID =" + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID }).Rows.Count > 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 966, out MmessageIcon);
                                    ErrPurchase.SetError(cboOrderType, MstrMessageCommon.Replace("#", "").Trim());
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrPurchase.Enabled = true;
                                    cboOrderType.Focus();
                                    return;
                                }
                            }
                            else
                            {
                                if (MobjClsBLLPurchase.FillCombos(new string[] { "PM.ReceiptAndPaymentID", "AccReceiptAndPaymentMaster PM Inner Join AccReceiptAndPaymentDetails PD ON PM.ReceiptAndPaymentID=PD.ReceiptAndPaymentID", "PM.OperationTypeID = " + (int)OperationType.PurchaseInvoice + " And PD.ReferenceID =" + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID + " And PM.ReceiptAndPaymentTypeID = " + (int)PaymentTypes.DirectExpense + "" }).Rows.Count > 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 966, out MmessageIcon);
                                    ErrPurchase.SetError(cboOrderType, MstrMessageCommon.Replace("#", "").Trim());
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrPurchase.Enabled = true;
                                    cboOrderType.Focus();
                                    return;
                                }
                            }

                            if (MobjClsBLLPurchase.FillCombos(new string[] { "ExpenseID", "InvExpenseMaster", "OperationTypeID = " + (int)OperationType.PurchaseInvoice + " And ReferenceID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID + " And IsDirectExpense = 0" }).Rows.Count > 0)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 997, out MmessageIcon).Replace("*", "Invoice"); ;
                                ErrPurchase.SetError(cboOrderType, MstrMessageCommon.Replace("#", "").Trim());
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                tcPurchase.SelectedTab = tpItemDetails;
                                cboOrderType.Focus();
                                return;
                            }
                            FillParameters();
                            FillDetailParameters();
                            if (!ChkTGNR.Checked)
                            {
                                blnIsSalesDone = false;
                                strItemSold = "";
                                if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PInvoiceCancelled)
                                    strItemSold = MobjClsBLLPurchase.StockValidation(true, false, out blnIsSalesDone);
                                if (!string.IsNullOrEmpty(strItemSold))
                                {
                                    if (!blnIsSalesDone)
                                    {
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 946, out MmessageIcon);
                                        MstrMessageCommon = MstrMessageCommon.Replace("@", strItemSold);
                                    }
                                    else
                                    {
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9001, out MmessageIcon);
                                        MstrMessageCommon = MstrMessageCommon.Replace("*", strItemSold);
                                    }
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPurchasestatus.Text = MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1);
                                    TmrFocus.Enabled = true;
                                    return;
                                }
                                if ((MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.GRNCancelled && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PInvoiceCancelled) && (IsStockAdjustmentDone() || !ValidateLocationQuantity(true)))
                                {
                                    return;
                                }
                            }
                            else if (MobjClsBLLPurchase.FillCombos(new string[] { "GRNID", "InvGRNMaster", "OrderTypeID = " + (int)OperationOrderType.GRNFromInvoice + " And ReferenceID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID + "" }).Rows.Count > 0)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 972, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1);
                                TmrFocus.Enabled = true;
                                return;
                            }

                            if (MobjClsBLLPurchase.DeleteInvoice(Convert.ToInt64(txtPurchaseNo.Tag)))
                            {
                                //try
                                //{
                                //    if (!ChkTGNR.Checked) // When GRN not Required
                                //    {
                                //        MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID = Convert.ToInt64(txtPurchaseNo.Tag);
                                //        DataSet dtsTemp2 = MobjClsBLLPurchase.PurchaseInvoiceDetailForStockAccount(MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID); // For Taking Updated Purchase Invoice Details

                                //        if (dtsTemp2.Tables[0].Rows.Count > 0)
                                //        {
                                //            int AccountId = Convert.ToInt32(dtsTemp2.Tables[1].Rows[0]["AccountId"].ToString());
                                //            int CompanyID = 0;
                                //            DateTime InvoiceDate = ClsCommonSettings.GetServerDate();
                                //            double Amount = 0;

                                //            for (int j = 0; j < dtsTemp2.Tables[0].Rows.Count; j++)
                                //            {
                                //                CompanyID = Convert.ToInt32(dtsTemp2.Tables[0].Rows[j]["CompanyID"].ToString());
                                //                InvoiceDate = Convert.ToDateTime(dtsTemp2.Tables[0].Rows[j]["InvoiceDate"].ToString());
                                //                Amount += Convert.ToDouble(dtsTemp2.Tables[0].Rows[j]["Amount"].ToString());
                                //            }
                                //            string TempAmt = "-" + Amount.ToString();
                                //            Amount = Convert.ToDouble(TempAmt);
                                //            MobjClsBLLPurchase.UpdateOpStockAccount(AccountId, CompanyID, InvoiceDate, Amount); // For Old Amount Updation
                                //        }
                                //    }
                                //}
                                //catch { }
                                AddNewPurchase();
                            }
                            else
                            {

                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrFocus.Enabled = true;
                            }
                            break;
                        case 5:
                            break;

                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorDeleteItem_CLick() " + ex.Message);
                MObjLogs.WriteLog("Error in BindingNavigatorDeleteItem_Click() " + ex.Message, 2);
            }
        }

        private void btnCurrency_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(cboCurrency.SelectedValue);
                using (FrmCurrency objCurrencyReference = new FrmCurrency())
                {
                    objCurrencyReference.ShowDialog();
                }
                LoadCombos(13, null);
                cboCurrency.SelectedValue = MintComboID;
                Changestatus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnCurrency_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in btnCurrency_Click() " + ex.Message, 2);
            }
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup objEmailPopUp = new FrmEmailPopup())
                {
                    switch (MintFromForm)
                    {
                        case 1:
                            objEmailPopUp.MsSubject = "Purchase Quotation Information";
                            objEmailPopUp.EmailFormType = EmailFormID.PurchaseQuotation;
                            objEmailPopUp.EmailSource = MobjClsBLLPurchase.GetPurchaseReport(1);
                            break;
                        case 2:
                            objEmailPopUp.MsSubject = "Purchase Order Information";
                            objEmailPopUp.EmailFormType = EmailFormID.PurchaseOrder;
                            objEmailPopUp.EmailSource = MobjClsBLLPurchase.GetPurchaseReport(2);
                            break;
                        case 3:

                            objEmailPopUp.MsSubject = "GRN Information";

                            objEmailPopUp.EmailFormType = EmailFormID.GRN;
                            objEmailPopUp.EmailSource = MobjClsBLLPurchase.GetPurchaseReport(3);
                            break;
                        case 4:
                            objEmailPopUp.MsSubject = "Purchase Invoice Information";
                            objEmailPopUp.EmailFormType = EmailFormID.PurchaseInvoice;
                            objEmailPopUp.EmailSource = MobjClsBLLPurchase.GetPurchaseReport(4);
                            break;
                    }
                    objEmailPopUp.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnEmail_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in BtnEmail_Click() " + ex.Message, 2);
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID;
                    switch (MintFromForm)
                    {
                        case 1:
                            ObjViewer.PiFormID = (int)FormID.PurchaseQuotation;
                            break;
                        case 2:
                            ObjViewer.PiFormID = (int)FormID.PurchaseOrder;
                            if (MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType == 4)
                            {
                                ObjViewer.Type = "Purchase Indent No";
                            }
                            else if (MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType == 6)
                            {
                                ObjViewer.Type = "Purchase Quotation No";
                            }

                            break;
                        case 3:
                            ObjViewer.PiFormID = (int)FormID.PurchaseGRN;
                            if (MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType == 8)
                            {
                                ObjViewer.Type = "Purchase Invoice No";
                            }
                            break;
                        case 4:
                            ObjViewer.PiFormID = (int)FormID.PurchaseInvoice;
                            if (MobjClsBLLPurchase.PobjClsDTOPurchase.intOrderType == 10)
                            {
                                ObjViewer.Type = "Purchase Order No";
                            }
                            break;
                    }
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnPrint_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in BtnPrint_Click() " + ex.Message, 2);
            }
        }

        private void BindingNavigatorCancelItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (BindingNavigatorCancelItem.Text.ToUpper() == "CANCEL")
                {
                    int intOldStatusID = MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID;
                    MobjClsBLLPurchase.PobjClsDTOPurchase.blnIsCancel = true;
                    if (MintFromForm == 1 || MintFromForm == 2)
                    {
                        int intStatusID = 0;
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 968, out MmessageIcon);
                        if (MintFromForm == 1)
                        {
                            MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation");
                            DataTable datQuotation = MobjClsBLLPurchase.FillCombos(new string[] { "StatusID", "InvPurchaseQuotationMaster", "PurchaseQuotationID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID });
                            if (datQuotation.Rows.Count > 0)
                                intStatusID = datQuotation.Rows[0]["StatusID"].ToInt32();
                        }
                        else if (MintFromForm == 2)
                        {
                            MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");
                            DataTable datOrder = MobjClsBLLPurchase.FillCombos(new string[] { "StatusID", "InvPurchaseOrderMaster", "PurchaseOrderID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID });
                            if (datOrder.Rows.Count > 0)
                                intStatusID = datOrder.Rows[0]["StatusID"].ToInt32();
                        }
                        if (intStatusID != (int)OperationStatusType.POrderSubmitted && intStatusID != (int)OperationStatusType.PQuotationSubmitted && intStatusID != (int)OperationStatusType.POrderOpened && intStatusID != (int)OperationStatusType.POrderSubmittedForApproval && intStatusID != (int)OperationStatusType.POrderRejected && intStatusID != (int)OperationStatusType.PQuotationOpened && intStatusID != (int)OperationStatusType.PQuotationSubmittedForApproval && intStatusID != (int)OperationStatusType.PQuotationRejected)
                        {
                            MessageBox.Show(MstrMessageCommon.Replace("*", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 955, out MmessageIcon);
                    switch (MintFromForm)
                    {
                        case 1:
                            MstrMessageCommon = MstrMessageCommon.Replace("***", "Quotation");
                            break;
                        case 2:
                         
                            if (!MblnAddStatus && MobjClsBLLPurchase.CheckPurchaseOrderExists(1))
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2267, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1);
                                TmrFocus.Enabled = true;
                                return;
                            }
                            MstrMessageCommon = MstrMessageCommon.Replace("***", "Order");
                            break;
                        case 3:
                            if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromInvoice)
                            {
                                if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseReturnID", "InvPurchaseReturnMaster", "PurchaseInvoiceID = " + cboOrderNo.SelectedValue.ToInt64() }).Rows.Count > 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 989, out MmessageIcon).Replace("*", "GRN");

                                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrPurchase.Enabled = true;
                                    tcPurchase.SelectedTab = tpItemDetails;
                                    return;
                                }
                            }
                            if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromOrder)//Now return against Invoice only
                            {
                                //if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseReturnID", "InvPurchaseReturnMaster", "PurchaseInvoiceID = " + cboOrderNo.SelectedValue.ToInt64() }).Rows.Count > 0)
                                //{
                                //    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 989, out MmessageIcon).Replace("*", "GRN");

                                //    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                //    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                //    TmrPurchase.Enabled = true;
                                //    tcPurchase.SelectedTab = tpItemDetails;
                                //    return;
                                //}
                            }
                            if (PintOperationTypeID == 0)
                                MstrMessageCommon = MstrMessageCommon.Replace("***", "GRN");
                            else
                                MstrMessageCommon = MstrMessageCommon.Replace("***", "Material Return");

                            break;
                        case 4:

                            if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseReturnID", "InvPurchaseReturnMaster", "PurchaseInvoiceID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID }).Rows.Count > 0)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 989, out MmessageIcon).Replace("*", "Invoice");

                                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrPurchase.Enabled = true;
                                tcPurchase.SelectedTab = tpItemDetails;
                                return;
                            }
                            if (Convert.ToInt32(cboPaymentTerms.SelectedValue) == (int)PaymentTerms.Credit)
                            {
                                if (MobjClsBLLPurchase.FillCombos(new string[] { "PM.ReceiptAndPaymentID", "AccReceiptAndPaymentMaster PM Inner Join AccReceiptAndPaymentDetails PD ON PM.ReceiptAndPaymentID=PD.ReceiptAndPaymentID", "PM.OperationTypeID = " + (int)OperationType.PurchaseInvoice + " And PD.ReferenceID =" + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID }).Rows.Count > 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 966, out MmessageIcon);
                                    ErrPurchase.SetError(cboOrderType, MstrMessageCommon.Replace("#", "").Trim());
                                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    TmrPurchase.Enabled = true;
                                    cboOrderType.Focus();
                                    return;
                                }
                            }
                            else if (!MblnAddStatus && MobjClsBLLPurchase.FillCombos(new string[] { "GRNID", "InvGRNMaster", "OrderTypeID = " + (int)OperationOrderType.GRNFromInvoice + " And ReferenceID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID + "" }).Rows.Count > 0)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 962, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblPurchasestatus.Text = MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1);
                                TmrFocus.Enabled = true;
                                return;
                            }
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 955, out MmessageIcon);
                            MstrMessageCommon = MstrMessageCommon.Replace("***", "Invoice");
                            break;
                    }

                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                        {
                            MblnCanShow = false;
                            objFrmCancellationDetails.ShowDialog();
                            MobjClsBLLPurchase.PobjClsDTOPurchase.strCancellationDate = objFrmCancellationDetails.PDtCancellationDate.ToString("dd-MMM-yyyy");
                            txtDescription.Text = objFrmCancellationDetails.PStrDescription;
                            MobjClsBLLPurchase.PobjClsDTOPurchase.intCancelledBy = ClsCommonSettings.UserID;
                            if (!objFrmCancellationDetails.PBlnIsCanelled)
                            {
                                DisplayOrderInfo(MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID);
                                switch (MintFromForm)
                                {
                                    case 1:
                                        MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (Int32)OperationStatusType.PQuotationCancelled;
                                        break;
                                    case 2:
                                        MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (Int32)OperationStatusType.POrderCancelled;
                                        break;
                                    case 3:
                                        MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (Int32)OperationStatusType.GRNCancelled;
                                        break;
                                    case 4:
                                        MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (Int32)OperationStatusType.PInvoiceCancelled;
                                        break;
                                }
                                MblnIsFromCancellation = true;
                                if (SavePurchase(MintFromForm))
                                {
                                    //AddNewPurchase();
                                    DisplayOrderInfo(MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID);
                                }
                                else
                                {
                                    MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = intOldStatusID;
                                }
                                MblnIsFromCancellation = false;
                            }
                        }
                    }
                }
                else
                {
                    MobjClsBLLPurchase.PobjClsDTOPurchase.blnIsCancel = false;
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 956, out MmessageIcon);
                    switch (MintFromForm)
                    {
                        case 1:
                            MstrMessageCommon = MstrMessageCommon.Replace("***", "Quotation");
                            break;
                        case 2:
                            MstrMessageCommon = MstrMessageCommon.Replace("***", "Order");
                            break;
                        case 3:
                            if (PintOperationTypeID == 0)
                                MstrMessageCommon = MstrMessageCommon.Replace("***", "GRN");
                            else
                                MstrMessageCommon = MstrMessageCommon.Replace("***", "Material Return");
                            break;
                        case 4:
                            MstrMessageCommon = MstrMessageCommon.Replace("***", "Invoice");
                            break;
                    }

                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        MblnCanShow = false;
                        switch (MintFromForm)
                        {
                            case 1:
                                MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (int)OperationStatusType.PQuotationOpened;
                                break;
                            case 2:
                                if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.POrderFromQuotation)
                                {
                                    if (ClsCommonSettings.PQApproval)
                                    {
                                        if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseQuotationID", "InvPurchaseQuotationMaster", "PurchaseQuotationID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID In( " + (Int32)OperationStatusType.PQuotationApproved + ")" }).Rows.Count == 0)//," + (int)OperationStatusType.PQuotationSubmitted + ")" }).Rows.Count == 0)
                                        {
                                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 961, out MmessageIcon);
                                            MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation");
                                            ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                            TmrPurchase.Enabled = true;
                                            tcPurchase.SelectedTab = tpItemDetails;
                                            cboOrderNo.Focus();
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseQuotationID", "InvPurchaseQuotationMaster", "PurchaseQuotationID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID In( " + (Int32)OperationStatusType.PQuotationSubmitted + ")" }).Rows.Count == 0)//," + (int)OperationStatusType.PQuotationSubmitted + ")" }).Rows.Count == 0)
                                        {
                                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2286, out MmessageIcon);
                                            MstrMessageCommon = MstrMessageCommon.Replace("*", "Quotation").Replace("approved", "submitted");
                                            ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                            TmrPurchase.Enabled = true;
                                            tcPurchase.SelectedTab = tpItemDetails;
                                            cboOrderNo.Focus();
                                            return;
                                        }
                                    }
                                }
                                if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.POrderFromGRN )
                                {
                                    bool IsGRNExists = MobjClsBLLPurchase.IsGRNExists(strCondition, txtPurchaseNo.Tag.ToInt64());
                                    if (IsGRNExists)
                                    {
                                        //MsMessageCommon = "Some reference Occurs.";
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2276, out MmessageIcon);
                                        if (MintFromForm == 3)
                                            MstrMessageCommon = MstrMessageCommon.Replace("ordered", "Invoiced");
                                        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                        TmrPurchase.Enabled = true;
                                        tcPurchase.SelectedTab = tpItemDetails;
                                        dgvPurchase.Focus();
                                        return;
                                    }

                                }
                                MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (int)OperationStatusType.POrderOpened;
                                MblnIsFromSubmitForApproval = false;
                                break;
                            case 3:
                                MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (int)OperationStatusType.GRNOpened;
                                if (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.GRNFromInvoice)
                                {
                                    if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseInvoiceID", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID = " + (Int32)OperationStatusType.PInvoiceOpened }).Rows.Count == 0)
                                    {
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 960, out MmessageIcon);
                                        MstrMessageCommon = MstrMessageCommon.Replace("*", "Invoice");
                                        ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                        TmrPurchase.Enabled = true;
                                        tcPurchase.SelectedTab = tpItemDetails;
                                        cboOrderNo.Focus();
                                        return;
                                    }
                                }
                                if (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.GRNFromOrder)
                                {
                                    if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseOrderID", "InvPurchaseOrderMaster", "PurchaseOrderID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.POrderApproved + "," + (int)OperationStatusType.POrderSubmitted + ")" }).Rows.Count == 0)//"," + (int)OperationStatusType.POrderSubmitted + ")" }).Rows.Count == 0)
                                    {
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 961, out MmessageIcon);
                                        MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");
                                        ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                        TmrPurchase.Enabled = true;
                                        tcPurchase.SelectedTab = tpItemDetails;
                                        cboOrderNo.Focus();
                                        return;
                                    }
                                }
                                else if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromTransfer)
                                {
                                    if (MobjClsBLLPurchase.FillCombos(new string[] { "StockTransferID,StockTransferNo", "InvStockTransferMaster", "ReferenceID In (Select WarehouseID From InvWarehouse Where CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + ") and StatusID = " + Convert.ToInt32(OperationStatusType.STIssued) + " And OrderTypeID In (" + (int)OperationOrderType.STWareHouseToWareHouseTransfer + "," + (int)OperationOrderType.STWareHouseToWareHouseTransferDemo + ") And StockTransferID = " + cboOrderNo.SelectedValue.ToInt64() }).Rows.Count == 0)
                                    {
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 982, out MmessageIcon);
                                        MstrMessageCommon = MstrMessageCommon.Replace("*", "Invoice");
                                        ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                        lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                        TmrPurchase.Enabled = true;
                                        tcPurchase.SelectedTab = tpItemDetails;
                                        cboOrderNo.Focus();
                                        return;
                                    }
                                }
                                break;
                            case 4:
                                if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.PInvoiceFromOrder)
                                {
                                    if (ClsCommonSettings.POApproval)
                                    {
                                        if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseOrderID", "InvPurchaseOrderMaster", "PurchaseOrderID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.POrderApproved + ")" }).Rows.Count == 0)//"," + (int)OperationStatusType.POrderSubmitted + ")" }).Rows.Count == 0)
                                        {
                                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 961, out MmessageIcon);
                                            MstrMessageCommon = MstrMessageCommon.Replace("*", "Order");
                                            ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                            TmrPurchase.Enabled = true;
                                            tcPurchase.SelectedTab = tpItemDetails;
                                            cboOrderNo.Focus();
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        if (MobjClsBLLPurchase.FillCombos(new string[] { "PurchaseOrderID", "InvPurchaseOrderMaster", "PurchaseOrderID = " + Convert.ToInt64(cboOrderNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.POrderSubmitted + ")" }).Rows.Count == 0)//"," + (int)OperationStatusType.POrderSubmitted + ")" }).Rows.Count == 0)
                                        {
                                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 961, out MmessageIcon);
                                            MstrMessageCommon = MstrMessageCommon.Replace("*", "Order").Replace("approved", "submitted");
                                            ErrPurchase.SetError(cboOrderNo, MstrMessageCommon.Replace("#", "").Trim());
                                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                            TmrPurchase.Enabled = true;
                                            tcPurchase.SelectedTab = tpItemDetails;
                                            cboOrderNo.Focus();
                                            return;
                                        }
                                    }
                                    decimal decExpenseAmt, decPaidAmount = 0;
                                    decExpenseAmt = MobjClsBLLPurchase.FillCombos(new string[] { "ISNULL(Sum(TotalAmount),0) as ExpenseAmt", "InvExpenseMaster", "OperationTypeID = " + (int)OperationType.PurchaseOrder + " And ReferenceID = " + cboOrderNo.SelectedValue.ToInt32() + " And IsDirectExpense=0" }).Rows[0]["ExpenseAmt"].ToDecimal();
                                    decPaidAmount = MobjClsBLLPurchase.FillCombos(new string[] { "ISNULL(Sum(PD.Amount),0) as PaidAmt", "AccReceiptAndPaymentMaster PM INNER JOIN AccReceiptAndPaymentDetails PD ON PD.ReceiptAndPaymentID=PM.ReceiptAndPaymentID", "PM.OperationTypeID = " + (int)OperationType.PurchaseOrder + " And PD.ReferenceID = " + (int)cboOrderNo.SelectedValue.ToInt32() + " And PM.ReceiptAndPaymentTypeID = " + (int)PaymentTypes.DirectExpense }).Rows[0]["PaidAmt"].ToDecimal();
                                    if (decPaidAmount < decExpenseAmt)
                                    {
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 975, out MmessageIcon).Replace("#", "");
                                        MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        tcPurchase.SelectedTab = tpItemDetails;
                                        return;
                                    }
                                }
                                MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (int)OperationStatusType.PInvoiceOpened;
                                break;
                        }
                        MblnIsFromCancellation = true;
                        DisplayPurchaseOrderDetail(MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID);
                        CalculateTotalAmt();
                        BindingNavigatorSaveItem.Enabled = false;
                        if (SavePurchase(MintFromForm))
                        {
                            //  AddNewPurchase();
                            DisplayOrderInfo(MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID);
                        }
                        else
                        {
                        }

                        MblnIsFromCancellation = false;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorCancelItem_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in BindingNavigatorCancelItem_Click() " + ex.Message, 2);
            }
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                MblnCanShow = true;

                if (IsApprovedDateExeedsQuotationDueDate())
                {
                    if (MblnCanShow)
                    {
                        switch (MintFromForm)
                        {
                            case 1:
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 953, out MmessageIcon);
                                MstrMessageCommon = MstrMessageCommon.Replace("***", "Quotation");
                                break;
                            case 2:
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 953, out MmessageIcon);
                                MstrMessageCommon = MstrMessageCommon.Replace("***", "Order");
                                break;
                        }
                    }

                    if (!MblnCanShow || MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(true))
                        {
                            MblnCanShow = false;
                            objFrmCancellationDetails.Text = "Approval Details";
                            objFrmCancellationDetails.lblCancelledDate.Text = "Approval Date";
                            objFrmCancellationDetails.ShowDialog();
                            MobjClsBLLPurchase.PobjClsDTOPurchase.strApprovedDate = objFrmCancellationDetails.PDtCancellationDate.ToString("dd-MMM-yyyy");
                            txtDescription.Text = objFrmCancellationDetails.PStrDescription;
                            MobjClsBLLPurchase.PobjClsDTOPurchase.intVerificationCriteria = objFrmCancellationDetails.PintVerificationCriteria;
                            if (!objFrmCancellationDetails.PBlnIsCanelled)
                            {
                                txtPurchaseNo.Focus();
                                if (MintFromForm == 1)
                                {
                                    MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (Int32)OperationStatusType.PQuotationApproved;
                                }
                                else if (MintFromForm == 2)
                                {
                                    MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (Int32)OperationStatusType.POrderApproved;
                                }
                                MobjClsBLLPurchase.PobjClsDTOPurchase.intApprovedBy = ClsCommonSettings.UserID;
                                MobjClsBLLPurchase.PobjClsDTOPurchase.strApprovedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                                MblnIsFromCancellation = true;
                                if (SavePurchase(MintFromForm))
                                {
                                    //   AddNewPurchase();
                                    DisplayOrderInfo(MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID);
                                    DisplayAllNoInSearchGrid();
                                }
                                else
                                {
                                }
                                MblnIsFromCancellation = false;
                                blnIsFrmApproval = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnApproveClick() " + ex.Message);
                MObjLogs.WriteLog("Error in BtnApproveClick() " + ex.Message, 2);
            }
        }

        private void dgvPurchase_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (dgvPurchase.ReadOnly)
                e.Cancel = true;
            else
            {
                dgvPurchase.Rows[e.Row.Index].Cells["Uom"].ReadOnly = false;

                if (MintFromForm == 1)
                {
                    //if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.PQuotationFromRFQ && cboOrderNo.SelectedValue != null)
                    //{
                    //    e.Cancel = true;
                    //}
                }
                else if (MintFromForm == 2)
                {
                    if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.POrderFromQuotation && cboOrderNo.SelectedValue != null)
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                switch (MintFromForm)
                {
                    case 1:

                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 954, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("***", "Quotation");
                        break;
                    case 2:
                        if (MobjClsBLLPurchase.FillCombos(new string[] { "PM.ReceiptAndPaymentID", "AccReceiptAndPaymentMaster PM Inner Join AccReceiptAndPaymentDetails PD ON PD.ReceiptAndPaymentID=PM.ReceiptAndPaymentID", "PM.OperationTypeID = " + (int)OperationType.PurchaseOrder + " And PD.ReferenceID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID }).Rows.Count > 0)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 967, out MmessageIcon);
                            ErrPurchase.SetError(cboOrderType, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            cboOrderType.Focus();
                            return;
                        }
                        if (MobjClsBLLPurchase.FillCombos(new string[] { "ExpenseID", "InvExpenseMaster", "OperationTypeID = " + (int)OperationType.PurchaseOrder + " And ReferenceID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID + " And IsDirectExpense = 0" }).Rows.Count > 0)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 997, out MmessageIcon).Replace("*", "Order").Replace("delete", "reject");
                            ErrPurchase.SetError(cboOrderType, MstrMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrPurchase.Enabled = true;
                            tcPurchase.SelectedTab = tpItemDetails;
                            cboOrderType.Focus();
                            return;
                        }
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 954, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("***", "Order");
                        break;
                }

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {

                    using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                    {
                        MblnCanShow = false;
                        objFrmCancellationDetails.Text = "Rejection Details";
                        objFrmCancellationDetails.lblCancelledDate.Text = "Rejected Date";
                        objFrmCancellationDetails.ShowDialog();
                        MobjClsBLLPurchase.PobjClsDTOPurchase.strApprovedDate = objFrmCancellationDetails.PDtCancellationDate.ToString("dd-MMM-yyyy");
                        txtDescription.Text = objFrmCancellationDetails.PStrDescription;
                        MobjClsBLLPurchase.PobjClsDTOPurchase.intVerificationCriteria = objFrmCancellationDetails.PintVerificationCriteria;
                        if (!objFrmCancellationDetails.PBlnIsCanelled)
                        {
                            txtPurchaseNo.Focus();
                            if (MintFromForm == 1)
                            {
                                MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (Int32)OperationStatusType.PQuotationRejected;
                            }
                            else if (MintFromForm == 2)
                            {
                                MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (Int32)OperationStatusType.POrderRejected;
                            }
                            MobjClsBLLPurchase.PobjClsDTOPurchase.intApprovedBy = ClsCommonSettings.UserID;
                            MobjClsBLLPurchase.PobjClsDTOPurchase.strApprovedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                            MblnIsFromCancellation = true;
                            if (SavePurchase(MintFromForm))
                            {
                                //AddNewPurchase();
                                DisplayOrderInfo(MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID);
                                DisplayAllNoInSearchGrid();
                            }
                            else
                            {
                            }
                            MblnIsFromCancellation = false;
                            blnIsFrmApproval = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnReject_Click() " + ex.Message);

                MObjLogs.WriteLog("Error in btnReject_Click() " + ex.Message, 2);
            }
        }

        private void TxtSsearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\'')
                e.Handled = true;
        }

        private void FrmPurchase_Shown(object sender, EventArgs e)
        {
            cboCompany.Focus();
        }

        private void btnAddExpense_Click(object sender, EventArgs e)
        {
            try
            {
                bool blnShow = false;
                bool blnCalculateExpense = true;

                if ((txtPurchaseNo.Tag != null && Convert.ToString(txtPurchaseNo.Tag) != "" && Convert.ToString(txtPurchaseNo.Tag) != "0") || txtExpenseAmount.Text.ToDecimal() != 0)
                {
                    blnShow = true;
                }
                if (blnShow)
                {
                    using (frmExpense objfrmExpense = new frmExpense(false))
                    {
                        objfrmExpense.intModuleID = 1;
                        Int64 intReferenceID = 0;
                        int intOperationType = 0;
                        if (MintFromForm == 1)
                        {
                            intOperationType = Convert.ToInt32(OperationType.PurchaseQuotation);
                            intReferenceID = Convert.ToInt64(txtPurchaseNo.Tag);
                        }
                        else if (MintFromForm == 2)
                        {
                            if (Convert.ToInt64(cboOrderNo.SelectedValue) != 0 && Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.POrderFromQuotation && Convert.ToInt64(txtPurchaseNo.Tag) == 0)
                            {
                                blnCalculateExpense = false;
                                intOperationType = Convert.ToInt32(OperationType.PurchaseQuotation);
                                objfrmExpense.PBlnIsEditable = false;
                                intReferenceID = Convert.ToInt64(cboOrderNo.SelectedValue);
                            }
                            else
                            {
                                intOperationType = Convert.ToInt32(OperationType.PurchaseOrder);
                                if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.POrderFromQuotation)
                                    objfrmExpense.PBlnIsEditable = false;
                                else
                                    objfrmExpense.PBlnIsEditable = true;
                                intReferenceID = Convert.ToInt64(txtPurchaseNo.Tag);
                            }
                        }
                        else if (MintFromForm == 3)
                            intOperationType = Convert.ToInt32(OperationType.GRN);
                        else if (MintFromForm == 4)
                        {
                            if (Convert.ToInt64(cboOrderNo.SelectedValue) != 0 && Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.PInvoiceFromOrder && Convert.ToInt64(txtPurchaseNo.Tag) == 0)
                            {
                                blnCalculateExpense = false;
                                intOperationType = Convert.ToInt32(OperationType.PurchaseOrder);
                                objfrmExpense.PBlnIsEditable = false;
                                intReferenceID = Convert.ToInt64(cboOrderNo.SelectedValue);
                            }
                            else
                            {
                                intOperationType = Convert.ToInt32(OperationType.PurchaseInvoice);
                                objfrmExpense.PBlnIsEditable = false;
                                if (Convert.ToInt32(cboOrderType.SelectedValue) == (int)OperationOrderType.PInvoiceFromOrder)
                                    objfrmExpense.PBlnIsEditable = false;
                                else
                                    objfrmExpense.PBlnIsEditable = true;
                                intReferenceID = Convert.ToInt64(txtPurchaseNo.Tag);
                            }
                        }
                        objfrmExpense.PintOperationType = intOperationType;
                        objfrmExpense.PintOperationID = intReferenceID;
                        objfrmExpense.PintCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                        if (intPurchaseIDToLoad != 0)
                            objfrmExpense.PBlnIsEditable = false;

                        objfrmExpense.ShowDialog();
                        if (blnCalculateExpense)
                            txtExpenseAmount.Text = objfrmExpense.GetExpenseAmount(intOperationType, Convert.ToInt64(txtPurchaseNo.Tag), 0).ToString("F" + MintExchangeCurrencyScale);
                        CalculateNetTotal();
                        MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandAmount = Convert.ToDecimal(txtSubTotal.Text);
                        MobjClsBLLPurchase.PobjClsDTOPurchase.decNetAmount = Convert.ToDecimal(txtTotalAmount.Text);
                        if (cboDiscount.SelectedItem != null)
                        {
                            MobjClsBLLPurchase.PobjClsDTOPurchase.blnGrandDiscount = cboDiscount.SelectedItem.ToString() == "%" ? true : false;
                            MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountAmt = (cboDiscount.SelectedItem.ToString() == "%" ? Convert.ToDecimal(txtDiscount.Text) / 100 * Convert.ToDecimal(txtSubTotal.Text) : Convert.ToDecimal(txtDiscount.Text));
                            MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountPercent = (cboDiscount.SelectedItem.ToString() == "%" ? Convert.ToDecimal(txtDiscount.Text) : (0.0).ToDecimal());

                        }
                        MobjClsBLLPurchase.PobjClsDTOPurchase.decNetAmountRounded = Math.Ceiling(MobjClsBLLPurchase.PobjClsDTOPurchase.decNetAmount);
                        MobjClsBLLPurchase.PobjClsDTOPurchase.decExpenseAmount = txtExpenseAmount.Text.ToDecimal();
                        MobjClsBLLPurchase.UpdateTotalAmount(MintFromForm);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnAddExpense_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in btnAddExpense_Click() " + ex.Message, 2);
            }
        }

        private void bnExpense_Click(object sender, EventArgs e)
        {
            btnAddExpense_Click(null, null);
        }

        private void btnDocuments_Click(object sender, EventArgs e)
        {
            try
            {
                ShowDocumentMaster();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnDocuments_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in btnDocuments_Click() " + ex.Message, 2);
            }
        }

        private void FrmPurchase_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.MdiFormClosing)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4214, out MmessageIcon).Replace("#", "").Trim();
                if (!MblnChangeStatus || MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                    DialogResult.Yes)
                {
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void dgvPurchase_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            try
            {
                CalculateTotalAmt();
                CalculateNetTotal();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvPurchase_UserDeletedRow() " + ex.Message);
                MObjLogs.WriteLog("Error in dgvPurchase_UserDeletedRow() " + ex.Message, 2);
            }
        }

        private void TmrPurchaseOrder_Tick(object sender, EventArgs e)
        {
            ErrPurchase.Clear();
            lblPurchasestatus.Text = "";
            TmrPurchase.Enabled = false;
        }



        private void cboCurrency_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cboCurrency.SelectedValue != null)
            {
                DataTable datCurrency = MobjClsBLLPurchase.FillCombos(new string[] { "CurrencyID,Scale", "CurrencyReference", "CurrencyID = " + Convert.ToInt32(cboCurrency.SelectedValue) });
                if (datCurrency.Rows.Count > 0)
                    MintExchangeCurrencyScale = Convert.ToInt32(datCurrency.Rows[0]["Scale"]);
            }
            CalculateTotalAmt();
            CalculateNetTotal();
            CalculateExchangeCurrencyRate();
        }

        private void bnSubmitForApproval_Click(object sender, EventArgs e)
        {
            bool status = true;
            MblnIsFromSubmitForApproval = true;
            if (dtpDueDate.Value < ClsCommonSettings.GetServerDate().Date)
            {
                status = false;
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 37, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    status = false;
                    return;
                }
                else
                {
                    status = true;
                }
            }
            if (status == true)
            {
                try
                {
                    txtPurchaseNo.Focus();
                    int intOldStatusID = MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID;
                    string strSupDocuments = MobjClsBLLPurchase.IsSupplierDocumentsValid((int)OperationType.Suppliers, cboSupplier.SelectedValue.ToInt32(), cboCompany.SelectedValue.ToInt32());
                    if (!string.IsNullOrEmpty(strSupDocuments))
                    {
                        MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 977, out MmessageIcon).Replace("***", strSupDocuments).Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    string strDocumentExpired = MobjClsBLLPurchase.IsMandatoryDocumentExpired((int)OperationType.Suppliers, cboSupplier.SelectedValue.ToInt32(), cboCompany.SelectedValue.ToInt32());
                    if (!string.IsNullOrEmpty(strDocumentExpired))
                    {
                        MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 978, out MmessageIcon).Replace("***", strDocumentExpired).Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    if (MintFromForm == 1)
                    {
                        string strDocument = MobjClsBLLPurchase.IsMandatoryDocumentsEntered((int)OperationType.PurchaseQuotation, MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID, cboCompany.SelectedValue.ToInt32());
                        if (!string.IsNullOrEmpty(strDocument))
                        {
                            MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 974, out MmessageIcon).Replace("***", strDocument).Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        if (ClsCommonSettings.PQApproval)
                            MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (Int32)OperationStatusType.PQuotationSubmittedForApproval;
                        else
                            MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (int)OperationStatusType.PQuotationSubmitted;
                    }
                    else if (MintFromForm == 2)
                    {
                        string strDocument = MobjClsBLLPurchase.IsMandatoryDocumentsEntered((int)OperationType.PurchaseOrder, MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID, cboCompany.SelectedValue.ToInt32());
                        if (!string.IsNullOrEmpty(strDocument))
                        {
                            MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 974, out MmessageIcon).Replace("***", strDocument).Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        if (ClsCommonSettings.POApproval)
                            MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (Int32)OperationStatusType.POrderSubmittedForApproval;
                        else
                            MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (int)OperationStatusType.POrderSubmitted;
                    }

                    MobjClsBLLPurchase.PobjClsDTOPurchase.intApprovedBy = ClsCommonSettings.UserID;
                    MobjClsBLLPurchase.PobjClsDTOPurchase.strApprovedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");

                    if (SavePurchase(MintFromForm))
                    {
                        //AddNewPurchase();
                        DisplayOrderInfo(MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID);
                        DisplayAllNoInSearchGrid();
                    }
                    else
                    {
                        MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = intOldStatusID;
                    }
                }
                catch (Exception ex)
                {
                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error in BtnSubmitForApprovalClick() " + ex.Message);
                    MObjLogs.WriteLog("Error in BtnSubmitForApprovalClick() " + ex.Message, 2);
                }
            }
            else
                AddNewPurchase();
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            //for (int i = 0; i < dgvPurchase.Rows.Count; i++)
            //    FillDiscountColumn(i, false);
            LoadCombos(8, "");
            Changestatus();
        }

        private void dgvPurchaseDisplay_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvPurchaseDisplay.Rows.Count > 0)
                {
                    if (Convert.ToInt64(dgvPurchaseDisplay.CurrentRow.Cells[0].Value) != 0)
                    {
                        MblnAddStatus = false;
                        ClearControls();
                        //MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID = Convert.ToInt32(dgvPurchaseDisplay.CurrentRow.Cells[0].Value);
                        DisplayOrderInfo(Convert.ToInt64(dgvPurchaseDisplay.CurrentRow.Cells[0].Value));
                        BtnPrint.Enabled = MblnPrintEmailPermission;
                        BtnEmail.Enabled = MblnPrintEmailPermission;
                        if (ClsCommonSettings.PblnIsLocationWise)
                        {
                            btnPickList.Enabled = MblnPrintEmailPermission;
                            btnPickListEmail.Enabled = MblnPrintEmailPermission;
                        }
                        string strPurchaseNo = "";
                        switch (MintFromForm)
                        {
                            case 1:
                                strPurchaseNo += " Quotation No ";
                                break;
                            case 2:
                                strPurchaseNo += " Order No ";
                                break;
                            case 3:
                                if (PintOperationTypeID == 0)
                                    strPurchaseNo += " GRN No ";
                                else
                                    strPurchaseNo += " Material Return No ";
                                break;
                            case 4:
                                strPurchaseNo += " Invoice No ";
                                break;
                        }
                        strPurchaseNo += dgvPurchaseDisplay.CurrentRow.Cells[1].Value.ToString();
                        lblPurchasestatus.Text = "Details of " + strPurchaseNo;
                        tcPurchase.SelectedTab = tpItemDetails;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvPurchaseDisplay_CellClick() " + ex.Message);
                MObjLogs.WriteLog("Error in dgvPurchaseDisplay_CellClick() " + ex.Message, 2);
            }
        }

        private void btnAddressChange_Click(object sender, EventArgs e)
        {
            Changestatus();
            CMSVendorAddress.Show(btnTContextmenu, btnTContextmenu.PointToClient(System.Windows.Forms.Cursor.Position));
        }

        private void txtPurchaseNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetterOrDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
                e.Handled = true;
            else
                e.Handled = false;
        }

        private void cboSCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable datCombos = null; //bool blnIsEnabled = false;
            string strSearchCondition = "";
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                strSearchCondition = "UM.RoleID Not In (1,2)";
            }
            if (!string.IsNullOrEmpty(strSearchCondition))
                strSearchCondition = strSearchCondition + " AND ";
            strSearchCondition = strSearchCondition + "UM.UserID <> 1";

            //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
            //{
            if (cboSCompany.SelectedValue == null || Convert.ToInt32(cboSCompany.SelectedValue) == -2)
            {
                //DataTable datCompany = (DataTable)cboSCompany.DataSource;
                //string strFilterCondition = "";
                //if (datCompany.Rows.Count > 0)
                //{
                //    strFilterCondition = "CompanyID In (";
                //    foreach (DataRow dr in datCompany.Rows)
                //        strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                //    strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                //    strFilterCondition += ")";
                //}
                //datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN " + strTableUserMaster + " UM " +
                //  " ON EM.EmployeeID=UM.EmployeeID INNER JOIN " + MstrTempTableName + " IM ON UM.UserID=IM.CreatedBy", "IM." + strFilterCondition });
                datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "UM.UserID,IsNull(EM.FirstName,UM.UserName) as UserName", "UserMaster UM Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID", strSearchCondition });
            }
            else
            {
                //datCombos = MobjclsBLLSTSales.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN " + strTableUserMaster + " UM " +
                //    " ON EM.EmployeeID=UM.EmployeeID INNER JOIN " + MstrTempTableName + " IM ON UM.UserID=IM.CreatedBy", "IM.CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue) });
                if (!string.IsNullOrEmpty(strSearchCondition))
                    strSearchCondition = " And " + strSearchCondition;
                datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "UM.UserID,IsNull(EM.FirstName,UM.UserName) as UserName", "UserMaster UM Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID ", "Case IsNull(EM.EmployeeID,0) When 0 Then " + cboSCompany.SelectedValue.ToInt32() + " Else EM.CompanyID  End = " + cboSCompany.SelectedValue.ToInt32() + "" + strSearchCondition });
            }
            //}
            //else
            //{
            //    //if (cboSCompany.SelectedValue == null || Convert.ToInt32(cboSCompany.SelectedValue) == -2)
            //    //{
            //    //    datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.FirstName","EmployeeMaster EM INNER JOIN UserMaster UM " +
            //    //        " ON EM.EmployeeID=UM.EmployeeID INNER JOIN " + strTempTableName + " IM ON UM.UserID=IM.CreatedBy", "" +
            //    //        " IM.CompanyID IN(SELECT CompanyID FROM STRoleFIeldsDetails WHERE RoleID=" + ClsCommonSettings.RoleID + 
            //    //        " AND ControlID=" + intTempEmployee + " AND IsVisible=1)" });
            //    //}
            //    //else
            //    //{
            //    //    datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.FirstName","EmployeeMaster EM INNER JOIN UserMaster UM " +
            //    //        " ON EM.EmployeeID=UM.EmployeeID INNER JOIN " + strTempTableName + " IM ON UM.UserID=IM.CreatedBy", "" +
            //    //        " IM.CompanyID IN(SELECT CompanyID FROM STRoleFIeldsDetails WHERE RoleID=" + ClsCommonSettings.RoleID + 
            //    //        " AND ControlID=" + intTempEmployee + " AND IsVisible=1 AND CompanyID=" + Convert.ToInt32(cboSCompany.SelectedValue) + ")" });
            //    //}
            //    try
            //    {
            //        DataTable datTemp = MobjClsBLLPurchase.FillCombos(new string[] { "IsVisible", "STRoleFieldsDetails", "" + 
            //            "RoleID=" + ClsCommonSettings.RoleID + " AND CompanyID=" + ClsCommonSettings.CompanyID + " " +
            //            "AND ControlID=" + intTempEmployee + " AND IsVisible=1" });
            //        if (datTemp != null)
            //            if (datTemp.Rows.Count > 0)
            //                datCombos = MobjClsBLLPurchase.GetEmployeeByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, intTempCompany);

            //        if (datCombos == null)
            //            datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
            //                            " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
            //        else
            //        {
            //            if (datCombos.Rows.Count == 0)
            //                datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
            //                            " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
            //        }
            //    }
            //    catch { }
            //}
            cboSExecutive.ValueMember = "UserID";
            cboSExecutive.DisplayMember = "UserName";
            cboSExecutive.DataSource = datCombos;

        }
        public void SettingPurchaseAccount()
        {
            clsBLLAccountSettings MobjclsBLLAccountSettings = new clsBLLAccountSettings();
            DataTable datPurchaseAccount = new DataTable();
            // cboPurchaseAccount.DataSource = null;
            datPurchaseAccount = MobjclsBLLAccountSettings.FillCombos(new string[] { "AccountID,AccountName", "AccAccountMaster", "AccountGroupID=" + (int)AccountGroups.PurchaseAccounts });
            cboPurchaseAccount.ValueMember = "AccountID";
            cboPurchaseAccount.DisplayMember = "AccountName";
            cboPurchaseAccount.DataSource = datPurchaseAccount;
            //cboPaymentTerms_SelectedIndexChanged(null, null);
        }
        public void SettingPurchaseAccounts()
        {
            clsBLLAccountSettings MobjclsBLLAccountSettings = new clsBLLAccountSettings();
            DataTable datPurchaseAccount = new DataTable();
            // cboPurchaseAccount.DataSource = null;
            datPurchaseAccount = MobjclsBLLAccountSettings.FillCombos(new string[] { "AccountID,AccountName", "AccAccountMaster", "AccountGroupID=" + Convert.ToInt32(AccountGroups.PurchaseAccounts) });
            cboPurchaseAccount.ValueMember = "AccountID";
            cboPurchaseAccount.DisplayMember = "AccountName";
            cboPurchaseAccount.DataSource = datPurchaseAccount;
            //cboPaymentTerms_SelectedIndexChanged(null, null);
        }

        private void btnAccountSettings_Click(object sender, EventArgs e)
        {
            using (FrmAccountSettings objFrmAccountSettings = new FrmAccountSettings())
            {
                objFrmAccountSettings.ShowDialog();
            }
        }

        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void btnIncoTerms_Click(object sender, EventArgs e)
        {
            FrmCommonRef objFrmCommonRef = new FrmCommonRef("Inco Terms", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "IncoTermsID,Description As IncoTerms", "STIncoTermsReference", "");
            objFrmCommonRef.ShowDialog();
            int intCurrentComboID = cboIncoTerms.SelectedValue.ToInt32();
            LoadCombos(20, "");
            if (objFrmCommonRef.NewID != 0)
                cboIncoTerms.SelectedValue = objFrmCommonRef.NewID;
            else
                cboIncoTerms.SelectedValue = intCurrentComboID;
        }

        private void btnContainerType_Click(object sender, EventArgs e)
        {
            FrmCommonRef objFrmCommonRef = new FrmCommonRef("Container Type", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "ContainerTypeID,Description As ContainerType", "STContainerTypeReference", "");
            objFrmCommonRef.ShowDialog();
            int intCurrentComboID = cboContainerType.SelectedValue.ToInt32();
            LoadCombos(19, "");
            if (objFrmCommonRef.NewID != 0)
                cboContainerType.SelectedValue = objFrmCommonRef.NewID;
            else
                cboContainerType.SelectedValue = intCurrentComboID;
        }

        private void dgvPurchase_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (MintFromForm == 3 && (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromTransfer))
                MobjclsBLLCommonUtility.SetSerialNo(dgvPurchase, e.RowIndex, true);
            else
                MobjclsBLLCommonUtility.SetSerialNo(dgvPurchase, e.RowIndex, false);
        }

        private void btnSupplierHistory_Click(object sender, EventArgs e)
        {
            if (cboSupplier.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 908, out MmessageIcon);
                ErrPurchase.SetError(cboSupplier, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrPurchase.Enabled = true;
                cboSupplier.Focus();
                return;
            }
            using (FrmVendorHistory objFrmVendorHistory = new FrmVendorHistory(2, cboSupplier.SelectedValue.ToInt32()))
            {
                objFrmVendorHistory.ShowDialog();
            }
        }

        private void TxtBSubtotal_TextChanged(object sender, EventArgs e)
        {
            int intDiscountTypeID = cboDiscount.SelectedValue.ToInt32();
            LoadCombos(8, "");
            cboDiscount.SelectedValue = intDiscountTypeID;
        }

        #endregion

        private void btnSuggest_Click(object sender, EventArgs e)
        {
            try
            {
                switch (MintFromForm)
                {
                    case 1:

                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 980, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("***", "Quotation");
                        break;
                    case 2:
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 980, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("***", "Order");
                        break;
                }

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {

                    //using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                    //{
                    //    objFrmCancellationDetails.Text = "Suggestion Details";
                    //    objFrmCancellationDetails.lblCancelledDate.Text = "Suggested Date";
                    //    objFrmCancellationDetails.ShowDialog();
                    //    MobjClsBLLPurchase.PobjClsDTOPurchase.strDescription = objFrmCancellationDetails.PStrDescription;
                    //    int intOperationTypeID = 0;
                    //    if (!objFrmCancellationDetails.PBlnIsCanelled)
                    //    {
                    //        txtPurchaseNo.Focus();
                    //        if (MintFromForm == 1)
                    //        {
                    //            MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (Int32)OperationStatusType.PQuotationSuggested;
                    //            intOperationTypeID = (int)OperationType.PurchaseQuotation;
                    //        }
                    //        else if (MintFromForm == 2)
                    //        {
                    //            MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (Int32)PurchaseSettings.OrderSuggested;
                    //            intOperationTypeID = (int)OperationType.PurchaseOrder;
                    //        }
                    //        MobjClsBLLPurchase.PobjClsDTOPurchase.intCancelledBy = ClsCommonSettings.UserID;
                    //        MobjClsBLLPurchase.PobjClsDTOPurchase.strCancellationDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");

                    //        if (MobjClsBLLPurchase.Suggest(intOperationTypeID))
                    //        {
                    //            AddNewPurchase();
                    //            DisplayAllNoInSearchGrid();
                    //        }
                    //        else
                    //        {
                    //        }
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnSuggest_Click() " + ex.Message);

                MObjLogs.WriteLog("Error in btnSuggest_Click() " + ex.Message, 2);
            }
        }

        private void dgvSuggestions_MouseLeave(object sender, EventArgs e)
        {
            // pnlSuggestions.Visible = false;
        }

        private void btnShowComments_Click(object sender, EventArgs e)
        {
            // pnlSuggestions.Focus();
            //    dgvSuggestions.Focus();
            // pnlSuggestions.Visible = true;
            DataTable datVerification = new DataTable();
            if (PintApprovePermission == (int)ApprovePermission.Approve || PintApprovePermission == (int)ApprovePermission.Both)
                datVerification = GetVerificationHistoryStatus(true);
            else
                datVerification = GetVerificationHistoryStatus(false);
            dgvSuggestions.Rows.Clear();
            for (int i = 0; i < datVerification.Rows.Count; i++)
            {
                dgvSuggestions.Rows.Add();
                dgvSuggestions[UserName.Index, i].Value = datVerification.Rows[i]["UserName"];
                dgvSuggestions[Status.Index, i].Value = datVerification.Rows[i]["Status"];
                dgvSuggestions[VerifiedDate.Index, i].Value = datVerification.Rows[i]["VerifiedDate"];
                dgvSuggestions[Comment.Index, i].Value = datVerification.Rows[i]["Comment"];
            }

        }

        private void btnDeny_Click(object sender, EventArgs e)
        {
            try
            {
                switch (MintFromForm)
                {
                    case 1:

                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 981, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("***", "Quotation");
                        break;
                    case 2:
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 981, out MmessageIcon);
                        MstrMessageCommon = MstrMessageCommon.Replace("***", "Order");
                        break;
                }

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {

                    //using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                    //{
                    //    objFrmCancellationDetails.Text = "Suggestion Details";
                    //    objFrmCancellationDetails.lblCancelledDate.Text = "Suggested Date";
                    //    objFrmCancellationDetails.ShowDialog();
                    //    MobjClsBLLPurchase.PobjClsDTOPurchase.strDescription = objFrmCancellationDetails.PStrDescription;
                    //    int intOperationTypeID = 0;
                    //    if (!objFrmCancellationDetails.PBlnIsCanelled)
                    //    {
                    //        txtPurchaseNo.Focus();
                    //        if (MintFromForm == 1)
                    //        {
                    //            MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (Int32)OperationStatusType.PQuotationDenied;
                    //            intOperationTypeID = (int)OperationType.PurchaseQuotation;
                    //        }
                    //        else if (MintFromForm == 2)
                    //        {
                    //            MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID = (Int32)OperationStatusType.POrderDenied;
                    //            intOperationTypeID = (int)OperationType.PurchaseOrder;
                    //        }
                    //        MobjClsBLLPurchase.PobjClsDTOPurchase.intCancelledBy = ClsCommonSettings.UserID;
                    //        MobjClsBLLPurchase.PobjClsDTOPurchase.strCancellationDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");

                    //        if (MobjClsBLLPurchase.Suggest(intOperationTypeID))
                    //        {
                    //            AddNewPurchase();
                    //            DisplayAllNoInSearchGrid();
                    //        }
                    //        else
                    //        {
                    //        }
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnSuggest_Click() " + ex.Message);

                MObjLogs.WriteLog("Error in btnSuggest_Click() " + ex.Message, 2);
            }
        }

        private bool FillItemLocationDetails()
        {

            if (cboWarehouse.SelectedValue.ToInt32() != 0)
            {
                int iTempID = 0;
                iTempID = CheckDuplicationInGrid();
                if (iTempID != -1)
                {
                    //MsMessageCommon = "Please check duplication of values.";
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 902, out MmessageIcon);
                    ErrPurchase.SetError(dgvPurchase, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    //mObjNotification.CallErrorMessage(MaMessageArr, 902, ClsCommonSettings.MessageCaption);
                    lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPurchase.Enabled = true;
                    dgvPurchase.Focus();
                    dgvPurchase.CurrentCell = dgvPurchase["ItemCode", iTempID];
                    tcPurchase.SelectedTab = tpItemDetails;
                    return false;
                }
                dgvLocationDetails.Rows.Clear();

                //for (int i = 0; i < dgvLocationDetails.Rows.Count; i++)
                //{

                //    if (dgvLocationDetails.Rows[i].Cells["LItemID"].Value != null && dgvLocationDetails.Rows[i].Cells["LBatchNo"].Value != null)
                //    {
                //        bool blnExists = false;
                //        foreach (DataGridViewRow dr in dgvPurchase.Rows)
                //        {
                //            if (dr.Cells["ItemID"].Value != null && dr.Cells["BatchNumber"].Value != null)
                //            {
                //                if (dr.Cells["ItemID"].Value.ToInt32() == dgvLocationDetails.Rows[i].Cells["LItemID"].Value.ToInt32() && dr.Cells["BatchNumber"].Value.ToString() == dgvLocationDetails.Rows[i].Cells["LBatchNo"].Value.ToString())
                //                {
                //                    blnExists = true;
                //                    break;
                //                }
                //            }
                //        }
                //        if (!blnExists)
                //        {
                //            dgvLocationDetails.Rows.RemoveAt(i);
                //            i--;
                //        }
                //    }
                //}

                //bool blnIsValid = true;
                //foreach (DataGridViewRow dr in dgvPurchase.Rows)
                //{
                //    if (dr.Cells[ItemID.Index].Value.ToInt32() != 0 && dr.Cells[Batchnumber.Index].Value != null)
                //    {
                //        decimal decQuantity = dr.Cells[Quantity.Index].Value.ToDecimal();
                //        decimal decLocationQuantity = 0;
                //        foreach (DataGridViewRow drRow in dgvLocationDetails.Rows)
                //        {
                //            if (dr.Cells[ItemID.Index].Value.ToInt32() == drRow.Cells[LItemID.Index].Value.ToInt32() && Convert.ToString(dr.Cells[Batchnumber.Index].Value) == Convert.ToString(drRow.Cells[LBatchNo.Index].Value))
                //                decLocationQuantity += drRow.Cells[LQuantity.Index].Value.ToDecimal();
                //        }
                //        if (decQuantity != decLocationQuantity)
                //        {
                //            blnIsValid = false;
                //            break;
                //        }
                //    }
                //}


                //if (!blnIsValid)
                //{
                if (dgvLocationDetails.Rows.Count == 1)
                {
                    dgvLocationDetails.Rows.Clear(); dtItemDetails.Rows.Clear();
                    foreach (DataGridViewRow drPurchaseRow in dgvPurchase.Rows)
                    {

                        if (drPurchaseRow.Cells["ItemID"].Value != null && drPurchaseRow.Cells["BatchNumber"].Value != null)
                        {

                            DataRow drItemDetailsRow = dtItemDetails.NewRow();
                            drItemDetailsRow["ItemID"] = drPurchaseRow.Cells["ItemID"].Value.ToInt32();
                            drItemDetailsRow["ItemCode"] = drPurchaseRow.Cells["ItemCode"].Value.ToString();
                            drItemDetailsRow["ItemName"] = drPurchaseRow.Cells["ItemName"].Value.ToString();
                            drItemDetailsRow["BatchNo"] = drPurchaseRow.Cells["BatchNumber"].Value.ToString();
                            drItemDetailsRow["UomID"] = drPurchaseRow.Cells["Uom"].Tag.ToInt32();
                            dtItemDetails.Rows.Add(drItemDetailsRow);

                            // dgvLocationDetails.Rows.Add();
                            dgvLocationDetails.RowCount = dgvLocationDetails.RowCount + 1;
                            int intRowIndex = dgvLocationDetails.RowCount - 2;
                            dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value = drPurchaseRow.Cells["ItemID"].Value.ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LItemCode"].Value = drPurchaseRow.Cells["ItemCode"].Value.ToString();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LItemName"].Value = drPurchaseRow.Cells["ItemName"].Value.ToString();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LBatchID"].Value = drPurchaseRow.Cells["BatchID"].Value.ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LBatchNo"].Value = drPurchaseRow.Cells["BatchNumber"].Value.ToString();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LQuantity"].Value = drPurchaseRow.Cells["Quantity"].Value.ToDecimal() + drPurchaseRow.Cells["ExtraQty"].Value.ToDecimal();

                            DataTable dtItemUOMs = MobjClsBLLPurchase.GetItemUOMs(drPurchaseRow.Cells["ItemID"].Value.ToInt32());
                            //if (MintFromForm == 3 && cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.GRNFromInvoice)
                            //    dtItemUOMs = MobjClsBLLPurchase.GetItemUOMs(drPurchaseRow.Cells["ItemID"].Value.ToInt32(), 1);
                            LUOM.DataSource = null;
                            LUOM.ValueMember = "UOMID";
                            LUOM.DisplayMember = "ShortName";
                            LUOM.DataSource = dtItemUOMs;
                            dgvLocationDetails.Rows[intRowIndex].Cells["LUOMID"].Value = drPurchaseRow.Cells["UOM"].Tag.ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = drPurchaseRow.Cells["UOM"].Tag.ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Tag = drPurchaseRow.Cells["UOM"].Tag.ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].FormattedValue;

                            DataRow dr = MobjClsBLLPurchase.GetItemDefaultLocation(drPurchaseRow.Cells["ItemID"].Value.ToInt32(), cboWarehouse.SelectedValue.ToInt32());

                            FillLocationGridCombo(LLocation.Index, intRowIndex);
                            dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dr["LocationID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag = dr["LocationID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].FormattedValue;

                            FillLocationGridCombo(LRow.Index, intRowIndex);
                            dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dr["RowID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag = dr["RowID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].FormattedValue;

                            FillLocationGridCombo(LBlock.Index, intRowIndex);
                            dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dr["BlockID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag = dr["BlockID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].FormattedValue;

                            FillLocationGridCombo(LLot.Index, intRowIndex);
                            dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dr["LotID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag = dr["LotID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].FormattedValue;

                            //dgvLocationDetails.Rows.Add();
                        }
                    }
                }
                //        else
                //        {
                //            dtItemDetails.Rows.Clear();
                //            foreach (DataGridViewRow drPurchaseRow in dgvPurchase.Rows)
                //            {
                //                bool blnIsExists = false;
                //                decimal decLocationQty = 0;
                //                if (drPurchaseRow.Cells["ItemID"].Value != null && drPurchaseRow.Cells["BatchNumber"].Value != null)
                //                {
                //                    DataRow drItemDetailsRow = dtItemDetails.NewRow();
                //                    drItemDetailsRow["ItemID"] = drPurchaseRow.Cells["ItemID"].Value.ToInt32();
                //                    drItemDetailsRow["ItemCode"] = drPurchaseRow.Cells["ItemCode"].Value.ToString();
                //                    drItemDetailsRow["ItemName"] = drPurchaseRow.Cells["ItemName"].Value.ToString();
                //                    drItemDetailsRow["BatchNo"] = drPurchaseRow.Cells["BatchNumber"].Value.ToString();
                //                    drItemDetailsRow["UOMID"] = drPurchaseRow.Cells["Uom"].Tag.ToInt32();
                //                    dtItemDetails.Rows.Add(drItemDetailsRow);

                //                    foreach (DataGridViewRow drLocationRow in dgvLocationDetails.Rows)
                //                    {
                //                        if (drLocationRow.Cells["LItemID"].Value != null && drLocationRow.Cells["LBatchNo"].Value != null)
                //                        {
                //                            if (drPurchaseRow.Cells["ItemID"].Value.ToInt32() == drLocationRow.Cells["LItemID"].Value.ToInt32() && drPurchaseRow.Cells["BatchNumber"].Value.ToString() == drLocationRow.Cells["LBatchNo"].Value.ToString())
                //                            {
                //                                blnIsExists = true;
                //                                decLocationQty += drLocationRow.Cells["LQuantity"].Value.ToDecimal();
                //                            }
                //                        }
                //                    }
                //                    if (!blnIsExists)
                //                    {
                //                        //  dgvLocationDetails.Rows.Add();
                //                        dgvLocationDetails.RowCount = dgvLocationDetails.RowCount + 1;
                //                        int intRowIndex = dgvLocationDetails.RowCount - 2;
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value = drPurchaseRow.Cells["ItemID"].Value.ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LItemCode"].Value = drPurchaseRow.Cells["ItemCode"].Value.ToString();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LItemName"].Value = drPurchaseRow.Cells["ItemName"].Value.ToString();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LBatchID"].Value = drPurchaseRow.Cells["BatchID"].Value.ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LBatchNo"].Value = drPurchaseRow.Cells["BatchNumber"].Value.ToString();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LQuantity"].Value = drPurchaseRow.Cells["Quantity"].Value.ToDecimal() + drPurchaseRow.Cells["ExtraQty"].Value.ToDecimal();

                //                        DataTable dtItemUOMs = MobjClsBLLPurchase.GetItemUOMs(drPurchaseRow.Cells["ItemID"].Value.ToInt32());
                //                        //if (MintFromForm == 3 && cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.GRNFromInvoice)
                //                        //    dtItemUOMs = MobjClsBLLPurchase.GetItemUOMs(drPurchaseRow.Cells["ItemID"].Value.ToInt32());
                //                        LUOM.DataSource = null;
                //                        LUOM.ValueMember = "UOMID";
                //                        LUOM.DisplayMember = "ShortName";
                //                        LUOM.DataSource = dtItemUOMs;
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LUOMID"].Value = drPurchaseRow.Cells["UOM"].Tag.ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = drPurchaseRow.Cells["UOM"].Tag.ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Tag = drPurchaseRow.Cells["UOM"].Tag.ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].FormattedValue;

                //                        DataRow dr = MobjClsBLLPurchase.GetItemDefaultLocation(drPurchaseRow.Cells["ItemID"].Value.ToInt32(), cboWarehouse.SelectedValue.ToInt32());

                //                        FillLocationGridCombo(LLocation.Index, intRowIndex);
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dr["LocationID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag = dr["LocationID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].FormattedValue;

                //                        FillLocationGridCombo(LRow.Index, intRowIndex);
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dr["RowID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag = dr["RowID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].FormattedValue;

                //                        FillLocationGridCombo(LBlock.Index, intRowIndex);
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dr["BlockID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag = dr["BlockID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].FormattedValue;

                //                        FillLocationGridCombo(LLocation.Index, intRowIndex);
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dr["LotID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag = dr["LotID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].FormattedValue;

                //                        //dgvLocationDetails.Rows.Add();
                //                    }
                //                    else
                //                    {
                //                        foreach (DataGridViewRow drLocationRow in dgvLocationDetails.Rows)
                //                        {
                //                            if (drLocationRow.Cells["LItemID"].Value != null && drLocationRow.Cells["LBatchNo"].Value != null)
                //                            {
                //                                if (drPurchaseRow.Cells["ItemID"].Value.ToInt32() == drLocationRow.Cells["LItemID"].Value.ToInt32() && drPurchaseRow.Cells["BatchNumber"].Value.ToString() == drLocationRow.Cells["LBatchNo"].Value.ToString())
                //                                {
                //                                    if ((drPurchaseRow.Cells["Quantity"].Value.ToDecimal() + drPurchaseRow.Cells["ExtraQty"].Value.ToDecimal()) - (decLocationQty - drLocationRow.Cells["LQuantity"].Value.ToDecimal()) <= 0)
                //                                        drLocationRow.Cells["LQuantity"].Value = 0;
                //                                    else
                //                                        drLocationRow.Cells["LQuantity"].Value = (drPurchaseRow.Cells["Quantity"].Value.ToDecimal() + drPurchaseRow.Cells["ExtraQty"].Value.ToDecimal()) - (decLocationQty - drLocationRow.Cells["LQuantity"].Value.ToDecimal());
                //                                    break;
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //            }


                //        }
                //    }
                //}
            }
            else
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 922, out MmessageIcon);
                ErrPurchase.SetError(cboWarehouse, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblPurchasestatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrPurchase.Enabled = true;
                tcPurchase.SelectedTab = tpItemDetails;
                cboWarehouse.Focus();
                return false;
            }
            return true;
        }
        private bool FillLocationGridCombo(int inColumnIndex, int intRowIndex)
        {
            try
            {
                DataTable dtLocation = new DataTable();
                int intTag = 0;
                if (inColumnIndex == LLocation.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32();
                    dtLocation = MobjClsBLLPurchase.FillCombos(new string[] { "Distinct LocationID,Location", "InvWarehouseDetails", "WarehouseID=" + cboWarehouse.SelectedValue.ToString() });

                    LLocation.DataSource = null;
                    LLocation.ValueMember = "LocationID";
                    LLocation.DisplayMember = "Location";
                    LLocation.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].FormattedValue;

                }
                else if (inColumnIndex == LRow.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag.ToInt32();
                    dtLocation = MobjClsBLLPurchase.FillCombos(new string[] { "Distinct  RowID, RowNumber", "InvWarehouseDetails", "WarehouseID=" + cboWarehouse.SelectedValue.ToString() + " And LocationID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32() });
                    LRow.DataSource = null;
                    LRow.ValueMember = "RowID";
                    LRow.DisplayMember = "RowNumber";
                    LRow.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].FormattedValue;
                }
                else if (inColumnIndex == LBlock.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag.ToInt32();
                    dtLocation = MobjClsBLLPurchase.FillCombos(new string[] { "Distinct BlockID,BlockNumber", "InvWarehouseDetails", " WarehouseID=" + cboWarehouse.SelectedValue.ToString() + " And LocationID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32() + " And RowID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag.ToInt32() });
                    LBlock.DataSource = null;
                    LBlock.ValueMember = "BlockID";
                    LBlock.DisplayMember = "BlockNumber";
                    LBlock.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].FormattedValue;
                }
                else if (inColumnIndex == LLot.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Tag.ToInt32();

                    dtLocation = MobjClsBLLPurchase.FillCombos(new string[] { " Distinct LotID, LotNumber", "InvWarehouseDetails", "WarehouseID=" + cboWarehouse.SelectedValue.ToString() + " And LocationID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32() + " And RowID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag.ToInt32() + " And BlockID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag.ToInt32() });
                    LLot.DataSource = null;
                    LLot.ValueMember = "LotID";
                    LLot.DisplayMember = "LotNumber";
                    LLot.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].FormattedValue;
                }

                return (dtLocation.Rows.Count > 0);

            }
            catch (Exception Ex)
            {
                MObjLogs.WriteLog("Error on FillLocationGridCombo(): " + this.Name + " " + Ex.Message.ToString(), 2);
                return false;

            }
        }

        private void dgvLocationDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                dgvLocationDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                int intTag = 0;

                if (e.RowIndex >= 0 && e.ColumnIndex == LLocation.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LLocation"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LLocation"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LLocation"].Tag = dgvLocationDetails.CurrentRow.Cells["LLocation"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LLocation"].Value = dgvLocationDetails.CurrentRow.Cells["LLocation"].FormattedValue;
                        LRow.DataSource = null;
                        LBlock.DataSource = null;
                        LLot.DataSource = null;
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Value = "";
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = "";
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = "";
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LLocation"].Value = dgvLocationDetails.CurrentRow.Cells["LLocation"].FormattedValue;
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex == LRow.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LRow"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LRow"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Tag = dgvLocationDetails.CurrentRow.Cells["LRow"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Value = dgvLocationDetails.CurrentRow.Cells["LRow"].FormattedValue;

                        LBlock.DataSource = null;
                        LLot.DataSource = null;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = "";
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = "";
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Value = dgvLocationDetails.CurrentRow.Cells["LRow"].FormattedValue;
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex == LBlock.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LBlock"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag = dgvLocationDetails.CurrentRow.Cells["LBlock"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = dgvLocationDetails.CurrentRow.Cells["LBlock"].FormattedValue;
                        LLot.DataSource = null;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = "";
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = dgvLocationDetails.CurrentRow.Cells["LBlock"].FormattedValue;
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex == LLot.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LLot"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LLot"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = dgvLocationDetails.CurrentRow.Cells["LLot"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = dgvLocationDetails.CurrentRow.Cells["LLot"].FormattedValue;
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = dgvLocationDetails.CurrentRow.Cells["LLot"].FormattedValue;
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        private void dgvLocationDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == LLocation.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
            else if (e.ColumnIndex == LBlock.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
            else if (e.ColumnIndex == LRow.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
            else if (e.ColumnIndex == LLot.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
        }

        private void tcPurchase_SelectedTabChanged(object sender, TabStripTabChangedEventArgs e)
        {
            if (tcPurchase.SelectedTab == tpLocationDetails)
            {
                FillItemLocationDetails();
            }
        }

        private void dgvLocationDetails_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == LItemCode.Index) || (e.ColumnIndex == LItemName.Index))
            {
                dgvLocationDetails.PiColumnIndex = e.ColumnIndex;
            }

        }

        private void dgvLocationDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvLocationDetails.IsCurrentCellDirty)
            {
                if (dgvLocationDetails.CurrentCell != null)
                    dgvLocationDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
        }

        private void dgvLocationDetails_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //if (dgvLocationDetails.CurrentCell.ColumnIndex == LItemCode.Index || dgvLocationDetails.CurrentCell.ColumnIndex == LItemName.Index)
                //{
                //    for (int i = 0; i < dgvLocationDetails.Columns.Count; i++)
                //    {
                //            dgvLocationDetails.Rows[dgvLocationDetails.CurrentCell.RowIndex].Cells[i].Value = null;
                //            dgvLocationDetails.Rows[dgvLocationDetails.CurrentCell.RowIndex].Cells[i].Tag = null;
                //    }
                //}
                dgvLocationDetails.PServerName = ClsCommonSettings.ServerName;
                string[] First = { "LItemCode", "LItemName", "LItemID", "LBatchNo", "LUOMID" };//first grid 
                string[] second = { "ItemCode", "ItemName", "ItemID", "BatchNo", "UOMID" };//inner grid
                dgvLocationDetails.aryFirstGridParam = First;
                dgvLocationDetails.arySecondGridParam = second;
                dgvLocationDetails.PiFocusIndex = LQuantity.Index;
                dgvLocationDetails.bBothScrollBar = true;
                dgvLocationDetails.iGridWidth = 350;
                dgvLocationDetails.pnlLeft = expandableSplitterLeft.Location.X + 5;
                dgvLocationDetails.pnlTop = expandableSplitterTop.Location.Y + PurchaseOrderBindingNavigator.Height + 10;
                dgvLocationDetails.ColumnsToHide = new string[] { "ItemID", "UOMID" };
                if (dgvLocationDetails.CurrentCell.ColumnIndex == LItemCode.Index)
                {
                    dgvLocationDetails.field = "ItemCode";
                    dgvLocationDetails.CurrentCell.Value = dgvLocationDetails.TextBoxText;
                }
                if (dgvLocationDetails.CurrentCell.ColumnIndex == LItemName.Index)
                {
                    // dgvPurchase.PiColumnIndex = 1;
                    dgvLocationDetails.field = "ItemName";
                    dgvLocationDetails.CurrentCell.Value = dgvLocationDetails.TextBoxText;
                }
                string strFilterString = dgvLocationDetails.field + " Like '" + dgvLocationDetails.TextBoxText + "%'";
                dtItemDetails.DefaultView.RowFilter = strFilterString;
                dgvLocationDetails.dtDataSource = dtItemDetails.DefaultView.ToTable();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvItemGrid_TextBoxChangedEvent() " + ex.Message);
                MObjLogs.WriteLog("Error in dgvItemGrid_TextBoxChangedEvent() " + ex.Message, 2);
            }
        }

        private void dgvLocationDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvLocationDetails.EditingControl.KeyPress += new KeyPressEventHandler(dgvLocationEditingControl_KeyPress);
        }

        void dgvLocationEditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvLocationDetails.CurrentCell.OwningColumn.Index == LQuantity.Index)
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
                int intUomScale = 0;
                if (dgvLocationDetails[Uom.Index, dgvLocationDetails.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    intUomScale = MobjClsBLLPurchase.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvLocationDetails[Uom.Index, dgvLocationDetails.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    if (intUomScale == 0)
                    {
                        if (e.KeyChar == '.')
                            e.Handled = true;
                    }
                    else
                    {
                        int dotIndex = -1;
                        if (txt.Text.Contains("."))
                        {
                            dotIndex = txt.Text.IndexOf('.');
                        }
                        if (e.KeyChar == '.')
                        {
                            if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            {
                                e.Handled = true;
                            }
                        }
                        else
                        {
                            if (char.IsDigit(e.KeyChar))
                            {
                                if (dotIndex != -1 && txt.SelectionStart > dotIndex)
                                {
                                    string[] splitText = txt.Text.Split('.');
                                    if (splitText.Length == 2)
                                    {
                                        if (splitText[1].Length - txt.SelectedText.Length >= intUomScale)
                                            e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private bool ValidateLocationGrid()
        {
            bool blnValid = true;
            int intColumnIndex = 0;
            foreach (DataGridViewRow drItemRow in dgvPurchase.Rows)
            {
                decimal decLocationQty = 0;
                foreach (DataGridViewRow drLocationRow in dgvLocationDetails.Rows)
                {
                    if (drLocationRow.Cells["LItemID"].Value.ToInt32() != 0)
                    {
                        if (drLocationRow.Cells["LLocation"].Tag.ToInt32() == 0)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 924, out MmessageIcon).Replace("#", "");
                            intColumnIndex = LLocation.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LRow"].Tag.ToInt32() == 0)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 931, out MmessageIcon).Replace("#", "");
                            intColumnIndex = LRow.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LBlock"].Tag.ToInt32() == 0)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 932, out MmessageIcon).Replace("#", "");
                            intColumnIndex = LBlock.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LLot"].Tag.ToInt32() == 0)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 930, out MmessageIcon).Replace("#", "");
                            intColumnIndex = LLot.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LQuantity"].Value.ToDecimal() <= 0)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 928, out MmessageIcon).Replace("#", "");
                            intColumnIndex = LQuantity.Index;
                            blnValid = false;
                        }
                        if (!blnValid)
                        {
                            MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            TmrPurchase.Enabled = true;
                            lblPurchasestatus.Text = MstrMessageCommon;
                            tcPurchase.SelectedTab = tpLocationDetails;
                            dgvLocationDetails.Focus();
                            dgvLocationDetails.CurrentCell = dgvLocationDetails[intColumnIndex, drLocationRow.Index];
                            break;
                        }
                    }
                    if (blnValid && drLocationRow.Cells["LItemID"].Value.ToInt32() != 0 && drLocationRow.Cells["LBatchNo"].Value != null)
                    {
                        if (drLocationRow.Cells["LItemID"].Value.ToInt32() == drItemRow.Cells["ItemID"].Value.ToInt32() && drLocationRow.Cells["LBatchNo"].Value.ToString() == drItemRow.Cells["BatchNumber"].Value.ToString())
                            decLocationQty += drLocationRow.Cells["LQuantity"].Value.ToDecimal();
                    }
                }
                if (!blnValid) break;
                if (blnValid && drItemRow.Cells["Quantity"].Value.ToDecimal() + drItemRow.Cells["ExtraQty"].Value.ToDecimal() != decLocationQty)
                {
                    blnValid = false;
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 933, out MmessageIcon).Replace("#", "");
                    MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    TmrPurchase.Enabled = true;
                    lblPurchasestatus.Text = MstrMessageCommon;
                    tcPurchase.SelectedTab = tpLocationDetails;
                    dgvLocationDetails.Focus();
                    dgvLocationDetails.CurrentCell = dgvLocationDetails[intColumnIndex, drItemRow.Index];
                    break;
                }
            }
            return blnValid;
        }

        private void dgvLocationDetails_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            bool blnValid = false;
            if (dgvLocationDetails.Rows[e.Row.Index].Cells["LItemID"].Value.ToInt32() != 0 && dgvLocationDetails.Rows[e.Row.Index].Cells["LBatchNo"].Value != null)
            {
                foreach (DataGridViewRow dr in dgvLocationDetails.Rows)
                {

                    if (dr.Cells["LItemID"].Value.ToInt32() != 0 && dr.Cells["LBatchNo"].Value != null)
                    {
                        if (dr.Index != e.Row.Index && dr.Cells["LItemID"].Value.ToInt32() == dgvLocationDetails.Rows[e.Row.Index].Cells["LItemID"].Value.ToInt32() && dr.Cells["LBatchNo"].Value.ToString() == dgvLocationDetails.Rows[e.Row.Index].Cells["LBatchNo"].Value.ToString())
                        {
                            blnValid = true;
                            break;
                        }
                    }
                }
            }
            else
            {
                blnValid = true;
            }
            if (!blnValid)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 929, out MmessageIcon).Replace("#", "");
                MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                e.Cancel = true;
            }
        }

        private void FillLocationDetailsParameters()
        {

            MobjClsBLLPurchase.PobjClsDTOPurchase.lstClsDTOPurchaseLocationDetailsCollection = new List<clsDTOPurchaseLocationDetails>();
            foreach (DataGridViewRow dr in dgvLocationDetails.Rows)
            {
                if (dr.Cells["LItemID"].Value != null && dr.Cells["LBatchNo"].Value != null)
                {
                    clsDTOPurchaseLocationDetails objLocationDetails = new clsDTOPurchaseLocationDetails();
                    objLocationDetails.intItemID = dr.Cells["LItemID"].Value.ToInt32();
                    objLocationDetails.lngBatchID = dr.Cells["LBatchID"].Value.ToInt64();
                    objLocationDetails.strBatchNo = dr.Cells["LBatchNo"].Value.ToString();
                    objLocationDetails.decQuantity = dr.Cells["LQuantity"].Value.ToDecimal();
                    objLocationDetails.intUOMID = dr.Cells["LUOM"].Tag.ToInt32();
                    objLocationDetails.intLocationID = dr.Cells["LLocation"].Tag.ToInt32();
                    objLocationDetails.intRowID = dr.Cells["LRow"].Tag.ToInt32();
                    objLocationDetails.intBlockID = dr.Cells["LBlock"].Tag.ToInt32();
                    objLocationDetails.intLotID = dr.Cells["LLot"].Tag.ToInt32();
                    MobjClsBLLPurchase.PobjClsDTOPurchase.lstClsDTOPurchaseLocationDetailsCollection.Add(objLocationDetails);
                }
            }
        }

        private void DisplayLocationDetails()
        {
            DataTable datLocationDetails = new DataTable();
            dgvLocationDetails.Rows.Clear();

            if (MintFromForm == 3)
                datLocationDetails = MobjClsBLLPurchase.GetPurchaseLocationDetails(true);
            else
                datLocationDetails = MobjClsBLLPurchase.GetPurchaseLocationDetails(false);

            for (int intRowIndex = 0; intRowIndex < datLocationDetails.Rows.Count; intRowIndex++)
            {
                dgvLocationDetails.RowCount = dgvLocationDetails.RowCount + 1;
                dgvLocationDetails["LItemID", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["ItemID"];
                dgvLocationDetails["LItemCode", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["ItemCode"];
                dgvLocationDetails["LItemName", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["ItemName"];
                dgvLocationDetails["LBatchNo", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["BatchNo"];
                dgvLocationDetails["LQuantity", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["Quantity"];

                DataTable dtItemUOMs = MobjClsBLLPurchase.GetItemUOMs(datLocationDetails.Rows[intRowIndex]["ItemID"].ToInt32());
                //if (MintFromForm == 3 && cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.GRNFromInvoice)
                //    dtItemUOMs = MobjClsBLLPurchase.GetItemUOMs(datLocationDetails.Rows[intRowIndex]["ItemID"].ToInt32(), 1);
                LUOM.DataSource = null;
                LUOM.ValueMember = "UOMID";
                LUOM.DisplayMember = "ShortName";
                LUOM.DataSource = dtItemUOMs;
                dgvLocationDetails.Rows[intRowIndex].Cells["LUOMID"].Value = datLocationDetails.Rows[intRowIndex]["UOMID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = datLocationDetails.Rows[intRowIndex]["UOMID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Tag = datLocationDetails.Rows[intRowIndex]["UOMID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].FormattedValue;

                FillLocationGridCombo(LLocation.Index, intRowIndex);
                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = datLocationDetails.Rows[intRowIndex]["LocationID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag = datLocationDetails.Rows[intRowIndex]["LocationID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].FormattedValue;

                FillLocationGridCombo(LRow.Index, intRowIndex);
                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = datLocationDetails.Rows[intRowIndex]["RowID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag = datLocationDetails.Rows[intRowIndex]["RowID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].FormattedValue;

                FillLocationGridCombo(LBlock.Index, intRowIndex);
                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = datLocationDetails.Rows[intRowIndex]["BlockID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag = datLocationDetails.Rows[intRowIndex]["BlockID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].FormattedValue;

                FillLocationGridCombo(LLot.Index, intRowIndex);
                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = datLocationDetails.Rows[intRowIndex]["LotID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag = datLocationDetails.Rows[intRowIndex]["LotID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].FormattedValue;
            }
        }

        private bool ValidateLocationQuantity(bool blnIsDeletion)
        {
            DataTable datLocationDetails = new DataTable();
            bool blnIsDemoQty = false;


            bool blnValid = true;
            if (MintFromForm == 3)
            {
                //if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GTFromTransfer)
                //{
                //    int intTransferTypeID = MobjClsBLLPurchase.FillCombos(new string[] { "OrderTypeID", "InvStockTransferMaster", "StockTransferID = " + cboOrderNo.SelectedValue.ToInt64() }).Rows[0]["OrderTypeID"].ToInt32();
                //    if (intTransferTypeID == (int)OperationOrderType.STWareHouseToWareHouseTransferDemo)
                //        blnIsDemoQty = true;
                //}
                datLocationDetails = MobjClsBLLPurchase.GetPurchaseLocationDetails(true);
            }
            else
                datLocationDetails = MobjClsBLLPurchase.GetPurchaseLocationDetails(false);

            foreach (DataRow dr in datLocationDetails.Rows)
            {

                clsDTOPurchaseLocationDetails objLocationDetails = new clsDTOPurchaseLocationDetails();
                objLocationDetails.intItemID = dr["ItemID"].ToInt32();
                objLocationDetails.lngBatchID = dr["BatchID"].ToInt64();
                objLocationDetails.intLocationID = dr["LocationID"].ToInt32();
                objLocationDetails.intRowID = dr["RowID"].ToInt32();
                objLocationDetails.intBlockID = dr["BlockID"].ToInt32();
                objLocationDetails.intLotID = dr["LotID"].ToInt32();

                decimal decAvailableQty = MobjClsBLLPurchase.GetItemLocationQuantity(objLocationDetails, blnIsDemoQty);
                DataTable dtGetUomDetails = MobjClsBLLPurchase.GetUomConversionValues(dr["UOMID"].ToInt32(), dr["ItemID"].ToInt32());
                //if (MintFromForm == 3 && cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.GRNFromInvoice)
                //    dtGetUomDetails = MobjClsBLLPurchase.GetUomConversionValues(dr["UOMID"].ToInt32(), dr["ItemID"].ToInt32(), 1);
                decimal decOldQuantity = dr["Quantity"].ToDecimal();
                if (dtGetUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                        decOldQuantity = decOldQuantity / decConversionValue;
                    else if (intConversionFactor == 2)
                        decOldQuantity = decOldQuantity * decConversionValue;
                }

                if (blnIsDeletion)
                {
                    if (decAvailableQty - decOldQuantity < 0)
                    {
                        blnValid = false;
                    }
                }
                else
                {
                    decimal decCurrentQuantity = 0;
                    foreach (DataGridViewRow drLocationRow in dgvLocationDetails.Rows)
                    {
                        if (drLocationRow.Cells["LItemID"].Value != null && drLocationRow.Cells["LBatchNo"].Value != null)
                        {
                            if (drLocationRow.Cells["LItemID"].Value.ToInt32() == dr["ItemID"].ToInt32() && drLocationRow.Cells["LBatchNo"].Value.ToString() == dr["BatchNo"].ToString())
                            {
                                dtGetUomDetails = MobjClsBLLPurchase.GetUomConversionValues(drLocationRow.Cells["LUOMID"].Tag.ToInt32(), dr["ItemID"].ToInt32());
                                //if (MintFromForm == 3 && cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.GRNFromInvoice)
                                //    dtGetUomDetails = MobjClsBLLPurchase.GetUomConversionValues(drLocationRow.Cells["LUOMID"].Tag.ToInt32(), dr["ItemID"].ToInt32(), 1);
                                decCurrentQuantity = drLocationRow.Cells["LQuantity"].Value.ToDecimal();
                                if (dtGetUomDetails.Rows.Count > 0)
                                {
                                    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                                    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                                    if (intConversionFactor == 1)
                                        decCurrentQuantity = decCurrentQuantity / decConversionValue;
                                    else if (intConversionFactor == 2)
                                        decCurrentQuantity = decCurrentQuantity * decConversionValue;

                                    break;
                                }

                            }
                        }
                    }
                    if ((decAvailableQty - decOldQuantity) < 0)
                    {
                        blnValid = false;
                    }
                }
                if (!blnValid)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 941, out MmessageIcon).Replace("#", "").Replace("@", dr["ItemName"].ToString());
                    MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    tcPurchase.SelectedTab = tpLocationDetails;
                    dgvLocationDetails.Focus();
                    break;
                }
            }
            return blnValid;
        }



        private void dgvLocationDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (e.ColumnIndex == LUOMID.Index)
                {
                    DataTable dtItemUOMs = MobjClsBLLPurchase.GetItemUOMs(dgvLocationDetails["LItemID", e.RowIndex].Value.ToInt32());
                    //if(MintFromForm == 3 && cboOrderType.SelectedValue.ToInt32() != (int)OperationOrderType.GRNFromInvoice)
                    //    dtItemUOMs = MobjClsBLLPurchase.GetItemUOMs(dgvLocationDetails["LItemID", e.RowIndex].Value.ToInt32(), 1);
                    LUOM.DataSource = null;
                    LUOM.ValueMember = "UOMID";
                    LUOM.DisplayMember = "ShortName";
                    LUOM.DataSource = dtItemUOMs;
                    dgvLocationDetails.Rows[e.RowIndex].Cells["LUOM"].Value = dgvLocationDetails["LUOMID", e.RowIndex].Value.ToInt32();
                    dgvLocationDetails.Rows[e.RowIndex].Cells["LUOM"].Tag = dgvLocationDetails["LUOMID", e.RowIndex].Value.ToInt32();
                    dgvLocationDetails.Rows[e.RowIndex].Cells["LUOM"].Value = dgvLocationDetails.Rows[e.RowIndex].Cells["LUOM"].FormattedValue;
                }
            }
        }

        private bool CheckLocationDuplication()
        {
            for (int intICounter = 0; intICounter < dgvLocationDetails.Rows.Count; intICounter++)
            {
                for (int intJCounter = intICounter + 1; intJCounter < dgvLocationDetails.Rows.Count; intJCounter++)
                {
                    if (dgvLocationDetails["LItemID", intICounter].Value.ToInt32() == dgvLocationDetails["LItemID", intJCounter].Value.ToInt32() &&
                          (dgvLocationDetails["LBatchNo", intICounter].Value != null && dgvLocationDetails["LBatchNo", intJCounter].Value != null && dgvLocationDetails["LBatchNo", intICounter].Value.ToString() == dgvLocationDetails["LBatchNo", intJCounter].Value.ToString()) &&
                          dgvLocationDetails["LLocation", intICounter].Tag.ToInt32() == dgvLocationDetails["LLocation", intJCounter].Tag.ToInt32() &&
                          dgvLocationDetails["LRow", intICounter].Tag.ToInt32() == dgvLocationDetails["LRow", intJCounter].Tag.ToInt32() &&
                          dgvLocationDetails["LBlock", intICounter].Tag.ToInt32() == dgvLocationDetails["LBlock", intJCounter].Tag.ToInt32() &&
                          dgvLocationDetails["LLot", intICounter].Tag.ToInt32() == dgvLocationDetails["LLot", intJCounter].Tag.ToInt32())
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 911, out MmessageIcon).Replace("*", dgvLocationDetails["LItemName", intICounter].Value.ToString()).Replace("@", dgvLocationDetails["LBatchNo", intICounter].Value.ToString()).Replace("#", "");
                        MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        tcPurchase.SelectedTab = tpLocationDetails;
                        dgvLocationDetails.Focus();
                        dgvLocationDetails.CurrentCell = dgvLocationDetails["LLocation", intJCounter];
                        return false;
                    }
                }
            }
            return true;
        }

        private void cboWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvLocationDetails.Rows.Clear();
        }

        private void SetPurchaseGridWidth()
        {
            switch (MintFromForm)
            {
                case 1:
                case 2:
                    ItemCode.Width = 85;
                    ItemName.Width = 300;
                    Quantity.Width = 100;
                    Uom.Width = 80;
                    Rate.Width = 100;
                    GrandAmount.Width = 100;
                    Discount.Width = 150;
                    DiscountAmount.Width = 100;
                    NetAmount.Width = 100;
                    NetAmount.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    break;
                case 3:

                    ItemCode.Width = 85;

                    if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromInvoice || cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromOrder)
                    {
                        ItemName.Width = 290;
                        OrderedQty.Width = 100;
                        ReceivedQty.Width = 100;
                        ExtraQty.Width = 100;
                        ExpiryDate.Width = 100;
                        Batchnumber.Width = 200;
                        // Rate.Width = 90;
                        // PurchaseRate.Width = 90;
                        ItemName.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    }
                    else if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromTransfer)
                    {
                        ItemName.Width = 400;
                        Batchnumber.Width = 150;
                        Rate.Width = 100;
                        OrderedQty.Width = 100;
                        ReceivedQty.Width = 100;
                        ItemName.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    }
                    else
                    {
                        ItemCode.Width = 100;
                        ItemName.Width = 330;
                        UsedQty.Width = 120;
                        //Batchnumber.Width = 150;
                        //Rate.Width = 100;
                        OrderedQty.Width = 120;
                        ReceivedQty.Width = 120;
                        Batchnumber.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    }


                    Quantity.Width = 120;
                    Uom.Width = 100;

                    break;
                case 4:
                    if (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.PInvoiceDirect)
                    {
                        if (ChkTGNR.Checked)
                        {
                            ItemCode.Width = 85;
                            ItemName.Width = 310;
                            Quantity.Width = 80;
                            Uom.Width = 70;
                            Rate.Width = 80;
                            PurchaseRate.Width = 80;
                            GrandAmount.Width = 90;
                            Discount.Width = 140;
                            DiscountAmount.Width = 80;
                            NetAmount.Width = 90;
                            NetAmount.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        }
                        else
                        {
                            ItemCode.Width = 65;
                            ItemName.Width = 150;
                            Quantity.Width = 70;
                            ExtraQty.Width = 65;
                            Uom.Width = 60;
                            Batchnumber.Width = 115;
                            ExpiryDate.Width = 80;
                            Rate.Width = 80;
                            PurchaseRate.Width = 80;
                            GrandAmount.Width = 90;
                            Discount.Width = 100;
                            DiscountAmount.Width = 80;
                            NetAmount.Width = 80;
                            NetAmount.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        }
                    }
                    else
                    {
                        if (ChkTGNR.Checked)
                        {
                            ItemCode.Width = 70;
                            ItemName.Width = 210;
                            Quantity.Width = 80;
                            OrderedQty.Width = 80;
                            ReceivedQty.Width = 80;
                            Uom.Width = 65;
                            Rate.Width = 90;
                            PurchaseRate.Width = 80;
                            GrandAmount.Width = 90;
                            Discount.Width = 110;
                            DiscountAmount.Width = 80;
                            NetAmount.Width = 80;
                            NetAmount.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        }
                        else
                        {
                            ItemCode.Width = 70;
                            ItemName.Width = 160;
                            Quantity.Width = 80;
                            OrderedQty.Width = 70;
                            ReceivedQty.Width = 70;
                            ExtraQty.Width = 65;
                            Uom.Width = 60;
                            Batchnumber.Width = 130;
                            ExpiryDate.Width = 80;
                            Rate.Width = 80;
                            PurchaseRate.Width = 80;
                            GrandAmount.Width = 90;
                            Discount.Width = 110;
                            DiscountAmount.Width = 70;
                            NetAmount.Width = 80;
                            if (expandableSplitterLeft.Expanded)
                                NetAmount.AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
                            else
                                NetAmount.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        }
                    }
                    break;
            }

        }

        private void dgvPurchase_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void expandableSplitterLeft_ExpandedChanged(object sender, ExpandedChangeEventArgs e)
        {
            if (MintFromForm == 4 && cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.PInvoiceFromOrder && !ChkTGNR.Checked)
            {
                if (expandableSplitterLeft.Expanded)
                    NetAmount.AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
                else
                    NetAmount.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            if (expandableSplitterLeft.Expanded)
            {
                if (dgvPurchaseDisplay.Columns.Count > 0)
                    dgvPurchaseDisplay.Columns[0].Visible = false;
            }
        }

        private void panelGridBottom_Click(object sender, EventArgs e)
        {

        }

        private void ShowSuggestions()
        {
            DataTable datVerification = new DataTable();
            if (PintApprovePermission == (int)ApprovePermission.Approve || PintApprovePermission == (int)ApprovePermission.Both)
                datVerification = GetVerificationHistoryStatus(true);
            else
                datVerification = GetVerificationHistoryStatus(false);
            dgvSuggestions.Rows.Clear();
            for (int i = 0; i < datVerification.Rows.Count; i++)
            {
                dgvSuggestions.Rows.Add();
                dgvSuggestions[UserName.Index, i].Value = datVerification.Rows[i]["UserName"];
                dgvSuggestions[Status.Index, i].Value = datVerification.Rows[i]["Status"];
                dgvSuggestions[VerifiedDate.Index, i].Value = datVerification.Rows[i]["VerifiedDate"];
                dgvSuggestions[Comment.Index, i].Value = datVerification.Rows[i]["Comment"];
            }

        }

        private void tcGeneral_SelectedTabChanged(object sender, TabStripTabChangedEventArgs e)
        {
            if (tcGeneral.SelectedTab == tiSuggestions)
            {
                DataTable datVerification = new DataTable();
                if (PintApprovePermission == (int)ApprovePermission.Approve || PintApprovePermission == (int)ApprovePermission.Both)
                    datVerification = GetVerificationHistoryStatus(true);
                else
                    datVerification = GetVerificationHistoryStatus(false);
                dgvSuggestions.Rows.Clear();
                for (int i = 0; i < datVerification.Rows.Count; i++)
                {
                    dgvSuggestions.Rows.Add();
                    dgvSuggestions[UserID.Index, i].Value = datVerification.Rows[i]["UserID"];
                    dgvSuggestions[UserName.Index, i].Value = datVerification.Rows[i]["UserName"];
                    dgvSuggestions[Status.Index, i].Value = datVerification.Rows[i]["Status"];
                    dgvSuggestions[VerifiedDate.Index, i].Value = datVerification.Rows[i]["VerifiedDate"];
                    dgvSuggestions[Comment.Index, i].Value = datVerification.Rows[i]["Comment"];
                    if (datVerification.Rows[i]["UserID"].ToInt32() != ClsCommonSettings.UserID)
                        dgvSuggestions[Comment.Index, i].ReadOnly = true;
                    //else
                    //{
                    //    if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.PQuotationApproved || MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID == (int)OperationStatusType.POrderApproved)
                    //        dgvSuggestions[Comment.Index, i].ReadOnly = true;
                    //}
                }
            }
        }

        private void dgvSuggestions_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == Comment.Index)
            {
                if (dgvSuggestions.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                    MobjClsBLLPurchase.UpdationVerificationTableRemarks(MintFromForm, MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID, dgvSuggestions.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
            }
        }

        private void txtTotalAmount_TextChanged(object sender, EventArgs e)
        {
            CalculateExchangeCurrencyRate();
            string strAmount = txtTotalAmount.Text;
            int intCurrency = Convert.ToInt32(cboCurrency.SelectedValue);
            lblAmountinWords.Text = MobjclsBLLCommonUtility.ConvertToWord(strAmount, intCurrency);
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();

            switch (MintFromForm)
            {
                case 1:
                    objHelp.strFormName = "PurchaseQuotation";
                    break;
                case 2:
                    objHelp.strFormName = "Purchaseorder";
                    break;
                case 3:
                    objHelp.strFormName = "GRN";
                    break;
                case 4:
                    objHelp.strFormName = "Purchaseinvoice";
                    break;
            }

            objHelp.ShowDialog();
            objHelp = null;
        }

        private void btnPickList_Click(object sender, EventArgs e)
        {
            try
            {
                if (MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PblnPickList = true;
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID;
                    switch (MintFromForm)
                    {
                        case 3:
                            ObjViewer.PiFormID = (int)FormID.PurchaseGRN;
                            break;
                        case 4:
                            ObjViewer.PiFormID = (int)FormID.PurchaseInvoice;
                            break;
                    }
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnPickList_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in btnPickList_Click() " + ex.Message, 2);
            }
        }

        private void btnPickListEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup objEmailPopUp = new FrmEmailPopup())
                {
                    switch (MintFromForm)
                    {
                        case 3:
                            objEmailPopUp.MsSubject = "GRN Location Details";
                            objEmailPopUp.EmailFormType = EmailFormID.PickList;
                            objEmailPopUp.EmailSource = MobjClsBLLPurchase.GetPurchaseReport(5);
                            break;
                        case 4:
                            objEmailPopUp.MsSubject = "Purchase Invoice Location Details";
                            objEmailPopUp.EmailFormType = EmailFormID.PickList;
                            objEmailPopUp.EmailSource = MobjClsBLLPurchase.GetPurchaseReport(6);
                            break;
                    }
                    objEmailPopUp.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnPickListEmail_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in btnPickListEmail_Click() " + ex.Message, 2);
            }
        }

        private void lnkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DataTable datAdvanceSearchedData = new DataTable();
            switch (MintFromForm)
            {

                case 1: // Purchase Quotation
                    using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.PurchaseQuotation))
                    {
                        objFrmSearchForm.ShowDialog();
                        if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                        {
                            datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                            datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                            dgvPurchaseDisplay.DataSource = datAdvanceSearchedData;
                            dgvPurchaseDisplay.Columns["ID"].Visible = false;
                            dgvPurchaseDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        }
                    }
                    break;
                case 2: // Purchase Order
                    using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.PurchaseOrder))
                    {
                        objFrmSearchForm.ShowDialog();
                        if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                        {
                            datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                            datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                            dgvPurchaseDisplay.DataSource = datAdvanceSearchedData;
                            dgvPurchaseDisplay.Columns["ID"].Visible = false;
                            dgvPurchaseDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        }
                    }
                    break;
                case 3: // GRN
                    if (PintOperationTypeID > 0)
                    {
                        using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.MaterialReturn))
                        {
                            objFrmSearchForm.ShowDialog();
                            if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                            {
                                datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                                datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                                dgvPurchaseDisplay.DataSource = datAdvanceSearchedData;
                                dgvPurchaseDisplay.Columns["ID"].Visible = false;
                                dgvPurchaseDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            }
                        }
                    }
                    else
                    {
                        using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.GRN))
                        {
                            objFrmSearchForm.ShowDialog();
                            if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                            {
                                datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                                datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                                dgvPurchaseDisplay.DataSource = datAdvanceSearchedData;
                                dgvPurchaseDisplay.Columns["ID"].Visible = false;
                                dgvPurchaseDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            }
                        }
                    }
                    break;
                case 4: // Purchase Invoice

                    using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.PurchaseInvoice))
                    {
                        objFrmSearchForm.ShowDialog();
                        if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                        {
                            datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                            datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                            dgvPurchaseDisplay.DataSource = datAdvanceSearchedData;
                            dgvPurchaseDisplay.Columns["ID"].Visible = false;
                            dgvPurchaseDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        }
                    }
                    break;
            }
        }

        private void txtDiscount_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
            CalculateNetTotal();
            for (int i = 0; i < dgvPurchase.Rows.Count; i++)
            {
                if (dgvPurchase.Rows[i].Cells["ItemID"].Value != null && dgvPurchase.Rows[i].Cells["Quantity"].Value.ToDecimal() > 0)
                {
                    dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = (dgvPurchase.Rows[i].Cells["NetAmount"].Value.ToDecimal() / dgvPurchase.Rows[i].Cells["Quantity"].Value.ToDecimal()).ToString("F" + MintExchangeCurrencyScale);
                    if (cboDiscount.SelectedValue.ToInt32() != 0 && cboDiscount.SelectedValue.ToInt32() != (int)PredefinedDiscounts.DefaultPurchaseDiscount)
                        dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = Convert.ToDecimal(MobjClsBLLPurchase.GetItemDiscountAmount(4, 0, cboDiscount.SelectedValue.ToInt32(), dgvPurchase.Rows[i].Cells["PurchaseRate"].Value.ToDouble())).ToString("F" + MintExchangeCurrencyScale);
                    else if (cboDiscount.SelectedValue.ToInt32() == (int)PredefinedDiscounts.DefaultPurchaseDiscount)
                    {
                        decimal decDiscountPercent = 0;
                        if (txtSubTotal.Text.ToDecimal() > 0)
                            decDiscountPercent = (txtDiscount.Text.ToDecimal() / txtSubTotal.Text.ToDecimal()) * 100;
                        decimal decItemNetAmount = dgvPurchase.Rows[i].Cells["NetAmount"].Value.ToDecimal() - (dgvPurchase.Rows[i].Cells["NetAmount"].Value.ToDecimal() * (decDiscountPercent / 100));
                        dgvPurchase.Rows[i].Cells["PurchaseRate"].Value = (decItemNetAmount / dgvPurchase.Rows[i].Cells["Quantity"].Value.ToDecimal()).ToString("F" + MintExchangeCurrencyScale);
                    }

                }
            }
        }

        private void dgvPurchase_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (MintFromForm == 3 && (cboOrderType.SelectedValue.ToInt32() == (int)OperationOrderType.GRNFromTransfer))
                MobjclsBLLCommonUtility.SetSerialNo(dgvPurchase, e.RowIndex, true);
            else
                MobjclsBLLCommonUtility.SetSerialNo(dgvPurchase, e.RowIndex, false);
        }


        private void btnActions_PopupOpen(object sender, PopupOpenEventArgs e)
        {
            int intTempOperationTypeID = 0;
            bool blnTempEditable = true;

            if (MintFromForm == 1)
            {
                intTempOperationTypeID = Convert.ToInt32(OperationType.PurchaseQuotation);
                if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PQuotationOpened && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PQuotationRejected && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PQuotationCancelled)
                    blnTempEditable = false;
            }
            else if (MintFromForm == 2)
            {
                intTempOperationTypeID = Convert.ToInt32(OperationType.PurchaseOrder);
                if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.POrderOpened && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.POrderRejected && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.POrderRejected)
                    blnTempEditable = false;
            }
            else if (MintFromForm == 4)
            {
                intTempOperationTypeID = (Int32)OperationType.PurchaseInvoice;
                if (MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PInvoiceOpened && MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID != (int)OperationStatusType.PInvoiceCancelled)
                    blnTempEditable = false;
            }

            bool blnShowDocumentForm = clsUtilities.IsDocumentExists(intTempOperationTypeID, Convert.ToInt64(txtPurchaseNo.Tag));
            if (blnShowDocumentForm == false)
            {
                if (blnTempEditable)
                    blnShowDocumentForm = true;
            }

            if (blnShowDocumentForm == false)
                btnDocuments.Enabled = false;
            else
                btnDocuments.Enabled = true;
        }

        private void ShowItemHistory_Click(object sender, EventArgs e)
        {
            using (FrmSupplierPurchaseHistory objFrmSupplierPurchaseHistory = new FrmSupplierPurchaseHistory())
            {
                objFrmSupplierPurchaseHistory.intCompanyID = this.cboCompany.SelectedValue.ToInt32();
                objFrmSupplierPurchaseHistory.intSupplierID = this.cboSupplier.SelectedValue.ToInt32();
                objFrmSupplierPurchaseHistory.intItemID = SItemID;
                objFrmSupplierPurchaseHistory.ShowDialog();
            }
        }

        private void dgvPurchase_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (MintFromForm == 1 || MintFromForm == 2 || MintFromForm == 4)
            {
                if (e.RowIndex >= 0)
                {
                    int intCurrentColumn = dgvPurchase.CurrentCell.ColumnIndex;

                    if (this.cboCompany.SelectedIndex >= 0 && cboSupplier.SelectedIndex >= 0)
                    {
                        if (intCurrentColumn == 1 || intCurrentColumn == 2)
                        {
                            if (e.Button == MouseButtons.Right)
                            {
                                if (dgvPurchase.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt32() > 0)
                                {
                                    SItemID = dgvPurchase.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt32();
                                    this.CntxtHistory.Show(this.dgvPurchase, this.dgvPurchase.PointToClient(Cursor.Position));
                                }
                            }
                        }
                    }
                }
            }
        }

        private void cboPaymentTerms_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (MintFromForm)
            {
                case 4:

                    if (cboPaymentTerms.SelectedValue.ToInt32() == (int)PaymentTerms.Credit)
                    {
                        cboPurchaseAccount.SelectedValue = MobjClsBLLPurchase.GetCompanyAccountID(Convert.ToInt32(TransactionTypes.CreditPurchase), cboCompany.SelectedValue.ToInt32());
                    }
                    else
                    {
                        cboPurchaseAccount.SelectedValue = MobjClsBLLPurchase.GetCompanyAccountID(Convert.ToInt32(TransactionTypes.CashPurchase), cboCompany.SelectedValue.ToInt32());
                    }
                    break;
            }
        }

        private void btnAccount_Click(object sender, EventArgs e)
        {
            using (FrmChartOfAccounts objChartOfAccounts = new FrmChartOfAccounts())
            {
                try
                {
                    // user will only be able to create account head or group under the account group that we set here
                    objChartOfAccounts.PermittedAccountGroup = AccountGroups.PurchaseAccounts;
                    if (this.cboPurchaseAccount.SelectedIndex != -1)
                    {
                        objChartOfAccounts.SelectedAccount = (Accounts)this.cboPurchaseAccount.SelectedValue.ToInt32();
                    }

                    objChartOfAccounts.MbCalledFromAccountSettings = true;
                }
                catch
                {
                }
                objChartOfAccounts.ShowDialog();
            }
            int intComboID = cboPurchaseAccount.SelectedValue.ToInt32();
            SettingPurchaseAccount();
            cboPurchaseAccount.SelectedValue = intComboID;

        }

        private void clbGRN_SelectedIndexChanged(object sender, EventArgs e)
        {
            strCondition = string.Empty;
            dgvPurchase.Rows.Clear();
            strCondition = strCondition + "GRNID IN(";
         
            foreach (DataRowView rowView in clbGRN.CheckedItems)
            {
                strCondition = strCondition + (rowView.Row["GRNID"]).ToString() + ",";
            }
            strCondition = strCondition.Remove(strCondition.Length - 1) + ")";
            if (clbGRN.CheckedItems.Count > 0)
            {
              
                        if (Convert.ToInt32(cboOrderType.SelectedValue) == (Int32)OperationOrderType.POrderFromGRN)
                        {
                            int intOldStatusID = MobjClsBLLPurchase.PobjClsDTOPurchase.intStatusID;

                            dgvPurchase.Rows.Clear();
                            DataTable DtPurOrderDetail = null;
                            DataTable dtGRNPurchaseRate = new DataTable();
                            decimal GRNPurchaseRate = 0;

                            DtPurOrderDetail = MobjClsBLLPurchase.GetDataPOrderFromGRN(strCondition);
                            if (dgvPurchase.Rows.Count > 0)
                            {

                                for (int i = 0; i < DtPurOrderDetail.Rows.Count; i++)
                                {
                                    dgvPurchase.RowCount = dgvPurchase.RowCount+1;
                                    dgvPurchase.Rows[i].Cells["ItemID"].Value = DtPurOrderDetail.Rows[i]["ItemID"];
                                    dgvPurchase.Rows[i].Cells["ItemCode"].Value = DtPurOrderDetail.Rows[i]["ItemCode"];
                                    dgvPurchase.Rows[i].Cells["ItemName"].Value = DtPurOrderDetail.Rows[i]["Description"];
                                    //   dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["Quantity"];
                                    int iItemID = Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]);

                                    FillComboColumn(Convert.ToInt32(DtPurOrderDetail.Rows[i]["ItemID"]), 1);
                                    dgvPurchase.Rows[i].Cells["Uom"].Value = DtPurOrderDetail.Rows[i]["BaseUomID"];
                                    dgvPurchase.Rows[i].Cells["Uom"].Tag = DtPurOrderDetail.Rows[i]["BaseUomID"];
                                    dgvPurchase.Rows[i].Cells["Uom"].Value = dgvPurchase.Rows[i].Cells["Uom"].FormattedValue;

                                    dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["Quantity"].ToDecimal();

                                    dtGRNPurchaseRate = MobjClsBLLPurchase.GetGRNPurchaseRate(strCondition, dgvPurchase.Rows[i].Cells["ItemID"].Value.ToInt32());
                                    if(dtGRNPurchaseRate != null && dtGRNPurchaseRate.Rows.Count > 0)
                                    GRNPurchaseRate = MobjClsBLLPurchase.CalculateGRNPurchaseRate(Convert.ToDecimal(dtGRNPurchaseRate.Rows[0]["PurchaseRate"]), Convert.ToInt32(dtGRNPurchaseRate.Rows[0]["UOMID"]), dgvPurchase.Rows[i].Cells["ItemID"].Value.ToInt32());


                                    if (GRNPurchaseRate > 0)
                                        dgvPurchase.Rows[i].Cells["Rate"].Value = GRNPurchaseRate;
                                    else
                                        dgvPurchase.Rows[i].Cells["Rate"].Value = 0;

                                    dgvPurchase.Rows[i].Cells["IsAutofilled"].Value = 1;
                                }
                            }
                            CalculateTotalAmt();

                            txtDiscount.Text = MobjClsBLLPurchase.PobjClsDTOPurchase.decGrandDiscountAmt.ToString("F" + MintExchangeCurrencyScale);

                            DtPurOrderDetail = null;

                            CalculateNetTotal();
                            //MobjClsBLLPurchase.PobjClsDTOPurchase.intPurchaseID = 0;
                            SetPurchaseStatus();
                            //SetControlsVisibility(false);
                            txtRemarks.ReadOnly = false;
                            ChkTGNR.Enabled = false;
                            dtpDueDate.Enabled = dtpDate.Enabled = true;
                            btnActions.Enabled = true;
                            dgvPurchase.Rows[dgvPurchase.Rows.Count - 1].ReadOnly = true;
                        }
                    }
  

        }

        private void cboGRNWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboOrderType.SelectedValue.ToInt32() == (Int32)OperationOrderType.POrderFromGRN)
                cboOrderType_SelectedIndexChanged(null, new EventArgs());
        }

        private void btnGRNWarehouse_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(cboGRNWarehouse.SelectedValue);
                using (FrmWarehouse objCreateWarehouse = new FrmWarehouse())
                {
                    objCreateWarehouse.PintWareHouse = Convert.ToInt32(cboGRNWarehouse.SelectedValue);
                    objCreateWarehouse.ShowDialog();

                    if (MblnAddStatus)
                    {
                        LoadCombos(5, null);
                    }
                    else
                    {
                        DataTable datCombos = MobjClsBLLPurchase.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", "InvWarehouse.CompanyID = " + MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID + " And Active = 1 UNION SELECT WarehouseID,WarehouseName FROM InvWarehouse WHERE WarehouseID =" + MobjClsBLLPurchase.PobjClsDTOPurchase.intWarehouseID });////fills all warehouse and saved warehouse if inactive
                        cboGRNWarehouse.ValueMember = "WarehouseID";
                        cboGRNWarehouse.DisplayMember = "WarehouseName";
                        cboGRNWarehouse.DataSource = datCombos;
                    }
                    if (objCreateWarehouse.PintWareHouse != 0 && cboGRNWarehouse.Enabled)
                        cboGRNWarehouse.SelectedValue = objCreateWarehouse.PintWareHouse;
                    else
                        cboGRNWarehouse.SelectedValue = MintComboID;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnWarehouse_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in btnWarehouse_Click() " + ex.Message, 2);
            }
        }

        private void txtDiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in txtDiscountAmount_KeyPress() " + ex.Message);
                MObjLogs.WriteLog("Error in txtDiscountAmount_KeyPress() " + ex.Message, 2);
            }
        }

        private void cboTaxScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalculateNetTotal();
        }

    }
}