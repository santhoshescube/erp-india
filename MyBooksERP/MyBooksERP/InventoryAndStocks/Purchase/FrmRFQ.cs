﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data;
using System.Data.SqlClient;



/* 
=================================================
   Author:		<Author,,Thasni>
   Create date: <Create Date,,28 June 2011>
   Description:	<Description,,RFQ Form>
================================================
*/

namespace MyBooksERP
{
    public partial class FrmRFQ : DevComponents.DotNetBar.Office2007Form
    {
        #region Declarations

        private MessageBoxIcon MmessageIcon;
        public long lngRFQIDToLoad { get; set; }
        public bool PblnDisableFunctionalities { get; set; }
        private bool MblnAddStatus;                       //Status for Addition/Updation mode 
        private bool MblnPrintEmailPermission = true;
        private bool MblnAddPermission = true;
        private bool MblnUpdatePermission = true;
        private bool MblnDeletePermission = true;
        private bool MblnAddUpdatePermission = true;
        private bool MblnCancelPermission = true;
        private bool MblnIsFromCancellation = false;
        private bool MblnChangeStatus = false;
        public bool PblnIsFrmApproval;
        private string MsMessageCommon;                 //  variable for assigning message
        private ArrayList MaMessageArr;                 // Error Message display
        private ArrayList MaStatusMessage;
        ClsBLLRFQ MobjClsBLLRFQ;
        ClsLogWriter mObjLogs;                          //  Object for Class Clslogs
        ClsNotification mObjNotification;               //  Object for Class ClsNotification
        DataTable dtTempDetails = new DataTable();
        int MintCompanyID;
        public int PintApprovalPermission = 0;

        #endregion   

        #region Constructor
        public FrmRFQ()
        {
            InitializeComponent();
            MobjClsBLLRFQ = new ClsBLLRFQ();
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
        }
        #endregion
        
        #region Functions

        private bool LoadCombos(int intType, string condition)
        {
            // function for loading combo
            // 0 - For Loading All Combo
            // 1 - For Loading CboCompanyName
            // 2 - For Loading CboSearchCompany           
            // 3 - For Loading CboSStatus
        
            
            if (string.IsNullOrEmpty(condition))
                condition = string.Empty;
            else
                condition = " And " + condition;
            try
            {
                clsBLLPermissionSettings objBllPermissionSettings = new clsBLLPermissionSettings();
                DataTable dtControlsPermissions = new DataTable();
                dtControlsPermissions = objBllPermissionSettings.GetControlsPermissions(ClsCommonSettings.RoleID, (Int32)eMenuID.RFQ, ClsCommonSettings.CompanyID);
                MblnCancelPermission = false;

                if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    MblnCancelPermission = true;
                else
                {
                    dtControlsPermissions.DefaultView.RowFilter = "ControlName = 'BindingNavigatorCancelItem'";
                    if (dtControlsPermissions.DefaultView.ToTable().Rows.Count > 0)
                        MblnCancelPermission = Convert.ToBoolean(dtControlsPermissions.DefaultView.ToTable().Rows[0]["IsVisible"]);
                }

                DataTable datCombos = new DataTable();
                //bool blnIsEnabled = false;
                if (intType == 0 || intType == 1)
                {
                    datCombos = null;
                    if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                        datCombos = MobjClsBLLRFQ.FillCombos(new string[] { "CompanyID,Name", "CompanyMaster", "" });
                    else
                        try
                        {
                            datCombos = MobjClsBLLRFQ.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RFQCompany);
                        }
                        catch { }
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "Name";
                    cboCompany.DataSource = datCombos;
                }
                if (intType == 0 || intType == 2)
                {
                    datCombos = null;
                    if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                        datCombos = MobjClsBLLRFQ.FillCombos(new string[] { "CompanyID,Name", "CompanyMaster", "" });
                    else
                    {
                        try
                        {
                            datCombos = MobjClsBLLRFQ.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RFQCompany);
                        }
                        catch { }

                        if (datCombos == null)
                            datCombos = MobjClsBLLRFQ.FillCombos(new string[] { "CompanyID,Name", "CompanyMaster", "CompanyID = " + ClsCommonSettings.CompanyID });
                        else
                        {
                            if (datCombos.Rows.Count == 0)
                                datCombos = MobjClsBLLRFQ.FillCombos(new string[] { "CompanyID,Name", "CompanyMaster", "CompanyID = " + ClsCommonSettings.CompanyID });
                        }
                    }
                    cboSCompany.ValueMember = "CompanyID";
                    cboSCompany.DisplayMember = "Name";
                    cboSCompany.DataSource = datCombos;
                }          
                if (intType == 0 || intType == 3)
                {
                    //datCombos = null;
                    //datCombos = MobjClsBLLRFQ.FillCombos(new string[] { "StatusID,Description", "STCommonStatusReference", "StatusTypeID =" + (int)StatusType.RFQ + " ", "StatusID", "Description" });
                    //DataRow dr = datCombos.NewRow();
                    //dr["StatusID"] = -2;
                    //dr["Description"] = "ALL";
                    //datCombos.Rows.InsertAt(dr, 0);
                    //cboSStatus.ValueMember = "StatusID";
                    //cboSStatus.DisplayMember = "Description";
                    //cboSStatus.DataSource = datCombos;
                }
                mObjLogs.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
                return false;
            }
        }

        private DataTable FillComboColumn(int intItemID)
        {
            try
            {
                
                    DataTable dtItemUOMs = MobjClsBLLRFQ.GetItemUOMs(intItemID);
                    Uom.DataSource = null;
                    Uom.ValueMember = "UOMID";
                    Uom.DisplayMember = "ShortName";
                    Uom.DataSource = dtItemUOMs;
                    return dtItemUOMs;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FillComboColumn() " + ex.Message);
                mObjLogs.WriteLog("Error in FillComboColumn() " + ex.Message, 2);
            }
            return null;
        }

        private void FillParameters()
        {
            MobjClsBLLRFQ.PobjClsDTORFQ.RFQNo = txtRFQNo.Text.Trim();
             MobjClsBLLRFQ.PobjClsDTORFQ.CompanyID = Convert.ToInt32(cboCompany.SelectedValue);
             MobjClsBLLRFQ.PobjClsDTORFQ.QuatationDate = dtpRFQDate.Value.ToString("dd-MMM-yyyy").ToDateTime();
             MobjClsBLLRFQ.PobjClsDTORFQ.DueDate = dtpDueDate.Value.ToString("dd-MMM-yyyy").ToDateTime();            
             MobjClsBLLRFQ.PobjClsDTORFQ.Remarks = txtRemarks.Text.Trim();  
            if (MblnAddStatus)
            {
                 MobjClsBLLRFQ.PobjClsDTORFQ.CreatedBy = ClsCommonSettings.UserID;
            }
          
            if ( MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Cancelled)
            {
                 MobjClsBLLRFQ.PobjClsDTORFQ.blnCancelled = true;
                 MobjClsBLLRFQ.PobjClsDTORFQ.CancellationDescription = txtDescription.Text.Trim();
            }
            else
            {
                 MobjClsBLLRFQ.PobjClsDTORFQ.blnCancelled = false;
            }

            if (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Rejected || MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Approved)
            {
                 MobjClsBLLRFQ.PobjClsDTORFQ.VerifcationDescription = txtDescription.Text.Trim();
            }
            else
            {
                MobjClsBLLRFQ.PobjClsDTORFQ.VerifcationDescription = "";
            }
        }

        private void FillDetailParameters()
        {
            if (dgvRFQ.CurrentRow != null)
                dgvRFQ.CurrentCell = dgvRFQ["ItemCode", dgvRFQ.CurrentRow.Index];
            MobjClsBLLRFQ.PobjClsDTORFQ.lstClsDTORFQDetails = new System.Collections.Generic.List<clsDTORFQDetails>();
            for (int i = 0; i < dgvRFQ.Rows.Count; i++)
            {
                if (dgvRFQ["ItemID", i].Value != null && dgvRFQ["Quantity", i].Value != null)
                {
                    clsDTORFQDetails objClsDTORFQDetails = new clsDTORFQDetails();
                    objClsDTORFQDetails.ItemID = Convert.ToInt32(dgvRFQ["ItemID", i].Value);
                    objClsDTORFQDetails.Quantity = Convert.ToDecimal(dgvRFQ["Quantity", i].Value);
                    objClsDTORFQDetails.UOMID = Convert.ToInt32(dgvRFQ["UOM", i].Tag);

                    MobjClsBLLRFQ.PobjClsDTORFQ.lstClsDTORFQDetails.Add(objClsDTORFQDetails);
                }
            }
        }

        private bool SaveHandler()
        {
            try
            {
                if (RFQvalidation())
                {
                    if (SaveRFQ())
                    {
                        MblnAddStatus = false;
                        if (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (int)RFQStatus.Opened)
                        {
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                            MessageBox.Show(MsMessageCommon.Remove(MsMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblRFQStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                            tmrRFQ.Enabled = true;
                        }
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SavePurchase() " + ex.Message);
                mObjLogs.WriteLog("Error in SavePurchase() " + ex.Message, 2);
                return false;
            }
        }

        private bool SaveRFQ()
        {
            try
            {
                if (!MblnIsFromCancellation)
                {
                    if (MblnAddStatus == true)
                        MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 937, out MmessageIcon);
                    else
                    {
                        if (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.SubmittedForApproval || MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Submitted)
                        {
                            if (ClsCommonSettings.RFQApproval)
                                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 32, out MmessageIcon).Replace("***", "RFQ");
                            else
                                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9011, out MmessageIcon).Replace("*", "RFQ");
                        }
                        else
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
                    }
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return false;
                }
                if (MblnAddStatus && txtRFQNo.ReadOnly && MobjClsBLLRFQ.IsRFQNoExists(txtRFQNo.Text,cboCompany.SelectedValue.ToInt32()))
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 938, out MmessageIcon);
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    else
                        GenerateRFQNo();
                }
                FillParameters();
                FillDetailParameters();

                if (MobjClsBLLRFQ.SaveRFQ(MblnAddStatus))
                {
                    txtRFQNo.Tag = MobjClsBLLRFQ.PobjClsDTORFQ.RFQID;
                    mObjLogs.WriteLog("Saved successfully:SaveRFQ()  " + this.Name + "", 0);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SaveRFQ() " + ex.Message);
                mObjLogs.WriteLog("Error in SaveRFQ() " + ex.Message, 2);
                return false;
            }
        }

        private void LoadReport()
        {
            try
            {
                if (MobjClsBLLRFQ.PobjClsDTORFQ.RFQID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MobjClsBLLRFQ.PobjClsDTORFQ.RFQID;
                    ObjViewer.PiFormID = (int)FormID.RFQ;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on form FrmRFQ:LoadReport " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadReport() " + Ex.Message.ToString());
            }
        }

        private void SetStatus()
        {
            lblDescription.Visible = false;
            txtDescription.Visible = false;
            

            switch (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID)
            {
                case (int)RFQStatus.Opened:
                    lblStatusText.ForeColor = Color.Brown;
                    lblStatusText.Text = "Opened";
                    break;
                case (int)RFQStatus.SubmittedForApproval:
                    lblStatusText.ForeColor = Color.BlueViolet;
                    lblStatusText.Text = "Submitted For Approval";
                    break;
                case (int)RFQStatus.Submitted:
                    lblStatusText.ForeColor = Color.BlueViolet;
                    lblStatusText.Text = "Submitted";
                    break;
                case (int)RFQStatus.Approved:
                    lblStatusText.ForeColor = Color.Green;
                    lblStatusText.Text = "Approved";
                    lblDescription.Visible = true;
                    txtDescription.Visible = true;
                    break;
                case (int)RFQStatus.Rejected:
                    lblStatusText.ForeColor = Color.Red;
                    lblStatusText.Text = "Rejected";
                    lblDescription.Visible = true;
                    txtDescription.Visible = true;
                    break;
                case (int)RFQStatus.Cancelled:
                    lblStatusText.ForeColor = Color.Red;
                    lblStatusText.Text = "Cancelled";
                    lblDescription.Visible = true;
                    txtDescription.Visible = true;
                    break;
                case (int)RFQStatus.Closed:
                    lblStatusText.ForeColor = Color.DarkViolet;
                    lblStatusText.Text = "Closed";
                    break;
                default:
                    lblStatusText.ForeColor = Color.Black;
                    lblStatusText.Text = "New";
                    break;
            }
        }

        private void LoadMessage()
        {
            try
            {
                MaMessageArr = new ArrayList();
                MaStatusMessage = new ArrayList();
                MaMessageArr = mObjNotification.FillMessageArray((int)FormID.RFQ, 4);
                MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.RFQ, 4);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadMessage() " + ex.Message);
                mObjLogs.WriteLog("Error in LoadMessage() " + ex.Message, 2);
            }

        }

        private void SetPermissions()
        {
            try
            {
                // Function for setting permissions
                clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
                if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.RFQ, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                    if (MblnAddPermission == true || MblnUpdatePermission == true)
                        MblnAddUpdatePermission = true;
                }
                else
                    MblnAddPermission = MblnAddUpdatePermission = MblnCancelPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

                //BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                //BindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
                //BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;

                if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    DataTable DtPermission = objClsBLLPermissionSettings.GetMenuPermissions(ClsCommonSettings.RoleID);
                    if (DtPermission.Rows.Count == 0)
                    {
                        BtnItem.Enabled = BtnUom.Enabled = false;
                    }
                    else
                    {
                        DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Products).ToString();
                        BtnItem.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);
                        DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.UOM).ToString();
                        BtnUom.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SetPermissions " + ex.Message);
                mObjLogs.WriteLog("Error in SetPermissions " + ex.Message, 2);
            }
        }

        private void AddNewRFQ() //Itype=1 from seraching ,itype=0 from clear
        {
            try
            {
                MobjClsBLLRFQ = new ClsBLLRFQ();
                MblnAddStatus = true;
                txtRFQNo.Tag = 0;
                txtRFQNo.Text = string.Empty;
                txtRFQNo.Enabled = true;
                cboCompany.Enabled = true;

                lblCreatedByText.Text = "Created By : " + ClsCommonSettings.strEmployeeName;
                lblCreatedDateValue.Text = "Created Date : " + ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                txtDescription.Visible = false;
                lblDescription.Visible = false;

                BindingNavigatorCancelItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorClearItem.Enabled = false;
                BindingNavigatorAddNewItem.Enabled = false;

                btnActions.Enabled = false;
                ClearControls();
                btnApprove.Visible = false;
                btnReject.Visible = false;
                btnDeny.Visible = btnSuggest.Visible = tiSuggestions.Visible = false;
                bnSubmitForApproval.Visible = false;
                GenerateRFQNo();
                DisplayAllNoInSearchGrid();               
                MsMessageCommon = "Add new RFQ";   
                lblRFQStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmrFocus.Enabled = true;
                errRFQ.Clear();
                BtnPrint.Enabled = false;
                BtnEmail.Enabled = false;
                MblnIsFromCancellation = false;
                dgvRFQ.Columns["ItemCode"].ReadOnly = dgvRFQ.Columns["ItemName"].ReadOnly = false;
                dgvRFQ.Rows.Clear();
                if (lngRFQIDToLoad != 0)
                    SetEnableDisable(false);
                MblnChangeStatus = false;
                cboCompany.Focus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in AddNewRFQ() " + ex.Message);
                mObjLogs.WriteLog("Error in AddNewRFQ() " + ex.Message, 2);
            }
        }

        private void ClearControls()
        {
           
            dgvRFQ.Enabled = true;
            panelTop.Enabled = true;
            errRFQ.Clear();        

            txtSRFQNo.Text = "";
            cboSCompany.SelectedIndex = -1;
            cboSExecutive.SelectedIndex = -1;           
            cboSStatus.SelectedIndex = -1;

            cboCompany.SelectedValue = ClsCommonSettings.CompanyID;
          
            dtpRFQDate.Value = ClsCommonSettings.GetServerDate();
            dtpDueDate.Value = ClsCommonSettings.GetServerDate();
           
            dgvRFQ.Rows.Clear();
            dgvRFQ.ClearSelection();

            txtDescription.Clear();
            
            txtRemarks.Text = string.Empty;

            MobjClsBLLRFQ.PobjClsDTORFQ.StatusID = (int)RFQStatus.Opened;
            lblStatusText.Text = "New";
            lblStatusText.ForeColor = Color.Black;
            SetEnableDisable(true);
            
            dtTempDetails = new DataTable();
            cboCompany.Focus();
        }

        private void GenerateRFQNo()
        {
            try
            {
                txtRFQNo.Text = GetRFQPrefix() + MobjClsBLLRFQ.GetNextRFQNo(cboCompany.SelectedValue.ToInt32()).ToString();
                lblRFQCount.Text = "Total Quotations " + MobjClsBLLRFQ.GetTotalRFQCount();
               
                if (ClsCommonSettings.blnRFQAutogenerate)
                    txtRFQNo.ReadOnly = false;
                else
                    txtRFQNo.ReadOnly = true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in GenerateRFQNo() " + ex.Message);
                mObjLogs.WriteLog("Error in GenerateRFQNo() " + ex.Message, 2);
            }
        }

        private string GetRFQPrefix()
        {
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue));
            return ClsCommonSettings.strRFQNumber;
        }      

        private void ShowErrorMessage(Control ctrl, int ErrorCode, bool ShowErrProvider)
        {
            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, ErrorCode, out MmessageIcon);
            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            if (ShowErrProvider)
                errRFQ.SetError(ctrl, MsMessageCommon.Replace("#", "").Trim());
            lblRFQStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            tmrRFQ.Enabled = true;
            ctrl.Focus();
        }

        private bool RFQvalidation()
        {
            try
            {
                errRFQ.Clear();
                lblRFQStatus.Text = "";
                if (cboCompany.SelectedIndex == -1)
                {
                    ShowErrorMessage(cboCompany, 14, true);
                    return false;
                }
                if (string.IsNullOrEmpty(txtRFQNo.Text.Trim()))
                {
                    ShowErrorMessage(txtRFQNo, 24, true);
                    return false;
                }
                if (!txtRFQNo.ReadOnly && MobjClsBLLRFQ.IsRFQNoExists((txtRFQNo.Text),cboCompany.SelectedValue.ToInt32()))
                {
                   ShowErrorMessage(txtRFQNo, 939, true);
                    return false;
                }
                if (dtpRFQDate.Value.Date > ClsCommonSettings.GetServerDate())
                {
                    // due date limit
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9006, out MmessageIcon);
                    MsMessageCommon = MsMessageCommon.Replace("*", "RFQ");
                    errRFQ.SetError(dtpRFQDate, MsMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MsMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblRFQStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    tmrRFQ.Enabled = true;
                    dtpRFQDate.Focus();
                    return false;
                }
                if (dtpDueDate.Value.Date < dtpRFQDate.Value.Date)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9009, out MmessageIcon);
                    MsMessageCommon = MsMessageCommon.Replace("*", "RFQ");
                    errRFQ.SetError(dtpDueDate, MsMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MsMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblRFQStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    tmrRFQ.Enabled = true;
                    dtpDueDate.Focus();
                    return false;
                }
                if (ClsCommonSettings.strRFQDueDateLimit.ToInt32() != 0 && (dtpDueDate.Value > dtpRFQDate.Value.AddDays(ClsCommonSettings.strRFQDueDateLimit.ToDouble())))
                {
                                          // due date limit
                        MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9005, out MmessageIcon).Replace("*", ClsCommonSettings.strRFQDueDateLimit);
                        errRFQ.SetError(dtpDueDate, MsMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MsMessageCommon.Remove(MsMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblRFQStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        tmrRFQ.Enabled = true;
                        dtpDueDate.Focus();
                        return false;
                }
               
                if (dgvRFQ.Rows.Count == 1)
                {
                    ShowErrorMessage(txtRFQNo, 26, false);
                    dgvRFQ.Focus();
                    return false;
                }

                if (ValidateRFQDetails() == false) return false;

                if (dgvRFQ.Rows.Count > 0)
                {
                    int iTempID = 0;
                    iTempID = CheckDuplicationInGrid();
                    if (iTempID != -1)
                    {
                        //MsMessageCommon = "Please check duplication of values.";
                        ShowErrorMessage(dgvRFQ, 31, false);
                        dgvRFQ.Focus();
                         dgvRFQ.CurrentCell = dgvRFQ["ItemCode", iTempID];
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in RFQvalidation() " + ex.Message);
                mObjLogs.WriteLog("Error in RFQvalidation() " + ex.Message, 2);
                return false;
            }
        }

        private int CheckDuplicationInGrid()
        {
            string SearchValuefirst = "";
            int RowIndexTemp = 0;

            foreach (DataGridViewRow rowValue in dgvRFQ.Rows)
            {
                if (rowValue.Cells[ItemID.Index].Value != null)
                {
                    SearchValuefirst = Convert.ToString(rowValue.Cells[ItemID.Index].Value);
                    RowIndexTemp = rowValue.Index;
                    foreach (DataGridViewRow row in dgvRFQ.Rows)
                    {
                        if (RowIndexTemp != row.Index)
                        {

                            if (row.Cells[ItemID.Index].Value != null)
                            {
                                if ((Convert.ToString(row.Cells[ItemID.Index].Value).Trim() == Convert.ToString(SearchValuefirst).Trim()))
                                    return row.Index;
                            }
                        }
                    }
                }
            }
            return -1;
        }

        private bool ValidateRFQDetails()
        {
            try
            {
                int iGridRowCount = dgvRFQ.RowCount-1;            

                for (int i = 0; i < iGridRowCount; i++)
                {
                    int iRowIndex = dgvRFQ.Rows[i].Index;

                    if (Convert.ToString(dgvRFQ.Rows[i].Cells["ItemCode"].Value).Trim() == "")
                    {
                        //MsMessageCommon = "Please enter Item code.";
                        ShowErrorMessage(dgvRFQ, 27, false);
                        dgvRFQ.Focus();
                        dgvRFQ.CurrentCell = dgvRFQ["ItemCode", iRowIndex];
                        return false;
                    }
                    if (Convert.ToString(dgvRFQ.Rows[i].Cells["ItemName"].Value).Trim() == "")
                    {
                        //MsMessageCommon = "Please enter Item Name.";
                        ShowErrorMessage(dgvRFQ, 28, false);
                        dgvRFQ.Focus();
                        dgvRFQ.CurrentCell = dgvRFQ["ItemName", iRowIndex];
                        return false;
                    }
                    if (Convert.ToString(dgvRFQ.Rows[i].Cells["Quantity"].Value).Trim() == "" || Convert.ToString(dgvRFQ.Rows[i].Cells["Quantity"].Value).Trim() == "." || Convert.ToDecimal(dgvRFQ.Rows[i].Cells["Quantity"].Value) == 0)
                    {
                        //MsMessageCommon = "Please enter Quantity.";
                        ShowErrorMessage(dgvRFQ, 29, false);
                        dgvRFQ.Focus();
                        dgvRFQ.CurrentCell = dgvRFQ["Quantity", iRowIndex];
                        return false;
                    }                  

                    if (Convert.ToInt32(dgvRFQ.Rows[i].Cells["Uom"].Tag) == 0)
                    {
                        ShowErrorMessage(dgvRFQ, 30, false);
                        dgvRFQ.Focus();
                        dgvRFQ.CurrentCell = dgvRFQ["Uom", iRowIndex];
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CheckPurOrderGridValidate() " + ex.Message);
                mObjLogs.WriteLog("Error in CheckPurOrderGridValidate() " + ex.Message, 2);
                return false;
            }
        }

        private void Changestatus()
        {
            //Changing status
            MblnChangeStatus = true;
            if (MblnAddStatus)
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            else
            {
                if (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Opened || MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Rejected)
                    BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                else
                    BindingNavigatorSaveItem.Enabled = false;
            }
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorClearItem.Enabled = true;
            errRFQ.Clear();
        }      

        private bool DisplayAllNoInSearchGrid()
        {
            try
            {
                DataTable dtTemp = null;
                string strFilterCondition = string.Empty;

                if (!string.IsNullOrEmpty(txtSRFQNo.Text.Trim()))
                {
                    strFilterCondition = " RFQNo = '" + txtSRFQNo.Text.Trim() + "'";
                }
                else
                {
                    if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                        strFilterCondition += " CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue);
                    else
                    {
                        dtTemp = null;
                        dtTemp = (DataTable)cboSCompany.DataSource;
                        if (dtTemp.Rows.Count > 0)
                        {
                            strFilterCondition += "CompanyID In (";
                            foreach (DataRow dr in dtTemp.Rows)
                            {
                                strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                            }
                            strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                            strFilterCondition += ")";
                        }
                    }

                    if (cboSExecutive.SelectedValue != null && Convert.ToInt32(cboSExecutive.SelectedValue) != -2)
                    {
                        if (!string.IsNullOrEmpty(strFilterCondition))
                            strFilterCondition += " And ";
                        strFilterCondition += "EmployeeID = " + Convert.ToInt32(cboSExecutive.SelectedValue);
                    }
                    else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                    {
                        //if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                        //{
                            dtTemp = null;
                            dtTemp = (DataTable)cboSExecutive.DataSource;
                            if (dtTemp.Rows.Count > 0)
                            {
                                clsBLLPermissionSettings MobjClsBLLPermissionSettings = new clsBLLPermissionSettings();
                                DataTable datTempPer = MobjClsBLLPermissionSettings.GetControlPermissions1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RFQEmployee);
                                if (datTempPer != null)
                                {
                                    if (datTempPer.Rows.Count > 0)
                                    {
                                        if (datTempPer.Rows[0]["IsVisible"].ToString() == "True")
                                            strFilterCondition += "And EmployeeID In (0,";
                                        else
                                            strFilterCondition += "And EmployeeID In (";
                                    }
                                    else
                                        strFilterCondition += "And EmployeeID In (";
                                }
                                else
                                    strFilterCondition += "And EmployeeID In (";
                                foreach (DataRow dr in dtTemp.Rows)
                                {
                                    strFilterCondition += Convert.ToInt32(dr["EmployeeID"]) + ",";
                                }
                                strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                                strFilterCondition += ")";
                            }
                        //}
                    }
                    if (cboSStatus.SelectedValue != null && Convert.ToInt32(cboSStatus.SelectedValue) != -2)
                    {
                        if (!string.IsNullOrEmpty(strFilterCondition))
                            strFilterCondition += " And ";
                        strFilterCondition += "StatusID = " + Convert.ToInt32(cboSStatus.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += " CreatedDate >= '" + dtpSFrom.Value.ToString("dd-MMM-yyyy") + "' And CreatedDate <= '" + dtpSTo.Value.ToString("dd-MMM-yyyy") + "'";
                }
                DataTable dtPurchases = MobjClsBLLRFQ.GetRFQNos();
              
                dtPurchases.DefaultView.RowFilter = strFilterCondition;
                dgvRFQDisplay.DataSource = null;
                dgvRFQDisplay.DataSource = dtPurchases.DefaultView.ToTable();
                dgvRFQDisplay.Columns["RFQID"].Visible = false;
                dgvRFQDisplay.Columns["CompanyID"].Visible = false;
                dgvRFQDisplay.Columns["DepartmentID"].Visible = false;
                dgvRFQDisplay.Columns["StatusID"].Visible = false;
                dgvRFQDisplay.Columns["EmployeeID"].Visible = false;
                dgvRFQDisplay.Columns["CreatedDate"].Visible = false;
                dgvRFQDisplay.Columns["RFQNo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                lblRFQCount.Text = "Total RFQs " + dgvRFQDisplay.Rows.Count;

                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayAllNoInSearchGrid() " + ex.Message);
                mObjLogs.WriteLog("Error in DisplayAllNoInSearchGrid() " + ex.Message, 2);
                return false;
            }
        }

        private bool DisplayRFQInfo(long RfQID)
        {
            try
            {
                if (MobjClsBLLRFQ.DisplayRFQInfo(RfQID))
                {
                    btnActions.Enabled = false;
                    cboCompany.Enabled = false;
                    txtRFQNo.Enabled = false;
                    BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                    txtRFQNo.Tag = MobjClsBLLRFQ.PobjClsDTORFQ.RFQID;
                    cboCompany.SelectedValue = MobjClsBLLRFQ.PobjClsDTORFQ.CompanyID;
                    if (cboCompany.SelectedValue == null)
                    {
                        DataTable datTemp = MobjClsBLLRFQ.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjClsBLLRFQ.PobjClsDTORFQ.CompanyID });
                        if (datTemp.Rows.Count > 0)
                            cboCompany.Text = datTemp.Rows[0]["Name"].ToString();
                    }
                    clsBLLLogin objClsBLLLogin = new clsBLLLogin();
                    objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue));
                    cboCompany.Enabled = false;
                    lblCreatedByText.Text = "Created By : " +MobjClsBLLRFQ.PobjClsDTORFQ.EmployeeName;
                    lblCreatedDateValue.Text = "Created Date : " + MobjClsBLLRFQ.PobjClsDTORFQ.CreatedDate.ToString("dd-MMM-yyyy");
                    txtRFQNo.Text = MobjClsBLLRFQ.PobjClsDTORFQ.RFQNo;
                    dtpRFQDate.Value = MobjClsBLLRFQ.PobjClsDTORFQ.QuatationDate;
                    dtpDueDate.Value = MobjClsBLLRFQ.PobjClsDTORFQ.DueDate;

                    if (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Cancelled)
                    {
                        lblCreatedByText.Text += "         | Cancelled By : " + MobjClsBLLRFQ.PobjClsDTORFQ.strCancelledBy;
                        lblCreatedDateValue.Text += "         | Cancelled Date : " + MobjClsBLLRFQ.PobjClsDTORFQ.CancellationDate.ToDateTime().ToString("dd-MMM-yyyy");
                        txtDescription.Text = MobjClsBLLRFQ.PobjClsDTORFQ.CancellationDescription;
                       
                    }
                    else if (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Rejected )
                    {
                        lblCreatedDateValue.Text += "       | Rejected Date :"+ MobjClsBLLRFQ.PobjClsDTORFQ.ApprovedDate.ToString("dd-MMM-yyyy");
                        txtDescription.Text = MobjClsBLLRFQ.PobjClsDTORFQ.VerifcationDescription;
                        lblCreatedByText.Text += "        | Rejected By :"+ MobjClsBLLRFQ.PobjClsDTORFQ.strApprovedBy;
                    }
                    else if (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Approved)
                    {
                        lblCreatedDateValue.Text += "       | Approved Date :" + MobjClsBLLRFQ.PobjClsDTORFQ.ApprovedDate.ToString("dd-MMM-yyyy");
                        txtDescription.Text = MobjClsBLLRFQ.PobjClsDTORFQ.VerifcationDescription;
                        lblCreatedByText.Text += "        | Approved By :" + MobjClsBLLRFQ.PobjClsDTORFQ.strApprovedBy;
                    }
                    LoadCombos(13, "");
                  
                    txtRemarks.Text = MobjClsBLLRFQ.PobjClsDTORFQ.Remarks;
                                        
                    if (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Opened)
                    {
                        BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
                        BindingNavigatorCancelItem.Text = "Cancel";
                        BindingNavigatorCancelItem.Tooltip = "Cancel";
                        BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                        BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                        SetEnableDisable(true);
                    }
                    else if (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Cancelled)
                    {
                        BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
                        BindingNavigatorCancelItem.Text = "Re Open";
                        BindingNavigatorCancelItem.Tooltip = "Re Open";
                        BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                        BindingNavigatorSaveItem.Enabled = false;
                        SetEnableDisable(true);
                    }
                    else if (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.SubmittedForApproval)
                    {
                        BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
                        BindingNavigatorCancelItem.Text = "Cancel";
                        BindingNavigatorCancelItem.Tooltip = "Cancel";
                        BindingNavigatorSaveItem.Enabled = false;
                        BindingNavigatorDeleteItem.Enabled = false;
                        if (lngRFQIDToLoad != 0)
                        {
                            btnActions.Visible = true;
                            btnApprove.Visible = btnReject.Visible = true;
                            DataTable dtVerificationHistoryDetails = GetVerificationHistoryStatus(false);
                            if (dtVerificationHistoryDetails.Rows.Count > 0)
                            {
                                if (dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)RFQStatus.Suggested)
                                {
                                    btnSuggest.Visible = false;
                                    btnDeny.Visible = true;
                                    tiSuggestions.Visible  = true;
                                }
                                else
                                {
                                    btnSuggest.Visible = true;
                                    btnDeny.Visible = false;
                                    tiSuggestions.Visible = true;
                                }
                            }
                            else
                            {
                                if (PintApprovalPermission == (int)ApprovePermission.Suggest || PintApprovalPermission == (int)ApprovePermission.Both)
                                {
                                    tiSuggestions.Visible = true;
                                    btnDeny.Visible = btnSuggest.Visible = true;
                                }
                            }
                        }
                        SetEnableDisable(false);
                    }
                    else
                    {
                        if (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Rejected)
                        {
                            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                            SetEnableDisable(true);
                            if (lngRFQIDToLoad != 0)
                            {
                                btnActions.Visible = true;
                                DataTable dtVerificationHistoryDetails = GetVerificationHistoryStatus(false);
                                if (dtVerificationHistoryDetails.Rows.Count > 0)
                                {
                                    if (dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)OperationStatusType.PQuotationSuggested)
                                    {
                                        btnSuggest.Visible = false;
                                        btnDeny.Visible = true;
                                        tiSuggestions.Visible = true;
                                    }
                                    else
                                    {
                                        btnSuggest.Visible = true;
                                        btnDeny.Visible = false;
                                        tiSuggestions.Visible = true;
                                    }
                                }
                                else
                                {
                                    if (PintApprovalPermission == (int)ApprovePermission.Suggest || PintApprovalPermission == (int)ApprovePermission.Both)
                                    {
                                        tiSuggestions.Visible = true;
                                        btnDeny.Visible = btnSuggest.Visible = true;
                                    }
                                }

                                if (PintApprovalPermission == (int)ApprovePermission.Both || PintApprovalPermission == (int)ApprovePermission.Approve)
                                {
                                    btnApprove.Visible = true;
                                }
                            }
                        }
                        else
                        {
                            BindingNavigatorDeleteItem.Enabled = false;
                            SetEnableDisable(false);
                            if (lngRFQIDToLoad != 0)
                            {
                                btnActions.Visible = true;
                                btnReject.Visible = true;
                            }
                        }
                        BindingNavigatorCancelItem.Enabled = false;
                        BindingNavigatorCancelItem.Text = "Cancel";
                        BindingNavigatorCancelItem.Tooltip = "Cancel";
                        BindingNavigatorSaveItem.Enabled = false;
                    }

                    if ((MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Opened || MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Rejected) && lngRFQIDToLoad == 0)
                    {
                            bnSubmitForApproval.Visible = MblnUpdatePermission;
                            btnActions.Enabled = MblnUpdatePermission;
                            if (ClsCommonSettings.RFQApproval)
                                bnSubmitForApproval.Text = bnSubmitForApproval.Tooltip = "Submit For Approval";
                            else
                                bnSubmitForApproval.Text = bnSubmitForApproval.Tooltip  = "Submit";
                    }
                    if (lngRFQIDToLoad != 0 && lngRFQIDToLoad != MobjClsBLLRFQ.PobjClsDTORFQ.RFQID)
                    {
                        btnActions.Visible = bnSubmitForApproval.Visible = btnApprove.Visible = btnReject.Visible = false;
                        tiSuggestions.Visible = btnSuggest.Visible = btnDeny.Visible = false;
                    }
                    DisplayRFQDetail();
                    PurchaseOrderBindingNavigator.Refresh();
                    lblRFQStatus.Text = string.Empty;
                    if (PblnIsFrmApproval)
                    {
                        BindingNavigatorAddNewItem.Enabled = false;
                        BindingNavigatorSaveItem.Enabled = false;
                        BindingNavigatorCancelItem.Enabled = false;
                        BindingNavigatorDeleteItem.Enabled = false;
                        BindingNavigatorClearItem.Enabled = false;
                        SetEnableDisable(false);
                    }
                    SetStatus();
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayRFQInfo() " + ex.Message);
                mObjLogs.WriteLog("Error in DisplayRFQInfo() " + ex.Message, 2);
                return false;
            }
        }

        private void SetEnableDisable(bool blnIsEnabled)
        {
            dgvRFQ.ReadOnly = !blnIsEnabled;
            cboCompany.Enabled = txtRFQNo.Enabled = dtpDueDate.Enabled = dtpRFQDate.Enabled = BindingNavigatorClearItem.Enabled = blnIsEnabled;
            txtRemarks.ReadOnly = !blnIsEnabled;
        }

        private void DisplayRFQDetail()
        {
            try
            {
                dgvRFQ.Rows.Clear();

                if (MobjClsBLLRFQ.PobjClsDTORFQ != null)
                {
                    int i = 0;
                    foreach (clsDTORFQDetails RFQDetails in MobjClsBLLRFQ.PobjClsDTORFQ.lstClsDTORFQDetails)
                    {
                        dgvRFQ.RowCount = dgvRFQ.RowCount + 1;
                        dgvRFQ.Rows[i].Cells["ItemCode"].Value = RFQDetails.ItemCode;
                        dgvRFQ.Rows[i].Cells["ItemName"].Value = RFQDetails.ItemName;
                       // dgvRFQ.Rows[i].Cells["Quantity"].Value = RFQDetails.Quantity;
                        dgvRFQ.Rows[i].Cells["ItemID"].Value = RFQDetails.ItemID;
                        FillComboColumn(RFQDetails.ItemID);
                        dgvRFQ.Rows[i].Cells["Uom"].Value = RFQDetails.UOMID;
                        dgvRFQ.Rows[i].Cells["Uom"].Tag = RFQDetails.UOMID;
                        dgvRFQ.Rows[i].Cells["Uom"].Value = dgvRFQ.Rows[i].Cells["Uom"].FormattedValue;
                        int intUomScale = 0;
                        if (dgvRFQ[Uom.Index, i].Tag.ToInt32() != 0)
                            intUomScale = MobjClsBLLRFQ.FillCombos(new string[] { "Scale", "STUOMReference", "UOMID = " + dgvRFQ[Uom.Index, i].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                        dgvRFQ.Rows[i].Cells["Quantity"].Value = RFQDetails.Quantity.ToString("F" + intUomScale);
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayPurchaseOrderDetail() " + ex.Message);
                mObjLogs.WriteLog("Error in DisplayPurchaseOrderDetail() " + ex.Message, 2);
            }
        }

        public void RefreshForm()
        {
            btnSRefresh_Click(null, null);
        }

        #endregion

        #region Events

        private void FrmRFQ_Load(object sender, EventArgs e)
        {
            try
            {
                //ArrangeControls();
                LoadMessage();
                dtpSFrom.Value = ClsCommonSettings.GetServerDate();
                dtpSTo.Value = ClsCommonSettings.GetServerDate();
                SetPermissions();
                LoadCombos(0, null);
                AddNewRFQ();

                if (lngRFQIDToLoad != 0)
                {
                    BindingNavigatorCancelItem.Enabled = false;
                    BindingNavigatorDeleteItem.Enabled = false;
                    BindingNavigatorSaveItem.Enabled = false;
                    MblnAddStatus = false;
                    PblnIsFrmApproval = true;
                    DisplayRFQInfo( lngRFQIDToLoad);
                    if (PblnDisableFunctionalities)
                    {
                        PurchaseOrderBindingNavigator.Enabled = false;
                    }
                    if (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.SubmittedForApproval)
                    {
                        DataTable dtVerificationHistoryDetails = GetVerificationHistoryStatus(false);
                        if (dtVerificationHistoryDetails.Rows.Count > 0 && (PintApprovalPermission == (int)ApprovePermission.Suggest || PintApprovalPermission == (int)ApprovePermission.Both))
                        {
                            if (dtVerificationHistoryDetails.Rows[0]["StatusID"].ToInt32() == (int)RFQStatus.Suggested)
                            {
                                btnSuggest.Visible = false;
                                btnDeny.Visible = true;
                                tiSuggestions.Visible = true;
                            }
                            else
                            {
                                btnSuggest.Visible = true;
                                btnDeny.Visible = false;
                                tiSuggestions.Visible = true;
                            }
                        }
                        else
                        {
                            if (PintApprovalPermission == (int)ApprovePermission.Suggest || PintApprovalPermission == (int)ApprovePermission.Both)
                            {
                                btnSuggest.Visible = btnDeny.Visible = true;
                                tiSuggestions.Visible = true;
                            }
                        }
                        //if (PintApprovalPermission == (int)ApprovePermission.Both)
                        //{
                        //    btnApprove.Visible = true;
                        //    btnReject.Visible = true;
                        //    btnSuggest.Visible = btnDeny.Visible = tiSuggestions.Visible  = true;
                        //}
                        //else if (PintApprovalPermission == (int)ApprovePermission.Approve)
                        //{
                        //    btnApprove.Visible = btnReject.Visible = true;
                        //}
                        //else if (PintApprovalPermission == (int)ApprovePermission.Suggest)
                        //{
                        //    btnSuggest.Visible = btnReject.Visible = tiSuggestions.Visible  = true;
                        //}
                        if (PintApprovalPermission == (int)ApprovePermission.Approve || PintApprovalPermission == (int)ApprovePermission.Both)
                            btnApprove.Visible = btnReject.Visible = true;

                        btnActions.Enabled = true; 
                    }
                    if (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Approved)
                    {
                        btnReject.Visible = true;
                        btnActions.Enabled = true;
                    }
                    if (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (Int32)RFQStatus.Rejected)
                    {
                        btnActions.Enabled = true;
                    }
                    SetEnableDisable(false);
                }
                BindingNavigatorClearItem.Enabled = false;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FrmPurchaseLoad() " + ex.Message);
                mObjLogs.WriteLog("Error in FrmPurchaseLoad() " + ex.Message, 2);
            }
        }

        private void btnSRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                DisplayAllNoInSearchGrid();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnSRefreshClick() " + ex.Message);
                mObjLogs.WriteLog("Error in btnSRefreshClick() " + ex.Message, 2);
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewRFQ();
        }
      
        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                txtRFQNo.Focus();
                if (SaveHandler())
                {
                    AddNewRFQ();
                    DisplayAllNoInSearchGrid();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorSaveItem_Click() " + ex.Message);
                mObjLogs.WriteLog("Error in BindingNavigatorSaveItem_Click() " + ex.Message, 2);
            }
        }

        private void dgvRFQ_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvRFQ.CurrentCell.ColumnIndex == 0 || dgvRFQ.CurrentCell.ColumnIndex == 1)
                {
                    for (int i = 0; i < dgvRFQ.Columns.Count; i++)
                    {
                        dgvRFQ.Rows[dgvRFQ.CurrentCell.RowIndex].Cells[i].Value = null;
                        dgvRFQ.Rows[dgvRFQ.CurrentCell.RowIndex].Cells[i].Tag = null;
                    }
                }
                dgvRFQ.PServerName = ClsCommonSettings.ServerName;
                string[] First = { "ItemCode", "ItemName", "ItemID" };//first grid 
                string[] second = { "ItemCode", "Description", "ItemID" };//inner grid
                dgvRFQ.aryFirstGridParam = First;
                dgvRFQ.arySecondGridParam = second;
                dgvRFQ.PiFocusIndex = Quantity.Index;
                dgvRFQ.iGridWidth = 350;
                dgvRFQ.bBothScrollBar = true;
                dgvRFQ.pnlLeft = expandableSplitterLeft.Location.X + 5;
                dgvRFQ.pnlTop = expandableSplitterTop.Location.Y + PurchaseOrderBindingNavigator.Height + 10;
                dgvRFQ.ColumnsToHide = new string[] { "ItemID" };
                    if (dgvRFQ.CurrentCell.ColumnIndex == ItemCode.Index)
                    {
                        dgvRFQ.field = "ItemCode";
                        dgvRFQ.CurrentCell.Value = dgvRFQ.TextBoxText;
                    }
                    if (dgvRFQ.CurrentCell.ColumnIndex == ItemName.Index)
                    {
                        dgvRFQ.field = "Description";
                        dgvRFQ.CurrentCell.Value = dgvRFQ.TextBoxText;
                    }
                int intParentID = MobjClsBLLRFQ.FillCombos(new string[] { "ParentID", "CompanyMaster", "CompanyID = " + cboCompany.SelectedValue.ToInt32() }).Rows[0]["ParentID"].ToInt32();
                DataTable datCompany = new DataTable();
                if (intParentID == 0)
                    datCompany = MobjClsBLLRFQ.FillCombos(new string[] { "CompanyID", "CompanyMaster", "(CompanyID = " + cboCompany.SelectedValue.ToInt32() + " Or ParentID = " + cboCompany.SelectedValue.ToInt32() + ")" });
                else
                    datCompany = MobjClsBLLRFQ.FillCombos(new string[] { "CompanyID", "CompanyMaster", "(CompanyID = " + intParentID + " Or ParentID = " + intParentID + ")" });
                if (datCompany.Rows.Count > 0)
                {
                    DataRow dr = datCompany.NewRow();
                    dr["CompanyID"] = 0;
                    datCompany.Rows.InsertAt(dr, 0);
                }
                dgvRFQ.PsQuery = "select [ItemCode] as ItemCode,[Description] as Description,[ItemID] as ItemID from STItemmaster where " + dgvRFQ.field + " like  '" + ((dgvRFQ.TextBoxText)) + "%' And ItemStatusID In ("+(int)OperationStatusType.ProductActive+")";
                dgvRFQ.PsQuery += " And IsNull(CompanyID,0) In (";
                foreach (DataRow dr in datCompany.Rows)
                    dgvRFQ.PsQuery += dr["CompanyID"].ToInt32() + ",";
                dgvRFQ.PsQuery = dgvRFQ.PsQuery.Remove(dgvRFQ.PsQuery.Length - 1);
                dgvRFQ.PsQuery += ")";
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvItemGrid_TextBoxChangedEvent() " + ex.Message);
                mObjLogs.WriteLog("Error in dgvItemGrid_TextBoxChangedEvent() " + ex.Message, 2);
            }
        }

        private void dgvRFQ_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == ItemCode.Index) || (e.ColumnIndex == ItemName.Index))
            {
                dgvRFQ.PiColumnIndex = e.ColumnIndex;
            }
        }

        private void BindingNavigatorClearItem_Click(object sender, EventArgs e)
        {
            ClearControls();
            BindingNavigatorAddNewItem.Enabled = true;
            BindingNavigatorSaveItem.Enabled = false;
            BindingNavigatorClearItem.Enabled = false;
        }

        private void dgvRFQ_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            this.dgvRFQ.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvRFQ.CurrentCell.OwningColumn.Name == "Quantity" )
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
                int intUomScale = 0;
                if (dgvRFQ[Uom.Index, dgvRFQ.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    intUomScale = MobjClsBLLRFQ.FillCombos(new string[] { "Scale", "STUOMReference", "UOMID = " + dgvRFQ[Uom.Index, dgvRFQ.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    if (intUomScale == 0)
                    {
                        if (e.KeyChar == '.')
                            e.Handled = true;
                    }
                    else
                    {
                        int dotIndex = -1;
                        if (txt.Text.Contains("."))
                        {
                            dotIndex = txt.Text.IndexOf('.');
                        }
                        if (e.KeyChar == '.')
                        {
                            if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            {
                                e.Handled = true;
                            }
                        }
                        else
                        {
                            if (char.IsDigit(e.KeyChar))
                            {
                                if (dotIndex != -1 && txt.SelectionStart > dotIndex)
                                {
                                    string[] splitText = txt.Text.Split('.');
                                    if (splitText.Length == 2)
                                    {
                                        if (splitText[1].Length - txt.SelectedText.Length >= intUomScale)
                                            e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (dgvRFQ.CurrentCell.OwningColumn.Name == "ItemCode" || dgvRFQ.CurrentCell.OwningColumn.Name == "ItemName")
            {
                if (e.KeyChar.ToString() == "'")
                {
                    e.Handled = true;
                }
            }
        }

        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void dgvRFQ_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == ItemID.Index && e.RowIndex >= 0)
                {
                    if (dgvRFQ["ItemCode", e.RowIndex].Value != null)
                    {
                        if (dgvRFQ.CurrentRow.Cells["ItemID"].Value != null)
                        {
                            int iItemID = Convert.ToInt32(dgvRFQ.CurrentRow.Cells["ItemID"].Value);
                           
                            int tag = -1;
                            if (dgvRFQ.CurrentRow.Cells["Uom"].Tag != null)
                            {
                                tag = Convert.ToInt32(dgvRFQ.CurrentRow.Cells["Uom"].Tag);
                            }
                           FillComboColumn(iItemID);
                           if (tag == -1)
                           {
                               DataTable datDefaultUom = MobjClsBLLRFQ.FillCombos(new string[] { "DefaultPurchaseUomID as UomID", "STItemMaster", "ItemID = " + iItemID + "" });
                               tag = Convert.ToInt32(datDefaultUom.Rows[0]["UomID"]);
                           }
                            dgvRFQ.CurrentRow.Cells["Uom"].Value = tag;
                            dgvRFQ.CurrentRow.Cells["Uom"].Tag = tag;
                            dgvRFQ.CurrentRow.Cells["Uom"].Value = dgvRFQ.CurrentRow.Cells["Uom"].FormattedValue;

                            tag = -1;
                        }
                        else
                        {
                            Uom.DataSource = null;
                        }
                    }
                    //else if (e.ColumnIndex == Quantity.Index)
                    //{
                    //    int intDecimalPart = 0;
                    //    if (dgvRFQ.CurrentCell.Value != null && !string.IsNullOrEmpty(dgvRFQ.CurrentCell.Value.ToString()) && dgvRFQ.CurrentCell.Value.ToString().Contains("."))
                    //        intDecimalPart = dgvRFQ.CurrentCell.Value.ToString().Split('.')[1].Length;
                    //    int intUomScale = 0;
                    //    if (dgvRFQ[Uom.Index, dgvRFQ.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    //        intUomScale = MobjClsBLLRFQ.FillCombos(new string[] { "Scale", "STUOMReference", "UOMID = " + dgvRFQ[Uom.Index, dgvRFQ.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                    //    if (intDecimalPart > intUomScale)
                    //    {
                    //        dgvRFQ.CurrentCell.Value = Math.Round(dgvRFQ.CurrentCell.Value.ToDecimal(), intUomScale).ToString();
                    //        dgvRFQ.EditingControl.Text = dgvRFQ.CurrentCell.Value.ToString();
                    //    }
                    //}
                }
                if (MobjClsBLLRFQ != null && MobjClsBLLRFQ.PobjClsDTORFQ != null && (MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == 0 || MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (int)RFQStatus.Opened || MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (int)RFQStatus.Rejected || MobjClsBLLRFQ.PobjClsDTORFQ.StatusID == (int)RFQStatus.Cancelled))
                Changestatus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvRFQ_CellValueChanged() " + ex.Message);
                mObjLogs.WriteLog("Error in dgvRFQ_CellValuChanged() " + ex.Message, 2);
            }
        }

        private void dgvRFQ_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvRFQ.IsCurrentCellDirty)
            {
                if (dgvRFQ.CurrentCell != null)
                    dgvRFQ.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dgvRFQ_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                int iRowIndex = 0;
                
                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    dgvRFQ.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    iRowIndex = dgvRFQ.CurrentCell.RowIndex;
                    if (e.ColumnIndex == Quantity.Index ||  e.ColumnIndex == Uom.Index )
                    {
                        if (Convert.ToString(dgvRFQ.CurrentRow.Cells[ItemID.Index].Value).Trim() == "")
                        {
                                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 28, out MmessageIcon);
                                errRFQ.SetError(dgvRFQ, MsMessageCommon.Replace("#", "").Trim());
                                e.Cancel = true;
                        }
                    }

                    if (e.ColumnIndex == Uom.Index )
                    {
                        if (dgvRFQ.CurrentRow.Cells["ItemID"].Value != null)
                        {
                            int iItemID = Convert.ToInt32(dgvRFQ.CurrentRow.Cells["ItemID"].Value);
                            int tag = -1;
                                if (dgvRFQ.CurrentRow.Cells["Uom"].Tag != null)
                                {
                                    tag = Convert.ToInt32(dgvRFQ.CurrentRow.Cells["Uom"].Tag);
                                }
                                FillComboColumn(iItemID);
                                if (tag != -1)
                                    dgvRFQ.CurrentRow.Cells["Uom"].Tag = tag;
                        }
                        else
                        {
                            Uom.DataSource = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvRFQ_CellBeginEdit() " + ex.Message);
                mObjLogs.WriteLog("Error in dgvRFQ_CellBeginEdit() " + ex.Message, 2);
            }
        }

        private void tmrFocus_Tick(object sender, EventArgs e)
        {
            cboCompany.Focus();
            tmrFocus.Enabled = false;
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Changestatus();
                if (MblnAddStatus)
                    GenerateRFQNo();

                int intCurrentCompanyID = 0;
                DataTable datCompany = MobjClsBLLRFQ.FillCombos(new string[] { "CompanyID,ParentID", "CompanyMaster", "CompanyID = " + cboCompany.SelectedValue.ToInt32() });
                if (datCompany.Rows[0]["ParentID"].ToInt32() == 0)
                    intCurrentCompanyID = datCompany.Rows[0]["CompanyID"].ToInt32();
                else
                    intCurrentCompanyID = datCompany.Rows[0]["ParentID"].ToInt32();
                if (intCurrentCompanyID != MintCompanyID)
                    dgvRFQ.Rows.Clear();
                MintCompanyID = intCurrentCompanyID;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in cboCompany_SelectedIndexChanged() " + ex.Message);
                mObjLogs.WriteLog("Error in cboCompany_SelectedIndexChanged() " + ex.Message, 2);
            }
        }

        private void BindingNavigatorCancelItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (BindingNavigatorCancelItem.Text.ToUpper() == "CANCEL")
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 33, out MmessageIcon).Replace("***", "RFQ");

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                        {
                            objFrmCancellationDetails.ShowDialog();
                            MobjClsBLLRFQ.PobjClsDTORFQ.CancellationDate = objFrmCancellationDetails.PDtCancellationDate;
                            txtDescription.Text = objFrmCancellationDetails.PStrDescription;
                            MobjClsBLLRFQ.PobjClsDTORFQ.intCancelledBy = ClsCommonSettings.UserID;

                            if (!objFrmCancellationDetails.PBlnIsCanelled)
                            {
                                int intOldStatus = MobjClsBLLRFQ.PobjClsDTORFQ.StatusID;
                                MobjClsBLLRFQ.PobjClsDTORFQ.StatusID = (Int32)RFQStatus.Cancelled;

                                MblnIsFromCancellation = true;
                                if (SaveHandler())
                                {
                                    BindingNavigatorCancelItem.Text = "Re Open";
                                    BindingNavigatorAddNewItem_Click(null, null);
                                }
                                else
                                {
                                    MobjClsBLLRFQ.PobjClsDTORFQ.StatusID = intOldStatus;
                                }
                                MblnIsFromCancellation = false;
                            }
                        }
                    }
                }
                else
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 34, out MmessageIcon).Replace("***", "RFQ");

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        MobjClsBLLRFQ.PobjClsDTORFQ.StatusID = (Int32)RFQStatus.Opened;

                        MblnIsFromCancellation = true;
                        if (SaveHandler())
                        {
                            BindingNavigatorCancelItem.Text = "Cancel";
                            //BindingNavigatorSaveItem.Enabled = MbUpdatePermission;
                            //BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
                            BindingNavigatorAddNewItem_Click(null, null);
                        }
                        else
                        {
                            MobjClsBLLRFQ.PobjClsDTORFQ.StatusID = (int)RFQStatus.Cancelled;
                        }
                        MblnIsFromCancellation = false;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorCancelItem_Click() " + ex.Message);
                mObjLogs.WriteLog("Error in BindingNavigatorCancelItem_Click() " + ex.Message, 2);
            }
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 940, out MmessageIcon);

                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                    {
                        objFrmCancellationDetails.Text = "Approval Details";
                        objFrmCancellationDetails.lblCancelledDate.Text = "Approval Date";
                        objFrmCancellationDetails.ShowDialog();
                        MobjClsBLLRFQ.PobjClsDTORFQ.ApprovedDate = objFrmCancellationDetails.PDtCancellationDate;
                        txtDescription.Text = objFrmCancellationDetails.PStrDescription;

                        if (!objFrmCancellationDetails.PBlnIsCanelled)
                        {
                            txtRFQNo.Focus();
                            int intOldStatusID = MobjClsBLLRFQ.PobjClsDTORFQ.StatusID;

                            MobjClsBLLRFQ.PobjClsDTORFQ.StatusID = (Int32)RFQStatus.Approved;
                            MobjClsBLLRFQ.PobjClsDTORFQ.ApprovedBy = ClsCommonSettings.UserID;
                            MobjClsBLLRFQ.PobjClsDTORFQ.ApprovedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy").ToDateTime();
                            MblnIsFromCancellation = true;
                            if (SaveHandler())
                            {
                                AddNewRFQ();
                                DisplayAllNoInSearchGrid();
                            }
                            else
                            {
                                MobjClsBLLRFQ.PobjClsDTORFQ.StatusID = intOldStatusID;
                            }
                            MblnIsFromCancellation = false;
                            PblnIsFrmApproval = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnApproveClick() " + ex.Message);
                mObjLogs.WriteLog("Error in BtnApproveClick() " + ex.Message, 2);
            }
        }

        private void dgvRFQ_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (dgvRFQ.ReadOnly)
                e.Cancel = true;
            else
            dgvRFQ.Rows[e.Row.Index].Cells["Uom"].ReadOnly = false;
        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (MobjClsBLLRFQ.FillCombos(new string[] { "PurchaseQuotationID", "STPurchaseQuotationMaster", "QuotationType = " + (int)OperationOrderType.PQuotationFromRFQ + " And ReferenceID = " + MobjClsBLLRFQ.PobjClsDTORFQ.RFQID }).Rows.Count > 0)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 941, out MmessageIcon).Replace("#", "");
                    MessageBox.Show(MsMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 35, out MmessageIcon).Replace("***", "RFQ");                

                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                    {
                        objFrmCancellationDetails.Text = "Rejection Details";
                        objFrmCancellationDetails.lblCancelledDate.Text = "Rejected Date";
                        objFrmCancellationDetails.ShowDialog();
                        MobjClsBLLRFQ.PobjClsDTORFQ.ApprovedDate  = objFrmCancellationDetails.PDtCancellationDate;
                        txtDescription.Text = objFrmCancellationDetails.PStrDescription;

                        if (!objFrmCancellationDetails.PBlnIsCanelled)
                        {
                            txtRFQNo.Focus();
                            int intOldStatusID = MobjClsBLLRFQ.PobjClsDTORFQ.StatusID;

                            MobjClsBLLRFQ.PobjClsDTORFQ.StatusID = (Int32)RFQStatus.Rejected;
                           MobjClsBLLRFQ.PobjClsDTORFQ.ApprovedBy = ClsCommonSettings.UserID;
                           MobjClsBLLRFQ.PobjClsDTORFQ.ApprovedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy").ToDateTime();
                            MblnIsFromCancellation = true;
                            if (SaveHandler())
                            {
                                AddNewRFQ();
                                DisplayAllNoInSearchGrid();
                            }
                            else
                            {
                                MobjClsBLLRFQ.PobjClsDTORFQ.StatusID = intOldStatusID;
                            }
                            MblnIsFromCancellation = false;
                            PblnIsFrmApproval = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnReject_Click() " + ex.Message);

                mObjLogs.WriteLog("Error in btnReject_Click() " + ex.Message, 2);
            }
        }

        private void txtSRFQNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\'')
                e.Handled = true;
        }

        private void FrmRFQ_Shown(object sender, EventArgs e)
        {
            cboCompany.Focus();
        }

        private void FrmRFQ_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.MdiFormClosing)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4214, out MmessageIcon).Replace("#", "").Trim();
                if (MblnChangeStatus)
                {
                    if (MessageBox.Show(MsMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                       DialogResult.Yes)
                    {
                        e.Cancel = false;
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else
                    e.Cancel = false;
            }
        }

        private void tmrRFQ_Tick(object sender, EventArgs e)
        {
            errRFQ.Clear();
            lblRFQStatus.Text = "";
            tmrRFQ.Enabled = false;
        }

        private void bnSubmitForApproval_Click(object sender, EventArgs e)
        {
            bool status = true;
            if (dtpDueDate.Value < ClsCommonSettings.GetServerDate().Date)
            {
                status = false;
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 37, out MmessageIcon);
                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    status = false;
                }
                else
                {
                    status = true;
                }
            }
            if (status == true)
            {
                try
                {
                    txtRFQNo.Focus();
                    int intOldStatusID = MobjClsBLLRFQ.PobjClsDTORFQ.StatusID;
                    if (ClsCommonSettings.RFQApproval)
                        MobjClsBLLRFQ.PobjClsDTORFQ.StatusID = (Int32)RFQStatus.SubmittedForApproval;
                    else
                        MobjClsBLLRFQ.PobjClsDTORFQ.StatusID = (int)RFQStatus.Submitted;
                    if (SaveHandler())
                    {
                        AddNewRFQ();
                        DisplayAllNoInSearchGrid();
                    }
                    else
                    {
                        MobjClsBLLRFQ.PobjClsDTORFQ.StatusID = intOldStatusID;
                    }
                }
                catch (Exception ex)
                {
                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error in BtnSubmitForApprovalClick() " + ex.Message);
                    mObjLogs.WriteLog("Error in BtnSubmitForApprovalClick() " + ex.Message, 2);
                }
            }
            else
            {
                AddNewRFQ();
            }
        }

        private void dgvRFQDisplay_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvRFQDisplay.Rows.Count > 0)
                {
                    if (Convert.ToInt64(dgvRFQDisplay.CurrentRow.Cells[0].Value) != 0)
                    {
                        MblnAddStatus = false;
                        DisplayRFQInfo( Convert.ToInt64(dgvRFQDisplay.CurrentRow.Cells[0].Value)); 
                        BindingNavigatorAddNewItem.Enabled = MblnAddPermission ;
                        BtnPrint.Enabled = MblnPrintEmailPermission;
                        BtnEmail.Enabled = MblnPrintEmailPermission;
                        lblRFQStatus.Text = "Details Of RFQ No "+dgvRFQDisplay.CurrentRow.Cells[1].Value.ToString();
                        if(lngRFQIDToLoad!=0)
                        {
                            if (Convert.ToInt64(dgvRFQDisplay.CurrentRow.Cells[0].Value) == lngRFQIDToLoad)
                                btnActions.Enabled = true;
                            else

                                btnActions.Enabled = false;
                        }

                        DataTable datQuotations = MobjClsBLLRFQ.FillCombos(new string[] { "PurchaseQuotationID", "STPurchaseQuotationMaster", "QuotationType = " + (int)OperationOrderType.PQuotationFromRFQ + " And ReferenceID = " + MobjClsBLLRFQ.PobjClsDTORFQ.RFQID });
                        if (datQuotations.Rows.Count > 0)
                        {
                            SetEnableDisable(false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvRFQDisplay_CellClick() " + ex.Message);
                mObjLogs.WriteLog("Error in dgvRFQDisplay_CellClick() " + ex.Message, 2);
            }
        }

        private void txtRFQNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetterOrDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
                e.Handled = true;
            else
                e.Handled = false;
        }

        private void CboSCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable datCombos = null; //bool blnIsEnabled = false;
            if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
            {
                if (cboSCompany.SelectedValue == null || Convert.ToInt32(cboSCompany.SelectedValue) == -2)
                {
                    DataTable datCompany = (DataTable)cboSCompany.DataSource;
                    string strFilterCondition = "";
                    if (datCompany.Rows.Count > 0)
                    {
                        strFilterCondition = "CompanyID In (";
                        foreach (DataRow dr in datCompany.Rows)
                            strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                        strFilterCondition += ")";
                    }
                    datCombos = MobjClsBLLRFQ.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
                        " ON EM.EmployeeID=UM.EmployeeID INNER JOIN STRFQMaster IM ON UM.UserID=IM.CreatedBy", "IM." + strFilterCondition });
                }
                else
                {
                    datCombos = MobjClsBLLRFQ.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
                        " ON EM.EmployeeID=UM.EmployeeID INNER JOIN STRFQMaster IM ON UM.UserID=IM.CreatedBy", "IM.CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue) });
                }
            }
            else
            {
                //if (cboSCompany.SelectedValue == null || Convert.ToInt32(cboSCompany.SelectedValue) == -2)
                //{
                //    datCombos = MobjClsBLLRFQ.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.FirstName","EmployeeMaster EM INNER JOIN UserMaster UM " +
                //        " ON EM.EmployeeID=UM.EmployeeID INNER JOIN STRFQMaster IM ON UM.UserID=IM.CreatedBy", "" +
                //        " IM.CompanyID IN(SELECT CompanyID FROM STRoleFIeldsDetails WHERE RoleID=" + ClsCommonSettings.RoleID + 
                //        " AND ControlID=" + (int)ControlOperationType.RFQEmployee + " AND IsVisible=1)" });
                //}
                //else
                //{
                //    datCombos = MobjClsBLLRFQ.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.FirstName","EmployeeMaster EM INNER JOIN UserMaster UM " +
                //        " ON EM.EmployeeID=UM.EmployeeID INNER JOIN STRFQMaster IM ON UM.UserID=IM.CreatedBy", "" +
                //        " IM.CompanyID IN(SELECT CompanyID FROM STRoleFIeldsDetails WHERE RoleID=" + ClsCommonSettings.RoleID + 
                //        " AND ControlID=" + (int)ControlOperationType.RFQEmployee + " AND IsVisible=1 AND CompanyID=" + Convert.ToInt32(cboSCompany.SelectedValue) + ")" });
                //}
                //if (datCombos.Rows.Count == 0)
                //    datCombos = MobjClsBLLRFQ.FillCombos(new string[] { "EmployeeID,FirstName", "EmployeeMaster", "EmployeeID = " + ClsCommonSettings.intEmployeeID });
                try
                {
                    DataTable datTemp = MobjClsBLLRFQ.FillCombos(new string[] { "IsVisible", "STRoleFieldsDetails", "" + 
                        "RoleID=" + ClsCommonSettings.RoleID + " AND CompanyID=" + ClsCommonSettings.CompanyID + " " +
                        "AND ControlID=" + (int)ControlOperationType.RFQEmployee + " AND IsVisible=1" });
                    if (datTemp != null)
                        if (datTemp.Rows.Count > 0)
                            datCombos = MobjClsBLLRFQ.GetEmployeeByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RFQCompany);

                    if (datCombos == null)
                        datCombos = MobjClsBLLRFQ.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
                                        " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
                    else
                    {
                        if (datCombos.Rows.Count == 0)
                            datCombos = MobjClsBLLRFQ.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
                                        " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
                    }
                }
                catch { }
            }
            cboSExecutive.ValueMember = "EmployeeID";
            cboSExecutive.DisplayMember = "EmployeeFullName";
            cboSExecutive.DataSource = datCombos;
        }

        private void dgvRFQ_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    dgvRFQ.EndEdit();
                    if (e.ColumnIndex == Uom.Index)
                    {
                        dgvRFQ.CurrentRow.Cells["Uom"].Tag = dgvRFQ.CurrentRow.Cells["Uom"].Value;
                        dgvRFQ.CurrentRow.Cells["Uom"].Value = dgvRFQ.CurrentRow.Cells["Uom"].FormattedValue;

                      //  int intDecimalPart = 0;
                      //  if (dgvRFQ.CurrentRow.Cells["Quantity"].Value != null && !string.IsNullOrEmpty(dgvRFQ.CurrentRow.Cells["Quantity"].Value.ToString()) && dgvRFQ.CurrentRow.Cells["Quantity"].Value.ToString().Contains("."))
                       //     intDecimalPart = dgvRFQ.CurrentRow.Cells["Quantity"].Value.ToString().Split('.')[1].Length;
                        int intUomScale = 0;
                        if (dgvRFQ[Uom.Index, dgvRFQ.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                            intUomScale = MobjClsBLLRFQ.FillCombos(new string[] { "Scale", "STUOMReference", "UOMID = " + dgvRFQ[Uom.Index, dgvRFQ.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                      //  if (intDecimalPart > intUomScale)
                      //  {
                            dgvRFQ.CurrentRow.Cells["Quantity"].Value = dgvRFQ.CurrentRow.Cells["Quantity"].Value.ToDecimal().ToString("F" + intUomScale);
                       // }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvRFQ_CellEndEdit() " + ex.Message);
                mObjLogs.WriteLog("Error in dgvRFQ_CellEndEdit() " + ex.Message, 2);
            }
        }

        private void BtnItem_Click(object sender, EventArgs e)
        {
            try
            {
                frmItemMaster objItemMaster = new frmItemMaster(false);
                objItemMaster.Text = "Product Master " + (ClsMainSettings.objFrmMain.MdiChildren.Length + 1);
                objItemMaster.MdiParent = ClsMainSettings.objFrmMain;
                objItemMaster.WindowState = FormWindowState.Maximized;
                objItemMaster.Show();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnItem_Click() " + ex.Message);
                mObjLogs.WriteLog("Error in BtnItem_Click() " + ex.Message, 2);
            }
        }

        private void BtnUom_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmUnitOfMeasurement objUoM = new FrmUnitOfMeasurement())
                {
                    objUoM.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnUom_Click() " + ex.Message);
                mObjLogs.WriteLog("Error in BtnUom_Click() " + ex.Message, 2);
            }
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                if (Convert.ToInt64(txtRFQNo.Tag) > 0)
                {
                    MobjClsBLLRFQ.PobjClsDTORFQ.RFQID = Convert.ToInt64(txtRFQNo.Tag);

                    if (MobjClsBLLRFQ.DeleteRFQ())
                    {
                        lblRFQStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        tmrFocus.Enabled = true;
                        AddNewRFQ();
                    }
                    else
                    {
                        lblRFQStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        tmrFocus.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BindingNavigatorDeleteItem_CLick() " + ex.Message);
                mObjLogs.WriteLog("Error in BindingNavigatorDeleteItem_Click() " + ex.Message, 2);
            }
        }

        private void dtpRFQDate_ValueChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void dtpDueDate_ValueChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "RFQ Information";
                    ObjEmailPopUp.EmailFormType = EmailFormID.RFQ;
                    ObjEmailPopUp.EmailSource = MobjClsBLLRFQ.GetRFQReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on Email:BtnEmail_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnEmail_Click " + Ex.Message.ToString());
            }
        }

        
        #endregion

        private void btnDeny_Click(object sender, EventArgs e)
        {
            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 942, out MmessageIcon).Replace("*", "Deny");

            if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                {
                    objFrmCancellationDetails.Text = "Suggestion Details";
                    objFrmCancellationDetails.lblCancelledDate.Text = "Suggested Date";
                    objFrmCancellationDetails.ShowDialog();

                    MobjClsBLLRFQ.PobjClsDTORFQ.ApprovedBy = ClsCommonSettings.UserID;
                    MobjClsBLLRFQ.PobjClsDTORFQ.ApprovedDate = ClsCommonSettings.GetServerDate();
                    MobjClsBLLRFQ.PobjClsDTORFQ.VerifcationDescription = objFrmCancellationDetails.PStrDescription;

                    if (!objFrmCancellationDetails.PBlnIsCanelled)
                    {
                        MobjClsBLLRFQ.PobjClsDTORFQ.StatusID = (Int32)RFQStatus.Denied;

                        if (MobjClsBLLRFQ.Suggest((int)OperationType.RFQ))
                        {
                            AddNewRFQ();
                        }
                        else
                        {
                        }
                    }
                }
            }
        }

        private void btnSuggest_Click(object sender, EventArgs e)
        {
            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 942, out MmessageIcon).Replace("*", "Suggest");

            if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                {
                    objFrmCancellationDetails.Text = "Suggestion Details";
                    objFrmCancellationDetails.lblCancelledDate.Text = "Suggested Date";
                    objFrmCancellationDetails.ShowDialog();
                    
                    MobjClsBLLRFQ.PobjClsDTORFQ.ApprovedBy = ClsCommonSettings.UserID;
                    MobjClsBLLRFQ.PobjClsDTORFQ.ApprovedDate = ClsCommonSettings.GetServerDate();
                    MobjClsBLLRFQ.PobjClsDTORFQ.VerifcationDescription = objFrmCancellationDetails.PStrDescription;

                    if (!objFrmCancellationDetails.PBlnIsCanelled)
                    {
                        MobjClsBLLRFQ.PobjClsDTORFQ.StatusID = (Int32)RFQStatus.Suggested;

                        if (MobjClsBLLRFQ.Suggest((int)OperationType.RFQ))
                        {
                            AddNewRFQ();
                        }
                        else
                        {
                        }
                    }
                }
            }
        }

        private DataTable GetVerificationHistoryStatus(bool blnAllUsers)
        {
            string strCondition = "";

                    strCondition += "VH.OperationTypeID = " + (int)OperationType.RFQ + " And VH.ReferenceID = " + MobjClsBLLRFQ.PobjClsDTORFQ.RFQID + " And VH.StatusID In (" + (int)RFQStatus.Suggested + "," + (int)RFQStatus.Denied + ")";
                    if (!blnAllUsers)
                        strCondition += " And VH.VerifiedBy = " + ClsCommonSettings.UserID;
            return MobjClsBLLRFQ.FillCombos(new string[] { "VH.StatusID,Convert(varchar(10),VH.VerifiedDate,103) as VerifiedDate,UM.UserID,UM.UserName,ST.Description as Status,VH.Remarks as Comment", "STVerificationHistory VH Inner Join UserMaster UM On UM.UserID = VH.VerifiedBy Inner Join STCommonStatusReference ST On ST.StatusID = VH.StatusID", strCondition });
        }

       
        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "RFQ";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void expandableSplitterLeft_ExpandedChanged(object sender, ExpandedChangeEventArgs e)
        {
            if (expandableSplitterLeft.Expanded)
            {
                if (dgvRFQDisplay.Columns.Count > 0)
                    dgvRFQDisplay.Columns[0].Visible = false;
            }
        }

        private void tcGeneral_SelectedTabChanged(object sender, TabStripTabChangedEventArgs e)
        {
            if (tcGeneral.SelectedTab == tiSuggestions)
            {
                DataTable datVerification = new DataTable();
                if (PintApprovalPermission == (int)ApprovePermission.Approve || PintApprovalPermission == (int)ApprovePermission.Both)
                    datVerification = GetVerificationHistoryStatus(true);
                else
                    datVerification = GetVerificationHistoryStatus(false);
                dgvSuggestions.Rows.Clear();
                for (int i = 0; i < datVerification.Rows.Count; i++)
                {
                    dgvSuggestions.Rows.Add();
                    dgvSuggestions[UserID.Index, i].Value = datVerification.Rows[i]["UserID"];
                    dgvSuggestions[UserName.Index, i].Value = datVerification.Rows[i]["UserName"];
                    dgvSuggestions[Status.Index, i].Value = datVerification.Rows[i]["Status"];
                    dgvSuggestions[VerifiedDate.Index, i].Value = datVerification.Rows[i]["VerifiedDate"];
                    dgvSuggestions[Comment.Index, i].Value = datVerification.Rows[i]["Comment"];
                    if (datVerification.Rows[i]["UserID"].ToInt32() != ClsCommonSettings.UserID)
                        dgvSuggestions[Comment.Index, i].ReadOnly = true;
                }
            }
        }

        private void dgvSuggestions_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == Comment.Index)
            {
                if (dgvSuggestions.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                    MobjClsBLLRFQ.UpdationVerificationTableRemarks(MobjClsBLLRFQ.PobjClsDTORFQ.RFQID, dgvSuggestions.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
            }
        }

        private void lnkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DataTable datAdvanceSearchedData = new DataTable();
            using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.RFQ))
            {
                objFrmSearchForm.ShowDialog();
                if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                {
                    datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                    datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                    dgvRFQDisplay.DataSource = datAdvanceSearchedData;
                    dgvRFQDisplay.Columns["ID"].Visible = false;
                    dgvRFQDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }
        

    }
}
