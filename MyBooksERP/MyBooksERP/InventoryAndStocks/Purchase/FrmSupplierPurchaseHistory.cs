﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmSupplierPurchaseHistory : Form
    {
        public int intCompanyID;
        public int intSupplierID;
        public int intItemID;

        clsBLLPurchase ObjClsBLLPurcahse;

        public FrmSupplierPurchaseHistory()
        {
            InitializeComponent();
            ObjClsBLLPurcahse = new clsBLLPurchase();
        }

        private void FrmSupplierPurchaseHistory_Load(object sender, EventArgs e)
        {
            this.dgvSupplierPurchaseHistory.DataSource = ObjClsBLLPurcahse.GetSupplierpurchaseHistory(intCompanyID, intSupplierID, intItemID);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
