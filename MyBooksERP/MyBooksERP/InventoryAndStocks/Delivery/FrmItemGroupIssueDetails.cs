﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace MyBooksERP
{
    public partial class FrmItemGroupIssueDetails : Form
    {
        public DataTable PDtItemGroupDetails { get; set; }
        private DataTable datMessages;
        private ClsNotificationNew MobjClsNotification;
        MessageBoxIcon mMessageIcon;
        private int MintItemGroupID;
        private int MintReferenceNo;
        private int MintReferenceSerialNo;
        private int MintWarehouseID;
        private int MintCompanyID;
        public bool MblnIsEditMode {get;set;}
        clsBLLItemIssueMaster MobjClsBllItemIssueMaster;
        public FrmItemGroupIssueDetails(int intItemGroupID,DataTable dtItemGroupDetails,int intReferenceNo,int intReferenceSerialNo,int intWarehouseID,int intCompanyID,bool blnIsEditMode)
        {
            InitializeComponent();
            MobjClsBllItemIssueMaster = new clsBLLItemIssueMaster();

            MintItemGroupID = intItemGroupID;
            PDtItemGroupDetails = dtItemGroupDetails;
            MintReferenceNo = intReferenceNo;
            MintReferenceSerialNo = intReferenceSerialNo;
            MintWarehouseID = intWarehouseID;
            MintCompanyID = intCompanyID;
            MblnIsEditMode = blnIsEditMode;
            
            dgvDetails.Rows.Clear();
            PDtItemGroupDetails.DefaultView.RowFilter = "ItemGroupID = " + intItemGroupID + " And ReferenceNo = " + intReferenceNo + " And ReferenceSerialNo = " + intReferenceSerialNo;

            if (PDtItemGroupDetails.DefaultView.ToTable().Rows.Count > 0)
            {
                for (int i = 0; i < PDtItemGroupDetails.DefaultView.ToTable().Rows.Count; i++)
                {
                    DataRow dr = PDtItemGroupDetails.DefaultView.ToTable().Rows[i];
                    dgvDetails.Rows.Add();
                    dgvDetails["ItemID", i].Value = dr["ItemID"];
                    dgvDetails["ItemCode", i].Value = dr["ItemCode"];
                    dgvDetails["ItemName", i].Value = dr["ItemName"];
                    FillComboColumns(Convert.ToInt32(dgvDetails["ItemID", i].Value), 1);
                    dgvDetails["Uom", i].Value = Convert.ToInt32(dr["UOMID"]);
                    dgvDetails["Uom", i].Tag = Convert.ToInt32(dr["UOMID"]);
                    dgvDetails["Uom", i].Value = dgvDetails["Uom", i].FormattedValue;

                    FillComboColumns(Convert.ToInt32(dgvDetails["ItemID", i].Value), 2);
                    dgvDetails["BatchNo", i].Value = Convert.ToInt64(dr["BatchID"]);
                    dgvDetails["BatchNo", i].Tag = Convert.ToInt64(dr["BatchID"]);
                    dgvDetails["BatchNo", i].Value = dgvDetails["BatchNo", i].FormattedValue;

                    dgvDetails["Quantity", i].Value = dr["Quantity"];
                }
            }
            else
            {
               
                DataTable dtItemDetails = MobjClsBllItemIssueMaster.GetItemGroupDetails(MintItemGroupID);


                for (int i = 0; i < dtItemDetails.Rows.Count; i++)
                {
                    dgvDetails.Rows.Add();
                    dgvDetails["ItemID", i].Value = dtItemDetails.Rows[i]["ItemID"];
                    dgvDetails["ItemCode", i].Value = dtItemDetails.Rows[i]["ItemCode"];
                    dgvDetails["ItemName", i].Value = dtItemDetails.Rows[i]["Description"];
                    FillComboColumns(Convert.ToInt32(dgvDetails["ItemID", i].Value), 1);
                    dgvDetails["Uom", i].Value = dtItemDetails.Rows[i]["UOMID"];
                    dgvDetails["Uom", i].Tag = dtItemDetails.Rows[i]["UOMID"];
                    dgvDetails["Uom", i].Value = dgvDetails["Uom", i].FormattedValue;
                    dgvDetails["Quantity", i].Value = dtItemDetails.Rows[i]["Quantity"];
                }
            }
        }
        private void LoadMessage()
        {
            try
            {
                //Loads error messages
                MobjClsNotification = new ClsNotificationNew();
                datMessages = new DataTable();
                datMessages = MobjClsNotification.FillMessageArray((int)FormID.ItemIssue, 4);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadMessage() " + ex.Message);
            }
        }
        private bool FillComboColumns(int intItemID,int intMode)
        {
           
            try
            {
                if (intMode == 0 || intMode == 1)
                {
                    DataTable dtItemUOMs = MobjClsBllItemIssueMaster.GetItemUoms(intItemID);
                    Uom.DataSource = null;
                    Uom.ValueMember = "UOMID";
                    Uom.DisplayMember = "Description";
                    Uom.DataSource = dtItemUOMs;
                }
                if (intMode == 0 || intMode == 2)
                {
                    DataTable datCombos = new DataTable();
                    BatchNo.DataSource = null;
                    if (!MblnIsEditMode)
                        datCombos = MobjClsBllItemIssueMaster.FillCombos(new string[] { "BD.BatchID,BD.BatchNo", "STBatchDetails BD Inner Join STStockDetails SD On SD.BatchID = BD.BatchID And SD.ItemID = BD.ItemID", "BD.ItemID = " + intItemID + " And SD.WarehouseID = " + MintWarehouseID + " And IsNull(SD.GRNQuantity-SD.SoldQuantity,0) > 0 And IsNull(BD.ExpiryDate,GetDate()) >= '" + ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy") + "'" });
                    else
                        datCombos = MobjClsBllItemIssueMaster.FillCombos(new string[] { "BD.BatchID,BD.BatchNo", "STBatchDetails BD Inner Join STStockDetails SD On SD.BatchID = BD.BatchID And SD.ItemID = BD.ItemID", "BD.ItemID = " + intItemID + " And SD.WarehouseID = " + MintWarehouseID + "" });
                    BatchNo.ValueMember = "BatchID";
                    BatchNo.DisplayMember = "BatchNo";
                    BatchNo.DataSource = datCombos;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FillComboColumn() " + ex.Message);
                //mObjLogs.WriteLog("Error in FillComboColumn() " + ex.Message, 2);
            }
            return true;
        }

        private void dgvDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == BatchNo.Index)
            {
                if (dgvDetails.CurrentRow.Cells["BatchNo"].Value != null)
                {
                    dgvDetails.CurrentRow.Cells["BatchNo"].Tag = dgvDetails.CurrentRow.Cells["BatchNo"].Value;
                    dgvDetails.CurrentRow.Cells["BatchNo"].Value = dgvDetails.CurrentRow.Cells["BatchNo"].FormattedValue;

                    decimal decStockQuantity = MobjClsBllItemIssueMaster.GetStockQuantity(Convert.ToInt32(dgvDetails.CurrentRow.Cells["ItemID"].Value), Convert.ToInt32(dgvDetails.CurrentRow.Cells["BatchNo"].Tag), MintCompanyID, MintWarehouseID);
                    DataTable dtUomDetails = new DataTable();
                    dtUomDetails = MobjClsBllItemIssueMaster.GetUomConversionValues(Convert.ToInt32(dgvDetails.CurrentRow.Cells["Uom"].Tag), Convert.ToInt32(dgvDetails.CurrentRow.Cells["ItemID"].Value));
                    if (dtUomDetails.Rows.Count > 0)
                    {
                        int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                        decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                        if (intConversionFactor == 1)
                            decStockQuantity = decStockQuantity * decConversionValue;
                        else if (intConversionFactor == 2)
                            decStockQuantity = decStockQuantity / decConversionValue;
                    }
                    dgvDetails.CurrentRow.Cells["QtyAvailable"].Value = decStockQuantity;
                }
            }
        }

        private bool ValidateFields()
        {
            
            for(int i=0;i<dgvDetails.Rows.Count;i++)
            {
                if (dgvDetails["ItemID", i].Value != null && dgvDetails["BatchNo", i].Tag == null)
                {
                    string MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4220, out mMessageIcon);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, mMessageIcon);
                    dgvDetails.CurrentCell = dgvDetails["BatchNo", i];
                    dgvDetails.Focus();
                    return false;
                }           
            }
            return true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (ValidateFields())
            {
                for (int i = 0; i < PDtItemGroupDetails.Rows.Count; i++)
                {
                    if (Convert.ToInt32(PDtItemGroupDetails.Rows[i]["ItemGroupID"]) == MintItemGroupID && Convert.ToInt32(PDtItemGroupDetails.Rows[i]["ReferenceNo"]) == MintReferenceNo && Convert.ToInt32(PDtItemGroupDetails.Rows[i]["ReferenceSerialNo"]) == MintReferenceSerialNo)
                    {
                        PDtItemGroupDetails.Rows[i].Delete();
                        PDtItemGroupDetails.AcceptChanges();
                        i--;
                    }
                }

                foreach (DataGridViewRow dr in dgvDetails.Rows)
                {
                    if (dr.Cells["ItemID"].Value != null)
                    {
                        DataRow drNewRow = PDtItemGroupDetails.NewRow();
                        drNewRow["ItemGroupID"] = MintItemGroupID;
                        drNewRow["ReferenceNo"] = MintReferenceNo;
                        drNewRow["ReferenceSerialNo"] = MintReferenceSerialNo;
                        drNewRow["ItemID"] = dr.Cells["ItemID"].Value;
                        drNewRow["ItemCode"] = dr.Cells["ItemCode"].Value;
                        drNewRow["ItemName"] = dr.Cells["ItemName"].Value;
                        drNewRow["BatchID"] = dr.Cells["BatchNo"].Tag;
                        drNewRow["UomID"] = dr.Cells["Uom"].Tag;
                        drNewRow["Quantity"] = dr.Cells["Quantity"].Value;
                        PDtItemGroupDetails.Rows.Add(drNewRow);
                    }
                }
                this.Close();
            }
        }

        private void dgvDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                int iRowIndex = 0;

                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    dgvDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    iRowIndex = dgvDetails.CurrentCell.RowIndex;

                    if (e.ColumnIndex != 0 && e.ColumnIndex != 1)
                    {
                        if (Convert.ToString(dgvDetails.CurrentRow.Cells[0].Value).Trim() == "")
                        {
                            e.Cancel = true;
                        }
                        else if (dgvDetails.Columns[e.ColumnIndex].Name == "Uom")
                        {
                            FillComboColumns(Convert.ToInt32(dgvDetails["ItemID", e.RowIndex].Value),1);
                        }
                        else if (dgvDetails.Columns[e.ColumnIndex].Name == "BatchNo")
                        {
                            FillComboColumns(Convert.ToInt32(dgvDetails["ItemID", e.RowIndex].Value),2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvItemDetailsGrid_CellBeginEdit() " + ex.Message);
              //  MobjClsLogWriter.WriteLog("Error in dgvItemDetailsGrid_CellBeginEdit() " + ex.Message, 2);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmItemGroupIssueDetails_Load(object sender, EventArgs e)
        {
            LoadMessage();
        }

        private void dgvDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == Uom.Index || e.ColumnIndex == BatchNo.Index)
            {
                if (e.RowIndex >=0)
                {
                    if (dgvDetails.Rows[e.RowIndex].Cells["Uom"].Tag != null && dgvDetails.Rows[e.RowIndex].Cells["BatchNo"].Tag != null)
                    {
                        decimal decStockQuantity = MobjClsBllItemIssueMaster.GetStockQuantity(Convert.ToInt32(dgvDetails.Rows[e.RowIndex].Cells["ItemID"].Value), Convert.ToInt32(dgvDetails.Rows[e.RowIndex].Cells["BatchNo"].Tag), MintCompanyID, MintWarehouseID);
                        DataTable dtUomDetails = new DataTable();
                        dtUomDetails = MobjClsBllItemIssueMaster.GetUomConversionValues(Convert.ToInt32(dgvDetails.Rows[e.RowIndex].Cells["Uom"].Tag), Convert.ToInt32(dgvDetails.Rows[e.RowIndex].Cells["ItemID"].Value));
                        if (dtUomDetails.Rows.Count > 0)
                        {
                            int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                            decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                            if (intConversionFactor == 1)
                                decStockQuantity = decStockQuantity * decConversionValue;
                            else if (intConversionFactor == 2)
                                decStockQuantity = decStockQuantity / decConversionValue;
                        }
                        dgvDetails.Rows[e.RowIndex].Cells["QtyAvailable"].Value = decStockQuantity;
                    }
                    else
                    {
                        dgvDetails.Rows[e.RowIndex].Cells["QtyAvailable"].Value = "0.000";
                    }
                }
            }
        }

        private void FrmItemGroupIssueDetails_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    
                }
            }
            catch (Exception)
            {
            }
        }

        private void dgvDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvDetails.IsCurrentCellDirty)
                {
                    if (dgvDetails.CurrentCell != null)
                    {
                        dgvDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    }
                }
            }
            catch (Exception)
            { }
        }
    }
}
