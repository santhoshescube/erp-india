using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data;
using System.Data.SqlClient;

using Microsoft.ReportingServices.ReportRendering;
using System.Collections.Generic;


/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,28 MAR 2011>
   Description:	<Description,,Item Issue Form>
================================================
*/
namespace MyBooksERP
{
    public class FrmDirectDelivery : DevComponents.DotNetBar.Office2007Form
    {

        #region Designer

        public bool blnDocumentChanged = false;
        public string strFileName = "";
        public string Type;
        public Command cmdCommandZoom;
        private DockContainerItem dockContainerItem1;
        private ExpandableSplitter expandableSplitterLeft;
        private PanelEx PanelLeft;
        private IContainer components;

        //------------------------------------------------------------------------------------------------------------------------

        //private bool MblnShowErrorMess;                  // Set To Display Error Message
        private MessageBoxIcon MmessageIcon;
        private DotNetBarManager dotNetBarManager1;
        private DockSite dockSite4;
        private DockSite dockSite1;
        private DockSite dockSite2;
        private DockSite dockSite3;
        private DockSite dockSite5;
        private DockSite dockSite6;
        private Bar bar1;
        private DockSite dockSite8;
        private PanelEx panelMain;
        private Bar PurchaseOrderBindingNavigator;
        private ButtonItem bnAddNewItem;
        private ExpandableSplitter expandableSplitterTop;
        private PanelEx panelTop;
        private ExpandablePanel expandablePanel1;
        private LabelX lblSCountStatus;
        private PanelEx panelLeftTop;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvIssueDisplay;
        private Label lblSCompany;
        private ButtonItem bnSaveItem;
        private ButtonItem bnClear;
        private ButtonItem bnDelete;
        private ButtonX btnRefresh;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSCompany;
        private System.Windows.Forms.DateTimePicker dtpIssuedDate;
        private Label lblIssuedDate;
        private Label lblIssueNo;
        internal Timer tmrItemIssue;
        internal ErrorProvider ErrItemIssue;
        private ButtonItem bnPrint;
        private ButtonItem bnEmail;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private DevComponents.DotNetBar.Controls.TextBoxX txtIssueNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSWarehouse;
        private Label lblSWarehouse;
        private Label lblIssuedBy;
        private Label lbl;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private DevComponents.DotNetBar.Controls.TextBoxX txtRemarks;
        private Label lblRemarks;
        private DevComponents.DotNetBar.Controls.WarningBox lblStatus;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private Label lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSIssuedBy;
        private Label lblSIssuedBy;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private PanelEx panelMiddle;
        private ClsInnerGridBar dgvItemDetailsGrid;
        private Label lblSIssueNo;
        private System.Windows.Forms.DateTimePicker dtpSTo;
        private System.Windows.Forms.DateTimePicker dtpSFrom;
        private Label lblTo;
        private Label lblFrom;
        private ButtonItem btnCustomerwisePrint;
        private DevComponents.DotNetBar.TabControl tcDeliveryNote;
        private TabControlPanel tabControlPanel1;
        private TabItem tiItemDetails;
        private TabControlPanel tabControlPanel2;
        private TabItem tiLocationDetails;
        private ClsInnerGridBar dgvLocationDetails;
        private ButtonItem btnPicklist;
        private ButtonItem btnPickListEmail;
        private DataGridViewTextBoxColumn LItemCode;
        private DataGridViewTextBoxColumn LItemName;
        private DataGridViewTextBoxColumn LItemID;
        private DataGridViewTextBoxColumn LBatchID;
        private DataGridViewTextBoxColumn LBatchNo;
        private DataGridViewTextBoxColumn LQuantity;
        private DataGridViewTextBoxColumn LAvailableQuantity;
        private DataGridViewComboBoxColumn LUOM;
        private DataGridViewTextBoxColumn LUOMID;
        private DataGridViewComboBoxColumn LLocation;
        private DataGridViewComboBoxColumn LRow;
        private DataGridViewComboBoxColumn LBlock;
        private DataGridViewComboBoxColumn LLot;
        private LinkLabel lnkLabelAdvanceSearch;
        private Label lblVendorAddress;
        private Label lblCustomer;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCustomer;
        public DevComponents.DotNetBar.Controls.TextBoxX txtVendorAddress;
        internal ContextMenuStrip CMSVendorAddress;
        private Label lblLpoNo;
        private System.Windows.Forms.TextBox txtLpoNo;
        private ButtonItem btnActions;
        private ButtonItem btnSalesOrder;
        private ButtonItem btnSalesInvoice;
        private PanelEx pnlBottom;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSubTotal;
        private Label lblSubTotal;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTotalAmount;
        private Label lblTotalAmount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtExchangeCurrencyRate;
        private Label lblCompanyCurrencyAmount;
        private Label lblAmtinWrds;
        private Label lblAmountinWords;
        private PanelEx panelEx3;
        private LabelX lblDirectDNstatus;
        private LabelX lblStatusName;
        private ContextMenuStrip CntxtHistory;
        private ToolStripMenuItem ShowItemHistory;
        private ToolStripMenuItem ShowSaleRate;
        private ToolStripMenuItem showGroupItemDetails;
        private DataGridViewTextBoxColumn ItemCode;
        private DataGridViewTextBoxColumn ItemName;
        private DataGridViewTextBoxColumn ItemID;
        private DataGridViewTextBoxColumn BatchNo;
        private DataGridViewTextBoxColumn ReferenceNo;
        private DataGridViewTextBoxColumn ReferenceSerialNo;
        private DataGridViewTextBoxColumn InvoicedQuantity;
        private DataGridViewTextBoxColumn DeliveredQty;
        private DataGridViewTextBoxColumn QtyAvailable;
        private DataGridViewTextBoxColumn Quantity;
        private DataGridViewTextBoxColumn Rate;
        private DataGridViewComboBoxColumn Uom;
        private DataGridViewTextBoxColumn NetAmount;
        private DataGridViewTextBoxColumn InvQty;
        private DataGridViewTextBoxColumn InvUOMID;
        private DataGridViewTextBoxColumn IsAutomatic;
        private DataGridViewCheckBoxColumn IsGroup;
        private DataGridViewTextBoxColumn BatchID;
        private DataGridViewTextBoxColumn IsGroupItem;
        private ButtonItem btnSampleIssue;
        private Label lblWarehouse;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDirectDelivery));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cmdCommandZoom = new DevComponents.DotNetBar.Command(this.components);
            this.dockContainerItem1 = new DevComponents.DotNetBar.DockContainerItem();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvIssueDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelLeftTop = new DevComponents.DotNetBar.PanelEx();
            this.lnkLabelAdvanceSearch = new System.Windows.Forms.LinkLabel();
            this.lblSIssueNo = new System.Windows.Forms.Label();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cboSIssuedBy = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSIssuedBy = new System.Windows.Forms.Label();
            this.cboSWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSWarehouse = new System.Windows.Forms.Label();
            this.btnRefresh = new DevComponents.DotNetBar.ButtonX();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboSCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSCompany = new System.Windows.Forms.Label();
            this.lblSCountStatus = new DevComponents.DotNetBar.LabelX();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.dotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.dockSite4 = new DevComponents.DotNetBar.DockSite();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.dockSite2 = new DevComponents.DotNetBar.DockSite();
            this.dockSite8 = new DevComponents.DotNetBar.DockSite();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.dockSite6 = new DevComponents.DotNetBar.DockSite();
            this.dockSite3 = new DevComponents.DotNetBar.DockSite();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.panelMain = new DevComponents.DotNetBar.PanelEx();
            this.panelMiddle = new DevComponents.DotNetBar.PanelEx();
            this.tcDeliveryNote = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.pnlBottom = new DevComponents.DotNetBar.PanelEx();
            this.panelEx3 = new DevComponents.DotNetBar.PanelEx();
            this.lblCompanyCurrencyAmount = new System.Windows.Forms.Label();
            this.txtTotalAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblSubTotal = new System.Windows.Forms.Label();
            this.txtSubTotal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtExchangeCurrencyRate = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblAmountinWords = new System.Windows.Forms.Label();
            this.lblAmtinWrds = new System.Windows.Forms.Label();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.dgvItemDetailsGrid = new ClsInnerGridBar();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReferenceNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReferenceSerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvoicedQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveredQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtyAvailable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Uom = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.NetAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvUOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsAutomatic = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsGroup = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsGroupItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiItemDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvLocationDetails = new ClsInnerGridBar();
            this.LItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LBatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LBatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LAvailableQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LUOM = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LUOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LLocation = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LRow = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LBlock = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LLot = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tiLocationDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.expandableSplitterTop = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.lblStatusName = new DevComponents.DotNetBar.LabelX();
            this.lblDirectDNstatus = new DevComponents.DotNetBar.LabelX();
            this.txtLpoNo = new System.Windows.Forms.TextBox();
            this.lblLpoNo = new System.Windows.Forms.Label();
            this.lblVendorAddress = new System.Windows.Forms.Label();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.cboCustomer = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtVendorAddress = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new System.Windows.Forms.Label();
            this.txtRemarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.lblIssuedBy = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.txtIssueNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.dtpIssuedDate = new System.Windows.Forms.DateTimePicker();
            this.lblIssuedDate = new System.Windows.Forms.Label();
            this.lblIssueNo = new System.Windows.Forms.Label();
            this.PurchaseOrderBindingNavigator = new DevComponents.DotNetBar.Bar();
            this.bnAddNewItem = new DevComponents.DotNetBar.ButtonItem();
            this.bnSaveItem = new DevComponents.DotNetBar.ButtonItem();
            this.bnClear = new DevComponents.DotNetBar.ButtonItem();
            this.bnDelete = new DevComponents.DotNetBar.ButtonItem();
            this.btnSampleIssue = new DevComponents.DotNetBar.ButtonItem();
            this.bnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.btnCustomerwisePrint = new DevComponents.DotNetBar.ButtonItem();
            this.btnPicklist = new DevComponents.DotNetBar.ButtonItem();
            this.bnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.btnPickListEmail = new DevComponents.DotNetBar.ButtonItem();
            this.btnActions = new DevComponents.DotNetBar.ButtonItem();
            this.btnSalesOrder = new DevComponents.DotNetBar.ButtonItem();
            this.btnSalesInvoice = new DevComponents.DotNetBar.ButtonItem();
            this.lblStatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.tmrItemIssue = new System.Windows.Forms.Timer(this.components);
            this.ErrItemIssue = new System.Windows.Forms.ErrorProvider(this.components);
            this.CMSVendorAddress = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CntxtHistory = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ShowItemHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowSaleRate = new System.Windows.Forms.ToolStripMenuItem();
            this.showGroupItemDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIssueDisplay)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.panelMain.SuspendLayout();
            this.panelMiddle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcDeliveryNote)).BeginInit();
            this.tcDeliveryNote.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.panelEx3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemDetailsGrid)).BeginInit();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocationDetails)).BeginInit();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrItemIssue)).BeginInit();
            this.CntxtHistory.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdCommandZoom
            // 
            this.cmdCommandZoom.Name = "cmdCommandZoom";
            // 
            // dockContainerItem1
            // 
            this.dockContainerItem1.Name = "dockContainerItem1";
            this.dockContainerItem1.Text = "dockContainerItem1";
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.dgvIssueDisplay);
            this.PanelLeft.Controls.Add(this.expandablePanel1);
            this.PanelLeft.Controls.Add(this.lblSCountStatus);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(200, 514);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 9;
            this.PanelLeft.Text = "panelEx1";
            // 
            // dgvIssueDisplay
            // 
            this.dgvIssueDisplay.AllowUserToAddRows = false;
            this.dgvIssueDisplay.AllowUserToDeleteRows = false;
            this.dgvIssueDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle51.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle51.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle51.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle51.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle51.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvIssueDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle51;
            this.dgvIssueDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle52.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle52.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle52.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle52.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle52.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvIssueDisplay.DefaultCellStyle = dataGridViewCellStyle52;
            this.dgvIssueDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvIssueDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvIssueDisplay.Location = new System.Drawing.Point(0, 225);
            this.dgvIssueDisplay.Name = "dgvIssueDisplay";
            this.dgvIssueDisplay.ReadOnly = true;
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle53.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle53.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle53.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle53.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle53.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvIssueDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle53;
            this.dgvIssueDisplay.RowHeadersVisible = false;
            this.dgvIssueDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvIssueDisplay.Size = new System.Drawing.Size(200, 263);
            this.dgvIssueDisplay.TabIndex = 13;
            this.dgvIssueDisplay.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvIssueDisplay_CellClick);
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expandablePanel1.Controls.Add(this.panelLeftTop);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(200, 225);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 100;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Search";
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelLeftTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelLeftTop.Controls.Add(this.lnkLabelAdvanceSearch);
            this.panelLeftTop.Controls.Add(this.lblSIssueNo);
            this.panelLeftTop.Controls.Add(this.dtpSTo);
            this.panelLeftTop.Controls.Add(this.dtpSFrom);
            this.panelLeftTop.Controls.Add(this.lblTo);
            this.panelLeftTop.Controls.Add(this.lblFrom);
            this.panelLeftTop.Controls.Add(this.cboSIssuedBy);
            this.panelLeftTop.Controls.Add(this.lblSIssuedBy);
            this.panelLeftTop.Controls.Add(this.cboSWarehouse);
            this.panelLeftTop.Controls.Add(this.lblSWarehouse);
            this.panelLeftTop.Controls.Add(this.btnRefresh);
            this.panelLeftTop.Controls.Add(this.txtSearch);
            this.panelLeftTop.Controls.Add(this.cboSCompany);
            this.panelLeftTop.Controls.Add(this.lblSCompany);
            this.panelLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftTop.Location = new System.Drawing.Point(0, 26);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(200, 199);
            this.panelLeftTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelLeftTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelLeftTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelLeftTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelLeftTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelLeftTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelLeftTop.Style.GradientAngle = 90;
            this.panelLeftTop.TabIndex = 101;
            // 
            // lnkLabelAdvanceSearch
            // 
            this.lnkLabelAdvanceSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lnkLabelAdvanceSearch.AutoSize = true;
            this.lnkLabelAdvanceSearch.Location = new System.Drawing.Point(3, 181);
            this.lnkLabelAdvanceSearch.Name = "lnkLabelAdvanceSearch";
            this.lnkLabelAdvanceSearch.Size = new System.Drawing.Size(87, 13);
            this.lnkLabelAdvanceSearch.TabIndex = 262;
            this.lnkLabelAdvanceSearch.TabStop = true;
            this.lnkLabelAdvanceSearch.Text = "Advance Search";
            this.lnkLabelAdvanceSearch.Click += new System.EventHandler(this.lnkLabelAdvanceSearch_Click);
            // 
            // lblSIssueNo
            // 
            this.lblSIssueNo.AutoSize = true;
            this.lblSIssueNo.Location = new System.Drawing.Point(3, 141);
            this.lblSIssueNo.Name = "lblSIssueNo";
            this.lblSIssueNo.Size = new System.Drawing.Size(49, 13);
            this.lblSIssueNo.TabIndex = 261;
            this.lblSIssueNo.Text = "Issue No";
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(77, 115);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.Size = new System.Drawing.Size(117, 20);
            this.dtpSTo.TabIndex = 260;
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(77, 89);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.Size = new System.Drawing.Size(117, 20);
            this.dtpSFrom.TabIndex = 259;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(3, 115);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 258;
            this.lblTo.Text = "To";
            this.lblTo.Click += new System.EventHandler(this.lblTo_Click);
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(3, 89);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 257;
            this.lblFrom.Text = "From";
            // 
            // cboSIssuedBy
            // 
            this.cboSIssuedBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSIssuedBy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSIssuedBy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSIssuedBy.DisplayMember = "Text";
            this.cboSIssuedBy.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSIssuedBy.DropDownHeight = 75;
            this.cboSIssuedBy.FormattingEnabled = true;
            this.cboSIssuedBy.IntegralHeight = false;
            this.cboSIssuedBy.ItemHeight = 14;
            this.cboSIssuedBy.Location = new System.Drawing.Point(78, 62);
            this.cboSIssuedBy.Name = "cboSIssuedBy";
            this.cboSIssuedBy.Size = new System.Drawing.Size(117, 20);
            this.cboSIssuedBy.TabIndex = 12;
            // 
            // lblSIssuedBy
            // 
            this.lblSIssuedBy.AutoSize = true;
            this.lblSIssuedBy.Location = new System.Drawing.Point(3, 62);
            this.lblSIssuedBy.Name = "lblSIssuedBy";
            this.lblSIssuedBy.Size = new System.Drawing.Size(53, 13);
            this.lblSIssuedBy.TabIndex = 128;
            this.lblSIssuedBy.Text = "Issued By";
            // 
            // cboSWarehouse
            // 
            this.cboSWarehouse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSWarehouse.DisplayMember = "Text";
            this.cboSWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSWarehouse.DropDownHeight = 75;
            this.cboSWarehouse.FormattingEnabled = true;
            this.cboSWarehouse.IntegralHeight = false;
            this.cboSWarehouse.ItemHeight = 14;
            this.cboSWarehouse.Location = new System.Drawing.Point(77, 36);
            this.cboSWarehouse.Name = "cboSWarehouse";
            this.cboSWarehouse.Size = new System.Drawing.Size(117, 20);
            this.cboSWarehouse.TabIndex = 11;
            // 
            // lblSWarehouse
            // 
            this.lblSWarehouse.AutoSize = true;
            this.lblSWarehouse.Location = new System.Drawing.Point(3, 40);
            this.lblSWarehouse.Name = "lblSWarehouse";
            this.lblSWarehouse.Size = new System.Drawing.Size(62, 13);
            this.lblSWarehouse.TabIndex = 125;
            this.lblSWarehouse.Text = "Warehouse";
            // 
            // btnRefresh
            // 
            this.btnRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnRefresh.Location = new System.Drawing.Point(124, 171);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(71, 23);
            this.btnRefresh.TabIndex = 13;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(77, 141);
            this.txtSearch.MaxLength = 20;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(117, 20);
            this.txtSearch.TabIndex = 8;
            this.txtSearch.WatermarkEnabled = false;
            this.txtSearch.WatermarkText = "Issue No";
            // 
            // cboSCompany
            // 
            this.cboSCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSCompany.DisplayMember = "Text";
            this.cboSCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSCompany.DropDownHeight = 75;
            this.cboSCompany.FormattingEnabled = true;
            this.cboSCompany.IntegralHeight = false;
            this.cboSCompany.ItemHeight = 14;
            this.cboSCompany.Location = new System.Drawing.Point(78, 10);
            this.cboSCompany.Name = "cboSCompany";
            this.cboSCompany.Size = new System.Drawing.Size(117, 20);
            this.cboSCompany.TabIndex = 9;
            this.cboSCompany.SelectedIndexChanged += new System.EventHandler(this.cboSCompany_SelectedIndexChanged);
            // 
            // lblSCompany
            // 
            this.lblSCompany.AutoSize = true;
            this.lblSCompany.Location = new System.Drawing.Point(3, 13);
            this.lblSCompany.Name = "lblSCompany";
            this.lblSCompany.Size = new System.Drawing.Size(51, 13);
            this.lblSCompany.TabIndex = 109;
            this.lblSCompany.Text = "Company";
            // 
            // lblSCountStatus
            // 
            // 
            // 
            // 
            this.lblSCountStatus.BackgroundStyle.Class = "";
            this.lblSCountStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSCountStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblSCountStatus.Location = new System.Drawing.Point(0, 488);
            this.lblSCountStatus.Name = "lblSCountStatus";
            this.lblSCountStatus.Size = new System.Drawing.Size(200, 26);
            this.lblSCountStatus.TabIndex = 102;
            this.lblSCountStatus.Text = "...";
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(179)))), ((int)(((byte)(219)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(200, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 514);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 10;
            this.expandableSplitterLeft.TabStop = false;
            this.expandableSplitterLeft.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitterLeft_ExpandedChanged);
            // 
            // dotNetBarManager1
            // 
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.dotNetBarManager1.BottomDockSite = this.dockSite4;
            this.dotNetBarManager1.EnableFullSizeDock = false;
            this.dotNetBarManager1.LeftDockSite = this.dockSite1;
            this.dotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.dotNetBarManager1.ParentForm = this;
            this.dotNetBarManager1.RightDockSite = this.dockSite2;
            this.dotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.dotNetBarManager1.ToolbarBottomDockSite = this.dockSite8;
            this.dotNetBarManager1.ToolbarLeftDockSite = this.dockSite5;
            this.dotNetBarManager1.ToolbarRightDockSite = this.dockSite6;
            this.dotNetBarManager1.TopDockSite = this.dockSite3;
            // 
            // dockSite4
            // 
            this.dockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite4.Location = new System.Drawing.Point(0, 514);
            this.dockSite4.Name = "dockSite4";
            this.dockSite4.Size = new System.Drawing.Size(1303, 0);
            this.dockSite4.TabIndex = 18;
            this.dockSite4.TabStop = false;
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(203, 0);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 514);
            this.dockSite1.TabIndex = 15;
            this.dockSite1.TabStop = false;
            // 
            // dockSite2
            // 
            this.dockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite2.Location = new System.Drawing.Point(1303, 0);
            this.dockSite2.Name = "dockSite2";
            this.dockSite2.Size = new System.Drawing.Size(0, 514);
            this.dockSite2.TabIndex = 16;
            this.dockSite2.TabStop = false;
            // 
            // dockSite8
            // 
            this.dockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite8.Location = new System.Drawing.Point(0, 514);
            this.dockSite8.Name = "dockSite8";
            this.dockSite8.Size = new System.Drawing.Size(1303, 0);
            this.dockSite8.TabIndex = 22;
            this.dockSite8.TabStop = false;
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(0, 0);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 514);
            this.dockSite5.TabIndex = 19;
            this.dockSite5.TabStop = false;
            // 
            // dockSite6
            // 
            this.dockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite6.Location = new System.Drawing.Point(1303, 0);
            this.dockSite6.Name = "dockSite6";
            this.dockSite6.Size = new System.Drawing.Size(0, 514);
            this.dockSite6.TabIndex = 20;
            this.dockSite6.TabStop = false;
            // 
            // dockSite3
            // 
            this.dockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite3.Location = new System.Drawing.Point(0, 0);
            this.dockSite3.Name = "dockSite3";
            this.dockSite3.Size = new System.Drawing.Size(1303, 0);
            this.dockSite3.TabIndex = 17;
            this.dockSite3.TabStop = false;
            // 
            // bar1
            // 
            this.bar1.AccessibleDescription = "DotNetBar Bar (bar1)";
            this.bar1.AccessibleName = "DotNetBar Bar";
            this.bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.bar1.Location = new System.Drawing.Point(0, 0);
            this.bar1.MenuBar = true;
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(36, 24);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.bar1.TabIndex = 0;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // panelMain
            // 
            this.panelMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelMain.Controls.Add(this.panelMiddle);
            this.panelMain.Controls.Add(this.expandableSplitterTop);
            this.panelMain.Controls.Add(this.panelTop);
            this.panelMain.Controls.Add(this.PurchaseOrderBindingNavigator);
            this.panelMain.Controls.Add(this.lblStatus);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(203, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1100, 514);
            this.panelMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelMain.Style.GradientAngle = 90;
            this.panelMain.TabIndex = 0;
            this.panelMain.Text = "panelMain";
            // 
            // panelMiddle
            // 
            this.panelMiddle.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelMiddle.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelMiddle.Controls.Add(this.tcDeliveryNote);
            this.panelMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMiddle.Location = new System.Drawing.Point(0, 146);
            this.panelMiddle.Name = "panelMiddle";
            this.panelMiddle.Size = new System.Drawing.Size(1100, 342);
            this.panelMiddle.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelMiddle.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelMiddle.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelMiddle.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelMiddle.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelMiddle.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelMiddle.Style.GradientAngle = 90;
            this.panelMiddle.TabIndex = 1;
            this.panelMiddle.Text = "panelMiddle";
            // 
            // tcDeliveryNote
            // 
            this.tcDeliveryNote.BackColor = System.Drawing.Color.Transparent;
            this.tcDeliveryNote.CanReorderTabs = true;
            this.tcDeliveryNote.Controls.Add(this.tabControlPanel1);
            this.tcDeliveryNote.Controls.Add(this.tabControlPanel2);
            this.tcDeliveryNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcDeliveryNote.Location = new System.Drawing.Point(0, 0);
            this.tcDeliveryNote.Name = "tcDeliveryNote";
            this.tcDeliveryNote.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcDeliveryNote.SelectedTabIndex = 0;
            this.tcDeliveryNote.Size = new System.Drawing.Size(1100, 342);
            this.tcDeliveryNote.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcDeliveryNote.TabIndex = 0;
            this.tcDeliveryNote.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcDeliveryNote.Tabs.Add(this.tiItemDetails);
            this.tcDeliveryNote.Tabs.Add(this.tiLocationDetails);
            this.tcDeliveryNote.Text = "tabControl1";
            this.tcDeliveryNote.SelectedTabChanged += new DevComponents.DotNetBar.TabStrip.SelectedTabChangedEventHandler(this.tcDeliveryNote_SelectedTabChanged);
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.pnlBottom);
            this.tabControlPanel1.Controls.Add(this.dgvItemDetailsGrid);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1100, 320);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(147)))), ((int)(((byte)(160)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 1;
            this.tabControlPanel1.TabItem = this.tiItemDetails;
            // 
            // pnlBottom
            // 
            this.pnlBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottom.Controls.Add(this.panelEx3);
            this.pnlBottom.Controls.Add(this.lblAmountinWords);
            this.pnlBottom.Controls.Add(this.lblAmtinWrds);
            this.pnlBottom.Controls.Add(this.lblTotalAmount);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(1, 230);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1098, 89);
            this.pnlBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottom.Style.GradientAngle = 90;
            this.pnlBottom.TabIndex = 1;
            // 
            // panelEx3
            // 
            this.panelEx3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.panelEx3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelEx3.CanvasColor = System.Drawing.Color.Transparent;
            this.panelEx3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx3.Controls.Add(this.lblCompanyCurrencyAmount);
            this.panelEx3.Controls.Add(this.txtTotalAmount);
            this.panelEx3.Controls.Add(this.lblSubTotal);
            this.panelEx3.Controls.Add(this.txtSubTotal);
            this.panelEx3.Controls.Add(this.txtExchangeCurrencyRate);
            this.panelEx3.Location = new System.Drawing.Point(601, 3);
            this.panelEx3.Name = "panelEx3";
            this.panelEx3.Size = new System.Drawing.Size(494, 83);
            this.panelEx3.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx3.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx3.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx3.Style.GradientAngle = 90;
            this.panelEx3.TabIndex = 311;
            // 
            // lblCompanyCurrencyAmount
            // 
            this.lblCompanyCurrencyAmount.AutoSize = true;
            this.lblCompanyCurrencyAmount.BackColor = System.Drawing.Color.White;
            this.lblCompanyCurrencyAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompanyCurrencyAmount.Location = new System.Drawing.Point(8, 32);
            this.lblCompanyCurrencyAmount.Name = "lblCompanyCurrencyAmount";
            this.lblCompanyCurrencyAmount.Size = new System.Drawing.Size(104, 9);
            this.lblCompanyCurrencyAmount.TabIndex = 307;
            this.lblCompanyCurrencyAmount.Text = "Amount in company currency";
            this.lblCompanyCurrencyAmount.Click += new System.EventHandler(this.lblCompanyCurrencyAmount_Click);
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTotalAmount.Border.Class = "TextBoxBorder";
            this.txtTotalAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(295, 27);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.Size = new System.Drawing.Size(187, 38);
            this.txtTotalAmount.TabIndex = 299;
            this.txtTotalAmount.TabStop = false;
            this.txtTotalAmount.Text = "0.00";
            this.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblSubTotal
            // 
            this.lblSubTotal.AutoSize = true;
            this.lblSubTotal.BackColor = System.Drawing.Color.White;
            this.lblSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubTotal.Location = new System.Drawing.Point(302, 9);
            this.lblSubTotal.Name = "lblSubTotal";
            this.lblSubTotal.Size = new System.Drawing.Size(37, 9);
            this.lblSubTotal.TabIndex = 298;
            this.lblSubTotal.Text = "Sub Total";
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtSubTotal.Border.Class = "TextBoxBorder";
            this.txtSubTotal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubTotal.Location = new System.Drawing.Point(295, 3);
            this.txtSubTotal.MaxLength = 20;
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.ReadOnly = true;
            this.txtSubTotal.Size = new System.Drawing.Size(185, 20);
            this.txtSubTotal.TabIndex = 292;
            this.txtSubTotal.TabStop = false;
            this.txtSubTotal.Text = "0.00";
            this.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtExchangeCurrencyRate
            // 
            this.txtExchangeCurrencyRate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtExchangeCurrencyRate.Border.Class = "TextBoxBorder";
            this.txtExchangeCurrencyRate.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtExchangeCurrencyRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExchangeCurrencyRate.Location = new System.Drawing.Point(0, 27);
            this.txtExchangeCurrencyRate.Name = "txtExchangeCurrencyRate";
            this.txtExchangeCurrencyRate.ReadOnly = true;
            this.txtExchangeCurrencyRate.Size = new System.Drawing.Size(286, 38);
            this.txtExchangeCurrencyRate.TabIndex = 306;
            this.txtExchangeCurrencyRate.TabStop = false;
            this.txtExchangeCurrencyRate.Text = "0.00";
            this.txtExchangeCurrencyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblAmountinWords
            // 
            this.lblAmountinWords.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountinWords.Location = new System.Drawing.Point(109, 62);
            this.lblAmountinWords.Name = "lblAmountinWords";
            this.lblAmountinWords.Size = new System.Drawing.Size(407, 14);
            this.lblAmountinWords.TabIndex = 309;
            // 
            // lblAmtinWrds
            // 
            this.lblAmtinWrds.AutoSize = true;
            this.lblAmtinWrds.Location = new System.Drawing.Point(15, 59);
            this.lblAmtinWrds.Name = "lblAmtinWrds";
            this.lblAmtinWrds.Size = new System.Drawing.Size(88, 13);
            this.lblAmtinWrds.TabIndex = 308;
            this.lblAmtinWrds.Text = "Amount in Words";
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.BackColor = System.Drawing.Color.White;
            this.lblTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(110, 62);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(45, 9);
            this.lblTotalAmount.TabIndex = 305;
            this.lblTotalAmount.Text = "Net Amount";
            // 
            // dgvItemDetailsGrid
            // 
            this.dgvItemDetailsGrid.AddNewRow = false;
            this.dgvItemDetailsGrid.AlphaNumericCols = new int[0];
            this.dgvItemDetailsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvItemDetailsGrid.BackgroundColor = System.Drawing.Color.White;
            this.dgvItemDetailsGrid.CapsLockCols = new int[0];
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle54.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle54.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle54.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle54.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle54.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItemDetailsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle54;
            this.dgvItemDetailsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvItemDetailsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.ItemID,
            this.BatchNo,
            this.ReferenceNo,
            this.ReferenceSerialNo,
            this.InvoicedQuantity,
            this.DeliveredQty,
            this.QtyAvailable,
            this.Quantity,
            this.Rate,
            this.Uom,
            this.NetAmount,
            this.InvQty,
            this.InvUOMID,
            this.IsAutomatic,
            this.IsGroup,
            this.BatchID,
            this.IsGroupItem});
            this.dgvItemDetailsGrid.DecimalCols = new int[] {
        10,
        12};
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle58.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle58.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle58.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle58.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle58.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvItemDetailsGrid.DefaultCellStyle = dataGridViewCellStyle58;
            this.dgvItemDetailsGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvItemDetailsGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvItemDetailsGrid.HasSlNo = false;
            this.dgvItemDetailsGrid.LastRowIndex = 0;
            this.dgvItemDetailsGrid.Location = new System.Drawing.Point(1, 1);
            this.dgvItemDetailsGrid.Name = "dgvItemDetailsGrid";
            this.dgvItemDetailsGrid.NegativeValueCols = new int[0];
            this.dgvItemDetailsGrid.NumericCols = new int[0];
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle59.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle59.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle59.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle59.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle59.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItemDetailsGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle59;
            this.dgvItemDetailsGrid.RowHeadersWidth = 50;
            this.dgvItemDetailsGrid.Size = new System.Drawing.Size(1098, 230);
            this.dgvItemDetailsGrid.TabIndex = 0;
            this.dgvItemDetailsGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItemDetailsGrid_CellValueChanged);
            this.dgvItemDetailsGrid.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvItemDetailsGrid_UserDeletingRow);
            this.dgvItemDetailsGrid.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvItemDetailsGrid_CellMouseClick);
            this.dgvItemDetailsGrid.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvItemDetailsGrid_CellBeginEdit);
            this.dgvItemDetailsGrid.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvItemDetailsGrid_RowsAdded);
            this.dgvItemDetailsGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItemDetailsGrid_CellEndEdit);
            this.dgvItemDetailsGrid.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvItemDetailsGrid_Textbox_TextChanged);
            this.dgvItemDetailsGrid.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvItemDetailsGrid_EditingControlShowing);
            this.dgvItemDetailsGrid.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvItemDetailsGrid_CurrentCellDirtyStateChanged);
            this.dgvItemDetailsGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvItemDetailsGrid_DataError);
            this.dgvItemDetailsGrid.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItemDetailsGrid_CellEnter);
            this.dgvItemDetailsGrid.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvItemDetailsGrid_RowsRemoved);
            this.dgvItemDetailsGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItemDetailsGrid_CellContentClick);
            // 
            // ItemCode
            // 
            this.ItemCode.HeaderText = "Item Code";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ItemName
            // 
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.MinimumWidth = 200;
            this.ItemName.Name = "ItemName";
            this.ItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemName.Width = 400;
            // 
            // ItemID
            // 
            this.ItemID.HeaderText = "ItemID";
            this.ItemID.Name = "ItemID";
            this.ItemID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemID.Visible = false;
            // 
            // BatchNo
            // 
            this.BatchNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BatchNo.HeaderText = "BatchNo";
            this.BatchNo.Name = "BatchNo";
            this.BatchNo.ReadOnly = true;
            this.BatchNo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BatchNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BatchNo.Visible = false;
            // 
            // ReferenceNo
            // 
            this.ReferenceNo.HeaderText = "ReferenceNo";
            this.ReferenceNo.Name = "ReferenceNo";
            this.ReferenceNo.ReadOnly = true;
            this.ReferenceNo.Visible = false;
            // 
            // ReferenceSerialNo
            // 
            this.ReferenceSerialNo.HeaderText = "ReferenceSerialNo";
            this.ReferenceSerialNo.Name = "ReferenceSerialNo";
            this.ReferenceSerialNo.ReadOnly = true;
            this.ReferenceSerialNo.Visible = false;
            // 
            // InvoicedQuantity
            // 
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.InvoicedQuantity.DefaultCellStyle = dataGridViewCellStyle55;
            this.InvoicedQuantity.HeaderText = "InvoicedQuantity";
            this.InvoicedQuantity.Name = "InvoicedQuantity";
            this.InvoicedQuantity.ReadOnly = true;
            this.InvoicedQuantity.Visible = false;
            // 
            // DeliveredQty
            // 
            this.DeliveredQty.HeaderText = "DeliveredQty";
            this.DeliveredQty.Name = "DeliveredQty";
            this.DeliveredQty.ReadOnly = true;
            this.DeliveredQty.Visible = false;
            // 
            // QtyAvailable
            // 
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.QtyAvailable.DefaultCellStyle = dataGridViewCellStyle56;
            this.QtyAvailable.HeaderText = "QtyAvailable";
            this.QtyAvailable.Name = "QtyAvailable";
            this.QtyAvailable.ReadOnly = true;
            // 
            // Quantity
            // 
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle57;
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.MaxInputLength = 10;
            this.Quantity.Name = "Quantity";
            this.Quantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Rate
            // 
            this.Rate.HeaderText = "Rate";
            this.Rate.Name = "Rate";
            // 
            // Uom
            // 
            this.Uom.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Uom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Uom.HeaderText = "UOM";
            this.Uom.Name = "Uom";
            this.Uom.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // NetAmount
            // 
            this.NetAmount.HeaderText = "NetAmount";
            this.NetAmount.Name = "NetAmount";
            // 
            // InvQty
            // 
            this.InvQty.HeaderText = "InvQty";
            this.InvQty.Name = "InvQty";
            this.InvQty.Visible = false;
            // 
            // InvUOMID
            // 
            this.InvUOMID.HeaderText = "InvUOMID";
            this.InvUOMID.Name = "InvUOMID";
            this.InvUOMID.Visible = false;
            // 
            // IsAutomatic
            // 
            this.IsAutomatic.HeaderText = "IsAutomatic";
            this.IsAutomatic.Name = "IsAutomatic";
            this.IsAutomatic.Visible = false;
            // 
            // IsGroup
            // 
            this.IsGroup.HeaderText = "IsGroup";
            this.IsGroup.Name = "IsGroup";
            this.IsGroup.Visible = false;
            // 
            // BatchID
            // 
            this.BatchID.HeaderText = "BatchID";
            this.BatchID.Name = "BatchID";
            this.BatchID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BatchID.Visible = false;
            // 
            // IsGroupItem
            // 
            this.IsGroupItem.HeaderText = "IsGroupItem";
            this.IsGroupItem.Name = "IsGroupItem";
            this.IsGroupItem.Visible = false;
            // 
            // tiItemDetails
            // 
            this.tiItemDetails.AttachedControl = this.tabControlPanel1;
            this.tiItemDetails.Name = "tiItemDetails";
            this.tiItemDetails.Text = "Item Details";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.dgvLocationDetails);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(1100, 320);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(147)))), ((int)(((byte)(160)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            this.tabControlPanel2.TabItem = this.tiLocationDetails;
            // 
            // dgvLocationDetails
            // 
            this.dgvLocationDetails.AddNewRow = false;
            this.dgvLocationDetails.AlphaNumericCols = new int[0];
            this.dgvLocationDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvLocationDetails.CapsLockCols = new int[0];
            this.dgvLocationDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocationDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LItemCode,
            this.LItemName,
            this.LItemID,
            this.LBatchID,
            this.LBatchNo,
            this.LQuantity,
            this.LAvailableQuantity,
            this.LUOM,
            this.LUOMID,
            this.LLocation,
            this.LRow,
            this.LBlock,
            this.LLot});
            this.dgvLocationDetails.DecimalCols = new int[0];
            dataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle61.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle61.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle61.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle61.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle61.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle61.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLocationDetails.DefaultCellStyle = dataGridViewCellStyle61;
            this.dgvLocationDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLocationDetails.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvLocationDetails.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvLocationDetails.HasSlNo = false;
            this.dgvLocationDetails.LastRowIndex = 0;
            this.dgvLocationDetails.Location = new System.Drawing.Point(1, 1);
            this.dgvLocationDetails.Name = "dgvLocationDetails";
            this.dgvLocationDetails.NegativeValueCols = new int[0];
            this.dgvLocationDetails.NumericCols = new int[0];
            this.dgvLocationDetails.Size = new System.Drawing.Size(1098, 318);
            this.dgvLocationDetails.TabIndex = 1;
            this.dgvLocationDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLocationDetails_CellValueChanged);
            this.dgvLocationDetails.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvLocationDetails_UserDeletingRow);
            this.dgvLocationDetails.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvLocationDetails_CellBeginEdit);
            this.dgvLocationDetails.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvLocationDetails_UserDeletedRow);
            this.dgvLocationDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLocationDetails_CellEndEdit);
            this.dgvLocationDetails.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvLocationDetails_Textbox_TextChanged);
            this.dgvLocationDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvLocationDetails_EditingControlShowing);
            this.dgvLocationDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvLocationDetails_CurrentCellDirtyStateChanged);
            this.dgvLocationDetails.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLocationDetails_CellEnter);
            // 
            // LItemCode
            // 
            this.LItemCode.HeaderText = "Item Code";
            this.LItemCode.Name = "LItemCode";
            this.LItemCode.Width = 60;
            // 
            // LItemName
            // 
            this.LItemName.HeaderText = "Item Name";
            this.LItemName.Name = "LItemName";
            this.LItemName.Width = 250;
            // 
            // LItemID
            // 
            this.LItemID.HeaderText = "ItemID";
            this.LItemID.Name = "LItemID";
            this.LItemID.Visible = false;
            // 
            // LBatchID
            // 
            this.LBatchID.HeaderText = "BatchID";
            this.LBatchID.Name = "LBatchID";
            this.LBatchID.Visible = false;
            // 
            // LBatchNo
            // 
            this.LBatchNo.HeaderText = "Batch No";
            this.LBatchNo.Name = "LBatchNo";
            this.LBatchNo.ReadOnly = true;
            this.LBatchNo.Width = 130;
            // 
            // LQuantity
            // 
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.LQuantity.DefaultCellStyle = dataGridViewCellStyle60;
            this.LQuantity.HeaderText = "Quantity";
            this.LQuantity.Name = "LQuantity";
            this.LQuantity.Width = 75;
            // 
            // LAvailableQuantity
            // 
            this.LAvailableQuantity.HeaderText = "Qty Available";
            this.LAvailableQuantity.Name = "LAvailableQuantity";
            this.LAvailableQuantity.ReadOnly = true;
            this.LAvailableQuantity.Width = 75;
            // 
            // LUOM
            // 
            this.LUOM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LUOM.HeaderText = "UOM";
            this.LUOM.Name = "LUOM";
            this.LUOM.ReadOnly = true;
            this.LUOM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LUOM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.LUOM.Width = 60;
            // 
            // LUOMID
            // 
            this.LUOMID.HeaderText = "UOMID";
            this.LUOMID.Name = "LUOMID";
            this.LUOMID.Visible = false;
            // 
            // LLocation
            // 
            this.LLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LLocation.HeaderText = "Location";
            this.LLocation.Name = "LLocation";
            // 
            // LRow
            // 
            this.LRow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LRow.HeaderText = "Row";
            this.LRow.Name = "LRow";
            // 
            // LBlock
            // 
            this.LBlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LBlock.HeaderText = "Block";
            this.LBlock.Name = "LBlock";
            // 
            // LLot
            // 
            this.LLot.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LLot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LLot.HeaderText = "Lot";
            this.LLot.Name = "LLot";
            // 
            // tiLocationDetails
            // 
            this.tiLocationDetails.AttachedControl = this.tabControlPanel2;
            this.tiLocationDetails.Name = "tiLocationDetails";
            this.tiLocationDetails.Text = "Location Details";
            // 
            // expandableSplitterTop
            // 
            this.expandableSplitterTop.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterTop.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitterTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitterTop.ExpandableControl = this.panelTop;
            this.expandableSplitterTop.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.expandableSplitterTop.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(179)))), ((int)(((byte)(219)))));
            this.expandableSplitterTop.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterTop.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterTop.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.Location = new System.Drawing.Point(0, 143);
            this.expandableSplitterTop.Name = "expandableSplitterTop";
            this.expandableSplitterTop.Size = new System.Drawing.Size(1100, 3);
            this.expandableSplitterTop.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterTop.TabIndex = 1;
            this.expandableSplitterTop.TabStop = false;
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelTop.Controls.Add(this.lblStatusName);
            this.panelTop.Controls.Add(this.lblDirectDNstatus);
            this.panelTop.Controls.Add(this.txtLpoNo);
            this.panelTop.Controls.Add(this.lblLpoNo);
            this.panelTop.Controls.Add(this.lblVendorAddress);
            this.panelTop.Controls.Add(this.lblCustomer);
            this.panelTop.Controls.Add(this.cboCustomer);
            this.panelTop.Controls.Add(this.txtVendorAddress);
            this.panelTop.Controls.Add(this.cboWarehouse);
            this.panelTop.Controls.Add(this.lblWarehouse);
            this.panelTop.Controls.Add(this.cboCompany);
            this.panelTop.Controls.Add(this.lblCompany);
            this.panelTop.Controls.Add(this.txtRemarks);
            this.panelTop.Controls.Add(this.lblRemarks);
            this.panelTop.Controls.Add(this.lblIssuedBy);
            this.panelTop.Controls.Add(this.lbl);
            this.panelTop.Controls.Add(this.txtIssueNo);
            this.panelTop.Controls.Add(this.dtpIssuedDate);
            this.panelTop.Controls.Add(this.lblIssuedDate);
            this.panelTop.Controls.Add(this.lblIssueNo);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1100, 118);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 0;
            // 
            // lblStatusName
            // 
            // 
            // 
            // 
            this.lblStatusName.BackgroundStyle.Class = "";
            this.lblStatusName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblStatusName.Location = new System.Drawing.Point(734, 7);
            this.lblStatusName.Name = "lblStatusName";
            this.lblStatusName.Size = new System.Drawing.Size(46, 23);
            this.lblStatusName.TabIndex = 254;
            this.lblStatusName.Text = "Status";
            // 
            // lblDirectDNstatus
            // 
            // 
            // 
            // 
            this.lblDirectDNstatus.BackgroundStyle.Class = "";
            this.lblDirectDNstatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDirectDNstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDirectDNstatus.Location = new System.Drawing.Point(786, 6);
            this.lblDirectDNstatus.Name = "lblDirectDNstatus";
            this.lblDirectDNstatus.Size = new System.Drawing.Size(302, 23);
            this.lblDirectDNstatus.TabIndex = 253;
            // 
            // txtLpoNo
            // 
            this.txtLpoNo.Location = new System.Drawing.Point(611, 83);
            this.txtLpoNo.MaxLength = 15;
            this.txtLpoNo.Name = "txtLpoNo";
            this.txtLpoNo.Size = new System.Drawing.Size(105, 20);
            this.txtLpoNo.TabIndex = 252;
            this.txtLpoNo.TextChanged += new System.EventHandler(this.txtLpoNo_TextChanged);
            // 
            // lblLpoNo
            // 
            this.lblLpoNo.AutoSize = true;
            this.lblLpoNo.Location = new System.Drawing.Point(520, 87);
            this.lblLpoNo.Name = "lblLpoNo";
            this.lblLpoNo.Size = new System.Drawing.Size(48, 13);
            this.lblLpoNo.TabIndex = 251;
            this.lblLpoNo.Text = "LPO No.";
            // 
            // lblVendorAddress
            // 
            this.lblVendorAddress.AutoSize = true;
            this.lblVendorAddress.BackColor = System.Drawing.Color.Transparent;
            this.lblVendorAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblVendorAddress.Location = new System.Drawing.Point(249, 34);
            this.lblVendorAddress.Name = "lblVendorAddress";
            this.lblVendorAddress.Size = new System.Drawing.Size(45, 13);
            this.lblVendorAddress.TabIndex = 222;
            this.lblVendorAddress.Text = "Address";
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.BackColor = System.Drawing.Color.Transparent;
            this.lblCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomer.Location = new System.Drawing.Point(249, 9);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(51, 13);
            this.lblCustomer.TabIndex = 221;
            this.lblCustomer.Text = "Customer";
            // 
            // cboCustomer
            // 
            this.cboCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCustomer.DisplayMember = "Text";
            this.cboCustomer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCustomer.DropDownHeight = 75;
            this.cboCustomer.FormattingEnabled = true;
            this.cboCustomer.IntegralHeight = false;
            this.cboCustomer.ItemHeight = 14;
            this.cboCustomer.Location = new System.Drawing.Point(306, 9);
            this.cboCustomer.Name = "cboCustomer";
            this.cboCustomer.Size = new System.Drawing.Size(171, 20);
            this.cboCustomer.TabIndex = 219;
            this.cboCustomer.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCustomer.WatermarkText = "Select Customer";
            this.cboCustomer.SelectedIndexChanged += new System.EventHandler(this.cboCustomer_SelectedIndexChanged);
            // 
            // txtVendorAddress
            // 
            // 
            // 
            // 
            this.txtVendorAddress.Border.Class = "TextBoxBorder";
            this.txtVendorAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtVendorAddress.Location = new System.Drawing.Point(306, 39);
            this.txtVendorAddress.MaxLength = 200;
            this.txtVendorAddress.Multiline = true;
            this.txtVendorAddress.Name = "txtVendorAddress";
            this.txtVendorAddress.ReadOnly = true;
            this.txtVendorAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtVendorAddress.Size = new System.Drawing.Size(171, 70);
            this.txtVendorAddress.TabIndex = 220;
            this.txtVendorAddress.TabStop = false;
            // 
            // cboWarehouse
            // 
            this.cboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWarehouse.DisplayMember = "Text";
            this.cboWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWarehouse.DropDownHeight = 75;
            this.cboWarehouse.FormattingEnabled = true;
            this.cboWarehouse.IntegralHeight = false;
            this.cboWarehouse.ItemHeight = 14;
            this.cboWarehouse.Location = new System.Drawing.Point(74, 34);
            this.cboWarehouse.Name = "cboWarehouse";
            this.cboWarehouse.Size = new System.Drawing.Size(158, 20);
            this.cboWarehouse.TabIndex = 1;
            this.cboWarehouse.SelectedIndexChanged += new System.EventHandler(this.cboWarehouse_SelectedIndexChanged);
            this.cboWarehouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(6, 34);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(62, 13);
            this.lblWarehouse.TabIndex = 218;
            this.lblWarehouse.Text = "Warehouse";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 75;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(74, 9);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(158, 20);
            this.cboCompany.TabIndex = 0;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Location = new System.Drawing.Point(6, 11);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 216;
            this.lblCompany.Text = "Company";
            // 
            // txtRemarks
            // 
            // 
            // 
            // 
            this.txtRemarks.Border.Class = "TextBoxBorder";
            this.txtRemarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtRemarks.Border.WordWrap = true;
            this.txtRemarks.Location = new System.Drawing.Point(786, 45);
            this.txtRemarks.MaxLength = 1000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(311, 64);
            this.txtRemarks.TabIndex = 6;
            this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(731, 47);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 214;
            this.lblRemarks.Text = "Remarks";
            // 
            // lblIssuedBy
            // 
            this.lblIssuedBy.AutoSize = true;
            this.lblIssuedBy.Location = new System.Drawing.Point(609, 35);
            this.lblIssuedBy.Name = "lblIssuedBy";
            this.lblIssuedBy.Size = new System.Drawing.Size(53, 13);
            this.lblIssuedBy.TabIndex = 5;
            this.lblIssuedBy.Text = "Issued By";
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(520, 35);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(53, 13);
            this.lbl.TabIndex = 211;
            this.lbl.Text = "Issued By";
            // 
            // txtIssueNo
            // 
            this.txtIssueNo.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtIssueNo.Border.Class = "TextBoxBorder";
            this.txtIssueNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtIssueNo.Location = new System.Drawing.Point(611, 6);
            this.txtIssueNo.MaxLength = 20;
            this.txtIssueNo.Name = "txtIssueNo";
            this.txtIssueNo.Size = new System.Drawing.Size(105, 20);
            this.txtIssueNo.TabIndex = 4;
            this.txtIssueNo.TabStop = false;
            this.txtIssueNo.TextChanged += new System.EventHandler(this.txtIssueNo_TextChanged);
            // 
            // dtpIssuedDate
            // 
            this.dtpIssuedDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpIssuedDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpIssuedDate.Location = new System.Drawing.Point(611, 57);
            this.dtpIssuedDate.Name = "dtpIssuedDate";
            this.dtpIssuedDate.Size = new System.Drawing.Size(105, 20);
            this.dtpIssuedDate.TabIndex = 5;
            this.dtpIssuedDate.ValueChanged += new System.EventHandler(this.dtpIssuedDate_ValueChanged);
            // 
            // lblIssuedDate
            // 
            this.lblIssuedDate.AutoSize = true;
            this.lblIssuedDate.Location = new System.Drawing.Point(520, 61);
            this.lblIssuedDate.Name = "lblIssuedDate";
            this.lblIssuedDate.Size = new System.Drawing.Size(64, 13);
            this.lblIssuedDate.TabIndex = 188;
            this.lblIssuedDate.Text = "Issued Date";
            // 
            // lblIssueNo
            // 
            this.lblIssueNo.AutoSize = true;
            this.lblIssueNo.Location = new System.Drawing.Point(520, 10);
            this.lblIssueNo.Name = "lblIssueNo";
            this.lblIssueNo.Size = new System.Drawing.Size(49, 13);
            this.lblIssueNo.TabIndex = 185;
            this.lblIssueNo.Text = "Issue No";
            // 
            // PurchaseOrderBindingNavigator
            // 
            this.PurchaseOrderBindingNavigator.AccessibleDescription = "DotNetBar Bar (PurchaseOrderBindingNavigator)";
            this.PurchaseOrderBindingNavigator.AccessibleName = "DotNetBar Bar";
            this.PurchaseOrderBindingNavigator.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.PurchaseOrderBindingNavigator.Dock = System.Windows.Forms.DockStyle.Top;
            this.PurchaseOrderBindingNavigator.DockLine = 1;
            this.PurchaseOrderBindingNavigator.DockOffset = 73;
            this.PurchaseOrderBindingNavigator.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.PurchaseOrderBindingNavigator.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.PurchaseOrderBindingNavigator.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnClear,
            this.bnDelete,
            this.btnSampleIssue,
            this.bnPrint,
            this.btnCustomerwisePrint,
            this.btnPicklist,
            this.bnEmail,
            this.btnPickListEmail,
            this.btnActions});
            this.PurchaseOrderBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.PurchaseOrderBindingNavigator.Name = "PurchaseOrderBindingNavigator";
            this.PurchaseOrderBindingNavigator.Size = new System.Drawing.Size(1100, 25);
            this.PurchaseOrderBindingNavigator.Stretch = true;
            this.PurchaseOrderBindingNavigator.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PurchaseOrderBindingNavigator.TabIndex = 12;
            this.PurchaseOrderBindingNavigator.TabStop = false;
            this.PurchaseOrderBindingNavigator.Text = "bar2";
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlEnter);
            this.bnAddNewItem.Text = "Add";
            this.bnAddNewItem.Tooltip = "Add New Information";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Text = "Save";
            this.bnSaveItem.Tooltip = "Save";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnClear
            // 
            this.bnClear.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnClear.Image = ((System.Drawing.Image)(resources.GetObject("bnClear.Image")));
            this.bnClear.Name = "bnClear";
            this.bnClear.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.bnClear.Text = "Clear";
            this.bnClear.Tooltip = "Clear";
            this.bnClear.Visible = false;
            this.bnClear.Click += new System.EventHandler(this.bnClear_Click);
            // 
            // bnDelete
            // 
            this.bnDelete.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnDelete.Image = ((System.Drawing.Image)(resources.GetObject("bnDelete.Image")));
            this.bnDelete.Name = "bnDelete";
            this.bnDelete.Text = "Delete";
            this.bnDelete.Tooltip = "Delete";
            this.bnDelete.Click += new System.EventHandler(this.bnDelete_Click);
            // 
            // btnSampleIssue
            // 
            this.btnSampleIssue.BeginGroup = true;
            this.btnSampleIssue.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSampleIssue.Image = global::MyBooksERP.Properties.Resources.DespatchSale;
            this.btnSampleIssue.Name = "btnSampleIssue";
            this.btnSampleIssue.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.btnSampleIssue.Text = "Sample Issue";
            this.btnSampleIssue.Click += new System.EventHandler(this.btnSampleIssue_Click);
            // 
            // bnPrint
            // 
            this.bnPrint.BeginGroup = true;
            this.bnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnPrint.Image = ((System.Drawing.Image)(resources.GetObject("bnPrint.Image")));
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.bnPrint.Text = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // btnCustomerwisePrint
            // 
            this.btnCustomerwisePrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnCustomerwisePrint.Image = ((System.Drawing.Image)(resources.GetObject("btnCustomerwisePrint.Image")));
            this.btnCustomerwisePrint.Name = "btnCustomerwisePrint";
            this.btnCustomerwisePrint.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.btnCustomerwisePrint.Text = "Customer Wise Print";
            this.btnCustomerwisePrint.Tooltip = "Customer Wise Print";
            this.btnCustomerwisePrint.Visible = false;
            this.btnCustomerwisePrint.Click += new System.EventHandler(this.btnCustomerwisePrint_Click);
            // 
            // btnPicklist
            // 
            this.btnPicklist.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPicklist.Image = ((System.Drawing.Image)(resources.GetObject("btnPicklist.Image")));
            this.btnPicklist.Name = "btnPicklist";
            this.btnPicklist.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.btnPicklist.Text = "Placement List";
            this.btnPicklist.Visible = false;
            this.btnPicklist.Click += new System.EventHandler(this.btnPicklist_Click);
            // 
            // bnEmail
            // 
            this.bnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnEmail.Image = ((System.Drawing.Image)(resources.GetObject("bnEmail.Image")));
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.bnEmail.Text = "Email";
            this.bnEmail.Click += new System.EventHandler(this.bnEmail_Click);
            // 
            // btnPickListEmail
            // 
            this.btnPickListEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPickListEmail.Image = ((System.Drawing.Image)(resources.GetObject("btnPickListEmail.Image")));
            this.btnPickListEmail.Name = "btnPickListEmail";
            this.btnPickListEmail.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.btnPickListEmail.Text = "Placement List Email";
            this.btnPickListEmail.Visible = false;
            this.btnPickListEmail.Click += new System.EventHandler(this.btnPickListEmail_Click);
            // 
            // btnActions
            // 
            this.btnActions.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnActions.Image = global::MyBooksERP.Properties.Resources.Add1;
            this.btnActions.Name = "btnActions";
            this.btnActions.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSalesOrder,
            this.btnSalesInvoice});
            this.btnActions.Text = "Actions";
            // 
            // btnSalesOrder
            // 
            this.btnSalesOrder.Name = "btnSalesOrder";
            this.btnSalesOrder.Text = " Create SalesOrder";
            this.btnSalesOrder.Click += new System.EventHandler(this.btnSalesOrder_Click);
            // 
            // btnSalesInvoice
            // 
            this.btnSalesInvoice.Name = "btnSalesInvoice";
            this.btnSalesInvoice.Text = "Create Sales Invoice";
            this.btnSalesInvoice.Click += new System.EventHandler(this.btnSalesInvoice_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblStatus.CloseButtonVisible = false;
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblStatus.Location = new System.Drawing.Point(0, 488);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.OptionsButtonVisible = false;
            this.lblStatus.Size = new System.Drawing.Size(1100, 26);
            this.lblStatus.TabIndex = 201;
            // 
            // tmrItemIssue
            // 
            this.tmrItemIssue.Interval = 2000;
            this.tmrItemIssue.Tick += new System.EventHandler(this.tmrItemIssue_Tick);
            // 
            // ErrItemIssue
            // 
            this.ErrItemIssue.ContainerControl = this;
            this.ErrItemIssue.RightToLeft = true;
            // 
            // CMSVendorAddress
            // 
            this.CMSVendorAddress.Name = "ContextMenuStrip1";
            this.CMSVendorAddress.Size = new System.Drawing.Size(61, 4);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Item Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle62;
            this.dataGridViewTextBoxColumn3.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Visible = false;
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle63;
            this.dataGridViewTextBoxColumn4.HeaderText = "QtyReceived";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle64;
            this.dataGridViewTextBoxColumn5.HeaderText = "Batch Number";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Visible = false;
            this.dataGridViewTextBoxColumn5.Width = 90;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle65;
            this.dataGridViewTextBoxColumn6.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn6.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle66.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle66;
            this.dataGridViewTextBoxColumn7.HeaderText = "Total";
            this.dataGridViewTextBoxColumn7.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Visible = false;
            this.dataGridViewTextBoxColumn7.Width = 90;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle67;
            this.dataGridViewTextBoxColumn8.HeaderText = "Status";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Visible = false;
            this.dataGridViewTextBoxColumn8.Width = 90;
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle68;
            this.dataGridViewTextBoxColumn9.HeaderText = "OrderDetailID";
            this.dataGridViewTextBoxColumn9.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Visible = false;
            this.dataGridViewTextBoxColumn9.Width = 90;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "PurchaseOrderID";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Visible = false;
            this.dataGridViewTextBoxColumn10.Width = 90;
            // 
            // dataGridViewTextBoxColumn11
            // 
            dataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle69;
            this.dataGridViewTextBoxColumn11.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn11.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Visible = false;
            this.dataGridViewTextBoxColumn11.Width = 90;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn12.HeaderText = "Uom";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn13.HeaderText = "BatchID";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Visible = false;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn14.HeaderText = "BatchNo";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle70;
            this.dataGridViewTextBoxColumn15.HeaderText = "QtyAvailable";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Visible = false;
            this.dataGridViewTextBoxColumn15.Width = 150;
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle71.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle71;
            this.dataGridViewTextBoxColumn16.HeaderText = "Issued Qty";
            this.dataGridViewTextBoxColumn16.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Visible = false;
            this.dataGridViewTextBoxColumn16.Width = 150;
            // 
            // dataGridViewTextBoxColumn17
            // 
            dataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle72;
            this.dataGridViewTextBoxColumn17.HeaderText = "Used Qty";
            this.dataGridViewTextBoxColumn17.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn17.Width = 150;
            // 
            // dataGridViewTextBoxColumn18
            // 
            dataGridViewCellStyle73.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn18.DefaultCellStyle = dataGridViewCellStyle73;
            this.dataGridViewTextBoxColumn18.HeaderText = "Issued Qty";
            this.dataGridViewTextBoxColumn18.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn19
            // 
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle74;
            this.dataGridViewTextBoxColumn19.HeaderText = "Used Qty";
            this.dataGridViewTextBoxColumn19.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn20
            // 
            dataGridViewCellStyle75.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn20.DefaultCellStyle = dataGridViewCellStyle75;
            this.dataGridViewTextBoxColumn20.HeaderText = "Used Qty";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            // 
            // CntxtHistory
            // 
            this.CntxtHistory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ShowItemHistory,
            this.ShowSaleRate,
            this.showGroupItemDetails});
            this.CntxtHistory.Name = "CntxtHistory";
            this.CntxtHistory.Size = new System.Drawing.Size(205, 70);
            // 
            // ShowItemHistory
            // 
            this.ShowItemHistory.Name = "ShowItemHistory";
            this.ShowItemHistory.Size = new System.Drawing.Size(204, 22);
            this.ShowItemHistory.Text = "Show Item History";
            this.ShowItemHistory.Visible = false;
            // 
            // ShowSaleRate
            // 
            this.ShowSaleRate.Name = "ShowSaleRate";
            this.ShowSaleRate.Size = new System.Drawing.Size(204, 22);
            this.ShowSaleRate.Text = "Show Sale Rates";
            this.ShowSaleRate.Visible = false;
            // 
            // showGroupItemDetails
            // 
            this.showGroupItemDetails.Name = "showGroupItemDetails";
            this.showGroupItemDetails.Size = new System.Drawing.Size(204, 22);
            this.showGroupItemDetails.Text = "Show Group Item Details";
            this.showGroupItemDetails.Click += new System.EventHandler(this.showGroupItemDetails_Click);
            // 
            // FrmDirectDelivery
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.ClientSize = new System.Drawing.Size(1303, 514);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.dockSite2);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.expandableSplitterLeft);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.dockSite3);
            this.Controls.Add(this.dockSite4);
            this.Controls.Add(this.dockSite5);
            this.Controls.Add(this.dockSite6);
            this.Controls.Add(this.dockSite8);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmDirectDelivery";
            this.Text = "Direct Delivery Note";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmItemIssue_Load);
            this.Shown += new System.EventHandler(this.FrmItemIssue_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmItemIssue_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmItemIssue_KeyDown);
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvIssueDisplay)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.panelMain.ResumeLayout(false);
            this.panelMiddle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcDeliveryNote)).EndInit();
            this.tcDeliveryNote.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panelEx3.ResumeLayout(false);
            this.panelEx3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemDetailsGrid)).EndInit();
            this.tabControlPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocationDetails)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrItemIssue)).EndInit();
            this.CntxtHistory.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        #endregion

        public FrmMain objFrmTradingMain;
        clsBLLDirectDelivery MobjClsBLLDirectDelivery;//Object Of clsBllItemIssueMaster
        string MstrCommonMessage;                   // for setting error message
        DataTable datMessages;                      // having forms error messages
        MessageBoxIcon MmsgMessageIcon;             // obj of message icon
        ClsNotificationNew MobjClsNotification;     //obj of clsNotification
        ClsLogWriter MobjClsLogWriter;              // obj of clsLogWriter

        bool blnIsEditMode = false;
        bool MblnPrintEmailPermission = false;
        bool MblnAddPermission = false;
        bool MblnUpdatePermission = false;
        bool MblnDeletePermission = false;
        bool MblnIsFromClear = false;
        bool MblnIsEditable = true;
        bool MchangeStatus = false;    //Check any edit or new entry done
        DataTable MDtItemGroupIssueDetails = new DataTable();
        DataTable dtItemDetails = new DataTable();
        public DataTable datItemDetails;
        DataTable BatchlessItems = new DataTable();
        DataTable dtGroupDetailsI = new DataTable();
        clsBLLCommonUtility mobjClsBllCommonUtility;
        public int PintFormType = 0; // 1 - Delivery Note 2 - Material Issue
        public int PintCompanyID = 0;// for selecting the company in add mode   -- alerts
        public long PlngReferenceID = 0;// for selecting the reference No (Invoice / DMR)   -- alerts
        public int PintOrderTypeID = 0;// for selecting the operation type  -- alerts
        public long lngItemIssueID = 0; // for selected item issue -- alerts
        private int MintVendorAddID = 0;
        decimal decDeliveredQty;
        decimal  decInvQty;
        private int SItemID;
        private int SIsGroup;
        int intCostingMethod = 0;
        int MintBaseCurrencyScale;

        bool IsMessageBoxShown = false;
        public FrmDirectDelivery()
        {
            //Constructor

            InitializeComponent();

            MobjClsBLLDirectDelivery = new clsBLLDirectDelivery();
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotificationNew();
            tmrItemIssue.Interval = ClsCommonSettings.TimerInterval;
            mobjClsBllCommonUtility = new clsBLLCommonUtility();
            MDtItemGroupIssueDetails.Columns.Add("ItemGroupID");
            MDtItemGroupIssueDetails.Columns.Add("ReferenceNo");
            MDtItemGroupIssueDetails.Columns.Add("ReferenceSerialNo");
            MDtItemGroupIssueDetails.Columns.Add("ItemID");
            MDtItemGroupIssueDetails.Columns.Add("ItemCode");
            MDtItemGroupIssueDetails.Columns.Add("ItemName");
            MDtItemGroupIssueDetails.Columns.Add("BatchID");
            MDtItemGroupIssueDetails.Columns.Add("UOMID");
            MDtItemGroupIssueDetails.Columns.Add("Quantity");

            BatchlessItems.Columns.Add("ItemID");
            BatchlessItems.Columns.Add("BatchID");
            BatchlessItems.Columns.Add("AvailableQty");
           


        }


        private void LoadMessage()
        {
            try
            {
                //Loads error messages
                datMessages = new DataTable();
                datMessages = MobjClsNotification.FillMessageArray((int)FormID.ItemIssue, ClsCommonSettings.ProductID);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadMessage() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in Load Message() " + ex.Message, 2);
            }
        }

        private void ArrangeControls()
        {
            if (PintFormType == 2)
            {
                this.Text = "Material Issue";

                //cboOperationType.Visible = false;
                //lblOperationType.Text = "DMR No";
                //cboReferenceNo.Left = cboOperationType.Left;
                //cboReferenceNo.Width = cboOperationType.Width;

               
                //cboSOperationType.Visible = false;

                lblSIssueNo.Top = lblTo.Top;
                txtSearch.Top = dtpSTo.Top;
                lblTo.Top = lblFrom.Top;
                dtpSTo.Top = dtpSFrom.Top;
                lblFrom.Top = lblSIssuedBy.Top;
                dtpSFrom.Top = cboSIssuedBy.Top;
                lblSIssuedBy.Top = lblSWarehouse.Top;
                cboSIssuedBy.Top = cboSWarehouse.Top;
                //lblSWarehouse.Top = lblSOperationType.Top;
                //cboSWarehouse.Top = cboSOperationType.Top;

                //panelLeftTop.Height = panelLeftTop.Height - cboSOperationType.Height;
                //expandablePanel1.Height = expandablePanel1.Height - cboSOperationType.Height;
            }
        }

        private void LoadCombo(int intType)
        {
            // 0 - To Load Combos
            // 1 - To Load Warehouse
            // 2 - To Load OperationType
            // 3 - To Load Search Company
            // 4 - To Load Search Operation Type
            // 5 - To Load Search Warehouse
            try
            {
                clsBLLPermissionSettings objBllPermissionSettings = new clsBLLPermissionSettings();
                DataTable datCombos = null;
                if (intType == 0 || intType == 1)
                {
                    datCombos = MobjClsBLLDirectDelivery.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID + "" });
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;
                }
                
                if (intType == 0 || intType == 2)
                {
                    datCombos = MobjClsBLLDirectDelivery.FillCombos(new string[] { "V.VendorID,V.VendorName", "InvVendorInformation V INNER JOIN InvVendorCompanyDetails VC ON V.VendorID = VC.VendorID ", " V.VendorTypeID = " + (int)VendorType.Customer + " AND VC.CompanyID = " + ClsCommonSettings.CompanyID + "  and V.StatusID=" + (int)VendorStatus.CustomerActive + "" });
                    cboCustomer.ValueMember = "VendorID";
                cboCustomer.DisplayMember = "VendorName";
                cboCustomer.DataSource = datCombos;
                }
                if (intType == 0 || intType == 3)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLDirectDelivery.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID + "" });
                    
                    cboSCompany.ValueMember = "CompanyID";
                    cboSCompany.DisplayMember = "CompanyName";
                    cboSCompany.DataSource = datCombos;
                    cboSCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;
                }
                //COMMENTED FOR MODIFICATION:DIRECT DELIVERY
                
                if (intType == 0 || intType == 5)
                {
                }
                if (intType == 0 || intType == 6)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Distinct InvWarehouse.WarehouseID,WarehouseName", "" +
                        "InvWarehouse INNER JOIN InvWarehouseCompanyDetails WC ON InvWarehouse.WarehouseID=WC.WarehouseID", "" +
                        "WC.CompanyID = " + cboCompany.SelectedValue.ToInt32() + " And IsActive = 1" });
                    cboWarehouse.ValueMember = "WarehouseID";
                    cboWarehouse.DisplayMember = "WarehouseName";
                    cboWarehouse.DataSource = datCombos;

                }
                if (intType == 0 || intType == 7)
                {
                }
                if (intType == 8)
                {
                    string strSearchCondition = "";
                    if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                    {
                        strSearchCondition = "UM.RoleID Not In (1,2)";
                    }
                    if (!string.IsNullOrEmpty(strSearchCondition))
                        strSearchCondition = strSearchCondition + " AND ";
                    strSearchCondition = strSearchCondition + "UM.UserID <> 1";

                    datCombos = MobjClsBLLDirectDelivery.FillCombos(new string[] { "UM.UserID,IsNull(EM.FirstName,UM.UserName) as UserName", "UserMaster UM Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID", strSearchCondition });
                    cboSIssuedBy.ValueMember = "UserID";
                    cboSIssuedBy.DisplayMember = "UserName";
                    cboSIssuedBy.DataSource = datCombos;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadCombos() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in LoadCombos() " + ex.Message, 2);
            }
        }

      //COMMENTED FOR MODIFICATION:DIRECT DELIVERY

        //private void cboOperationType_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //    try
        //    {
        //        if (cboOperationType.SelectedValue != null && cboWarehouse.SelectedValue != null)
        //        {

        //            if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesInvoice)
        //            {
        //                dgvItemDetailsGrid.Visible = true;
        //                dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
        //                dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = true;
        //                btnCustomerwisePrint.Enabled = false;

        //                BatchNo.Visible = true;
        //                ItemCode.ReadOnly = ItemName.ReadOnly = BatchNo.ReadOnly = Uom.ReadOnly = Quantity.ReadOnly = false;
        //                dgvItemDetailsGrid.AllowUserToAddRows = true;
        //                dgvItemDetailsGrid.AllowUserToDeleteRows = true;
        //               // cboWarehouse.Enabled = true;
        //            }
              
        //            else if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTTransfer)
        //            {
        //                dgvItemDetailsGrid.Visible = true;
        //                dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
        //                btnCustomerwisePrint.Enabled = false;
        //                BatchNo.Visible = true;
        //                dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = false;
        //                ItemCode.ReadOnly = ItemName.ReadOnly = BatchNo.ReadOnly = Uom.ReadOnly = Quantity.ReadOnly = true;
        //                dgvItemDetailsGrid.AllowUserToAddRows = false;
        //                dgvItemDetailsGrid.AllowUserToDeleteRows = false;
        //                //  cboWarehouse.Enabled = false;
        //            }
        //            else if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesOrder)
        //            {
        //                dgvItemDetailsGrid.Visible = true;
        //                dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
        //                dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = true;
        //                btnCustomerwisePrint.Enabled = false;
        //                dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].HeaderText = "SalesOrderQty";
        //                BatchNo.Visible = true;
        //                ItemCode.ReadOnly = ItemName.ReadOnly = BatchNo.ReadOnly = Uom.ReadOnly = Quantity.ReadOnly = false;
        //                dgvItemDetailsGrid.AllowUserToAddRows = false;
        //                dgvItemDetailsGrid.AllowUserToDeleteRows = false;
        //            }
        //            if (!blnIsEditMode)
        //                FillReferenceNo();
        //        }
        //        else
        //        {
        //            cboReferenceNo.DataSource = null;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        if (ClsCommonSettings.ShowErrorMess)
        //            MessageBox.Show("Error in cboOperationType_SelectedIndexChanged() " + ex.Message);
        //        MobjClsLogWriter.WriteLog("Error in cboOperationType_SelectedIndexChanged() " + ex.Message, 2);
        //    }
        //}

        //COMMENTED FOR DIRECT DELIVERY

        //private void FillReferenceNo()
        //{
        //    try
        //    {
        //        if (cboOperationType.SelectedValue != null)
        //        {
        //            dgvItemDetailsGrid.Rows.Clear();
        //            DataTable dtReference = null;
        //            string strCondition = string.Empty;
        //            if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesInvoice)
        //            {
        //                if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID == 0)
        //                    strCondition = "InvSalesInvoiceMaster.StatusID In(" + (Int32)OperationStatusType.SPartiallyDelivered + "," + (Int32)OperationStatusType.SInvoiceOpen + " )And SalesInvoiceID  Not In (SELECT DISTINCT SIM.SalesInvoiceID FROM InvSalesInvoiceMaster SIM inner join InvSalesOrderMaster SOM on SOM.SalesOrderID = SIM.SalesOrderID inner join InvItemIssueMaster IM ON SOM.SalesOrderID =IM.ReferenceID and IM.OrderTypeID =" + (int)OperationOrderType.DNOTSalesOrder + " ) And InvSalesInvoiceMaster.CompanyID =" + Convert.ToInt32(cboCompany.SelectedValue) + "";
        //                dtReference = MobjClsBLLDirectDelivery.FillCombos(new string[] { "SalesInvoiceID as ReferenceID,SalesInvoiceNo as ReferenceNo", "InvSalesInvoiceMaster", strCondition });
        //            }

        //            else if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTTransfer)
        //            {
        //                if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID == 0)
        //                    strCondition = "InvStockTransferMaster.StatusID In (" + (Int32)OperationStatusType.STApproved + ","+(int)OperationStatusType.STSubmitted+") And InvStockTransferMaster.WarehouseID =" + Convert.ToInt32(cboWarehouse.SelectedValue)+"";
        //                dtReference = MobjClsBLLDirectDelivery.FillCombos(new string[] { "StockTransferID as ReferenceID,StockTransferNo as ReferenceNo", "InvStockTransferMaster", strCondition });
        //            }
        //            //else if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTDMR)
        //            //{
        //            //    if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID == 0)
        //            //        strCondition = "PrdDMRMaster.StatusID In (" + (int)OperationStatusType.DMROpened + ")";
        //            //    dtReference = MobjClsBLLDirectDelivery.FillCombos(new string[] { "DMRID as ReferenceID,DMRNo as ReferenceNo", "PrdDMRMaster", strCondition });
        //            //}
        //            else if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesOrder)
        //            {
        //                if(MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID ==0)
        //                    strCondition = "InvSalesOrderMaster.StatusID In (" + (Int32)OperationStatusType.SOrderSubmitted + "," + (Int32)OperationStatusType.SOrderClosed + "," + (Int32)OperationStatusType.SOrderApproved + ")And SalesOrderID Not In( select SalesOrderID from InvSalesOrderMaster where  DeliveredStatus = " + (int)SalesOrderDeliveryStatus.Delivered + ")And SalesOrderID Not In(SELECT DISTINCT SM.SalesOrderID FROM InvSalesOrderMaster SM inner  join InvSalesInvoiceMaster SIM on SIM.SalesOrderID = SM.SalesOrderID inner JOIN InvItemIssueMaster IM ON SIM.SalesInvoiceID =IM.ReferenceID and IM.OrderTypeID = "+(int)OperationOrderType.DNOTSalesInvoice +") And InvSalesOrderMaster.CompanyID =" + Convert.ToInt32(cboCompany.SelectedValue) + "";
        //                dtReference = MobjClsBLLDirectDelivery.FillCombos(new string[] { "SalesOrderID as ReferenceID,SalesOrderNo as ReferenceNo", "InvSalesOrderMaster", strCondition });
        //            }
        //            else
        //                dtReference = null;
        //            //  cboReferenceNo.DataSource = null;
        //          //  cboReferenceNo.DataSource = null;
        //            cboReferenceNo.ValueMember = "ReferenceID";
        //            cboReferenceNo.DisplayMember = "ReferenceNo";
        //            cboReferenceNo.DataSource = dtReference;
        //        }
        //        else
        //        {
        //            cboReferenceNo.DataSource = null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ClsCommonSettings.ShowErrorMess)
        //            MessageBox.Show("Error in FillReferenceNo() " + ex.Message);
        //        MobjClsLogWriter.WriteLog("Error in FillReferenceNo() " + ex.Message, 2);
        //    }
        //}

        private void FrmItemIssue_Load(object sender, EventArgs e)
        {
            try
            {
                if (!ClsCommonSettings.PblnIsLocationWise)
                {
                   tiLocationDetails.Visible = false;
                   btnPicklist.Visible = false;
                   btnPickListEmail.Visible = false;
                }
                dtpSFrom.Value = ClsCommonSettings.GetServerDate();
                dtpSTo.Value = ClsCommonSettings.GetServerDate();
                LoadCombo(0);
                LoadMessage();
                SetPermissions();
                AddNewItemIssue();
                ArrangeControls();
                dtpIssuedDate.MaxDate = ClsCommonSettings.GetServerDate();
                if (cboCustomer.Items.Count > 0)
                    cboCustomer.SelectedIndex = -1;
                if (PintCompanyID != 0 && PintOrderTypeID != 0 && PlngReferenceID != 0)
                {
                    cboCompany.SelectedValue = PintCompanyID;
                    if (cboWarehouse.Items.Count > 0)
                        cboWarehouse.SelectedIndex = 0;
                    //COMMENTED FOR MODIFICATION
                    //cboOperationType.SelectedValue = PintOrderTypeID;
                    //cboReferenceNo.SelectedValue = PlngReferenceID;
                }
                else if (lngItemIssueID != 0)
                {
                    MobjClsBLLDirectDelivery = new clsBLLDirectDelivery();
                    MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID = lngItemIssueID;
                    DisplayIssueInfo();
                }
                btnSampleIssue.Visible = ClsMainSettings.AlMajdalPrePrintedFormat;
                btnSampleIssue.Enabled = false;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FrmItemIssue_Load() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in FrmItemIssue_Load() " + ex.Message, 2);
            }
        }
     
        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                if(PintFormType == 1)
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory , (Int32)eMenuID.DeliveryNote, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                else if(PintFormType == 2)
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.MaterialIssue, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }
        //private void FillBatchNoColumn(int intItemID, bool blnIsEdit)
        //{
        //    DataTable datCombos = new DataTable();
        //    BatchNo.DataSource = null;
        //    if (!blnIsEdit)
        //        datCombos = MobjClsBLLDirectDelivery.FillCombos(new string[] { "BD.BatchID,BD.BatchNo", "InvBatchDetails BD Inner Join InvItemStockDetails SD On SD.BatchID = BD.BatchID And SD.ItemID = BD.ItemID", "BD.ItemID = " + intItemID + " And SD.WarehouseID = " + Convert.ToInt32(cboWarehouse.SelectedValue) + " And IsNull((SD.GRNQuantity+SD.ReceivedFromTransfer + SD.ProducedQuantity)-(SD.SoldQuantity+ SD.TransferedQuantity+SD.DamagedQuantity+SD.DemoQuantity),0) > 0 And IsNull(BD.ExpiryDate,GetDate()) >= '" + ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy") + "'" });
        //    else
        //        datCombos = MobjClsBLLDirectDelivery.FillCombos(new string[] { "BD.BatchID,BD.BatchNo", "InvBatchDetails BD Inner Join InvItemStockDetails SD On SD.BatchID = BD.BatchID And SD.ItemID = BD.ItemID", "BD.ItemID = " + intItemID + " And SD.WarehouseID = " + Convert.ToInt32(cboWarehouse.SelectedValue) + "" });
        //    BatchNo.ValueMember = "BatchID";
        //    BatchNo.DisplayMember = "BatchNo";
        //    BatchNo.DataSource = datCombos;
        //}
        private DataTable FillUomColumn(int intItemID, bool blnIsGroup)
        {
            try
            {
                DataTable datCombos = new DataTable();
                
                if (!blnIsGroup)
                {
                    datCombos = MobjClsBLLDirectDelivery.GetItemUoms(intItemID);
                    Uom.ValueMember = "UOMID";
                    Uom.DisplayMember = "ShortName";
                    Uom.DataSource = datCombos;
                }
                else
                {
                    datCombos = MobjClsBLLDirectDelivery.FillCombos(new string[] { "UOMID,Code as ShortName", "InvUOMReference", "UOMID = " + (int)PredefinedUOMs.Numbers });
                    Uom.ValueMember = "UOMID";
                    Uom.DisplayMember = "ShortName";
                    Uom.DataSource = datCombos;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FillUomColumn() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in FillUomColumn() " + ex.Message, 2);
            }
            return null;
        }
        
        private void dgvItemDetailsGrid_Textbox_TextChanged(object sender, EventArgs e)
        {
            //Item Selection
            try
            {
                if (dgvItemDetailsGrid.CurrentCell.ColumnIndex == 0 || dgvItemDetailsGrid.CurrentCell.ColumnIndex == 1)
                {
                    for (int i = 0; i < dgvItemDetailsGrid.Columns.Count; i++)
                        dgvItemDetailsGrid.Rows[dgvItemDetailsGrid.CurrentCell.RowIndex].Cells[i].Value = null;
                }

                
                    dgvItemDetailsGrid.PServerName = ClsCommonSettings.ServerName;
                    string[] First = { "ItemCode", "ItemName", "ItemID", "BatchID", "BatchNo", "QtyAvailable","Rate", "IsGroup", "IsGroupItem" };//first grid 
                    string[] second = { "ItemCode", "ItemName", "ItemID", "BatchID", "BatchNo", "QtyAvailable","Rate", "IsGroup", "IsGroupItem" };//inner grid
                    dgvItemDetailsGrid.aryFirstGridParam = First;
                    dgvItemDetailsGrid.arySecondGridParam = second;
                    dgvItemDetailsGrid.PiFocusIndex = Quantity.Index;
                    dgvItemDetailsGrid.iGridWidth = 500;
                    dgvItemDetailsGrid.bBothScrollBar = true;
                    dgvItemDetailsGrid.pnlLeft = expandableSplitterLeft.Location.X + 5;
                    dgvItemDetailsGrid.pnlTop = expandableSplitterTop.Location.Y + PurchaseOrderBindingNavigator.Height + 10;
                    dgvItemDetailsGrid.ColumnsToHide = new string[] { "ItemID", "IsGroup", "BatchID", "ExpiryDate", "BatchNo", "QtyAvailable" };
                    dgvItemDetailsGrid.field = "ItemCode";
                   DataTable dtItemSelectionTable = MobjClsBLLDirectDelivery.GetDataForItemSelection(ClsCommonSettings.CompanyID,Convert.ToInt32(cboWarehouse.SelectedValue));
                    string strFilterCondition = "";
                    if (dgvItemDetailsGrid.CurrentCell.OwningColumn.Index == 0)
                        strFilterCondition = " ItemCode Like '" + ((dgvItemDetailsGrid.TextBoxText.ToUpper())) + "%'";
                    else
                        strFilterCondition = " ItemName Like '" + ((dgvItemDetailsGrid.TextBoxText.ToUpper())) + "%'";

                    dtItemSelectionTable.DefaultView.RowFilter = strFilterCondition;


                    DataTable datItemDetails = dtItemSelectionTable.DefaultView.ToTable();

                   

                    dgvItemDetailsGrid.dtDataSource = datItemDetails;

                
                
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvItemDetailsGrid_TextboxChangedEvent() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvItemDetailsGrid_TextboxChangedEvent() " + ex.Message, 2);
            }
        }

        //private void cboReferenceNo_SelectedIndexChanged(object sender, EventArgs e)
        //{
            
        //    if (!blnIsEditMode)
        //    {
        //        bool blnValid = true;
        //        if (cboReferenceNo.SelectedValue != null)
        //        {
        //            if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesInvoice)
        //            {
        //                //if (!MblnIsFromClear && MobjClsBLLDirectDelivery.FillCombos(new string[] { "SalesInvoiceID", "InvSalesInvoiceMaster", "SalesInvoiceID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) + " And StatusID = " + (Int32)OperationStatusType.SInvoiceOpen }).Rows.Count == 0)
        //                //{
        //                //    // Please select opened sales invoice
        //                //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4223, out MmsgMessageIcon);
        //                //    MstrCommonMessage = MstrCommonMessage.Replace("*", "Sales Invoice");
        //                //    blnValid = false;
        //                //}

        //                //if (MobjClsBLLDirectDelivery.GetJobOrderIDAgainstInvoice(cboReferenceNo.SelectedValue.ToInt64()) == 0)
        //                //{
        //                    dgvItemDetailsGrid.Visible = true;
        //                    dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
        //                    dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = true;
        //                    btnCustomerwisePrint.Enabled = false;

        //                    BatchNo.Visible = true;
        //                    ItemCode.ReadOnly = ItemName.ReadOnly = BatchNo.ReadOnly = Uom.ReadOnly = Quantity.ReadOnly = false;
        //                    dgvItemDetailsGrid.AllowUserToAddRows = true;
        //                    dgvItemDetailsGrid.AllowUserToDeleteRows = true;
        //                    // cboWarehouse.Enabled = true;
        //               // }
        //               // else 
        //               // {
        //               //     dgvItemDetailsGrid.Visible = true;
        //               //     dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
        //               //     btnCustomerwisePrint.Enabled = false;
        //               //     dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = false;
        //               ////     BatchNo.Visible = false;
        //               //     ItemCode.ReadOnly = ItemName.ReadOnly = BatchNo.ReadOnly = Uom.ReadOnly = Quantity.ReadOnly = true;
        //               //     dgvItemDetailsGrid.AllowUserToAddRows = false;
        //               //     dgvItemDetailsGrid.AllowUserToDeleteRows = false;
        //               //     //  cboWarehouse.Enabled = false;
        //               // }

        //            }
        //            else if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTTransfer)
        //            {
        //                if (!MblnIsFromClear && MobjClsBLLDirectDelivery.FillCombos(new string[] { "StockTransferID", "InvStockTransferMaster", "StockTransferID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.STSubmitted+","+(int)OperationStatusType.STApproved+")" }).Rows.Count == 0)
        //                {
        //                    // Please select opened sales invoice
        //                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4241, out MmsgMessageIcon);
        //                    if (ClsCommonSettings.StockTransferApproval)
        //                        MstrCommonMessage = MstrCommonMessage.Replace("*", "approved");
        //                    else
        //                        MstrCommonMessage = MstrCommonMessage.Replace("*", "Submitted");
        //                    blnValid = false;
        //                }
        //                 dgvItemDetailsGrid.Visible = true;
        //                dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
        //                dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = true;
        //                btnCustomerwisePrint.Enabled = false;

        //                BatchNo.Visible = true;
        //                ItemCode.ReadOnly = ItemName.ReadOnly = BatchNo.ReadOnly = Uom.ReadOnly = Quantity.ReadOnly = false;
        //                dgvItemDetailsGrid.AllowUserToAddRows = true;

        //            }
        //            else if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesOrder)
        //            {
        //                if (!MblnIsFromClear && MobjClsBLLDirectDelivery.FillCombos(new string[] { "SalesOrderID", "InvSalesOrderMaster", "SalesOrderID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) + " And StatusID In(" + (Int32)OperationStatusType.SOrderApproved +","+(Int32)OperationStatusType.SOrderClosed +"," +(Int32)OperationStatusType.SOrderSubmitted +")" }).Rows.Count == 0)
        //                {
        //                    // Please select sales order 
        //                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4251, out MmsgMessageIcon);
        //                    MstrCommonMessage = MstrCommonMessage.Replace("*", "Sales Order");
        //                    blnValid = false;
        //                }


        //                dgvItemDetailsGrid.Visible = true;
        //                dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
        //                dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = true;
        //                btnCustomerwisePrint.Enabled = false;

        //                BatchNo.Visible = true;
        //                ItemCode.ReadOnly = ItemName.ReadOnly = BatchNo.ReadOnly = Uom.ReadOnly = Quantity.ReadOnly = false;
        //                dgvItemDetailsGrid.AllowUserToAddRows = true;



        //            }
        //            if (!blnValid)
        //            {
        //                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
        //                ErrItemIssue.SetError(cboReferenceNo, MstrCommonMessage.Replace("#", "").Trim());
        //                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
        //                tmrItemIssue.Enabled = true;
        //                cboReferenceNo.Focus();
        //            }
        //        }
        //        if (!blnIsEditMode )
        //        {
        //            dgvItemDetailsGrid.Rows.Clear();

        //            DataTable dtItemDetails = MobjClsBLLDirectDelivery.GetItemDetails(Convert.ToInt32(cboOperationType.SelectedValue), Convert.ToInt32(cboReferenceNo.SelectedValue));
        //            //DataTable datJobOrderDetails = new DataTable();
        //            //if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice && MobjClsBLLDirectDelivery.GetJobOrderIDAgainstInvoice(cboReferenceNo.SelectedValue.ToInt64()) !=0)
        //            //    {
        //            //   datJobOrderDetails = mobjClsBllCommonUtility.FillCombos(new string[] { "JobOrderProductID,ItemID,IsNull(BatchID,0) as BatchID,IsFromStore", "PrdJobOrderProducts", "JobOrderID = " + MobjClsBLLDirectDelivery.GetJobOrderIDAgainstInvoice(cboReferenceNo.SelectedValue.ToInt64()) });
        //            //}

        //            int intRowCount = 0;
        //            foreach (DataRow dr in dtItemDetails.Rows)
        //            {
        //                int intRowIndex = -1;
        //                if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice)
        //                {
        //                    foreach (DataGridViewRow row in dgvItemDetailsGrid.Rows)
        //                    {
        //                        if (row.Cells["ItemID"].Value.ToInt32() == dr["ItemID"].ToInt32() && row.Cells["BatchNo"].Tag.ToInt64() == dr["BatchID"].ToInt64())
        //                        {
        //                            intRowIndex = row.Index;
        //                            break;
        //                        }
        //                    }
        //                }

        //                if (intRowIndex == -1)
        //                {
        //                    dgvItemDetailsGrid.RowCount = dgvItemDetailsGrid.RowCount + 1;
        //                    if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTTransfer) //|| ((cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice) && (MobjClsBLLDirectDelivery.GetJobOrderIDAgainstInvoice(cboReferenceNo.SelectedValue.ToInt64()) != 0)))
        //                        intRowCount = dgvItemDetailsGrid.Rows.Count - 1;
        //                    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesOrder)
        //                    //{
        //                        intRowCount = dgvItemDetailsGrid.Rows.Count - 2;    
        //                    //}
        //                    // dgvItemDetailsGrid.Rows.Add();
        //                    dgvItemDetailsGrid["ReferenceSerialNo", intRowCount].Value = dr["ReferenceSerialNo"];
        //                    dgvItemDetailsGrid["ItemID", intRowCount].Value = dr["ItemID"];
        //                    dgvItemDetailsGrid["IsGroup", intRowCount].Value = dr["IsGroup"];
        //                    dgvItemDetailsGrid["ItemCode", intRowCount].Value = dr["Code"];
        //                    dgvItemDetailsGrid["ItemName", intRowCount].Value = dr["ItemName"];
        //                    dgvItemDetailsGrid["ReferenceNo", intRowCount].Value = dr["ReferenceNo"];
        //                    dgvItemDetailsGrid["InvoicedQuantity", intRowCount].Value = dr["InvoicedQuantity"];
        //                    dgvItemDetailsGrid["InvQty", intRowCount].Value = dr["InvQty"];
        //                    dgvItemDetailsGrid["InvUOMID", intRowCount].Value = dr["InvUOMID"];
        //                    dgvItemDetailsGrid["DeliveredQty", intRowCount].Value = dr["DeliveredQty"];
                          //FillUomColumn(Convert.ToInt32(dr["ItemID"]), Convert.ToBoolean(dr["IsGroup"]));
        //                    dgvItemDetailsGrid["Uom", intRowCount].Value = Convert.ToInt32(dr["InvUOMID"]);
        //                    dgvItemDetailsGrid["Uom", intRowCount].Tag = dgvItemDetailsGrid["Uom", intRowCount].Value;
        //                    dgvItemDetailsGrid["Uom", intRowCount].Value = dgvItemDetailsGrid["Uom", intRowCount].FormattedValue;

        //                    //decimal decDeliveredQty = dr["DeliveredQty"].ToDecimal();
        //                    //decDeliveredQty = decDeliveredQty;
        //                    decimal decQty = dr["InvQty"].ToDecimal();
                         
        //                    int intUomScale = 0;

        //                    if (dgvItemDetailsGrid[Uom.Index, intRowCount].Tag.ToInt32() != 0)
        //                        intUomScale = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Scale,Code as ShortName", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid[Uom.Index, intRowCount].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                           
        //                     //   dgvItemDetailsGrid.Rows[intRowCount].Cells[IsFromStore.Index].Value = 1;
        //                        // if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTTransfer)
        //                        //{
        //                        FillBatchNoColumn(Convert.ToInt32(dr["ItemID"]), false);
        //                        //if (mobjClsBllCommonUtility.FillCombos(new string[] { "BatchID", "InvBatchDetails", "ItemID = " + dr["ItemID"].ToInt32() + " And BatchID = " + dr["BatchID"].ToInt64() + " And IsNull(ExpiryDate,'" + ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy") + "') = '" + ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy") }).Rows.Count > 0)
        //                        //{
        //                        dgvItemDetailsGrid["BatchNo", intRowCount].Value = dr["BatchID"].ToInt64();
        //                        dgvItemDetailsGrid["BatchNo", intRowCount].Tag = dgvItemDetailsGrid["BatchNo", intRowCount].Value.ToInt64();
        //                        dgvItemDetailsGrid["BatchNo", intRowCount].Value = dgvItemDetailsGrid["BatchNo", intRowCount].FormattedValue;
        //                        //}

        //                        if (dgvItemDetailsGrid["BatchNo", intRowCount].Tag.ToInt32() != 0)
        //                            dgvItemDetailsGrid.Rows[intRowCount].Cells["QtyAvailable"].Value = GetAvailableQuantity(dgvItemDetailsGrid.Rows[intRowCount].Cells["ItemID"].Value.ToInt32(), dgvItemDetailsGrid.Rows[intRowCount].Cells["BatchNo"].Tag.ToInt64(), dgvItemDetailsGrid.CurrentRow.Cells["Uom"].Tag.ToInt32()).ToString("F" + intUomScale);
        //                        else
        //                            dgvItemDetailsGrid.Rows[intRowCount].Cells["QtyAvailable"].Value = 0;
        //                        //}
                            
        //                    DataTable datUom = mobjClsBllCommonUtility.FillCombos(new string[] { "Code as ShortName,Scale", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid.Rows[intRowCount].Cells["InvUOMID"].Value.ToInt32() });
        //                    //dgvItemDetailsGrid["Quantity", intRowCount].Value = dr["InvQty"].ToDecimal().ToString("F"+intUomScale);
        //                    dgvItemDetailsGrid[InvoicedQuantity.Index, intRowCount].Value = dgvItemDetailsGrid[InvQty.Index, intRowCount].Value.ToDecimal();//.ToString("F" + datUom.Rows[0]["Scale"].ToInt32()) + " " + datUom.Rows[0]["ShortName"].ToString();

        //                    dgvItemDetailsGrid["IsAutomatic", intRowCount].Value = "true";
        //                }
        //                else
        //                {
        //                    //dgvItemDetailsGrid.Rows[intRowCount].Cells[IsFromStore.Index].Value = 1;
        //                    int intUOMID = dr["InvUOMID"].ToInt32();
        //                    decimal decInvoicedQty = dr["InvQty"].ToDecimal();
        //                    decInvoicedQty = mobjClsBllCommonUtility.ConvertBaseUnitQtyToOtherQty(dgvItemDetailsGrid[InvUOMID.Index, intRowIndex].Value.ToInt32(), dr["ItemID"].ToInt32(), mobjClsBllCommonUtility.ConvertQtyToBaseUnitQty(dr["InvUOMID"].ToInt32(), dr["ItemID"].ToInt32(), dr["InvQty"].ToDecimal(), 1), 1);
        //                    DataTable datUOm = mobjClsBllCommonUtility.FillCombos(new string[] { "Scale,ShortName", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid.Rows[intRowIndex].Cells["InvUOMID"].Value.ToInt32() });

        //                    dgvItemDetailsGrid[InvQty.Index, intRowIndex].Value = dgvItemDetailsGrid[InvQty.Index, intRowIndex].Value.ToDecimal() + decInvoicedQty;
        //                    dgvItemDetailsGrid[InvoicedQuantity.Index, intRowIndex].Value = dgvItemDetailsGrid[InvQty.Index, intRowIndex].Value.ToDecimal();//.ToString("F"+datUOm.Rows[0]["Scale"].ToInt32()) + " " + datUOm.Rows[0]["ShortName"].ToString();

        //                    int intUomScale = 0;

        //                    if (dgvItemDetailsGrid[Uom.Index, intRowCount].Tag.ToInt32() != 0)
        //                        intUomScale = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Scale,ShortName", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid[Uom.Index, intRowCount].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
        //                    dgvItemDetailsGrid[Quantity.Index, intRowIndex].Value = dgvItemDetailsGrid[InvQty.Index, intRowIndex].Value.ToDecimal().ToString("F"+intUomScale);
        //                }
        //            }
        //        }
        //    }
        //    tcDeliveryNote.SelectedTab = tiItemDetails;
        //}

        private void dgvItemDetailsGrid_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 || e.ColumnIndex == 1)
            {
                dgvItemDetailsGrid.PiColumnIndex = e.ColumnIndex;
            }
        }

        private void dgvItemDetailsGrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (e.ColumnIndex == dgvItemDetailsGrid.Columns["ItemCode"].Index || e.ColumnIndex == dgvItemDetailsGrid.Columns["ItemName"].Index)
                    {
                        if (cboCompany.SelectedIndex == -1)
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4263, out MmessageIcon);
                            MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                            tmrItemIssue.Enabled = true;
                            cboCompany.Focus();
                        }
                    }
                }

                //int iRowIndex = 0;

                //if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                //{
                    //dgvItemDetailsGrid.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    //iRowIndex = dgvItemDetailsGrid.CurrentCell.RowIndex;
                    //if (dgvItemDetailsGrid.Columns[e.ColumnIndex].Name == "Uom")
                    //{
                    //    FillUomColumn(Convert.ToInt32(dgvItemDetailsGrid["ItemID", e.RowIndex].Value), Convert.ToBoolean(dgvItemDetailsGrid["IsGroup", e.RowIndex].Value));
                    //}
                    //if (e.ColumnIndex == Uom.Index)
                    //{
                    //    DataTable datSalesDel = new DataTable();

                    //    Int32 intItemID = Convert.ToInt32(dgvItemDetailsGrid.Rows[e.RowIndex].Cells["ItemID"].Value);
                    //    //int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                    //    int intUOMID = Convert.ToInt32(dgvItemDetailsGrid.Rows[e.RowIndex].Cells["Uom"].Tag);

                    //    if (intItemID != 0)
                    //    {
                    //        datSalesDel = FillUomColumn(intItemID, Convert.ToBoolean(dgvItemDetailsGrid.Rows[e.RowIndex].Cells["IsGroup"].Value));
                    //    }
                    //}
                    
                //}
                int iRowIndex = 0;

                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    dgvItemDetailsGrid.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    iRowIndex = dgvItemDetailsGrid.CurrentCell.RowIndex;

                    if (e.ColumnIndex != 0 && e.ColumnIndex != 1)
                    {
                        if (Convert.ToString(dgvItemDetailsGrid.CurrentRow.Cells[0].Value).Trim() == "")
                        {
                            e.Cancel = true;
                        }
                        else if (dgvItemDetailsGrid.Columns[e.ColumnIndex].Name == "Uom")
                        {
                            FillUomColumn(Convert.ToInt32(dgvItemDetailsGrid["ItemID", e.RowIndex].Value), Convert.ToBoolean(dgvItemDetailsGrid["IsGroup", e.RowIndex].Value));
                        }
                        //else if (dgvItemDetailsGrid.Columns[e.ColumnIndex].Name == "BatchNo")
                        //{

                          
                        //    FillBatchNoColumn(Convert.ToInt32(dgvItemDetailsGrid["ItemID", e.RowIndex].Value), false);

                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvItemDetailsGrid_CellBeginEdit() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvItemDetailsGrid_CellBeginEdit() " + ex.Message, 2);
            }
        }

        

        private void dgvItemDetailsGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvItemDetailsGrid["ItemID", e.RowIndex].Value != null)
                {
                    if (dgvItemDetailsGrid.Columns[e.ColumnIndex].Name == "BatchNo")
                    {
                        if (dgvItemDetailsGrid.CurrentRow.Cells["BatchNo"].Value != null)
                        {
                            //dgvItemDetailsGrid.CurrentRow.Cells["BatchNo"].Tag = dgvItemDetailsGrid.CurrentRow.Cells["BatchNo"].Value.ToInt64();
                            //dgvItemDetailsGrid.CurrentRow.Cells["BatchNo"].Value = dgvItemDetailsGrid.CurrentRow.Cells["BatchNo"].FormattedValue;

                            dgvItemDetailsGrid.CurrentRow.Cells["BatchNo"].Tag = dgvItemDetailsGrid.CurrentRow.Cells["BatchID"].Value.ToInt64();
                            //dgvItemDetailsGrid.CurrentRow.Cells["BatchID"].Value = dgvItemDetailsGrid.CurrentRow.Cells["BatchID"].FormattedValue;
                            int intUomScale = 0;
                            if (dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                                intUomScale = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();



                            dgvItemDetailsGrid.CurrentRow.Cells["QtyAvailable"].Value = GetAvailableQuantity(dgvItemDetailsGrid.CurrentRow.Cells["ItemID"].Value.ToInt32(), dgvItemDetailsGrid.CurrentRow.Cells["BatchNo"].Tag.ToInt64(), dgvItemDetailsGrid.CurrentRow.Cells["Uom"].Tag.ToInt32(), dgvItemDetailsGrid.CurrentRow.Cells["IsGroup"].Value.ToBoolean()).ToString("F" + intUomScale);



                        }
                    }
                    else if (e.ColumnIndex==Uom.Index)
                   {

                       dgvItemDetailsGrid.CurrentRow.Cells["Uom"].Tag = dgvItemDetailsGrid.CurrentRow.Cells["Uom"].Value;
                       dgvItemDetailsGrid.CurrentRow.Cells["Uom"].Value = dgvItemDetailsGrid.CurrentRow.Cells["Uom"].FormattedValue;
                       int intUomScale = 0;
                       if (dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                           intUomScale = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                       dgvItemDetailsGrid.CurrentRow.Cells["BatchNo"].Tag = dgvItemDetailsGrid.CurrentRow.Cells["BatchID"].Value.ToInt64();

                       dgvItemDetailsGrid.CurrentRow.Cells["QtyAvailable"].Value = GetAvailableQuantity(dgvItemDetailsGrid.CurrentRow.Cells["ItemID"].Value.ToInt32(), dgvItemDetailsGrid.CurrentRow.Cells["BatchNo"].Tag.ToInt64(), dgvItemDetailsGrid.CurrentRow.Cells["Uom"].Tag.ToInt32(), dgvItemDetailsGrid.CurrentRow.Cells["IsGroup"].Value.ToBoolean()).ToString("F" + intUomScale);
                      
                      //decimal decConvertedQuantity = 0;
                      // DataTable dtUomDetails = new DataTable();
                      // dtUomDetails = MobjClsBLLDirectDelivery.GetUomConversionValues(dgvItemDetailsGrid.CurrentRow.Cells["Uom"].Tag.ToInt32(), dgvItemDetailsGrid.CurrentRow.Cells["ItemID"].Value.ToInt32());
                      // if (dtUomDetails.Rows.Count > 0)
                      // {
                      //     decConvertedQuantity = Convert.ToDecimal(dgvItemDetailsGrid.CurrentRow.Cells["Quantity"].Value);
                      //     int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                      //     decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                      //     if (intConversionFactor == 1)
                      //         decConvertedQuantity = decConvertedQuantity * decConversionValue;
                      //     else if (intConversionFactor == 2)
                      //         decConvertedQuantity = decConvertedQuantity / decConversionValue;
                      //     dgvItemDetailsGrid.CurrentRow.Cells["Quantity"].Value = decConvertedQuantity;

                      // }
                      // else
                      // {

                      // }
                       int intItemID = Convert.ToInt32(dgvItemDetailsGrid.Rows[e.RowIndex].Cells["ItemID"].Value);
                       int intUOMID = Convert.ToInt32(dgvItemDetailsGrid.Rows[e.RowIndex].Cells["Uom"].Tag);

                       if (Convert.ToBoolean(dgvItemDetailsGrid[IsGroup.Index, e.RowIndex].Value) == false)
                           dgvItemDetailsGrid.Rows[e.RowIndex].Cells["Rate"].Value = mobjClsBllCommonUtility.GetSaleRate(intItemID, intUOMID, dgvItemDetailsGrid.CurrentRow.Cells["BatchID"].Value.ToInt64(), cboCompany.SelectedValue.ToInt32()); //(Convert.ToDecimal(dtrSalesRate[0].ItemArray[5].ToString()) / MobjclsBLLSTSales.clsDTOSTSalesMaster.decExchangeCurrencyRate).ToString("F" + MintExchangeCurrencyScale);

                   }
                    else  if (e.ColumnIndex == Quantity.Index || e.ColumnIndex == Rate.Index)
                    {
                        CalculateTotalAmt();
                    }
                    
                } 
               
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvItemDetailsgrid_CellEndEdit() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvItemDetailsGrid_CellEndEdit() " + ex.Message, 2);
            }
        }
        private void CalculateTotalAmt()
        {
            decimal decQty = 0;
            decimal decRate = 0;
            decimal decNetAmount = 0;

            int iGridRowCount = dgvItemDetailsGrid.RowCount;
            iGridRowCount = iGridRowCount - 1;
            for (int iCounter = 0; iCounter < iGridRowCount; iCounter++)
            {
                decQty = Convert.ToDecimal(dgvItemDetailsGrid.Rows[iCounter].Cells[Quantity.Index].Value);
                if (dgvItemDetailsGrid.Rows[iCounter].Cells[Rate.Index].Value != null)
                    decRate = Convert.ToDecimal(dgvItemDetailsGrid.Rows[iCounter].Cells[Rate.Index].Value);


                dgvItemDetailsGrid.Rows[iCounter].Cells[NetAmount.Index].Value = decQty * decRate;
                if (dgvItemDetailsGrid.Rows[iCounter].Cells[NetAmount.Index].Value != null)
                    decNetAmount = decNetAmount + Convert.ToDecimal(dgvItemDetailsGrid.Rows[iCounter].Cells[NetAmount.Index].Value);
            }
            MintBaseCurrencyScale = ClsCommonUtility.GetCurrencyScaleByCompany(cboCompany.SelectedValue.ToInt32());
            txtTotalAmount.Text = decNetAmount.ToString("F" + MintBaseCurrencyScale);
            txtSubTotal.Text = decNetAmount.ToString("F" + MintBaseCurrencyScale);
            CalculateExchangeCurrencyRate();
        }
        private void CalculateExchangeCurrencyRate()
        {
            int intCompanyID = 0, intCurrencyID = 0;
            int intScale;

            intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            string strCurrency = new clsBLLCommonUtility().GetCompanyCurrency(intCompanyID, out intScale, out intCurrencyID);

            if (MobjClsBLLDirectDelivery.GetExchangeCurrencyRate(intCompanyID, intCurrencyID))
            {
                txtExchangeCurrencyRate.Text = Convert.ToDecimal(MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.decExchangeCurrencyRate * Convert.ToDecimal(txtTotalAmount.Text)).ToString("F" + MintBaseCurrencyScale);
            }
            string strAmount = txtTotalAmount.Text;

            lblAmountinWords.Text = new clsBLLCommonUtility().ConvertToWord(strAmount, intCurrencyID);
        }
        private decimal GetAvailableQuantity(int intItemID,long lngBatchID,int intUOMID,bool IsGroup)
        {
            if (IsGroup == false && lngBatchID != 0)
            {
                decimal decStockQuantity = MobjClsBLLDirectDelivery.GetStockQuantity(intItemID, lngBatchID, Convert.ToInt32(cboCompany.SelectedValue), Convert.ToInt32(cboWarehouse.SelectedValue));
                DataTable dtUomDetails = new DataTable();
                dtUomDetails = MobjClsBLLDirectDelivery.GetUomConversionValues(intUOMID, intItemID);
                if (dtUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                        decStockQuantity = decStockQuantity * decConversionValue;
                    else if (intConversionFactor == 2)
                        decStockQuantity = decStockQuantity / decConversionValue;
                }
                return decStockQuantity;
            }
            else if (IsGroup == true && lngBatchID == 0)
            {
                decimal QtyAvailableForGrp = MobjClsBLLDirectDelivery.GetStockQuantityForGroup(intItemID, Convert.ToInt32(cboWarehouse.SelectedValue));
                DataTable dtUomDetails = MobjClsBLLDirectDelivery.GetUomConversionValues(intUOMID, intItemID);
                if (dtUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                        QtyAvailableForGrp = QtyAvailableForGrp * decConversionValue;
                    else if (intConversionFactor == 2)
                        QtyAvailableForGrp = QtyAvailableForGrp / decConversionValue;
                }
                return QtyAvailableForGrp;
            }
            else if (IsGroup == false && lngBatchID == 0)
            {
                decimal QtyAvailableForBatchlessItems = MobjClsBLLDirectDelivery.GetStockQuantityForBatchlessItems(intItemID, Convert.ToInt32(cboWarehouse.SelectedValue));
                DataTable dtUomDetails = MobjClsBLLDirectDelivery.GetUomConversionValues(intUOMID, intItemID);
                if (dtUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                        QtyAvailableForBatchlessItems = QtyAvailableForBatchlessItems * decConversionValue;
                    else if (intConversionFactor == 2)
                        QtyAvailableForBatchlessItems = QtyAvailableForBatchlessItems / decConversionValue;
                }
                return QtyAvailableForBatchlessItems;
            }
            else
            {
                return 0;
            }
        }

        private void dgvItemDetailsGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvItemDetailsGrid.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
            dgvItemDetailsGrid.CellLeave += new DataGridViewCellEventHandler(dgvItemDetailsGrid_CellLeave);
        }

        void dgvItemDetailsGrid_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvItemDetailsGrid.CurrentCell.OwningColumn.Name == "Quantity")
            {
                decimal decValue = 0;
                try
                {
                    decValue = Convert.ToDecimal(dgvItemDetailsGrid[e.ColumnIndex, e.RowIndex].Value);
                }
                catch
                {
                    dgvItemDetailsGrid[e.ColumnIndex, e.RowIndex].Value = 0;
                }
            }
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvItemDetailsGrid.CurrentCell.OwningColumn.Name == "Quantity")
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
                int intUomScale = 0;
                if (dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    intUomScale = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    if (intUomScale == 0)
                    {
                        if (e.KeyChar == '.')
                            e.Handled = true;
                    }
                    else
                    {
                        int dotIndex = -1;
                        if (txt.Text.Contains("."))
                        {
                            dotIndex = txt.Text.IndexOf('.');
                        }
                        if (e.KeyChar == '.')
                        {
                            if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            {
                                e.Handled = true;
                            }
                        }
                        else
                        {
                            if (char.IsDigit(e.KeyChar))
                            {
                                if (dotIndex != -1 && txt.SelectionStart > dotIndex)
                                {
                                    string[] splitText = txt.Text.Split('.');
                                    if (splitText.Length == 2)
                                    {
                                        if (splitText[1].Length - txt.SelectedText.Length >= intUomScale)
                                            e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
                //int intDecimalPart = 0;
                //if (dgvItemDetailsGrid.EditingControl != null && !string.IsNullOrEmpty(dgvItemDetailsGrid.EditingControl.Text) && dgvItemDetailsGrid.EditingControl.Text.Contains("."))
                //    intDecimalPart = dgvItemDetailsGrid.EditingControl.Text.Split('.')[1].Length;
                //int intUomScale = 0;
                //if (dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                //    intUomScale = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                
                //if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                //{
                //    e.Handled = true;
                //}
                //if (e.KeyChar.ToString() == "." && intUomScale == 0)
                //{
                //    e.Handled = true;
                //}

                //if (e.KeyChar != 13 && e.KeyChar != 08 && dgvItemDetailsGrid.EditingControl != null && !string.IsNullOrEmpty(dgvItemDetailsGrid.EditingControl.Text) && (dgvItemDetailsGrid.EditingControl.Text.Contains(".") && ((e.KeyChar == 46) || intDecimalPart == intUomScale)))//checking more than one "."
                //{
                //    e.Handled = true;
                //}
            }
        }

        private void dgvItemDetailsGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvItemDetailsGrid.CurrentCell == null) return;

            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                if (dgvItemDetailsGrid.Columns[e.ColumnIndex].Name == "ItemID" ||
                    dgvItemDetailsGrid.Columns[e.ColumnIndex].Name == "Uom")
                {
                    //dgvItemDetailsGrid["Uom", e.RowIndex].Value = null;
                    //dgvItemDetailsGrid["Uom", e.RowIndex].Tag = null;
                    //dgvItemDetailsGrid["BatchNo", e.RowIndex].Value = null;
                    //dgvItemDetailsGrid["BatchNo", e.RowIndex].Tag = null;


                 // FillUomColumn(Convert.ToInt32(dgvItemDetailsGrid["ItemID", e.RowIndex].Value),Convert.ToBoolean(dgvItemDetailsGrid["IsGroup", e.RowIndex].Value) );
                    int intUomScale = 0;

                        if (dgvItemDetailsGrid[Uom.Index, e.RowIndex].Tag.ToInt32() != 0)
                            intUomScale = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Scale,Code as ShortName", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid[Uom.Index, e.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                        int intBatchID = Convert.ToInt32(dgvItemDetailsGrid.Rows[e.RowIndex].Cells["BatchID"].Value);
                        Int32 intItemID = Convert.ToInt32(dgvItemDetailsGrid.Rows[e.RowIndex].Cells["ItemID"].Value);
                        if (intItemID != 0)
                        {
                            dgvItemDetailsGrid.Rows[e.RowIndex].Cells["BatchNo"].Tag = 
                                dgvItemDetailsGrid.Rows[e.RowIndex].Cells["BatchID"].Value.ToInt64();
                            
                            dgvItemDetailsGrid.Rows[e.RowIndex].Cells["QtyAvailable"].Value = 
                                GetAvailableQuantity(dgvItemDetailsGrid.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt32(), 
                                dgvItemDetailsGrid.Rows[e.RowIndex].Cells["BatchNo"].Tag.ToInt64(), 
                                dgvItemDetailsGrid.CurrentRow.Cells["Uom"].Tag.ToInt32(),
                                Convert.ToBoolean(dgvItemDetailsGrid.CurrentRow.Cells["IsGroup"].Value)).ToString("F" + intUomScale);
                        }
                }
                
                else if (dgvItemDetailsGrid.Columns[e.ColumnIndex].Name == "IsGroup")
                {
                    DataTable datSalesRate = new DataTable();
                    int intUOMID = 0;
                    Int32 intItemID = Convert.ToInt32(dgvItemDetailsGrid.Rows[e.RowIndex].Cells["ItemID"].Value);
                   // int intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);

                    if (intItemID != 0)
                    {
                        datSalesRate = FillUomColumn(intItemID, Convert.ToBoolean(dgvItemDetailsGrid.Rows[e.RowIndex].Cells["IsGroup"].Value));
                        if (Convert.ToBoolean(dgvItemDetailsGrid.Rows[e.RowIndex].Cells["IsGroup"].Value) == false)
                        {
                            FillUomColumn(intItemID, Convert.ToBoolean(dgvItemDetailsGrid.Rows[e.RowIndex].Cells["IsGroup"].Value));
                            DataTable datDefaultUom = MobjClsBLLDirectDelivery.FillCombos(new string[] { "DefaultSalesUomID as UomID", "InvItemMaster", "ItemID = " + intItemID + "" });
                            intUOMID = Convert.ToInt32(datDefaultUom.Rows[0]["UomID"]);

                        }
                        else
                        {
                            intUOMID = (int)PredefinedUOMs.Numbers;

                        }
                        dgvItemDetailsGrid.Rows[e.RowIndex].Cells["Uom"].Tag = intUOMID;
                        dgvItemDetailsGrid.Rows[e.RowIndex].Cells["Uom"].Value = intUOMID;
                        dgvItemDetailsGrid.Rows[e.RowIndex].Cells["Uom"].Value = dgvItemDetailsGrid.Rows[e.RowIndex].Cells["Uom"].FormattedValue;
                        
                    }
                }
                else if (e.ColumnIndex == Rate.Index)
                {
                    CalculateTotalAmt();
                }
                
                ChangeStatus();
            }
        }
        private void FillParameters()
        {
            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.strDirectDeliveryNo = txtIssueNo.Text;
            //COMMENTED FOR MODIFICATION:DIRECT-OPERATIONTYPEID FROM DB-ADD
            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intOrderTypeID = (Int32)OperationOrderType.DirectDeliveryNote;
            //COMMENTED FOR MODIFICATION:DIRECT-ADD REFERENCE ID-0
            //MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intReferenceID = Convert.ToInt32(cboReferenceNo.SelectedValue);
            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intWarehouseID = Convert.ToInt32(cboWarehouse.SelectedValue);
            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.strIssuedDate = dtpIssuedDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.strRemarks = txtRemarks.Text;
            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intVendorAddID = Convert.ToInt32(MintVendorAddID);
            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intIssuedBy = ClsCommonSettings.UserID;
            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.Lpono = txtLpoNo.Text.Trim();
            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.decNetAmount = txtTotalAmount.Text.Trim();
            

        }

        private void FillDetParameters()
        {
            try
            {
                //dgvItemDetailsGrid.CommitEdit(DataGridViewDataErrorContexts.Commit);
                //if (dgvItemDetailsGrid.CurrentCell != null)
                //    dgvItemDetailsGrid.CurrentCell = dgvItemDetailsGrid["ItemCode", dgvItemDetailsGrid.CurrentRow.Index];
                MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.lstDirectDeliveryDetails = new List<clsDTODirectDeliveryDetails>();

                decimal TotalQtyBatchless = 0;
                int iBatchless = 0;
                decimal AvailbleQtyinBatch = 0;

                DataTable datUsedQtyBatchWise = new DataTable();
                datUsedQtyBatchWise.Columns.Add("ItemID");
                datUsedQtyBatchWise.Columns.Add("BatchID");
                datUsedQtyBatchWise.Columns.Add("TakenQuantity", typeof(decimal));
                datUsedQtyBatchWise.Columns.Add("UOMID");
                    foreach (DataGridViewRow dr in dgvItemDetailsGrid.Rows)
                    {
                        clsDTODirectDeliveryDetails objDtoItemIssueDetails=null;
                        if (dr.Cells["ItemID"].Value != null && (Convert.ToInt32(dr.Cells["BatchID"].Value)!=0) && (Convert.ToBoolean(dr.Cells["IsGroup"].Value)!=true) )
                        {

                            objDtoItemIssueDetails = new clsDTODirectDeliveryDetails();
                            objDtoItemIssueDetails.intItemID = Convert.ToInt32(dr.Cells["ItemID"].Value);
                            objDtoItemIssueDetails.intBatchID = Convert.ToInt32(dr.Cells["BatchID"].Value);
                            //objDtoItemIssueDetails.intReferenceSerialNo = Convert.ToInt32(dr.Cells["ReferenceSerialNo"].Value);
                            objDtoItemIssueDetails.decQuantity = Convert.ToDecimal(dr.Cells["Quantity"].Value);
                            objDtoItemIssueDetails.intUOMID = Convert.ToInt32(dr.Cells["Uom"].Tag);
                            objDtoItemIssueDetails.blnIsGroup = Convert.ToBoolean(dr.Cells["IsGroup"].Value);

                            objDtoItemIssueDetails.decRate = Convert.ToDecimal(dr.Cells["Rate"].Value);
                            objDtoItemIssueDetails.decNetAmount = Convert.ToDecimal(dr.Cells["NetAmount"].Value);
                            //objDtoItemIssueDetails.intBatchless = iBatchless;
                            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intBatchless = iBatchless;
                            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.lstDirectDeliveryDetails.Add(objDtoItemIssueDetails);
                            datUsedQtyBatchWise.Rows.Add();
                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["ItemID"] = Convert.ToInt32(dr.Cells["ItemID"].Value);
                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["BatchID"] = Convert.ToInt32(dr.Cells["BatchID"].Value);
                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["TakenQuantity"] = Convert.ToDecimal(dr.Cells["Quantity"].Value);
                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["UOMID"] = Convert.ToInt32(dr.Cells["Uom"].Tag);

                        }
                        else if (dr.Cells["ItemID"].Value != null && (Convert.ToInt32(dr.Cells["BatchID"].Value) == 0) && (Convert.ToBoolean(dr.Cells["IsGroup"].Value) != true))
                        {
                            decimal QtySelected = 0;
                                iBatchless = iBatchless + 1;
                                //if (!blnIsEditMode)
                                //{
                                    BatchlessItems = MobjClsBLLDirectDelivery.BatchLessItems(Convert.ToInt32(dr.Cells["ItemID"].Value), cboWarehouse.SelectedValue.ToInt32());

                                //}
                                //else
                                //{
                                //    BatchlessItems = MobjClsBLLDirectDelivery.BatchLessItemsEditMode(Convert.ToInt32(dr.Cells["ItemID"].Value), MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID.ToInt32());
                                //}
                                TotalQtyBatchless = Convert.ToDecimal(dr.Cells["Quantity"].Value);

                                foreach (DataRow drBatchLess in BatchlessItems.Rows)
                                {
                                    if (Convert.ToDecimal(dr.Cells["Quantity"].Value) == QtySelected)
                                    {
                                        break;
                                    }
                                    decimal decTakenQuantity = 0;
                                    decimal decAvailableQty = 0;
                                    decAvailableQty = mobjClsBllCommonUtility.ConvertBaseUnitQtyToOtherQty(Convert.ToInt32(dr.Cells["Uom"].Tag), Convert.ToInt32(dr.Cells["ItemID"].Value), Convert.ToDecimal(drBatchLess["AvailableQty"]), 2);
                                    
                                    //Total Qty FROM EACH  Item and Batch;decTakenQuantity
                                    if (datUsedQtyBatchWise != null && datUsedQtyBatchWise.Rows.Count > 0)
                                    {
                                        decTakenQuantity = datUsedQtyBatchWise.Compute("SUM(TakenQuantity)", "ItemID=" + Convert.ToInt32(dr.Cells["ItemID"].Value) + " AND BatchID=" + Convert.ToInt32(drBatchLess["BatchID"]) + "").ToDecimal();
                                    }
                                    //if AvailableQty not equal to 0

                                    if ((decAvailableQty - decTakenQuantity) > 0)
                                    {
                                        //get remaining available qty
                                        decAvailableQty = (decAvailableQty - decTakenQuantity);
                                        objDtoItemIssueDetails = new clsDTODirectDeliveryDetails();
                                        //clsDTOBatchlessItemIssueDetails objBatchless = new clsDTOBatchlessItemIssueDetails();
                                        objDtoItemIssueDetails.intItemID = Convert.ToInt32(dr.Cells["ItemID"].Value);

                                        objDtoItemIssueDetails.intUOMID = Convert.ToInt32(dr.Cells["Uom"].Tag);
                                        objDtoItemIssueDetails.blnIsGroup = Convert.ToBoolean(dr.Cells["IsGroup"].Value);

                                        objDtoItemIssueDetails.decRate = Convert.ToDecimal(dr.Cells["Rate"].Value);
                                        objDtoItemIssueDetails.decNetAmount = Convert.ToDecimal(dr.Cells["NetAmount"].Value);

                                        objDtoItemIssueDetails.intBatchID = Convert.ToInt32(drBatchLess["BatchID"]);
                                        //objBatchless.intBatchless = iBatchless;
                                        MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intBatchless = iBatchless;
                                        AvailbleQtyinBatch = decAvailableQty;


                                        if (AvailbleQtyinBatch < TotalQtyBatchless)
                                        {

                                            QtySelected = QtySelected + AvailbleQtyinBatch;
                                            TotalQtyBatchless = TotalQtyBatchless - AvailbleQtyinBatch;
                                            objDtoItemIssueDetails.decQuantity = AvailbleQtyinBatch;
                                        }
                                        else
                                        {
                                            QtySelected = QtySelected + TotalQtyBatchless;

                                            objDtoItemIssueDetails.decQuantity = TotalQtyBatchless;
                                        }

                                        MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.lstDirectDeliveryDetails.Add(objDtoItemIssueDetails);
                                        //Add selected itemid,batchid and quantity from each batch to the new datatable
                                        datUsedQtyBatchWise.Rows.Add();
                                        datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["ItemID"] = Convert.ToInt32(dr.Cells["ItemID"].Value);
                                        datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["BatchID"] = Convert.ToInt32(drBatchLess["BatchID"]);
                                        datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["TakenQuantity"] = objDtoItemIssueDetails.decQuantity;
                                        datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["UOMID"] = Convert.ToInt32(dr.Cells["Uom"].Tag);
                                    }
                                }
                                
                            
                        }
                         else if (dr.Cells["ItemID"].Value != null && (Convert.ToInt32(dr.Cells["BatchID"].Value)==0) && (Convert.ToBoolean(dr.Cells["IsGroup"].Value)==true) )
                        {

                            objDtoItemIssueDetails = new clsDTODirectDeliveryDetails();
                            objDtoItemIssueDetails.intItemID = Convert.ToInt32(dr.Cells["ItemID"].Value);
                            objDtoItemIssueDetails.intBatchID = Convert.ToInt32(dr.Cells["BatchID"].Value);
                            //objDtoItemIssueDetails.intReferenceSerialNo = Convert.ToInt32(dr.Cells["ReferenceSerialNo"].Value);
                            objDtoItemIssueDetails.decQuantity = Convert.ToDecimal(dr.Cells["Quantity"].Value);
                            objDtoItemIssueDetails.intUOMID = Convert.ToInt32(dr.Cells["Uom"].Tag);
                            objDtoItemIssueDetails.blnIsGroup = Convert.ToBoolean(dr.Cells["IsGroup"].Value);

                            objDtoItemIssueDetails.decRate = Convert.ToDecimal(dr.Cells["Rate"].Value);
                            objDtoItemIssueDetails.decNetAmount = Convert.ToDecimal(dr.Cells["NetAmount"].Value);
                            //objDtoItemIssueDetails.intBatchless = iBatchless;
                            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intBatchless = iBatchless;
                           // MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.lstDirectDeliveryDetails.Add(objDtoItemIssueDetails);


                            if (objDtoItemIssueDetails.blnIsGroup)
                            {
                                dtGroupDetailsI = MobjClsBLLDirectDelivery.GetGroupDetails(objDtoItemIssueDetails.intItemID);
                                //MDtItemGroupIssueDetails.DefaultView.RowFilter = "ItemGroupID = " + objDtoItemIssueDetails.intItemID + " And ReferenceNo = " + MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intReferenceID + " And ReferenceSerialNo = " + objDtoItemIssueDetails.intReferenceSerialNo;
                                objDtoItemIssueDetails.lstItemGroupIssueDetailsDN = new List<clsDTOItemGroupIssueDetailsDN>();
                                foreach (DataRow drRow in dtGroupDetailsI.Rows)
                                {
                                    clsDTOItemGroupIssueDetailsDN objItemIssueDetails = new clsDTOItemGroupIssueDetailsDN();
                                    objItemIssueDetails.intItemID = Convert.ToInt32(drRow["ItemID"]);
                                    BatchlessItems = MobjClsBLLDirectDelivery.BatchLessItems(Convert.ToInt32(drRow["ItemID"]), cboWarehouse.SelectedValue.ToInt32());

                                    TotalQtyBatchless = (Convert.ToDecimal(drRow["Quantity"])) *( Convert.ToDecimal(dr.Cells["Quantity"].Value));
                                    decimal QtySelectedGrp = 0;
                                    foreach (DataRow drBatchLess in BatchlessItems.Rows)
                                    {
                                        if (((Convert.ToDecimal(drRow["Quantity"])) * (Convert.ToDecimal(dr.Cells["Quantity"].Value))) == QtySelectedGrp)
                                        {
                                            break;
                                        }
                                        decimal decTakenQuantityG = 0;
                                        if (datUsedQtyBatchWise != null && datUsedQtyBatchWise.Rows.Count > 0)
                                        {
                                            //Total Qty FROM EACH  Item and Batch;decTakenQuantity
                                            decTakenQuantityG = datUsedQtyBatchWise.Compute("SUM(TakenQuantity)", "ItemID=" + Convert.ToInt32(drBatchLess["ItemID"]) + " AND BatchID=" + Convert.ToInt32(drBatchLess["BatchID"]) + "").ToDecimal();

                                        }
                                        //decTakenQuantityG=datUsedQtyBatchWise.Compute("SUM(TakenQuantity)", "ItemID=" + Convert.ToInt32(dr.Cells["ItemID"].Value) + " AND BatchID=" + Convert.ToInt32(drBatchLess["BatchID"]) + "").ToDecimal();
                                        //if AvailableQty not equal to 0
                                        if ((Convert.ToDecimal(drBatchLess["AvailableQty"]) - decTakenQuantityG) > 0)
                                        {
                                            drBatchLess["AvailableQty"] = (Convert.ToDecimal(drBatchLess["AvailableQty"]) - decTakenQuantityG);
                                            objItemIssueDetails = new clsDTOItemGroupIssueDetailsDN();
                                            //clsDTOBatchlessItemIssueDetails objBatchless = new clsDTOBatchlessItemIssueDetails();
                                            objItemIssueDetails.intItemID = Convert.ToInt32(drBatchLess["ItemID"]);

                                            objItemIssueDetails.intUOMID = Convert.ToInt32(drRow["UOMID"]);



                                            objItemIssueDetails.intBatchID = Convert.ToInt32(drBatchLess["BatchID"]);

                                            AvailbleQtyinBatch = Convert.ToDecimal(drBatchLess["AvailableQty"]);


                                            if (AvailbleQtyinBatch < TotalQtyBatchless)
                                            {

                                                QtySelectedGrp = QtySelectedGrp + AvailbleQtyinBatch;
                                                TotalQtyBatchless = TotalQtyBatchless - AvailbleQtyinBatch;
                                                objItemIssueDetails.decQuantity = AvailbleQtyinBatch;
                                            }
                                            else
                                            {
                                                QtySelectedGrp = QtySelectedGrp + TotalQtyBatchless;

                                                objItemIssueDetails.decQuantity = TotalQtyBatchless;
                                            }
                                            //decimal decTakenQty = 0;
                                            //decTakenQty = mobjClsBllCommonUtility.ConvertBaseUnitQtyToOtherQty(Convert.ToInt32(dr.Cells["Uom"].Tag), Convert.ToInt32(drBatchLess["ItemID"]), objItemIssueDetails.decQuantity, 2);
                                            objDtoItemIssueDetails.lstItemGroupIssueDetailsDN.Add(objItemIssueDetails);
                                            //Add selected itemid,batchid and quantity from each batch to the new datatable
                                            datUsedQtyBatchWise.Rows.Add();
                                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["ItemID"] = Convert.ToInt32(drBatchLess["ItemID"]);
                                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["BatchID"] = Convert.ToInt32(drBatchLess["BatchID"]);
                                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["TakenQuantity"] = objItemIssueDetails.decQuantity;
                                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["UOMID"] = Convert.ToInt32(drRow["UOMID"]);
                                        }
                                    }



                                    //MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.lstDirectDeliveryDetails.Add(objDtoItemIssueDetails); 
                                   
                                }
                            }
                            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.lstDirectDeliveryDetails.Add(objDtoItemIssueDetails); 
                       }

                      
                    }
                
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FillDetParameters() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in FillDetParameters() " + ex.Message, 2);
            }
        }

        private void ClearControls()
        {
            cboCompany.Enabled = true;
            MblnIsFromClear = true;
            MDtItemGroupIssueDetails.Rows.Clear();
            //if (!blnIsEditMode)
            //{
            cboCompany.SelectedIndex = -1;
            cboWarehouse.SelectedIndex = -1;
            //cboReferenceNo.SelectedIndex = -1;

            
            cboWarehouse.Text =  "";
            txtLpoNo.Text = "";
            txtSubTotal.Text = "";
            txtTotalAmount.Text = "";
            txtExchangeCurrencyRate.Text = "";
            lblAmountinWords.Text = "";

            //}
            dgvItemDetailsGrid.Rows.Clear();
            dtpIssuedDate.Value = ClsCommonSettings.GetServerDate();


            cboSCompany.SelectedIndex = -1;
           //cboSOperationType.SelectedIndex = -1;
            cboSWarehouse.SelectedIndex = -1;
            cboSIssuedBy.SelectedIndex = -1;
            cboSCompany.Text  = cboSWarehouse.Text = cboSIssuedBy.Text = "";

       
                //cboOperationType.SelectedValue = (int)OperationOrderType.DNOTSalesInvoice;
               // cboSOperationType.SelectedValue = (int)OperationOrderType.DNOTSalesInvoice;

            txtVendorAddress.Text = "";

            //dgvItemDetailsGrid.ReadOnly = false;
            dtpIssuedDate.Enabled = true;
            txtRemarks.Enabled = true;
            //cboCompany.Text = "";

            dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;

            txtRemarks.Clear();
            cboCompany.Focus();

            //dgvItemDetailsGrid.ReadOnly = false;
           // dgvItemDetailsGrid.Columns["InvoicedQuantity"].ReadOnly = true;
           dgvItemDetailsGrid.Columns["QtyAvailable"].ReadOnly = true;
          dgvItemDetailsGrid.Columns["BatchNo"].ReadOnly = true;

            MblnIsFromClear = false;
            bnDelete.Enabled = false;
            bnSaveItem.Enabled = false;
            bnEmail.Enabled = false;
            bnAddNewItem.Enabled = false;
            bnPrint.Enabled = false;
            btnPicklist.Enabled = false;
            btnPickListEmail.Enabled = false;
            MchangeStatus = false;
            bnClear.Enabled = false;
            //dgvItemDetailsGrid.ReadOnly = false;
            //cboOperationType.Enabled = true;
            //cboReferenceNo.Enabled = true;
            cboWarehouse.Enabled = true;
            blnIsEditMode = false;
            btnCustomerwisePrint.Enabled = false;
            btnActions.Enabled = false;

            IsMessageBoxShown = false;
        }

        private void ChangeStatus()
        {
            if (!blnIsEditMode)
                bnSaveItem.Enabled = MblnAddPermission;
            else
            {
                if(MblnIsEditable)
                bnSaveItem.Enabled = MblnUpdatePermission;
                bnAddNewItem.Enabled = MblnAddPermission;
            }
            ErrItemIssue.Clear();
            lblStatus.Text = "";
            MchangeStatus = true;
            bnClear.Enabled = true;
        }

        private void AddNewItemIssue()
        {
            try
            {
                dtItemDetails = new DataTable();
                
                dtItemDetails.Columns.Add("ItemCode");
                dtItemDetails.Columns.Add("ItemName");
                dtItemDetails.Columns.Add("ItemID");
                dtItemDetails.Columns.Add("BatchID");
                dtItemDetails.Columns.Add("BatchNo");
                dtItemDetails.Columns.Add("UOMID");
                ClearControls();
                dgvItemDetailsGrid.Visible = true;
                blnIsEditMode = false;
                MblnIsEditable = true;
                MobjClsBLLDirectDelivery = new clsBLLDirectDelivery(); ;
                //MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intCompanyID = ClsCommonSettings.CompanyID;
                MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intIssuedBy = ClsCommonSettings.UserID;
                lblIssuedBy.Text = ClsCommonSettings.strEmployeeName;
                //  txtIssueNo.Text = ClsCommonSettings.ItemIssuePrefix + MobjClsBLLDirectDelivery.GetNextIssueNo().ToString();
                GenerateItemIssueNo();
                lblDirectDNstatus.Text = "New";
                //cboSIssuedBy.SelectedValue = ClsCommonSettings.intEmployeeID;
                //cboSCompany.SelectedValue = ClsCommonSettings.CompanyID;
                //cboSOperationType.SelectedIndex = -1;
                //cboSWarehouse.SelectedIndex = -1;

                panelMiddle.Visible = true;
                expandableSplitterTop.Visible = true;

                DisplayAllIssueNos();
                bnDelete.Enabled = false;
                dgvItemDetailsGrid.ReadOnly = false;
                //cboOperationType.Enabled = true;
                //cboReferenceNo.Enabled = true;
                cboWarehouse.Enabled = true;
                cboCustomer.Enabled = true;
                bnSaveItem.Enabled = false;
                bnEmail.Enabled = false;
                bnAddNewItem.Enabled = false;
                bnPrint.Enabled = false;
                btnPicklist.Enabled = false;
                MchangeStatus = false;
                bnClear.Enabled = false;
                cboCustomer.SelectedIndex = -1;

                //if (PintFormType == 1)
                //    cboOperationType.SelectedValue = (Int32)OperationOrderType.DNOTSalesInvoice;
                //else
                //    cboOperationType.SelectedValue = (Int32)OperationOrderType.DNOTDMR;
                if (PintFormType == 1)
                    lblStatus.Text = "Add New Delivery Note";
                else
                    lblStatus.Text = "Add new material issue";

                lblDirectDNstatus.ForeColor = Color.Black;

                ErrItemIssue.Clear();
                cboCompany.Focus();

                Uom.ReadOnly = false;
                btnSampleIssue.Enabled = false;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in AddNewItemIssue() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in AddNewItemIssue() " + ex.Message, 2);
            }
        }

        private void FrmItemIssue_Shown(object sender, EventArgs e)
        {
            cboCompany.Focus();
        }

        private void bnSaveItem_Click(object sender, EventArgs e)
        {
      //      tcDeliveryNote.SelectedTab = tiLocationDetails;
          //  tcDeliveryNote.SelectedTab = tiItemDetails;
            try
            {
                // Saving Information
               
               
                if (  FillItemLocationDetails() && ValidateFields())
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID == 0 ? 1 : 3), out MmsgMessageIcon);

                    if (!IsMessageBoxShown)
                    {
                        if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                            MessageBoxIcon.Information) == DialogResult.No)
                            return;
                    }

                    if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID == 0 && txtIssueNo.ReadOnly && MobjClsBLLDirectDelivery.FillCombos(new string[] { "ItemIssueNo", "InvItemIssueMaster", "ItemIssueNo = '" + txtIssueNo.Text + "' And ItemIssueID <> " + MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID + " And CompanyID = "+cboCompany.SelectedValue.ToInt32() }).Rows.Count > 0)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 9002, out MmessageIcon);
                        MstrCommonMessage = MstrCommonMessage.Replace("*", "Item Issue");
                        if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            return ;
                        else
                            GenerateItemIssueNo();
                    }
                //COMMENTED FOR direct---

                    //if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice)
                    //{
                    //    if (MobjClsBLLDirectDelivery.GetBalanceAmount(cboReferenceNo.SelectedValue.ToInt64()) > 0)
                    //    {
                    //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4250, out MmsgMessageIcon);
                    //        MstrCommonMessage = MstrCommonMessage.Replace("#", "");
                    //        if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    //        {
                    //            return;
                    //        }                           
                    //    }
                    //}
                    if (blnIsEditMode)
                    {
                        MobjClsBLLDirectDelivery.DeleteItemGroupIssueDetails();
                        MobjClsBLLDirectDelivery.DeleteItemIssueDetails();
                        
                    }
                    FillParameters();
                    FillDetParameters();
                    FillLocationDetailsParameters();
                    try
                    {
                        if (MobjClsBLLDirectDelivery.SaveItemIssue())
                        {
                            //Delivery note alert
                            //commeneted for modification:DIRECT
                            //if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice)
                            //{
                                //objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.DeliveryNote;
                                //objFrmTradingMain.tmrAlert.Enabled = true;
                            //}

                            AddNewItemIssue();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        MobjClsLogWriter.WriteLog("Error on save function()" + this.Name + " " + ex.Message, 2);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnSaveItem_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in bnSaveItem_Click() " + ex.Message, 2);
            }
        }

        private bool ValidateFields()
        {
            try
            {
                // Validating datas
                MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                if (dgvItemDetailsGrid.CurrentCell != null)
                    dgvItemDetailsGrid.CurrentCell = dgvItemDetailsGrid["ItemCode", dgvItemDetailsGrid.CurrentRow.Index];
                bool blnValid = true;
                Control cntrlFocus = null;
                ErrItemIssue.Clear();

                if (!txtIssueNo.ReadOnly && MobjClsBLLDirectDelivery.FillCombos(new string[] { "ItemIssueNo", "InvItemIssueMaster", "ItemIssueNo = '" + txtIssueNo.Text + "' And ItemIssueID <> "+MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID+" And CompanyID = "+cboCompany.SelectedValue.ToInt32() }).Rows.Count > 0)
                {
                    //This ItemIssue No Already Exists
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4224, out MmsgMessageIcon);
                    cntrlFocus = txtIssueNo;
                    blnValid = false;
                }
                else if (cboWarehouse.SelectedValue == null)
                {
                    //Please select Warehouse
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4203, out MmsgMessageIcon);
                    cntrlFocus = cboWarehouse;
                    blnValid = false;
                }
                else if (cboCustomer.SelectedValue == null)
                {
                    //Please select customer
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4256, out MmsgMessageIcon);
                    cntrlFocus = cboCustomer;
                    blnValid = false;
                }
                else if (cboCompany.SelectedValue == null)
                {
                    //Please select customer
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4263, out MmsgMessageIcon);
                    cntrlFocus = cboCompany;
                    blnValid = false;
                }
                else if (txtIssueNo.Text == "")
                {
                    //Please select customer
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4265, out MmsgMessageIcon);
                    cntrlFocus = txtIssueNo;
                    blnValid = false;
                }   
//commented:direct
               
                //else if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID == 0 && Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesInvoice)
                //{
                    //if (MobjClsBLLDirectDelivery.FillCombos(new string[] { "SalesInvoiceID", "InvSalesInvoiceMaster", "SalesInvoiceID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) + " And StatusID = " + (Int32)OperationStatusType.SInvoiceOpen }).Rows.Count == 0)
                    //{
                    //    // Please select opened sales invoice
                    //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4223, out MmsgMessageIcon);
                    //    MstrCommonMessage = MstrCommonMessage.Replace("*", "Sales Invoice");
                    //    cntrlFocus = cboReferenceNo;
                    //    blnValid = false;
                    //}
                    //if (MobjClsBLLDirectDelivery.FillCombos(new string[] { "SalesReturnID", "InvSalesReturnMaster", "SalesInvoiceID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) }).Rows.Count > 0)
                    //{
                    //    // Sales return have been done
                    //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4225, out MmsgMessageIcon);
                    //    MstrCommonMessage = MstrCommonMessage.Replace("*", " this Sales Invoice");
                    //    cntrlFocus = cboReferenceNo;
                    //    blnValid = false;
                    //}
                //}
                //direct-
                //else if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID == 0 && cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTTransfer)
                //{
                //    if (MobjClsBLLDirectDelivery.FillCombos(new string[] { "StockTransferID", "InvStockTransferMaster", "StockTransferID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.STSubmitted + "," + (int)OperationStatusType.STApproved + ")" }).Rows.Count == 0)
                //    {
                //        // Please select approved stock transfer
                //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4241, out MmsgMessageIcon);
                //        if (ClsCommonSettings.StockTransferApproval)
                //            MstrCommonMessage = MstrCommonMessage.Replace("*", "approved");
                //        else
                //            MstrCommonMessage = MstrCommonMessage.Replace("*", "Submitted");
                //        cntrlFocus = cboReferenceNo;
                //        blnValid = false;
                //    }
                //}
                //else if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID == 0 && cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTDMR)
                //{
                //    if (MobjClsBLLDirectDelivery.FillCombos(new string[] { "DMRID", "PrdDMRMaster", "DMRID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.DMROpened + ")" }).Rows.Count == 0)
                //    {
                //        // Please select opened DMR No
                //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4246, out MmsgMessageIcon);

                //        MstrCommonMessage = MstrCommonMessage.Replace("*", "Opened");
                //        blnValid = false;
                //    }
                //}


                else if (blnValid)
                {
                    int iTempID = 0;
                    iTempID = CheckDuplicationInGrid();
                    if (iTempID != -1)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4243, out MmessageIcon);
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        tmrItemIssue.Enabled = true;
                        dgvItemDetailsGrid.CurrentCell = dgvItemDetailsGrid["ItemCode", iTempID];
                        dgvItemDetailsGrid.Focus();
                        return false;
                    }
                }

                if (!blnValid)
                {
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    ErrItemIssue.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                    tmrItemIssue.Enabled = true;
                    tcDeliveryNote.SelectedTab = tiItemDetails;
                    cntrlFocus.Focus();
                }
                    //commenetd for mofif
                else if (!ValidateItemIssueGrid())
                {
                    blnValid = false;
                }
                else if (ClsCommonSettings.PblnIsLocationWise)
                {
                    if (!ValidateLocationGrid() || !CheckLocationDuplication() || !ValidateLocationQuantity())
                        blnValid = false;
                }
                return blnValid;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in ValidateFields() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in ValidateFields() " + ex.Message, 2);
                return false;
            }
        }
        private int CheckDuplicationInGrid()
        {
            int intItemID = 0; bool blnIsGroup = false;
            long lngBatchID = 0;
            int RowIndexTemp = 0;
            bool blnCheckBatchID = false;

            foreach (DataGridViewRow rowValue in dgvItemDetailsGrid.Rows)
            {
                if (rowValue.Cells["ItemID"].Value != null)
                {
                    intItemID = Convert.ToInt32(rowValue.Cells["ItemID"].Value);
                    blnIsGroup = Convert.ToBoolean(rowValue.Cells["IsGroup"].Value);
                    lngBatchID = rowValue.Cells["BatchID"].Value.ToInt64();
                    RowIndexTemp = rowValue.Index;
                    foreach (DataGridViewRow row in dgvItemDetailsGrid.Rows)
                    {
                        if (RowIndexTemp != row.Index)
                        {
                            if (row.Cells["ItemID"].Value != null && row.Cells["BatchID"].Value.ToInt64() != 0)
                            {
                                if ((Convert.ToInt32(row.Cells["ItemID"].Value) == intItemID) && (row.Cells["BatchID"].Value.ToInt64() == lngBatchID))
                                    return row.Index;
                            }
                            else if (row.Cells["ItemID"].Value != null)
                            {
                                if ((Convert.ToInt32(row.Cells["ItemID"].Value) == intItemID) && Convert.ToBoolean(row.Cells["IsGroup"].Value) == blnIsGroup)
                                    return row.Index;
                            }
                        }
                    }
                }
            }
            return -1;
        }
        private decimal ConvertValue(decimal decValue, int intUomID, int intItemID)
        {
            DataTable dtUomDetails = MobjClsBLLDirectDelivery.GetUomConversionValues(intUomID, intItemID);
            if (dtUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                    decValue = decValue / decConversionValue;
                else if (intConversionFactor == 2)
                    decValue = decValue * decConversionValue;
            }
            return decValue;
        }

        private bool ValidateItemIssueGrid()
        {
            bool blnIsValid = true;
            int intItemCount = 0, intRowIndex = 0;
            string strColumnName = "";
            IsMessageBoxShown = false;
            DataTable dtOldItemIssueDetails = new DataTable();
        //    //DataTable datItemDetails = MobjClsBLLDirectDelivery.GetItemDetails(cboOperationType.SelectedValue.ToInt32(),cboReferenceNo.SelectedValue.ToInt32());

            if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID != 0)
                dtOldItemIssueDetails = MobjClsBLLDirectDelivery.GetItemIssueDetails();
            List<int> lstSelectedItems = new List<int>();
            List<int> lstCheckedSerialNos = new List<int>();
            for (int i = 0; i < dgvItemDetailsGrid.Rows.Count; i++)
            {
                if (dgvItemDetailsGrid["ItemID", i].Value != null)
                {
                    if (!lstSelectedItems.Contains(dgvItemDetailsGrid["ItemID", i].Value.ToInt32()))
                        lstSelectedItems.Add(dgvItemDetailsGrid["ItemID", i].Value.ToInt32());
                    if (dgvItemDetailsGrid["Quantity", i].Value == null || Convert.ToDecimal(dgvItemDetailsGrid["Quantity", i].Value) == 0)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4215, out MmsgMessageIcon);
                        intRowIndex = i;
                        strColumnName = "Quantity";
                        blnIsValid = false;
                    }
                    else if (dgvItemDetailsGrid["Uom", i].Tag == null)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4209, out MmsgMessageIcon);
                        intRowIndex = i;
                        strColumnName = "Uom";
                        blnIsValid = false;
                    }
                    //else if (dgvItemDetailsGrid["BatchID", i].Value.ToInt32() == 0)//&& (Convert.ToBoolean(dgvItemDetailsGrid["IsGroup", i].Value) == false))//&& dgvItemDetailsGrid["IsFromStore",i].Value.ToBoolean() ==true)//&& ((cboOperationType.SelectedValue.ToInt32 () != (int)OperationOrderType.DNOTSalesInvoice) || ((cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice && MobjClsBLLDirectDelivery.GetJobOrderIDAgainstInvoice(cboReferenceNo.SelectedValue.ToInt64()) == 0))))
                    //{

                    //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4220, out MmessageIcon);
                    //    intRowIndex = i;
                    //    strColumnName = "BatchNo";
                    //    blnIsValid = false;
                    //}
                   

                    else if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID == 0)
                    {
                        if (dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal() > dgvItemDetailsGrid.Rows[i].Cells[QtyAvailable.Index].Value.ToDecimal())
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4252, out MmsgMessageIcon);
                            intRowIndex = i;
                            strColumnName = "Quantity";
                            blnIsValid = false;
                        }
                    }
                    
                    intItemCount++;
                }
                if (!blnIsEditMode)
                {
                    if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID == 0)
                    {
                        if (dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal() > dgvItemDetailsGrid.Rows[i].Cells[QtyAvailable.Index].Value.ToDecimal())
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4257, out MmsgMessageIcon);
                            intRowIndex = i;
                            strColumnName = "Quantity";
                            blnIsValid = false;
                        }
                    }
                    if (Convert.ToBoolean(dgvItemDetailsGrid["IsGroup", i].Value) == true)
                    {
                        DataTable datIsGroup = null;
                        datIsGroup = MobjClsBLLDirectDelivery.GetQtyInGroup(dgvItemDetailsGrid["ItemID", i].Value.ToInt32());

                        for (int intICounter2 = 0; intICounter2 < datIsGroup.Rows.Count; intICounter2++)
                        {
                            int intItemID = Convert.ToInt32(datIsGroup.Rows[intICounter2]["ItemID"]);
                            decimal QtyInGrp = datIsGroup.Rows[intICounter2]["Quantity"].ToDecimal();
                            decimal TotalQtyInGrp = QtyInGrp * dgvItemDetailsGrid["Quantity", i].Value.ToInt32();


                            for (int iCounter3 = 0; iCounter3 < dgvItemDetailsGrid.Rows.Count; iCounter3++)
                            {
                                decimal decStock = 0;
                                if ((intItemID == dgvItemDetailsGrid.Rows[iCounter3].Cells[ItemID.Index].Value.ToInt32()) &&  (dgvItemDetailsGrid.Rows[iCounter3].Cells[IsGroup.Index].Value.ToBoolean()!=true))
                                {
                                    decStock = MobjClsBLLDirectDelivery.GetStockQuantity(dgvItemDetailsGrid.Rows[iCounter3].Cells[ItemID.Index].Value.ToInt32(), 0, cboCompany.SelectedValue.ToInt32(), cboWarehouse.SelectedValue.ToInt32());
                                    if (TotalQtyInGrp + dgvItemDetailsGrid.Rows[iCounter3].Cells[Quantity.Index].Value.ToInt32() > decStock)
                                    {
                                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4264, out MmsgMessageIcon).Replace("*", dgvItemDetailsGrid.Rows[iCounter3].Cells[ItemName.Index].Value.ToString());
                                        //MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4215, out MmsgMessageIcon);
                                        intRowIndex = i;
                                        strColumnName = "ItemCode";
                                        blnIsValid = false;
                                    }
                                }
                            }

                        }
                    }


                }
                else
                {
                    decimal decOldQuantityDelivered = 0;
                    if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID != 0)
                    {
                        dtOldItemIssueDetails.DefaultView.RowFilter = "ItemID = " + Convert.ToInt32(dgvItemDetailsGrid["ItemID", i].Value) + " And BatchID = " + Convert.ToInt32(dgvItemDetailsGrid["BatchID", i].Value) + "";
                        foreach (DataRow dr in dtOldItemIssueDetails.DefaultView.ToTable().Rows)
                        {
                            decOldQuantityDelivered += ConvertValue(Convert.ToDecimal(dr["Quantity"]), Convert.ToInt32(dr["UomID"]), Convert.ToInt32(dr["ItemID"]));
                        }
                    }
                    if (!IsSalesInvoiceExists() || !IsSalesOrderExists())
                    {
                        //MstrCommonMessage = "Reference Exists .";
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4262, out MmsgMessageIcon);
                        intRowIndex = i;
                        strColumnName = "ItemCode";
                        blnIsValid = false;

                        
                    }
                    if (dgvItemDetailsGrid.Rows[i].Cells[QtyAvailable.Index].Value.ToDecimal() == 0)
                    {
                        if ((dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal() > (dgvItemDetailsGrid.Rows[i].Cells[QtyAvailable.Index].Value.ToDecimal()+decOldQuantityDelivered))&&(dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()!=decOldQuantityDelivered))
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4257, out MmsgMessageIcon);
                            intRowIndex = i;
                            strColumnName = "Quantity";
                            blnIsValid = false;
                        }
                    }
                    
                    else if (dgvItemDetailsGrid.Rows[i].Cells[QtyAvailable.Index].Value.ToDecimal()>0)
                    {
                       
                        if (dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal() > (decOldQuantityDelivered + dgvItemDetailsGrid.Rows[i].Cells[QtyAvailable.Index].Value.ToDecimal()))
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4257, out MmsgMessageIcon);
                            intRowIndex = i;
                            strColumnName = "Quantity";
                            blnIsValid = false;
                        }
                        //else if (dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()<decOldQuantityDelivered)
                        //{
                        //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4258, out MmsgMessageIcon);
                        //    intRowIndex = i;
                        //    strColumnName = "Quantity";
                        //    blnIsValid = true;
                        //}
                    }
                }
            }
            if (intItemCount == 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4204, out MmsgMessageIcon);
                intRowIndex = 0;
                strColumnName = "ItemCode";
                blnIsValid = false;
            }
            //else if (blnIsValid)
            //{
            //    string strItemName = "";
            //    foreach (DataRow dr in datItemDetails.Rows)
            //    {
            //        if (!lstSelectedItems.Contains(dr["ItemID"].ToInt32()))
            //        {
            //            strItemName = dr["ItemName"].ToString();
            //            break;
            //        }
            //    }
            //    if (!string.IsNullOrEmpty(strItemName))
            //    {
            //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4244, out MmessageIcon).Replace("*", strItemName);
            //        strColumnName = "ItemCode";
            //        intRowIndex = dgvItemDetailsGrid.RowCount - 1;
            //        blnIsValid = false;
            //    }
            //}
            if (!blnIsValid)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                ErrItemIssue.SetError(dgvItemDetailsGrid, MstrCommonMessage.Replace("#", "").Trim());
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrItemIssue.Enabled = true;
                tcDeliveryNote.SelectedTab = tiItemDetails;
                dgvItemDetailsGrid.CurrentCell = dgvItemDetailsGrid[strColumnName, intRowIndex];
                dgvItemDetailsGrid.Focus();
            }
            else
            blnIsValid =  ValidateCutOffPrice();

            return blnIsValid;
        }

        private bool ValidateCutOffPrice()
        {

            /* Validation for Sale Rate > CutOff price*/

            for (int i = 0; i < dgvItemDetailsGrid.Rows.Count - 1; i++)
            {
                decimal decCurrentSalesRate = 0;
                decimal decCutoffRate = mobjClsBllCommonUtility.GetCutOffPrice(dgvItemDetailsGrid.Rows[i].Cells[ItemID.Index].Value.ToInt32());
                DataTable dtTemp = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Rate", "InvItemIssueDetails", "ItemIssueID = " + MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID + " AND ItemID =" + dgvItemDetailsGrid.Rows[i].Cells[ItemID.Index].Value.ToInt32() + " AND IsGroup = '" + dgvItemDetailsGrid.Rows[i].Cells[IsGroupItem.Index].Value.ToBoolean() + "'" });

                if (dtTemp != null && dtTemp.Rows.Count > 0)
                    decCurrentSalesRate = dtTemp.Rows[0]["Rate"].ToDecimal();

                if (decCurrentSalesRate == 0)
                {
                    if (decCutoffRate > (dgvItemDetailsGrid.Rows[i].Cells[NetAmount.Index].Value.ToDecimal() / dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()))
                    {
                        dgvItemDetailsGrid.Rows[i].Cells[Rate.Index].Style.ForeColor = Color.Red;
                        MstrCommonMessage = "Sale Rate Less Than Cut Off Rate. Do you Wish To Save ?";
                        IsMessageBoxShown = true;
                        dgvItemDetailsGrid.Focus();
                        dgvItemDetailsGrid.CurrentCell = dgvItemDetailsGrid[Rate.Index, i];
                        if ((MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " (" + decCutoffRate.ToString() + ")", ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.No)
                            return false;
                        else
                            return true;
                    }
                    else
                    {
                        dgvItemDetailsGrid.Rows[i].Cells[Rate.Index].Style.ForeColor = Color.Black;
                    }
                }
                else
                {
                    if (dgvItemDetailsGrid.Rows[i].Cells[Rate.Index].Value.ToDecimal() > decCutoffRate || dgvItemDetailsGrid.Rows[i].Cells[Rate.Index].Value.ToDecimal() == decCurrentSalesRate)
                    {
                        dgvItemDetailsGrid.Rows[i].Cells[Rate.Index].Style.ForeColor = Color.Black;
                    }
                    else
                    {
                        dgvItemDetailsGrid.Rows[i].Cells[Rate.Index].Style.ForeColor = Color.Red;
                        MstrCommonMessage = "Sale Rate Less Than Cut Off Rate. Do you Wish To Save ?";
                        IsMessageBoxShown = true;
                        dgvItemDetailsGrid.Focus();
                        dgvItemDetailsGrid.CurrentCell = dgvItemDetailsGrid[Rate.Index, i];
                        if ((MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim() + " (" + decCutoffRate.ToString() + ")", ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.No)
                            return false;
                        else
                            return true;
                    }
                }
            }
            return true;
        }
        //private bool ValidateStockQuantity(int intItemID, int intBatchID, decimal decQuantity, int intUomID)
        //{
        //    decimal decStockQuantity = MobjClsBLLDirectDelivery.GetStockQuantity(intItemID, intBatchID, MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intCompanyID, ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy"), Convert.ToInt32(cboWarehouse.SelectedValue));
        //    if (intUomID != 0)
        //    {
        //        DataTable dtUomDetails = MobjClsBLLDirectDelivery.GetUomConversionValues(intUomID, intItemID);
        //        if (dtUomDetails.Rows.Count > 0)
        //        {
        //            int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
        //            decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
        //            if (intConversionFactor == 1)
        //                decQuantity = decQuantity / decConversionValue;
        //            else if (intConversionFactor == 2)
        //                decQuantity = decQuantity * decConversionValue;
        //        }
        //    }
        //    if (decQuantity > decStockQuantity)
        //    {
        //        return false;
        //    }
        //    return true;
        //}

        private void DisplayAllIssueNos()
        {
            //Function for displaying all schedule Nos Filter By Condition
            string strCondition = string.Empty;

            if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
            {
                strCondition = "ItemIssueNo = '" + txtSearch.Text + "'";
            }
            else
            {
                if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                {
                    strCondition += "CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue) + "";
                }
                else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    DataTable dtCompany = (DataTable)cboSCompany.DataSource;

                    if (dtCompany.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(strCondition))
                            strCondition += " And ";
                        strCondition += "CompanyID In (";
                        foreach (DataRow dr in dtCompany.Rows)
                            strCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                        strCondition = strCondition.Remove(strCondition.Length - 1);
                        strCondition += ")";
                    }
                }

                if (cboSWarehouse.SelectedValue != null && Convert.ToInt32(cboSWarehouse.SelectedValue) != -2)
                {
                    if (!string.IsNullOrEmpty(strCondition))
                        strCondition += " And ";
                    strCondition += "WarehouseID = " + Convert.ToInt32(cboSWarehouse.SelectedValue) + "";
                }
                else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                    {
                        DataTable dtWarehouse = (DataTable)cboSWarehouse.DataSource;

                        if (dtWarehouse.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(strCondition))
                                strCondition += " And ";
                            strCondition += "WarehouseID In (";
                            foreach (DataRow dr in dtWarehouse.Rows)
                                strCondition += Convert.ToInt32(dr["WarehouseID"]) + ",";
                            strCondition = strCondition.Remove(strCondition.Length - 1);
                            strCondition += ")";
                        }
                    }
                }

                if (cboSIssuedBy.SelectedValue != null && Convert.ToInt32(cboSIssuedBy.SelectedValue) != -2)
                {
                    if (!string.IsNullOrEmpty(strCondition))
                        strCondition += " And ";
                    strCondition += "IssuedBy = " + Convert.ToInt32(cboSIssuedBy.SelectedValue) + "";
                }
                //else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                //{
                //    //if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                //    //{
                //        DataTable dtEmployee = (DataTable)cboSIssuedBy.DataSource;

                //        if (dtEmployee.Rows.Count > 0)
                //        {
                //            if (!string.IsNullOrEmpty(strCondition))
                //                strCondition += " And ";

                //            //clsBLLPermissionSettings MobjClsBLLPermissionSettings = new clsBLLPermissionSettings();
                //            //DataTable datTempPer = MobjClsBLLPermissionSettings.GetControlPermissions1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.DelNoteEmployee);
                //            //if (datTempPer != null)
                //            //{
                //            //    if (datTempPer.Rows.Count > 0)
                //            //    {
                //            //        if (datTempPer.Rows[0]["IsVisible"].ToString() == "True")
                //            //            strCondition += "EmployeeID In (0,";
                //            //        else
                //            //            strCondition += "EmployeeID In (";
                //            //    }
                //            //    else
                //            //        strCondition += "EmployeeID In (";
                //            //}
                //            //else
                //                strCondition += "EmployeeID In (";

                //            foreach (DataRow dr in dtEmployee.Rows)
                //                strCondition += Convert.ToInt32(dr["EmployeeID"]) + ",";
                //            strCondition = strCondition.Remove(strCondition.Length - 1);
                //            strCondition += ")";
                //        }
                //    //}
                //}
                if (!string.IsNullOrEmpty(strCondition))
                    strCondition += " And ";
                strCondition += " IssuedDate >= '" + dtpSFrom.Value.ToString("dd-MMM-yyyy") + "' And IssuedDate <= '" + dtpSTo.Value.ToString("dd-MMM-yyyy") + "'";

                if (!string.IsNullOrEmpty(strCondition))
                    strCondition += " And ";
                strCondition += " OrderTypeID = 22";
            }
         
            DataTable dtIssues = MobjClsBLLDirectDelivery.GetIssueNos(strCondition);
            dgvIssueDisplay.DataSource = null;
            dgvIssueDisplay.DataSource = dtIssues;
            dgvIssueDisplay.Columns[0].Visible = false;
            dgvIssueDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            lblSCountStatus.Text = "Total Issues " + dgvIssueDisplay.Rows.Count.ToString();
        }

        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            if (!CheckDontEdit())
            {
                return;
            }
            AddNewItemIssue();
        }

        private string GetItemIssuePrefix()
        {
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue));
            if (PintFormType == 1)
                return ClsCommonSettings.ItemIssuePrefix;
            else
                return ClsCommonSettings.MaterialIssuePrefix;
        }


        private void GenerateItemIssueNo()
        {
            txtIssueNo.Text = GetItemIssuePrefix() + MobjClsBLLDirectDelivery.GetNextIssueNo(cboCompany.SelectedValue.ToInt32(), PintFormType).ToString();

            if (PintFormType == 1)
            {
                if (ClsCommonSettings.ItemIssueAutogenerate)
                    txtIssueNo.ReadOnly = false;
                else
                    txtIssueNo.ReadOnly = true;
            }
            else
            {
                if (ClsCommonSettings.MaterialIssueAutogenerate)
                    txtIssueNo.ReadOnly = false;
                else
                    txtIssueNo.ReadOnly = true;
            }
        }

        private void bnClear_Click(object sender, EventArgs e)
        {
            if (!CheckDontEdit())
            {
                return;
            }
            ClearControls();
        }
        public bool DeleteDeliveryNoteValiadtion()
        {
            if (MobjClsBLLDirectDelivery.CheckSalesOrderExists())
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4261, out MmessageIcon);
                MstrCommonMessage = MstrCommonMessage.Replace("*", " SalesOrder");
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                return false;
            }
            if (MobjClsBLLDirectDelivery.CheckSalesInvoiceExists())
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4261, out MmessageIcon);
                MstrCommonMessage = MstrCommonMessage.Replace("*", " SalesInvoice");
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                return false;
            }
            return true;
        }
        private void bnDelete_Click(object sender, EventArgs e)
        {
            //Do you want to delete the Item ?
            //commentefd
           if (DeleteDeliveryNoteValiadtion())
           {

               if (!IsSalesInvoiceExists() && !IsSalesOrderExists())
               {
                   MstrCommonMessage = "Reference Exists .";
                   MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                   lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                   tmrItemIssue.Enabled = true;
                   
                   return;
               }
                if (!dgvItemDetailsGrid.ReadOnly)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4213, out MmsgMessageIcon);
                    if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;
                    try
                    {
                        
                        MobjClsBLLDirectDelivery.DeleteItemIssue();
                        //delivery note alert
                        //commented for
                        //if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice)
                        //{
                        //    int itemIssue = MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID.ToInt32();

                        //    clsBLLAlertMoment objclsBLLAlertMoment = new clsBLLAlertMoment();
                        //    objclsBLLAlertMoment.DeleteAlert(itemIssue, (int)AlertSettingsTypes.DeliveryNote);

                        //    objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.DeliveryNote;
                        //    objFrmTradingMain.tmrAlert.Enabled = true;
                        //}

                        AddNewItemIssue();
                    }

                    catch (Exception ex)
                    {
                        if (ClsCommonSettings.ShowErrorMess)
                            MessageBox.Show("Error in bnDelete_Click() " + ex.Message);
                        MobjClsLogWriter.WriteLog("Error in bnDelete_Click() " + ex.Message, 2);
                    }
                }
                else
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4221, out MmsgMessageIcon);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        //commenetd for modif
        //private bool DeleteValidation()
        //{
        //    if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesInvoice)
        //    {
        //        if (MobjClsBLLDirectDelivery.FillCombos(new string[] { "SalesReturnID", "InvSalesReturnMaster", "SalesInvoiceID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) }).Rows.Count > 0)
        //        {
        //            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4225,out MmessageIcon);
        //            MstrCommonMessage = MstrCommonMessage.Replace("*", "this SalesInvoice");
        //            MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
        //            return false;
        //        }
        //    }

        //    if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesOrder)
        //    {
        //        if (MobjClsBLLDirectDelivery.FillCombos(new string[] { "SalesReturnID", "InvSalesReturnMaster", "SalesInvoiceID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) }).Rows.Count > 0)
        //        {
        //            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4225, out MmessageIcon);
        //            MstrCommonMessage = MstrCommonMessage.Replace("*", "this SalesOrder");
        //            MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
        //            return false;
        //        }
        //    }

        //    else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTTransfer)
        //    {
        //        if (MobjClsBLLDirectDelivery.FillCombos(new string[] { "ExpenseID", "InvExpenseMaster", "OperationTypeID = " + (int)OperationType.StockTransfer + " And ReferenceID = " + cboReferenceNo.SelectedValue.ToInt64() +" And IsDirectExpense = 0"}).Rows.Count > 0)
        //        {
        //            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4247,out MmessageIcon);
        //            MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
        //            return false;
        //        }
        //    }
        //    //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTDMR)
        //    //{
        //    //    if (mobjClsBllCommonUtility.FillCombos(new string[] { "1", "PrdRawMaterialUsedDetails", "ItemIssueID = " + MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID }).Rows.Count > 0)
        //    //    {
        //    //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4249,out MmessageIcon);
        //    //        MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
        //    //        return false;
        //    //    }
        //    //}
        //    return true;

        //}

        private void cboWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvItemDetailsGrid.Rows.Clear();
            dgvLocationDetails.Rows.Clear();
            //commented for
            //if (cboWarehouse.SelectedIndex != -1)
            //    cboOperationType_SelectedIndexChanged(null, null);
            ChangeStatus();
        }

        private void DisplayIssueInfo()
        {
            // function for displaying Issue Information
            if (MobjClsBLLDirectDelivery.DisplayItemIssuenfo())
            {
                ClearControls();
                blnIsEditMode = true;
                cboCompany.SelectedValue = MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intCompanyID;
                if (cboCompany.SelectedValue == null)
                {
                    DataTable datTemp = MobjClsBLLDirectDelivery.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intCompanyID });
                    if (datTemp.Rows.Count > 0)
                        cboCompany.Text = datTemp.Rows[0]["CompanyName"].ToString();
                }
                LoadCombo(6);
                txtIssueNo.Text = MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.strDirectDeliveryNo;
               LoadCombo(2);
               cboCustomer.SelectedValue = MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intVendorID;
               if (cboCustomer.SelectedValue == null)
               { 
                   DataTable datTemp = MobjClsBLLDirectDelivery.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", "VendorTypeID=" + (int)VendorType.Customer + " And StatusID = " + (int)VendorStatus.CustomerActive });
                   if (datTemp.Rows.Count > 0)
                       cboCustomer.Text = datTemp.Rows[0]["VendorName"].ToString();
               }
               //cboCustomer.SelectedValue = MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intVendorID;
               //if (cboCustomer.SelectedValue == null)
               //    cboCustomer.Text = MobjclsBLLSTSales.clsDTOSTSalesMaster.strVendorName;
               //AddToAddressMenu();
               //MintVendorAddID = MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intVendorAddID;
               //lblVendorAddress.Text = MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.strAddressName + " Address";
                cboWarehouse.SelectedValue = MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intWarehouseID;
                txtLpoNo.Text = MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.Lpono;
                
                lblIssuedBy.Text = MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.strIssuedBy;
                if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intStatusID == (Int32)OperationStatusType.DDNFullyInvoiced)
                {
                    lblDirectDNstatus.ForeColor = Color.Green;
                    lblDirectDNstatus.Text = "Invoiced";
                }
                else if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intStatusID == (Int32)OperationStatusType.DDNPartiallyInvoiced)
                {
                    lblDirectDNstatus.ForeColor = Color.Green;
                    lblDirectDNstatus.Text = "Partially Invoiced";
                }
                else if(MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intStatusID == (Int32)OperationStatusType.DDNopen)
                {
                    lblDirectDNstatus.ForeColor = Color.Brown;
                    lblDirectDNstatus.Text = "Open";
                }
                

                
                        dgvItemDetailsGrid.Visible = true;
                        dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
                        
                        btnCustomerwisePrint.Enabled = false;

                        //BatchNo.Visible = true;
                        ItemCode.ReadOnly = ItemName.ReadOnly  = Quantity.ReadOnly = false;
                       Uom.ReadOnly = true;
                        //Uom.ReadOnly = false;
                        dgvItemDetailsGrid.AllowUserToAddRows = true;
                        dgvItemDetailsGrid.AllowUserToDeleteRows = true;
                       
                    
               

                dtpIssuedDate.Value = Convert.ToDateTime(MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.strIssuedDate);
                txtRemarks.Text = MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.strRemarks;
                DataTable dtDetDetails = MobjClsBLLDirectDelivery.GetItemIssueDetails();
         
                MDtItemGroupIssueDetails.Rows.Clear();
               
              
                dgvItemDetailsGrid.DataSource = null;
                dgvItemDetailsGrid.Rows.Clear();
                dtItemDetails.Rows.Clear();

                   

               
               if (dgvItemDetailsGrid.Rows.Count > 0)
               {
                   for (int i = 0; i < dtDetDetails.Rows.Count; i++)
                   {
                         
                            //dgvItemDetailsGrid.Rows.Add();
                            //int intRowCount = dgvItemDetailsGrid.RowCount - 1;
                       if (Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) == false)
                       {
                           
                           DataTable datData = MobjClsBLLDirectDelivery.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + dtDetDetails.Rows[i]["ItemID"].ToInt32() + "" });
                           if (datData.Rows.Count > 0)
                           {
                               intCostingMethod = datData.Rows[0]["CostingMethodID"].ToInt32();
                           }
                       }
                       dgvItemDetailsGrid.RowCount = dgvItemDetailsGrid.RowCount + 1;
                            dgvItemDetailsGrid.Rows[i].Cells["ItemID"].Value = dtDetDetails.Rows[i]["ItemID"];
                            //if (Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) == false && intCostingMethod == 3)
                            //{
                                dgvItemDetailsGrid.Rows[i].Cells["BatchNo"].Value = dtDetDetails.Rows[i]["BatchNo"];
                                dgvItemDetailsGrid.Rows[i].Cells["BatchID"].Value = dtDetDetails.Rows[i]["BatchID"];
                            //}
                            //else if (Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) == false && intCostingMethod != 3)
                            //{
                            //    dgvItemDetailsGrid.Rows[i].Cells["BatchNo"].Value = "";
                            //    dgvItemDetailsGrid.Rows[i].Cells["BatchID"].Value = 0;

                            //}
                            //else if (Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) == true)
                            //{
                            //    dgvItemDetailsGrid.Rows[i].Cells["BatchNo"].Value = "";
                            //    dgvItemDetailsGrid.Rows[i].Cells["BatchID"].Value =0;
                            //}

                            
                            dgvItemDetailsGrid.Rows[i].Cells["ReferenceSerialNo"].Value = dtDetDetails.Rows[i]["ReferenceSerialNo"];
                            dgvItemDetailsGrid.Rows[i].Cells["ItemCode"].Value = dtDetDetails.Rows[i]["Code"];
                            dgvItemDetailsGrid.Rows[i].Cells["ItemName"].Value = dtDetDetails.Rows[i]["ItemName"];
                            //dgvItemDetailsGrid.Rows[i].Cells["Quantity"].Value = dtDetDetails.Rows[i]["Quantity"];
                            dgvItemDetailsGrid.Rows[i].Cells["IsGroup"].Value = dtDetDetails.Rows[i]["IsGroup"];
                            FillUomColumn(Convert.ToInt32(dtDetDetails.Rows[i]["ItemID"]), Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]));
                            dgvItemDetailsGrid.Rows[i].Cells["Uom"].Value = Convert.ToInt32(dtDetDetails.Rows[i]["UomID"]);
                            dgvItemDetailsGrid.Rows[i].Cells["Uom"].Tag = Convert.ToInt32(dtDetDetails.Rows[i]["UomID"]);
                            dgvItemDetailsGrid.Rows[i].Cells["Uom"].Value = dgvItemDetailsGrid.Rows[i].Cells["Uom"].FormattedValue;



                          //  dgvItemDetailsGrid["BatchNo", i].Value = dtDetDetails.Rows[i]["BatchNo"].ToString();
                          //  dgvItemDetailsGrid["BatchID", i].Value = Convert.ToInt32(dtDetDetails.Rows[i]["BatchID"]);
                          //  dgvItemDetailsGrid["ItemID", intRowCount].Value = Convert.ToInt32(dtDetDetails.Rows[i]["ItemID"]);
                          //  dgvItemDetailsGrid["ReferenceSerialNo", intRowCount].Value = Convert.ToInt32(dtDetDetails.Rows[i]["ReferenceSerialNo"]);
                          //  dgvItemDetailsGrid["ItemCode", intRowCount].Value = Convert.ToString(dtDetDetails.Rows[i]["Code"]);
                          //  dgvItemDetailsGrid["ItemName", intRowCount].Value = Convert.ToString(dtDetDetails.Rows[i]["ItemName"]);
                          //dgvItemDetailsGrid["Quantity", i].Value = Convert.ToDecimal(dtDetDetails.Rows[i]["Quantity"]);
                          //  dgvItemDetailsGrid["IsGroup", intRowCount].Value = Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]);
                          //  FillUomColumn(Convert.ToInt32(dtDetDetails.Rows[i]["ItemID"]), Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]));
                          //  dgvItemDetailsGrid.Rows[intRowCount].Cells["Uom"].Value = Convert.ToInt32(dtDetDetails.Rows[i]["UomID"]);
                          //  dgvItemDetailsGrid.Rows[intRowCount].Cells["Uom"].Tag = Convert.ToInt32(dtDetDetails.Rows[i]["UomID"]);
                          //  dgvItemDetailsGrid.Rows[intRowCount].Cells["Uom"].Value = dgvItemDetailsGrid.Rows[intRowCount].Cells["Uom"].FormattedValue;


                            int intUomScale = 0;
                            if (dgvItemDetailsGrid[Uom.Index, i].Tag.ToInt32() != 0)
                                intUomScale = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid[Uom.Index, i].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                           
                            dgvItemDetailsGrid.Rows[i].Cells["Quantity"].Value = dtDetDetails.Rows[i]["Quantity"].ToDecimal().ToString("F" + intUomScale);
                            dgvItemDetailsGrid.Rows[i].Cells["DeliveredQty"].Value = dtDetDetails.Rows[i]["DeliveredQty"].ToDecimal().ToString("F" + intUomScale);
                            dgvItemDetailsGrid.Rows[i].Cells["Rate"].Value = dtDetDetails.Rows[i]["Rate"];
                            dgvItemDetailsGrid.Rows[i].Cells["NetAmount"].Value = dtDetDetails.Rows[i]["NetAmount"];
                            CalculateTotalAmt();
                           

                           // BatchNo.Visible = true;
                            
                            //FillBatchNoColumn(Convert.ToInt32(dtDetDetails.Rows[i]["ItemID"]), true);
                            //dgvItemDetailsGrid.Rows[i].Cells["BatchNo"].Value = dtDetDetails.Rows[i]["BatchID"];
                            dgvItemDetailsGrid.Rows[i].Cells["BatchNo"].Tag = Convert.ToInt32(dtDetDetails.Rows[i]["BatchID"]);
                            //dgvItemDetailsGrid.Rows[i].Cells["BatchNo"].Value = dgvItemDetailsGrid.Rows[i].Cells["BatchNo"].FormattedValue;


                            //if (Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) == false && intCostingMethod == 3)
                            //{
                            //    dgvItemDetailsGrid.Rows[i].Cells["BatchNo"].Tag = Convert.ToInt32(dtDetDetails.Rows[i]["BatchID"]);
                            //}
                            //else if (Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) == false && intCostingMethod != 3)
                            //{
                            //    dgvItemDetailsGrid.Rows[i].Cells["BatchNo"].Tag = 0;

                            //}
                            //else if (Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) == true)
                            //{
                            //    dgvItemDetailsGrid.Rows[i].Cells["BatchNo"].Tag = 0;
                            //}


                            decimal decStockQuantity = 0;
                            if (Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) == true)
                            {
                                decStockQuantity = MobjClsBLLDirectDelivery.GetStockQuantityForGroup(Convert.ToInt32(dtDetDetails.Rows[i]["ItemID"]), MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intWarehouseID);
                            }
                            else
                            {

                               
                               decStockQuantity = MobjClsBLLDirectDelivery.GetStockQuantity(Convert.ToInt32(dtDetDetails.Rows[i]["ItemID"]), Convert.ToInt32(dtDetDetails.Rows[i]["BatchID"]), MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intCompanyID, MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intWarehouseID);

                            }
                            if (Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) != true && intCostingMethod == 3)
                                {
                                    DataTable dtUomDetails = MobjClsBLLDirectDelivery.GetUomConversionValues(Convert.ToInt32(dgvItemDetailsGrid["Uom", i].Tag), Convert.ToInt32(dgvItemDetailsGrid["ItemID", i].Value));
                                    if (dtUomDetails.Rows.Count > 0)
                                    {
                                        int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                                        decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                                        if (intConversionFactor == 1)
                                            decStockQuantity = decStockQuantity * decConversionValue;
                                        else if (intConversionFactor == 2)
                                            decStockQuantity = decStockQuantity / decConversionValue;
                                    }
                                    dgvItemDetailsGrid["QtyAvailable", i].Value = decStockQuantity.ToString("F" + intUomScale);
                                }
                                else if (intCostingMethod != 3 && Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) != true)
                                {
                                    decimal QtyAvailableForBatchlessItems = MobjClsBLLDirectDelivery.GetStockQuantityForBatchlessItems(Convert.ToInt32(dtDetDetails.Rows[i]["ItemID"]), MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intWarehouseID);
                                    DataTable dtUomDetails = MobjClsBLLDirectDelivery.GetUomConversionValues(Convert.ToInt32(dgvItemDetailsGrid["Uom", i].Tag), Convert.ToInt32(dgvItemDetailsGrid["ItemID", i].Value));
                                    if (dtUomDetails.Rows.Count > 0)
                                    {
                                        int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                                        decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                                        if (intConversionFactor == 1)
                                            QtyAvailableForBatchlessItems = QtyAvailableForBatchlessItems * decConversionValue;
                                        else if (intConversionFactor == 2)
                                            QtyAvailableForBatchlessItems = QtyAvailableForBatchlessItems / decConversionValue;
                                    }
                                    dgvItemDetailsGrid["QtyAvailable", i].Value = QtyAvailableForBatchlessItems.ToString("F" + intUomScale);
                                }
                                else if (Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) == true)
                                {
                                    decimal QtyAvailableForGrp = MobjClsBLLDirectDelivery.GetStockQuantityForGroup(Convert.ToInt32(dtDetDetails.Rows[i]["ItemID"]), Convert.ToInt32(cboWarehouse.SelectedValue));
                                    DataTable dtUomDetails = MobjClsBLLDirectDelivery.GetUomConversionValues(Convert.ToInt32(dgvItemDetailsGrid["Uom", i].Tag), Convert.ToInt32(dgvItemDetailsGrid["ItemID", i].Value));
                                    if (dtUomDetails.Rows.Count > 0)
                                    {
                                        int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                                        decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                                        if (intConversionFactor == 1)
                                            QtyAvailableForGrp = QtyAvailableForGrp * decConversionValue;
                                        else if (intConversionFactor == 2)
                                            QtyAvailableForGrp = QtyAvailableForGrp / decConversionValue;
                                    }
                                    dgvItemDetailsGrid["QtyAvailable", i].Value = QtyAvailableForGrp.ToString("F" + intUomScale);
                                }
                                
                            DataRow drItemDetailsRow = dtItemDetails.NewRow();
                            drItemDetailsRow["ItemID"] = dtDetDetails.Rows[i]["ItemID"].ToInt32();
                            drItemDetailsRow["ItemCode"] = dtDetDetails.Rows[i]["Code"].ToString();
                            drItemDetailsRow["ItemName"] = dtDetDetails.Rows[i]["ItemName"].ToString();
                            drItemDetailsRow["BatchID"] = dtDetDetails.Rows[i]["BatchID"].ToInt64();
                            drItemDetailsRow["BatchNo"] = dtDetDetails.Rows[i]["BatchNo"].ToString();
                            drItemDetailsRow["UomID"] = dtDetDetails.Rows[i]["UomID"].ToInt32();
                            dtItemDetails.Rows.Add(drItemDetailsRow);
                        }
                        
                    }

                   // DisplayLocationDetails();

                bnSaveItem.Enabled = false;
                bnAddNewItem.Enabled = MblnAddPermission;
                MblnIsEditable = true;

               
         

                if (MblnIsEditable)
                    bnDelete.Enabled = MblnDeletePermission;
                else
                    bnDelete.Enabled = false;

                bnEmail.Enabled = MblnPrintEmailPermission;
                bnPrint.Enabled = MblnPrintEmailPermission;
                btnPicklist.Enabled = MblnPrintEmailPermission;
                btnPickListEmail.Enabled = MblnPrintEmailPermission;
               
                cboWarehouse.Enabled = false;
                cboCompany.Enabled = false;
                cboCustomer.Enabled = false;
                btnSampleIssue.Enabled = true;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                DisplayAllIssueNos();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnRefresh_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in btnRefresh_Click() " + ex.Message, 2);
            }
        }

        private void dgvIssueDisplay_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!CheckDontEdit())
            {
                return;
            }
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (dgvIssueDisplay["ItemIssueID", e.RowIndex].Value != null)
                    {
                        MobjClsBLLDirectDelivery = new clsBLLDirectDelivery();
                        MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID = Convert.ToInt32(dgvIssueDisplay["ItemIssueID", e.RowIndex].Value);
                        DisplayIssueInfo();
                        EnableOrDisableButtons();
                        tcDeliveryNote.SelectedTab = tiItemDetails;
                        MchangeStatus = false;
                        bnSaveItem.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvIssueDisplay_CellClick() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvIssueDisplay_CellClick() " + ex.Message, 2);
            }
        }

        private void EnableOrDisableButtons()
        {
            if (blnIsEditMode)
            {
                btnActions.Enabled = true;
                if (IsSalesOrderExists())
                {
                    btnSalesOrder .Enabled = true;
                }
                else
                    btnSalesOrder.Enabled = false;

                if (IsSalesInvoiceExists())
                {
                    btnSalesInvoice.Enabled = true;
                }
                else
                    btnSalesInvoice.Enabled = false;

                if (IsSalesOrderExists() && IsSalesInvoiceExists())
                {
                    bnDelete.Enabled = true;
                    dgvItemDetailsGrid.ReadOnly = false;
                    dgvItemDetailsGrid.Columns["BatchNo"].ReadOnly = true;
                    dgvItemDetailsGrid.Columns["QtyAvailable"].ReadOnly = true;
                   dgvItemDetailsGrid.Columns["Uom"].ReadOnly = true;
                }
                else
                {
                    bnDelete.Enabled = false;
                    dgvItemDetailsGrid.ReadOnly = true;
                   
                    
                }
            }
        }

        private void FrmItemIssue_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.MdiFormClosing)
            {
                if (MchangeStatus)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4214, out MmsgMessageIcon).Replace("#", "").Trim();
                    if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                        DialogResult.Yes)
                    {
                        //MobjClsNotification = null;
                        //MobjClsLogWriter = null;
                        // MobjClsBLLDirectDelivery = null;
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        private void dgvItemDetailsGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void tmrItemIssue_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            tmrItemIssue.Enabled = false;
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID;

                    if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intOperationTypeID == (int)OperationOrderType.DNOTSalesInvoice)
                    {
                        ObjViewer.Type = "Sales Invoice No";
                    }
              
                    else if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intOperationTypeID == (int)OperationOrderType.DNOTTransfer)
                    {
                        ObjViewer.Type = "Transfer Order No";
                    }
              
                    ObjViewer.PblnCustomerWiseDeliveryNote = false;
                    ObjViewer.PblnPickList = false;
                    ObjViewer.PiFormID = (int)FormID.ItemIssue;
                    ObjViewer.AlMajdalPrePrintedFormat = ClsMainSettings.AlMajdalPrePrintedFormat;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnPrint_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in bnPrint_Click() " + ex.Message, 2);
            }
        }

        private void bnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Delivery Note";
                    ObjEmailPopUp.EmailFormType = EmailFormID.ItemIssue;
                    ObjEmailPopUp.EmailSource = MobjClsBLLDirectDelivery.GetDeliveryNoteReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnEmail_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in bnEmail_Click() " + ex.Message, 2);
            }
        }

        private void dgvItemDetailsGrid_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {

            //if (dgvItemDetailsGrid.ReadOnly)
            //    e.Cancel = true;
            //else
            //{
            //    // if (dgvItemDetailsGrid["IsAutomatic", e.Row.Index].Value != null && dgvItemDetailsGrid["IsAutomatic", e.Row.Index].Value.ToString().ToUpper() == "TRUE")
            //    //   e.Cancel = true;
            //    e.Cancel = true;
            //    foreach (DataGridViewRow dr in dgvItemDetailsGrid.Rows)
            //    {
            //        if (dr.Index != e.Row.Index && dr.Cells["ItemID"].Value.ToInt32() == e.Row.Cells["ItemID"].Value.ToInt32() )
            //        {
            //            e.Cancel = false;
            //            break;
            //        }
            //    }
            //}
            ChangeStatus();
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            objClsBLLLogin.GetCompanySettings(cboCompany.SelectedValue.ToInt32());
            if (!blnIsEditMode)
            {
                cboWarehouse.SelectedIndex = -1;
                //cboReferenceNo.DataSource = null;
                //cboReferenceNo.SelectedIndex = -1;
                
                LoadCombo(6);
                GenerateItemIssueNo();
            }
            ChangeStatus();
        }

        private void dtpIssuedDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtRemarks_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboSCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable datEmployees = null, datWarehouse = null; //bool blnIsEnabled = false;
            clsBLLPermissionSettings objBllPermissionSettings = new clsBLLPermissionSettings();
            //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
            //{
            if (cboSCompany.SelectedValue != null || Convert.ToInt32(cboSCompany.SelectedValue) != -2)
            {
                //datEmployees = MobjClsBLLDirectDelivery.FillCombos(new string[] { "EmployeeID,EmployeeFullName", "EmployeeMaster", "CompanyID = "+cboSCompany.SelectedValue.ToInt32()+"" });
                datWarehouse = MobjClsBLLDirectDelivery.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", "CompanyID = "+cboSCompany.SelectedValue.ToInt32()+"" });
            }
            else
            {
                //datEmployees = MobjClsBLLDirectDelivery.FillCombos(new string[] { "EmployeeID,EmployeeFullName", "EmployeeMaster", "" });
                datWarehouse = MobjClsBLLDirectDelivery.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", "" });
            }
            //}
            //else
            //{
            //    if (cboSCompany.SelectedValue != null || Convert.ToInt32(cboSCompany.SelectedValue) != -2)
            //    {
            //        DataTable datTemp = MobjClsBLLDirectDelivery.FillCombos(new string[] { "IsVisible", "STRoleFieldsDetails", "" + 
            //            "RoleID=" + ClsCommonSettings.RoleID + " AND CompanyID=" + ClsCommonSettings.CompanyID + " " +
            //            "AND ControlID=" + (int)ControlOperationType.DelNoteEmployee + " AND IsVisible=1" });
            //        if(datTemp != null)
            //            if(datTemp.Rows.Count > 0)
            //                datEmployees = MobjClsBLLDirectDelivery.GetEmployeeByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.DelNoteCompany);
            //        datWarehouse = MobjClsBLLDirectDelivery.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", "CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue) });

            //        if (datEmployees == null)
            //            datEmployees = MobjClsBLLDirectDelivery.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
            //                            " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
            //        else
            //        {
            //            if (datEmployees.Rows.Count == 0)
            //                datEmployees = MobjClsBLLDirectDelivery.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
            //                            " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
            //        }
            //    }                
            //}
            //DataRow drNewRow = datEmployees.NewRow();
            //drNewRow["EmployeeID"] = -2;
            //drNewRow["FirstName"] = "ALL";
            //datEmployees.Rows.InsertAt(drNewRow, 0);
            //cboSIssuedBy.ValueMember = "EmployeeID";
            //cboSIssuedBy.DisplayMember = "EmployeeFullName";
            //cboSIssuedBy.DataSource = datEmployees;

            DataRow drNewRow = datWarehouse.NewRow();
            drNewRow["WarehouseID"] = -2;
            drNewRow["WarehouseName"] = "ALL";
            datWarehouse.Rows.InsertAt(drNewRow, 0);
            cboSWarehouse.ValueMember = "WarehouseID";
            cboSWarehouse.DisplayMember = "WarehouseName";
            cboSWarehouse.DataSource = datWarehouse;
            LoadCombo(8);
          //  LoadCombo(4);                
        }
        public void RefreshForm()
        {
            //int intReferenceID = Convert.ToInt32(cboReferenceNo.SelectedValue);
            //cboOperationType_SelectedIndexChanged(null, null);
            //if (intReferenceID != 0)
            //    cboReferenceNo.SelectedValue = intReferenceID;
            btnRefresh_Click(null, null);
        }

        //private void cboSOperationType_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}

        private void dgvItemDetailsGrid_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            
            try
            {
                if (dgvItemDetailsGrid.IsCurrentCellDirty)
                {
                    if (dgvItemDetailsGrid.CurrentCell != null)
                    {
                        dgvItemDetailsGrid.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    }
                }
            }
            catch (Exception)
            { }
        }


        private void FrmItemIssue_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Alt | Keys.S:
                        bnSaveItem_Click(sender, new EventArgs());//save
                        break;
                    case Keys.Alt | Keys.R:
                        bnDelete_Click(sender, new EventArgs());//OK
                        break;


                }
            }
            catch (Exception)
            {
            }
        }

        private void dgvItemDetailsGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {

            }
            catch
            {

            }
        }

        private Boolean CheckDontEdit()
        {
            bool blnFlag = true;
            if (MchangeStatus)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 36, out MmsgMessageIcon).Replace("#", "").Trim();
                if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                    DialogResult.No)
                {
                    blnFlag = false;
                }
                else
                {
                    blnFlag = true;
                }
            }
            return blnFlag;
        }

        private void txtIssueNo_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void btnCustomerwisePrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID;

                    if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intOperationTypeID == 9)
                    {
                        ObjViewer.Type = "Sales Invoice No";
                    }
                    else if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intOperationTypeID == 10)
                    {
                        ObjViewer.Type = "Pont Of Sale No";

                    }
                    else
                    {
                        ObjViewer.Type = "Delivery Schedule No";
                        ObjViewer.PblnCustomerWiseDeliveryNote = true;
                    }

                    ObjViewer.PiFormID = (int)FormID.ItemIssue;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnPrint_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in bnPrint_Click() " + ex.Message, 2);
            }
        }

        private bool FillItemLocationDetails()
        {
            int iTempID = 0;
            iTempID = CheckDuplicationInGrid();
            if (iTempID != -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4243, out MmessageIcon);
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                tmrItemIssue.Enabled = true;
                tcDeliveryNote.SelectedTab = tiItemDetails;
                dgvItemDetailsGrid.CurrentCell = dgvItemDetailsGrid["ItemCode", iTempID];
                dgvItemDetailsGrid.Focus();
                return false ;
            }
     
            //for (int i = 0; i < dgvLocationDetails.Rows.Count; i++)
            //{

            //    if (dgvLocationDetails.Rows[i].Cells["LItemID"].Value != null && dgvLocationDetails.Rows[i].Cells["LBatchNo"].Value != null)
            //    {
            //        bool blnExists = false;
            //        foreach (DataGridViewRow dr in dgvItemDetailsGrid.Rows)
            //        {
            //            if (dr.Cells["ItemID"].Value != null && !string.IsNullOrEmpty(Convert.ToString(dr.Cells["BatchNo"].Value)))
            //            {
            //                if (dr.Cells["ItemID"].Value.ToInt32() == dgvLocationDetails.Rows[i].Cells["LItemID"].Value.ToInt32() && dr.Cells["BatchNo"].Tag.ToInt64() == dgvLocationDetails.Rows[i].Cells["LBatchID"].Value.ToInt64())
            //                {
            //                    blnExists = true;
            //                    break;
            //                }
            //            }
            //        }
            //        if (!blnExists)
            //        {
            //            dgvLocationDetails.Rows.RemoveAt(i);
            //            i--;
            //        }
            //    }
            //}
            dgvLocationDetails.Rows.Clear();

            //bool blnIsValid = true;
            //foreach (DataGridViewRow dr in dgvItemDetailsGrid.Rows)
            //{
            //    if (dr.Cells[ItemID.Index].Value.ToInt32() != 0 && dr.Cells[BatchNo.Index].Value != null)
            //    {
            //        decimal decQuantity = dr.Cells[Quantity.Index].Value.ToDecimal();
            //        decimal decLocationQuantity = 0;
            //        foreach (DataGridViewRow drRow in dgvLocationDetails.Rows)
            //        {
            //            if (dr.Cells[ItemID.Index].Value.ToInt32() == drRow.Cells[LItemID.Index].Value.ToInt32() && Convert.ToInt64(dr.Cells[BatchNo.Index].Tag) == drRow.Cells[LBatchID.Index].Value.ToInt64())
            //                decLocationQuantity += drRow.Cells[LQuantity.Index].Value.ToDecimal();
            //        }
            //        if (decQuantity != decLocationQuantity)
            //        {
            //            blnIsValid = false;
            //            break;
            //        }
            //    }
            //}
            //if (!blnIsValid)
            //{
                if (dgvLocationDetails.Rows.Count == 1)
                {
                    dgvLocationDetails.Rows.Clear(); dtItemDetails.Rows.Clear();
                    foreach (DataGridViewRow drItemRow in dgvItemDetailsGrid.Rows)
                    {

                        if (drItemRow.Cells["ItemID"].Value != null && !string.IsNullOrEmpty(Convert.ToString(drItemRow.Cells["BatchNo"].Value)))
                        {

                            DataRow drItemDetailsRow = dtItemDetails.NewRow();
                            drItemDetailsRow["ItemID"] = drItemRow.Cells["ItemID"].Value.ToInt32();
                            drItemDetailsRow["ItemCode"] = drItemRow.Cells["ItemCode"].Value.ToString();
                            drItemDetailsRow["ItemName"] = drItemRow.Cells["ItemName"].Value.ToString();
                            drItemDetailsRow["BatchID"] = drItemRow.Cells["BatchNo"].Tag.ToInt64();
                            drItemDetailsRow["BatchNo"] = drItemRow.Cells["BatchNo"].Value.ToString();
                            drItemDetailsRow["UomID"] = drItemRow.Cells["Uom"].Tag.ToInt32();
                            dtItemDetails.Rows.Add(drItemDetailsRow);

                            // dgvLocationDetails.Rows.Add();
                            dgvLocationDetails.RowCount = dgvLocationDetails.RowCount + 1;
                            int intRowIndex = dgvLocationDetails.RowCount - 2;
                            dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value = drItemRow.Cells["ItemID"].Value.ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LItemCode"].Value = drItemRow.Cells["ItemCode"].Value.ToString();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LItemName"].Value = drItemRow.Cells["ItemName"].Value.ToString();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LBatchID"].Value = drItemRow.Cells["BatchNo"].Tag.ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LBatchNo"].Value = drItemRow.Cells["BatchNo"].Value.ToString();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LQuantity"].Value = drItemRow.Cells["Quantity"].Value.ToDecimal();

                            DataTable dtItemUOMs = MobjClsBLLDirectDelivery.GetItemUoms(drItemRow.Cells["ItemID"].Value.ToInt32());
                            LUOM.DataSource = null;
                            LUOM.ValueMember = "UOMID";
                            LUOM.DisplayMember = "ShortName";
                            LUOM.DataSource = dtItemUOMs;
                            dgvLocationDetails.Rows[intRowIndex].Cells["LUOMID"].Value = drItemRow.Cells["UOM"].Tag.ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = drItemRow.Cells["UOM"].Tag.ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Tag = drItemRow.Cells["UOM"].Tag.ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].FormattedValue;

                            DataRow dr = MobjClsBLLDirectDelivery.GetItemDefaultLocation(drItemRow.Cells["ItemID"].Value.ToInt32(), cboWarehouse.SelectedValue.ToInt32());

                            FillLocationGridCombo(LLocation.Index, intRowIndex);
                            dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dr["LocationID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag = dr["LocationID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].FormattedValue;
                            DataTable dtLocation = new DataTable();
                            if (string.IsNullOrEmpty(Convert.ToString(dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value)))
                            {
                                dtLocation = (DataTable)LLocation.DataSource;
                                if (dtLocation != null && dtLocation.Rows.Count > 0)
                                {
                                    dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value = dtLocation.Rows[0]["LocationID"].ToInt32();
                                    dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag = dtLocation.Rows[0]["LocationID"].ToInt32();
                                    dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].FormattedValue;
                                }
                            }

                            FillLocationGridCombo(LRow.Index, intRowIndex);
                            dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dr["RowID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag = dr["RowID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].FormattedValue;

                            if (string.IsNullOrEmpty(Convert.ToString(dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value)))
                            {
                                dtLocation = (DataTable)LRow.DataSource;
                                if (dtLocation != null && dtLocation.Rows.Count > 0)
                                {
                                    dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value = dtLocation.Rows[0]["RowID"].ToInt32();
                                    dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag = dtLocation.Rows[0]["RowID"].ToInt32();
                                    dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].FormattedValue;
                                }
                            }

                            FillLocationGridCombo(LBlock.Index, intRowIndex);
                            dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dr["BlockID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag = dr["BlockID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].FormattedValue;

                            if (string.IsNullOrEmpty(Convert.ToString(dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value)))
                            {
                                dtLocation = (DataTable)LBlock.DataSource;
                                if (dtLocation != null && dtLocation.Rows.Count > 0)
                                {
                                    dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value = dtLocation.Rows[0]["BlockID"].ToInt32();
                                    dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag = dtLocation.Rows[0]["BlockID"].ToInt32();
                                    dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].FormattedValue;
                                }
                            }

                            FillLocationGridCombo(LLot.Index, intRowIndex);
                            dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dr["LotID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag = dr["LotID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].FormattedValue;

                            if (string.IsNullOrEmpty(Convert.ToString(dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value)))
                            {
                                dtLocation = (DataTable)LLot.DataSource;
                                if (dtLocation != null && dtLocation.Rows.Count > 0)
                                {
                                    dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value = dtLocation.Rows[0]["LotID"].ToInt32();
                                    dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Tag = dtLocation.Rows[0]["LotID"].ToInt32();
                                    dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].FormattedValue;
                                }
                            }

                            clsDTODirectDeliveryLocationDetails objLocationDetails = new clsDTODirectDeliveryLocationDetails();
                            objLocationDetails.intItemID = dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value.ToInt32();
                            objLocationDetails.lngBatchID = dgvLocationDetails.Rows[intRowIndex].Cells["LBatchID"].Value.ToInt64();
                            objLocationDetails.intLocationID = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag.ToInt32();
                            objLocationDetails.intRowID = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag.ToInt32();
                            objLocationDetails.intBlockID = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag.ToInt32();
                            objLocationDetails.intLotID = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag.ToInt32();
                            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intCompanyID = cboCompany.SelectedValue.ToInt32();
                            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intWarehouseID = cboWarehouse.SelectedValue.ToInt32();

                            decimal decStockQuantity = MobjClsBLLDirectDelivery.GetItemLocationQuantity(objLocationDetails);
                            DataTable dtUomDetails = new DataTable();
                            dtUomDetails = MobjClsBLLDirectDelivery.GetUomConversionValues(dgvLocationDetails.Rows[intRowIndex].Cells["LUom"].Tag.ToInt32(), dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value.ToInt32());
                            if (dtUomDetails.Rows.Count > 0)
                            {
                                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                                if (intConversionFactor == 1)
                                    decStockQuantity = decStockQuantity * decConversionValue;
                                else if (intConversionFactor == 2)
                                    decStockQuantity = decStockQuantity / decConversionValue;
                            }
                            dgvLocationDetails.Rows[intRowIndex].Cells["LAvailableQuantity"].Value = decStockQuantity;

                            //dgvLocationDetails.Rows.Add();
                        }
                    }
                }
                //else
                //{
                //    dtItemDetails.Rows.Clear();
                //    foreach (DataGridViewRow drItemRow in dgvItemDetailsGrid.Rows)
                //    {
                //        bool blnIsExists = false;
                //        decimal decLocationQty = 0;
                //        if (drItemRow.Cells["ItemID"].Value != null && !string.IsNullOrEmpty(Convert.ToString(drItemRow.Cells["BatchNo"].Value)))
                //        {
                //            DataRow drItemDetailsRow = dtItemDetails.NewRow();
                //            drItemDetailsRow["ItemID"] = drItemRow.Cells["ItemID"].Value.ToInt32();
                //            drItemDetailsRow["ItemCode"] = drItemRow.Cells["ItemCode"].Value.ToString();
                //            drItemDetailsRow["ItemName"] = drItemRow.Cells["ItemName"].Value.ToString();
                //            drItemDetailsRow["BatchID"] = drItemRow.Cells["BatchNo"].Tag.ToInt64();
                //            drItemDetailsRow["BatchNo"] = drItemRow.Cells["BatchNo"].Value.ToString();
                //            drItemDetailsRow["UOMID"] = drItemRow.Cells["Uom"].Tag.ToInt32();
                //            dtItemDetails.Rows.Add(drItemDetailsRow);

                //            foreach (DataGridViewRow drLocationRow in dgvLocationDetails.Rows)
                //            {
                //                if (drLocationRow.Cells["LItemID"].Value != null && drLocationRow.Cells["LBatchID"].Value != null)
                //                {
                //                    if (drItemRow.Cells["ItemID"].Value.ToInt32() == drLocationRow.Cells["LItemID"].Value.ToInt32() && drItemRow.Cells["BatchNo"].Tag.ToInt64() == drLocationRow.Cells["LBatchID"].Value.ToInt32())
                //                    {
                //                        blnIsExists = true;
                //                        decLocationQty += drLocationRow.Cells["LQuantity"].Value.ToDecimal();
                //                    }
                //                }
                //            }
                //            if (!blnIsExists)
                //            {
                //                //  dgvLocationDetails.Rows.Add();
                //                dgvLocationDetails.RowCount = dgvLocationDetails.RowCount + 1;
                //                int intRowIndex = dgvLocationDetails.RowCount - 2;
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value = drItemRow.Cells["ItemID"].Value.ToInt32();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LItemCode"].Value = drItemRow.Cells["ItemCode"].Value.ToString();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LItemName"].Value = drItemRow.Cells["ItemName"].Value.ToString();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LBatchID"].Value = drItemRow.Cells["BatchNo"].Tag.ToInt32();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LBatchNo"].Value = drItemRow.Cells["BatchNo"].Value.ToString();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LQuantity"].Value = drItemRow.Cells["Quantity"].Value.ToDecimal();

                //                DataTable dtItemUOMs = MobjClsBLLDirectDelivery.GetItemUoms(drItemRow.Cells["ItemID"].Value.ToInt32());
                //                LUOM.DataSource = null;
                //                LUOM.ValueMember = "UOMID";
                //                LUOM.DisplayMember = "ShortName";
                //                LUOM.DataSource = dtItemUOMs;
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LUOMID"].Value = drItemRow.Cells["UOM"].Tag.ToInt32();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = drItemRow.Cells["UOM"].Tag.ToInt32();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Tag = drItemRow.Cells["UOM"].Tag.ToInt32();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].FormattedValue;

                //                DataRow dr = MobjClsBLLDirectDelivery.GetItemDefaultLocation(drItemRow.Cells["ItemID"].Value.ToInt32(), cboWarehouse.SelectedValue.ToInt32());

                //                FillLocationGridCombo(LLocation.Index, intRowIndex);
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dr["LocationID"].ToInt32();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag = dr["LocationID"].ToInt32();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].FormattedValue;

                //                DataTable dtLocation = new DataTable();
                //                if (string.IsNullOrEmpty(Convert.ToString(dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value)))
                //                {
                //                    dtLocation = (DataTable)LLocation.DataSource;
                //                    if (dtLocation != null && dtLocation.Rows.Count > 0)
                //                    {
                //                        dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value = dtLocation.Rows[0]["LocationID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag = dtLocation.Rows[0]["LocationID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].FormattedValue;
                //                    }
                //                }

                //                FillLocationGridCombo(LRow.Index, intRowIndex);
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dr["RowID"].ToInt32();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag = dr["RowID"].ToInt32();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].FormattedValue;

                //                if (string.IsNullOrEmpty(Convert.ToString(dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value)))
                //                {
                //                    dtLocation = (DataTable)LRow.DataSource;
                //                    if (dtLocation != null && dtLocation.Rows.Count > 0)
                //                    {
                //                        dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value = dtLocation.Rows[0]["RowID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag = dtLocation.Rows[0]["RowID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].FormattedValue;
                //                    }
                //                }

                //                FillLocationGridCombo(LBlock.Index, intRowIndex);
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dr["BlockID"].ToInt32();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag = dr["BlockID"].ToInt32();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].FormattedValue;

                //                if (string.IsNullOrEmpty(Convert.ToString(dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value)))
                //                {
                //                    dtLocation = (DataTable)LBlock.DataSource;
                //                    if (dtLocation != null && dtLocation.Rows.Count > 0)
                //                    {
                //                        dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value = dtLocation.Rows[0]["BlockID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag = dtLocation.Rows[0]["BlockID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].FormattedValue;
                //                    }
                //                }

                //                FillLocationGridCombo(LLot.Index, intRowIndex);
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dr["LotID"].ToInt32();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag = dr["LotID"].ToInt32();
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].FormattedValue;

                //                if (string.IsNullOrEmpty(Convert.ToString(dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value)))
                //                {
                //                    dtLocation = (DataTable)LLot.DataSource;
                //                    if (dtLocation != null && dtLocation.Rows.Count > 0)
                //                    {
                //                        dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value = dtLocation.Rows[0]["LotID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Tag = dtLocation.Rows[0]["LotID"].ToInt32();
                //                        dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].FormattedValue;
                //                    }
                //                }
                //                //FillLocationGridCombo(LRow.Index, intRowIndex);
                //                //dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dr["RowID"].ToInt32();
                //                //dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag = dr["RowID"].ToInt32();
                //                //dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].FormattedValue;

                //                //FillLocationGridCombo(LBlock.Index, intRowIndex);
                //                //dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dr["BlockID"].ToInt32();
                //                //dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag = dr["BlockID"].ToInt32();
                //                //dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].FormattedValue;

                //                //FillLocationGridCombo(LLocation.Index, intRowIndex);
                //                //dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dr["LotID"].ToInt32();
                //                //dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag = dr["LotID"].ToInt32();
                //                //dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].FormattedValue;

                //                clsDTODirectDeliveryLocationDetails objLocationDetails = new clsDTODirectDeliveryLocationDetails();
                //                objLocationDetails.intItemID = dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value.ToInt32();
                //                objLocationDetails.lngBatchID = dgvLocationDetails.Rows[intRowIndex].Cells["LBatchID"].Value.ToInt64();
                //                objLocationDetails.intLocationID = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag.ToInt32();
                //                objLocationDetails.intRowID = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag.ToInt32();
                //                objLocationDetails.intBlockID = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag.ToInt32();
                //                objLocationDetails.intLotID = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag.ToInt32();
                //                MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intCompanyID = cboCompany.SelectedValue.ToInt32();
                //                MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intWarehouseID = cboWarehouse.SelectedValue.ToInt32();

                //                decimal decStockQuantity = MobjClsBLLDirectDelivery.GetItemLocationQuantity(objLocationDetails);
                //                DataTable dtUomDetails = new DataTable();
                //                dtUomDetails = MobjClsBLLDirectDelivery.GetUomConversionValues(dgvLocationDetails.Rows[intRowIndex].Cells["LUom"].Tag.ToInt32(), dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value.ToInt32());
                //                if (dtUomDetails.Rows.Count > 0)
                //                {
                //                    int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                //                    decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                //                    if (intConversionFactor == 1)
                //                        decStockQuantity = decStockQuantity * decConversionValue;
                //                    else if (intConversionFactor == 2)
                //                        decStockQuantity = decStockQuantity / decConversionValue;
                //                }
                //                dgvLocationDetails.Rows[intRowIndex].Cells["LAvailableQuantity"].Value = decStockQuantity;

                //                //dgvLocationDetails.Rows.Add();
                //            }
                //            else
                //            {
                //                foreach (DataGridViewRow drLocationRow in dgvLocationDetails.Rows)
                //                {
                //                    if (drLocationRow.Cells["LItemID"].Value != null && drLocationRow.Cells["LBatchID"].Value != null)
                //                    {
                //                        if (drItemRow.Cells["ItemID"].Value.ToInt32() == drLocationRow.Cells["LItemID"].Value.ToInt32() && drItemRow.Cells["BatchNo"].Tag.ToInt64() == drLocationRow.Cells["LBatchID"].Value.ToInt64())
                //                        {
                //                            if (drItemRow.Cells["Quantity"].Value.ToDecimal() - (decLocationQty - drLocationRow.Cells["LQuantity"].Value.ToDecimal()) <= 0)
                //                                drLocationRow.Cells["LQuantity"].Value = 0;
                //                            else
                //                                drLocationRow.Cells["LQuantity"].Value = drItemRow.Cells["Quantity"].Value.ToDecimal() - (decLocationQty - drLocationRow.Cells["LQuantity"].Value.ToDecimal());
                //                            break;
                //                        }
                //                    }
                //                }
                //            }
                //        }
                  //  }

              //  }
            //}
            return true;
        }

        private bool FillLocationGridCombo(int inColumnIndex, int intRowIndex)
        {
            try
            {
                DataTable dtLocation = new DataTable();
                int intTag = 0;
                if (inColumnIndex == LLocation.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32();

                    if(MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID != 0)
                        dtLocation = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Distinct W.LocationID,W.Location", "InvWarehouseDetails W", "W.WarehouseID=" + cboWarehouse.SelectedValue.ToString() });
                    else
                        dtLocation = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Distinct W.LocationID,W.Location", "InvWarehouseDetails W Inner Join InvItemLocation L On L.ItemID = " + dgvLocationDetails.Rows[intRowIndex].Cells[LItemID.Index].Value.ToInt32() + " And L.BatchID = " + dgvLocationDetails.Rows[intRowIndex].Cells[LBatchID.Index].Value.ToInt64() + " And L.WarehouseID = W.WarehouseID And L.LocationID = W.LocationID And IsNull(L.Quantity,0) > 0", "W.WarehouseID=" + cboWarehouse.SelectedValue.ToString() });

                    LLocation.DataSource = null;
                    LLocation.ValueMember = "LocationID";
                    LLocation.DisplayMember = "Location";
                    LLocation.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].FormattedValue;

                    if (string.IsNullOrEmpty(Convert.ToString(dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value)))
                    {
                        if (dtLocation.Rows.Count > 0)
                        {
                            dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value = dtLocation.Rows[0]["LocationID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag = dtLocation.Rows[0]["LocationID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].FormattedValue;
                        }
                    }

                }
                else if (inColumnIndex == LRow.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag.ToInt32();
                    if(MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID  !=0)
                    dtLocation = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Distinct  RowID, RowNumber", "InvWarehouseDetails", "WarehouseID=" + cboWarehouse.SelectedValue.ToString() + " And LocationID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32() });
                    else
                        dtLocation = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Distinct W.RowID,W.RowNumber", "InvWarehouseDetails W Inner Join InvItemLocation L On L.ItemID = " + dgvLocationDetails.Rows[intRowIndex].Cells[LItemID.Index].Value.ToInt32() + " And L.BatchID = " + dgvLocationDetails.Rows[intRowIndex].Cells[LBatchID.Index].Value.ToInt64() + " And   L.WarehouseID = W.WarehouseID And L.LocationID = W.LocationID And L.RowID = W.RowID And IsNull(L.Quantity,0) > 0", "W.WarehouseID=" + cboWarehouse.SelectedValue.ToString() + " And W.LocationID = " + dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32() });
                    LRow.DataSource = null;
                    LRow.ValueMember = "RowID";
                    LRow.DisplayMember = "RowNumber";
                    LRow.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].FormattedValue;


                    if (string.IsNullOrEmpty(Convert.ToString(dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value)))
                    {
                        if (dtLocation.Rows.Count > 0)
                        {
                            dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value = dtLocation.Rows[0]["RowID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag = dtLocation.Rows[0]["RowID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].FormattedValue;
                        }
                    }
                }
                else if (inColumnIndex == LBlock.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag.ToInt32();
                    if(MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID !=0)
                    dtLocation = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Distinct BlockID,BlockNumber", "InvWarehouseDetails", " WarehouseID=" + cboWarehouse.SelectedValue.ToString() + " And LocationID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32() + " And RowID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag.ToInt32() });
                    else
                        dtLocation = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Distinct W.BlockID,W.BlockNumber", "InvWarehouseDetails W Inner Join InvItemLocation L On L.ItemID = " + dgvLocationDetails.Rows[intRowIndex].Cells[LItemID.Index].Value.ToInt32() + " And L.BatchID = " + dgvLocationDetails.Rows[intRowIndex].Cells[LBatchID.Index].Value.ToInt64() + " And  L.WarehouseID = W.WarehouseID And L.LocationID = W.LocationID And L.RowID = W.RowID And L.BlockID = W.BlockID And IsNull(L.Quantity,0) > 0", "W.WarehouseID=" + cboWarehouse.SelectedValue.ToString() + " And W.LocationID = " + dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32() + " And W.RowID = " + dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag.ToInt32() });
                    LBlock.DataSource = null;
                    LBlock.ValueMember = "BlockID";
                    LBlock.DisplayMember = "BlockNumber";
                    LBlock.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].FormattedValue;


                    if (string.IsNullOrEmpty(Convert.ToString(dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value)))
                    {
                        if (dtLocation.Rows.Count > 0)
                        {
                            dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value = dtLocation.Rows[0]["BlockID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag = dtLocation.Rows[0]["BlockID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].FormattedValue;
                        }
                    }
                }
                else if (inColumnIndex == LLot.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Tag.ToInt32();
                    if(MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID !=0)
                    dtLocation = MobjClsBLLDirectDelivery.FillCombos(new string[] { " Distinct LotID, LotNumber", "InvWarehouseDetails", "WarehouseID=" + cboWarehouse.SelectedValue.ToString() + " And LocationID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32() + " And RowID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag.ToInt32() + " And BlockID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag.ToInt32() });
                    else
                        dtLocation = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Distinct W.LotID,W.LotNumber", "InvWarehouseDetails W Inner Join InvItemLocation L On L.ItemID = " + dgvLocationDetails.Rows[intRowIndex].Cells[LItemID.Index].Value.ToInt32() + " And L.BatchID = " + dgvLocationDetails.Rows[intRowIndex].Cells[LBatchID.Index].Value.ToInt64() + " And  L.WarehouseID = W.WarehouseID And L.LocationID = W.LocationID And L.RowID = W.RowID And L.BlockID = W.BlockID  And L.LotID = W.LotID And IsNull(L.Quantity,0) > 0", "W.WarehouseID=" + cboWarehouse.SelectedValue.ToString() + " And W.LocationID = " + dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32() + " And W.RowID = " + dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag.ToInt32() + " And W.BlockID = " + dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag.ToInt32() });
                    LLot.DataSource = null;
                    LLot.ValueMember = "LotID";
                    LLot.DisplayMember = "LotNumber";
                    LLot.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].FormattedValue;

                    if (string.IsNullOrEmpty(Convert.ToString(dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value)))
                    {
                        if (dtLocation.Rows.Count > 0)
                        {
                            dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value = dtLocation.Rows[0]["LotID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Tag = dtLocation.Rows[0]["LotID"].ToInt32();
                            dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].FormattedValue;
                        }
                    }
                }

                return (dtLocation.Rows.Count > 0);

            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on FillLocationGridCombo(): " + this.Name + " " + Ex.Message.ToString(), 2);
                return false;
            }
        }

        private bool CheckLocationDuplication()
        {
            for (int intICounter = 0; intICounter < dgvLocationDetails.Rows.Count; intICounter++)
            {
                for (int intJCounter = intICounter + 1; intJCounter < dgvLocationDetails.Rows.Count; intJCounter++)
                {
                    if (dgvLocationDetails["LItemID", intICounter].Value.ToInt32() == dgvLocationDetails["LItemID", intJCounter].Value.ToInt32() &&
                          (dgvLocationDetails["LBatchID", intICounter].Value != null && dgvLocationDetails["LBatchID", intJCounter].Value != null && dgvLocationDetails["LBatchID", intICounter].Value.ToInt64() == dgvLocationDetails["LBatchID", intJCounter].Value.ToInt64()) &&
                          dgvLocationDetails["LLocation", intICounter].Tag.ToInt32() == dgvLocationDetails["LLocation", intJCounter].Tag.ToInt32() &&
                          dgvLocationDetails["LRow", intICounter].Tag.ToInt32() == dgvLocationDetails["LRow", intJCounter].Tag.ToInt32() &&
                          dgvLocationDetails["LBlock", intICounter].Tag.ToInt32() == dgvLocationDetails["LBlock", intJCounter].Tag.ToInt32() &&
                          dgvLocationDetails["LLot", intICounter].Tag.ToInt32() == dgvLocationDetails["LLot", intJCounter].Tag.ToInt32())
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4235, out MmessageIcon).Replace("*", dgvLocationDetails["LItemName", intICounter].Value.ToString()).Replace("@", dgvLocationDetails["LBatchNo", intICounter].Value.ToString()).Replace("#", "");
                        MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        tcDeliveryNote.SelectedTab = tiLocationDetails;
                        dgvLocationDetails.Focus();
                        dgvLocationDetails.CurrentCell = dgvLocationDetails["LLocation", intJCounter];
                        return false;
                    }
                }
            }
            return true;
        }

        private void dgvLocationDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (e.ColumnIndex == LUOMID.Index)
                {
                    DataTable dtItemUOMs = MobjClsBLLDirectDelivery.GetItemUoms(dgvLocationDetails["LItemID", e.RowIndex].Value.ToInt32());
                    LUOM.DataSource = null;
                    LUOM.ValueMember = "UOMID";
                    LUOM.DisplayMember = "ShortName";
                    LUOM.DataSource = dtItemUOMs;
                    dgvLocationDetails.Rows[e.RowIndex].Cells["LUOM"].Value = dgvLocationDetails["LUOMID", e.RowIndex].Value.ToInt32();
                    dgvLocationDetails.Rows[e.RowIndex].Cells["LUOM"].Tag = dgvLocationDetails["LUOMID", e.RowIndex].Value.ToInt32();
                    dgvLocationDetails.Rows[e.RowIndex].Cells["LUOM"].Value = dgvLocationDetails.Rows[e.RowIndex].Cells["LUOM"].FormattedValue;

                }
            }
        }


        private void DisplayLocationDetails()
        {
            DataTable datLocationDetails = new DataTable();
            dgvLocationDetails.Rows.Clear();

                datLocationDetails = MobjClsBLLDirectDelivery.GetItemIssueLocationDetails();

            for (int intRowIndex = 0; intRowIndex < datLocationDetails.Rows.Count; intRowIndex++)
            {
                dgvLocationDetails.RowCount = dgvLocationDetails.RowCount + 1;
                dgvLocationDetails["LItemID", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["ItemID"];
                dgvLocationDetails["LItemCode", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["ItemCode"];
                dgvLocationDetails["LItemName", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["ItemName"];
                dgvLocationDetails["LBatchNo", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["BatchNo"];
                dgvLocationDetails["LBatchID", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["BatchID"];
                dgvLocationDetails["LQuantity", intRowIndex].Value = datLocationDetails.Rows[intRowIndex]["Quantity"];

                DataTable dtItemUOMs = MobjClsBLLDirectDelivery.GetItemUoms(datLocationDetails.Rows[intRowIndex]["ItemID"].ToInt32());
                LUOM.DataSource = null;
                LUOM.ValueMember = "UOMID";
                LUOM.DisplayMember = "ShortName";
                LUOM.DataSource = dtItemUOMs;
                dgvLocationDetails.Rows[intRowIndex].Cells["LUOMID"].Value = datLocationDetails.Rows[intRowIndex]["UOMID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = datLocationDetails.Rows[intRowIndex]["UOMID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Tag = datLocationDetails.Rows[intRowIndex]["UOMID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LUOM"].FormattedValue;

                FillLocationGridCombo(LLocation.Index, intRowIndex);
                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = datLocationDetails.Rows[intRowIndex]["LocationID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag = datLocationDetails.Rows[intRowIndex]["LocationID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].FormattedValue;

                FillLocationGridCombo(LRow.Index, intRowIndex);
                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = datLocationDetails.Rows[intRowIndex]["RowID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag = datLocationDetails.Rows[intRowIndex]["RowID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].FormattedValue;

                FillLocationGridCombo(LBlock.Index, intRowIndex);
                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = datLocationDetails.Rows[intRowIndex]["BlockID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag = datLocationDetails.Rows[intRowIndex]["BlockID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].FormattedValue;

                FillLocationGridCombo(LLot.Index, intRowIndex);
                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = datLocationDetails.Rows[intRowIndex]["LotID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag = datLocationDetails.Rows[intRowIndex]["LotID"].ToInt32();
                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].FormattedValue;

                clsDTODirectDeliveryLocationDetails objLocationDetails = new clsDTODirectDeliveryLocationDetails();
                objLocationDetails.intItemID = dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value.ToInt32();
                objLocationDetails.lngBatchID = dgvLocationDetails.Rows[intRowIndex].Cells["LBatchID"].Value.ToInt64();
                objLocationDetails.intLocationID = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag.ToInt32();
                objLocationDetails.intRowID = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag.ToInt32();
                objLocationDetails.intBlockID = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag.ToInt32();
                objLocationDetails.intLotID = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag.ToInt32();
                MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intCompanyID = cboCompany.SelectedValue.ToInt32();
                MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intWarehouseID = cboWarehouse.SelectedValue.ToInt32();
                decimal decStockQuantity = MobjClsBLLDirectDelivery.GetItemLocationQuantity(objLocationDetails);
                DataTable dtUomDetails = new DataTable();
                dtUomDetails = MobjClsBLLDirectDelivery.GetUomConversionValues(dgvLocationDetails.Rows[intRowIndex].Cells["LUom"].Tag.ToInt32(), dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value.ToInt32());
                if (dtUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                        decStockQuantity = decStockQuantity * decConversionValue;
                    else if (intConversionFactor == 2)
                        decStockQuantity = decStockQuantity / decConversionValue;
                }
                dgvLocationDetails.Rows[intRowIndex].Cells["LAvailableQuantity"].Value = decStockQuantity;
            }
        }

        private void FillLocationDetailsParameters()
        {

            MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.lstDirectDeliveryLocationDetails = new List<clsDTODirectDeliveryLocationDetails>();
            foreach (DataGridViewRow dr in dgvLocationDetails.Rows)
            {
                if (dr.Cells["LItemID"].Value != null && dr.Cells["LBatchID"].Value != null)
                {
                    clsDTODirectDeliveryLocationDetails objLocationDetails = new clsDTODirectDeliveryLocationDetails ();
                    objLocationDetails.intItemID = dr.Cells["LItemID"].Value.ToInt32();
                    objLocationDetails.lngBatchID = dr.Cells["LBatchID"].Value.ToInt64();
                    objLocationDetails.strBatchNo = dr.Cells["LBatchNo"].Value.ToString();
                    objLocationDetails.decQuantity = dr.Cells["LQuantity"].Value.ToDecimal();
                    objLocationDetails.intUOMID = dr.Cells["LUOM"].Tag.ToInt32();
                    objLocationDetails.intLocationID = dr.Cells["LLocation"].Tag.ToInt32();
                    objLocationDetails.intRowID = dr.Cells["LRow"].Tag.ToInt32();
                    objLocationDetails.intBlockID = dr.Cells["LBlock"].Tag.ToInt32();
                    objLocationDetails.intLotID = dr.Cells["LLot"].Tag.ToInt32();
                    MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.lstDirectDeliveryLocationDetails.Add(objLocationDetails);
                }
            }
        }

        private void tcDeliveryNote_SelectedTabChanged(object sender, TabStripTabChangedEventArgs e)
        {
            if (tcDeliveryNote.SelectedTab == tiLocationDetails)
            {
                FillItemLocationDetails();
            }

        }

        private void dgvLocationDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == LLocation.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
            else if (e.ColumnIndex == LBlock.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
            else if (e.ColumnIndex == LRow.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
            else if (e.ColumnIndex == LLot.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
        }

        private void dgvLocationDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                dgvLocationDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                int intTag = 0;

                if (e.RowIndex >= 0 && e.ColumnIndex == LLocation.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LLocation"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LLocation"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LLocation"].Tag = dgvLocationDetails.CurrentRow.Cells["LLocation"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LLocation"].Value = dgvLocationDetails.CurrentRow.Cells["LLocation"].FormattedValue;
                        LRow.DataSource = null;
                        LBlock.DataSource = null;
                        LLot.DataSource = null;
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Value = "";
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = "";
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = "";
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LLocation"].Value = dgvLocationDetails.CurrentRow.Cells["LLocation"].FormattedValue;
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex == LRow.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LRow"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LRow"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Tag = dgvLocationDetails.CurrentRow.Cells["LRow"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Value = dgvLocationDetails.CurrentRow.Cells["LRow"].FormattedValue;

                        LBlock.DataSource = null;
                        LLot.DataSource = null;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = "";
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = "";
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Value = dgvLocationDetails.CurrentRow.Cells["LRow"].FormattedValue;
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex == LBlock.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LBlock"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag = dgvLocationDetails.CurrentRow.Cells["LBlock"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = dgvLocationDetails.CurrentRow.Cells["LBlock"].FormattedValue;
                        LLot.DataSource = null;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = "";
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = dgvLocationDetails.CurrentRow.Cells["LBlock"].FormattedValue;
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex == LLot.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LLot"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LLot"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = dgvLocationDetails.CurrentRow.Cells["LLot"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = dgvLocationDetails.CurrentRow.Cells["LLot"].FormattedValue;
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = dgvLocationDetails.CurrentRow.Cells["LLot"].FormattedValue;

                    clsDTODirectDeliveryLocationDetails objLocationDetails = new clsDTODirectDeliveryLocationDetails();
                    objLocationDetails.intItemID = dgvLocationDetails.CurrentRow.Cells["LItemID"].Value.ToInt32();
                    objLocationDetails.lngBatchID = dgvLocationDetails.CurrentRow.Cells["LBatchID"].Value.ToInt64();
                    objLocationDetails.intLocationID = dgvLocationDetails.CurrentRow.Cells["LLocation"].Tag.ToInt32();
                    objLocationDetails.intRowID = dgvLocationDetails.CurrentRow.Cells["LRow"].Tag.ToInt32();
                    objLocationDetails.intBlockID = dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag.ToInt32();
                    objLocationDetails.intLotID = dgvLocationDetails.CurrentRow.Cells["LLot"].Tag.ToInt32();
                    MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intCompanyID = cboCompany.SelectedValue.ToInt32();
                    MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intWarehouseID = cboWarehouse.SelectedValue.ToInt32();

                    decimal decStockQuantity =  MobjClsBLLDirectDelivery.GetItemLocationQuantity(objLocationDetails);
                    DataTable dtUomDetails = new DataTable();
                    dtUomDetails = MobjClsBLLDirectDelivery.GetUomConversionValues(dgvLocationDetails.CurrentRow.Cells["LUom"].Tag.ToInt32(), dgvLocationDetails.CurrentRow.Cells["LItemID"].Value.ToInt32());
                    if (dtUomDetails.Rows.Count > 0)
                    {
                        int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                        decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                        if (intConversionFactor == 1)
                            decStockQuantity = decStockQuantity * decConversionValue;
                        else if (intConversionFactor == 2)
                            decStockQuantity = decStockQuantity / decConversionValue;
                    }
                    dgvLocationDetails.CurrentRow.Cells["LAvailableQuantity"].Value = decStockQuantity;
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        private void dgvLocationDetails_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //if (dgvLocationDetails.CurrentCell.ColumnIndex == LItemCode.Index || dgvLocationDetails.CurrentCell.ColumnIndex == LItemName.Index)
                //{
                //    for (int i = 0; i < dgvLocationDetails.Columns.Count; i++)
                //    {
                //            dgvLocationDetails.Rows[dgvLocationDetails.CurrentCell.RowIndex].Cells[i].Value = null;
                //            dgvLocationDetails.Rows[dgvLocationDetails.CurrentCell.RowIndex].Cells[i].Tag = null;
                //    }
                //}
                dgvLocationDetails.PServerName = ClsCommonSettings.ServerName;
                string[] First = { "LItemCode", "LItemName", "LItemID", "LBatchNo","LBatchID", "LUOMID" };//first grid 
                string[] second = { "ItemCode", "ItemName", "ItemID", "BatchNo", "BatchID","UOMID" };//inner grid
                dgvLocationDetails.aryFirstGridParam = First;
                dgvLocationDetails.arySecondGridParam = second;
                dgvLocationDetails.PiFocusIndex = LQuantity.Index;
                dgvLocationDetails.bBothScrollBar = true;
                dgvLocationDetails.pnlLeft = expandableSplitterLeft.Location.X + 5;
                dgvLocationDetails.pnlTop = expandableSplitterTop.Location.Y + PurchaseOrderBindingNavigator.Height + 10;
                dgvLocationDetails.ColumnsToHide = new string[] { "ItemID", "UOMID","BatchID" };
                if (dgvLocationDetails.CurrentCell.ColumnIndex == LItemCode.Index)
                {
                    dgvLocationDetails.field = "ItemCode";
                    dgvLocationDetails.CurrentCell.Value = dgvLocationDetails.TextBoxText;
                }
                if (dgvLocationDetails.CurrentCell.ColumnIndex == LItemName.Index)
                {
                    // dgvPurchase.PiColumnIndex = 1;
                    dgvLocationDetails.field = "ItemName";
                    dgvLocationDetails.CurrentCell.Value = dgvLocationDetails.TextBoxText;
                }
                string strFilterString = dgvLocationDetails.field + " Like '" + dgvLocationDetails.TextBoxText + "%'";
                dtItemDetails.DefaultView.RowFilter = strFilterString;
                dgvLocationDetails.dtDataSource = dtItemDetails.DefaultView.ToTable();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvItemGrid_TextBoxChangedEvent() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvItemGrid_TextBoxChangedEvent() " + ex.Message, 2);
            }
        }

        private void dgvLocationDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvLocationDetails.EditingControl.KeyPress += new KeyPressEventHandler(dgvLocationEditingControl_KeyPress);
        }

        void dgvLocationEditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvLocationDetails.CurrentCell.OwningColumn.Index == LQuantity.Index)
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
                int intUomScale = 0;
                if (dgvLocationDetails[Uom.Index, dgvLocationDetails.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    intUomScale = MobjClsBLLDirectDelivery.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvLocationDetails[Uom.Index, dgvLocationDetails.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    if (intUomScale == 0)
                    {
                        if (e.KeyChar == '.')
                            e.Handled = true;
                    }
                    else
                    {
                        int dotIndex = -1;
                        if (txt.Text.Contains("."))
                        {
                            dotIndex = txt.Text.IndexOf('.');
                        }
                        if (e.KeyChar == '.')
                        {
                            if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            {
                                e.Handled = true;
                            }
                        }
                        else
                        {
                            if (char.IsDigit(e.KeyChar))
                            {
                                if (dotIndex != -1 && txt.SelectionStart > dotIndex)
                                {
                                    string[] splitText = txt.Text.Split('.');
                                    if (splitText.Length == 2)
                                    {
                                        if (splitText[1].Length - txt.SelectedText.Length >= intUomScale)
                                            e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void dgvLocationDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvLocationDetails.IsCurrentCellDirty)
            {
                if (dgvLocationDetails.CurrentCell != null)
                    dgvLocationDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
        }

        private void dgvLocationDetails_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == LItemCode.Index) || (e.ColumnIndex == LItemName.Index))
            {
                dgvLocationDetails.PiColumnIndex = e.ColumnIndex;
            }
        }

        private void dgvLocationDetails_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            
        }

        private void dgvLocationDetails_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            bool blnValid = false;
            if (dgvLocationDetails.Rows[e.Row.Index].Cells["LItemID"].Value.ToInt32() != 0 && dgvLocationDetails.Rows[e.Row.Index].Cells["LBatchID"].Value != null)
            {
                foreach (DataGridViewRow dr in dgvLocationDetails.Rows)
                {

                    if (dr.Cells["LItemID"].Value.ToInt32() != 0 && dr.Cells["LBatchID"].Value != null)
                    {
                        if (dr.Index != e.Row.Index && dr.Cells["LItemID"].Value.ToInt32() == dgvLocationDetails.Rows[e.Row.Index].Cells["LItemID"].Value.ToInt32() && dr.Cells["LBatchID"].Value.ToInt64() == dgvLocationDetails.Rows[e.Row.Index].Cells["LBatchID"].Value.ToInt64())
                        {
                            blnValid = true;
                            break;
                        }
                    }
                }
            }
            else
            {
                blnValid = true;
            }
            if (!blnValid)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4242, out MmessageIcon).Replace("#", "");
                MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                e.Cancel = true;
            }
        }
        private bool ValidateLocationGrid()
        {
            bool blnValid = true;
            int intColumnIndex = 0;
            foreach (DataGridViewRow drItemRow in dgvItemDetailsGrid.Rows)
            {
                decimal decLocationQty = 0;
                foreach (DataGridViewRow drLocationRow in dgvLocationDetails.Rows)
                {
                    if (drLocationRow.Cells["LItemID"].Value.ToInt32() != 0)
                    {
                        if (drLocationRow.Cells["LLocation"].Tag.ToInt32() == 0)
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4205, out MmessageIcon).Replace("#", "");
                            intColumnIndex = LLocation.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LRow"].Tag.ToInt32() == 0)
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4206, out MmessageIcon).Replace("#", "");
                            intColumnIndex = LRow.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LBlock"].Tag.ToInt32() == 0)
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4207, out MmessageIcon).Replace("#", "");
                            intColumnIndex = LBlock.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LLot"].Tag.ToInt32() == 0)
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4208, out MmessageIcon).Replace("#", "");
                            intColumnIndex = LLot.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LQuantity"].Value.ToDecimal() <= 0)
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4215, out MmessageIcon).Replace("#", "");
                            intColumnIndex = LQuantity.Index;
                            blnValid = false;
                        }
                        if (!blnValid)
                        {
                            MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            tmrItemIssue.Enabled = true;
                            lblStatus.Text = MstrCommonMessage;
                            tcDeliveryNote.SelectedTab = tiLocationDetails;
                            dgvLocationDetails.Focus();
                            dgvLocationDetails.CurrentCell = dgvLocationDetails[intColumnIndex, drLocationRow.Index];
                            break;
                        }
                    }
                    if (blnValid && drLocationRow.Cells["LItemID"].Value.ToInt32() != 0 && drLocationRow.Cells["LBatchID"].Value != null)
                    {
                        if (drLocationRow.Cells["LItemID"].Value.ToInt32() == drItemRow.Cells["ItemID"].Value.ToInt32() && drLocationRow.Cells["LBatchID"].Value.ToInt64() == drItemRow.Cells["BatchNo"].Tag.ToInt64())
                            decLocationQty += drLocationRow.Cells["LQuantity"].Value.ToDecimal();
                    }
                }
                if (!blnValid) break;
                if (blnValid && drItemRow.Cells["Quantity"].Value.ToDecimal() != decLocationQty)
                {
                    blnValid = false;
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4239, out MmessageIcon).Replace("#", "");
                    MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    tmrItemIssue.Enabled = true;
                    lblStatus.Text = MstrCommonMessage;
                    tcDeliveryNote.SelectedTab = tiLocationDetails;
                    dgvLocationDetails.Focus();
                    dgvLocationDetails.CurrentCell = dgvLocationDetails[intColumnIndex, drItemRow.Index];
                    break;
                }
            }
            return blnValid;
        }

        private bool ValidateLocationQuantity()
        {
            DataTable datLocationDetails = new DataTable();
            bool blnValid = true;
                datLocationDetails = MobjClsBLLDirectDelivery.GetItemIssueLocationDetails();

            foreach (DataGridViewRow dr in dgvLocationDetails.Rows)
            {

                clsDTODirectDeliveryLocationDetails objLocationDetails = new clsDTODirectDeliveryLocationDetails();
                objLocationDetails.intItemID = dr.Cells["LItemID"].Value.ToInt32();
                objLocationDetails.lngBatchID = dr.Cells["LBatchID"].Value.ToInt64();
                objLocationDetails.intLocationID = dr.Cells["LLocation"].Tag.ToInt32();
                objLocationDetails.intRowID = dr.Cells["LRow"].Tag.ToInt32();
                objLocationDetails.intBlockID = dr.Cells["LBlock"].Tag.ToInt32();
                objLocationDetails.intLotID = dr.Cells["LLot"].Tag.ToInt32();

                decimal decAvailableQty = MobjClsBLLDirectDelivery.GetItemLocationQuantity(objLocationDetails);
                DataTable dtGetUomDetails = MobjClsBLLDirectDelivery.GetUomConversionValues(dr.Cells["LUOM"].Tag.ToInt32(), dr.Cells["LItemID"].Value.ToInt32());
                datLocationDetails.DefaultView.RowFilter = "ItemID = " + objLocationDetails.intItemID + " And BatchID = " + objLocationDetails.lngBatchID + " And LocationID = " + objLocationDetails.intLocationID + " And RowID = " + objLocationDetails.intRowID + " And BlockID = " + objLocationDetails.intBlockID + " And LotID = " + objLocationDetails.intLotID;
                decimal decOldQuantity = 0,decCurrentQty=0;
                if(datLocationDetails.DefaultView.ToTable().Rows.Count > 0)
                decOldQuantity = datLocationDetails.DefaultView.ToTable().Rows[0]["Quantity"].ToDecimal();
                decCurrentQty = dr.Cells["LQuantity"].Value.ToDecimal();
                if (dtGetUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                    {
                        decOldQuantity = decOldQuantity / decConversionValue;
                        decCurrentQty = decCurrentQty / decConversionValue;
                    }
                    else if (intConversionFactor == 2)
                    {
                        decOldQuantity = decOldQuantity * decConversionValue;
                        decCurrentQty = decCurrentQty * decConversionValue;
                    }
                }

                    if ((decAvailableQty + decOldQuantity) - decCurrentQty < 0)
                    {
                        blnValid = false;
                    }
                if (!blnValid)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4240, out MmessageIcon).Replace("#", "").Replace("@", dr.Cells["LItemName"].Value.ToString());
                    MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    tcDeliveryNote.SelectedTab = tiLocationDetails;
                    dgvLocationDetails.Focus();
                    break;
                }
            }
            return blnValid;
        }

        private void btnPicklist_Click(object sender, EventArgs e)
        {
            try
            {
                if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID;
                    ObjViewer.Type = "Delivery Note";
                    ObjViewer.PblnCustomerWiseDeliveryNote = false;
                    ObjViewer.PblnPickList = true;
                    ObjViewer.PiFormID = (int)FormID.ItemIssue;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnPrint_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in bnPrint_Click() " + ex.Message, 2);
            }
        }

        private void btnPickListEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Delivery Note Location Details";
                    ObjEmailPopUp.EmailFormType = EmailFormID.PickList;
                    ObjEmailPopUp.EmailSource = MobjClsBLLDirectDelivery.DisplayItemIssueLocationReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on Email:btnPickListEmail_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnPickListEmail_Click " + Ex.Message.ToString());
            }
        }

        //private int CheckDuplicationInGrid()
        //{
        //    int intItemID = 0;
        //    long lngBatchID = 0;
        //    int RowIndexTemp = 0;

        //    foreach (DataGridViewRow rowValue in dgvItemDetailsGrid.Rows)
        //    {
        //        if (rowValue.Cells[ItemID.Index].Value != null && rowValue.Cells[BatchNo.Index].Tag != null)
        //        {
        //            intItemID = Convert.ToInt32(rowValue.Cells[ItemID.Index].Value);
        //            lngBatchID = rowValue.Cells[BatchNo.Index].Tag.ToInt64();
        //            RowIndexTemp = rowValue.Index;
        //            foreach (DataGridViewRow row in dgvItemDetailsGrid.Rows)
        //            {
        //                if (RowIndexTemp != row.Index)
        //                {
        //                    if (row.Cells[ItemID.Index].Value != null && row.Cells[BatchNo.Index].Tag.ToInt64() != 0)
        //                    {
        //                        if ((Convert.ToInt32(row.Cells[ItemID.Index].Value) == intItemID) && (row.Cells[BatchNo.Index].Tag.ToInt64() == lngBatchID))
        //                            return row.Index;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return -1;
        //}

        private void expandableSplitterLeft_ExpandedChanged(object sender, ExpandedChangeEventArgs e)
        {
            if (expandableSplitterLeft.Expanded)
            {
                if (dgvIssueDisplay.Columns.Count > 0)
                    dgvIssueDisplay.Columns[0].Visible = false;
            }
        }

        private void lnkLabelAdvanceSearch_Click(object sender, EventArgs e)
        {
            DataTable datAdvanceSearchedData = new DataTable();
            if ((int)PintFormType == 1)
            {
                using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.DeliveryNote))
                {
                    objFrmSearchForm.ShowDialog();
                    if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                    {
                        datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                        datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                        datAdvanceSearchedData.Columns["ID"].ColumnName = "ItemIssueID";
                        dgvIssueDisplay.DataSource = datAdvanceSearchedData;
                        dgvIssueDisplay.Columns["ItemIssueID"].Visible = false;
                        dgvIssueDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    }
                }
            }
            else
            {
                using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.MaterialIssue))
                {
                    objFrmSearchForm.ShowDialog();
                    if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                    {
                        datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                        datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                        datAdvanceSearchedData.Columns["ID"].ColumnName = "ItemIssueID";
                        datAdvanceSearchedData.Columns["No"].ColumnName = "ItemIssueNo";
                        dgvIssueDisplay.DataSource = datAdvanceSearchedData;
                        dgvIssueDisplay.Columns["ItemIssueID"].Visible = false;
                        dgvIssueDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    }
                }
            }
        }

        private void dgvItemDetailsGrid_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            //if (cboOperationType.SelectedValue.ToInt32() != (int)OperationOrderType.DNOTTransfer)
                mobjClsBllCommonUtility.SetSerialNo(dgvItemDetailsGrid, e.RowIndex, false);
            //else
            //    mobjClsBllCommonUtility.SetSerialNo(dgvItemDetailsGrid, e.RowIndex, true);
        }

        private void dgvItemDetailsGrid_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            //if (cboOperationType.SelectedValue.ToInt32() != (int)OperationOrderType.DNOTTransfer)
                mobjClsBllCommonUtility.SetSerialNo(dgvItemDetailsGrid, e.RowIndex, false);
            //else
               // mobjClsBllCommonUtility.SetSerialNo(dgvItemDetailsGrid, e.RowIndex, true);
        }

        private void cboCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCustomer.SelectedValue != null)
            {
                if(MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID == 0)
                MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intVendorID = Convert.ToInt32(cboCustomer.SelectedValue);

               AddToAddressMenu();
                DisplayAddress();
                //Changestatus();
                
            }
            //else
            //{
            //    CMSVendorAddress.Items.Clear();
            //}
        }
        private bool AddToAddressMenu()
        {
            try
            {
                CMSVendorAddress.Items.Clear();
                //CMSVendorAddress.Items.Add("Permanent");
                DataTable datAddMenu = MobjClsBLLDirectDelivery.DtGetAddressName();

                for (int intICounter = 0; intICounter <= datAddMenu.Rows.Count - 1; intICounter++)
                {
                    ToolStripMenuItem tItem = new ToolStripMenuItem();
                    tItem.Tag = Convert.ToString(datAddMenu.Rows[intICounter][0]);
                    tItem.Text = Convert.ToString(datAddMenu.Rows[intICounter][1]);
                    CMSVendorAddress.Items.Add(tItem);
                }

                lblVendorAddress.Text = CMSVendorAddress.Items[0].Text + " Address";
                MintVendorAddID = Convert.ToInt32(CMSVendorAddress.Items[0].Tag);

                CMSVendorAddress.ItemClicked -= new ToolStripItemClickedEventHandler(this.MenuItem_Click);
                CMSVendorAddress.ItemClicked += new ToolStripItemClickedEventHandler(this.MenuItem_Click);
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in AddToAddressMenu() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in AddToAddressMenu() " + ex.Message, 2);
            }
            return false;
        }
        private bool DisplayAddress()
        {
            try
            {
                txtVendorAddress.Text = MobjClsBLLDirectDelivery.GetVendorAddressInformation(MintVendorAddID);
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in DisplayAddress() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in DisplayAddress() " + ex.Message, 2);
            }
            return false;
        }
        private void MenuItem_Click(System.Object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(cboCustomer.SelectedValue) > 0)
                {
                    lblVendorAddress.Text = e.ClickedItem.Text + " Address";
                    MintVendorAddID = Convert.ToInt32(e.ClickedItem.Tag);
                    DisplayAddress();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in MenuItem_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in MenuItem_Click() " + ex.Message, 2);
            }
        }

        private void lblTo_Click(object sender, EventArgs e)
        {

        }

        private void btnSalesOrder_Click(object sender, EventArgs e)
        {
            if (!IsSalesOrderExists())
            {
                MstrCommonMessage = " Order Or Invoice Exists For " + txtIssueNo.Text.Trim();
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                tmrItemIssue.Enabled = true;
                return;
            }
            else
            {
                FrmSalesOrder objFrmSalesOrder = new FrmSalesOrder(MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID);
                objFrmSalesOrder.WindowState = FormWindowState.Maximized;
                objFrmSalesOrder.MdiParent = this.ParentForm;
                objFrmSalesOrder.objFrmTradingMain = objFrmTradingMain;
                objFrmSalesOrder.Show();
            }
        }

        private void btnSalesInvoice_Click(object sender, EventArgs e)
        {
            if (!IsSalesInvoiceExists())
            {
                MstrCommonMessage = " Order Or Invoice Exists For " + txtIssueNo.Text.Trim();
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                tmrItemIssue.Enabled = true;
                return;
            }
            else
            {
                FrmSalesInvoice objFrmSalesInvoice = new FrmSalesInvoice(MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID);
                objFrmSalesInvoice.WindowState = FormWindowState.Maximized;
                objFrmSalesInvoice.MdiParent = this.ParentForm;
                objFrmSalesInvoice.objFrmTradingMain = objFrmTradingMain;
                objFrmSalesInvoice.Show();
            }
        }

        private bool IsSalesOrderExists()
        {
            if (MobjClsBLLDirectDelivery.IsSalesOrderExists())
            {
                return true;
            }
            else
                return false;
        }

        private bool IsSalesInvoiceExists()
        {
            if (MobjClsBLLDirectDelivery.IsSalesInvoiceExists())
            {
                return true;
            }
            else
                return false;
        }

        private void txtLpoNo_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void lblCompanyCurrencyAmount_Click(object sender, EventArgs e)
        {

        }

        private void showGroupItemDetails_Click(object sender, EventArgs e)
        {

            frmItemGroupMaster objfrmItemGroupMaster = new frmItemGroupMaster();
            objfrmItemGroupMaster.PintProductGroupID = dgvItemDetailsGrid.Rows[dgvItemDetailsGrid.CurrentRow.Index].Cells["ItemID"].Value.ToInt32();
            objfrmItemGroupMaster.ShowDialog();
        }

        private void dgvItemDetailsGrid_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                int intCurrentColumn = dgvItemDetailsGrid.CurrentCell.ColumnIndex;

                if (this.cboCompany.SelectedIndex >= 0 && cboCustomer.SelectedIndex >= 0)
                {
                    if (intCurrentColumn == 0 || intCurrentColumn == 1)
                    {
                        if (e.Button == MouseButtons.Right)
                        {
                            if (dgvItemDetailsGrid.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt32() > 0)
                            {
                                SItemID = dgvItemDetailsGrid.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt32();
                                SIsGroup = Convert.ToBoolean(dgvItemDetailsGrid.Rows[e.RowIndex].Cells["IsGroup"].Value) == true ? 1 : 0;
                                CntxtHistory.Items["showGroupItemDetails"].Visible = SIsGroup == 1 ? true : false;
                                this.CntxtHistory.Show(this.dgvItemDetailsGrid, this.dgvItemDetailsGrid.PointToClient(Cursor.Position));
                            }
                        }
                    }
                }
            }
        }

        private void btnSampleIssue_Click(object sender, EventArgs e)
        {
            try
            {
                if (MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID;

                    ObjViewer.PblnCustomerWiseDeliveryNote = false;
                    ObjViewer.PblnPickList = false;
                    ObjViewer.PiFormID = (int)FormID.ItemIssue;
                    ObjViewer.AlMajdalPrePrintedFormat = ClsMainSettings.AlMajdalPrePrintedFormat;
                    ObjViewer.SampleIssue = true;
                    ObjViewer.ShowDialog();
                }
            }
            catch { }
        }      
    }
}
