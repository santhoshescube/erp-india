using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data;
using System.Data.SqlClient;

using Microsoft.ReportingServices.ReportRendering;
using System.Collections.Generic;


/* 
=================================================
   Author:		<Author,,Midhun>//
   Create date: <Create Date,,28 MAR 2011>
   Description:	<Description,,Item Issue Form>
================================================
*/
namespace MyBooksERP
{
    public class FrmItemIssue : DevComponents.DotNetBar.Office2007Form
    {

        #region Designer

        public bool blnDocumentChanged = false;
        public string strFileName = "";
        public string Type;
        public Command cmdCommandZoom;
        private DockContainerItem dockContainerItem1;
        private ExpandableSplitter expandableSplitterLeft;
        private PanelEx PanelLeft;
        private IContainer components;

        //------------------------------------------------------------------------------------------------------------------------

        //private bool MblnShowErrorMess;                  // Set To Display Error Message
        private MessageBoxIcon MmessageIcon;
        private DotNetBarManager dotNetBarManager1;
        private DockSite dockSite4;
        private DockSite dockSite1;
        private DockSite dockSite2;
        private DockSite dockSite3;
        private DockSite dockSite5;
        private DockSite dockSite6;
        private Bar bar1;
        private DockSite dockSite8;
        private PanelEx panelMain;
        private Bar PurchaseOrderBindingNavigator;
        private ButtonItem bnAddNewItem;
        private ExpandableSplitter expandableSplitterTop;
        private PanelEx panelTop;
        private ExpandablePanel expandablePanel1;
        private LabelX lblSCountStatus;
        private PanelEx panelLeftTop;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvIssueDisplay;
        private Label lblSCompany;
        private ButtonItem bnSaveItem;
        private ButtonItem bnClear;
        private ButtonItem bnDelete;
        private ButtonX btnRefresh;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSCompany;
        private System.Windows.Forms.DateTimePicker dtpIssuedDate;
        private Label lblIssuedDate;
        private Label lblIssueNo;
        internal Timer tmrItemIssue;
        internal ErrorProvider ErrItemIssue;
        private ButtonItem bnPrint;
        private ButtonItem bnEmail;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private DevComponents.DotNetBar.Controls.TextBoxX txtIssueNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSWarehouse;
        private Label lblSWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSOperationType;
        private Label lblSOperationType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboReferenceNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboOperationType;
        private Label lblOperationType;
        private Label lblIssuedBy;
        private Label lbl;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private DevComponents.DotNetBar.Controls.TextBoxX txtRemarks;
        private Label lblRemarks;
        private DevComponents.DotNetBar.Controls.WarningBox lblStatus;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private Label lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSIssuedBy;
        private Label lblSIssuedBy;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private PanelEx panelMiddle;
        private ClsInnerGridBar dgvItemDetailsGrid;
        private Label lblSIssueNo;
        private System.Windows.Forms.DateTimePicker dtpSTo;
        private System.Windows.Forms.DateTimePicker dtpSFrom;
        private Label lblTo;
        private Label lblFrom;
        private ButtonItem btnCustomerwisePrint;
        private DevComponents.DotNetBar.TabControl tcDeliveryNote;
        private TabControlPanel tabControlPanel1;
        private TabItem tiItemDetails;
        private ButtonItem btnPicklist;
        private ButtonItem btnPickListEmail;
        private LinkLabel lnkLabelAdvanceSearch;
        private Label lblLpoNo;
        private System.Windows.Forms.TextBox txtLpoNo;
        private ContextMenuStrip CntxtHistory;
        private ToolStripMenuItem ShowItemHistory;
        private ToolStripMenuItem ShowSaleRate;
        private ToolStripMenuItem showGroupItemDetails;
        private DataGridViewTextBoxColumn ItemCode;
        private DataGridViewTextBoxColumn ItemName;
        private DataGridViewTextBoxColumn ItemID;
        private DataGridViewTextBoxColumn BatchNo;
        private DataGridViewTextBoxColumn ReferenceNo;
        private DataGridViewTextBoxColumn ReferenceSerialNo;
        private DataGridViewTextBoxColumn InvoicedQuantity;
        private DataGridViewTextBoxColumn DeliveredQty;
        private DataGridViewTextBoxColumn QtyAvailable;
        private DataGridViewTextBoxColumn Quantity;
        private DataGridViewComboBoxColumn Uom;
        private DataGridViewTextBoxColumn PackingUnit;
        private DataGridViewTextBoxColumn InvQty;
        private DataGridViewTextBoxColumn InvUOMID;
        private DataGridViewTextBoxColumn IsAutomatic;
        private DataGridViewCheckBoxColumn IsGroup;
        private DataGridViewTextBoxColumn decCurrentTotalDeliveredQty;
        private DataGridViewTextBoxColumn TotalOldQuantityForMinItems;
        private DataGridViewTextBoxColumn OldBatchID;
        private DataGridViewTextBoxColumn BatchID;
        private Label lblWarehouse;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmItemIssue));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cmdCommandZoom = new DevComponents.DotNetBar.Command(this.components);
            this.dockContainerItem1 = new DevComponents.DotNetBar.DockContainerItem();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvIssueDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelLeftTop = new DevComponents.DotNetBar.PanelEx();
            this.lnkLabelAdvanceSearch = new System.Windows.Forms.LinkLabel();
            this.lblSIssueNo = new System.Windows.Forms.Label();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cboSIssuedBy = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSIssuedBy = new System.Windows.Forms.Label();
            this.cboSWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSWarehouse = new System.Windows.Forms.Label();
            this.cboSOperationType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSOperationType = new System.Windows.Forms.Label();
            this.btnRefresh = new DevComponents.DotNetBar.ButtonX();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboSCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSCompany = new System.Windows.Forms.Label();
            this.lblSCountStatus = new DevComponents.DotNetBar.LabelX();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.dotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.dockSite4 = new DevComponents.DotNetBar.DockSite();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.dockSite2 = new DevComponents.DotNetBar.DockSite();
            this.dockSite8 = new DevComponents.DotNetBar.DockSite();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.dockSite6 = new DevComponents.DotNetBar.DockSite();
            this.dockSite3 = new DevComponents.DotNetBar.DockSite();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.panelMain = new DevComponents.DotNetBar.PanelEx();
            this.panelMiddle = new DevComponents.DotNetBar.PanelEx();
            this.tcDeliveryNote = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvItemDetailsGrid = new ClsInnerGridBar();
            this.tiItemDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.expandableSplitterTop = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.txtLpoNo = new System.Windows.Forms.TextBox();
            this.lblLpoNo = new System.Windows.Forms.Label();
            this.cboWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new System.Windows.Forms.Label();
            this.txtRemarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.lblIssuedBy = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.cboReferenceNo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboOperationType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblOperationType = new System.Windows.Forms.Label();
            this.txtIssueNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.dtpIssuedDate = new System.Windows.Forms.DateTimePicker();
            this.lblIssuedDate = new System.Windows.Forms.Label();
            this.lblIssueNo = new System.Windows.Forms.Label();
            this.PurchaseOrderBindingNavigator = new DevComponents.DotNetBar.Bar();
            this.bnAddNewItem = new DevComponents.DotNetBar.ButtonItem();
            this.bnSaveItem = new DevComponents.DotNetBar.ButtonItem();
            this.bnClear = new DevComponents.DotNetBar.ButtonItem();
            this.bnDelete = new DevComponents.DotNetBar.ButtonItem();
            this.bnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.btnCustomerwisePrint = new DevComponents.DotNetBar.ButtonItem();
            this.btnPicklist = new DevComponents.DotNetBar.ButtonItem();
            this.bnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.btnPickListEmail = new DevComponents.DotNetBar.ButtonItem();
            this.lblStatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.tmrItemIssue = new System.Windows.Forms.Timer(this.components);
            this.ErrItemIssue = new System.Windows.Forms.ErrorProvider(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CntxtHistory = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ShowItemHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowSaleRate = new System.Windows.Forms.ToolStripMenuItem();
            this.showGroupItemDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReferenceNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReferenceSerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvoicedQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveredQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtyAvailable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Uom = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.PackingUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvUOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsAutomatic = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsGroup = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.decCurrentTotalDeliveredQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalOldQuantityForMinItems = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OldBatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIssueDisplay)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.panelMain.SuspendLayout();
            this.panelMiddle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcDeliveryNote)).BeginInit();
            this.tcDeliveryNote.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemDetailsGrid)).BeginInit();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrItemIssue)).BeginInit();
            this.CntxtHistory.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdCommandZoom
            // 
            this.cmdCommandZoom.Name = "cmdCommandZoom";
            // 
            // dockContainerItem1
            // 
            this.dockContainerItem1.Name = "dockContainerItem1";
            this.dockContainerItem1.Text = "dockContainerItem1";
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.dgvIssueDisplay);
            this.PanelLeft.Controls.Add(this.expandablePanel1);
            this.PanelLeft.Controls.Add(this.lblSCountStatus);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(200, 514);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 9;
            this.PanelLeft.Text = "panelEx1";
            // 
            // dgvIssueDisplay
            // 
            this.dgvIssueDisplay.AllowUserToAddRows = false;
            this.dgvIssueDisplay.AllowUserToDeleteRows = false;
            this.dgvIssueDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvIssueDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvIssueDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvIssueDisplay.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvIssueDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvIssueDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvIssueDisplay.Location = new System.Drawing.Point(0, 225);
            this.dgvIssueDisplay.Name = "dgvIssueDisplay";
            this.dgvIssueDisplay.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvIssueDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvIssueDisplay.RowHeadersVisible = false;
            this.dgvIssueDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvIssueDisplay.Size = new System.Drawing.Size(200, 263);
            this.dgvIssueDisplay.TabIndex = 13;
            this.dgvIssueDisplay.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvIssueDisplay_CellClick);
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expandablePanel1.Controls.Add(this.panelLeftTop);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(200, 225);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 100;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Search";
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelLeftTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelLeftTop.Controls.Add(this.lnkLabelAdvanceSearch);
            this.panelLeftTop.Controls.Add(this.lblSIssueNo);
            this.panelLeftTop.Controls.Add(this.dtpSTo);
            this.panelLeftTop.Controls.Add(this.dtpSFrom);
            this.panelLeftTop.Controls.Add(this.lblTo);
            this.panelLeftTop.Controls.Add(this.lblFrom);
            this.panelLeftTop.Controls.Add(this.cboSIssuedBy);
            this.panelLeftTop.Controls.Add(this.lblSIssuedBy);
            this.panelLeftTop.Controls.Add(this.cboSWarehouse);
            this.panelLeftTop.Controls.Add(this.lblSWarehouse);
            this.panelLeftTop.Controls.Add(this.cboSOperationType);
            this.panelLeftTop.Controls.Add(this.lblSOperationType);
            this.panelLeftTop.Controls.Add(this.btnRefresh);
            this.panelLeftTop.Controls.Add(this.txtSearch);
            this.panelLeftTop.Controls.Add(this.cboSCompany);
            this.panelLeftTop.Controls.Add(this.lblSCompany);
            this.panelLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftTop.Location = new System.Drawing.Point(0, 26);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(200, 199);
            this.panelLeftTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelLeftTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelLeftTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelLeftTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelLeftTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelLeftTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelLeftTop.Style.GradientAngle = 90;
            this.panelLeftTop.TabIndex = 101;
            // 
            // lnkLabelAdvanceSearch
            // 
            this.lnkLabelAdvanceSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lnkLabelAdvanceSearch.AutoSize = true;
            this.lnkLabelAdvanceSearch.Location = new System.Drawing.Point(3, 181);
            this.lnkLabelAdvanceSearch.Name = "lnkLabelAdvanceSearch";
            this.lnkLabelAdvanceSearch.Size = new System.Drawing.Size(87, 13);
            this.lnkLabelAdvanceSearch.TabIndex = 262;
            this.lnkLabelAdvanceSearch.TabStop = true;
            this.lnkLabelAdvanceSearch.Text = "Advance Search";
            this.lnkLabelAdvanceSearch.Click += new System.EventHandler(this.lnkLabelAdvanceSearch_Click);
            // 
            // lblSIssueNo
            // 
            this.lblSIssueNo.AutoSize = true;
            this.lblSIssueNo.Location = new System.Drawing.Point(3, 151);
            this.lblSIssueNo.Name = "lblSIssueNo";
            this.lblSIssueNo.Size = new System.Drawing.Size(49, 13);
            this.lblSIssueNo.TabIndex = 261;
            this.lblSIssueNo.Text = "Issue No";
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(78, 125);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.Size = new System.Drawing.Size(117, 20);
            this.dtpSTo.TabIndex = 260;
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(78, 102);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.Size = new System.Drawing.Size(117, 20);
            this.dtpSFrom.TabIndex = 259;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(3, 128);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 258;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(3, 105);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 257;
            this.lblFrom.Text = "From";
            // 
            // cboSIssuedBy
            // 
            this.cboSIssuedBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSIssuedBy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSIssuedBy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSIssuedBy.DisplayMember = "Text";
            this.cboSIssuedBy.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSIssuedBy.DropDownHeight = 75;
            this.cboSIssuedBy.FormattingEnabled = true;
            this.cboSIssuedBy.IntegralHeight = false;
            this.cboSIssuedBy.ItemHeight = 14;
            this.cboSIssuedBy.Location = new System.Drawing.Point(78, 79);
            this.cboSIssuedBy.Name = "cboSIssuedBy";
            this.cboSIssuedBy.Size = new System.Drawing.Size(117, 20);
            this.cboSIssuedBy.TabIndex = 12;
            // 
            // lblSIssuedBy
            // 
            this.lblSIssuedBy.AutoSize = true;
            this.lblSIssuedBy.Location = new System.Drawing.Point(3, 82);
            this.lblSIssuedBy.Name = "lblSIssuedBy";
            this.lblSIssuedBy.Size = new System.Drawing.Size(53, 13);
            this.lblSIssuedBy.TabIndex = 128;
            this.lblSIssuedBy.Text = "Issued By";
            // 
            // cboSWarehouse
            // 
            this.cboSWarehouse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSWarehouse.DisplayMember = "Text";
            this.cboSWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSWarehouse.DropDownHeight = 75;
            this.cboSWarehouse.FormattingEnabled = true;
            this.cboSWarehouse.IntegralHeight = false;
            this.cboSWarehouse.ItemHeight = 14;
            this.cboSWarehouse.Location = new System.Drawing.Point(78, 56);
            this.cboSWarehouse.Name = "cboSWarehouse";
            this.cboSWarehouse.Size = new System.Drawing.Size(117, 20);
            this.cboSWarehouse.TabIndex = 11;
            // 
            // lblSWarehouse
            // 
            this.lblSWarehouse.AutoSize = true;
            this.lblSWarehouse.Location = new System.Drawing.Point(3, 59);
            this.lblSWarehouse.Name = "lblSWarehouse";
            this.lblSWarehouse.Size = new System.Drawing.Size(62, 13);
            this.lblSWarehouse.TabIndex = 125;
            this.lblSWarehouse.Text = "Warehouse";
            // 
            // cboSOperationType
            // 
            this.cboSOperationType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSOperationType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSOperationType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSOperationType.DisplayMember = "Text";
            this.cboSOperationType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSOperationType.DropDownHeight = 75;
            this.cboSOperationType.FormattingEnabled = true;
            this.cboSOperationType.IntegralHeight = false;
            this.cboSOperationType.ItemHeight = 14;
            this.cboSOperationType.Location = new System.Drawing.Point(78, 33);
            this.cboSOperationType.Name = "cboSOperationType";
            this.cboSOperationType.Size = new System.Drawing.Size(117, 20);
            this.cboSOperationType.TabIndex = 10;
            this.cboSOperationType.SelectedIndexChanged += new System.EventHandler(this.cboSOperationType_SelectedIndexChanged);
            // 
            // lblSOperationType
            // 
            this.lblSOperationType.AutoSize = true;
            this.lblSOperationType.Location = new System.Drawing.Point(3, 36);
            this.lblSOperationType.Name = "lblSOperationType";
            this.lblSOperationType.Size = new System.Drawing.Size(80, 13);
            this.lblSOperationType.TabIndex = 123;
            this.lblSOperationType.Text = "Operation Type";
            // 
            // btnRefresh
            // 
            this.btnRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnRefresh.Location = new System.Drawing.Point(124, 171);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(71, 23);
            this.btnRefresh.TabIndex = 13;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(78, 148);
            this.txtSearch.MaxLength = 20;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(117, 20);
            this.txtSearch.TabIndex = 8;
            this.txtSearch.WatermarkEnabled = false;
            this.txtSearch.WatermarkText = "Issue No";
            // 
            // cboSCompany
            // 
            this.cboSCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSCompany.DisplayMember = "Text";
            this.cboSCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSCompany.DropDownHeight = 75;
            this.cboSCompany.FormattingEnabled = true;
            this.cboSCompany.IntegralHeight = false;
            this.cboSCompany.ItemHeight = 14;
            this.cboSCompany.Location = new System.Drawing.Point(78, 10);
            this.cboSCompany.Name = "cboSCompany";
            this.cboSCompany.Size = new System.Drawing.Size(117, 20);
            this.cboSCompany.TabIndex = 9;
            this.cboSCompany.SelectedIndexChanged += new System.EventHandler(this.cboSCompany_SelectedIndexChanged);
            // 
            // lblSCompany
            // 
            this.lblSCompany.AutoSize = true;
            this.lblSCompany.Location = new System.Drawing.Point(3, 13);
            this.lblSCompany.Name = "lblSCompany";
            this.lblSCompany.Size = new System.Drawing.Size(51, 13);
            this.lblSCompany.TabIndex = 109;
            this.lblSCompany.Text = "Company";
            // 
            // lblSCountStatus
            // 
            // 
            // 
            // 
            this.lblSCountStatus.BackgroundStyle.Class = "";
            this.lblSCountStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSCountStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblSCountStatus.Location = new System.Drawing.Point(0, 488);
            this.lblSCountStatus.Name = "lblSCountStatus";
            this.lblSCountStatus.Size = new System.Drawing.Size(200, 26);
            this.lblSCountStatus.TabIndex = 102;
            this.lblSCountStatus.Text = "...";
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(179)))), ((int)(((byte)(219)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(200, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 514);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 10;
            this.expandableSplitterLeft.TabStop = false;
            this.expandableSplitterLeft.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitterLeft_ExpandedChanged);
            // 
            // dotNetBarManager1
            // 
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.dotNetBarManager1.BottomDockSite = this.dockSite4;
            this.dotNetBarManager1.EnableFullSizeDock = false;
            this.dotNetBarManager1.LeftDockSite = this.dockSite1;
            this.dotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.dotNetBarManager1.ParentForm = this;
            this.dotNetBarManager1.RightDockSite = this.dockSite2;
            this.dotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.dotNetBarManager1.ToolbarBottomDockSite = this.dockSite8;
            this.dotNetBarManager1.ToolbarLeftDockSite = this.dockSite5;
            this.dotNetBarManager1.ToolbarRightDockSite = this.dockSite6;
            this.dotNetBarManager1.TopDockSite = this.dockSite3;
            // 
            // dockSite4
            // 
            this.dockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite4.Location = new System.Drawing.Point(0, 514);
            this.dockSite4.Name = "dockSite4";
            this.dockSite4.Size = new System.Drawing.Size(1264, 0);
            this.dockSite4.TabIndex = 18;
            this.dockSite4.TabStop = false;
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(203, 0);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 514);
            this.dockSite1.TabIndex = 15;
            this.dockSite1.TabStop = false;
            // 
            // dockSite2
            // 
            this.dockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite2.Location = new System.Drawing.Point(1264, 0);
            this.dockSite2.Name = "dockSite2";
            this.dockSite2.Size = new System.Drawing.Size(0, 514);
            this.dockSite2.TabIndex = 16;
            this.dockSite2.TabStop = false;
            // 
            // dockSite8
            // 
            this.dockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite8.Location = new System.Drawing.Point(0, 514);
            this.dockSite8.Name = "dockSite8";
            this.dockSite8.Size = new System.Drawing.Size(1264, 0);
            this.dockSite8.TabIndex = 22;
            this.dockSite8.TabStop = false;
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(0, 0);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 514);
            this.dockSite5.TabIndex = 19;
            this.dockSite5.TabStop = false;
            // 
            // dockSite6
            // 
            this.dockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite6.Location = new System.Drawing.Point(1264, 0);
            this.dockSite6.Name = "dockSite6";
            this.dockSite6.Size = new System.Drawing.Size(0, 514);
            this.dockSite6.TabIndex = 20;
            this.dockSite6.TabStop = false;
            // 
            // dockSite3
            // 
            this.dockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite3.Location = new System.Drawing.Point(0, 0);
            this.dockSite3.Name = "dockSite3";
            this.dockSite3.Size = new System.Drawing.Size(1264, 0);
            this.dockSite3.TabIndex = 17;
            this.dockSite3.TabStop = false;
            // 
            // bar1
            // 
            this.bar1.AccessibleDescription = "DotNetBar Bar (bar1)";
            this.bar1.AccessibleName = "DotNetBar Bar";
            this.bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.bar1.Location = new System.Drawing.Point(0, 0);
            this.bar1.MenuBar = true;
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(36, 24);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.bar1.TabIndex = 0;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // panelMain
            // 
            this.panelMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelMain.Controls.Add(this.panelMiddle);
            this.panelMain.Controls.Add(this.expandableSplitterTop);
            this.panelMain.Controls.Add(this.panelTop);
            this.panelMain.Controls.Add(this.PurchaseOrderBindingNavigator);
            this.panelMain.Controls.Add(this.lblStatus);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(203, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1061, 514);
            this.panelMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelMain.Style.GradientAngle = 90;
            this.panelMain.TabIndex = 0;
            this.panelMain.Text = "panelMain";
            // 
            // panelMiddle
            // 
            this.panelMiddle.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelMiddle.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelMiddle.Controls.Add(this.tcDeliveryNote);
            this.panelMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMiddle.Location = new System.Drawing.Point(0, 108);
            this.panelMiddle.Name = "panelMiddle";
            this.panelMiddle.Size = new System.Drawing.Size(1061, 380);
            this.panelMiddle.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelMiddle.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelMiddle.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelMiddle.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelMiddle.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelMiddle.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelMiddle.Style.GradientAngle = 90;
            this.panelMiddle.TabIndex = 1;
            this.panelMiddle.Text = "panelMiddle";
            // 
            // tcDeliveryNote
            // 
            this.tcDeliveryNote.BackColor = System.Drawing.Color.Transparent;
            this.tcDeliveryNote.CanReorderTabs = true;
            this.tcDeliveryNote.Controls.Add(this.tabControlPanel1);
            this.tcDeliveryNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcDeliveryNote.Location = new System.Drawing.Point(0, 0);
            this.tcDeliveryNote.Name = "tcDeliveryNote";
            this.tcDeliveryNote.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcDeliveryNote.SelectedTabIndex = 0;
            this.tcDeliveryNote.Size = new System.Drawing.Size(1061, 380);
            this.tcDeliveryNote.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcDeliveryNote.TabIndex = 0;
            this.tcDeliveryNote.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcDeliveryNote.Tabs.Add(this.tiItemDetails);
            this.tcDeliveryNote.Text = "tabControl1";
            this.tcDeliveryNote.SelectedTabChanged += new DevComponents.DotNetBar.TabStrip.SelectedTabChangedEventHandler(this.tcDeliveryNote_SelectedTabChanged);
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.dgvItemDetailsGrid);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1061, 358);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(147)))), ((int)(((byte)(160)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 1;
            this.tabControlPanel1.TabItem = this.tiItemDetails;
            // 
            // dgvItemDetailsGrid
            // 
            this.dgvItemDetailsGrid.AddNewRow = false;
            this.dgvItemDetailsGrid.AlphaNumericCols = new int[0];
            this.dgvItemDetailsGrid.BackgroundColor = System.Drawing.Color.White;
            this.dgvItemDetailsGrid.CapsLockCols = new int[0];
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItemDetailsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvItemDetailsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvItemDetailsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.ItemID,
            this.BatchNo,
            this.ReferenceNo,
            this.ReferenceSerialNo,
            this.InvoicedQuantity,
            this.DeliveredQty,
            this.QtyAvailable,
            this.Quantity,
            this.Uom,
            this.PackingUnit,
            this.InvQty,
            this.InvUOMID,
            this.IsAutomatic,
            this.IsGroup,
            this.decCurrentTotalDeliveredQty,
            this.TotalOldQuantityForMinItems,
            this.OldBatchID,
            this.BatchID});
            this.dgvItemDetailsGrid.DecimalCols = new int[0];
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvItemDetailsGrid.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvItemDetailsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvItemDetailsGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvItemDetailsGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvItemDetailsGrid.HasSlNo = false;
            this.dgvItemDetailsGrid.LastRowIndex = 0;
            this.dgvItemDetailsGrid.Location = new System.Drawing.Point(1, 1);
            this.dgvItemDetailsGrid.Name = "dgvItemDetailsGrid";
            this.dgvItemDetailsGrid.NegativeValueCols = new int[0];
            this.dgvItemDetailsGrid.NumericCols = new int[0];
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItemDetailsGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvItemDetailsGrid.RowHeadersWidth = 50;
            this.dgvItemDetailsGrid.Size = new System.Drawing.Size(1059, 356);
            this.dgvItemDetailsGrid.TabIndex = 0;
            this.dgvItemDetailsGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItemDetailsGrid_CellValueChanged);
            this.dgvItemDetailsGrid.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvItemDetailsGrid_UserDeletingRow);
            this.dgvItemDetailsGrid.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvItemDetailsGrid_CellMouseClick);
            this.dgvItemDetailsGrid.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvItemDetailsGrid_CellBeginEdit);
            this.dgvItemDetailsGrid.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvItemDetailsGrid_RowsAdded);
            this.dgvItemDetailsGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItemDetailsGrid_CellEndEdit);
            this.dgvItemDetailsGrid.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvItemDetailsGrid_Textbox_TextChanged);
            this.dgvItemDetailsGrid.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvItemDetailsGrid_EditingControlShowing);
            this.dgvItemDetailsGrid.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvItemDetailsGrid_CurrentCellDirtyStateChanged);
            this.dgvItemDetailsGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvItemDetailsGrid_DataError);
            this.dgvItemDetailsGrid.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItemDetailsGrid_CellEnter);
            this.dgvItemDetailsGrid.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvItemDetailsGrid_RowsRemoved);
            this.dgvItemDetailsGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItemDetailsGrid_CellContentClick);
            // 
            // tiItemDetails
            // 
            this.tiItemDetails.AttachedControl = this.tabControlPanel1;
            this.tiItemDetails.Name = "tiItemDetails";
            this.tiItemDetails.Text = "Item Details";
            // 
            // expandableSplitterTop
            // 
            this.expandableSplitterTop.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterTop.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitterTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitterTop.ExpandableControl = this.panelTop;
            this.expandableSplitterTop.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.expandableSplitterTop.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(179)))), ((int)(((byte)(219)))));
            this.expandableSplitterTop.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterTop.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterTop.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.Location = new System.Drawing.Point(0, 105);
            this.expandableSplitterTop.Name = "expandableSplitterTop";
            this.expandableSplitterTop.Size = new System.Drawing.Size(1061, 3);
            this.expandableSplitterTop.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterTop.TabIndex = 1;
            this.expandableSplitterTop.TabStop = false;
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelTop.Controls.Add(this.txtLpoNo);
            this.panelTop.Controls.Add(this.lblLpoNo);
            this.panelTop.Controls.Add(this.cboWarehouse);
            this.panelTop.Controls.Add(this.lblWarehouse);
            this.panelTop.Controls.Add(this.cboCompany);
            this.panelTop.Controls.Add(this.lblCompany);
            this.panelTop.Controls.Add(this.txtRemarks);
            this.panelTop.Controls.Add(this.lblRemarks);
            this.panelTop.Controls.Add(this.lblIssuedBy);
            this.panelTop.Controls.Add(this.lbl);
            this.panelTop.Controls.Add(this.cboReferenceNo);
            this.panelTop.Controls.Add(this.cboOperationType);
            this.panelTop.Controls.Add(this.lblOperationType);
            this.panelTop.Controls.Add(this.txtIssueNo);
            this.panelTop.Controls.Add(this.dtpIssuedDate);
            this.panelTop.Controls.Add(this.lblIssuedDate);
            this.panelTop.Controls.Add(this.lblIssueNo);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1061, 80);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 0;
            // 
            // txtLpoNo
            // 
            this.txtLpoNo.Enabled = false;
            this.txtLpoNo.Location = new System.Drawing.Point(604, 7);
            this.txtLpoNo.MaxLength = 15;
            this.txtLpoNo.Name = "txtLpoNo";
            this.txtLpoNo.Size = new System.Drawing.Size(112, 20);
            this.txtLpoNo.TabIndex = 253;
            // 
            // lblLpoNo
            // 
            this.lblLpoNo.AutoSize = true;
            this.lblLpoNo.Location = new System.Drawing.Point(561, 9);
            this.lblLpoNo.Name = "lblLpoNo";
            this.lblLpoNo.Size = new System.Drawing.Size(44, 13);
            this.lblLpoNo.TabIndex = 252;
            this.lblLpoNo.Text = "LPONO";
            // 
            // cboWarehouse
            // 
            this.cboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWarehouse.DisplayMember = "Text";
            this.cboWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWarehouse.DropDownHeight = 75;
            this.cboWarehouse.FormattingEnabled = true;
            this.cboWarehouse.IntegralHeight = false;
            this.cboWarehouse.ItemHeight = 14;
            this.cboWarehouse.Location = new System.Drawing.Point(122, 31);
            this.cboWarehouse.Name = "cboWarehouse";
            this.cboWarehouse.Size = new System.Drawing.Size(204, 20);
            this.cboWarehouse.TabIndex = 1;
            this.cboWarehouse.SelectedIndexChanged += new System.EventHandler(this.cboWarehouse_SelectedIndexChanged);
            this.cboWarehouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(25, 33);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(62, 13);
            this.lblWarehouse.TabIndex = 218;
            this.lblWarehouse.Text = "Warehouse";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 75;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(122, 7);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(204, 20);
            this.cboCompany.TabIndex = 0;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Location = new System.Drawing.Point(25, 9);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 216;
            this.lblCompany.Text = "Company";
            // 
            // txtRemarks
            // 
            // 
            // 
            // 
            this.txtRemarks.Border.Class = "TextBoxBorder";
            this.txtRemarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtRemarks.Border.WordWrap = true;
            this.txtRemarks.Location = new System.Drawing.Point(777, 7);
            this.txtRemarks.MaxLength = 1000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(311, 64);
            this.txtRemarks.TabIndex = 6;
            this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(722, 10);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 214;
            this.lblRemarks.Text = "Remarks";
            // 
            // lblIssuedBy
            // 
            this.lblIssuedBy.AutoSize = true;
            this.lblIssuedBy.Location = new System.Drawing.Point(447, 33);
            this.lblIssuedBy.Name = "lblIssuedBy";
            this.lblIssuedBy.Size = new System.Drawing.Size(53, 13);
            this.lblIssuedBy.TabIndex = 5;
            this.lblIssuedBy.Text = "Issued By";
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(365, 33);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(53, 13);
            this.lbl.TabIndex = 211;
            this.lbl.Text = "Issued By";
            // 
            // cboReferenceNo
            // 
            this.cboReferenceNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboReferenceNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboReferenceNo.DisplayMember = "Text";
            this.cboReferenceNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboReferenceNo.DropDownHeight = 75;
            this.cboReferenceNo.FormattingEnabled = true;
            this.cboReferenceNo.IntegralHeight = false;
            this.cboReferenceNo.ItemHeight = 14;
            this.cboReferenceNo.Location = new System.Drawing.Point(244, 54);
            this.cboReferenceNo.Name = "cboReferenceNo";
            this.cboReferenceNo.Size = new System.Drawing.Size(82, 20);
            this.cboReferenceNo.TabIndex = 3;
            this.cboReferenceNo.SelectedIndexChanged += new System.EventHandler(this.cboReferenceNo_SelectedIndexChanged);
            this.cboReferenceNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // cboOperationType
            // 
            this.cboOperationType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOperationType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOperationType.DisplayMember = "Text";
            this.cboOperationType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboOperationType.DropDownHeight = 75;
            this.cboOperationType.FormattingEnabled = true;
            this.cboOperationType.IntegralHeight = false;
            this.cboOperationType.ItemHeight = 14;
            this.cboOperationType.Location = new System.Drawing.Point(122, 54);
            this.cboOperationType.Name = "cboOperationType";
            this.cboOperationType.Size = new System.Drawing.Size(121, 20);
            this.cboOperationType.TabIndex = 2;
            this.cboOperationType.SelectedIndexChanged += new System.EventHandler(this.cboOperationType_SelectedIndexChanged);
            this.cboOperationType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblOperationType
            // 
            this.lblOperationType.AutoSize = true;
            this.lblOperationType.Location = new System.Drawing.Point(25, 56);
            this.lblOperationType.Name = "lblOperationType";
            this.lblOperationType.Size = new System.Drawing.Size(80, 13);
            this.lblOperationType.TabIndex = 205;
            this.lblOperationType.Text = "Operation Type";
            // 
            // txtIssueNo
            // 
            this.txtIssueNo.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtIssueNo.Border.Class = "TextBoxBorder";
            this.txtIssueNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtIssueNo.Location = new System.Drawing.Point(450, 7);
            this.txtIssueNo.MaxLength = 20;
            this.txtIssueNo.Name = "txtIssueNo";
            this.txtIssueNo.Size = new System.Drawing.Size(105, 20);
            this.txtIssueNo.TabIndex = 4;
            this.txtIssueNo.TabStop = false;
            this.txtIssueNo.TextChanged += new System.EventHandler(this.txtIssueNo_TextChanged);
            // 
            // dtpIssuedDate
            // 
            this.dtpIssuedDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpIssuedDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpIssuedDate.Location = new System.Drawing.Point(450, 54);
            this.dtpIssuedDate.Name = "dtpIssuedDate";
            this.dtpIssuedDate.Size = new System.Drawing.Size(105, 20);
            this.dtpIssuedDate.TabIndex = 5;
            this.dtpIssuedDate.ValueChanged += new System.EventHandler(this.dtpIssuedDate_ValueChanged);
            // 
            // lblIssuedDate
            // 
            this.lblIssuedDate.AutoSize = true;
            this.lblIssuedDate.Location = new System.Drawing.Point(365, 56);
            this.lblIssuedDate.Name = "lblIssuedDate";
            this.lblIssuedDate.Size = new System.Drawing.Size(64, 13);
            this.lblIssuedDate.TabIndex = 188;
            this.lblIssuedDate.Text = "Issued Date";
            // 
            // lblIssueNo
            // 
            this.lblIssueNo.AutoSize = true;
            this.lblIssueNo.Location = new System.Drawing.Point(365, 10);
            this.lblIssueNo.Name = "lblIssueNo";
            this.lblIssueNo.Size = new System.Drawing.Size(49, 13);
            this.lblIssueNo.TabIndex = 185;
            this.lblIssueNo.Text = "Issue No";
            // 
            // PurchaseOrderBindingNavigator
            // 
            this.PurchaseOrderBindingNavigator.AccessibleDescription = "DotNetBar Bar (PurchaseOrderBindingNavigator)";
            this.PurchaseOrderBindingNavigator.AccessibleName = "DotNetBar Bar";
            this.PurchaseOrderBindingNavigator.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.PurchaseOrderBindingNavigator.Dock = System.Windows.Forms.DockStyle.Top;
            this.PurchaseOrderBindingNavigator.DockLine = 1;
            this.PurchaseOrderBindingNavigator.DockOffset = 73;
            this.PurchaseOrderBindingNavigator.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.PurchaseOrderBindingNavigator.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.PurchaseOrderBindingNavigator.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnClear,
            this.bnDelete,
            this.bnPrint,
            this.btnCustomerwisePrint,
            this.btnPicklist,
            this.bnEmail,
            this.btnPickListEmail});
            this.PurchaseOrderBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.PurchaseOrderBindingNavigator.Name = "PurchaseOrderBindingNavigator";
            this.PurchaseOrderBindingNavigator.Size = new System.Drawing.Size(1061, 25);
            this.PurchaseOrderBindingNavigator.Stretch = true;
            this.PurchaseOrderBindingNavigator.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PurchaseOrderBindingNavigator.TabIndex = 12;
            this.PurchaseOrderBindingNavigator.TabStop = false;
            this.PurchaseOrderBindingNavigator.Text = "bar2";
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlEnter);
            this.bnAddNewItem.Text = "Add";
            this.bnAddNewItem.Tooltip = "Add New Information";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Text = "Save";
            this.bnSaveItem.Tooltip = "Save";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnClear
            // 
            this.bnClear.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnClear.Image = ((System.Drawing.Image)(resources.GetObject("bnClear.Image")));
            this.bnClear.Name = "bnClear";
            this.bnClear.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.bnClear.Text = "Clear";
            this.bnClear.Tooltip = "Clear";
            this.bnClear.Visible = false;
            this.bnClear.Click += new System.EventHandler(this.bnClear_Click);
            // 
            // bnDelete
            // 
            this.bnDelete.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnDelete.Image = ((System.Drawing.Image)(resources.GetObject("bnDelete.Image")));
            this.bnDelete.Name = "bnDelete";
            this.bnDelete.Text = "Delete";
            this.bnDelete.Tooltip = "Delete";
            this.bnDelete.Click += new System.EventHandler(this.bnDelete_Click);
            // 
            // bnPrint
            // 
            this.bnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnPrint.Image = ((System.Drawing.Image)(resources.GetObject("bnPrint.Image")));
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.bnPrint.Text = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // btnCustomerwisePrint
            // 
            this.btnCustomerwisePrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnCustomerwisePrint.Image = ((System.Drawing.Image)(resources.GetObject("btnCustomerwisePrint.Image")));
            this.btnCustomerwisePrint.Name = "btnCustomerwisePrint";
            this.btnCustomerwisePrint.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.btnCustomerwisePrint.Text = "Customer Wise Print";
            this.btnCustomerwisePrint.Tooltip = "Customer Wise Print";
            this.btnCustomerwisePrint.Visible = false;
            this.btnCustomerwisePrint.Click += new System.EventHandler(this.btnCustomerwisePrint_Click);
            // 
            // btnPicklist
            // 
            this.btnPicklist.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPicklist.Image = ((System.Drawing.Image)(resources.GetObject("btnPicklist.Image")));
            this.btnPicklist.Name = "btnPicklist";
            this.btnPicklist.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.btnPicklist.Text = "Placement List";
            this.btnPicklist.Visible = false;
            this.btnPicklist.Click += new System.EventHandler(this.btnPicklist_Click);
            // 
            // bnEmail
            // 
            this.bnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnEmail.Image = ((System.Drawing.Image)(resources.GetObject("bnEmail.Image")));
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.bnEmail.Text = "Email";
            this.bnEmail.Click += new System.EventHandler(this.bnEmail_Click);
            // 
            // btnPickListEmail
            // 
            this.btnPickListEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPickListEmail.Image = ((System.Drawing.Image)(resources.GetObject("btnPickListEmail.Image")));
            this.btnPickListEmail.Name = "btnPickListEmail";
            this.btnPickListEmail.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.btnPickListEmail.Text = "Placement List Email";
            this.btnPickListEmail.Visible = false;
            this.btnPickListEmail.Click += new System.EventHandler(this.btnPickListEmail_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblStatus.CloseButtonVisible = false;
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblStatus.Location = new System.Drawing.Point(0, 488);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.OptionsButtonVisible = false;
            this.lblStatus.Size = new System.Drawing.Size(1061, 26);
            this.lblStatus.TabIndex = 201;
            // 
            // tmrItemIssue
            // 
            this.tmrItemIssue.Interval = 2000;
            this.tmrItemIssue.Tick += new System.EventHandler(this.tmrItemIssue_Tick);
            // 
            // ErrItemIssue
            // 
            this.ErrItemIssue.ContainerControl = this;
            this.ErrItemIssue.RightToLeft = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Item Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn3.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Visible = false;
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn4.HeaderText = "QtyReceived";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn5.HeaderText = "Batch Number";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Visible = false;
            this.dataGridViewTextBoxColumn5.Width = 90;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn6.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn6.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn7.HeaderText = "Total";
            this.dataGridViewTextBoxColumn7.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Visible = false;
            this.dataGridViewTextBoxColumn7.Width = 90;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn8.HeaderText = "Status";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Visible = false;
            this.dataGridViewTextBoxColumn8.Width = 90;
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn9.HeaderText = "OrderDetailID";
            this.dataGridViewTextBoxColumn9.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Visible = false;
            this.dataGridViewTextBoxColumn9.Width = 90;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "PurchaseOrderID";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Visible = false;
            this.dataGridViewTextBoxColumn10.Width = 90;
            // 
            // dataGridViewTextBoxColumn11
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewTextBoxColumn11.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn11.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Visible = false;
            this.dataGridViewTextBoxColumn11.Width = 90;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn12.HeaderText = "Uom";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn13.HeaderText = "BatchID";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Visible = false;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn14.HeaderText = "BatchNo";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewTextBoxColumn15.HeaderText = "QtyAvailable";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Visible = false;
            this.dataGridViewTextBoxColumn15.Width = 150;
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn16.HeaderText = "Issued Qty";
            this.dataGridViewTextBoxColumn16.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Visible = false;
            this.dataGridViewTextBoxColumn16.Width = 150;
            // 
            // dataGridViewTextBoxColumn17
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn17.HeaderText = "Used Qty";
            this.dataGridViewTextBoxColumn17.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn17.Width = 150;
            // 
            // dataGridViewTextBoxColumn18
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn18.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewTextBoxColumn18.HeaderText = "Issued Qty";
            this.dataGridViewTextBoxColumn18.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn19
            // 
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewTextBoxColumn19.HeaderText = "Used Qty";
            this.dataGridViewTextBoxColumn19.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn20
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn20.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewTextBoxColumn20.HeaderText = "Used Qty";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            // 
            // CntxtHistory
            // 
            this.CntxtHistory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ShowItemHistory,
            this.ShowSaleRate,
            this.showGroupItemDetails});
            this.CntxtHistory.Name = "CntxtHistory";
            this.CntxtHistory.Size = new System.Drawing.Size(205, 70);
            // 
            // ShowItemHistory
            // 
            this.ShowItemHistory.Name = "ShowItemHistory";
            this.ShowItemHistory.Size = new System.Drawing.Size(204, 22);
            this.ShowItemHistory.Text = "Show Item History";
            this.ShowItemHistory.Visible = false;
            // 
            // ShowSaleRate
            // 
            this.ShowSaleRate.Name = "ShowSaleRate";
            this.ShowSaleRate.Size = new System.Drawing.Size(204, 22);
            this.ShowSaleRate.Text = "Show Sale Rates";
            this.ShowSaleRate.Visible = false;
            // 
            // showGroupItemDetails
            // 
            this.showGroupItemDetails.Name = "showGroupItemDetails";
            this.showGroupItemDetails.Size = new System.Drawing.Size(204, 22);
            this.showGroupItemDetails.Text = "Show Group Item Details";
            this.showGroupItemDetails.Click += new System.EventHandler(this.showGroupItemDetails_Click);
            // 
            // ItemCode
            // 
            this.ItemCode.HeaderText = "Item Code";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemCode.Width = 75;
            // 
            // ItemName
            // 
            this.ItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.MinimumWidth = 200;
            this.ItemName.Name = "ItemName";
            this.ItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ItemID
            // 
            this.ItemID.HeaderText = "ItemID";
            this.ItemID.Name = "ItemID";
            this.ItemID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemID.Visible = false;
            // 
            // BatchNo
            // 
            this.BatchNo.HeaderText = "BatchNo";
            this.BatchNo.Name = "BatchNo";
            this.BatchNo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BatchNo.Visible = false;
            this.BatchNo.Width = 150;
            // 
            // ReferenceNo
            // 
            this.ReferenceNo.HeaderText = "ReferenceNo";
            this.ReferenceNo.Name = "ReferenceNo";
            this.ReferenceNo.ReadOnly = true;
            this.ReferenceNo.Visible = false;
            // 
            // ReferenceSerialNo
            // 
            this.ReferenceSerialNo.HeaderText = "ReferenceSerialNo";
            this.ReferenceSerialNo.Name = "ReferenceSerialNo";
            this.ReferenceSerialNo.ReadOnly = true;
            this.ReferenceSerialNo.Visible = false;
            // 
            // InvoicedQuantity
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.InvoicedQuantity.DefaultCellStyle = dataGridViewCellStyle5;
            this.InvoicedQuantity.HeaderText = "InvoicedQuantity";
            this.InvoicedQuantity.Name = "InvoicedQuantity";
            this.InvoicedQuantity.ReadOnly = true;
            // 
            // DeliveredQty
            // 
            this.DeliveredQty.HeaderText = "DeliveredQty";
            this.DeliveredQty.Name = "DeliveredQty";
            this.DeliveredQty.ReadOnly = true;
            this.DeliveredQty.Width = 80;
            // 
            // QtyAvailable
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.QtyAvailable.DefaultCellStyle = dataGridViewCellStyle6;
            this.QtyAvailable.HeaderText = "QtyAvailable";
            this.QtyAvailable.Name = "QtyAvailable";
            this.QtyAvailable.ReadOnly = true;
            this.QtyAvailable.Width = 80;
            // 
            // Quantity
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle7;
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.MaxInputLength = 10;
            this.Quantity.Name = "Quantity";
            this.Quantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Quantity.Width = 80;
            // 
            // Uom
            // 
            this.Uom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Uom.HeaderText = "UOM";
            this.Uom.Name = "Uom";
            this.Uom.ReadOnly = true;
            this.Uom.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Uom.Width = 90;
            // 
            // PackingUnit
            // 
            this.PackingUnit.HeaderText = "PKG";
            this.PackingUnit.MaxInputLength = 50;
            this.PackingUnit.Name = "PackingUnit";
            // 
            // InvQty
            // 
            this.InvQty.HeaderText = "InvQty";
            this.InvQty.Name = "InvQty";
            this.InvQty.Visible = false;
            // 
            // InvUOMID
            // 
            this.InvUOMID.HeaderText = "InvUOMID";
            this.InvUOMID.Name = "InvUOMID";
            this.InvUOMID.Visible = false;
            // 
            // IsAutomatic
            // 
            this.IsAutomatic.HeaderText = "IsAutomatic";
            this.IsAutomatic.Name = "IsAutomatic";
            this.IsAutomatic.Visible = false;
            // 
            // IsGroup
            // 
            this.IsGroup.HeaderText = "IsGroup";
            this.IsGroup.Name = "IsGroup";
            this.IsGroup.Visible = false;
            // 
            // decCurrentTotalDeliveredQty
            // 
            this.decCurrentTotalDeliveredQty.HeaderText = "decCurrentTotalDeliveredQty";
            this.decCurrentTotalDeliveredQty.Name = "decCurrentTotalDeliveredQty";
            this.decCurrentTotalDeliveredQty.Visible = false;
            // 
            // TotalOldQuantityForMinItems
            // 
            this.TotalOldQuantityForMinItems.HeaderText = "TotalOldQuantityForMinItems";
            this.TotalOldQuantityForMinItems.Name = "TotalOldQuantityForMinItems";
            this.TotalOldQuantityForMinItems.Visible = false;
            // 
            // OldBatchID
            // 
            this.OldBatchID.HeaderText = "OldBatchID";
            this.OldBatchID.Name = "OldBatchID";
            this.OldBatchID.Visible = false;
            // 
            // BatchID
            // 
            this.BatchID.HeaderText = "BatchID";
            this.BatchID.Name = "BatchID";
            this.BatchID.Visible = false;
            // 
            // FrmItemIssue
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.ClientSize = new System.Drawing.Size(1264, 514);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.dockSite2);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.expandableSplitterLeft);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.dockSite3);
            this.Controls.Add(this.dockSite4);
            this.Controls.Add(this.dockSite5);
            this.Controls.Add(this.dockSite6);
            this.Controls.Add(this.dockSite8);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmItemIssue";
            this.Text = "Delivery Note";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmItemIssue_Load);
            this.Shown += new System.EventHandler(this.FrmItemIssue_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmItemIssue_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmItemIssue_KeyDown);
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvIssueDisplay)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.panelMain.ResumeLayout(false);
            this.panelMiddle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcDeliveryNote)).EndInit();
            this.tcDeliveryNote.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemDetailsGrid)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderBindingNavigator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrItemIssue)).EndInit();
            this.CntxtHistory.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        #endregion

        public FrmMain objFrmTradingMain;
        clsBLLItemIssueMaster MobjClsBLLItemIssueMaster;//Object Of clsBllItemIssueMaster
        string MstrCommonMessage;                   // for setting error message
        DataTable datMessages;                      // having forms error messages
        MessageBoxIcon MmsgMessageIcon;             // obj of message icon
        ClsNotificationNew MobjClsNotification;     //obj of clsNotification
        ClsLogWriter MobjClsLogWriter;              // obj of clsLogWriter

        bool blnIsEditMode = false;
        bool MblnPrintEmailPermission = false;
        bool MblnAddPermission = false;
        bool MblnUpdatePermission = false;
        bool MblnDeletePermission = false;
        bool MblnIsFromClear = false;
        bool MblnIsEditable = true;
        bool MchangeStatus = false;    //Check any edit or new entry done
        DataTable MDtItemGroupIssueDetails = new DataTable();
        DataTable dtItemDetails = new DataTable();
        clsBLLCommonUtility mobjClsBllCommonUtility;
        public int PintFormType = 0; // 1 - Delivery Note 2 - Material Issue
        public int PintCompanyID = 0;// for selecting the company in add mode   -- alerts
        public long PlngReferenceID = 0;// for selecting the reference No (Invoice / DMR)   -- alerts
        public int PintOrderTypeID = 0;// for selecting the operation type  -- alerts
        public long lngItemIssueID = 0; // for selected item issue -- alerts
        //decimal decOldDeliveredQty;
        //old quantity
        //decimal decOldQuantityDelivered = 0;
        //decimal decOldDeliveredQuantity = 0;
        decimal decOldQuantityDelivered = 0;
        decimal decOldDeliveredQuantity = 0;
        decimal  decInvQty;
        private int SItemID;
        private int SIsGroup;
        decimal decTotalOldQuantityForMinItem;
        DataTable BatchlessItems = new DataTable();
        DataTable dtGroupDetailsI = new DataTable();
        public FrmItemIssue()
        {
            //Constructor

            InitializeComponent();

            MobjClsBLLItemIssueMaster = new clsBLLItemIssueMaster();
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotificationNew();
            tmrItemIssue.Interval = ClsCommonSettings.TimerInterval;
            mobjClsBllCommonUtility = new clsBLLCommonUtility();
            MDtItemGroupIssueDetails.Columns.Add("ItemGroupID");
            MDtItemGroupIssueDetails.Columns.Add("ReferenceNo");
            MDtItemGroupIssueDetails.Columns.Add("ReferenceSerialNo");
            MDtItemGroupIssueDetails.Columns.Add("ItemID");
            MDtItemGroupIssueDetails.Columns.Add("ItemCode");
            MDtItemGroupIssueDetails.Columns.Add("ItemName");
            MDtItemGroupIssueDetails.Columns.Add("BatchID");
            MDtItemGroupIssueDetails.Columns.Add("UOMID");
            MDtItemGroupIssueDetails.Columns.Add("Quantity");
        }


        private void LoadMessage()
        {
            try
            {
                //Loads error messages
                datMessages = new DataTable();
                datMessages = MobjClsNotification.FillMessageArray((int)FormID.ItemIssue, ClsCommonSettings.ProductID);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadMessage() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in Load Message() " + ex.Message, 2);
            }
        }

        private void ArrangeControls()
        {
            if (PintFormType == 2)
            {
                this.Text = "Material Issue";

                cboOperationType.Visible = false;
                lblOperationType.Text = "DMR No";
                cboReferenceNo.Left = cboOperationType.Left;
                cboReferenceNo.Width = cboOperationType.Width;

                lblSOperationType.Visible = false;
                cboSOperationType.Visible = false;

                lblSIssueNo.Top = lblTo.Top;
                txtSearch.Top = dtpSTo.Top;
                lblTo.Top = lblFrom.Top;
                dtpSTo.Top = dtpSFrom.Top;
                lblFrom.Top = lblSIssuedBy.Top;
                dtpSFrom.Top = cboSIssuedBy.Top;
                lblSIssuedBy.Top = lblSWarehouse.Top;
                cboSIssuedBy.Top = cboSWarehouse.Top;
                lblSWarehouse.Top = lblSOperationType.Top;
                cboSWarehouse.Top = cboSOperationType.Top;

                panelLeftTop.Height = panelLeftTop.Height - cboSOperationType.Height;
                expandablePanel1.Height = expandablePanel1.Height - cboSOperationType.Height;
            }
        }

        private void LoadCombo(int intType)
        {
            // 0 - To Load Combos
            // 1 - To Load Warehouse
            // 2 - To Load OperationType
            // 3 - To Load Search Company
            // 4 - To Load Search Operation Type
            // 5 - To Load Search Warehouse
            try
            {
                clsBLLPermissionSettings objBllPermissionSettings = new clsBLLPermissionSettings();
                DataTable datCombos = null;
                if (intType == 0 || intType == 1)
                {
                    datCombos = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID + "" });
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;
                }
                if (intType == 0 || intType == 2)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "OrderTypeID,OrderType", "CommonOrderTypeReference", "OperationTypeID In (" + (int)OperationType.DeliveryNote + ") And OrderTypeID In (" + (int)OperationOrderType.DNOTSalesInvoice + "," + (int)OperationOrderType.DNOTTransfer + "," + (int)OperationOrderType.DNOTTransfer + "," + (int)OperationOrderType.DNOTSalesOrder + ")" });
                    cboOperationType.DisplayMember = "OrderType";
                    cboOperationType.ValueMember = "OrderTypeID";
                    cboOperationType.DataSource = datCombos;
                }
                if (intType == 0 || intType == 3)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID + "" });
                    
                    cboSCompany.ValueMember = "CompanyID";
                    cboSCompany.DisplayMember = "CompanyName";
                    cboSCompany.DataSource = datCombos;
                }
                if (intType == 0 || intType == 4)
                {
                    datCombos = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "OrderTypeID,OrderType", "CommonOrderTypeReference", "OperationTypeID In (" + (int)OperationType.DeliveryNote + ") And OrderTypeID In (" + (int)OperationOrderType.DNOTSalesInvoice + "," + (int)OperationOrderType.DNOTTransfer + "," + (int)OperationOrderType.DNOTSalesOrder +")" });
                    cboSOperationType.DisplayMember = "OrderType";
                    cboSOperationType.ValueMember = "OrderTypeID";
                    cboSOperationType.DataSource = datCombos;
                }
                if (intType == 0 || intType == 5)
                {
                }
                if (intType == 0 || intType == 6)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "Distinct InvWarehouse.WarehouseID,WarehouseName", "" +
                        "InvWarehouse INNER JOIN InvWarehouseCompanyDetails WC ON InvWarehouse.WarehouseID=WC.WarehouseID", "" +
                        "WC.CompanyID = " + cboCompany.SelectedValue.ToInt32() + " And IsActive = 1" });
                    cboWarehouse.ValueMember = "WarehouseID";
                    cboWarehouse.DisplayMember = "WarehouseName";
                    cboWarehouse.DataSource = datCombos;

                }
                if (intType == 0 || intType == 7)
                {
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadCombos() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in LoadCombos() " + ex.Message, 2);
            }
        }

      
        private void cboOperationType_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                cboReferenceNo.DataSource = null;
                if (cboOperationType.SelectedValue != null && cboWarehouse.SelectedValue != null)
                {

                    txtLpoNo.Text = string.Empty;
                    if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesInvoice)
                    {
                        dgvItemDetailsGrid.Visible = true;
                        dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
                        dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = true;
                        btnCustomerwisePrint.Enabled = false;
                        dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].HeaderText = "InvoicedQuantity";

                        //BatchNo.Visible = true;
                        ItemCode.ReadOnly = ItemName.ReadOnly = Quantity.ReadOnly = false;
                        Uom.ReadOnly = true;
                        //= BatchNo.ReadOnly//
                        dgvItemDetailsGrid.ReadOnly = false;
                        dgvItemDetailsGrid.AllowUserToAddRows = true;
                        dgvItemDetailsGrid.AllowUserToDeleteRows = true;
                       // cboWarehouse.Enabled = true;
                    }
              
                    else if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTTransfer)
                    {
                        dgvItemDetailsGrid.Visible = true;
                        dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
                        btnCustomerwisePrint.Enabled = false;
                        //BatchNo.Visible = true;
                        dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = false;
                        ItemCode.ReadOnly = ItemName.ReadOnly   = Quantity.ReadOnly = true;
                        //BatchNo.ReadOnly =
                        
                        dgvItemDetailsGrid.ReadOnly = true;
                        dgvItemDetailsGrid.AllowUserToAddRows = false;
                        dgvItemDetailsGrid.AllowUserToDeleteRows = false;
                        //  cboWarehouse.Enabled = false;
                    }
                    else if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesOrder)
                    {
                        dgvItemDetailsGrid.Visible = true;
                        dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
                        dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = true;
                        btnCustomerwisePrint.Enabled = false;
                        dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].HeaderText = "SalesOrderQty";
                       // BatchNo.Visible = true;
                        ItemCode.ReadOnly = ItemName.ReadOnly   = Quantity.ReadOnly = false;
                        Uom.ReadOnly = true;
                        //= BatchNo.ReadOnly//Uom.ReadOnly
                        dgvItemDetailsGrid.ReadOnly = false;
                        dgvItemDetailsGrid.AllowUserToAddRows = false;
                        dgvItemDetailsGrid.AllowUserToDeleteRows = true;
                    }
                    if (!blnIsEditMode)
                        FillReferenceNo();
                }
                else
                {
                    cboReferenceNo.DataSource = null;
                }

            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in cboOperationType_SelectedIndexChanged() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in cboOperationType_SelectedIndexChanged() " + ex.Message, 2);
            }
        }
        private void FillReferenceNoEditMode()
        {
            if (cboOperationType.SelectedValue != null && cboCompany.SelectedValue != null)
            {
                dgvItemDetailsGrid.Rows.Clear();
                DataTable dtReference = null;
                string strCondition = string.Empty;
                int intCompanyID = cboCompany.SelectedValue.ToInt32(); ;
                if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesInvoice)
                {



                    cboReferenceNo.DataSource = null;
                    DataTable dtCombo = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "SalesInvoiceID ,SalesInvoiceNo ", "" +
                        "InvSalesInvoiceMaster", "CompanyID=" + intCompanyID + " AND DeliveryNotRequired = 0" });
                    cboReferenceNo.ValueMember = "SalesInvoiceID";
                    cboReferenceNo.DisplayMember = "SalesInvoiceNo";
                    cboReferenceNo.DataSource = dtCombo;
                    dtCombo.DefaultView.RowFilter = "SalesInvoiceID = " + MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intReferenceID;
                    MblnIsEditable = false;
                    
                }
                else if (Convert.ToInt32(cboOperationType.SelectedValue) == (int)OperationOrderType.DNOTSalesOrder)
                {

                    cboReferenceNo.DataSource = null;
                    DataTable dtCombo = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "SalesOrderID,SalesOrderNo", "InvSalesOrderMaster", "CompanyID=" + intCompanyID });
                    cboReferenceNo.ValueMember = "SalesOrderID";
                    cboReferenceNo.DisplayMember = "SalesOrderNo";
                    cboReferenceNo.DataSource = dtCombo;
                    MblnIsEditable = false;
                }
               
            } 
        }
        private void FillReferenceNo()
        {
            try
            {
                if (cboOperationType.SelectedValue != null && cboCompany.SelectedValue!=null)
                {
                    dgvItemDetailsGrid.Rows.Clear();
                    DataTable dtReference = null;
                    string strCondition = string.Empty;
                    int intCompanyID = cboCompany.SelectedValue.ToInt32(); ;
                    if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesInvoice)
                    {
                        if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID == 0)
                           // strCondition = "InvSalesInvoiceMaster.StatusID In(" + (Int32)OperationStatusType.SPartiallyDelivered + "," + (Int32)OperationStatusType.SInvoiceOpen + " )And SalesInvoiceID  Not In (SELECT DISTINCT SIM.SalesInvoiceID FROM InvSalesInvoiceMaster SIM inner join InvSalesOrderMaster SOM on SOM.SalesOrderID = SIM.SalesOrderID inner join InvItemIssueMaster IM ON SOM.SalesOrderID =IM.ReferenceID and IM.OrderTypeID =" + (int)OperationOrderType.DNOTSalesOrder + " ) And InvSalesInvoiceMaster.CompanyID =" + Convert.ToInt32(cboCompany.SelectedValue) + "";
                        //dtReference = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "SalesInvoiceID as ReferenceID,SalesInvoiceNo as ReferenceNo", "InvSalesInvoiceMaster", strCondition });
                       dtReference = MobjClsBLLItemIssueMaster.GetSalesInvoiceNos(intCompanyID);
                    }

                    else if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTTransfer)
                    {
                        if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID == 0)
                            strCondition = "InvStockTransferMaster.StatusID In (" + (Int32)OperationStatusType.STApproved + "," + (int)OperationStatusType.STSubmitted + ") And InvStockTransferMaster.WarehouseID =" + Convert.ToInt32(cboWarehouse.SelectedValue) + "";
                        dtReference = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "StockTransferID as ReferenceID,StockTransferNo as ReferenceNo", "InvStockTransferMaster", strCondition });
                            
                    }
                    //else if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTDMR)
                    //{
                    //    if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID == 0)
                    //        strCondition = "PrdDMRMaster.StatusID In (" + (int)OperationStatusType.DMROpened + ")";
                    //    dtReference = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "DMRID as ReferenceID,DMRNo as ReferenceNo", "PrdDMRMaster", strCondition });
                    //}
                    else if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesOrder)
                    {
                        if(MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID ==0)
                        //    strCondition = "InvSalesOrderMaster.StatusID In (" + (Int32)OperationStatusType.SOrderSubmitted + "," + (Int32)OperationStatusType.SOrderClosed + "," + (Int32)OperationStatusType.SOrderApproved + ")And SalesOrderID Not In( select SalesOrderID from InvSalesOrderMaster where  DeliveredStatus = " + (int)SalesOrderDeliveryStatus.Delivered + ")And SalesOrderID Not In(SELECT DISTINCT SM.SalesOrderID FROM InvSalesOrderMaster SM inner  join InvSalesInvoiceMaster SIM on SIM.SalesOrderID = SM.SalesOrderID inner JOIN InvItemIssueMaster IM ON SIM.SalesInvoiceID =IM.ReferenceID and IM.OrderTypeID = "+(int)OperationOrderType.DNOTSalesInvoice +") And InvSalesOrderMaster.CompanyID =" + Convert.ToInt32(cboCompany.SelectedValue) + "";
                        //dtReference = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "SalesOrderID as ReferenceID,SalesOrderNo as ReferenceNo", "InvSalesOrderMaster", strCondition });
                        dtReference = MobjClsBLLItemIssueMaster.GetSalesOrderNos(intCompanyID);
                    }
                    else
                        dtReference = null;
                    //  cboReferenceNo.DataSource = null;
                  //  cboReferenceNo.DataSource = null;
                    cboReferenceNo.ValueMember = "ReferenceID";
                    cboReferenceNo.DisplayMember = "ReferenceNo";
                    cboReferenceNo.DataSource = dtReference;
                }
                else
                {
                    cboReferenceNo.DataSource = null;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FillReferenceNo() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in FillReferenceNo() " + ex.Message, 2);
            }
        }

        private void FrmItemIssue_Load(object sender, EventArgs e)
        {
            try
            {
                if (!ClsCommonSettings.PblnIsLocationWise)
                {
                   btnPicklist.Visible = false;
                   btnPickListEmail.Visible = false;
                }
                dtpSFrom.Value = ClsCommonSettings.GetServerDate();
                dtpSTo.Value = ClsCommonSettings.GetServerDate();
                LoadCombo(0);
                LoadMessage();
                SetPermissions();
                AddNewItemIssue();
                ArrangeControls();
                dtpIssuedDate.MaxDate = ClsCommonSettings.GetServerDate();

                if (PintCompanyID != 0 && PintOrderTypeID != 0 && PlngReferenceID != 0)
                {
                    cboCompany.SelectedValue = PintCompanyID;
                    if (cboWarehouse.Items.Count > 0)
                        cboWarehouse.SelectedIndex = 0;
                    cboOperationType.SelectedValue = PintOrderTypeID;
                    cboReferenceNo.SelectedValue = PlngReferenceID;
                }
                else if (lngItemIssueID != 0)
                {
                    MobjClsBLLItemIssueMaster = new clsBLLItemIssueMaster();
                    MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID = lngItemIssueID;
                    DisplayIssueInfo();
                }

            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FrmItemIssue_Load() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in FrmItemIssue_Load() " + ex.Message, 2);
            }
        }
     
        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                if(PintFormType == 1)
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory , (Int32)eMenuID.DeliveryNote, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                else if(PintFormType == 2)
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.MaterialIssue, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }
        private void FillBatchNoColumn(int intItemID, bool blnIsEdit)
        {
            //DataTable datCombos = new DataTable();
            //BatchNo.DataSource = null;
            //if (!blnIsEdit)
            //    datCombos = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "BD.BatchID,BD.BatchNo", "InvBatchDetails BD Inner Join InvItemStockDetails SD On SD.BatchID = BD.BatchID And SD.ItemID = BD.ItemID", "BD.ItemID = " + intItemID + " And SD.WarehouseID = " + Convert.ToInt32(cboWarehouse.SelectedValue) + " And IsNull((SD.GRNQuantity+SD.ReceivedFromTransfer + SD.ProducedQuantity)-(SD.SoldQuantity+ SD.TransferedQuantity+SD.DamagedQuantity+SD.DemoQuantity),0) > 0 And IsNull(BD.ExpiryDate,GetDate()) >= '"+ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")+"'" });
            //else
            //    datCombos = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "BD.BatchID,BD.BatchNo", "InvBatchDetails BD Inner Join InvItemStockDetails SD On SD.BatchID = BD.BatchID And SD.ItemID = BD.ItemID", "BD.ItemID = " + intItemID + " And SD.WarehouseID = " + Convert.ToInt32(cboWarehouse.SelectedValue) + "" });
            //BatchNo.ValueMember = "BatchID";
            //BatchNo.DisplayMember = "BatchNo";
            //BatchNo.DataSource = datCombos;
        }

        private void dgvItemDetailsGrid_Textbox_TextChanged(object sender, EventArgs e)
        {
            //Item Selection
            try
            {
                if (cboOperationType.SelectedValue != null && cboReferenceNo.SelectedValue != null && cboWarehouse.SelectedValue != null && cboOperationType.SelectedValue.ToInt32()!=(Int32)OperationOrderType.DNOTTransfer)
                {
                    //if (dgvItemDetailsGrid.CurrentCell.ColumnIndex == 0 || dgvItemDetailsGrid.CurrentCell.ColumnIndex == 1)
                    //{
                    //    for (int i = 0; i < dgvItemDetailsGrid.Columns.Count; i++)
                    //        dgvItemDetailsGrid.Rows[dgvItemDetailsGrid.CurrentCell.RowIndex].Cells[i].Value = null;
                    //}
                    dgvItemDetailsGrid.PServerName = ClsCommonSettings.ServerName;
                    string[] First = { "ItemCode", "ItemName", "ItemID", "ReferenceNo", "ReferenceSerialNo", "InvoicedQuantity", "DeliveredQty", "InvQty", "InvUOMID", "IsGroup", "BatchID", "BatchNo" };//first grid 
                    string[] second = { "ItemCode", "ItemName", "ItemID", "ReferenceNo", "ReferenceSerialNo", "InvoicedQuantity", "DeliveredQty", "InvQty", "InvUOMID", "IsGroup", "BatchID", "BatchNo" };//inner grid
                    dgvItemDetailsGrid.aryFirstGridParam = First;
                    dgvItemDetailsGrid.arySecondGridParam = second;
                    dgvItemDetailsGrid.PiFocusIndex = Quantity.Index;
                    dgvItemDetailsGrid.iGridWidth = 500;
                    dgvItemDetailsGrid.bBothScrollBar = true;
                    dgvItemDetailsGrid.pnlLeft = expandableSplitterLeft.Location.X + 5;
                    dgvItemDetailsGrid.pnlTop = expandableSplitterTop.Location.Y + PurchaseOrderBindingNavigator.Height + 10;
                    dgvItemDetailsGrid.ColumnsToHide = new string[] { "ItemID", "ReferenceNo", "ReferenceSerialNo", "InvQty", "InvUOMID", "IsGroup", "BatchID", "BatchNo" ,"LPONumber"};

                    dgvItemDetailsGrid.field = "ItemCode";
                    DataTable dtItemSelectionTable = MobjClsBLLItemIssueMaster.GetDataForItemSelection(Convert.ToInt32(cboOperationType.SelectedValue), Convert.ToInt32(cboReferenceNo.SelectedValue), Convert.ToInt32(cboWarehouse.SelectedValue), Convert.ToInt32(cboCompany.SelectedValue));
                    string strFilterCondition = "";
                    if (dgvItemDetailsGrid.CurrentCell.ColumnIndex == 0)
                    {
                        strFilterCondition = " ItemCode Like '" + ((dgvItemDetailsGrid.TextBoxText.ToUpper())) + "%'";
                        dtItemSelectionTable.DefaultView.RowFilter = strFilterCondition;
                    }
                    else if (dgvItemDetailsGrid.CurrentCell.ColumnIndex == 1)
                    {
                        strFilterCondition = " ItemName Like '" + ((dgvItemDetailsGrid.TextBoxText.ToUpper())) + "%'";
                        dtItemSelectionTable.DefaultView.RowFilter = strFilterCondition;
                    }

                  


                    DataTable datItemDetails = dtItemSelectionTable.DefaultView.ToTable();

                    //for (int i = 0; i < datItemDetails.Rows.Count; i++)
                    //{
                    //    for (int j = i + 1; j < datItemDetails.Rows.Count; j++)
                    //    {
                    //        if (datItemDetails.Rows[i]["ItemID"].ToInt32() == datItemDetails.Rows[j]["ItemID"].ToInt32() && datItemDetails.Rows[i]["BatchID"].ToInt32() == datItemDetails.Rows[j]["BatchID"].ToInt32())
                    //        {
                    //            int intUOMID = datItemDetails.Rows[j]["InvUOMID"].ToInt32();
                    //            decimal decInvoicedQty = datItemDetails.Rows[j]["InvQty"].ToDecimal();
                    //            decInvoicedQty = mobjClsBllCommonUtility.ConvertBaseUnitQtyToOtherQty(datItemDetails.Rows[i]["InvUOMID"].ToInt32(), datItemDetails.Rows[i]["ItemID"].ToInt32(), mobjClsBllCommonUtility.ConvertQtyToBaseUnitQty(datItemDetails.Rows[j]["InvUOMID"].ToInt32(), datItemDetails.Rows[j]["ItemID"].ToInt32(), datItemDetails.Rows[j]["InvQty"].ToDecimal(), 1), 1);
                    //            DataTable datUOm = mobjClsBllCommonUtility.FillCombos(new string[] { "Scale,Code as ShortName", "InvUOMReference", "UOMID = " + datItemDetails.Rows[i]["InvUOMID"].ToInt32() });

                    //            datItemDetails.Rows[i]["InvQty"] = datItemDetails.Rows[i]["InvQty"].ToDecimal() + decInvoicedQty;
                    //            datItemDetails.Rows[i]["InvoicedQuantity"] = datItemDetails.Rows[i]["InvQty"].ToDecimal().ToString("F" + datUOm.Rows[0]["Scale"].ToInt32()) + " " + datUOm.Rows[0]["ShortName"].ToString();
                    //            datItemDetails.Rows[j].Delete();
                    //            j--;
                    //            datItemDetails.AcceptChanges();
                    //        }
                    //    }
                    //}

                    //if(cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice && MobjClsBLLItemIssueMaster.GetJobOrderIDAgainstInvoice(cboReferenceNo.SelectedValue.ToInt64()) !=0)
                    //{
                    //    DataTable datJobOrderProductDetails  = mobjClsBllCommonUtility.FillCombos(new string[] {"ItemID,BatchID","PrdJobOrderProducts","JobOrderID = "+MobjClsBLLItemIssueMaster.GetJobOrderIDAgainstInvoice(cboReferenceNo.SelectedValue.ToInt64())+" And IsFromStore =0"});
                        
                    //    for (int i = 0; i < datItemDetails.Rows.Count; i++)
                    //    {
                    //        foreach (DataRow dr in datJobOrderProductDetails.Rows)
                    //        {
                    //            if (dr["ItemID"].ToInt64() == datItemDetails.Rows[i]["ItemID"].ToInt64())
                    //            {
                    //                datItemDetails.Rows[i].Delete();
                    //                i--;
                    //                datItemDetails.AcceptChanges();
                    //                break;
                    //            }
                    //        }
                    //    }
                    //}

                    dgvItemDetailsGrid.dtDataSource = datItemDetails;

                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvItemDetailsGrid_TextboxChangedEvent() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvItemDetailsGrid_TextboxChangedEvent() " + ex.Message, 2);
            }
        }

        private void cboReferenceNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (!blnIsEditMode)
            {
                bool blnValid = true;
                if (cboReferenceNo.SelectedValue != null)
                {
                    if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesInvoice)
                    {
                        //if (!MblnIsFromClear && MobjClsBLLItemIssueMaster.FillCombos(new string[] { "SalesInvoiceID", "InvSalesInvoiceMaster", "SalesInvoiceID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) + " And StatusID = " + (Int32)OperationStatusType.SInvoiceOpen }).Rows.Count == 0)
                        //{
                        //    // Please select opened sales invoice
                        //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4223, out MmsgMessageIcon);
                        //    MstrCommonMessage = MstrCommonMessage.Replace("*", "Sales Invoice");
                        //    blnValid = false;
                        //}

                        //if (MobjClsBLLItemIssueMaster.GetJobOrderIDAgainstInvoice(cboReferenceNo.SelectedValue.ToInt64()) == 0)
                        //{
                            dgvItemDetailsGrid.Visible = true;
                            dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
                            dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = true;
                            dgvItemDetailsGrid.Columns[DeliveredQty.Index].Visible = true;
                            btnCustomerwisePrint.Enabled = false;

                            //BatchNo.Visible = true;
                            ItemCode.ReadOnly = ItemName.ReadOnly    = Quantity.ReadOnly = false;
                            //BatchNo.ReadOnly//Uom.ReadOnly
                            Uom.ReadOnly = true;
                            dgvItemDetailsGrid.ReadOnly = false;
                            dgvItemDetailsGrid.AllowUserToAddRows = true;
                            dgvItemDetailsGrid.AllowUserToDeleteRows = true;
                            // cboWarehouse.Enabled = true;
                       // }
                       // else 
                       // {
                       //     dgvItemDetailsGrid.Visible = true;
                       //     dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
                       //     btnCustomerwisePrint.Enabled = false;
                       //     dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = false;
                       ////     BatchNo.Visible = false;
                       //     ItemCode.ReadOnly = ItemName.ReadOnly = BatchNo.ReadOnly = Uom.ReadOnly = Quantity.ReadOnly = true;
                       //     dgvItemDetailsGrid.AllowUserToAddRows = false;
                       //     dgvItemDetailsGrid.AllowUserToDeleteRows = false;
                       //     //  cboWarehouse.Enabled = false;
                       // }

                    }
                    else if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTTransfer)
                    {
                        if (!MblnIsFromClear && MobjClsBLLItemIssueMaster.FillCombos(new string[] { "StockTransferID", "InvStockTransferMaster", "StockTransferID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.STSubmitted+","+(int)OperationStatusType.STApproved+")" }).Rows.Count == 0)
                        {
                            // Please select opened sales invoice
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4241, out MmsgMessageIcon);
                            if (ClsCommonSettings.StockTransferApproval)
                                MstrCommonMessage = MstrCommonMessage.Replace("*", "approved");
                            else
                                MstrCommonMessage = MstrCommonMessage.Replace("*", "Submitted");
                            blnValid = false;
                        }
                         dgvItemDetailsGrid.Visible = true;
                        dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
                        dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = false;
                        dgvItemDetailsGrid.Columns[DeliveredQty.Index].Visible = false;
                        btnCustomerwisePrint.Enabled = false;
                        dgvItemDetailsGrid.ReadOnly = true;
                       // BatchNo.Visible = true;
                        ItemCode.ReadOnly = ItemName.ReadOnly   = Quantity.ReadOnly = false;
                        //BatchNo.ReadOnly//Uom.ReadOnly
                        dgvItemDetailsGrid.ReadOnly = true;
                        dgvItemDetailsGrid.AllowUserToAddRows = false;

                    }
                    else if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesOrder)
                    {
                        //if (!MblnIsFromClear && MobjClsBLLItemIssueMaster.FillCombos(new string[] { "SalesOrderID", "InvSalesOrderMaster", "SalesOrderID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) + " And StatusID In(" + (Int32)OperationStatusType.SOrderApproved +","+(Int32)OperationStatusType.SOrderClosed +"," +(Int32)OperationStatusType.SOrderSubmitted +")" }).Rows.Count == 0)
                        //{
                        //    // Please select sales order 
                        //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4251, out MmsgMessageIcon);
                        //    MstrCommonMessage = MstrCommonMessage.Replace("*", "Sales Order");
                        //    blnValid = false;
                        //}


                        dgvItemDetailsGrid.Visible = true;
                        dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
                        dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = true;
                        dgvItemDetailsGrid.Columns[DeliveredQty.Index].Visible = true;
                        btnCustomerwisePrint.Enabled = false;

                        //BatchNo.Visible = true;
                        ItemCode.ReadOnly = ItemName.ReadOnly   = Quantity.ReadOnly = false;
                        Uom.ReadOnly = true;
                        //= BatchNo.ReadOnly//= Uom.ReadOnly
                        dgvItemDetailsGrid.ReadOnly = false;
                        dgvItemDetailsGrid.AllowUserToAddRows = true;



                    }
                    if (!blnValid)
                    {
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        ErrItemIssue.SetError(cboReferenceNo, MstrCommonMessage.Replace("#", "").Trim());
                        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                        tmrItemIssue.Enabled = true;
                        cboReferenceNo.Focus();
                    }
                }
                if (!blnIsEditMode )
                {
                    dgvItemDetailsGrid.Rows.Clear();

                    DataTable dtItemDetails = MobjClsBLLItemIssueMaster.GetItemDetails(Convert.ToInt32(cboOperationType.SelectedValue), Convert.ToInt32(cboReferenceNo.SelectedValue));
                   
    
                    int intRowCount = 0;

                    foreach (DataRow dr in dtItemDetails.Rows)
                    {
                        if(cboOperationType.SelectedValue.ToInt32() != (int)OperationOrderType.DNOTTransfer)
                            txtLpoNo.Text = Convert.ToString(dr["LPONumber"]);
                        
                        dgvItemDetailsGrid.RowCount = dgvItemDetailsGrid.RowCount + 1;

                        if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTTransfer) //|| ((cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice) && (MobjClsBLLItemIssueMaster.GetJobOrderIDAgainstInvoice(cboReferenceNo.SelectedValue.ToInt64()) != 0)))
                            intRowCount = dgvItemDetailsGrid.Rows.Count - 1;
                        else if( (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesOrder)||(cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice))
                        {
                            intRowCount = dgvItemDetailsGrid.Rows.Count - 2;    
                        }

                        dgvItemDetailsGrid["ReferenceSerialNo", intRowCount].Value = dr["ReferenceSerialNo"];
                        dgvItemDetailsGrid["IsGroup", intRowCount].Value = dr["IsGroup"];
                        dgvItemDetailsGrid["ItemID", intRowCount].Value = dr["ItemID"];

                        if (cboOperationType.SelectedValue.ToInt32() != (int)OperationOrderType.DNOTTransfer)
                            dgvItemDetailsGrid["PackingUnit", intRowCount].Value = dr["PackingUnit"];

                        dgvItemDetailsGrid["ItemCode", intRowCount].Value = dr["Code"];
                        dgvItemDetailsGrid["ItemName", intRowCount].Value = dr["ItemName"];
                        dgvItemDetailsGrid["ReferenceNo", intRowCount].Value = dr["ReferenceNo"];

                        dgvItemDetailsGrid["InvUOMID", intRowCount].Value = dr["InvUOMID"];
                        FillUomColumn(Convert.ToInt32(dr["ItemID"]), Convert.ToBoolean(dr["IsGroup"]));
                        dgvItemDetailsGrid["Uom", intRowCount].Value = Convert.ToInt32(dr["InvUOMID"]);
                        dgvItemDetailsGrid["Uom", intRowCount].Tag = dgvItemDetailsGrid["Uom", intRowCount].Value;
                        dgvItemDetailsGrid["Uom", intRowCount].Value = dgvItemDetailsGrid["Uom", intRowCount].FormattedValue;

                        int intUomScale = 0;

                        if (dgvItemDetailsGrid[Uom.Index, intRowCount].Tag.ToInt32() != 0)
                            intUomScale = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "Scale,Code as ShortName", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid[Uom.Index, intRowCount].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                        dgvItemDetailsGrid["InvoicedQuantity", intRowCount].Value = dr["InvoicedQuantity"].ToDecimal().ToString("F" + intUomScale);
                        dgvItemDetailsGrid["InvQty", intRowCount].Value = dr["InvQty"].ToDecimal().ToString("F" + intUomScale);
                        dgvItemDetailsGrid["DeliveredQty", intRowCount].Value = dr["DeliveredQty"].ToDecimal().ToString("F" + intUomScale);                      

                        decimal decQty = dr["InvQty"].ToDecimal();                        
                        dgvItemDetailsGrid["BatchNo", intRowCount].Value = dr["BatchNo"];                       
                        dgvItemDetailsGrid["BatchID", intRowCount].Value = dr["BatchID"].ToInt64();                       
                        dgvItemDetailsGrid.Rows[intRowCount].Cells["QtyAvailable"].Value = GetAvailableQuantity(dgvItemDetailsGrid.Rows[intRowCount].Cells["ItemID"].Value.ToInt32(), dgvItemDetailsGrid.Rows[intRowCount].Cells["BatchID"].Value.ToInt64(), dgvItemDetailsGrid.CurrentRow.Cells["Uom"].Tag.ToInt32(), dgvItemDetailsGrid.Rows[intRowCount].Cells["IsGroup"].Value.ToBoolean()).ToString("F" + intUomScale);
                            
                        if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTTransfer)
                            dgvItemDetailsGrid["Quantity", intRowCount].Value = dr["InvQty"].ToDecimal();
                            
                        DataTable datUom = mobjClsBllCommonUtility.FillCombos(new string[] { "Code as ShortName,Scale", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid.Rows[intRowCount].Cells["InvUOMID"].Value.ToInt32() });
                        dgvItemDetailsGrid[InvoicedQuantity.Index, intRowCount].Value = dgvItemDetailsGrid[InvQty.Index, intRowCount].Value.ToDecimal();//.ToString("F" + datUom.Rows[0]["Scale"].ToInt32()) + " " + datUom.Rows[0]["ShortName"].ToString();
                        dgvItemDetailsGrid["IsAutomatic", intRowCount].Value = "true";
                    }
                }
            }
            tcDeliveryNote.SelectedTab = tiItemDetails;
        }

        private void dgvItemDetailsGrid_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 || e.ColumnIndex == 1)
            {
                dgvItemDetailsGrid.PiColumnIndex = e.ColumnIndex;
            }
        }

        private void dgvItemDetailsGrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                int iRowIndex = 0;

                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    dgvItemDetailsGrid.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    iRowIndex = dgvItemDetailsGrid.CurrentCell.RowIndex;

                    if (e.ColumnIndex != 0 && e.ColumnIndex != 1)
                    {
                        if (Convert.ToString(dgvItemDetailsGrid.CurrentRow.Cells[0].Value).Trim() == "")
                        {
                            e.Cancel = true;
                        }
                        else if (dgvItemDetailsGrid.Columns[e.ColumnIndex].Name == "Uom")
                        {
                            FillUomColumn(Convert.ToInt32(dgvItemDetailsGrid["ItemID", e.RowIndex].Value),  Convert.ToBoolean(dgvItemDetailsGrid["IsGroup", e.RowIndex].Value));
                        }
                        else if (dgvItemDetailsGrid.Columns[e.ColumnIndex].Name == "BatchNo")
                        {

                            //if (Convert.ToBoolean(dgvItemDetailsGrid["IsGroup", e.RowIndex].Value) == true)
                            //{
                            //    FrmItemGroupIssueDetails frmItemGroupIssueDetails = new FrmItemGroupIssueDetails(Convert.ToInt32(dgvItemDetailsGrid["ItemID", e.RowIndex].Value), MDtItemGroupIssueDetails, Convert.ToInt32(cboReferenceNo.SelectedValue), Convert.ToInt32(dgvItemDetailsGrid["ReferenceSerialNo", e.RowIndex].Value), Convert.ToInt32(cboWarehouse.SelectedValue), Convert.ToInt32(cboCompany.SelectedValue), blnIsEditMode);
                            //    frmItemGroupIssueDetails.ShowDialog();
                            //    MDtItemGroupIssueDetails = frmItemGroupIssueDetails.PDtItemGroupDetails;
                            //}

                            //else
                            //{
                               // FillBatchNoColumn(Convert.ToInt32(dgvItemDetailsGrid["ItemID", e.RowIndex].Value), false);

                           // }

                               
                                    //FillBatchNoColumn(Convert.ToInt32(dgvItemDetailsGrid["ItemID", e.RowIndex].Value), false);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvItemDetailsGrid_CellBeginEdit() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvItemDetailsGrid_CellBeginEdit() " + ex.Message, 2);
            }
        }

        private void FillUomColumn(int intItemID, bool blnIsGroup)
        {
            try
            {
                DataTable datCombos = null;
                    Uom.DataSource = null;
                    if (!blnIsGroup)
                        datCombos = MobjClsBLLItemIssueMaster.GetItemUoms(intItemID);
                    else
                        datCombos = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "UOMID,Code as ShortName", "InvUOMReference", "UOMID = " + (int)PredefinedUOMs.Numbers });
                    Uom.ValueMember = "UOMID";
                    Uom.DisplayMember = "ShortName";
                    Uom.DataSource = datCombos;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FillUomColumn() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in FillUomColumn() " + ex.Message, 2);
            }
        }

        private void dgvItemDetailsGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvItemDetailsGrid["ItemID", e.RowIndex].Value != null)
                {
                    if (dgvItemDetailsGrid.Columns[e.ColumnIndex].Name == "Uom")
                    {
                        if (dgvItemDetailsGrid.CurrentRow.Cells["Uom"].Value != null)
                        {
                            dgvItemDetailsGrid.CurrentRow.Cells["Uom"].Tag = dgvItemDetailsGrid.CurrentRow.Cells["Uom"].Value;
                            dgvItemDetailsGrid.CurrentRow.Cells["Uom"].Value = dgvItemDetailsGrid.CurrentRow.Cells["Uom"].FormattedValue;

                            int intUomScale = 0;
                            if (dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                                intUomScale = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                            dgvItemDetailsGrid.CurrentRow.Cells["Quantity"].Value = dgvItemDetailsGrid.CurrentRow.Cells["Quantity"].Value.ToDecimal().ToString("F" + intUomScale);

                            dgvItemDetailsGrid.CurrentRow.Cells["QtyAvailable"].Value = GetAvailableQuantity(dgvItemDetailsGrid.CurrentRow.Cells["ItemID"].Value.ToInt32(), dgvItemDetailsGrid.CurrentRow.Cells["BatchID"].Value.ToInt64(), dgvItemDetailsGrid.CurrentRow.Cells["Uom"].Tag.ToInt32(), dgvItemDetailsGrid.CurrentRow.Cells["IsGroup"].Value.ToBoolean()).ToString("F" + intUomScale);
                        }
                    }
                    //else if (dgvItemDetailsGrid.Columns[e.ColumnIndex].Name == "BatchNo")
                    //{
                    //    if (dgvItemDetailsGrid.CurrentRow.Cells["BatchNo"].Value != null)
                    //    {
                    //        //dgvItemDetailsGrid.CurrentRow.Cells["BatchNo"].Tag = dgvItemDetailsGrid.CurrentRow.Cells["BatchNo"].Value;
                    //        //dgvItemDetailsGrid.CurrentRow.Cells["BatchNo"].Value = dgvItemDetailsGrid.CurrentRow.Cells["BatchNo"].FormattedValue;


                    //        int intUomScale = 0;
                    //        if (dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    //            intUomScale = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();



                    //        dgvItemDetailsGrid.CurrentRow.Cells["QtyAvailable"].Value = GetAvailableQuantity(dgvItemDetailsGrid.CurrentRow.Cells["ItemID"].Value.ToInt32(), dgvItemDetailsGrid.CurrentRow.Cells["BatchID"].Value.ToInt64(), dgvItemDetailsGrid.CurrentRow.Cells["Uom"].Tag.ToInt32(), dgvItemDetailsGrid.CurrentRow.Cells["IsGroup"].Value.ToBoolean()).ToString("F" + intUomScale);
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvItemDetailsgrid_CellEndEdit() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvItemDetailsGrid_CellEndEdit() " + ex.Message, 2);
            }
        }

        private decimal GetAvailableQuantity(int intItemID, long lngBatchID, int intUOMID, bool IsGroup)
        {


            if (IsGroup == false && lngBatchID != 0)
            {
                decimal decStockQuantity = MobjClsBLLItemIssueMaster.GetStockQuantity(intItemID, lngBatchID, Convert.ToInt32(cboCompany.SelectedValue), Convert.ToInt32(cboWarehouse.SelectedValue));
                DataTable dtUomDetails = new DataTable();
                dtUomDetails = MobjClsBLLItemIssueMaster.GetUomConversionValues(intUOMID, intItemID);
                if (dtUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                        decStockQuantity = decStockQuantity * decConversionValue;
                    else if (intConversionFactor == 2)
                        decStockQuantity = decStockQuantity / decConversionValue;
                }
                return decStockQuantity;
            }
            else if (IsGroup == true && lngBatchID == 0)
            {
                decimal QtyAvailableForGrp = MobjClsBLLItemIssueMaster.GetStockQuantityForGroup(intItemID, Convert.ToInt32(cboWarehouse.SelectedValue));
                DataTable dtUomDetails = MobjClsBLLItemIssueMaster.GetUomConversionValues(intUOMID, intItemID);
                if (dtUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                        QtyAvailableForGrp = QtyAvailableForGrp * decConversionValue;
                    else if (intConversionFactor == 2)
                        QtyAvailableForGrp = QtyAvailableForGrp / decConversionValue;
                }
                return QtyAvailableForGrp;
            }
            else if (IsGroup == false && lngBatchID == 0)
            {
                decimal QtyAvailableForBatchlessItems = MobjClsBLLItemIssueMaster.GetStockQuantityForBatchlessItems(intItemID, Convert.ToInt32(cboWarehouse.SelectedValue));
                DataTable dtUomDetails = MobjClsBLLItemIssueMaster.GetUomConversionValues(intUOMID, intItemID);
                if (dtUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                        QtyAvailableForBatchlessItems = QtyAvailableForBatchlessItems * decConversionValue;
                    else if (intConversionFactor == 2)
                        QtyAvailableForBatchlessItems = QtyAvailableForBatchlessItems / decConversionValue;
                }
                return QtyAvailableForBatchlessItems;
            }
            else
            {
                return 0;
            }
        }

        private void dgvItemDetailsGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvItemDetailsGrid.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
            dgvItemDetailsGrid.CellLeave += new DataGridViewCellEventHandler(dgvItemDetailsGrid_CellLeave);
        }

        void dgvItemDetailsGrid_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvItemDetailsGrid.CurrentCell.OwningColumn.Name == "Quantity")
            {
                decimal decValue = 0;
                try
                {
                    decValue = Convert.ToDecimal(dgvItemDetailsGrid[e.ColumnIndex, e.RowIndex].Value);
                }
                catch
                {
                    dgvItemDetailsGrid[e.ColumnIndex, e.RowIndex].Value = 0;
                }
            }
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvItemDetailsGrid.CurrentCell.OwningColumn.Name == "Quantity")
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
                int intUomScale = 0;
                if (dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    intUomScale = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    if (intUomScale == 0)
                    {
                        if (e.KeyChar == '.')
                            e.Handled = true;
                    }
                    else
                    {
                        int dotIndex = -1;
                        if (txt.Text.Contains("."))
                        {
                            dotIndex = txt.Text.IndexOf('.');
                        }
                        if (e.KeyChar == '.')
                        {
                            if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            {
                                e.Handled = true;
                            }
                        }
                        else
                        {
                            if (char.IsDigit(e.KeyChar))
                            {
                                if (dotIndex != -1 && txt.SelectionStart > dotIndex)
                                {
                                    string[] splitText = txt.Text.Split('.');
                                    if (splitText.Length == 2)
                                    {
                                        if (splitText[1].Length - txt.SelectedText.Length >= intUomScale)
                                            e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
                //int intDecimalPart = 0;
                //if (dgvItemDetailsGrid.EditingControl != null && !string.IsNullOrEmpty(dgvItemDetailsGrid.EditingControl.Text) && dgvItemDetailsGrid.EditingControl.Text.Contains("."))
                //    intDecimalPart = dgvItemDetailsGrid.EditingControl.Text.Split('.')[1].Length;
                //int intUomScale = 0;
                //if (dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                //    intUomScale = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                
                //if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                //{
                //    e.Handled = true;
                //}
                //if (e.KeyChar.ToString() == "." && intUomScale == 0)
                //{
                //    e.Handled = true;
                //}

                //if (e.KeyChar != 13 && e.KeyChar != 08 && dgvItemDetailsGrid.EditingControl != null && !string.IsNullOrEmpty(dgvItemDetailsGrid.EditingControl.Text) && (dgvItemDetailsGrid.EditingControl.Text.Contains(".") && ((e.KeyChar == 46) || intDecimalPart == intUomScale)))//checking more than one "."
                //{
                //    e.Handled = true;
                //}
            }
        }

        private void dgvItemDetailsGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                
                    if (dgvItemDetailsGrid.Columns[e.ColumnIndex].Name == "ItemID")
                    {
                        Uom.DataSource = null;
                        //BatchNo.DataSource = null;

                        dgvItemDetailsGrid["Uom", e.RowIndex].Value = null;
                        dgvItemDetailsGrid["Uom", e.RowIndex].Tag = null;
                        //dgvItemDetailsGrid["BatchNo", e.RowIndex].Value = null;
                        //dgvItemDetailsGrid["BatchNo", e.RowIndex].Tag = null;
                    }
                 else if (dgvItemDetailsGrid.Columns[e.ColumnIndex].Name == "IsGroup")
                 {
                       if (cboOperationType.SelectedValue != null && cboReferenceNo.SelectedValue != null && cboWarehouse.SelectedValue != null && cboOperationType.SelectedValue.ToInt32()!=(Int32)OperationOrderType.DNOTTransfer)
                       {
                            DataTable dtgetdetails = MobjClsBLLItemIssueMaster.GetDataForItemSelection(Convert.ToInt32(cboOperationType.SelectedValue), Convert.ToInt32(cboReferenceNo.SelectedValue), Convert.ToInt32(cboWarehouse.SelectedValue), Convert.ToInt32(cboCompany.SelectedValue));
                            dtgetdetails.DefaultView.RowFilter = "ItemID = " + dgvItemDetailsGrid[ItemID.Index, e.RowIndex].Value.ToInt32();
                            
                           if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID != 0)
                            {
                            int intCostingMethod = 0;
                            DataTable datData = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + dgvItemDetailsGrid[ItemID.Index, e.RowIndex].Value.ToInt32() + "" });
                            if (datData.Rows.Count > 0)
                            {
                                intCostingMethod = datData.Rows[0]["CostingMethodID"].ToInt32();
                            }
                            
                                decTotalOldQuantityForMinItem = MobjClsBLLItemIssueMaster.GetDeliveredQtyNonBatchItems(Convert.ToInt32(dgvItemDetailsGrid["ItemID", e.RowIndex].Value), Convert.ToInt32(MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID));
                                if (intCostingMethod != 3)
                                {
                                    
                                    dgvItemDetailsGrid["TotalOldQuantityForMinItems",e.RowIndex].Value = decTotalOldQuantityForMinItem;

                                }
                                else
                                {
                                    
                                    dgvItemDetailsGrid["TotalOldQuantityForMinItems",e.RowIndex].Value = 0;

                                }  
                            }
                            if (dtgetdetails.DefaultView.ToTable().Rows.Count > 0)
                            {
                                DataRow dr = dtgetdetails.DefaultView.ToTable().Rows[0];
                                //FillBatchNoColumn(Convert.ToInt32(dgvItemDetailsGrid["ItemID", e.RowIndex].Value), false);
                                //if (dr["BatchID"].ToInt64() > 0)
                                //{
                                //    dgvItemDetailsGrid["BatchNo", e.RowIndex].ReadOnly = true;
                                //    dgvItemDetailsGrid["BatchNo", e.RowIndex].Value = dr["BatchID"].ToInt64();
                                //    dgvItemDetailsGrid["BatchNo", e.RowIndex].Tag = dr["BatchID"].ToInt64();
                                //    dgvItemDetailsGrid["BatchNo", e.RowIndex].Value = dgvItemDetailsGrid.Rows[e.RowIndex].Cells["BatchNo"].FormattedValue;
                                //}
                                //else
                                //{
                                //    dgvItemDetailsGrid["BatchNo", e.RowIndex].ReadOnly = false;

                                //}
                                
                                    FillUomColumn(Convert.ToInt32(dgvItemDetailsGrid["ItemID", e.RowIndex].Value), Convert.ToBoolean(dgvItemDetailsGrid["IsGroup", e.RowIndex].Value));
                                    dgvItemDetailsGrid["Uom", e.RowIndex].Value = Convert.ToInt32(dr["InvUOMID"]);
                                    dgvItemDetailsGrid["Uom", e.RowIndex].Tag = dr["InvUOMID"].ToInt64();
                                    dgvItemDetailsGrid["Uom", e.RowIndex].Value = dgvItemDetailsGrid.Rows[e.RowIndex].Cells["Uom"].FormattedValue;
                               


                                int intUomScale = 0;

                                if (dgvItemDetailsGrid["Uom", e.RowIndex].Tag.ToInt32() != 0)
                                    intUomScale = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "Scale,Code as ShortName", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid["Uom", e.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                                //if (dgvItemDetailsGrid["BatchNo", e.RowIndex].Tag.ToInt32() != 0)
                                dgvItemDetailsGrid.Rows[e.RowIndex].Cells["QtyAvailable"].Value = GetAvailableQuantity(dgvItemDetailsGrid.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt32(), dgvItemDetailsGrid.Rows[e.RowIndex].Cells["BatchID"].Value.ToInt64(), dgvItemDetailsGrid.Rows[e.RowIndex].Cells["Uom"].Tag.ToInt32(), dgvItemDetailsGrid.Rows[e.RowIndex].Cells["IsGroup"].Value.ToBoolean()).ToString("F" + intUomScale);
                                //else
                                //    dgvItemDetailsGrid.Rows[e.RowIndex].Cells["QtyAvailable"].Value = 0;
                           }
                       }
                 }
                        //FillBatchNoColumn(Convert.ToInt32(dr["ItemID"]), false);



                    
                    //else if (e.ColumnIndex == Quantity.Index)
                    //{
                    //            int intDecimalPart = 0;
                    //            if (dgvItemDetailsGrid.CurrentCell.Value != null && !string.IsNullOrEmpty(dgvItemDetailsGrid.CurrentCell.Value.ToString()) && dgvItemDetailsGrid.CurrentCell.Value.ToString().Contains("."))
                    //                intDecimalPart = dgvItemDetailsGrid.CurrentCell.Value.ToString().Split('.')[1].Length;
                    //            int intUomScale = 0;
                    //            if (dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    //                intUomScale = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid[Uom.Index, dgvItemDetailsGrid.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                    //            if (intDecimalPart > intUomScale)
                    //            {
                    //                dgvItemDetailsGrid.CurrentCell.Value = Math.Round(dgvItemDetailsGrid.CurrentCell.Value.ToDecimal(), intUomScale).ToString();
                    //                dgvItemDetailsGrid.EditingControl.Text = dgvItemDetailsGrid.CurrentCell.Value.ToString();
                    //            }
                    //}
                    ChangeStatus();
                
            }
        }
        private void FillParameters()
        {
            MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.strItemIssueNo = txtIssueNo.Text;
            MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intOperationTypeID = Convert.ToInt32(cboOperationType.SelectedValue);

           
            MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intReferenceID = Convert.ToInt32(cboReferenceNo.SelectedValue);

            if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intOperationTypeID == (Int32)OperationOrderType.DNOTSalesInvoice)
            {
                DataTable dtCustomerSI = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "VendorID,VendorAddID", "InvSalesInvoiceMaster", "SalesInvoiceID = " + MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intReferenceID + "" });
                MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intVendorID = dtCustomerSI.Rows[0]["VendorID"].ToInt32();
                MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intVendorAddID = dtCustomerSI.Rows[0]["VendorAddID"].ToInt32();
                
            }
            else if(MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intOperationTypeID ==(Int32)OperationOrderType.DNOTSalesOrder)
            {
                DataTable dtCustomerSO = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "VendorID,VendorAddID", "InvSalesOrderMaster", "SalesOrderID = " + MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intReferenceID+"" });
                MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intVendorID = dtCustomerSO.Rows[0]["VendorID"].ToInt32();
                MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intVendorAddID = dtCustomerSO.Rows[0]["VendorAddID"].ToInt32();
            }
            else if(MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intOperationTypeID ==(Int32)OperationOrderType.DNOTTransfer)
            {
                DataTable dtCustomerSTO = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "VendorID,VendorAddID", "InvStockTransferMaster", "StockTransferID = " + MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intReferenceID +""});
                MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intVendorID = dtCustomerSTO.Rows[0]["VendorID"].ToInt32();
                MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intVendorAddID = dtCustomerSTO.Rows[0]["VendorAddID"].ToInt32();
            }
            MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intWarehouseID = Convert.ToInt32(cboWarehouse.SelectedValue);
            MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.strIssuedDate = dtpIssuedDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.strRemarks = txtRemarks.Text;
            MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intIssuedBy = ClsCommonSettings.UserID;
            MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.Lpono = txtLpoNo.Text.Trim();

        }

        private void FillDetParameters()
        {
            try
            {
                dgvItemDetailsGrid.CommitEdit(DataGridViewDataErrorContexts.Commit);
                if (dgvItemDetailsGrid.CurrentCell != null)
                    dgvItemDetailsGrid.CurrentCell = dgvItemDetailsGrid["ItemCode", dgvItemDetailsGrid.CurrentRow.Index];
                MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.lstItemIssueDetails = new List<clsDTOItemIssueDetails>();
                decimal TotalQtyBatchless = 0;
      
                decimal AvailbleQtyinBatch = 0;

                DataTable datUsedQtyBatchWise = new DataTable();
                datUsedQtyBatchWise.Columns.Add("ItemID");
                datUsedQtyBatchWise.Columns.Add("BatchID");
                datUsedQtyBatchWise.Columns.Add("TakenQuantity", typeof(decimal));
                datUsedQtyBatchWise.Columns.Add("UOMID");
                //datUsedQtyBatchWise = null;
                    foreach (DataGridViewRow dr in dgvItemDetailsGrid.Rows)
                    {
                        clsDTOItemIssueDetails objDtoItemIssueDetails = null;
                        if (dr.Cells["ItemID"].Value != null && (Convert.ToInt32(dr.Cells["BatchID"].Value) != 0) && (Convert.ToBoolean(dr.Cells["IsGroup"].Value) != true))
                        {
                            objDtoItemIssueDetails = new clsDTOItemIssueDetails();
                            objDtoItemIssueDetails.intItemID = Convert.ToInt32(dr.Cells["ItemID"].Value);
                            objDtoItemIssueDetails.intBatchID = Convert.ToInt32(dr.Cells["BatchID"].Value);
                            objDtoItemIssueDetails.intReferenceSerialNo = Convert.ToInt32(dr.Cells["ReferenceSerialNo"].Value);
                            objDtoItemIssueDetails.decQuantity = Convert.ToDecimal(dr.Cells["Quantity"].Value);
                            objDtoItemIssueDetails.strPackingUnit = Convert.ToString(dr.Cells["PackingUnit"].Value);
                            objDtoItemIssueDetails.intUOMID = Convert.ToInt32(dr.Cells["Uom"].Tag);
                            objDtoItemIssueDetails.blnIsGroup = Convert.ToBoolean(dr.Cells["IsGroup"].Value);
                            objDtoItemIssueDetails.decQty = dr.Cells["DeliveredQty"].Value.ToDecimal();
                            objDtoItemIssueDetails.decOldQty = decOldQuantityDelivered;
                            objDtoItemIssueDetails.decOldDeliveredQty = decOldDeliveredQuantity;
                            MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.lstItemIssueDetails.Add(objDtoItemIssueDetails);
                            datUsedQtyBatchWise.Rows.Add();
                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["ItemID"] = Convert.ToInt32(dr.Cells["ItemID"].Value);
                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["BatchID"] = Convert.ToInt32(dr.Cells["BatchID"].Value);
                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["TakenQuantity"] = Convert.ToDecimal(dr.Cells["Quantity"].Value);
                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["UOMID"] = Convert.ToInt32(dr.Cells["Uom"].Tag);
                        }
                        else if (dr.Cells["ItemID"].Value != null && (Convert.ToInt32(dr.Cells["BatchID"].Value) == 0) && (Convert.ToBoolean(dr.Cells["IsGroup"].Value) != true))
                        {

                            decimal QtySelected = 0;
                            //if (!blnIsEditMode)
                            //{
                            BatchlessItems = MobjClsBLLItemIssueMaster.BatchLessItems(Convert.ToInt32(dr.Cells["ItemID"].Value), cboWarehouse.SelectedValue.ToInt32());

                            //}
                            //else
                            //{
                            //    BatchlessItems = MobjClsBLLDirectDelivery.BatchLessItemsEditMode(Convert.ToInt32(dr.Cells["ItemID"].Value), MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intDirectDeliveryID.ToInt32());
                            //}
                            TotalQtyBatchless = Convert.ToDecimal(dr.Cells["Quantity"].Value);

                            foreach (DataRow drBatchLess in BatchlessItems.Rows)
                            {
                                if (Convert.ToDecimal(dr.Cells["Quantity"].Value) == QtySelected)
                                {
                                    break;
                                }
                                decimal decTakenQuantity = 0;
                                decimal decAvailableQty = 0;
                                decAvailableQty = mobjClsBllCommonUtility.ConvertBaseUnitQtyToOtherQty(Convert.ToInt32(dr.Cells["Uom"].Tag), Convert.ToInt32(dr.Cells["ItemID"].Value), Convert.ToDecimal(drBatchLess["AvailableQty"]), 2);
                                //Total Qty FROM EACH  Item and Batch;decTakenQuantity
                                if (datUsedQtyBatchWise != null && datUsedQtyBatchWise.Rows.Count > 0)
                                {
                                    decTakenQuantity = datUsedQtyBatchWise.Compute("SUM(TakenQuantity)", "ItemID=" + Convert.ToInt32(dr.Cells["ItemID"].Value) + " AND BatchID=" + Convert.ToInt32(drBatchLess["BatchID"]) + "").ToDecimal();
                                }
                                     //if AvailableQty not equal to 0

                                if ((decAvailableQty - decTakenQuantity) > 0)
                                {
                                    //get remaining available qty
                                    decAvailableQty = (decAvailableQty - decTakenQuantity);
                                    objDtoItemIssueDetails = new clsDTOItemIssueDetails();
                                    //clsDTOBatchlessItemIssueDetails objBatchless = new clsDTOBatchlessItemIssueDetails();
                                    objDtoItemIssueDetails.intItemID = Convert.ToInt32(dr.Cells["ItemID"].Value);

                                    objDtoItemIssueDetails.intUOMID = Convert.ToInt32(dr.Cells["Uom"].Tag);
                                    objDtoItemIssueDetails.blnIsGroup = Convert.ToBoolean(dr.Cells["IsGroup"].Value);


                                    objDtoItemIssueDetails.intReferenceSerialNo = Convert.ToInt32(dr.Cells["ReferenceSerialNo"].Value);
                                    objDtoItemIssueDetails.intBatchID = Convert.ToInt32(drBatchLess["BatchID"]);
                                    //objBatchless.intBatchless = iBatchless;
                                    objDtoItemIssueDetails.strPackingUnit = Convert.ToString(dr.Cells["PackingUnit"].Value);
                                    AvailbleQtyinBatch = decAvailableQty;


                                    if (AvailbleQtyinBatch < TotalQtyBatchless)
                                    {

                                        QtySelected = QtySelected + AvailbleQtyinBatch;
                                        TotalQtyBatchless = TotalQtyBatchless - AvailbleQtyinBatch;
                                        objDtoItemIssueDetails.decQuantity = AvailbleQtyinBatch;
                                    }
                                    else
                                    {
                                        QtySelected = QtySelected + TotalQtyBatchless;

                                        objDtoItemIssueDetails.decQuantity = TotalQtyBatchless;
                                    }

                                    MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.lstItemIssueDetails.Add(objDtoItemIssueDetails);
                                    //Add selected itemid,batchid and quantity from each batch to the new datatable
                                    datUsedQtyBatchWise.Rows.Add();
                                    datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["ItemID"] = Convert.ToInt32(dr.Cells["ItemID"].Value);
                                    datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["BatchID"] = Convert.ToInt32(drBatchLess["BatchID"]);
                                    datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["TakenQuantity"] = objDtoItemIssueDetails.decQuantity;
                                    datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["UOMID"] = Convert.ToInt32(dr.Cells["Uom"].Tag);
                                }
                            }


                        }
                        else if (dr.Cells["ItemID"].Value != null && (Convert.ToInt32(dr.Cells["BatchID"].Value) == 0) && (Convert.ToBoolean(dr.Cells["IsGroup"].Value) == true))
                        {

                            objDtoItemIssueDetails = new clsDTOItemIssueDetails();
                            objDtoItemIssueDetails.intItemID = Convert.ToInt32(dr.Cells["ItemID"].Value);
                            objDtoItemIssueDetails.intBatchID = Convert.ToInt32(dr.Cells["BatchID"].Value);
                            //objDtoItemIssueDetails.intReferenceSerialNo = Convert.ToInt32(dr.Cells["ReferenceSerialNo"].Value);
                            objDtoItemIssueDetails.decQuantity = Convert.ToDecimal(dr.Cells["Quantity"].Value);
                            objDtoItemIssueDetails.intUOMID = Convert.ToInt32(dr.Cells["Uom"].Tag);
                            objDtoItemIssueDetails.strPackingUnit = Convert.ToString(dr.Cells["PackingUnit"].Value);
                            objDtoItemIssueDetails.blnIsGroup = Convert.ToBoolean(dr.Cells["IsGroup"].Value);

                            objDtoItemIssueDetails.intReferenceSerialNo = Convert.ToInt32(dr.Cells["ReferenceSerialNo"].Value);

                            // MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.lstDirectDeliveryDetails.Add(objDtoItemIssueDetails);


                            if (objDtoItemIssueDetails.blnIsGroup)
                            {
                                dtGroupDetailsI = MobjClsBLLItemIssueMaster.GetGroupDetails(objDtoItemIssueDetails.intItemID);
                                //MDtItemGroupIssueDetails.DefaultView.RowFilter = "ItemGroupID = " + objDtoItemIssueDetails.intItemID + " And ReferenceNo = " + MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.intReferenceID + " And ReferenceSerialNo = " + objDtoItemIssueDetails.intReferenceSerialNo;
                                objDtoItemIssueDetails.lstItemGroupIssueDetails = new List<clsDTOItemGroupIssueDetails>();
                                foreach (DataRow drRow in dtGroupDetailsI.Rows)
                                {
                                    clsDTOItemGroupIssueDetails objItemIssueDetails = new clsDTOItemGroupIssueDetails();
                                    objItemIssueDetails.intItemID = Convert.ToInt32(drRow["ItemID"]);
                                    BatchlessItems = MobjClsBLLItemIssueMaster.BatchLessItems(Convert.ToInt32(drRow["ItemID"]), cboWarehouse.SelectedValue.ToInt32());

                                    TotalQtyBatchless = (Convert.ToDecimal(drRow["Quantity"])) * (Convert.ToDecimal(dr.Cells["Quantity"].Value));
                                    decimal QtySelectedGrp = 0;
                                    foreach (DataRow drBatchLess in BatchlessItems.Rows)
                                    {
                                        if (((Convert.ToDecimal(drRow["Quantity"])) * (Convert.ToDecimal(dr.Cells["Quantity"].Value))) == QtySelectedGrp)
                                        {
                                            break;
                                        }
                                        decimal decTakenQuantityG = 0;
                                        if (datUsedQtyBatchWise!=null && datUsedQtyBatchWise.Rows.Count > 0)
                                        {
                                            //Total Qty FROM EACH  Item and Batch;decTakenQuantity
                                            decTakenQuantityG = datUsedQtyBatchWise.Compute("SUM(TakenQuantity)", "ItemID=" + Convert.ToInt32(drBatchLess["ItemID"]) + " AND BatchID=" + Convert.ToInt32(drBatchLess["BatchID"]) + "").ToDecimal();

                                        } 
                                        //decTakenQuantityG=datUsedQtyBatchWise.Compute("SUM(TakenQuantity)", "ItemID=" + Convert.ToInt32(dr.Cells["ItemID"].Value) + " AND BatchID=" + Convert.ToInt32(drBatchLess["BatchID"]) + "").ToDecimal();
                                        //if AvailableQty not equal to 0
                                        if ((Convert.ToDecimal(drBatchLess["AvailableQty"]) - decTakenQuantityG) > 0)
                                        {
                                            drBatchLess["AvailableQty"] = (Convert.ToDecimal(drBatchLess["AvailableQty"]) - decTakenQuantityG);
                                            objItemIssueDetails = new clsDTOItemGroupIssueDetails();
                                            //clsDTOBatchlessItemIssueDetails objBatchless = new clsDTOBatchlessItemIssueDetails();
                                            objItemIssueDetails.intItemID = Convert.ToInt32(drBatchLess["ItemID"]);

                                            objItemIssueDetails.intUOMID = Convert.ToInt32(drRow["UOMID"]);



                                            objItemIssueDetails.intBatchID = Convert.ToInt32(drBatchLess["BatchID"]);

                                            AvailbleQtyinBatch = Convert.ToDecimal(drBatchLess["AvailableQty"]);


                                            if (AvailbleQtyinBatch < TotalQtyBatchless)
                                            {

                                                QtySelectedGrp = QtySelectedGrp + AvailbleQtyinBatch;
                                                TotalQtyBatchless = TotalQtyBatchless - AvailbleQtyinBatch;
                                                objItemIssueDetails.decQuantity = AvailbleQtyinBatch;
                                            }
                                            else
                                            {
                                                QtySelectedGrp = QtySelectedGrp + TotalQtyBatchless;

                                                objItemIssueDetails.decQuantity = TotalQtyBatchless;
                                            }

                                            objDtoItemIssueDetails.lstItemGroupIssueDetails.Add(objItemIssueDetails);
                                            //Add selected itemid,batchid and quantity from each batch to the new datatable
                                            datUsedQtyBatchWise.Rows.Add();
                                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["ItemID"] = Convert.ToInt32(drBatchLess["ItemID"]);
                                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["BatchID"] = Convert.ToInt32(drBatchLess["BatchID"]);
                                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["TakenQuantity"] = objItemIssueDetails.decQuantity;
                                            datUsedQtyBatchWise.Rows[datUsedQtyBatchWise.Rows.Count - 1]["UOMID"] = Convert.ToInt32(drRow["UOMID"]);
                                        }
                                    }



                                    //MobjClsBLLDirectDelivery.PobjClsDTODeliveryMaster.lstDirectDeliveryDetails.Add(objDtoItemIssueDetails); 

                                }
                            }
                            MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.lstItemIssueDetails.Add(objDtoItemIssueDetails);
                        }
                    }
                
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FillDetParameters() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in FillDetParameters() " + ex.Message, 2);
            }
        }

        private void ClearControls()
        {
            cboCompany.Enabled = true;
            MblnIsFromClear = true;
            MDtItemGroupIssueDetails.Rows.Clear();
            //if (!blnIsEditMode)
            //{
            cboCompany.SelectedIndex = -1;
            cboWarehouse.SelectedIndex = -1;
            cboReferenceNo.SelectedIndex = -1;
       

            cboWarehouse.Text = cboReferenceNo.Text = cboOperationType.Text = "";
            //}
            dgvItemDetailsGrid.Rows.Clear();
            dtpIssuedDate.Value = ClsCommonSettings.GetServerDate();


            cboSCompany.SelectedIndex = -1;
           cboSOperationType.SelectedIndex = -1;
            cboSWarehouse.SelectedIndex = -1;
            cboSIssuedBy.SelectedIndex = -1;
            cboSCompany.Text = cboSOperationType.Text = cboSWarehouse.Text = cboSIssuedBy.Text = "";
            txtLpoNo.Text = "";

       
                cboOperationType.SelectedValue = (int)OperationOrderType.DNOTSalesInvoice;
               // cboSOperationType.SelectedValue = (int)OperationOrderType.DNOTSalesInvoice;
           


            dgvItemDetailsGrid.ReadOnly = false;
            dtpIssuedDate.Enabled = true;
            txtRemarks.Enabled = true;
            //cboCompany.Text = "";

            dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;

            txtRemarks.Clear();
            cboCompany.Focus();

            dgvItemDetailsGrid.ReadOnly = false;
            dgvItemDetailsGrid.Columns["InvoicedQuantity"].ReadOnly = true;
            dgvItemDetailsGrid.Columns["QtyAvailable"].ReadOnly = true;

            MblnIsFromClear = false;
            bnDelete.Enabled = false;
            bnSaveItem.Enabled = false;
            bnEmail.Enabled = false;
            bnAddNewItem.Enabled = false;
            bnPrint.Enabled = false;
            btnPicklist.Enabled = false;
            btnPickListEmail.Enabled = false;
            MchangeStatus = false;
            bnClear.Enabled = false;
            dgvItemDetailsGrid.ReadOnly = false;
            cboOperationType.Enabled = true;
            cboReferenceNo.Enabled = true;
            cboWarehouse.Enabled = true;
            blnIsEditMode = false;
            btnCustomerwisePrint.Enabled = false;
           
        }

        private void ChangeStatus()
        {
            if (!blnIsEditMode)
                bnSaveItem.Enabled = MblnAddPermission;
            else
            {
                if(MblnIsEditable)
                bnSaveItem.Enabled = MblnUpdatePermission;
                bnAddNewItem.Enabled = MblnAddPermission;
            }
            ErrItemIssue.Clear();
            lblStatus.Text = "";
            MchangeStatus = true;
            bnClear.Enabled = true;
        }

        private void AddNewItemIssue()
        {
            try
            {
                dtItemDetails = new DataTable();
                
                dtItemDetails.Columns.Add("ItemCode");
                dtItemDetails.Columns.Add("ItemName");
                dtItemDetails.Columns.Add("ItemID");
                dtItemDetails.Columns.Add("BatchID");
                dtItemDetails.Columns.Add("BatchNo");
                dtItemDetails.Columns.Add("UOMID");
                ClearControls();
                dgvItemDetailsGrid.Visible = true;
                blnIsEditMode = false;
                MblnIsEditable = true;
                MobjClsBLLItemIssueMaster = new clsBLLItemIssueMaster();
                //MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intCompanyID = ClsCommonSettings.CompanyID;
                MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intIssuedBy = ClsCommonSettings.UserID;
                lblIssuedBy.Text = ClsCommonSettings.strEmployeeName;
                //  txtIssueNo.Text = ClsCommonSettings.ItemIssuePrefix + MobjClsBLLItemIssueMaster.GetNextIssueNo().ToString();
                GenerateItemIssueNo();

                //cboSIssuedBy.SelectedValue = ClsCommonSettings.intEmployeeID;
                //cboSCompany.SelectedValue = ClsCommonSettings.CompanyID;
                //cboSOperationType.SelectedIndex = -1;
                //cboSWarehouse.SelectedIndex = -1;

                panelMiddle.Visible = true;
                expandableSplitterTop.Visible = true;

                DisplayAllIssueNos();
                bnDelete.Enabled = false;
                dgvItemDetailsGrid.ReadOnly = false;
                cboOperationType.Enabled = true;
                cboReferenceNo.Enabled = true;
                cboWarehouse.Enabled = true;
                bnSaveItem.Enabled = false;
                bnEmail.Enabled = false;
                bnAddNewItem.Enabled = false;
                bnPrint.Enabled = false;
                btnPicklist.Enabled = false;
                MchangeStatus = false;
                bnClear.Enabled = false;

                //if (PintFormType == 1)
                //    cboOperationType.SelectedValue = (Int32)OperationOrderType.DNOTSalesInvoice;
                //else
                //    cboOperationType.SelectedValue = (Int32)OperationOrderType.DNOTDMR;
                if (PintFormType == 1)
                    lblStatus.Text = "Add New Delivery Note";
                else
                    lblStatus.Text = "Add new material issue";
                ErrItemIssue.Clear();
                cboCompany.Focus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in AddNewItemIssue() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in AddNewItemIssue() " + ex.Message, 2);
            }
        }

        private void FrmItemIssue_Shown(object sender, EventArgs e)
        {
            cboCompany.Focus();
        }

        private void bnSaveItem_Click(object sender, EventArgs e)
        {
      //      tcDeliveryNote.SelectedTab = tiLocationDetails;
          //  tcDeliveryNote.SelectedTab = tiItemDetails;
            try
            {
                // Saving Information
               
               
                if (  FillItemLocationDetails() && ValidateFields())
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID == 0 ? 1 : 3), out MmsgMessageIcon);

                    if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                        MessageBoxIcon.Information) == DialogResult.No)
                        return;

                    if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID == 0 && txtIssueNo.ReadOnly && MobjClsBLLItemIssueMaster.FillCombos(new string[] { "ItemIssueNo", "InvItemIssueMaster", "ItemIssueNo = '" + txtIssueNo.Text + "' And ItemIssueID <> " + MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID + " And CompanyID = "+cboCompany.SelectedValue.ToInt32() }).Rows.Count > 0)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 9002, out MmessageIcon);
                        MstrCommonMessage = MstrCommonMessage.Replace("*", "Item Issue");
                        if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            return ;
                        else
                            GenerateItemIssueNo();
                    }

                    //if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice)
                    //{
                    //    if (MobjClsBLLItemIssueMaster.GetBalanceAmount(cboReferenceNo.SelectedValue.ToInt64()) > 0)
                    //    {
                    //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4250, out MmsgMessageIcon);
                    //        MstrCommonMessage = MstrCommonMessage.Replace("#", "");
                    //        if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    //        {
                    //            return;
                    //        }                           
                    //    }
                    //}
                    if (blnIsEditMode)
                    {
                        MobjClsBLLItemIssueMaster.DeleteItemGroupIssueDetails();
                        MobjClsBLLItemIssueMaster.DeleteItemIssueDetails();
                      
                    }
                    FillParameters();
                    FillDetParameters();

                    try
                    {
                        if (MobjClsBLLItemIssueMaster.SaveItemIssue())
                        {
                            //Delivery note alert
                            if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice)
                            {
                                objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.DeliveryNote;
                                objFrmTradingMain.tmrAlert.Enabled = true;
                            }

                            AddNewItemIssue();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        MobjClsLogWriter.WriteLog("Error on save function()" + this.Name + " " + ex.Message, 2);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnSaveItem_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in bnSaveItem_Click() " + ex.Message, 2);
            }
        }

        private bool ValidateFields()
        {
            try
            {
                // Validating datas
                MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                if (dgvItemDetailsGrid.CurrentCell != null)
                    dgvItemDetailsGrid.CurrentCell = dgvItemDetailsGrid["ItemCode", dgvItemDetailsGrid.CurrentRow.Index];
                bool blnValid = true;
                Control cntrlFocus = null;
                ErrItemIssue.Clear();

                if (!txtIssueNo.ReadOnly && MobjClsBLLItemIssueMaster.FillCombos(new string[] { "ItemIssueNo", "InvItemIssueMaster", "ItemIssueNo = '" + txtIssueNo.Text + "' And ItemIssueID <> "+MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID+" And CompanyID = "+cboCompany.SelectedValue.ToInt32() }).Rows.Count > 0)
                {
                    //This ItemIssue No Already Exists
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4224, out MmsgMessageIcon);
                    cntrlFocus = txtIssueNo;
                    blnValid = false;
                }
                else if (cboOperationType.SelectedValue == null)
                {
                    //Please select Operation Type
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4201, out MmsgMessageIcon);
                    cntrlFocus = cboOperationType;
                    blnValid = false;
                }
               

                else if (cboReferenceNo.SelectedValue == null)
                {
                    //Please select Reference No
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4202, out MmsgMessageIcon);
                    cntrlFocus = cboReferenceNo;
                    blnValid = false;
                }
                else if (cboWarehouse.SelectedValue == null)
                {
                    //Please select Warehouse
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4203, out MmsgMessageIcon);
                    cntrlFocus = cboWarehouse;
                    blnValid = false;
                }
                else if (cboCompany.SelectedValue == null)
                {
                    //Please select customer
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4263, out MmsgMessageIcon);
                    cntrlFocus = cboCompany;
                    blnValid = false;
                }
                else if (txtIssueNo.Text == "")
                {
                    //Please select customer
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4265, out MmsgMessageIcon);
                    cntrlFocus = txtIssueNo;
                    blnValid = false;
                }   
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice && mobjClsBllCommonUtility.FillCombos(new string[] { "InvoiceDate", "InvSalesInvoiceMaster", "SalesInvoiceID = " + cboReferenceNo.SelectedValue.ToInt64() }).Rows[0]["InvoiceDate"].ToDateTime() > dtpIssuedDate.Value)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4245, out MmsgMessageIcon).Replace("*", "Invoice");
                    cntrlFocus = dtpIssuedDate;
                    blnValid = false;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTTransfer && mobjClsBllCommonUtility.FillCombos(new string[] { "OrderDate", "InvStockTransferMaster", "StockTransferID = " + cboReferenceNo.SelectedValue.ToInt64() }).Rows[0]["OrderDate"].ToDateTime() > dtpIssuedDate.Value)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4245, out MmsgMessageIcon).Replace("*", "Transfer Order");
                    cntrlFocus = dtpIssuedDate;
                    blnValid = false;
                }
                else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesOrder && mobjClsBllCommonUtility.FillCombos(new string[] { "OrderDate", "InvSalesOrderMaster", "SalesOrderID = " + cboReferenceNo.SelectedValue.ToInt64() }).Rows[0]["OrderDate"].ToDateTime() > dtpIssuedDate.Value)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4245, out MmsgMessageIcon).Replace("*", "Order");
                    cntrlFocus = dtpIssuedDate;
                    blnValid = false;
                }
                else if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID!= 0 && Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesInvoice)
                {
                    //if (MobjClsBLLItemIssueMaster.FillCombos(new string[] { "SalesInvoiceID", "InvSalesInvoiceMaster", "SalesInvoiceID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) + " And StatusID = " + (Int32)OperationStatusType.SInvoiceOpen }).Rows.Count == 0)
                    //{
                    //    // Please select opened sales invoice
                    //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4223, out MmsgMessageIcon);
                    //    MstrCommonMessage = MstrCommonMessage.Replace("*", "Sales Invoice");
                    //    cntrlFocus = cboReferenceNo;
                    //    blnValid = false;
                    //}
                    if (MobjClsBLLItemIssueMaster.FillCombos(new string[] { "SalesReturnID", "InvSalesReturnMaster", "SalesInvoiceID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) }).Rows.Count > 0)
                    {
                        // Sales return have been done
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4225, out MmsgMessageIcon);
                        MstrCommonMessage = MstrCommonMessage.Replace("*", " this Sales Invoice");
                        cntrlFocus = cboReferenceNo;
                        blnValid = false;
                    }
                }
                else if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID == 0 && cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTTransfer)
                {
                    if (MobjClsBLLItemIssueMaster.FillCombos(new string[] { "StockTransferID", "InvStockTransferMaster", "StockTransferID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.STSubmitted + "," + (int)OperationStatusType.STApproved + ")" }).Rows.Count == 0)
                    {
                        // Please select approved stock transfer
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4241, out MmsgMessageIcon);
                        if (ClsCommonSettings.StockTransferApproval)
                            MstrCommonMessage = MstrCommonMessage.Replace("*", "approved");
                        else
                            MstrCommonMessage = MstrCommonMessage.Replace("*", "Submitted");
                        cntrlFocus = cboReferenceNo;
                        blnValid = false;
                    }
                }
                
                else if (dgvItemDetailsGrid.Rows.Count == 1)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4204, out MmsgMessageIcon);
                    cntrlFocus = dgvItemDetailsGrid;
                    blnValid = false;

                }
                //else if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID == 0 && cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTDMR)
                //{
                //    if (MobjClsBLLItemIssueMaster.FillCombos(new string[] { "DMRID", "PrdDMRMaster", "DMRID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) + " And StatusID In (" + (Int32)OperationStatusType.DMROpened + ")" }).Rows.Count == 0)
                //    {
                //        // Please select opened DMR No
                //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4246, out MmsgMessageIcon);

                //        MstrCommonMessage = MstrCommonMessage.Replace("*", "Opened");
                //        blnValid = false;
                //    }
                //}


                else if (blnValid)
                {
                    int iTempID = 0;
                    iTempID = CheckDuplicationInGrid();
                    if (iTempID != -1)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4243, out MmessageIcon);
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        tmrItemIssue.Enabled = true;
                        dgvItemDetailsGrid.CurrentCell = dgvItemDetailsGrid["ItemCode", iTempID];
                        dgvItemDetailsGrid.Focus();
                        return false;
                    }
                }

                if (!blnValid)
                {
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    ErrItemIssue.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                    lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                    tmrItemIssue.Enabled = true;
                    tcDeliveryNote.SelectedTab = tiItemDetails;
                    cntrlFocus.Focus();
                }
                else if (!ValidateItemIssueGrid())
                {
                    blnValid = false;
                }
                return blnValid;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in ValidateFields() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in ValidateFields() " + ex.Message, 2);
                return false;
            }
        }

        private decimal ConvertValue(decimal decValue, int intUomID, int intItemID)
        {
            DataTable dtUomDetails = MobjClsBLLItemIssueMaster.GetUomConversionValues(intUomID, intItemID);
            if (dtUomDetails.Rows.Count > 0)
            {
                int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                if (intConversionFactor == 1)
                    decValue = decValue / decConversionValue;
                else if (intConversionFactor == 2)
                    decValue = decValue * decConversionValue;
            }
            return decValue;
        }

        private bool ValidateItemIssueGrid()
        {
            bool blnIsValid = true;
            int intItemCount = 0, intRowIndex = 0; 
           
            string strColumnName = "";
            DataTable dtOldItemIssueDetails = new DataTable();
            DataTable datItemDetails = MobjClsBLLItemIssueMaster.GetItemDetails(cboOperationType.SelectedValue.ToInt32(),cboReferenceNo.SelectedValue.ToInt32());

            if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID != 0)
                dtOldItemIssueDetails = MobjClsBLLItemIssueMaster.GetItemIssueDetails();
            List<int> lstSelectedItems = new List<int>();
            List<int> lstCheckedSerialNos = new List<int>();
            for (int i = 0; i < dgvItemDetailsGrid.Rows.Count-1; i++)
            {
                if (dgvItemDetailsGrid["ItemID", i].Value == null)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4204, out MmsgMessageIcon);
                    intRowIndex = 0;
                    strColumnName = "ItemCode";
                    blnIsValid = false;
                }
            }
            for (int i = 0; i < dgvItemDetailsGrid.Rows.Count; i++)
            {
                
                if (dgvItemDetailsGrid["ItemID", i].Value != null)
                {
                    if (!lstSelectedItems.Contains(dgvItemDetailsGrid["ItemID", i].Value.ToInt32()))
                        lstSelectedItems.Add(dgvItemDetailsGrid["ItemID", i].Value.ToInt32());
                    if (dgvItemDetailsGrid["Quantity", i].Value == null || Convert.ToDecimal(dgvItemDetailsGrid["Quantity", i].Value) == 0)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4215, out MmsgMessageIcon);
                        intRowIndex = i;
                        strColumnName = "Quantity";
                        blnIsValid = false;
                    }
                    else if (dgvItemDetailsGrid["Uom", i].Tag == null)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4209, out MmsgMessageIcon);
                        intRowIndex = i;
                        strColumnName = "Uom";
                        blnIsValid = false;
                    }
                   



                    //else if (dgvItemDetailsGrid["BatchNo", i].Tag.ToInt64() == 0 )//&& (Convert.ToBoolean(dgvItemDetailsGrid["IsGroup", i].Value) == false))//&& dgvItemDetailsGrid["IsFromStore",i].Value.ToBoolean() ==true)//&& ((cboOperationType.SelectedValue.ToInt32 () != (int)OperationOrderType.DNOTSalesInvoice) || ((cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice && MobjClsBLLItemIssueMaster.GetJobOrderIDAgainstInvoice(cboReferenceNo.SelectedValue.ToInt64()) == 0))))
                    //{

                    //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4220, out MmessageIcon);
                    //    intRowIndex = i;
                    //    strColumnName = "BatchNo";
                    //    blnIsValid = false;
                    //}
                    
                    
                    if (!blnIsEditMode)
                    {
                          if (dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal() > dgvItemDetailsGrid.Rows[i].Cells[QtyAvailable.Index].Value.ToDecimal())
                            {
                                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4252, out MmsgMessageIcon);
                                intRowIndex = i;
                                strColumnName = "Quantity";
                                blnIsValid = false;
                            }
                         if (Convert.ToBoolean(dgvItemDetailsGrid["IsGroup", i].Value) == true)
                          {
                              DataTable datIsGroup = null;
                              datIsGroup = MobjClsBLLItemIssueMaster.GetQtyInGroup(dgvItemDetailsGrid["ItemID", i].Value.ToInt32());

                              for (int intICounter2 = 0; intICounter2 < datIsGroup.Rows.Count; intICounter2++)
                              {
                                  int intItemID = Convert.ToInt32(datIsGroup.Rows[intICounter2]["ItemID"]);
                                  decimal Qty = datIsGroup.Rows[intICounter2]["Quantity"].ToDecimal();
                                  decimal TotalQty = Qty * dgvItemDetailsGrid["Quantity", i].Value.ToInt32();

                                  for (int iCounter3 = 0; iCounter3 < dgvItemDetailsGrid.Rows.Count; iCounter3++)
                                  {
                                      if ((intItemID == dgvItemDetailsGrid.Rows[iCounter3].Cells[ItemID.Index].Value.ToInt32()) &&  (dgvItemDetailsGrid.Rows[iCounter3].Cells[IsGroup.Index].Value.ToBoolean() != true))
                                      {
                                          if (TotalQty + dgvItemDetailsGrid.Rows[iCounter3].Cells[Quantity.Index].Value.ToInt32() > MobjClsBLLItemIssueMaster.GetStockQuantity(dgvItemDetailsGrid.Rows[iCounter3].Cells[ItemID.Index].Value.ToInt32(), 0, cboCompany.SelectedValue.ToInt32(), cboWarehouse.SelectedValue.ToInt32()))
                                          {
                                              MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4264, out MmsgMessageIcon).Replace("*", dgvItemDetailsGrid.Rows[iCounter3].Cells[ItemName.Index].Value.ToString());
                                              //MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4215, out MmsgMessageIcon);
                                              intRowIndex = i;
                                              strColumnName = "ItemCode";
                                              blnIsValid = false;
                                          }
                                      }
                                  }

                              }
                          }

                            
                                decimal decQty = 0;
                                int intCostingMethod = 0;
                                DataTable datData = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + dgvItemDetailsGrid.Rows[i].Cells["ItemID"].Value.ToInt32() + "" });
                                if (datData.Rows.Count > 0)
                                {
                                    intCostingMethod = datData.Rows[0]["CostingMethodID"].ToInt32();
                                }
                                if (intCostingMethod != 3)
                                {
                                    for (int j = 0; j < dgvItemDetailsGrid.Rows.Count - 1; j++)
                                    {
                                        if (dgvItemDetailsGrid.Rows[j].Cells["ItemID"].Value.ToInt32() == dgvItemDetailsGrid.Rows[i].Cells["ItemID"].Value.ToInt32())
                                        {
                                            decQty = decQty + dgvItemDetailsGrid.Rows[j].Cells[Quantity.Index].Value.ToDecimal();
                                        }

                                    }
                                }
                                else
                                {
                                    decQty = dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal();
                                }
                                dgvItemDetailsGrid.Rows[i].Cells["decCurrentTotalDeliveredQty"].Value = decQty;
                               if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice)
                              {
                                  if ((dgvItemDetailsGrid.Rows[i].Cells[InvQty.Index].Value.ToDecimal() - dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal()) < dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal())
                                        {
                                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4253, out MmsgMessageIcon);
                                            intRowIndex = i;
                                            strColumnName = "Quantity";
                                            blnIsValid = false;
                                        }
                               }
                             else  if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesOrder)
                             {
                                 if ((dgvItemDetailsGrid.Rows[i].Cells[InvQty.Index].Value.ToDecimal() - dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal()) < dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal())
                                    {
                                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4254, out MmsgMessageIcon);
                                        intRowIndex = i;
                                        strColumnName = "Quantity";
                                        blnIsValid = false;
                                    }
                              }




                            //if ((dgvItemDetailsGrid.Rows[i].Cells[InvQty.Index].Value.ToDecimal() - dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal()) < dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal())
                            //{
                            //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4253, out MmsgMessageIcon);
                            //    intRowIndex = i;
                            //    strColumnName = "Quantity";
                            //    blnIsValid = false;
                            //}
                        

                        //if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesOrder)
                        //{
                        //    if ((dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal() + dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal()) > dgvItemDetailsGrid.Rows[i].Cells[InvQty.Index].Value.ToDecimal())
                        //    {
                        //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4254, out MmsgMessageIcon);
                        //        intRowIndex = i;
                        //        strColumnName = "Quantity";
                        //        blnIsValid = false;
                        //    }
                            
                        //}
                       
                    }
                    else
                    {   //Old quantity
                       
                        //decimal decOldDeliveredQuantity = 0;
                        //decimal decOldQuantityDelivered = 0;
                        decimal decDeliverdQtyItemWise = 0;
                        decimal decTotalDeliveredQtyItemWise = 0;
                        decimal decOldQuantityDeliveredItemWise = 0;
                        decimal decOldDeliveredQty = 0;
                       
                        if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID != 0)
                        {
                            //dtOldItemIssueDetails.DefaultView.RowFilter = "ItemID = " + Convert.ToInt32(dgvItemDetailsGrid["ItemID", i].Value) + " ";
                            //And BatchID = " + Convert.ToInt32(dgvItemDetailsGrid["BatchID", i].Value) + "

                            
                        
                            if (i < dtOldItemIssueDetails.Rows.Count)
                            {
                                DataRow dr = dtOldItemIssueDetails.DefaultView.ToTable().Rows[i];
                                decOldQuantityDelivered += Convert.ToDecimal(dr["Quantity"]);
                                decOldDeliveredQuantity += Convert.ToDecimal(dr["DeliveredQty"]);

                                decDeliverdQtyItemWise = Convert.ToDecimal(dr["DeliveredQty"]);
                                decOldQuantityDeliveredItemWise = Convert.ToDecimal(dr["Quantity"]);
                                //decTotalDeliveredQtyItemWise 
                                decTotalDeliveredQtyItemWise = MobjClsBLLItemIssueMaster.GetDeliveredQty(Convert.ToInt32(dr["ItemID"]), Convert.ToInt32(dr["ReferenceID"]), Convert.ToInt32(dr["OrderTypeID"]));
                                


                                //}
                            }
                            else
                            {
                                //decDeliverdQtyItemWise = dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal();
                                decOldQuantityDeliveredItemWise = 0;
                            }

                                //}
                                decimal decNewDeliveredQuantity = 0;
                                decNewDeliveredQuantity = decOldDeliveredQuantity - decOldQuantityDelivered;

                                decOldDeliveredQty = decDeliverdQtyItemWise - decOldQuantityDeliveredItemWise;

                                decimal decQty = 0;
                                int intCostingMethod = 0;
                                DataTable datData = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + dgvItemDetailsGrid.Rows[i].Cells["ItemID"].Value.ToInt32() + "" });
                                if (datData.Rows.Count > 0)
                                {
                                    intCostingMethod = datData.Rows[0]["CostingMethodID"].ToInt32();
                                }
                                if (intCostingMethod != 3 && dgvItemDetailsGrid.Rows[i].Cells[IsGroup.Index].Value.ToBoolean()==false)
                                {
                                    for (int j = 0; j < dgvItemDetailsGrid.Rows.Count - 1; j++)
                                    {
                                        if (dgvItemDetailsGrid.Rows[j].Cells["ItemID"].Value.ToInt32() == dgvItemDetailsGrid.Rows[i].Cells["ItemID"].Value.ToInt32())
                                        {
                                            decQty = decQty + dgvItemDetailsGrid.Rows[j].Cells[Quantity.Index].Value.ToDecimal();
                                        }

                                    }
                                }
                                //else
                                //{
                                //    decQty = dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal();
                                //}
                                dgvItemDetailsGrid.Rows[i].Cells["decCurrentTotalDeliveredQty"].Value = decQty;
                                    if (dgvItemDetailsGrid.Rows[i].Cells[QtyAvailable.Index].Value.ToDecimal() == 0)
                                    {
                                        //old validation
                                        //if (dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal() > decOldQuantityDelivered)

                                        if (dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal() > decDeliverdQtyItemWise)
                                        {
                                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4252, out MmsgMessageIcon);
                                            intRowIndex = i;
                                            strColumnName = "Quantity";
                                            blnIsValid = false;
                                        }
                                    }
                               
                                     if (dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal() > (decOldQuantityDeliveredItemWise + dgvItemDetailsGrid.Rows[i].Cells[QtyAvailable.Index].Value.ToDecimal()))

                                    
                                    {
                                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4252, out MmsgMessageIcon);
                                        intRowIndex = i;
                                        strColumnName = "Quantity";
                                        blnIsValid = false;
                                    }
                               
                                if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice)
                                {

                                    if (decDeliverdQtyItemWise != dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    {
                                        if (((dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal() - decOldQuantityDeliveredItemWise) + dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()) > dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                        {
                                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4253, out MmsgMessageIcon);
                                            intRowIndex = i;
                                            strColumnName = "Quantity";
                                            blnIsValid = false;
                                        }
                                    }



                                    if (decDeliverdQtyItemWise == dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    {

                                        if ((dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()) > decOldQuantityDeliveredItemWise)
                                        {
                                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4260, out MmsgMessageIcon);
                                            intRowIndex = i;
                                            strColumnName = "Quantity";
                                            blnIsValid = false;
                                        }
                                    }




                                    //if ((intCostingMethod != 3) && (dgvItemDetailsGrid.Rows[i].Cells[IsGroup.Index].Value.ToBoolean()==false))
                                    //{
                                    //    if ((dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal()) != dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    //    {
                                    //        //if (((dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal() - (dgvItemDetailsGrid.Rows[i].Cells[TotalOldQuantityForMinItems.Index].Value.ToDecimal() - decOldQuantityDeliveredItemWise)) - dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()) < 0 )
                                    //        //if (((dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal() - decOldQuantityDeliveredItemWise) + dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()) > dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    //        if (dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal() < ((dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal() - dgvItemDetailsGrid.Rows[i].Cells[TotalOldQuantityForMinItems.Index].Value.ToDecimal()) + decQty))
                                    //        {
                                    //            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4253, out MmsgMessageIcon);
                                    //            intRowIndex = i;
                                    //            strColumnName = "Quantity";
                                    //            blnIsValid = false;
                                    //        }
                                    //    }
                                    //    if ((dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal()) == dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    //    {
                                    //        if (dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal() > decOldQuantityDeliveredItemWise)
                                    //        {
                                    //            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4260, out MmsgMessageIcon);
                                    //            intRowIndex = i;
                                    //            strColumnName = "Quantity";
                                    //            blnIsValid = false;
                                    //        }
                                    //    }

                                    //}
                                    //else  if ((intCostingMethod == 3) && (dgvItemDetailsGrid.Rows[i].Cells[IsGroup.Index].Value.ToBoolean()==false))
                                    //{
                                    //    if (decDeliverdQtyItemWise != dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    //    {
                                    //        if (((dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal() - decOldQuantityDeliveredItemWise) + dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()) > dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    //        {
                                    //            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4253, out MmsgMessageIcon);
                                    //            intRowIndex = i;
                                    //            strColumnName = "Quantity";
                                    //            blnIsValid = false;
                                    //        }
                                    //    }


                                      
                                    //    if (decDeliverdQtyItemWise == dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    //    {
                                            
                                    //        if ((dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()) > decOldQuantityDeliveredItemWise)
                                    //        {
                                    //            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4260, out MmsgMessageIcon);
                                    //            intRowIndex = i;
                                    //            strColumnName = "Quantity";
                                    //            blnIsValid = false;
                                    //        }
                                    //    }
                                    //}
                                    //else if (dgvItemDetailsGrid.Rows[i].Cells[IsGroup.Index].Value.ToBoolean() == true)
                                    //{
                                    //    if (decDeliverdQtyItemWise != dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    //    {
                                    //        if (((dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal() - decOldQuantityDeliveredItemWise) + dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()) > dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    //        {
                                    //            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4253, out MmsgMessageIcon);
                                    //            intRowIndex = i;
                                    //            strColumnName = "Quantity";
                                    //            blnIsValid = false;
                                    //        }
                                    //    }


                               
                                    //    if (decDeliverdQtyItemWise == dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    //    {
                                            
                                    //        if ((dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()) > decOldQuantityDeliveredItemWise)
                                    //        {
                                    //            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4260, out MmsgMessageIcon);
                                    //            intRowIndex = i;
                                    //            strColumnName = "Quantity";
                                    //            blnIsValid = false;
                                    //        }
                                    //    }
                                    //}

                                }

                                if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesOrder)
                                {

                                    if (decDeliverdQtyItemWise != dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    {
                                        if (((dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal() - decOldQuantityDeliveredItemWise) + dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()) > dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                       
                                        {
                                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4254, out MmsgMessageIcon);
                                            intRowIndex = i;
                                            strColumnName = "Quantity";
                                            blnIsValid = false;
                                        }
                                    }
                                   

                                    if (decDeliverdQtyItemWise == dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    {
                                       
                                        if ((dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()) > decOldQuantityDeliveredItemWise)
                                       
                                        {
                                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4259, out MmsgMessageIcon);
                                            intRowIndex = i;
                                            strColumnName = "Quantity";
                                            blnIsValid = false;
                                        }
                                    }



                                    //if (intCostingMethod != 3)
                                    //{
                                    //    if ((dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal()) != dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    //    {
                                    //        //if (((dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal() - (dgvItemDetailsGrid.Rows[i].Cells[TotalOldQuantityForMinItems.Index].Value.ToDecimal() - decOldQuantityDeliveredItemWise)) - dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()) < 0 )
                                    //        //if (((dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal() - decOldQuantityDeliveredItemWise) + dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()) > dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    //        if (dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal() < ((dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal() - dgvItemDetailsGrid.Rows[i].Cells[TotalOldQuantityForMinItems.Index].Value.ToDecimal())+decQty))
                                    //        {
                                    //            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4254, out MmsgMessageIcon);
                                    //            intRowIndex = i;
                                    //            strColumnName = "Quantity";
                                    //            dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value = decOldQuantityDeliveredItemWise.ToString();
                                    //            blnIsValid = false;
                                    //        }
                                            
                                    //    }
                                    //    if ((dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal()) == dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    //    {
                                    //        if (dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal() > decOldQuantityDeliveredItemWise)
                                    //        {
                                    //            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4259, out MmsgMessageIcon);
                                    //            intRowIndex = i;
                                    //            strColumnName = "Quantity";
                                    //            blnIsValid = false;
                                    //        }
                                    //    }
                                    //}

                                    //else if (intCostingMethod == 3)
                                    //{
                                    //    if (decDeliverdQtyItemWise != dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    //    {
                                    //        if (((dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal() - decOldQuantityDeliveredItemWise)+dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()) > dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal() )
                                    //            //- dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal()))
                                    //        {
                                    //            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4254, out MmsgMessageIcon);
                                    //            intRowIndex = i;
                                    //            strColumnName = "Quantity";
                                    //            blnIsValid = false;
                                    //        }
                                    //    }
                                    //    //if ((dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal()) == (dgvItemDetailsGrid.Rows[i].Cells[DeliveredQty.Index].Value.ToDecimal()))

                                    //    if (decDeliverdQtyItemWise == dgvItemDetailsGrid.Rows[i].Cells[InvoicedQuantity.Index].Value.ToDecimal())
                                    //    {
                                    //        //oldvalidation
                                    //        if ((dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal()) > decOldQuantityDeliveredItemWise)
                                    //        //if ((dgvItemDetailsGrid.Rows[i].Cells[Quantity.Index].Value.ToDecimal())> decDeliverdQtyItemWise)
                                    //        {
                                    //            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4259, out MmsgMessageIcon);
                                    //            intRowIndex = i;
                                    //            strColumnName = "Quantity";
                                    //            blnIsValid = false;
                                    //        }
                                    //    }
                                    //}

                                }
                            //}
                         }
                    }
                }
                    //else 
                    //{
                        
                //        if (blnIsValid)
                //        {

                //            datItemDetails.DefaultView.RowFilter = "ItemID = " + dgvItemDetailsGrid[ItemID.Index, i].Value.ToInt32();
                //            decimal decInvoicedQty = 0;
                //            foreach (DataRow dr in datItemDetails.DefaultView.ToTable().Rows)
                //            {
                //                //decInvoicedQty = dr["InvQty"].ToDecimal();
                //                decInvoicedQty += ConvertValue(dr["InvQty"].ToDecimal(), dr["InvUOMID"].ToInt32(), dr["ItemID"].ToInt32());
                //            }

                //            decimal decQuantity = 0, decCurrentQty = Convert.ToDecimal(dgvItemDetailsGrid["Quantity", i].Value);
                //            decCurrentQty = ConvertValue(decCurrentQty, Convert.ToInt32(dgvItemDetailsGrid["Uom", i].Tag), Convert.ToInt32(dgvItemDetailsGrid["ItemID", i].Value));

                //            decQuantity = decCurrentQty;
                //            //if (!lstCheckedSerialNos.Contains(Convert.ToInt32(dgvItemDetailsGrid["ReferenceSerialNo", i].Value)))
                //            //{
                //            for (int j = 0; j < dgvItemDetailsGrid.Rows.Count; j++)
                //            {
                //                if (j != i)
                //                {
                //                    if (Convert.ToInt32(dgvItemDetailsGrid["ItemID", i].Value) == Convert.ToInt32(dgvItemDetailsGrid["ItemID", j].Value))
                //                        decCurrentQty += ConvertValue(Convert.ToDecimal(dgvItemDetailsGrid["Quantity", j].Value), Convert.ToInt32(dgvItemDetailsGrid["Uom", j].Tag), Convert.ToInt32(dgvItemDetailsGrid["ItemID", j].Value));
                //                    if (Convert.ToInt32(dgvItemDetailsGrid["ItemID", i].Value) == Convert.ToInt32(dgvItemDetailsGrid["ItemID", j].Value) && Convert.ToInt32(dgvItemDetailsGrid["BatchNo", i].Tag) == Convert.ToInt32(dgvItemDetailsGrid["BatchNo", j].Tag))
                //                        decQuantity += ConvertValue(Convert.ToDecimal(dgvItemDetailsGrid["Quantity", j].Value), Convert.ToInt32(dgvItemDetailsGrid["Uom", j].Tag), Convert.ToInt32(dgvItemDetailsGrid["ItemID", j].Tag));
                //                }
                //            }
                //            //lstCheckedSerialNos.Add(Convert.ToInt32(dgvItemDetailsGrid["ReferenceSerialNo", i].Value));
                //            decimal decOldQuantity = 0;
                //            if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID != 0)
                //            {
                //                dtOldItemIssueDetails.DefaultView.RowFilter = "ItemID = " + Convert.ToInt32(dgvItemDetailsGrid["ItemID", i].Value) + " And BatchID = " + Convert.ToInt32(dgvItemDetailsGrid["BatchNo", i].Tag) + "";
                //                foreach (DataRow dr in dtOldItemIssueDetails.DefaultView.ToTable().Rows)
                //                {
                //                    decOldQuantity += ConvertValue(Convert.ToDecimal(dr["Quantity"]), Convert.ToInt32(dr["UomID"]), Convert.ToInt32(dr["ItemID"]));
                //                }
                //            }

                //            //if (Math.Round(decCurrentQty, 3) != Math.Round(decInvoicedQty, 3))
                //            //{
                //            //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4219, out MmsgMessageIcon);
                //            //    intRowIndex = i;
                //            //    strColumnName = "Quantity";
                //            //    blnIsValid = false;
                //            //}
                //            else if (Convert.ToBoolean(dgvItemDetailsGrid["IsGroup", i].Value) == false)
                //            {


                //                if (!ValidateStockQuantity(Convert.ToInt32(dgvItemDetailsGrid["ItemID", i].Value), Convert.ToInt32(dgvItemDetailsGrid["BatchNo", i].Tag), decQuantity - decOldQuantity, 0))
                //                {
                //                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4212, out MmsgMessageIcon);
                //                    MstrCommonMessage = MstrCommonMessage.Replace("*", Convert.ToString(dgvItemDetailsGrid["ItemName", i].Value));
                //                    intRowIndex = i;
                //                    strColumnName = "Quantity";
                //                    blnIsValid = false;
                //                }
                //            }


                        
                //    }
                   
                //}
                intItemCount++;
            }
           
            //else if (blnIsValid)
            //{
            //    string strItemName = "";
            //    foreach(DataRow dr in datItemDetails.Rows)
            //    {
            //        if(!lstSelectedItems.Contains(dr["ItemID"].ToInt32()))
            //        {
            //            strItemName = dr["ItemName"].ToString();
            //            break;
            //        }
            //    }
            //    if(!string.IsNullOrEmpty(strItemName))
            //    {
            //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4244, out MmessageIcon).Replace("*", strItemName);
            //        strColumnName = "ItemCode";
            //        intRowIndex = dgvItemDetailsGrid.RowCount - 1;
            //        blnIsValid = false;
            //    }
            //}
            if (!blnIsValid)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                ErrItemIssue.SetError(dgvItemDetailsGrid, MstrCommonMessage.Replace("#", "").Trim());
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrItemIssue.Enabled = true;
                tcDeliveryNote.SelectedTab = tiItemDetails;
                dgvItemDetailsGrid.CurrentCell = dgvItemDetailsGrid[strColumnName, intRowIndex];
                dgvItemDetailsGrid.Focus();
            }
            return blnIsValid;
        }

        private bool ValidateStockQuantity(int intItemID, int intBatchID, decimal decQuantity, int intUomID)
        {
            decimal decStockQuantity = MobjClsBLLItemIssueMaster.GetStockQuantity(intItemID, intBatchID, MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intCompanyID, Convert.ToInt32(cboWarehouse.SelectedValue));
            if (intUomID != 0)
            {
                DataTable dtUomDetails = MobjClsBLLItemIssueMaster.GetUomConversionValues(intUomID, intItemID);
                if (dtUomDetails.Rows.Count > 0)
                {
                    int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                    decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);
                    if (intConversionFactor == 1)
                        decQuantity = decQuantity / decConversionValue;
                    else if (intConversionFactor == 2)
                        decQuantity = decQuantity * decConversionValue;
                }
            }
            if (decQuantity > decStockQuantity)
            {
                return false;
            }
            return true;
        }

        private void DisplayAllIssueNos()
        {
            //Function for displaying all schedule Nos Filter By Condition
            string strCondition = string.Empty;

            if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
            {
                strCondition = "ItemIssueNo = '" + txtSearch.Text + "'";
            }
            else
            {
                if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                {
                    strCondition += "CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue) + "";
                }
                else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    DataTable dtCompany = (DataTable)cboSCompany.DataSource;

                    if (dtCompany.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(strCondition))
                            strCondition += " And ";
                        strCondition += "CompanyID In (";
                        foreach (DataRow dr in dtCompany.Rows)
                            strCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                        strCondition = strCondition.Remove(strCondition.Length - 1);
                        strCondition += ")";
                    }
                }
                if (cboSOperationType.SelectedValue != null && Convert.ToInt32(cboSOperationType.SelectedValue) != -2)
                {
                    if (!string.IsNullOrEmpty(strCondition))
                        strCondition += " And ";
                    strCondition += "OrderTypeID = " + Convert.ToInt32(cboSOperationType.SelectedValue) + "";
                }
                if (cboSWarehouse.SelectedValue != null && Convert.ToInt32(cboSWarehouse.SelectedValue) != -2)
                {
                    if (!string.IsNullOrEmpty(strCondition))
                        strCondition += " And ";
                    strCondition += "WarehouseID = " + Convert.ToInt32(cboSWarehouse.SelectedValue) + "";
                }
                else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                    {
                        DataTable dtWarehouse = (DataTable)cboSWarehouse.DataSource;

                        if (dtWarehouse.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(strCondition))
                                strCondition += " And ";
                            strCondition += "WarehouseID In (";
                            foreach (DataRow dr in dtWarehouse.Rows)
                                strCondition += Convert.ToInt32(dr["WarehouseID"]) + ",";
                            strCondition = strCondition.Remove(strCondition.Length - 1);
                            strCondition += ")";
                        }
                    }
                }

                if (cboSIssuedBy.SelectedValue != null && Convert.ToInt32(cboSIssuedBy.SelectedValue) != -2)
                {
                    if (!string.IsNullOrEmpty(strCondition))
                        strCondition += " And ";
                    strCondition += "UserID = " + Convert.ToInt32(cboSIssuedBy.SelectedValue) + "";
                }
                //else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                //{
                //    //if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                //    //{
                //        DataTable dtEmployee = (DataTable)cboSIssuedBy.DataSource;

                //        if (dtEmployee.Rows.Count > 0)
                //        {
                //            if (!string.IsNullOrEmpty(strCondition))
                //                strCondition += " And ";

                //            //clsBLLPermissionSettings MobjClsBLLPermissionSettings = new clsBLLPermissionSettings();
                //            //DataTable datTempPer = MobjClsBLLPermissionSettings.GetControlPermissions1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.DelNoteEmployee);
                //            //if (datTempPer != null)
                //            //{
                //            //    if (datTempPer.Rows.Count > 0)
                //            //    {
                //            //        if (datTempPer.Rows[0]["IsVisible"].ToString() == "True")
                //            //            strCondition += "EmployeeID In (0,";
                //            //        else
                //            //            strCondition += "EmployeeID In (";
                //            //    }
                //            //    else
                //            //        strCondition += "EmployeeID In (";
                //            //}
                //            //else
                //                strCondition += "UserID In (";

                //            foreach (DataRow dr in dtEmployee.Rows)
                //                strCondition += Convert.ToInt32(dr["UserID"]) + ",";
                //            strCondition = strCondition.Remove(strCondition.Length - 1);
                //            strCondition += ")";
                //        }
                //    //}
                //}
                if (!string.IsNullOrEmpty(strCondition))
                    strCondition += " And ";
                strCondition += " IssuedDate >= '" + dtpSFrom.Value.ToString("dd-MMM-yyyy") + "' And IssuedDate <= '" + dtpSTo.Value.ToString("dd-MMM-yyyy") + "'";
            }

         
            DataTable dtIssues = MobjClsBLLItemIssueMaster.GetIssueNos(strCondition);
            dgvIssueDisplay.DataSource = null;
            dgvIssueDisplay.DataSource = dtIssues;
            dgvIssueDisplay.Columns[0].Visible = false;
            dgvIssueDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            lblSCountStatus.Text = "Total Issues " + dgvIssueDisplay.Rows.Count.ToString();
        }

        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            if (!CheckDontEdit())
            {
                return;
            }
            AddNewItemIssue();
        }

        private string GetItemIssuePrefix()
        {
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue));
            if (PintFormType == 1)
                return ClsCommonSettings.ItemIssuePrefix;
            else
                return ClsCommonSettings.MaterialIssuePrefix;
        }


        private void GenerateItemIssueNo()
        {
            txtIssueNo.Text = GetItemIssuePrefix() + MobjClsBLLItemIssueMaster.GetNextIssueNo(cboCompany.SelectedValue.ToInt32(), PintFormType).ToString();

            if (PintFormType == 1)
            {
                if (ClsCommonSettings.ItemIssueAutogenerate)
                    txtIssueNo.ReadOnly = false;
                else
                    txtIssueNo.ReadOnly = true;
            }
            else
            {
                if (ClsCommonSettings.MaterialIssueAutogenerate)
                    txtIssueNo.ReadOnly = false;
                else
                    txtIssueNo.ReadOnly = true;
            }
        }

        private void bnClear_Click(object sender, EventArgs e)
        {
            if (!CheckDontEdit())
            {
                return;
            }
            ClearControls();
        }

        private void bnDelete_Click(object sender, EventArgs e)
        {
            //Do you want to delete the Item ?
            if (DeleteValidation())
            {
                //if (!dgvItemDetailsGrid.ReadOnly)
                //{
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4213, out MmsgMessageIcon);
                    if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;
                    try
                    {
                        MobjClsBLLItemIssueMaster.DeleteItemIssue();
                        //delivery note alert
                        if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice)
                        {
                            //int itemIssue = MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID.ToInt32();

                            //clsBLLAlertMoment objclsBLLAlertMoment = new clsBLLAlertMoment();
                            //objclsBLLAlertMoment.DeleteAlert(itemIssue, (int)AlertSettingsTypes.DeliveryNote);

                            //objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.DeliveryNote;
                            //objFrmTradingMain.tmrAlert.Enabled = true;
                        }

                        AddNewItemIssue();
                    }

                    catch (Exception ex)
                    {
                        if (ClsCommonSettings.ShowErrorMess)
                            MessageBox.Show("Error in bnDelete_Click() " + ex.Message);
                        MobjClsLogWriter.WriteLog("Error in bnDelete_Click() " + ex.Message, 2);
                    }
                //}
                //else
                //{
                //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4221, out MmsgMessageIcon);
                //    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                //        MessageBoxButtons.OK, MessageBoxIcon.Information);
                //}
            }
        }

        private bool DeleteValidation()
        {
            if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTSalesInvoice)
            {
                if (MobjClsBLLItemIssueMaster.FillCombos(new string[] { "SalesReturnID", "InvSalesReturnMaster", "SalesInvoiceID = " + Convert.ToInt32(cboReferenceNo.SelectedValue) }).Rows.Count > 0)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4225,out MmessageIcon);
                    MstrCommonMessage = MstrCommonMessage.Replace("*", "this SalesInvoice");
                    MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return false;
                }
            }

            if (Convert.ToInt32(cboOperationType.SelectedValue) == (Int32)OperationOrderType.DNOTTransfer)
            {
                if (MobjClsBLLItemIssueMaster.FillCombos(new string[] { "GRN.GRNID", "InvGRNMaster GRN  INNER JOIN InvGRNReferenceDetails  GRND ON GRN.GRNID=GRND.GRNID", "GRN.OrderTypeID = " + (Int32)OperationOrderType.GRNFromTransfer + " And GRND.ReferenceID =" + cboReferenceNo.SelectedValue.ToInt64() }).Rows.Count > 0)
                {
                   MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4261, out MmessageIcon);
                    MstrCommonMessage = MstrCommonMessage.Replace("*", "GRN ");
                    MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return false;
                }
            }

            else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTTransfer)
            {
                if (MobjClsBLLItemIssueMaster.FillCombos(new string[] { "ExpenseID", "InvExpenseMaster", "OperationTypeID = " + (int)OperationType.StockTransfer + " And ReferenceID = " + cboReferenceNo.SelectedValue.ToInt64() +" And IsDirectExpense = 0"}).Rows.Count > 0)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4247,out MmessageIcon);
                    MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return false;
                }
            }
            //else if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTDMR)
            //{
            //    if (mobjClsBllCommonUtility.FillCombos(new string[] { "1", "PrdRawMaterialUsedDetails", "ItemIssueID = " + MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID }).Rows.Count > 0)
            //    {
            //        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4249,out MmessageIcon);
            //        MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            //        return false;
            //    }
            //}
            return true;

        }

        private void cboWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvItemDetailsGrid.Rows.Clear();
            if (cboWarehouse.SelectedIndex != -1)
                cboOperationType_SelectedIndexChanged(null, null);
            ChangeStatus();
        }

        private void DisplayIssueInfo()
        {
            // function for displaying Issue Information
            if (MobjClsBLLItemIssueMaster.DisplayItemIssuenfo())
            {
                ClearControls();
                blnIsEditMode = true;                
                cboCompany.SelectedValue = MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intCompanyID;

                if (cboCompany.SelectedValue == null)
                {
                    DataTable datTemp = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intCompanyID });

                    if (datTemp.Rows.Count > 0)
                        cboCompany.Text = datTemp.Rows[0]["CompanyName"].ToString();
                }

                LoadCombo(6);
                txtIssueNo.Text = MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.strItemIssueNo;
                LoadCombo(2);
                cboOperationType.SelectedValue = MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intOperationTypeID;

                if (cboOperationType.SelectedValue == null)
                {
                    DataTable datTemp = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "*", "CommonOrderTypeReference", "OrderTypeID = " + MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intOperationTypeID });

                    if (datTemp.Rows.Count > 0)
                        cboOperationType.Text = datTemp.Rows[0]["OrderType"].ToString();
                }

                cboWarehouse.SelectedValue = MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intWarehouseID;

                if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice || cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesOrder)
                {
                    FillReferenceNoEditMode();
                    cboReferenceNo.SelectedValue = MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intReferenceID;
                }

                if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTTransfer)
                    FillReferenceNo();

                lblIssuedBy.Text = MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.strIssuedBy;
                cboReferenceNo.SelectedValue = MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intReferenceID;
                txtLpoNo.Text = MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.Lpono;
                
                if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice)
                {
                    dgvItemDetailsGrid.Visible = true;
                    dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
                    dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = true;
                    dgvItemDetailsGrid.Columns[DeliveredQty.Index].Visible = true;

                    btnCustomerwisePrint.Enabled = false;
                    ItemCode.ReadOnly = ItemName.ReadOnly = Quantity.ReadOnly = false;

                    Uom.ReadOnly = true;
                    dgvItemDetailsGrid.ReadOnly = false;
                    dgvItemDetailsGrid.AllowUserToAddRows = true;
                    dgvItemDetailsGrid.AllowUserToDeleteRows = true;
                }

                if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesOrder)
                {                    
                    dgvItemDetailsGrid.Visible = true;
                    dgvItemDetailsGrid.Columns["ReferenceNo"].Visible = false;
                    dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = true;
                    dgvItemDetailsGrid.Columns[DeliveredQty.Index].Visible = true;

                    btnCustomerwisePrint.Enabled = false;
                    ItemCode.ReadOnly = ItemName.ReadOnly   = Quantity.ReadOnly = false;
                    Uom.ReadOnly = true;
                    dgvItemDetailsGrid.ReadOnly = false;
                    dgvItemDetailsGrid.AllowUserToAddRows = true;
                    dgvItemDetailsGrid.AllowUserToDeleteRows = true;                   
                }

                if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTTransfer)
                {
                    dgvItemDetailsGrid.ReadOnly = true;
                    dgvItemDetailsGrid.Columns[InvoicedQuantity.Index].Visible = false;
                    dgvItemDetailsGrid.Columns[DeliveredQty.Index].Visible = false;
                }

                dtpIssuedDate.Value = Convert.ToDateTime(MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.strIssuedDate);
                txtRemarks.Text = MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.strRemarks;
                DataTable dtDetDetails = MobjClsBLLItemIssueMaster.GetItemIssueDetails();
                MDtItemGroupIssueDetails.Rows.Clear();
               
                dgvItemDetailsGrid.DataSource = null;
                dgvItemDetailsGrid.Rows.Clear();
                dtItemDetails.Rows.Clear();

                for (int i = 0; i < dtDetDetails.Rows.Count; i++)
                {
                    dgvItemDetailsGrid.RowCount = dgvItemDetailsGrid.RowCount + 1;                        
                    decimal decDeliveredQtyForBatchlessItems = 0;
                    decTotalOldQuantityForMinItem = 0;
                    decimal decQty = 0;
                    int intCostingMethod = 0;
                    DataTable datData = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + dtDetDetails.Rows[i]["ItemID"].ToInt32() + "" });
                    
                    if (datData.Rows.Count > 0)
                        intCostingMethod = datData.Rows[0]["CostingMethodID"].ToInt32();

                    dgvItemDetailsGrid.Rows[i].Cells["IsGroup"].Value = Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]);
                    dgvItemDetailsGrid.Rows[i].Cells["ItemID"].Value  = Convert.ToInt32(dtDetDetails.Rows[i]["ItemID"]);
                    dgvItemDetailsGrid.Rows[i].Cells["ReferenceNo"].Value = dtDetDetails.Rows[i]["ReferenceNo"];
                    dgvItemDetailsGrid.Rows[i].Cells["ReferenceSerialNo"].Value= Convert.ToInt32(dtDetDetails.Rows[i]["ReferenceSerialNo"]);
                    dgvItemDetailsGrid.Rows[i].Cells["ItemCode"].Value = Convert.ToString(dtDetDetails.Rows[i]["Code"]);
                    dgvItemDetailsGrid.Rows[i].Cells["ItemName"].Value= Convert.ToString(dtDetDetails.Rows[i]["ItemName"]);
                            
                    FillUomColumn(Convert.ToInt32(dtDetDetails.Rows[i]["ItemID"]), Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]));
                    dgvItemDetailsGrid.Rows[i].Cells["Uom"].Value = Convert.ToInt32(dtDetDetails.Rows[i]["UomID"]);
                    dgvItemDetailsGrid.Rows[i].Cells["Uom"].Tag = Convert.ToInt32(dtDetDetails.Rows[i]["UomID"]);
                    dgvItemDetailsGrid.Rows[i].Cells["Uom"].Value = dgvItemDetailsGrid.Rows[i].Cells["Uom"].FormattedValue;
                    dgvItemDetailsGrid.Rows[i].Cells["PackingUnit"].Value = Convert.ToString(dtDetDetails.Rows[i]["PackingUnit"]);

                    int intUomScale = 0;

                    if (  dgvItemDetailsGrid.Rows[i].Cells["Uom"].Tag.ToInt32() != 0)
                        intUomScale = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvItemDetailsGrid.Rows[i].Cells["Uom"].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                    dgvItemDetailsGrid.Rows[i].Cells["Quantity"].Value = dtDetDetails.Rows[i]["Quantity"].ToDecimal().ToString("F" + intUomScale);
                    decDeliveredQtyForBatchlessItems = MobjClsBLLItemIssueMaster.GetDeliveredQty(Convert.ToInt32(dtDetDetails.Rows[i]["ItemID"]), Convert.ToInt32(dtDetDetails.Rows[i]["ReferenceID"]), Convert.ToInt32(dtDetDetails.Rows[i]["OrderTypeID"]));
                    decTotalOldQuantityForMinItem = MobjClsBLLItemIssueMaster.GetDeliveredQtyNonBatchItems(Convert.ToInt32(dtDetDetails.Rows[i]["ItemID"]), Convert.ToInt32(dtDetDetails.Rows[i]["ItemIssueID"]));
                            
                    if (Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) != true && intCostingMethod == 3)
                    {
                        dgvItemDetailsGrid.Rows[i].Cells["BatchNo"].ReadOnly = true;
                        dgvItemDetailsGrid.Rows[i].Cells["DeliveredQty"].Value = dtDetDetails.Rows[i]["DeliveredQty"].ToDecimal().ToString("F" + intUomScale);
                        dgvItemDetailsGrid.Rows[i].Cells["TotalOldQuantityForMinItems"].Value = 0;
                    }
                    else if (intCostingMethod != 3 && Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) != true)
                    {
                        dgvItemDetailsGrid.Rows[i].Cells["BatchNo"].ReadOnly = true;
                        dgvItemDetailsGrid.Rows[i].Cells["OldBatchID"].Value = dtDetDetails.Rows[i]["BatchID"].ToInt32();
                        dgvItemDetailsGrid.Rows[i].Cells["DeliveredQty"].Value = dtDetDetails.Rows[i]["DeliveredQty"].ToDecimal().ToString("F" + intUomScale);
                        dgvItemDetailsGrid.Rows[i].Cells["TotalOldQuantityForMinItems"].Value = decTotalOldQuantityForMinItem;                        
                    }
                    else if (Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) == true)
                    {
                        dgvItemDetailsGrid.Rows[i].Cells["BatchNo"].ReadOnly = true;
                        dgvItemDetailsGrid.Rows[i].Cells["DeliveredQty"].Value  = dtDetDetails.Rows[i]["DeliveredQty"].ToDecimal().ToString("F" + intUomScale);
                        dgvItemDetailsGrid.Rows[i].Cells["TotalOldQuantityForMinItems"].Value = 0;
                    }  

                    dgvItemDetailsGrid.Rows[i].Cells["BatchNo"].Value= dtDetDetails.Rows[i]["BatchNo"];
                    dgvItemDetailsGrid.Rows[i].Cells["BatchID"].Value = dtDetDetails.Rows[i]["BatchID"];
                               
                    if (cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesInvoice)
                    {
                        dgvItemDetailsGrid.Rows[i].Cells["InvoicedQuantity"].Value = dtDetDetails.Rows[i]["InvoicedQuantity"].ToDecimal().ToString("F" + intUomScale);
                        dgvItemDetailsGrid.Rows[i].Cells["InvQty"].Value = dtDetDetails.Rows[i]["InvQty"].ToDecimal().ToString("F" + intUomScale);
                        dgvItemDetailsGrid.Rows[i].Cells["InvUOMID"].Value= dtDetDetails.Rows[i]["InvUOMID"].ToString();
                    }
                    else if(cboOperationType.SelectedValue.ToInt32() == (int)OperationOrderType.DNOTSalesOrder)
                    {
                        dgvItemDetailsGrid.Rows[i].Cells["InvoicedQuantity"].Value = dtDetDetails.Rows[i]["InvoicedQuantity"].ToDecimal().ToString("F" + intUomScale);
                        dgvItemDetailsGrid.Rows[i].Cells["InvQty"].Value = dtDetDetails.Rows[i]["InvQty"].ToDecimal().ToString("F" + intUomScale);
                        dgvItemDetailsGrid.Rows[i].Cells["InvUOMID"].Value = dtDetDetails.Rows[i]["InvUOMID"].ToString();
                    } 
                    else
                    {
                        dgvItemDetailsGrid.Rows[i].Cells["InvoicedQuantity"].Value = dtDetDetails.Rows[i]["Quantity"].ToDecimal().ToString("F" + intUomScale);
                        dgvItemDetailsGrid.Rows[i].Cells["InvQty"].Value = dtDetDetails.Rows[i]["Quantity"].ToDecimal().ToString("F" + intUomScale);
                        dgvItemDetailsGrid.Rows[i].Cells["InvUOMID"].Value = dtDetDetails.Rows[i]["UOMID"].ToString();
                    }
                                
                    if (Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) != true && intCostingMethod == 3)
                    {
                        decimal decStockQuantity = MobjClsBLLItemIssueMaster.GetStockQuantity(Convert.ToInt32(dtDetDetails.Rows[i]["ItemID"]), Convert.ToInt32(dtDetDetails.Rows[i]["BatchID"]), MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intCompanyID, MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intWarehouseID);
                        DataTable dtUomDetails = MobjClsBLLItemIssueMaster.GetUomConversionValues(Convert.ToInt32(dgvItemDetailsGrid.Rows[i].Cells["Uom"].Tag), Convert.ToInt32(dgvItemDetailsGrid.Rows[i].Cells["ItemID"].Value));

                        if (dtUomDetails.Rows.Count > 0)
                        {
                            int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                            decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);

                            if (intConversionFactor == 1)
                                decStockQuantity = decStockQuantity * decConversionValue;
                            else if (intConversionFactor == 2)
                                decStockQuantity = decStockQuantity / decConversionValue;
                        }

                        dgvItemDetailsGrid.Rows[i].Cells["QtyAvailable"].Value= decStockQuantity.ToString("F" + intUomScale);
                    }
                    else if (intCostingMethod != 3 && Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) != true)
                    {
                        decimal QtyAvailableForBatchlessItems = MobjClsBLLItemIssueMaster.GetStockQuantityForBatchlessItems(Convert.ToInt32(dtDetDetails.Rows[i]["ItemID"]), MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intWarehouseID);
                        DataTable dtUomDetails = MobjClsBLLItemIssueMaster.GetUomConversionValues(Convert.ToInt32(dgvItemDetailsGrid["Uom", i].Tag), Convert.ToInt32(dgvItemDetailsGrid["ItemID", i].Value));

                        if (dtUomDetails.Rows.Count > 0)
                        {
                            int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                            decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);

                            if (intConversionFactor == 1)
                                QtyAvailableForBatchlessItems = QtyAvailableForBatchlessItems * decConversionValue;
                            else if (intConversionFactor == 2)
                                QtyAvailableForBatchlessItems = QtyAvailableForBatchlessItems / decConversionValue;
                        }

                        dgvItemDetailsGrid.Rows[i].Cells["QtyAvailable"].Value= QtyAvailableForBatchlessItems.ToString("F" + intUomScale);
                    }
                    else if (Convert.ToBoolean(dtDetDetails.Rows[i]["IsGroup"]) == true)
                    {
                        decimal QtyAvailableForGrp = MobjClsBLLItemIssueMaster.GetStockQuantityForGroup(Convert.ToInt32(dtDetDetails.Rows[i]["ItemID"]), Convert.ToInt32(cboWarehouse.SelectedValue));
                        DataTable dtUomDetails = MobjClsBLLItemIssueMaster.GetUomConversionValues(Convert.ToInt32(dgvItemDetailsGrid["Uom", i].Tag), Convert.ToInt32(dgvItemDetailsGrid["ItemID", i].Value));

                        if (dtUomDetails.Rows.Count > 0)
                        {
                            int intConversionFactor = Convert.ToInt32(dtUomDetails.Rows[0]["ConvertionFactorID"]);
                            decimal decConversionValue = Convert.ToDecimal(dtUomDetails.Rows[0]["ConvertionValue"]);

                            if (intConversionFactor == 1)
                                QtyAvailableForGrp = QtyAvailableForGrp * decConversionValue;
                            else if (intConversionFactor == 2)
                                QtyAvailableForGrp = QtyAvailableForGrp / decConversionValue;
                        }

                        dgvItemDetailsGrid.Rows[i].Cells["QtyAvailable"].Value = QtyAvailableForGrp.ToString("F" + intUomScale);
                    }
                }

                bnSaveItem.Enabled = false;
                bnAddNewItem.Enabled = MblnAddPermission;
                MblnIsEditable = true;

                if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intOperationTypeID == (int)OperationOrderType.DNOTSalesInvoice)
                {
                    if (MobjClsBLLItemIssueMaster.FillCombos(new string[] { "SalesReturnID", "InvSalesReturnMaster", "SalesInvoiceID = " + MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intReferenceID }).Rows.Count > 0)
                    {
                        MstrCommonMessage  = MobjClsNotification.GetErrorMessage(datMessages, 4238, out MmessageIcon);
                        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        MblnIsEditable = false;
                    }
                    else
                        lblStatus.Text = "Details of delivery note " + MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.strItemIssueNo;    
                }

                if (MblnIsEditable)
                    bnDelete.Enabled = MblnDeletePermission;
                else
                    bnDelete.Enabled = false;

                bnEmail.Enabled = MblnPrintEmailPermission;
                bnPrint.Enabled = MblnPrintEmailPermission;
                btnPicklist.Enabled = MblnPrintEmailPermission;
                btnPickListEmail.Enabled = MblnPrintEmailPermission;
                cboOperationType.Enabled = false;
                cboReferenceNo.Enabled = false;
                cboWarehouse.Enabled = false;
                cboCompany.Enabled = false;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                DisplayAllIssueNos();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnRefresh_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in btnRefresh_Click() " + ex.Message, 2);
            }
        }

        private void dgvIssueDisplay_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!CheckDontEdit())
            {
                return;
            }
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (dgvIssueDisplay["ItemIssueID", e.RowIndex].Value != null)
                    {
                        //intTotalOldQuantityForMinItem = 0;
                        MobjClsBLLItemIssueMaster = new clsBLLItemIssueMaster();
                        MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID = Convert.ToInt32(dgvIssueDisplay["ItemIssueID", e.RowIndex].Value);
                        
                        DisplayIssueInfo();

                        tcDeliveryNote.SelectedTab = tiItemDetails;
                        MchangeStatus = false;
                        bnSaveItem.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvIssueDisplay_CellClick() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvIssueDisplay_CellClick() " + ex.Message, 2);
            }
        }

        private void FrmItemIssue_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.MdiFormClosing)
            {
                if (MchangeStatus)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4214, out MmsgMessageIcon).Replace("#", "").Trim();
                    if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                        DialogResult.Yes)
                    {
                        //MobjClsNotification = null;
                        //MobjClsLogWriter = null;
                        // MobjClsBLLItemIssueMaster = null;
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        private void dgvItemDetailsGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void tmrItemIssue_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            tmrItemIssue.Enabled = false;
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.IsBOQEnabled = ClsMainSettings.BOQEnabled;
                    ObjViewer.PiRecId = MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID;

                    if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intOperationTypeID == (int)OperationOrderType.DNOTSalesInvoice)
                    {
                        ObjViewer.Type = "Sales Invoice No";
                    }
                    else if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intOperationTypeID == (int)OperationOrderType.DNOTSalesOrder)
                    {
                        ObjViewer.Type = "Sales Order No";
                    }
                    else if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intOperationTypeID == (int)OperationOrderType.DNOTTransfer)
                    {
                        ObjViewer.Type = "Transfer Order No";
                    }
                    
                    ObjViewer.PblnCustomerWiseDeliveryNote = false;
                    ObjViewer.PblnPickList = false;
                    ObjViewer.PiFormID = (int)FormID.ItemIssue;
                    ObjViewer.AlMajdalPrePrintedFormat = ClsMainSettings.AlMajdalPrePrintedFormat;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnPrint_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in bnPrint_Click() " + ex.Message, 2);
            }
        }

        private void bnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Delivery Note";
                    ObjEmailPopUp.EmailFormType = EmailFormID.ItemIssue;
                    ObjEmailPopUp.EmailSource = MobjClsBLLItemIssueMaster.GetDeliveryNoteReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnEmail_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in bnEmail_Click() " + ex.Message, 2);
            }
        }

        private void dgvItemDetailsGrid_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {

            //if (dgvItemDetailsGrid.ReadOnly)
            //    e.Cancel = true;
            //else
            //{
            //    //
            //    // if (dgvItemDetailsGrid["IsAutomatic", e.Row.Index].Value != null && dgvItemDetailsGrid["IsAutomatic", e.Row.Index].Value.ToString().ToUpper() == "TRUE")
            //    //   e.Cancel = true;
            //    e.Cancel = true;
            //    foreach (DataGridViewRow dr in dgvItemDetailsGrid.Rows)
            //    {
            //        if (dr.Index != e.Row.Index && dr.Cells["ItemID"].Value.ToInt32() == e.Row.Cells["ItemID"].Value.ToInt32())
            //        {
            //            e.Cancel = false;
            //            break;
            //        }
            //    }
            //}
            ChangeStatus();
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            objClsBLLLogin.GetCompanySettings(cboCompany.SelectedValue.ToInt32());
            if (!blnIsEditMode)
            {
                cboWarehouse.SelectedIndex = -1;
                cboReferenceNo.DataSource = null;
                cboReferenceNo.SelectedIndex = -1;
                LoadCombo(2);
                LoadCombo(6);
                GenerateItemIssueNo();
            }
            ChangeStatus();
        }

        private void dtpIssuedDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtRemarks_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboSCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable datEmployees = null, datWarehouse = null; //bool blnIsEnabled = false;
            clsBLLPermissionSettings objBllPermissionSettings = new clsBLLPermissionSettings();
            //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
            //{
            if (cboSCompany.SelectedValue != null || Convert.ToInt32(cboSCompany.SelectedValue) != -2)
            {
                datEmployees = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "EmployeeID,EmployeeFullName", "EmployeeMaster", "CompanyID = "+cboSCompany.SelectedValue.ToInt32()+"" });
                datWarehouse = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", "CompanyID = "+cboSCompany.SelectedValue.ToInt32()+"" });
            }
            else
            {
                datEmployees = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "EmployeeID,EmployeeFullName", "EmployeeMaster", "" });
                datWarehouse = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", "" });
            }
            //}
            //else
            //{
            //    if (cboSCompany.SelectedValue != null || Convert.ToInt32(cboSCompany.SelectedValue) != -2)
            //    {
            //        DataTable datTemp = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "IsVisible", "STRoleFieldsDetails", "" + 
            //            "RoleID=" + ClsCommonSettings.RoleID + " AND CompanyID=" + ClsCommonSettings.CompanyID + " " +
            //            "AND ControlID=" + (int)ControlOperationType.DelNoteEmployee + " AND IsVisible=1" });
            //        if(datTemp != null)
            //            if(datTemp.Rows.Count > 0)
            //                datEmployees = MobjClsBLLItemIssueMaster.GetEmployeeByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.DelNoteCompany);
            //        datWarehouse = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", "CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue) });

            //        if (datEmployees == null)
            //            datEmployees = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
            //                            " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
            //        else
            //        {
            //            if (datEmployees.Rows.Count == 0)
            //                datEmployees = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
            //                            " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
            //        }
            //    }                
            //}
            //DataRow drNewRow = datEmployees.NewRow();
            //drNewRow["EmployeeID"] = -2;
            //drNewRow["FirstName"] = "ALL";
            //datEmployees.Rows.InsertAt(drNewRow, 0);
            string strSearchCondition = "";
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                strSearchCondition = "UM.RoleID Not In (1,2)";
            }
            if (!string.IsNullOrEmpty(strSearchCondition))
                strSearchCondition = strSearchCondition + " AND ";
            strSearchCondition = strSearchCondition + "UM.UserID <> 1";

            datEmployees = MobjClsBLLItemIssueMaster.FillCombos(new string[] { "UM.UserID,IsNull(EM.FirstName,UM.UserName) as UserName", "UserMaster UM Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID", strSearchCondition });
            //cboSExecutive.ValueMember = "UserID";
            //cboSExecutive.DisplayMember = "UserName";
            //cboSExecutive.DataSource = datCombos;
            cboSIssuedBy.ValueMember = "UserID";
            cboSIssuedBy.DisplayMember = "UserName";
            cboSIssuedBy.DataSource = datEmployees;


            DataRow drNewRow = datWarehouse.NewRow();
            drNewRow["WarehouseID"] = -2;
            drNewRow["WarehouseName"] = "ALL";
            datWarehouse.Rows.InsertAt(drNewRow, 0);
            cboSWarehouse.ValueMember = "WarehouseID";
            cboSWarehouse.DisplayMember = "WarehouseName";
            cboSWarehouse.DataSource = datWarehouse;

          //  LoadCombo(4);                
        }
        public void RefreshForm()
        {
            //int intReferenceID = Convert.ToInt32(cboReferenceNo.SelectedValue);
            //cboOperationType_SelectedIndexChanged(null, null);
            //if (intReferenceID != 0)
            //    cboReferenceNo.SelectedValue = intReferenceID;
            btnRefresh_Click(null, null);
        }

        private void cboSOperationType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dgvItemDetailsGrid_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvItemDetailsGrid.IsCurrentCellDirty)
                {
                    if (dgvItemDetailsGrid.CurrentCell != null)
                    {
                        dgvItemDetailsGrid.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    }
                }
            }
            catch (Exception)
            { }
        }


        private void FrmItemIssue_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Alt | Keys.S:
                        bnSaveItem_Click(sender, new EventArgs());//save
                        break;
                    case Keys.Alt | Keys.R:
                        bnDelete_Click(sender, new EventArgs());//OK
                        break;


                }
            }
            catch (Exception)
            {
            }
        }

        private void dgvItemDetailsGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {

            }
            catch
            {

            }
        }

        private Boolean CheckDontEdit()
        {
            bool blnFlag = true;
            if (MchangeStatus)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 36, out MmsgMessageIcon).Replace("#", "").Trim();
                if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                    DialogResult.No)
                {
                    blnFlag = false;
                }
                else
                {
                    blnFlag = true;
                }
            }
            return blnFlag;
        }

        private void txtIssueNo_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void btnCustomerwisePrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID;

                    if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intOperationTypeID == 9)
                    {
                        ObjViewer.Type = "Sales Invoice No";
                    }
                    else if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intOperationTypeID == 10)
                    {
                        ObjViewer.Type = "Pont Of Sale No";

                    }
                    else
                    {
                        ObjViewer.Type = "Delivery Schedule No";
                        ObjViewer.PblnCustomerWiseDeliveryNote = true;
                    }

                    ObjViewer.PiFormID = (int)FormID.ItemIssue;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnPrint_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in bnPrint_Click() " + ex.Message, 2);
            }
        }

        private bool FillItemLocationDetails()
        {
            int iTempID = 0;
            iTempID = CheckDuplicationInGrid();
            if (iTempID != -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4243, out MmessageIcon);
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                tmrItemIssue.Enabled = true;
                tcDeliveryNote.SelectedTab = tiItemDetails;
                dgvItemDetailsGrid.CurrentCell = dgvItemDetailsGrid["ItemCode", iTempID];
                dgvItemDetailsGrid.Focus();
                return false ;
            }
            return true;
        }

        private void tcDeliveryNote_SelectedTabChanged(object sender, TabStripTabChangedEventArgs e)
        {
            
        }

        private void btnPicklist_Click(object sender, EventArgs e)
        {
            try
            {
                if (MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MobjClsBLLItemIssueMaster.PobjClsDTOItemIssueMaster.intItemIssueID;
                    ObjViewer.Type = "Delivery Note";
                    ObjViewer.PblnCustomerWiseDeliveryNote = false;
                    ObjViewer.PblnPickList = true;
                    ObjViewer.PiFormID = (int)FormID.ItemIssue;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnPrint_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in bnPrint_Click() " + ex.Message, 2);
            }
        }

        private void btnPickListEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Delivery Note Location Details";
                    ObjEmailPopUp.EmailFormType = EmailFormID.PickList;
                    ObjEmailPopUp.EmailSource = MobjClsBLLItemIssueMaster.DisplayItemIssueLocationReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on Email:btnPickListEmail_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnPickListEmail_Click " + Ex.Message.ToString());
            }
        }

        private int CheckDuplicationInGrid()
        {
            int intItemID = 0;
            long lngBatchID = 0;
            int RowIndexTemp = 0;
            bool blnIsGroup = false;

            foreach (DataGridViewRow rowValue in dgvItemDetailsGrid.Rows)
            {
                if (rowValue.Cells[ItemID.Index].Value != null && rowValue.Cells[BatchID.Index].Value != null)
                {
                    intItemID = Convert.ToInt32(rowValue.Cells[ItemID.Index].Value);
                   blnIsGroup = Convert.ToBoolean(rowValue.Cells["IsGroup"].Value);
                    lngBatchID = rowValue.Cells[BatchID.Index].Value.ToInt64();
                    RowIndexTemp = rowValue.Index;
                    foreach (DataGridViewRow row in dgvItemDetailsGrid.Rows)
                    {
                        if (RowIndexTemp != row.Index)
                        {
                            if (row.Cells[ItemID.Index].Value != null && row.Cells[BatchID.Index].Value.ToInt64() != 0)
                            {
                                if ((Convert.ToInt32(row.Cells[ItemID.Index].Value) == intItemID) && (row.Cells[BatchID.Index].Value.ToInt64() == lngBatchID))
                                    return row.Index;
                            }
                            else if (row.Cells[ItemID.Index].Value != null)
                            {
                                if ((Convert.ToInt32(row.Cells[ItemID.Index].Value) == intItemID) && Convert.ToBoolean(row.Cells[IsGroup.Index].Value) == blnIsGroup)
                                    return row.Index;
                            }
                        }
                    }
                }
            }
            return -1;
        }

        private void expandableSplitterLeft_ExpandedChanged(object sender, ExpandedChangeEventArgs e)
        {
            if (expandableSplitterLeft.Expanded)
            {
                if (dgvIssueDisplay.Columns.Count > 0)
                    dgvIssueDisplay.Columns[0].Visible = false;
            }
        }

        private void lnkLabelAdvanceSearch_Click(object sender, EventArgs e)
        {
            DataTable datAdvanceSearchedData = new DataTable();
            if ((int)PintFormType == 1)
            {
                using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.DeliveryNote))
                {
                    objFrmSearchForm.ShowDialog();
                    if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                    {
                        datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                        datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                        datAdvanceSearchedData.Columns["ID"].ColumnName = "ItemIssueID";
                        dgvIssueDisplay.DataSource = datAdvanceSearchedData;
                        dgvIssueDisplay.Columns["ItemIssueID"].Visible = false;
                        dgvIssueDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    }
                }
            }
            else
            {
                using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.MaterialIssue))
                {
                    objFrmSearchForm.ShowDialog();
                    if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                    {
                        datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                        datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                        datAdvanceSearchedData.Columns["ID"].ColumnName = "ItemIssueID";
                        datAdvanceSearchedData.Columns["No"].ColumnName = "ItemIssueNo";
                        dgvIssueDisplay.DataSource = datAdvanceSearchedData;
                        dgvIssueDisplay.Columns["ItemIssueID"].Visible = false;
                        dgvIssueDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    }
                }
            }
        }

        private void dgvItemDetailsGrid_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (cboOperationType.SelectedValue.ToInt32() != (int)OperationOrderType.DNOTTransfer)
                mobjClsBllCommonUtility.SetSerialNo(dgvItemDetailsGrid, e.RowIndex, false);
            else
                mobjClsBllCommonUtility.SetSerialNo(dgvItemDetailsGrid, e.RowIndex, true);
        }

        private void dgvItemDetailsGrid_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (cboOperationType.SelectedValue.ToInt32() != (int)OperationOrderType.DNOTTransfer)
                mobjClsBllCommonUtility.SetSerialNo(dgvItemDetailsGrid, e.RowIndex, false);
            else
                mobjClsBllCommonUtility.SetSerialNo(dgvItemDetailsGrid, e.RowIndex, true);
        }

        private void showGroupItemDetails_Click(object sender, EventArgs e)
        {

            frmItemGroupMaster objfrmItemGroupMaster = new frmItemGroupMaster();
            objfrmItemGroupMaster.PintProductGroupID = dgvItemDetailsGrid.Rows[dgvItemDetailsGrid.CurrentRow.Index].Cells["ItemID"].Value.ToInt32();
            objfrmItemGroupMaster.ShowDialog();
        }

        private void dgvItemDetailsGrid_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                int intCurrentColumn = dgvItemDetailsGrid.CurrentCell.ColumnIndex;

               if(cboOperationType.SelectedValue.ToInt32() != (int)OperationOrderType.DNOTTransfer)
                {
                    if (intCurrentColumn == 0 || intCurrentColumn == 1)
                    {
                        if (e.Button == MouseButtons.Right)
                        {
                            if (dgvItemDetailsGrid.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt32() > 0)
                            {
                          
                                SItemID = dgvItemDetailsGrid.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt32();
                                SIsGroup = Convert.ToBoolean(dgvItemDetailsGrid.Rows[e.RowIndex].Cells["IsGroup"].Value) == true ? 1 : 0;
                                CntxtHistory.Items["showGroupItemDetails"].Visible = SIsGroup == 1 ? true : false;
                                this.CntxtHistory.Show(this.dgvItemDetailsGrid, this.dgvItemDetailsGrid.PointToClient(Cursor.Position));
                            }
                        }
                    }
                }
            }
        }


      
    }
}
