﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DevComponents.Editors;
using System.Windows.Forms.DataVisualization.Charting;

namespace MyBooksERP
{
    public partial class FrmSummary : DevComponents.DotNetBar.Office2007Form
    {
        #region Declaration

        ClsLogWriter MobjClsLogWriter;                      // obj of LogWriter
        clsBLLPermissionSettings objBllPermissionSettings;  // obj of PermissionSettings
        ClsNotificationNew MobjClsNotification;             // obj of clsNotification
        clsBLLSummary MobjclsBLLSummary;                    // object Of clsBLLSummary

        MessageBoxIcon MmsgMessageIcon;                     // obj of message icon// for setting error message

        DataTable datMessages;                              // having forms error messages
        DataTable m_datChartDataSource = null;    
    
        string MstrCommonMessage;
        string m_strTitleText = "";
        string m_strFilterText = "";
        string m_strGroupCondition = "";
        string m_strSearchCondition = "";
        string m_strSelectedFields = "";      

        int m_intFilterLevel = 2;
  
        bool MbViewPermission = true;
        bool MblnPrintEmailPermission = false;              // To Set View Permission For Print/Email
        bool MblnAddPermission = false;                     // To Set Add Permission
        bool MblnUpdatePermission = false;                  // To Set Update Permission
        bool MblnDeletePermission = false;                  // To Set Delete Permission

        #endregion

        #region Constructor

        public FrmSummary()
        {
            InitializeComponent();

            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            objBllPermissionSettings = new clsBLLPermissionSettings();
            MobjClsNotification = new ClsNotificationNew();
            MobjclsBLLSummary = new clsBLLSummary();
        }

        #endregion

        private void LoadMessage()
        {
            try
            {
                //For loading error messages
                datMessages = new DataTable();
                datMessages = MobjClsNotification.FillMessageArray((int)FormID.ItemIssue, ClsCommonSettings.ProductID);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadMessage() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in Load Message() " + ex.Message, 2);
            }
        }

        private void LoadCombo(int intType)
        {
            // 0 - To Load All Combos
            // 1 - To Load Company
            // 2 - To Load Warehouse
            // 3 - To Load cboItems
            // 4 - To Load cboBatch
            // 5 - To Load cboChartTypes
            // 6 - To Load cboPeriod
            try
            {
                DataTable datCombos = null;

                clsBLLPermissionSettings objBllPermissionSettings = new clsBLLPermissionSettings();
                //DataTable dtControlsPermissions = objBllPermissionSettings.GetControlsPermissions(ClsCommonSettings.RoleID, (Int32)MenuID.DeliveryNote, ClsCommonSettings.CompanyID);

                if (intType == 0 || intType == 1)
                {
                    cboWarehouse.DataSource = null;
                    cboItems.DataSource = null;
                    cboBatch.DataSource = null;                    
                    
                    datCombos = null;
                    cboCompany.SelectedIndex = cboWarehouse.SelectedIndex = cboItems.SelectedIndex = cboBatch.SelectedIndex = -1;

                    datCombos = MobjclsBLLSummary.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID="+ClsCommonSettings.LoginCompanyID });

                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;

                    if (datCombos != null && datCombos.Rows.Count > 0)
                        cboCompany.SelectedIndex = -1;
                }
                if (intType == 0 || intType == 2)
                {
                    cboItems.DataSource = null;
                    cboBatch.DataSource = null;                    
                    
                    datCombos = null;
                    cboWarehouse.SelectedIndex = cboItems.SelectedIndex = cboBatch.SelectedIndex = -1;

                    //datCombos = MobjclsBLLSummary.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", " (CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + " Or CompanyID in (Select CompanyID From CompanyMaster Where ParentID= " + Convert.ToInt32(cboCompany.SelectedValue) + ")) And IsActive=1" });
                    datCombos = MobjclsBLLSummary.FillCombos(new string[] { "Distinct InvWarehouse.WarehouseID,WarehouseName", "" +
                            "InvWarehouse INNER JOIN InvWarehouseCompanyDetails WC ON InvWarehouse.WarehouseID=WC.WarehouseID", "" +
                            "WC.CompanyID = " + cboCompany.SelectedValue.ToInt32() + " And IsActive = 1" });
                    cboWarehouse.ValueMember = "WarehouseID";
                    cboWarehouse.DisplayMember = "WarehouseName";
                    cboWarehouse.DataSource = datCombos;

                    if (datCombos != null && datCombos.Rows.Count > 0)
                        cboWarehouse.SelectedIndex = -1;
                }
                if (intType == 0 || intType == 3)
                {
                    cboBatch.DataSource = null;
                    datCombos = null;
                    cboItems.SelectedIndex = cboBatch.SelectedIndex = -1;

                    int intLstSummarySelectedIndex = -1;

                    if (LstSummary.SelectedItems.Count > 0)
                        intLstSummarySelectedIndex = LstSummary.SelectedItems[0].Index;

                    if ((cboPeriod.SelectedIndex == 2 && (intLstSummarySelectedIndex == (Int32)SummaryTypes.PI || intLstSummarySelectedIndex == (Int32)SummaryTypes.GRN || intLstSummarySelectedIndex == (Int32)SummaryTypes.POS || intLstSummarySelectedIndex == (Int32)SummaryTypes.DN || intLstSummarySelectedIndex == (Int32)SummaryTypes.TO)) || (intLstSummarySelectedIndex == (Int32)SummaryTypes.Stock))
                    {
                        if (cboWarehouse.SelectedValue != null && Convert.ToInt32(cboWarehouse.SelectedValue) > 0)
                            datCombos = MobjclsBLLSummary.FillCombos(new string[] { "Distinct IM.ItemID,ItemName", "InvItemMaster IM Inner Join InvItemStockDetails SD ON IM.ItemID = SD.ItemID",
                                                                               "SD.WarehouseID =" + Convert.ToInt32(cboWarehouse.SelectedValue) + "AND SD.CompanyID = "+ Convert.ToInt32(cboCompany.SelectedValue)});
                    }
                    else
                    {
                        if (cboCompany.SelectedValue != null && Convert.ToInt32(cboCompany.SelectedValue) > 0)
                            datCombos = MobjclsBLLSummary.FillCombos(new string[] { "Distinct IM.ItemID,ItemName", "InvItemMaster IM Inner Join InvItemStockDetails SD ON IM.ItemID = SD.ItemID",
                                                                               "SD.CompanyID =" + Convert.ToInt32(cboCompany.SelectedValue) });
                    }
                    cboItems.ValueMember = "ItemID";
                    cboItems.DisplayMember = "ItemName";
                    cboItems.DataSource = datCombos;

                    if (datCombos != null && datCombos.Rows.Count > 0)
                        cboItems.SelectedIndex = -1;
                }
                if (intType == 0 || intType == 4)
                {
                    datCombos = null;
                    cboBatch.SelectedIndex = -1;

                    DataTable dtSummary = MobjclsBLLSummary.FillCombos(new string[] { "CostingMethodID", "InvItemDetails", "ItemID = " + Convert.ToInt32(cboItems.SelectedValue) });

                    int intCostingMethodID = 0, intLstSummarySelectedIndex = -1;

                    if (LstSummary.SelectedItems.Count > 0)
                        intLstSummarySelectedIndex = LstSummary.SelectedItems[0].Index;

                    if (dtSummary.Rows.Count > 0)
                    {
                        if (dtSummary.Rows[0]["CostingMethodID"] != DBNull.Value)
                            intCostingMethodID = Convert.ToInt32(dtSummary.Rows[0]["CostingMethodID"]);
                    }

                    if (intLstSummarySelectedIndex != (Int32)SummaryTypes.PI && intLstSummarySelectedIndex != (Int32)SummaryTypes.SI)
                    {
                        if (cboWarehouse.SelectedValue != null && Convert.ToInt32(cboWarehouse.SelectedValue) > 0 && cboWarehouse.Enabled)
                            datCombos = MobjclsBLLSummary.FillCombos(new string[] { "Distinct BD.BatchID,BD.BatchNo ", "InvItemMaster IM Inner Join InvItemStockDetails SD ON IM.ItemID = SD.ItemID Inner Join InvBatchDetails BD ON BD.ItemID = SD.ItemID",
                                                                              "SD.WarehouseID =" + Convert.ToInt32(cboWarehouse.SelectedValue) + " AND SD.ItemID ="  + Convert.ToInt32(cboItems.SelectedValue) });
                        else
                            datCombos = MobjclsBLLSummary.FillCombos(new string[] { "Distinct BD.BatchID,BD.BatchNo ", "InvItemMaster IM Inner Join InvItemStockDetails SD ON IM.ItemID = SD.ItemID Inner Join InvBatchDetails BD ON BD.ItemID = SD.ItemID",
                                                                              "SD.ItemID ="  + Convert.ToInt32(cboItems.SelectedValue) });

                    }
                    cboBatch.ValueMember = "BatchID";
                    cboBatch.DisplayMember = "BatchNo";
                    cboBatch.DataSource = datCombos;

                    if (datCombos != null && datCombos.Rows.Count > 0)
                        cboBatch.SelectedIndex = -1;
                }
                if (intType == 0 || intType == 5)
                {
                    datCombos = null;
                    cboChartTypes.SelectedIndex = -1;

                    datCombos = MobjclsBLLSummary.FillCombos(new string[] { "ChartTypeID,ChartType ", "ChartTypeReference", "" });
                    cboChartTypes.ValueMember = "ChartTypeID";
                    cboChartTypes.DisplayMember = "ChartType";
                    cboChartTypes.DataSource = datCombos;
                }
                if (intType == 0 || intType == 6)
                {
                    DataTable datPeriod = new DataTable();
                    DataRow dr = null;

                    datPeriod.Columns.Add("TypeID");
                    datPeriod.Columns.Add("Description");
                    cboPeriod.SelectedIndex = -1;

                    int intLstSummarySelectedIndex = -1;

                    if (LstSummary.SelectedItems.Count > 0)
                        intLstSummarySelectedIndex = LstSummary.SelectedItems[0].Index;

                    if (intLstSummarySelectedIndex == (Int32)SummaryTypes.Stock)
                    {
                        dr = datPeriod.NewRow();
                        dr["TypeID"] = 3;
                        dr["Description"] = "Summary";
                        datPeriod.Rows.InsertAt(dr, 0);
                    }

                    dr = datPeriod.NewRow();
                    dr["TypeID"] = 2;
                    dr["Description"] = "Daily";
                    datPeriod.Rows.InsertAt(dr, 0);

                    dr = datPeriod.NewRow();
                    dr["TypeID"] = 1;
                    dr["Description"] = "Monthly";
                    datPeriod.Rows.InsertAt(dr, 0);                    
                    
                    dr = datPeriod.NewRow();
                    dr["TypeID"] = 0;
                    dr["Description"] = "Yearly";
                    datPeriod.Rows.InsertAt(dr, 0);

                    cboPeriod.ValueMember = "TypeID";
                    cboPeriod.DisplayMember = "Description";
                    cboPeriod.DataSource = datPeriod;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadCombos() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in LoadCombos() " + ex.Message, 2);
            }
        }

        private void FrmSummary_Load(object sender, EventArgs e)
        {
            SetPermissions();
            LoadCombo(0);
            cboPeriod.SelectedIndex = 0;
            dtpFrom.Value = DateTime.Now.AddMonths(-1);
            dtpTo.Value = DateTime.Now;
            BtnBack.Enabled = false;
            LstSummary.Items[0].Selected = true;
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Reports, (Int32)eMenuID.InventorySummary, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            MbViewPermission = (MblnAddPermission || MblnPrintEmailPermission || MblnUpdatePermission || MblnDeletePermission);
            //btnShow.Enabled = MbViewPermission;
        }

        private void cboCompany_SelectedValueChanged(object sender, EventArgs e)
        {
            int intLstSummarySelectedIndex = -1;

            if (LstSummary.SelectedItems.Count > 0)
                intLstSummarySelectedIndex = LstSummary.SelectedItems[0].Index;

            //if (((intLstSummarySelectedIndex != (Int32)SummaryTypes.PR) && (intLstSummarySelectedIndex != (Int32)SummaryTypes.SR) && (intLstSummarySelectedIndex != (Int32)SummaryTypes.SI) && (intLstSummarySelectedIndex != (Int32)SummaryTypes.PI)))
            if ((cboPeriod.SelectedIndex == 2 && (intLstSummarySelectedIndex == (Int32)SummaryTypes.PI || intLstSummarySelectedIndex == (Int32)SummaryTypes.GRN || intLstSummarySelectedIndex == (Int32)SummaryTypes.POS || intLstSummarySelectedIndex == (Int32)SummaryTypes.DN || intLstSummarySelectedIndex == (Int32)SummaryTypes.TO)) || (intLstSummarySelectedIndex == (Int32)SummaryTypes.Stock))
                LoadCombo(2);
            else
                LoadCombo(3);
        }

        private void cboWarehouse_SelectedValueChanged(object sender, EventArgs e)
        {
            LoadCombo(3);
        }

        private void cboItems_SelectedValueChanged(object sender, EventArgs e)
        {
            LoadCombo(4);
        }

        private void SetInitialse(int intType)
        {
            if (intType == 0)
            {
                dtpFrom.Format = dtpTo.Format = eDateTimePickerFormat.Custom;
                dtpFrom.CustomFormat = dtpTo.CustomFormat = "yyyy";
                dtpFrom.Value = Convert.ToDateTime("01 Jan " + DateTime.Now.Year.ToString());
                dtpTo.Value = Convert.ToDateTime("31 Dec " + DateTime.Now.Year.ToString());
            }
            else if (intType == 1)
            {
                dtpFrom.Format = dtpTo.Format = eDateTimePickerFormat.Custom;
                dtpFrom.CustomFormat = dtpTo.CustomFormat = "MMM yyyy";
                dtpFrom.Value = Convert.ToDateTime(DateTime.Now.Month.ToString() + " 01 " + " " + DateTime.Now.Year);
                dtpTo.Value = Convert.ToDateTime(DateTime.Now.AddMonths(1).Month.ToString() + " 01 " + " " + DateTime.Now.AddMonths(1).Year);
            }
            else
            {
                dtpFrom.Format = dtpTo.Format = eDateTimePickerFormat.Custom;
                dtpFrom.CustomFormat = dtpTo.CustomFormat = "dd MMM yyyy";
                dtpFrom.Value = DateTime.Now;
                dtpTo.Value = DateTime.Now;
            }
            if ((cboPeriod.SelectedIndex == 2 && (LstSummary.SelectedIndices.Count > 0 && LstSummary.SelectedIndices[0] == (Int32)SummaryTypes.PI || LstSummary.SelectedIndices[0] == (Int32)SummaryTypes.GRN || LstSummary.SelectedIndices[0] == (Int32)SummaryTypes.POS || LstSummary.SelectedIndices[0] == (Int32)SummaryTypes.DN || LstSummary.SelectedIndices[0] == (Int32)SummaryTypes.TO)) || (LstSummary.SelectedIndices.Count > 0 && LstSummary.SelectedIndices[0] == (Int32)SummaryTypes.Stock))
            {
                cboWarehouse.Enabled = true;
                LoadCombo(2);
            }
            else
                cboWarehouse.Enabled = false;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            if (FormValidation())
                FilterChart();
        }

        private void FillParameters()
        {
            GetSearchCondition();
            MobjclsBLLSummary.MobjclsDTOSummary.SummaryReportType = (LstSummary.SelectedItems[0].Index == 0) ? 0 : 1;
            MobjclsBLLSummary.MobjclsDTOSummary.CompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            MobjclsBLLSummary.MobjclsDTOSummary.WarehouseID = Convert.ToInt32(cboWarehouse.SelectedValue);
            MobjclsBLLSummary.MobjclsDTOSummary.SummaryType = cboPeriod.SelectedIndex;
            MobjclsBLLSummary.MobjclsDTOSummary.ParameterFromDate = dtpFrom.Value;
            MobjclsBLLSummary.MobjclsDTOSummary.ParameterToDate = dtpTo.Value;
            MobjclsBLLSummary.MobjclsDTOSummary.SelectFields = m_strSelectedFields;
            MobjclsBLLSummary.MobjclsDTOSummary.SearchCondition = m_strSearchCondition;
            MobjclsBLLSummary.MobjclsDTOSummary.GroupByCondition = m_strGroupCondition;
        }

        private void FilterChart()
        {
            FillParameters();

            m_datChartDataSource = MobjclsBLLSummary.GetStockDetails();

            //if (cboItems.SelectedValue != null && cboBatch.SelectedValue == null)
            //{
            //    m_datChartDataSource.Columns.Remove("BatchName");
            //    m_datChartDataSource.Columns.Add("BatchName");
            //}

            SetChartSettings(ChartSummary.Series[0].ChartType);
            SetColumns();

            if (m_datChartDataSource.Rows.Count > 0)
            {
                m_datChartDataSource.DefaultView.RowFilter = m_strFilterText;
                AdcSummary.DataSource = m_datChartDataSource.DefaultView.ToTable();

                m_datChartDataSource.DefaultView.RowFilter = " EntryType = 4";
                ChartSummary.DataSource = m_datChartDataSource.DefaultView.ToTable();
                ChartSummary.Show();
            }
            else
                ClearSummary();
        }

        private void ClearSummary()
        {
            AdcSummary.DataSource = null;
            AdcSummary.Nodes.Clear();
            ChartSummary.DataSource = null;
            ChartSummary.DataBind();

            expandableSplitterMiddle.Expanded = false;
        }

        private void SetColumns()
        {
            AdcSummary.Columns["Period"].Width.Absolute = 130;
            AdcSummary.Columns["Description"].Width.Absolute = 120;
            AdcSummary.Columns["Item"].Width.Absolute = 150;
            AdcSummary.Columns["Batch"].Width.Absolute = 150;

            AdcSummary.Columns["ReceivedQuantity"].Width.Absolute = 80;
            AdcSummary.Columns["InvoicedQuantity"].Width.Absolute = 80;
            AdcSummary.Columns["SoldQuantity"].Width.Absolute = 80;
            AdcSummary.Columns["DamagedQuantity"].Width.Absolute = 80;
            AdcSummary.Columns["OpeningQuantity"].Width.Absolute = 80;
            AdcSummary.Columns["TransferedQuantity"].Width.Absolute = 80;
            AdcSummary.Columns["ReceivedFromTransfer"].Width.Absolute = 120;
            AdcSummary.Columns["DemoQuantity"].Width.Absolute = 80;
            
            AdcSummary.Columns["Period"].DataFieldName = "Period";
            AdcSummary.Columns["Description"].DataFieldName = "Description";

            AdcSummary.Columns["InvoicedQuantity"].Visible = false;

            if (m_intFilterLevel == 2)
            {
                AdcSummary.Columns["Item"].Visible = true;
                AdcSummary.Columns["Batch"].Visible = false;

                BtnBack.Enabled = false;
            }
            else if (m_intFilterLevel == 1)
            {
                AdcSummary.Columns["Item"].DataFieldName = "Item";
                AdcSummary.Columns["Item"].Visible = true;
                AdcSummary.Columns["Batch"].Visible = true;

                BtnBack.Enabled = true;
            }
            else
            {
                AdcSummary.Columns["Item"].Visible = false;
                AdcSummary.Columns["Batch"].Visible = true;
                BtnBack.Enabled = true;
            }
           
            AdcSummary.Columns["Item"].DataFieldName = "Item";
            AdcSummary.Columns["Batch"].DataFieldName = "BatchName";

            if (LstSummary.SelectedItems[0].Index == (Int32)SummaryTypes.Stock)
            {
                AdcSummary.Columns["ReceivedQuantity"].Text = "Received Qty";
                AdcSummary.Columns["InvoicedQuantity"].Text = "Invoiced Qty";
                AdcSummary.Columns["SoldQuantity"].Text = "Sold Qty";
                //AdcSummary.Columns["InvoicedQuantity"].Visible = true;
                AdcSummary.Columns["SoldQuantity"].Visible = true;
                AdcSummary.Columns["DamagedQuantity"].Visible = true;
                AdcSummary.Columns["OpeningQuantity"].Visible = true;
                AdcSummary.Columns["TransferedQuantity"].Visible = true;
                AdcSummary.Columns["ReceivedFromTransfer"].Visible = true;
                AdcSummary.Columns["DemoQuantity"].Visible = false;

                AdcSummary.Columns["ReceivedQuantity"].StretchToFill = AdcSummary.Columns["SoldQuantity"].StretchToFill = false;
                AdcSummary.Columns["DemoQuantity"].StretchToFill = true;

                AdcSummary.Columns["ReceivedQuantity"].DataFieldName = "ReceivedQuantity";
                AdcSummary.Columns["InvoicedQuantity"].DataFieldName = "InvoicedQuantity";
                AdcSummary.Columns["SoldQuantity"].DataFieldName = "SoldQuantity";
                AdcSummary.Columns["DamagedQuantity"].DataFieldName = "DamagedQuantity";
                AdcSummary.Columns["OpeningQuantity"].DataFieldName = "OpeningStock";
                AdcSummary.Columns["TransferedQuantity"].DataFieldName = "TransferedQuantity";
                AdcSummary.Columns["ReceivedFromTransfer"].DataFieldName = "ReceivedFromTransfer";
                AdcSummary.Columns["DemoQuantity"].DataFieldName = "DemoQuantity";
            }
            else
            {
                AdcSummary.Columns["Period"].Width.Absolute = 130;
                AdcSummary.Columns["Description"].Width.Absolute = 100;
                AdcSummary.Columns["Item"].Width.Absolute = 200;
                AdcSummary.Columns["Batch"].Width.Absolute = 200;

                AdcSummary.Columns["ReceivedQuantity"].Width.Absolute = 140;
                AdcSummary.Columns["InvoicedQuantity"].Width.Absolute = 140;
                AdcSummary.Columns["SoldQuantity"].Width.Absolute = 140;

                AdcSummary.Columns["ReceivedQuantity"].Text = "Quantity";
                AdcSummary.Columns["InvoicedQuantity"].Text = "Discount Amount";
                AdcSummary.Columns["SoldQuantity"].Text = "Amount";
                AdcSummary.Columns["DamagedQuantity"].Visible = false;
                AdcSummary.Columns["OpeningQuantity"].Visible = false;
                AdcSummary.Columns["TransferedQuantity"].Visible = false;
                AdcSummary.Columns["ReceivedFromTransfer"].Visible = false;
                AdcSummary.Columns["DemoQuantity"].Visible = false;

                if ((LstSummary.SelectedItems[0].Index == (Int32)SummaryTypes.GRN) || (LstSummary.SelectedItems[0].Index == (Int32)SummaryTypes.DN))
                {
                    AdcSummary.Columns["InvoicedQuantity"].Visible = false;
                    AdcSummary.Columns["SoldQuantity"].Visible = false;

                    AdcSummary.Columns["DemoQuantity"].StretchToFill = AdcSummary.Columns["SoldQuantity"].StretchToFill = false;
                    AdcSummary.Columns["ReceivedQuantity"].StretchToFill = true;
                }
                else
                {
                    //AdcSummary.Columns["InvoicedQuantity"].Visible = true;
                    AdcSummary.Columns["SoldQuantity"].Visible = true;

                    AdcSummary.Columns["ReceivedQuantity"].StretchToFill = AdcSummary.Columns["DemoQuantity"].StretchToFill = false;
                    AdcSummary.Columns["SoldQuantity"].StretchToFill = true;
                }

                AdcSummary.Columns["ReceivedQuantity"].DataFieldName = "Quantity";
                AdcSummary.Columns["InvoicedQuantity"].DataFieldName = "DiscountAmount";
                AdcSummary.Columns["SoldQuantity"].DataFieldName = "Amount";

                AdcSummary.Columns["SoldQuantity"].Width.Absolute = 100;
            }
        }

        private void SetChartType()
        {
            if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Area)
                SetChartSettings(SeriesChartType.Area);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Bar)
                SetChartSettings(SeriesChartType.Bar);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Boxsplot)
                SetChartSettings(SeriesChartType.BoxPlot);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Bubble)
                SetChartSettings(SeriesChartType.Bubble);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.CandileStick)
                SetChartSettings(SeriesChartType.Candlestick);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Column)
                SetChartSettings(SeriesChartType.Column);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Doughnut)
                SetChartSettings(SeriesChartType.Doughnut);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.ErrorBar)
                SetChartSettings(SeriesChartType.ErrorBar);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.FastLine)
                SetChartSettings(SeriesChartType.FastLine);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.FastPoint)
                SetChartSettings(SeriesChartType.FastPoint);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Funnel)
                SetChartSettings(SeriesChartType.Funnel);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Line)
                SetChartSettings(SeriesChartType.Line);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Pie)
                SetChartSettings(SeriesChartType.Pie);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Point)
                SetChartSettings(SeriesChartType.Point);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Polar)
                SetChartSettings(SeriesChartType.Polar);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Pyramid)
                SetChartSettings(SeriesChartType.Pyramid);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Radar)
                SetChartSettings(SeriesChartType.Radar);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Range)
                SetChartSettings(SeriesChartType.Range);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.RangeBar)
                SetChartSettings(SeriesChartType.RangeBar);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.RangeColumn)
                SetChartSettings(SeriesChartType.RangeColumn);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Spline)
                SetChartSettings(SeriesChartType.Spline);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.SplineArea)
                SetChartSettings(SeriesChartType.SplineArea);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.StackedArea)
                SetChartSettings(SeriesChartType.StackedArea);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.StackedBar100)
                SetChartSettings(SeriesChartType.StackedArea100);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.StackedBar)
                SetChartSettings(SeriesChartType.StackedBar);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.StackedColumn)
                SetChartSettings(SeriesChartType.StackedColumn);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.StackedColumn100)
                SetChartSettings(SeriesChartType.StackedColumn100);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.StepLine)
                SetChartSettings(SeriesChartType.StepLine);
            else if (cboChartTypes.SelectedValue.ToInt32() == (Int32)sChartType.Stock)
                SetChartSettings(SeriesChartType.Stock);
        }

        private void SetChartSettings(SeriesChartType ChartType)
        {
            if (LstSummary.SelectedItems.Count < 1)
                return;

            ChartSummary.Series[0].ChartType = ChartSummary.Series[1].ChartType = ChartSummary.Series[2].ChartType = ChartType;

            if (m_intFilterLevel != 2)
                ChartSummary.Series[0].XValueMember = ChartSummary.Series[1].XValueMember = ChartSummary.Series[2].XValueMember = "ChartYValue";
            else
                ChartSummary.Series[0].XValueMember = ChartSummary.Series[1].XValueMember = ChartSummary.Series[2].XValueMember = "ChartXValue";

            if (LstSummary.SelectedItems[0].Index == (Int32)SummaryTypes.Stock)
            {
                ChartSummary.Titles[1].Text = "Stock Summary of " + m_strTitleText;

                ChartSummary.Series[0].YValueMembers = "ReceivedQuantity";
                ChartSummary.Series[1].YValueMembers = "SoldQuantity";
                ChartSummary.Series[2].YValueMembers = "DamagedQuantity";

                ChartSummary.Series[0].Name = "Received Qty";
                ChartSummary.Series[1].Name = "Sold Qty";
                ChartSummary.Series[2].Name = "Damaged Qty";
            }
            else
            {
                ChartSummary.Titles[1].Text = "Summary of " + m_strTitleText;

                ChartSummary.Series[0].YValueMembers = "Quantity";
                ChartSummary.Series[1].YValueMembers = "DiscountAmount";
                ChartSummary.Series[2].YValueMembers = "Amount";

                ChartSummary.Series[0].Name = "Quantity";
                ChartSummary.Series[1].Name = "DiscountAmount";
                ChartSummary.Series[2].Name = "Amount";
            }
        }

        private void GetSearchCondition()
        {
            if (LstSummary.SelectedItems[0].Index == (Int32)SummaryTypes.Stock)
            {
                m_strSearchCondition = " Where SS.WarehouseID = " + Convert.ToInt32(cboWarehouse.SelectedValue) + " ";
                m_strGroupCondition = " SS.ItemID ,";
            }
            else
            {
                m_strSearchCondition = " Where SS.OperationTypeID = " + Convert.ToInt32(LstSummary.SelectedItems[0].Tag) + " AND SS.CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + " ";
                m_strGroupCondition = " SS.OperationTypeID, SS.ItemID ,";
            }

            m_strSelectedFields = " SS.ItemID ,";

            if (LstSummary.SelectedItems[0].Index == (Int32)SummaryTypes.Stock)
                m_strTitleText = cboWarehouse.Text;
            else
                m_strTitleText = cboCompany.Text;

            m_intFilterLevel = 2;

            if (cboItems.SelectedValue != null)
            {
                m_intFilterLevel = 1;
                m_strSearchCondition += " AND SS.ItemID = " + Convert.ToInt32(cboItems.SelectedValue) + " ";

                if (cboPeriod.SelectedIndex == (Int32)Periods.Summary)
                    m_strSelectedFields += " MAX(SS.BatchID) ,";
                else
                {
                    m_strSelectedFields += " SS.BatchID ,";
                    m_strGroupCondition += " SS.BatchID ,";
                }
                m_strTitleText = cboItems.Text;
            }

            if (cboBatch.SelectedValue != null)
            {
                m_intFilterLevel = 0;
                m_strSearchCondition += " AND SS.BatchID = " + Convert.ToInt32(cboBatch.SelectedValue) + " ";
                m_strTitleText = cboItems.Text + " - " + cboBatch.Text;
            }
            if (cboItems.SelectedValue == null && cboBatch.SelectedValue == null)
                m_strSelectedFields += " MAX(SS.BatchID) ,";

            if (m_strGroupCondition.Length > 0)
            {
                m_strGroupCondition = " Group By " + m_strGroupCondition.Substring(0, m_strGroupCondition.Length - 1);
                m_strSelectedFields = m_strSelectedFields.Substring(0, m_strSelectedFields.Length - 1);
            }
        }

        private bool FormValidation()
        {
            Control cntrlFocus = null;
            bool blnValid = true;

            if (LstSummary.SelectedItems.Count < 1)
            {
                MstrCommonMessage = "Please Select Report Type.";
                //MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4201, out MmsgMessageIcon);
                cntrlFocus = LstSummary;
                blnValid = false;
            }
            else if (cboCompany.SelectedValue == null)
            {
                MstrCommonMessage = "Please Select Company.";
                //MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4201, out MmsgMessageIcon);
                cntrlFocus = cboCompany;
                blnValid = false;
            }
            else if (cboWarehouse.SelectedValue == null && (LstSummary.SelectedItems[0].Index == (Int32)SummaryTypes.Stock))
            {
                MstrCommonMessage = "Please Select Warehouse.";
                //MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4201, out MmsgMessageIcon);
                cntrlFocus = cboWarehouse;
                blnValid = false;
            }
            else if (cboPeriod.SelectedIndex == -1)
            {
                MstrCommonMessage = "Please Select Period.";
                //MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4201, out MmsgMessageIcon);
                cntrlFocus = cboPeriod;
                blnValid = false;
            }
            else if (cboPeriod.SelectedIndex == (Int32)Periods.Summary)
            {
                if (cboItems.SelectedValue == null)
                {
                    MstrCommonMessage = "Please Select an Item.";
                    //MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4201, out MmsgMessageIcon);
                    cntrlFocus = cboItems;
                    blnValid = false;
                }
            }
            else if (dtpFrom.Value == null || dtpFrom.Value == dtpFrom.MinDate || dtpFrom.Text == string.Empty)
            {
                MstrCommonMessage = "Please Select From Date.";
                cntrlFocus = dtpFrom;
                blnValid = false;

            }
            else if (dtpTo.Value == null || dtpTo.Value == dtpTo.MinDate || dtpTo.Text == string.Empty)
            {
                MstrCommonMessage = "Please Select To Date.";
                cntrlFocus = dtpTo;
                blnValid = false;
            }

            if (!blnValid)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ErrSummary.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                lblSalesStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrSummary.Enabled = true;
                cntrlFocus.Focus();
            }
            return blnValid;
        }

        private void BtnShowTop_Click(object sender, EventArgs e)
        {
            btnShow_Click(sender, e);
        }

        private void BtnShowChart_Click(object sender, EventArgs e)
        {
            btnShow_Click(sender, e);
        }

        private void cboChartTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetChartType();
        }

        private void TrkBarChart_Scroll(object sender, EventArgs e)
        {
            ChartSummary.ChartAreas[0].Area3DStyle.Inclination = TrkBarChart.Value;
        }

        private void ChartSummary_MouseDown(object sender, MouseEventArgs e)
        {
            HitTestResult htrResult;
            htrResult = ChartSummary.HitTest(e.X, e.Y);

            if (htrResult.ChartElementType == ChartElementType.DataPoint && htrResult.PointIndex >= 0)
            {
                DataPoint dp = ChartSummary.Series[0].Points[htrResult.PointIndex];

                DataTable Dtable = new DataTable();
                int intCurrentRowIndex = 0;
                string strTitleText = "";

                intCurrentRowIndex = htrResult.PointIndex;

                if (m_datChartDataSource.Rows.Count > 0)
                {
                    m_datChartDataSource.DefaultView.RowFilter = " EntryType = 4";

                    if (m_intFilterLevel == 2 && m_datChartDataSource.DefaultView.ToTable().Rows[intCurrentRowIndex]["ItemID"] != null && m_datChartDataSource.DefaultView.ToTable().Rows[intCurrentRowIndex]["ItemID"].ToInt32() != 0)
                    {
                        strTitleText = m_datChartDataSource.DefaultView.ToTable().Rows[intCurrentRowIndex]["Item"].ToString();
                        LoadCombo(3);
                        cboItems.SelectedValue = m_datChartDataSource.DefaultView.ToTable().Rows[intCurrentRowIndex]["ItemID"].ToInt32();
                    }
                    if (m_intFilterLevel == 1 && m_datChartDataSource.DefaultView.ToTable().Rows[intCurrentRowIndex]["BatchID"] != null && m_datChartDataSource.DefaultView.ToTable().Rows[intCurrentRowIndex]["BatchID"].ToInt32() != 0)
                    {
                        LoadCombo(3);
                        cboItems.SelectedValue = m_datChartDataSource.DefaultView.ToTable().Rows[intCurrentRowIndex]["ItemID"].ToInt32();
                        LoadCombo(4);
                        cboBatch.SelectedValue = m_datChartDataSource.DefaultView.ToTable().Rows[intCurrentRowIndex]["BatchID"].ToInt32();
                        strTitleText = m_datChartDataSource.DefaultView.ToTable().Rows[intCurrentRowIndex]["BatchName"].ToString();
                    }
                    if (m_intFilterLevel != 0)
                    {
                        m_strTitleText = strTitleText;
                        FilterChart();
                    }
                }
            }
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            string strTitleText = "";

            if (m_datChartDataSource.Rows.Count > 0)
            {
                if (m_intFilterLevel == 0)
                {
                    cboBatch.SelectedValue = -1;
                    strTitleText = m_datChartDataSource.Rows[0]["Item"].ToString();
                }
                else if (m_intFilterLevel == 1)
                {
                    cboItems.SelectedValue = cboBatch.SelectedValue = -1;
                    strTitleText = cboWarehouse.Text;
                }

                m_strTitleText = strTitleText;
                FilterChart();
            }
        }

        private void cboPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetInitialse(cboPeriod.SelectedIndex);
        }

        private void LstSummary_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (LstSummary.SelectedItems.Count < 1)
                return;

            Exppanelleft.Text = ExpPanelright.TitleText = ChartSummary.Titles[0].Text = ChartSummary.Titles[1].Text = LstSummary.SelectedItems[0].Text + " Summary";

            ClearSummary();
            LoadCombo(0);
            SetColumns();

            int intLstSummarySelectedIndex = -1;

            if (LstSummary.SelectedItems.Count > 0)
                intLstSummarySelectedIndex = LstSummary.SelectedItems[0].Index;

            cboWarehouse.Enabled = cboBatch.Enabled = true;

            if  (intLstSummarySelectedIndex == (Int32)SummaryTypes.PI)   
                cboBatch.Enabled = false;
            else if ((intLstSummarySelectedIndex == (Int32)SummaryTypes.PR)|| (intLstSummarySelectedIndex == (Int32)SummaryTypes.SR))
                cboWarehouse.Enabled = false;
            else if (intLstSummarySelectedIndex == (Int32)SummaryTypes.SI)
                cboWarehouse.Enabled = cboBatch.Enabled = false;

            if ((cboPeriod.SelectedIndex == 2 && (LstSummary.SelectedIndices.Count > 0 && LstSummary.SelectedIndices[0] == (Int32)SummaryTypes.PI || LstSummary.SelectedIndices[0] == (Int32)SummaryTypes.GRN || LstSummary.SelectedIndices[0] == (Int32)SummaryTypes.POS || LstSummary.SelectedIndices[0] == (Int32)SummaryTypes.DN || LstSummary.SelectedIndices[0] == (Int32)SummaryTypes.TO)) || (LstSummary.SelectedIndices.Count > 0 && LstSummary.SelectedIndices[0] == (Int32)SummaryTypes.Stock))
            {
                cboWarehouse.Enabled = true;
            }
            else
                cboWarehouse.Enabled = false;

        }


        private void Changestatus(object sender, EventArgs e)
        {
            ErrSummary.Clear();
        }

        private void Combo_Keydown(object sender, KeyEventArgs e)
        {
            ComboBox cbo = new ComboBox();
            cbo = (ComboBox)sender;

            cbo.DroppedDown = false;
        }
    }
}