﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



namespace MyBooksERP
{
    public partial class FrmOpeningStock : DevComponents.DotNetBar.Office2007Form
    {
        bool MblnShowErrorMess = false;             // Checking whether error messages are showing or not
        int MintRecordCount, MintRecordPosition;    // for current record position and total record count
        string MstrCommonMessage;                   // for setting error message
        DataTable datMessages;                      // having forms error messages
        MessageBoxIcon MmsgMessageIcon;             // obj of message icon
        ClsNotificationNew MobjClsNotification;     //obj of clsNotification
        ClsLogWriter MobjClsLogWriter;              // obj of log writer
        private bool MblnIsReceipt;
        private bool MblnIsFromOK;
        clsBLLOpeningStock MobjClsBllOpeningStock = new clsBLLOpeningStock();

        bool blnIsEditMode = false;
        bool MblnPrintEmailPermission = false;
        bool MblnAddPermission = false;
        bool MblnUpdatePermission = false;
        bool MblnDeletePermission = false;
        private int MintopeningStockID = 0;
        private int MintCompanyID = 0;
        private int MintCurrencyScale = 0;
        private clsBLLCommonUtility MobjCommonUtility = new clsBLLCommonUtility();

        public FrmOpeningStock()
        {
            InitializeComponent();
            MobjClsBllOpeningStock = new clsBLLOpeningStock();
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotificationNew();
            tmrOpeningStock.Interval = ClsCommonSettings.TimerInterval;
            MblnShowErrorMess = true;
        }
        private void ResetForm()
        {
            // Reset All Controls
            try
            {
                //dtpDate.MaxDate = ClsCommonSettings.GetServerDate().AddHours(1);
                //dtpDate.Value = ClsCommonSettings.GetServerDate();
                DataTable datDate = MobjClsBllOpeningStock.FillCombos(new string[] { "BookStartDate", "CompanyMaster", "CompanyID= ( select CompanyID from InvWarehouse where WarehouseID= " + cboWarehouse.SelectedValue.ToInt32() + " )" });
                if (datDate.Rows.Count > 0)
                {
                    dtpDate.Value = datDate.Rows[0]["BookStartDate"].ToDateTime();
                }
                else
                {
                    //dtpDate.MaxDate = ClsCommonSettings.GetServerDate().AddHours(1);
                    dtpDate.Value = ClsCommonSettings.GetServerDate();
                }
                LoadCombo(1,0);
                cboWarehouse.SelectedIndex = -1;
                MintCompanyID = 0;
                MintCurrencyScale = 2;
                dgvOpeningStockDetails.Rows.Clear();

                MobjClsBllOpeningStock = new clsBLLOpeningStock();
                //   MobjClsBllOpeningStock.PobjClsDTOOpeningStock.intCompanyID = ClsCommonSettings.CompanyID;

                MintRecordCount = MobjClsBllOpeningStock.GetRecordCount();
                MintRecordPosition = MintRecordCount + 1;
                bnCountItem.Text = " of " + (MintRecordCount + 1).ToString();
                bnPositionItem.Text = (MintRecordCount + 1).ToString();

                btnOk.Enabled = false;
                btnSave.Enabled = false;
                bnAddNewItem.Enabled = false;
                bnDeleteItem.Enabled = false;
                bnEmail.Enabled = false;
                bnPrint.Enabled = false;
                bnSaveItem.Enabled = false;

                lblStatus.Text = "Add Opening Stock";
                tmrOpeningStock.Enabled = true;
                ErrOpeningStock.Clear();
                MblnIsFromOK = false;
                cboWarehouse.Enabled = true;
                SetBindingNavigatorButtons();
                CalculateTotal();
                cboWarehouse.Focus();
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on ResetForm() " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on ResetForm() " + Ex.Message.ToString());
            }
        }
        private void LoadCombo(int intType, int intReferenceID)
        {
            // 1 - Load Warehouse
            // 2 - Load UOM ComboBoxColumn
            DataTable datCombos = null;
            if (intType == 1)
            {
                datCombos = MobjClsBllOpeningStock.FillCombos(new string[] { "Distinct InvWarehouse.WarehouseID,WarehouseName", "InvWarehouse INNER JOIN InvWarehouseCompanyDetails WC ON InvWarehouse.WarehouseID=WC.WarehouseID", "(IsActive=1 OR InvWarehouse.WarehouseID = " + intReferenceID + ") AND WC.CompanyID=" + ClsCommonSettings.LoginCompanyID });
                cboWarehouse.ValueMember = "WarehouseID";
                cboWarehouse.DisplayMember = "WarehouseName";
                cboWarehouse.DataSource = datCombos;
            }
            else if (intType == 2)
            {
                UOM.DataSource = null;
                datCombos = MobjClsBllOpeningStock.GetItemUOMs(intReferenceID);
                UOM.DisplayMember = "ShortName";
                UOM.ValueMember = "UOMID";
                UOM.DataSource = datCombos;
            }
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.OpeningStock, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void FrmOpeningStockEntry_Load(object sender, EventArgs e)
        {
            try
            {
                LoadCombo(1, 0);
                LoadMessage();
                SetPermissions();
                ResetForm();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in FrmOpeningStockEntry_Load() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in FrmOpeningStockEntry_Load() " + ex.Message, 2);
            }
        }
        private void LoadMessage()
        {
            //Loads error messages
            datMessages = new DataTable();
            datMessages = MobjClsNotification.FillMessageArray((int)FormID.OpeningStock, ClsCommonSettings.ProductID);
        }
        private void DisplayOpeningStockInfo()
        {
            //Displaying Opening Stock Info
            MobjClsBllOpeningStock.PobjClsDTOOpeningStock.intRowNumber = MintRecordPosition;

            if (MobjClsBllOpeningStock.GetOpeningStockInfo())
            {
                MintopeningStockID = Convert.ToInt32(MobjClsBllOpeningStock.PobjClsDTOOpeningStock.intOpeningStockID); //Get opening stockID for print               

                LoadCombo(1, MobjClsBllOpeningStock.PobjClsDTOOpeningStock.intWarehouseID);
                
                cboWarehouse.SelectedValue = MobjClsBllOpeningStock.PobjClsDTOOpeningStock.intWarehouseID;
                dtpDate.Value = Convert.ToDateTime(MobjClsBllOpeningStock.PobjClsDTOOpeningStock.strOpeningDate);
                bnPositionItem.Text = MobjClsBllOpeningStock.PobjClsDTOOpeningStock.intRowNumber.ToString();
                bnCountItem.Text = " of " + MintRecordCount.ToString();

                DataTable dtOpeningStockDetails = MobjClsBllOpeningStock.GetOpeningStockDetails();
                dgvOpeningStockDetails.Rows.Clear();
                for (int iRowCount = 0; iRowCount < dtOpeningStockDetails.Rows.Count; iRowCount++)
                {
                    DataRow dr = dtOpeningStockDetails.Rows[iRowCount];
                    dgvOpeningStockDetails.Rows.Add();
                    dgvOpeningStockDetails["ItemCode", iRowCount].Value = dr["ItemCode"];
                    dgvOpeningStockDetails["ItemName", iRowCount].Value = dr["ItemName"];
                    dgvOpeningStockDetails["ItemID", iRowCount].Value = dr["ItemID"];
                    dgvOpeningStockDetails["ItemStatusID", iRowCount].Value = dr["ItemStatusID"];
                    dgvOpeningStockDetails["BatchID", iRowCount].Value = dr["BatchID"];
                    dgvOpeningStockDetails["BatchNo", iRowCount].Value = dr["BatchNo"];
                    
                    LoadCombo(2, Convert.ToInt32(dr["ItemID"]));
                    dgvOpeningStockDetails["UOM", iRowCount].Value = Convert.ToInt32(dr["UOMID"]);
                    dgvOpeningStockDetails["UOM", iRowCount].Tag = Convert.ToInt32(dr["UOMID"]);
                    dgvOpeningStockDetails["UOM", iRowCount].Value = dgvOpeningStockDetails["UOM", iRowCount].FormattedValue;

                    int intUomScale = 0;

                    if (dgvOpeningStockDetails[UOM.Index, iRowCount].Tag.ToInt32() != 0)
                        intUomScale = MobjClsBllOpeningStock.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvOpeningStockDetails[UOM.Index, iRowCount].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                    dgvOpeningStockDetails["Quantity", iRowCount].Value = dr["Quantity"].ToDecimal().ToString("F" + intUomScale);
                    dgvOpeningStockDetails["ExpiryDate", iRowCount].Value = dr["ExpiryDate"];
                    dgvOpeningStockDetails["Rate", iRowCount].Value = dr["Rate"];
                }

                //bnSaveItem.Enabled = MblnUpdatePermission;
                //btnOk.Enabled = MblnUpdatePermission;
                //btnSave.Enabled = MblnUpdatePermission;

                bnAddNewItem.Enabled = MblnAddPermission;
                bnDeleteItem.Enabled = MblnDeletePermission;
                bnEmail.Enabled = MblnPrintEmailPermission;
                bnPrint.Enabled = MblnPrintEmailPermission;

                cboWarehouse.Enabled = false;
                dtpDate.Focus();
                btnOk.Enabled = false;
                btnSave.Enabled = false;
                bnSaveItem.Enabled = false;
            }
        }
        private void bnMoveFirstItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition > 1)
                {
                    MintRecordPosition = 1;
                    DisplayOpeningStockInfo();
                    lblStatus.Text = MobjClsNotification.GetErrorMessage(datMessages, 9);
                    tmrOpeningStock.Enabled = true;
                    SetBindingNavigatorButtons();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on BindingNavigator Move first item click()" + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on BindingNavigator Move first item click " + Ex.Message.ToString());
            }
        }

        private void bnMovePreviousItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition > 1)
                {
                    MintRecordPosition--;
                    DisplayOpeningStockInfo();
                    //MintopeningStockID = MobjClsBllOpeningStock
                    lblStatus.Text = MobjClsNotification.GetErrorMessage(datMessages, 10);
                    tmrOpeningStock.Enabled = true;
                    SetBindingNavigatorButtons();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on BindingNavigator Move Previous item click() " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on BindingNavigator Move Previous item click() " + Ex.Message.ToString());
            }
        }

        private void bnMoveNextItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition < MintRecordCount)
                {
                    MintRecordPosition++;
                    DisplayOpeningStockInfo();

                    lblStatus.Text = MobjClsNotification.GetErrorMessage(datMessages, 11);
                    tmrOpeningStock.Enabled = true;
                    SetBindingNavigatorButtons();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on BindingNavigator move next item click() " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on BindingNavigator move next item click() " + Ex.Message.ToString());
            }
        }

        private void bnMoveLastItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition <= MintRecordCount + 1)
                {
                    MintRecordPosition = MintRecordCount;
                    DisplayOpeningStockInfo();

                    lblStatus.Text = MobjClsNotification.GetErrorMessage(datMessages, 12);
                    tmrOpeningStock.Enabled = true;
                    SetBindingNavigatorButtons();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on Binding navigator move last item click() " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on Binding Navigator move last item click() " + Ex.Message.ToString());
            }
        }

        private void SetBindingNavigatorButtons()
        {
            bnMoveNextItem.Enabled = bnMoveFirstItem.Enabled = bnMoveLastItem.Enabled = true;
            bnMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(bnPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCount;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                bnMoveNextItem.Enabled = bnMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                bnMoveFirstItem.Enabled = bnMovePreviousItem.Enabled = false;
            }
        }


        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            ResetForm();
        }
        private void FillParameters()
        {

            MobjClsBllOpeningStock.PobjClsDTOOpeningStock.intWarehouseID = Convert.ToInt32(cboWarehouse.SelectedValue);
            DataTable datWarehouse = MobjClsBllOpeningStock.FillCombos(new string[] { "CompanyID", "InvWarehouse", "WarehouseID = " + MobjClsBllOpeningStock.PobjClsDTOOpeningStock.intWarehouseID });
            MobjClsBllOpeningStock.PobjClsDTOOpeningStock.intCompanyID = ClsCommonSettings.LoginCompanyID;
            MobjClsBllOpeningStock.PobjClsDTOOpeningStock.strOpeningDate = dtpDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBllOpeningStock.PobjClsDTOOpeningStock.strCreatedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
            MobjClsBllOpeningStock.PobjClsDTOOpeningStock.strModifiedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
            MobjClsBllOpeningStock.PobjClsDTOOpeningStock.intCreatedBy = ClsCommonSettings.UserID;
            MobjClsBllOpeningStock.PobjClsDTOOpeningStock.intModifiedBy = ClsCommonSettings.UserID;
            MobjClsBllOpeningStock.PobjClsDTOOpeningStock.intOperationTypeID = (int)OperationType.OpeningStock;
             
            MobjClsBllOpeningStock.PobjClsDTOOpeningStock.lstOpeningStockDetails = new List<clsDTOOpeningStockDetails>();
            foreach (DataGridViewRow dr in dgvOpeningStockDetails.Rows)
            {
                if (dr.Cells["ItemID"].Value != null)
                {
                    clsDTOOpeningStockDetails objClsDTOOpeningStockDetails = new clsDTOOpeningStockDetails();
                    objClsDTOOpeningStockDetails.intItemID = Convert.ToInt32(dr.Cells["ItemID"].Value);
                    objClsDTOOpeningStockDetails.strBatchNumber = Convert.ToString(dr.Cells["BatchNo"].Value);
                    objClsDTOOpeningStockDetails.decRate = Convert.ToDecimal(dr.Cells["Rate"].Value);
                    objClsDTOOpeningStockDetails.decQuantity = dr.Cells["Quantity"].Value.ToDecimal();
                    objClsDTOOpeningStockDetails.intUOMID = Convert.ToInt32(dr.Cells["UOM"].Tag);
                    if (dr.Cells["ExpiryDate"].Value != null && dr.Cells["ExpiryDate"].Value != DBNull.Value)
                        objClsDTOOpeningStockDetails.strExpiryDate = Convert.ToDateTime(dr.Cells["ExpiryDate"].Value).ToString("dd-MMM-yyyy");
                    MobjClsBllOpeningStock.PobjClsDTOOpeningStock.lstOpeningStockDetails.Add(objClsDTOOpeningStockDetails);
                }
            }
        }

        private void bnSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveFunction())
                {
                    ResetForm();
                }
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on Save:Save() " + this.Name + " " + ex.Message.ToString(), 3);

                if (MblnShowErrorMess)
                    MessageBox.Show("Error on Save() " + ex.Message.ToString());
            }
        }
        private bool SaveFunction()
        {
            if (ValidateFields())
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (MobjClsBllOpeningStock.PobjClsDTOOpeningStock.intOpeningStockID == 0 ? 1 : 3), out MmsgMessageIcon);

              
                FillParameters();
                bool blnIsLocation = false;
                bool blnIsSalesDone = false;
                string strInValidItem = MobjClsBllOpeningStock.StockValidation(false,out blnIsLocation,out blnIsSalesDone);
                if (!string.IsNullOrEmpty(strInValidItem))
                {
                    if (blnIsLocation)
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4314, out MmsgMessageIcon);
                    else if (blnIsSalesDone)
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 9001, out MmsgMessageIcon);
                    else
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4307, out MmsgMessageIcon);
                    MstrCommonMessage = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                    MstrCommonMessage = MstrCommonMessage.Replace("*", strInValidItem);
                    MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    lblStatus.Text = MstrCommonMessage;
                    tmrOpeningStock.Enabled = true;
                    return false;
                }

                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                  MessageBoxIcon.Information) == DialogResult.No)
                    return false;

                if (MobjClsBllOpeningStock.SaveOpeningStockInfo())
                {
                    //FrmBarCode objBarcode = new FrmBarCode();
                    //foreach (clsDTOOpeningStockDetails objOpeningStockDetails in MobjClsBllOpeningStock.PobjClsDTOOpeningStock.lstOpeningStockDetails)
                    //{
                    //    objBarcode.CreateBarCode(0, objOpeningStockDetails.intItemID, objOpeningStockDetails.intBatchID, objOpeningStockDetails.strBatchNumber);
                    //}
                    return true;
                }
            }
            return false;
        }
        private void bnDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsStockAdjustmentDone())
                    return ;
              
                FillParameters();
                bool blnIsLocation = false;
                bool blnIsSalesDone = false;
                string strInValidItem = MobjClsBllOpeningStock.StockValidation(true, out blnIsLocation, out blnIsSalesDone);
                if (!string.IsNullOrEmpty(strInValidItem))
                {
                    if (blnIsLocation)
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4314, out MmsgMessageIcon);
                    else if (blnIsSalesDone)
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 9001, out MmsgMessageIcon);
                    else
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4307, out MmsgMessageIcon);
                    MstrCommonMessage = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                    MstrCommonMessage = MstrCommonMessage.Replace("*", strInValidItem);
                    MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    return;
                }

                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4268, out MmsgMessageIcon);
                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    return;
                MobjClsBllOpeningStock.DeleteOpeningStockInfo();
                ResetForm();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnDeleteItem_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in bnDeleteItem_Click() " + ex.Message, 2);
            }
        }

        private void dgvOpeningStockDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                ChangeStatus(null, null);
                if (cboWarehouse.SelectedIndex==-1)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4301, out MmsgMessageIcon);
                    ErrOpeningStock.SetError(cboWarehouse, MstrCommonMessage.Replace("#", "").Trim());
                    lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                    tmrOpeningStock.Enabled = true;
                    cboWarehouse.Focus();
                e.Cancel = true;
                 }
                int iRowIndex = 0;
                //----------------------------------------------------------------------------------------------------------------
                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {

                    dgvOpeningStockDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    iRowIndex = dgvOpeningStockDetails.CurrentCell.RowIndex;
                    if (dgvOpeningStockDetails.Columns[e.ColumnIndex].Name != "ItemCode" && dgvOpeningStockDetails.Columns[e.ColumnIndex].Name != "ItemName")
                    {
                        if (Convert.ToString(dgvOpeningStockDetails.CurrentRow.Cells["ItemID"].Value).Trim() == "")
                        {
                            e.Cancel = true;
                        }
                    }

                    //------------------------------------------------------------UOM----------------------------------------------------------

                    if (dgvOpeningStockDetails.Columns[e.ColumnIndex].Name == "UOM")
                    {
                        if (dgvOpeningStockDetails.CurrentRow.Cells["ItemID"].Value != null)
                        {
                            LoadCombo(2, Convert.ToInt32(dgvOpeningStockDetails.CurrentRow.Cells["ItemID"].Value));
                        }
                        else
                        {
                            UOM.DataSource = null;
                        }

                    }

                }
                if (e.ColumnIndex == BatchNo.Index || e.ColumnIndex == ExpiryDate.Index)
                {
                    if (dgvOpeningStockDetails.CurrentRow.Cells["ItemID"].Value != null)
                    {
                        if (MobjClsBllOpeningStock.GetCostingMethod(dgvOpeningStockDetails.CurrentRow.Cells["ItemID"].Value.ToInt32()) != (int)CostingMethodReference.Batch)
                        {
                            e.Cancel = true;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvOpeningStockDetails_CellBeginEdit() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvOpeningStockDetails_CellBeginEdit() " + ex.Message, 2);
            }
        }

        private void dgvOpeningStockDetails_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == 0) || (e.ColumnIndex == 1))
            {
                dgvOpeningStockDetails.PiColumnIndex = e.ColumnIndex;
            }
        }

        private void dgvOpeningStockDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >=0)
            {

                if (e.ColumnIndex == Quantity.Index || e.ColumnIndex == Rate.Index)
                {

                    decimal decValue = 0;
                    try
                    {
                        decValue = Convert.ToDecimal(dgvOpeningStockDetails[e.ColumnIndex, e.RowIndex].Value);
                    }
                    catch
                    {
                        dgvOpeningStockDetails[e.ColumnIndex, e.RowIndex].Value = 0;
                    }
                    CalculateTotal();
                }
                if (dgvOpeningStockDetails.Columns[e.ColumnIndex].Name == "ItemID" && e.RowIndex >= 0)
                {
                    if (dgvOpeningStockDetails.Rows[e.RowIndex].Cells["ItemID"].Value != null)
                    {
                        dgvOpeningStockDetails[Rate.Index, e.RowIndex].Value = 0;
                        dgvOpeningStockDetails[Quantity.Index, e.RowIndex].Value = 0;
                        int iItemID = Convert.ToInt32(dgvOpeningStockDetails.Rows[e.RowIndex].Cells["ItemID"].Value);
                        int tag = -1;
                        //if (dgvOpeningStockDetails.CurrentRow.Cells["UOM"].Value != null)
                        //{
                        //    tag = dgvOpeningStockDetails.CurrentRow.Cells["UOM"].Tag.ToInt32();
                        //}
                        LoadCombo(2, iItemID);

                        if (tag == -1)
                        {
                            DataTable datDefaultUom = MobjClsBllOpeningStock.FillCombos(new string[] { "BaseUomID as UomID", "InvItemMaster", "ItemID = " + iItemID + "" });
                            tag = Convert.ToInt32(datDefaultUom.Rows[0]["UomID"]);
                        }
                        dgvOpeningStockDetails.Rows[e.RowIndex].Cells["Uom"].Value = tag;
                        dgvOpeningStockDetails.Rows[e.RowIndex].Cells["Uom"].Tag = tag;
                        dgvOpeningStockDetails.Rows[e.RowIndex].Cells["Uom"].Value = dgvOpeningStockDetails.Rows[e.RowIndex].Cells["Uom"].FormattedValue;
                    }
                    else
                    {
                        UOM.DataSource = null;
                    }
                }
            }
        }

        private void dgvOpeningStockDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvOpeningStockDetails.IsCurrentCellDirty)
            {
                if (dgvOpeningStockDetails.CurrentCell != null)
                    dgvOpeningStockDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
        }

        private void dgvOpeningStockDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvOpeningStockDetails.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvOpeningStockDetails.CurrentCell.OwningColumn.Name == "Quantity" || dgvOpeningStockDetails.CurrentCell.OwningColumn.Name == "Rate")
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (dgvOpeningStockDetails.EditingControl != null && !string.IsNullOrEmpty(dgvOpeningStockDetails.EditingControl.Text) && dgvOpeningStockDetails.EditingControl.Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true;
                }
            }
            else if (dgvOpeningStockDetails.CurrentCell.OwningColumn.Name == "BatchNo" || dgvOpeningStockDetails.CurrentCell.OwningColumn.Name == "ItemCode" || dgvOpeningStockDetails.CurrentCell.OwningColumn.Name == "ItemName")
            {
                if (e.KeyChar.ToString() == "'")
                    e.Handled = true;
            }
        }
        private void ChangeStatus(object sender, EventArgs e)
        {
            // changing function controls status
            if (blnIsEditMode)
            {
                btnSave.Enabled = MblnUpdatePermission;
                btnOk.Enabled = MblnUpdatePermission;
                bnSaveItem.Enabled = MblnUpdatePermission;
                bnAddNewItem.Enabled = MblnAddPermission;

            }
            else
            {
                btnSave.Enabled = MblnAddPermission;
                btnOk.Enabled = MblnAddPermission;
                bnSaveItem.Enabled = MblnAddPermission;
            }
        }
        private void dgvOpeningStockDetails_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dgvOpeningStockDetails.PServerName = ClsCommonSettings.ServerName;
                string[] First = { "ItemCode", "ItemName", "ItemID","ItemStatusID" };//first grid 
                string[] second = { "ItemCode", "Description", "ItemID","ItemStatusID" };//inner grid
                dgvOpeningStockDetails.aryFirstGridParam = First;
                dgvOpeningStockDetails.arySecondGridParam = second;
                dgvOpeningStockDetails.PiFocusIndex = Quantity.Index;
                dgvOpeningStockDetails.iGridWidth = 350;
                dgvOpeningStockDetails.bBothScrollBar = true;
                dgvOpeningStockDetails.ColumnsToHide  = new string[]{"ItemID","ItemStatusID"};
                if (dgvOpeningStockDetails.CurrentCell.ColumnIndex == 0)
                {
                    dgvOpeningStockDetails.field = "Code";

                }
                else if (dgvOpeningStockDetails.CurrentCell.ColumnIndex == 1)
                {
                    dgvOpeningStockDetails.field = "ItemName";
                }

                //int intParentID = MobjClsBllOpeningStock.FillCombos(new string[] { "ParentID", "CompanyMaster", "CompanyID = " + MintCompanyID }).Rows[0]["ParentID"].ToInt32();
                //DataTable datCompany = new DataTable();
                //if (intParentID == 0)
                //    datCompany = MobjClsBllOpeningStock.FillCombos(new string[] { "CompanyID", "CompanyMaster", "(CompanyID = " + MintCompanyID  + " Or ParentID = " + MintCompanyID + ")" });
                //else
                //    datCompany = MobjClsBllOpeningStock.FillCombos(new string[] { "CompanyID", "CompanyMaster", "(CompanyID = " + intParentID + " Or ParentID = " + intParentID + ")" });
                //if (datCompany.Rows.Count > 0)
                //{
                //    DataRow dr = datCompany.NewRow();
                //    dr["CompanyID"] = 0;
                //    datCompany.Rows.InsertAt(dr, 0);
                //}
                dgvOpeningStockDetails.PsQuery = "select IM.[Code] as ItemCode,IM.[ItemName] as Description,IM.[ItemID] as ItemID,IM.StatusID  as ItemStatusID from InvItemMaster IM inner join  InvItemDetails ITD ON IM.ItemID=ITD.ItemID where upper(IM." + dgvOpeningStockDetails.field + ") like upper( '" + ((dgvOpeningStockDetails.TextBoxText)) + "%')";
                dgvOpeningStockDetails.PsQuery += " And IsNull(CompanyID,0) In (0,"+MintCompanyID+") and ITD.ProductTypeID=1 Order By "+dgvOpeningStockDetails.field+"";
                //foreach (DataRow dr in datCompany.Rows)
                //    dgvOpeningStockDetails.PsQuery += dr["CompanyID"].ToInt32() + ",";
                //dgvOpeningStockDetails.PsQuery = dgvOpeningStockDetails.PsQuery.Remove(dgvOpeningStockDetails.PsQuery.Length - 1);
                //dgvOpeningStockDetails.PsQuery += ")";
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvOpeningStockDetails_Textbox_TextChanged() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvOpeningStockDetails_Textbox_TextChanged() " + ex.Message, 2);
            }
        }

        private void dgvOpeningStockDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    //------------------------------------------------------------UOM----------------------------------------------------------

                    if (dgvOpeningStockDetails.Columns[e.ColumnIndex].Name == "UOM")
                    {

                        dgvOpeningStockDetails.CurrentRow.Cells["UOM"].Tag = dgvOpeningStockDetails.CurrentRow.Cells["UOM"].Value;
                        dgvOpeningStockDetails.CurrentRow.Cells["UOM"].Value = dgvOpeningStockDetails.CurrentRow.Cells["UOM"].FormattedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvOpeningStockDetails_CellEndEdit() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvOpeningStockDetails_CellEndEdit() " + ex.Message, 2);
            }
        }

        private void btnWarehouse_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmWarehouse objWarehouse = new FrmWarehouse())
                {
                    objWarehouse.PintWareHouse = Convert.ToInt32(cboWarehouse.SelectedValue);
                    objWarehouse.ShowDialog();
                }
                if (MobjClsBllOpeningStock.PobjClsDTOOpeningStock.intOpeningStockID == 0)
                {
                    int intComboID = cboWarehouse.SelectedValue.ToInt32();
                    LoadCombo(1, 0);
                    cboWarehouse.SelectedValue = intComboID;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnWarehouse_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in btnWarehouse_Click() " + ex.Message, 2);
            }
        }

        private bool ValidateFields()
        {
            // Validating datas
            if (dgvOpeningStockDetails.CurrentCell != null)
                dgvOpeningStockDetails.CurrentCell = dgvOpeningStockDetails["ItemCode", dgvOpeningStockDetails.CurrentRow.Index];

            bool blnValid = true;
            Control cntrlFocus = null;
            ErrOpeningStock.Clear();

            if (cboWarehouse.SelectedValue == null)
            {
                //Please select Warehouse
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4301, out MmsgMessageIcon);
                cntrlFocus = cboWarehouse;
                blnValid = false;
            }

            if (dtpDate.Value.Date > ClsCommonSettings.GetServerDate().Date)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4312, out MmsgMessageIcon);
                cntrlFocus = dtpDate;
                blnValid = false;
            }
            if (!blnValid)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                ErrOpeningStock.SetError(cntrlFocus, MstrCommonMessage.Replace("#", "").Trim());
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrOpeningStock.Enabled = true;
                cntrlFocus.Focus();
            }
            else if (IsStockAdjustmentDone())
            {
                blnValid = false;
            }
            else if (!ValidateOpeningStockGrid())
            {
                blnValid = false;
            }
            return blnValid;
        }

        private bool IsStockAdjustmentDone()
        {
            DataTable datDetails = new DataTable();
            datDetails = MobjClsBllOpeningStock.GetOpeningStockDetails();
                foreach (DataRow dr in datDetails.Rows)
                {
                    if (MobjClsBllOpeningStock.IsStockAdjustmentDone(dr["ItemID"].ToInt32(), dr["BatchID"].ToInt64(),cboWarehouse.SelectedValue.ToInt32()))
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4313, out MmsgMessageIcon);
                        MessageBox.Show(MstrCommonMessage.Remove(MstrCommonMessage.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        tmrOpeningStock.Enabled = true;
                        dgvOpeningStockDetails.Focus();
                        dgvOpeningStockDetails.CurrentCell = dgvOpeningStockDetails["ItemCode", 0];
                        return true;
                    }
                }
            return false;
        }

        private bool ValidateOpeningStockGrid()
        {
            bool blnIsValid = true;
            int intItemCount = 0, intRowIndex = 0;
            string strColumnName = "";
            DataTable dtOldItemIssueDetails = new DataTable();

            for (int i = 0; i < dgvOpeningStockDetails.Rows.Count; i++)
            {
                if (dgvOpeningStockDetails["ItemID", i].Value != null)
                {
                    Int32 intCostingMethod = MobjClsBllOpeningStock.GetCostingMethod(dgvOpeningStockDetails.Rows[i].Cells["ItemID"].Value.ToInt32());
                    if (dgvOpeningStockDetails["BatchNo", i].Value == null && intCostingMethod==(int)CostingMethodReference.Batch)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4303, out MmsgMessageIcon);
                        intRowIndex = i;
                        strColumnName = "BatchNo";
                        blnIsValid = false;
                    }
                    else if (intCostingMethod==(int)CostingMethodReference.Batch && !IsValidBatch(Convert.ToString(dgvOpeningStockDetails["BatchNo", i].Value), Convert.ToInt64(dgvOpeningStockDetails["ItemID", i].Value)))
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4309, out MmsgMessageIcon);
                        intRowIndex = i;
                        strColumnName = "BatchNo";
                        blnIsValid = false;
                    }
                    else if (dgvOpeningStockDetails["Quantity", i].Value == null || Convert.ToDecimal(dgvOpeningStockDetails["Quantity", i].Value) == 0)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4304, out MmsgMessageIcon);
                        intRowIndex = i;
                        strColumnName = "Quantity";
                        blnIsValid = false;
                    }
                    else if (dgvOpeningStockDetails["ItemStatusID",i].Value.ToInt32() == (int)OperationStatusType.ProductActive && (dgvOpeningStockDetails["Rate", i].Value == null || dgvOpeningStockDetails["Rate", i].Value.ToDecimal() == 0))
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4305, out MmsgMessageIcon);
                        intRowIndex = i;
                        strColumnName = "Rate";
                        blnIsValid = false;
                    }
                    //else if (dgvOpeningStockDetails["ItemStatusID",i].Value.ToInt32() == (int)ProductStatus.Active &&( dgvOpeningStockDetails["SaleRate", i].Value == null || dgvOpeningStockDetails["SaleRate", i].Value.ToDecimal() == 0))
                    //{
                    //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4311, out MmsgMessageIcon);
                    //    intRowIndex = i;
                    //    strColumnName = "SaleRate";
                    //    blnIsValid = false;
                    //}
                    else if (dgvOpeningStockDetails["UOM", i].Tag == null)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4306, out MmsgMessageIcon);
                        intRowIndex = i;
                        strColumnName = "UOM";
                        blnIsValid = false;
                    }
                    else if (Convert.ToBoolean(MobjClsBllOpeningStock.FillCombos(new string[] { "ExpiryDateMandatory", "InvItemMaster", "ItemID = " + dgvOpeningStockDetails.Rows[i].Cells["ItemID"].Value.ToInt32() }).Rows[0]["ExpiryDateMandatory"]) == true && (dgvOpeningStockDetails.Rows[i].Cells["ExpiryDate"].Value == DBNull.Value || dgvOpeningStockDetails.Rows[i].Cells["ExpiryDate"].Value == null))
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4315, out MmsgMessageIcon);
                        intRowIndex = i;
                        strColumnName = "ExpiryDate";
                        blnIsValid = false;
                    }
                    else if (dgvOpeningStockDetails.Rows[i].Cells["ExpiryDate"].Value != DBNull.Value && dgvOpeningStockDetails.Rows[i].Cells["ExpiryDate"].Value != null)
                    {
                        DateTime dtExpiryDate = new DateTime();
                        try
                        {
                            dtExpiryDate = Convert.ToDateTime(dgvOpeningStockDetails.Rows[i].Cells["ExpiryDate"].Value);
                        }
                        catch
                        {

                        }
                        if (dtExpiryDate < dtpDate.Value.Date)
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4310, out MmsgMessageIcon);
                            intRowIndex = i;
                            strColumnName = "ExpiryDate";
                            blnIsValid = false;
                        }
                    }
                    intItemCount++;
                }
            }
            if (intItemCount == 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4302, out MmsgMessageIcon);
                intRowIndex = 0;
                strColumnName = "ItemCode";
                blnIsValid = false;
            }
            if (blnIsValid)
            {
                for (int i = 0; i < dgvOpeningStockDetails.Rows.Count; i++)
                {
                    for (int j = i + 1; j < dgvOpeningStockDetails.Rows.Count; j++)
                    {
                        if (dgvOpeningStockDetails["ItemID", j].Value != null)
                        {
                            //if (Convert.ToString(dgvOpeningStockDetails["BatchNo", i].Value).ToUpper() == Convert.ToString(dgvOpeningStockDetails["BatchNo", j].Value).ToUpper() && Convert.ToString(dgvOpeningStockDetails["BatchNo", i].Value).Trim() != "")
                            //{
                            if (Convert.ToString(dgvOpeningStockDetails["ItemID", i].Value).ToInt32() == Convert.ToString(dgvOpeningStockDetails["ItemID", j].Value).ToInt32())
                            {
                                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4308, out MmsgMessageIcon);
                                intRowIndex = j;
                                strColumnName = "ItemCode";
                                blnIsValid = false;
                                break;
                            }
                        }
                    }
                    if (!blnIsValid)
                        break;
                }
            }
            if (!blnIsValid)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                ErrOpeningStock.SetError(dgvOpeningStockDetails, MstrCommonMessage.Replace("#", "").Trim());
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrOpeningStock.Enabled = true;
                dgvOpeningStockDetails.CurrentCell = dgvOpeningStockDetails[strColumnName, intRowIndex];
                dgvOpeningStockDetails.Focus();
            }
            return blnIsValid;
        }

        private void FrmOpeningStockEntry_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (!MblnIsFromOK)
            //{
            if(btnSave.Enabled==true )
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4269, out MmsgMessageIcon).Replace("#", "").Trim();
                if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                    DialogResult.Yes)
                {
                    //MobjClsNotification = null;
                    //MobjClsLogWriter = null;
                    //MobjClsBllOpeningStock = null;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void tmrOpeningStock_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            ErrOpeningStock.Clear();
        }

        private void bnClear_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void FrmOpeningStockEntry_Shown(object sender, EventArgs e)
        {
            cboWarehouse.Focus();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bnSaveItem_Click(null, null);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveFunction())
                {
                    MblnIsFromOK = true;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                {
                    MessageBox.Show("Error in BtnOk_Click() " + ex.Message);
                }
                MobjClsLogWriter.WriteLog("Error in BtnOk_Click() " + ex.Message, 2);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool IsValidBatch(string strBatchNo, Int64 intItemID)
        {
            DataTable dtBatchDetails = MobjClsBllOpeningStock.FillCombos(new string[] { "BatchID,ItemID", "InvBatchDetails", "BatchNo = '" + strBatchNo + "'" });
            if (dtBatchDetails.Rows.Count > 0)
            {
                if (Convert.ToInt64(dtBatchDetails.Rows[0]["ItemID"]) != intItemID)
                    return false;
            }
            return true;
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();

        }

        private void LoadReport()
        {
            try
            {
                if (MintopeningStockID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MintopeningStockID;
                    ObjViewer.PiFormID = (int)FormID.OpeningStock;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form OpeningStockEntry:LoadReport " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadReport() " + Ex.Message.ToString());
            }
        }
        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }
        private void bnEmail_Click(object sender, EventArgs e)
        {
            using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
            {
                ObjEmailPopUp.MsSubject = "Opening Stock Information";
                ObjEmailPopUp.EmailFormType = EmailFormID.OpeningStock;
                ObjEmailPopUp.EmailSource = MobjClsBllOpeningStock.GetOpeningStockReport();
                ObjEmailPopUp.ShowDialog();
            }
        }

        private void cboWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboWarehouse.SelectedValue != null)
            {
                DataTable datTemp = MobjClsBllOpeningStock.FillCombos(new string[] { "CM.CompanyID,CR.Scale,CM.BookStartDate", "CompanyMaster CM INNER JOIN InvWarehouse WH ON CM.CompanyID=WH.CompanyID Inner Join CurrencyReference CR On CR.CurrencyID = CM.CurrencyId", "WarehouseID=" + Convert.ToInt32(cboWarehouse.SelectedValue) });
                if (datTemp.Rows.Count > 0)
                {
                    MintCompanyID = Convert.ToInt32(datTemp.Rows[0]["CompanyID"]);
                    MintCurrencyScale = datTemp.Rows[0]["Scale"].ToInt32();
                    dtpDate.Value = datTemp.Rows[0]["BookStartDate"].ToDateTime();
                }
            }
            else
                MintCurrencyScale = 2;
            if (cboWarehouse.Enabled)
            {
                dgvOpeningStockDetails.Rows.Clear();
                CalculateTotal();
            }
        }

        private void btnProduct_Click(object sender, EventArgs e)
        {
            CallProducts();
        }
        private void CallProducts()
        {
            frmItemMaster objItemMaster1 = null;

            try
            {
                objItemMaster1 = new frmItemMaster(ClsCommonSettings.PblnIsTaxable);
                objItemMaster1.Text = "Product Master " + (ClsMainSettings.objFrmMain.MdiChildren.Length + 1);
                objItemMaster1.MdiParent = ClsMainSettings.objFrmMain;
                objItemMaster1.WindowState = FormWindowState.Maximized;
                objItemMaster1.BringToFront();
                objItemMaster1.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objItemMaster1.Dispose();
                System.GC.Collect();
                CallProducts();
            }
        }

        private void dgvOpeningStockDetails_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjCommonUtility.SetSerialNo(dgvOpeningStockDetails, e.RowIndex, false);
        }

        private void dgvOpeningStockDetails_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjCommonUtility.SetSerialNo(dgvOpeningStockDetails, e.RowIndex, false);
            CalculateTotal();
        }

        //private bool ValidateLocationQuantity(bool blnIsDeletion)
        //{
        //    DataTable datOpeningStockDetails = new DataTable();
        //    bool blnValid = true;
        //    datOpeningStockDetails = MobjClsBllOpeningStock.GetOpeningStockDetails();

        //    foreach (DataRow dr in datOpeningStockDetails.Rows)
        //    {

        //        clsDTOPurchaseLocationDetails objLocationDetails = new clsDTOPurchaseLocationDetails();
        //        objLocationDetails.intItemID = dr["ItemID"].ToInt32();
        //        objLocationDetails.lngBatchID = dr["BatchID"].ToInt64();
        //        objLocationDetails.intLocationID = dr["LocationID"].ToInt32();
        //        objLocationDetails.intRowID = dr["RowID"].ToInt32();
        //        objLocationDetails.intBlockID = dr["BlockID"].ToInt32();
        //        objLocationDetails.intLotID = dr["LotID"].ToInt32();

        //        decimal decAvailableQty = MobjClsBllOpeningStock.GetDefaultLocationQuantity(objLocationDetails, blnIsDemoQty);
        //        DataTable dtGetUomDetails = MobjClsBLLPurchase.GetUomConversionValues(dr["UOMID"].ToInt32(), dr["ItemID"].ToInt32());
        //        decimal decOldQuantity = dr["Quantity"].ToDecimal();
        //        if (dtGetUomDetails.Rows.Count > 0)
        //        {
        //            int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
        //            decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
        //            if (intConversionFactor == 1)
        //                decOldQuantity = decOldQuantity / decConversionValue;
        //            else if (intConversionFactor == 2)
        //                decOldQuantity = decOldQuantity * decConversionValue;
        //        }

        //        if (blnIsDeletion)
        //        {
        //            if (decAvailableQty - decOldQuantity < 0)
        //            {
        //                blnValid = false;
        //            }
        //        }
        //        else
        //        {
        //            decimal decCurrentQuantity = 0;
        //            foreach (DataGridViewRow drLocationRow in dgvLocationDetails.Rows)
        //            {
        //                if (drLocationRow.Cells["LItemID"].Value != null && drLocationRow.Cells["LBatchNo"].Value != null)
        //                {
        //                    if (drLocationRow.Cells["LItemID"].Value.ToInt32() == dr["ItemID"].ToInt32() && drLocationRow.Cells["LBatchNo"].Value.ToString() == dr["BatchNo"].ToString())
        //                    {
        //                        dtGetUomDetails = MobjClsBLLPurchase.GetUomConversionValues(drLocationRow.Cells["LUOMID"].Tag.ToInt32().ToInt32(), dr["ItemID"].ToInt32());
        //                        decCurrentQuantity = drLocationRow.Cells["LQuantity"].Value.ToDecimal();
        //                        if (dtGetUomDetails.Rows.Count > 0)
        //                        {
        //                            int intConversionFactor = Convert.ToInt32(dtGetUomDetails.Rows[0]["ConvertionFactorID"]);
        //                            decimal decConversionValue = Convert.ToDecimal(dtGetUomDetails.Rows[0]["ConvertionValue"]);
        //                            if (intConversionFactor == 1)
        //                                decCurrentQuantity = decCurrentQuantity / decConversionValue;
        //                            else if (intConversionFactor == 2)
        //                                decCurrentQuantity = decCurrentQuantity * decConversionValue;

        //                            break;
        //                        }

        //                    }
        //                }
        //            }
        //            if ((decAvailableQty - decOldQuantity) + decCurrentQuantity < 0)
        //            {
        //                blnValid = false;
        //            }
        //        }
        //        if (!blnValid)
        //        {
        //            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 941, out MmessageIcon).Replace("#", "").Replace("@", dr["ItemName"].ToString());
        //            MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
        //            tcPurchase.SelectedTab = tpLocationDetails;
        //            dgvLocationDetails.Focus();
        //            break;
        //        }
        //    }
        //    return blnValid;
        //}

        private void CalculateTotal()
        {
            decimal decTotalAmount = 0;
            foreach (DataGridViewRow dr in dgvOpeningStockDetails.Rows)
            {
                if (dr.Cells[ItemID.Index].Value.ToInt64() != 0)
                {
                    dr.Cells[Total.Index].Value = (dr.Cells[Quantity.Index].Value.ToDecimal() * dr.Cells[Rate.Index].Value.ToDecimal()).ToString("F" + MintCurrencyScale);
                    decTotalAmount += dr.Cells[Total.Index].Value.ToDecimal();
                }
            }
            txtNetAmount.Text = decTotalAmount.ToString("F" + MintCurrencyScale);
        }

    }
}
