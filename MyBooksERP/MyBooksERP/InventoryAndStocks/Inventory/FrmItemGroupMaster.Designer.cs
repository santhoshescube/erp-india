﻿namespace MyBooksERP
{
    partial class frmItemGroupMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmItemGroupMaster));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ItemGroupsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bnMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bnMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bnPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bnCancel = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrint = new System.Windows.Forms.ToolStripButton();
            this.bnEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnHelp = new System.Windows.Forms.ToolStripButton();
            this.pnlItemMaster = new System.Windows.Forms.Panel();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.txtBatch = new System.Windows.Forms.TextBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.cboPricingScheme = new System.Windows.Forms.ComboBox();
            this.cboCostingMethod = new System.Windows.Forms.ComboBox();
            this.cboBaseUom = new System.Windows.Forms.ComboBox();
            this.cboSubCategory = new System.Windows.Forms.ComboBox();
            this.cboCategory = new System.Windows.Forms.ComboBox();
            this.txtMachineCost = new DemoClsDataGridview.DecimalTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnPricingScheme = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSubCategory = new System.Windows.Forms.Button();
            this.btnCategory = new System.Windows.Forms.Button();
            this.btnBaseUom = new System.Windows.Forms.Button();
            this.LblCategory = new System.Windows.Forms.Label();
            this.LblItemType = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLabourCost = new DemoClsDataGridview.DecimalTextBox();
            this.iblLabourCost = new System.Windows.Forms.Label();
            this.txtSaleAmount = new DemoClsDataGridview.DecimalTextBox();
            this.txtActualAmount = new DemoClsDataGridview.DecimalTextBox();
            this.dgvItem = new ClsInnerGrid();
            this.label2 = new System.Windows.Forms.Label();
            this.lblActualAmount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtShortName = new System.Windows.Forms.TextBox();
            this.lblShortName = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtGroupCode = new System.Windows.Forms.TextBox();
            this.lblItemCode = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.errItemGroups = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmItemGroups = new System.Windows.Forms.Timer(this.components);
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.toolStripStatusLabel11 = new DevComponents.DotNetBar.LabelItem();
            this.lblStatus = new DevComponents.DotNetBar.LabelItem();
            this.dgvColItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColUom = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvColQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColItemRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColSerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColBaseRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ItemGroupsBindingNavigator)).BeginInit();
            this.ItemGroupsBindingNavigator.SuspendLayout();
            this.pnlItemMaster.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errItemGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // ItemGroupsBindingNavigator
            // 
            this.ItemGroupsBindingNavigator.AddNewItem = null;
            this.ItemGroupsBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.ItemGroupsBindingNavigator.CountItem = this.bnCountItem;
            this.ItemGroupsBindingNavigator.DeleteItem = null;
            this.ItemGroupsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnMoveFirstItem,
            this.bnMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bnPositionItem,
            this.bnCountItem,
            this.bindingNavigatorSeparator1,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnDeleteItem,
            this.bnCancel,
            this.ToolStripSeparator,
            this.bnPrint,
            this.bnEmail,
            this.toolStripSeparator1,
            this.bnHelp});
            this.ItemGroupsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.ItemGroupsBindingNavigator.MoveFirstItem = null;
            this.ItemGroupsBindingNavigator.MoveLastItem = null;
            this.ItemGroupsBindingNavigator.MoveNextItem = null;
            this.ItemGroupsBindingNavigator.MovePreviousItem = null;
            this.ItemGroupsBindingNavigator.Name = "ItemGroupsBindingNavigator";
            this.ItemGroupsBindingNavigator.PositionItem = this.bnPositionItem;
            this.ItemGroupsBindingNavigator.Size = new System.Drawing.Size(694, 25);
            this.ItemGroupsBindingNavigator.TabIndex = 4;
            this.ItemGroupsBindingNavigator.Text = "bindingNavigator1";
            // 
            // bnCountItem
            // 
            this.bnCountItem.Name = "bnCountItem";
            this.bnCountItem.Size = new System.Drawing.Size(35, 22);
            this.bnCountItem.Text = "of {0}";
            this.bnCountItem.ToolTipText = "Total number of items";
            // 
            // bnMoveFirstItem
            // 
            this.bnMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
            this.bnMoveFirstItem.Name = "bnMoveFirstItem";
            this.bnMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveFirstItem.Text = "Move first";
            this.bnMoveFirstItem.Click += new System.EventHandler(this.MoveFirstItem);
            // 
            // bnMovePreviousItem
            // 
            this.bnMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem.Image")));
            this.bnMovePreviousItem.Name = "bnMovePreviousItem";
            this.bnMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bnMovePreviousItem.Text = "Move previous";
            this.bnMovePreviousItem.Click += new System.EventHandler(this.MovePreviousItem);
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPositionItem
            // 
            this.bnPositionItem.AccessibleName = "Position";
            this.bnPositionItem.AutoSize = false;
            this.bnPositionItem.Name = "bnPositionItem";
            this.bnPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bnPositionItem.Text = "0";
            this.bnPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoveNextItem
            // 
            this.bnMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
            this.bnMoveNextItem.Name = "bnMoveNextItem";
            this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveNextItem.Text = "Move next";
            this.bnMoveNextItem.Click += new System.EventHandler(this.MoveNextItem);
            // 
            // bnMoveLastItem
            // 
            this.bnMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
            this.bnMoveLastItem.Name = "bnMoveLastItem";
            this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveLastItem.Text = "Move last";
            this.bnMoveLastItem.Click += new System.EventHandler(this.MoveLastItem);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bnAddNewItem.ToolTipText = "Add new Item";
            this.bnAddNewItem.Click += new System.EventHandler(this.AddNewItem);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bnSaveItem.ToolTipText = "Save Item";
            this.bnSaveItem.Click += new System.EventHandler(this.SaveItem);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bnDeleteItem.Image")));
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteItem.ToolTipText = "Delete Item";
            this.bnDeleteItem.Click += new System.EventHandler(this.DeleteItem);
            // 
            // bnCancel
            // 
            this.bnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnCancel.Image = ((System.Drawing.Image)(resources.GetObject("bnCancel.Image")));
            this.bnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnCancel.Name = "bnCancel";
            this.bnCancel.Size = new System.Drawing.Size(23, 22);
            this.bnCancel.ToolTipText = "Clear";
            this.bnCancel.Click += new System.EventHandler(this.Cancel);
            // 
            // ToolStripSeparator
            // 
            this.ToolStripSeparator.Name = "ToolStripSeparator";
            this.ToolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPrint
            // 
            this.bnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnPrint.Image = ((System.Drawing.Image)(resources.GetObject("bnPrint.Image")));
            this.bnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(23, 22);
            this.bnPrint.ToolTipText = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // bnEmail
            // 
            this.bnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnEmail.Enabled = false;
            this.bnEmail.Image = ((System.Drawing.Image)(resources.GetObject("bnEmail.Image")));
            this.bnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Size = new System.Drawing.Size(23, 22);
            this.bnEmail.ToolTipText = "Email";
            this.bnEmail.Click += new System.EventHandler(this.Email);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnHelp
            // 
            this.bnHelp.Enabled = false;
            this.bnHelp.Image = ((System.Drawing.Image)(resources.GetObject("bnHelp.Image")));
            this.bnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnHelp.Name = "bnHelp";
            this.bnHelp.Size = new System.Drawing.Size(23, 22);
            this.bnHelp.ToolTipText = "Help";
            // 
            // pnlItemMaster
            // 
            this.pnlItemMaster.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlItemMaster.BackColor = System.Drawing.Color.Transparent;
            this.pnlItemMaster.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItemMaster.Controls.Add(this.dtpDate);
            this.pnlItemMaster.Controls.Add(this.txtBatch);
            this.pnlItemMaster.Controls.Add(this.lblDate);
            this.pnlItemMaster.Controls.Add(this.cboPricingScheme);
            this.pnlItemMaster.Controls.Add(this.cboCostingMethod);
            this.pnlItemMaster.Controls.Add(this.cboBaseUom);
            this.pnlItemMaster.Controls.Add(this.cboSubCategory);
            this.pnlItemMaster.Controls.Add(this.cboCategory);
            this.pnlItemMaster.Controls.Add(this.txtMachineCost);
            this.pnlItemMaster.Controls.Add(this.label7);
            this.pnlItemMaster.Controls.Add(this.btnPricingScheme);
            this.pnlItemMaster.Controls.Add(this.label5);
            this.pnlItemMaster.Controls.Add(this.label4);
            this.pnlItemMaster.Controls.Add(this.btnSubCategory);
            this.pnlItemMaster.Controls.Add(this.btnCategory);
            this.pnlItemMaster.Controls.Add(this.btnBaseUom);
            this.pnlItemMaster.Controls.Add(this.LblCategory);
            this.pnlItemMaster.Controls.Add(this.LblItemType);
            this.pnlItemMaster.Controls.Add(this.label3);
            this.pnlItemMaster.Controls.Add(this.txtLabourCost);
            this.pnlItemMaster.Controls.Add(this.iblLabourCost);
            this.pnlItemMaster.Controls.Add(this.txtSaleAmount);
            this.pnlItemMaster.Controls.Add(this.txtActualAmount);
            this.pnlItemMaster.Controls.Add(this.dgvItem);
            this.pnlItemMaster.Controls.Add(this.label2);
            this.pnlItemMaster.Controls.Add(this.lblActualAmount);
            this.pnlItemMaster.Controls.Add(this.label1);
            this.pnlItemMaster.Controls.Add(this.label9);
            this.pnlItemMaster.Controls.Add(this.label8);
            this.pnlItemMaster.Controls.Add(this.label6);
            this.pnlItemMaster.Controls.Add(this.txtShortName);
            this.pnlItemMaster.Controls.Add(this.lblShortName);
            this.pnlItemMaster.Controls.Add(this.txtDescription);
            this.pnlItemMaster.Controls.Add(this.lblDescription);
            this.pnlItemMaster.Controls.Add(this.txtGroupCode);
            this.pnlItemMaster.Controls.Add(this.lblItemCode);
            this.pnlItemMaster.Controls.Add(this.shapeContainer1);
            this.pnlItemMaster.Location = new System.Drawing.Point(4, 28);
            this.pnlItemMaster.Name = "pnlItemMaster";
            this.pnlItemMaster.Size = new System.Drawing.Size(686, 399);
            this.pnlItemMaster.TabIndex = 0;
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(532, 20);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(103, 20);
            this.dtpDate.TabIndex = 4;
            // 
            // txtBatch
            // 
            this.txtBatch.BackColor = System.Drawing.Color.White;
            this.txtBatch.Location = new System.Drawing.Point(448, 21);
            this.txtBatch.MaxLength = 50;
            this.txtBatch.Name = "txtBatch";
            this.txtBatch.Size = new System.Drawing.Size(78, 20);
            this.txtBatch.TabIndex = 3;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Location = new System.Drawing.Point(361, 24);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(87, 13);
            this.lblDate.TabIndex = 245;
            this.lblDate.Text = "Batch No.&& Date";
            // 
            // cboPricingScheme
            // 
            this.cboPricingScheme.BackColor = System.Drawing.Color.White;
            this.cboPricingScheme.FormattingEnabled = true;
            this.cboPricingScheme.Location = new System.Drawing.Point(448, 72);
            this.cboPricingScheme.Name = "cboPricingScheme";
            this.cboPricingScheme.Size = new System.Drawing.Size(190, 21);
            this.cboPricingScheme.TabIndex = 6;
            // 
            // cboCostingMethod
            // 
            this.cboCostingMethod.FormattingEnabled = true;
            this.cboCostingMethod.Location = new System.Drawing.Point(448, 46);
            this.cboCostingMethod.Name = "cboCostingMethod";
            this.cboCostingMethod.Size = new System.Drawing.Size(190, 21);
            this.cboCostingMethod.TabIndex = 5;
            // 
            // cboBaseUom
            // 
            this.cboBaseUom.FormattingEnabled = true;
            this.cboBaseUom.Location = new System.Drawing.Point(448, 153);
            this.cboBaseUom.Name = "cboBaseUom";
            this.cboBaseUom.Size = new System.Drawing.Size(186, 21);
            this.cboBaseUom.TabIndex = 9;
            // 
            // cboSubCategory
            // 
            this.cboSubCategory.FormattingEnabled = true;
            this.cboSubCategory.Location = new System.Drawing.Point(448, 126);
            this.cboSubCategory.Name = "cboSubCategory";
            this.cboSubCategory.Size = new System.Drawing.Size(186, 21);
            this.cboSubCategory.TabIndex = 8;
            // 
            // cboCategory
            // 
            this.cboCategory.FormattingEnabled = true;
            this.cboCategory.Location = new System.Drawing.Point(448, 99);
            this.cboCategory.Name = "cboCategory";
            this.cboCategory.Size = new System.Drawing.Size(187, 21);
            this.cboCategory.TabIndex = 7;
            // 
            // txtMachineCost
            // 
            this.txtMachineCost.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtMachineCost.Location = new System.Drawing.Point(102, 366);
            this.txtMachineCost.MaxLength = 13;
            this.txtMachineCost.Name = "txtMachineCost";
            this.txtMachineCost.ShortcutsEnabled = false;
            this.txtMachineCost.Size = new System.Drawing.Size(100, 20);
            this.txtMachineCost.TabIndex = 12;
            this.txtMachineCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMachineCost.TextChanged += new System.EventHandler(this.txtLabourCost_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(12, 369);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 236;
            this.label7.Text = "Machine Cost";
            // 
            // btnPricingScheme
            // 
            this.btnPricingScheme.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPricingScheme.Location = new System.Drawing.Point(644, 73);
            this.btnPricingScheme.Name = "btnPricingScheme";
            this.btnPricingScheme.Size = new System.Drawing.Size(29, 20);
            this.btnPricingScheme.TabIndex = 12;
            this.btnPricingScheme.Text = "...";
            this.btnPricingScheme.Click += new System.EventHandler(this.btnPricingScheme_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(361, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 232;
            this.label5.Text = "Pricing Scheme";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(361, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 230;
            this.label4.Text = "Costing Method";
            // 
            // btnSubCategory
            // 
            this.btnSubCategory.Location = new System.Drawing.Point(644, 126);
            this.btnSubCategory.Name = "btnSubCategory";
            this.btnSubCategory.Size = new System.Drawing.Size(29, 20);
            this.btnSubCategory.TabIndex = 6;
            this.btnSubCategory.Text = "...";
            this.btnSubCategory.UseVisualStyleBackColor = true;
            this.btnSubCategory.Click += new System.EventHandler(this.btnSubCategory_Click);
            // 
            // btnCategory
            // 
            this.btnCategory.Location = new System.Drawing.Point(644, 99);
            this.btnCategory.Name = "btnCategory";
            this.btnCategory.Size = new System.Drawing.Size(29, 20);
            this.btnCategory.TabIndex = 4;
            this.btnCategory.Text = "...";
            this.btnCategory.UseVisualStyleBackColor = true;
            this.btnCategory.Click += new System.EventHandler(this.btnCategory_Click);
            // 
            // btnBaseUom
            // 
            this.btnBaseUom.Location = new System.Drawing.Point(644, 153);
            this.btnBaseUom.Name = "btnBaseUom";
            this.btnBaseUom.Size = new System.Drawing.Size(29, 20);
            this.btnBaseUom.TabIndex = 8;
            this.btnBaseUom.Text = "...";
            this.btnBaseUom.UseVisualStyleBackColor = true;
            this.btnBaseUom.Click += new System.EventHandler(this.btnBaseUom_Click);
            // 
            // LblCategory
            // 
            this.LblCategory.AutoSize = true;
            this.LblCategory.Location = new System.Drawing.Point(361, 103);
            this.LblCategory.Name = "LblCategory";
            this.LblCategory.Size = new System.Drawing.Size(49, 13);
            this.LblCategory.TabIndex = 223;
            this.LblCategory.Text = "Category";
            // 
            // LblItemType
            // 
            this.LblItemType.AutoSize = true;
            this.LblItemType.Location = new System.Drawing.Point(361, 130);
            this.LblItemType.Name = "LblItemType";
            this.LblItemType.Size = new System.Drawing.Size(71, 13);
            this.LblItemType.TabIndex = 222;
            this.LblItemType.Text = "Sub Category";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(361, 157);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 221;
            this.label3.Text = "Base UOM";
            // 
            // txtLabourCost
            // 
            this.txtLabourCost.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtLabourCost.Location = new System.Drawing.Point(102, 338);
            this.txtLabourCost.MaxLength = 13;
            this.txtLabourCost.Name = "txtLabourCost";
            this.txtLabourCost.ShortcutsEnabled = false;
            this.txtLabourCost.Size = new System.Drawing.Size(100, 20);
            this.txtLabourCost.TabIndex = 11;
            this.txtLabourCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLabourCost.TextChanged += new System.EventHandler(this.txtLabourCost_TextChanged);
            // 
            // iblLabourCost
            // 
            this.iblLabourCost.AutoSize = true;
            this.iblLabourCost.BackColor = System.Drawing.Color.Transparent;
            this.iblLabourCost.Location = new System.Drawing.Point(13, 342);
            this.iblLabourCost.Name = "iblLabourCost";
            this.iblLabourCost.Size = new System.Drawing.Size(64, 13);
            this.iblLabourCost.TabIndex = 86;
            this.iblLabourCost.Text = "Labour Cost";
            // 
            // txtSaleAmount
            // 
            this.txtSaleAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtSaleAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtSaleAmount.Location = new System.Drawing.Point(565, 368);
            this.txtSaleAmount.MaxLength = 13;
            this.txtSaleAmount.Name = "txtSaleAmount";
            this.txtSaleAmount.ShortcutsEnabled = false;
            this.txtSaleAmount.Size = new System.Drawing.Size(100, 20);
            this.txtSaleAmount.TabIndex = 14;
            this.txtSaleAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSaleAmount.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // txtActualAmount
            // 
            this.txtActualAmount.Enabled = false;
            this.txtActualAmount.Location = new System.Drawing.Point(565, 340);
            this.txtActualAmount.Name = "txtActualAmount";
            this.txtActualAmount.ShortcutsEnabled = false;
            this.txtActualAmount.Size = new System.Drawing.Size(100, 20);
            this.txtActualAmount.TabIndex = 13;
            this.txtActualAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtActualAmount.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // dgvItem
            // 
            this.dgvItem.AddNewRow = false;
            this.dgvItem.AlphaNumericCols = new int[0];
            this.dgvItem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvItem.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            this.dgvItem.CapsLockCols = new int[0];
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColItemCode,
            this.dgvColItemName,
            this.dgvColUom,
            this.dgvColQuantity,
            this.dgvColItemRate,
            this.dgvColTotal,
            this.dgvColItemID,
            this.dgvColSerialNo,
            this.dgvColBaseRate});
            this.dgvItem.DecimalCols = new int[] {
        3};
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvItem.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvItem.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvItem.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvItem.HasSlNo = false;
            this.dgvItem.LastRowIndex = 0;
            this.dgvItem.Location = new System.Drawing.Point(5, 200);
            this.dgvItem.Name = "dgvItem";
            this.dgvItem.NegativeValueCols = new int[0];
            this.dgvItem.NumericCols = new int[0];
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItem.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            this.dgvItem.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvItem.Size = new System.Drawing.Size(675, 131);
            this.dgvItem.TabIndex = 10;
            this.dgvItem.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.ItemValueChanged);
            this.dgvItem.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvItem_UserDeletingRow);
            this.dgvItem.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvItem_CellBeginEdit);
            this.dgvItem.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvItem_RowsAdded);
            this.dgvItem.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvItem_Textbox_TextChanged);
            this.dgvItem.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvItem_EditingControlShowing);
            this.dgvItem.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvItem_CurrentCellDirtyStateChanged);
            this.dgvItem.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.ItemCellEnter);
            this.dgvItem.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvItem_RowsRemoved);
            this.dgvItem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(482, 371);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Sale Amount";
            // 
            // lblActualAmount
            // 
            this.lblActualAmount.AutoSize = true;
            this.lblActualAmount.Location = new System.Drawing.Point(482, 342);
            this.lblActualAmount.Name = "lblActualAmount";
            this.lblActualAmount.Size = new System.Drawing.Size(68, 13);
            this.lblActualAmount.TabIndex = 13;
            this.lblActualAmount.Text = "Material Cost";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 182);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Item Details";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(781, 178);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 81;
            this.label9.Text = "End Date";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(781, 152);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 79;
            this.label8.Text = "Start Date";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "General Info";
            // 
            // txtShortName
            // 
            this.txtShortName.BackColor = System.Drawing.SystemColors.Info;
            this.txtShortName.Location = new System.Drawing.Point(79, 46);
            this.txtShortName.MaxLength = 200;
            this.txtShortName.Multiline = true;
            this.txtShortName.Name = "txtShortName";
            this.txtShortName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtShortName.Size = new System.Drawing.Size(244, 43);
            this.txtShortName.TabIndex = 1;
            this.txtShortName.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // lblShortName
            // 
            this.lblShortName.AutoSize = true;
            this.lblShortName.Location = new System.Drawing.Point(13, 50);
            this.lblShortName.Name = "lblShortName";
            this.lblShortName.Size = new System.Drawing.Size(35, 13);
            this.lblShortName.TabIndex = 73;
            this.lblShortName.Text = "Name";
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtDescription.Location = new System.Drawing.Point(79, 95);
            this.txtDescription.MaxLength = 500;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(244, 78);
            this.txtDescription.TabIndex = 2;
            this.txtDescription.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(13, 95);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 13);
            this.lblDescription.TabIndex = 2;
            this.lblDescription.Text = "Description";
            // 
            // txtGroupCode
            // 
            this.txtGroupCode.BackColor = System.Drawing.SystemColors.Info;
            this.txtGroupCode.Location = new System.Drawing.Point(79, 20);
            this.txtGroupCode.MaxLength = 30;
            this.txtGroupCode.Name = "txtGroupCode";
            this.txtGroupCode.Size = new System.Drawing.Size(100, 20);
            this.txtGroupCode.TabIndex = 0;
            this.txtGroupCode.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // lblItemCode
            // 
            this.lblItemCode.AutoSize = true;
            this.lblItemCode.Location = new System.Drawing.Point(13, 24);
            this.lblItemCode.Name = "lblItemCode";
            this.lblItemCode.Size = new System.Drawing.Size(32, 13);
            this.lblItemCode.TabIndex = 0;
            this.lblItemCode.Text = "Code";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(684, 397);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 77;
            this.lineShape2.X2 = 680;
            this.lineShape2.Y1 = 11;
            this.lineShape2.Y2 = 11;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 77;
            this.lineShape1.X2 = 680;
            this.lineShape1.Y1 = 193;
            this.lineShape1.Y2 = 193;
            // 
            // cboStatus
            // 
            this.cboStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboStatus.BackColor = System.Drawing.SystemColors.Info;
            this.cboStatus.DropDownHeight = 75;
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.IntegralHeight = false;
            this.cboStatus.Location = new System.Drawing.Point(139, 444);
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Size = new System.Drawing.Size(52, 21);
            this.cboStatus.TabIndex = 1;
            this.cboStatus.Visible = false;
            this.cboStatus.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.cboStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboStatus_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(96, 447);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 84;
            this.label10.Text = "Status";
            this.label10.Visible = false;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(517, 442);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(80, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.SaveItem);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(10, 442);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(80, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.SaveItem);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(605, 442);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.Cose);
            // 
            // errItemGroups
            // 
            this.errItemGroups.ContainerControl = this;
            this.errItemGroups.RightToLeft = true;
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.toolStripStatusLabel11,
            this.lblStatus});
            this.bar1.Location = new System.Drawing.Point(0, 485);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(694, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 81;
            this.bar1.TabStop = false;
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Text = "Status:";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            // 
            // dgvColItemCode
            // 
            this.dgvColItemCode.HeaderText = "Code";
            this.dgvColItemCode.MaxInputLength = 20;
            this.dgvColItemCode.Name = "dgvColItemCode";
            this.dgvColItemCode.Width = 70;
            // 
            // dgvColItemName
            // 
            this.dgvColItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvColItemName.HeaderText = "Item Name";
            this.dgvColItemName.MaxInputLength = 50;
            this.dgvColItemName.Name = "dgvColItemName";
            // 
            // dgvColUom
            // 
            this.dgvColUom.FillWeight = 90F;
            this.dgvColUom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColUom.HeaderText = "Uom";
            this.dgvColUom.Name = "dgvColUom";
            this.dgvColUom.Width = 90;
            // 
            // dgvColQuantity
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColQuantity.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvColQuantity.HeaderText = "Quantity";
            this.dgvColQuantity.MaxInputLength = 15;
            this.dgvColQuantity.Name = "dgvColQuantity";
            this.dgvColQuantity.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvColQuantity.Width = 80;
            // 
            // dgvColItemRate
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColItemRate.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvColItemRate.HeaderText = "Rate";
            this.dgvColItemRate.MaxInputLength = 8;
            this.dgvColItemRate.Name = "dgvColItemRate";
            this.dgvColItemRate.ReadOnly = true;
            this.dgvColItemRate.Width = 80;
            // 
            // dgvColTotal
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColTotal.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvColTotal.HeaderText = "Total";
            this.dgvColTotal.Name = "dgvColTotal";
            this.dgvColTotal.ReadOnly = true;
            this.dgvColTotal.Width = 110;
            // 
            // dgvColItemID
            // 
            this.dgvColItemID.HeaderText = "ItemID";
            this.dgvColItemID.Name = "dgvColItemID";
            this.dgvColItemID.Visible = false;
            // 
            // dgvColSerialNo
            // 
            this.dgvColSerialNo.HeaderText = "SerialNumber";
            this.dgvColSerialNo.Name = "dgvColSerialNo";
            this.dgvColSerialNo.Visible = false;
            // 
            // dgvColBaseRate
            // 
            this.dgvColBaseRate.HeaderText = "Base Rate";
            this.dgvColBaseRate.Name = "dgvColBaseRate";
            this.dgvColBaseRate.Visible = false;
            // 
            // frmItemGroupMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 504);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.pnlItemMaster);
            this.Controls.Add(this.ItemGroupsBindingNavigator);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cboStatus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmItemGroupMaster";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Product Assembly";
            this.Load += new System.EventHandler(this.FormLoad);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClosingForm);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormKeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.ItemGroupsBindingNavigator)).EndInit();
            this.ItemGroupsBindingNavigator.ResumeLayout(false);
            this.ItemGroupsBindingNavigator.PerformLayout();
            this.pnlItemMaster.ResumeLayout(false);
            this.pnlItemMaster.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errItemGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator ItemGroupsBindingNavigator;
        private System.Windows.Forms.ToolStripLabel bnCountItem;
        private System.Windows.Forms.ToolStripButton bnMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bnMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bnPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bnMoveNextItem;
        private System.Windows.Forms.ToolStripButton bnMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton bnAddNewItem;
        internal System.Windows.Forms.ToolStripButton bnSaveItem;
        internal System.Windows.Forms.ToolStripButton bnDeleteItem;
        private System.Windows.Forms.ToolStripButton bnCancel;
        private System.Windows.Forms.ToolStripSeparator ToolStripSeparator;
        internal System.Windows.Forms.ToolStripButton bnHelp;
        private System.Windows.Forms.ToolStripButton bnPrint;
        private System.Windows.Forms.ToolStripButton bnEmail;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Panel pnlItemMaster;
        private System.Windows.Forms.ComboBox cboStatus;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtShortName;
        private System.Windows.Forms.Label lblShortName;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox txtGroupCode;
        private System.Windows.Forms.Label lblItemCode;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblActualAmount;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private ClsInnerGrid dgvItem;
        private System.Windows.Forms.ErrorProvider errItemGroups;
        private System.Windows.Forms.Timer tmItemGroups;
        private DemoClsDataGridview.DecimalTextBox txtSaleAmount;
        private DemoClsDataGridview.DecimalTextBox txtActualAmount;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem toolStripStatusLabel11;
        private DevComponents.DotNetBar.LabelItem lblStatus;
        private DemoClsDataGridview.DecimalTextBox txtLabourCost;
        private System.Windows.Forms.Label iblLabourCost;
        private System.Windows.Forms.Label LblCategory;
        private System.Windows.Forms.Label LblItemType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSubCategory;
        private System.Windows.Forms.Button btnCategory;
        private System.Windows.Forms.Button btnBaseUom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnPricingScheme;
        private DemoClsDataGridview.DecimalTextBox txtMachineCost;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboCategory;
        private System.Windows.Forms.ComboBox cboPricingScheme;
        private System.Windows.Forms.ComboBox cboCostingMethod;
        private System.Windows.Forms.ComboBox cboBaseUom;
        private System.Windows.Forms.ComboBox cboSubCategory;
        private System.Windows.Forms.TextBox txtBatch;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemName;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColUom;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColSerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColBaseRate;
    }
}