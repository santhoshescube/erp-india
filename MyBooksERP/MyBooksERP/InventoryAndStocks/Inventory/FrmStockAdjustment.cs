﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;
//using Microsoft.VisualBasic;




namespace MyBooksERP
{
    /*****************************************************
     * Created By       : Ratheesh
     * Creation Date    : 20 June 2011
     * Description      : Handle Stock Adjustments
     * ***************************************************/

    public partial class FrmStockAdjustment : DevComponents.DotNetBar.Office2007Form
    {
        #region Declarations
        private bool MbViewPermission = false;
        private bool MbAddPermission = false;
        private bool MbUpdatePermission = false;
        private bool MbDeletePermission = false;
        private bool MbAddUpdatePermission = false;
        private bool mbPrintPermission = false;
        public int intStockAdjustment;
        private long MintStockAdjustmentID;
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;
        ClsNotification mObjNotification;
        ClsLogWriter MobjClsLogWriter;
        // -------------------------------------------------------
        private clsBLLStockAdjustments objStockAdjustmentBLL;
        private ClsLogWriter objLogWriter;
        private int? NullInt = null;
        private eMode mode = eMode.Unknown;
        private ClsLogWriter Logger
        {
            get
            {
                if (this.objLogWriter == null) this.objLogWriter = new ClsLogWriter(Application.StartupPath);
                return this.objLogWriter;
            }
        }
        private int? WareHouseID = null;
        private long? StockAdjustmentID = null;
        private bool InputChanged = false;
        private bool GridBinding = false;
        private int SelectedValue = 0;
        private int DefautCurrencyScale = 3;
        bool blnCheckStatus = false;
        private clsBLLCommonUtility MobjClsBLLCommonUtility;
        DataTable dtItemDetails = null;
        #endregion

        #region Form

        public FrmStockAdjustment()
        {
            this.InitializeComponent();
            this.mObjNotification = new ClsNotification();
            this.MmessageIcon = MessageBoxIcon.Information;
            this.dgvStockAdjustment.Enabled = false;
            this.objStockAdjustmentBLL = new clsBLLStockAdjustments();
            this.MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            this.MobjClsBLLCommonUtility = new clsBLLCommonUtility();
            this.AddCommonHandler();
        }

        private void FrmStockAdjustment_Load(object sender, EventArgs e)
        {
            this.TmrStatus.Interval = ClsCommonSettings.TimerInterval;
            this.TmrStatus.Enabled = true;
            this.LoadPermissions();
            this.FillCombos();
            this.LoadInitial();
            this.LoadMessage();
            this.AddNewRecord();

            if (!ClsCommonSettings.PblnIsLocationWise)
                tiLocationDetails.Visible = false;

        }

        private void FrmStockAdjustment_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.MdiFormClosing)
            {
                //if (this.InputChanged == true)
                //{
                if (this.BindingNavigatorSaveItem.Enabled == true)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8, out MmessageIcon);
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        this.objStockAdjustmentBLL = null;

                        if (this.objLogWriter != null)
                            this.objLogWriter = null;

                        e.Cancel = false;
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        #endregion

        #region Functions

        private void AddCommonHandler()
        {
            this.txtAdjustmentNo.TextChanged += new EventHandler(OnInputChanged);
            this.txtRemarks.TextChanged += new EventHandler(OnInputChanged);
            this.cboCompany.SelectedIndexChanged += new EventHandler(OnInputChanged);
            this.cboReason.SelectedIndexChanged += new EventHandler(OnInputChanged);
            this.cboWarehouse.SelectedIndexChanged += new EventHandler(OnInputChanged);
            this.dtpAdjustmentDate.ValueChanged += new EventHandler(OnInputChanged);
        }

        private void AddNewRecord()
        {
            try
            {
                this.objStockAdjustmentBLL = new clsBLLStockAdjustments();
                this.dtItemDetails = new DataTable();

                dtItemDetails.Columns.Add("ItemCode");
                dtItemDetails.Columns.Add("ItemName");
                dtItemDetails.Columns.Add("ItemID");
                dtItemDetails.Columns.Add("BatchNo");
                dtItemDetails.Columns.Add("BatchID");

                this.ClearControls();
                this.mode = eMode.Insert;
                 this.cboWarehouse.Enabled = true;
             //   this.dgvStockAdjustment.Columns[ActualQty.Index].Visible = false;
            //    this.dgvLocationDetails.Columns[LActualQty.Index].Visible = false;
                
                LItemName.Width = 200;
                this.dgvStockAdjustment.Rows.Clear();
                this.dgvLocationDetails.Rows.Clear();

                this.BindingNavigatorAddNewItem.Enabled = this.BindingNavigatorDeleteItem.Enabled = false;
                this.txtAdjustmentNo.Text = this.objStockAdjustmentBLL.GenerateAdjustmentNo(cboCompany.SelectedValue.ToInt32());
              
                if (ClsCommonSettings.blnStockAdjustmentNumberAutogenerate)
                    txtAdjustmentNo.ReadOnly = false;
                else
                    txtAdjustmentNo.ReadOnly = true;

                lblCreatedByText.Text = " Created By : " + ClsCommonSettings.strEmployeeName;
                lblCreatedDateValue.Text = " Created Date : " + ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");

                btnSRefresh_Click(null,null);

                cboCompany.Focus();

                cboReason.Enabled = true;
                btnReason.Enabled = true;
                txtAdjustmentNo.Enabled = true;
                dtpAdjustmentDate.Enabled = true;
                BindingNavigatorClearItem.Enabled = false;
                BindingNavigatorAddNewItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                this.GridBinding = false;
                cboCompany.Enabled = true;
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog(string.Format("Error on AddNewRecord() Form Name : {0}, Exception {1}", this.Name, ex.ToString()), 3);
            }
        }

        private void FillCombos()
        {
            try
            {
                DataTable datCombos = new DataTable();
               // if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                datCombos = new clsBLLPurchase().FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID=" + ClsCommonSettings.LoginCompanyID });
                //else
                //    datCombos = new clsBLLPurchase().GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.StAdjCompany);
                
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.DataSource = datCombos;

                clsUtilities.BindComboBox(this.cboReason, eReferenceTables.StockAdjustmentReason);
                cboCompany.SelectedValue = Convert.ToString(ClsCommonSettings.LoginCompanyID);
                cboCompany.Enabled = false;

                ////////////////Search Combos

                datCombos = null;
               // if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                datCombos = MobjClsBLLCommonUtility.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID=" + ClsCommonSettings.LoginCompanyID });
               // else
                //    datCombos = new clsBLLPurchase().GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.stPurIndCompany);

                cboSCompany.ValueMember = "CompanyID";
                cboSCompany.DisplayMember = "CompanyName";
                cboSCompany.DataSource = datCombos;
                cboSCompany.SelectedValue = Convert.ToString(ClsCommonSettings.LoginCompanyID);
                //cboSCompany.Enabled = false;
                /////////////////////////////
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog("Error on FillCombos() " + ex.ToString(), 4);
            }
        }

        private void LoadPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.StockAdjustment, out mbPrintPermission, out MbAddPermission, out MbUpdatePermission, out MbDeletePermission);

                //if (MbAddPermission == true || MbUpdatePermission == true)
                //    MbAddUpdatePermission = true;

                //if (MbAddPermission == true || MbUpdatePermission == true || MbDeletePermission == true || mbPrintPermission == true)
                //    MbViewPermission = true;
            }
            else
                MbAddPermission = MbUpdatePermission = MbDeletePermission = mbPrintPermission = true;

            //BindingNavigatorAddNewItem.Enabled = MbAddPermission;
            //BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
            //BindingNavigatorSaveItem.Enabled = BtnOk.Enabled = BtnSave.Enabled = MbAddUpdatePermission;
            //BtnPrint.Enabled = BtnEmail.Enabled = mbPrintPermission;
        }

        private void ClearControls()
        {
            this.ErrorStatus.Clear();

            this.cboSCompany.SelectedIndex = -1;
            this.cboSEmployee.SelectedIndex = -1;

            if (ClsCommonSettings.CompanyID <= 0)
                ClsCommonSettings.CompanyID = MobjClsBLLCommonUtility.GetCompanyID();

            this.cboCompany.SelectedValue = ClsCommonSettings.CompanyID;
            //this.cboCompany.Text = "";
            this.cboWarehouse.SelectedIndex = -1;
            this.cboWarehouse.Text = "";
            this.cboReason.SelectedIndex = -1;
            this.txtAdjustmentNo.Text = string.Empty;
            this.txtRemarks.Text = string.Empty;
            // construct date mannualy to avoid culture specific errors
            this.dtpAdjustmentDate.Value = new DateTime(ClsCommonSettings.GetServerDate().Year, ClsCommonSettings.GetServerDate().Month, ClsCommonSettings.GetServerDate().Day);
            this.dtpAdjustmentDate.MaxDate = new DateTime(ClsCommonSettings.GetServerDate().Year, ClsCommonSettings.GetServerDate().Month, ClsCommonSettings.GetServerDate().Day);
            this.dgvStockAdjustment.Rows.Clear();
            this.lblStockAdjustmentStatus.Text = string.Empty;
            this.StockAdjustmentID = -1;
        }

        private void LoadInitial()
        {
            this.dgvStockAdjustment.ShowCellToolTips = false;
            this.ClearControls();
            this.InputChanged = false;
        }

        private void LoadMessage()
        {
            this.MaMessageArr = new ArrayList();
            this.MaStatusMessage = new ArrayList();
            this.MaMessageArr = mObjNotification.FillMessageArray((int)FormID.StockAdjustment, ClsCommonSettings.ProductID);
            this.MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.StockAdjustment, ClsCommonSettings.ProductID);
        }

        private bool FormSave()
        {
            // variable to track database updation status
            bool Success = false;
            try
            {
                if (this.mode == eMode.Insert)
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);
                else
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);

                // Show confirmation to user
                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;
                this.objStockAdjustmentBLL.StockAdjustment.AdjustmentNo = this.txtAdjustmentNo.Text.Trim();
                if (objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID == 0 && objStockAdjustmentBLL.IsAdjustmentNumberExists())
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9002, out MmessageIcon);
                    MsMessageCommon = MsMessageCommon.Replace("*", "Adjustment");
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    else
                        this.txtAdjustmentNo.Text = objStockAdjustmentBLL.GenerateAdjustmentNo(cboCompany.SelectedValue.ToInt32());
                }
                else if (objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID > 0 && this.objStockAdjustmentBLL.IsAdjustmentNumberExists())
                {
                    MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 1411, out MmessageIcon).Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return false;
                }

                this.objStockAdjustmentBLL.StockAdjustment.AdjustmentNo = this.txtAdjustmentNo.Text.Trim();
                this.objStockAdjustmentBLL.StockAdjustment.AdjustmentDate = this.dtpAdjustmentDate.Value.Date;
                this.objStockAdjustmentBLL.StockAdjustment.WarehouseID = this.cboWarehouse.SelectedValue.ToInt32();
                this.objStockAdjustmentBLL.StockAdjustment.ReasonID = this.cboReason.SelectedValue.ToInt32();
                this.objStockAdjustmentBLL.StockAdjustment.Remarks = this.txtRemarks.Text.Trim();
                this.objStockAdjustmentBLL.StockAdjustment.CompanyID = this.cboCompany.SelectedValue.ToInt32();
                this.objStockAdjustmentBLL.StockAdjustment.CreatedBy = ClsCommonSettings.UserID;


                int SerialNo = 0;

                // clear items (if any)
                this.objStockAdjustmentBLL.StockAdjustment.AdjustedItems.Clear();

                foreach (DataGridViewRow row in this.dgvStockAdjustment.Rows)
                {
                    // make sure data exists in each row
                    if (row.Cells["ItemId"].Value.ToInt64() > 0)
                    {
                        // increment serial number by 1
                        SerialNo += 1;

                        this.objStockAdjustmentBLL.StockAdjustment.AdjustedItems.Add(new clsDTOAdjustedItem
                        {
                            SerialNo = SerialNo,
                            BatchID = row.Cells["cboBatchNo"].Tag.ToInt64(),
                            ItemID = row.Cells["ItemId"].Value.ToInt32(),
                            CurrentQuantity = row.Cells["CurrentQty"].Value.ToDouble(),
                            NewQuantity = row.Cells["NewQty"].Value.ToDouble(),
                            Rate = row.Cells["LIFORate"].Value.ToDouble()
                        });
                    }
                }

                this.objStockAdjustmentBLL.StockAdjustment.AdjustedLocationDetails.Clear();

                foreach (DataGridViewRow row in this.dgvLocationDetails.Rows)
                {
                    // make sure data exists in each row
                    if (row.Cells["LItemId"].Value.ToInt32() > 0)
                    {
                        // increment serial number by 1
                        SerialNo += 1;

                        this.objStockAdjustmentBLL.StockAdjustment.AdjustedLocationDetails.Add(new clsDTOAdjustedLocationDetails
                        {
                            SerialNo = SerialNo,
                            BatchID = row.Cells["LBatchID"].Value.ToInt64(),
                            ItemID = row.Cells["LItemId"].Value.ToInt32(),
                            CurrentQuantity = row.Cells["LCurrentQty"].Value.ToDouble(),
                            NewQuantity = row.Cells["LNewQty"].Value.ToDouble(),
                            LocationID = row.Cells["LLocation"].Tag.ToInt32(),
                            RowID = row.Cells["LRow"].Tag.ToInt32(),
                            BlockID = row.Cells["LBlock"].Tag.ToInt32(),
                            LotID = row.Cells["LLot"].Tag.ToInt32()
                        });
                    }
                }

                if (this.mode == eMode.Insert)
                    Success = this.objStockAdjustmentBLL.InsertStockAdjustments();
                else
                {
                    // updated mode
                    this.objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID = (long)this.StockAdjustmentID;
                    this.objStockAdjustmentBLL.StockAdjustment.ModifiedBY = ClsCommonSettings.UserID;
                    Success = this.objStockAdjustmentBLL.UpdateStockAdjustments();
                }

                // returns true if database updation is successful otherwise false.
                return Success;
            }
            catch (Exception Ex)
            {
                this.Logger.WriteLog(string.Format("Error on FormSave() Form Name : {0}, Error : {1}", this.Name, Ex.ToString()), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show(Ex.ToString());

                return false;
            }
        }

        private bool FormValidation()
        {
            if (this.cboCompany.SelectedIndex == -1)
            {
                this.ShowMessage(this.cboCompany, 1406, true);
                return false;
            }

            if (this.cboWarehouse.SelectedIndex == -1)
            {
                this.ShowMessage(this.cboWarehouse, 1401, true);
                return false;
            }

            if (this.cboReason.Text.Trim() == string.Empty)
            {
                this.ShowMessage(this.cboReason, 1402, true);
                return false;
            }

            if (this.txtAdjustmentNo.Text.Trim() == string.Empty)
            {
                this.ShowMessage(this.txtAdjustmentNo, 1403, true);
                return false;
            }
            else
            {
                // validate duplication
                if (this.mode == eMode.Update)
                    this.objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID = (long)this.StockAdjustmentID;
                else
                    this.objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID = 0;

                this.objStockAdjustmentBLL.StockAdjustment.AdjustmentNo = this.txtAdjustmentNo.Text.Trim();

                if (objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID == 0  && objStockAdjustmentBLL.IsAdjustmentNumberExists())
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9002, out MmessageIcon);
                    MsMessageCommon = MsMessageCommon.Replace("*", "Adjustment");
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    else
                        this.txtAdjustmentNo.Text = objStockAdjustmentBLL.GenerateAdjustmentNo(cboCompany.SelectedValue.ToInt32());
                }
                else if (objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID > 0 && this.objStockAdjustmentBLL.IsAdjustmentNumberExists())
                {
                    MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 1411, out MmessageIcon).Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return false;
                }
            }

            if (this.dgvStockAdjustment.Rows.Count < 1)
            {
                this.ShowMessage(this.dgvStockAdjustment, 1405, false);
                return false;
            }
            else
            {
                this.dgvStockAdjustment.CurrentCell = dgvStockAdjustment[5, 0];
            }

            bool BatchSelected = false;
            bool ItemSelected = false;

            int ErrorRow = 0;


            List<clsDTOAdjustedItem> AdjustedItems = new List<clsDTOAdjustedItem>();
            clsDTOAdjustedItem AdjustedItem;
            for (int i = 0; i <= dgvStockAdjustment.RowCount - 2; i++)
            {
                AdjustedItem = new clsDTOAdjustedItem();

                ItemSelected = BatchSelected = false;
                ErrorRow = i + 1;

                if (this.dgvStockAdjustment.Rows[i].Cells["ItemID"].Value.ToInt64() <= 0)
                {
                    ItemSelected = false;
                    this.dgvStockAdjustment.Rows[i].Cells["ItemCode"].Selected = true;
                    break;
                }
                else
                    ItemSelected = true;

                if (this.dgvStockAdjustment.Rows[i].Cells["cboBatchNo"] != null && ItemSelected)
                {
                    DataTable datCostingMethod = MobjClsBLLCommonUtility.FillCombos(new string[] { "DISTINCT CostingMethodID", "InvCostingAndPricingDetails", "ItemID = " + this.dgvStockAdjustment.Rows[i].Cells["ItemID"].Value.ToInt64() });
                    if (this.dgvStockAdjustment.Rows[i].Cells["cboBatchNo"].Tag.ToInt64() > 0 || (datCostingMethod != null && datCostingMethod.Rows[0]["CostingMethodID"].ToInt32() != (int)CostingMethodReference.Batch))
                    {
                        BatchSelected = true;

                        AdjustedItem.ItemID = this.dgvStockAdjustment.Rows[i].Cells["ItemID"].Value.ToInt32();
                        AdjustedItem.BatchID = this.dgvStockAdjustment.Rows[i].Cells["cboBatchNo"].Tag.ToInt64();

                        var items = AdjustedItems.Find(item => item.ItemID == AdjustedItem.ItemID && item.BatchID == AdjustedItem.BatchID);

                        if (items == null)
                            AdjustedItems.Add(AdjustedItem);
                        else
                        {
                            MessageBox.Show(string.Format(mObjNotification.GetErrorMessage(MaMessageArr, 1412, out MmessageIcon).Replace("#", ""), ErrorRow.ToString()), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.dgvStockAdjustment.Rows[i].Cells["cboBatchNo"].Selected = true;
                            return false;
                        }
                    }
                    else
                    {
                        this.dgvStockAdjustment.Rows[i].Cells["cboBatchNo"].Selected = true;
                        break;
                    }
                }
                //if (BatchSelected && ItemSelected)
                //{
                //    if (this.dgvStockAdjustment.Rows[i].Cells["NewQty"].Value.ToDecimal() > 0)
                //        ValueEntered = true;
                //    else
                //    {
                //        this.dgvStockAdjustment.Rows[i].Cells["NewQty"].Selected = true;
                //        break;
                //    }
                //}

                foreach (DataGridViewCell cell in this.dgvStockAdjustment.Rows[i].Cells)
                    cell.Selected = false;
            }

            if (!ItemSelected)
            {
                this.ShowMessage(this.dgvStockAdjustment, 1410, false, "*", ErrorRow.ToString());
                return false;
            }
            else if (!BatchSelected)
            {
                this.ShowMessage(this.dgvStockAdjustment, 1408, false, "*", ErrorRow.ToString());
                return false;
            }

           

            return true;
        }

        private void ShowMessage(Control control, int MessageCode, bool ShowErrorProvider)
        {
            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, MessageCode, out MmessageIcon);
            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            lblStockAdjustmentStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            TmrStatus.Enabled = true;
            control.Focus();

            if (ShowErrorProvider)
            {
                this.ErrorStatus.Clear();
                this.ErrorStatus.SetError(control, lblStockAdjustmentStatus.Text);
                control.Focus();
            }
        }

        private void ShowMessage(Control control, int MessageCode, bool ShowErrorProvider, string ReplaceCharacter, string ToBeReplacedWith)
        {
            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, MessageCode, out MmessageIcon);
            string Message = MsMessageCommon.Replace("#", string.Empty).Replace(ReplaceCharacter, ToBeReplacedWith).Trim();
            MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            lblStockAdjustmentStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            TmrStatus.Enabled = true;
            control.Focus();

            if (ShowErrorProvider)
            {
                this.ErrorStatus.Clear();
                this.ErrorStatus.SetError(control, lblStockAdjustmentStatus.Text);
                control.Focus();
            }
        }

        private void DisableEnableButtons(ControlState state)
        {
            this.BtnEmail.Enabled = (state == ControlState.Enable ? (BtnEmail.Enabled = false) : BtnEmail.Enabled = mbPrintPermission);
            this.BtnPrint.Enabled = (state == ControlState.Enable ? (BtnPrint.Enabled = false) : BtnPrint.Enabled = mbPrintPermission);
            this.BtnSave.Enabled =
            this.BtnOk.Enabled =
            this.BindingNavigatorSaveItem.Enabled = (state == ControlState.Enable ? ((MbAddPermission && this.mode == eMode.Insert) || (MbUpdatePermission && this.mode == eMode.Update)) : false);
            this.BindingNavigatorClearItem.Enabled = true;
        }

        private void FillBatchNoColumn(long ItemId)
        {
            try
            {
                DataTable dt = new DataTable();

                dt = MobjClsBLLCommonUtility.FillCombos(new string[] { "B.BatchID, B.BatchNo", "InvBatchDetails B INNER JOIN InvItemStockDetails S ON B.ItemID = S.ItemID AND B.BatchID = S.BatchID", string.Format("B.ItemID = {0} AND S.WarehouseID = {1}", ItemId, this.cboWarehouse.SelectedValue ?? 0) });

                if (dt.Rows.Count > 0)
                {
                    cboBatchNo.DataSource = null;
                    cboBatchNo.ValueMember = "BatchID";
                    cboBatchNo.DisplayMember = "BatchNo";
                    cboBatchNo.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog(string.Format("Error on FillBatchNoColumn() Form Name : {0}, Exception : {1}", this.Name, ex.ToString()), 3);
            }
        }


        private void BindData(long lngStockAdjustmentID)
        {
            try
            {
                this.BtnEmail.Enabled = true;
                bool success = this.objStockAdjustmentBLL.GetStockAdjustmentDetails(lngStockAdjustmentID);

                if (success)
                {
                    this.mode = eMode.Update;
                    this.cboCompany.Enabled = this.cboWarehouse.Enabled = false;
                  //  this.dgvStockAdjustment.Columns[ActualQty.Index].Visible = true;
                    this.StockAdjustmentID = this.objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID;


                    this.txtAdjustmentNo.Text = this.objStockAdjustmentBLL.StockAdjustment.AdjustmentNo;
                    this.txtRemarks.Text = this.objStockAdjustmentBLL.StockAdjustment.Remarks;
                    this.SetComboIndex(this.cboCompany, this.objStockAdjustmentBLL.StockAdjustment.CompanyID);

                    lblCreatedByText.Text = " Created By : " + this.objStockAdjustmentBLL.StockAdjustment.CreatedByName;
                    lblCreatedDateValue.Text = " Created Date : " + this.objStockAdjustmentBLL.StockAdjustment.CreatedDate.ToString("dd-MMM-yyyy");

                    if (this.cboCompany.SelectedValue == null)
                    {
                        DataTable datTemp = MobjClsBLLCommonUtility.FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + this.objStockAdjustmentBLL.StockAdjustment.CompanyID });
                        if (datTemp.Rows.Count > 0)
                            cboCompany.Text = datTemp.Rows[0]["Name"].ToString();
                        blnCheckStatus = true;
                    }
                    this.SetComboIndex(this.cboWarehouse, this.objStockAdjustmentBLL.StockAdjustment.WarehouseID);
                    if (this.cboWarehouse.SelectedValue == null)
                    {
                        DataTable datTemp = MobjClsBLLCommonUtility.FillCombos(new string[] { "*", "InvWarehouse", "WarehouseID = " + this.objStockAdjustmentBLL.StockAdjustment.WarehouseID });
                        if (datTemp.Rows.Count > 0)
                            cboWarehouse.Text = datTemp.Rows[0]["WarehouseName"].ToString();
                        blnCheckStatus = true;
                    }
                    this.SetComboIndex(this.cboReason, this.objStockAdjustmentBLL.StockAdjustment.ReasonID);
                    this.dtpAdjustmentDate.Text = this.objStockAdjustmentBLL.StockAdjustment.AdjustmentDate.ToString("dd-MMM-yyyy");

                    this.DefautCurrencyScale = ClsCommonUtility.GetCurrencyScaleByCompany(this.cboCompany.SelectedValue.ToInt32());
                    this.dgvStockAdjustment.Rows.Clear();
                    this.GridBinding = true;
                    int RowIndex = 0;

                    // Add details to grid
                    foreach (var AdjustedItem in objStockAdjustmentBLL.StockAdjustment.AdjustedItems)
                    {
                        this.dgvStockAdjustment.RowCount = dgvStockAdjustment.RowCount + 1;
                        RowIndex = this.dgvStockAdjustment.RowCount - 2;

                        this.dgvStockAdjustment.Rows[RowIndex].Cells["ItemID"].Value = AdjustedItem.ItemID;
                        this.dgvStockAdjustment.Rows[RowIndex].Cells["ItemName"].Value = AdjustedItem.ItemName;
                        this.dgvStockAdjustment.Rows[RowIndex].Cells["ItemCode"].Value = AdjustedItem.ItemCode;

                        // fill combo column
                        this.FillBatchNoColumn(AdjustedItem.ItemID);

                        this.dgvStockAdjustment.Rows[RowIndex].Cells["cboBatchNo"].Value = AdjustedItem.BatchID;
                        this.dgvStockAdjustment.Rows[RowIndex].Cells["cboBatchNo"].Tag = AdjustedItem.BatchID;
                        this.dgvStockAdjustment.Rows[RowIndex].Cells["cboBatchNo"].Value = this.dgvStockAdjustment.Rows[RowIndex].Cells["cboBatchNo"].FormattedValue;
                        this.dgvStockAdjustment.Rows[RowIndex].Cells["CurrentQty"].Value = AdjustedItem.CurrentQuantity.Format(3);
                        this.dgvStockAdjustment.Rows[RowIndex].Cells["NewQty"].Value = AdjustedItem.NewQuantity.Format(3);
                        this.dgvStockAdjustment.Rows[RowIndex].Cells["Difference"].Value = AdjustedItem.Difference.Format(3);
                        this.dgvStockAdjustment.Rows[RowIndex].Cells["LIFORate"].Value = AdjustedItem.Rate.CurrencyFormat(this.DefautCurrencyScale);
                        this.dgvStockAdjustment.Rows[RowIndex].Cells["ActualQty"].Value = AdjustedItem.ActualQty.Format(3);
                    }

                    //// Add Location details to grid
                    //this.dgvLocationDetails.Rows.Clear();
                    //this.GridBinding = true;
                    //RowIndex = 0;
                    //foreach (var AdjustedItem in objStockAdjustmentBLL.StockAdjustment.AdjustedLocationDetails)
                    //{
                    //    this.dgvLocationDetails.RowCount = dgvLocationDetails.RowCount + 1;
                    //    RowIndex = this.dgvLocationDetails.RowCount - 2;

                    //    this.dgvLocationDetails.Rows[RowIndex].Cells["LItemID"].Value = AdjustedItem.ItemID;
                    //    this.dgvLocationDetails.Rows[RowIndex].Cells["LItemName"].Value = AdjustedItem.ItemName;
                    //    this.dgvLocationDetails.Rows[RowIndex].Cells["LItemCode"].Value = AdjustedItem.ItemCode;

                    //    this.dgvLocationDetails.Rows[RowIndex].Cells["LBatchID"].Value = AdjustedItem.BatchID;
                    //    this.dgvLocationDetails.Rows[RowIndex].Cells["LBatchNo"].Value = AdjustedItem.BatchNo;
                    //    this.dgvLocationDetails.Rows[RowIndex].Cells["LCurrentQty"].Value = AdjustedItem.CurrentQuantity.Format(3);
                    //    this.dgvLocationDetails.Rows[RowIndex].Cells["LNewQty"].Value = AdjustedItem.NewQuantity.Format(3);

                    //    FillLocationGridCombo(LLocation.Index, RowIndex);
                    //    dgvLocationDetails.Rows[RowIndex].Cells["LLocation"].Value = AdjustedItem.LocationID;
                    //    dgvLocationDetails.Rows[RowIndex].Cells["LLocation"].Tag = AdjustedItem.LocationID;
                    //    dgvLocationDetails.Rows[RowIndex].Cells["LLocation"].Value = dgvLocationDetails.Rows[RowIndex].Cells["LLocation"].FormattedValue;

                    //    FillLocationGridCombo(LRow.Index, RowIndex);
                    //    dgvLocationDetails.Rows[RowIndex].Cells["LRow"].Value = AdjustedItem.RowID;
                    //    dgvLocationDetails.Rows[RowIndex].Cells["LRow"].Tag = AdjustedItem.RowID;
                    //    dgvLocationDetails.Rows[RowIndex].Cells["LRow"].Value = dgvLocationDetails.Rows[RowIndex].Cells["LRow"].FormattedValue;

                    //    FillLocationGridCombo(LBlock.Index, RowIndex);
                    //    dgvLocationDetails.Rows[RowIndex].Cells["LBlock"].Value = AdjustedItem.BlockID;
                    //    dgvLocationDetails.Rows[RowIndex].Cells["LBlock"].Tag = AdjustedItem.BlockID;
                    //    dgvLocationDetails.Rows[RowIndex].Cells["LBlock"].Value = dgvLocationDetails.Rows[RowIndex].Cells["LBlock"].FormattedValue;

                    //    FillLocationGridCombo(LLot.Index, RowIndex);
                    //    dgvLocationDetails.Rows[RowIndex].Cells["LLot"].Value = AdjustedItem.LotID;
                    //    dgvLocationDetails.Rows[RowIndex].Cells["LLot"].Tag = AdjustedItem.LotID;
                    //    dgvLocationDetails.Rows[RowIndex].Cells["LLot"].Value = dgvLocationDetails.Rows[RowIndex].Cells["LLot"].FormattedValue;

                    //    this.dgvLocationDetails.Rows[RowIndex].Cells["LActualQty"].Value = objStockAdjustmentBLL.GetItemLocationQuantity(AdjustedItem, cboWarehouse.SelectedValue.ToInt32()).Format(3);
                    //}
                }
               // ActualQty.Visible = LActualQty.Visible = true;
               // LItemName.Width = 150;
                this.DisableEnableButtons(ControlState.Disable);
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog(string.Format("Error on BindData() Form Name : {0}, Exception : {1} ", this.Name, ex.ToString()), 3);
            }
            finally
            {
                //this.GridBinding = false;
            }
        }

        private void SetComboIndex(ComboBox cbo, int value)
        {
            if (value > 0 && cbo.Items.Count > 0)
                cbo.SelectedValue = value.ToString();
            else
                cbo.SelectedIndex = -1;
        }

        private void SetStatusText(int MessageCode)
        {
            this.MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, MessageCode, out MmessageIcon);
            this.lblStockAdjustmentStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            this.TmrStatus.Enabled = true;
        }

        #endregion

        #region Events

        private void OnInputChanged(object sender, EventArgs args)
        {
            this.InputChanged = true;
            // clear error provider
            this.ErrorStatus.Clear();
            this.DisableEnableButtons(ControlState.Enable);
        }

        private void cboWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.dgvStockAdjustment.Enabled = this.cboWarehouse.SelectedIndex != -1;

                if (this.cboWarehouse.SelectedIndex == -1)
                    this.dgvStockAdjustment.Rows.Clear();

                if (this.WareHouseID != NullInt)
                {
                    if ((int)this.WareHouseID != Convert.ToInt32(this.cboWarehouse.SelectedValue))
                        this.dgvStockAdjustment.Rows.Clear();
                }

                this.WareHouseID = Convert.ToInt32(this.cboWarehouse.SelectedValue);
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog("Error on cboWarehouse_SelectedIndexChanged() " + ex.ToString(), 3);
            }
        }

        private void cboWarehouse_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = ((ComboBox)sender).DataSource as DataTable;

                if (this.cboWarehouse.Text.Trim() == string.Empty)
                    this.dgvStockAdjustment.Enabled = false;

                if (dt != null && dt.Select("WarehouseName = '" + this.cboWarehouse.Text + "'").Length == 0)
                    this.dgvStockAdjustment.Enabled = false;
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog(string.Format("Error on cboWarehouse_TextChanged() Form Name = '{0}', Exception = {1}", this.Name, ex.ToString()), 4);
            }
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboCompany.SelectedIndex != -1)
                {
                    int CompanyId = Convert.ToInt32(this.cboCompany.SelectedValue);
                    if (objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID == 0)
                    {
                        clsBLLLogin objBllLogin = new clsBLLLogin();
                        objBllLogin.GetCompanySettings(cboCompany.SelectedValue.ToInt32());
                        this.txtAdjustmentNo.Text = this.objStockAdjustmentBLL.GenerateAdjustmentNo(cboCompany.SelectedValue.ToInt32());
                  
                    if (ClsCommonSettings.blnStockAdjustmentNumberAutogenerate)
                        txtAdjustmentNo.ReadOnly = true;
                    else
                        txtAdjustmentNo.ReadOnly = false; 
                    }
                    clsUtilities.BindComboBox(this.cboWarehouse, eReferenceTables.WareHouse, NullInt, CompanyId);

                    this.DefautCurrencyScale = ClsCommonUtility.GetCurrencyScaleByCompany(CompanyId);
                }
                else
                    cboCompany.Text = "";
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog("Error on dgvStockAdjustment_Textbox_TextChanged() " + ex.ToString(), 4);
            }
        }

        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void cboWarehouse_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.cboWarehouse.DroppedDown = false;
        }

        private void cboReason_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboReason.DroppedDown = false;

        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            BindingNavigatorSaveItem_Click(sender, e);
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (FormValidation())
            {
                if (FormSave())
                {
                    this.InputChanged = false;
                    this.Close();
                }
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReason_Click(object sender, EventArgs e)
        {
            try
            {
                this.SelectedValue = this.cboReason.SelectedValue.ToInt32();
                new FrmCommonRef("Reason", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "StockAdjustmentReasonID,StockAdjustmentReason", "InvStockAdjustmentReasonReference", string.Empty).ShowDialog();
                //new ClsCommonUtility().FillCombos(this.cboReason, new string[] { "ReasonID, Description", "STStockAdjustmentReasonReference", string.Empty, "ReasonID", "Description" });
                clsUtilities.BindComboBox(this.cboReason, eReferenceTables.StockAdjustmentReason);
                // restore previous selection
                this.cboReason.SelectedValue = SelectedValue;
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog(string.Format("Error on btnReason_Click() Form Name : {0}, Exception : {1}", this.Name, ex.ToString()), 3);
            }
        }

        private void btnCompany_Click(object sender, EventArgs e)
        {
            try
            {
                this.SelectedValue = this.cboCompany.SelectedValue.ToInt32();
                new FrmCompany().ShowDialog();

                DataTable datCombos = new DataTable();
                if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    datCombos = new clsBLLPurchase().FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                else
                    datCombos = new clsBLLPurchase().GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.StAdjCompany);

                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.DataSource = datCombos;

                // restore previous selection
                this.cboCompany.SelectedValue = SelectedValue;
                if (this.cboCompany.SelectedValue == null)
                {
                    DataTable datTemp = new ClsCommonUtility().FillCombos(new string[] { "*", "CompanyMaster", "CompanyID = " + SelectedValue });
                    if (datTemp.Rows.Count > 0)
                        cboCompany.Text = datTemp.Rows[0]["CompanyName"].ToString();
                }
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog(string.Format("Error on btnCompany_Click() Form Name : {0}, Exception : {1}", this.Name, ex.ToString()), 3);
            }
        }

        private void btnWareHouse_Click(object sender, EventArgs e)
        {
            try
            {
                this.SelectedValue = this.cboWarehouse.SelectedValue.ToInt32();
                new FrmWarehouse().ShowDialog();
                //new ClsCommonUtility().FillCombos(this.cboWarehouse, new string[] { "WarehouseID, WarehouseName", "STWarehouse", string.Format("CompanyID = {0}", this.cboCompany.SelectedValue), "WarehouseID", "WarehouseName" });
                // restore previous selection
                this.cboWarehouse.SelectedValue = SelectedValue;
                if (this.cboWarehouse.SelectedValue == null)
                {
                    DataTable datTemp = new ClsCommonUtility().FillCombos(new string[] { "*", "InvWarehouse", "WarehouseID = " + SelectedValue });
                    if (datTemp.Rows.Count > 0)
                        cboWarehouse.Text = datTemp.Rows[0]["WarehouseName"].ToString();
                }
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog(string.Format("Error on btnCompany_Click() Form Name : {0}, Exception : {1}", this.Name, ex.ToString()), 3);
            }
        }

        private void TextboxNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
            if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
            {
                e.Handled = true;
            }
            if (((TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            {
                e.Handled = true;
            }
        }

        private void TmrStatus_Tick(object sender, EventArgs e)
        {
            lblStockAdjustmentStatus.Text = "";
            TmrStatus.Enabled = false;
        }

        private void CancelToolStripButton_Click(object sender, EventArgs e)
        {
            this.AddNewRecord();
        }

        #endregion

        #region Data Grid Events

        private void dgvStockAdjustment_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            int ColumnIndex = Convert.ToInt32((((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex));

            e.Control.KeyPress -= this.TextboxNumeric_KeyPress;
            if (ColumnIndex == NewQty.Index)
            {
                e.Control.KeyPress += this.TextboxNumeric_KeyPress;
            }
        }

        private void dgvStockAdjustment_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                if (this.mode == eMode.Update)
                {
                    e.Cancel = true;
                    return;
                }

                if (e.RowIndex >= 0 && e.ColumnIndex > 0)
                {
                    this.dgvStockAdjustment.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    if (this.dgvStockAdjustment.Rows[e.RowIndex].Cells["ItemID"].Value != null)
                    {
                        Int64 _ItemID = this.dgvStockAdjustment.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt64();
                        //fill batch combo only if costing method of current item is 'Batch' (CostingMethodID : 3|Batch )
                        DataTable datCostingMethod = MobjClsBLLCommonUtility.FillCombos(new string[] { "DISTINCT CostingMethodID", "InvCostingAndPricingDetails", "ItemID = " + _ItemID });
                        if (datCostingMethod != null && datCostingMethod.Rows.Count > 0)
                        {
                            if (e.ColumnIndex == cboBatchNo.Index)
                            {
                                int tag = -1;

                                if (this.dgvStockAdjustment.CurrentRow.Cells["cboBatchNo"].Tag != null)
                                    tag = this.dgvStockAdjustment.CurrentRow.Cells["cboBatchNo"].Tag.ToInt32();


                                if (datCostingMethod.Rows[0]["CostingMethodID"].ToInt32() == (int)CostingMethodReference.Batch)
                                {
                                    this.FillBatchNoColumn(_ItemID);

                                    if (tag != -1)
                                        this.dgvStockAdjustment.CurrentRow.Cells["cboBatchNo"].Tag = tag;
                                }
                                else
                                {
                                    e.Cancel = true;
                                    //Int64 lngBatchID = 0;
                                    //int intItemID = _ItemID.ToInt32();
                                    //int intWarehouseID = this.cboWarehouse.SelectedValue.ToInt32();
                                    //decimal decZero = 0;

                                    //using (DataTable dt = this.objStockAdjustmentBLL.GetRateAndCurrentStock(lngBatchID, intItemID, intWarehouseID))
                                    //{
                                    //    if (dt.Rows.Count > 0)
                                    //    {
                                    //        decimal LifoRate = dt.Rows[0]["Rate"].ToDecimal();
                                    //        decimal CurrentStock = dt.Rows[0]["GRNQuantity"].ToDecimal();
                                    //        this.dgvStockAdjustment.CurrentRow.Cells["CurrentQty"].Value = CurrentStock.Format(3);
                                    //        this.dgvStockAdjustment.CurrentRow.Cells["ActualQty"].Value = CurrentStock.Format(3);
                                    //        this.dgvStockAdjustment.CurrentRow.Cells["LIFORate"].Value = LifoRate.CurrencyFormat(this.DefautCurrencyScale);
                                    //        this.dgvStockAdjustment.CurrentRow.Cells["NewQty"].Value = decZero.Format(3);
                                    //        decimal decDifference = decZero - CurrentStock;

                                    //        this.dgvStockAdjustment.Rows[e.RowIndex].Cells[Difference.Index].Value = decDifference.Format(3);
                                    //    }
                                    //}
                                }
                            }

                            else if (e.ColumnIndex == NewQty.Index)
                            {
                                if (dgvStockAdjustment[cboBatchNo.Index, e.RowIndex].Tag.ToInt64() == 0 && datCostingMethod.Rows[0]["CostingMethodID"].ToInt32() == (int)CostingMethodReference.Batch)
                                    e.Cancel = true;
                            }
                        }
                    }
                    else
                        cboBatchNo.DataSource = null;

                    //if (e.RowIndex == 1 || e.RowIndex == 0)
                    //{
                    //    this.dgvStockAdjustment.Rows[e.RowIndex].Cells["ItemID"].Value = null;
                    //}
                }
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog(string.Format("Error on dgvStockAdjustment_CellBeginEdit() Form Name : {0}, Error : {1}", this.Name, ex.ToString()), 3);
            }
        }

        private void dgvStockAdjustment_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { return; }
            catch { }
        }

        private void dgvStockAdjustment_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    if (e.ColumnIndex == 2)
                    {
                        if (this.dgvStockAdjustment.CurrentRow != null && this.dgvStockAdjustment.CurrentRow.Cells["cboBatchNo"].Value != null)
                        {
                            this.dgvStockAdjustment.CurrentRow.Cells["cboBatchNo"].Tag = this.dgvStockAdjustment.CurrentRow.Cells["cboBatchNo"].Value;
                            this.dgvStockAdjustment.CurrentRow.Cells["cboBatchNo"].Value = this.dgvStockAdjustment.CurrentRow.Cells["cboBatchNo"].FormattedValue;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog(string.Format("Error on dgvStockAdjustment_CellEndEdit() Form Name : {0}, Error : {1}", this.Name, ex.ToString()), 3);
            }
        }

        private void dgvStockAdjustment_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                // enable save and ok buttons
                this.OnInputChanged(new object(), new EventArgs());

                decimal decZero = 0;
                if (e.ColumnIndex == ItemCode.Index || e.ColumnIndex == ItemName.Index)
                {
                    if (this.dgvStockAdjustment.CurrentRow != null && !GridBinding)
                    {
                        this.cboBatchNo.DataSource = null;
                        this.dgvStockAdjustment.CurrentRow.Cells["cboBatchNo"].Value = null;
                        this.dgvStockAdjustment.CurrentRow.Cells["cboBatchNo"].Tag = null;
                        this.dgvStockAdjustment.CurrentRow.Cells["CurrentQty"].Value = decZero.Format(3);
                        this.dgvStockAdjustment.CurrentRow.Cells["ActualQty"].Value = decZero.Format(3);
                        this.dgvStockAdjustment.CurrentRow.Cells["LIFORate"].Value = decZero.CurrencyFormat(this.DefautCurrencyScale);
                        this.dgvStockAdjustment.CurrentRow.Cells["NewQty"].Value = decZero.Format(3);
                        this.dgvStockAdjustment.CurrentRow.Cells["Difference"].Value = decZero.Format(3);
                    }
                }

                if (e.ColumnIndex == cboBatchNo.Index && e.RowIndex >= 0)
                {
                    if (dgvStockAdjustment["ItemCode", e.RowIndex].Value != null)
                    {
                        //if (dgvStockAdjustment.CurrentRow.Cells["cboBatchNo"].Value != null && objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID ==0)
                        if (objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID == 0)
                        {
                            Int64 lngBatchID = this.dgvStockAdjustment.CurrentRow.Cells["cboBatchNo"].Tag.ToInt64();
                            int intItemID = this.dgvStockAdjustment.CurrentRow.Cells["ItemID"].Value.ToInt32();
                            int intWarehouseID = this.cboWarehouse.SelectedValue.ToInt32();

                            using (DataTable dt = this.objStockAdjustmentBLL.GetRateAndCurrentStock(lngBatchID, intItemID, intWarehouseID))
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    decimal LifoRate = dt.Rows[0]["Rate"].ToDecimal();
                                    decimal CurrentStock = dt.Rows[0]["GRNQuantity"].ToDecimal();
                                    this.dgvStockAdjustment.CurrentRow.Cells["CurrentQty"].Value = CurrentStock.Format(3);
                                    this.dgvStockAdjustment.CurrentRow.Cells["ActualQty"].Value = CurrentStock.Format(3);
                                    this.dgvStockAdjustment.CurrentRow.Cells["LIFORate"].Value = LifoRate.CurrencyFormat(this.DefautCurrencyScale);
                                    this.dgvStockAdjustment.CurrentRow.Cells["NewQty"].Value = decZero.Format(3);
                                    decimal decDifference = decZero - CurrentStock;

                                    this.dgvStockAdjustment.Rows[e.RowIndex].Cells[Difference.Index].Value = decDifference.Format(3);
                                }
                            }
                        }
                    }
                }
                else if (e.ColumnIndex == NewQty.Index && e.RowIndex >= 0)
                {
                    // New Quantity column
                    decimal NewQuantity = this.dgvStockAdjustment.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToDecimal();
                    decimal CurrentQty = this.dgvStockAdjustment.Rows[e.RowIndex].Cells[e.ColumnIndex - 1].Value.ToDecimal();
                    decimal Difference = NewQuantity - CurrentQty;

                    this.dgvStockAdjustment.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = NewQuantity.Format(3);
                    this.dgvStockAdjustment.Rows[e.RowIndex].Cells[e.ColumnIndex + 1].Value = Difference.Format(3);
                }
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog(string.Format("Error on dgvStockAdjustment_CellValueChanged() Form Name : {0}, Error : {1}", this.Name, ex.ToString()), 3);
            }
        }

        private void dgvStockAdjustment_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (this.dgvStockAdjustment.IsCurrentCellDirty)
            {
                if (this.dgvStockAdjustment.CurrentCell != null)
                    this.dgvStockAdjustment.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
        }

        private void dgvStockAdjustment_Enter(object sender, EventArgs e)
        {
            //this.dgvStockAdjustment.CurrentCell = this.dgvStockAdjustment[2, 0];
        }

        private void dgvStockAdjustment_Leave(object sender, EventArgs e)
        {
            this.dgvStockAdjustment.ClearSelection();
        }

        private void dgvStockAdjustment_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.dgvStockAdjustment.PServerName = ClsCommonSettings.ServerName;
                this.dgvStockAdjustment.aryFirstGridParam = new string[] { "ItemCode", "ItemName", "ItemID" };
                this.dgvStockAdjustment.arySecondGridParam = new string[] { "ItemCode", "Description", "ItemID" };
                this.dgvStockAdjustment.PiFocusIndex = NewQty.Index;
                this.dgvStockAdjustment.iGridWidth = 350;
                this.dgvStockAdjustment.bBothScrollBar = true;
                this.dgvStockAdjustment.ColumnsToHide = new string[] { "ItemID" };
                dgvStockAdjustment.pnlLeft = expandableSplitterLeft.Location.X + 5;
                dgvStockAdjustment.pnlTop = expandableSplitter1.Location.Y + BindingNavigatorStockAdjustment.Height + 10;
                this.dgvStockAdjustment.field = this.dgvStockAdjustment.CurrentCell.ColumnIndex == 0 ? "Code" : (this.dgvStockAdjustment.CurrentCell.ColumnIndex == 1 ? "ItemName" : string.Empty);

                this.dgvStockAdjustment.PsQuery = string.Format("SELECT DISTINCT [Code] AS ItemCode, [ItemName] AS Description, M.[ItemID] AS ItemID " +
                                                                    " FROM InvItemMaster M " +
                                                                    " INNER JOIN InvItemStockMaster S ON M.ItemID = S.ItemID " +
                                                                    " INNER JOIN InvItemStockDetails D ON D.ItemID = S.ItemID " +
                                                                    " WHERE upper({0}) LIKE upper('{1}%') {2}",
                                                                    this.dgvStockAdjustment.field,
                                                                    this.dgvStockAdjustment.TextBoxText.Replace("'", string.Empty),
                                                                    this.cboWarehouse.SelectedIndex != -1 ? " AND D.WarehouseID = " + this.cboWarehouse.SelectedValue+" Order by "+dgvStockAdjustment.field+"" : string.Empty
                                                                );
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog("Error on dgvStockAdjustment_Textbox_TextChanged() " + ex.ToString(), 4);
            }
        }

        private void dgvStockAdjustment_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == 0) || (e.ColumnIndex == 1))
            {
                this.dgvStockAdjustment.PiColumnIndex = e.ColumnIndex;
            }
        }

        #endregion

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {

            if (FillItemLocationDetails() )//&& ValidateLocationGrid())
            {
                if (this.FormValidation())
                {
                    if (this.FormSave())
                    {
                        this.MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                        this.lblStockAdjustmentStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        MessageBox.Show(this.lblStockAdjustmentStatus.Text, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        this.TmrStatus.Enabled = true;
                        this.AddNewRecord();
                    }
                }
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewRecord();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {

                // first ask confirmation to the user
                if (MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 4268, out MmessageIcon).Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (this.StockAdjustmentID > 0)
                    {
                        this.objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID = (long)this.StockAdjustmentID;
                        this.objStockAdjustmentBLL.StockAdjustment.ModifiedBY = ClsCommonSettings.UserID;
                        bool success = this.objStockAdjustmentBLL.DeleteStockAdjustment();

                        if (success)
                        {
                            this.SetStatusText(4);
                            MessageBox.Show("Information deleted successfully", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.AddNewRecord();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog(string.Format("Error on BindingNavigatorDeleteItem_Click() Form Name : {0}, Exception : {1} ", this.Name, ex.ToString()), 3);
            }

        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void LoadReport()
        {
            try
            {
              //  if (MintStockAdjustmentID > 0)
                if (objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID > 0)

                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();

                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID;  //StockAdjustmentID  objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID
                    ObjViewer.PiFormID = (int)FormID.StockAdjustment;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form Print:LoadReport() " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadReport() " + Ex.Message.ToString());
            }
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
            //    if (MintStockAdjustmentID > 0)
                if (objStockAdjustmentBLL.StockAdjustment.StockAdjustmentID > 0)

                {
                    using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                    {
                        ObjEmailPopUp.MsSubject = "Stock Adjustment";
                        ObjEmailPopUp.EmailFormType = EmailFormID.StockAdjustment;

                        ObjEmailPopUp.EmailSource = objStockAdjustmentBLL.DisplayStockAdjustmentEmail(intStockAdjustment);
                        ObjEmailPopUp.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnEmail_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in bnEmail_Click() " + ex.Message, 2);
            }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "StockAdjustment";
            objHelp.ShowDialog();
            objHelp = null;
        
        }

        private void dgvStockAdjustmentDisplay_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (dgvStockAdjustmentDisplay["StockAdjustmentID", e.RowIndex].Value != null)
                    BindData(dgvStockAdjustmentDisplay["StockAdjustmentID", e.RowIndex].Value.ToInt64());

                cboReason.Enabled = false;
                btnReason.Enabled = false;
                txtAdjustmentNo.Enabled = false;
                dtpAdjustmentDate.Enabled = false;
                BindingNavigatorAddNewItem.Enabled = true;
                BindingNavigatorClearItem.Enabled = false;
            }
        }

        private void btnSRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                DisplayAllNoInSearchGrid();
               
            }
            catch { }
        }

        private bool DisplayAllNoInSearchGrid()
        {
            try
            {
                //Filling Search Grid
                DataTable dtAdjustments = objStockAdjustmentBLL.GetAllAdjustments();
                DataTable dtTemp = null;
                string strFilterCondition = string.Empty;
                if (!string.IsNullOrEmpty(txtSAdjustmentNo.Text.Trim()))
                {
                    strFilterCondition = "AdjustmentNo = '" + txtSAdjustmentNo.Text.Trim() + "'";
                }
                else
                {
                    if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                        strFilterCondition += " CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue);
                    else
                    {
                        dtTemp = null;
                        dtTemp = (DataTable)cboSCompany.DataSource;
                        if (dtTemp.Rows.Count > 0)
                        {
                            strFilterCondition += "CompanyID In (";
                            foreach (DataRow dr in dtTemp.Rows)
                            {
                                strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                            }
                            strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                            strFilterCondition += ")";
                        }
                    }

                    if (cboSEmployee.SelectedValue != null && Convert.ToInt32(cboSEmployee.SelectedValue) != -2)
                    {
                        if (!string.IsNullOrEmpty(strFilterCondition))
                            strFilterCondition += " And ";
                        strFilterCondition += "EmployeeID = " + Convert.ToInt32(cboSEmployee.SelectedValue);
                    }
                    else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                    {
                        //if (cboSEmployee.SelectedValue != null && Convert.ToInt32(cboSEmployee.SelectedValue) != -2)
                        //{
                            dtTemp = null;
                            dtTemp = (DataTable)cboSEmployee.DataSource;
                            if (dtTemp.Rows.Count > 1)
                            {
                                clsBLLPermissionSettings MobjClsBLLPermissionSettings = new clsBLLPermissionSettings();
                                DataTable datTempPer = MobjClsBLLPermissionSettings.GetControlPermissions1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.StAdjEmployee);
                                if (datTempPer != null)
                                {
                                    if (datTempPer.Rows.Count > 0)
                                    {
                                        if (datTempPer.Rows[0]["IsVisible"].ToString() == "True")
                                            strFilterCondition += "And EmployeeID In (0,";
                                        else
                                            strFilterCondition += "And EmployeeID In (";
                                    }
                                    else
                                        strFilterCondition += "And EmployeeID In (";
                                }
                                else
                                    strFilterCondition += "And EmployeeID In (";

                                foreach (DataRow dr in dtTemp.Rows)
                                {
                                    strFilterCondition += Convert.ToInt32(dr["EmployeeID"]) + ",";
                                }
                                strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                                strFilterCondition += ")";
                            }
                        //}
                    }
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += " AdjustmentDate >= '" + dtpSFrom.Value.ToString("dd-MMM-yyyy") + "' And AdjustmentDate <= '" + dtpSTo.Value.ToString("dd-MMM-yyyy") + "' ";

                }
                dtAdjustments.DefaultView.RowFilter = strFilterCondition;

                dgvStockAdjustmentDisplay.DataSource = null;
                dgvStockAdjustmentDisplay.DataSource = dtAdjustments.DefaultView.ToTable();
                dgvStockAdjustmentDisplay.Columns["StockAdjustmentID"].Visible = false;
                dgvStockAdjustmentDisplay.Columns["CompanyID"].Visible = false;
                dgvStockAdjustmentDisplay.Columns["EmployeeID"].Visible = false;
                dgvStockAdjustmentDisplay.Columns["CreatedDate"].Visible = false;
                dgvStockAdjustmentDisplay.Columns["AdjustmentNo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                lblStockAdjustmentCount.Text = "Total Adjustments : " + dgvStockAdjustmentDisplay.Rows.Count.ToString();
                return true;
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Error on DisplayAllNoInSearchGrid() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DisplayAllNoInSearchGrid()" + ex.Message.ToString());
                return false;
            }
        }


        private bool FillLocationGridCombo(int inColumnIndex, int intRowIndex)
        {
            try
            {
                DataTable dtLocation = new DataTable();
                int intTag = 0;
                if (inColumnIndex == LLocation.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32();
                    dtLocation = MobjClsBLLCommonUtility.FillCombos(new string[] { "Distinct LocationID,Location", "InvWarehouseDetails", "WarehouseID=" + cboWarehouse.SelectedValue.ToString() });

                    LLocation.DataSource = null;
                    LLocation.ValueMember = "LocationID";
                    LLocation.DisplayMember = "Location";
                    LLocation.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].FormattedValue;

                }
                else if (inColumnIndex == LRow.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag.ToInt32();
                    dtLocation = MobjClsBLLCommonUtility.FillCombos(new string[] { "Distinct  RowID, RowNumber", "InvWarehouseDetails", "WarehouseID=" + cboWarehouse.SelectedValue.ToString() + " And LocationID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32() });
                    LRow.DataSource = null;
                    LRow.ValueMember = "RowID";
                    LRow.DisplayMember = "RowNumber";
                    LRow.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].FormattedValue;
                }
                else if (inColumnIndex == LBlock.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag.ToInt32();
                    dtLocation = MobjClsBLLCommonUtility.FillCombos(new string[] { "Distinct BlockID,BlockNumber", "InvWarehouseDetails", " WarehouseID=" + cboWarehouse.SelectedValue.ToString() + " And LocationID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32() + " And RowID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag.ToInt32() });
                    LBlock.DataSource = null;
                    LBlock.ValueMember = "BlockID";
                    LBlock.DisplayMember = "BlockNumber";
                    LBlock.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].FormattedValue;
                }
                else if (inColumnIndex == LLot.Index)
                {
                    intTag = dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Tag.ToInt32();

                    dtLocation = MobjClsBLLCommonUtility.FillCombos(new string[] { " Distinct LotID, LotNumber", "InvWarehouseDetails", "WarehouseID=" + cboWarehouse.SelectedValue.ToString() + " And LocationID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LLocation.Index].Tag.ToInt32() + " And RowID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LRow.Index].Tag.ToInt32() + " And BlockID=" + dgvLocationDetails.Rows[intRowIndex].Cells[LBlock.Index].Tag.ToInt32() });
                    LLot.DataSource = null;
                    LLot.ValueMember = "LotID";
                    LLot.DisplayMember = "LotNumber";
                    LLot.DataSource = dtLocation;

                    dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Tag = intTag;
                    dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].Value = dgvLocationDetails.Rows[intRowIndex].Cells[LLot.Index].FormattedValue;
                }

                return (dtLocation.Rows.Count > 0);

            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on FillLocationGridCombo(): " + this.Name + " " + Ex.Message.ToString(), 2);
                return false;

            }
        }

        private bool FillItemLocationDetails()
        {

            if (FormValidation())
            {
                //for (int i = 0; i < dgvLocationDetails.Rows.Count; i++)
                //{

                //    if (dgvLocationDetails.Rows[i].Cells["LItemID"].Value != null && dgvLocationDetails.Rows[i].Cells["LBatchID"].Value != null)
                //    {
                //        bool blnExists = false;
                //        foreach (DataGridViewRow dr in dgvStockAdjustment.Rows)
                //        {
                //            if (dr.Cells["ItemID"].Value != null && dr.Cells["cboBatchNo"].Tag != null)
                //            {
                //                if (dr.Cells["ItemID"].Value.ToInt32() == dgvLocationDetails.Rows[i].Cells["LItemID"].Value.ToInt32() && dr.Cells["cboBatchNo"].Tag.ToInt64() == dgvLocationDetails.Rows[i].Cells["LBatchID"].Value.ToInt64())
                //                {
                //                    blnExists = true;
                //                    break;
                //                }
                //            }
                //        }
                //        if (!blnExists)
                //        {
                //            dgvLocationDetails.Rows.RemoveAt(i);
                //            i--;
                //        }
                //    }
                //}
                dgvLocationDetails.Rows.Clear();
                //bool blnIsValid = true;
                //foreach (DataGridViewRow dr in dgvStockAdjustment.Rows)
                //{
                //    if (dr.Cells[ItemID.Index].Value.ToInt32() != 0 && dr.Cells[cboBatchNo.Index].Tag.ToInt64() != 0)
                //    {
                //        decimal decDifference = dr.Cells["Difference"].Value.ToDecimal();
                //        decimal decLocationDifference = 0;
                //        foreach (DataGridViewRow drRow in dgvLocationDetails.Rows)
                //        {
                //            if (dr.Cells[ItemID.Index].Value.ToInt32() == drRow.Cells[LItemID.Index].Value.ToInt32() && dr.Cells[cboBatchNo.Index].Tag.ToInt64() == drRow.Cells[LBatchID.Index].Value.ToInt64())
                //                decLocationDifference += drRow.Cells[LDifference.Index].Value.ToDecimal();
                //        }
                //        if (decLocationDifference != decDifference)
                //        {
                //            blnIsValid = false;
                //            break;
                //        }
                //    }
                //}

                //if (!blnIsValid)
                //{
                //    if (this.mode != eMode.Update)
                //    {
                        if (cboWarehouse.SelectedValue.ToInt32() != 0)
                        {
                            if (dgvLocationDetails.Rows.Count == 1)
                            {
                                dgvLocationDetails.Rows.Clear(); dtItemDetails.Rows.Clear();
                                foreach (DataGridViewRow drPurchaseRow in dgvStockAdjustment.Rows)
                                {

                                    if (drPurchaseRow.Cells["ItemID"].Value != null && drPurchaseRow.Cells["cboBatchNo"].Tag != null)
                                    {

                                        DataRow drItemDetailsRow = dtItemDetails.NewRow();
                                        drItemDetailsRow["ItemID"] = drPurchaseRow.Cells["ItemID"].Value.ToInt32();
                                        drItemDetailsRow["ItemCode"] = drPurchaseRow.Cells["ItemCode"].Value.ToString();
                                        drItemDetailsRow["ItemName"] = drPurchaseRow.Cells["ItemName"].Value.ToString();
                                        drItemDetailsRow["BatchNo"] = drPurchaseRow.Cells["cboBatchNo"].Value.ToString();
                                        drItemDetailsRow["BatchID"] = drPurchaseRow.Cells["cboBatchNo"].Tag.ToInt32();
                                        dtItemDetails.Rows.Add(drItemDetailsRow);

                                        dgvLocationDetails.RowCount = dgvLocationDetails.RowCount + 1;
                                        int intRowIndex = dgvLocationDetails.RowCount - 2;
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value = drPurchaseRow.Cells["ItemID"].Value.ToInt32();
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LItemCode"].Value = drPurchaseRow.Cells["ItemCode"].Value.ToString();
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LItemName"].Value = drPurchaseRow.Cells["ItemName"].Value.ToString();
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LBatchID"].Value = drPurchaseRow.Cells["cboBatchNo"].Tag.ToInt32();
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LBatchNo"].Value = drPurchaseRow.Cells["cboBatchNo"].Value.ToString();



                                        DataRow dr = objStockAdjustmentBLL.GetItemDefaultLocation(drPurchaseRow.Cells["ItemID"].Value.ToInt32(), cboWarehouse.SelectedValue.ToInt32());

                                        FillLocationGridCombo(LLocation.Index, intRowIndex);
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dr["LocationID"].ToInt32();
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag = dr["LocationID"].ToInt32();
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].FormattedValue;

                                        FillLocationGridCombo(LRow.Index, intRowIndex);
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dr["RowID"].ToInt32();
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag = dr["RowID"].ToInt32();
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].FormattedValue;

                                        FillLocationGridCombo(LBlock.Index, intRowIndex);
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dr["BlockID"].ToInt32();
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag = dr["BlockID"].ToInt32();
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].FormattedValue;

                                        FillLocationGridCombo(LLot.Index, intRowIndex);
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dr["LotID"].ToInt32();
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag = dr["LotID"].ToInt32();
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].FormattedValue;

                                        clsDTOAdjustedLocationDetails clsLocationDetails = new clsDTOAdjustedLocationDetails();
                                        clsLocationDetails.ItemID = dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value.ToInt32();
                                        clsLocationDetails.BatchID = dgvLocationDetails.Rows[intRowIndex].Cells["LBatchID"].Value.ToInt32();
                                        clsLocationDetails.LocationID = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag.ToInt32();
                                        clsLocationDetails.RowID = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag.ToInt32();
                                        clsLocationDetails.BlockID = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag.ToInt32();
                                        clsLocationDetails.LotID = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag.ToInt32();

                                        dgvLocationDetails.Rows[intRowIndex].Cells["LCurrentQty"].Value = objStockAdjustmentBLL.GetItemLocationQuantity(clsLocationDetails, cboWarehouse.SelectedValue.ToInt32());
                                        dgvLocationDetails.Rows[intRowIndex].Cells["LNewQty"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LCurrentQty"].Value.ToDecimal() + drPurchaseRow.Cells["Difference"].Value.ToDecimal();

                                        //dgvLocationDetails.Rows.Add();
                                    }
                                }
                            }
                            //else
                            //{
                            //    dtItemDetails.Rows.Clear();
                            //    foreach (DataGridViewRow drPurchaseRow in dgvStockAdjustment.Rows)
                            //    {
                            //        bool blnIsExists = false;
                            //        decimal decDifference = 0;
                            //        if (drPurchaseRow.Cells["ItemID"].Value != null && drPurchaseRow.Cells["cboBatchNo"].Tag != null)
                            //        {
                            //            DataRow drItemDetailsRow = dtItemDetails.NewRow();
                            //            drItemDetailsRow["ItemID"] = drPurchaseRow.Cells["ItemID"].Value.ToInt32();
                            //            drItemDetailsRow["ItemCode"] = drPurchaseRow.Cells["ItemCode"].Value.ToString();
                            //            drItemDetailsRow["ItemName"] = drPurchaseRow.Cells["ItemName"].Value.ToString();
                            //            drItemDetailsRow["BatchNo"] = drPurchaseRow.Cells["cboBatchNo"].Value.ToString();
                            //            drItemDetailsRow["BatchID"] = drPurchaseRow.Cells["cboBatchNo"].Tag.ToInt32();
                            //            dtItemDetails.Rows.Add(drItemDetailsRow);

                            //            foreach (DataGridViewRow drLocationRow in dgvLocationDetails.Rows)
                            //            {
                            //                if (drLocationRow.Cells["LItemID"].Value != null && drLocationRow.Cells["LBatchID"].Value != null)
                            //                {
                            //                    if (drPurchaseRow.Cells["ItemID"].Value.ToInt32() == drLocationRow.Cells["LItemID"].Value.ToInt32() && drPurchaseRow.Cells["cboBatchNo"].Tag.ToInt64() == drLocationRow.Cells["LBatchID"].Value.ToInt64())
                            //                    {
                            //                        blnIsExists = true;
                            //                        decDifference += drLocationRow.Cells["LDifference"].Value.ToDecimal();
                            //                    }
                            //                }
                            //            }
                            //            if (!blnIsExists)
                            //            {
                            //                //  dgvLocationDetails.Rows.Add();
                            //                dgvLocationDetails.RowCount = dgvLocationDetails.RowCount + 1;
                            //                int intRowIndex = dgvLocationDetails.RowCount - 2;
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LItemID"].Value = drPurchaseRow.Cells["ItemID"].Value.ToInt32();
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LItemCode"].Value = drPurchaseRow.Cells["ItemCode"].Value.ToString();
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LItemName"].Value = drPurchaseRow.Cells["ItemName"].Value.ToString();
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LBatchID"].Value = drPurchaseRow.Cells["cboBatchNo"].Tag.ToInt32();
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LBatchNo"].Value = drPurchaseRow.Cells["cboBatchNo"].Value.ToString();
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LCurrentQty"].Value = drPurchaseRow.Cells["CurrentQty"].Value.ToDecimal();
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LNewQty"].Value = drPurchaseRow.Cells["NewQty"].Value.ToDecimal();


                            //                DataRow dr = objStockAdjustmentBLL.GetItemDefaultLocation(drPurchaseRow.Cells["ItemID"].Value.ToInt32(), cboWarehouse.SelectedValue.ToInt32());

                            //                FillLocationGridCombo(LLocation.Index, intRowIndex);
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dr["LocationID"].ToInt32();
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Tag = dr["LocationID"].ToInt32();
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLocation"].FormattedValue;

                            //                FillLocationGridCombo(LRow.Index, intRowIndex);
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dr["RowID"].ToInt32();
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Tag = dr["RowID"].ToInt32();
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LRow"].FormattedValue;

                            //                FillLocationGridCombo(LBlock.Index, intRowIndex);
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dr["BlockID"].ToInt32();
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Tag = dr["BlockID"].ToInt32();
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LBlock"].FormattedValue;

                            //                FillLocationGridCombo(LLocation.Index, intRowIndex);
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dr["LotID"].ToInt32();
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Tag = dr["LotID"].ToInt32();
                            //                dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].Value = dgvLocationDetails.Rows[intRowIndex].Cells["LLot"].FormattedValue;

                            //                //dgvLocationDetails.Rows.Add();
                            //            }
                            //            else
                            //            {
                            //                foreach (DataGridViewRow drLocationRow in dgvLocationDetails.Rows)
                            //                {
                            //                    if (drLocationRow.Cells["LItemID"].Value != null && drLocationRow.Cells["LBatchNo"].Value != null)
                            //                    {
                            //                        if (drPurchaseRow.Cells["ItemID"].Value.ToInt32() == drLocationRow.Cells["LItemID"].Value.ToInt32() && drPurchaseRow.Cells["cboBatchNo"].Tag.ToInt64() == drLocationRow.Cells["LBatchID"].Value.ToInt64())
                            //                        {
                            //                            drLocationRow.Cells["LNewQty"].Value = drPurchaseRow.Cells[NewQty.Index].Value;
                            //                            //if ((drLocationRow.Cells["LCurrentQty"].Value.ToDecimal() - drLocationRow.Cells["LDifference"].Value.ToDecimal() + decDifference) <= 0)
                            //                            //    drLocationRow.Cells["LNewQty"].Value = 0;
                            //                            //else
                            //                            //    drLocationRow.Cells["LNewQty"].Value = (drLocationRow.Cells["LCurrentQty"].Value.ToDecimal() - drLocationRow.Cells["LDifference"].Value.ToDecimal() + decDifference).Format(3);
                            //                            //break;
                            //                        }
                            //                    }
                            //                }
                            //            }
                                  //  }
                             //   }


                            //}
                        }
                        else
                        {
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1401, out MmessageIcon);
                            ErrorStatus.SetError(cboWarehouse, MsMessageCommon.Replace("#", "").Trim());
                            MessageBox.Show(MsMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblStockAdjustmentStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                            TmrStatus.Enabled = true;
                            tcStockAdjustment.SelectedTab = tiItemDetails;
                            cboWarehouse.Focus();
                        }
                 //  }
              //  }
                return true;
            }
            else
                tcStockAdjustment.SelectedTab = tiItemDetails;
            return false;
        }

        private bool ValidateLocationGrid()
        {
            bool blnValid = true;
            int intColumnIndex = 0;
            foreach (DataGridViewRow drItemRow in dgvStockAdjustment.Rows)
            {
                decimal decLocationDifference = 0;
                foreach (DataGridViewRow drLocationRow in dgvLocationDetails.Rows)
                {
                    if (drLocationRow.Cells["LItemID"].Value.ToInt32() != 0)
                    {
                        if (drLocationRow.Cells["LLocation"].Tag.ToInt32() == 0)
                        {
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1413, out MmessageIcon).Replace("#", "");
                            intColumnIndex = LLocation.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LRow"].Tag.ToInt32() == 0)
                        {
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1414, out MmessageIcon).Replace("#", "");
                            intColumnIndex = LRow.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LBlock"].Tag.ToInt32() == 0)
                        {
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1415, out MmessageIcon).Replace("#", "");
                            intColumnIndex = LBlock.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LLot"].Tag.ToInt32() == 0)
                        {
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1416, out MmessageIcon).Replace("#", "");
                            intColumnIndex = LLot.Index;
                            blnValid = false;
                        }
                        else if (drLocationRow.Cells["LNewQty"].Value.ToDecimal() < 0)
                        {
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1417, out MmessageIcon).Replace("#", "");
                            intColumnIndex = LNewQty.Index;
                            blnValid = false;
                        }
                        if (!blnValid)
                        {
                            MessageBox.Show(MsMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            TmrStatus.Enabled = true;
                            lblStockAdjustmentStatus.Text = MsMessageCommon;
                            tcStockAdjustment.SelectedTab = tiLocationDetails;
                            dgvLocationDetails.Focus();
                            dgvLocationDetails.CurrentCell = dgvLocationDetails[intColumnIndex, drLocationRow.Index];
                            break;
                        }
                    }
                    if (blnValid && drLocationRow.Cells["LItemID"].Value.ToInt32() != 0 && drLocationRow.Cells["LBatchID"].Value != null)
                    {
                        if (drLocationRow.Cells["LItemID"].Value.ToInt32() == drItemRow.Cells["ItemID"].Value.ToInt32() && drLocationRow.Cells["LBatchID"].Value.ToInt64() == drItemRow.Cells["cboBatchNo"].Tag.ToInt64())
                            decLocationDifference += drLocationRow.Cells["LDifference"].Value.ToDecimal();
                    }
                }
                if (!blnValid) break;
                if (blnValid && drItemRow.Cells["Difference"].Value.ToDecimal() != decLocationDifference)
                {
                    blnValid = false;
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1418, out MmessageIcon).Replace("#", "").Replace("*",drItemRow.Cells["ItemName"].Value.ToString()); 
                    MessageBox.Show(MsMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    TmrStatus.Enabled = true;
                    lblStockAdjustmentStatus.Text = MsMessageCommon;
                    tcStockAdjustment.SelectedTab = tiLocationDetails;
                    dgvLocationDetails.Focus();
                    dgvLocationDetails.CurrentCell = dgvLocationDetails[intColumnIndex, drItemRow.Index];
                    break;
                }
            }
            return blnValid;
        }

        private void dgvLocationDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                dgvLocationDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                int intTag = 0;

                if (e.RowIndex >= 0 && e.ColumnIndex == LLocation.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LLocation"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LLocation"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LLocation"].Tag = dgvLocationDetails.CurrentRow.Cells["LLocation"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LLocation"].Value = dgvLocationDetails.CurrentRow.Cells["LLocation"].FormattedValue;
                        LRow.DataSource = null;
                        LBlock.DataSource = null;
                        LLot.DataSource = null;
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Value = "";
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = "";
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = "";
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LLocation"].Value = dgvLocationDetails.CurrentRow.Cells["LLocation"].FormattedValue;
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex == LRow.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LRow"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LRow"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Tag = dgvLocationDetails.CurrentRow.Cells["LRow"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Value = dgvLocationDetails.CurrentRow.Cells["LRow"].FormattedValue;

                        LBlock.DataSource = null;
                        LLot.DataSource = null;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = "";
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = "";
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LRow"].Value = dgvLocationDetails.CurrentRow.Cells["LRow"].FormattedValue;
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex == LBlock.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LBlock"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Tag = dgvLocationDetails.CurrentRow.Cells["LBlock"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = dgvLocationDetails.CurrentRow.Cells["LBlock"].FormattedValue;
                        LLot.DataSource = null;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = 0;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = "";
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LBlock"].Value = dgvLocationDetails.CurrentRow.Cells["LBlock"].FormattedValue;
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex == LLot.Index)
                {
                    intTag = dgvLocationDetails.CurrentRow.Cells["LLot"].Tag.ToInt32();
                    if (intTag != dgvLocationDetails.CurrentRow.Cells["LLot"].Value.ToInt32())
                    {
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Tag = dgvLocationDetails.CurrentRow.Cells["LLot"].Value;
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = dgvLocationDetails.CurrentRow.Cells["LLot"].FormattedValue;
                    }
                    else
                        dgvLocationDetails.CurrentRow.Cells["LLot"].Value = dgvLocationDetails.CurrentRow.Cells["LLot"].FormattedValue;

                    clsDTOAdjustedLocationDetails clsLocationDetails = new clsDTOAdjustedLocationDetails();
                    clsLocationDetails.ItemID = dgvLocationDetails.Rows[e.RowIndex].Cells["LItemID"].Value.ToInt32();
                    clsLocationDetails.BatchID = dgvLocationDetails.Rows[e.RowIndex].Cells["LBatchID"].Value.ToInt32();
                    clsLocationDetails.LocationID = dgvLocationDetails.Rows[e.RowIndex].Cells["LLocation"].Tag.ToInt32();
                    clsLocationDetails.RowID = dgvLocationDetails.Rows[e.RowIndex].Cells["LRow"].Tag.ToInt32();
                    clsLocationDetails.BlockID = dgvLocationDetails.Rows[e.RowIndex].Cells["LBlock"].Tag.ToInt32();
                    clsLocationDetails.LotID = dgvLocationDetails.Rows[e.RowIndex].Cells["LLot"].Tag.ToInt32();

                    dgvLocationDetails.Rows[e.RowIndex].Cells["LCurrentQty"].Value = objStockAdjustmentBLL.GetItemLocationQuantity(clsLocationDetails, cboWarehouse.SelectedValue.ToInt32());
                }
            }
            catch (Exception)
            {
                return;
            }

        }

        private void dgvLocationDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (this.mode == eMode.Update)
            {
                e.Cancel = true;
                return;
            }

            if (e.ColumnIndex == LLocation.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
            else if (e.ColumnIndex == LBlock.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
            else if (e.ColumnIndex == LRow.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
            else if (e.ColumnIndex == LLot.Index)
            {
                FillLocationGridCombo(e.ColumnIndex, e.RowIndex);
            }
        }

        private void tcStockAdjustment_SelectedTabChanged(object sender, DevComponents.DotNetBar.TabStripTabChangedEventArgs e)
        {
            if (tcStockAdjustment.SelectedTab == tiLocationDetails)
                FillItemLocationDetails();
        }

        private void dgvLocationDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            int ColumnIndex = Convert.ToInt32((((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex));

            e.Control.KeyPress -= this.TextboxNumeric_KeyPress;
            if (ColumnIndex == LNewQty.Index)
            {
                e.Control.KeyPress += this.TextboxNumeric_KeyPress;
            }
        }

        private void dgvLocationDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                // enable save and ok buttons
                this.OnInputChanged(new object(), new EventArgs());

                decimal decZero = 0;
                if (e.ColumnIndex == LItemCode.Index || e.ColumnIndex == LItemName.Index)
                {
                    //if (this.dgvLocationDetails.CurrentRow != null && !GridBinding)
                    //{
                    //    this.dgvLocationDetails.CurrentRow.Cells["LBatchID"].Value = null;
                    //    this.dgvLocationDetails.CurrentRow.Cells["LBatchNo"].Tag = null;
                    //    this.dgvLocationDetails.CurrentRow.Cells["LCurrentQty"].Value = decZero.Format(3);
                    //    this.dgvLocationDetails.CurrentRow.Cells["LActualQty"].Value = decZero.Format(3);
                    //    this.dgvLocationDetails.CurrentRow.Cells["LNewQty"].Value = decZero.CurrencyFormat(this.DefautCurrencyScale);
                    //    this.dgvLocationDetails.CurrentRow.Cells["Difference"].Value = decZero.Format(3);
                    //}
                }

                else if ((e.ColumnIndex == LNewQty.Index || e.ColumnIndex == LCurrentQty.Index) && e.RowIndex >= 0)
                {
                    // New Quantity column
                    decimal NewQuantity = this.dgvLocationDetails.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToDecimal();
                    decimal CurrentQty = this.dgvLocationDetails.Rows[e.RowIndex].Cells[e.ColumnIndex - 1].Value.ToDecimal();
                    decimal Difference = NewQuantity - CurrentQty;

                    this.dgvLocationDetails.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = NewQuantity.Format(3);
                    this.dgvLocationDetails.Rows[e.RowIndex].Cells[e.ColumnIndex + 1].Value = Difference.Format(3);
                }
            }
            catch (Exception ex)
            {
                this.Logger.WriteLog(string.Format("Error on dgvLocationDetails_CellValueChanged() Form Name : {0}, Error : {1}", this.Name, ex.ToString()), 3);
            }
        }

        private void dgvLocationDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvLocationDetails.IsCurrentCellDirty)
            {
                if (dgvLocationDetails.CurrentCell != null)
                    dgvLocationDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
        }

        private void dgvLocationDetails_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            bool blnValid = false;
            if (dgvLocationDetails.Rows[e.Row.Index].Cells["LItemID"].Value.ToInt32() != 0 && dgvLocationDetails.Rows[e.Row.Index].Cells["LBatchID"].Value != null)
            {
                foreach (DataGridViewRow dr in dgvLocationDetails.Rows)
                {

                    if (dr.Cells["LItemID"].Value.ToInt32() != 0 && dr.Cells["LBatchID"].Value != null)
                    {
                        if (dr.Index != e.Row.Index && dr.Cells["LItemID"].Value.ToInt32() == dgvLocationDetails.Rows[e.Row.Index].Cells["LItemID"].Value.ToInt32() && dr.Cells["LBatchID"].Value.ToInt64() == dgvLocationDetails.Rows[e.Row.Index].Cells["LBatchID"].Value.ToInt64())
                        {
                            blnValid = true;
                            break;
                        }
                    }
                }
            }
            else
            {
                blnValid = true;
            }
            if (!blnValid)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1419, out MmessageIcon).Replace("#", "");
                MessageBox.Show(MsMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                e.Cancel = true;
            }
        }

        private void dgvLocationDetails_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //if (dgvLocationDetails.CurrentCell.ColumnIndex == LItemCode.Index || dgvLocationDetails.CurrentCell.ColumnIndex == LItemName.Index)
                //{
                //    for (int i = 0; i < dgvLocationDetails.Columns.Count; i++)
                //    {
                //            dgvLocationDetails.Rows[dgvLocationDetails.CurrentCell.RowIndex].Cells[i].Value = null;
                //            dgvLocationDetails.Rows[dgvLocationDetails.CurrentCell.RowIndex].Cells[i].Tag = null;
                //    }
                //}
                dgvLocationDetails.PServerName = ClsCommonSettings.ServerName;
                string[] First = { "LItemCode", "LItemName", "LItemID", "LBatchNo", "LBatchID" };//first grid 
                string[] second = { "ItemCode", "ItemName", "ItemID", "BatchNo", "BatchID" };//inner grid
                dgvLocationDetails.aryFirstGridParam = First;
                dgvLocationDetails.arySecondGridParam = second;
                dgvLocationDetails.PiFocusIndex = LNewQty.Index;
                dgvLocationDetails.bBothScrollBar = true;
                dgvLocationDetails.ColumnsToHide = new string[] { "ItemID", "BatchID" };
                dgvLocationDetails.pnlLeft = expandableSplitterLeft.Location.X + 5;
                dgvLocationDetails.pnlTop = expandableSplitter1.Location.Y + BindingNavigatorStockAdjustment.Height + 10;
                if (dgvLocationDetails.CurrentCell.ColumnIndex == LItemCode.Index)
                {
                    dgvLocationDetails.field = "ItemCode";
                    dgvLocationDetails.CurrentCell.Value = dgvLocationDetails.TextBoxText;
                }
                if (dgvLocationDetails.CurrentCell.ColumnIndex == LItemName.Index)
                {
                    // dgvPurchase.PiColumnIndex = 1;
                    dgvLocationDetails.field = "ItemName";
                    dgvLocationDetails.CurrentCell.Value = dgvLocationDetails.TextBoxText;
                }
                string strFilterString = dgvLocationDetails.field + " Like '" + dgvLocationDetails.TextBoxText + "%'";
                dtItemDetails.DefaultView.RowFilter = strFilterString;
                dgvLocationDetails.dtDataSource = dtItemDetails.DefaultView.ToTable();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvItemGrid_TextBoxChangedEvent() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvItemGrid_TextBoxChangedEvent() " + ex.Message, 2);
            }
        }

        private void dgvStockAdjustment_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

          
        }

        private void dgvLocationDetails_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == LItemCode.Index) || (e.ColumnIndex == LItemName.Index))
            {
                dgvLocationDetails.PiColumnIndex = e.ColumnIndex;
            }

        }

        private void cboSCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable datCombos = null; //bool blnIsEnabled = false;
            //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
            //{
               if (cboSCompany.SelectedValue == null || Convert.ToInt32(cboSCompany.SelectedValue) == -2)
                {
                    DataTable datCompany = (DataTable)cboSCompany.DataSource;
                    string strFilterCondition = "";
                    //datCompany.Rows.RemoveAt(0);
                    //datCompany.AcceptChanges();
                    if (datCompany.Rows.Count > 0)
                    {
                        strFilterCondition = "CompanyID In (";
                        foreach (DataRow dr in datCompany.Rows)
                            strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                        strFilterCondition += ")";
                    }
                    datCombos = MobjClsBLLCommonUtility.FillCombos(new string[] { "EmployeeID,EmployeeFullName", "EmployeeMaster", strFilterCondition });
                }
                else
                {
                    datCombos = MobjClsBLLCommonUtility.FillCombos(new string[] { "EmployeeID,EmployeeFullName", "EmployeeMaster", "CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue) });
                }
            //}
            //else
            //{
            //    try
            //    {
            //        try
            //        {
            //            DataTable datTemp = MobjClsBLLCommonUtility.FillCombos(new string[] { "IsVisible", "STRoleFieldsDetails", "" + 
            //            "RoleID=" + ClsCommonSettings.RoleID + " AND CompanyID=" + ClsCommonSettings.CompanyID + " " +
            //            "AND ControlID=" + (int)ControlOperationType.StAdjEmployee + " AND IsVisible=1" });
            //            if (datTemp != null)
            //                if (datTemp.Rows.Count > 0)
            //                    datCombos = objStockAdjustmentBLL.GetEmployeeByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.StAdjCompany);

            //            if (datCombos == null)
            //                datCombos = MobjClsBLLCommonUtility.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
            //                            " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
            //            else
            //            {
            //                if (datCombos.Rows.Count == 0)
            //                    datCombos = MobjClsBLLCommonUtility.FillCombos(new string[] { "DISTINCT EM.EmployeeID,EM.EmployeeFullName","EmployeeMaster EM INNER JOIN UserMaster UM " +
            //                            " ON EM.EmployeeID=UM.EmployeeID", "UM.UserID = " + ClsCommonSettings.UserID });
            //            }
            //        }
            //        catch { }
            //    }
            //    catch { }
            //}
            cboSEmployee.ValueMember = "EmployeeID";
            cboSEmployee.DisplayMember = "EmployeeFullName";
            cboSEmployee.DataSource = datCombos;
        }

        private void BindingNavigatorClearItem_Click(object sender, EventArgs e)
        {
            AddNewRecord();
        }

        private void FrmStockAdjustment_Shown(object sender, EventArgs e)
        {
            cboCompany.Focus();
        }

        private void expandableSplitterLeft_ExpandedChanged(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
        {
            if (expandableSplitterLeft.Expanded)
            {
                if (dgvStockAdjustmentDisplay.Columns.Count > 0)
                    dgvStockAdjustmentDisplay.Columns[0].Visible = false;
            }
        }

        private void lnkLabelAdvanceSearch_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DataTable datAdvanceSearchedData = new DataTable();
            using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.StockAdjustment))
            {
                objFrmSearchForm.ShowDialog();
                if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                {
                    datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                    datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                    datAdvanceSearchedData.Columns["ID"].ColumnName = "StockAdjustmentID";
                    dgvStockAdjustmentDisplay.DataSource = datAdvanceSearchedData;
                    dgvStockAdjustmentDisplay.Columns["StockAdjustmentID"].Visible = false;
                    dgvStockAdjustmentDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

        private void dgvStockAdjustment_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjClsBLLCommonUtility.SetSerialNo(dgvStockAdjustment, e.RowIndex, false);
        }

        private void dgvStockAdjustment_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjClsBLLCommonUtility.SetSerialNo(dgvStockAdjustment, e.RowIndex, false);
        }

        private void dgvStockAdjustment_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            decimal decZero = 0;
            if (e.RowIndex >= 0 && (e.ColumnIndex == ItemName.Index || e.ColumnIndex == ItemCode.Index))
            {
                if (this.dgvStockAdjustment.Rows[e.RowIndex].Cells["ItemID"].Value != null && !GridBinding)
                {
                    Int64 _ItemID = this.dgvStockAdjustment.Rows[e.RowIndex].Cells["ItemID"].Value.ToInt64();                    

                    //fill batch combo only if costing method of current item is 'Batch' (CostingMethodID : 3|Batch )
                    DataTable datCostingMethod = MobjClsBLLCommonUtility.FillCombos(new string[] { "DISTINCT CostingMethodID", "InvCostingAndPricingDetails", "ItemID = " + _ItemID });

                    if (datCostingMethod != null && datCostingMethod.Rows.Count > 0)
                    {
                        if (datCostingMethod.Rows[0]["CostingMethodID"].ToInt32() == (int)CostingMethodReference.Batch)
                        {
                            int tag = -1;

                            if (this.dgvStockAdjustment.CurrentRow.Cells["cboBatchNo"].Tag != null)
                                tag = this.dgvStockAdjustment.CurrentRow.Cells["cboBatchNo"].Tag.ToInt32();
                                
                            this.FillBatchNoColumn(_ItemID);

                            if (tag != -1)
                                this.dgvStockAdjustment.CurrentRow.Cells["cboBatchNo"].Tag = tag;

                            //this.dgvStockAdjustment.CurrentRow.Cells["CurrentQty"].Value = decZero.Format(3);
                            //this.dgvStockAdjustment.CurrentRow.Cells["ActualQty"].Value = decZero.Format(3);
                            //this.dgvStockAdjustment.CurrentRow.Cells["LIFORate"].Value = decZero.CurrencyFormat(this.DefautCurrencyScale);
                            //this.dgvStockAdjustment.CurrentRow.Cells["NewQty"].Value = decZero.Format(3);
                            //this.dgvStockAdjustment.CurrentRow.Cells["Difference"].Value = decZero.Format(3);
                        }
                        else
                        {
                            cboBatchNo.DataSource = null;
                            Int64 lngBatchID = 0;
                            int intItemID = _ItemID.ToInt32();
                            int intWarehouseID = this.cboWarehouse.SelectedValue.ToInt32();

                            using (DataTable dt = this.objStockAdjustmentBLL.GetRateAndCurrentStock(lngBatchID, intItemID, intWarehouseID))
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    decimal LifoRate = dt.Rows[0]["Rate"].ToDecimal();
                                    decimal CurrentStock = dt.Rows[0]["GRNQuantity"].ToDecimal();
                                    this.dgvStockAdjustment.CurrentRow.Cells["CurrentQty"].Value = CurrentStock.Format(3);
                                    this.dgvStockAdjustment.CurrentRow.Cells["ActualQty"].Value = CurrentStock.Format(3);
                                    this.dgvStockAdjustment.CurrentRow.Cells["LIFORate"].Value = LifoRate.CurrencyFormat(this.DefautCurrencyScale);
                                    //this.dgvStockAdjustment.CurrentRow.Cells["NewQty"].Value = decZero.Format(3);
                                    decimal decDifference = decZero - CurrentStock;

                                    this.dgvStockAdjustment.Rows[e.RowIndex].Cells[Difference.Index].Value = decDifference.Format(3);
                                }
                            }
                        }
                    }
                }
            }
        }

      
    }
}
