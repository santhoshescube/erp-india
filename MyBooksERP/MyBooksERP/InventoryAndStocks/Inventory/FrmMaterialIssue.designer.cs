﻿namespace MyBooksERP
{
    partial class FrmMaterialIssue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMaterialIssue));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelMain = new DevComponents.DotNetBar.PanelEx();
            this.panelMiddle = new DevComponents.DotNetBar.PanelEx();
            this.panelTcMaterialIssue = new DevComponents.DotNetBar.PanelEx();
            this.tcMaterialIssue = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvMaterialIssue = new ClsInnerGridBar();
            this.dgvColItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColBatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColBatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColQtyAvailable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColActualQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColUOM = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvColRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColNetAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiItemDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.pnlBottom = new DevComponents.DotNetBar.PanelEx();
            this.pnlBottomRight = new DevComponents.DotNetBar.PanelEx();
            this.cboCurrency = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.lblNetAmount = new System.Windows.Forms.Label();
            this.txtNetAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.pnlMainBottom = new System.Windows.Forms.Panel();
            this.lblCreatedDateValue = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblCreatedByText = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblStatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.txtRemarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.expandableSplitterTop = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.tcGeneral = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.txtQuantity = new DemoClsDataGridview.DecimalDotNetBarTextBox();
            this.txtMaterialIssueNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblMaterialIssueNo = new System.Windows.Forms.Label();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.lblIssuedDate = new System.Windows.Forms.Label();
            this.cboWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.dtpIssuedDate = new System.Windows.Forms.DateTimePicker();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.cboBOM = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new System.Windows.Forms.Label();
            this.lblProject = new System.Windows.Forms.Label();
            this.lblIssuedByText = new System.Windows.Forms.Label();
            this.lblIssuedBy = new System.Windows.Forms.Label();
            this.tiGeneral = new DevComponents.DotNetBar.TabItem(this.components);
            this.bnMaterialIssue = new DevComponents.DotNetBar.Bar();
            this.bnAddNewItem = new DevComponents.DotNetBar.ButtonItem();
            this.bnSaveItem = new DevComponents.DotNetBar.ButtonItem();
            this.bnClear = new DevComponents.DotNetBar.ButtonItem();
            this.bnDelete = new DevComponents.DotNetBar.ButtonItem();
            this.bnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.bnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvMaterialIssueDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.epMaterialIssue = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelLeftTop = new DevComponents.DotNetBar.PanelEx();
            this.cboSBOM = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSearchProject = new System.Windows.Forms.Label();
            this.lblSIssueNo = new System.Windows.Forms.Label();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cboSIssuedBy = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSIssuedBy = new System.Windows.Forms.Label();
            this.cboSWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSWarehouse = new System.Windows.Forms.Label();
            this.btnRefresh = new DevComponents.DotNetBar.ButtonX();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboSCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSCompany = new System.Windows.Forms.Label();
            this.lblSCountStatus = new DevComponents.DotNetBar.LabelX();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.tmrMaterialIssue = new System.Windows.Forms.Timer(this.components);
            this.ErrMaterialIssue = new System.Windows.Forms.ErrorProvider(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblProduct = new DevComponents.DotNetBar.LabelX();
            this.panelMain.SuspendLayout();
            this.panelMiddle.SuspendLayout();
            this.panelTcMaterialIssue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcMaterialIssue)).BeginInit();
            this.tcMaterialIssue.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialIssue)).BeginInit();
            this.pnlBottom.SuspendLayout();
            this.pnlBottomRight.SuspendLayout();
            this.pnlMainBottom.SuspendLayout();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).BeginInit();
            this.tcGeneral.SuspendLayout();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnMaterialIssue)).BeginInit();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialIssueDisplay)).BeginInit();
            this.epMaterialIssue.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrMaterialIssue)).BeginInit();
            this.SuspendLayout();
            // 
            // panelMain
            // 
            this.panelMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelMain.Controls.Add(this.panelMiddle);
            this.panelMain.Controls.Add(this.expandableSplitterTop);
            this.panelMain.Controls.Add(this.panelTop);
            this.panelMain.Controls.Add(this.bnMaterialIssue);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(203, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(891, 514);
            this.panelMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelMain.Style.GradientAngle = 90;
            this.panelMain.TabIndex = 0;
            this.panelMain.Text = "panelMain";
            // 
            // panelMiddle
            // 
            this.panelMiddle.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelMiddle.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelMiddle.Controls.Add(this.panelTcMaterialIssue);
            this.panelMiddle.Controls.Add(this.pnlBottom);
            this.panelMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMiddle.Location = new System.Drawing.Point(0, 133);
            this.panelMiddle.Name = "panelMiddle";
            this.panelMiddle.Size = new System.Drawing.Size(891, 381);
            this.panelMiddle.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelMiddle.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelMiddle.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelMiddle.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelMiddle.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelMiddle.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelMiddle.Style.GradientAngle = 90;
            this.panelMiddle.TabIndex = 1;
            this.panelMiddle.Text = "panelMiddle";
            // 
            // panelTcMaterialIssue
            // 
            this.panelTcMaterialIssue.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTcMaterialIssue.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelTcMaterialIssue.Controls.Add(this.tcMaterialIssue);
            this.panelTcMaterialIssue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTcMaterialIssue.Location = new System.Drawing.Point(0, 0);
            this.panelTcMaterialIssue.Name = "panelTcMaterialIssue";
            this.panelTcMaterialIssue.Size = new System.Drawing.Size(891, 250);
            this.panelTcMaterialIssue.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTcMaterialIssue.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTcMaterialIssue.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTcMaterialIssue.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTcMaterialIssue.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTcMaterialIssue.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTcMaterialIssue.Style.GradientAngle = 90;
            this.panelTcMaterialIssue.TabIndex = 0;
            this.panelTcMaterialIssue.Text = "panelEx1";
            // 
            // tcMaterialIssue
            // 
            this.tcMaterialIssue.BackColor = System.Drawing.Color.Transparent;
            this.tcMaterialIssue.CanReorderTabs = true;
            this.tcMaterialIssue.Controls.Add(this.tabControlPanel1);
            this.tcMaterialIssue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMaterialIssue.Location = new System.Drawing.Point(0, 0);
            this.tcMaterialIssue.Name = "tcMaterialIssue";
            this.tcMaterialIssue.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcMaterialIssue.SelectedTabIndex = 0;
            this.tcMaterialIssue.Size = new System.Drawing.Size(891, 250);
            this.tcMaterialIssue.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcMaterialIssue.TabIndex = 0;
            this.tcMaterialIssue.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcMaterialIssue.Tabs.Add(this.tiItemDetails);
            this.tcMaterialIssue.TabStop = false;
            this.tcMaterialIssue.Text = "tabControl1";
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.dgvMaterialIssue);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(891, 228);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 0;
            this.tabControlPanel1.TabItem = this.tiItemDetails;
            // 
            // dgvMaterialIssue
            // 
            this.dgvMaterialIssue.AddNewRow = false;
            this.dgvMaterialIssue.AlphaNumericCols = new int[0];
            this.dgvMaterialIssue.BackgroundColor = System.Drawing.Color.White;
            this.dgvMaterialIssue.CapsLockCols = new int[0];
            this.dgvMaterialIssue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMaterialIssue.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColItemCode,
            this.dgvColItemName,
            this.dgvColItemID,
            this.dgvColBatchID,
            this.dgvColBatchNo,
            this.dgvColExpiryDate,
            this.dgvColQtyAvailable,
            this.dgvColQuantity,
            this.dgvColActualQuantity,
            this.dgvColUOM,
            this.dgvColRate,
            this.dgvColNetAmount});
            this.dgvMaterialIssue.DecimalCols = new int[] {
        6};
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMaterialIssue.DefaultCellStyle = dataGridViewCellStyle16;
            this.dgvMaterialIssue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMaterialIssue.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvMaterialIssue.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvMaterialIssue.HasSlNo = false;
            this.dgvMaterialIssue.LastRowIndex = 0;
            this.dgvMaterialIssue.Location = new System.Drawing.Point(1, 1);
            this.dgvMaterialIssue.Name = "dgvMaterialIssue";
            this.dgvMaterialIssue.NegativeValueCols = new int[0];
            this.dgvMaterialIssue.NumericCols = new int[0];
            this.dgvMaterialIssue.RowHeadersWidth = 50;
            this.dgvMaterialIssue.Size = new System.Drawing.Size(889, 226);
            this.dgvMaterialIssue.TabIndex = 0;
            this.dgvMaterialIssue.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialIssue_CellValueChanged);
            this.dgvMaterialIssue.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvMaterialIssue_CellBeginEdit);
            this.dgvMaterialIssue.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvMaterialIssue_UserDeletedRow);
            this.dgvMaterialIssue.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvMaterialIssue_RowsAdded);
            this.dgvMaterialIssue.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialIssue_CellEndEdit);
            this.dgvMaterialIssue.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvMaterialIssue_Textbox_TextChanged);
            this.dgvMaterialIssue.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvMaterialIssue_EditingControlShowing);
            this.dgvMaterialIssue.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvMaterialIssue_CurrentCellDirtyStateChanged);
            this.dgvMaterialIssue.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialIssue_CellEnter);
            this.dgvMaterialIssue.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvMaterialIssue_RowsRemoved);
            // 
            // dgvColItemCode
            // 
            this.dgvColItemCode.HeaderText = "Item Code";
            this.dgvColItemCode.MaxInputLength = 20;
            this.dgvColItemCode.Name = "dgvColItemCode";
            // 
            // dgvColItemName
            // 
            this.dgvColItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvColItemName.HeaderText = "Item Name";
            this.dgvColItemName.MaxInputLength = 100;
            this.dgvColItemName.Name = "dgvColItemName";
            // 
            // dgvColItemID
            // 
            this.dgvColItemID.HeaderText = "ItemID";
            this.dgvColItemID.Name = "dgvColItemID";
            this.dgvColItemID.Visible = false;
            // 
            // dgvColBatchID
            // 
            this.dgvColBatchID.HeaderText = "BatchID";
            this.dgvColBatchID.Name = "dgvColBatchID";
            this.dgvColBatchID.Visible = false;
            // 
            // dgvColBatchNo
            // 
            this.dgvColBatchNo.HeaderText = "Batch No";
            this.dgvColBatchNo.Name = "dgvColBatchNo";
            this.dgvColBatchNo.ReadOnly = true;
            this.dgvColBatchNo.Visible = false;
            // 
            // dgvColExpiryDate
            // 
            this.dgvColExpiryDate.HeaderText = "Expiry Date";
            this.dgvColExpiryDate.Name = "dgvColExpiryDate";
            this.dgvColExpiryDate.ReadOnly = true;
            this.dgvColExpiryDate.Visible = false;
            // 
            // dgvColQtyAvailable
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColQtyAvailable.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvColQtyAvailable.HeaderText = "Qty Available";
            this.dgvColQtyAvailable.Name = "dgvColQtyAvailable";
            this.dgvColQtyAvailable.ReadOnly = true;
            // 
            // dgvColQuantity
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColQuantity.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgvColQuantity.HeaderText = "Quantity";
            this.dgvColQuantity.MaxInputLength = 12;
            this.dgvColQuantity.Name = "dgvColQuantity";
            // 
            // dgvColActualQuantity
            // 
            this.dgvColActualQuantity.HeaderText = "Actual Quantity";
            this.dgvColActualQuantity.Name = "dgvColActualQuantity";
            this.dgvColActualQuantity.ReadOnly = true;
            this.dgvColActualQuantity.Visible = false;
            // 
            // dgvColUOM
            // 
            this.dgvColUOM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColUOM.HeaderText = "UOM";
            this.dgvColUOM.Name = "dgvColUOM";
            this.dgvColUOM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvColUOM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dgvColRate
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColRate.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvColRate.HeaderText = "Rate";
            this.dgvColRate.MaxInputLength = 12;
            this.dgvColRate.Name = "dgvColRate";
            this.dgvColRate.ReadOnly = true;
            // 
            // dgvColNetAmount
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColNetAmount.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvColNetAmount.HeaderText = "Net Amount";
            this.dgvColNetAmount.Name = "dgvColNetAmount";
            this.dgvColNetAmount.ReadOnly = true;
            this.dgvColNetAmount.Width = 125;
            // 
            // tiItemDetails
            // 
            this.tiItemDetails.AttachedControl = this.tabControlPanel1;
            this.tiItemDetails.Name = "tiItemDetails";
            this.tiItemDetails.Text = "Material Details";
            // 
            // pnlBottom
            // 
            this.pnlBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottom.Controls.Add(this.pnlBottomRight);
            this.pnlBottom.Controls.Add(this.pnlMainBottom);
            this.pnlBottom.Controls.Add(this.txtRemarks);
            this.pnlBottom.Controls.Add(this.lblRemarks);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 250);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(891, 131);
            this.pnlBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottom.Style.GradientAngle = 90;
            this.pnlBottom.TabIndex = 1;
            // 
            // pnlBottomRight
            // 
            this.pnlBottomRight.CanvasColor = System.Drawing.Color.Transparent;
            this.pnlBottomRight.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottomRight.Controls.Add(this.cboCurrency);
            this.pnlBottomRight.Controls.Add(this.lblCurrency);
            this.pnlBottomRight.Controls.Add(this.lblNetAmount);
            this.pnlBottomRight.Controls.Add(this.txtNetAmount);
            this.pnlBottomRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlBottomRight.Location = new System.Drawing.Point(534, 0);
            this.pnlBottomRight.Name = "pnlBottomRight";
            this.pnlBottomRight.Size = new System.Drawing.Size(357, 105);
            this.pnlBottomRight.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottomRight.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottomRight.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottomRight.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottomRight.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottomRight.Style.BorderSide = DevComponents.DotNetBar.eBorderSide.Top;
            this.pnlBottomRight.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottomRight.Style.GradientAngle = 90;
            this.pnlBottomRight.TabIndex = 294;
            // 
            // cboCurrency
            // 
            this.cboCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCurrency.DisplayMember = "Text";
            this.cboCurrency.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCurrency.DropDownHeight = 75;
            this.cboCurrency.Enabled = false;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.IntegralHeight = false;
            this.cboCurrency.ItemHeight = 14;
            this.cboCurrency.Location = new System.Drawing.Point(132, 8);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(221, 20);
            this.cboCurrency.TabIndex = 264;
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrency.Location = new System.Drawing.Point(7, 10);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(49, 13);
            this.lblCurrency.TabIndex = 265;
            this.lblCurrency.Text = "Currency";
            // 
            // lblNetAmount
            // 
            this.lblNetAmount.AutoSize = true;
            this.lblNetAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNetAmount.Location = new System.Drawing.Point(6, 36);
            this.lblNetAmount.Name = "lblNetAmount";
            this.lblNetAmount.Size = new System.Drawing.Size(120, 24);
            this.lblNetAmount.TabIndex = 263;
            this.lblNetAmount.Text = "Net Amount";
            // 
            // txtNetAmount
            // 
            // 
            // 
            // 
            this.txtNetAmount.Border.Class = "TextBoxBorder";
            this.txtNetAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtNetAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNetAmount.Location = new System.Drawing.Point(132, 31);
            this.txtNetAmount.MaxLength = 20;
            this.txtNetAmount.Name = "txtNetAmount";
            this.txtNetAmount.Size = new System.Drawing.Size(221, 29);
            this.txtNetAmount.TabIndex = 262;
            this.txtNetAmount.TabStop = false;
            this.txtNetAmount.Text = "0.00";
            this.txtNetAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNetAmount.WatermarkEnabled = false;
            // 
            // pnlMainBottom
            // 
            this.pnlMainBottom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlMainBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMainBottom.Controls.Add(this.lblCreatedDateValue);
            this.pnlMainBottom.Controls.Add(this.lblCreatedByText);
            this.pnlMainBottom.Controls.Add(this.lblStatus);
            this.pnlMainBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlMainBottom.Location = new System.Drawing.Point(0, 105);
            this.pnlMainBottom.Name = "pnlMainBottom";
            this.pnlMainBottom.Size = new System.Drawing.Size(891, 26);
            this.pnlMainBottom.TabIndex = 293;
            // 
            // lblCreatedDateValue
            // 
            this.lblCreatedDateValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedDateValue.CloseButtonVisible = false;
            this.lblCreatedDateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCreatedDateValue.Location = new System.Drawing.Point(550, 0);
            this.lblCreatedDateValue.Name = "lblCreatedDateValue";
            this.lblCreatedDateValue.OptionsButtonVisible = false;
            this.lblCreatedDateValue.Size = new System.Drawing.Size(149, 24);
            this.lblCreatedDateValue.TabIndex = 256;
            this.lblCreatedDateValue.Text = "Created Date";
            // 
            // lblCreatedByText
            // 
            this.lblCreatedByText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedByText.CloseButtonVisible = false;
            this.lblCreatedByText.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblCreatedByText.Location = new System.Drawing.Point(699, 0);
            this.lblCreatedByText.Name = "lblCreatedByText";
            this.lblCreatedByText.OptionsButtonVisible = false;
            this.lblCreatedByText.Size = new System.Drawing.Size(190, 24);
            this.lblCreatedByText.TabIndex = 258;
            this.lblCreatedByText.Text = "Created By";
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblStatus.CloseButtonVisible = false;
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblStatus.Image = ((System.Drawing.Image)(resources.GetObject("lblStatus.Image")));
            this.lblStatus.Location = new System.Drawing.Point(0, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.OptionsButtonVisible = false;
            this.lblStatus.Size = new System.Drawing.Size(550, 24);
            this.lblStatus.TabIndex = 20;
            // 
            // txtRemarks
            // 
            // 
            // 
            // 
            this.txtRemarks.Border.Class = "TextBoxBorder";
            this.txtRemarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtRemarks.Border.WordWrap = true;
            this.txtRemarks.Location = new System.Drawing.Point(111, 8);
            this.txtRemarks.MaxLength = 2000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(365, 91);
            this.txtRemarks.TabIndex = 0;
            this.txtRemarks.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(18, 15);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 214;
            this.lblRemarks.Text = "Remarks";
            // 
            // expandableSplitterTop
            // 
            this.expandableSplitterTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterTop.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitterTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitterTop.ExpandableControl = this.panelTop;
            this.expandableSplitterTop.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterTop.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterTop.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterTop.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterTop.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.Location = new System.Drawing.Point(0, 130);
            this.expandableSplitterTop.Name = "expandableSplitterTop";
            this.expandableSplitterTop.Size = new System.Drawing.Size(891, 3);
            this.expandableSplitterTop.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterTop.TabIndex = 3;
            this.expandableSplitterTop.TabStop = false;
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelTop.Controls.Add(this.tcGeneral);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(891, 105);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 0;
            // 
            // tcGeneral
            // 
            this.tcGeneral.BackColor = System.Drawing.Color.Transparent;
            this.tcGeneral.CanReorderTabs = true;
            this.tcGeneral.Controls.Add(this.tabControlPanel2);
            this.tcGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcGeneral.Location = new System.Drawing.Point(0, 0);
            this.tcGeneral.Name = "tcGeneral";
            this.tcGeneral.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcGeneral.SelectedTabIndex = 0;
            this.tcGeneral.Size = new System.Drawing.Size(891, 105);
            this.tcGeneral.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcGeneral.TabIndex = 0;
            this.tcGeneral.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcGeneral.Tabs.Add(this.tiGeneral);
            this.tcGeneral.TabStop = false;
            this.tcGeneral.Text = "tabControl1";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.lblProduct);
            this.tabControlPanel2.Controls.Add(this.txtQuantity);
            this.tabControlPanel2.Controls.Add(this.txtMaterialIssueNo);
            this.tabControlPanel2.Controls.Add(this.lblMaterialIssueNo);
            this.tabControlPanel2.Controls.Add(this.lblQuantity);
            this.tabControlPanel2.Controls.Add(this.lblIssuedDate);
            this.tabControlPanel2.Controls.Add(this.cboWarehouse);
            this.tabControlPanel2.Controls.Add(this.dtpIssuedDate);
            this.tabControlPanel2.Controls.Add(this.lblWarehouse);
            this.tabControlPanel2.Controls.Add(this.cboBOM);
            this.tabControlPanel2.Controls.Add(this.cboCompany);
            this.tabControlPanel2.Controls.Add(this.lblCompany);
            this.tabControlPanel2.Controls.Add(this.lblProject);
            this.tabControlPanel2.Controls.Add(this.lblIssuedByText);
            this.tabControlPanel2.Controls.Add(this.lblIssuedBy);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(891, 83);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 1;
            this.tabControlPanel2.TabItem = this.tiGeneral;
            // 
            // txtQuantity
            // 
            // 
            // 
            // 
            this.txtQuantity.Border.Class = "TextBoxBorder";
            this.txtQuantity.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtQuantity.Location = new System.Drawing.Point(371, 58);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.ShortcutsEnabled = false;
            this.txtQuantity.Size = new System.Drawing.Size(105, 20);
            this.txtQuantity.TabIndex = 5;
            this.txtQuantity.TextChanged += new System.EventHandler(this.txtQuantity_TextChanged);
            // 
            // txtMaterialIssueNo
            // 
            this.txtMaterialIssueNo.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtMaterialIssueNo.Border.Class = "TextBoxBorder";
            this.txtMaterialIssueNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMaterialIssueNo.Location = new System.Drawing.Point(86, 58);
            this.txtMaterialIssueNo.MaxLength = 20;
            this.txtMaterialIssueNo.Name = "txtMaterialIssueNo";
            this.txtMaterialIssueNo.Size = new System.Drawing.Size(92, 20);
            this.txtMaterialIssueNo.TabIndex = 2;
            this.txtMaterialIssueNo.TabStop = false;
            this.txtMaterialIssueNo.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // lblMaterialIssueNo
            // 
            this.lblMaterialIssueNo.AutoSize = true;
            this.lblMaterialIssueNo.BackColor = System.Drawing.Color.Transparent;
            this.lblMaterialIssueNo.Location = new System.Drawing.Point(18, 62);
            this.lblMaterialIssueNo.Name = "lblMaterialIssueNo";
            this.lblMaterialIssueNo.Size = new System.Drawing.Size(49, 13);
            this.lblMaterialIssueNo.TabIndex = 225;
            this.lblMaterialIssueNo.Text = "Issue No";
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.BackColor = System.Drawing.Color.Transparent;
            this.lblQuantity.Location = new System.Drawing.Point(319, 62);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(46, 13);
            this.lblQuantity.TabIndex = 233;
            this.lblQuantity.Text = "Quantity";
            // 
            // lblIssuedDate
            // 
            this.lblIssuedDate.AutoSize = true;
            this.lblIssuedDate.BackColor = System.Drawing.Color.Transparent;
            this.lblIssuedDate.Location = new System.Drawing.Point(597, 38);
            this.lblIssuedDate.Name = "lblIssuedDate";
            this.lblIssuedDate.Size = new System.Drawing.Size(64, 13);
            this.lblIssuedDate.TabIndex = 226;
            this.lblIssuedDate.Text = "Issued Date";
            this.lblIssuedDate.Visible = false;
            // 
            // cboWarehouse
            // 
            this.cboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWarehouse.DisplayMember = "Text";
            this.cboWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWarehouse.DropDownHeight = 75;
            this.cboWarehouse.FormattingEnabled = true;
            this.cboWarehouse.IntegralHeight = false;
            this.cboWarehouse.ItemHeight = 14;
            this.cboWarehouse.Location = new System.Drawing.Point(86, 32);
            this.cboWarehouse.Name = "cboWarehouse";
            this.cboWarehouse.Size = new System.Drawing.Size(204, 20);
            this.cboWarehouse.TabIndex = 1;
            this.cboWarehouse.SelectedIndexChanged += new System.EventHandler(this.cboWarehouse_SelectedIndexChanged);
            this.cboWarehouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // dtpIssuedDate
            // 
            this.dtpIssuedDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpIssuedDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpIssuedDate.Location = new System.Drawing.Point(184, 58);
            this.dtpIssuedDate.Name = "dtpIssuedDate";
            this.dtpIssuedDate.Size = new System.Drawing.Size(106, 20);
            this.dtpIssuedDate.TabIndex = 3;
            this.dtpIssuedDate.ValueChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.BackColor = System.Drawing.Color.Transparent;
            this.lblWarehouse.Location = new System.Drawing.Point(18, 37);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(62, 13);
            this.lblWarehouse.TabIndex = 230;
            this.lblWarehouse.Text = "Warehouse";
            // 
            // cboBOM
            // 
            this.cboBOM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBOM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBOM.DisplayMember = "Text";
            this.cboBOM.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboBOM.DropDownHeight = 75;
            this.cboBOM.FormattingEnabled = true;
            this.cboBOM.IntegralHeight = false;
            this.cboBOM.ItemHeight = 14;
            this.cboBOM.Location = new System.Drawing.Point(371, 4);
            this.cboBOM.Name = "cboBOM";
            this.cboBOM.Size = new System.Drawing.Size(204, 20);
            this.cboBOM.TabIndex = 4;
            this.cboBOM.SelectedIndexChanged += new System.EventHandler(this.cboBOM_SelectedIndexChanged);
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 75;
            this.cboCompany.Enabled = false;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(86, 6);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(204, 20);
            this.cboCompany.TabIndex = 0;
            this.cboCompany.TabStop = false;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.BackColor = System.Drawing.Color.Transparent;
            this.lblCompany.Location = new System.Drawing.Point(18, 8);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 229;
            this.lblCompany.Text = "Company";
            // 
            // lblProject
            // 
            this.lblProject.AutoSize = true;
            this.lblProject.BackColor = System.Drawing.Color.Transparent;
            this.lblProject.Location = new System.Drawing.Point(319, 8);
            this.lblProject.Name = "lblProject";
            this.lblProject.Size = new System.Drawing.Size(31, 13);
            this.lblProject.TabIndex = 232;
            this.lblProject.Text = "BOM";
            // 
            // lblIssuedByText
            // 
            this.lblIssuedByText.AutoSize = true;
            this.lblIssuedByText.BackColor = System.Drawing.Color.Transparent;
            this.lblIssuedByText.Location = new System.Drawing.Point(689, 9);
            this.lblIssuedByText.Name = "lblIssuedByText";
            this.lblIssuedByText.Size = new System.Drawing.Size(53, 13);
            this.lblIssuedByText.TabIndex = 4;
            this.lblIssuedByText.Text = "Issued By";
            this.lblIssuedByText.Visible = false;
            // 
            // lblIssuedBy
            // 
            this.lblIssuedBy.AutoSize = true;
            this.lblIssuedBy.BackColor = System.Drawing.Color.Transparent;
            this.lblIssuedBy.Location = new System.Drawing.Point(597, 10);
            this.lblIssuedBy.Name = "lblIssuedBy";
            this.lblIssuedBy.Size = new System.Drawing.Size(53, 13);
            this.lblIssuedBy.TabIndex = 228;
            this.lblIssuedBy.Text = "Issued By";
            this.lblIssuedBy.Visible = false;
            // 
            // tiGeneral
            // 
            this.tiGeneral.AttachedControl = this.tabControlPanel2;
            this.tiGeneral.Name = "tiGeneral";
            this.tiGeneral.Text = "General";
            // 
            // bnMaterialIssue
            // 
            this.bnMaterialIssue.AccessibleDescription = "DotNetBar Bar (bnMaterialIssue)";
            this.bnMaterialIssue.AccessibleName = "DotNetBar Bar";
            this.bnMaterialIssue.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.bnMaterialIssue.Dock = System.Windows.Forms.DockStyle.Top;
            this.bnMaterialIssue.DockLine = 1;
            this.bnMaterialIssue.DockOffset = 73;
            this.bnMaterialIssue.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.bnMaterialIssue.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.bnMaterialIssue.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnClear,
            this.bnDelete,
            this.bnPrint,
            this.bnEmail});
            this.bnMaterialIssue.Location = new System.Drawing.Point(0, 0);
            this.bnMaterialIssue.Name = "bnMaterialIssue";
            this.bnMaterialIssue.Size = new System.Drawing.Size(891, 25);
            this.bnMaterialIssue.Stretch = true;
            this.bnMaterialIssue.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.bnMaterialIssue.TabIndex = 12;
            this.bnMaterialIssue.TabStop = false;
            this.bnMaterialIssue.Text = "bar2";
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnAddNewItem.Image = global::MyBooksERP.Properties.Resources.Add;
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlEnter);
            this.bnAddNewItem.Text = "Add";
            this.bnAddNewItem.Tooltip = "Add New Information";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnSaveItem.Image = global::MyBooksERP.Properties.Resources.save;
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Text = "Save";
            this.bnSaveItem.Tooltip = "Save";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnClear
            // 
            this.bnClear.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.bnClear.Name = "bnClear";
            this.bnClear.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.bnClear.Text = "Clear";
            this.bnClear.Tooltip = "Clear";
            this.bnClear.Visible = false;
            this.bnClear.Click += new System.EventHandler(this.bnClear_Click);
            // 
            // bnDelete
            // 
            this.bnDelete.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnDelete.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.bnDelete.Name = "bnDelete";
            this.bnDelete.Text = "Delete";
            this.bnDelete.Tooltip = "Delete";
            this.bnDelete.Click += new System.EventHandler(this.bnDelete_Click);
            // 
            // bnPrint
            // 
            this.bnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.bnPrint.Text = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // bnEmail
            // 
            this.bnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.bnEmail.Text = "Email";
            this.bnEmail.Click += new System.EventHandler(this.bnEmail_Click);
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(0, 0);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 514);
            this.dockSite1.TabIndex = 26;
            this.dockSite1.TabStop = false;
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(200, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 514);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 25;
            this.expandableSplitterLeft.TabStop = false;
            this.expandableSplitterLeft.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitterLeft_ExpandedChanged);
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.dgvMaterialIssueDisplay);
            this.PanelLeft.Controls.Add(this.epMaterialIssue);
            this.PanelLeft.Controls.Add(this.lblSCountStatus);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(200, 514);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 4;
            this.PanelLeft.Text = "panelEx1";
            // 
            // dgvMaterialIssueDisplay
            // 
            this.dgvMaterialIssueDisplay.AllowUserToAddRows = false;
            this.dgvMaterialIssueDisplay.AllowUserToDeleteRows = false;
            this.dgvMaterialIssueDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMaterialIssueDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dgvMaterialIssueDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMaterialIssueDisplay.DefaultCellStyle = dataGridViewCellStyle18;
            this.dgvMaterialIssueDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMaterialIssueDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvMaterialIssueDisplay.Location = new System.Drawing.Point(0, 220);
            this.dgvMaterialIssueDisplay.Name = "dgvMaterialIssueDisplay";
            this.dgvMaterialIssueDisplay.ReadOnly = true;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMaterialIssueDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dgvMaterialIssueDisplay.RowHeadersVisible = false;
            this.dgvMaterialIssueDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMaterialIssueDisplay.Size = new System.Drawing.Size(200, 268);
            this.dgvMaterialIssueDisplay.TabIndex = 7;
            this.dgvMaterialIssueDisplay.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialIssueDisplay_CellClick);
            // 
            // epMaterialIssue
            // 
            this.epMaterialIssue.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.epMaterialIssue.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.epMaterialIssue.Controls.Add(this.panelLeftTop);
            this.epMaterialIssue.Dock = System.Windows.Forms.DockStyle.Top;
            this.epMaterialIssue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.epMaterialIssue.Location = new System.Drawing.Point(0, 0);
            this.epMaterialIssue.Name = "epMaterialIssue";
            this.epMaterialIssue.Size = new System.Drawing.Size(200, 220);
            this.epMaterialIssue.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.epMaterialIssue.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.epMaterialIssue.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.epMaterialIssue.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.epMaterialIssue.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.epMaterialIssue.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.epMaterialIssue.Style.GradientAngle = 90;
            this.epMaterialIssue.TabIndex = 5;
            this.epMaterialIssue.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.epMaterialIssue.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.epMaterialIssue.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.epMaterialIssue.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.epMaterialIssue.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.epMaterialIssue.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.epMaterialIssue.TitleStyle.GradientAngle = 90;
            this.epMaterialIssue.TitleText = "Search";
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelLeftTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelLeftTop.Controls.Add(this.cboSBOM);
            this.panelLeftTop.Controls.Add(this.lblSearchProject);
            this.panelLeftTop.Controls.Add(this.lblSIssueNo);
            this.panelLeftTop.Controls.Add(this.dtpSTo);
            this.panelLeftTop.Controls.Add(this.dtpSFrom);
            this.panelLeftTop.Controls.Add(this.lblTo);
            this.panelLeftTop.Controls.Add(this.lblFrom);
            this.panelLeftTop.Controls.Add(this.cboSIssuedBy);
            this.panelLeftTop.Controls.Add(this.lblSIssuedBy);
            this.panelLeftTop.Controls.Add(this.cboSWarehouse);
            this.panelLeftTop.Controls.Add(this.lblSWarehouse);
            this.panelLeftTop.Controls.Add(this.btnRefresh);
            this.panelLeftTop.Controls.Add(this.txtSearch);
            this.panelLeftTop.Controls.Add(this.cboSCompany);
            this.panelLeftTop.Controls.Add(this.lblSCompany);
            this.panelLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftTop.Location = new System.Drawing.Point(0, 26);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(200, 194);
            this.panelLeftTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelLeftTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelLeftTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelLeftTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelLeftTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelLeftTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelLeftTop.Style.GradientAngle = 90;
            this.panelLeftTop.TabIndex = 6;
            // 
            // cboSBOM
            // 
            this.cboSBOM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSBOM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSBOM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSBOM.DisplayMember = "Text";
            this.cboSBOM.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSBOM.DropDownHeight = 75;
            this.cboSBOM.FormattingEnabled = true;
            this.cboSBOM.IntegralHeight = false;
            this.cboSBOM.ItemHeight = 14;
            this.cboSBOM.Location = new System.Drawing.Point(78, 56);
            this.cboSBOM.Name = "cboSBOM";
            this.cboSBOM.Size = new System.Drawing.Size(117, 20);
            this.cboSBOM.TabIndex = 2;
            // 
            // lblSearchProject
            // 
            this.lblSearchProject.AutoSize = true;
            this.lblSearchProject.Location = new System.Drawing.Point(3, 59);
            this.lblSearchProject.Name = "lblSearchProject";
            this.lblSearchProject.Size = new System.Drawing.Size(31, 13);
            this.lblSearchProject.TabIndex = 263;
            this.lblSearchProject.Text = "BOM";
            // 
            // lblSIssueNo
            // 
            this.lblSIssueNo.AutoSize = true;
            this.lblSIssueNo.Location = new System.Drawing.Point(3, 150);
            this.lblSIssueNo.Name = "lblSIssueNo";
            this.lblSIssueNo.Size = new System.Drawing.Size(49, 13);
            this.lblSIssueNo.TabIndex = 261;
            this.lblSIssueNo.Text = "Issue No";
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(78, 124);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.Size = new System.Drawing.Size(117, 20);
            this.dtpSTo.TabIndex = 5;
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(78, 101);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.Size = new System.Drawing.Size(117, 20);
            this.dtpSFrom.TabIndex = 4;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(3, 105);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 258;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(3, 129);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 257;
            this.lblFrom.Text = "From";
            // 
            // cboSIssuedBy
            // 
            this.cboSIssuedBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSIssuedBy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSIssuedBy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSIssuedBy.DisplayMember = "Text";
            this.cboSIssuedBy.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSIssuedBy.DropDownHeight = 75;
            this.cboSIssuedBy.FormattingEnabled = true;
            this.cboSIssuedBy.IntegralHeight = false;
            this.cboSIssuedBy.ItemHeight = 14;
            this.cboSIssuedBy.Location = new System.Drawing.Point(78, 78);
            this.cboSIssuedBy.Name = "cboSIssuedBy";
            this.cboSIssuedBy.Size = new System.Drawing.Size(117, 20);
            this.cboSIssuedBy.TabIndex = 3;
            this.cboSIssuedBy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblSIssuedBy
            // 
            this.lblSIssuedBy.AutoSize = true;
            this.lblSIssuedBy.Location = new System.Drawing.Point(3, 81);
            this.lblSIssuedBy.Name = "lblSIssuedBy";
            this.lblSIssuedBy.Size = new System.Drawing.Size(53, 13);
            this.lblSIssuedBy.TabIndex = 128;
            this.lblSIssuedBy.Text = "Issued By";
            // 
            // cboSWarehouse
            // 
            this.cboSWarehouse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSWarehouse.DisplayMember = "Text";
            this.cboSWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSWarehouse.DropDownHeight = 75;
            this.cboSWarehouse.FormattingEnabled = true;
            this.cboSWarehouse.IntegralHeight = false;
            this.cboSWarehouse.ItemHeight = 14;
            this.cboSWarehouse.Location = new System.Drawing.Point(78, 33);
            this.cboSWarehouse.Name = "cboSWarehouse";
            this.cboSWarehouse.Size = new System.Drawing.Size(117, 20);
            this.cboSWarehouse.TabIndex = 1;
            this.cboSWarehouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblSWarehouse
            // 
            this.lblSWarehouse.AutoSize = true;
            this.lblSWarehouse.Location = new System.Drawing.Point(3, 36);
            this.lblSWarehouse.Name = "lblSWarehouse";
            this.lblSWarehouse.Size = new System.Drawing.Size(62, 13);
            this.lblSWarehouse.TabIndex = 125;
            this.lblSWarehouse.Text = "Warehouse";
            // 
            // btnRefresh
            // 
            this.btnRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnRefresh.Location = new System.Drawing.Point(123, 168);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(71, 23);
            this.btnRefresh.TabIndex = 7;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(78, 147);
            this.txtSearch.MaxLength = 20;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(117, 20);
            this.txtSearch.TabIndex = 6;
            this.txtSearch.WatermarkEnabled = false;
            this.txtSearch.WatermarkText = "Issue No";
            // 
            // cboSCompany
            // 
            this.cboSCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSCompany.DisplayMember = "Text";
            this.cboSCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSCompany.DropDownHeight = 75;
            this.cboSCompany.FormattingEnabled = true;
            this.cboSCompany.IntegralHeight = false;
            this.cboSCompany.ItemHeight = 14;
            this.cboSCompany.Location = new System.Drawing.Point(78, 10);
            this.cboSCompany.Name = "cboSCompany";
            this.cboSCompany.Size = new System.Drawing.Size(117, 20);
            this.cboSCompany.TabIndex = 0;
            this.cboSCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblSCompany
            // 
            this.lblSCompany.AutoSize = true;
            this.lblSCompany.Location = new System.Drawing.Point(3, 13);
            this.lblSCompany.Name = "lblSCompany";
            this.lblSCompany.Size = new System.Drawing.Size(51, 13);
            this.lblSCompany.TabIndex = 109;
            this.lblSCompany.Text = "Company";
            // 
            // lblSCountStatus
            // 
            // 
            // 
            // 
            this.lblSCountStatus.BackgroundStyle.Class = "";
            this.lblSCountStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSCountStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblSCountStatus.Location = new System.Drawing.Point(0, 488);
            this.lblSCountStatus.Name = "lblSCountStatus";
            this.lblSCountStatus.Size = new System.Drawing.Size(200, 26);
            this.lblSCountStatus.TabIndex = 8;
            this.lblSCountStatus.Text = "...";
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(0, 0);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 514);
            this.dockSite5.TabIndex = 27;
            this.dockSite5.TabStop = false;
            // 
            // tmrMaterialIssue
            // 
            this.tmrMaterialIssue.Tick += new System.EventHandler(this.tmrMaterialIssue_Tick);
            // 
            // ErrMaterialIssue
            // 
            this.ErrMaterialIssue.ContainerControl = this;
            this.ErrMaterialIssue.RightToLeft = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 20;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "Item Name";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "BatchID";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Batch No";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 175;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn6.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn6.MaxInputLength = 12;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 110;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewTextBoxColumn7.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn7.MaxInputLength = 12;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 110;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewTextBoxColumn8.HeaderText = "Net Amount";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 125;
            // 
            // lblProduct
            // 
            this.lblProduct.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblProduct.BackgroundStyle.Class = "";
            this.lblProduct.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblProduct.ForeColor = System.Drawing.Color.Black;
            this.lblProduct.Location = new System.Drawing.Point(322, 32);
            this.lblProduct.Name = "lblProduct";
            this.lblProduct.Size = new System.Drawing.Size(565, 23);
            this.lblProduct.TabIndex = 237;
            this.lblProduct.Text = "Product";
            this.lblProduct.WordWrap = true;
            // 
            // FrmMaterialIssue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 514);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.expandableSplitterLeft);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.dockSite5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMaterialIssue";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Material Issue";
            this.Load += new System.EventHandler(this.FrmMaterialIssue_Load);
            this.panelMain.ResumeLayout(false);
            this.panelMiddle.ResumeLayout(false);
            this.panelTcMaterialIssue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcMaterialIssue)).EndInit();
            this.tcMaterialIssue.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialIssue)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.pnlBottomRight.ResumeLayout(false);
            this.pnlBottomRight.PerformLayout();
            this.pnlMainBottom.ResumeLayout(false);
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).EndInit();
            this.tcGeneral.ResumeLayout(false);
            this.tabControlPanel2.ResumeLayout(false);
            this.tabControlPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnMaterialIssue)).EndInit();
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialIssueDisplay)).EndInit();
            this.epMaterialIssue.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrMaterialIssue)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx panelMain;
        private DevComponents.DotNetBar.PanelEx panelMiddle;
        private DevComponents.DotNetBar.TabControl tcMaterialIssue;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private DevComponents.DotNetBar.TabItem tiItemDetails;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterTop;
        private DevComponents.DotNetBar.PanelEx panelTop;
        private DevComponents.DotNetBar.Bar bnMaterialIssue;
        private DevComponents.DotNetBar.ButtonItem bnAddNewItem;
        private DevComponents.DotNetBar.ButtonItem bnSaveItem;
        private DevComponents.DotNetBar.ButtonItem bnClear;
        private DevComponents.DotNetBar.ButtonItem bnDelete;
        private DevComponents.DotNetBar.ButtonItem bnPrint;
        private DevComponents.DotNetBar.ButtonItem bnEmail;
        private DevComponents.DotNetBar.DockSite dockSite1;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterLeft;
        private DevComponents.DotNetBar.PanelEx PanelLeft;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvMaterialIssueDisplay;
        private DevComponents.DotNetBar.ExpandablePanel epMaterialIssue;
        private DevComponents.DotNetBar.PanelEx panelLeftTop;
        private System.Windows.Forms.Label lblSIssueNo;
        private System.Windows.Forms.DateTimePicker dtpSTo;
        private System.Windows.Forms.DateTimePicker dtpSFrom;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSIssuedBy;
        private System.Windows.Forms.Label lblSIssuedBy;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSWarehouse;
        private System.Windows.Forms.Label lblSWarehouse;
        private DevComponents.DotNetBar.ButtonX btnRefresh;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSCompany;
        private System.Windows.Forms.Label lblSCompany;
        private DevComponents.DotNetBar.LabelX lblSCountStatus;
        private DevComponents.DotNetBar.DockSite dockSite5;
        private DevComponents.DotNetBar.TabControl tcGeneral;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboWarehouse;
        private System.Windows.Forms.Label lblWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private System.Windows.Forms.Label lblCompany;
        private System.Windows.Forms.Label lblIssuedByText;
        private System.Windows.Forms.Label lblIssuedBy;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMaterialIssueNo;
        private System.Windows.Forms.DateTimePicker dtpIssuedDate;
        private System.Windows.Forms.Label lblIssuedDate;
        private System.Windows.Forms.Label lblMaterialIssueNo;
        private DevComponents.DotNetBar.TabItem tiGeneral;
        private System.Windows.Forms.Timer tmrMaterialIssue;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private ClsInnerGridBar dgvMaterialIssue;
        internal System.Windows.Forms.ErrorProvider ErrMaterialIssue;
        private DevComponents.DotNetBar.PanelEx panelTcMaterialIssue;
        private DevComponents.DotNetBar.PanelEx pnlBottom;
        private DevComponents.DotNetBar.PanelEx pnlBottomRight;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCurrency;
        private System.Windows.Forms.Label lblCurrency;
        private System.Windows.Forms.Label lblNetAmount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtNetAmount;
        private System.Windows.Forms.Panel pnlMainBottom;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedByText;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedDateValue;
        private DevComponents.DotNetBar.Controls.WarningBox lblStatus;
        private DevComponents.DotNetBar.Controls.TextBoxX txtRemarks;
        private System.Windows.Forms.Label lblRemarks;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboBOM;
        private System.Windows.Forms.Label lblProject;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSBOM;
        private System.Windows.Forms.Label lblSearchProject;
        private System.Windows.Forms.Label lblQuantity;
        private DemoClsDataGridview.DecimalDotNetBarTextBox txtQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColBatchID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColBatchNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColExpiryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColQtyAvailable;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColActualQuantity;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColNetAmount;
        private DevComponents.DotNetBar.LabelX lblProduct;
    }
}