﻿namespace MyBooksERP
{
    partial class FrmLocationFilter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLocationFilter));
            this.pnlXMain = new DevComponents.DotNetBar.PanelEx();
            this.CboXBatch = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblbatch = new DevComponents.DotNetBar.LabelX();
            this.CboXItem = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblItem = new DevComponents.DotNetBar.LabelX();
            this.CboXLot = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblLot = new DevComponents.DotNetBar.LabelX();
            this.cboXRow = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblRow = new DevComponents.DotNetBar.LabelX();
            this.CboXBlock = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboXLocation = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblBlock = new DevComponents.DotNetBar.LabelX();
            this.LblLocation = new DevComponents.DotNetBar.LabelX();
            this.btnXOk = new DevComponents.DotNetBar.ButtonX();
            this.btnXCancel = new DevComponents.DotNetBar.ButtonX();
            this.pnlXMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlXMain
            // 
            this.pnlXMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlXMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlXMain.Controls.Add(this.CboXBatch);
            this.pnlXMain.Controls.Add(this.lblbatch);
            this.pnlXMain.Controls.Add(this.CboXItem);
            this.pnlXMain.Controls.Add(this.lblItem);
            this.pnlXMain.Controls.Add(this.CboXLot);
            this.pnlXMain.Controls.Add(this.lblLot);
            this.pnlXMain.Controls.Add(this.cboXRow);
            this.pnlXMain.Controls.Add(this.lblRow);
            this.pnlXMain.Controls.Add(this.CboXBlock);
            this.pnlXMain.Controls.Add(this.CboXLocation);
            this.pnlXMain.Controls.Add(this.lblBlock);
            this.pnlXMain.Controls.Add(this.LblLocation);
            this.pnlXMain.Location = new System.Drawing.Point(9, 5);
            this.pnlXMain.Name = "pnlXMain";
            this.pnlXMain.Size = new System.Drawing.Size(317, 165);
            this.pnlXMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlXMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlXMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlXMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlXMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlXMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlXMain.Style.GradientAngle = 90;
            this.pnlXMain.TabIndex = 0;
            // 
            // CboXBatch
            // 
            this.CboXBatch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboXBatch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboXBatch.DisplayMember = "Text";
            this.CboXBatch.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboXBatch.FormattingEnabled = true;
            this.CboXBatch.ItemHeight = 14;
            this.CboXBatch.Location = new System.Drawing.Point(77, 134);
            this.CboXBatch.Name = "CboXBatch";
            this.CboXBatch.Size = new System.Drawing.Size(225, 20);
            this.CboXBatch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboXBatch.TabIndex = 5;
            this.CboXBatch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxX_KeyDown);
            // 
            // lblbatch
            // 
            this.lblbatch.AutoSize = true;
            // 
            // 
            // 
            this.lblbatch.BackgroundStyle.Class = "";
            this.lblbatch.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblbatch.Location = new System.Drawing.Point(10, 134);
            this.lblbatch.Name = "lblbatch";
            this.lblbatch.Size = new System.Drawing.Size(31, 15);
            this.lblbatch.TabIndex = 44;
            this.lblbatch.Text = "Batch";
            // 
            // CboXItem
            // 
            this.CboXItem.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboXItem.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboXItem.DisplayMember = "Text";
            this.CboXItem.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboXItem.FormattingEnabled = true;
            this.CboXItem.ItemHeight = 14;
            this.CboXItem.Location = new System.Drawing.Point(77, 109);
            this.CboXItem.Name = "CboXItem";
            this.CboXItem.Size = new System.Drawing.Size(225, 20);
            this.CboXItem.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboXItem.TabIndex = 4;
            this.CboXItem.SelectionChangeCommitted += new System.EventHandler(this.CboXItem_SelectionChangeCommitted);
            this.CboXItem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxX_KeyDown);
            // 
            // lblItem
            // 
            this.lblItem.AutoSize = true;
            // 
            // 
            // 
            this.lblItem.BackgroundStyle.Class = "";
            this.lblItem.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblItem.Location = new System.Drawing.Point(10, 109);
            this.lblItem.Name = "lblItem";
            this.lblItem.Size = new System.Drawing.Size(24, 15);
            this.lblItem.TabIndex = 42;
            this.lblItem.Text = "Item";
            // 
            // CboXLot
            // 
            this.CboXLot.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboXLot.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboXLot.DisplayMember = "Text";
            this.CboXLot.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboXLot.FormattingEnabled = true;
            this.CboXLot.ItemHeight = 14;
            this.CboXLot.Location = new System.Drawing.Point(77, 84);
            this.CboXLot.Name = "CboXLot";
            this.CboXLot.Size = new System.Drawing.Size(225, 20);
            this.CboXLot.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboXLot.TabIndex = 3;
            this.CboXLot.SelectionChangeCommitted += new System.EventHandler(this.CboXLot_SelectionChangeCommitted);
            this.CboXLot.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxX_KeyDown);
            // 
            // lblLot
            // 
            this.lblLot.AutoSize = true;
            // 
            // 
            // 
            this.lblLot.BackgroundStyle.Class = "";
            this.lblLot.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblLot.Location = new System.Drawing.Point(10, 84);
            this.lblLot.Name = "lblLot";
            this.lblLot.Size = new System.Drawing.Size(18, 15);
            this.lblLot.TabIndex = 40;
            this.lblLot.Text = "Lot";
            // 
            // cboXRow
            // 
            this.cboXRow.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboXRow.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboXRow.DisplayMember = "Text";
            this.cboXRow.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboXRow.FormattingEnabled = true;
            this.cboXRow.ItemHeight = 14;
            this.cboXRow.Location = new System.Drawing.Point(77, 34);
            this.cboXRow.Name = "cboXRow";
            this.cboXRow.Size = new System.Drawing.Size(225, 20);
            this.cboXRow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboXRow.TabIndex = 1;
            this.cboXRow.SelectionChangeCommitted += new System.EventHandler(this.cboXRow_SelectionChangeCommitted);
            this.cboXRow.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxX_KeyDown);
            // 
            // lblRow
            // 
            this.lblRow.AutoSize = true;
            // 
            // 
            // 
            this.lblRow.BackgroundStyle.Class = "";
            this.lblRow.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblRow.Location = new System.Drawing.Point(10, 34);
            this.lblRow.Name = "lblRow";
            this.lblRow.Size = new System.Drawing.Size(25, 15);
            this.lblRow.TabIndex = 38;
            this.lblRow.Text = "Row";
            // 
            // CboXBlock
            // 
            this.CboXBlock.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboXBlock.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboXBlock.DisplayMember = "Text";
            this.CboXBlock.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboXBlock.FormattingEnabled = true;
            this.CboXBlock.ItemHeight = 14;
            this.CboXBlock.Location = new System.Drawing.Point(77, 59);
            this.CboXBlock.Name = "CboXBlock";
            this.CboXBlock.Size = new System.Drawing.Size(225, 20);
            this.CboXBlock.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboXBlock.TabIndex = 2;
            this.CboXBlock.SelectionChangeCommitted += new System.EventHandler(this.CboXBlock_SelectionChangeCommitted);
            this.CboXBlock.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxX_KeyDown);
            // 
            // CboXLocation
            // 
            this.CboXLocation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboXLocation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboXLocation.DisplayMember = "Text";
            this.CboXLocation.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboXLocation.FormattingEnabled = true;
            this.CboXLocation.ItemHeight = 14;
            this.CboXLocation.Location = new System.Drawing.Point(77, 9);
            this.CboXLocation.Name = "CboXLocation";
            this.CboXLocation.Size = new System.Drawing.Size(225, 20);
            this.CboXLocation.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboXLocation.TabIndex = 0;
            this.CboXLocation.SelectionChangeCommitted += new System.EventHandler(this.CboXLocation_SelectionChangeCommitted);
            this.CboXLocation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxX_KeyDown);
            // 
            // lblBlock
            // 
            this.lblBlock.AutoSize = true;
            // 
            // 
            // 
            this.lblBlock.BackgroundStyle.Class = "";
            this.lblBlock.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblBlock.Location = new System.Drawing.Point(10, 59);
            this.lblBlock.Name = "lblBlock";
            this.lblBlock.Size = new System.Drawing.Size(29, 15);
            this.lblBlock.TabIndex = 35;
            this.lblBlock.Text = "Block";
            // 
            // LblLocation
            // 
            this.LblLocation.AutoSize = true;
            // 
            // 
            // 
            this.LblLocation.BackgroundStyle.Class = "";
            this.LblLocation.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblLocation.Location = new System.Drawing.Point(10, 9);
            this.LblLocation.Name = "LblLocation";
            this.LblLocation.Size = new System.Drawing.Size(44, 15);
            this.LblLocation.TabIndex = 34;
            this.LblLocation.Text = "Location";
            // 
            // btnXOk
            // 
            this.btnXOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnXOk.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnXOk.Location = new System.Drawing.Point(194, 176);
            this.btnXOk.Name = "btnXOk";
            this.btnXOk.Size = new System.Drawing.Size(63, 23);
            this.btnXOk.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnXOk.TabIndex = 0;
            this.btnXOk.Text = "&Ok";
            this.btnXOk.Click += new System.EventHandler(this.btnXOk_Click);
            // 
            // btnXCancel
            // 
            this.btnXCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnXCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnXCancel.Location = new System.Drawing.Point(263, 176);
            this.btnXCancel.Name = "btnXCancel";
            this.btnXCancel.Size = new System.Drawing.Size(63, 23);
            this.btnXCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnXCancel.TabIndex = 1;
            this.btnXCancel.Text = "&Cancel";
            this.btnXCancel.Click += new System.EventHandler(this.btnXCancel_Click);
            // 
            // FrmLocationFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 204);
            this.Controls.Add(this.btnXCancel);
            this.Controls.Add(this.btnXOk);
            this.Controls.Add(this.pnlXMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLocationFilter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Filter";
            this.Load += new System.EventHandler(this.FrmLocationFilter_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmLocationFilter_FormClosing);
            this.pnlXMain.ResumeLayout(false);
            this.pnlXMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx pnlXMain;
        private DevComponents.DotNetBar.ButtonX btnXOk;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboXBatch;
        private DevComponents.DotNetBar.LabelX lblbatch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboXItem;
        private DevComponents.DotNetBar.LabelX lblItem;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboXLot;
        private DevComponents.DotNetBar.LabelX lblLot;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboXRow;
        private DevComponents.DotNetBar.LabelX lblRow;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboXBlock;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboXLocation;
        private DevComponents.DotNetBar.LabelX lblBlock;
        private DevComponents.DotNetBar.LabelX LblLocation;
        private DevComponents.DotNetBar.ButtonX btnXCancel;
    }
}