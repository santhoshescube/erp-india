﻿namespace MyBooksERP
{
    partial class frmItemMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmItemMaster));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            this.CommandZoom = new DevComponents.DotNetBar.Command(this.components);
            this.dockContainerItem1 = new DevComponents.DotNetBar.DockContainerItem();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvItems = new ClsDataGirdViewX();
            this.dgvColItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColRowNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelLeftTop = new DevComponents.DotNetBar.PanelEx();
            this.lnkLabelAdvanceSearch = new System.Windows.Forms.LinkLabel();
            this.lblSItemCode = new System.Windows.Forms.Label();
            this.txtSearchCode = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnSearch = new DevComponents.DotNetBar.ButtonX();
            this.txtSearchDescription = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboSearchSubCategory = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboSearchCategory = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.LblSCategory = new System.Windows.Forms.Label();
            this.LblSSubCategory = new System.Windows.Forms.Label();
            this.LblSItemName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCountStatus = new DevComponents.DotNetBar.LabelX();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.dotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.dockSite4 = new DevComponents.DotNetBar.DockSite();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.dockSite2 = new DevComponents.DotNetBar.DockSite();
            this.dockSite8 = new DevComponents.DotNetBar.DockSite();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.dockSite6 = new DevComponents.DotNetBar.DockSite();
            this.dockSite3 = new DevComponents.DotNetBar.DockSite();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.pnlBottom = new DevComponents.DotNetBar.PanelEx();
            this.tabItemMaster = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.lblImageText = new System.Windows.Forms.Label();
            this.btnNext = new DevComponents.DotNetBar.ButtonX();
            this.btnPrev = new DevComponents.DotNetBar.ButtonX();
            this.BtnAttach = new DevComponents.DotNetBar.ButtonX();
            this.btnAddImage = new DevComponents.DotNetBar.ButtonX();
            this.mnuAddImage = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.btnBarcodeCreate = new DevComponents.DotNetBar.ButtonX();
            this.btnAddBarcodeImage = new DevComponents.DotNetBar.ButtonX();
            this.mnuBarcodeAddImage = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuPrintBarcode = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCreateBarcode = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRemoveBarcode = new System.Windows.Forms.ToolStripMenuItem();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCreateBarCode = new DevComponents.DotNetBar.ButtonX();
            this.pbBarCode = new System.Windows.Forms.PictureBox();
            this.txtNote = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label12 = new System.Windows.Forms.Label();
            this.LblNote = new System.Windows.Forms.Label();
            this.pbItemPhoto = new System.Windows.Forms.PictureBox();
            this.LblGuaranteePeriod = new System.Windows.Forms.Label();
            this.txtGuaranteePeriod = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtWarrantyPeriod = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboTaxCode = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnTaxCode = new DevComponents.DotNetBar.ButtonX();
            this.cboMadeIn = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnMadeIn = new DevComponents.DotNetBar.ButtonX();
            this.cboManufacturer = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnManufacturer = new DevComponents.DotNetBar.ButtonX();
            this.txtModel = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label11 = new System.Windows.Forms.Label();
            this.LblWarrantyPeriod = new System.Windows.Forms.Label();
            this.LblMadeIn = new System.Windows.Forms.Label();
            this.LblModel = new System.Windows.Forms.Label();
            this.LblManufacturer = new System.Windows.Forms.Label();
            this.LblTaxCode = new System.Windows.Forms.Label();
            this.LblBarcode = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.tbpGeneral = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel6 = new DevComponents.DotNetBar.TabControlPanel();
            this.lblPurchaseRate = new System.Windows.Forms.Label();
            this.rdbLandingCost = new System.Windows.Forms.RadioButton();
            this.txtActualRate = new DemoClsDataGridview.DecimalDotNetBarTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.rdbActualRate = new System.Windows.Forms.RadioButton();
            this.rdbPurchaseRate = new System.Windows.Forms.RadioButton();
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.dgvBatchwiseCosting = new ClsInnerGridBar();
            this.ColBatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColBatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPurchaseRate = new DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn();
            this.ColLandingCost = new DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn();
            this.ColActualRate = new DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn();
            this.ColSaleRate = new DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn();
            this.cboPricingScheme = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblPricingScheme = new System.Windows.Forms.Label();
            this.lblSaleRate = new System.Windows.Forms.Label();
            this.cboCostingMethod = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.LblCostingMethod = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.shapeContainer3 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.txtPurchaseRate = new DemoClsDataGridview.DecimalDotNetBarTextBox();
            this.txtSaleRate = new DemoClsDataGridview.DecimalDotNetBarTextBox();
            this.tbpCosting = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvItemLocations = new ClsInnerGridBar();
            this.dgvColWarehouse = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvColLocation = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvColRow = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvColBlock = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvColLot = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvColReorderPoint = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColReorderQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColSerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.txtReorderPoint = new DemoClsDataGridview.DecimalDotNetBarTextBox();
            this.lblReorderpoint = new System.Windows.Forms.Label();
            this.txtReorderQuantity = new DemoClsDataGridview.DecimalDotNetBarTextBox();
            this.lblReorderQuantity = new System.Windows.Forms.Label();
            this.tbpLocation = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel7 = new DevComponents.DotNetBar.TabControlPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnAddFeature = new DevComponents.DotNetBar.ButtonX();
            this.dgvFeatures = new ClsDataGirdViewX();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvDimensions = new ClsDataGirdViewX();
            this.clmCboDimension = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.clmDimensionValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmCboDimensionUOM = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabDimensionAndFeatures = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.chkUOMSelectAll = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboDefaultSalesUom = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboDefaultPurchaseUom = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LblUnitOfMeasurements = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.dgvUnitsOfMeasurements = new ClsInnerGridBar();
            this.dgvClmChkSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgvColUomType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbpMeasurements = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel5 = new DevComponents.DotNetBar.TabControlPanel();
            this.webBatchDetails = new System.Windows.Forms.WebBrowser();
            this.tbpBatch = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel13 = new DevComponents.DotNetBar.TabControlPanel();
            this.gbSaleRateHistory = new System.Windows.Forms.GroupBox();
            this.dgvRateHistory = new ClsDataGirdViewX();
            this.lblNoSaleRecordsFound = new System.Windows.Forms.Label();
            this.tpbSaleRate = new DevComponents.DotNetBar.TabItem(this.components);
            this.panelGridBottom = new DevComponents.DotNetBar.PanelEx();
            this.lblItemMasterStatus = new DevComponents.DotNetBar.LabelX();
            this.expandableSplitterTop = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.label36 = new System.Windows.Forms.Label();
            this.cboProductType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtCutOffPrice = new DemoClsDataGridview.DecimalTextBox();
            this.lblCutOffPrice = new System.Windows.Forms.Label();
            this.chkExpiryDateMandatory = new System.Windows.Forms.CheckBox();
            this.txtDescriptionArabic = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label5 = new System.Windows.Forms.Label();
            this.txtShortName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtDescription = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtItemCode = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label10 = new System.Windows.Forms.Label();
            this.LblCategory = new System.Windows.Forms.Label();
            this.LblItemType = new System.Windows.Forms.Label();
            this.cboCategory = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboSubCategory = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.label2 = new System.Windows.Forms.Label();
            this.cboBaseUom = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnSubCategory = new DevComponents.DotNetBar.ButtonX();
            this.btnCategory = new DevComponents.DotNetBar.ButtonX();
            this.btnBaseUom = new DevComponents.DotNetBar.ButtonX();
            this.LblTCompany = new System.Windows.Forms.Label();
            this.LblTVendorAddress = new System.Windows.Forms.Label();
            this.LblTVendor = new System.Windows.Forms.Label();
            this.ItemMasterBindingNavigator = new DevComponents.DotNetBar.Bar();
            this.bnAddNewItem = new DevComponents.DotNetBar.ButtonItem();
            this.bnSaveItem = new DevComponents.DotNetBar.ButtonItem();
            this.bnCancel = new DevComponents.DotNetBar.ButtonItem();
            this.bnDeleteItem = new DevComponents.DotNetBar.ButtonItem();
            this.bnUom = new DevComponents.DotNetBar.ButtonItem();
            this.btnUomConversion = new DevComponents.DotNetBar.ButtonItem();
            this.btnPricingScheme = new DevComponents.DotNetBar.ButtonItem();
            this.bnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.bnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.tabComponents = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabProductionStage = new DevComponents.DotNetBar.TabItem(this.components);
            this.errorItemMaster = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tmItemMaster = new System.Windows.Forms.Timer(this.components);
            this.tabControlPanel4 = new DevComponents.DotNetBar.TabControlPanel();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.tabControlPanel8 = new DevComponents.DotNetBar.TabControlPanel();
            this.clsInnerGridBar1 = new ClsInnerGridBar();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelEx3 = new DevComponents.DotNetBar.PanelEx();
            this.decimalDotNetBarTextBox1 = new DemoClsDataGridview.DecimalDotNetBarTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.decimalDotNetBarTextBox2 = new DemoClsDataGridview.DecimalDotNetBarTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tabControlPanel9 = new DevComponents.DotNetBar.TabControlPanel();
            this.decimalDotNetBarTextBox3 = new DemoClsDataGridview.DecimalDotNetBarTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.clsInnerGridBar2 = new ClsInnerGridBar();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewDoubleInputColumn1 = new DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn();
            this.dataGridViewDoubleInputColumn2 = new DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn();
            this.dataGridViewDoubleInputColumn3 = new DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn();
            this.comboBoxEx1 = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.comboBoxEx2 = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.shapeContainer4 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.decimalDotNetBarTextBox4 = new DemoClsDataGridview.DecimalDotNetBarTextBox();
            this.decimalDotNetBarTextBox5 = new DemoClsDataGridview.DecimalDotNetBarTextBox();
            this.tabControlPanel10 = new DevComponents.DotNetBar.TabControlPanel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.comboBoxEx3 = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboBoxEx4 = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.shapeContainer5 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape7 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.clsInnerGridBar3 = new ClsInnerGridBar();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControlPanel11 = new DevComponents.DotNetBar.TabControlPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.clsDataGirdViewX1 = new ClsDataGirdViewX();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.clsDataGirdViewX2 = new ClsDataGirdViewX();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabControlPanel12 = new DevComponents.DotNetBar.TabControlPanel();
            this.label24 = new System.Windows.Forms.Label();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX6 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX7 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX8 = new DevComponents.DotNetBar.ButtonX();
            this.label25 = new System.Windows.Forms.Label();
            this.buttonX9 = new DevComponents.DotNetBar.ButtonX();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxX1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBoxX2 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX3 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.comboBoxEx5 = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.comboBoxEx6 = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.buttonX11 = new DevComponents.DotNetBar.ButtonX();
            this.comboBoxEx7 = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.buttonX12 = new DevComponents.DotNetBar.ButtonX();
            this.textBoxX4 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.shapeContainer6 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape8 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.panelEx1.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabItemMaster)).BeginInit();
            this.tabItemMaster.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            this.mnuAddImage.SuspendLayout();
            this.mnuBarcodeAddImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBarCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemPhoto)).BeginInit();
            this.tabControlPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBatchwiseCosting)).BeginInit();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemLocations)).BeginInit();
            this.panelEx2.SuspendLayout();
            this.tabControlPanel7.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFeatures)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDimensions)).BeginInit();
            this.tabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUnitsOfMeasurements)).BeginInit();
            this.tabControlPanel5.SuspendLayout();
            this.tabControlPanel13.SuspendLayout();
            this.gbSaleRateHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRateHistory)).BeginInit();
            this.panelGridBottom.SuspendLayout();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemMasterBindingNavigator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorItemMaster)).BeginInit();
            this.tabControlPanel4.SuspendLayout();
            this.tabControlPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clsInnerGridBar1)).BeginInit();
            this.panelEx3.SuspendLayout();
            this.tabControlPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clsInnerGridBar2)).BeginInit();
            this.tabControlPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clsInnerGridBar3)).BeginInit();
            this.tabControlPanel11.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clsDataGirdViewX1)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clsDataGirdViewX2)).BeginInit();
            this.tabControlPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandZoom
            // 
            this.CommandZoom.Name = "CommandZoom";
            // 
            // dockContainerItem1
            // 
            this.dockContainerItem1.Name = "dockContainerItem1";
            this.dockContainerItem1.Text = "dockContainerItem1";
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.dgvItems);
            this.PanelLeft.Controls.Add(this.expandablePanel1);
            this.PanelLeft.Controls.Add(this.lblCountStatus);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(275, 541);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 9;
            this.PanelLeft.Text = "panelEx1";
            // 
            // dgvItems
            // 
            this.dgvItems.AddNewRow = false;
            this.dgvItems.AllowUserToAddRows = false;
            this.dgvItems.AllowUserToDeleteRows = false;
            this.dgvItems.AllowUserToResizeRows = false;
            this.dgvItems.AlphaNumericCols = new int[0];
            this.dgvItems.BackgroundColor = System.Drawing.Color.White;
            this.dgvItems.CapsLockCols = new int[0];
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItems.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColItemID,
            this.dgvColRowNumber,
            this.dgvColItemCode,
            this.dgvColItemName});
            this.dgvItems.DecimalCols = new int[0];
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvItems.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvItems.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvItems.HasSlNo = false;
            this.dgvItems.LastRowIndex = 0;
            this.dgvItems.Location = new System.Drawing.Point(0, 170);
            this.dgvItems.Name = "dgvItems";
            this.dgvItems.NegativeValueCols = new int[0];
            this.dgvItems.NumericCols = new int[0];
            this.dgvItems.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItems.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvItems.RowHeadersVisible = false;
            this.dgvItems.RowHeadersWidth = 45;
            this.dgvItems.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvItems.Size = new System.Drawing.Size(275, 345);
            this.dgvItems.TabIndex = 1;
            this.dgvItems.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItems_CellDoubleClick);
            // 
            // dgvColItemID
            // 
            this.dgvColItemID.DataPropertyName = "ItemID";
            this.dgvColItemID.HeaderText = "ItemID";
            this.dgvColItemID.Name = "dgvColItemID";
            this.dgvColItemID.ReadOnly = true;
            this.dgvColItemID.Visible = false;
            // 
            // dgvColRowNumber
            // 
            this.dgvColRowNumber.DataPropertyName = "RowNumber";
            this.dgvColRowNumber.HeaderText = "RowNumber";
            this.dgvColRowNumber.Name = "dgvColRowNumber";
            this.dgvColRowNumber.ReadOnly = true;
            this.dgvColRowNumber.Visible = false;
            // 
            // dgvColItemCode
            // 
            this.dgvColItemCode.DataPropertyName = "Code";
            this.dgvColItemCode.HeaderText = "Code";
            this.dgvColItemCode.Name = "dgvColItemCode";
            this.dgvColItemCode.ReadOnly = true;
            // 
            // dgvColItemName
            // 
            this.dgvColItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvColItemName.DataPropertyName = "Name";
            this.dgvColItemName.HeaderText = "Name";
            this.dgvColItemName.Name = "dgvColItemName";
            this.dgvColItemName.ReadOnly = true;
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expandablePanel1.Controls.Add(this.panelLeftTop);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(275, 170);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 0;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Search";
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelLeftTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelLeftTop.Controls.Add(this.lnkLabelAdvanceSearch);
            this.panelLeftTop.Controls.Add(this.lblSItemCode);
            this.panelLeftTop.Controls.Add(this.txtSearchCode);
            this.panelLeftTop.Controls.Add(this.btnSearch);
            this.panelLeftTop.Controls.Add(this.txtSearchDescription);
            this.panelLeftTop.Controls.Add(this.cboSearchSubCategory);
            this.panelLeftTop.Controls.Add(this.cboSearchCategory);
            this.panelLeftTop.Controls.Add(this.LblSCategory);
            this.panelLeftTop.Controls.Add(this.LblSSubCategory);
            this.panelLeftTop.Controls.Add(this.LblSItemName);
            this.panelLeftTop.Controls.Add(this.label1);
            this.panelLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftTop.Location = new System.Drawing.Point(0, 26);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(275, 141);
            this.panelLeftTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelLeftTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelLeftTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelLeftTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelLeftTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelLeftTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelLeftTop.Style.GradientAngle = 90;
            this.panelLeftTop.TabIndex = 0;
            // 
            // lnkLabelAdvanceSearch
            // 
            this.lnkLabelAdvanceSearch.AutoSize = true;
            this.lnkLabelAdvanceSearch.Location = new System.Drawing.Point(3, 122);
            this.lnkLabelAdvanceSearch.Name = "lnkLabelAdvanceSearch";
            this.lnkLabelAdvanceSearch.Size = new System.Drawing.Size(87, 13);
            this.lnkLabelAdvanceSearch.TabIndex = 6;
            this.lnkLabelAdvanceSearch.TabStop = true;
            this.lnkLabelAdvanceSearch.Text = "Advance Search";
            this.lnkLabelAdvanceSearch.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLabelAdvanceSearch_LinkClicked);
            // 
            // lblSItemCode
            // 
            this.lblSItemCode.AutoSize = true;
            this.lblSItemCode.Location = new System.Drawing.Point(11, 65);
            this.lblSItemCode.Name = "lblSItemCode";
            this.lblSItemCode.Size = new System.Drawing.Size(55, 13);
            this.lblSItemCode.TabIndex = 110;
            this.lblSItemCode.Text = "Item Code";
            // 
            // txtSearchCode
            // 
            this.txtSearchCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            // 
            // 
            // 
            this.txtSearchCode.Border.Class = "TextBoxBorder";
            this.txtSearchCode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearchCode.Location = new System.Drawing.Point(85, 62);
            this.txtSearchCode.MaxLength = 20;
            this.txtSearchCode.Name = "txtSearchCode";
            this.txtSearchCode.Size = new System.Drawing.Size(184, 20);
            this.txtSearchCode.TabIndex = 2;
            // 
            // btnSearch
            // 
            this.btnSearch.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSearch.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSearch.Location = new System.Drawing.Point(198, 114);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(71, 23);
            this.btnSearch.TabIndex = 5;
            this.btnSearch.Text = "Refresh";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSearchDescription
            // 
            this.txtSearchDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtSearchDescription.Border.Class = "TextBoxBorder";
            this.txtSearchDescription.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearchDescription.Location = new System.Drawing.Point(85, 88);
            this.txtSearchDescription.MaxLength = 20;
            this.txtSearchDescription.Name = "txtSearchDescription";
            this.txtSearchDescription.Size = new System.Drawing.Size(184, 20);
            this.txtSearchDescription.TabIndex = 3;
            // 
            // cboSearchSubCategory
            // 
            this.cboSearchSubCategory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSearchSubCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSearchSubCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchSubCategory.DisplayMember = "Text";
            this.cboSearchSubCategory.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSearchSubCategory.DropDownHeight = 75;
            this.cboSearchSubCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboSearchSubCategory.FormattingEnabled = true;
            this.cboSearchSubCategory.IntegralHeight = false;
            this.cboSearchSubCategory.ItemHeight = 14;
            this.cboSearchSubCategory.Location = new System.Drawing.Point(85, 36);
            this.cboSearchSubCategory.Name = "cboSearchSubCategory";
            this.cboSearchSubCategory.Size = new System.Drawing.Size(184, 20);
            this.cboSearchSubCategory.TabIndex = 1;
            // 
            // cboSearchCategory
            // 
            this.cboSearchCategory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSearchCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSearchCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchCategory.DisplayMember = "Text";
            this.cboSearchCategory.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSearchCategory.DropDownHeight = 75;
            this.cboSearchCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboSearchCategory.FormattingEnabled = true;
            this.cboSearchCategory.IntegralHeight = false;
            this.cboSearchCategory.ItemHeight = 14;
            this.cboSearchCategory.Location = new System.Drawing.Point(85, 10);
            this.cboSearchCategory.Name = "cboSearchCategory";
            this.cboSearchCategory.Size = new System.Drawing.Size(184, 20);
            this.cboSearchCategory.TabIndex = 0;
            this.cboSearchCategory.SelectedIndexChanged += new System.EventHandler(this.cboSearchCategory_SelectedIndexChanged);
            // 
            // LblSCategory
            // 
            this.LblSCategory.AutoSize = true;
            this.LblSCategory.Location = new System.Drawing.Point(11, 13);
            this.LblSCategory.Name = "LblSCategory";
            this.LblSCategory.Size = new System.Drawing.Size(49, 13);
            this.LblSCategory.TabIndex = 109;
            this.LblSCategory.Text = "Category";
            // 
            // LblSSubCategory
            // 
            this.LblSSubCategory.AutoSize = true;
            this.LblSSubCategory.Location = new System.Drawing.Point(11, 39);
            this.LblSSubCategory.Name = "LblSSubCategory";
            this.LblSSubCategory.Size = new System.Drawing.Size(71, 13);
            this.LblSSubCategory.TabIndex = 108;
            this.LblSSubCategory.Text = "Sub Category";
            // 
            // LblSItemName
            // 
            this.LblSItemName.AutoSize = true;
            this.LblSItemName.Location = new System.Drawing.Point(11, 91);
            this.LblSItemName.Name = "LblSItemName";
            this.LblSItemName.Size = new System.Drawing.Size(58, 13);
            this.LblSItemName.TabIndex = 107;
            this.LblSItemName.Text = "Item Name";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 3;
            this.label1.Visible = false;
            // 
            // lblCountStatus
            // 
            // 
            // 
            // 
            this.lblCountStatus.BackgroundStyle.Class = "";
            this.lblCountStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCountStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblCountStatus.Location = new System.Drawing.Point(0, 515);
            this.lblCountStatus.Name = "lblCountStatus";
            this.lblCountStatus.Size = new System.Drawing.Size(275, 26);
            this.lblCountStatus.TabIndex = 0;
            this.lblCountStatus.Text = "...";
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(179)))), ((int)(((byte)(219)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(275, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 541);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 10;
            this.expandableSplitterLeft.TabStop = false;
            // 
            // dotNetBarManager1
            // 
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.dotNetBarManager1.BottomDockSite = this.dockSite4;
            this.dotNetBarManager1.EnableFullSizeDock = false;
            this.dotNetBarManager1.LeftDockSite = this.dockSite1;
            this.dotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.dotNetBarManager1.ParentForm = this;
            this.dotNetBarManager1.RightDockSite = this.dockSite2;
            this.dotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.dotNetBarManager1.ToolbarBottomDockSite = this.dockSite8;
            this.dotNetBarManager1.ToolbarLeftDockSite = this.dockSite5;
            this.dotNetBarManager1.ToolbarRightDockSite = this.dockSite6;
            this.dotNetBarManager1.TopDockSite = this.dockSite3;
            // 
            // dockSite4
            // 
            this.dockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite4.Location = new System.Drawing.Point(0, 541);
            this.dockSite4.Name = "dockSite4";
            this.dockSite4.Size = new System.Drawing.Size(1184, 0);
            this.dockSite4.TabIndex = 18;
            this.dockSite4.TabStop = false;
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(278, 0);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 541);
            this.dockSite1.TabIndex = 15;
            this.dockSite1.TabStop = false;
            // 
            // dockSite2
            // 
            this.dockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite2.Location = new System.Drawing.Point(1184, 0);
            this.dockSite2.Name = "dockSite2";
            this.dockSite2.Size = new System.Drawing.Size(0, 541);
            this.dockSite2.TabIndex = 16;
            this.dockSite2.TabStop = false;
            // 
            // dockSite8
            // 
            this.dockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite8.Location = new System.Drawing.Point(0, 541);
            this.dockSite8.Name = "dockSite8";
            this.dockSite8.Size = new System.Drawing.Size(1184, 0);
            this.dockSite8.TabIndex = 22;
            this.dockSite8.TabStop = false;
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(0, 0);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 541);
            this.dockSite5.TabIndex = 19;
            this.dockSite5.TabStop = false;
            // 
            // dockSite6
            // 
            this.dockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite6.Location = new System.Drawing.Point(1184, 0);
            this.dockSite6.Name = "dockSite6";
            this.dockSite6.Size = new System.Drawing.Size(0, 541);
            this.dockSite6.TabIndex = 20;
            this.dockSite6.TabStop = false;
            // 
            // dockSite3
            // 
            this.dockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite3.Location = new System.Drawing.Point(0, 0);
            this.dockSite3.Name = "dockSite3";
            this.dockSite3.Size = new System.Drawing.Size(1184, 0);
            this.dockSite3.TabIndex = 17;
            this.dockSite3.TabStop = false;
            // 
            // bar1
            // 
            this.bar1.AccessibleDescription = "DotNetBar Bar (bar1)";
            this.bar1.AccessibleName = "DotNetBar Bar";
            this.bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.bar1.Location = new System.Drawing.Point(0, 0);
            this.bar1.MenuBar = true;
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(36, 24);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.bar1.TabIndex = 0;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx1.Controls.Add(this.pnlBottom);
            this.panelEx1.Controls.Add(this.expandableSplitterTop);
            this.panelEx1.Controls.Add(this.panelTop);
            this.panelEx1.Controls.Add(this.ItemMasterBindingNavigator);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(278, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(906, 541);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 0;
            this.panelEx1.Text = "panelEx1";
            // 
            // pnlBottom
            // 
            this.pnlBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.pnlBottom.Controls.Add(this.tabItemMaster);
            this.pnlBottom.Controls.Add(this.panelGridBottom);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBottom.Location = new System.Drawing.Point(0, 188);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(906, 353);
            this.pnlBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottom.Style.GradientAngle = 90;
            this.pnlBottom.TabIndex = 18;
            this.pnlBottom.Text = "panelEx3";
            // 
            // tabItemMaster
            // 
            this.tabItemMaster.BackColor = System.Drawing.Color.Transparent;
            this.tabItemMaster.CanReorderTabs = true;
            this.tabItemMaster.Controls.Add(this.tabControlPanel3);
            this.tabItemMaster.Controls.Add(this.tabControlPanel1);
            this.tabItemMaster.Controls.Add(this.tabControlPanel6);
            this.tabItemMaster.Controls.Add(this.tabControlPanel2);
            this.tabItemMaster.Controls.Add(this.tabControlPanel7);
            this.tabItemMaster.Controls.Add(this.tabControlPanel5);
            this.tabItemMaster.Controls.Add(this.tabControlPanel13);
            this.tabItemMaster.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabItemMaster.Location = new System.Drawing.Point(0, 0);
            this.tabItemMaster.Name = "tabItemMaster";
            this.tabItemMaster.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabItemMaster.SelectedTabIndex = 2;
            this.tabItemMaster.Size = new System.Drawing.Size(906, 327);
            this.tabItemMaster.TabIndex = 1;
            this.tabItemMaster.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tabItemMaster.Tabs.Add(this.tbpGeneral);
            this.tabItemMaster.Tabs.Add(this.tabDimensionAndFeatures);
            this.tabItemMaster.Tabs.Add(this.tbpMeasurements);
            this.tabItemMaster.Tabs.Add(this.tbpCosting);
            this.tabItemMaster.Tabs.Add(this.tbpLocation);
            this.tabItemMaster.Tabs.Add(this.tbpBatch);
            this.tabItemMaster.Tabs.Add(this.tpbSaleRate);
            this.tabItemMaster.Text = "Components";
            this.tabItemMaster.ThemeAware = true;
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.tabControlPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel1.Controls.Add(this.lblImageText);
            this.tabControlPanel1.Controls.Add(this.btnAddBarcodeImage);
            this.tabControlPanel1.Controls.Add(this.btnNext);
            this.tabControlPanel1.Controls.Add(this.btnPrev);
            this.tabControlPanel1.Controls.Add(this.BtnAttach);
            this.tabControlPanel1.Controls.Add(this.btnAddImage);
            this.tabControlPanel1.Controls.Add(this.btnBarcodeCreate);
            this.tabControlPanel1.Controls.Add(this.label4);
            this.tabControlPanel1.Controls.Add(this.btnCreateBarCode);
            this.tabControlPanel1.Controls.Add(this.pbBarCode);
            this.tabControlPanel1.Controls.Add(this.txtNote);
            this.tabControlPanel1.Controls.Add(this.label12);
            this.tabControlPanel1.Controls.Add(this.LblNote);
            this.tabControlPanel1.Controls.Add(this.pbItemPhoto);
            this.tabControlPanel1.Controls.Add(this.LblGuaranteePeriod);
            this.tabControlPanel1.Controls.Add(this.txtWarrantyPeriod);
            this.tabControlPanel1.Controls.Add(this.txtGuaranteePeriod);
            this.tabControlPanel1.Controls.Add(this.cboTaxCode);
            this.tabControlPanel1.Controls.Add(this.btnTaxCode);
            this.tabControlPanel1.Controls.Add(this.cboMadeIn);
            this.tabControlPanel1.Controls.Add(this.btnMadeIn);
            this.tabControlPanel1.Controls.Add(this.cboManufacturer);
            this.tabControlPanel1.Controls.Add(this.btnManufacturer);
            this.tabControlPanel1.Controls.Add(this.txtModel);
            this.tabControlPanel1.Controls.Add(this.label11);
            this.tabControlPanel1.Controls.Add(this.LblWarrantyPeriod);
            this.tabControlPanel1.Controls.Add(this.LblMadeIn);
            this.tabControlPanel1.Controls.Add(this.LblModel);
            this.tabControlPanel1.Controls.Add(this.LblManufacturer);
            this.tabControlPanel1.Controls.Add(this.LblTaxCode);
            this.tabControlPanel1.Controls.Add(this.LblBarcode);
            this.tabControlPanel1.Controls.Add(this.shapeContainer1);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(906, 300);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 0;
            this.tabControlPanel1.TabItem = this.tbpGeneral;
            this.tabControlPanel1.ThemeAware = true;
            this.tabControlPanel1.Visible = false;
            this.tabControlPanel1.Click += new System.EventHandler(this.tabControlPanel1_Click);
            // 
            // lblImageText
            // 
            this.lblImageText.AutoSize = true;
            this.lblImageText.BackColor = System.Drawing.Color.Transparent;
            this.lblImageText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImageText.Location = new System.Drawing.Point(699, 148);
            this.lblImageText.Name = "lblImageText";
            this.lblImageText.Size = new System.Drawing.Size(34, 13);
            this.lblImageText.TabIndex = 240;
            this.lblImageText.Text = "0 of 0";
            this.lblImageText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNext
            // 
            this.btnNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnNext.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnNext.Location = new System.Drawing.Point(758, 145);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(33, 20);
            this.btnNext.TabIndex = 239;
            this.btnNext.TabStop = false;
            this.btnNext.Text = "Next";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPrev.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnPrev.Location = new System.Drawing.Point(647, 145);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(33, 20);
            this.btnPrev.TabIndex = 238;
            this.btnPrev.TabStop = false;
            this.btnPrev.Text = "Prev";
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // BtnAttach
            // 
            this.BtnAttach.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnAttach.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnAttach.Location = new System.Drawing.Point(486, 72);
            this.BtnAttach.Name = "BtnAttach";
            this.BtnAttach.Size = new System.Drawing.Size(60, 23);
            this.BtnAttach.TabIndex = 13;
            this.BtnAttach.TabStop = false;
            this.BtnAttach.Text = "Attach";
            this.BtnAttach.Click += new System.EventHandler(this.BtnAttach_Click);
            // 
            // btnAddImage
            // 
            this.btnAddImage.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddImage.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddImage.ContextMenuStrip = this.mnuAddImage;
            this.btnAddImage.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnAddImage.Location = new System.Drawing.Point(547, 72);
            this.btnAddImage.Name = "btnAddImage";
            this.btnAddImage.Size = new System.Drawing.Size(20, 23);
            this.btnAddImage.TabIndex = 14;
            this.btnAddImage.Text = "6";
            this.btnAddImage.Click += new System.EventHandler(this.btnAddImage_Click);
            // 
            // mnuAddImage
            // 
            this.mnuAddImage.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAdd,
            this.mnuRemove});
            this.mnuAddImage.Name = "mnuAddImage";
            this.mnuAddImage.Size = new System.Drawing.Size(141, 48);
            // 
            // mnuAdd
            // 
            this.mnuAdd.Name = "mnuAdd";
            this.mnuAdd.Size = new System.Drawing.Size(140, 22);
            this.mnuAdd.Text = "Attach More";
            this.mnuAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // mnuRemove
            // 
            this.mnuRemove.Name = "mnuRemove";
            this.mnuRemove.Size = new System.Drawing.Size(140, 22);
            this.mnuRemove.Text = "Remove";
            this.mnuRemove.Click += new System.EventHandler(this.mnuRemove_Click);
            // 
            // btnBarcodeCreate
            // 
            this.btnBarcodeCreate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnBarcodeCreate.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnBarcodeCreate.Location = new System.Drawing.Point(486, 205);
            this.btnBarcodeCreate.Name = "btnBarcodeCreate";
            this.btnBarcodeCreate.Size = new System.Drawing.Size(60, 23);
            this.btnBarcodeCreate.TabIndex = 15;
            this.btnBarcodeCreate.TabStop = false;
            this.btnBarcodeCreate.Text = "Create";
            this.btnBarcodeCreate.Click += new System.EventHandler(this.btnBarcodeCreate_Click);
            // 
            // btnAddBarcodeImage
            // 
            this.btnAddBarcodeImage.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddBarcodeImage.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddBarcodeImage.ContextMenuStrip = this.mnuBarcodeAddImage;
            this.btnAddBarcodeImage.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnAddBarcodeImage.Location = new System.Drawing.Point(547, 205);
            this.btnAddBarcodeImage.Name = "btnAddBarcodeImage";
            this.btnAddBarcodeImage.Size = new System.Drawing.Size(20, 23);
            this.btnAddBarcodeImage.TabIndex = 16;
            this.btnAddBarcodeImage.Text = "6";
            this.btnAddBarcodeImage.Click += new System.EventHandler(this.btnAddBarcodeImage_Click);
            // 
            // mnuBarcodeAddImage
            // 
            this.mnuBarcodeAddImage.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuPrintBarcode,
            this.mnuCreateBarcode,
            this.mnuRemoveBarcode});
            this.mnuBarcodeAddImage.Name = "mnuAddImage";
            this.mnuBarcodeAddImage.Size = new System.Drawing.Size(164, 70);
            // 
            // mnuPrintBarcode
            // 
            this.mnuPrintBarcode.Name = "mnuPrintBarcode";
            this.mnuPrintBarcode.Size = new System.Drawing.Size(163, 22);
            this.mnuPrintBarcode.Text = "Print Barcode";
            this.mnuPrintBarcode.Visible = false;
            this.mnuPrintBarcode.Click += new System.EventHandler(this.mnuPrintBarcode_Click);
            // 
            // mnuCreateBarcode
            // 
            this.mnuCreateBarcode.Name = "mnuCreateBarcode";
            this.mnuCreateBarcode.Size = new System.Drawing.Size(163, 22);
            this.mnuCreateBarcode.Text = "Create Barcode";
            this.mnuCreateBarcode.Click += new System.EventHandler(this.mnuCreateBarcode_Click);
            // 
            // mnuRemoveBarcode
            // 
            this.mnuRemoveBarcode.Name = "mnuRemoveBarcode";
            this.mnuRemoveBarcode.Size = new System.Drawing.Size(163, 22);
            this.mnuRemoveBarcode.Text = "Remove Barcode";
            this.mnuRemoveBarcode.Click += new System.EventHandler(this.mnuRemoveBarcode_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(483, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 237;
            this.label4.Text = "Item Image";
            // 
            // btnCreateBarCode
            // 
            this.btnCreateBarCode.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCreateBarCode.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCreateBarCode.Location = new System.Drawing.Point(849, 167);
            this.btnCreateBarCode.Name = "btnCreateBarCode";
            this.btnCreateBarCode.Size = new System.Drawing.Size(41, 22);
            this.btnCreateBarCode.TabIndex = 28;
            this.btnCreateBarCode.TabStop = false;
            this.btnCreateBarCode.Text = "Create";
            this.btnCreateBarCode.Visible = false;
            this.btnCreateBarCode.Click += new System.EventHandler(this.btnCreateBarCode_Click);
            // 
            // pbBarCode
            // 
            this.pbBarCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbBarCode.Location = new System.Drawing.Point(579, 167);
            this.pbBarCode.Name = "pbBarCode";
            this.pbBarCode.Size = new System.Drawing.Size(267, 113);
            this.pbBarCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbBarCode.TabIndex = 229;
            this.pbBarCode.TabStop = false;
            // 
            // txtNote
            // 
            // 
            // 
            // 
            this.txtNote.Border.Class = "TextBoxBorder";
            this.txtNote.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtNote.Location = new System.Drawing.Point(119, 127);
            this.txtNote.MaxLength = 1000;
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNote.Size = new System.Drawing.Size(288, 153);
            this.txtNote.TabIndex = 12;
            this.txtNote.Text = "\r\n\r\n\r\n";
            this.txtNote.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(537, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 13);
            this.label12.TabIndex = 227;
            this.label12.Text = "Photo";
            // 
            // LblNote
            // 
            this.LblNote.AutoSize = true;
            this.LblNote.BackColor = System.Drawing.Color.Transparent;
            this.LblNote.Location = new System.Drawing.Point(24, 184);
            this.LblNote.Name = "LblNote";
            this.LblNote.Size = new System.Drawing.Size(30, 13);
            this.LblNote.TabIndex = 226;
            this.LblNote.Text = "Note";
            // 
            // pbItemPhoto
            // 
            this.pbItemPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbItemPhoto.Location = new System.Drawing.Point(579, 23);
            this.pbItemPhoto.Name = "pbItemPhoto";
            this.pbItemPhoto.Size = new System.Drawing.Size(267, 120);
            this.pbItemPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbItemPhoto.TabIndex = 225;
            this.pbItemPhoto.TabStop = false;
            this.pbItemPhoto.MouseLeave += new System.EventHandler(this.pbItemPhoto_MouseLeave);
            this.pbItemPhoto.Click += new System.EventHandler(this.pbItemPhoto_Click);
            this.pbItemPhoto.MouseHover += new System.EventHandler(this.pbItemPhoto_MouseHover);
            this.pbItemPhoto.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbItemPhoto_MouseUp);
            // 
            // LblGuaranteePeriod
            // 
            this.LblGuaranteePeriod.AutoSize = true;
            this.LblGuaranteePeriod.BackColor = System.Drawing.Color.Transparent;
            this.LblGuaranteePeriod.Location = new System.Drawing.Point(203, 105);
            this.LblGuaranteePeriod.Name = "LblGuaranteePeriod";
            this.LblGuaranteePeriod.Size = new System.Drawing.Size(90, 13);
            this.LblGuaranteePeriod.TabIndex = 223;
            this.LblGuaranteePeriod.Text = "Guarantee Period";
            // 
            // txtGuaranteePeriod
            // 
            // 
            // 
            // 
            this.txtGuaranteePeriod.Border.Class = "TextBoxBorder";
            this.txtGuaranteePeriod.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGuaranteePeriod.Location = new System.Drawing.Point(296, 101);
            this.txtGuaranteePeriod.MaxLength = 5;
            this.txtGuaranteePeriod.Name = "txtGuaranteePeriod";
            this.txtGuaranteePeriod.Size = new System.Drawing.Size(65, 20);
            this.txtGuaranteePeriod.TabIndex = 9;
            this.txtGuaranteePeriod.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // txtWarrantyPeriod
            // 
            // 
            // 
            // 
            this.txtWarrantyPeriod.Border.Class = "TextBoxBorder";
            this.txtWarrantyPeriod.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtWarrantyPeriod.Location = new System.Drawing.Point(119, 101);
            this.txtWarrantyPeriod.MaxLength = 5;
            this.txtWarrantyPeriod.Name = "txtWarrantyPeriod";
            this.txtWarrantyPeriod.Size = new System.Drawing.Size(65, 20);
            this.txtWarrantyPeriod.TabIndex = 8;
            this.txtWarrantyPeriod.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // cboTaxCode
            // 
            this.cboTaxCode.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboTaxCode.DropDownHeight = 75;
            this.cboTaxCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboTaxCode.FormattingEnabled = true;
            this.cboTaxCode.IntegralHeight = false;
            this.cboTaxCode.ItemHeight = 14;
            this.cboTaxCode.Location = new System.Drawing.Point(498, 260);
            this.cboTaxCode.Name = "cboTaxCode";
            this.cboTaxCode.Size = new System.Drawing.Size(38, 20);
            this.cboTaxCode.TabIndex = 17;
            this.cboTaxCode.Visible = false;
            this.cboTaxCode.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // btnTaxCode
            // 
            this.btnTaxCode.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnTaxCode.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnTaxCode.Location = new System.Drawing.Point(538, 260);
            this.btnTaxCode.Name = "btnTaxCode";
            this.btnTaxCode.Size = new System.Drawing.Size(29, 20);
            this.btnTaxCode.TabIndex = 18;
            this.btnTaxCode.Text = "...";
            this.btnTaxCode.Visible = false;
            this.btnTaxCode.Click += new System.EventHandler(this.btnTaxCode_Click);
            // 
            // cboMadeIn
            // 
            this.cboMadeIn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboMadeIn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboMadeIn.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboMadeIn.DropDownHeight = 75;
            this.cboMadeIn.DropDownWidth = 198;
            this.cboMadeIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboMadeIn.FormattingEnabled = true;
            this.cboMadeIn.IntegralHeight = false;
            this.cboMadeIn.ItemHeight = 14;
            this.cboMadeIn.Location = new System.Drawing.Point(119, 75);
            this.cboMadeIn.Name = "cboMadeIn";
            this.cboMadeIn.Size = new System.Drawing.Size(242, 20);
            this.cboMadeIn.TabIndex = 6;
            this.cboMadeIn.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.cboMadeIn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_KeyDown);
            // 
            // btnMadeIn
            // 
            this.btnMadeIn.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnMadeIn.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnMadeIn.Location = new System.Drawing.Point(367, 75);
            this.btnMadeIn.Name = "btnMadeIn";
            this.btnMadeIn.Size = new System.Drawing.Size(40, 20);
            this.btnMadeIn.TabIndex = 7;
            this.btnMadeIn.Text = "...";
            this.btnMadeIn.Click += new System.EventHandler(this.btnMadeIn_Click);
            // 
            // cboManufacturer
            // 
            this.cboManufacturer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboManufacturer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboManufacturer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboManufacturer.DropDownHeight = 75;
            this.cboManufacturer.DropDownWidth = 198;
            this.cboManufacturer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboManufacturer.FormattingEnabled = true;
            this.cboManufacturer.IntegralHeight = false;
            this.cboManufacturer.ItemHeight = 14;
            this.cboManufacturer.Location = new System.Drawing.Point(119, 49);
            this.cboManufacturer.Name = "cboManufacturer";
            this.cboManufacturer.Size = new System.Drawing.Size(242, 20);
            this.cboManufacturer.TabIndex = 4;
            this.cboManufacturer.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.cboManufacturer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_KeyDown);
            // 
            // btnManufacturer
            // 
            this.btnManufacturer.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnManufacturer.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnManufacturer.Location = new System.Drawing.Point(367, 50);
            this.btnManufacturer.Name = "btnManufacturer";
            this.btnManufacturer.Size = new System.Drawing.Size(40, 20);
            this.btnManufacturer.TabIndex = 5;
            this.btnManufacturer.Text = "...";
            this.btnManufacturer.Click += new System.EventHandler(this.btnManufacturer_Click);
            // 
            // txtModel
            // 
            // 
            // 
            // 
            this.txtModel.Border.Class = "TextBoxBorder";
            this.txtModel.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtModel.Location = new System.Drawing.Point(119, 23);
            this.txtModel.MaxLength = 20;
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(242, 20);
            this.txtModel.TabIndex = 3;
            this.txtModel.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(11, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 13);
            this.label11.TabIndex = 94;
            this.label11.Text = "General Info";
            // 
            // LblWarrantyPeriod
            // 
            this.LblWarrantyPeriod.AutoSize = true;
            this.LblWarrantyPeriod.BackColor = System.Drawing.Color.Transparent;
            this.LblWarrantyPeriod.Location = new System.Drawing.Point(30, 103);
            this.LblWarrantyPeriod.Name = "LblWarrantyPeriod";
            this.LblWarrantyPeriod.Size = new System.Drawing.Size(83, 13);
            this.LblWarrantyPeriod.TabIndex = 88;
            this.LblWarrantyPeriod.Text = "Warranty Period";
            // 
            // LblMadeIn
            // 
            this.LblMadeIn.AutoSize = true;
            this.LblMadeIn.BackColor = System.Drawing.Color.Transparent;
            this.LblMadeIn.Location = new System.Drawing.Point(28, 79);
            this.LblMadeIn.Name = "LblMadeIn";
            this.LblMadeIn.Size = new System.Drawing.Size(46, 13);
            this.LblMadeIn.TabIndex = 91;
            this.LblMadeIn.Text = "Made-In";
            // 
            // LblModel
            // 
            this.LblModel.AutoSize = true;
            this.LblModel.BackColor = System.Drawing.Color.Transparent;
            this.LblModel.Location = new System.Drawing.Point(28, 27);
            this.LblModel.Name = "LblModel";
            this.LblModel.Size = new System.Drawing.Size(36, 13);
            this.LblModel.TabIndex = 90;
            this.LblModel.Text = "Model";
            // 
            // LblManufacturer
            // 
            this.LblManufacturer.AutoSize = true;
            this.LblManufacturer.BackColor = System.Drawing.Color.Transparent;
            this.LblManufacturer.Location = new System.Drawing.Point(28, 53);
            this.LblManufacturer.Name = "LblManufacturer";
            this.LblManufacturer.Size = new System.Drawing.Size(70, 13);
            this.LblManufacturer.TabIndex = 89;
            this.LblManufacturer.Text = "Manufacturer";
            // 
            // LblTaxCode
            // 
            this.LblTaxCode.AutoSize = true;
            this.LblTaxCode.BackColor = System.Drawing.Color.Transparent;
            this.LblTaxCode.Location = new System.Drawing.Point(448, 267);
            this.LblTaxCode.Name = "LblTaxCode";
            this.LblTaxCode.Size = new System.Drawing.Size(53, 13);
            this.LblTaxCode.TabIndex = 86;
            this.LblTaxCode.Text = "Tax Code";
            this.LblTaxCode.Visible = false;
            // 
            // LblBarcode
            // 
            this.LblBarcode.AutoSize = true;
            this.LblBarcode.BackColor = System.Drawing.Color.Transparent;
            this.LblBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBarcode.Location = new System.Drawing.Point(483, 184);
            this.LblBarcode.Name = "LblBarcode";
            this.LblBarcode.Size = new System.Drawing.Size(79, 13);
            this.LblBarcode.TabIndex = 87;
            this.LblBarcode.Text = "Barcode Image";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(1, 1);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(904, 298);
            this.shapeContainer1.TabIndex = 15;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 73;
            this.lineShape1.X2 = 898;
            this.lineShape1.Y1 = 12;
            this.lineShape1.Y2 = 12;
            // 
            // tbpGeneral
            // 
            this.tbpGeneral.AttachedControl = this.tabControlPanel1;
            this.tbpGeneral.Name = "tbpGeneral";
            this.tbpGeneral.Text = "General";
            // 
            // tabControlPanel6
            // 
            this.tabControlPanel6.Controls.Add(this.lblPurchaseRate);
            this.tabControlPanel6.Controls.Add(this.rdbLandingCost);
            this.tabControlPanel6.Controls.Add(this.txtActualRate);
            this.tabControlPanel6.Controls.Add(this.label13);
            this.tabControlPanel6.Controls.Add(this.rdbActualRate);
            this.tabControlPanel6.Controls.Add(this.rdbPurchaseRate);
            this.tabControlPanel6.Controls.Add(this.dgvBatchwiseCosting);
            this.tabControlPanel6.Controls.Add(this.cboPricingScheme);
            this.tabControlPanel6.Controls.Add(this.BtnShow);
            this.tabControlPanel6.Controls.Add(this.lblPricingScheme);
            this.tabControlPanel6.Controls.Add(this.lblSaleRate);
            this.tabControlPanel6.Controls.Add(this.cboCostingMethod);
            this.tabControlPanel6.Controls.Add(this.LblCostingMethod);
            this.tabControlPanel6.Controls.Add(this.label9);
            this.tabControlPanel6.Controls.Add(this.shapeContainer3);
            this.tabControlPanel6.Controls.Add(this.txtPurchaseRate);
            this.tabControlPanel6.Controls.Add(this.txtSaleRate);
            this.tabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel6.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel6.Name = "tabControlPanel6";
            this.tabControlPanel6.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel6.Size = new System.Drawing.Size(906, 300);
            this.tabControlPanel6.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.tabControlPanel6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel6.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel6.Style.GradientAngle = 90;
            this.tabControlPanel6.TabIndex = 6;
            this.tabControlPanel6.TabItem = this.tbpCosting;
            this.tabControlPanel6.ThemeAware = true;
            // 
            // lblPurchaseRate
            // 
            this.lblPurchaseRate.AutoSize = true;
            this.lblPurchaseRate.BackColor = System.Drawing.Color.Transparent;
            this.lblPurchaseRate.Location = new System.Drawing.Point(659, 57);
            this.lblPurchaseRate.Name = "lblPurchaseRate";
            this.lblPurchaseRate.Size = new System.Drawing.Size(78, 13);
            this.lblPurchaseRate.TabIndex = 249;
            this.lblPurchaseRate.Text = "Purchase Rate";
            // 
            // rdbLandingCost
            // 
            this.rdbLandingCost.AutoSize = true;
            this.rdbLandingCost.BackColor = System.Drawing.Color.White;
            this.rdbLandingCost.Location = new System.Drawing.Point(702, 27);
            this.rdbLandingCost.Name = "rdbLandingCost";
            this.rdbLandingCost.Size = new System.Drawing.Size(87, 17);
            this.rdbLandingCost.TabIndex = 248;
            this.rdbLandingCost.TabStop = true;
            this.rdbLandingCost.Text = "Landing Cost";
            this.rdbLandingCost.UseVisualStyleBackColor = false;
            this.rdbLandingCost.CheckedChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // txtActualRate
            // 
            // 
            // 
            // 
            this.txtActualRate.Border.Class = "TextBoxBorder";
            this.txtActualRate.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtActualRate.Location = new System.Drawing.Point(794, 53);
            this.txtActualRate.MaxLength = 12;
            this.txtActualRate.Name = "txtActualRate";
            this.txtActualRate.ShortcutsEnabled = false;
            this.txtActualRate.Size = new System.Drawing.Size(100, 20);
            this.txtActualRate.TabIndex = 8;
            this.txtActualRate.Text = "0";
            this.txtActualRate.Visible = false;
            this.txtActualRate.TextChanged += new System.EventHandler(this.txtActualRate_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(487, 9);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(116, 13);
            this.label13.TabIndex = 247;
            this.label13.Text = "Sale Rate Based On";
            // 
            // rdbActualRate
            // 
            this.rdbActualRate.AutoSize = true;
            this.rdbActualRate.BackColor = System.Drawing.Color.Transparent;
            this.rdbActualRate.Location = new System.Drawing.Point(603, 29);
            this.rdbActualRate.Name = "rdbActualRate";
            this.rdbActualRate.Size = new System.Drawing.Size(81, 17);
            this.rdbActualRate.TabIndex = 5;
            this.rdbActualRate.Tag = "";
            this.rdbActualRate.Text = "Actual Rate";
            this.rdbActualRate.UseVisualStyleBackColor = false;
            this.rdbActualRate.CheckedChanged += new System.EventHandler(this.rdbActualRate_CheckedChanged);
            // 
            // rdbPurchaseRate
            // 
            this.rdbPurchaseRate.AutoSize = true;
            this.rdbPurchaseRate.BackColor = System.Drawing.Color.Transparent;
            this.rdbPurchaseRate.Location = new System.Drawing.Point(488, 29);
            this.rdbPurchaseRate.Name = "rdbPurchaseRate";
            this.rdbPurchaseRate.Size = new System.Drawing.Size(96, 17);
            this.rdbPurchaseRate.TabIndex = 4;
            this.rdbPurchaseRate.Tag = "";
            this.rdbPurchaseRate.Text = "Purchase Rate";
            this.rdbPurchaseRate.UseVisualStyleBackColor = false;
            this.rdbPurchaseRate.CheckedChanged += new System.EventHandler(this.rdbPurchaseRate_CheckedChanged);
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(367, 53);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(40, 20);
            this.BtnShow.TabIndex = 3;
            this.BtnShow.Text = "Show";
            this.BtnShow.Tooltip = "Show";
            this.BtnShow.Visible = false;
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // dgvBatchwiseCosting
            // 
            this.dgvBatchwiseCosting.AddNewRow = true;
            this.dgvBatchwiseCosting.AllowUserToAddRows = false;
            this.dgvBatchwiseCosting.AllowUserToDeleteRows = false;
            this.dgvBatchwiseCosting.AllowUserToResizeColumns = false;
            this.dgvBatchwiseCosting.AllowUserToResizeRows = false;
            this.dgvBatchwiseCosting.AlphaNumericCols = new int[0];
            this.dgvBatchwiseCosting.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvBatchwiseCosting.BackgroundColor = System.Drawing.Color.White;
            this.dgvBatchwiseCosting.CapsLockCols = new int[0];
            this.dgvBatchwiseCosting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvBatchwiseCosting.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColBatchID,
            this.ColBatchNo,
            this.ColExpiryDate,
            this.ColPurchaseRate,
            this.ColLandingCost,
            this.ColActualRate,
            this.ColSaleRate});
            this.dgvBatchwiseCosting.DecimalCols = new int[] {
        1,
        2};
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBatchwiseCosting.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvBatchwiseCosting.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvBatchwiseCosting.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvBatchwiseCosting.HasSlNo = false;
            this.dgvBatchwiseCosting.LastRowIndex = 0;
            this.dgvBatchwiseCosting.Location = new System.Drawing.Point(1, 79);
            this.dgvBatchwiseCosting.Name = "dgvBatchwiseCosting";
            this.dgvBatchwiseCosting.NegativeValueCols = new int[0];
            this.dgvBatchwiseCosting.NumericCols = new int[0];
            this.dgvBatchwiseCosting.RowHeadersWidth = 50;
            this.dgvBatchwiseCosting.Size = new System.Drawing.Size(911, 218);
            this.dgvBatchwiseCosting.TabIndex = 9;
            this.dgvBatchwiseCosting.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.RowAddedEventIncludingLastRow);
            this.dgvBatchwiseCosting.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBatchwiseCosting_CellEndEdit);
            this.dgvBatchwiseCosting.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.RowRemovedEventIncludingLastRow);
            // 
            // ColBatchID
            // 
            this.ColBatchID.DataPropertyName = "ColBatchID";
            this.ColBatchID.HeaderText = "BatchID";
            this.ColBatchID.Name = "ColBatchID";
            this.ColBatchID.ReadOnly = true;
            this.ColBatchID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColBatchID.Visible = false;
            // 
            // ColBatchNo
            // 
            this.ColBatchNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColBatchNo.DataPropertyName = "ColBatchNo";
            this.ColBatchNo.HeaderText = "Batch No";
            this.ColBatchNo.Name = "ColBatchNo";
            this.ColBatchNo.ReadOnly = true;
            this.ColBatchNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColBatchNo.Visible = false;
            // 
            // ColExpiryDate
            // 
            this.ColExpiryDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColExpiryDate.DataPropertyName = "ColExpiryDate";
            this.ColExpiryDate.HeaderText = "Expiry Date";
            this.ColExpiryDate.Name = "ColExpiryDate";
            this.ColExpiryDate.ReadOnly = true;
            this.ColExpiryDate.Visible = false;
            // 
            // ColPurchaseRate
            // 
            this.ColPurchaseRate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            // 
            // 
            // 
            this.ColPurchaseRate.BackgroundStyle.Class = "DataGridViewNumericBorder";
            this.ColPurchaseRate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ColPurchaseRate.DataPropertyName = "ColPurchaseRate";
            this.ColPurchaseRate.HeaderText = "Purchase Rate";
            this.ColPurchaseRate.Increment = 1;
            this.ColPurchaseRate.Name = "ColPurchaseRate";
            this.ColPurchaseRate.ReadOnly = true;
            this.ColPurchaseRate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ColLandingCost
            // 
            this.ColLandingCost.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            // 
            // 
            // 
            this.ColLandingCost.BackgroundStyle.Class = "DataGridViewNumericBorder";
            this.ColLandingCost.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ColLandingCost.DataPropertyName = "ColLandingCost";
            this.ColLandingCost.HeaderText = "Landing Cost";
            this.ColLandingCost.Increment = 1;
            this.ColLandingCost.Name = "ColLandingCost";
            this.ColLandingCost.ReadOnly = true;
            this.ColLandingCost.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ColActualRate
            // 
            this.ColActualRate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            // 
            // 
            // 
            this.ColActualRate.BackgroundStyle.Class = "DataGridViewNumericBorder";
            this.ColActualRate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ColActualRate.DataPropertyName = "ColActualRate";
            this.ColActualRate.HeaderText = "Actual Rate";
            this.ColActualRate.Increment = 1;
            this.ColActualRate.Name = "ColActualRate";
            this.ColActualRate.ReadOnly = true;
            // 
            // ColSaleRate
            // 
            this.ColSaleRate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            // 
            // 
            // 
            this.ColSaleRate.BackgroundStyle.Class = "DataGridViewNumericBorder";
            this.ColSaleRate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ColSaleRate.DataPropertyName = "ColSaleRate";
            this.ColSaleRate.HeaderText = "Sale Rate";
            this.ColSaleRate.Increment = 1;
            this.ColSaleRate.Name = "ColSaleRate";
            this.ColSaleRate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // cboPricingScheme
            // 
            this.cboPricingScheme.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboPricingScheme.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboPricingScheme.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cboPricingScheme.DropDownHeight = 75;
            this.cboPricingScheme.DropDownWidth = 198;
            this.cboPricingScheme.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboPricingScheme.FormattingEnabled = true;
            this.cboPricingScheme.IntegralHeight = false;
            this.cboPricingScheme.ItemHeight = 14;
            this.cboPricingScheme.Location = new System.Drawing.Point(119, 53);
            this.cboPricingScheme.Name = "cboPricingScheme";
            this.cboPricingScheme.Size = new System.Drawing.Size(242, 20);
            this.cboPricingScheme.TabIndex = 2;
            this.cboPricingScheme.SelectedIndexChanged += new System.EventHandler(this.cboPricingScheme_SelectedIndexChanged);
            this.cboPricingScheme.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboPricingScheme_KeyPress);
            // 
            // lblPricingScheme
            // 
            this.lblPricingScheme.AutoSize = true;
            this.lblPricingScheme.BackColor = System.Drawing.Color.Transparent;
            this.lblPricingScheme.Location = new System.Drawing.Point(24, 55);
            this.lblPricingScheme.Name = "lblPricingScheme";
            this.lblPricingScheme.Size = new System.Drawing.Size(81, 13);
            this.lblPricingScheme.TabIndex = 239;
            this.lblPricingScheme.Text = "Pricing Scheme";
            // 
            // lblSaleRate
            // 
            this.lblSaleRate.AutoSize = true;
            this.lblSaleRate.BackColor = System.Drawing.Color.Transparent;
            this.lblSaleRate.Location = new System.Drawing.Point(490, 57);
            this.lblSaleRate.Name = "lblSaleRate";
            this.lblSaleRate.Size = new System.Drawing.Size(54, 13);
            this.lblSaleRate.TabIndex = 237;
            this.lblSaleRate.Text = "Sale Rate";
            this.lblSaleRate.Visible = false;
            // 
            // cboCostingMethod
            // 
            this.cboCostingMethod.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCostingMethod.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCostingMethod.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cboCostingMethod.DropDownHeight = 75;
            this.cboCostingMethod.DropDownWidth = 198;
            this.cboCostingMethod.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboCostingMethod.FormattingEnabled = true;
            this.cboCostingMethod.IntegralHeight = false;
            this.cboCostingMethod.ItemHeight = 14;
            this.cboCostingMethod.Location = new System.Drawing.Point(119, 27);
            this.cboCostingMethod.Name = "cboCostingMethod";
            this.cboCostingMethod.Size = new System.Drawing.Size(242, 20);
            this.cboCostingMethod.TabIndex = 1;
            this.cboCostingMethod.SelectedIndexChanged += new System.EventHandler(this.cboCostingMethod_SelectedIndexChanged);
            this.cboCostingMethod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCostingMethod_KeyPress);
            // 
            // LblCostingMethod
            // 
            this.LblCostingMethod.AutoSize = true;
            this.LblCostingMethod.BackColor = System.Drawing.Color.Transparent;
            this.LblCostingMethod.Location = new System.Drawing.Point(24, 31);
            this.LblCostingMethod.Name = "LblCostingMethod";
            this.LblCostingMethod.Size = new System.Drawing.Size(81, 13);
            this.LblCostingMethod.TabIndex = 236;
            this.LblCostingMethod.Text = "Costing Method";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 95;
            this.label9.Text = "Details";
            // 
            // shapeContainer3
            // 
            this.shapeContainer3.Location = new System.Drawing.Point(1, 1);
            this.shapeContainer3.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer3.Name = "shapeContainer3";
            this.shapeContainer3.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape4,
            this.lineShape3});
            this.shapeContainer3.Size = new System.Drawing.Size(904, 298);
            this.shapeContainer3.TabIndex = 0;
            this.shapeContainer3.TabStop = false;
            // 
            // lineShape4
            // 
            this.lineShape4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 498;
            this.lineShape4.X2 = 887;
            this.lineShape4.Y1 = 16;
            this.lineShape4.Y2 = 16;
            // 
            // lineShape3
            // 
            this.lineShape3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 14;
            this.lineShape3.X2 = 403;
            this.lineShape3.Y1 = 16;
            this.lineShape3.Y2 = 16;
            // 
            // txtPurchaseRate
            // 
            // 
            // 
            // 
            this.txtPurchaseRate.Border.Class = "TextBoxBorder";
            this.txtPurchaseRate.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPurchaseRate.Location = new System.Drawing.Point(741, 53);
            this.txtPurchaseRate.MaxLength = 12;
            this.txtPurchaseRate.Name = "txtPurchaseRate";
            this.txtPurchaseRate.ShortcutsEnabled = false;
            this.txtPurchaseRate.Size = new System.Drawing.Size(100, 20);
            this.txtPurchaseRate.TabIndex = 6;
            this.txtPurchaseRate.Text = "0";
            this.txtPurchaseRate.TextChanged += new System.EventHandler(this.txtPurchaseRate_TextChanged);
            // 
            // txtSaleRate
            // 
            // 
            // 
            // 
            this.txtSaleRate.Border.Class = "TextBoxBorder";
            this.txtSaleRate.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSaleRate.Location = new System.Drawing.Point(547, 53);
            this.txtSaleRate.MaxLength = 12;
            this.txtSaleRate.Name = "txtSaleRate";
            this.txtSaleRate.ShortcutsEnabled = false;
            this.txtSaleRate.Size = new System.Drawing.Size(106, 20);
            this.txtSaleRate.TabIndex = 7;
            this.txtSaleRate.Text = "0";
            this.txtSaleRate.Visible = false;
            this.txtSaleRate.TextChanged += new System.EventHandler(this.txtSaleRate_TextChanged);
            // 
            // tbpCosting
            // 
            this.tbpCosting.AttachedControl = this.tabControlPanel6;
            this.tbpCosting.Name = "tbpCosting";
            this.tbpCosting.Text = "Costing Method";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel2.Controls.Add(this.dgvItemLocations);
            this.tabControlPanel2.Controls.Add(this.panelEx2);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(906, 300);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            this.tabControlPanel2.TabItem = this.tbpLocation;
            this.tabControlPanel2.ThemeAware = true;
            // 
            // dgvItemLocations
            // 
            this.dgvItemLocations.AddNewRow = true;
            this.dgvItemLocations.AlphaNumericCols = new int[0];
            this.dgvItemLocations.BackgroundColor = System.Drawing.Color.White;
            this.dgvItemLocations.CapsLockCols = new int[0];
            this.dgvItemLocations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvItemLocations.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColWarehouse,
            this.dgvColLocation,
            this.dgvColRow,
            this.dgvColBlock,
            this.dgvColLot,
            this.dgvColReorderPoint,
            this.dgvColReorderQuantity,
            this.dgvColSerialNo});
            this.dgvItemLocations.DecimalCols = new int[] {
        1,
        2};
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvItemLocations.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvItemLocations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvItemLocations.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvItemLocations.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvItemLocations.HasSlNo = false;
            this.dgvItemLocations.LastRowIndex = 0;
            this.dgvItemLocations.Location = new System.Drawing.Point(1, 1);
            this.dgvItemLocations.Name = "dgvItemLocations";
            this.dgvItemLocations.NegativeValueCols = new int[0];
            this.dgvItemLocations.NumericCols = new int[0];
            this.dgvItemLocations.RowHeadersWidth = 50;
            this.dgvItemLocations.Size = new System.Drawing.Size(904, 298);
            this.dgvItemLocations.TabIndex = 1;
            this.dgvItemLocations.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgv_CellBeginEdit);
            this.dgvItemLocations.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvItemLocations_CellValidating);
            this.dgvItemLocations.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.RowAddedEventExcludingLastRow);
            this.dgvItemLocations.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItemLocations_CellEndEdit);
            this.dgvItemLocations.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvItemLocations_CurrentCellDirtyStateChanged);
            this.dgvItemLocations.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvItemLocations_DataError_1);
            this.dgvItemLocations.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.RowRemovedEventExcludingLastRow);
            this.dgvItemLocations.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvItemLocations_RowHeaderMouseClick);
            // 
            // dgvColWarehouse
            // 
            this.dgvColWarehouse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColWarehouse.HeaderText = "Warehouse";
            this.dgvColWarehouse.Name = "dgvColWarehouse";
            this.dgvColWarehouse.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvColWarehouse.Width = 200;
            // 
            // dgvColLocation
            // 
            this.dgvColLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColLocation.HeaderText = "Location";
            this.dgvColLocation.Name = "dgvColLocation";
            this.dgvColLocation.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvColLocation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dgvColLocation.Visible = false;
            this.dgvColLocation.Width = 150;
            // 
            // dgvColRow
            // 
            this.dgvColRow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColRow.HeaderText = "Row";
            this.dgvColRow.Name = "dgvColRow";
            this.dgvColRow.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvColRow.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dgvColRow.Visible = false;
            // 
            // dgvColBlock
            // 
            this.dgvColBlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColBlock.HeaderText = "Block";
            this.dgvColBlock.Name = "dgvColBlock";
            this.dgvColBlock.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvColBlock.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dgvColBlock.Visible = false;
            // 
            // dgvColLot
            // 
            this.dgvColLot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColLot.HeaderText = "Lot";
            this.dgvColLot.Name = "dgvColLot";
            this.dgvColLot.Visible = false;
            // 
            // dgvColReorderPoint
            // 
            this.dgvColReorderPoint.HeaderText = "Reorder Point";
            this.dgvColReorderPoint.MaxInputLength = 6;
            this.dgvColReorderPoint.MinimumWidth = 75;
            this.dgvColReorderPoint.Name = "dgvColReorderPoint";
            this.dgvColReorderPoint.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvColReorderPoint.Width = 75;
            // 
            // dgvColReorderQuantity
            // 
            this.dgvColReorderQuantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgvColReorderQuantity.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvColReorderQuantity.HeaderText = "Reorder Quantity";
            this.dgvColReorderQuantity.MaxInputLength = 6;
            this.dgvColReorderQuantity.MinimumWidth = 75;
            this.dgvColReorderQuantity.Name = "dgvColReorderQuantity";
            this.dgvColReorderQuantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvColSerialNo
            // 
            this.dgvColSerialNo.HeaderText = "StorageOrderID";
            this.dgvColSerialNo.Name = "dgvColSerialNo";
            this.dgvColSerialNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvColSerialNo.Visible = false;
            this.dgvColSerialNo.Width = 50;
            // 
            // panelEx2
            // 
            this.panelEx2.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx2.Controls.Add(this.txtReorderPoint);
            this.panelEx2.Controls.Add(this.lblReorderpoint);
            this.panelEx2.Controls.Add(this.txtReorderQuantity);
            this.panelEx2.Controls.Add(this.lblReorderQuantity);
            this.panelEx2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEx2.Location = new System.Drawing.Point(1, 1);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(904, 40);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 0;
            this.panelEx2.TabStop = true;
            this.panelEx2.Visible = false;
            // 
            // txtReorderPoint
            // 
            // 
            // 
            // 
            this.txtReorderPoint.Border.Class = "TextBoxBorder";
            this.txtReorderPoint.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtReorderPoint.Location = new System.Drawing.Point(118, 13);
            this.txtReorderPoint.MaxLength = 12;
            this.txtReorderPoint.Name = "txtReorderPoint";
            this.txtReorderPoint.ShortcutsEnabled = false;
            this.txtReorderPoint.Size = new System.Drawing.Size(78, 20);
            this.txtReorderPoint.TabIndex = 1;
            this.txtReorderPoint.TextChanged += new System.EventHandler(this.ChangeStatus);
            this.txtReorderPoint.Enter += new System.EventHandler(this.txtReorderPoint_Enter);
            // 
            // lblReorderpoint
            // 
            this.lblReorderpoint.AutoSize = true;
            this.lblReorderpoint.BackColor = System.Drawing.Color.Transparent;
            this.lblReorderpoint.Location = new System.Drawing.Point(23, 15);
            this.lblReorderpoint.Name = "lblReorderpoint";
            this.lblReorderpoint.Size = new System.Drawing.Size(72, 13);
            this.lblReorderpoint.TabIndex = 244;
            this.lblReorderpoint.Text = "Reorder Point";
            // 
            // txtReorderQuantity
            // 
            // 
            // 
            // 
            this.txtReorderQuantity.Border.Class = "TextBoxBorder";
            this.txtReorderQuantity.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtReorderQuantity.Location = new System.Drawing.Point(328, 13);
            this.txtReorderQuantity.MaxLength = 12;
            this.txtReorderQuantity.Name = "txtReorderQuantity";
            this.txtReorderQuantity.ShortcutsEnabled = false;
            this.txtReorderQuantity.Size = new System.Drawing.Size(78, 20);
            this.txtReorderQuantity.TabIndex = 2;
            this.txtReorderQuantity.TextChanged += new System.EventHandler(this.ChangeStatus);
            this.txtReorderQuantity.Enter += new System.EventHandler(this.txtReorderPoint_Enter);
            // 
            // lblReorderQuantity
            // 
            this.lblReorderQuantity.AutoSize = true;
            this.lblReorderQuantity.BackColor = System.Drawing.Color.Transparent;
            this.lblReorderQuantity.Location = new System.Drawing.Point(218, 17);
            this.lblReorderQuantity.Name = "lblReorderQuantity";
            this.lblReorderQuantity.Size = new System.Drawing.Size(87, 13);
            this.lblReorderQuantity.TabIndex = 245;
            this.lblReorderQuantity.Text = "Reorder Quantity";
            // 
            // tbpLocation
            // 
            this.tbpLocation.AttachedControl = this.tabControlPanel2;
            this.tbpLocation.Name = "tbpLocation";
            this.tbpLocation.Text = "Reorder Level";
            // 
            // tabControlPanel7
            // 
            this.tabControlPanel7.Controls.Add(this.groupBox2);
            this.tabControlPanel7.Controls.Add(this.groupBox1);
            this.tabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel7.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel7.Name = "tabControlPanel7";
            this.tabControlPanel7.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel7.Size = new System.Drawing.Size(906, 300);
            this.tabControlPanel7.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.tabControlPanel7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel7.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel7.Style.GradientAngle = 90;
            this.tabControlPanel7.TabIndex = 7;
            this.tabControlPanel7.TabItem = this.tabDimensionAndFeatures;
            this.tabControlPanel7.ThemeAware = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.btnAddFeature);
            this.groupBox2.Controls.Add(this.dgvFeatures);
            this.groupBox2.Location = new System.Drawing.Point(423, 16);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(471, 261);
            this.groupBox2.TabIndex = 251;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Features";
            // 
            // btnAddFeature
            // 
            this.btnAddFeature.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddFeature.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddFeature.Location = new System.Drawing.Point(9, 18);
            this.btnAddFeature.Name = "btnAddFeature";
            this.btnAddFeature.Size = new System.Drawing.Size(87, 23);
            this.btnAddFeature.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddFeature.TabIndex = 249;
            this.btnAddFeature.Text = "Add Feature";
            this.btnAddFeature.Click += new System.EventHandler(this.btnAddFeature_Click);
            // 
            // dgvFeatures
            // 
            this.dgvFeatures.AddNewRow = false;
            this.dgvFeatures.AllowUserToAddRows = false;
            this.dgvFeatures.AllowUserToOrderColumns = true;
            this.dgvFeatures.AllowUserToResizeRows = false;
            this.dgvFeatures.AlphaNumericCols = new int[0];
            this.dgvFeatures.BackgroundColor = System.Drawing.Color.White;
            this.dgvFeatures.CapsLockCols = new int[0];
            this.dgvFeatures.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvFeatures.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewComboBoxColumn1,
            this.dataGridViewTextBoxColumn10});
            this.dgvFeatures.DecimalCols = new int[0];
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFeatures.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvFeatures.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvFeatures.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvFeatures.HasSlNo = false;
            this.dgvFeatures.LastRowIndex = 0;
            this.dgvFeatures.Location = new System.Drawing.Point(9, 47);
            this.dgvFeatures.Name = "dgvFeatures";
            this.dgvFeatures.NegativeValueCols = new int[0];
            this.dgvFeatures.NumericCols = new int[0];
            this.dgvFeatures.RowHeadersWidth = 50;
            this.dgvFeatures.Size = new System.Drawing.Size(456, 211);
            this.dgvFeatures.TabIndex = 248;
            this.dgvFeatures.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFeatures_CellValueChanged);
            this.dgvFeatures.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFeatures_CellDoubleClick);
            this.dgvFeatures.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvFeatures_UserDeletedRow);
            this.dgvFeatures.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.RowAddedEventIncludingLastRow);
            this.dgvFeatures.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvFeatures_DataError);
            this.dgvFeatures.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.RowRemovedEventIncludingLastRow);
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.HeaderText = "Feature";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.ReadOnly = true;
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn10.HeaderText = "Value(s)";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.dgvDimensions);
            this.groupBox1.Location = new System.Drawing.Point(9, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(395, 266);
            this.groupBox1.TabIndex = 250;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dimensions";
            // 
            // dgvDimensions
            // 
            this.dgvDimensions.AddNewRow = false;
            this.dgvDimensions.AlphaNumericCols = new int[0];
            this.dgvDimensions.BackgroundColor = System.Drawing.Color.White;
            this.dgvDimensions.CapsLockCols = new int[0];
            this.dgvDimensions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvDimensions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmCboDimension,
            this.clmDimensionValue,
            this.clmCboDimensionUOM});
            this.dgvDimensions.DecimalCols = new int[0];
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDimensions.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgvDimensions.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvDimensions.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvDimensions.HasSlNo = false;
            this.dgvDimensions.LastRowIndex = 0;
            this.dgvDimensions.Location = new System.Drawing.Point(9, 20);
            this.dgvDimensions.Name = "dgvDimensions";
            this.dgvDimensions.NegativeValueCols = new int[0];
            this.dgvDimensions.NumericCols = new int[0];
            this.dgvDimensions.RowHeadersWidth = 50;
            this.dgvDimensions.Size = new System.Drawing.Size(380, 242);
            this.dgvDimensions.TabIndex = 243;
            this.dgvDimensions.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDimensions_CellValueChanged);
            this.dgvDimensions.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvDimensions_CellBeginEdit);
            this.dgvDimensions.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.RowAddedEventExcludingLastRow);
            this.dgvDimensions.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDimensions_CellEndEdit);
            this.dgvDimensions.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvDimensions_EditingControlShowing);
            this.dgvDimensions.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvDimensions_CurrentCellDirtyStateChanged);
            this.dgvDimensions.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvDimensions_DataError);
            this.dgvDimensions.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.RowRemovedEventExcludingLastRow);
            // 
            // clmCboDimension
            // 
            this.clmCboDimension.DataPropertyName = "DimensionID";
            this.clmCboDimension.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clmCboDimension.HeaderText = "Dimension";
            this.clmCboDimension.Name = "clmCboDimension";
            this.clmCboDimension.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clmCboDimension.Width = 150;
            // 
            // clmDimensionValue
            // 
            this.clmDimensionValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmDimensionValue.DataPropertyName = "DimensionValue";
            this.clmDimensionValue.HeaderText = "Value";
            this.clmDimensionValue.Name = "clmDimensionValue";
            // 
            // clmCboDimensionUOM
            // 
            this.clmCboDimensionUOM.DataPropertyName = "UOMID";
            this.clmCboDimensionUOM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clmCboDimensionUOM.HeaderText = "UOM";
            this.clmCboDimensionUOM.Name = "clmCboDimensionUOM";
            this.clmCboDimensionUOM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clmCboDimensionUOM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.clmCboDimensionUOM.Width = 80;
            // 
            // tabDimensionAndFeatures
            // 
            this.tabDimensionAndFeatures.AttachedControl = this.tabControlPanel7;
            this.tabDimensionAndFeatures.Name = "tabDimensionAndFeatures";
            this.tabDimensionAndFeatures.Text = "Dimensions && Features";
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel3.Controls.Add(this.chkUOMSelectAll);
            this.tabControlPanel3.Controls.Add(this.label8);
            this.tabControlPanel3.Controls.Add(this.cboDefaultSalesUom);
            this.tabControlPanel3.Controls.Add(this.cboDefaultPurchaseUom);
            this.tabControlPanel3.Controls.Add(this.label6);
            this.tabControlPanel3.Controls.Add(this.label7);
            this.tabControlPanel3.Controls.Add(this.LblUnitOfMeasurements);
            this.tabControlPanel3.Controls.Add(this.shapeContainer2);
            this.tabControlPanel3.Controls.Add(this.dgvUnitsOfMeasurements);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(906, 300);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 0;
            this.tabControlPanel3.TabItem = this.tbpMeasurements;
            this.tabControlPanel3.ThemeAware = true;
            // 
            // chkUOMSelectAll
            // 
            this.chkUOMSelectAll.AutoSize = true;
            this.chkUOMSelectAll.Location = new System.Drawing.Point(82, 50);
            this.chkUOMSelectAll.Name = "chkUOMSelectAll";
            this.chkUOMSelectAll.Size = new System.Drawing.Size(15, 14);
            this.chkUOMSelectAll.TabIndex = 113;
            this.chkUOMSelectAll.UseVisualStyleBackColor = true;
            this.chkUOMSelectAll.CheckedChanged += new System.EventHandler(this.chkUOMSelectAll_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(392, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 112;
            this.label8.Text = "Default UOM";
            // 
            // cboDefaultSalesUom
            // 
            this.cboDefaultSalesUom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDefaultSalesUom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDefaultSalesUom.DisplayMember = "Text";
            this.cboDefaultSalesUom.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDefaultSalesUom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboDefaultSalesUom.FormattingEnabled = true;
            this.cboDefaultSalesUom.ItemHeight = 14;
            this.cboDefaultSalesUom.Location = new System.Drawing.Point(452, 73);
            this.cboDefaultSalesUom.Name = "cboDefaultSalesUom";
            this.cboDefaultSalesUom.Size = new System.Drawing.Size(125, 20);
            this.cboDefaultSalesUom.TabIndex = 109;
            // 
            // cboDefaultPurchaseUom
            // 
            this.cboDefaultPurchaseUom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDefaultPurchaseUom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDefaultPurchaseUom.DisplayMember = "Text";
            this.cboDefaultPurchaseUom.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDefaultPurchaseUom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboDefaultPurchaseUom.FormattingEnabled = true;
            this.cboDefaultPurchaseUom.ItemHeight = 14;
            this.cboDefaultPurchaseUom.Location = new System.Drawing.Point(452, 47);
            this.cboDefaultPurchaseUom.Name = "cboDefaultPurchaseUom";
            this.cboDefaultPurchaseUom.Size = new System.Drawing.Size(125, 20);
            this.cboDefaultPurchaseUom.TabIndex = 108;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(394, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 110;
            this.label6.Text = "Purchase";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(394, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 111;
            this.label7.Text = "Sales";
            // 
            // LblUnitOfMeasurements
            // 
            this.LblUnitOfMeasurements.AutoSize = true;
            this.LblUnitOfMeasurements.BackColor = System.Drawing.Color.Transparent;
            this.LblUnitOfMeasurements.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblUnitOfMeasurements.Location = new System.Drawing.Point(24, 18);
            this.LblUnitOfMeasurements.Name = "LblUnitOfMeasurements";
            this.LblUnitOfMeasurements.Size = new System.Drawing.Size(130, 13);
            this.LblUnitOfMeasurements.TabIndex = 0;
            this.LblUnitOfMeasurements.Text = "Units of Measurement";
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(1, 1);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2});
            this.shapeContainer2.Size = new System.Drawing.Size(904, 298);
            this.shapeContainer2.TabIndex = 107;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 74;
            this.lineShape2.X2 = 844;
            this.lineShape2.Y1 = 24;
            this.lineShape2.Y2 = 24;
            // 
            // dgvUnitsOfMeasurements
            // 
            this.dgvUnitsOfMeasurements.AddNewRow = false;
            this.dgvUnitsOfMeasurements.AllowDrop = true;
            this.dgvUnitsOfMeasurements.AllowUserToAddRows = false;
            this.dgvUnitsOfMeasurements.AllowUserToOrderColumns = true;
            this.dgvUnitsOfMeasurements.AllowUserToResizeColumns = false;
            this.dgvUnitsOfMeasurements.AllowUserToResizeRows = false;
            this.dgvUnitsOfMeasurements.AlphaNumericCols = new int[0];
            this.dgvUnitsOfMeasurements.BackgroundColor = System.Drawing.Color.White;
            this.dgvUnitsOfMeasurements.CapsLockCols = new int[0];
            this.dgvUnitsOfMeasurements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvUnitsOfMeasurements.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvClmChkSelect,
            this.dgvColUomType});
            this.dgvUnitsOfMeasurements.DecimalCols = new int[] {
        3};
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvUnitsOfMeasurements.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvUnitsOfMeasurements.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvUnitsOfMeasurements.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvUnitsOfMeasurements.HasSlNo = false;
            this.dgvUnitsOfMeasurements.LastRowIndex = 0;
            this.dgvUnitsOfMeasurements.Location = new System.Drawing.Point(25, 44);
            this.dgvUnitsOfMeasurements.Name = "dgvUnitsOfMeasurements";
            this.dgvUnitsOfMeasurements.NegativeValueCols = new int[0];
            this.dgvUnitsOfMeasurements.NumericCols = new int[0];
            this.dgvUnitsOfMeasurements.RowHeadersWidth = 50;
            this.dgvUnitsOfMeasurements.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvUnitsOfMeasurements.Size = new System.Drawing.Size(326, 231);
            this.dgvUnitsOfMeasurements.TabIndex = 8;
            this.dgvUnitsOfMeasurements.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUnitsOfMeasurements_CellValueChanged);
            this.dgvUnitsOfMeasurements.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvUnitsOfMeasurements_UserDeletingRow);
            this.dgvUnitsOfMeasurements.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvUnitsOfMeasurements_UserDeletedRow);
            this.dgvUnitsOfMeasurements.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.RowAddedEventIncludingLastRow);
            this.dgvUnitsOfMeasurements.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvUnitsOfMeasurements_CurrentCellDirtyStateChanged);
            this.dgvUnitsOfMeasurements.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.RowRemovedEventIncludingLastRow);
            // 
            // dgvClmChkSelect
            // 
            this.dgvClmChkSelect.HeaderText = "";
            this.dgvClmChkSelect.Name = "dgvClmChkSelect";
            this.dgvClmChkSelect.Width = 28;
            // 
            // dgvColUomType
            // 
            this.dgvColUomType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvColUomType.HeaderText = "UOM";
            this.dgvColUomType.Name = "dgvColUomType";
            this.dgvColUomType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvColUomType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tbpMeasurements
            // 
            this.tbpMeasurements.AttachedControl = this.tabControlPanel3;
            this.tbpMeasurements.Name = "tbpMeasurements";
            this.tbpMeasurements.Text = "Measurements";
            // 
            // tabControlPanel5
            // 
            this.tabControlPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel5.Controls.Add(this.webBatchDetails);
            this.tabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel5.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel5.Name = "tabControlPanel5";
            this.tabControlPanel5.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel5.Size = new System.Drawing.Size(906, 300);
            this.tabControlPanel5.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.tabControlPanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel5.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel5.Style.GradientAngle = 90;
            this.tabControlPanel5.TabIndex = 5;
            this.tabControlPanel5.TabItem = this.tbpBatch;
            this.tabControlPanel5.ThemeAware = true;
            // 
            // webBatchDetails
            // 
            this.webBatchDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBatchDetails.Location = new System.Drawing.Point(1, 1);
            this.webBatchDetails.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBatchDetails.Name = "webBatchDetails";
            this.webBatchDetails.Size = new System.Drawing.Size(904, 298);
            this.webBatchDetails.TabIndex = 0;
            // 
            // tbpBatch
            // 
            this.tbpBatch.AttachedControl = this.tabControlPanel5;
            this.tbpBatch.Name = "tbpBatch";
            this.tbpBatch.Text = "Batch Info";
            this.tbpBatch.Visible = false;
            // 
            // tabControlPanel13
            // 
            this.tabControlPanel13.Controls.Add(this.gbSaleRateHistory);
            this.tabControlPanel13.Controls.Add(this.lblNoSaleRecordsFound);
            this.tabControlPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel13.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel13.Name = "tabControlPanel13";
            this.tabControlPanel13.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel13.Size = new System.Drawing.Size(906, 300);
            this.tabControlPanel13.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.tabControlPanel13.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel13.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel13.Style.GradientAngle = 90;
            this.tabControlPanel13.TabIndex = 8;
            this.tabControlPanel13.TabItem = this.tpbSaleRate;
            this.tabControlPanel13.ThemeAware = true;
            // 
            // gbSaleRateHistory
            // 
            this.gbSaleRateHistory.BackColor = System.Drawing.Color.Transparent;
            this.gbSaleRateHistory.Controls.Add(this.dgvRateHistory);
            this.gbSaleRateHistory.Location = new System.Drawing.Point(15, 12);
            this.gbSaleRateHistory.Name = "gbSaleRateHistory";
            this.gbSaleRateHistory.Size = new System.Drawing.Size(321, 268);
            this.gbSaleRateHistory.TabIndex = 251;
            this.gbSaleRateHistory.TabStop = false;
            this.gbSaleRateHistory.Text = "Sale Rate History";
            // 
            // dgvRateHistory
            // 
            this.dgvRateHistory.AddNewRow = false;
            this.dgvRateHistory.AlphaNumericCols = new int[0];
            this.dgvRateHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRateHistory.BackgroundColor = System.Drawing.Color.White;
            this.dgvRateHistory.CapsLockCols = new int[0];
            this.dgvRateHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvRateHistory.DecimalCols = new int[0];
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRateHistory.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvRateHistory.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvRateHistory.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvRateHistory.HasSlNo = false;
            this.dgvRateHistory.LastRowIndex = 0;
            this.dgvRateHistory.Location = new System.Drawing.Point(6, 18);
            this.dgvRateHistory.Name = "dgvRateHistory";
            this.dgvRateHistory.NegativeValueCols = new int[0];
            this.dgvRateHistory.NumericCols = new int[0];
            this.dgvRateHistory.ReadOnly = true;
            this.dgvRateHistory.RowHeadersWidth = 50;
            this.dgvRateHistory.Size = new System.Drawing.Size(309, 242);
            this.dgvRateHistory.TabIndex = 243;
            // 
            // lblNoSaleRecordsFound
            // 
            this.lblNoSaleRecordsFound.AutoSize = true;
            this.lblNoSaleRecordsFound.BackColor = System.Drawing.Color.Transparent;
            this.lblNoSaleRecordsFound.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoSaleRecordsFound.Location = new System.Drawing.Point(334, 153);
            this.lblNoSaleRecordsFound.Name = "lblNoSaleRecordsFound";
            this.lblNoSaleRecordsFound.Size = new System.Drawing.Size(142, 13);
            this.lblNoSaleRecordsFound.TabIndex = 3;
            this.lblNoSaleRecordsFound.Text = "No Sale Records Found";
            // 
            // tpbSaleRate
            // 
            this.tpbSaleRate.AttachedControl = this.tabControlPanel13;
            this.tpbSaleRate.Name = "tpbSaleRate";
            this.tpbSaleRate.Text = "SaleRate History";
            // 
            // panelGridBottom
            // 
            this.panelGridBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelGridBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelGridBottom.Controls.Add(this.lblItemMasterStatus);
            this.panelGridBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGridBottom.Location = new System.Drawing.Point(0, 327);
            this.panelGridBottom.Name = "panelGridBottom";
            this.panelGridBottom.Size = new System.Drawing.Size(906, 26);
            this.panelGridBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelGridBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelGridBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelGridBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelGridBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelGridBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelGridBottom.Style.GradientAngle = 90;
            this.panelGridBottom.TabIndex = 2;
            // 
            // lblItemMasterStatus
            // 
            // 
            // 
            // 
            this.lblItemMasterStatus.BackgroundStyle.Class = "";
            this.lblItemMasterStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblItemMasterStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblItemMasterStatus.Location = new System.Drawing.Point(0, 3);
            this.lblItemMasterStatus.Name = "lblItemMasterStatus";
            this.lblItemMasterStatus.Size = new System.Drawing.Size(906, 23);
            this.lblItemMasterStatus.TabIndex = 0;
            this.lblItemMasterStatus.Text = "labelX2";
            // 
            // expandableSplitterTop
            // 
            this.expandableSplitterTop.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterTop.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitterTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitterTop.ExpandableControl = this.panelTop;
            this.expandableSplitterTop.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.expandableSplitterTop.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(179)))), ((int)(((byte)(219)))));
            this.expandableSplitterTop.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterTop.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterTop.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expandableSplitterTop.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.Location = new System.Drawing.Point(0, 185);
            this.expandableSplitterTop.Name = "expandableSplitterTop";
            this.expandableSplitterTop.Size = new System.Drawing.Size(906, 3);
            this.expandableSplitterTop.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterTop.TabIndex = 0;
            this.expandableSplitterTop.TabStop = false;
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelTop.Controls.Add(this.label36);
            this.panelTop.Controls.Add(this.cboProductType);
            this.panelTop.Controls.Add(this.txtCutOffPrice);
            this.panelTop.Controls.Add(this.lblCutOffPrice);
            this.panelTop.Controls.Add(this.chkExpiryDateMandatory);
            this.panelTop.Controls.Add(this.txtDescriptionArabic);
            this.panelTop.Controls.Add(this.label5);
            this.panelTop.Controls.Add(this.txtShortName);
            this.panelTop.Controls.Add(this.txtDescription);
            this.panelTop.Controls.Add(this.txtItemCode);
            this.panelTop.Controls.Add(this.label10);
            this.panelTop.Controls.Add(this.LblCategory);
            this.panelTop.Controls.Add(this.LblItemType);
            this.panelTop.Controls.Add(this.cboCategory);
            this.panelTop.Controls.Add(this.cboSubCategory);
            this.panelTop.Controls.Add(this.cboStatus);
            this.panelTop.Controls.Add(this.label2);
            this.panelTop.Controls.Add(this.cboBaseUom);
            this.panelTop.Controls.Add(this.btnSubCategory);
            this.panelTop.Controls.Add(this.btnCategory);
            this.panelTop.Controls.Add(this.btnBaseUom);
            this.panelTop.Controls.Add(this.LblTCompany);
            this.panelTop.Controls.Add(this.LblTVendorAddress);
            this.panelTop.Controls.Add(this.LblTVendor);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(906, 160);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 0;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(505, 11);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(71, 13);
            this.label36.TabIndex = 232;
            this.label36.Text = "Product Type";
            // 
            // cboProductType
            // 
            this.cboProductType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboProductType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboProductType.DisplayMember = "Text";
            this.cboProductType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboProductType.DropDownHeight = 75;
            this.cboProductType.DropDownWidth = 198;
            this.cboProductType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboProductType.FormattingEnabled = true;
            this.cboProductType.IntegralHeight = false;
            this.cboProductType.ItemHeight = 14;
            this.cboProductType.Location = new System.Drawing.Point(604, 7);
            this.cboProductType.Name = "cboProductType";
            this.cboProductType.Size = new System.Drawing.Size(242, 20);
            this.cboProductType.TabIndex = 6;
            this.cboProductType.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // txtCutOffPrice
            // 
            this.txtCutOffPrice.Location = new System.Drawing.Point(119, 131);
            this.txtCutOffPrice.MaxLength = 10;
            this.txtCutOffPrice.Name = "txtCutOffPrice";
            this.txtCutOffPrice.ShortcutsEnabled = false;
            this.txtCutOffPrice.Size = new System.Drawing.Size(288, 20);
            this.txtCutOffPrice.TabIndex = 4;
            this.txtCutOffPrice.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // lblCutOffPrice
            // 
            this.lblCutOffPrice.AutoSize = true;
            this.lblCutOffPrice.Location = new System.Drawing.Point(24, 136);
            this.lblCutOffPrice.Name = "lblCutOffPrice";
            this.lblCutOffPrice.Size = new System.Drawing.Size(64, 13);
            this.lblCutOffPrice.TabIndex = 230;
            this.lblCutOffPrice.Text = "CutOff Price";
            // 
            // chkExpiryDateMandatory
            // 
            this.chkExpiryDateMandatory.AutoSize = true;
            this.chkExpiryDateMandatory.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkExpiryDateMandatory.Location = new System.Drawing.Point(508, 139);
            this.chkExpiryDateMandatory.Name = "chkExpiryDateMandatory";
            this.chkExpiryDateMandatory.Size = new System.Drawing.Size(143, 17);
            this.chkExpiryDateMandatory.TabIndex = 5;
            this.chkExpiryDateMandatory.Text = "Expiry Date is Mandatory";
            this.chkExpiryDateMandatory.UseVisualStyleBackColor = true;
            this.chkExpiryDateMandatory.Visible = false;
            // 
            // txtDescriptionArabic
            // 
            // 
            // 
            // 
            this.txtDescriptionArabic.Border.Class = "TextBoxBorder";
            this.txtDescriptionArabic.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDescriptionArabic.Location = new System.Drawing.Point(119, 104);
            this.txtDescriptionArabic.MaxLength = 150;
            this.txtDescriptionArabic.Name = "txtDescriptionArabic";
            this.txtDescriptionArabic.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescriptionArabic.Size = new System.Drawing.Size(288, 20);
            this.txtDescriptionArabic.TabIndex = 3;
            this.txtDescriptionArabic.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 13);
            this.label5.TabIndex = 224;
            this.label5.Text = "Item Name (Arabic)";
            // 
            // txtShortName
            // 
            // 
            // 
            // 
            this.txtShortName.Border.Class = "TextBoxBorder";
            this.txtShortName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtShortName.Location = new System.Drawing.Point(119, 32);
            this.txtShortName.MaxLength = 50;
            this.txtShortName.Name = "txtShortName";
            this.txtShortName.Size = new System.Drawing.Size(242, 20);
            this.txtShortName.TabIndex = 1;
            this.txtShortName.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // txtDescription
            // 
            // 
            // 
            // 
            this.txtDescription.Border.Class = "TextBoxBorder";
            this.txtDescription.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDescription.Location = new System.Drawing.Point(119, 57);
            this.txtDescription.MaxLength = 200;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(357, 41);
            this.txtDescription.TabIndex = 2;
            this.txtDescription.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // txtItemCode
            // 
            // 
            // 
            // 
            this.txtItemCode.Border.Class = "TextBoxBorder";
            this.txtItemCode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtItemCode.Location = new System.Drawing.Point(119, 7);
            this.txtItemCode.MaxLength = 20;
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Size = new System.Drawing.Size(242, 20);
            this.txtItemCode.TabIndex = 0;
            this.txtItemCode.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(505, 118);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 223;
            this.label10.Text = "Status";
            // 
            // LblCategory
            // 
            this.LblCategory.AutoSize = true;
            this.LblCategory.Location = new System.Drawing.Point(505, 37);
            this.LblCategory.Name = "LblCategory";
            this.LblCategory.Size = new System.Drawing.Size(49, 13);
            this.LblCategory.TabIndex = 220;
            this.LblCategory.Text = "Category";
            // 
            // LblItemType
            // 
            this.LblItemType.AutoSize = true;
            this.LblItemType.Location = new System.Drawing.Point(505, 64);
            this.LblItemType.Name = "LblItemType";
            this.LblItemType.Size = new System.Drawing.Size(71, 13);
            this.LblItemType.TabIndex = 219;
            this.LblItemType.Text = "Sub Category";
            // 
            // cboCategory
            // 
            this.cboCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCategory.DisplayMember = "Text";
            this.cboCategory.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCategory.DropDownHeight = 75;
            this.cboCategory.DropDownWidth = 198;
            this.cboCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboCategory.FormattingEnabled = true;
            this.cboCategory.IntegralHeight = false;
            this.cboCategory.ItemHeight = 14;
            this.cboCategory.Location = new System.Drawing.Point(604, 33);
            this.cboCategory.Name = "cboCategory";
            this.cboCategory.Size = new System.Drawing.Size(242, 20);
            this.cboCategory.TabIndex = 7;
            this.cboCategory.SelectedIndexChanged += new System.EventHandler(this.cboCategory_SelectedIndexChanged);
            this.cboCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_KeyDown);
            // 
            // cboSubCategory
            // 
            this.cboSubCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSubCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSubCategory.DisplayMember = "Text";
            this.cboSubCategory.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSubCategory.DropDownHeight = 75;
            this.cboSubCategory.DropDownWidth = 198;
            this.cboSubCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboSubCategory.FormattingEnabled = true;
            this.cboSubCategory.IntegralHeight = false;
            this.cboSubCategory.ItemHeight = 14;
            this.cboSubCategory.Location = new System.Drawing.Point(604, 60);
            this.cboSubCategory.Name = "cboSubCategory";
            this.cboSubCategory.Size = new System.Drawing.Size(242, 20);
            this.cboSubCategory.TabIndex = 9;
            this.cboSubCategory.SelectedIndexChanged += new System.EventHandler(this.SubcategoryChangeStatus);
            this.cboSubCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_KeyDown);
            // 
            // cboStatus
            // 
            this.cboStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboStatus.DisplayMember = "Text";
            this.cboStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboStatus.DropDownHeight = 75;
            this.cboStatus.DropDownWidth = 198;
            this.cboStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.IntegralHeight = false;
            this.cboStatus.ItemHeight = 14;
            this.cboStatus.Location = new System.Drawing.Point(604, 114);
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Size = new System.Drawing.Size(242, 20);
            this.cboStatus.TabIndex = 13;
            this.cboStatus.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.cboStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(505, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 213;
            this.label2.Text = "Base UOM";
            // 
            // cboBaseUom
            // 
            this.cboBaseUom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBaseUom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBaseUom.DisplayMember = "Text";
            this.cboBaseUom.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboBaseUom.DropDownHeight = 75;
            this.cboBaseUom.DropDownWidth = 198;
            this.cboBaseUom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboBaseUom.FormattingEnabled = true;
            this.cboBaseUom.IntegralHeight = false;
            this.cboBaseUom.ItemHeight = 14;
            this.cboBaseUom.Location = new System.Drawing.Point(604, 87);
            this.cboBaseUom.Name = "cboBaseUom";
            this.cboBaseUom.Size = new System.Drawing.Size(242, 20);
            this.cboBaseUom.TabIndex = 11;
            this.cboBaseUom.SelectedIndexChanged += new System.EventHandler(this.cboBaseUom_SelectedIndexChanged);
            this.cboBaseUom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_KeyDown);
            // 
            // btnSubCategory
            // 
            this.btnSubCategory.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSubCategory.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSubCategory.Location = new System.Drawing.Point(856, 60);
            this.btnSubCategory.Name = "btnSubCategory";
            this.btnSubCategory.Size = new System.Drawing.Size(29, 20);
            this.btnSubCategory.TabIndex = 10;
            this.btnSubCategory.Text = "...";
            this.btnSubCategory.Click += new System.EventHandler(this.btnSubCategory_Click);
            // 
            // btnCategory
            // 
            this.btnCategory.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCategory.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCategory.Location = new System.Drawing.Point(856, 33);
            this.btnCategory.Name = "btnCategory";
            this.btnCategory.Size = new System.Drawing.Size(29, 20);
            this.btnCategory.TabIndex = 8;
            this.btnCategory.Text = "...";
            this.btnCategory.Click += new System.EventHandler(this.btnCategory_Click);
            // 
            // btnBaseUom
            // 
            this.btnBaseUom.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnBaseUom.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnBaseUom.Location = new System.Drawing.Point(856, 87);
            this.btnBaseUom.Name = "btnBaseUom";
            this.btnBaseUom.Size = new System.Drawing.Size(29, 20);
            this.btnBaseUom.TabIndex = 12;
            this.btnBaseUom.Text = "...";
            this.btnBaseUom.Click += new System.EventHandler(this.btnBaseUom_Click);
            // 
            // LblTCompany
            // 
            this.LblTCompany.AutoSize = true;
            this.LblTCompany.Location = new System.Drawing.Point(24, 9);
            this.LblTCompany.Name = "LblTCompany";
            this.LblTCompany.Size = new System.Drawing.Size(55, 13);
            this.LblTCompany.TabIndex = 200;
            this.LblTCompany.Text = "Item Code";
            // 
            // LblTVendorAddress
            // 
            this.LblTVendorAddress.AutoSize = true;
            this.LblTVendorAddress.Location = new System.Drawing.Point(24, 63);
            this.LblTVendorAddress.Name = "LblTVendorAddress";
            this.LblTVendorAddress.Size = new System.Drawing.Size(58, 13);
            this.LblTVendorAddress.TabIndex = 194;
            this.LblTVendorAddress.Text = "Item Name";
            // 
            // LblTVendor
            // 
            this.LblTVendor.AutoSize = true;
            this.LblTVendor.Location = new System.Drawing.Point(24, 36);
            this.LblTVendor.Name = "LblTVendor";
            this.LblTVendor.Size = new System.Drawing.Size(63, 13);
            this.LblTVendor.TabIndex = 184;
            this.LblTVendor.Text = "Short Name";
            // 
            // ItemMasterBindingNavigator
            // 
            this.ItemMasterBindingNavigator.AccessibleDescription = "DotNetBar Bar (ItemMasterBindingNavigator)";
            this.ItemMasterBindingNavigator.AccessibleName = "DotNetBar Bar";
            this.ItemMasterBindingNavigator.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.ItemMasterBindingNavigator.Dock = System.Windows.Forms.DockStyle.Top;
            this.ItemMasterBindingNavigator.DockLine = 1;
            this.ItemMasterBindingNavigator.DockOffset = 73;
            this.ItemMasterBindingNavigator.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.ItemMasterBindingNavigator.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.ItemMasterBindingNavigator.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnCancel,
            this.bnDeleteItem,
            this.bnUom,
            this.btnUomConversion,
            this.btnPricingScheme,
            this.bnPrint,
            this.bnEmail,
            this.BtnPrint});
            this.ItemMasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.ItemMasterBindingNavigator.Name = "ItemMasterBindingNavigator";
            this.ItemMasterBindingNavigator.Size = new System.Drawing.Size(906, 25);
            this.ItemMasterBindingNavigator.Stretch = true;
            this.ItemMasterBindingNavigator.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ItemMasterBindingNavigator.TabIndex = 12;
            this.ItemMasterBindingNavigator.TabStop = false;
            this.ItemMasterBindingNavigator.Text = "bar2";
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlEnter);
            this.bnAddNewItem.Text = "Add";
            this.bnAddNewItem.Tooltip = "Add New Information";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlS);
            this.bnSaveItem.Text = "Save";
            this.bnSaveItem.Tooltip = "Save";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnCancel
            // 
            this.bnCancel.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnCancel.Image = ((System.Drawing.Image)(resources.GetObject("bnCancel.Image")));
            this.bnCancel.Name = "bnCancel";
            this.bnCancel.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.bnCancel.Text = "Clear";
            this.bnCancel.Tooltip = "Clear";
            this.bnCancel.Click += new System.EventHandler(this.bnCancel_Click);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bnDeleteItem.Image")));
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlD);
            this.bnDeleteItem.Text = "Delete";
            this.bnDeleteItem.Tooltip = "Delete";
            this.bnDeleteItem.Click += new System.EventHandler(this.bnDeleteItem_Click);
            // 
            // bnUom
            // 
            this.bnUom.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnUom.Icon = ((System.Drawing.Icon)(resources.GetObject("bnUom.Icon")));
            this.bnUom.Name = "bnUom";
            this.bnUom.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlU);
            this.bnUom.Text = "Uom";
            this.bnUom.Tooltip = "Unit Of Measurement";
            this.bnUom.Click += new System.EventHandler(this.bnUom_Click);
            // 
            // btnUomConversion
            // 
            this.btnUomConversion.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnUomConversion.Image = global::MyBooksERP.Properties.Resources.UMO_Conversion;
            this.btnUomConversion.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.btnUomConversion.Name = "btnUomConversion";
            this.btnUomConversion.Text = "Uom Conversion";
            this.btnUomConversion.Click += new System.EventHandler(this.btnUomConversion_Click);
            // 
            // btnPricingScheme
            // 
            this.btnPricingScheme.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPricingScheme.Image = global::MyBooksERP.Properties.Resources.Pricing_Scheme;
            this.btnPricingScheme.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.btnPricingScheme.Name = "btnPricingScheme";
            this.btnPricingScheme.Text = "Pricing Scheme";
            this.btnPricingScheme.Click += new System.EventHandler(this.btnPricingScheme_Click);
            // 
            // bnPrint
            // 
            this.bnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnPrint.Image = ((System.Drawing.Image)(resources.GetObject("bnPrint.Image")));
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.bnPrint.Text = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // bnEmail
            // 
            this.bnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnEmail.Image = ((System.Drawing.Image)(resources.GetObject("bnEmail.Image")));
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.bnEmail.Text = "Email";
            this.bnEmail.Visible = false;
            this.bnEmail.Click += new System.EventHandler(this.bnEmail_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Text = "buttonItem1";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // tabComponents
            // 
            this.tabComponents.Name = "tabComponents";
            this.tabComponents.Text = "Components";
            // 
            // tabProductionStage
            // 
            this.tabProductionStage.Name = "tabProductionStage";
            this.tabProductionStage.Text = "Production Stages";
            // 
            // errorItemMaster
            // 
            this.errorItemMaster.ContainerControl = this;
            this.errorItemMaster.RightToLeft = true;
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(57, 6);
            // 
            // tmItemMaster
            // 
            this.tmItemMaster.Tick += new System.EventHandler(this.TimerTick);
            // 
            // tabControlPanel4
            // 
            this.tabControlPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel4.Controls.Add(this.webBrowser1);
            this.tabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel4.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel4.Name = "tabControlPanel4";
            this.tabControlPanel4.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel4.Size = new System.Drawing.Size(906, 318);
            this.tabControlPanel4.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.tabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel4.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel4.Style.GradientAngle = 90;
            this.tabControlPanel4.TabIndex = 5;
            this.tabControlPanel4.ThemeAware = true;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(1, 1);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(904, 316);
            this.webBrowser1.TabIndex = 0;
            // 
            // tabControlPanel8
            // 
            this.tabControlPanel8.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel8.Controls.Add(this.clsInnerGridBar1);
            this.tabControlPanel8.Controls.Add(this.panelEx3);
            this.tabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel8.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel8.Name = "tabControlPanel8";
            this.tabControlPanel8.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel8.Size = new System.Drawing.Size(906, 318);
            this.tabControlPanel8.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.tabControlPanel8.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel8.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel8.Style.GradientAngle = 90;
            this.tabControlPanel8.TabIndex = 2;
            this.tabControlPanel8.ThemeAware = true;
            // 
            // clsInnerGridBar1
            // 
            this.clsInnerGridBar1.AddNewRow = true;
            this.clsInnerGridBar1.AlphaNumericCols = new int[0];
            this.clsInnerGridBar1.BackgroundColor = System.Drawing.Color.White;
            this.clsInnerGridBar1.CapsLockCols = new int[0];
            this.clsInnerGridBar1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.clsInnerGridBar1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewComboBoxColumn2,
            this.dataGridViewComboBoxColumn3,
            this.dataGridViewComboBoxColumn4,
            this.dataGridViewComboBoxColumn5,
            this.dataGridViewComboBoxColumn6,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13});
            this.clsInnerGridBar1.DecimalCols = new int[] {
        1,
        2};
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.clsInnerGridBar1.DefaultCellStyle = dataGridViewCellStyle12;
            this.clsInnerGridBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clsInnerGridBar1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.clsInnerGridBar1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.clsInnerGridBar1.HasSlNo = false;
            this.clsInnerGridBar1.LastRowIndex = 0;
            this.clsInnerGridBar1.Location = new System.Drawing.Point(1, 41);
            this.clsInnerGridBar1.Name = "clsInnerGridBar1";
            this.clsInnerGridBar1.NegativeValueCols = new int[0];
            this.clsInnerGridBar1.NumericCols = new int[0];
            this.clsInnerGridBar1.RowHeadersWidth = 50;
            this.clsInnerGridBar1.Size = new System.Drawing.Size(904, 276);
            this.clsInnerGridBar1.TabIndex = 1;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn2.HeaderText = "Warehouse";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.Width = 200;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn3.HeaderText = "Location";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn3.Visible = false;
            this.dataGridViewComboBoxColumn3.Width = 150;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn4.HeaderText = "Row";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn4.Visible = false;
            // 
            // dataGridViewComboBoxColumn5
            // 
            this.dataGridViewComboBoxColumn5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn5.HeaderText = "Block";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn5.Visible = false;
            // 
            // dataGridViewComboBoxColumn6
            // 
            this.dataGridViewComboBoxColumn6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn6.HeaderText = "Lot";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "Reorder Point";
            this.dataGridViewTextBoxColumn11.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn11.MinimumWidth = 75;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Width = 75;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn12.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn12.HeaderText = "Reorder Quantity";
            this.dataGridViewTextBoxColumn12.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn12.MinimumWidth = 75;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "StorageOrderID";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Visible = false;
            this.dataGridViewTextBoxColumn13.Width = 50;
            // 
            // panelEx3
            // 
            this.panelEx3.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx3.Controls.Add(this.decimalDotNetBarTextBox1);
            this.panelEx3.Controls.Add(this.label3);
            this.panelEx3.Controls.Add(this.decimalDotNetBarTextBox2);
            this.panelEx3.Controls.Add(this.label14);
            this.panelEx3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEx3.Location = new System.Drawing.Point(1, 1);
            this.panelEx3.Name = "panelEx3";
            this.panelEx3.Size = new System.Drawing.Size(904, 40);
            this.panelEx3.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx3.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx3.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx3.Style.GradientAngle = 90;
            this.panelEx3.TabIndex = 0;
            this.panelEx3.TabStop = true;
            // 
            // decimalDotNetBarTextBox1
            // 
            // 
            // 
            // 
            this.decimalDotNetBarTextBox1.Border.Class = "TextBoxBorder";
            this.decimalDotNetBarTextBox1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.decimalDotNetBarTextBox1.Location = new System.Drawing.Point(118, 13);
            this.decimalDotNetBarTextBox1.MaxLength = 12;
            this.decimalDotNetBarTextBox1.Name = "decimalDotNetBarTextBox1";
            this.decimalDotNetBarTextBox1.ShortcutsEnabled = false;
            this.decimalDotNetBarTextBox1.Size = new System.Drawing.Size(78, 20);
            this.decimalDotNetBarTextBox1.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(23, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 244;
            this.label3.Text = "Reorder Point";
            // 
            // decimalDotNetBarTextBox2
            // 
            // 
            // 
            // 
            this.decimalDotNetBarTextBox2.Border.Class = "TextBoxBorder";
            this.decimalDotNetBarTextBox2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.decimalDotNetBarTextBox2.Location = new System.Drawing.Point(328, 13);
            this.decimalDotNetBarTextBox2.MaxLength = 12;
            this.decimalDotNetBarTextBox2.Name = "decimalDotNetBarTextBox2";
            this.decimalDotNetBarTextBox2.ShortcutsEnabled = false;
            this.decimalDotNetBarTextBox2.Size = new System.Drawing.Size(78, 20);
            this.decimalDotNetBarTextBox2.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(218, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 13);
            this.label14.TabIndex = 245;
            this.label14.Text = "Reorder Quantity";
            // 
            // tabControlPanel9
            // 
            this.tabControlPanel9.Controls.Add(this.decimalDotNetBarTextBox3);
            this.tabControlPanel9.Controls.Add(this.label15);
            this.tabControlPanel9.Controls.Add(this.radioButton1);
            this.tabControlPanel9.Controls.Add(this.radioButton2);
            this.tabControlPanel9.Controls.Add(this.clsInnerGridBar2);
            this.tabControlPanel9.Controls.Add(this.comboBoxEx1);
            this.tabControlPanel9.Controls.Add(this.buttonX1);
            this.tabControlPanel9.Controls.Add(this.label16);
            this.tabControlPanel9.Controls.Add(this.label17);
            this.tabControlPanel9.Controls.Add(this.comboBoxEx2);
            this.tabControlPanel9.Controls.Add(this.label18);
            this.tabControlPanel9.Controls.Add(this.label19);
            this.tabControlPanel9.Controls.Add(this.shapeContainer4);
            this.tabControlPanel9.Controls.Add(this.decimalDotNetBarTextBox4);
            this.tabControlPanel9.Controls.Add(this.decimalDotNetBarTextBox5);
            this.tabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel9.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel9.Name = "tabControlPanel9";
            this.tabControlPanel9.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel9.Size = new System.Drawing.Size(906, 318);
            this.tabControlPanel9.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.tabControlPanel9.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel9.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel9.Style.GradientAngle = 90;
            this.tabControlPanel9.TabIndex = 6;
            this.tabControlPanel9.ThemeAware = true;
            // 
            // decimalDotNetBarTextBox3
            // 
            // 
            // 
            // 
            this.decimalDotNetBarTextBox3.Border.Class = "TextBoxBorder";
            this.decimalDotNetBarTextBox3.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.decimalDotNetBarTextBox3.Location = new System.Drawing.Point(794, 53);
            this.decimalDotNetBarTextBox3.MaxLength = 12;
            this.decimalDotNetBarTextBox3.Name = "decimalDotNetBarTextBox3";
            this.decimalDotNetBarTextBox3.ShortcutsEnabled = false;
            this.decimalDotNetBarTextBox3.Size = new System.Drawing.Size(100, 20);
            this.decimalDotNetBarTextBox3.TabIndex = 8;
            this.decimalDotNetBarTextBox3.Text = "0";
            this.decimalDotNetBarTextBox3.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(487, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(116, 13);
            this.label15.TabIndex = 247;
            this.label15.Text = "Sale Rate Based On";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.BackColor = System.Drawing.Color.Transparent;
            this.radioButton1.Location = new System.Drawing.Point(623, 29);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(81, 17);
            this.radioButton1.TabIndex = 5;
            this.radioButton1.Tag = "";
            this.radioButton1.Text = "Actual Rate";
            this.radioButton1.UseVisualStyleBackColor = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.BackColor = System.Drawing.Color.Transparent;
            this.radioButton2.Location = new System.Drawing.Point(508, 29);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(96, 17);
            this.radioButton2.TabIndex = 4;
            this.radioButton2.Tag = "";
            this.radioButton2.Text = "Purchase Rate";
            this.radioButton2.UseVisualStyleBackColor = false;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(367, 53);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(40, 20);
            this.buttonX1.TabIndex = 3;
            this.buttonX1.Text = "Show";
            this.buttonX1.Tooltip = "Show";
            this.buttonX1.Visible = false;
            // 
            // clsInnerGridBar2
            // 
            this.clsInnerGridBar2.AddNewRow = true;
            this.clsInnerGridBar2.AllowUserToAddRows = false;
            this.clsInnerGridBar2.AllowUserToDeleteRows = false;
            this.clsInnerGridBar2.AllowUserToResizeColumns = false;
            this.clsInnerGridBar2.AllowUserToResizeRows = false;
            this.clsInnerGridBar2.AlphaNumericCols = new int[0];
            this.clsInnerGridBar2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.clsInnerGridBar2.BackgroundColor = System.Drawing.Color.White;
            this.clsInnerGridBar2.CapsLockCols = new int[0];
            this.clsInnerGridBar2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.clsInnerGridBar2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewDoubleInputColumn1,
            this.dataGridViewDoubleInputColumn2,
            this.dataGridViewDoubleInputColumn3});
            this.clsInnerGridBar2.DecimalCols = new int[] {
        1,
        2};
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.clsInnerGridBar2.DefaultCellStyle = dataGridViewCellStyle13;
            this.clsInnerGridBar2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.clsInnerGridBar2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.clsInnerGridBar2.HasSlNo = false;
            this.clsInnerGridBar2.LastRowIndex = 0;
            this.clsInnerGridBar2.Location = new System.Drawing.Point(1, 79);
            this.clsInnerGridBar2.Name = "clsInnerGridBar2";
            this.clsInnerGridBar2.NegativeValueCols = new int[0];
            this.clsInnerGridBar2.NumericCols = new int[0];
            this.clsInnerGridBar2.RowHeadersWidth = 50;
            this.clsInnerGridBar2.Size = new System.Drawing.Size(911, 236);
            this.clsInnerGridBar2.TabIndex = 9;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "ColBatchID";
            this.dataGridViewTextBoxColumn14.HeaderText = "BatchID";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn15.DataPropertyName = "ColBatchNo";
            this.dataGridViewTextBoxColumn15.HeaderText = "Batch No";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn16.DataPropertyName = "ColExpiryDate";
            this.dataGridViewTextBoxColumn16.HeaderText = "Expiry Date";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewDoubleInputColumn1
            // 
            this.dataGridViewDoubleInputColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            // 
            // 
            // 
            this.dataGridViewDoubleInputColumn1.BackgroundStyle.Class = "DataGridViewNumericBorder";
            this.dataGridViewDoubleInputColumn1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dataGridViewDoubleInputColumn1.DataPropertyName = "ColPurchaseRate";
            this.dataGridViewDoubleInputColumn1.HeaderText = "Purchase Rate";
            this.dataGridViewDoubleInputColumn1.Increment = 1;
            this.dataGridViewDoubleInputColumn1.Name = "dataGridViewDoubleInputColumn1";
            this.dataGridViewDoubleInputColumn1.ReadOnly = true;
            this.dataGridViewDoubleInputColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewDoubleInputColumn2
            // 
            this.dataGridViewDoubleInputColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            // 
            // 
            // 
            this.dataGridViewDoubleInputColumn2.BackgroundStyle.Class = "DataGridViewNumericBorder";
            this.dataGridViewDoubleInputColumn2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dataGridViewDoubleInputColumn2.DataPropertyName = "ColActualRate";
            this.dataGridViewDoubleInputColumn2.HeaderText = "Actual Rate";
            this.dataGridViewDoubleInputColumn2.Increment = 1;
            this.dataGridViewDoubleInputColumn2.Name = "dataGridViewDoubleInputColumn2";
            this.dataGridViewDoubleInputColumn2.ReadOnly = true;
            // 
            // dataGridViewDoubleInputColumn3
            // 
            this.dataGridViewDoubleInputColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            // 
            // 
            // 
            this.dataGridViewDoubleInputColumn3.BackgroundStyle.Class = "DataGridViewNumericBorder";
            this.dataGridViewDoubleInputColumn3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dataGridViewDoubleInputColumn3.DataPropertyName = "ColSaleRate";
            this.dataGridViewDoubleInputColumn3.HeaderText = "Sale Rate";
            this.dataGridViewDoubleInputColumn3.Increment = 1;
            this.dataGridViewDoubleInputColumn3.Name = "dataGridViewDoubleInputColumn3";
            this.dataGridViewDoubleInputColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // comboBoxEx1
            // 
            this.comboBoxEx1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxEx1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxEx1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.comboBoxEx1.DropDownHeight = 75;
            this.comboBoxEx1.DropDownWidth = 198;
            this.comboBoxEx1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBoxEx1.FormattingEnabled = true;
            this.comboBoxEx1.IntegralHeight = false;
            this.comboBoxEx1.ItemHeight = 14;
            this.comboBoxEx1.Location = new System.Drawing.Point(119, 53);
            this.comboBoxEx1.Name = "comboBoxEx1";
            this.comboBoxEx1.Size = new System.Drawing.Size(242, 20);
            this.comboBoxEx1.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(24, 55);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 13);
            this.label16.TabIndex = 239;
            this.label16.Text = "Pricing Scheme";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(549, 57);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 13);
            this.label17.TabIndex = 237;
            this.label17.Text = "Sale Rate";
            this.label17.Visible = false;
            // 
            // comboBoxEx2
            // 
            this.comboBoxEx2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxEx2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxEx2.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.comboBoxEx2.DropDownHeight = 75;
            this.comboBoxEx2.DropDownWidth = 198;
            this.comboBoxEx2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBoxEx2.FormattingEnabled = true;
            this.comboBoxEx2.IntegralHeight = false;
            this.comboBoxEx2.ItemHeight = 14;
            this.comboBoxEx2.Location = new System.Drawing.Point(119, 27);
            this.comboBoxEx2.Name = "comboBoxEx2";
            this.comboBoxEx2.Size = new System.Drawing.Size(242, 20);
            this.comboBoxEx2.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Location = new System.Drawing.Point(24, 31);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(81, 13);
            this.label18.TabIndex = 236;
            this.label18.Text = "Costing Method";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(6, 9);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(46, 13);
            this.label19.TabIndex = 95;
            this.label19.Text = "Details";
            // 
            // shapeContainer4
            // 
            this.shapeContainer4.Location = new System.Drawing.Point(1, 1);
            this.shapeContainer4.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer4.Name = "shapeContainer4";
            this.shapeContainer4.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape5,
            this.lineShape6});
            this.shapeContainer4.Size = new System.Drawing.Size(904, 316);
            this.shapeContainer4.TabIndex = 0;
            this.shapeContainer4.TabStop = false;
            // 
            // lineShape5
            // 
            this.lineShape5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lineShape5.Name = "lineShape4";
            this.lineShape5.X1 = 498;
            this.lineShape5.X2 = 887;
            this.lineShape5.Y1 = 16;
            this.lineShape5.Y2 = 16;
            // 
            // lineShape6
            // 
            this.lineShape6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lineShape6.Name = "lineShape3";
            this.lineShape6.X1 = 14;
            this.lineShape6.X2 = 403;
            this.lineShape6.Y1 = 16;
            this.lineShape6.Y2 = 16;
            // 
            // decimalDotNetBarTextBox4
            // 
            // 
            // 
            // 
            this.decimalDotNetBarTextBox4.Border.Class = "TextBoxBorder";
            this.decimalDotNetBarTextBox4.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.decimalDotNetBarTextBox4.Location = new System.Drawing.Point(794, 29);
            this.decimalDotNetBarTextBox4.MaxLength = 12;
            this.decimalDotNetBarTextBox4.Name = "decimalDotNetBarTextBox4";
            this.decimalDotNetBarTextBox4.ShortcutsEnabled = false;
            this.decimalDotNetBarTextBox4.Size = new System.Drawing.Size(100, 20);
            this.decimalDotNetBarTextBox4.TabIndex = 6;
            this.decimalDotNetBarTextBox4.Text = "0";
            this.decimalDotNetBarTextBox4.Visible = false;
            // 
            // decimalDotNetBarTextBox5
            // 
            // 
            // 
            // 
            this.decimalDotNetBarTextBox5.Border.Class = "TextBoxBorder";
            this.decimalDotNetBarTextBox5.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.decimalDotNetBarTextBox5.Location = new System.Drawing.Point(623, 53);
            this.decimalDotNetBarTextBox5.MaxLength = 12;
            this.decimalDotNetBarTextBox5.Name = "decimalDotNetBarTextBox5";
            this.decimalDotNetBarTextBox5.ShortcutsEnabled = false;
            this.decimalDotNetBarTextBox5.Size = new System.Drawing.Size(106, 20);
            this.decimalDotNetBarTextBox5.TabIndex = 7;
            this.decimalDotNetBarTextBox5.Text = "0";
            this.decimalDotNetBarTextBox5.Visible = false;
            // 
            // tabControlPanel10
            // 
            this.tabControlPanel10.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel10.Controls.Add(this.checkBox1);
            this.tabControlPanel10.Controls.Add(this.label20);
            this.tabControlPanel10.Controls.Add(this.comboBoxEx3);
            this.tabControlPanel10.Controls.Add(this.comboBoxEx4);
            this.tabControlPanel10.Controls.Add(this.label21);
            this.tabControlPanel10.Controls.Add(this.label22);
            this.tabControlPanel10.Controls.Add(this.label23);
            this.tabControlPanel10.Controls.Add(this.shapeContainer5);
            this.tabControlPanel10.Controls.Add(this.clsInnerGridBar3);
            this.tabControlPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel10.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel10.Name = "tabControlPanel10";
            this.tabControlPanel10.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel10.Size = new System.Drawing.Size(906, 318);
            this.tabControlPanel10.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.tabControlPanel10.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel10.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel10.Style.GradientAngle = 90;
            this.tabControlPanel10.TabIndex = 0;
            this.tabControlPanel10.ThemeAware = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(82, 50);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 113;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(392, 19);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(80, 13);
            this.label20.TabIndex = 112;
            this.label20.Text = "Default UOM";
            // 
            // comboBoxEx3
            // 
            this.comboBoxEx3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxEx3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxEx3.DisplayMember = "Text";
            this.comboBoxEx3.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxEx3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBoxEx3.FormattingEnabled = true;
            this.comboBoxEx3.ItemHeight = 14;
            this.comboBoxEx3.Location = new System.Drawing.Point(452, 73);
            this.comboBoxEx3.Name = "comboBoxEx3";
            this.comboBoxEx3.Size = new System.Drawing.Size(125, 20);
            this.comboBoxEx3.TabIndex = 109;
            // 
            // comboBoxEx4
            // 
            this.comboBoxEx4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxEx4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxEx4.DisplayMember = "Text";
            this.comboBoxEx4.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxEx4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBoxEx4.FormattingEnabled = true;
            this.comboBoxEx4.ItemHeight = 14;
            this.comboBoxEx4.Location = new System.Drawing.Point(452, 47);
            this.comboBoxEx4.Name = "comboBoxEx4";
            this.comboBoxEx4.Size = new System.Drawing.Size(125, 20);
            this.comboBoxEx4.TabIndex = 108;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(394, 50);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 13);
            this.label21.TabIndex = 110;
            this.label21.Text = "Purchase";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Location = new System.Drawing.Point(394, 76);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(33, 13);
            this.label22.TabIndex = 111;
            this.label22.Text = "Sales";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(24, 18);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(130, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Units of Measurement";
            // 
            // shapeContainer5
            // 
            this.shapeContainer5.Location = new System.Drawing.Point(1, 1);
            this.shapeContainer5.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer5.Name = "shapeContainer5";
            this.shapeContainer5.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape7});
            this.shapeContainer5.Size = new System.Drawing.Size(904, 316);
            this.shapeContainer5.TabIndex = 107;
            this.shapeContainer5.TabStop = false;
            // 
            // lineShape7
            // 
            this.lineShape7.Name = "lineShape2";
            this.lineShape7.X1 = 74;
            this.lineShape7.X2 = 844;
            this.lineShape7.Y1 = 24;
            this.lineShape7.Y2 = 24;
            // 
            // clsInnerGridBar3
            // 
            this.clsInnerGridBar3.AddNewRow = false;
            this.clsInnerGridBar3.AllowDrop = true;
            this.clsInnerGridBar3.AllowUserToAddRows = false;
            this.clsInnerGridBar3.AllowUserToOrderColumns = true;
            this.clsInnerGridBar3.AllowUserToResizeColumns = false;
            this.clsInnerGridBar3.AllowUserToResizeRows = false;
            this.clsInnerGridBar3.AlphaNumericCols = new int[0];
            this.clsInnerGridBar3.BackgroundColor = System.Drawing.Color.White;
            this.clsInnerGridBar3.CapsLockCols = new int[0];
            this.clsInnerGridBar3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.clsInnerGridBar3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn17});
            this.clsInnerGridBar3.DecimalCols = new int[] {
        3};
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.clsInnerGridBar3.DefaultCellStyle = dataGridViewCellStyle14;
            this.clsInnerGridBar3.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.clsInnerGridBar3.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.clsInnerGridBar3.HasSlNo = false;
            this.clsInnerGridBar3.LastRowIndex = 0;
            this.clsInnerGridBar3.Location = new System.Drawing.Point(25, 44);
            this.clsInnerGridBar3.Name = "clsInnerGridBar3";
            this.clsInnerGridBar3.NegativeValueCols = new int[0];
            this.clsInnerGridBar3.NumericCols = new int[0];
            this.clsInnerGridBar3.RowHeadersWidth = 50;
            this.clsInnerGridBar3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.clsInnerGridBar3.Size = new System.Drawing.Size(326, 231);
            this.clsInnerGridBar3.TabIndex = 8;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 28;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn17.HeaderText = "UOM";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabControlPanel11
            // 
            this.tabControlPanel11.Controls.Add(this.groupBox3);
            this.tabControlPanel11.Controls.Add(this.groupBox4);
            this.tabControlPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel11.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel11.Name = "tabControlPanel11";
            this.tabControlPanel11.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel11.Size = new System.Drawing.Size(906, 318);
            this.tabControlPanel11.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.tabControlPanel11.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel11.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel11.Style.GradientAngle = 90;
            this.tabControlPanel11.TabIndex = 7;
            this.tabControlPanel11.ThemeAware = true;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.buttonX2);
            this.groupBox3.Controls.Add(this.clsDataGirdViewX1);
            this.groupBox3.Location = new System.Drawing.Point(423, 16);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(471, 261);
            this.groupBox3.TabIndex = 251;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Features";
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(9, 18);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(87, 23);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.TabIndex = 249;
            this.buttonX2.Text = "Add Feature";
            // 
            // clsDataGirdViewX1
            // 
            this.clsDataGirdViewX1.AddNewRow = false;
            this.clsDataGirdViewX1.AllowUserToAddRows = false;
            this.clsDataGirdViewX1.AllowUserToOrderColumns = true;
            this.clsDataGirdViewX1.AllowUserToResizeRows = false;
            this.clsDataGirdViewX1.AlphaNumericCols = new int[0];
            this.clsDataGirdViewX1.BackgroundColor = System.Drawing.Color.White;
            this.clsDataGirdViewX1.CapsLockCols = new int[0];
            this.clsDataGirdViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.clsDataGirdViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19});
            this.clsDataGirdViewX1.DecimalCols = new int[0];
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.clsDataGirdViewX1.DefaultCellStyle = dataGridViewCellStyle15;
            this.clsDataGirdViewX1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.clsDataGirdViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.clsDataGirdViewX1.HasSlNo = false;
            this.clsDataGirdViewX1.LastRowIndex = 0;
            this.clsDataGirdViewX1.Location = new System.Drawing.Point(9, 47);
            this.clsDataGirdViewX1.Name = "clsDataGirdViewX1";
            this.clsDataGirdViewX1.NegativeValueCols = new int[0];
            this.clsDataGirdViewX1.NumericCols = new int[0];
            this.clsDataGirdViewX1.RowHeadersWidth = 50;
            this.clsDataGirdViewX1.Size = new System.Drawing.Size(456, 211);
            this.clsDataGirdViewX1.TabIndex = 248;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "Feature";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn19.HeaderText = "Value(s)";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.clsDataGirdViewX2);
            this.groupBox4.Location = new System.Drawing.Point(9, 14);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(395, 266);
            this.groupBox4.TabIndex = 250;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Dimensions";
            // 
            // clsDataGirdViewX2
            // 
            this.clsDataGirdViewX2.AddNewRow = false;
            this.clsDataGirdViewX2.AlphaNumericCols = new int[0];
            this.clsDataGirdViewX2.BackgroundColor = System.Drawing.Color.White;
            this.clsDataGirdViewX2.CapsLockCols = new int[0];
            this.clsDataGirdViewX2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.clsDataGirdViewX2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewComboBoxColumn7,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewComboBoxColumn8});
            this.clsDataGirdViewX2.DecimalCols = new int[0];
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.clsDataGirdViewX2.DefaultCellStyle = dataGridViewCellStyle16;
            this.clsDataGirdViewX2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.clsDataGirdViewX2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.clsDataGirdViewX2.HasSlNo = false;
            this.clsDataGirdViewX2.LastRowIndex = 0;
            this.clsDataGirdViewX2.Location = new System.Drawing.Point(9, 20);
            this.clsDataGirdViewX2.Name = "clsDataGirdViewX2";
            this.clsDataGirdViewX2.NegativeValueCols = new int[0];
            this.clsDataGirdViewX2.NumericCols = new int[0];
            this.clsDataGirdViewX2.RowHeadersWidth = 50;
            this.clsDataGirdViewX2.Size = new System.Drawing.Size(380, 242);
            this.clsDataGirdViewX2.TabIndex = 243;
            // 
            // dataGridViewComboBoxColumn7
            // 
            this.dataGridViewComboBoxColumn7.DataPropertyName = "DimensionID";
            this.dataGridViewComboBoxColumn7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn7.HeaderText = "Dimension";
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            this.dataGridViewComboBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn7.Width = 150;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn20.DataPropertyName = "DimensionValue";
            this.dataGridViewTextBoxColumn20.HeaderText = "Value";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            // 
            // dataGridViewComboBoxColumn8
            // 
            this.dataGridViewComboBoxColumn8.DataPropertyName = "UOMID";
            this.dataGridViewComboBoxColumn8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn8.HeaderText = "UOM";
            this.dataGridViewComboBoxColumn8.Name = "dataGridViewComboBoxColumn8";
            this.dataGridViewComboBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn8.Width = 80;
            // 
            // tabControlPanel12
            // 
            this.tabControlPanel12.CanvasColor = System.Drawing.SystemColors.Control;
            this.tabControlPanel12.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel12.Controls.Add(this.label24);
            this.tabControlPanel12.Controls.Add(this.buttonX3);
            this.tabControlPanel12.Controls.Add(this.buttonX4);
            this.tabControlPanel12.Controls.Add(this.buttonX5);
            this.tabControlPanel12.Controls.Add(this.buttonX6);
            this.tabControlPanel12.Controls.Add(this.buttonX7);
            this.tabControlPanel12.Controls.Add(this.buttonX8);
            this.tabControlPanel12.Controls.Add(this.label25);
            this.tabControlPanel12.Controls.Add(this.buttonX9);
            this.tabControlPanel12.Controls.Add(this.pictureBox1);
            this.tabControlPanel12.Controls.Add(this.textBoxX1);
            this.tabControlPanel12.Controls.Add(this.label26);
            this.tabControlPanel12.Controls.Add(this.label27);
            this.tabControlPanel12.Controls.Add(this.pictureBox2);
            this.tabControlPanel12.Controls.Add(this.label28);
            this.tabControlPanel12.Controls.Add(this.textBoxX2);
            this.tabControlPanel12.Controls.Add(this.textBoxX3);
            this.tabControlPanel12.Controls.Add(this.comboBoxEx5);
            this.tabControlPanel12.Controls.Add(this.buttonX10);
            this.tabControlPanel12.Controls.Add(this.comboBoxEx6);
            this.tabControlPanel12.Controls.Add(this.buttonX11);
            this.tabControlPanel12.Controls.Add(this.comboBoxEx7);
            this.tabControlPanel12.Controls.Add(this.buttonX12);
            this.tabControlPanel12.Controls.Add(this.textBoxX4);
            this.tabControlPanel12.Controls.Add(this.label29);
            this.tabControlPanel12.Controls.Add(this.label30);
            this.tabControlPanel12.Controls.Add(this.label31);
            this.tabControlPanel12.Controls.Add(this.label32);
            this.tabControlPanel12.Controls.Add(this.label33);
            this.tabControlPanel12.Controls.Add(this.label34);
            this.tabControlPanel12.Controls.Add(this.label35);
            this.tabControlPanel12.Controls.Add(this.shapeContainer6);
            this.tabControlPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel12.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel12.Name = "tabControlPanel12";
            this.tabControlPanel12.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel12.Size = new System.Drawing.Size(906, 318);
            this.tabControlPanel12.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.tabControlPanel12.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel12.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel12.Style.GradientAngle = 90;
            this.tabControlPanel12.TabIndex = 0;
            this.tabControlPanel12.ThemeAware = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(699, 148);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(34, 13);
            this.label24.TabIndex = 240;
            this.label24.Text = "0 of 0";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(758, 145);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Size = new System.Drawing.Size(33, 20);
            this.buttonX3.TabIndex = 239;
            this.buttonX3.TabStop = false;
            this.buttonX3.Text = "Next";
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Location = new System.Drawing.Point(647, 145);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Size = new System.Drawing.Size(33, 20);
            this.buttonX4.TabIndex = 238;
            this.buttonX4.TabStop = false;
            this.buttonX4.Text = "Prev";
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX5.Location = new System.Drawing.Point(486, 72);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Size = new System.Drawing.Size(60, 23);
            this.buttonX5.TabIndex = 13;
            this.buttonX5.TabStop = false;
            this.buttonX5.Text = "Attach";
            // 
            // buttonX6
            // 
            this.buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX6.ContextMenuStrip = this.mnuAddImage;
            this.buttonX6.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.buttonX6.Location = new System.Drawing.Point(547, 72);
            this.buttonX6.Name = "buttonX6";
            this.buttonX6.Size = new System.Drawing.Size(20, 23);
            this.buttonX6.TabIndex = 14;
            this.buttonX6.Text = "6";
            // 
            // buttonX7
            // 
            this.buttonX7.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX7.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX7.Location = new System.Drawing.Point(486, 205);
            this.buttonX7.Name = "buttonX7";
            this.buttonX7.Size = new System.Drawing.Size(60, 23);
            this.buttonX7.TabIndex = 15;
            this.buttonX7.TabStop = false;
            this.buttonX7.Text = "Create";
            // 
            // buttonX8
            // 
            this.buttonX8.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX8.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX8.ContextMenuStrip = this.mnuBarcodeAddImage;
            this.buttonX8.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.buttonX8.Location = new System.Drawing.Point(547, 205);
            this.buttonX8.Name = "buttonX8";
            this.buttonX8.Size = new System.Drawing.Size(20, 23);
            this.buttonX8.TabIndex = 16;
            this.buttonX8.Text = "6";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(483, 50);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(59, 13);
            this.label25.TabIndex = 237;
            this.label25.Text = "Item Image";
            // 
            // buttonX9
            // 
            this.buttonX9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX9.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX9.Location = new System.Drawing.Point(849, 167);
            this.buttonX9.Name = "buttonX9";
            this.buttonX9.Size = new System.Drawing.Size(41, 22);
            this.buttonX9.TabIndex = 28;
            this.buttonX9.TabStop = false;
            this.buttonX9.Text = "Create";
            this.buttonX9.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(579, 167);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(267, 113);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 229;
            this.pictureBox1.TabStop = false;
            // 
            // textBoxX1
            // 
            // 
            // 
            // 
            this.textBoxX1.Border.Class = "TextBoxBorder";
            this.textBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX1.Location = new System.Drawing.Point(119, 127);
            this.textBoxX1.MaxLength = 1999;
            this.textBoxX1.Multiline = true;
            this.textBoxX1.Name = "textBoxX1";
            this.textBoxX1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxX1.Size = new System.Drawing.Size(288, 153);
            this.textBoxX1.TabIndex = 12;
            this.textBoxX1.Text = "\r\n\r\n\r\n";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(537, 5);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(40, 13);
            this.label26.TabIndex = 227;
            this.label26.Text = "Photo";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Location = new System.Drawing.Point(24, 184);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(30, 13);
            this.label27.TabIndex = 226;
            this.label27.Text = "Note";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(579, 23);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(267, 120);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 225;
            this.pictureBox2.TabStop = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Location = new System.Drawing.Point(203, 105);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(90, 13);
            this.label28.TabIndex = 223;
            this.label28.Text = "Guarantee Period";
            // 
            // textBoxX2
            // 
            // 
            // 
            // 
            this.textBoxX2.Border.Class = "TextBoxBorder";
            this.textBoxX2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX2.Location = new System.Drawing.Point(296, 101);
            this.textBoxX2.MaxLength = 5;
            this.textBoxX2.Name = "textBoxX2";
            this.textBoxX2.Size = new System.Drawing.Size(65, 20);
            this.textBoxX2.TabIndex = 9;
            // 
            // textBoxX3
            // 
            // 
            // 
            // 
            this.textBoxX3.Border.Class = "TextBoxBorder";
            this.textBoxX3.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX3.Location = new System.Drawing.Point(119, 101);
            this.textBoxX3.MaxLength = 5;
            this.textBoxX3.Name = "textBoxX3";
            this.textBoxX3.Size = new System.Drawing.Size(65, 20);
            this.textBoxX3.TabIndex = 8;
            // 
            // comboBoxEx5
            // 
            this.comboBoxEx5.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxEx5.DropDownHeight = 75;
            this.comboBoxEx5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBoxEx5.FormattingEnabled = true;
            this.comboBoxEx5.IntegralHeight = false;
            this.comboBoxEx5.ItemHeight = 14;
            this.comboBoxEx5.Location = new System.Drawing.Point(498, 260);
            this.comboBoxEx5.Name = "comboBoxEx5";
            this.comboBoxEx5.Size = new System.Drawing.Size(38, 20);
            this.comboBoxEx5.TabIndex = 17;
            this.comboBoxEx5.Visible = false;
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX10.Location = new System.Drawing.Point(538, 260);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Size = new System.Drawing.Size(29, 20);
            this.buttonX10.TabIndex = 18;
            this.buttonX10.Text = "...";
            this.buttonX10.Visible = false;
            // 
            // comboBoxEx6
            // 
            this.comboBoxEx6.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxEx6.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxEx6.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxEx6.DropDownHeight = 75;
            this.comboBoxEx6.DropDownWidth = 198;
            this.comboBoxEx6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBoxEx6.FormattingEnabled = true;
            this.comboBoxEx6.IntegralHeight = false;
            this.comboBoxEx6.ItemHeight = 14;
            this.comboBoxEx6.Location = new System.Drawing.Point(119, 75);
            this.comboBoxEx6.Name = "comboBoxEx6";
            this.comboBoxEx6.Size = new System.Drawing.Size(242, 20);
            this.comboBoxEx6.TabIndex = 6;
            // 
            // buttonX11
            // 
            this.buttonX11.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX11.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX11.Location = new System.Drawing.Point(367, 75);
            this.buttonX11.Name = "buttonX11";
            this.buttonX11.Size = new System.Drawing.Size(40, 20);
            this.buttonX11.TabIndex = 7;
            this.buttonX11.Text = "...";
            // 
            // comboBoxEx7
            // 
            this.comboBoxEx7.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxEx7.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxEx7.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxEx7.DropDownHeight = 75;
            this.comboBoxEx7.DropDownWidth = 198;
            this.comboBoxEx7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBoxEx7.FormattingEnabled = true;
            this.comboBoxEx7.IntegralHeight = false;
            this.comboBoxEx7.ItemHeight = 14;
            this.comboBoxEx7.Location = new System.Drawing.Point(119, 49);
            this.comboBoxEx7.Name = "comboBoxEx7";
            this.comboBoxEx7.Size = new System.Drawing.Size(242, 20);
            this.comboBoxEx7.TabIndex = 4;
            // 
            // buttonX12
            // 
            this.buttonX12.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX12.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX12.Location = new System.Drawing.Point(367, 50);
            this.buttonX12.Name = "buttonX12";
            this.buttonX12.Size = new System.Drawing.Size(40, 20);
            this.buttonX12.TabIndex = 5;
            this.buttonX12.Text = "...";
            // 
            // textBoxX4
            // 
            // 
            // 
            // 
            this.textBoxX4.Border.Class = "TextBoxBorder";
            this.textBoxX4.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX4.Location = new System.Drawing.Point(119, 23);
            this.textBoxX4.MaxLength = 20;
            this.textBoxX4.Name = "textBoxX4";
            this.textBoxX4.Size = new System.Drawing.Size(242, 20);
            this.textBoxX4.TabIndex = 3;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(11, 5);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(77, 13);
            this.label29.TabIndex = 94;
            this.label29.Text = "General Info";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Location = new System.Drawing.Point(30, 103);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(83, 13);
            this.label30.TabIndex = 88;
            this.label30.Text = "Warranty Period";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Location = new System.Drawing.Point(28, 79);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(46, 13);
            this.label31.TabIndex = 91;
            this.label31.Text = "Made-In";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Location = new System.Drawing.Point(28, 27);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(36, 13);
            this.label32.TabIndex = 90;
            this.label32.Text = "Model";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Location = new System.Drawing.Point(28, 53);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(70, 13);
            this.label33.TabIndex = 89;
            this.label33.Text = "Manufacturer";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Location = new System.Drawing.Point(448, 267);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 13);
            this.label34.TabIndex = 86;
            this.label34.Text = "Tax Code";
            this.label34.Visible = false;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(483, 184);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(79, 13);
            this.label35.TabIndex = 87;
            this.label35.Text = "Barcode Image";
            // 
            // shapeContainer6
            // 
            this.shapeContainer6.Location = new System.Drawing.Point(1, 1);
            this.shapeContainer6.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer6.Name = "shapeContainer6";
            this.shapeContainer6.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape8});
            this.shapeContainer6.Size = new System.Drawing.Size(904, 316);
            this.shapeContainer6.TabIndex = 15;
            this.shapeContainer6.TabStop = false;
            // 
            // lineShape8
            // 
            this.lineShape8.Name = "lineShape1";
            this.lineShape8.X1 = 73;
            this.lineShape8.X2 = 898;
            this.lineShape8.Y1 = 12;
            this.lineShape8.Y2 = 12;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ItemID";
            this.dataGridViewTextBoxColumn1.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "RowNumber";
            this.dataGridViewTextBoxColumn2.HeaderText = "RowNumber";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Code";
            this.dataGridViewTextBoxColumn3.HeaderText = "Code";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn4.HeaderText = "Name";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Quantity";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewTextBoxColumn5.HeaderText = "Conversion Value";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Quantity";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewTextBoxColumn6.HeaderText = "OrderID";
            this.dataGridViewTextBoxColumn6.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Dimension";
            this.dataGridViewTextBoxColumn7.HeaderText = "Reorder Point";
            this.dataGridViewTextBoxColumn7.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Quantity";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn8.HeaderText = "Reorder Quantity";
            this.dataGridViewTextBoxColumn8.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "DimensionValue";
            this.dataGridViewTextBoxColumn9.HeaderText = "StorageOrderID";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // frmItemMaster
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.ClientSize = new System.Drawing.Size(1184, 541);
            this.Controls.Add(this.panelEx1);
            this.Controls.Add(this.dockSite2);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.expandableSplitterLeft);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.dockSite3);
            this.Controls.Add(this.dockSite4);
            this.Controls.Add(this.dockSite5);
            this.Controls.Add(this.dockSite6);
            this.Controls.Add(this.dockSite8);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmItemMaster";
            this.Text = "Product";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmItemMaster_Load);
            this.Shown += new System.EventHandler(this.frmItemMaster1_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmItemMaster_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmItemMaster1_KeyDown);
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.panelEx1.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabItemMaster)).EndInit();
            this.tabItemMaster.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            this.tabControlPanel1.PerformLayout();
            this.mnuAddImage.ResumeLayout(false);
            this.mnuBarcodeAddImage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbBarCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemPhoto)).EndInit();
            this.tabControlPanel6.ResumeLayout(false);
            this.tabControlPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBatchwiseCosting)).EndInit();
            this.tabControlPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemLocations)).EndInit();
            this.panelEx2.ResumeLayout(false);
            this.panelEx2.PerformLayout();
            this.tabControlPanel7.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFeatures)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDimensions)).EndInit();
            this.tabControlPanel3.ResumeLayout(false);
            this.tabControlPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUnitsOfMeasurements)).EndInit();
            this.tabControlPanel5.ResumeLayout(false);
            this.tabControlPanel13.ResumeLayout(false);
            this.tabControlPanel13.PerformLayout();
            this.gbSaleRateHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRateHistory)).EndInit();
            this.panelGridBottom.ResumeLayout(false);
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemMasterBindingNavigator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorItemMaster)).EndInit();
            this.tabControlPanel4.ResumeLayout(false);
            this.tabControlPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.clsInnerGridBar1)).EndInit();
            this.panelEx3.ResumeLayout(false);
            this.panelEx3.PerformLayout();
            this.tabControlPanel9.ResumeLayout(false);
            this.tabControlPanel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clsInnerGridBar2)).EndInit();
            this.tabControlPanel10.ResumeLayout(false);
            this.tabControlPanel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clsInnerGridBar3)).EndInit();
            this.tabControlPanel11.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.clsDataGirdViewX1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.clsDataGirdViewX2)).EndInit();
            this.tabControlPanel12.ResumeLayout(false);
            this.tabControlPanel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LblTVendor;
        private System.Windows.Forms.Label LblTVendorAddress;
        private System.Windows.Forms.Label LblTCompany;
        private System.Windows.Forms.Label LblSCategory;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.ButtonX btnSearch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSearchSubCategory;
        private ClsDataGirdViewX dgvItems;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSearchCategory;
        private DevComponents.DotNetBar.ButtonItem bnUom;
        private DevComponents.DotNetBar.ButtonX btnBaseUom;
        private DevComponents.DotNetBar.LabelX lblCountStatus;
        private DevComponents.DotNetBar.ButtonX btnCategory;
        private DevComponents.DotNetBar.ButtonX btnSubCategory;
        private System.Windows.Forms.ErrorProvider errorItemMaster;
        private ClsInnerGridBar dgvUnitsOfMeasurements;
        private System.Windows.Forms.Label LblUnitOfMeasurements;
        private DevComponents.DotNetBar.LabelX lblItemMasterStatus;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSearchCode;
        private DevComponents.DotNetBar.Controls.TextBoxX txtShortName;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDescription;
        private DevComponents.DotNetBar.Controls.TextBoxX txtItemCode;
        private DevComponents.DotNetBar.Controls.TextBoxX txtNote;

        #region DotNet Bar Controls
        public bool DocumentChanged = false;
        public string FileName = "";
        public DevComponents.DotNetBar.Command CommandZoom;
        private DevComponents.DotNetBar.DockContainerItem dockContainerItem1;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterLeft;
        private DevComponents.DotNetBar.PanelEx PanelLeft;
        //private IContainer components; 

        private DevComponents.DotNetBar.DotNetBarManager dotNetBarManager1;
        private DevComponents.DotNetBar.DockSite dockSite4;
        private DevComponents.DotNetBar.DockSite dockSite1;
        private DevComponents.DotNetBar.DockSite dockSite2;
        private DevComponents.DotNetBar.DockSite dockSite3;
        private DevComponents.DotNetBar.DockSite dockSite5;
        private DevComponents.DotNetBar.DockSite dockSite6;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.DockSite dockSite8;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterTop;
        private DevComponents.DotNetBar.PanelEx panelTop;
        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.PanelEx pnlBottom;
        private DevComponents.DotNetBar.PanelEx panelGridBottom;
        private DevComponents.DotNetBar.PanelEx panelLeftTop;
        private System.Windows.Forms.Label LblSSubCategory;
        private System.Windows.Forms.Label LblSItemName;
        private ClsInnerGridBar dgvItemLocations;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSearchDescription;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboBaseUom;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCategory;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSubCategory;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboStatus;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label LblCategory;
        private System.Windows.Forms.Label LblItemType;
        private DevComponents.DotNetBar.TabControl tabItemMaster;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private DevComponents.DotNetBar.TabItem tbpLocation;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private DevComponents.DotNetBar.TabItem tbpGeneral;
        private DevComponents.DotNetBar.Controls.TextBoxX txtModel;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboTaxCode;
        private DevComponents.DotNetBar.ButtonX btnTaxCode;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboMadeIn;
        private DevComponents.DotNetBar.ButtonX btnMadeIn;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboManufacturer;
        private DevComponents.DotNetBar.ButtonX btnManufacturer;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label LblWarrantyPeriod;
        private System.Windows.Forms.Label LblMadeIn;
        private System.Windows.Forms.Label LblModel;
        private System.Windows.Forms.Label LblManufacturer;
        private System.Windows.Forms.Label LblTaxCode;
        private System.Windows.Forms.Label LblBarcode;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGuaranteePeriod;
        private DevComponents.DotNetBar.Controls.TextBoxX txtWarrantyPeriod;
        private System.Windows.Forms.Label LblGuaranteePeriod;
        private System.Windows.Forms.PictureBox pbItemPhoto;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label LblNote;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel3;
        private DevComponents.DotNetBar.TabItem tbpMeasurements;
        #endregion

        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private DevComponents.DotNetBar.ButtonX btnCreateBarCode;
        private System.Windows.Forms.PictureBox pbBarCode;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel5;
        private DevComponents.DotNetBar.TabItem tbpBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.WebBrowser webBatchDetails;
        private System.Windows.Forms.Timer tmItemMaster;
        private System.Windows.Forms.ContextMenuStrip mnuAddImage;
        private System.Windows.Forms.ToolStripMenuItem mnuAdd;
        private System.Windows.Forms.ToolStripMenuItem mnuRemove;
        private System.Windows.Forms.ContextMenuStrip mnuBarcodeAddImage;
        private System.Windows.Forms.ToolStripMenuItem mnuCreateBarcode;
        private System.Windows.Forms.ToolStripMenuItem mnuRemoveBarcode;
        private System.Windows.Forms.Label label4;
        private DevComponents.DotNetBar.ButtonX btnBarcodeCreate;
        private DevComponents.DotNetBar.ButtonX btnAddBarcodeImage;
        private DevComponents.DotNetBar.ButtonX BtnAttach;
        private DevComponents.DotNetBar.ButtonX btnAddImage;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDescriptionArabic;
        private System.Windows.Forms.Label label5;
        private DevComponents.DotNetBar.ButtonItem btnUomConversion;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDefaultSalesUom;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDefaultPurchaseUom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private DevComponents.DotNetBar.PanelEx panelEx2;
        private DemoClsDataGridview.DecimalDotNetBarTextBox txtReorderPoint;
        private System.Windows.Forms.Label lblReorderpoint;
        private DemoClsDataGridview.DecimalDotNetBarTextBox txtReorderQuantity;
        private System.Windows.Forms.Label lblReorderQuantity;
        private System.Windows.Forms.Label label8;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel6;
        private System.Windows.Forms.Label label9;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private DevComponents.DotNetBar.TabItem tbpCosting;
        private System.Windows.Forms.Label lblSaleRate;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCostingMethod;
        private System.Windows.Forms.Label LblCostingMethod;
        private DemoClsDataGridview.DecimalDotNetBarTextBox txtSaleRate;
        private ClsInnerGridBar dgvBatchwiseCosting;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboPricingScheme;
        private System.Windows.Forms.Label lblPricingScheme;
        private DevComponents.DotNetBar.ButtonX BtnShow;
        private DemoClsDataGridview.DecimalDotNetBarTextBox txtPurchaseRate;
        private DevComponents.DotNetBar.ButtonItem BtnPrint;
        private DevComponents.DotNetBar.ButtonItem btnPricingScheme;
        private System.Windows.Forms.Label lblSItemCode;
        private System.Windows.Forms.RadioButton rdbPurchaseRate;
        private System.Windows.Forms.RadioButton rdbActualRate;
        private System.Windows.Forms.CheckBox chkExpiryDateMandatory;
        private DemoClsDataGridview.DecimalDotNetBarTextBox txtActualRate;
        private System.Windows.Forms.Label label13;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private System.Windows.Forms.LinkLabel lnkLabelAdvanceSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColRowNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemName;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel7;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevComponents.DotNetBar.ButtonX btnAddFeature;
        private ClsDataGirdViewX dgvFeatures;
        private System.Windows.Forms.GroupBox groupBox1;
        private ClsDataGirdViewX dgvDimensions;
        private DevComponents.DotNetBar.TabItem tabDimensionAndFeatures;
        private DevComponents.DotNetBar.TabItem tabComponents;
        private DevComponents.DotNetBar.TabItem tabProductionStage;
        private System.Windows.Forms.Label lblImageText;
        private DevComponents.DotNetBar.ButtonX btnNext;
        private DevComponents.DotNetBar.ButtonX btnPrev;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColWarehouse;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColLocation;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColRow;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColBlock;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColLot;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColReorderPoint;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColReorderQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColSerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewComboBoxColumn clmCboDimension;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDimensionValue;
        private System.Windows.Forms.DataGridViewComboBoxColumn clmCboDimensionUOM;
        private System.Windows.Forms.CheckBox chkUOMSelectAll;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dgvClmChkSelect;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColUomType;
        private System.Windows.Forms.ToolStripMenuItem mnuPrintBarcode;
        private System.Windows.Forms.Label lblCutOffPrice;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel13;
        private DevComponents.DotNetBar.TabItem tpbSaleRate;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel4;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel8;
        private ClsInnerGridBar clsInnerGridBar1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private DevComponents.DotNetBar.PanelEx panelEx3;
        private DemoClsDataGridview.DecimalDotNetBarTextBox decimalDotNetBarTextBox1;
        private System.Windows.Forms.Label label3;
        private DemoClsDataGridview.DecimalDotNetBarTextBox decimalDotNetBarTextBox2;
        private System.Windows.Forms.Label label14;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel9;
        private DemoClsDataGridview.DecimalDotNetBarTextBox decimalDotNetBarTextBox3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private ClsInnerGridBar clsInnerGridBar2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn dataGridViewDoubleInputColumn1;
        private DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn dataGridViewDoubleInputColumn2;
        private DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn dataGridViewDoubleInputColumn3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx comboBoxEx1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private DevComponents.DotNetBar.Controls.ComboBoxEx comboBoxEx2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape5;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape6;
        private DemoClsDataGridview.DecimalDotNetBarTextBox decimalDotNetBarTextBox4;
        private DemoClsDataGridview.DecimalDotNetBarTextBox decimalDotNetBarTextBox5;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel10;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label20;
        private DevComponents.DotNetBar.Controls.ComboBoxEx comboBoxEx3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx comboBoxEx4;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer5;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape7;
        private ClsInnerGridBar clsInnerGridBar3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel11;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private ClsDataGirdViewX clsDataGirdViewX1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.GroupBox groupBox4;
        private ClsDataGirdViewX clsDataGirdViewX2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn8;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel12;
        private System.Windows.Forms.Label label24;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.ButtonX buttonX6;
        private DevComponents.DotNetBar.ButtonX buttonX7;
        private DevComponents.DotNetBar.ButtonX buttonX8;
        private System.Windows.Forms.Label label25;
        private DevComponents.DotNetBar.ButtonX buttonX9;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label28;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX2;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx comboBoxEx5;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private DevComponents.DotNetBar.Controls.ComboBoxEx comboBoxEx6;
        private DevComponents.DotNetBar.ButtonX buttonX11;
        private DevComponents.DotNetBar.Controls.ComboBoxEx comboBoxEx7;
        private DevComponents.DotNetBar.ButtonX buttonX12;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX4;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer6;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape8;
        private System.Windows.Forms.Label lblNoSaleRecordsFound;
        private DemoClsDataGridview.DecimalTextBox txtCutOffPrice;
        private System.Windows.Forms.GroupBox gbSaleRateHistory;
        private ClsDataGirdViewX dgvRateHistory;
        private System.Windows.Forms.Label label36;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboProductType;
        private System.Windows.Forms.RadioButton rdbLandingCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColBatchID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColBatchNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColExpiryDate;
        private DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn ColPurchaseRate;
        private DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn ColLandingCost;
        private DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn ColActualRate;
        private DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn ColSaleRate;
        private System.Windows.Forms.Label lblPurchaseRate;
        private DevComponents.DotNetBar.ButtonItem bnAddNewItem;
        private DevComponents.DotNetBar.ButtonItem bnSaveItem;
        private DevComponents.DotNetBar.ButtonItem bnDeleteItem;
        private DevComponents.DotNetBar.ButtonItem bnPrint;
        private DevComponents.DotNetBar.ButtonItem bnEmail;
        private DevComponents.DotNetBar.Bar ItemMasterBindingNavigator;
        private DevComponents.DotNetBar.ButtonItem bnCancel;
    }
}