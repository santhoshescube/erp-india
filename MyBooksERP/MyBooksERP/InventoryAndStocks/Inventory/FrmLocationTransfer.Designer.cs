﻿namespace MyBooksERP
{
    partial class FrmLocationTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLocationTransfer));
            this.ItemMasterBindingNavigator = new DevComponents.DotNetBar.Bar();
            this.bnCancel = new DevComponents.DotNetBar.ButtonItem();
            this.LblWareHouse = new DevComponents.DotNetBar.LabelX();
            this.pnlMain = new DevComponents.DotNetBar.PanelEx();
            this.chkBXDamegedQty = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.btnXClose = new DevComponents.DotNetBar.ButtonX();
            this.btnXShow = new DevComponents.DotNetBar.ButtonX();
            this.CboXCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblXCompany = new DevComponents.DotNetBar.LabelX();
            this.PnlTree = new DevComponents.DotNetBar.PanelEx();
            this.lblXArrow = new DevComponents.DotNetBar.LabelX();
            this.btnExapandAllWH = new DevComponents.DotNetBar.ButtonX();
            this.btnXCollapseWH = new DevComponents.DotNetBar.ButtonX();
            this.btnXExpandALL = new DevComponents.DotNetBar.ButtonX();
            this.btnXCollapse = new DevComponents.DotNetBar.ButtonX();
            this.btnXFilterItem = new DevComponents.DotNetBar.ButtonX();
            this.btnXFilterWarehouse = new DevComponents.DotNetBar.ButtonX();
            this.advTrvTransferLoc = new DevComponents.AdvTree.AdvTree();
            this.ColNodeTransferLoc = new DevComponents.AdvTree.ColumnHeader();
            this.ColCurrentQtyTransferLoc = new DevComponents.AdvTree.ColumnHeader();
            this.ColMovedQtyTransferLoc = new DevComponents.AdvTree.ColumnHeader();
            this.ColMovedStatusTransferLoc = new DevComponents.AdvTree.ColumnHeader();
            this.ColNodeKeyTransferLoc = new DevComponents.AdvTree.ColumnHeader();
            this.nodeConnector2 = new DevComponents.AdvTree.NodeConnector();
            this.elementStyle2 = new DevComponents.DotNetBar.ElementStyle();
            this.advTrvOriginalLoc = new DevComponents.AdvTree.AdvTree();
            this.ColNode = new DevComponents.AdvTree.ColumnHeader();
            this.ColCurrentQty = new DevComponents.AdvTree.ColumnHeader();
            this.ColMovingQty = new DevComponents.AdvTree.ColumnHeader();
            this.colMovedStatus = new DevComponents.AdvTree.ColumnHeader();
            this.colNodeKey = new DevComponents.AdvTree.ColumnHeader();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.nodeConnector1 = new DevComponents.AdvTree.NodeConnector();
            this.elementStyle1 = new DevComponents.DotNetBar.ElementStyle();
            this.CboXWareHoue = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.contextMenuStripGenerator = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tlsmBtnDeleteNode = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.node1 = new DevComponents.AdvTree.Node();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.tmrLocationMovements = new System.Windows.Forms.Timer(this.components);
            this.lblLocationMovementstatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.errProLocationTransfer = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ItemMasterBindingNavigator)).BeginInit();
            this.pnlMain.SuspendLayout();
            this.PnlTree.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.advTrvTransferLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advTrvOriginalLoc)).BeginInit();
            this.contextMenuStripGenerator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errProLocationTransfer)).BeginInit();
            this.SuspendLayout();
            // 
            // ItemMasterBindingNavigator
            // 
            this.ItemMasterBindingNavigator.AccessibleDescription = "DotNetBar Bar (ItemMasterBindingNavigator)";
            this.ItemMasterBindingNavigator.AccessibleName = "DotNetBar Bar";
            this.ItemMasterBindingNavigator.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.ItemMasterBindingNavigator.Dock = System.Windows.Forms.DockStyle.Top;
            this.ItemMasterBindingNavigator.DockLine = 1;
            this.ItemMasterBindingNavigator.DockOffset = 73;
            this.ItemMasterBindingNavigator.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.ItemMasterBindingNavigator.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.bnCancel});
            this.ItemMasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.ItemMasterBindingNavigator.Name = "ItemMasterBindingNavigator";
            this.ItemMasterBindingNavigator.Size = new System.Drawing.Size(878, 25);
            this.ItemMasterBindingNavigator.Stretch = true;
            this.ItemMasterBindingNavigator.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ItemMasterBindingNavigator.TabIndex = 13;
            this.ItemMasterBindingNavigator.TabStop = false;
            this.ItemMasterBindingNavigator.Text = "bar2";
            // 
            // bnCancel
            // 
            this.bnCancel.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnCancel.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.bnCancel.Name = "bnCancel";
            this.bnCancel.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.bnCancel.Text = "Clear";
            this.bnCancel.Tooltip = "Clear";
            this.bnCancel.Click += new System.EventHandler(this.bnCancel_Click);
            // 
            // LblWareHouse
            // 
            this.LblWareHouse.AutoSize = true;
            // 
            // 
            // 
            this.LblWareHouse.BackgroundStyle.Class = "";
            this.LblWareHouse.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblWareHouse.Location = new System.Drawing.Point(12, 32);
            this.LblWareHouse.Name = "LblWareHouse";
            this.LblWareHouse.Size = new System.Drawing.Size(59, 15);
            this.LblWareHouse.TabIndex = 14;
            this.LblWareHouse.Text = "Warehouse";
            // 
            // pnlMain
            // 
            this.pnlMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlMain.Controls.Add(this.chkBXDamegedQty);
            this.pnlMain.Controls.Add(this.btnXClose);
            this.pnlMain.Controls.Add(this.btnXShow);
            this.pnlMain.Controls.Add(this.CboXCompany);
            this.pnlMain.Controls.Add(this.lblXCompany);
            this.pnlMain.Controls.Add(this.PnlTree);
            this.pnlMain.Controls.Add(this.CboXWareHoue);
            this.pnlMain.Controls.Add(this.LblWareHouse);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 25);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(878, 503);
            this.pnlMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlMain.Style.GradientAngle = 90;
            this.pnlMain.TabIndex = 15;
            this.pnlMain.Text = "panelEx1";
            // 
            // chkBXDamegedQty
            // 
            this.chkBXDamegedQty.AutoSize = true;
            // 
            // 
            // 
            this.chkBXDamegedQty.BackgroundStyle.Class = "";
            this.chkBXDamegedQty.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkBXDamegedQty.Location = new System.Drawing.Point(7, 471);
            this.chkBXDamegedQty.Name = "chkBXDamegedQty";
            this.chkBXDamegedQty.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkBXDamegedQty.Size = new System.Drawing.Size(100, 15);
            this.chkBXDamegedQty.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkBXDamegedQty.TabIndex = 32;
            this.chkBXDamegedQty.Text = "Damaged Items";
            this.chkBXDamegedQty.CheckedChanged += new System.EventHandler(this.chkBXDamegedQty_CheckedChanged);
            // 
            // btnXClose
            // 
            this.btnXClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnXClose.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnXClose.Location = new System.Drawing.Point(790, 472);
            this.btnXClose.Name = "btnXClose";
            this.btnXClose.Size = new System.Drawing.Size(75, 23);
            this.btnXClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnXClose.TabIndex = 3;
            this.btnXClose.Text = "&Close";
            this.btnXClose.Click += new System.EventHandler(this.btnXClose_Click);
            // 
            // btnXShow
            // 
            this.btnXShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnXShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnXShow.Location = new System.Drawing.Point(366, 30);
            this.btnXShow.Name = "btnXShow";
            this.btnXShow.Size = new System.Drawing.Size(58, 20);
            this.btnXShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnXShow.TabIndex = 2;
            this.btnXShow.Text = "Show";
            this.btnXShow.Click += new System.EventHandler(this.btnXShow_Click);
            // 
            // CboXCompany
            // 
            this.CboXCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboXCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboXCompany.DisplayMember = "Text";
            this.CboXCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboXCompany.FormattingEnabled = true;
            this.CboXCompany.ItemHeight = 14;
            this.CboXCompany.Location = new System.Drawing.Point(77, 7);
            this.CboXCompany.Name = "CboXCompany";
            this.CboXCompany.Size = new System.Drawing.Size(282, 20);
            this.CboXCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboXCompany.TabIndex = 0;
            this.CboXCompany.SelectionChangeCommitted += new System.EventHandler(this.CboXCompany_SelectionChangeCommitted);
            this.CboXCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxX_KeyDown);
            // 
            // lblXCompany
            // 
            this.lblXCompany.AutoSize = true;
            // 
            // 
            // 
            this.lblXCompany.BackgroundStyle.Class = "";
            this.lblXCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblXCompany.Location = new System.Drawing.Point(12, 9);
            this.lblXCompany.Name = "lblXCompany";
            this.lblXCompany.Size = new System.Drawing.Size(50, 15);
            this.lblXCompany.TabIndex = 31;
            this.lblXCompany.Text = "Company";
            // 
            // PnlTree
            // 
            this.PnlTree.CanvasColor = System.Drawing.SystemColors.Control;
            this.PnlTree.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.PnlTree.Controls.Add(this.lblXArrow);
            this.PnlTree.Controls.Add(this.btnExapandAllWH);
            this.PnlTree.Controls.Add(this.btnXCollapseWH);
            this.PnlTree.Controls.Add(this.btnXExpandALL);
            this.PnlTree.Controls.Add(this.btnXCollapse);
            this.PnlTree.Controls.Add(this.btnXFilterItem);
            this.PnlTree.Controls.Add(this.btnXFilterWarehouse);
            this.PnlTree.Controls.Add(this.advTrvTransferLoc);
            this.PnlTree.Controls.Add(this.advTrvOriginalLoc);
            this.PnlTree.Location = new System.Drawing.Point(12, 57);
            this.PnlTree.Name = "PnlTree";
            this.PnlTree.Size = new System.Drawing.Size(853, 408);
            this.PnlTree.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PnlTree.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PnlTree.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PnlTree.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PnlTree.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PnlTree.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PnlTree.Style.GradientAngle = 90;
            this.PnlTree.TabIndex = 21;
            // 
            // lblXArrow
            // 
            this.lblXArrow.AutoSize = true;
            // 
            // 
            // 
            this.lblXArrow.BackgroundStyle.Class = "";
            this.lblXArrow.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblXArrow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblXArrow.Location = new System.Drawing.Point(419, 197);
            this.lblXArrow.Name = "lblXArrow";
            this.lblXArrow.Size = new System.Drawing.Size(17, 16);
            this.lblXArrow.TabIndex = 40;
            this.lblXArrow.Text = ">>";
            // 
            // btnExapandAllWH
            // 
            this.btnExapandAllWH.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnExapandAllWH.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnExapandAllWH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExapandAllWH.Location = new System.Drawing.Point(484, 387);
            this.btnExapandAllWH.Name = "btnExapandAllWH";
            this.btnExapandAllWH.Size = new System.Drawing.Size(18, 18);
            this.btnExapandAllWH.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnExapandAllWH.TabIndex = 5;
            this.btnExapandAllWH.Text = "-";
            this.btnExapandAllWH.Tooltip = "Expand all ";
            this.btnExapandAllWH.Click += new System.EventHandler(this.btnExapandAllWH_Click);
            // 
            // btnXCollapseWH
            // 
            this.btnXCollapseWH.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnXCollapseWH.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnXCollapseWH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXCollapseWH.Location = new System.Drawing.Point(461, 387);
            this.btnXCollapseWH.Name = "btnXCollapseWH";
            this.btnXCollapseWH.Size = new System.Drawing.Size(18, 18);
            this.btnXCollapseWH.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnXCollapseWH.TabIndex = 4;
            this.btnXCollapseWH.Text = "+";
            this.btnXCollapseWH.Tooltip = "Collapse all ";
            this.btnXCollapseWH.Click += new System.EventHandler(this.btnXCollapseWH_Click);
            // 
            // btnXExpandALL
            // 
            this.btnXExpandALL.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnXExpandALL.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnXExpandALL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXExpandALL.Location = new System.Drawing.Point(353, 387);
            this.btnXExpandALL.Name = "btnXExpandALL";
            this.btnXExpandALL.Size = new System.Drawing.Size(18, 18);
            this.btnXExpandALL.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnXExpandALL.TabIndex = 2;
            this.btnXExpandALL.Text = "-";
            this.btnXExpandALL.Tooltip = "Expand All ";
            this.btnXExpandALL.Click += new System.EventHandler(this.btnXExpandALL_Click);
            // 
            // btnXCollapse
            // 
            this.btnXCollapse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnXCollapse.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnXCollapse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXCollapse.Location = new System.Drawing.Point(375, 387);
            this.btnXCollapse.Name = "btnXCollapse";
            this.btnXCollapse.Size = new System.Drawing.Size(18, 18);
            this.btnXCollapse.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnXCollapse.TabIndex = 1;
            this.btnXCollapse.Text = "+";
            this.btnXCollapse.Tooltip = "CollapseAll";
            this.btnXCollapse.Click += new System.EventHandler(this.btnXCollapse_Click);
            // 
            // btnXFilterItem
            // 
            this.btnXFilterItem.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnXFilterItem.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnXFilterItem.Image = global::MyBooksERP.Properties.Resources.Filter1;
            this.btnXFilterItem.Location = new System.Drawing.Point(397, 387);
            this.btnXFilterItem.Name = "btnXFilterItem";
            this.btnXFilterItem.Size = new System.Drawing.Size(18, 18);
            this.btnXFilterItem.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnXFilterItem.TabIndex = 0;
            this.btnXFilterItem.Tooltip = "Filter Item Locations";
            this.btnXFilterItem.Click += new System.EventHandler(this.btnXFilterItem_Click);
            // 
            // btnXFilterWarehouse
            // 
            this.btnXFilterWarehouse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnXFilterWarehouse.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnXFilterWarehouse.Image = global::MyBooksERP.Properties.Resources.Filter1;
            this.btnXFilterWarehouse.Location = new System.Drawing.Point(438, 387);
            this.btnXFilterWarehouse.Name = "btnXFilterWarehouse";
            this.btnXFilterWarehouse.Size = new System.Drawing.Size(18, 18);
            this.btnXFilterWarehouse.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnXFilterWarehouse.TabIndex = 3;
            this.btnXFilterWarehouse.Tooltip = "Filter Warehouse";
            this.btnXFilterWarehouse.Click += new System.EventHandler(this.btnXFilterWarehouse_Click);
            // 
            // advTrvTransferLoc
            // 
            this.advTrvTransferLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline;
            this.advTrvTransferLoc.AllowDrop = true;
            this.advTrvTransferLoc.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.advTrvTransferLoc.BackgroundStyle.Class = "TreeBorderKey";
            this.advTrvTransferLoc.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.advTrvTransferLoc.CellEdit = true;
            this.advTrvTransferLoc.Columns.Add(this.ColNodeTransferLoc);
            this.advTrvTransferLoc.Columns.Add(this.ColCurrentQtyTransferLoc);
            this.advTrvTransferLoc.Columns.Add(this.ColMovedQtyTransferLoc);
            this.advTrvTransferLoc.Columns.Add(this.ColMovedStatusTransferLoc);
            this.advTrvTransferLoc.Columns.Add(this.ColNodeKeyTransferLoc);
            this.advTrvTransferLoc.GridLinesColor = System.Drawing.SystemColors.ControlLight;
            this.advTrvTransferLoc.GridRowLines = true;
            this.advTrvTransferLoc.HotTracking = true;
            this.advTrvTransferLoc.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.advTrvTransferLoc.Location = new System.Drawing.Point(438, 2);
            this.advTrvTransferLoc.MultiSelect = true;
            this.advTrvTransferLoc.Name = "advTrvTransferLoc";
            this.advTrvTransferLoc.NodesConnector = this.nodeConnector2;
            this.advTrvTransferLoc.NodeStyle = this.elementStyle2;
            this.advTrvTransferLoc.PathSeparator = ";";
            this.advTrvTransferLoc.Size = new System.Drawing.Size(413, 385);
            this.advTrvTransferLoc.Styles.Add(this.elementStyle2);
            this.advTrvTransferLoc.TabIndex = 1;
            this.advTrvTransferLoc.Text = "advTrvTransferedLoc";
            this.advTrvTransferLoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.advTrvTransferLoc_KeyDown);
            this.advTrvTransferLoc.BeforeNodeDrop += new DevComponents.AdvTree.TreeDragDropEventHandler(this.advTrvTransferLoc_BeforeNodeDrop);
            this.advTrvTransferLoc.NodeMouseDown += new DevComponents.AdvTree.TreeNodeMouseEventHandler(this.advTrvTransferLoc_NodeMouseDown);
            // 
            // ColNodeTransferLoc
            // 
            this.ColNodeTransferLoc.Editable = false;
            this.ColNodeTransferLoc.Name = "ColNodeTransferLoc";
            this.ColNodeTransferLoc.SortingEnabled = false;
            this.ColNodeTransferLoc.Width.Absolute = 250;
            // 
            // ColCurrentQtyTransferLoc
            // 
            this.ColCurrentQtyTransferLoc.Editable = false;
            this.ColCurrentQtyTransferLoc.Name = "ColCurrentQtyTransferLoc";
            this.ColCurrentQtyTransferLoc.SortingEnabled = false;
            this.ColCurrentQtyTransferLoc.Text = "Current Qty";
            this.ColCurrentQtyTransferLoc.Width.Absolute = 75;
            // 
            // ColMovedQtyTransferLoc
            // 
            this.ColMovedQtyTransferLoc.Editable = false;
            this.ColMovedQtyTransferLoc.Name = "ColMovedQtyTransferLoc";
            this.ColMovedQtyTransferLoc.SortingEnabled = false;
            this.ColMovedQtyTransferLoc.Text = " Moved Qty";
            this.ColMovedQtyTransferLoc.Width.Absolute = 75;
            // 
            // ColMovedStatusTransferLoc
            // 
            this.ColMovedStatusTransferLoc.Editable = false;
            this.ColMovedStatusTransferLoc.Name = "ColMovedStatusTransferLoc";
            this.ColMovedStatusTransferLoc.SortingEnabled = false;
            this.ColMovedStatusTransferLoc.Text = "Moved";
            this.ColMovedStatusTransferLoc.Visible = false;
            this.ColMovedStatusTransferLoc.Width.Absolute = 100;
            // 
            // ColNodeKeyTransferLoc
            // 
            this.ColNodeKeyTransferLoc.Editable = false;
            this.ColNodeKeyTransferLoc.Name = "ColNodeKeyTransferLoc";
            this.ColNodeKeyTransferLoc.SortingEnabled = false;
            this.ColNodeKeyTransferLoc.Text = "Key";
            this.ColNodeKeyTransferLoc.Visible = false;
            this.ColNodeKeyTransferLoc.Width.Absolute = 100;
            // 
            // nodeConnector2
            // 
            this.nodeConnector2.LineColor = System.Drawing.SystemColors.ControlText;
            // 
            // elementStyle2
            // 
            this.elementStyle2.Class = "";
            this.elementStyle2.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.elementStyle2.Name = "elementStyle2";
            this.elementStyle2.TextColor = System.Drawing.SystemColors.ControlText;
            // 
            // advTrvOriginalLoc
            // 
            this.advTrvOriginalLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline;
            this.advTrvOriginalLoc.AllowDrop = true;
            this.advTrvOriginalLoc.AllowUserToReorderColumns = true;
            this.advTrvOriginalLoc.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.advTrvOriginalLoc.BackgroundStyle.Class = "TreeBorderKey";
            this.advTrvOriginalLoc.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.advTrvOriginalLoc.CellEdit = true;
            this.advTrvOriginalLoc.Columns.Add(this.ColNode);
            this.advTrvOriginalLoc.Columns.Add(this.ColCurrentQty);
            this.advTrvOriginalLoc.Columns.Add(this.ColMovingQty);
            this.advTrvOriginalLoc.Columns.Add(this.colMovedStatus);
            this.advTrvOriginalLoc.Columns.Add(this.colNodeKey);
            this.advTrvOriginalLoc.GridLinesColor = System.Drawing.SystemColors.ControlLight;
            this.advTrvOriginalLoc.GridRowLines = true;
            this.advTrvOriginalLoc.ImageList = this.imageList1;
            this.advTrvOriginalLoc.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.advTrvOriginalLoc.Location = new System.Drawing.Point(2, 2);
            this.advTrvOriginalLoc.MultiSelect = true;
            this.advTrvOriginalLoc.MultiSelectRule = DevComponents.AdvTree.eMultiSelectRule.AnyNode;
            this.advTrvOriginalLoc.Name = "advTrvOriginalLoc";
            this.advTrvOriginalLoc.NodesConnector = this.nodeConnector1;
            this.advTrvOriginalLoc.NodeStyle = this.elementStyle1;
            this.advTrvOriginalLoc.PathSeparator = ";";
            this.advTrvOriginalLoc.Size = new System.Drawing.Size(413, 385);
            this.advTrvOriginalLoc.Styles.Add(this.elementStyle1);
            this.advTrvOriginalLoc.TabIndex = 0;
            this.advTrvOriginalLoc.Text = "advTrvOriginalLocations";
            this.advTrvOriginalLoc.NodeDragStart += new System.EventHandler(this.advTrvOriginalLoc_NodeDragStart);
            this.advTrvOriginalLoc.AfterNodeDrop += new DevComponents.AdvTree.TreeDragDropEventHandler(this.advTrvOriginalLoc_AfterNodeDrop);
            this.advTrvOriginalLoc.NodeDragFeedback += new DevComponents.AdvTree.TreeDragFeedbackEventHander(this.advTrvOriginalLoc_NodeDragFeedback);
            this.advTrvOriginalLoc.NodeMouseDown += new DevComponents.AdvTree.TreeNodeMouseEventHandler(this.advTrvOriginalLoc_NodeMouseDown);
            // 
            // ColNode
            // 
            this.ColNode.Editable = false;
            this.ColNode.Name = "ColNode";
            this.ColNode.SortingEnabled = false;
            this.ColNode.Width.Absolute = 250;
            // 
            // ColCurrentQty
            // 
            this.ColCurrentQty.Editable = false;
            this.ColCurrentQty.Name = "ColCurrentQty";
            this.ColCurrentQty.SortingEnabled = false;
            this.ColCurrentQty.Text = "Current Qty";
            this.ColCurrentQty.Width.Absolute = 75;
            // 
            // ColMovingQty
            // 
            this.ColMovingQty.MaxInputLength = 4;
            this.ColMovingQty.Name = "ColMovingQty";
            this.ColMovingQty.SortingEnabled = false;
            this.ColMovingQty.Text = "Moving Qty";
            this.ColMovingQty.Width.Absolute = 75;
            // 
            // colMovedStatus
            // 
            this.colMovedStatus.Name = "colMovedStatus";
            this.colMovedStatus.SortingEnabled = false;
            this.colMovedStatus.Text = "Moved";
            this.colMovedStatus.Visible = false;
            this.colMovedStatus.Width.Absolute = 100;
            // 
            // colNodeKey
            // 
            this.colNodeKey.Name = "colNodeKey";
            this.colNodeKey.SortingEnabled = false;
            this.colNodeKey.Text = "Key";
            this.colNodeKey.Visible = false;
            this.colNodeKey.Width.Absolute = 100;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "warehouse-16.png");
            // 
            // nodeConnector1
            // 
            this.nodeConnector1.LineColor = System.Drawing.SystemColors.ControlText;
            // 
            // elementStyle1
            // 
            this.elementStyle1.Class = "";
            this.elementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.elementStyle1.Name = "elementStyle1";
            this.elementStyle1.TextColor = System.Drawing.SystemColors.ControlText;
            // 
            // CboXWareHoue
            // 
            this.CboXWareHoue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboXWareHoue.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboXWareHoue.DisplayMember = "Text";
            this.CboXWareHoue.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboXWareHoue.FormattingEnabled = true;
            this.CboXWareHoue.ItemHeight = 14;
            this.CboXWareHoue.Location = new System.Drawing.Point(77, 30);
            this.CboXWareHoue.Name = "CboXWareHoue";
            this.CboXWareHoue.Size = new System.Drawing.Size(282, 20);
            this.CboXWareHoue.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboXWareHoue.TabIndex = 1;
            this.CboXWareHoue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxX_KeyDown);
            // 
            // contextMenuStripGenerator
            // 
            this.contextMenuStripGenerator.BackColor = System.Drawing.Color.White;
            this.contextMenuStripGenerator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlsmBtnDeleteNode});
            this.contextMenuStripGenerator.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Table;
            this.contextMenuStripGenerator.Name = "contextMenuStripGenerator";
            this.contextMenuStripGenerator.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.contextMenuStripGenerator.Size = new System.Drawing.Size(108, 26);
            // 
            // tlsmBtnDeleteNode
            // 
            this.tlsmBtnDeleteNode.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.tlsmBtnDeleteNode.Name = "tlsmBtnDeleteNode";
            this.tlsmBtnDeleteNode.Size = new System.Drawing.Size(107, 22);
            this.tlsmBtnDeleteNode.Text = "Delete";
            this.tlsmBtnDeleteNode.ToolTipText = "Delete";
            // 
            // buttonItem1
            // 
            this.buttonItem1.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem1.Image = global::MyBooksERP.Properties.Resources.Filter1;
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.Text = "Filter";
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Image = global::MyBooksERP.Properties.Resources.Filter1;
            this.buttonX2.Location = new System.Drawing.Point(435, 198);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(20, 20);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.TabIndex = 37;
            this.buttonX2.Tooltip = "Filter Item Locations";
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Image = global::MyBooksERP.Properties.Resources.Filter1;
            this.buttonX3.Location = new System.Drawing.Point(435, 198);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Size = new System.Drawing.Size(20, 20);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.TabIndex = 37;
            this.buttonX3.Tooltip = "Filter Item Locations";
            // 
            // node1
            // 
            this.node1.Expanded = true;
            this.node1.HostedControl = this.buttonX3;
            this.node1.Name = "node1";
            this.node1.Text = "buttonX3";
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "batch.png");
            this.imageList2.Images.SetKeyName(1, "blocks.png");
            this.imageList2.Images.SetKeyName(2, "item-16.png");
            this.imageList2.Images.SetKeyName(3, "locations.png");
            this.imageList2.Images.SetKeyName(4, "lots.png");
            this.imageList2.Images.SetKeyName(5, "raws.png");
            // 
            // tmrLocationMovements
            // 
            this.tmrLocationMovements.Tick += new System.EventHandler(this.tmrLocationMovements_Tick);
            // 
            // lblLocationMovementstatus
            // 
            this.lblLocationMovementstatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblLocationMovementstatus.CloseButtonVisible = false;
            this.lblLocationMovementstatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblLocationMovementstatus.Location = new System.Drawing.Point(0, 528);
            this.lblLocationMovementstatus.Name = "lblLocationMovementstatus";
            this.lblLocationMovementstatus.OptionsButtonVisible = false;
            this.lblLocationMovementstatus.Size = new System.Drawing.Size(878, 20);
            this.lblLocationMovementstatus.TabIndex = 35;
            // 
            // errProLocationTransfer
            // 
            this.errProLocationTransfer.ContainerControl = this;
            // 
            // FrmLocationTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 548);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.ItemMasterBindingNavigator);
            this.Controls.Add(this.lblLocationMovementstatus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLocationTransfer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Location Movements";
            this.Load += new System.EventHandler(this.FrmLocationTransfer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ItemMasterBindingNavigator)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.PnlTree.ResumeLayout(false);
            this.PnlTree.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.advTrvTransferLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advTrvOriginalLoc)).EndInit();
            this.contextMenuStripGenerator.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errProLocationTransfer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.DotNetBar.Bar ItemMasterBindingNavigator;
        internal DevComponents.DotNetBar.ButtonItem bnCancel;
        private DevComponents.DotNetBar.LabelX LblWareHouse;
        private DevComponents.DotNetBar.PanelEx pnlMain;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboXWareHoue;
        private DevComponents.DotNetBar.PanelEx PnlTree;
        private DevComponents.AdvTree.AdvTree advTrvTransferLoc;
        private DevComponents.AdvTree.NodeConnector nodeConnector2;
        private DevComponents.DotNetBar.ElementStyle elementStyle2;
        private DevComponents.AdvTree.AdvTree advTrvOriginalLoc;
        private DevComponents.AdvTree.NodeConnector nodeConnector1;
        private DevComponents.DotNetBar.ElementStyle elementStyle1;
        private DevComponents.AdvTree.ColumnHeader ColNode;
        private DevComponents.AdvTree.ColumnHeader ColCurrentQty;
        private DevComponents.AdvTree.ColumnHeader ColMovingQty;
        private DevComponents.AdvTree.ColumnHeader ColNodeTransferLoc;
        private DevComponents.AdvTree.ColumnHeader ColCurrentQtyTransferLoc;
        private DevComponents.AdvTree.ColumnHeader ColMovedQtyTransferLoc;
        private DevComponents.AdvTree.ColumnHeader colMovedStatus;
        private DevComponents.AdvTree.ColumnHeader colNodeKey;
        private DevComponents.AdvTree.ColumnHeader ColMovedStatusTransferLoc;
        private DevComponents.AdvTree.ColumnHeader ColNodeKeyTransferLoc;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripGenerator;
        private System.Windows.Forms.ToolStripMenuItem tlsmBtnDeleteNode;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboXCompany;
        private DevComponents.DotNetBar.LabelX lblXCompany;
        private DevComponents.DotNetBar.ButtonX btnXShow;
        private System.Windows.Forms.ImageList imageList1;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonX btnXFilterItem;
        private DevComponents.DotNetBar.ButtonX btnXFilterWarehouse;
        private DevComponents.DotNetBar.ButtonX btnXExpandALL;
        private DevComponents.DotNetBar.ButtonX btnXCollapse;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.AdvTree.Node node1;
        private DevComponents.DotNetBar.ButtonX btnExapandAllWH;
        private DevComponents.DotNetBar.ButtonX btnXCollapseWH;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.Timer tmrLocationMovements;
        private DevComponents.DotNetBar.ButtonX btnXClose;
        private DevComponents.DotNetBar.Controls.WarningBox lblLocationMovementstatus;
        private DevComponents.DotNetBar.LabelX lblXArrow;
        private System.Windows.Forms.ErrorProvider errProLocationTransfer;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkBXDamegedQty;
    }
}