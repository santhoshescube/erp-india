﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmMaterialIssue : DevComponents.DotNetBar.Office2007Form
    {
        public FrmMain objFrmTradingMain;
        clsBLLMaterialIssueMaster MobjClsBLLMaterialIssueMaster;//Object Of clsBllMaterialIssueMaster
        string MstrCommonMessage;                   // for setting error message
        DataTable datMessages;                      // having forms error messages
        MessageBoxIcon MmsgMessageIcon;             // obj of message icon
        ClsNotificationNew MobjClsNotification;     //obj of clsNotification
        ClsLogWriter MobjClsLogWriter;              // obj of clsLogWriter

        bool blnIsEditMode = false;
        bool MblnPrintEmailPermission = false;
        bool MblnAddPermission = false;
        bool MblnUpdatePermission = false;
        bool MblnDeletePermission = false;
        bool MchangeStatus = false;    //Check any edit or new entry done
        clsBLLCommonUtility mobjClsBllCommonUtility;
        int intCurrencyScale = 3;
        public long lngMaterialIssueID = 0;

        private enum MaterialIssueMessageCodes
        {
            MaterialIssueNoExists = 9651,
            MaterialIssueNoExistsContinue = 9652,
            QuantityExceedsAvailableQty = 9653,
            MaterialReturnDone = 9654,
            ItemSold = 9655,
            EnterMaterialIssueNo=9656,
            EnterProject = 9657
        }

        public FrmMaterialIssue()
        {
            InitializeComponent();

            MobjClsBLLMaterialIssueMaster = new clsBLLMaterialIssueMaster();
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotificationNew();
            tmrMaterialIssue.Interval = ClsCommonSettings.TimerInterval;
            mobjClsBllCommonUtility = new clsBLLCommonUtility();            
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.MaterialIssue, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void LoadMessage()
        {
            try
            {
                //Loads error messages
                datMessages = new DataTable();
                datMessages = MobjClsNotification.FillMessageArray((int)FormID.MaterialIssue, ClsCommonSettings.ProductID);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadMessage() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in Load Message() " + ex.Message, 2);
            }
        }


        private void FillCombo(int intType)
        {
            // 0 - All, 1- Company,2-Warehouse,3-Employee,4- Search Company,5- Search Warehouse,6- SearchEmployee,7-Search IssuedBy 8 - Cuurency
            // 9 - Project 10 - Search Project
            try
            {
                DataTable datCombos = null;
                if (intType == 0 || intType == 1)
                {
                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;
                }

                if (intType == 0 || intType == 2)
                {
                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "Distinct IW.WarehouseID,IW.WarehouseName", "" +
                        "InvWarehouse IW INNER JOIN InvWarehouseCompanyDetails WC ON IW.WarehouseID=WC.WarehouseID", "" +
                        "IsActive=1 AND WC.CompanyID = " + ClsCommonSettings.LoginCompanyID });
                    cboWarehouse.ValueMember = "WarehouseID";
                    cboWarehouse.DisplayMember = "WarehouseName";
                    cboWarehouse.DataSource = datCombos;
                }
              
                if (intType == 0 || intType == 4)
                {
                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                    cboSCompany.ValueMember = "CompanyID";
                    cboSCompany.DisplayMember = "CompanyName";
                    cboSCompany.DataSource = datCombos;
                }

                if (intType == 0 || intType == 5)
                {
                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "Distinct IW.WarehouseID,IW.WarehouseName", "" +
                        "InvWarehouse IW INNER JOIN InvWarehouseCompanyDetails WC ON IW.WarehouseID=WC.WarehouseID", "" +
                        "IsActive=1 AND WC.CompanyID = " + ClsCommonSettings.LoginCompanyID });
                    cboSWarehouse.ValueMember = "WarehouseID";
                    cboSWarehouse.DisplayMember = "WarehouseName";
                    cboSWarehouse.DataSource = datCombos;
                }
          
                if (intType == 0 || intType == 7)
                {
                    string strFilterCondition = "";
                    if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                        strFilterCondition = "UM.RoleID Not In (1,2)";

                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "UM.UserID,IsNull( EM.EmployeeFullName+' - ' +EM.EmployeeNumber,UM.UserName) as UserName", "UserMaster UM Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID", strFilterCondition });
                    cboSIssuedBy.ValueMember = "UserID";
                    cboSIssuedBy.DisplayMember = "UserName";
                    cboSIssuedBy.DataSource = datCombos;
                }
                if (intType == 0 || intType == 8)
                {
                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "CurrencyID,CurrencyName", "CurrencyReference", "" });
                    cboCurrency.ValueMember = "CurrencyID";
                    cboCurrency.DisplayMember = "CurrencyName";
                    cboCurrency.DataSource = datCombos;
                }
                if (intType == 0 || intType == 9)
                {
                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "PTM.TemplateID,PTM.Template + ' - ' + PTM.Code AS Template," +
                        "IM.ItemName + ' - ' + IM.Code AS ItemName", "" +
                        "InvProductionTemplateMaster PTM INNER JOIN InvItemMaster IM ON PTM.ProductID = IM.ItemID", "" +
                        "PTM.CompanyID = " + ClsCommonSettings.LoginCompanyID });
                    cboBOM.ValueMember = "TemplateID";
                    cboBOM.DisplayMember = "Template";
                    cboBOM.DataSource = datCombos;
                }

                if (intType == 0 || intType == 10)
                {
                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "TemplateID,Template + ' - ' + Code AS Template", "" +
                        "InvProductionTemplateMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID });
                    cboSBOM.ValueMember = "TemplateID";
                    cboSBOM.DisplayMember = "Template";
                    cboSBOM.DataSource = datCombos;
                }
            }
            catch(Exception ex)
            {
                MobjClsLogWriter.WriteLog("Following error occured in load combos : " + ex.Message, 1);
            }
        }

        private void ClearControls()
        {
            if(!blnIsEditMode)
            txtMaterialIssueNo.Text = "";
            cboCurrency.SelectedIndex = -1;
            dgvMaterialIssue.Rows.Clear();
            txtRemarks.Clear();
            txtNetAmount.Text = "0.000";
            dtpIssuedDate.Value = ClsCommonSettings.GetServerDate();
            dtpIssuedDate.MaxDate = ClsCommonSettings.GetServerDate();
        }

        private void ClearSearchControls()
        {
            cboSCompany.SelectedIndex = -1;
            cboSWarehouse.SelectedIndex = -1;
            cboSIssuedBy.SelectedIndex = -1;
            cboSBOM.SelectedIndex = -1;
            dtpSFrom.Value = ClsCommonSettings.GetServerDate();
            dtpSTo.Value = ClsCommonSettings.GetServerDate();
        }

        private void AddNew()
        {
            blnIsEditMode = false;
            ErrMaterialIssue.Clear();
            tmrMaterialIssue.Enabled = false;

            bnClear.Enabled = true;
            panelTop.Enabled = panelMiddle.Enabled = pnlBottom.Enabled = true;
        //    cboCompany.Enabled = true;
            cboWarehouse.Enabled = true;
            cboBOM.Enabled = true;

            cboCompany.SelectedIndex = -1;
            cboWarehouse.SelectedIndex = -1;
            cboBOM.SelectedIndex = -1;
            lblIssuedByText.Text = ClsCommonSettings.strEmployeeName;
            ClearControls();
            MobjClsBLLMaterialIssueMaster = new clsBLLMaterialIssueMaster();
            //cboCompany.SelectedValue = ClsCommonSettings.CompanyID;
            lblCreatedByText.Text = "Created By : " + ClsCommonSettings.strEmployeeName;
            lblCreatedDateValue.Text = "Created Date : " + ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
            btnRefresh_Click(null, null);
            bnSaveItem.Enabled = bnDelete.Enabled = bnClear.Enabled = bnEmail.Enabled = bnPrint.Enabled = false;
            lblStatus.Text = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.AddNewInformation).Replace("#", "").Replace("*", "Material Issue");
            txtQuantity.Text = "";
            lblProduct.Text = "Product : ";
            MchangeStatus = false;
            cboWarehouse.Focus();
        }

        private void ChangeStatus(object sender,EventArgs e)
        {
            MchangeStatus = true;
            bnClear.Enabled = true;
            
            if (!blnIsEditMode)
            {
                bnSaveItem.Enabled = MblnAddPermission;
            }
            else
            {
                bnSaveItem.Enabled = MblnUpdatePermission;
            }
        }

        private void GenerateMaterialIssueNo()
        {
            string strMaterialIssueNo = "";
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            DataTable dtTemp = objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue));
            strMaterialIssueNo = ClsCommonSettings.MaterialIssuePrefix;
            dtTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "(IsNull(Max(SlNo),0) + 1) as SlNo", "InvMaterialIssueMaster", "CompanyID = " + cboCompany.SelectedValue.ToInt32() });
            if (dtTemp.Rows.Count > 0)
            {
                strMaterialIssueNo += dtTemp.Rows[0]["SlNo"].ToStringCustom();
            }

            if (ClsCommonSettings.MaterialIssueAutogenerate)
                txtMaterialIssueNo.ReadOnly = true;
            else
                txtMaterialIssueNo.ReadOnly = false;

            txtMaterialIssueNo.Text = "MI" + strMaterialIssueNo;
        }

        private void FillComboColumn(int intRowIndex)
        {
            if (dgvMaterialIssue.Rows[intRowIndex].Cells[dgvColItemID.Index].Value.ToInt32() > 0)
            {
                int intUOMID = dgvMaterialIssue.Rows[intRowIndex].Cells[dgvColUOM.Index].Tag.ToInt32();

                DataTable datCombos = MobjClsBLLMaterialIssueMaster.GetItemUOMs(dgvMaterialIssue.Rows[intRowIndex].Cells[dgvColItemID.Index].Value.ToInt32());
                dgvColUOM.ValueMember = "UOMID";
                dgvColUOM.DisplayMember = "ShortName";
                dgvColUOM.DataSource = datCombos;

                if (intUOMID > 0)
                {
                    dgvMaterialIssue.Rows[intRowIndex].Cells[dgvColUOM.Index].Value = intUOMID;
                    dgvMaterialIssue.Rows[intRowIndex].Cells[dgvColUOM.Index].Tag = dgvMaterialIssue.Rows[intRowIndex].Cells[dgvColUOM.Index].Value;
                    dgvMaterialIssue.Rows[intRowIndex].Cells[dgvColUOM.Index].Value = dgvMaterialIssue.Rows[intRowIndex].Cells[dgvColUOM.Index].FormattedValue;
                }
            }
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedValue.ToInt32() != 0)
            {
                DataTable datCurrencyDetails = mobjClsBllCommonUtility.FillCombos(new string[] { "CR.Scale,CR.CurrencyID", "CurrencyReference CR Inner Join CompanyMaster CM On CM.CurrencyId = CR.CurrencyID", "CM.CompanyID = " + cboCompany.SelectedValue.ToInt32() });
                intCurrencyScale = datCurrencyDetails.Rows[0]["Scale"].ToInt32();
                cboCurrency.SelectedValue = datCurrencyDetails.Rows[0]["CurrencyID"].ToInt32();

                if (!blnIsEditMode)
                    GenerateMaterialIssueNo();
            }
            else
                intCurrencyScale = 3;
           // FillCombo(2);
          //  FillCombo(3);
            ChangeStatus(sender,e);
        }

        private void FrmMaterialIssue_Load(object sender, EventArgs e)
        {
            tmrMaterialIssue.Interval = ClsCommonSettings.TimerInterval;

            FillCombo(0);
            SetPermissions();
            LoadMessage();
            ClearSearchControls();
            AddNew();

            if (lngMaterialIssueID != 0)
            {
                MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID = lngMaterialIssueID;
                DisplayMaterialIssueInfo();
            }
        }

        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private bool FillParameters()
        {
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueDate = dtpIssuedDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueNo = txtMaterialIssueNo.Text;
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.WarehouseID = cboWarehouse.SelectedValue.ToInt32();
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.TemplateID = cboBOM.SelectedValue.ToInt64();
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.Quantity = txtQuantity.Text.ToDecimal();
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.CompanyID = cboCompany.SelectedValue.ToInt32();
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.NetAmount = txtNetAmount.Text.ToDecimal();
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.NetAmountRounded = Math.Ceiling(txtNetAmount.Text.ToDecimal());
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.CreatedBy = ClsCommonSettings.UserID;
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.CreatedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.Remarks = txtRemarks.Text.Trim();

            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueDetails = new List<clsDTOMaterialIssueDetails>();
            //MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueLocationDetails = new List<clsDTOMaterialIssueLocationDetails>();
            int intSerialNo = 0;
            foreach (DataGridViewRow dr in dgvMaterialIssue.Rows)
            {
                if (dr.Cells[dgvColItemID.Index].Value.ToInt32() != 0)
                {
                    clsDTOMaterialIssueDetails objDTOMaterialDetails = new clsDTOMaterialIssueDetails();
                    objDTOMaterialDetails.SerialNo = ++intSerialNo;
                    objDTOMaterialDetails.ItemID = dr.Cells[dgvColItemID.Index].Value.ToInt32();
                    objDTOMaterialDetails.BatchID = dr.Cells[dgvColBatchID.Index].Value.ToInt64();
                    objDTOMaterialDetails.Quantity = dr.Cells[dgvColQuantity.Index].Value.ToDecimal();
                    objDTOMaterialDetails.ActualQuantity = dr.Cells[dgvColActualQuantity.Index].Value.ToDecimal();
                    objDTOMaterialDetails.UOMID = dr.Cells[dgvColUOM.Index].Tag.ToInt32();
                    objDTOMaterialDetails.Rate = dr.Cells[dgvColRate.Index].Value.ToDecimal();
                    objDTOMaterialDetails.NetAmount = dr.Cells[dgvColNetAmount.Index].Value.ToDecimal();
                    MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueDetails.Add(objDTOMaterialDetails);

                    //clsDTOMaterialIssueLocationDetails objLocationDetails = new clsDTOMaterialIssueLocationDetails();
                    //objLocationDetails.SerialNo = objDTOMaterialDetails.SerialNo;
                    //objLocationDetails.ItemID = objDTOMaterialDetails.ItemID;
                    //objLocationDetails.BatchID = objDTOMaterialDetails.BatchID;
                    //objLocationDetails.Quantity = objDTOMaterialDetails.Quantity;
                    //objLocationDetails.UOMID = objDTOMaterialDetails.UOMID;

                    //DataRow drLocation = MobjClsBLLMaterialIssueMaster.GetItemDefaultLocation(objLocationDetails.ItemID, cboWarehouse.SelectedValue.ToInt32());
                    //objLocationDetails.LocationID = drLocation["LocationID"].ToInt32();
                    //objLocationDetails.RowID = drLocation["RowID"].ToInt32();
                    //objLocationDetails.BlockID = drLocation["BlockID"].ToInt32();
                    //objLocationDetails.LotID = drLocation["LotID"].ToInt32();
                    //MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueLocationDetails.Add(objLocationDetails);
                }
            }
            return true;
        }

        private bool DisplayMaterialIssueInfo()
        {
            panelTop.Enabled = panelMiddle.Enabled = pnlBottom.Enabled = true;
            if (MobjClsBLLMaterialIssueMaster.FillMaterialIssueMasterInfo())
            {
                cboCompany.SelectedValue = MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.CompanyID;
                cboWarehouse.SelectedValue = MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.WarehouseID;
                cboBOM.SelectedValue = MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.TemplateID;
                txtQuantity.Text = MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.Quantity.ToStringCustom();
                txtMaterialIssueNo.Text = MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueNo;
                dtpIssuedDate.Value = MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueDate.ToDateTime();
                lblIssuedByText.Text = MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.CreatedByText;
                txtRemarks.Text = MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.Remarks;

                lblCreatedByText.Text = "Created By : " + MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.CreatedByText;
                lblCreatedDateValue.Text = "Created Date : " + MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.CreatedDate;

                getItemDetails();

                txtNetAmount.Text = MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.NetAmount.ToString();

                bnDelete.Enabled = MblnDeletePermission;
                lblStatus.Text = "Details of Material Issue No '" + txtMaterialIssueNo.Text + "'";

                cboCompany.Enabled = cboWarehouse.Enabled=cboBOM.Enabled  = false;
                bnSaveItem.Enabled = false;
                bnAddNewItem.Enabled = MblnAddPermission;
                bnEmail.Enabled = bnPrint.Enabled = MblnPrintEmailPermission;
                blnIsEditMode = true;
                return true;
            }
            return false;
        }

        private void getItemDetails()
        {
            dgvMaterialIssue.Rows.Clear();
            DataTable datMaterialIssueDetails = new DataTable();

            if (MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID > 0)
                datMaterialIssueDetails = MobjClsBLLMaterialIssueMaster.GetMaterialIssueDetails();
            else if (MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID == 0 && cboBOM.SelectedIndex >= 0)
                datMaterialIssueDetails = MobjClsBLLMaterialIssueMaster.GetProductionTemplateDetails(cboBOM.SelectedValue.ToInt64());

            for (int iCounter = 0; iCounter < datMaterialIssueDetails.Rows.Count; iCounter++)
            {
                dgvMaterialIssue.Rows.Add();
                DataRow dr = datMaterialIssueDetails.Rows[iCounter];
                dgvMaterialIssue.Rows[iCounter].Cells[dgvColItemID.Index].Value = dr["ItemID"].ToInt32();
                dgvMaterialIssue.Rows[iCounter].Cells[dgvColItemName.Index].Value = dr["ItemName"].ToStringCustom();
                dgvMaterialIssue.Rows[iCounter].Cells[dgvColItemCode.Index].Value = dr["Code"].ToStringCustom();

                if (MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID > 0)
                {
                    dgvMaterialIssue.Rows[iCounter].Cells[dgvColBatchNo.Index].Value = dr["BatchNo"].ToStringCustom();
                    dgvMaterialIssue.Rows[iCounter].Cells[dgvColBatchID.Index].Value = dr["BatchID"].ToInt64();
                    dgvMaterialIssue.Rows[iCounter].Cells[dgvColExpiryDate.Index].Value = dr["ExpiryDate"].ToStringCustom();
                    dgvMaterialIssue.Rows[iCounter].Cells[dgvColActualQuantity.Index].Value = dr["ActualQuantity"].ToDecimal();
                }
                else
                    dgvMaterialIssue.Rows[iCounter].Cells[dgvColActualQuantity.Index].Value = dr["Quantity"].ToDecimal();

                dgvMaterialIssue.Rows[iCounter].Cells[dgvColQuantity.Index].Value = dr["Quantity"].ToDecimal();                
                FillComboColumn(iCounter);
                dgvMaterialIssue.Rows[iCounter].Cells[dgvColUOM.Index].Value = dr["UOMID"].ToInt32();
                dgvMaterialIssue.Rows[iCounter].Cells[dgvColUOM.Index].Tag = dgvMaterialIssue.Rows[iCounter].Cells[dgvColUOM.Index].Value;
                dgvMaterialIssue.Rows[iCounter].Cells[dgvColUOM.Index].Value = dgvMaterialIssue.Rows[iCounter].Cells[dgvColUOM.Index].FormattedValue;
                dgvMaterialIssue.Rows[iCounter].Cells[dgvColRate.Index].Value = dr["Rate"].ToDecimal();

                if (MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID > 0)
                    dgvMaterialIssue.Rows[iCounter].Cells[dgvColNetAmount.Index].Value = dr["NetAmount"].ToDecimal();
                else
                    dgvMaterialIssue.Rows[iCounter].Cells[dgvColNetAmount.Index].Value = dr["ItemTotal"].ToDecimal();
            }
        }

        private void dgvMaterialIssue_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgvColUOM.Index)
            {
                dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColUOM.Index].Tag = dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColUOM.Index].Value;
                dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColUOM.Index].Value = dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColUOM.Index].FormattedValue;
                dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColRate.Index].Value = mobjClsBllCommonUtility.GetSaleRate(dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value.ToInt32(), dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColUOM.Index].Tag.ToInt32(), dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColBatchID.Index].Value.ToInt64(), cboCompany.SelectedValue.ToInt32()).ToString("F"+intCurrencyScale);
                dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColQtyAvailable.Index].Value = mobjClsBllCommonUtility.ConvertBaseUnitQtyToOtherQty(dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColUOM.Index].Tag.ToInt32(), dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value.ToInt32(), mobjClsBllCommonUtility.GetStockQuantity(dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value.ToInt32(), dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColBatchID.Index].Value.ToInt64(), cboCompany.SelectedValue.ToInt32(), cboWarehouse.SelectedValue.ToInt32()),1);

                int intUomScale = 0;
                if (dgvMaterialIssue[dgvColUOM.Index, e.RowIndex].Tag.ToInt32() != 0)
                    intUomScale = mobjClsBllCommonUtility.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvMaterialIssue[dgvColUOM.Index, e.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value = dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value.ToDecimal().ToString("F" + intUomScale);

            }
            else if (e.ColumnIndex == dgvColQuantity.Index)
            {
                int intUomScale = 0;
                if (dgvMaterialIssue[dgvColUOM.Index, e.RowIndex].Tag.ToInt32() != 0)
                    intUomScale = mobjClsBllCommonUtility.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvMaterialIssue[dgvColUOM.Index, e.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value = dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value.ToDecimal().ToString("F" + intUomScale);

            }
        }

        private void dgvMaterialIssue_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                ChangeStatus(null, null);
                if (e.ColumnIndex == dgvColRate.Index || e.ColumnIndex == dgvColQuantity.Index)
                {
                    CalculateNetAmount();
                }
                else if (e.ColumnIndex == dgvColItemID.Index)
                {
                    FillComboColumn(e.RowIndex);
                    DataTable datItemDetails = mobjClsBllCommonUtility.FillCombos(new string[] { "DefaultSalesUomID", "InvItemMaster", "ItemID  = " + dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value.ToInt32() });
                    if (datItemDetails.Rows.Count > 0)
                    {
                        dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColUOM.Index].Value = datItemDetails.Rows[0]["DefaultSalesUomID"].ToInt32();
                        dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColUOM.Index].Tag = dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColUOM.Index].Value;
                        dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColUOM.Index].Value = dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColUOM.Index].FormattedValue;
                    }
                }
                else if (e.ColumnIndex == dgvColUOM.Index)
                {
                    dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColRate.Index].Value = mobjClsBllCommonUtility.GetSaleRate(dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value.ToInt32(), dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColUOM.Index].Tag.ToInt32(), dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColBatchID.Index].Value.ToInt64(), cboCompany.SelectedValue.ToInt32()).ToString("F"+intCurrencyScale);
                    dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColQtyAvailable.Index].Value = mobjClsBllCommonUtility.ConvertBaseUnitQtyToOtherQty(dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColUOM.Index].Tag.ToInt32(), dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value.ToInt32(), mobjClsBllCommonUtility.GetStockQuantity(dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value.ToInt32(), dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColBatchID.Index].Value.ToInt64(), cboCompany.SelectedValue.ToInt32(), cboWarehouse.SelectedValue.ToInt32()), 1);

                    int intUomScale = 0;
                    if (dgvMaterialIssue[dgvColUOM.Index, e.RowIndex].Tag.ToInt32() != 0)
                        intUomScale = mobjClsBllCommonUtility.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvMaterialIssue[dgvColUOM.Index, e.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                    dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value = dgvMaterialIssue.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value.ToDecimal().ToString("F" + intUomScale);

                }
            }
        }

        private void CalculateNetAmount()
        {
            decimal decNetAmount = 0,decGridNetAmount =0;
            foreach (DataGridViewRow dr in dgvMaterialIssue.Rows)
            {
                if (dr.Cells[dgvColItemID.Index].Value.ToInt32() != 0)
                {
                    decGridNetAmount = dr.Cells[dgvColQuantity.Index].Value.ToDecimal() * dr.Cells[dgvColRate.Index].Value.ToDecimal();
                    dr.Cells[dgvColNetAmount.Index].Value = decGridNetAmount.ToString("F"+intCurrencyScale);
                    decNetAmount += decGridNetAmount;
                }
            }

            txtNetAmount.Text = decNetAmount.ToString("F"+intCurrencyScale);
        }

        private void dgvMaterialIssue_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (e.ColumnIndex == dgvColUOM.Index)
                {
                    FillComboColumn(e.RowIndex);
                }
            }
        }

        private void dgvMaterialIssue_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvMaterialIssue.CurrentCell.ColumnIndex == dgvColItemCode.Index || dgvMaterialIssue.CurrentCell.ColumnIndex == dgvColItemName.Index)
                {
                //    for (int i = 0; i < dgvMaterialIssue.Columns.Count; i++)
                //        dgvMaterialIssue.Rows[dgvMaterialIssue.CurrentCell.RowIndex].Cells[i].Value = null;
                //}
                dgvMaterialIssue.PServerName = ClsCommonSettings.ServerName;
                string[] First = null;
                string[] second = null;

                First = new string[] { "dgvColItemCode", "dgvColItemName", "dgvColItemID", "dgvColBatchNo","dgvColExpiryDate", "dgvColBatchID", "dgvColRate", "dgvColQtyAvailable" };//first grid 
                second = new string[] { "ItemCode", "ItemName", "ItemID", "BatchNo", "ExpiryDate", "BatchID", "Rate", "QtyAvailable" };//inner grid

             
                dgvMaterialIssue.aryFirstGridParam = First;
                dgvMaterialIssue.arySecondGridParam = second;
                dgvMaterialIssue.PiFocusIndex = dgvColQuantity.Index;
                dgvMaterialIssue.iGridWidth = 500;
                dgvMaterialIssue.pnlLeft = expandableSplitterLeft.Location.X + 5;
                dgvMaterialIssue.pnlTop = bnMaterialIssue.Height + 10;
                dgvMaterialIssue.bBothScrollBar = true;
                dgvMaterialIssue.ColumnsToHide = new string[] { "ItemID", "BatchID" };

                DataTable dtDatasource = MobjClsBLLMaterialIssueMaster.GetItems(Convert.ToInt32(cboCompany.SelectedValue),cboWarehouse.SelectedValue.ToInt32());

                if (dgvMaterialIssue.CurrentCell.ColumnIndex == dgvColItemCode.Index)
                {
                    dgvMaterialIssue.CurrentCell.Value = dgvMaterialIssue.TextBoxText;
                    dgvMaterialIssue.field = "ItemCode";
                }
                else if (dgvMaterialIssue.CurrentCell.ColumnIndex == dgvColItemName.Index)
                {
                    dgvMaterialIssue.CurrentCell.Value = dgvMaterialIssue.TextBoxText;
                    dgvMaterialIssue.field = "ItemName";
                }
                string strFilterString = dgvMaterialIssue.field + " Like '" + dgvMaterialIssue.TextBoxText + "%'";
                dtDatasource.DefaultView.RowFilter = strFilterString;
                dgvMaterialIssue.dtDataSource = dtDatasource.DefaultView.ToTable();
            }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvMaterialIssue_Textbox_TextChanged() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvMaterialIssue_Textbox_TextChanged() " + ex.Message, 2);
            }
        }

        private void dgvMaterialIssue_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if ((e.ColumnIndex == dgvColItemCode.Index) || (e.ColumnIndex == dgvColItemName.Index))
                {
                    dgvMaterialIssue.PiColumnIndex = e.ColumnIndex;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvMaterialIssue_CellEnter() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvMaterialIssue_CellEnter() " + ex.Message, 2);
            }
        }

        private void bnSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                // Checking Reference is Exists or not
                bool blnEdit = true;

                if (MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID > 0)
                {
                    DataTable datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "ProductionID", "InvProductionMaster", "" +
                        "MaterialIssueID = " + MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID });

                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 25, out MmsgMessageIcon);
                            lblStatus.Text = MstrCommonMessage.Split('#').Last();
                            MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                            blnEdit = false;
                        }
                    }
                }
                // End

                if (blnEdit)
                {
                    if (ValidateMaterialIssueMaster())
                    {
                        FillParameters();

                        if (blnIsEditMode)
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.DoYouWishToUpdate, out MmsgMessageIcon).Replace("#", "");
                        else
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.DoYouWishToSave, out MmsgMessageIcon).Replace("#", "");

                        if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MmsgMessageIcon) == DialogResult.No)
                            return;

                        if (txtMaterialIssueNo.ReadOnly && mobjClsBllCommonUtility.FillCombos(new string[] { "1", "InvMaterialIssueMaster", "MaterialIssueNo = '" + txtMaterialIssueNo.Text + "' And CompanyID = " + cboCompany.SelectedValue.ToInt32() + " And MaterialIssueID <> " + MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID }).Rows.Count > 0)
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MaterialIssueMessageCodes.MaterialIssueNoExistsContinue, out MmsgMessageIcon).Replace("#", "");

                            if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MmsgMessageIcon) == DialogResult.No)
                                return;

                            GenerateMaterialIssueNo();
                            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueNo = txtMaterialIssueNo.Text;
                        }

                        if (MobjClsBLLMaterialIssueMaster.SaveMaterialIssue())
                        {
                            if (!blnIsEditMode)
                                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.SavedSuccessfully, out MmsgMessageIcon).Replace("#", "");
                            else
                                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.UpdatedSuccessfully, out MmsgMessageIcon).Replace("#", "");

                            //objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.MaterialIssue;
                            //objFrmTradingMain.tmrAlert.Enabled = true;

                            MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                            AddNew();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Following error occured in bnSaveItem_Click : " + ex.Message, 1);
            }
        }

        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void bnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                //if (mobjClsBllCommonUtility.FillCombos(new string[] { "1", "STMaterialReturnMaster", "MaterialIssueID = " + MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID }).Rows.Count > 0)
                //{
                //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MaterialIssueMessageCodes.MaterialReturnDone, out MmsgMessageIcon).Replace("#","");
                //    MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                //    return;
                //}
                bool blnDelete = true;

                DataTable datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "ProductionID", "InvProductionMaster", "" +
                    "MaterialIssueID = " + MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID });

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 25, out MmsgMessageIcon);
                        lblStatus.Text = MstrCommonMessage.Split('#').Last();
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        blnDelete = false;
                    }
                }

                if (blnDelete)
                {
                    DataTable datMaterialIssueDetails = new DataTable();
                    datMaterialIssueDetails = MobjClsBLLMaterialIssueMaster.GetMaterialIssueDetails();

                    if (datMaterialIssueDetails.Rows.Count > 0)
                    {
                        foreach (DataRow dr in datMaterialIssueDetails.Rows)
                        {
                            decimal decOldQuantity = 0, decIssuedQty = 0, decReturnedQty = 0;

                            //DataTable datAlreadyReturnedQty = mobjClsBllCommonUtility.FillCombos(new string[] { "IsNull(Sum(dbo.fnInvConvertQtyToBaseUnitQty(MD.ItemID,MD.UOMID,1,MD.ReturnedQuantity,1)),0) as ReturnedQty", "InvMaterialReturnDetails MD Inner Join InvMaterialReturnMaster MR On MR.MaterialReturnID = MD.MaterialReturnID", "MR.WareHouseID = " + cboWarehouse.SelectedValue.ToInt32() + " AND MD.ItemID = " + dr["ItemID"].ToInt32() + " And MD.BatchID = " + dr["BatchID"].ToInt64() });
                            //if (datAlreadyReturnedQty.Rows.Count > 0)
                            //    decReturnedQty = datAlreadyReturnedQty.Rows[0]["ReturnedQty"].ToDecimal();

                            DataTable datIssuedQuantity = mobjClsBllCommonUtility.FillCombos(new string[] { "IsNull(Sum(dbo.fnInvConvertQtyToBaseUnitQty(MD.ItemID,MD.UOMID,1,MD.Quantity,1)),0) as IssuedQty", "InvMaterialIssueDetails MD Inner Join InvMaterialIssueMaster MI On MI.MaterialIssueID = MD.MaterialIssueID ", "MI.WareHouseID = " + cboWarehouse.SelectedValue.ToInt32() + " AND MD.ItemID = " + dr["ItemID"].ToInt32() + " And MD.BatchID = " + dr["BatchID"].ToInt64() });
                            if (datIssuedQuantity.Rows.Count > 0)
                                decIssuedQty = datIssuedQuantity.Rows[0]["IssuedQty"].ToDecimal();

                            decOldQuantity = mobjClsBllCommonUtility.ConvertQtyToBaseUnitQty(dr["UOMID"].ToInt32(), dr["ItemID"].ToInt32(), dr["Quantity"].ToDecimal(), 1);

                            if (decIssuedQty - decOldQuantity < decReturnedQty)
                            {
                                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MaterialIssueMessageCodes.MaterialReturnDone, out MmsgMessageIcon).Replace("#", "");

                                MstrCommonMessage = MstrCommonMessage.Replace("*", dr["ItemName"].ToStringCustom());
                                MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                                dgvMaterialIssue.Focus();
                                dgvMaterialIssue.CurrentCell = dgvMaterialIssue[dgvColItemName.Index, dgvMaterialIssue.RowCount - 1];
                                return;
                            }
                        }
                    }

                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.DoYouWishToDelete, out MmsgMessageIcon).Replace("#", "");

                    if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MmsgMessageIcon) == DialogResult.No)
                        return;

                    if (MobjClsBLLMaterialIssueMaster.DeleteMaterialIssue())
                    {
                        clsBLLAlertMoment objclsBLLAlertMoment = new clsBLLAlertMoment();
                        int MaterialIssueID = MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID.ToInt32();
                        objclsBLLAlertMoment.DeleteAlert(MaterialIssueID, (int)AlertSettingsTypes.MaterialIssue);

                        //Material issue alert
                        //objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.MaterialIssue;
                        //objFrmTradingMain.tmrAlert.Enabled = true;

                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.DeletedSuccessfully, out MmsgMessageIcon).Replace("#", "");
                        MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        AddNew();
                    }
                }
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Following error occured in bnDeleteItem_Click : " + ex.Message, 1);

            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            string strSearchCondition = "";

            if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
                strSearchCondition += "MI.MaterialIssueNo = '" + txtSearch.Text.Trim() + "'";
            else
            {
                if (cboSCompany.SelectedValue.ToInt32() != 0)
                    strSearchCondition += "MI.CompanyID = " + cboSCompany.SelectedValue.ToInt32();
                if (cboSWarehouse.SelectedValue.ToInt32() != 0)
                {
                    if (!string.IsNullOrEmpty(strSearchCondition))
                        strSearchCondition += " AND ";
                    strSearchCondition += "MI.WarehouseID = " + cboSWarehouse.SelectedValue.ToInt32();
                }

                if (cboSBOM.SelectedValue.ToInt32() != 0)
                {
                    if (!string.IsNullOrEmpty(strSearchCondition))
                        strSearchCondition += " AND ";
                    strSearchCondition += "MI.TemplateID = " + cboSBOM.SelectedValue.ToInt32();
                }
             
                if (cboSIssuedBy.SelectedValue.ToInt32() != 0)
                {
                    if (!string.IsNullOrEmpty(strSearchCondition))
                        strSearchCondition += " AND ";
                    strSearchCondition += "MI.CreatedBy = " + cboSIssuedBy.SelectedValue.ToInt32();
                }
                else if(ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    if(((DataTable)cboSIssuedBy.DataSource).Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(strSearchCondition))
                        strSearchCondition += " AND ";
                    
                    strSearchCondition += " MI.CreatedBy In (";
                    foreach (DataRow dr in ((DataTable)cboSIssuedBy.DataSource).Rows)
                    {
                        strSearchCondition += dr["UserID"].ToInt32()+",";
                    }
                 
                    strSearchCondition = strSearchCondition.Remove(strSearchCondition.Length - 1);
                    strSearchCondition += ")";
                    }
                }

                if (!string.IsNullOrEmpty(strSearchCondition))
                    strSearchCondition += " And ";
                strSearchCondition += "MI.MaterialIssueDate Between '" + dtpSFrom.Value.ToString("dd-MMM-yyyy") + "' And '" + dtpSTo.Value.ToString("dd-MMM-yyyy") + "'";
            }

            DataTable datMaterialIssues = MobjClsBLLMaterialIssueMaster.GetAllMaterialIssues(strSearchCondition);
            dgvMaterialIssueDisplay.DataSource = null;
            dgvMaterialIssueDisplay.DataSource = datMaterialIssues;
            if (dgvMaterialIssueDisplay.Columns.Count > 0)
            {
                dgvMaterialIssueDisplay.Columns[0].Visible = false;
                dgvMaterialIssueDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            lblSCountStatus.Text = "Total Issues : " + dgvMaterialIssueDisplay.Rows.Count;

        }

        private void bnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void dgvMaterialIssueDisplay_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                if (dgvMaterialIssueDisplay.Rows[e.RowIndex].Cells[0].Value.ToInt64() > 0)
                {
                    MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID =dgvMaterialIssueDisplay.Rows[e.RowIndex].Cells[0].Value.ToInt64();
                    DisplayMaterialIssueInfo();
                }
            }
            
        }

        private bool ValidateMaterialIssueMaster()
        {
            dgvMaterialIssue.CommitEdit(DataGridViewDataErrorContexts.Commit);
            dgvMaterialIssue.EndEdit();
            int intMessageCode = 0;
            Control cntrl=null;
            if (cboCompany.SelectedValue.ToInt32() == 0)
            {
                intMessageCode = (int)MessageCode.SelectCompany;
                cntrl = cboCompany;
            }
            else if (cboWarehouse.SelectedValue.ToInt32() == 0)
            {
                intMessageCode = (int)MessageCode.SelectWarehouse;
                cntrl = cboWarehouse;
            }
            else if (cboBOM.SelectedValue.ToInt32() == 0)
            {
                intMessageCode = (int)MaterialIssueMessageCodes.EnterProject;
                cntrl = cboBOM;
            }

            else if (string.IsNullOrEmpty(txtMaterialIssueNo.Text.Trim()))
            {
                intMessageCode = (int)MaterialIssueMessageCodes.EnterMaterialIssueNo;
                cntrl = txtMaterialIssueNo;
            }

            else if (!txtMaterialIssueNo.ReadOnly && mobjClsBllCommonUtility.FillCombos(new string[] { "1", "InvMaterialIssueMaster", "MaterialIssueNo = '" + txtMaterialIssueNo.Text + "' And CompanyID = " + cboCompany.SelectedValue.ToInt32() + " And MaterialIssueID <> " + MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID }).Rows.Count > 0)
            {
                intMessageCode = (int)MaterialIssueMessageCodes.MaterialIssueNoExists;
                cntrl = txtMaterialIssueNo;
            }

            if (intMessageCode != 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, intMessageCode, out MmsgMessageIcon).Replace("#", "");
                ErrMaterialIssue.SetError(cntrl, MstrCommonMessage);
                MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                tmrMaterialIssue.Enabled = true;
                return false;
            }
            else
            {
                if (CheckDuplication())
                    return false;

                return ValidateMaterialIssueDetails();
            }
        }

        private bool ValidateMaterialIssueDetails()
        {
            int intColumnIndex = -1, intRowIndex=-1 , intMessageCode = 0;
            int intItemCount =0;
            DataTable datMaterialIssueDetails = new DataTable();
            if (blnIsEditMode)
                datMaterialIssueDetails = MobjClsBLLMaterialIssueMaster.GetMaterialIssueDetails();

            foreach (DataGridViewRow dr in dgvMaterialIssue.Rows)
            {
                if (dr.Cells[dgvColItemID.Index].Value.ToInt32() > 0)
                {
                    if (string.IsNullOrEmpty(dr.Cells[dgvColItemCode.Index].Value.ToStringCustom()))
                    {
                        intColumnIndex = dgvColItemCode.Index;
                        intRowIndex = dr.Index;
                        intMessageCode = (int)MessageCode.SelectItemCode;
                        break;
                    }
                    else if (string.IsNullOrEmpty(dr.Cells[dgvColItemName.Index].Value.ToStringCustom()))
                    {
                        intColumnIndex = dgvColItemName.Index;
                        intRowIndex = dr.Index;
                        intMessageCode = (int)MessageCode.SelectItemName;
                        break;
                    }
                    else if (dr.Cells[dgvColQuantity.Index].Value.ToDecimal() <=0)
                    {
                        intColumnIndex = dgvColQuantity.Index;
                        intRowIndex = dr.Index;
                        intMessageCode = (int)MessageCode.EnterQuantity;
                        break;
                    }
                    else if (dr.Cells[dgvColUOM.Index].Tag.ToInt32() == 0)
                    {
                        intColumnIndex = dgvColUOM.Index;
                        intRowIndex = dr.Index;
                        intMessageCode = (int)MessageCode.EnterUOM;
                        break;
                    }
                    else
                    {
                        decimal decOldQuantity = 0, decCurrentQty = 0, decStockQty = 0, decEmployeeStockQty = 0, decIssuedQty = 0, decReturnedQty = 0;


                        //DataTable datAlreadyReturnedQty = mobjClsBllCommonUtility.FillCombos(new string[] { "IsNull(Sum(dbo.fnInvConvertQtyToBaseUnitQty(MD.ItemID,MD.UOMID,1,MD.ReturnedQuantity,1)),0) as ReturnedQty", "InvMaterialReturnDetails MD Inner Join InvMaterialReturnMaster MR On MR.MaterialReturnID = MD.MaterialReturnID", "MR.WareHouseID = " + cboWarehouse.SelectedValue.ToInt32() + " AND MR.ProjectID = "+ cboBOM.SelectedValue.ToInt32() +" AND  MD.ItemID = " + dr.Cells[dgvColItemID.Index].Value.ToInt32() + " And MD.BatchID = " + dr.Cells[dgvColBatchID.Index].Value.ToInt64() });
                        //if (datAlreadyReturnedQty.Rows.Count > 0)
                        //    decReturnedQty = datAlreadyReturnedQty.Rows[0]["ReturnedQty"].ToDecimal();

                        DataTable datIssuedQuantity = mobjClsBllCommonUtility.FillCombos(new string[] { "" +
                            "IsNull(Sum(dbo.fnInvConvertQtyToBaseUnitQty(MD.ItemID,MD.UOMID,1,MD.Quantity,1)),0) as IssuedQty", "" +
                            "InvMaterialIssueDetails MD Inner Join InvMaterialIssueMaster MI On MI.MaterialIssueID = MD.MaterialIssueID ", "" +
                            "MI.WareHouseID = " + cboWarehouse.SelectedValue.ToInt32() + " AND " +
                            "MI.TemplateID = "+ cboBOM.SelectedValue.ToInt32() +" AND " +
                            "MD.ItemID = " + dr.Cells[dgvColItemID.Index].Value.ToInt32() + " And " +
                            "MD.BatchID = " + dr.Cells[dgvColBatchID.Index].Value.ToInt64() });

                        if (datIssuedQuantity.Rows.Count > 0)
                            decIssuedQty = datIssuedQuantity.Rows[0]["IssuedQty"].ToDecimal();


                        decStockQty = mobjClsBllCommonUtility.GetStockQuantity(dr.Cells[dgvColItemID.Index].Value.ToInt32(), dr.Cells[dgvColBatchID.Index].Value.ToInt64(), cboCompany.SelectedValue.ToInt32(), cboWarehouse.SelectedValue.ToInt32());
                        //decEmployeeStockQty = mobjClsBllCommonUtility.GetEmployeeStockQuantity(dr.Cells[dgvColItemID.Index].Value.ToInt32(), dr.Cells[dgvColBatchID.Index].Value.ToInt64(), cboEmployee.SelectedValue.ToInt32());

                        if (blnIsEditMode)
                        {
                            datMaterialIssueDetails.DefaultView.RowFilter = "ItemID = " + dr.Cells[dgvColItemID.Index].Value.ToInt32() + " And BatchID = " + dr.Cells[dgvColBatchID.Index].Value.ToInt64();
                            if (datMaterialIssueDetails.DefaultView.ToTable().Rows.Count > 0)
                            {
                                DataRow drMaterialIssueRow = datMaterialIssueDetails.DefaultView.ToTable().Rows[0];
                                decOldQuantity = mobjClsBllCommonUtility.ConvertQtyToBaseUnitQty(drMaterialIssueRow["UOMID"].ToInt32(), drMaterialIssueRow["ItemID"].ToInt32(), drMaterialIssueRow["Quantity"].ToDecimal(), 1);
                            }
                        }

                        decCurrentQty = mobjClsBllCommonUtility.ConvertQtyToBaseUnitQty(dr.Cells[dgvColUOM.Index].Tag.ToInt32(),dr.Cells[dgvColItemID.Index].Value.ToInt32(),dr.Cells[dgvColQuantity.Index].Value.ToDecimal(),1);

                        if (decStockQty + decOldQuantity < decCurrentQty)
                        {
                            intColumnIndex = dgvColQuantity.Index;
                            intRowIndex = dr.Index;
                            intMessageCode = (int)MaterialIssueMessageCodes.QuantityExceedsAvailableQty;
                            break;
                        }
                        else if(((decIssuedQty - decOldQuantity)+decCurrentQty) < decReturnedQty)
                        {
                            intColumnIndex = dgvColQuantity.Index;
                            intRowIndex = dr.Index;
                            intMessageCode = (int)MaterialIssueMessageCodes.MaterialReturnDone;
                            break;
                        }


                        //if ((decEmployeeStockQty - decOldQuantity) + decCurrentQty < 0)
                        //{
                        //    intColumnIndex = dgvColQuantity.Index;
                        //    intRowIndex = dr.Index;
                        //    intMessageCode = (int)MaterialIssueMessageCodes.ItemSold;
                        //    break;
                        //}
                    }
                    intItemCount++;
                }
            }

            if (datMaterialIssueDetails.Rows.Count > 0)
            {
               
                foreach (DataRow dr in datMaterialIssueDetails.Rows)
                {
                    bool blnExists = false;
                    foreach (DataGridViewRow drGridRow in dgvMaterialIssue.Rows)
                    {
                        if(dr["ItemID"].ToInt32() == drGridRow.Cells[dgvColItemID.Index].Value.ToInt32() && dr["BatchID"].ToInt64() == drGridRow.Cells[dgvColBatchID.Index].Value.ToInt64())
                        {
                            blnExists = true;
                            break;
                        }
                    }

                    if (!blnExists)
                    {
                        decimal decOldQuantity = 0, decEmployeeStockQty = 0,decIssuedQty=0,decReturnedQty=0;
                      //  decEmployeeStockQty = mobjClsBllCommonUtility.GetEmployeeStockQuantity(dr["ItemID"].ToInt32(), dr["BatchID"].ToInt64(), cboEmployee.SelectedValue.ToInt32());
                        decOldQuantity = mobjClsBllCommonUtility.ConvertQtyToBaseUnitQty(dr["UOMID"].ToInt32(), dr["ItemID"].ToInt32(), dr["Quantity"].ToDecimal(), 1);

                        //DataTable datAlreadyReturnedQty = mobjClsBllCommonUtility.FillCombos(new string[] { "IsNull(Sum(dbo.fnInvConvertQtyToBaseUnitQty(MD.ItemID,MD.UOMID,1,MD.ReturnedQuantity,1)),0) as ReturnedQty", "InvMaterialReturnDetails MD Inner Join InvMaterialReturnMaster MR On MR.MaterialReturnID = MD.MaterialReturnID", "MR.WareHouseID = " + cboWarehouse.SelectedValue.ToInt32() + " AND MD.ItemID = " + dr["ItemID"].ToInt32() + " And MD.BatchID = " + dr["BatchID"].ToInt64() });
                        //if (datAlreadyReturnedQty.Rows.Count > 0)
                        //    decReturnedQty = datAlreadyReturnedQty.Rows[0]["ReturnedQty"].ToDecimal();

                        DataTable datIssuedQuantity = mobjClsBllCommonUtility.FillCombos(new string[] { "" +
                            "IsNull(Sum(dbo.fnInvConvertQtyToBaseUnitQty(MD.ItemID,MD.UOMID,1,MD.Quantity,1)),0) as IssuedQty", "" +
                            "InvMaterialIssueDetails MD Inner Join InvMaterialIssueMaster MI On MI.MaterialIssueID = MD.MaterialIssueID ", "" +
                            "MI.WareHouseID = " + cboWarehouse.SelectedValue.ToInt32() + " AND MD.ItemID = " + dr["ItemID"].ToInt32() + " And " +
                            "MD.BatchID = " + dr["BatchID"].ToInt64() });
                        if (datIssuedQuantity.Rows.Count > 0)
                            decIssuedQty = datIssuedQuantity.Rows[0]["IssuedQty"].ToDecimal();

                        //if (decEmployeeStockQty - decOldQuantity < 0)
                        //{
                        //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MaterialIssueMessageCodes.ItemSold, out MmsgMessageIcon).Replace("#", "");

                        //    MstrCommonMessage = MstrCommonMessage.Replace("*", dr["ItemName"].ToStringCustom());

                        //    MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        //    dgvMaterialIssue.Focus();
                        //    dgvMaterialIssue.CurrentCell = dgvMaterialIssue[dgvColItemName.Index, dgvMaterialIssue.RowCount - 1];
                        //    return false;
                        //}

                        if (decIssuedQty - decOldQuantity < decReturnedQty)
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MaterialIssueMessageCodes.MaterialReturnDone, out MmsgMessageIcon).Replace("#", "");

                            MstrCommonMessage = MstrCommonMessage.Replace("*", dr["ItemName"].ToStringCustom());

                            MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                            dgvMaterialIssue.Focus();
                            dgvMaterialIssue.CurrentCell = dgvMaterialIssue[dgvColItemName.Index, dgvMaterialIssue.RowCount - 1];
                            return false;
                        }
                    }
                }
            }

            if (intMessageCode > 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, intMessageCode, out MmsgMessageIcon).Replace("#", "");
                
                if (intMessageCode == (int)MaterialIssueMessageCodes.ItemSold || intMessageCode == (int)MaterialIssueMessageCodes.MaterialReturnDone)
                    MstrCommonMessage = MstrCommonMessage.Replace("*", dgvMaterialIssue.Rows[intRowIndex].Cells[dgvColItemName.Index].Value.ToStringCustom());

                MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                dgvMaterialIssue.Focus();
                dgvMaterialIssue.CurrentCell = dgvMaterialIssue[intColumnIndex, intRowIndex];
                return false;
            }
            else if (intItemCount == 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.SelectItemDetails, out MmsgMessageIcon).Replace("#", "");
                MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                dgvMaterialIssue.Focus();
                dgvMaterialIssue.CurrentCell = dgvMaterialIssue[dgvColItemCode.Index, 0];
                return false;
            }
            return true;
        }

        private bool CheckDuplication()
        {
            bool blnDuplicate = false;
            int intRowIndex=-1;
            for (int intICounter = 0; intICounter < dgvMaterialIssue.Rows.Count; intICounter++)
            {
                int intItemID = dgvMaterialIssue.Rows[intICounter].Cells[dgvColItemID.Index].Value.ToInt32();
                long lngBatchID = dgvMaterialIssue.Rows[intICounter].Cells[dgvColBatchID.Index].Value.ToInt64();
                if (intItemID > 0)
                {
                    for (int intJCounter = intICounter + 1; intJCounter < dgvMaterialIssue.Rows.Count; intJCounter++)
                    {
                        if (dgvMaterialIssue.Rows[intJCounter].Cells[dgvColItemID.Index].Value.ToInt32() == intItemID && dgvMaterialIssue.Rows[intJCounter].Cells[dgvColBatchID.Index].Value.ToInt64() == lngBatchID)
                        {
                            blnDuplicate = true;
                            intRowIndex = intJCounter;
                            break;
                        }
                    }
                }
                if (blnDuplicate)
                    break;
            }

            if (blnDuplicate)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.CheckDuplication, out MmsgMessageIcon).Replace("#", "");
                MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                dgvMaterialIssue.Focus();
                dgvMaterialIssue.CurrentCell = dgvMaterialIssue[dgvColItemCode.Index, intRowIndex];
            }

            return blnDuplicate;
        }

     

        private void tmrMaterialIssue_Tick(object sender, EventArgs e)
        {
            ErrMaterialIssue.Clear();
            tmrMaterialIssue.Enabled = false;
        }

        private void expandableSplitterLeft_ExpandedChanged(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
        {
            if (expandableSplitterLeft.Expanded)
            {
                if (dgvMaterialIssueDisplay.Rows.Count > 0)
                    dgvMaterialIssueDisplay.Columns[0].Visible = false;
            }
        }

        private void dgvMaterialIssue_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvMaterialIssue.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvMaterialIssue.CurrentCell.OwningColumn.Name == "dgvColQuantity")
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
                int intUomScale = 0;
                if (dgvMaterialIssue[dgvColUOM.Index, dgvMaterialIssue.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    intUomScale = mobjClsBllCommonUtility.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvMaterialIssue[dgvColUOM.Index, dgvMaterialIssue.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    if (intUomScale == 0)
                    {
                        if (e.KeyChar == '.')
                            e.Handled = true;
                    }
                    else
                    {
                        int dotIndex = -1;
                        if (txt.Text.Contains("."))
                        {
                            dotIndex = txt.Text.IndexOf('.');
                        }
                        if (e.KeyChar == '.')
                        {
                            if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            {
                                e.Handled = true;
                            }
                        }
                        else
                        {
                            if (char.IsDigit(e.KeyChar))
                            {
                                if (dotIndex != -1 && txt.SelectionStart > dotIndex)
                                {
                                    string[] splitText = txt.Text.Split('.');
                                    if (splitText.Length == 2)
                                    {
                                        if (splitText[1].Length - txt.SelectedText.Length >= intUomScale)
                                            e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void dgvMaterialIssue_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvMaterialIssue.IsCurrentCellDirty)
                {
                    if (dgvMaterialIssue.CurrentCell != null)
                    {
                        dgvMaterialIssue.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    }
                }
            }
            catch (Exception)
            { }
        }

        private void cboWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            int intCompanyID = 0;
            DataTable datWarehouse = mobjClsBllCommonUtility.FillCombos(new string[] { "CompanyID", "InvWarehouse", "WarehouseID = " + cboWarehouse.SelectedValue.ToInt32() });
            if (datWarehouse.Rows.Count > 0)
                intCompanyID = datWarehouse.Rows[0]["CompanyID"].ToInt32();
            cboCompany.SelectedValue = intCompanyID;
            dgvMaterialIssue.Rows.Clear();
            CalculateNetAmount();
            ChangeStatus(sender, e);
        }

        private void dgvMaterialIssue_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            CalculateNetAmount();
            ChangeStatus(null, null);
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {
            if (MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID;

                ObjViewer.PiFormID = (int)FormID.MaterialIssue;
                ObjViewer.ShowDialog();
            }
        }

        private void bnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Material Issue";
                    ObjEmailPopUp.EmailFormType = EmailFormID.MaterialIssue;
                    ObjEmailPopUp.EmailSource = MobjClsBLLMaterialIssueMaster.GetMaterialIssueReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnEmail_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in bnEmail_Click() " + ex.Message, 2);
            }
        }

        private void dgvMaterialIssue_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            mobjClsBllCommonUtility.SetSerialNo(dgvMaterialIssue,e.RowIndex,false);
        }

        private void dgvMaterialIssue_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            mobjClsBllCommonUtility.SetSerialNo(dgvMaterialIssue,e.RowIndex,false);
        }

        private void cboBOM_SelectedIndexChanged(object sender, EventArgs e)
        {
            getItemDetails();
            getItemDetailsWithQty();

            if (cboBOM.SelectedItem != null)
                lblProduct.Text = "Product : " + ((System.Data.DataRowView)(cboBOM.SelectedItem)).Row.ItemArray[2].ToStringCustom();
        }

        private void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            getItemDetailsWithQty();
        }

        private void getItemDetailsWithQty()
        {
            decimal decQty = txtQuantity.Text.ToDecimal();

            if (decQty == 0)
                decQty = 1;

            for (int iCounter = 0; iCounter <= dgvMaterialIssue.Rows.Count - 2; iCounter++)
            {
                dgvMaterialIssue.Rows[iCounter].Cells[dgvColQuantity.Index].Value =
                    dgvMaterialIssue.Rows[iCounter].Cells[dgvColActualQuantity.Index].Value.ToDecimal() * decQty;
            }
        }
    }
}