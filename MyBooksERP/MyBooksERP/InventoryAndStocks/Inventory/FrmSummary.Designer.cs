﻿namespace MyBooksERP
{
    partial class FrmSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSummary));
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("Stock", "StockSummory.png");
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("Purchase Invoice", 2);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("GRN", 8);
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("Debit Note", 4);
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("Sales Invoice", 1);
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("POS", 10);
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("Credit Note", 3);
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem("Delivery Note", 9);
            System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem("Stock Transfer", 7);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.LegendItem legendItem1 = new System.Windows.Forms.DataVisualization.Charting.LegendItem();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tmrSummary = new System.Windows.Forms.Timer(this.components);
            this.DockSite5 = new DevComponents.DotNetBar.DockSite();
            this.controlContainerItem1 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem2 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem3 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem4 = new DevComponents.DotNetBar.ControlContainerItem();
            this.ImgAccounts = new System.Windows.Forms.ImageList(this.components);
            this.ImgLargeIcon = new System.Windows.Forms.ImageList(this.components);
            this.DockSite3 = new DevComponents.DotNetBar.DockSite();
            this.DockSite4 = new DevComponents.DotNetBar.DockSite();
            this.DockSite1 = new DevComponents.DotNetBar.DockSite();
            this.DockSite2 = new DevComponents.DotNetBar.DockSite();
            this.GridPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.DockSite7 = new DevComponents.DotNetBar.DockSite();
            this.DotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.PanelMain = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.ExpandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.pnlLeft = new DevComponents.DotNetBar.PanelEx();
            this.Exppanelleft = new DevComponents.DotNetBar.ExpandablePanel();
            this.PanelSummary = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.LstSummary = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.pnlRight = new DevComponents.DotNetBar.PanelEx();
            this.expandableSplitterMiddle = new DevComponents.DotNetBar.ExpandableSplitter();
            this.PanelRightMiddle = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.panelChart = new System.Windows.Forms.Panel();
            this.BtnBack = new DevComponents.DotNetBar.ButtonX();
            this.TrkBarChart = new System.Windows.Forms.TrackBar();
            this.cboChartTypes = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ChartSummary = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.DgvSummary = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.AdcSummary = new DevComponents.AdvTree.AdvTree();
            this.Period = new DevComponents.AdvTree.ColumnHeader();
            this.Description = new DevComponents.AdvTree.ColumnHeader();
            this.Item = new DevComponents.AdvTree.ColumnHeader();
            this.Batch = new DevComponents.AdvTree.ColumnHeader();
            this.ReceivedQuantity = new DevComponents.AdvTree.ColumnHeader();
            this.InvoicedQuantity = new DevComponents.AdvTree.ColumnHeader();
            this.SoldQuantity = new DevComponents.AdvTree.ColumnHeader();
            this.DamagedQuantity = new DevComponents.AdvTree.ColumnHeader();
            this.OpeningQuantity = new DevComponents.AdvTree.ColumnHeader();
            this.TransferedQuantity = new DevComponents.AdvTree.ColumnHeader();
            this.ReceivedFromTransfer = new DevComponents.AdvTree.ColumnHeader();
            this.DemoQuantity = new DevComponents.AdvTree.ColumnHeader();
            this.ElementStyle6 = new DevComponents.DotNetBar.ElementStyle();
            this.ElementStyle3 = new DevComponents.DotNetBar.ElementStyle();
            this.ElementStyle4 = new DevComponents.DotNetBar.ElementStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblSalesStatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.ExpPanelright = new DevComponents.DotNetBar.ExpandablePanel();
            this.lblItems = new DevComponents.DotNetBar.LabelX();
            this.lblBatches = new DevComponents.DotNetBar.LabelX();
            this.lblWarehouse = new DevComponents.DotNetBar.LabelX();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboItems = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboBatch = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnShow = new DevComponents.DotNetBar.ButtonX();
            this.lblTo = new DevComponents.DotNetBar.LabelX();
            this.dtpTo = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.lblFrom = new DevComponents.DotNetBar.LabelX();
            this.dtpFrom = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.lblPeriod = new DevComponents.DotNetBar.LabelX();
            this.cboPeriod = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.Yearwise = new DevComponents.Editors.ComboItem();
            this.Monthwise = new DevComponents.Editors.ComboItem();
            this.Daywise = new DevComponents.Editors.ComboItem();
            this.Summary = new DevComponents.Editors.ComboItem();
            this.dockSite8 = new DevComponents.DotNetBar.DockSite();
            this.expandableSplitterBottom = new DevComponents.DotNetBar.ExpandableSplitter();
            this.dockSite6 = new DevComponents.DotNetBar.DockSite();
            this.ErrSummary = new System.Windows.Forms.ErrorProvider(this.components);
            this.PanelMain.SuspendLayout();
            this.pnlLeft.SuspendLayout();
            this.PanelSummary.SuspendLayout();
            this.pnlRight.SuspendLayout();
            this.PanelRightMiddle.SuspendLayout();
            this.panelChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrkBarChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdcSummary)).BeginInit();
            this.panel1.SuspendLayout();
            this.ExpPanelright.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // DockSite5
            // 
            this.DockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite5.Dock = System.Windows.Forms.DockStyle.Top;
            this.DockSite5.Location = new System.Drawing.Point(0, 0);
            this.DockSite5.Name = "DockSite5";
            this.DockSite5.Size = new System.Drawing.Size(311, 0);
            this.DockSite5.TabIndex = 93;
            this.DockSite5.TabStop = false;
            // 
            // controlContainerItem1
            // 
            this.controlContainerItem1.AllowItemResize = false;
            this.controlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem1.Name = "controlContainerItem1";
            // 
            // controlContainerItem2
            // 
            this.controlContainerItem2.AllowItemResize = false;
            this.controlContainerItem2.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem2.Name = "controlContainerItem2";
            // 
            // controlContainerItem3
            // 
            this.controlContainerItem3.AllowItemResize = false;
            this.controlContainerItem3.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem3.Name = "controlContainerItem3";
            // 
            // controlContainerItem4
            // 
            this.controlContainerItem4.AllowItemResize = false;
            this.controlContainerItem4.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem4.Name = "controlContainerItem4";
            // 
            // ImgAccounts
            // 
            this.ImgAccounts.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgAccounts.ImageStream")));
            this.ImgAccounts.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgAccounts.Images.SetKeyName(0, "AccountGroup1.png");
            this.ImgAccounts.Images.SetKeyName(1, "Account1.PNG");
            // 
            // ImgLargeIcon
            // 
            this.ImgLargeIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgLargeIcon.ImageStream")));
            this.ImgLargeIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgLargeIcon.Images.SetKeyName(0, "StockSummory.png");
            this.ImgLargeIcon.Images.SetKeyName(1, "SalesSummary.PNG");
            this.ImgLargeIcon.Images.SetKeyName(2, "PurchaseSummary.PNG");
            this.ImgLargeIcon.Images.SetKeyName(3, "CreditDebit Note.png");
            this.ImgLargeIcon.Images.SetKeyName(4, "DebitNote.png");
            this.ImgLargeIcon.Images.SetKeyName(5, "Purchase Invoice.png");
            this.ImgLargeIcon.Images.SetKeyName(6, "Sales invoice.png");
            this.ImgLargeIcon.Images.SetKeyName(7, "TransferOorder.png");
            this.ImgLargeIcon.Images.SetKeyName(8, "GRN.png");
            this.ImgLargeIcon.Images.SetKeyName(9, "Delivery.png");
            this.ImgLargeIcon.Images.SetKeyName(10, "Point of sale.png");
            // 
            // DockSite3
            // 
            this.DockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.DockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.DockSite3.Location = new System.Drawing.Point(0, 0);
            this.DockSite3.Name = "DockSite3";
            this.DockSite3.Size = new System.Drawing.Size(1301, 0);
            this.DockSite3.TabIndex = 98;
            this.DockSite3.TabStop = false;
            // 
            // DockSite4
            // 
            this.DockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.DockSite4.Location = new System.Drawing.Point(0, 576);
            this.DockSite4.Name = "DockSite4";
            this.DockSite4.Size = new System.Drawing.Size(1301, 0);
            this.DockSite4.TabIndex = 99;
            this.DockSite4.TabStop = false;
            // 
            // DockSite1
            // 
            this.DockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.DockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.DockSite1.Location = new System.Drawing.Point(0, 0);
            this.DockSite1.Name = "DockSite1";
            this.DockSite1.Size = new System.Drawing.Size(0, 576);
            this.DockSite1.TabIndex = 96;
            this.DockSite1.TabStop = false;
            // 
            // DockSite2
            // 
            this.DockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.DockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.DockSite2.Location = new System.Drawing.Point(1301, 0);
            this.DockSite2.Name = "DockSite2";
            this.DockSite2.Size = new System.Drawing.Size(0, 576);
            this.DockSite2.TabIndex = 97;
            this.DockSite2.TabStop = false;
            // 
            // DockSite7
            // 
            this.DockSite7.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite7.Dock = System.Windows.Forms.DockStyle.Top;
            this.DockSite7.Location = new System.Drawing.Point(0, 0);
            this.DockSite7.Name = "DockSite7";
            this.DockSite7.Size = new System.Drawing.Size(1301, 0);
            this.DockSite7.TabIndex = 101;
            this.DockSite7.TabStop = false;
            // 
            // DotNetBarManager1
            // 
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.DotNetBarManager1.BottomDockSite = this.DockSite4;
            this.DotNetBarManager1.ColorScheme.BarCaptionBackground = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.BarCaptionBackground2 = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.BarCaptionInactiveBackground = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(157)))), ((int)(((byte)(145)))));
            this.DotNetBarManager1.ColorScheme.ExplorerBarBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(172)))), ((int)(((byte)(89)))));
            this.DotNetBarManager1.ColorScheme.ExplorerBarBackground2 = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(121)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.ItemCheckedBorder = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(97)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.ItemDisabledText = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(157)))), ((int)(((byte)(145)))));
            this.DotNetBarManager1.ColorScheme.ItemHotBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(125)))));
            this.DotNetBarManager1.ColorScheme.ItemHotBorder = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.ItemPressedBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.DotNetBarManager1.ColorScheme.ItemPressedBorder = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.ItemSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(67)))));
            this.DotNetBarManager1.EnableFullSizeDock = false;
            this.DotNetBarManager1.LeftDockSite = this.DockSite1;
            this.DotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.DotNetBarManager1.ParentForm = null;
            this.DotNetBarManager1.RightDockSite = this.DockSite2;
            this.DotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.DotNetBarManager1.ToolbarTopDockSite = this.DockSite7;
            this.DotNetBarManager1.TopDockSite = this.DockSite3;
            this.DotNetBarManager1.UseGlobalColorScheme = true;
            // 
            // PanelMain
            // 
            this.PanelMain.BackColor = System.Drawing.SystemColors.Control;
            this.PanelMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.PanelMain.Controls.Add(this.ExpandableSplitterLeft);
            this.PanelMain.Controls.Add(this.pnlRight);
            this.PanelMain.Controls.Add(this.pnlLeft);
            this.PanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelMain.Location = new System.Drawing.Point(0, 0);
            this.PanelMain.Name = "PanelMain";
            this.PanelMain.Size = new System.Drawing.Size(1301, 576);
            // 
            // 
            // 
            this.PanelMain.Style.BackColor = System.Drawing.SystemColors.Control;
            this.PanelMain.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelMain.Style.BackColorGradientAngle = 90;
            this.PanelMain.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelMain.Style.BorderBottomWidth = 1;
            this.PanelMain.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelMain.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelMain.Style.BorderLeftWidth = 1;
            this.PanelMain.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelMain.Style.BorderRightWidth = 1;
            this.PanelMain.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelMain.Style.BorderTopWidth = 1;
            this.PanelMain.Style.Class = "";
            this.PanelMain.Style.CornerDiameter = 4;
            this.PanelMain.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.PanelMain.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.PanelMain.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelMain.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.PanelMain.StyleMouseDown.Class = "";
            this.PanelMain.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.PanelMain.StyleMouseOver.Class = "";
            this.PanelMain.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.PanelMain.TabIndex = 31;
            // 
            // ExpandableSplitterLeft
            // 
            this.ExpandableSplitterLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(220)))), ((int)(((byte)(184)))));
            this.ExpandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(192)))), ((int)(((byte)(148)))));
            this.ExpandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None;
            this.ExpandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.None;
            this.ExpandableSplitterLeft.ExpandableControl = this.pnlLeft;
            this.ExpandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(45)))), ((int)(((byte)(150)))));
            this.ExpandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.ExpandableSplitterLeft.ExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.ExpandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.ExpandableSplitterLeft.GripDarkColor = System.Drawing.SystemColors.ControlText;
            this.ExpandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.ExpandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.ExpandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.ExpandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(142)))), ((int)(((byte)(75)))));
            this.ExpandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(139)))));
            this.ExpandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.ExpandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.ExpandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(45)))), ((int)(((byte)(150)))));
            this.ExpandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.ExpandableSplitterLeft.HotExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.ExpandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.ExpandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(45)))), ((int)(((byte)(150)))));
            this.ExpandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.ExpandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.ExpandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.ExpandableSplitterLeft.Location = new System.Drawing.Point(250, 0);
            this.ExpandableSplitterLeft.Name = "ExpandableSplitterLeft";
            this.ExpandableSplitterLeft.Size = new System.Drawing.Size(2, 570);
            this.ExpandableSplitterLeft.TabIndex = 109;
            this.ExpandableSplitterLeft.TabStop = false;
            // 
            // pnlLeft
            // 
            this.pnlLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.pnlLeft.Controls.Add(this.Exppanelleft);
            this.pnlLeft.Controls.Add(this.PanelSummary);
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(250, 570);
            this.pnlLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlLeft.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.pnlLeft.Style.BackColor2.Color = System.Drawing.SystemColors.Control;
            this.pnlLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlLeft.Style.BorderColor.Color = System.Drawing.SystemColors.ControlDarkDark;
            this.pnlLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlLeft.Style.GradientAngle = 90;
            this.pnlLeft.TabIndex = 92;
            // 
            // Exppanelleft
            // 
            this.Exppanelleft.CanvasColor = System.Drawing.SystemColors.Control;
            this.Exppanelleft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.Exppanelleft.Dock = System.Windows.Forms.DockStyle.Top;
            this.Exppanelleft.Location = new System.Drawing.Point(0, 0);
            this.Exppanelleft.Name = "Exppanelleft";
            this.Exppanelleft.Size = new System.Drawing.Size(250, 31);
            this.Exppanelleft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.Exppanelleft.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.Exppanelleft.Style.BackColor2.Color = System.Drawing.SystemColors.Control;
            this.Exppanelleft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.Exppanelleft.Style.BorderColor.Color = System.Drawing.SystemColors.ControlDarkDark;
            this.Exppanelleft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.Exppanelleft.Style.GradientAngle = 90;
            this.Exppanelleft.TabIndex = 47;
            this.Exppanelleft.Text = "Stock Summary";
            this.Exppanelleft.TitleHeight = 0;
            this.Exppanelleft.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.Exppanelleft.TitleStyle.BackColor1.Color = System.Drawing.Color.White;
            this.Exppanelleft.TitleStyle.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(154)))));
            this.Exppanelleft.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.Exppanelleft.TitleStyle.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(67)))));
            this.Exppanelleft.TitleStyle.BorderWidth = 2;
            this.Exppanelleft.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.Exppanelleft.TitleStyle.GradientAngle = 90;
            this.Exppanelleft.TitleText = "Stock Summary";
            // 
            // PanelSummary
            // 
            this.PanelSummary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelSummary.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelSummary.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.PanelSummary.Controls.Add(this.LstSummary);
            this.PanelSummary.Location = new System.Drawing.Point(6, 39);
            this.PanelSummary.Name = "PanelSummary";
            this.PanelSummary.Size = new System.Drawing.Size(234, 268);
            // 
            // 
            // 
            this.PanelSummary.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelSummary.Style.BackColorGradientAngle = 90;
            this.PanelSummary.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelSummary.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelSummary.Style.BorderBottomWidth = 1;
            this.PanelSummary.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelSummary.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelSummary.Style.BorderLeftWidth = 1;
            this.PanelSummary.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelSummary.Style.BorderRightWidth = 1;
            this.PanelSummary.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelSummary.Style.BorderTopWidth = 1;
            this.PanelSummary.Style.Class = "";
            this.PanelSummary.Style.CornerDiameter = 4;
            this.PanelSummary.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.PanelSummary.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.PanelSummary.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelSummary.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.PanelSummary.StyleMouseDown.Class = "";
            this.PanelSummary.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.PanelSummary.StyleMouseOver.Class = "";
            this.PanelSummary.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.PanelSummary.TabIndex = 31;
            // 
            // LstSummary
            // 
            // 
            // 
            // 
            this.LstSummary.Border.Class = "ListViewBorder";
            this.LstSummary.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LstSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LstSummary.HideSelection = false;
            listViewItem1.Tag = "13";
            listViewItem1.ToolTipText = "Stock";
            listViewItem2.Tag = "5";
            listViewItem2.ToolTipText = "Purchase Invoice";
            listViewItem3.Tag = "4";
            listViewItem3.ToolTipText = "GRN";
            listViewItem4.Tag = "6";
            listViewItem4.ToolTipText = "Debit Note";
            listViewItem5.Tag = "9";
            listViewItem5.ToolTipText = "Sales Invoice";
            listViewItem6.Tag = "10";
            listViewItem6.ToolTipText = "POS";
            listViewItem7.Tag = "11";
            listViewItem7.ToolTipText = "Credit Note";
            listViewItem8.Tag = "18";
            listViewItem8.ToolTipText = "Delivery Note";
            listViewItem9.Tag = "22";
            listViewItem9.ToolTipText = "Stock Transfer";
            this.LstSummary.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8,
            listViewItem9});
            this.LstSummary.LargeImageList = this.ImgLargeIcon;
            this.LstSummary.Location = new System.Drawing.Point(0, 0);
            this.LstSummary.MultiSelect = false;
            this.LstSummary.Name = "LstSummary";
            this.LstSummary.Size = new System.Drawing.Size(228, 262);
            this.LstSummary.SmallImageList = this.ImgLargeIcon;
            this.LstSummary.TabIndex = 2;
            this.LstSummary.UseCompatibleStateImageBehavior = false;
            this.LstSummary.SelectedIndexChanged += new System.EventHandler(this.LstSummary_SelectedIndexChanged);
            // 
            // pnlRight
            // 
            this.pnlRight.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlRight.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.pnlRight.Controls.Add(this.expandableSplitterMiddle);
            this.pnlRight.Controls.Add(this.DgvSummary);
            this.pnlRight.Controls.Add(this.AdcSummary);
            this.pnlRight.Controls.Add(this.PanelRightMiddle);
            this.pnlRight.Controls.Add(this.panel1);
            this.pnlRight.Controls.Add(this.ExpPanelright);
            this.pnlRight.Controls.Add(this.dockSite8);
            this.pnlRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRight.Location = new System.Drawing.Point(250, 0);
            this.pnlRight.Name = "pnlRight";
            this.pnlRight.Size = new System.Drawing.Size(1045, 570);
            this.pnlRight.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlRight.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.pnlRight.Style.BackColor2.Color = System.Drawing.SystemColors.Control;
            this.pnlRight.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlRight.Style.BorderColor.Color = System.Drawing.SystemColors.Control;
            this.pnlRight.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlRight.Style.GradientAngle = 90;
            this.pnlRight.TabIndex = 105;
            // 
            // expandableSplitterMiddle
            // 
            this.expandableSplitterMiddle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(220)))), ((int)(((byte)(184)))));
            this.expandableSplitterMiddle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(192)))), ((int)(((byte)(148)))));
            this.expandableSplitterMiddle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None;
            this.expandableSplitterMiddle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.None;
            this.expandableSplitterMiddle.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.expandableSplitterMiddle.ExpandableControl = this.PanelRightMiddle;
            this.expandableSplitterMiddle.Expanded = false;
            this.expandableSplitterMiddle.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(45)))), ((int)(((byte)(150)))));
            this.expandableSplitterMiddle.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterMiddle.ExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitterMiddle.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterMiddle.GripDarkColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitterMiddle.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterMiddle.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.expandableSplitterMiddle.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterMiddle.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(142)))), ((int)(((byte)(75)))));
            this.expandableSplitterMiddle.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(139)))));
            this.expandableSplitterMiddle.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterMiddle.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterMiddle.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(45)))), ((int)(((byte)(150)))));
            this.expandableSplitterMiddle.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterMiddle.HotExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitterMiddle.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterMiddle.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(45)))), ((int)(((byte)(150)))));
            this.expandableSplitterMiddle.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterMiddle.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.expandableSplitterMiddle.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterMiddle.Location = new System.Drawing.Point(0, 542);
            this.expandableSplitterMiddle.Name = "expandableSplitterMiddle";
            this.expandableSplitterMiddle.Size = new System.Drawing.Size(1045, 2);
            this.expandableSplitterMiddle.TabIndex = 105;
            this.expandableSplitterMiddle.TabStop = false;
            // 
            // PanelRightMiddle
            // 
            this.PanelRightMiddle.BackColor = System.Drawing.SystemColors.Control;
            this.PanelRightMiddle.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelRightMiddle.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.PanelRightMiddle.Controls.Add(this.panelChart);
            this.PanelRightMiddle.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelRightMiddle.Location = new System.Drawing.Point(0, 346);
            this.PanelRightMiddle.Name = "PanelRightMiddle";
            this.PanelRightMiddle.Size = new System.Drawing.Size(1057, 198);
            // 
            // 
            // 
            this.PanelRightMiddle.Style.BackColor = System.Drawing.SystemColors.Control;
            this.PanelRightMiddle.Style.BackColor2 = System.Drawing.SystemColors.Control;
            this.PanelRightMiddle.Style.BackColorGradientAngle = 90;
            this.PanelRightMiddle.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelRightMiddle.Style.BorderBottomWidth = 1;
            this.PanelRightMiddle.Style.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.PanelRightMiddle.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelRightMiddle.Style.BorderLeftWidth = 1;
            this.PanelRightMiddle.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelRightMiddle.Style.BorderRightWidth = 1;
            this.PanelRightMiddle.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.PanelRightMiddle.Style.BorderTopWidth = 1;
            this.PanelRightMiddle.Style.Class = "";
            this.PanelRightMiddle.Style.CornerDiameter = 4;
            this.PanelRightMiddle.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.PanelRightMiddle.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.PanelRightMiddle.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelRightMiddle.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.PanelRightMiddle.StyleMouseDown.Class = "";
            this.PanelRightMiddle.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.PanelRightMiddle.StyleMouseOver.Class = "";
            this.PanelRightMiddle.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.PanelRightMiddle.TabIndex = 107;
            this.PanelRightMiddle.Visible = false;
            // 
            // panelChart
            // 
            this.panelChart.Controls.Add(this.BtnBack);
            this.panelChart.Controls.Add(this.TrkBarChart);
            this.panelChart.Controls.Add(this.cboChartTypes);
            this.panelChart.Controls.Add(this.ChartSummary);
            this.panelChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelChart.Location = new System.Drawing.Point(0, 0);
            this.panelChart.Name = "panelChart";
            this.panelChart.Size = new System.Drawing.Size(1051, 192);
            this.panelChart.TabIndex = 111;
            // 
            // BtnBack
            // 
            this.BtnBack.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnBack.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnBack.Location = new System.Drawing.Point(11, 3);
            this.BtnBack.Name = "BtnBack";
            this.BtnBack.Size = new System.Drawing.Size(48, 21);
            this.BtnBack.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnBack.TabIndex = 1013;
            this.BtnBack.Text = "<<";
            this.BtnBack.Tooltip = "Show";
            this.BtnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // TrkBarChart
            // 
            this.TrkBarChart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TrkBarChart.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.TrkBarChart.Location = new System.Drawing.Point(999, 31);
            this.TrkBarChart.Maximum = 90;
            this.TrkBarChart.Minimum = -90;
            this.TrkBarChart.Name = "TrkBarChart";
            this.TrkBarChart.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.TrkBarChart.Size = new System.Drawing.Size(45, 144);
            this.TrkBarChart.SmallChange = 10;
            this.TrkBarChart.TabIndex = 1009;
            this.TrkBarChart.Scroll += new System.EventHandler(this.TrkBarChart_Scroll);
            // 
            // cboChartTypes
            // 
            this.cboChartTypes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboChartTypes.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboChartTypes.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboChartTypes.DisplayMember = "Text";
            this.cboChartTypes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboChartTypes.DropDownHeight = 134;
            this.cboChartTypes.FormattingEnabled = true;
            this.cboChartTypes.IntegralHeight = false;
            this.cboChartTypes.ItemHeight = 17;
            this.cboChartTypes.Location = new System.Drawing.Point(885, 3);
            this.cboChartTypes.Name = "cboChartTypes";
            this.cboChartTypes.Size = new System.Drawing.Size(130, 23);
            this.cboChartTypes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboChartTypes.TabIndex = 1008;
            this.cboChartTypes.SelectedIndexChanged += new System.EventHandler(this.cboChartTypes_SelectedIndexChanged);
            // 
            // ChartSummary
            // 
            this.ChartSummary.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ChartSummary.BackSecondaryColor = System.Drawing.Color.Transparent;
            this.ChartSummary.BorderlineColor = System.Drawing.Color.Transparent;
            this.ChartSummary.BorderlineWidth = 4;
            this.ChartSummary.BorderSkin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ChartSummary.BorderSkin.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.VerticalCenter;
            this.ChartSummary.BorderSkin.BorderColor = System.Drawing.Color.Transparent;
            this.ChartSummary.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.ChartSummary.BorderSkin.SkinStyle = System.Windows.Forms.DataVisualization.Charting.BorderSkinStyle.FrameTitle1;
            chartArea1.AlignmentOrientation = ((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations)((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Vertical | System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal)));
            chartArea1.Area3DStyle.Enable3D = true;
            chartArea1.Area3DStyle.IsClustered = true;
            chartArea1.Area3DStyle.IsRightAngleAxes = false;
            chartArea1.Area3DStyle.LightStyle = System.Windows.Forms.DataVisualization.Charting.LightStyle.Realistic;
            chartArea1.Area3DStyle.Rotation = 0;
            chartArea1.AxisY.TitleAlignment = System.Drawing.StringAlignment.Near;
            chartArea1.BackColor = System.Drawing.Color.Silver;
            chartArea1.CursorX.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.CursorX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.CursorX.IsUserEnabled = true;
            chartArea1.CursorX.IsUserSelectionEnabled = true;
            chartArea1.Name = "ChartArea1";
            chartArea1.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ChartSummary.ChartAreas.Add(chartArea1);
            this.ChartSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            legendItem1.BackImageTransparentColor = System.Drawing.Color.Blue;
            legend1.CustomItems.Add(legendItem1);
            legend1.Name = "Legend1";
            this.ChartSummary.Legends.Add(legend1);
            this.ChartSummary.Location = new System.Drawing.Point(0, 0);
            this.ChartSummary.Name = "ChartSummary";
            this.ChartSummary.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series1.BackSecondaryColor = System.Drawing.Color.White;
            series1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(62)))));
            series1.BorderWidth = 0;
            series1.ChartArea = "ChartArea1";
            series1.Color = System.Drawing.Color.Green;
            series1.IsValueShownAsLabel = true;
            series1.LabelForeColor = System.Drawing.Color.Red;
            series1.Legend = "Legend1";
            series1.MarkerBorderColor = System.Drawing.Color.Transparent;
            series1.Name = "Received Qty";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series1.YValuesPerPoint = 2;
            series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Sold Qty";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series2.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Damaged Qty";
            series3.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series3.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            this.ChartSummary.Series.Add(series1);
            this.ChartSummary.Series.Add(series2);
            this.ChartSummary.Series.Add(series3);
            this.ChartSummary.Size = new System.Drawing.Size(1051, 192);
            this.ChartSummary.TabIndex = 51;
            this.ChartSummary.Text = "Summary Chart";
            title1.Name = "StockChart";
            title1.Text = "Stock Chart";
            title1.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Embed;
            title1.ToolTip = "Stock Chart";
            title2.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title2.Name = "ReportHead";
            title2.Text = "Stock Summary";
            title2.ToolTip = "Stock Summary";
            this.ChartSummary.Titles.Add(title1);
            this.ChartSummary.Titles.Add(title2);
            this.ChartSummary.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ChartSummary_MouseDown);
            // 
            // DgvSummary
            // 
            this.DgvSummary.AllowUserToAddRows = false;
            this.DgvSummary.AllowUserToDeleteRows = false;
            this.DgvSummary.AllowUserToResizeColumns = false;
            this.DgvSummary.AllowUserToResizeRows = false;
            this.DgvSummary.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvSummary.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DgvSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvSummary.DefaultCellStyle = dataGridViewCellStyle2;
            this.DgvSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvSummary.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvSummary.Location = new System.Drawing.Point(0, 94);
            this.DgvSummary.Name = "DgvSummary";
            this.DgvSummary.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvSummary.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DgvSummary.RowHeadersVisible = false;
            this.DgvSummary.Size = new System.Drawing.Size(1045, 450);
            this.DgvSummary.TabIndex = 99;
            this.DgvSummary.Visible = false;
            // 
            // AdcSummary
            // 
            this.AdcSummary.AllowDrop = true;
            this.AdcSummary.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.AdcSummary.BackgroundStyle.Class = "TreeBorderKey";
            this.AdcSummary.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.AdcSummary.Columns.Add(this.Period);
            this.AdcSummary.Columns.Add(this.Description);
            this.AdcSummary.Columns.Add(this.Item);
            this.AdcSummary.Columns.Add(this.Batch);
            this.AdcSummary.Columns.Add(this.ReceivedQuantity);
            this.AdcSummary.Columns.Add(this.InvoicedQuantity);
            this.AdcSummary.Columns.Add(this.SoldQuantity);
            this.AdcSummary.Columns.Add(this.DamagedQuantity);
            this.AdcSummary.Columns.Add(this.OpeningQuantity);
            this.AdcSummary.Columns.Add(this.TransferedQuantity);
            this.AdcSummary.Columns.Add(this.ReceivedFromTransfer);
            this.AdcSummary.Columns.Add(this.DemoQuantity);
            this.AdcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AdcSummary.DragDropEnabled = false;
            this.AdcSummary.DragDropNodeCopyEnabled = false;
            this.AdcSummary.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle;
            this.AdcSummary.ExpandWidth = 14;
            this.AdcSummary.GridColumnLineResizeEnabled = true;
            this.AdcSummary.HotTracking = true;
            this.AdcSummary.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.AdcSummary.Location = new System.Drawing.Point(0, 94);
            this.AdcSummary.Name = "AdcSummary";
            this.AdcSummary.NodeStyle = this.ElementStyle6;
            this.AdcSummary.PathSeparator = ";";
            this.AdcSummary.Size = new System.Drawing.Size(1045, 450);
            this.AdcSummary.Styles.Add(this.ElementStyle6);
            this.AdcSummary.Styles.Add(this.ElementStyle3);
            this.AdcSummary.Styles.Add(this.ElementStyle4);
            this.AdcSummary.TabIndex = 100;
            this.AdcSummary.Text = "Documents";
            // 
            // Period
            // 
            this.Period.MinimumWidth = 120;
            this.Period.Name = "Period";
            this.Period.SortingEnabled = false;
            this.Period.Text = "Period";
            this.Period.Width.Absolute = 90;
            // 
            // Description
            // 
            this.Description.Name = "Description";
            this.Description.SortingEnabled = false;
            this.Description.Text = "Description";
            this.Description.Width.Absolute = 90;
            // 
            // Item
            // 
            this.Item.MinimumWidth = 100;
            this.Item.Name = "Item";
            this.Item.SortingEnabled = false;
            this.Item.Text = "Item";
            this.Item.Width.Absolute = 160;
            // 
            // Batch
            // 
            this.Batch.MinimumWidth = 100;
            this.Batch.Name = "Batch";
            this.Batch.SortingEnabled = false;
            this.Batch.Text = "Batch";
            this.Batch.Width.Absolute = 160;
            // 
            // ReceivedQuantity
            // 
            this.ReceivedQuantity.DataFieldName = "ReceivedQuantity";
            this.ReceivedQuantity.MinimumWidth = 80;
            this.ReceivedQuantity.Name = "ReceivedQuantity";
            this.ReceivedQuantity.SortingEnabled = false;
            this.ReceivedQuantity.Text = "Received Qty";
            this.ReceivedQuantity.Width.Absolute = 80;
            // 
            // InvoicedQuantity
            // 
            this.InvoicedQuantity.DataFieldName = "InvoicedQuantity";
            this.InvoicedQuantity.ImageAlignment = DevComponents.AdvTree.eColumnImageAlignment.Right;
            this.InvoicedQuantity.MinimumWidth = 80;
            this.InvoicedQuantity.Name = "InvoicedQuantity";
            this.InvoicedQuantity.SortingEnabled = false;
            this.InvoicedQuantity.Text = "Invoiced Qty";
            this.InvoicedQuantity.Width.Absolute = 80;
            // 
            // SoldQuantity
            // 
            this.SoldQuantity.DataFieldName = "SoldQuantity";
            this.SoldQuantity.ImageAlignment = DevComponents.AdvTree.eColumnImageAlignment.Right;
            this.SoldQuantity.MinimumWidth = 80;
            this.SoldQuantity.Name = "SoldQuantity";
            this.SoldQuantity.SortingEnabled = false;
            this.SoldQuantity.Text = "Sold Qty";
            this.SoldQuantity.Width.Absolute = 100;
            // 
            // DamagedQuantity
            // 
            this.DamagedQuantity.DataFieldName = "DamagedQuantity";
            this.DamagedQuantity.MinimumWidth = 80;
            this.DamagedQuantity.Name = "DamagedQuantity";
            this.DamagedQuantity.SortingEnabled = false;
            this.DamagedQuantity.Text = "Damaged Qty";
            this.DamagedQuantity.Width.Absolute = 50;
            // 
            // OpeningQuantity
            // 
            this.OpeningQuantity.DataFieldName = "OpeningQuantity";
            this.OpeningQuantity.MinimumWidth = 80;
            this.OpeningQuantity.Name = "OpeningQuantity";
            this.OpeningQuantity.SortingEnabled = false;
            this.OpeningQuantity.Text = "Opening Qty";
            this.OpeningQuantity.Width.Absolute = 80;
            // 
            // TransferedQuantity
            // 
            this.TransferedQuantity.Name = "TransferedQuantity";
            this.TransferedQuantity.SortingEnabled = false;
            this.TransferedQuantity.Text = "Transfered Qty";
            this.TransferedQuantity.Width.Absolute = 80;
            // 
            // ReceivedFromTransfer
            // 
            this.ReceivedFromTransfer.Name = "ReceivedFromTransfer";
            this.ReceivedFromTransfer.SortingEnabled = false;
            this.ReceivedFromTransfer.Text = "ReceivedFromTransfer";
            this.ReceivedFromTransfer.Width.Absolute = 80;
            // 
            // DemoQuantity
            // 
            this.DemoQuantity.Name = "DemoQuantity";
            this.DemoQuantity.SortingEnabled = false;
            this.DemoQuantity.StretchToFill = true;
            this.DemoQuantity.Text = "Demo Qty";
            this.DemoQuantity.Width.Absolute = 50;
            // 
            // ElementStyle6
            // 
            this.ElementStyle6.Class = "";
            this.ElementStyle6.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ElementStyle6.Name = "ElementStyle6";
            this.ElementStyle6.TextColor = System.Drawing.SystemColors.ControlText;
            // 
            // ElementStyle3
            // 
            this.ElementStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ElementStyle3.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(224)))), ((int)(((byte)(252)))));
            this.ElementStyle3.BackColorGradientAngle = 90;
            this.ElementStyle3.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ElementStyle3.BorderBottomWidth = 1;
            this.ElementStyle3.BorderColor = System.Drawing.Color.DarkGray;
            this.ElementStyle3.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ElementStyle3.BorderLeftWidth = 1;
            this.ElementStyle3.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ElementStyle3.BorderRightWidth = 1;
            this.ElementStyle3.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ElementStyle3.BorderTopWidth = 1;
            this.ElementStyle3.Class = "";
            this.ElementStyle3.CornerDiameter = 4;
            this.ElementStyle3.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ElementStyle3.Description = "BlueLight";
            this.ElementStyle3.Name = "ElementStyle3";
            this.ElementStyle3.PaddingBottom = 1;
            this.ElementStyle3.PaddingLeft = 1;
            this.ElementStyle3.PaddingRight = 1;
            this.ElementStyle3.PaddingTop = 1;
            this.ElementStyle3.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(84)))), ((int)(((byte)(115)))));
            // 
            // ElementStyle4
            // 
            this.ElementStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(244)))), ((int)(((byte)(213)))));
            this.ElementStyle4.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(216)))), ((int)(((byte)(105)))));
            this.ElementStyle4.BackColorGradientAngle = 90;
            this.ElementStyle4.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ElementStyle4.BorderBottomWidth = 1;
            this.ElementStyle4.BorderColor = System.Drawing.Color.DarkGray;
            this.ElementStyle4.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ElementStyle4.BorderLeftWidth = 1;
            this.ElementStyle4.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ElementStyle4.BorderRightWidth = 1;
            this.ElementStyle4.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ElementStyle4.BorderTopWidth = 1;
            this.ElementStyle4.Class = "";
            this.ElementStyle4.CornerDiameter = 4;
            this.ElementStyle4.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ElementStyle4.Description = "Yellow";
            this.ElementStyle4.Name = "ElementStyle4";
            this.ElementStyle4.PaddingBottom = 1;
            this.ElementStyle4.PaddingLeft = 1;
            this.ElementStyle4.PaddingRight = 1;
            this.ElementStyle4.PaddingTop = 1;
            this.ElementStyle4.TextColor = System.Drawing.Color.Black;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblSalesStatus);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 544);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1045, 26);
            this.panel1.TabIndex = 110;
            // 
            // lblSalesStatus
            // 
            this.lblSalesStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblSalesStatus.CloseButtonVisible = false;
            this.lblSalesStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblSalesStatus.Image = ((System.Drawing.Image)(resources.GetObject("lblSalesStatus.Image")));
            this.lblSalesStatus.Location = new System.Drawing.Point(0, 2);
            this.lblSalesStatus.Name = "lblSalesStatus";
            this.lblSalesStatus.OptionsButtonVisible = false;
            this.lblSalesStatus.Size = new System.Drawing.Size(1045, 24);
            this.lblSalesStatus.TabIndex = 21;
            // 
            // ExpPanelright
            // 
            this.ExpPanelright.CanvasColor = System.Drawing.SystemColors.Control;
            this.ExpPanelright.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ExpPanelright.Controls.Add(this.lblItems);
            this.ExpPanelright.Controls.Add(this.lblBatches);
            this.ExpPanelright.Controls.Add(this.lblWarehouse);
            this.ExpPanelright.Controls.Add(this.lblCompany);
            this.ExpPanelright.Controls.Add(this.cboCompany);
            this.ExpPanelright.Controls.Add(this.cboWarehouse);
            this.ExpPanelright.Controls.Add(this.cboItems);
            this.ExpPanelright.Controls.Add(this.cboBatch);
            this.ExpPanelright.Controls.Add(this.btnShow);
            this.ExpPanelright.Controls.Add(this.lblTo);
            this.ExpPanelright.Controls.Add(this.dtpTo);
            this.ExpPanelright.Controls.Add(this.lblFrom);
            this.ExpPanelright.Controls.Add(this.dtpFrom);
            this.ExpPanelright.Controls.Add(this.lblPeriod);
            this.ExpPanelright.Controls.Add(this.cboPeriod);
            this.ExpPanelright.Dock = System.Windows.Forms.DockStyle.Top;
            this.ExpPanelright.Location = new System.Drawing.Point(0, 0);
            this.ExpPanelright.Name = "ExpPanelright";
            this.ExpPanelright.Size = new System.Drawing.Size(1045, 94);
            this.ExpPanelright.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.ExpPanelright.Style.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.ExpPanelright.Style.BackColor2.Color = System.Drawing.SystemColors.Control;
            this.ExpPanelright.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.ExpPanelright.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(67)))));
            this.ExpPanelright.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.ExpPanelright.Style.GradientAngle = 90;
            this.ExpPanelright.TabIndex = 47;
            this.ExpPanelright.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.ExpPanelright.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.Control;
            this.ExpPanelright.TitleStyle.BackColor2.Color = System.Drawing.SystemColors.Control;
            this.ExpPanelright.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.ExpPanelright.TitleStyle.BorderColor.Color = System.Drawing.SystemColors.ControlDarkDark;
            this.ExpPanelright.TitleStyle.BorderWidth = 2;
            this.ExpPanelright.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.ExpPanelright.TitleStyle.GradientAngle = 90;
            this.ExpPanelright.TitleText = "Stock Summary";
            // 
            // lblItems
            // 
            // 
            // 
            // 
            this.lblItems.BackgroundStyle.Class = "";
            this.lblItems.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblItems.Location = new System.Drawing.Point(213, 33);
            this.lblItems.Name = "lblItems";
            this.lblItems.Size = new System.Drawing.Size(31, 23);
            this.lblItems.TabIndex = 10011;
            this.lblItems.Text = "Items";
            // 
            // lblBatches
            // 
            // 
            // 
            // 
            this.lblBatches.BackgroundStyle.Class = "";
            this.lblBatches.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblBatches.Location = new System.Drawing.Point(652, 32);
            this.lblBatches.Name = "lblBatches";
            this.lblBatches.Size = new System.Drawing.Size(40, 23);
            this.lblBatches.TabIndex = 10010;
            this.lblBatches.Text = "Batches";
            this.lblBatches.Visible = false;
            // 
            // lblWarehouse
            // 
            // 
            // 
            // 
            this.lblWarehouse.BackgroundStyle.Class = "";
            this.lblWarehouse.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWarehouse.Location = new System.Drawing.Point(14, 59);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(58, 23);
            this.lblWarehouse.TabIndex = 10009;
            this.lblWarehouse.Text = "Warehouse";
            // 
            // lblCompany
            // 
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(14, 33);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(48, 23);
            this.lblCompany.TabIndex = 10008;
            this.lblCompany.Text = "Company";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(78, 33);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(121, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 10004;
            this.cboCompany.SelectionChangeCommitted += new System.EventHandler(this.cboCompany_SelectedValueChanged);
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.Changestatus);
            this.cboCompany.SelectedValueChanged += new System.EventHandler(this.cboCompany_SelectedValueChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_Keydown);
            // 
            // cboWarehouse
            // 
            this.cboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWarehouse.DisplayMember = "Text";
            this.cboWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWarehouse.DropDownHeight = 134;
            this.cboWarehouse.FormattingEnabled = true;
            this.cboWarehouse.IntegralHeight = false;
            this.cboWarehouse.ItemHeight = 14;
            this.cboWarehouse.Location = new System.Drawing.Point(78, 59);
            this.cboWarehouse.Name = "cboWarehouse";
            this.cboWarehouse.Size = new System.Drawing.Size(121, 20);
            this.cboWarehouse.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboWarehouse.TabIndex = 10005;
            this.cboWarehouse.SelectionChangeCommitted += new System.EventHandler(this.cboWarehouse_SelectedValueChanged);
            this.cboWarehouse.SelectedIndexChanged += new System.EventHandler(this.Changestatus);
            this.cboWarehouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_Keydown);
            // 
            // cboItems
            // 
            this.cboItems.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboItems.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboItems.DisplayMember = "Text";
            this.cboItems.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboItems.DropDownHeight = 134;
            this.cboItems.FormattingEnabled = true;
            this.cboItems.IntegralHeight = false;
            this.cboItems.ItemHeight = 14;
            this.cboItems.Location = new System.Drawing.Point(298, 33);
            this.cboItems.Name = "cboItems";
            this.cboItems.Size = new System.Drawing.Size(121, 20);
            this.cboItems.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboItems.TabIndex = 10006;
            this.cboItems.SelectionChangeCommitted += new System.EventHandler(this.cboItems_SelectedValueChanged);
            this.cboItems.SelectedValueChanged += new System.EventHandler(this.Changestatus);
            this.cboItems.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_Keydown);
            // 
            // cboBatch
            // 
            this.cboBatch.DisplayMember = "Text";
            this.cboBatch.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboBatch.DropDownHeight = 134;
            this.cboBatch.FormattingEnabled = true;
            this.cboBatch.IntegralHeight = false;
            this.cboBatch.ItemHeight = 14;
            this.cboBatch.Location = new System.Drawing.Point(698, 33);
            this.cboBatch.Name = "cboBatch";
            this.cboBatch.Size = new System.Drawing.Size(121, 20);
            this.cboBatch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboBatch.TabIndex = 10007;
            this.cboBatch.Visible = false;
            this.cboBatch.SelectedValueChanged += new System.EventHandler(this.Changestatus);
            this.cboBatch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_Keydown);
            // 
            // btnShow
            // 
            this.btnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShow.Location = new System.Drawing.Point(629, 58);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(63, 21);
            this.btnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShow.TabIndex = 1012;
            this.btnShow.Text = "Show";
            this.btnShow.Tooltip = "Show";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // lblTo
            // 
            // 
            // 
            // 
            this.lblTo.BackgroundStyle.Class = "";
            this.lblTo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTo.Location = new System.Drawing.Point(431, 59);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(15, 23);
            this.lblTo.TabIndex = 1011;
            this.lblTo.Text = "To";
            // 
            // dtpTo
            // 
            // 
            // 
            // 
            this.dtpTo.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtpTo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpTo.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtpTo.ButtonDropDown.Visible = true;
            this.dtpTo.CustomFormat = "dd MMM yyyy";
            this.dtpTo.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.dtpTo.IsPopupCalendarOpen = false;
            this.dtpTo.Location = new System.Drawing.Point(492, 59);
            // 
            // 
            // 
            this.dtpTo.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpTo.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.dtpTo.MonthCalendar.BackgroundStyle.Class = "";
            this.dtpTo.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpTo.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.dtpTo.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpTo.MonthCalendar.DisplayMonth = new System.DateTime(2011, 11, 1, 0, 0, 0, 0);
            this.dtpTo.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dtpTo.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpTo.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtpTo.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpTo.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtpTo.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.dtpTo.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpTo.MonthCalendar.TodayButtonVisible = true;
            this.dtpTo.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(121, 20);
            this.dtpTo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtpTo.TabIndex = 1010;
            this.dtpTo.ValueChanged += new System.EventHandler(this.Changestatus);
            // 
            // lblFrom
            // 
            // 
            // 
            // 
            this.lblFrom.BackgroundStyle.Class = "";
            this.lblFrom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFrom.Location = new System.Drawing.Point(431, 33);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(56, 23);
            this.lblFrom.TabIndex = 1009;
            this.lblFrom.Text = "From Date";
            // 
            // dtpFrom
            // 
            // 
            // 
            // 
            this.dtpFrom.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtpFrom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpFrom.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtpFrom.ButtonDropDown.Visible = true;
            this.dtpFrom.CustomFormat = "dd MMM yyyy";
            this.dtpFrom.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.dtpFrom.IsPopupCalendarOpen = false;
            this.dtpFrom.Location = new System.Drawing.Point(492, 33);
            // 
            // 
            // 
            this.dtpFrom.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpFrom.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.dtpFrom.MonthCalendar.BackgroundStyle.Class = "";
            this.dtpFrom.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpFrom.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.dtpFrom.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpFrom.MonthCalendar.DisplayMonth = new System.DateTime(2011, 11, 1, 0, 0, 0, 0);
            this.dtpFrom.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dtpFrom.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpFrom.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtpFrom.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpFrom.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtpFrom.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.dtpFrom.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpFrom.MonthCalendar.TodayButtonVisible = true;
            this.dtpFrom.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(121, 20);
            this.dtpFrom.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtpFrom.TabIndex = 1008;
            this.dtpFrom.ValueChanged += new System.EventHandler(this.Changestatus);
            // 
            // lblPeriod
            // 
            // 
            // 
            // 
            this.lblPeriod.BackgroundStyle.Class = "";
            this.lblPeriod.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPeriod.Location = new System.Drawing.Point(213, 59);
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Size = new System.Drawing.Size(76, 23);
            this.lblPeriod.TabIndex = 1007;
            this.lblPeriod.Text = "Summary Type";
            // 
            // cboPeriod
            // 
            this.cboPeriod.DisplayMember = "Text";
            this.cboPeriod.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboPeriod.DropDownHeight = 134;
            this.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPeriod.FormattingEnabled = true;
            this.cboPeriod.IntegralHeight = false;
            this.cboPeriod.ItemHeight = 14;
            this.cboPeriod.Items.AddRange(new object[] {
            this.Yearwise,
            this.Monthwise,
            this.Daywise,
            this.Summary});
            this.cboPeriod.Location = new System.Drawing.Point(298, 59);
            this.cboPeriod.Name = "cboPeriod";
            this.cboPeriod.Size = new System.Drawing.Size(121, 20);
            this.cboPeriod.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboPeriod.TabIndex = 1006;
            this.cboPeriod.SelectedIndexChanged += new System.EventHandler(this.cboPeriod_SelectedIndexChanged);
            this.cboPeriod.SelectedValueChanged += new System.EventHandler(this.Changestatus);
            // 
            // Yearwise
            // 
            this.Yearwise.Text = "Year wise";
            // 
            // Monthwise
            // 
            this.Monthwise.Text = "Month wise";
            // 
            // Daywise
            // 
            this.Daywise.Text = "Day wise";
            // 
            // Summary
            // 
            this.Summary.Text = "Summary";
            // 
            // dockSite8
            // 
            this.dockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite8.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite8.Location = new System.Drawing.Point(0, 0);
            this.dockSite8.Name = "dockSite8";
            this.dockSite8.Size = new System.Drawing.Size(1045, 0);
            this.dockSite8.TabIndex = 93;
            this.dockSite8.TabStop = false;
            // 
            // expandableSplitterBottom
            // 
            this.expandableSplitterBottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(220)))), ((int)(((byte)(184)))));
            this.expandableSplitterBottom.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(192)))), ((int)(((byte)(148)))));
            this.expandableSplitterBottom.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None;
            this.expandableSplitterBottom.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.None;
            this.expandableSplitterBottom.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitterBottom.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(45)))), ((int)(((byte)(150)))));
            this.expandableSplitterBottom.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterBottom.ExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitterBottom.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterBottom.GripDarkColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitterBottom.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterBottom.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.expandableSplitterBottom.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterBottom.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(142)))), ((int)(((byte)(75)))));
            this.expandableSplitterBottom.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(139)))));
            this.expandableSplitterBottom.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterBottom.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterBottom.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(45)))), ((int)(((byte)(150)))));
            this.expandableSplitterBottom.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterBottom.HotExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitterBottom.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterBottom.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(45)))), ((int)(((byte)(150)))));
            this.expandableSplitterBottom.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterBottom.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.expandableSplitterBottom.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterBottom.Location = new System.Drawing.Point(0, 0);
            this.expandableSplitterBottom.Name = "expandableSplitterBottom";
            this.expandableSplitterBottom.Size = new System.Drawing.Size(137, 2);
            this.expandableSplitterBottom.TabIndex = 104;
            this.expandableSplitterBottom.TabStop = false;
            // 
            // dockSite6
            // 
            this.dockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite6.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite6.Location = new System.Drawing.Point(0, 0);
            this.dockSite6.Name = "dockSite6";
            this.dockSite6.Size = new System.Drawing.Size(311, 0);
            this.dockSite6.TabIndex = 93;
            this.dockSite6.TabStop = false;
            // 
            // ErrSummary
            // 
            this.ErrSummary.ContainerControl = this;
            this.ErrSummary.RightToLeft = true;
            // 
            // FrmSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1301, 576);
            this.Controls.Add(this.PanelMain);
            this.Controls.Add(this.DockSite3);
            this.Controls.Add(this.DockSite4);
            this.Controls.Add(this.DockSite1);
            this.Controls.Add(this.DockSite2);
            this.Controls.Add(this.DockSite7);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmSummary";
            this.Text = "Summary";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmSummary_Load);
            this.PanelMain.ResumeLayout(false);
            this.pnlLeft.ResumeLayout(false);
            this.PanelSummary.ResumeLayout(false);
            this.pnlRight.ResumeLayout(false);
            this.PanelRightMiddle.ResumeLayout(false);
            this.panelChart.ResumeLayout(false);
            this.panelChart.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrkBarChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdcSummary)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ExpPanelright.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtpTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrSummary)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Timer tmrSummary;
        internal DevComponents.DotNetBar.DockSite DockSite5;
        internal System.Windows.Forms.ImageList ImgAccounts;
        internal System.Windows.Forms.ImageList ImgLargeIcon;
        internal DevComponents.DotNetBar.DockSite DockSite3;
        internal DevComponents.DotNetBar.DockSite DockSite4;
        internal DevComponents.DotNetBar.DockSite DockSite1;
        internal DevComponents.DotNetBar.DockSite DockSite2;
        internal System.Drawing.Printing.PrintDocument GridPrintDocument;
        internal DevComponents.DotNetBar.DockSite DockSite7;
        internal DevComponents.DotNetBar.DotNetBarManager DotNetBarManager1;
        internal DevComponents.DotNetBar.Controls.GroupPanel PanelMain;
        internal DevComponents.DotNetBar.DockSite dockSite6;
        internal DevComponents.DotNetBar.PanelEx pnlLeft;
        internal DevComponents.DotNetBar.ExpandablePanel Exppanelleft;
        internal DevComponents.DotNetBar.Controls.GroupPanel PanelSummary;
        internal DevComponents.DotNetBar.Controls.ListViewEx LstSummary;
        //internal DevComponents.DotNetBar.Controls.GroupPanel PanelMiddleAAA;
        internal DevComponents.DotNetBar.ExpandableSplitter expandableSplitterBottom;
        internal DevComponents.DotNetBar.PanelEx pnlRight;
        internal DevComponents.DotNetBar.ExpandablePanel ExpPanelright;
        internal DevComponents.DotNetBar.DockSite dockSite8;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem1;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem2;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem3;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem4;
        internal DevComponents.DotNetBar.Controls.GroupPanel PanelRightMiddle;
        private DevComponents.AdvTree.AdvTree AdcSummary;
        private DevComponents.AdvTree.ColumnHeader Period;
        private DevComponents.AdvTree.ColumnHeader Item;
        private DevComponents.AdvTree.ColumnHeader Batch;
        private DevComponents.AdvTree.ColumnHeader ReceivedQuantity;
        private DevComponents.AdvTree.ColumnHeader InvoicedQuantity;
        private DevComponents.AdvTree.ColumnHeader SoldQuantity;
        private DevComponents.AdvTree.ColumnHeader DamagedQuantity;
        private DevComponents.AdvTree.ColumnHeader OpeningQuantity;
        private DevComponents.DotNetBar.ElementStyle ElementStyle6;
        internal DevComponents.DotNetBar.ElementStyle ElementStyle3;
        internal DevComponents.DotNetBar.ElementStyle ElementStyle4;
        internal DevComponents.DotNetBar.Controls.DataGridViewX DgvSummary;
        internal DevComponents.DotNetBar.ExpandableSplitter expandableSplitterMiddle;
        internal DevComponents.DotNetBar.ExpandableSplitter ExpandableSplitterLeft;
        private DevComponents.DotNetBar.LabelX lblTo;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtpTo;
        private DevComponents.DotNetBar.LabelX lblFrom;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtpFrom;
        private DevComponents.DotNetBar.LabelX lblPeriod;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboPeriod;
        private DevComponents.Editors.ComboItem Yearwise;
        private DevComponents.Editors.ComboItem Monthwise;
        private DevComponents.Editors.ComboItem Daywise;
        private DevComponents.DotNetBar.ButtonX btnShow;
        internal System.Windows.Forms.ErrorProvider ErrSummary;
        private System.Windows.Forms.Panel panel1;
        private DevComponents.DotNetBar.Controls.WarningBox lblSalesStatus;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboChartTypes;
        private System.Windows.Forms.Panel panelChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart ChartSummary;
        private System.Windows.Forms.TrackBar TrkBarChart;
        private DevComponents.DotNetBar.LabelX lblItems;
        private DevComponents.DotNetBar.LabelX lblBatches;
        private DevComponents.DotNetBar.LabelX lblWarehouse;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboItems;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboBatch;
        private DevComponents.DotNetBar.ButtonX BtnBack;
        private DevComponents.AdvTree.ColumnHeader Description;
        private DevComponents.Editors.ComboItem Summary;
        private DevComponents.AdvTree.ColumnHeader TransferedQuantity;
        private DevComponents.AdvTree.ColumnHeader ReceivedFromTransfer;
        private DevComponents.AdvTree.ColumnHeader DemoQuantity;

    }
}