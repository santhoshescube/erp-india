﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using DevComponents.AdvTree;

namespace MyBooksERP
{
    struct MovingItem
    {
        public int intOldLocationID;
        public int intOldRowID;
        public int intOldBlockID;
        public int intOldLotID;
        public int intOldItemID;
        public int intOldBatchID;
        public decimal decOldCurrentQty;
        public decimal decOldMovingQty;
        //-----------------------
        public int intNewLocationID;
        public int intNewRowID;
        public int intNewBlockID;
        public int intNewLotID;
        public int intNewItemID;
        public int intNewBatchID;
        public decimal decNewCurrentQty;
        public decimal decNewMovedQty;
    };

    public partial class FrmLocationTransfer : Form
    {
        // Form ID= 99
        //Messages 9550-9600
        private int MintFilterLocationID = 0;
        private int MintFilterRowID = 0;
        private int MintFilterBlockID = 0;
        private int MintFilterLotID = 0;
        private int MintFilterItemID = 0;
        private int MintFilterBatchID = 0;
        private bool MblnFilterBtnOk = false;
        private bool MblnFreeZoneMovement = false;
        private bool MblnDamagedQtyMovement = false;
        private bool MblnExternalDragging = false;//to Check Drag and Drop in Same Tree or External dragDrop
        private bool MblnItemMovedStatus = false;   // Loading Tree If Filter Conditin is applied
        private bool MblnShowErrorMess = false;
        private int MintComapnyID = 0;
        private int MintUserID = 0;
        private int MintTimerInterval = 0;
        private string MstrReportFooter = "";

        private string MstrMessageCaption = "";
        private string MstrCommonMessage = "";
        private ArrayList MaMessageArr;//Displaying message
        private MessageBoxIcon MmessageIcon;

        private MovingItem structMovingItem;//Struct
        private clsBLLLocationTransfer MobclsBLLLocationTransfer;
        private ClsLogWriter MobjClsLogWriter;
        private ClsNotification MobjClsNotification;

        public FrmLocationTransfer()
        {
            InitializeComponent();

            MobclsBLLLocationTransfer = new clsBLLLocationTransfer();
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotification();
            tmrLocationMovements.Interval = ClsCommonSettings.TimerInterval;

            MmessageIcon = MessageBoxIcon.Information;

            MblnShowErrorMess = ClsCommonSettings.ShowErrorMess;
            MintComapnyID = ClsCommonSettings.CompanyID;         // current companyid
            MintUserID = ClsCommonSettings.UserID;            // current userid
            MintTimerInterval = ClsCommonSettings.TimerInterval;     // current companyid
            MstrMessageCaption = ClsCommonSettings.MessageCaption;    //Message caption
            MstrReportFooter = ClsCommonSettings.ReportFooter;
        }

        // Control Events
        private void FrmLocationTransfer_Load(object sender, EventArgs e)
        {
            LoadMessage();
            LoadCombo(1);
        }

        private void CboXCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (Convert.ToInt32(CboXCompany.SelectedValue) > 0)
            {
                LoadCombo(2);//warehouse
            }
        }

        private void ComboBoxX_KeyDown(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;

            if (e.KeyCode == Keys.Enter)
            {
                string strComboName = cbo.Name;

                switch (strComboName)
                {
                    case "CboXCompany":
                        CboXCompany_SelectionChangeCommitted(sender, new EventArgs());
                        break;
                    default:
                        break;
                }
            }
        }

        private void chkBXDamegedQty_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBXDamegedQty.Checked)
                MblnDamagedQtyMovement = true;
            else
                MblnDamagedQtyMovement = false;

            ResetForm(true);
            LoadCombo(2);//Warehouse Combo Filling
        }

        private void bnCancel_Click(object sender, EventArgs e)
        {
            ResetForm(false);
        }

        private void btnXShow_Click(object sender, EventArgs e)
        {
            if (FormValidation())
            {
                DisplayOriginalLocationInfoByFilterCondition();
                DisplayWarehouseInfoDetail();
            }
        }

        private void btnXFilterItem_Click(object sender, EventArgs e)
        {
            if (FormValidation())
            {
                ShowFilterForm(1);//Call From first tree ie Item Location

                if (MblnFilterBtnOk)
                {
                    DisplayOriginalLocationInfoByFilterCondition();
                    ClearFilterVariables();
                   
                    if (MblnItemMovedStatus == true)
                        DisplayWarehouseInfoDetail();
                }
            }
        }

        private void btnXFilterWarehouse_Click(object sender, EventArgs e)
        {
            if (FormValidation())
            {
                ShowFilterForm(2);//Call From Second tree ie Warehouse

                if (MblnFilterBtnOk)
                {
                    DisplayWarehouseInfoDetail();
                    ClearFilterVariables();
                }
            }
        }

        private void btnXExpandALL_Click(object sender, EventArgs e)
        {
            if (advTrvOriginalLoc.Nodes.Count > 0)
                advTrvOriginalLoc.ExpandAll();
        }

        private void btnXCollapse_Click(object sender, EventArgs e)
        {
            if (advTrvOriginalLoc.Nodes.Count > 0)
                advTrvOriginalLoc.CollapseAll();
        }

        private void btnXCollapseWH_Click(object sender, EventArgs e)
        {
            if (advTrvTransferLoc.Nodes.Count > 0)
                advTrvTransferLoc.CollapseAll();
        }

        private void btnExapandAllWH_Click(object sender, EventArgs e)
        {
            if (advTrvTransferLoc.Nodes.Count > 0)
                advTrvTransferLoc.ExpandAll();
        }

        private void btnXClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tmrLocationMovements_Tick(object sender, EventArgs e)
        {
            tmrLocationMovements.Enabled = false;
            errProLocationTransfer.Clear();
            lblLocationMovementstatus.Text = "";
        }

        private void advTrvOriginalLoc_NodeMouseDown(object sender, DevComponents.AdvTree.TreeNodeMouseEventArgs e)
        {
            if (e.Node.Level <= 5)
            {
                if (e.Node.Level ==3 && e.Node.Parent.Parent.Text == "FreeZone")
                {
                    advTrvOriginalLoc.DragDropEnabled = true;
                }
                else
                {
                    advTrvOriginalLoc.DragDropEnabled = false;
                }
            }
            else
            {
                advTrvOriginalLoc.DragDropEnabled = true;
            }
        }

        private void advTrvOriginalLoc_NodeDragStart(object sender, EventArgs e)
        {
            MblnExternalDragging = true;
        }

        private void advTrvTransferLoc_NodeMouseDown(object sender, TreeNodeMouseEventArgs e)
        {
            advTrvOriginalLoc.DragDropEnabled = false;

            if (e.Node.Level <= 5)
            {
                if (e.Node.Level == 3 && e.Node.Parent.Parent.Text == "FreeZone")
                    advTrvTransferLoc.DragDropEnabled = true;
                else
                    advTrvTransferLoc.DragDropEnabled = false;
            }
            else
            {
                advTrvTransferLoc.DragDropEnabled = true;
            }
        }

        private void advTrvOriginalLoc_NodeDragFeedback(object sender, DevComponents.AdvTree.TreeDragFeedbackEventArgs e)
        {
            advTrvTransferLoc.DragDropEnabled = true;
            e.AllowDrop = false;
        }

        private void advTrvTransferLoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                if (advTrvTransferLoc.SelectedNode != null)
                {
                    if (advTrvTransferLoc.SelectedNode.Level == 6)
                    {
                       // DeleteNode();
                    }
                }
            }
        }

        private void advTrvOriginalLoc_AfterNodeDrop(object sender, TreeDragDropEventArgs e)
        {
            MblnExternalDragging = false;
        }

        private void advTrvTransferLoc_BeforeNodeDrop(object sender, TreeDragDropEventArgs e)
        {
            int ItemID = 0;
            Node MovingNode = e.Node.Copy();//BAtch
            Node OldParentNode = e.OldParentNode.Copy();// item
            Node NewParentNode = e.NewParentNode;//newparent Item or Lot

            ItemID = Convert.ToInt32(OldParentNode.Tag);

            if (FormValidation())
            {
                if (MblnExternalDragging == true)//External Draggging
                {
                    if (e.Node.Cells[2].Text == "")
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MaMessageArr, 9552, out MmessageIcon);//"Please Enter Moving Quantity";
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblLocationMovementstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        tmrLocationMovements.Enabled = true;
                        MblnExternalDragging = false;
                        e.Cancel = true;
                        return;
                    }
                    else
                    {
                        decimal qty = 0;
                        if (Decimal.TryParse(e.Node.Cells[2].Text, out qty))
                        {
                            if (e.Node.Cells[2].Text.ToDecimal() <= 0)
                            {
                                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MaMessageArr, 9553, out MmessageIcon);//"Moving Quantity should be greater than zero";
                                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblLocationMovementstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                                tmrLocationMovements.Enabled = true;
                                MblnExternalDragging = false;
                                e.Cancel = true;
                                return;
                            }
                            if (e.Node.Cells[1].Text.ToDecimal() - e.Node.Cells[2].Text.ToDecimal() < 0)
                            {
                                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MaMessageArr, 9553, out MmessageIcon);//"Moving Quantity should be greater than zero";
                                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblLocationMovementstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                                tmrLocationMovements.Enabled = true;
                                MblnExternalDragging = false;
                                e.Cancel = true;
                                return;
                            }
                        }
                        else
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(MaMessageArr, 9554, out MmessageIcon);//"Please Enter a valid  Quantity";
                            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblLocationMovementstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                            tmrLocationMovements.Enabled = true;
                            MblnExternalDragging = false;
                            e.Cancel = true;
                            return;
                        }
                    }
                }

                if (e.NewParentNode.Level == 4)//adding to lot
                {
                    SettingIDs(e);

                    if (MblnExternalDragging == true && structMovingItem.intOldLocationID == Convert.ToInt32(e.NewParentNode.Parent.Parent.Parent.Tag) && structMovingItem.intOldRowID == Convert.ToInt32(e.NewParentNode.Parent.Parent.Tag) &&
                        structMovingItem.intOldBlockID == Convert.ToInt32(e.NewParentNode.Parent.Tag) && structMovingItem.intOldLotID == Convert.ToInt32(e.NewParentNode.Tag))
                    {
                        //moving to same location is blocked
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MaMessageArr, 9555, out MmessageIcon);//"Moving to  Same Loction is not permitted";
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblLocationMovementstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        tmrLocationMovements.Enabled = true;
                        MblnExternalDragging = false;
                        e.Cancel = true;
                        return;
                    }

                    if (e.NewParentNode.HasChildNodes == true)
                    {
                        bool blncall = false;
                        foreach (Node item in e.NewParentNode.Nodes)
                        {
                            if (item.Tag.ToInt32() == ItemID)
                            {
                                blncall = true;
                                advTrvTransferLoc.BeginInvoke(new InsertNodeDelegate(InsertNode), MovingNode, item, OldParentNode, false, e);
                                e.Node.Cells[3].Text = "1";
                            }
                        }
                        if (blncall == false)
                        {
                            advTrvTransferLoc.BeginInvoke(new InsertNodeDelegate(InsertNode), MovingNode, NewParentNode, OldParentNode, true, e);
                            e.Node.Cells[3].Text = "1";
                        }
                    }
                    else
                    {
                        advTrvTransferLoc.BeginInvoke(new InsertNodeDelegate(InsertNode), MovingNode, NewParentNode, OldParentNode, true, e);
                        e.Node.Cells[3].Text = "1";
                    }
                }
                else if (e.NewParentNode.Level == 5)//adding to item
                {
                    SettingIDs(e);

                    if (MblnExternalDragging == true && structMovingItem.intOldLocationID == Convert.ToInt32(e.NewParentNode.Parent.Parent.Parent.Parent.Tag) && structMovingItem.intOldRowID == Convert.ToInt32(e.NewParentNode.Parent.Parent.Parent.Tag) &&
                        structMovingItem.intOldBlockID == Convert.ToInt32(e.NewParentNode.Parent.Parent.Tag) && structMovingItem.intOldLotID == Convert.ToInt32(e.NewParentNode.Parent.Tag))
                    {
                        //moving to same location is blocked
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MaMessageArr, 9555, out MmessageIcon);//"Moving to  Same Loction is not permitted";
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblLocationMovementstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        tmrLocationMovements.Enabled = true;
                        MblnExternalDragging = false;
                        e.Cancel = true;
                        return;
                    }

                    if (ItemID == e.NewParentNode.Tag.ToInt32())
                    {
                        advTrvTransferLoc.BeginInvoke(new InsertNodeDelegate(InsertNode), MovingNode, NewParentNode, OldParentNode, false, e);
                        e.Node.Cells[3].Text = "1";
                    }
                }
                else if (e.NewParentNode.Level==1 && e.NewParentNode.Text == "FreeZone")
                {

                    string s = e.NewParentNode.Cells[4].Text;
                    MblnFreeZoneMovement = true;
                    SettingIDs(e);
                    if (MblnExternalDragging == true && structMovingItem.intOldLocationID == structMovingItem.intNewLocationID &&
                        structMovingItem.intOldRowID == structMovingItem.intNewRowID &&
                        structMovingItem.intOldBlockID == structMovingItem.intNewBlockID && structMovingItem.intOldLotID == structMovingItem.intNewLotID)
                    {
                        //moving to same location is blocked
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(MaMessageArr, 9555, out MmessageIcon);//"Moving to  Same Loction is not permitted";
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblLocationMovementstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                        tmrLocationMovements.Enabled = true;
                        MblnExternalDragging = false;
                        e.Cancel = true;
                        return;
                    }
                    if (e.NewParentNode.HasChildNodes == true)
                    {
                        bool blncall = false;
                        foreach (Node item in e.NewParentNode.Nodes)
                        {
                            if (item.Tag.ToInt32() == ItemID)
                            {
                                blncall = true;
                                advTrvTransferLoc.BeginInvoke(new InsertNodeDelegate(InsertNode), MovingNode, item, OldParentNode, false, e);
                                e.Node.Cells[3].Text = "1";
                            }
                        }
                        if (blncall == false)
                        {
                            advTrvTransferLoc.BeginInvoke(new InsertNodeDelegate(InsertNode), MovingNode, NewParentNode, OldParentNode, true, e);
                            e.Node.Cells[3].Text = "1";
                        }
                    }
                    else
                    {
                        advTrvTransferLoc.BeginInvoke(new InsertNodeDelegate(InsertNode), MovingNode, NewParentNode, OldParentNode, true, e);
                        e.Node.Cells[3].Text = "1";
                    }




                    MblnFreeZoneMovement=false;
                    
                }
                else if (e.NewParentNode.Level == 2 && e.NewParentNode.Parent.Text=="FreeZone")
                {
                    MblnFreeZoneMovement = true;
                    SettingIDs(e);
                    if (ItemID == e.NewParentNode.Tag.ToInt32())
                    {
                        advTrvTransferLoc.BeginInvoke(new InsertNodeDelegate(InsertNode), MovingNode, NewParentNode, OldParentNode, false, e);
                        e.Node.Cells[3].Text = "1";
                    }
                    MblnFreeZoneMovement = false;

                }
                               

                advTrvTransferLoc.Refresh();
            }
            e.Cancel = true;

        }

        // User defined functions
        private delegate void InsertNodeDelegate(Node MovingNode, Node NewParentNode, Node MovingNodesParent, bool blnParent, TreeDragDropEventArgs e);
       
        private void InsertNode(Node MovingNode, Node NewParentNode, Node MovingNodesParent, bool blnParent, TreeDragDropEventArgs e)// for Setting Current Cell
        {
            //Moving Node=Which node to be moved(batch node)
            //NewParentNode=new Parent Node(moving node added to this node)
            //MovingNodesParent=(if moving node have parent then moving nodes parent ie (item node))

            MovingNode.Cells[3].Text = "1"; // if value is 1 then this node is moved

            MblnItemMovedStatus = true;// Loading Tree If Filter Conditin is applied

            MovingNodesParent.Image = imageList2.Images[2];
            MovingNode.Image = imageList2.Images[0];

            if (blnParent)
            {
                int i = 0;

                NewParentNode.Nodes.Add(MovingNodesParent);
                foreach (Node item in NewParentNode.Nodes)
                {
                    i += 1;
                }
                if (i > 0)
                {
                    NewParentNode.Nodes[i - 1].Nodes.Add(MovingNode);
                }
                else
                {
                    NewParentNode.Nodes[0].Nodes.Add(MovingNode);
                }
            }
            else
            {
                NewParentNode.Nodes.Add(MovingNode);

            }
            NewParentNode.Expand();
            SaveItemMovements(); //Save Item Movements    

            if (MblnExternalDragging)//External drag - from  1 st tree to second
            {
                //quantity updating
                MovingNode.Cells[1].Text = e.Node.Cells[2].Text;

                e.Node.Cells[1].Text = Convert.ToString(e.Node.Cells[1].Text.ToDecimal() - e.Node.Cells[2].Text.ToDecimal());
                e.Node.Cells[2].Text = "";
                MblnExternalDragging = false;
            }
            else // internal dragging 2nd tree only
            {
                //removing nodes

                int iNodeCount = 0;
                foreach (Node child in e.Node.Parent.Nodes)
                {
                    iNodeCount += 1;
                }
                if (iNodeCount == 1)
                {
                    e.Node.Parent.Remove();
                }
                else if (iNodeCount > 1)
                {
                    e.Node.Remove();
                }
            }
        }
        private bool SettingIDs(TreeDragDropEventArgs e)
        {
             structMovingItem = new MovingItem();

             if (e.Node.Level == 3)
             {
                 structMovingItem.intOldLocationID = 1;//item-lot-block-row-loction
                 structMovingItem.intOldRowID = 1;
                 structMovingItem.intOldBlockID = 1;
                 structMovingItem.intOldLotID = 1;
             }
             if (e.Node.Level == 6)
             {
                 structMovingItem.intOldLocationID = Convert.ToInt32(e.OldParentNode.Parent.Parent.Parent.Parent.Tag);//item-lot-block-row-loction
                 structMovingItem.intOldRowID = Convert.ToInt32(e.OldParentNode.Parent.Parent.Parent.Tag);
                 structMovingItem.intOldBlockID = Convert.ToInt32(e.OldParentNode.Parent.Parent.Tag);
                 structMovingItem.intOldLotID = Convert.ToInt32(e.OldParentNode.Parent.Tag);
             }
                    structMovingItem.intOldItemID = Convert.ToInt32(e.OldParentNode.Tag);
                    structMovingItem.intOldBatchID = Convert.ToInt32(e.Node.Tag);
                               
                if (MblnExternalDragging == true)
                {
                    structMovingItem.decOldCurrentQty = e.Node.Cells[1].Text.ToDecimal() - e.Node.Cells[2].Text.ToDecimal();
                }
                else
                {
                  structMovingItem.decOldCurrentQty = e.Node.Cells[1].Text.ToDecimal();
                }

                structMovingItem.decOldMovingQty = e.Node.Cells[2].Text.ToDecimal();
                if (e.NewParentNode.Level == 1 || e.NewParentNode.Level == 2)
                {
                    structMovingItem.intNewLocationID = 1;//batch-item-lot-block-row-loction
                    structMovingItem.intNewRowID = 1;
                    structMovingItem.intNewBlockID = 1;
                    structMovingItem.intNewLotID = 1;
                    structMovingItem.intNewItemID = Convert.ToInt32(e.Node.Parent.Tag);
                    structMovingItem.intNewBatchID = Convert.ToInt32(e.Node.Tag);
                    structMovingItem.decNewCurrentQty = e.Node.Cells[2].Text.ToDecimal();
                    structMovingItem.decNewMovedQty = e.Node.Cells[2].Text.ToDecimal();
                }
                if (e.NewParentNode.Level == 4)//adding to lot
                {
                    structMovingItem.intNewLocationID = Convert.ToInt32(e.NewParentNode.Parent.Parent.Parent.Tag);//batch-item-lot-block-row-loction
                    structMovingItem.intNewRowID = Convert.ToInt32(e.NewParentNode.Parent.Parent.Tag);
                    structMovingItem.intNewBlockID = Convert.ToInt32(e.NewParentNode.Parent.Tag);
                    structMovingItem.intNewLotID = Convert.ToInt32(e.NewParentNode.Tag);
                    structMovingItem.intNewItemID = Convert.ToInt32(e.Node.Parent.Tag);
                    structMovingItem.intNewBatchID = Convert.ToInt32(e.Node.Tag);
                    structMovingItem.decNewCurrentQty = e.Node.Cells[2].Text.ToDecimal();
                    structMovingItem.decNewMovedQty = e.Node.Cells[2].Text.ToDecimal();
                }
                else if (e.NewParentNode.Level == 5)//adding to item
                {
                    structMovingItem.intNewLocationID = Convert.ToInt32(e.NewParentNode.Parent.Parent.Parent.Parent.Tag);//batch-item-lot-block-row-loction
                    structMovingItem.intNewRowID = Convert.ToInt32(e.NewParentNode.Parent.Parent.Parent.Tag);
                    structMovingItem.intNewBlockID = Convert.ToInt32(e.NewParentNode.Parent.Parent.Tag);
                    structMovingItem.intNewLotID = Convert.ToInt32(e.NewParentNode.Parent.Tag);
                    structMovingItem.intNewItemID = Convert.ToInt32(e.Node.Parent.Tag);
                    structMovingItem.intNewBatchID = Convert.ToInt32(e.Node.Tag);
                    structMovingItem.decNewCurrentQty = e.Node.Cells[2].Text.ToDecimal();
                    structMovingItem.decNewMovedQty = e.Node.Cells[2].Text.ToDecimal();
                }
                return true;
        }








        //private bool SettingIDs(TreeDragDropEventArgs e)
        //{ //old One
        //    // moving Items key setting 
        //    structMovingItem = new MovingItem();
        //    if (MblnFreeZoneMovement == false)
        //    {
        //        if (MblnExternalDragging == true)
        //        {
        //        string[] sLocationInfo = new string[6];
        //        sLocationInfo = e.Node.Cells[4].Text.Split('-');
        //        //structMovingItem.intOldLocationID = Convert.ToInt32(e.OldParentNode.Parent.Parent.Parent.Parent.Tag);//item-lot-block-row-loction
        //        //structMovingItem.intOldRowID = Convert.ToInt32(e.OldParentNode.Parent.Parent.Parent.Tag);
        //        //structMovingItem.intOldBlockID = Convert.ToInt32(e.OldParentNode.Parent.Parent.Tag);
        //        //structMovingItem.intOldLotID = Convert.ToInt32(e.OldParentNode.Parent.Tag);
        //        //structMovingItem.intOldItemID = Convert.ToInt32(e.OldParentNode.Tag);
        //        //structMovingItem.intOldBatchID = Convert.ToInt32(e.Node.Tag);

        //        structMovingItem.intOldLocationID = sLocationInfo[0].ToInt32();//item-lot-block-row-loction
        //        structMovingItem.intOldRowID = sLocationInfo[1].ToInt32();
        //        structMovingItem.intOldBlockID = sLocationInfo[2].ToInt32();
        //        structMovingItem.intOldLotID = sLocationInfo[3].ToInt32();
        //        structMovingItem.intOldItemID = sLocationInfo[4].ToInt32();
        //        structMovingItem.intOldBatchID = sLocationInfo[5].ToInt32();
                
        //            structMovingItem.decOldCurrentQty = e.Node.Cells[1].Text.ToDecimal() - e.Node.Cells[2].Text.ToDecimal();
        //        }
        //        else
        //        {
        //            string[] sLocationInfo = new string[4];
        //            sLocationInfo = e.Node.Cells[4].Text.Split('-');
        //            //structMovingItem.intOldLocationID = Convert.ToInt32(e.OldParentNode.Parent.Parent.Parent.Parent.Tag);//item-lot-block-row-loction
        //            //structMovingItem.intOldRowID = Convert.ToInt32(e.OldParentNode.Parent.Parent.Parent.Tag);
        //            //structMovingItem.intOldBlockID = Convert.ToInt32(e.OldParentNode.Parent.Parent.Tag);
        //            //structMovingItem.intOldLotID = Convert.ToInt32(e.OldParentNode.Parent.Tag);
        //            //structMovingItem.intOldItemID = Convert.ToInt32(e.OldParentNode.Tag);
        //            //structMovingItem.intOldBatchID = Convert.ToInt32(e.Node.Tag);

        //            structMovingItem.intOldLocationID = sLocationInfo[0].ToInt32();//item-lot-block-row-loction
        //            structMovingItem.intOldRowID = sLocationInfo[1].ToInt32();
        //            structMovingItem.intOldBlockID = sLocationInfo[2].ToInt32();
        //            structMovingItem.intOldLotID = sLocationInfo[3].ToInt32();
        //            structMovingItem.intOldItemID = Convert.ToInt32(e.Node.Parent.Tag);
        //            structMovingItem.intOldBatchID = Convert.ToInt32(e.Node.Tag);

        //            structMovingItem.decOldCurrentQty = e.Node.Cells[1].Text.ToDecimal();
        //        }

        //        structMovingItem.decOldMovingQty = e.Node.Cells[2].Text.ToDecimal();

        //        if (e.NewParentNode.Level == 4)//adding to lot
        //        {
        //            structMovingItem.intNewLocationID = Convert.ToInt32(e.NewParentNode.Parent.Parent.Parent.Tag);//batch-item-lot-block-row-loction
        //            structMovingItem.intNewRowID = Convert.ToInt32(e.NewParentNode.Parent.Parent.Tag);
        //            structMovingItem.intNewBlockID = Convert.ToInt32(e.NewParentNode.Parent.Tag);
        //            structMovingItem.intNewLotID = Convert.ToInt32(e.NewParentNode.Tag);
        //            structMovingItem.intNewItemID = Convert.ToInt32(e.Node.Parent.Tag);
        //            structMovingItem.intNewBatchID = Convert.ToInt32(e.Node.Tag);
        //            structMovingItem.decNewCurrentQty = e.Node.Cells[2].Text.ToDecimal();
        //            structMovingItem.decNewMovedQty = e.Node.Cells[2].Text.ToDecimal();
        //        }
        //        else if (e.NewParentNode.Level == 5)//adding to item
        //        {
        //            structMovingItem.intNewLocationID = Convert.ToInt32(e.NewParentNode.Parent.Parent.Parent.Parent.Tag);//batch-item-lot-block-row-loction
        //            structMovingItem.intNewRowID = Convert.ToInt32(e.NewParentNode.Parent.Parent.Parent.Tag);
        //            structMovingItem.intNewBlockID = Convert.ToInt32(e.NewParentNode.Parent.Parent.Tag);
        //            structMovingItem.intNewLotID = Convert.ToInt32(e.NewParentNode.Parent.Tag);
        //            structMovingItem.intNewItemID = Convert.ToInt32(e.Node.Parent.Tag);
        //            structMovingItem.intNewBatchID = Convert.ToInt32(e.Node.Tag);
        //            structMovingItem.decNewCurrentQty = e.Node.Cells[2].Text.ToDecimal();
        //            structMovingItem.decNewMovedQty = e.Node.Cells[2].Text.ToDecimal();
        //        }
        //    }
        //    else//Freezone Movement
        //    {
        //        if (MblnExternalDragging == true)
        //        {
        //            string[] sLocationInfo = new string[6];
        //            sLocationInfo = e.Node.Cells[4].Text.Split('-');

        //            structMovingItem.intOldLocationID = sLocationInfo[0].ToInt32();//item-lot-block-row-loction
        //            structMovingItem.intOldRowID = sLocationInfo[1].ToInt32();
        //            structMovingItem.intOldBlockID = sLocationInfo[2].ToInt32();
        //            structMovingItem.intOldLotID = sLocationInfo[3].ToInt32();
        //            structMovingItem.intOldItemID = sLocationInfo[4].ToInt32();
        //            structMovingItem.intOldBatchID = sLocationInfo[5].ToInt32();
        //            structMovingItem.decOldCurrentQty = e.Node.Cells[1].Text.ToDecimal() - e.Node.Cells[2].Text.ToDecimal();
        //            structMovingItem.decOldMovingQty = e.Node.Cells[2].Text.ToDecimal();
        //            structMovingItem.intNewLocationID = 1;//batch-item-lot-block-row-loction
        //            structMovingItem.intNewRowID = 1;
        //            structMovingItem.intNewBlockID = 1;
        //            structMovingItem.intNewLotID = 1;
        //            structMovingItem.intNewItemID = sLocationInfo[4].ToInt32();
        //            structMovingItem.intNewBatchID = Convert.ToInt32(e.Node.Tag);
        //            structMovingItem.decNewCurrentQty = e.Node.Cells[2].Text.ToDecimal();
        //            structMovingItem.decNewMovedQty = e.Node.Cells[2].Text.ToDecimal();
        //        }
        //        else
        //        {

        //        }

        //    }
        //    return true;
        //}

        private bool SaveItemMovements()
        {
            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.intCompanyID = Convert.ToInt32(CboXCompany.SelectedValue);
            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.intWarehouseID = Convert.ToInt32(CboXWareHoue.SelectedValue);

            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.intLocationID = structMovingItem.intOldLocationID;
            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.intRowID = structMovingItem.intOldRowID;
            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.intBlockID = structMovingItem.intOldBlockID;
            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.intLotID = structMovingItem.intOldLotID;
            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.intItemID = structMovingItem.intOldItemID;
            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.intBatchID = structMovingItem.intOldBatchID;
            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.decQuantity = structMovingItem.decOldMovingQty;//Moving Qty

            MobclsBLLLocationTransfer.SaveItemMovements(false, MblnDamagedQtyMovement);//update the Existing details//old updating
            //new insert
            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.intLocationID = structMovingItem.intNewLocationID;
            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.intRowID = structMovingItem.intNewRowID;
            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.intBlockID = structMovingItem.intNewBlockID;
            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.intLotID = structMovingItem.intNewLotID;
            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.intItemID = structMovingItem.intNewItemID;
            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.intBatchID = structMovingItem.intNewBatchID;
            MobclsBLLLocationTransfer.clsDTOLLocationTransfer.decQuantity = structMovingItem.decNewMovedQty;

            MobclsBLLLocationTransfer.SaveItemMovements(true, MblnDamagedQtyMovement);// new movement Insert

            MstrCommonMessage = MobjClsNotification.GetErrorMessage(MaMessageArr, 9556, out MmessageIcon);//"Item Moved SuccessFully";
            lblLocationMovementstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);

            tmrLocationMovements.Enabled = true;
            structMovingItem = new MovingItem();//Clearing Values;

            return true;
        }

        private bool TreeTraverseAndUpdate(string strNodeKey, string strNodeQty)
        {//for delete node then updating original tree

            string[] strNodeKeys = new string[6];
            strNodeKeys = strNodeKey.Split('-');
            int intLocID = strNodeKeys[0].ToInt32();
            int intRowID = strNodeKeys[1].ToInt32();
            int intBlockID = strNodeKeys[2].ToInt32();
            int intLotID = strNodeKeys[3].ToInt32();
            int intItemID = strNodeKeys[4].ToInt32();
            int intBatchID = strNodeKeys[5].ToInt32();
            int intQty = strNodeQty.ToInt32();

            Node RootNode = new Node();
            RootNode = advTrvOriginalLoc.Nodes[0];//

            foreach (Node LocNode in RootNode.Nodes)//LOCATION
            {
                if (LocNode.Tag.ToInt32() == intLocID)
                {
                    foreach (Node RowNode in LocNode.Nodes)//ROWS
                    {
                        if (RowNode.Tag.ToInt32() == intRowID)
                        {
                            foreach (Node BlockNode in RowNode.Nodes)//BLOCKS
                            {
                                if (BlockNode.Tag.ToInt32() == intBlockID)
                                {
                                    foreach (Node LotNode in BlockNode.Nodes)//LOT
                                    {
                                        if (LotNode.Tag.ToInt32() == intLotID)
                                        {
                                            foreach (Node ItemNode in LotNode.Nodes)//item
                                            {
                                                if (ItemNode.Tag.ToInt32() == intItemID)
                                                {
                                                    foreach (Node BatchNode in ItemNode.Nodes)
                                                    {
                                                        if (BatchNode.Tag.ToInt32() == intBatchID)
                                                        {
                                                            if (BatchNode.Cells[3].Text == "1")
                                                            {
                                                                BatchNode.Cells[1].Text = Convert.ToString(BatchNode.Cells[1].Text.ToInt32() + intQty);

                                                            }
                                                        }//if
                                                    }//foreach (Node BatchNode
                                                }//if

                                            }//foreach (Node ItemNode
                                        }//if
                                    }//foreach (Node LotNode
                                }//if
                            }//foreach (Node BlockNode
                        }//if
                    }//foreach (Node RowNode
                }//if
            }//foreach (Node LocNode


            return true;
        }

        private bool DeleteNode()
        {
            if (advTrvTransferLoc.SelectedNode != null)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MaMessageArr, 9557, out MmessageIcon);//"do you want to Delete this batch";
                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MmessageIcon) == DialogResult.No)
                    return false;
                int iNodeCount = 0;

                string strNodeKey = advTrvTransferLoc.SelectedNode.Cells[4].Text;
                string strNodeQty = advTrvTransferLoc.SelectedNode.Cells[2].Text;

                foreach (Node child in advTrvTransferLoc.SelectedNode.Parent.Nodes)
                {
                    iNodeCount += 1;
                }
                if (iNodeCount == 1)
                {
                    advTrvTransferLoc.SelectedNode.Parent.Remove();
                }
                else if (iNodeCount > 1)
                {
                    advTrvTransferLoc.SelectedNode.Remove();
                }
                TreeTraverseAndUpdate(strNodeKey, strNodeQty);
            }
            return true;
        }

        private bool FormValidation()
        {
            if (Convert.ToInt32(CboXCompany.SelectedValue) <= 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MaMessageArr, 9550, out MmessageIcon);//"Please Select Company";
                errProLocationTransfer.SetError(CboXCompany, MstrCommonMessage.Replace("#", "").Trim());
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblLocationMovementstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                tmrLocationMovements.Enabled = true;
                CboXCompany.Focus();
                return false;
            }
            if (Convert.ToInt32(CboXWareHoue.SelectedValue) <= 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(MaMessageArr, 9551, out MmessageIcon);//"Please Select a warehouse";
                errProLocationTransfer.SetError(CboXWareHoue, MstrCommonMessage.Replace("#", "").Trim());
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblLocationMovementstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                tmrLocationMovements.Enabled = true;
                CboXWareHoue.Focus();
                return false;
            }
            return true;
        }

        private void LoadMessage()
        {
            try
            {
                MaMessageArr = new ArrayList();
                MaMessageArr = MobjClsNotification.FillMessageArray(Convert.ToInt32(FormID.LocationTransfer), 4);
            }
            catch (Exception ex)
            {
                if (MblnShowErrorMess)
                    MessageBox.Show("Error in LoadMessage() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in LoadMessage() " + ex.Message, 2);
            }
        }

        private bool LoadCombo(int intType)
        {
            DataTable datCombos = new DataTable();

            if (intType == 0 || intType == 1)//Company Combo
            {
                if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.CompanyID,CM.[Name]", "STItemLocation IL inner join CompanyMaster CM on IL.CompanyID=Cm.CompanyID", "" });
                }
                else
                {
                    try
                    {
                        datCombos = MobclsBLLLocationTransfer.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.ItemAllocCompany);
                    }
                    catch { }
                }
                CboXCompany.ValueMember = "CompanyID";
                CboXCompany.DisplayMember = "Name";
                CboXCompany.DataSource = datCombos;
                CboXCompany.SelectedIndex = -1;
            }

            if (intType == 0 || intType == 2)//Warehouse Combo
            {
                if (MblnDamagedQtyMovement == true)
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.WarehouseID,WH.WarehouseName", "STItemLocation IL inner join STWarehouse WH on IL.WarehouseID=WH.WarehouseID", "IL.CompanyID=" + Convert.ToInt32(CboXCompany.SelectedValue) + "  and IL.DamagedQuantity>0" });
                else
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.WarehouseID,WH.WarehouseName", "STItemLocation IL inner join STWarehouse WH on IL.WarehouseID=WH.WarehouseID", "IL.CompanyID=" + Convert.ToInt32(CboXCompany.SelectedValue) + "" });

                CboXWareHoue.ValueMember = "WarehouseID";
                CboXWareHoue.DisplayMember = "WarehouseName";
                CboXWareHoue.DataSource = datCombos;
                CboXWareHoue.SelectedIndex = -1;
            }
            return true;
        }

        private void ResetForm(bool blnStatus)
        {
            if (blnStatus == false)
            {
                CboXCompany.Text = "";
                CboXCompany.SelectedIndex = -1;
            }
            CboXWareHoue.Text = "";
            CboXWareHoue.SelectedIndex = -1;

            advTrvOriginalLoc.Nodes.Clear();
            advTrvTransferLoc.Nodes.Clear();
            ClearFilterVariables();
        }

        private bool DisplayOriginalLocationInfoByFilterCondition()
        {
            try
            {//Display original Location info HANDLES THE FILTER cONDITION

                int intWarehouseID = Convert.ToInt32(CboXWareHoue.SelectedValue);
                int intLocID = MintFilterLocationID;
                int intRowID = MintFilterRowID;
                int intBlockID = MintFilterBlockID;
                int intLotID = MintFilterLotID;
                int intItemID = MintFilterItemID;
                int intBatchID = MintFilterBatchID;

                advTrvOriginalLoc.Nodes.Clear();
                DevComponents.AdvTree.Node RootNode = new DevComponents.AdvTree.Node(CboXWareHoue.Text);
                advTrvOriginalLoc.Nodes.Add(RootNode);
                RootNode = advTrvOriginalLoc.Nodes[0];
                RootNode.Image = imageList1.Images[0];
                
                if (RootNode == null)
                    return false;

                DataTable DtWarehouseDetail = MobclsBLLLocationTransfer.GetItemLocationDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID, MblnDamagedQtyMovement);
                
                if (DtWarehouseDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < DtWarehouseDetail.Rows.Count; i++)
                    {
                        DevComponents.AdvTree.Node LocationNode;
                        LocationNode = new DevComponents.AdvTree.Node();
                        LocationNode.Image = imageList2.Images[3];
                        LocationNode.Name = Convert.ToString(DtWarehouseDetail.Rows[i]["Location"]);
                        LocationNode.Text = Convert.ToString(DtWarehouseDetail.Rows[i]["Location"]);
                        LocationNode.Tag = Convert.ToInt32(DtWarehouseDetail.Rows[i]["LocationID"]);
                        RootNode.Nodes.Add(LocationNode);//LOCATION
                       
                        if (MintFilterLocationID <= 0)
                        {
                            intLocID = Convert.ToInt32(DtWarehouseDetail.Rows[i]["LocationID"]);
                        }

                        intRowID = MintFilterRowID;
                        intBlockID = MintFilterBlockID;
                        intLotID = MintFilterLotID;
                        intItemID = MintFilterItemID;
                        intBatchID = MintFilterBatchID;

                        DataTable DtRows = MobclsBLLLocationTransfer.GetItemRowDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID, MblnDamagedQtyMovement);

                        for (int j = 0; j < DtRows.Rows.Count; j++)
                        {
                            if (Convert.ToInt32(DtRows.Rows[j]["WarehouseLocationTypeID"]) != Convert.ToInt32(WarehouseLocationTypeReference.FreeZone))
                            {
                                DevComponents.AdvTree.Node RowNode;
                                RowNode = new DevComponents.AdvTree.Node();
                                RowNode.Image = imageList2.Images[5];
                                RowNode.Name = Convert.ToString(DtRows.Rows[j]["RowNumber"]);
                                RowNode.Text = Convert.ToString(DtRows.Rows[j]["RowNumber"]);
                                RowNode.Tag = Convert.ToInt32(DtRows.Rows[j]["RowID"]);
                                RootNode.Nodes[i].Nodes.Add(RowNode);
                            }
                            if (MintFilterRowID <= 0)
                            {
                                intRowID = Convert.ToInt32(DtRows.Rows[j]["RowID"]);
                            }
                            intBlockID = MintFilterBlockID;
                            intLotID = MintFilterLotID;
                            intItemID = MintFilterItemID;
                            intBatchID = MintFilterBatchID;

                            DataTable DtBlocks = MobclsBLLLocationTransfer.GetItemBlockDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID, MblnDamagedQtyMovement);

                            for (int k = 0; k < DtBlocks.Rows.Count; k++)
                            {
                                if (Convert.ToInt32(DtBlocks.Rows[k]["WarehouseLocationTypeID"]) != Convert.ToInt32(WarehouseLocationTypeReference.FreeZone))
                                {
                                    DevComponents.AdvTree.Node BlockNode;
                                    BlockNode = new DevComponents.AdvTree.Node();
                                    BlockNode.Image = imageList2.Images[1];
                                    BlockNode.Name = Convert.ToString(DtBlocks.Rows[k]["BlockNumber"]);
                                    BlockNode.Text = Convert.ToString(DtBlocks.Rows[k]["BlockNumber"]);
                                    BlockNode.Tag = Convert.ToInt32(DtBlocks.Rows[k]["BlockID"]);
                                    RootNode.Nodes[i].Nodes[j].Nodes.Add(BlockNode);
                                }
                                if (MintFilterBlockID <= 0)
                                {
                                    intBlockID = Convert.ToInt32(DtBlocks.Rows[k]["BlockID"]);
                                }

                                intLotID = MintFilterLotID;
                                intItemID = MintFilterItemID;
                                intBatchID = MintFilterBatchID;

                                DataTable DtLots = MobclsBLLLocationTransfer.GetItemLotDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID, MblnDamagedQtyMovement);

                                for (int l = 0; l < DtLots.Rows.Count; l++)
                                {
                                    if (Convert.ToInt32(DtLots.Rows[l]["WarehouseLocationTypeID"]) != Convert.ToInt32(WarehouseLocationTypeReference.FreeZone))
                                    {
                                        DevComponents.AdvTree.Node LotNode;
                                        LotNode = new DevComponents.AdvTree.Node();
                                        LotNode.Image = imageList2.Images[4];
                                        LotNode.Name = Convert.ToString(DtLots.Rows[l]["LotNumber"]);
                                        LotNode.Text = Convert.ToString(DtLots.Rows[l]["LotNumber"]);
                                        LotNode.Tag = Convert.ToInt32(DtLots.Rows[l]["LotID"]);
                                        RootNode.Nodes[i].Nodes[j].Nodes[k].Nodes.Add(LotNode);
                                    }
                                    if (MintFilterLotID <= 0)
                                    {
                                        intLotID = Convert.ToInt32(DtLots.Rows[l]["LotID"]);
                                    }
                                    intItemID = MintFilterItemID;
                                    intBatchID = MintFilterBatchID;

                                    DataTable DtItem = MobclsBLLLocationTransfer.GetItemItemDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID, MblnDamagedQtyMovement);
                                    
                                    for (int m = 0; m < DtItem.Rows.Count; m++)
                                    {
                                        if (Convert.ToInt32(DtLots.Rows[l]["WarehouseLocationTypeID"]) != Convert.ToInt32(WarehouseLocationTypeReference.FreeZone))
                                        {
                                            DevComponents.AdvTree.Node ItemNode;
                                            ItemNode = new DevComponents.AdvTree.Node();
                                            ItemNode.Image = imageList2.Images[2];
                                            ItemNode.Name = Convert.ToString(DtItem.Rows[m]["Description"]);
                                            ItemNode.Text = Convert.ToString(DtItem.Rows[m]["Description"]);
                                            ItemNode.Tag = Convert.ToInt32(DtItem.Rows[m]["ItemID"]);
                                            RootNode.Nodes[i].Nodes[j].Nodes[k].Nodes[l].Nodes.Add(ItemNode);
                                        }
                                        else
                                        {
                                            DevComponents.AdvTree.Node ItemNode;
                                            ItemNode = new DevComponents.AdvTree.Node();
                                            ItemNode.Image = imageList2.Images[2];
                                            ItemNode.Name = Convert.ToString(DtItem.Rows[m]["Description"]);
                                            ItemNode.Text = Convert.ToString(DtItem.Rows[m]["Description"]);
                                            ItemNode.Tag = Convert.ToInt32(DtItem.Rows[m]["ItemID"]);
                                            RootNode.Nodes[i].Nodes.Add(ItemNode);
                                        }
                                        if (MintFilterItemID <= 0)
                                        {
                                            intItemID = Convert.ToInt32(DtItem.Rows[m]["ItemID"]);
                                        }
                                        intBatchID = MintFilterBatchID;

                                        DataTable DtBatch = MobclsBLLLocationTransfer.GetItemBatchDetailByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID, intItemID, intBatchID, MblnDamagedQtyMovement);
                                        
                                        for (int n = 0; n < DtBatch.Rows.Count; n++)
                                        {
                                            if (Convert.ToInt32(DtLots.Rows[l]["WarehouseLocationTypeID"]) != Convert.ToInt32(WarehouseLocationTypeReference.FreeZone))
                                            {
                                                DevComponents.AdvTree.Node BatchNode;
                                                BatchNode = new DevComponents.AdvTree.Node();
                                                BatchNode.Image = imageList2.Images[0];
                                                DevComponents.AdvTree.Cell cell = new DevComponents.AdvTree.Cell();
                                                BatchNode.Cells.Add(cell);
                                                cell = new DevComponents.AdvTree.Cell();
                                                BatchNode.Cells.Add(cell);
                                                cell = new DevComponents.AdvTree.Cell();
                                                BatchNode.Cells.Add(cell);
                                                cell = new DevComponents.AdvTree.Cell();
                                                BatchNode.Cells.Add(cell);
                                                BatchNode.Name = Convert.ToString(DtBatch.Rows[n]["BatchNo"]);//+ " " + Convert.ToString(DtBatch.Rows[n]["Quantity"]);
                                                BatchNode.Text = Convert.ToString(DtBatch.Rows[n]["BatchNo"]);//+ " " + Convert.ToString(DtBatch.Rows[n]["Quantity"]);
                                                BatchNode.Tag = Convert.ToInt32(DtBatch.Rows[n]["BatchID"]);
                                                RootNode.Nodes[i].Nodes[j].Nodes[k].Nodes[l].Nodes[m].Nodes.Add(BatchNode);
                                                RootNode.Nodes[i].Nodes[j].Nodes[k].Nodes[l].Nodes[m].Nodes[n].Cells[1].Text = Convert.ToString(DtBatch.Rows[n]["Quantity"]);
                                                RootNode.Nodes[i].Nodes[j].Nodes[k].Nodes[l].Nodes[m].Nodes[n].Cells[2].Text = Convert.ToString(DtBatch.Rows[n]["Quantity"]);
                                                RootNode.Nodes[i].Nodes[j].Nodes[k].Nodes[l].Nodes[m].Nodes[n].Cells[3].Text = "0";//Setting value as zero for identify moved nodes--if the node is moved then value is changed to 1
                                                RootNode.Nodes[i].Nodes[j].Nodes[k].Nodes[l].Nodes[m].Nodes[n].Cells[4].Text = DtWarehouseDetail.Rows[i]["LocationID"] + "-" + DtRows.Rows[j]["RowID"] + "-" + DtBlocks.Rows[k]["BlockID"] + "-" + DtLots.Rows[l]["LotID"] + "-" + DtItem.Rows[m]["ItemID"] + "-" + DtBatch.Rows[n]["BatchID"];// for identify the node
                                                DevComponents.AdvTree.Node t = new DevComponents.AdvTree.Node();
                                                t = RootNode.Nodes[i];
                                                t.ExpandAll();
                                            }
                                            else
                                            {
                                                DevComponents.AdvTree.Node BatchNode;
                                                BatchNode = new DevComponents.AdvTree.Node();
                                                BatchNode.Image = imageList2.Images[0];
                                                DevComponents.AdvTree.Cell cell = new DevComponents.AdvTree.Cell();
                                                BatchNode.Cells.Add(cell);
                                                cell = new DevComponents.AdvTree.Cell();
                                                BatchNode.Cells.Add(cell);
                                                cell = new DevComponents.AdvTree.Cell();
                                                BatchNode.Cells.Add(cell);
                                                cell = new DevComponents.AdvTree.Cell();
                                                BatchNode.Cells.Add(cell);
                                                BatchNode.Name = Convert.ToString(DtBatch.Rows[n]["BatchNo"]);//+ " " + Convert.ToString(DtBatch.Rows[n]["Quantity"]);
                                                BatchNode.Text = Convert.ToString(DtBatch.Rows[n]["BatchNo"]);//+ " " + Convert.ToString(DtBatch.Rows[n]["Quantity"]);
                                                BatchNode.Tag = Convert.ToInt32(DtBatch.Rows[n]["BatchID"]);
                                                RootNode.Nodes[i].Nodes[m].Nodes.Add(BatchNode);
                                                RootNode.Nodes[i].Nodes[m].Nodes[n].Cells[1].Text = Convert.ToString(DtBatch.Rows[n]["Quantity"]);
                                                RootNode.Nodes[i].Nodes[m].Nodes[n].Cells[2].Text = Convert.ToString(DtBatch.Rows[n]["Quantity"]);
                                                RootNode.Nodes[i].Nodes[m].Nodes[n].Cells[3].Text = "0";//Setting value as zero for identify moved nodes--if the node is moved then value is changed to 1
                                                RootNode.Nodes[i].Nodes[m].Nodes[n].Cells[4].Text = DtWarehouseDetail.Rows[i]["LocationID"] + "-" + DtRows.Rows[j]["RowID"] + "-" + DtBlocks.Rows[k]["BlockID"] + "-" + DtLots.Rows[l]["LotID"] + "-" + DtItem.Rows[m]["ItemID"] + "-" + DtBatch.Rows[n]["BatchID"];// for identify the node
                                                DevComponents.AdvTree.Node t = new DevComponents.AdvTree.Node();
                                                t = RootNode.Nodes[i];
                                                t.ExpandAll();
                                            }
                                        }// for (int n = 0; n < DtBatch.Rows.Count; n++)

                                    }//for (int m = 0; m < DtItem.Rows.Count; m++)

                                }//for (int l = 0; l < DtLots.Rows.Count; l++)
                            }//for (int k = 0; k < DtBlocks.Rows.Count; k++)
                        }// for (int j = 0; j < DtRows.Rows.Count; j++)
                    }//for (int i = 0; i < DtWarehouseDetail.Rows.Count; i++)//location
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on DisplayWarehouseInfoDetail " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("DisplayWarehouseInfoDetail " + Ex.Message.ToString());
            }
            return true;
        }

        private void DisplayWarehouseInfoDetail()//.........DataDisplaying
        {
            try
            {//Using

                int intWarehouseID = Convert.ToInt32(CboXWareHoue.SelectedValue);
                int intLocID = MintFilterLocationID;
                int intRowID = MintFilterRowID;
                int intBlockID = MintFilterBlockID;
                int intLotID = MintFilterLotID;

                advTrvTransferLoc.Nodes.Clear();
                DevComponents.AdvTree.Node RootNode = new DevComponents.AdvTree.Node(CboXWareHoue.Text);
                RootNode.Image = imageList1.Images[0];
                advTrvTransferLoc.Nodes.Add(RootNode);


                if (RootNode == null)
                    return;

                MblnItemMovedStatus = false; //Item Moved Or Not

                DataTable DtWarehouseDetail = MobclsBLLLocationTransfer.GetWarehouseLocInfoByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID);
                
                if (DtWarehouseDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < DtWarehouseDetail.Rows.Count; i++)
                    {
                        DevComponents.AdvTree.Node LocationNode;
                        LocationNode = new DevComponents.AdvTree.Node();
                        LocationNode.Image = imageList2.Images[3];
                        LocationNode.Name = Convert.ToString(DtWarehouseDetail.Rows[i]["Location"]);
                        LocationNode.Text = Convert.ToString(DtWarehouseDetail.Rows[i]["Location"]);
                        LocationNode.Tag = Convert.ToString(DtWarehouseDetail.Rows[i]["LocationID"]);
                        RootNode.Nodes.Add(LocationNode);//LOCATION
                        
                        if (MintFilterLocationID <= 0)
                        {
                            intLocID = Convert.ToInt32(DtWarehouseDetail.Rows[i]["LocationID"]);
                        }

                        intRowID = MintFilterRowID;
                        intBlockID = MintFilterBlockID;
                        intLotID = MintFilterLotID;

                        DataTable DtRows = MobclsBLLLocationTransfer.GetWarehouseRowInfoByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID);
                        
                        for (int j = 0; j < DtRows.Rows.Count; j++)
                        {
                            if (Convert.ToInt32(DtRows.Rows[j]["WarehouseLocationTypeID"]) != Convert.ToInt32(WarehouseLocationTypeReference.FreeZone))
                            {

                                DevComponents.AdvTree.Node RowNode;
                                RowNode = new DevComponents.AdvTree.Node();
                                RowNode.Image = imageList2.Images[5];
                                RowNode.Name = Convert.ToString(DtRows.Rows[j]["RowNumber"]);
                                RowNode.Text = Convert.ToString(DtRows.Rows[j]["RowNumber"]);
                                RowNode.Tag = Convert.ToInt32(DtRows.Rows[j]["RowID"]);
                                RootNode.Nodes[i].Nodes.Add(RowNode);
                            }
                            if (MintFilterRowID <= 0)
                            {
                                intRowID = Convert.ToInt32(DtRows.Rows[j]["RowID"]);
                            }

                            intBlockID = MintFilterBlockID;
                            intLotID = MintFilterLotID;

                            DataTable DtBlocks = MobclsBLLLocationTransfer.GetWarehouseBlockInfoByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID);
                           
                            for (int k = 0; k < DtBlocks.Rows.Count; k++)
                            {
                                if (Convert.ToInt32(DtBlocks.Rows[k]["WarehouseLocationTypeID"]) != Convert.ToInt32(WarehouseLocationTypeReference.FreeZone))
                                {

                                    DevComponents.AdvTree.Node BlockNode;
                                    BlockNode = new DevComponents.AdvTree.Node();
                                    BlockNode.Image = imageList2.Images[1];
                                    BlockNode.Name = Convert.ToString(DtBlocks.Rows[k]["BlockNumber"]);
                                    BlockNode.Text = Convert.ToString(DtBlocks.Rows[k]["BlockNumber"]);
                                    BlockNode.Tag = Convert.ToInt32(DtBlocks.Rows[k]["BlockID"]);
                                    RootNode.Nodes[i].Nodes[j].Nodes.Add(BlockNode);
                                }
                                if (MintFilterBlockID <= 0)
                                {
                                    intBlockID = Convert.ToInt32(DtBlocks.Rows[k]["BlockID"]);
                                }

                                intLotID = MintFilterLotID;
                                DataTable DtLots = MobclsBLLLocationTransfer.GetWarehouseLotInfoByFilterCondition(intWarehouseID, intLocID, intRowID, intBlockID, intLotID);
                                
                                for (int l = 0; l < DtLots.Rows.Count; l++)
                                {

                                    if (Convert.ToInt32(DtLots.Rows[l]["WarehouseLocationTypeID"]) != Convert.ToInt32(WarehouseLocationTypeReference.FreeZone))
                                    {



                                        DevComponents.AdvTree.Node LotNode;
                                        LotNode = new DevComponents.AdvTree.Node();
                                        LotNode.Image = imageList2.Images[4];

                                        DevComponents.AdvTree.Cell cell = new DevComponents.AdvTree.Cell();
                                        LotNode.Cells.Add(cell);
                                        cell = new DevComponents.AdvTree.Cell();
                                        LotNode.Cells.Add(cell);
                                        cell = new DevComponents.AdvTree.Cell();
                                        LotNode.Cells.Add(cell);
                                        cell = new DevComponents.AdvTree.Cell();
                                        LotNode.Cells.Add(cell);
                                        LotNode.Name = Convert.ToString(DtLots.Rows[l]["LotNumber"]);
                                        LotNode.Text = Convert.ToString(DtLots.Rows[l]["LotNumber"]);
                                        LotNode.Tag = Convert.ToInt32(DtLots.Rows[l]["LotID"]);
                                        RootNode.Nodes[i].Nodes[j].Nodes[k].Nodes.Add(LotNode);
                                        RootNode.Nodes[i].Nodes[j].Nodes[k].Nodes[l].Cells[4].Text = DtWarehouseDetail.Rows[i]["LocationID"] + "-" + DtRows.Rows[j]["RowID"] + "-" + DtBlocks.Rows[k]["BlockID"] + "-" + DtLots.Rows[l]["LotID"];// for identify the node
                                    }
                                    else
                                    {
                                        DevComponents.AdvTree.Cell cell = new DevComponents.AdvTree.Cell();
                                        RootNode.Nodes[i].Cells.Add(cell);
                                        cell = new DevComponents.AdvTree.Cell();
                                        RootNode.Nodes[i].Cells.Add(cell);
                                        cell = new DevComponents.AdvTree.Cell();
                                        RootNode.Nodes[i].Cells.Add(cell);
                                        cell = new DevComponents.AdvTree.Cell();
                                        RootNode.Nodes[i].Cells.Add(cell);
                                                                
                                        RootNode.Nodes[i].Cells[4].Text = DtWarehouseDetail.Rows[i]["LocationID"] + "-" + DtRows.Rows[j]["RowID"] + "-" + DtBlocks.Rows[k]["BlockID"] + "-" + DtLots.Rows[l]["LotID"];// for identify the node
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on DisplayWarehouseInfoDetail " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("DisplayWarehouseInfoDetail " + Ex.Message.ToString());
            }
        }

        private void ShowFilterForm(int intType)
        {
            using (FrmLocationFilter ObjFilter = new FrmLocationFilter(intType, Convert.ToInt32(CboXWareHoue.SelectedValue), MblnDamagedQtyMovement))
            {
                ObjFilter.ShowDialog();
                MintFilterLocationID = ObjFilter.PintFilterLocationID;
                MintFilterRowID = ObjFilter.PintFilterRowID;
                MintFilterBlockID = ObjFilter.PintFilterBlockID;
                MintFilterLotID = ObjFilter.PintFilterLotID;
                MintFilterItemID = ObjFilter.PintFilterItemID;
                MintFilterBatchID = ObjFilter.PintFilterBatchID;
                MblnFilterBtnOk = ObjFilter.PblnBtnOk;
            }
        }

        private void ClearFilterVariables()
        {
            MintFilterLocationID = 0;
            MintFilterRowID = 0;
            MintFilterBlockID = 0;
            MintFilterLotID = 0;
            MintFilterItemID = 0;
            MintFilterBatchID = 0;
            MblnFilterBtnOk = false;
        }
    }
}
