﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmMaterialReturn : DevComponents.DotNetBar.Office2007Form
    {
        private enum MaterialReturnMessageCodes
        {
            MaterialReturnNoExists = 9751,
            MaterialReturnNoExistsContinue = 9752,
            ReturnedQtyExceedsIssuedQty = 9753,
            ItemSold = 9754,
            TransferedQtyExceedsStock = 9755,
            EnterMaterialReturnNo = 9756,
            EnterProject = 9757
        }


        clsBLLMaterialReturnMaster MobjClsBLLMaterialReturnMaster;//Object Of clsBllMaterialReturnMaster
        string MstrCommonMessage;                   // for setting error message
        DataTable datMessages;                      // having forms error messages
        MessageBoxIcon MmsgMessageIcon;             // obj of message icon
        ClsNotificationNew MobjClsNotification;     //obj of clsNotification
        ClsLogWriter MobjClsLogWriter;              // obj of clsLogWriter

        bool blnIsEditMode = false;
        bool MblnPrintEmailPermission = false;
        bool MblnAddPermission = false;
        bool MblnUpdatePermission = false;
        bool MblnDeletePermission = false;
        bool MchangeStatus = false;    //Check any edit or new entry done
        clsBLLCommonUtility mobjClsBllCommonUtility;
        int intCurrencyScale = 3;

        public FrmMaterialReturn()
        {
            InitializeComponent();

            MobjClsBLLMaterialReturnMaster = new clsBLLMaterialReturnMaster();
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotificationNew();
            tmrMaterialReturn.Interval = ClsCommonSettings.TimerInterval;
            mobjClsBllCommonUtility = new clsBLLCommonUtility();
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.MaterialReturn, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void LoadMessage()
        {
            try
            {
                //Loads error messages
                datMessages = new DataTable();
                datMessages = MobjClsNotification.FillMessageArray((int)FormID.MaterialReturn, ClsCommonSettings.ProductID);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadMessage() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in Load Message() " + ex.Message, 2);
            }
        }


        private void FillCombo(int intType)
        {
            // 0 - All, 1- Company,2-Warehouse,3-Employee,4- Search Company,5- Search Warehouse,6- SearchEmployee,7-Currency
            // 8 - Project, 9 - Search Project, 10- Created By Search
            try
            {
                DataTable datCombos = null;
                if (intType == 0 || intType == 1)
                {
                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;
                }
                if (intType == 0 || intType == 2)
                {
                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "Distinct InvWarehouse.WarehouseID,WarehouseName", "" +
                        "InvWarehouse INNER JOIN InvWarehouseCompanyDetails WC ON InvWarehouse.WarehouseID=WC.WarehouseID", "" +
                        "WC.CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + " And IsActive = 1" });
                    cboWarehouse.ValueMember = "WarehouseID";
                    cboWarehouse.DisplayMember = "WarehouseName";
                    cboWarehouse.DataSource = datCombos;
                }
                if (intType == 0 || intType == 4)
                {
                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                    cboSCompany.ValueMember = "CompanyID";
                    cboSCompany.DisplayMember = "CompanyName";
                    cboSCompany.DataSource = datCombos;
                }
                if (intType == 0 || intType == 5)
                {
                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "Distinct InvWarehouse.WarehouseID,WarehouseName", "" +
                        "InvWarehouse INNER JOIN InvWarehouseCompanyDetails WC ON InvWarehouse.WarehouseID=WC.WarehouseID", "" +
                        "WC.CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue) + " And IsActive = 1" });
                    cboSWarehouse.ValueMember = "WarehouseID";
                    cboSWarehouse.DisplayMember = "WarehouseName";
                    cboSWarehouse.DataSource = datCombos;
                }
          
                if (intType == 0 || intType == 7)
                {
                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "CurrencyID,CurrencyName", "CurrencyReference", "" });
                    cboCurrency.ValueMember = "CurrencyID";
                    cboCurrency.DisplayMember = "CurrencyName";
                    cboCurrency.DataSource = datCombos;
                }

                if (intType == 0 || intType == 8)
                {
                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "ProjectID,ProjectName", "Projects", "" });
                    cboProject.ValueMember = "ProjectID";
                    cboProject.DisplayMember = "ProjectName";
                    cboProject.DataSource = datCombos;
                }

                if (intType == 0 || intType == 9)
                {
                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "ProjectID,ProjectName", "Projects", "" });
                    cboSProject.ValueMember = "ProjectID";
                    cboSProject.DisplayMember = "ProjectName";
                    cboSProject.DataSource = datCombos;
                }

                if (intType == 0 || intType == 10)
                {
                    string strFilterCondition = "";
                    if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                        strFilterCondition = "UM.RoleID Not In (1,2)";

                    datCombos = mobjClsBllCommonUtility.FillCombos(new string[] { "UM.UserID,IsNull( EM.EmployeeFullName+' - ' +EM.EmployeeNumber,UM.UserName) as UserName", "UserMaster UM Left Join EmployeeMaster EM On EM.EmployeeID = UM.EmployeeID", strFilterCondition });
                    cboSCreatedBy.DisplayMember = "UserName";
                    cboSCreatedBy.ValueMember = "UserID";
                    cboSCreatedBy.DataSource = datCombos;
                }
            }
            catch(Exception ex)
            {
                MobjClsLogWriter.WriteLog("Following error occured in load combos : " + ex.Message, 1);
            }
        }

        private void ClearControls()
        {
            txtMaterialReturnNo.Text = "";
            cboCurrency.SelectedIndex = -1;

            dgvMaterialReturn.Rows.Clear();
            CalculateNetAmount();
            txtRemarks.Clear();
            dtpReturnedDate.Value = ClsCommonSettings.GetServerDate();
           // dtpReturnedDate.MinDate = ClsCommonSettings.GetServerDate();
            dtpReturnedDate.MaxDate = ClsCommonSettings.GetServerDate();
        }

        private void ClearSearchControls()
        {
            cboSCompany.SelectedIndex = -1;
            cboSWarehouse.SelectedIndex = -1;
            cboSProject.SelectedIndex = -1;
            cboSCreatedBy.SelectedIndex = -1;
            dtpSFrom.Value = ClsCommonSettings.GetServerDate();
            dtpSTo.Value = ClsCommonSettings.GetServerDate();
        }

        private void AddNew()
        {
            blnIsEditMode = false;
            ErrMaterialReturn.Clear();
            tmrMaterialReturn.Enabled = false;

           // cboCompany.Enabled = true;
            cboWarehouse.Enabled = true;
            cboProject.Enabled = true;

            cboCompany.SelectedIndex = -1;
            cboWarehouse.SelectedIndex = -1;
            cboProject.SelectedIndex = -1;

            cboCompany.Text = cboWarehouse.Text = cboProject.Text = "";

            ClearControls();
            MobjClsBLLMaterialReturnMaster = new clsBLLMaterialReturnMaster();
           // cboCompany.SelectedValue = ClsCommonSettings.CompanyID;
            lblCreatedByText.Text = "Created By : " + ClsCommonSettings.strEmployeeName;
            lblCreatedDateValue.Text = "Created Date : " + ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
            btnRefresh_Click(null, null);
            bnSaveItem.Enabled = bnDelete.Enabled = bnClear.Enabled = bnEmail.Enabled = bnPrint.Enabled = false;
            lblStatus.Text = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.AddNewInformation).Replace("#", "").Replace("*", "Material Return");

            MchangeStatus = false;
            cboWarehouse.Focus();
        }

        private void ChangeStatus(object sender,EventArgs e)
        {
            MchangeStatus = true;
            bnClear.Enabled = true;
            
            if (!blnIsEditMode)
            {
                bnSaveItem.Enabled = MblnAddPermission;
            }
            else
            {
                bnSaveItem.Enabled = MblnUpdatePermission;
            }
        }

        private void GenerateMaterialReturnNo()
        {
            string strMaterialReturnNo = "";
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            DataTable dtTemp = objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue));
            strMaterialReturnNo = ClsCommonSettings.PMaterialReturnPrefix;
            dtTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "(IsNull(Max(SlNo),0) + 1) as SlNo", "InvMaterialReturnMaster", "CompanyID = " + cboCompany.SelectedValue.ToInt32() });
            if (dtTemp.Rows.Count > 0)
            {
                strMaterialReturnNo += dtTemp.Rows[0]["SlNo"].ToStringCustom();
            }

            if (ClsCommonSettings.PMaterialReturnAutogenerate)
                txtMaterialReturnNo.ReadOnly = true;
            else
                txtMaterialReturnNo.ReadOnly = false;

            txtMaterialReturnNo.Text = strMaterialReturnNo;
        }

        private void FillComboColumn(int intRowIndex)
        {
            if (dgvMaterialReturn.Rows[intRowIndex].Cells[dgvColItemID.Index].Value.ToInt32() > 0)
            {
                int intUOMID = dgvMaterialReturn.Rows[intRowIndex].Cells[dgvColUOM.Index].Tag.ToInt32();

                DataTable datCombos = MobjClsBLLMaterialReturnMaster.GetItemUOMs(dgvMaterialReturn.Rows[intRowIndex].Cells[dgvColItemID.Index].Value.ToInt32());
                dgvColUOM.ValueMember = "UOMID";
                dgvColUOM.DisplayMember = "ShortName";
                dgvColUOM.DataSource = datCombos;

                if (intUOMID > 0)
                {
                    dgvMaterialReturn.Rows[intRowIndex].Cells[dgvColUOM.Index].Value = intUOMID;
                    dgvMaterialReturn.Rows[intRowIndex].Cells[dgvColUOM.Index].Tag = dgvMaterialReturn.Rows[intRowIndex].Cells[dgvColUOM.Index].Value;
                    dgvMaterialReturn.Rows[intRowIndex].Cells[dgvColUOM.Index].Value = dgvMaterialReturn.Rows[intRowIndex].Cells[dgvColUOM.Index].FormattedValue;
                }
            }
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedValue.ToInt32() != 0)
            {
                DataTable datCurrencyDetails = mobjClsBllCommonUtility.FillCombos(new string[] { "CR.Scale,CR.CurrencyID", "CurrencyReference CR Inner Join CompanyMaster CM On CM.CurrencyId = CR.CurrencyID", "CM.CompanyID = " + cboCompany.SelectedValue.ToInt32() });
                intCurrencyScale = datCurrencyDetails.Rows[0]["Scale"].ToInt32();
                cboCurrency.SelectedValue = datCurrencyDetails.Rows[0]["CurrencyID"].ToInt32();

                if (!blnIsEditMode)
                    GenerateMaterialReturnNo();
            }
            else
                intCurrencyScale = 3;
            ChangeStatus(sender,e);
           // FillCombo(2);
           // FillCombo(3);
        }

        private void FrmMaterialReturn_Load(object sender, EventArgs e)
        {
            tmrMaterialReturn.Interval = ClsCommonSettings.TimerInterval;

            FillCombo(0);
            SetPermissions();
            LoadMessage();
            ClearSearchControls();
            AddNew();
        }

        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private bool FillParameters()
        {
            MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnDate = dtpReturnedDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnNo = txtMaterialReturnNo.Text;
            MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.WarehouseID = cboWarehouse.SelectedValue.ToInt32();
            MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.ProjectID = cboProject.SelectedValue.ToInt32(); 
            MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.CompanyID = cboCompany.SelectedValue.ToInt32();
            MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.CreatedBy = ClsCommonSettings.UserID;
            MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.CreatedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
            MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.Remarks = txtRemarks.Text.Trim();
            MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.NetAmount = txtNetAmount.Text.ToDecimal();

            MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnDetails = new List<clsDTOMaterialReturnDetails>();
            MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnLocationDetails = new List<clsDTOMaterialReturnLocationDetails>();
            int intSerialNo = 0;
            foreach (DataGridViewRow dr in dgvMaterialReturn.Rows)
            {
                if (dr.Cells[dgvColItemID.Index].Value.ToInt32() != 0)
                {
                    clsDTOMaterialReturnDetails objDTOMaterialDetails = new clsDTOMaterialReturnDetails();
                    objDTOMaterialDetails.SerialNo = ++intSerialNo;
                    objDTOMaterialDetails.ItemID = dr.Cells[dgvColItemID.Index].Value.ToInt32();
                    objDTOMaterialDetails.BatchID = dr.Cells[dgvColBatchID.Index].Value.ToInt64();
                    objDTOMaterialDetails.ReturnedQuantity = dr.Cells[dgvColQuantity.Index].Value.ToDecimal();
                    objDTOMaterialDetails.UOMID = dr.Cells[dgvColUOM.Index].Tag.ToInt32();
                    objDTOMaterialDetails.Rate = dr.Cells[dgvColRate.Index].Value.ToDecimal();
                    objDTOMaterialDetails.NetAmount = dr.Cells[dgvColNetAmount.Index].Value.ToDecimal();
                    MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnDetails.Add(objDTOMaterialDetails);

                    clsDTOMaterialReturnLocationDetails objLocationDetails = new clsDTOMaterialReturnLocationDetails();
                    objLocationDetails.SerialNo = objDTOMaterialDetails.SerialNo;
                    objLocationDetails.ItemID = objDTOMaterialDetails.ItemID;
                    objLocationDetails.BatchID = objDTOMaterialDetails.BatchID;
                    objLocationDetails.Quantity = objDTOMaterialDetails.ReturnedQuantity;
                    objLocationDetails.UOMID = objDTOMaterialDetails.UOMID;

                    DataRow drLocation = MobjClsBLLMaterialReturnMaster.GetItemDefaultLocation(objLocationDetails.ItemID, cboWarehouse.SelectedValue.ToInt32());
                    objLocationDetails.LocationID = drLocation["LocationID"].ToInt32();
                    objLocationDetails.RowID = drLocation["RowID"].ToInt32();
                    objLocationDetails.BlockID = drLocation["BlockID"].ToInt32();
                    objLocationDetails.LotID = drLocation["LotID"].ToInt32();
                    MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnLocationDetails.Add(objLocationDetails);
                }
            }
            return true;
        }

        private bool DisplayMaterialReturnInfo()
        {
            if (MobjClsBLLMaterialReturnMaster.FillMaterialReturnMasterInfo())
            {
                cboCompany.SelectedValue = MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.CompanyID;
                cboWarehouse.SelectedValue = MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.WarehouseID;
                cboProject.SelectedValue = MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.ProjectID;
                txtMaterialReturnNo.Text = MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnNo;
                dtpReturnedDate.Value = MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnDate.ToDateTime();
                lblCreatedByText.Text = "Created By : "+ MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.CreatedByText;
                lblCreatedDateValue.Text = "Created Date : " + MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.CreatedDate;
                txtRemarks.Text = MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.Remarks;

                dgvMaterialReturn.Rows.Clear();
                DataTable datMaterialReturnDetails = MobjClsBLLMaterialReturnMaster.GetMaterialReturnDetails();
                for(int iCounter=0;iCounter<datMaterialReturnDetails.Rows.Count;iCounter++)
                {
                    dgvMaterialReturn.Rows.Add();
                    DataRow dr = datMaterialReturnDetails.Rows[iCounter];
                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColItemID.Index].Value = dr["ItemID"].ToInt32();
                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColItemName.Index].Value = dr["ItemName"].ToStringCustom();
                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColItemCode.Index].Value = dr["ItemCode"].ToStringCustom();
                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColBatchNo.Index].Value = dr["BatchNo"].ToStringCustom();
                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColBatchID.Index].Value = dr["BatchID"].ToInt64();
                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColQuantity.Index].Value = dr["Quantity"].ToDecimal();
                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColRate.Index].Value = dr["Rate"].ToDecimal();
                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColNetAmount.Index].Value = dr["NetAmount"].ToDecimal();
                    
                    FillComboColumn(iCounter);
                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColUOM.Index].Value = dr["UOMID"].ToInt32();
                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColUOM.Index].Tag = dr["UOMID"].ToInt32();
                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColUOM.Index].Value = dgvMaterialReturn.Rows[iCounter].Cells[dgvColUOM.Index].FormattedValue;

                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColIssuedQty.Index].Value = dr["IssuedQuantity"].ToStringCustom();
                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColTempIssuedQty.Index].Value = dr["TempIssuedQty"].ToDecimal();
                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColReturnedQty.Index].Value = dr["ReturnedQuantity"].ToStringCustom();
                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColIssuedUomID.Index].Value = dr["IssuedUOMID"].ToInt32();
                    dgvMaterialReturn.Rows[iCounter].Cells[dgvColIssuedRate.Index].Value = dr["IssuedRate"].ToDecimal();


                }

                txtNetAmount.Text = MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.NetAmount.ToString("F" + intCurrencyScale);
                lblStatus.Text = "Details of Material Return No '" + txtMaterialReturnNo.Text+"'";
                cboCompany.Enabled = cboWarehouse.Enabled = cboProject.Enabled =false ;
                bnDelete.Enabled = MblnDeletePermission;
                bnSaveItem.Enabled = false;
                bnAddNewItem.Enabled = MblnAddPermission;
                bnEmail.Enabled = bnPrint.Enabled = MblnPrintEmailPermission;
                blnIsEditMode = true;
                return true;
            }
            return false;
        }

        private void dgvMaterialReturn_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgvColUOM.Index)
            {
                dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColUOM.Index].Tag = dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColUOM.Index].Value;
                dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColUOM.Index].Value = dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColUOM.Index].FormattedValue;

                int intUomScale = 0;
                if (dgvMaterialReturn[dgvColUOM.Index, e.RowIndex].Tag.ToInt32() != 0)
                    intUomScale = mobjClsBllCommonUtility.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvMaterialReturn[dgvColUOM.Index, e.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value = dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value.ToDecimal().ToString("F" + intUomScale);

                decimal decRate = dgvMaterialReturn[dgvColIssuedRate.Index, e.RowIndex].Value.ToDecimal();
                decRate = mobjClsBllCommonUtility.ConvertQtyToBaseUnitQty(dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColIssuedUomID.Index].Value.ToInt32(),dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value.ToInt32(), decRate, 1);
                decRate = mobjClsBllCommonUtility.ConvertBaseUnitQtyToOtherQty(dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColUOM.Index].Tag.ToInt32(), dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value.ToInt32(), decRate, 1);
                dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColRate.Index].Value = decRate.ToString("F" + intCurrencyScale);

            }
            else if (e.ColumnIndex == dgvColQuantity.Index)
            {
                int intUomScale = 0;
                if (dgvMaterialReturn[dgvColUOM.Index, e.RowIndex].Tag.ToInt32() != 0)
                    intUomScale = mobjClsBllCommonUtility.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvMaterialReturn[dgvColUOM.Index, e.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value = dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value.ToDecimal().ToString("F" + intUomScale);

            }
        }

        private void CalculateNetAmount()
        {
            decimal decNetAmount = 0, decGridNetAmount = 0;
            foreach (DataGridViewRow dr in dgvMaterialReturn.Rows)
            {
                if (dr.Cells[dgvColItemID.Index].Value.ToInt32() != 0)
                {
                    decGridNetAmount = dr.Cells[dgvColQuantity.Index].Value.ToDecimal() * dr.Cells[dgvColRate.Index].Value.ToDecimal();
                    dr.Cells[dgvColNetAmount.Index].Value = decGridNetAmount.ToString("F" + intCurrencyScale);
                    decNetAmount += decGridNetAmount;
                }
            }

            txtNetAmount.Text = decNetAmount.ToString("F" + intCurrencyScale);
        }

        private void dgvMaterialReturn_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                ChangeStatus(null, null);
               
                if (e.ColumnIndex == dgvColItemID.Index)
                {
                    FillComboColumn(e.RowIndex);
                    DataTable datItemDetails = mobjClsBllCommonUtility.FillCombos(new string[] { "DefaultSalesUomID", "InvItemMaster", "ItemID  = " + dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value.ToInt32() });
                    if (datItemDetails.Rows.Count > 0)
                    {
                        dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColUOM.Index].Value = datItemDetails.Rows[0]["DefaultSalesUomID"].ToInt32();
                        dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColUOM.Index].Tag = datItemDetails.Rows[0]["DefaultSalesUomID"].ToInt32();
                        dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColUOM.Index].Value = dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColUOM.Index].FormattedValue;

                       
                    }
                }
                else if (e.ColumnIndex == dgvColUOM.Index)
                {
                    int intUomScale = 0;
                    if (dgvMaterialReturn[dgvColUOM.Index, e.RowIndex].Tag.ToInt32() != 0)
                        intUomScale = mobjClsBllCommonUtility.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvMaterialReturn[dgvColUOM.Index, e.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();
                    dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value = dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value.ToDecimal().ToString("F" + intUomScale);


                }
                else if (e.ColumnIndex == dgvColRate.Index || e.ColumnIndex == dgvColQuantity.Index)
                {
                    CalculateNetAmount();
                }
                else if (e.ColumnIndex == dgvColIssuedRate.Index)
                {
                    decimal decRate = dgvMaterialReturn[dgvColIssuedRate.Index, e.RowIndex].Value.ToDecimal();
                    decRate = mobjClsBllCommonUtility.ConvertQtyToBaseUnitQty(dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColIssuedUomID.Index].Value.ToInt32(), dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value.ToInt32(), decRate, 1);
                    decRate = mobjClsBllCommonUtility.ConvertBaseUnitQtyToOtherQty(dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColUOM.Index].Tag.ToInt32(), dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value.ToInt32(), decRate, 1);
                    dgvMaterialReturn.Rows[e.RowIndex].Cells[dgvColRate.Index].Value = decRate.ToString("F" + intCurrencyScale);

                }
            }
        }

      
        private void dgvMaterialReturn_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (e.ColumnIndex == dgvColUOM.Index)
                {
                    FillComboColumn(e.RowIndex);
                }
            }
        }

        private void dgvMaterialReturn_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvMaterialReturn.CurrentCell.ColumnIndex == dgvColItemCode.Index || dgvMaterialReturn.CurrentCell.ColumnIndex == dgvColItemName.Index)
                {
                //    for (int i = 0; i < dgvMaterialReturn.Columns.Count; i++)
                //        dgvMaterialReturn.Rows[dgvMaterialReturn.CurrentCell.RowIndex].Cells[i].Value = null;
                //}
                dgvMaterialReturn.PServerName = ClsCommonSettings.ServerName;
                string[] First = null;
                string[] second = null;

                First = new string[] { "dgvColItemCode", "dgvColItemName", "dgvColItemID", "dgvColBatchNo", "dgvColBatchID", "dgvColIssuedQty","dgvColReturnedQty","dgvColTempIssuedQty","dgvColIssuedUomID","dgvColIssuedRate" };//first grid 
                second = new string[] { "ItemCode", "ItemName", "ItemID", "BatchNo", "BatchID", "IssuedQuantity", "ReturnedQuantity","TempIssuedQty","IssuedUOMID","IssuedRate" };//inner grid

             
                dgvMaterialReturn.aryFirstGridParam = First;
                dgvMaterialReturn.arySecondGridParam = second;
                dgvMaterialReturn.PiFocusIndex = dgvColQuantity.Index;
                dgvMaterialReturn.iGridWidth = 500;
                dgvMaterialReturn.pnlLeft = expandableSplitterLeft.Location.X + 5;
                dgvMaterialReturn.pnlTop = bnMaterialReturn.Height + 10;
                dgvMaterialReturn.bBothScrollBar = true;
                dgvMaterialReturn.ColumnsToHide = new string[] { "ItemID", "BatchID","TempIssuedQty","IssuedUOMID" };

                DataTable dtDatasource = MobjClsBLLMaterialReturnMaster.GetItems(cboCompany.SelectedValue.ToInt32(),cboWarehouse.SelectedValue.ToInt32(),cboProject.SelectedValue.ToInt32());

                if (dgvMaterialReturn.CurrentCell.ColumnIndex == dgvColItemCode.Index)
                {
                    dgvMaterialReturn.CurrentCell.Value = dgvMaterialReturn.TextBoxText;
                    dgvMaterialReturn.field = "ItemCode";
                }
                else if (dgvMaterialReturn.CurrentCell.ColumnIndex == dgvColItemName.Index)
                {
                    dgvMaterialReturn.CurrentCell.Value = dgvMaterialReturn.TextBoxText;
                    dgvMaterialReturn.field = "ItemName";
                }
                string strFilterString = dgvMaterialReturn.field + " Like '" + dgvMaterialReturn.TextBoxText + "%'";
                dtDatasource.DefaultView.RowFilter = strFilterString;
                dgvMaterialReturn.dtDataSource = dtDatasource.DefaultView.ToTable();
            }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvMaterialReturn_Textbox_TextChanged() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvMaterialReturn_Textbox_TextChanged() " + ex.Message, 2);
            }
        }

        private void dgvMaterialReturn_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if ((e.ColumnIndex == dgvColItemCode.Index) || (e.ColumnIndex == dgvColItemName.Index))
                {
                    dgvMaterialReturn.PiColumnIndex = e.ColumnIndex;
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvMaterialReturn_CellEnter() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in dgvMaterialReturn_CellEnter() " + ex.Message, 2);
            }
        }

        private void bnSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateMaterialReturnMaster())
                {
                    FillParameters();

                    if (blnIsEditMode)
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.DoYouWishToUpdate, out MmsgMessageIcon).Replace("#","");
                    else
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.DoYouWishToSave, out MmsgMessageIcon).Replace("#","");

                    if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MmsgMessageIcon) == DialogResult.No)
                        return;

                    if (txtMaterialReturnNo.ReadOnly && mobjClsBllCommonUtility.FillCombos(new string[] { "1", "InvMaterialReturnMaster", "MaterialReturnNo = '" + txtMaterialReturnNo.Text + "' And CompanyID = "+cboCompany.SelectedValue.ToInt32()+" And MaterialReturnID <> " + MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnID }).Rows.Count > 0)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages,(int)MaterialReturnMessageCodes.MaterialReturnNoExistsContinue,out MmsgMessageIcon).Replace("#","");
                        
                        if(MessageBox.Show(MstrCommonMessage,ClsCommonSettings.MessageCaption,MessageBoxButtons.YesNo,MmsgMessageIcon)== DialogResult.No)
                            return ;

                        GenerateMaterialReturnNo();
                        MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnNo = txtMaterialReturnNo.Text;
                    }


                    if (MobjClsBLLMaterialReturnMaster.SaveMaterialReturn())
                    {
                        if (!blnIsEditMode)
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.SavedSuccessfully, out MmsgMessageIcon).Replace("#","");
                        else
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.UpdatedSuccessfully, out MmsgMessageIcon).Replace("#", "");
                        MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        AddNew();
                    }
                }
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Following error occured in bnSaveItem_Click : " + ex.Message, 1);
            }
        }

        private void bnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                DataTable datMaterialReturnDetails = MobjClsBLLMaterialReturnMaster.GetMaterialReturnDetails();
                foreach (DataRow dr in datMaterialReturnDetails.Rows)
                {
                    decimal decTransferedQty = 0, decEmployeeIssuedQty = 0, decEmployeeReturnedQty = 0,decCurrentQty = 0;

                    decCurrentQty = mobjClsBllCommonUtility.ConvertQtyToBaseUnitQty(dr["UOMID"].ToInt32(),dr["ItemID"].ToInt32(),dr["Quantity"].ToDecimal(),1);
                    decimal decGRNQuantity = 0, decSoldQuantity = 0;

                    DataTable datTemp = new DataTable();
                    datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "IsNull(GRNQuantity,0) as GRNQuantity,IsNull(SoldQuantity,0) as SoldQuantity,IsNull(TransferedQuantity,0) as TransferedQty", "InvItemStockDetails", "ItemID = " + dr["ItemID"].ToInt32() + " And BatchID = " + dr["BatchID"].ToInt64() + " And WarehouseID = " + cboWarehouse.SelectedValue.ToInt32() });
                    if (datTemp.Rows.Count > 0)
                    {
                        decTransferedQty = datTemp.Rows[0]["TransferedQty"].ToDecimal();
                        decGRNQuantity = datTemp.Rows[0]["GRNQuantity"].ToDecimal();
                        decSoldQuantity = datTemp.Rows[0]["SoldQuantity"].ToDecimal();
                    }

                    if ((decGRNQuantity - decSoldQuantity) < (decTransferedQty + decCurrentQty))
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MaterialReturnMessageCodes.TransferedQtyExceedsStock, out MmsgMessageIcon).Replace("#", "");

                        MstrCommonMessage = MstrCommonMessage.Replace("*", dr["ItemName"].ToStringCustom());

                        MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        dgvMaterialReturn.Focus();
                        dgvMaterialReturn.CurrentCell = dgvMaterialReturn[dgvColItemName.Index, dgvMaterialReturn.RowCount - 1];
                        return ;
                    }
                    //DataTable datTemp = new DataTable();
                    //datTemp = mobjClsBllCommonUtility.FillCombos(new string[]{"IsNull(TransferedQuantity,0) as TransferedQty","STStockDetails","ItemID = "+dr["ItemID"].ToInt32()+" And BatchID = "+dr["BatchID"].ToInt64()+" And WarehouseID = "+cboWarehouse.SelectedValue.ToInt32()});
                    //if (datTemp.Rows.Count > 0)
                    //    decTransferedQty = datTemp.Rows[0]["TransferedQty"].ToDecimal();
                    //datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "IsNull(Sum(dbo.STConvertQtyToBaseUnitQty(MD.ItemID,MD.UOMID,1,MD.Quantity,1)),0) as IssuedQuantity", "STMaterialIssueDetails MD Inner Join STMaterialIssueMaster MI On MI.MaterialIssueID = MD.MaterialIssueID", "MI.EmployeeID = " + cboEmployee.SelectedValue.ToInt32() + " And MI.WarehouseID = " + cboEmployee.SelectedValue.ToInt32()+" And MD.ItemID = "+dr["ItemID"].ToInt32() +" And MD.BatchID = "+dr["BatchID"].ToInt64() });
                    //if (datTemp.Rows.Count > 0)
                    //    decEmployeeIssuedQty = datTemp.Rows[0]["IssuedQuantity"].ToDecimal();
                    //datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "IsNull(Sum(dbo.STConvertQtyToBaseUnitQty(MD.ItemID,MD.UOMID,1,MD.Quantity,1)),0) as ReturnedQuantity", "STMaterialIssueDetails MD Inner Join STMaterialIssueMaster MR On MR.MaterialIssueID = MD.MaterialIssueID", "MR.EmployeeID = " + cboEmployee.SelectedValue.ToInt32() + " And MR.WarehouseID = " + cboEmployee.SelectedValue.ToInt32() + " And MD.ItemID = " + dr["ItemID"].ToInt32() + " And MD.BatchID = " + dr["BatchID"].ToInt64() + " And MaterialReturnID <> " + MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnID });
                    //if (datTemp.Rows.Count > 0)
                    //    decEmployeeReturnedQty = datTemp.Rows[0]["ReturnedQuantity"].ToDecimal();

                    //if ((decEmployeeIssuedQty - decEmployeeReturnedQty) > (decTransferedQty - decCurrentQty))
                    //{
                    //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MaterialReturnMessageCodes.EmployeeStockExceedsTransferedQty, out MmsgMessageIcon).Replace("*", dr["ItemName"].ToStringCustom().Replace("#", ""));
                    //    MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    //    return;
                    //}
                    //if (mobjClsBllCommonUtility.GetStockQuantity(dr["ItemID"].ToInt32(), dr["BatchID"].ToInt64(), cboCompany.SelectedValue.ToInt32(), cboWarehouse.SelectedValue.ToInt32()) - mobjClsBllCommonUtility.ConvertQtyToBaseUnitQty(dr["UOMID"].ToInt32(), dr["ItemID"].ToInt32(), dr["ReturnedQuantity"].ToInt64(), 1) < 0)
                    //{
                    //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MaterialReturnMessageCodes.TransferedQtyExceedsStock, out MmsgMessageIcon).Replace("*", dr["ItemName"].ToStringCustom().Replace("#", ""));
                    //    MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    //    return;
                    //}
                }
            //if (mobjClsBllCommonUtility.FillCombos(new string[] { "1", "STDirectSalesInvoiceMaster", "MaterialReturnID = " + MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnID }).Rows.Count > 0)
            //{
            //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MaterialReturnMessageCodes.DirectSalesInvoiceDone, out MmsgMessageIcon).Replace("#", "");
            //    MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
            //    return;
            //}

                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.DoYouWishToDelete, out MmsgMessageIcon).Replace("#","");

                if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MmsgMessageIcon) == DialogResult.No)
                    return;

                if (MobjClsBLLMaterialReturnMaster.DeleteMaterialReturn())
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.DeletedSuccessfully, out MmsgMessageIcon).Replace("#","");
                    MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    AddNew();
                }
            }
            catch (Exception ex)
            {
                MobjClsLogWriter.WriteLog("Following error occured in bnDeleteItem_Click : " + ex.Message, 1);

            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            string strSearchCondition = "";

            if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
                strSearchCondition += "MR.MaterialReturnNo = '" + txtSearch.Text.Trim() + "'";
            else
            {
                if (cboSCompany.SelectedValue.ToInt32() != 0)
                    strSearchCondition += "MR.CompanyID = " + cboSCompany.SelectedValue.ToInt32();
                if (cboSWarehouse.SelectedValue.ToInt32() != 0)
                {
                    if (!string.IsNullOrEmpty(strSearchCondition))
                        strSearchCondition += " AND ";
                    strSearchCondition += "MR.WarehouseID = " + cboSWarehouse.SelectedValue.ToInt32();
                }
                if (cboSProject.SelectedValue.ToInt32() != 0)
                {
                    if (!string.IsNullOrEmpty(strSearchCondition))
                        strSearchCondition += " AND ";
                    strSearchCondition += "MR.ProjectID = " + cboSProject.SelectedValue.ToInt32();
                }
                if (cboSCreatedBy.SelectedValue.ToInt32() != 0)
                {
                    if (!string.IsNullOrEmpty(strSearchCondition))
                        strSearchCondition += " AND ";
                    strSearchCondition += "MR.CreatedBy = " + cboSCreatedBy.SelectedValue.ToInt32();
                }
                else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    if (((DataTable)cboSCreatedBy.DataSource).Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(strSearchCondition))
                            strSearchCondition += " AND ";
                        strSearchCondition += "MR.CreatedBy In (";
                        foreach (DataRow dr in ((DataTable)cboSCreatedBy.DataSource).Rows)
                        {
                            strSearchCondition += dr["UserID"].ToInt32() + ",";
                        }
                        strSearchCondition = strSearchCondition.Remove(strSearchCondition.Length - 1);
                        strSearchCondition += ")";
                    }
                }

                if (!string.IsNullOrEmpty(strSearchCondition))
                    strSearchCondition += " And ";
                strSearchCondition += "MR.MaterialReturnDate Between '" + dtpSFrom.Value.ToString("dd-MMM-yyyy") + "' And '" + dtpSTo.Value.ToString("dd-MMM-yyyy") + "'";
            }

            DataTable datMaterialReturns = MobjClsBLLMaterialReturnMaster.GetAllMaterialReturns(strSearchCondition);
            dgvMaterialReturnDisplay.DataSource = null;
            dgvMaterialReturnDisplay.DataSource = datMaterialReturns;
            if (dgvMaterialReturnDisplay.Columns.Count > 0)
            {
                dgvMaterialReturnDisplay.Columns[0].Visible = false;
                dgvMaterialReturnDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            lblSCountStatus.Text = "Total Returns : " + dgvMaterialReturnDisplay.Rows.Count;

        }

        private void bnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void dgvMaterialReturnDisplay_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                if (dgvMaterialReturnDisplay.Rows[e.RowIndex].Cells[0].Value.ToInt64() > 0)
                {
                    MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnID =dgvMaterialReturnDisplay.Rows[e.RowIndex].Cells[0].Value.ToInt64();
                    DisplayMaterialReturnInfo();
                }
            }
            
        }

        private bool ValidateMaterialReturnMaster()
        {
            dgvMaterialReturn.CommitEdit(DataGridViewDataErrorContexts.Commit);
            dgvMaterialReturn.EndEdit();
            int intMessageCode = 0;
            Control cntrl=null;
            if (cboCompany.SelectedValue.ToInt32() == 0)
            {
                intMessageCode = (int)MessageCode.SelectCompany;
                cntrl = cboCompany;
            }
            else if (cboWarehouse.SelectedValue.ToInt32() == 0)
            {
                intMessageCode = (int)MessageCode.SelectWarehouse;
                cntrl = cboWarehouse;
            }
            else if (cboProject.SelectedValue.ToInt32() == 0)
            {
                intMessageCode = (int)MaterialReturnMessageCodes.EnterProject;
                cntrl = cboProject;
            }
            else if (string.IsNullOrEmpty(txtMaterialReturnNo.Text.Trim()))
            {
                intMessageCode = (int)MaterialReturnMessageCodes.EnterMaterialReturnNo;
                cntrl = txtMaterialReturnNo;
            }
            else if (!txtMaterialReturnNo.ReadOnly && mobjClsBllCommonUtility.FillCombos(new string[] { "1", "InvMaterialReturnMaster", "MaterialReturnNo = '" + txtMaterialReturnNo.Text + "' And CompanyID = " + cboCompany.SelectedValue.ToInt32() + " And MaterialReturnID <> " + MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnID }).Rows.Count > 0)
            {
                intMessageCode = (int)MaterialReturnMessageCodes.MaterialReturnNoExists;
                cntrl = txtMaterialReturnNo;
            }
            //else if (blnIsEditMode && mobjClsBllCommonUtility.FillCombos(new string[] { "1", "STDirectSalesInvoiceMaster", "MaterialReturnID = " + MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnID }).Rows.Count > 0)
            //{
            //    intMessageCode = (int)MaterialReturnMessageCodes.DirectSalesInvoiceDone;
            //    cntrl = txtMaterialReturnNo;
            //}

            if (intMessageCode != 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, intMessageCode, out MmsgMessageIcon).Replace("#", "");
                ErrMaterialReturn.SetError(cntrl, MstrCommonMessage);
                MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                tmrMaterialReturn.Enabled = true;
                return false;
            }
            else
            {
                if (CheckDuplication())
                    return false;

                return ValidateMaterialReturnDetails();
            }
        }

        private bool ValidateMaterialReturnDetails()
        {
            int intColumnIndex = -1, intRowIndex=-1 , intMessageCode = 0;
            int intItemCount =0;
            DataTable datMaterialReturnDetails = new DataTable();
            if (blnIsEditMode)
                datMaterialReturnDetails = MobjClsBLLMaterialReturnMaster.GetMaterialReturnDetails();

            foreach (DataGridViewRow dr in dgvMaterialReturn.Rows)
            {
                if (dr.Cells[dgvColItemID.Index].Value.ToInt32() > 0)
                {
                    if (string.IsNullOrEmpty(dr.Cells[dgvColItemCode.Index].Value.ToStringCustom()))
                    {
                        intColumnIndex = dgvColItemCode.Index;
                        intRowIndex = dr.Index;
                        intMessageCode = (int)MessageCode.SelectItemCode;
                        break;
                    }
                    else if (string.IsNullOrEmpty(dr.Cells[dgvColItemName.Index].Value.ToStringCustom()))
                    {
                        intColumnIndex = dgvColItemName.Index;
                        intRowIndex = dr.Index;
                        intMessageCode = (int)MessageCode.SelectItemName;
                        break;
                    }
                    else if (dr.Cells[dgvColQuantity.Index].Value.ToDecimal() <=0)
                    {
                        intColumnIndex = dgvColQuantity.Index;
                        intRowIndex = dr.Index;
                        intMessageCode = (int)MessageCode.EnterQuantity;
                        break;
                    }
                    else if (dr.Cells[dgvColUOM.Index].Tag.ToInt32() == 0)
                    {
                        intColumnIndex = dgvColUOM.Index;
                        intRowIndex = dr.Index;
                        intMessageCode = (int)MessageCode.EnterUOM;
                        break;
                    }
                    else
                    {
                        decimal decOldQuantity = 0, decCurrentQty =0, decStockQty = 0,decEmployeeStockQty=0,decIssuedQty=0,decReturnedQty =0;

                        DataTable datAlreadyReturnedQty = mobjClsBllCommonUtility.FillCombos(new string[] {"IsNull(Sum(dbo.fnInvConvertQtyToBaseUnitQty(MD.ItemID,MD.UOMID,1,MD.ReturnedQuantity,1)),0) as ReturnedQty","InvMaterialReturnDetails MD Inner Join InvMaterialReturnMaster MR On MR.MaterialReturnID = MD.MaterialReturnID","MR.WareHouseID = "+cboWarehouse.SelectedValue.ToInt32()+" AND MR.ProjectID = "+cboProject.SelectedValue.ToInt32()+" AND MD.ItemID = "+dr.Cells[dgvColItemID.Index].Value.ToInt32()+" And MD.BatchID = "+dr.Cells[dgvColBatchID.Index].Value.ToInt64()});
                        if(datAlreadyReturnedQty.Rows.Count > 0)
                            decReturnedQty = datAlreadyReturnedQty.Rows[0]["ReturnedQty"].ToDecimal();

                        DataTable datIssuedQuantity = mobjClsBllCommonUtility.FillCombos(new string[] { "IsNull(Sum(dbo.fnInvConvertQtyToBaseUnitQty(MD.ItemID,MD.UOMID,1,MD.Quantity,1)),0) as IssuedQty", "InvMaterialIssueDetails MD Inner Join InvMaterialIssueMaster MI On MI.MaterialIssueID = MD.MaterialIssueID ", "MI.WareHouseID = "+cboWarehouse.SelectedValue.ToInt32()+" AND MI.ProjectID = "+cboProject.SelectedValue.ToInt32()+" AND MD.ItemID = " + dr.Cells[dgvColItemID.Index].Value.ToInt32() + " And MD.BatchID = " + dr.Cells[dgvColBatchID.Index].Value.ToInt64() });
                        if (datIssuedQuantity.Rows.Count > 0)
                            decIssuedQty = datIssuedQuantity.Rows[0]["IssuedQty"].ToDecimal();

                        decStockQty = mobjClsBllCommonUtility.GetStockQuantity(dr.Cells[dgvColItemID.Index].Value.ToInt32(),dr.Cells[dgvColBatchID.Index].Value.ToInt64(),cboCompany.SelectedValue.ToInt32(),cboWarehouse.SelectedValue.ToInt32());
                        //decIssuedQty = mobjClsBllCommonUtility.ConvertQtyToBaseUnitQty(dr.Cells[dgvColUOM.Index].Value.ToInt32(), dr.Cells[dgvColItemID.Index].Value.ToInt32(), dr.Cells[dgvColTempIssuedQty.Index].Value.ToDecimal(), 1);
                        //decEmployeeStockQty = mobjClsBllCommonUtility.GetEmployeeStockQuantity(dr.Cells[dgvColItemID.Index].Value.ToInt32(), dr.Cells[dgvColBatchID.Index].Value.ToInt64(),cboEmployee.SelectedValue.ToInt32());

                        if (blnIsEditMode)
                        {
                            datMaterialReturnDetails.DefaultView.RowFilter = "ItemID = " + dr.Cells[dgvColItemID.Index].Value.ToInt32() + " And BatchID = " + dr.Cells[dgvColBatchID.Index].Value.ToInt64();
                            if (datMaterialReturnDetails.DefaultView.ToTable().Rows.Count > 0)
                            {
                                DataRow drMaterialReturnRow = datMaterialReturnDetails.DefaultView.ToTable().Rows[0];
                                decOldQuantity = mobjClsBllCommonUtility.ConvertQtyToBaseUnitQty(drMaterialReturnRow["UOMID"].ToInt32(), drMaterialReturnRow["ItemID"].ToInt32(), drMaterialReturnRow["Quantity"].ToDecimal(), 1);
                            }
                        }

                        decCurrentQty = mobjClsBllCommonUtility.ConvertQtyToBaseUnitQty(dr.Cells[dgvColUOM.Index].Tag.ToInt32(), dr.Cells[dgvColItemID.Index].Value.ToInt32(), dr.Cells[dgvColQuantity.Index].Value.ToDecimal(), 1);

                        if (decIssuedQty < ((decReturnedQty - decOldQuantity) + decCurrentQty))
                        {
                            intColumnIndex = dgvColQuantity.Index;
                            intRowIndex = dr.Index;
                            intMessageCode = (int)MaterialReturnMessageCodes.ReturnedQtyExceedsIssuedQty;
                            break;
                        }


                        decimal decTransferedQty = 0, decGRNQuantity = 0, decSoldQuantity = 0;

                        DataTable datTemp = new DataTable();
                        datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "IsNull(GRNQuantity,0) as GRNQuantity,IsNull(SoldQuantity,0) as SoldQuantity,IsNull(TransferedQuantity,0) as TransferedQty", "InvItemStockDetails", "ItemID = " + dr.Cells[dgvColItemID.Index].Value.ToInt32() + " And BatchID = " + dr.Cells[dgvColBatchID.Index].Value.ToInt64() + " And WarehouseID = " + cboWarehouse.SelectedValue.ToInt32() });
                        if (datTemp.Rows.Count > 0)
                        {
                            decTransferedQty = datTemp.Rows[0]["TransferedQty"].ToDecimal();
                            decGRNQuantity = datTemp.Rows[0]["GRNQuantity"].ToDecimal();
                            decSoldQuantity = datTemp.Rows[0]["SoldQuantity"].ToDecimal();
                        }

                        if ((decGRNQuantity - decSoldQuantity) < ((decTransferedQty + decOldQuantity) - decCurrentQty))
                        {
                            intColumnIndex = dgvColQuantity.Index;
                            intRowIndex = dr.Index;
                            intMessageCode = (int)MaterialReturnMessageCodes.TransferedQtyExceedsStock;
                            break;
                        }

                    }
                    intItemCount++;
                }
            }

            if (intMessageCode > 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, intMessageCode, out MmsgMessageIcon).Replace("#", "");

                if(intMessageCode == (int)MaterialReturnMessageCodes.ItemSold || intMessageCode == (int)MaterialReturnMessageCodes.TransferedQtyExceedsStock)
                    MstrCommonMessage = MstrCommonMessage.Replace("*",dgvMaterialReturn[dgvColItemName.Index,intRowIndex].Value.ToStringCustom());

                MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                dgvMaterialReturn.Focus();
                dgvMaterialReturn.CurrentCell = dgvMaterialReturn[intColumnIndex, intRowIndex];
                return false;
            }
            else if (intItemCount == 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.SelectItemDetails, out MmsgMessageIcon).Replace("#", "");
                MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                dgvMaterialReturn.Focus();
                dgvMaterialReturn.CurrentCell = dgvMaterialReturn[dgvColItemCode.Index, 0];
                return false;
            }
            else
            {
             if (datMaterialReturnDetails.Rows.Count > 0)
            {
               
                foreach (DataRow dr in datMaterialReturnDetails.Rows)
                {
                    bool blnExists = false;
                    foreach (DataGridViewRow drGridRow in dgvMaterialReturn.Rows)
                    {
                        if(dr["ItemID"].ToInt32() == drGridRow.Cells[dgvColItemID.Index].Value.ToInt32() && dr["BatchID"].ToInt64() == drGridRow.Cells[dgvColBatchID.Index].Value.ToInt64())
                        {
                            blnExists = true;
                            break;
                        }
                    }

                    if (!blnExists)
                    {
                        decimal decOldQuantity = 0, decStockQty = 0;
                        //decStockQty = mobjClsBllCommonUtility.GetStockQuantity(dr["ItemID"].ToInt32(),dr["BatchID"].ToInt64(),cboCompany.SelectedValue.ToInt32(),cboWarehouse.SelectedValue.ToInt32());
                        decOldQuantity = mobjClsBllCommonUtility.ConvertQtyToBaseUnitQty(dr["UOMID"].ToInt32(), dr["ItemID"].ToInt32(), dr["Quantity"].ToDecimal(), 1);
                        //if (decStockQty - decOldQuantity < 0)
                        //{
                        //    //MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MaterialReturnMessageCodes.TransferedQtyExceedsStock, out MmsgMessageIcon).Replace("#", "");

                        //    //MstrCommonMessage = MstrCommonMessage.Replace("*", dr["ItemName"].ToStringCustom());

                        //    //MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        //    //dgvMaterialReturn.Focus();
                        //    //dgvMaterialReturn.CurrentCell = dgvMaterialReturn[dgvColItemName.Index, dgvMaterialReturn.RowCount - 1];
                        //    //return false;
                        //}
                          //decimal decTransferedQty = 0, decEmployeeIssuedQty = 0, decEmployeeReturnedQty = 0, decCurrentQty = 0;

                          //  decCurrentQty = mobjClsBllCommonUtility.ConvertQtyToBaseUnitQty(dr["UOMID"].ToInt32(), dr["ItemID"].ToInt32(), dr["Quantity"].ToInt32(), 1);
                          //  DataTable datTemp = new DataTable();
                          //  datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "IsNull(TransferedQuantity,0) as TransferedQty", "STStockDetails", "ItemID = " + dr["ItemID"].ToInt32() + " And BatchID = " + dr["BatchID"].ToInt64() + " And WarehouseID = " + cboWarehouse.SelectedValue.ToInt32() });
                          //  if (datTemp.Rows.Count > 0)
                          //      decTransferedQty = datTemp.Rows[0]["TransferedQty"].ToDecimal();
                          //  datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "IsNull(Sum(dbo.STConvertQtyToBaseUnitQty(MD.ItemID,MD.UOMID,1,MD.Quantity,1)),0) as IssuedQuantity", "STMaterialIssueDetails MD Inner Join STMaterialIssueMaster MI On MI.MaterialIssueID = MD.MaterialIssueID", "MI.EmployeeID = " + cboEmployee.SelectedValue.ToInt32() + " And MI.WarehouseID = " + cboEmployee.SelectedValue.ToInt32()+" And MD.ItemID = "+dr["ItemID"].ToInt32()+" And MD.BatchID = "+dr["BatchID"].ToInt64() });
                          //  if (datTemp.Rows.Count > 0)
                          //      decEmployeeIssuedQty = datTemp.Rows[0]["IssuedQuantity"].ToDecimal();
                          //  datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "IsNull(Sum(dbo.STConvertQtyToBaseUnitQty(MD.ItemID,MD.UOMID,1,MD.Quantity,1)),0) as ReturnedQuantity", "STMaterialIssueDetails MD Inner Join STMaterialIssueMaster MR On MR.MaterialIssueID = MD.MaterialIssueID", "MR.EmployeeID = " + cboEmployee.SelectedValue.ToInt32() + " And MR.WarehouseID = " + cboEmployee.SelectedValue.ToInt32() + " And MD.ItemID = " + dr["ItemID"].ToInt32() + " And MD.BatchID = " + dr["BatchID"].ToInt64() + " And MaterialReturnID <> " + MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnID });
                          //  if (datTemp.Rows.Count > 0)
                          //      decEmployeeReturnedQty = datTemp.Rows[0]["ReturnedQuantity"].ToDecimal();

                          //  if ((decEmployeeIssuedQty - decEmployeeReturnedQty) > (decTransferedQty - decCurrentQty))
                          //  {
                          //      MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MaterialReturnMessageCodes.EmployeeStockExceedsTransferedQty, out MmsgMessageIcon).Replace("*", dr["ItemName"].ToStringCustom().Replace("#", ""));
                          //      MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                          //      return false;
                          //  }
                        decimal decTransferedQty = 0, decGRNQuantity = 0, decSoldQuantity = 0;

                        DataTable datTemp = new DataTable();
                        datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "IsNull(GRNQuantity,0) as GRNQuantity,IsNull(SoldQuantity,0) as SoldQuantity,IsNull(TransferedQuantity,0) as TransferedQty", "InvItemStockDetails", "ItemID = " + dr["ItemID"].ToInt32() + " And BatchID = " + dr["BatchID"].ToInt64() + " And WarehouseID = " + cboWarehouse.SelectedValue.ToInt32() });
                        if (datTemp.Rows.Count > 0)
                        {
                            decTransferedQty = datTemp.Rows[0]["TransferedQty"].ToDecimal();
                            decGRNQuantity = datTemp.Rows[0]["GRNQuantity"].ToDecimal();
                            decSoldQuantity = datTemp.Rows[0]["SoldQuantity"].ToDecimal();
                        }

                        if ((decGRNQuantity - decSoldQuantity) < (decTransferedQty + decOldQuantity))
                        {
                            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MaterialReturnMessageCodes.TransferedQtyExceedsStock, out MmsgMessageIcon).Replace("#", "");

                                MstrCommonMessage = MstrCommonMessage.Replace("*", dr["ItemName"].ToStringCustom());

                                MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                                dgvMaterialReturn.Focus();
                                dgvMaterialReturn.CurrentCell = dgvMaterialReturn[dgvColItemName.Index, dgvMaterialReturn.RowCount - 1];
                                return false;
                        }
                    }
                }
            }

            }
            return true;
        }

        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private bool CheckDuplication()
        {
            bool blnDuplicate = false;
            int intRowIndex=-1;
            for (int intICounter = 0; intICounter < dgvMaterialReturn.Rows.Count; intICounter++)
            {
                int intItemID = dgvMaterialReturn.Rows[intICounter].Cells[dgvColItemID.Index].Value.ToInt32();
                long lngBatchID = dgvMaterialReturn.Rows[intICounter].Cells[dgvColBatchID.Index].Value.ToInt64();
                if (intItemID > 0)
                {
                    for (int intJCounter = intICounter + 1; intJCounter < dgvMaterialReturn.Rows.Count; intJCounter++)
                    {
                        if (dgvMaterialReturn.Rows[intJCounter].Cells[dgvColItemID.Index].Value.ToInt32() == intItemID && dgvMaterialReturn.Rows[intJCounter].Cells[dgvColBatchID.Index].Value.ToInt64() == lngBatchID)
                        {
                            blnDuplicate = true;
                            intRowIndex = intJCounter;
                            break;
                        }
                    }
                }
                if (blnDuplicate)
                    break;
            }

            if (blnDuplicate)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (int)MessageCode.CheckDuplication, out MmsgMessageIcon).Replace("#", "");
                MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                dgvMaterialReturn.Focus();
                dgvMaterialReturn.CurrentCell = dgvMaterialReturn[dgvColItemCode.Index, intRowIndex];
            }

            return blnDuplicate;
        }

      
        private void tmrMaterialReturn_Tick(object sender, EventArgs e)
        {
            ErrMaterialReturn.Clear();
            tmrMaterialReturn.Enabled = false;
        }

        private void expandableSplitterLeft_ExpandedChanged(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
        {
            if (expandableSplitterLeft.Expanded)
            {
                if (dgvMaterialReturnDisplay.Rows.Count > 0)
                    dgvMaterialReturnDisplay.Columns[0].Visible = false;
            }
        }

        private void dgvMaterialReturn_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvMaterialReturn.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvMaterialReturn.CurrentCell.OwningColumn.Name == "dgvColQuantity")
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
                int intUomScale = 0;
                if (dgvMaterialReturn[dgvColUOM.Index, dgvMaterialReturn.CurrentCell.RowIndex].Tag.ToInt32() != 0)
                    intUomScale = mobjClsBllCommonUtility.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvMaterialReturn[dgvColUOM.Index, dgvMaterialReturn.CurrentCell.RowIndex].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    if (intUomScale == 0)
                    {
                        if (e.KeyChar == '.')
                            e.Handled = true;
                    }
                    else
                    {
                        int dotIndex = -1;
                        if (txt.Text.Contains("."))
                        {
                            dotIndex = txt.Text.IndexOf('.');
                        }
                        if (e.KeyChar == '.')
                        {
                            if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            {
                                e.Handled = true;
                            }
                        }
                        else
                        {
                            if (char.IsDigit(e.KeyChar))
                            {
                                if (dotIndex != -1 && txt.SelectionStart > dotIndex)
                                {
                                    string[] splitText = txt.Text.Split('.');
                                    if (splitText.Length == 2)
                                    {
                                        if (splitText[1].Length - txt.SelectedText.Length >= intUomScale)
                                            e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void dgvMaterialReturn_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvMaterialReturn.IsCurrentCellDirty)
                {
                    if (dgvMaterialReturn.CurrentCell != null)
                    {
                        dgvMaterialReturn.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    }
                }
            }
            catch (Exception)
            { }
        }

        private void cboWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            int intCompanyID = 0;
            DataTable datWarehouse = mobjClsBllCommonUtility.FillCombos(new string[] { "CompanyID", "InvWarehouse", "WarehouseID = " + cboWarehouse.SelectedValue.ToInt32() });
            if (datWarehouse.Rows.Count > 0)
                intCompanyID = datWarehouse.Rows[0]["CompanyID"].ToInt32();
            cboCompany.SelectedValue = intCompanyID;

            dgvMaterialReturn.Rows.Clear();
            FillCombo(8);
            CalculateNetAmount();
            ChangeStatus(sender, e);
        }

        private void dgvMaterialReturn_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            CalculateNetAmount();
            ChangeStatus(null, null);
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillCombo(8);
            ChangeStatus(sender, e);
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {
            if (MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnID > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = MobjClsBLLMaterialReturnMaster.objClsDTOMaterialReturnMaster.MaterialReturnID;

                ObjViewer.PiFormID = (int)FormID.MaterialReturn;
                ObjViewer.ShowDialog();
            }
        }

        private void bnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Material Return";
                    ObjEmailPopUp.EmailFormType = EmailFormID.MaterialReturn;
                    ObjEmailPopUp.EmailSource = MobjClsBLLMaterialReturnMaster.GetMaterialReturnReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnEmail_Click() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in bnEmail_Click() " + ex.Message, 2);
            }
        }

        private void dgvMaterialReturn_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            mobjClsBllCommonUtility.SetSerialNo(dgvMaterialReturn,e.RowIndex,false);
        }

        private void dgvMaterialReturn_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            mobjClsBllCommonUtility.SetSerialNo(dgvMaterialReturn,e.RowIndex,false);
        }

        private void cboProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvMaterialReturn.Rows.Clear();
        }

     

    }
}
