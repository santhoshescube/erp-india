using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace MyBooksERP
{
    /// <summary>
    /// Summary description for frmItemMaster
    /// </summary>
    public partial class frmItemMaster : DevComponents.DotNetBar.Office2007Form
    {
        #region Private Variables
        bool MblnShowErrorMess = true, MblnTaxable;   // Checking whether error messages are showing or not; Company has tax

        string MstrMessageCommon; // Common messages to display

        DataTable MdatMessages;

        WebBrowser MobjBroswer;
        ToolStripControlHost Mobjtlstripcnthost;

        clsBLLItemMaster _objClsBLLItemMaster;
        ClsLogWriter MobjClsLogs;            // Log writer class
        ClsNotificationNew MobjClsNotification; // Object of the Notification class
        MessageBoxIcon MmsgMessageBoxIcon;

        private bool MblnPrintEmailPermission = false;    // Set Print and Email Permission
        private bool MblnAddPermission = true;           //Set Add Permission
        private bool MblnUpdatePermission = true;        //Set Update Permission
        private bool MblnDeletePermission = true;        //Set Delete Permission
        private bool MblnAddUpdatePermission = true;     //Set Add Update Permission
        private bool MblnCancelPermission = true;
        private bool MblnCostingTabPermission = true;

        // Set Cancel Permission
        private bool IsFirstTime = true;
        private bool MBlnChangeStatus = false;
        private string strItemcodePrefix;
        private int MintUOM = 0, MintPricingID = 0, MintBaseUomID = 0, MintSizeUomID = 0, MintWeightUomID = 0, MintSaleUomID = 0, MintPurchaseUomID = 0;
        double dblSaleRate = 0;
        #endregion

        private int ImageIndex = 0;

        private clsMessage objUserMessage = null;
        private bool loading = false;
        private bool messageShown = false;

        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.ItemMaster);

                return this.objUserMessage;
            }
        }

        public clsBLLItemMaster MobjClsBLLItemMaster
        {
            get
            {
                if (this._objClsBLLItemMaster == null)
                    this._objClsBLLItemMaster = new clsBLLItemMaster();

                return this._objClsBLLItemMaster;
            }
        }

        public List<clsFeatureTemp> Features
        {
            get { return this.MobjClsBLLItemMaster.objClsDTOItemMaster.Features; }
            set { this.MobjClsBLLItemMaster.objClsDTOItemMaster.Features = value; }
        }

        private List<clsDTOItemImages> RemovedImages = new List<clsDTOItemImages>();
        private clsBLLCommonUtility MobjClsCommonUtility = new clsBLLCommonUtility();

        public frmItemMaster(bool bTaxable)
        {
            InitializeComponent();

            MblnTaxable = bTaxable;           // Company has tax

            MobjClsLogs = new ClsLogWriter(Application.StartupPath);   // New object of LogWriter class
            MobjClsNotification = new ClsNotificationNew();   // New object of Notification class
            tmItemMaster.Interval = ClsCommonSettings.TimerInterval;   // Setting timer interval
            //showing webbrowser in toolstripsplitbtn
            MobjBroswer = new WebBrowser();
            MobjBroswer.ScrollBarsEnabled = false;
            MobjBroswer.Name = "UsedLevel";
            Mobjtlstripcnthost = new ToolStripControlHost(MobjBroswer);
            GetPrefix();
        }

        private void frmItemMaster_Load(object sender, EventArgs e)
        {
            loading = true;
            try
            {
                SetPermissions();
                SetControlPermissions();
                IsFirstTime = true;
                LoadMessage();  // Loading messages for display
                LoadCombos(0);   // Loading comboboxes with values            
                ResetForm();   // Setting the form for adding new item
                tmItemMaster.Enabled = true;
                btnAddBarcodeImage.Enabled = false;
                btnBarcodeCreate.Enabled = false;
                IsFirstTime = false;
            }
            finally
            {
                loading = false;
            }
        }

        /// <summary>
        /// Loading comboboxes in the form with values
        /// </summary>
        private bool LoadCombos(int intType)
        {
            try
            {
                DataTable datCombos = new DataTable();

                if (intType == 0 || intType == 1)    // Item Category filling
                {
                    datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "CategoryID, CategoryName", "InvCategoryReference", "" });
                    cboCategory.DisplayMember = "CategoryName";
                    cboCategory.ValueMember = "CategoryID";
                    cboCategory.DataSource = datCombos;
                }

                if (intType == 2) // Item sub category
                {
                    datCombos = null;
                    cboSubCategory.Text = string.Empty;
                    datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "SubCategoryID, SubCategoryName", "InvSubCategoryReference", "CategoryID=" + Convert.ToInt32(cboCategory.SelectedValue) });
                    cboSubCategory.DisplayMember = "SubCategoryName";
                    cboSubCategory.ValueMember = "SubCategoryID";
                    cboSubCategory.DataSource = datCombos;
                }

                if (intType == 0 || intType == 3) // Base UOM
                {
                    datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "UOMID, UOMName", "InvUOMReference", "" });
                    cboBaseUom.DisplayMember = "UOMName";
                    cboBaseUom.ValueMember = "UOMID";
                    cboBaseUom.DataSource = datCombos;
                }

                if (intType == 0 || intType == 4) // Costing Method
                {
                    datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "CostingMethodID, CostingMethod", "InvCostingMethodReference", "CostingMethodID >" + (int)CostingMethodReference.Batch + "" });
                    cboCostingMethod.DisplayMember = "CostingMethod";
                    cboCostingMethod.ValueMember = "CostingMethodID";
                    cboCostingMethod.DataSource = datCombos;
                }

                if (intType == 0 || intType == 5) // Manufacturer
                {
                    datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "ManufactureID, Manufacture", "InvManufactureReference", "" });
                    cboManufacturer.DisplayMember = "Manufacture";
                    cboManufacturer.ValueMember = "ManufactureID";
                    cboManufacturer.DataSource = datCombos;
                }

                if (intType == 0 || intType == 6) // Made-In
                {
                    datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "CountryID, CountryName", "CountryReference", "" });
                    cboMadeIn.DisplayMember = "CountryName";
                    cboMadeIn.ValueMember = "CountryID";
                    cboMadeIn.DataSource = datCombos;
                }

                if (intType == 0 || intType == 8) // Size UOM
                {
                    datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "UOMTypeID, UOMType", "InvUOMTypeReference", "" });
                    clmCboDimension.DisplayMember = "UOMType";
                    clmCboDimension.ValueMember = "UOMTypeID";
                    clmCboDimension.DataSource = datCombos;
                }

                if (intType == 0 || intType == 9) // Weight UOM
                {
                    datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "UOMID, UOMName", "InvUOMReference", "" });
                    clmCboDimensionUOM.DisplayMember = "UOMName";
                    clmCboDimensionUOM.ValueMember = "UOMID";
                    clmCboDimensionUOM.DataSource = datCombos;
                }

                if (intType == 0 || intType == 9) // Search Category
                {
                    datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "CategoryID, CategoryName", "InvCategoryReference", "" });
                    cboSearchCategory.DisplayMember = "CategoryName";
                    cboSearchCategory.ValueMember = "CategoryID";
                    cboSearchCategory.DataSource = datCombos;
                }

                if (intType == 10) // Search SubCategory
                {
                    datCombos = null;
                    cboSearchSubCategory.Text = string.Empty;
                    datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "SubCategoryID, SubCategoryName", "InvSubCategoryReference", "CategoryID=" + Convert.ToInt32(cboSearchCategory.SelectedValue.ToString()) + "" });
                    cboSearchSubCategory.DisplayMember = "SubCategoryName";
                    cboSearchSubCategory.ValueMember = "SubCategoryID";
                    cboSearchSubCategory.DataSource = datCombos;
                }

                if (intType == 0 || intType == 11) // Warehouse
                {
                    string strCondition = " CompanyID IN (" + ClsCommonSettings.CompanyID + ")" ;
                    //DataTable datCompany = new DataTable();
                    //datCompany = MobjClsBLLItemMaster.FillCombos(new string[] { "CompanyID", "CompanyMaster", "(CompanyID = " + ClsCommonSettings.LoginCompanyID + " Or ParentID = " + ClsCommonSettings.LoginCompanyID + ")" });
                    //foreach (DataRow dr in datCompany.Rows)
                    //{
                    //    strCondition += dr["CompanyID"].ToInt32() + ",";
                    //}
                    //strCondition = strCondition.Remove(strCondition.Length - 1);
                    //strCondition += ")";

                    datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "WarehouseID, WarehouseName", "InvWarehouse", strCondition });
                    dgvColWarehouse.DisplayMember = "WarehouseName";
                    dgvColWarehouse.ValueMember = "WarehouseID";
                    dgvColWarehouse.DataSource = datCombos;
                }

                //if (intType == 0 || intType == 12) // Uom Types
                //{
                //    // UOMTypeReference
                //    datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "TradingTypeID, TradingType", "InvTradingTypeReference", "" });
                //    dgvColUomType.DisplayMember = "TradingType";
                //    dgvColUomType.ValueMember = "TradingTypeID";
                //    dgvColUomType.DataSource = datCombos;
                //}

                if (intType == 0 || intType == 14) // Uom
                {
                    // StatusID = 69 => Extra Items
                    datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "StatusID, Status", "CommonStatusReference", "OperationTypeID=" + (int)OperationType.Products + "" });
                    cboStatus.DisplayMember = "Status";
                    cboStatus.ValueMember = "StatusID";
                    cboStatus.DataSource = datCombos;

                    cboStatus.SelectedValue = (int)OperationStatusType.ProductActive;
                }

                //if (intType == 0 || intType == 15) // Company
                //{
                //    datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                //    cboCompany.ValueMember = "CompanyID";
                //    cboCompany.DisplayMember = "CompanyName";
                //    cboCompany.DataSource = datCombos;
                //}

                if (intType == 0 || intType == 16) // Pricing Scheme
                {
                    //datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "PricingSchemeID, PricingSchemeName", "InvPricingSchemeReference", "" });
                    
                    cboPricingScheme.Text = "";
                   
                        datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "PricingSchemeID, PricingSchemeName", "InvPricingSchemeReference","CurrencyID IN (0," + 
                        "(SELECT CurrencyId FROM CompanyMaster WHERE CompanyID="+ClsCommonSettings.CompanyID+"))" });
                        cboPricingScheme.DisplayMember = "PricingSchemeName";
                        cboPricingScheme.ValueMember = "PricingSchemeID";
                        cboPricingScheme.DataSource = datCombos;
                    
                }
                if (intType == 0 || intType == 17) // pRODUCTtYPE
                {
                    // StatusID = 69 => Extra Items
                    datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "ProductTypeID, ProductType", "ProductTypeReference", " ProductTypeID IN (1,2,3) " });
                    cboProductType.DisplayMember = "ProductType";
                    cboProductType.ValueMember = "ProductTypeID";
                    cboProductType.DataSource = datCombos;

                    cboProductType.SelectedValue = (int)ProductType.InventoryItemSKU;
                }
               
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        /// <summary>
        /// Loading the variables for messages
        /// </summary>
        private void LoadMessage()
        {
            MdatMessages = MobjClsNotification.FillMessageArray((int)FormID.ItemMaster, 4);
        }

        /// <summary>
        /// Setting the form for adding new item
        /// </summary>
        /// <returns></returns>
        private bool ResetForm()
        {
            MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID = 0;
            LoadCombos(17);
            BindUOMSByDimensionID();
            ClearForm();    // Clear all fields in the form
            ClearSearchControls();
            FillItemsGrid();    // Filling the items in the grid            
            lblItemMasterStatus.Text = "Add new Information";
            GenerateItemCode();
            webBatchDetails.DocumentText = "";
            btnAddBarcodeImage.Enabled = false;
            btnBarcodeCreate.Enabled = false;
            MBlnChangeStatus = false;
            dblSaleRate = 0;

            // remove unsaved images
            this.RemoveImages();

            return true;

        }
        private void GenerateItemCode()
        {
            try
            {
                GetPrefix();
                txtItemCode.Text = strItemcodePrefix;//+ (MobjClsBLLItemMaster.GenerateItemCode() + 1).ToString();

                if (! ClsCommonSettings.blnProductCodeAutogenerate)
                    txtItemCode.ReadOnly = true;
                else
                    txtItemCode.ReadOnly = false;
                //LblSCountStatus.Text = "Total Quotations " + MobjClsBLLPurchase.GetLastPurchaseNo(1, false);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in GenerateItemCode() " + ex.Message);
                MobjClsLogs.WriteLog("Error in GenerateItemCode) " + ex.Message, 2);
                txtItemCode.Text = strItemcodePrefix + "1";
            }
        }
        /// <summary>
        /// Clearing fields in the form
        /// </summary>
        private void ClearForm()
        {
            //if (cboItemType.SelectedValue.ToInt32() == (int)eItemType.RawMaterial)
            //{
            cboBaseUom.Enabled = true;
            btnBaseUom.Enabled = true;
            //}
            //else
            //{
            //    cboBaseUom.SelectedValue = (int)PredefinedUOMs.Numbers;
            //    cboBaseUom.Enabled = false;
            //    btnBaseUom.Enabled = true;

            //}
            //cboCompany.Enabled = true;
            txtItemCode.Tag = "0";
            txtDescription.Clear();
            txtDescriptionArabic.Clear();
            txtItemCode.Clear();
            txtShortName.Clear();
            cboCategory.Text = "";
            cboSubCategory.SelectedIndex = -1;
            cboCategory.SelectedIndex = -1;
            cboSubCategory.Text = "";
            cboBaseUom.SelectedIndex = -1;
            //cboCompany.SelectedIndex = -1;
            //cboCompany.Text = "";
            cboBaseUom.Text = "";
            cboStatus.SelectedValue = (Int32)OperationStatusType.ProductActive;
            //cboStatus.Text = "";
            txtCutOffPrice.Clear();

            //txtColour.Clear();
            txtReorderPoint.Clear();
            txtReorderQuantity.Clear();
            txtGuaranteePeriod.Clear();
            txtModel.Clear();
            txtNote.Clear();
            txtWarrantyPeriod.Clear();

            rdbPurchaseRate.Checked = true;
            rdbActualRate.Checked = false;
            cboCostingMethod.SelectedIndex = -1;
            cboPricingScheme.SelectedIndex = -1;
           txtPurchaseRate.Text=txtSaleRate.Text = "0";
            //txtPurchaseRate.Text = "0";
            //txtActualRate.Text = "0";
            dgvBatchwiseCosting.DataSource = MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails = getNewTable();
            lblSaleRate.Visible = true;
            txtSaleRate.Visible = true;
            BtnShow.Visible = false;

            cboMadeIn.Text = "";
            cboTaxCode.Text = "";
            cboManufacturer.Text = "";
            cboMadeIn.SelectedIndex = -1;
            cboTaxCode.SelectedIndex = -1;
            cboManufacturer.SelectedIndex = -1;
            cboProductType.SelectedIndex = 0;
            rdbLandingCost.Checked = false;
            pbItemPhoto.Image = null;
            pbBarCode.Image = null;

            dgvItemLocations.Rows.Clear();
            dgvUnitsOfMeasurements.Rows.Clear();
            dgvDimensions.Rows.Clear();
            dgvFeatures.Rows.Clear();
            dgvRateHistory.DataSource = "";
            dgvRateHistory.Visible = false;
            gbSaleRateHistory.Visible = false;
            lblNoSaleRecordsFound.Visible = true;

            // Disable buttons
            bnAddNewItem.Enabled = false;
            bnDeleteItem.Enabled = false;
            bnEmail.Enabled = false;
            bnPrint.Enabled = false;

            mnuAddImage.Visible = false;
            pbItemPhoto.Tag = "";
            errorItemMaster.Clear();


            this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images.Clear();
            this.MobjClsBLLItemMaster.objClsDTOItemMaster.Features.Clear();
            this.MobjClsBLLItemMaster.objClsDTOItemMaster.Dimensions.Clear();

            // clear dimension grid
            this.dgvDimensions.Rows.Clear();
            // clear feature grid
            this.dgvFeatures.Rows.Clear();

            //clear UOM grid
            this.dgvUnitsOfMeasurements.Rows.Clear();
 
            // clear photo in picture box (if any)
            this.pbItemPhoto.Image = null;
            this.pbItemPhoto.Tag = null;
            // clear imageIndex and image text
            this.ImageIndex = 0;
            this.lblImageText.Text = "0 of 0";

            this.EnableDisableButtons();
            if(cboCostingMethod.Items.Count>0)
                cboCostingMethod.SelectedValue =(int) CostingMethodReference.NONE;
            if(cboPricingScheme.Items.Count>0)
                cboPricingScheme.SelectedIndex = 0;

            MBlnChangeStatus = false;
        }

        private void ClearSearchControls()
        {
            cboSearchCategory.SelectedIndex = -1;
            cboSearchSubCategory.SelectedIndex = -1;
            txtSearchCode.Clear();
            txtSearchDescription.Clear();
            tabItemMaster.SelectedTab = tbpGeneral;
        }

        /// <summary>
        /// Filling the items in the grid
        /// </summary>
        private void FillItemsGrid()
        {
            int intTotalRows;

            // Searching an item according to the item code/description
            DataTable datItems = MobjClsBLLItemMaster.SearchItem(Convert.ToInt32(cboSearchCategory.SelectedValue),
                Convert.ToInt32(cboSearchSubCategory.SelectedValue), txtSearchDescription.Text.Trim().Replace("'", "''"),
                txtSearchCode.Text.Trim().Replace("'", "''"),ClsCommonSettings.CompanyID, out intTotalRows);

            datItems.Columns["ItemID"].ColumnName = "ItemID";
            datItems.Columns["Code"].ColumnName = "Code";
            datItems.Columns["ItemName"].ColumnName = "Name";
            dgvItems.DataSource = null;
            dgvItems.DataSource = datItems.DefaultView.ToTable(true, "ItemID", "Code", "Name"); ;
            dgvItems.Columns[0].Visible = false;
            dgvItems.Columns[1].Visible = false;
            dgvItems.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            lblCountStatus.Text = "Total Items: " + intTotalRows.ToString();
        }

        /// <summary>
        /// Validating the fields in the form and showing the validation messages
        /// </summary>
        /// <returns></returns>
        private bool ItemMasterValidation()
        {
            bool bValidationOk = true;
            errorItemMaster.Clear();
           
            //if (cboCompany.SelectedIndex != -1)
            //{
            //    intCompanyId = cboCompany.SelectedValue.ToInt32();
            //}
            //else
            //{
            //    intCompanyId = 0;
            //}
            
            if (txtItemCode.Text.Trim().Length == 0)    // Validating Item code
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1601, out MmsgMessageBoxIcon);
                errorItemMaster.SetError(txtItemCode, MstrMessageCommon.Replace("#", "").Trim());
                txtItemCode.Focus();
                bValidationOk = false;
            }
            else if (txtDescription.Text.Trim().Length == 0)    // Validating item description
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1602, out MmsgMessageBoxIcon);
                errorItemMaster.SetError(txtDescription, MstrMessageCommon.Replace("#", "").Trim());
                txtDescription.Focus();
                bValidationOk = false;
            }
            //else if (Convert.ToInt32(cboCategory.SelectedValue) == 0)   // Validating item category
            //{
            //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1604, out MmsgMessageBoxIcon);
            //    errorItemMaster.SetError(cboCategory, MstrMessageCommon.Replace("#", "").Trim());
            //    cboCategory.Focus();
            //    bValidationOk = false;
            //}
            //else if (Convert.ToInt32(cboSubCategory.SelectedValue) == 0)   // Validating item Subcategory
            //{
            //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1630, out MmsgMessageBoxIcon);
            //    errorItemMaster.SetError(cboSubCategory, MstrMessageCommon.Replace("#", "").Trim());
            //    cboSubCategory.Focus();
            //    bValidationOk = false;
            //}
            else if (Convert.ToInt32(cboBaseUom.SelectedValue) == 0)    // Validating base uom
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1615, out MmsgMessageBoxIcon);
                errorItemMaster.SetError(cboBaseUom, MstrMessageCommon.Replace("#", "").Trim());
                cboBaseUom.Focus();
                bValidationOk = false;
            }
            else if (Convert.ToInt32(cboDefaultPurchaseUom.SelectedValue) == 0)    // Validating default purchase uom
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1653, out MmsgMessageBoxIcon);
                errorItemMaster.SetError(cboDefaultPurchaseUom, MstrMessageCommon.Replace("#", "").Trim());
                tabItemMaster.SelectedTab = tbpMeasurements;
                cboDefaultPurchaseUom.Focus();
                bValidationOk = false;
            }
            else if (Convert.ToInt32(cboDefaultSalesUom.SelectedValue) == 0)    // Validating default sales uom
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1654, out MmsgMessageBoxIcon);
                errorItemMaster.SetError(cboDefaultPurchaseUom, MstrMessageCommon.Replace("#", "").Trim());
                tabItemMaster.SelectedTab = tbpMeasurements;
                cboDefaultSalesUom.Focus();
                bValidationOk = false;
            }
            else if (!txtItemCode.ReadOnly && MobjClsBLLItemMaster.DuplicateItemCodeExists(txtItemCode.Text.Trim(),ClsCommonSettings.CompanyID)) // Validating duplicate item code
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1606, out MmsgMessageBoxIcon);
                errorItemMaster.SetError(txtItemCode, MstrMessageCommon.Replace("#", "").Trim());
                txtItemCode.Focus();
                bValidationOk = false;
            }
            else if (this.cboStatus.SelectedIndex == -1)
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1660, out MmsgMessageBoxIcon);
                errorItemMaster.SetError(this.cboStatus, MstrMessageCommon.Replace("#", "").Trim());
                this.cboStatus.Focus();
                bValidationOk = false;
            }
            
            if (cboCostingMethod.SelectedValue.ToInt32() == 0)
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1657, out MmsgMessageBoxIcon);
                errorItemMaster.SetError(cboCostingMethod, MstrMessageCommon.Replace("#", "").Trim());
                tabItemMaster.SelectedTab = tbpCosting;
                cboCostingMethod.Focus();
                bValidationOk = false;
            }
            if (cboPricingScheme.SelectedValue.ToInt32() == 0)
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1658, out MmsgMessageBoxIcon);
                errorItemMaster.SetError(cboPricingScheme, MstrMessageCommon.Replace("#", "").Trim());
                tabItemMaster.SelectedTab = tbpCosting;
                cboPricingScheme.Focus();
                bValidationOk = false;
            }
            

            if (!bValidationOk) // If the validation fails
            {
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageBoxIcon);
                lblItemMasterStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmItemMaster.Enabled = true;
            }
            return bValidationOk;
        }
        private bool ItemMasterCategoryValidation()
        {
            bool bValidationOk = true;
            errorItemMaster.Clear();
            //if (Convert.ToInt32(cboCategory.SelectedValue) == 0)   // Validating item category
            //{
            //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1604, out MmsgMessageBoxIcon);
            //    errorItemMaster.SetError(cboCategory, MstrMessageCommon.Replace("#", "").Trim());
            //    cboCategory.Focus();
            //    bValidationOk = false;
            //}
            //if (!bValidationOk) // If the validation fails
            //{
            //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageBoxIcon);
            //    lblItemMasterStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            //    tmItemMaster.Enabled = true;
            //}
            return bValidationOk;

        }

        /// <summary>
        /// Validating UOM grid
        /// </summary>
        /// <returns></returns>
        private bool UOMGridValidation()
        {
            dgvUnitsOfMeasurements.CommitEdit(DataGridViewDataErrorContexts.Commit);

            //if (!CheckInvalidUOM())
            //{
            //    this.UserMessage.ShowMessage(1675, this.dgvUnitsOfMeasurements);
            //    tabItemMaster.SelectedTab = tbpMeasurements;
            //    dgvUnitsOfMeasurements.Focus();
            //    return false;
            //}
            return true;
        }

        private bool CheckInvalidUOM()
        {
            bool selected = false;

            // Validating for duplicate UOM
            foreach (DataGridViewRow dgvRow in dgvUnitsOfMeasurements.Rows)
            {
                if (dgvRow.Cells[0].Value.ToBoolean())
                {
                    selected = true;
                    break;
                }
            }

            return selected;
        }

        /// <summary>
        /// Validating Item Location grid
        /// </summary>
        /// <returns></returns>
        /// 

        private bool CheckInputType(object o)
        {
            decimal temp;
            // Int32.TryParse(TxtNoLocation.Text, out temp))
            if (!(decimal.TryParse(o.ToString(), out  temp)))
            {
                return false;
            }
            else
                return true;
        }
        private bool ItemLocationGridValidation()
        {
            bool blnValid = true;
            List<string> lsLocations = new List<string>();

            dgvItemLocations.CommitEdit(DataGridViewDataErrorContexts.Commit);
            // Validating for duplicate Locations
            foreach (DataGridViewRow dgvRow in dgvItemLocations.Rows)
            {
                if (Convert.ToString(dgvRow.Cells[dgvColWarehouse.Index].Value) != "")
                {
                    string strItemLocation = Convert.ToString(dgvRow.Cells[dgvColWarehouse.Index].Value);
                    if (Convert.ToInt32(dgvRow.Cells[dgvColWarehouse.Index].Value) > 0) lsLocations.Add(strItemLocation);

                    foreach (DataGridViewRow dgvLocationRow in dgvItemLocations.Rows)
                    {
                        if (dgvItemLocations.CurrentRow != null)
                        {
                            if ((Convert.ToString(dgvLocationRow.Cells[dgvColWarehouse.Index].Value).Trim().Length > 0))
                            {
                                if (Convert.ToString(dgvLocationRow.Cells[dgvColLot.Index].Value).Trim() == "" && Convert.ToString(dgvLocationRow.Cells[dgvColReorderPoint.Index].Value).Trim() == "")
                                {
                                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1621, out MmsgMessageBoxIcon);
                                    dgvItemLocations.CurrentCell = dgvLocationRow.Cells[dgvColWarehouse.Index];
                                    blnValid = false;
                                    break;
                                }
                                if (Convert.ToString(dgvLocationRow.Cells[dgvColLocation.Index].Value).Trim().Length > 0 && Convert.ToString(dgvLocationRow.Cells[dgvColLot.Index].Value).Trim() == "")
                                {
                                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1621, out MmsgMessageBoxIcon);
                                    dgvItemLocations.CurrentCell = dgvLocationRow.Cells[dgvColWarehouse.Index];
                                    blnValid = false;
                                    break;
                                }
                            }

                            if (Convert.ToString(dgvLocationRow.Cells[dgvColWarehouse.Index].Value).Trim().Length == 0) { }
                            else if (!(Convert.ToString(dgvLocationRow.Cells[dgvColWarehouse.Index].Value).Trim().Length > 0))
                            {
                                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1621, out MmsgMessageBoxIcon);
                                dgvItemLocations.CurrentCell = dgvLocationRow.Cells[dgvColWarehouse.Index];
                                blnValid = false;
                                break;
                            }

                            if (Convert.ToString(dgvLocationRow.Cells[dgvColWarehouse.Index].Value).Trim().Length == 0) { }
                            else if (dgvRow.Index != dgvLocationRow.Index && dgvLocationRow.Cells[dgvColWarehouse.Index].Value != null)
                            {
                                if ((Convert.ToString(dgvLocationRow.Cells[dgvColWarehouse.Index].Value).ToString()).Equals(strItemLocation))
                                {
                                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1612, out MmsgMessageBoxIcon);
                                    dgvItemLocations.CurrentCell = dgvLocationRow.Cells[dgvColWarehouse.Index];
                                    blnValid = false;
                                    break;
                                }
                            }
                            else if (dgvItemLocations[dgvColReorderPoint.Index, dgvLocationRow.Index].Value != null && !CheckInputType(dgvItemLocations[dgvColReorderPoint.Index, dgvLocationRow.Index].Value))
                            {
                                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1655, out MmsgMessageBoxIcon);// "Reorder point value type should be numeric";
                                dgvItemLocations.CurrentCell = dgvLocationRow.Cells[dgvColReorderPoint.Index];
                                blnValid = false;
                                break;
                            }
                            //else if (Convert.ToDecimal(dgvItemLocations[dgvColReorderPoint.Index, dgvLocationRow.Index].Value) <= 0)
                            //{
                            //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1650, out MmsgMessageBoxIcon);
                            //    MstrMessageCommon = MstrMessageCommon.Replace("*", "Point");
                            //    dgvItemLocations.CurrentCell = dgvLocationRow.Cells[dgvColReorderPoint.Index];
                            //    blnValid = false;
                            //    break;
                            //}
                            else if (dgvItemLocations[dgvColReorderQuantity.Index, dgvLocationRow.Index].Value != null && !CheckInputType(dgvItemLocations[dgvColReorderQuantity.Index, dgvLocationRow.Index].Value))
                            {
                                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1656, out MmsgMessageBoxIcon);//"Reorder quantity value type should be numeric";
                                dgvItemLocations.CurrentCell = dgvLocationRow.Cells[dgvColReorderQuantity.Index];
                                blnValid = false;
                                break;
                            }
                            //else if (Convert.ToDecimal(dgvItemLocations[dgvColReorderQuantity.Index, dgvLocationRow.Index].Value) <= 0)
                            //{
                            //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1650, out MmsgMessageBoxIcon);
                            //    MstrMessageCommon = MstrMessageCommon.Replace("*", "Quantity");
                            //    dgvItemLocations.CurrentCell = dgvLocationRow.Cells[dgvColReorderQuantity.Index];
                            //    blnValid = false;
                            //    break;
                            //}
                        }
                    }
                }
            }

            //if (lsLocations.Count == 0)
            //{
            //    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1610, out MmsgMessageBoxIcon);
            //    dgvItemLocations.CurrentCell = dgvItemLocations.Rows[0].Cells[dgvColWarehouse.Index];
            //    blnValid = false;
            //}

            if (blnValid == false)
            {
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageBoxIcon);
                lblItemMasterStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmItemMaster.Enabled = true;
                tabItemMaster.SelectedTab = tbpLocation;
                //  dgvItemLocations.Focus();
            }
            errorItemMaster.Clear();
            return blnValid;
        }

        /// <summary>
        /// Saving new item details
        /// </summary>
        /// <returns></returns>
        private bool SaveItemDetails()
        {
            try
            {
                if (MessageBox.Show(MobjClsNotification.GetErrorMessage(MdatMessages,
                    (MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID == 0 ? 1622 : 1623), out MmsgMessageBoxIcon).Replace("#", "").Trim(),
                    ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return false;
                else
                {
                    if (!ValidateOnSave())
                        return false;
                }

                if (MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID == 0 && txtItemCode.ReadOnly && MobjClsBLLItemMaster.DuplicateItemCodeExists(txtItemCode.Text.Trim(), ClsCommonSettings.CompanyID))
                {
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 9002, out MmsgMessageBoxIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("* no", "Item Code");
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    else
                        GenerateItemCode();
                }

                // Setting values to the business object

                MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID = txtItemCode.Tag.ToInt32();
                MobjClsBLLItemMaster.objClsDTOItemMaster.strItemCode = txtItemCode.Text.Trim().Replace("'", "`");
                MobjClsBLLItemMaster.objClsDTOItemMaster.strShortName = txtShortName.Text.Trim().Replace("'", "`");
                MobjClsBLLItemMaster.objClsDTOItemMaster.strDescription = txtDescription.Text.Trim().Replace("'", "`");
                MobjClsBLLItemMaster.objClsDTOItemMaster.strDescriptionArabic = txtDescriptionArabic.Text.Trim().Replace("'", "`");
                MobjClsBLLItemMaster.objClsDTOItemMaster.intSubCategoryID = cboSubCategory.SelectedValue.ToInt32();
                MobjClsBLLItemMaster.objClsDTOItemMaster.intCategoryID = cboCategory.SelectedValue.ToInt32();
                MobjClsBLLItemMaster.objClsDTOItemMaster.intBaseUomID = cboBaseUom.SelectedValue.ToInt32();
                MobjClsBLLItemMaster.objClsDTOItemMaster.intDefaultPurchaseUomID = cboDefaultPurchaseUom.SelectedValue.ToInt32();
                MobjClsBLLItemMaster.objClsDTOItemMaster.intDefaultSalesUomID = cboDefaultSalesUom.SelectedValue.ToInt32();
                MobjClsBLLItemMaster.objClsDTOItemMaster.intCompanyID = ClsCommonSettings.CompanyID;
                MobjClsBLLItemMaster.objClsDTOItemMaster.intItemStatusID = cboStatus.SelectedValue.ToInt32();
                MobjClsBLLItemMaster.objClsDTOItemMaster.intCreatedBy = ClsCommonSettings.UserID;
                MobjClsBLLItemMaster.objClsDTOItemMaster.decCutOffPrice = (txtCutOffPrice.Text == "") ? 0 : txtCutOffPrice.Text.ToDecimal();
                MobjClsBLLItemMaster.objClsDTOItemMaster.decReorderLevel = txtReorderPoint.Text.Trim().Length > 0 ? txtReorderPoint.Text.ToDecimal() : 0;
                MobjClsBLLItemMaster.objClsDTOItemMaster.decReorderQuantity = txtReorderPoint.Text.Trim().Length > 0 ? txtReorderQuantity.Text.ToDecimal() : 0;
                MobjClsBLLItemMaster.objClsDTOItemMaster.strGuaranteePeriod = txtGuaranteePeriod.Text.Trim().Replace("'", "`");
                MobjClsBLLItemMaster.objClsDTOItemMaster.intMadeInID = cboMadeIn.SelectedValue.ToInt32();
                MobjClsBLLItemMaster.objClsDTOItemMaster.intManufacturerID = cboManufacturer.SelectedValue.ToInt32();
                MobjClsBLLItemMaster.objClsDTOItemMaster.strModel = txtModel.Text.Trim().Replace("'", "`");
                MobjClsBLLItemMaster.objClsDTOItemMaster.strNotes = txtNote.Text.Trim().Replace("'", "`");
                MobjClsBLLItemMaster.objClsDTOItemMaster.intTaxID = cboTaxCode.SelectedValue.ToInt32();
                MobjClsBLLItemMaster.objClsDTOItemMaster.strWarrantyPeriod = txtWarrantyPeriod.Text.Trim().Replace("'", "`");
                MobjClsBLLItemMaster.objClsDTOItemMaster.strImageFile = pbItemPhoto.Tag.ToStringCustom();
                MobjClsBLLItemMaster.objClsDTOItemMaster.intCostingMethodID = cboCostingMethod.SelectedValue.ToInt32();

                if (cboPricingScheme.SelectedValue != null)
                    MobjClsBLLItemMaster.objClsDTOItemMaster.intPricingSchemeID = cboPricingScheme.SelectedValue.ToInt32();
                if (cboProductType.SelectedValue != null)
                    MobjClsBLLItemMaster.objClsDTOItemMaster.intProductTypeID = cboProductType.SelectedValue.ToInt32();

                MobjClsBLLItemMaster.objClsDTOItemMaster.decPurchaseRate = txtPurchaseRate.Text.Trim().Length > 0 ? txtPurchaseRate.Text.ToDecimal() : 0;
                MobjClsBLLItemMaster.objClsDTOItemMaster.decActualRate = txtActualRate.Text.Trim().Length > 0 ? txtActualRate.Text.ToDecimal()  : 0;
                MobjClsBLLItemMaster.objClsDTOItemMaster.decSaleRate = txtSaleRate.Text.Trim().Length > 0 ? txtSaleRate.Text.ToDecimal() : 0;                
                MobjClsBLLItemMaster.objClsDTOItemMaster.blnCalculationBasedOnRate = rdbPurchaseRate.Checked;
                MobjClsBLLItemMaster.objClsDTOItemMaster.blnExpiryDateMandatory = chkExpiryDateMandatory.Checked;
                MobjClsBLLItemMaster.objClsDTOItemMaster.blnIsLandingCostEffected = rdbLandingCost.Checked;
                if (pbBarCode.Image != null)
                    MobjClsBLLItemMaster.objClsDTOItemMaster.imgBarcodeImage = new ClsImages().GetImageDataStream(pbBarCode.Image,
                        System.Drawing.Imaging.ImageFormat.Jpeg);
                else
                    MobjClsBLLItemMaster.objClsDTOItemMaster.imgBarcodeImage = null;

                this.SetFeatures();

                SetItemSorageLocations();   // Setting item storage locations into the object
                SetItemUOMs();  // Setting item UOMs into the object


                // Calling the business method for saving item details
                if (MobjClsBLLItemMaster.SaveItemInfo())
                {
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages,
                        (MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID == 0 ? 1631 : 1632), out MmsgMessageBoxIcon);

                    // Setting the returned ItemID
                    txtItemCode.Tag = MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID.ToString();
                    MobjClsLogs.WriteLog("Saved successfully:SaveItemDetails()  " + this.Name + "", 0);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on Save:SaveItemDetails() " + this.Name + " " + Ex.Message.ToString(), 3);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on SaveItemDetails() " + Ex.Message.ToString());

                return false;
            }
        }

        private bool ValidateOnSave()
        {
            if (MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID == 0 && MobjClsBLLItemMaster.DuplicateItemCodeExists(txtItemCode.Text.Trim(), 0))
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 9002, out MmsgMessageBoxIcon);
                MstrMessageCommon = MstrMessageCommon.Replace("* no", "Item Code");
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return false;
                else
                    GenerateItemCode();
            }
            return true;
        }
        private void SetFeatures()
        {
            this.MobjClsBLLItemMaster.objClsDTOItemMaster.Dimensions.Clear();

            // set dimensions
            if (this.dgvDimensions.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in this.dgvDimensions.Rows)
                {
                    if (row.Cells[0].Value.ToInt32() > 0 && row.Cells[2].Value.ToInt32() > 0)
                    {
                        this.MobjClsBLLItemMaster.objClsDTOItemMaster.Dimensions.Add(new clsDTOItemDimension
                        {
                            DimensionID = row.Cells[0].Value.ToInt32(),
                            DimensionValue = row.Cells[1].Value.ToDecimal(),
                            UOMID = row.Cells[2].Value.ToInt32()
                        });
                    }
                }
            }
        }

        private void BindUOMSByItemID(Int64 ItemID, DataGridViewComboBoxColumn cbo)
        {
            cbo.DataSource = null;
            cbo.DataSource = clsBLLItemMaster.GetUOMsByItemID(ItemID);
            cbo.ValueMember = "UOMID";
            cbo.DisplayMember = "UOMName";
        }


        /// <summary>
        /// Setting item storage locations into the object
        /// </summary>
        private void SetItemSorageLocations()
        {
            MobjClsBLLItemMaster.objClsDTOItemMaster.objclsDTOItemReorder = new List<clsDTOItemReorder>();
            foreach (DataGridViewRow dgvRowLocation in dgvItemLocations.Rows)
            {
                if (dgvRowLocation.Cells[dgvColWarehouse.Index].Value != null && Convert.ToString(dgvRowLocation.Cells[dgvColWarehouse.Index].Value) != "")
                {
                    if (Convert.ToInt32(dgvRowLocation.Cells[dgvColWarehouse.Index].Value) != 0)
                    {
                        clsDTOItemReorder objItemReorder = new clsDTOItemReorder();
                        objItemReorder.intItemID = MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID;
                        objItemReorder.decReorderLevel =
                            dgvRowLocation.Cells[dgvColReorderPoint.Index].Value == null || Convert.ToString(dgvRowLocation.Cells[dgvColReorderPoint.Index].Value) == "" ? 0 : Convert.ToDecimal(dgvRowLocation.Cells[dgvColReorderPoint.Index].Value);
                        objItemReorder.decReorderQuantity =
                            dgvRowLocation.Cells[dgvColReorderQuantity.Index].Value == null || Convert.ToString(dgvRowLocation.Cells[dgvColReorderQuantity.Index].Value) == "" ? 0 : Convert.ToDecimal(dgvRowLocation.Cells[dgvColReorderQuantity.Index].Value);
                        objItemReorder.intWarehouseID = Convert.ToInt32(dgvRowLocation.Cells[dgvColWarehouse.Index].Value);

                        if (Convert.ToString(dgvRowLocation.Cells[dgvColWarehouse.Index].Value) != "" &&
                            dgvRowLocation.Cells["dgvColLocation"].Tag != null && Convert.ToString(dgvRowLocation.Cells["dgvColLocation"].Tag) != "" &&
                            dgvRowLocation.Cells["dgvColRow"].Tag != null && Convert.ToString(dgvRowLocation.Cells["dgvColRow"].Tag) != "" &&
                            dgvRowLocation.Cells["dgvColBlock"].Tag != null && Convert.ToString(dgvRowLocation.Cells["dgvColBlock"].Tag) != "" &&
                            dgvRowLocation.Cells["dgvColLot"].Tag != null && Convert.ToString(dgvRowLocation.Cells["dgvColLot"].Tag) != "")
                        {
                            if (Convert.ToInt32(dgvRowLocation.Cells[dgvColWarehouse.Index].Value) != 0 && Convert.ToInt32(dgvRowLocation.Cells["dgvColLocation"].Tag) != 0 &&
                                 Convert.ToInt32(dgvRowLocation.Cells["dgvColRow"].Tag) != 0 && Convert.ToInt32(dgvRowLocation.Cells["dgvColBlock"].Tag) != 0 && Convert.ToInt32(dgvRowLocation.Cells["dgvColLot"].Tag) != 0)
                            {
                                objItemReorder.intLocationID = Convert.ToInt32(dgvRowLocation.Cells["dgvColLocation"].Tag);
                                objItemReorder.intRowID = Convert.ToInt32(dgvRowLocation.Cells["dgvColRow"].Tag);
                                objItemReorder.intBlockID = Convert.ToInt32(dgvRowLocation.Cells["dgvColBlock"].Tag);
                                objItemReorder.intLotID = Convert.ToInt32(dgvRowLocation.Cells["dgvColLot"].Tag);
                            }
                        }
                        MobjClsBLLItemMaster.objClsDTOItemMaster.objclsDTOItemReorder.Add(objItemReorder);
                    }
                }
            }
        }

        /// <summary>
        /// Setting item UOMs into the object
        /// </summary>
        private void SetItemUOMs()
        {
            MobjClsBLLItemMaster.objClsDTOItemMaster.objclsDTOItemUOM = new List<clsDTOItemUOM>();
            foreach (DataGridViewRow dgvRowUOM in dgvUnitsOfMeasurements.Rows)
            {
                if (dgvRowUOM.Cells[0].Value.ToBoolean() && dgvRowUOM.Cells[1].Tag.ToInt32() > 0)
                {
                    clsDTOItemUOM objDTOItemUOM = new clsDTOItemUOM();
                    objDTOItemUOM.intItemID = MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID;
                    //objDTOItemUOM.intItemUOMType = Convert.ToInt32(dgvRowUOM.Cells[dgvColUomType.Index].Value);
                    objDTOItemUOM.intUOMID = Convert.ToInt32(dgvRowUOM.Cells[1].Tag);
                    MobjClsBLLItemMaster.objClsDTOItemMaster.objclsDTOItemUOM.Add(objDTOItemUOM);
                }
            }
        }

        /// <summary>
        /// Displaying item storage locations
        /// </summary>
        private void DisplayItemReorder()
        {
            int intRowIndex = 0;
            dgvItemLocations.Rows.Clear();
            // Reading item storage locations from the object
            foreach (clsDTOItemReorder objclsDTOItemReorder in MobjClsBLLItemMaster.objClsDTOItemMaster.objclsDTOItemReorder)
            {
                dgvItemLocations.RowCount = dgvItemLocations.RowCount + 1;
                dgvItemLocations[dgvColWarehouse.Index, intRowIndex].Value = (object)objclsDTOItemReorder.intWarehouseID;
                dgvItemLocations[dgvColReorderPoint.Index, intRowIndex].Value = Convert.ToString(objclsDTOItemReorder.decReorderLevel);
                dgvItemLocations[dgvColReorderQuantity.Index, intRowIndex].Value = Convert.ToString(objclsDTOItemReorder.decReorderQuantity);
                dgvItemLocations[dgvColSerialNo.Index, intRowIndex].Value = Convert.ToString(objclsDTOItemReorder.intSerialNo);

                FillLocationGridCombo(dgvColLocation.Index, objclsDTOItemReorder.intWarehouseID, 0, 0, 0);

                dgvItemLocations[dgvColLocation.Index, intRowIndex].Value = objclsDTOItemReorder.intLocationID;
                dgvItemLocations[dgvColLocation.Index, intRowIndex].Tag = objclsDTOItemReorder.intLocationID;
                dgvItemLocations[dgvColLocation.Index, intRowIndex].Value = dgvItemLocations[dgvColLocation.Index, intRowIndex].FormattedValue;

                FillLocationGridCombo(dgvColRow.Index, objclsDTOItemReorder.intWarehouseID, objclsDTOItemReorder.intLocationID, 0, 0);

                dgvItemLocations[dgvColRow.Index, intRowIndex].Value = objclsDTOItemReorder.intRowID;
                dgvItemLocations[dgvColRow.Index, intRowIndex].Tag = objclsDTOItemReorder.intRowID;
                dgvItemLocations[dgvColRow.Index, intRowIndex].Value = dgvItemLocations[dgvColRow.Index, intRowIndex].FormattedValue;

                FillLocationGridCombo(dgvColBlock.Index, objclsDTOItemReorder.intWarehouseID, objclsDTOItemReorder.intLocationID, objclsDTOItemReorder.intRowID, 0);

                dgvItemLocations[dgvColBlock.Index, intRowIndex].Value = objclsDTOItemReorder.intBlockID;
                dgvItemLocations[dgvColBlock.Index, intRowIndex].Tag = objclsDTOItemReorder.intBlockID;
                dgvItemLocations[dgvColBlock.Index, intRowIndex].Value = dgvItemLocations[dgvColBlock.Index, intRowIndex].FormattedValue;

                FillLocationGridCombo(dgvColLot.Index, objclsDTOItemReorder.intWarehouseID, objclsDTOItemReorder.intLocationID, objclsDTOItemReorder.intRowID, objclsDTOItemReorder.intBlockID);

                dgvItemLocations[dgvColLot.Index, intRowIndex].Value = objclsDTOItemReorder.intLotID;
                dgvItemLocations[dgvColLot.Index, intRowIndex].Tag = objclsDTOItemReorder.intLotID;
                dgvItemLocations[dgvColLot.Index, intRowIndex].Value = dgvItemLocations[dgvColLot.Index, intRowIndex].FormattedValue;
                intRowIndex++;
            }
        }

        /// <summary>
        /// Displaying item UOMs
        /// </summary>
        private void DisplayItemUOMs()
        {
            // Reading item UOMs from the object
            foreach (clsDTOItemUOM objclsDTOItemUOM in MobjClsBLLItemMaster.objClsDTOItemMaster.objclsDTOItemUOM)
            {
                for (int i = 0; i < this.dgvUnitsOfMeasurements.Rows.Count; i++)
                {
                    if (this.dgvUnitsOfMeasurements.Rows[i].Cells[1].Tag.ToInt32() == objclsDTOItemUOM.intUOMID)
                    {
                        this.dgvUnitsOfMeasurements.Rows[i].Cells[0].Value = true;
                    }
                }
            }
        }

        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void bnSaveItem_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if (dgvBatchwiseCosting.CurrentRow != null)
            //    {
            //        dgvBatchwiseCosting.CurrentCell = dgvBatchwiseCosting.CurrentRow.Cells["ColBatchNo"];
            //        dgvBatchwiseCosting.BeginEdit(true); // For Batchwise Rate Updation
            //    }
            //}
            //catch { }
            SaveItemMaster();   // Save item details
        }

        /// <summary>
        /// Validating and saving item master details
        /// </summary>
        private void SaveItemMaster()
        {
            // Validation for the fields
            dgvItemLocations.EndEdit(DataGridViewDataErrorContexts.Commit);

            if (ItemMasterValidation() && UOMGridValidation() && ItemLocationGridValidation() && this.ValidateProductFeatures())
            {
                if (SaveItemDetails())  // Saving new item details
                {
                    this.RemoveImages();

                    lblItemMasterStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmItemMaster.Enabled = true;
                    ResetForm();
                }
            }
        }

        private bool ValidateProductFeatures()
        {
            bool componentsAdded = false;
            bool dimensionAdded = false;
            bool featureAdded = false;
            dgvDimensions.EndEdit();

            //if (this.cboItemType.SelectedValue.ToInt32() == (int)eItemType.Product)
            //{
            //    if (this.dgvProductionStages.Rows.Count == 0)
            //    {
            //        this.UserMessage.ShowMessage(1674, this.dgvProductionStages, this.errorItemMaster);
            //        tabItemMaster.SelectedTab = this.tabProductionStage;
            //        return false;
            //    }
            //}

            if (this.dgvDimensions.Rows.Count > 1)
            {
                List<int> DimensionIDs = new List<int>();
                this.dgvDimensions.ClearSelection();

                foreach (DataGridViewRow row in this.dgvDimensions.Rows)
                {
                    if (row.Index <= this.dgvDimensions.Rows.Count - 1)
                    {
                        if (row.Cells[0].Value.ToInt32() > 0)
                        {
                            if (row.Cells[1].Value.ToDecimal() > 0)
                            {
                                if (row.Cells[2].Value.ToInt32() > 0)
                                {
                                    if (DimensionIDs.Contains(row.Cells[0].Value.ToInt32()))
                                    {
                                        MessageBox.Show("Duplicate dimension exists!!", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        this.tabItemMaster.SelectedTab = this.tabDimensionAndFeatures;
                                        row.Cells[0].Selected = true;
                                        return false;
                                    }
                                    else
                                        DimensionIDs.Add(row.Cells[0].Value.ToInt32());

                                    dimensionAdded = true;
                                }
                                else
                                {
                                    this.UserMessage.ShowMessage(1663, this.dgvDimensions, this.errorItemMaster);
                                    return false;
                                }
                            }
                            else
                            {
                                this.UserMessage.ShowMessage(1661);
                                row.Cells[1].Selected = true;
                                this.tabItemMaster.SelectedTab = this.tabDimensionAndFeatures;
                                return false;
                            }
                        }
                        else if (row.Cells[1].Value.ToInt32() > 0 || row.Cells[2].Value.ToInt32() > 0)
                        {
                            this.UserMessage.ShowMessage(1662, this.dgvDimensions, this.errorItemMaster);
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private void btnCategory_Click(object sender, EventArgs e)
        {
            try
            {
                // Calling Reference form for ItemCategory
                FrmCommonRef objCommon = new FrmCommonRef("Category", new int[] { 1, 0 }, "CategoryID, CategoryName As Category", "InvCategoryReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intCurrentSelectedValue = cboCategory.SelectedValue.ToInt32();
                LoadCombos(1);
                if (objCommon.NewID != 0)
                    cboCategory.SelectedValue = objCommon.NewID;
                else
                    cboCategory.SelectedValue = intCurrentSelectedValue;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnCategory_Click() " + Ex.Message.ToString());
            }
        }

        private void btnSubCategory_Click(object sender, EventArgs e)
        {
            try
            {
                if (ItemMasterCategoryValidation())
                {
                    // Calling Reference form for ItemType
                    frmSubCategory objSubCategory = new frmSubCategory();

                    if (this.cboCategory.SelectedIndex != -1)
                        objSubCategory.CategoryID = this.cboCategory.SelectedValue.ToInt32();

                    objSubCategory.ShowDialog();
                    objSubCategory.Dispose();
                    int intCurrentComboID = cboSubCategory.SelectedValue.ToInt32();
                    LoadCombos(2);
                    cboSubCategory.SelectedValue = intCurrentComboID;
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnItemType_Click() " + Ex.Message.ToString());
            }
        }

        private void btnBaseUom_Click(object sender, EventArgs e)
        {
            int intCurrentComboID = cboBaseUom.SelectedValue.ToInt32();
            MintUOM = cboBaseUom.SelectedValue.ToInt32();
            AddUOM();
            cboBaseUom.SelectedValue = intCurrentComboID;
        }

        private void btnCostingMethod_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Costing Method", new int[] { 1, 0 }, "CostingMethodID, CostingMethod As [Costing Method]", "InvCostingMethodReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intCurrentComboID = cboCostingMethod.SelectedValue.ToInt32();
                LoadCombos(4);
                if (objCommon.NewID != 0)
                    cboCostingMethod.SelectedValue = objCommon.NewID;
                else
                    cboCostingMethod.SelectedValue = intCurrentComboID;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnCostingMethod_Click() " + Ex.Message.ToString());
            }
        }

        private void btnManufacturer_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Manufacturer", new int[] { 1, 0 }, "ManufactureID, Manufacture", "InvManufactureReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intCurrentComboID = cboManufacturer.SelectedValue.ToInt32();
                LoadCombos(5);
                if (objCommon.NewID != 0)
                    cboManufacturer.SelectedValue = objCommon.NewID;
                else
                    cboManufacturer.SelectedValue = intCurrentComboID;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnManufacturer_Click() " + Ex.Message.ToString());
            }
        }

        private void btnMadeIn_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Country", new int[] { 1, 0 }, "CountryID, CountryName As Country", "CountryReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intCurrentComboID = cboMadeIn.SelectedValue.ToInt32();
                LoadCombos(6);
                if (objCommon.NewID != 0)
                    cboMadeIn.SelectedValue = objCommon.NewID;
                else
                    cboMadeIn.SelectedValue = intCurrentComboID;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnMadeIn_Click() " + Ex.Message.ToString());
            }
        }

        private void btnTaxCode_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Tax Code", new int[] { 1, 0 }, "TaxID, TaxName As [Tax Code]", "InvTaxReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                LoadCombos(7);
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnTaxCode_Click() " + Ex.Message.ToString());
            }
        }

        private void btnSizeUom_Click(object sender, EventArgs e)
        {
            //MintUOM = cboSizeUom.SelectedValue.ToInt32();
            //AddUOM();   // Add new UOM
            //if(MintUOM > 0)
            //    cboSizeUom.SelectedValue = MintUOM;
        }

        /// <summary>
        /// Add new UOM
        /// </summary>
        private void AddUOM()
        {
            try
            {
                if (cboBaseUom.SelectedValue != null)
                    MintBaseUomID = Convert.ToInt32(cboBaseUom.SelectedValue.ToString());
                //if (cboSizeUom.SelectedValue != null)
                //    MintSizeUomID = Convert.ToInt32(cboSizeUom.SelectedValue.ToString());
                //if (cboWeightUom.SelectedValue != null)
                // MintWeightUomID = Convert.ToInt32(cboWeightUom.SelectedValue.ToString());
                if (cboDefaultPurchaseUom.SelectedValue != null)
                    MintPurchaseUomID = Convert.ToInt32(cboDefaultPurchaseUom.SelectedValue.ToString());
                if (cboDefaultSalesUom.SelectedValue != null)
                    MintSaleUomID = Convert.ToInt32(cboDefaultSalesUom.SelectedValue.ToString());
                using (FrmUnitOfMeasurement objUnitOfMeasurement = new FrmUnitOfMeasurement())
                {
                    objUnitOfMeasurement.PintUOM = MintUOM;
                    objUnitOfMeasurement.ShowDialog();
                }
                LoadCombos(3);
                LoadCombos(8);
                LoadCombos(9);
                //LoadCombos(13);
                cboBaseUom.SelectedValue = MintBaseUomID.ToString();
                //cboSizeUom.SelectedValue = MintSizeUomID.ToString();
                //cboWeightUom.SelectedValue = MintWeightUomID.ToString();
                cboDefaultPurchaseUom.SelectedValue = MintPurchaseUomID.ToString();
                cboDefaultSalesUom.SelectedValue = MintSaleUomID.ToString();
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on form FrmItemMaster:FrmItemMaster " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnBaseUom_Click() " + Ex.Message.ToString());
            }
        }

        private void ChangeStatus(object sender, EventArgs e)
        {
            if (!IsFirstTime)
            {
                //bnSaveItem.Enabled = MblnAddUpdatePermission;
                bnCancel.Enabled = true;
            }
            else
            {
                // bnSaveItem.Enabled = false;
                bnCancel.Enabled = false;
            }
            if (MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID == 0)
            {
                bnSaveItem.Enabled = MblnAddPermission;
            }
            else
                bnSaveItem.Enabled = MblnUpdatePermission;

            errorItemMaster.Clear();
            MBlnChangeStatus = true;
        }

        private void dgv_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                ChangeStatus(null, null);
                if (((DataGridView)sender).Name == dgvItemLocations.Name)
                {
                    if (e.ColumnIndex == dgvColReorderPoint.Index || e.ColumnIndex == dgvColReorderQuantity.Index)
                    {
                        if (dgvItemLocations[dgvColWarehouse.Index, e.RowIndex].Value == null ||
                            Convert.ToInt32(dgvItemLocations[dgvColWarehouse.Index, e.RowIndex].Value) == 0)
                        {
                            MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1647, out MmsgMessageBoxIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                            e.Cancel = true;
                        }
                    }
                    if (e.RowIndex >= 0 && (e.ColumnIndex == dgvColLocation.Index || e.ColumnIndex == dgvColRow.Index || e.ColumnIndex == dgvColBlock.Index || e.ColumnIndex == dgvColLot.Index))
                    {
                        int intWareHouseID = dgvItemLocations.Rows[e.RowIndex].Cells["dgvColWarehouse"].Value.ToInt32();
                        int intLocationID = dgvItemLocations.Rows[e.RowIndex].Cells["dgvColLocation"].Tag.ToInt32();
                        int intRowID = dgvItemLocations.Rows[e.RowIndex].Cells["dgvColRow"].Tag.ToInt32();
                        int intBlockID = dgvItemLocations.Rows[e.RowIndex].Cells["dgvColBlock"].Tag.ToInt32();

                        FillLocationGridCombo(e.ColumnIndex, intWareHouseID, intLocationID, intRowID, intBlockID);
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on form dgv_CellBeginEdit : " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on dgv_CellBeginEdit() " + Ex.Message.ToString());
            }
        }
        private bool FillLocationGridCombo(int inColumnIndex, int intWareHouseID, int intLocationID, int intRowID, int intBlockID)
        {
            try
            {
                DataTable dtLocation = new DataTable();

                if (inColumnIndex == dgvColLocation.Index)
                {
                    dtLocation = MobjClsBLLItemMaster.FillCombos(new string[] { "Distinct LocationID,Location", "InvWarehouseDetails", "WarehouseLocationTypeID=" + Convert.ToInt32(WarehouseLocationTypeReference.ActiveLocations) + " and WarehouseID=" + intWareHouseID.ToString() });

                    dgvColLocation.DataSource = null;
                    dgvColLocation.ValueMember = "LocationID";
                    dgvColLocation.DisplayMember = "Location";
                    dgvColLocation.DataSource = dtLocation;

                }
                else if (inColumnIndex == dgvColRow.Index)
                {
                    dtLocation = MobjClsBLLItemMaster.FillCombos(new string[] { "Distinct  RowID, RowNumber", "InvWarehouseDetails", "WarehouseLocationTypeID=" + Convert.ToInt32(WarehouseLocationTypeReference.ActiveLocations) + " and WarehouseID=" + intWareHouseID.ToString() + " And LocationID=" + intLocationID.ToString() });
                    dgvColRow.DataSource = null;
                    dgvColRow.ValueMember = "RowID";
                    dgvColRow.DisplayMember = "RowNumber";
                    dgvColRow.DataSource = dtLocation;
                }
                else if (inColumnIndex == dgvColBlock.Index)
                {
                    dtLocation = MobjClsBLLItemMaster.FillCombos(new string[] { "Distinct BlockID,BlockNumber", "InvWarehouseDetails", "WarehouseLocationTypeID=" + Convert.ToInt32(WarehouseLocationTypeReference.ActiveLocations) + " and WarehouseID=" + intWareHouseID.ToString() + " And LocationID=" + intLocationID.ToString() + " And RowID=" + intRowID.ToString() });
                    dgvColBlock.DataSource = null;
                    dgvColBlock.ValueMember = "BlockID";
                    dgvColBlock.DisplayMember = "BlockNumber";
                    dgvColBlock.DataSource = dtLocation;
                }
                else if (inColumnIndex == dgvColLot.Index)
                {
                    dtLocation = MobjClsBLLItemMaster.FillCombos(new string[] { " Distinct LotID, LotNumber", "InvWarehouseDetails", "WarehouseLocationTypeID=" + Convert.ToInt32(WarehouseLocationTypeReference.ActiveLocations) + " and WarehouseID=" + intWareHouseID.ToString() + " And LocationID=" + intLocationID.ToString() + " And RowID=" + intRowID.ToString() + " And BlockID=" + intBlockID.ToString() });
                    dgvColLot.DataSource = null;
                    dgvColLot.ValueMember = "LotID";
                    dgvColLot.DisplayMember = "LotNumber";
                    dgvColLot.DataSource = dtLocation;
                }

                return (dtLocation.Rows.Count > 0);

            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on FillLocationGridCombo(): " + this.Name + " " + Ex.Message.ToString(), 2);
                return false;

            }
        }
        /// <summary>
        /// Display item details and set binding navigator buttons
        /// </summary>
        private void DisplayItem()
        {
            try
            {
                this.loading = true;
                DisplayItemInfo();  // Displaying Item information
                lblItemMasterStatus.Text = "Information of " + txtDescription.Text;
                tmItemMaster.Enabled = true;
            }
            finally
            {
                this.loading = false;
            }
        }

        /// <summary>
        /// Displaying item details of the selected row number
        /// </summary>
        private void DisplayItemInfo()
        {
            if (MobjClsBLLItemMaster.DisplayItemInfo()) // Calling business method for getting item details
            {
                // Displaying item information

                this.ImageIndex = 0;

                this.pbItemPhoto.Image = null;
                this.pbItemPhoto.Tag = null;

                txtItemCode.Tag = MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID;
                txtItemCode.Text = MobjClsBLLItemMaster.objClsDTOItemMaster.strItemCode;
                txtShortName.Text = MobjClsBLLItemMaster.objClsDTOItemMaster.strShortName;
                txtDescription.Text = MobjClsBLLItemMaster.objClsDTOItemMaster.strDescription;
                txtDescriptionArabic.Text = MobjClsBLLItemMaster.objClsDTOItemMaster.strDescriptionArabic;
                cboCategory.SelectedValue = MobjClsBLLItemMaster.objClsDTOItemMaster.intCategoryID;
                cboSubCategory.SelectedValue = MobjClsBLLItemMaster.objClsDTOItemMaster.intSubCategoryID;
                cboBaseUom.SelectedValue = MobjClsBLLItemMaster.objClsDTOItemMaster.intBaseUomID;
                cboStatus.SelectedValue = MobjClsBLLItemMaster.objClsDTOItemMaster.intItemStatusID;
                chkExpiryDateMandatory.Checked = MobjClsBLLItemMaster.objClsDTOItemMaster.blnExpiryDateMandatory;
                cboManufacturer.SelectedValue = MobjClsBLLItemMaster.objClsDTOItemMaster.intManufacturerID;
                txtReorderPoint.Text = MobjClsBLLItemMaster.objClsDTOItemMaster.decReorderLevel.ToStringCustom();
                txtReorderQuantity.Text = MobjClsBLLItemMaster.objClsDTOItemMaster.decReorderQuantity.ToStringCustom();
                txtCutOffPrice.Text = MobjClsBLLItemMaster.objClsDTOItemMaster.decCutOffPrice.ToStringCustom();

                txtGuaranteePeriod.Text = MobjClsBLLItemMaster.objClsDTOItemMaster.strGuaranteePeriod;
                cboManufacturer.SelectedValue = MobjClsBLLItemMaster.objClsDTOItemMaster.intManufacturerID;
                txtModel.Text = MobjClsBLLItemMaster.objClsDTOItemMaster.strModel;
                txtNote.Text = MobjClsBLLItemMaster.objClsDTOItemMaster.strNotes;
                txtWarrantyPeriod.Text = MobjClsBLLItemMaster.objClsDTOItemMaster.strWarrantyPeriod;

                cboMadeIn.SelectedValue = MobjClsBLLItemMaster.objClsDTOItemMaster.intMadeInID;
                cboTaxCode.SelectedValue = MobjClsBLLItemMaster.objClsDTOItemMaster.intTaxID;
                rdbLandingCost.Checked = MobjClsBLLItemMaster.objClsDTOItemMaster.blnIsLandingCostEffected;
                // -- Getting Details in Costing Method Tab
                if (!rdbLandingCost.Checked)
                {
                    if (MobjClsBLLItemMaster.objClsDTOItemMaster.blnCalculationBasedOnRate == true)
                    {
                        rdbPurchaseRate.Checked = true;
                        rdbActualRate.Checked = false;
                    }
                    else
                    {
                        rdbPurchaseRate.Checked = false;
                        rdbActualRate.Checked = true;
                    }
                }
                DataTable datTemp = MobjClsBLLItemMaster.GetBatchDetailsFromCostingPricing(MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID, 
                    MobjClsBLLItemMaster.objClsDTOItemMaster.intCompanyID); // Get BatchDetails For Corresponding Costing Method and Pricing Scheme

                if (datTemp != null && datTemp.Rows.Count > 0)
                {
                    dgvBatchwiseCosting.DataSource = MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails = datTemp;
                    if (dgvBatchwiseCosting.Rows[0].Cells["ColPurchaseRate"].Value.ToDecimal() > 0)
                    {
                        txtPurchaseRate.Text =Convert.ToString(dgvBatchwiseCosting.Rows[0].Cells["ColPurchaseRate"].Value);
                    }
                }
                else
                    dgvBatchwiseCosting.DataSource = MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails = getNewTable();
                cboCostingMethod.SelectedValue = MobjClsBLLItemMaster.objClsDTOItemMaster.intCostingMethodID;

                cboProductType.SelectedValue = MobjClsBLLItemMaster.objClsDTOItemMaster.intProductTypeID;
                DataTable datTempRate = new DataTable();
                if (cboCostingMethod.SelectedValue.ToInt32() == (int)CostingMethodReference.Batch)
                {
                    cboPricingScheme.SelectedIndex = -1;
                    cboPricingScheme.SelectedValue = MobjClsBLLItemMaster.objClsDTOItemMaster.intPricingSchemeID;

                    dgvBatchwiseCosting.DataSource = MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails = getNewTable();
                    DataTable datTemp1 = MobjClsBLLItemMaster.GetBatchDetailsFromCostingPricing(MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID, 0);
                    dgvBatchwiseCosting.DataSource = MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails = datTemp1;
                }
                else
                {   
                    datTempRate = MobjClsBLLItemMaster.FillCombos(new string[] { "*", "InvCostingAndPricingDetails", "ItemID=" + 
                        "" + MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID });
                    if (datTempRate.Rows.Count == 1)
                        txtSaleRate.Text = datTempRate.Rows[0]["SaleRate"].ToString();

                    if (datTempRate.Rows.Count > 1)
                    {
                        datTempRate = MobjClsBLLItemMaster.FillCombos(new string[] { "*", "InvCostingAndPricingDetails", "ItemID=" + 
                            "" + MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID + " AND ISNULL(BatchID,0) <> 0" });                        
                    }
                    cboPricingScheme.SelectedIndex = -1;
                    cboPricingScheme.SelectedValue = MobjClsBLLItemMaster.objClsDTOItemMaster.intPricingSchemeID;
                }

                if (datTemp != null && datTemp.Rows.Count > 0)
                    txtSaleRate.Text = datTemp.Rows[0]["ColSaleRate"].ToString();
                else if (datTempRate != null && datTempRate.Rows.Count > 0)
                    txtSaleRate.Text = datTempRate.Rows[datTempRate.Rows.Count - 1]["SaleRate"].ToString();

                dblSaleRate = txtSaleRate.Text.ToDouble();
                // END -- Getting Details in Costing Method Tab

                pbItemPhoto.Tag = MobjClsBLLItemMaster.objClsDTOItemMaster.strImageFile;
                if (MobjClsBLLItemMaster.objClsDTOItemMaster.imgBarcodeImage != null)
                    pbBarCode.Image = new ClsImages().GetImage(MobjClsBLLItemMaster.objClsDTOItemMaster.imgBarcodeImage);
                else
                    pbBarCode.Image = null;

                if (MobjClsBLLItemMaster.objClsDTOItemMaster.strImageFile.ToStringCustom() != string.Empty)
                {
                    if (File.Exists(ClsCommonSettings.strServerPath + @"\Product Images\Low Resolution\" + MobjClsBLLItemMaster.objClsDTOItemMaster.strImageFile))
                        pbItemPhoto.Load(ClsCommonSettings.strServerPath + @"\Product Images\Low Resolution\" + MobjClsBLLItemMaster.objClsDTOItemMaster.strImageFile);
                    else
                        pbItemPhoto.Image = null;
                }
                else
                    pbItemPhoto.Image = null;

                DisplayItemReorder();   // Displaying item storage locations
                cboBaseUom_SelectedIndexChanged(null, null);
                FillDefaultUoms();
                DisplayItemUOMs();  // Displaying item UOMs
                DisplaySaleRateHistory();

                cboDefaultPurchaseUom.SelectedValue = MobjClsBLLItemMaster.objClsDTOItemMaster.intDefaultPurchaseUomID;
                cboDefaultSalesUom.SelectedValue = MobjClsBLLItemMaster.objClsDTOItemMaster.intDefaultSalesUomID;

                this.BindFeatureGrid(this.MobjClsBLLItemMaster.objClsDTOItemMaster.Features);
                
                this.BindDimensions();

                if (this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images.Count > 0)
                {
                    this.ImageIndex = 1;

                    clsDTOItemImages image = this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images[this.ImageIndex - 1];
                    if (image != null)
                    {
                        if (File.Exists(ClsCommonSettings.strServerPath + @"\Product Images\High Resolution\" + image.FileName))
                        {
                            FileStream f = new FileStream(ClsCommonSettings.strServerPath + @"\Product Images\High Resolution\" + image.FileName, FileMode.Open, FileAccess.Read);
                            pbItemPhoto.Image = Image.FromStream(f);
                            pbItemPhoto.SizeMode = PictureBoxSizeMode.StretchImage;
                            f.Close();

                            pbItemPhoto.Tag = image.FileName;
                        }
                    }
                }

                this.EnableDisableButtons();

                DataSet dtsBatch = MobjClsBLLItemMaster.GetProductBatchDetails();
                webBatchDetails.DocumentText = new ClsWebform().GetWebFormData("Batch Details", EmailFormID.ItemBatch, dtsBatch);

                // Enable buttons
                bnAddNewItem.Enabled = MblnAddPermission;
                bnSaveItem.Enabled = MblnUpdatePermission;
                bnDeleteItem.Enabled = MblnDeletePermission;
                bnEmail.Enabled = MblnPrintEmailPermission;
                bnPrint.Enabled = MblnPrintEmailPermission;
                btnAddBarcodeImage.Enabled = true;
                btnBarcodeCreate.Enabled = true;
                cboBaseUom.Enabled = false;
                btnBaseUom.Enabled = false;
                if (MobjClsBLLItemMaster.CheckItemExists())
                {
                    
                    //cboCompany.Enabled = false;
                    foreach (DataGridViewRow dr in dgvUnitsOfMeasurements.Rows)
                    {
                        if (dr.Cells[0].Value != null)
                        {
                            if (MobjClsBLLItemMaster.CheckItemAndUOMExists(MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID, dr.Cells[1].Value.ToInt32(), 0))
                                dr.ReadOnly = true;
                        }
                    }
                }
                else
                {
                    //if (cboItemType.SelectedValue.ToInt32() == (int)eItemType.RawMaterial)
                  //  {
                        cboBaseUom.Enabled = true;
                        btnBaseUom.Enabled = true;
                    //}
                    //cboCompany.Enabled = true;
                    foreach (DataGridViewRow dr in dgvUnitsOfMeasurements.Rows)
                    {
                        dr.ReadOnly = false;
                    }
                }
            }
        }

        private void DisplaySaleRateHistory()
        {
            DataTable dtSaleRateHistory = MobjClsBLLItemMaster.ItemSaleHistory(MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID);
            if (dtSaleRateHistory.Rows.Count > 0)
            {
                dgvRateHistory.DataSource = dtSaleRateHistory;
                dgvRateHistory.Columns[0].Width = 115;
                dgvRateHistory.Columns[1].Width = 142;
                dgvRateHistory.Visible = true;
                lblNoSaleRecordsFound.Visible = false;
                gbSaleRateHistory.Visible = true;
            }
            else
            {
                lblNoSaleRecordsFound.Visible = true;
                dgvRateHistory.Visible = false;
                gbSaleRateHistory.Visible = false;
            }
        }

        private void pbItemPhoto_MouseHover(object sender, EventArgs e)
        {
            //this.Cursor = Cursors.Hand;
        }

        private void pbItemPhoto_MouseLeave(object sender, EventArgs e)
        {
            // this.Cursor = Cursors.Default;
        }

        private void pbItemPhoto_MouseUp(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Right)
            //    mnuAddImage.Show(pbItemPhoto, new Point(e.X, e.Y));
        }

        private void mnuAdd_Click(object sender, EventArgs e)
        {
            BtnAttach.Text = "Attach";
            try
            {
                AddImage();

                //this.MBlnChangeStatus = true;
                this.ChangeStatus(sender, new EventArgs());
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on form pbItemPhoto_Click : " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on pbItemPhoto_Click() " + Ex.Message.ToString());
            }
        }

        private void mnuRemove_Click(object sender, EventArgs e)
        {
            BtnAttach.Text = "Remove";
            pbItemPhoto.Image = null;
            pbItemPhoto.Tag = "";

            try
            {
                if (this.ImageIndex > 0)
                {
                    if (this.ImageIndex <= this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images.Count)
                    {
                        clsDTOItemImages image = this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images[this.ImageIndex - 1];

                        if (image != null)
                        {
                            this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images.RemoveAt(this.ImageIndex - 1);

                            this.ImageIndex = this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images.Count;
                            this.ShowImage();
                            this.EnableDisableButtons();

                            this.RemovedImages.Add(image);

                            //string strHighResolutionPath = ClsCommonSettings.strServerPath + @"\Product Images\High Resolution\" + image.FileName;
                            //string strLowResolutionPath = ClsCommonSettings.strServerPath + @"\Product Images\Low Resolution\" + image.FileName;

                            //if (File.Exists(strHighResolutionPath))
                            //    File.Delete(strHighResolutionPath);

                            //if (File.Exists(strLowResolutionPath))
                            //    File.Delete(strLowResolutionPath);

                            this.ChangeStatus(sender, new EventArgs());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void RemoveImages()
        {
            try
            {
                foreach (var image in this.RemovedImages)
                {
                    if (image.FileName != null && image.FileName.Trim().Length > 0)
                    {
                        string strHighResolutionPath = ClsCommonSettings.strServerPath + @"\Product Images\High Resolution\" + image.FileName;
                        string strLowResolutionPath = ClsCommonSettings.strServerPath + @"\Product Images\Low Resolution\" + image.FileName;

                        if (File.Exists(strHighResolutionPath))
                            File.Delete(strHighResolutionPath);

                        if (File.Exists(strLowResolutionPath))
                            File.Delete(strLowResolutionPath);
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
            finally
            {
                this.RemovedImages.Clear();
            }
        }

        private void pbItemPhoto_Click(object sender, EventArgs e)
        {

        }

        private void AddImage()
        {
            using (OpenFileDialog MofdItemPhoto = new OpenFileDialog())
            {
                MofdItemPhoto.Filter = "Images (*.jpg;*.jpeg;*.tif;*.png;*.bmp)|*.jpg;*.tif;*.jpeg;*.png;*.bmp";

                if (MofdItemPhoto.ShowDialog() != DialogResult.Cancel)
                {
                    if (MofdItemPhoto.FileName.Trim().Length > 0)
                    {
                        FileInfo fi = new FileInfo(MofdItemPhoto.FileName);
                        if (fi.Extension == ".jpg" || fi.Extension == ".jpeg" || fi.Extension == ".tif" || fi.Extension == ".png" || fi.Extension == ".bmp")
                        {
                            if (pbItemPhoto.Tag == null)
                                pbItemPhoto.Tag = MofdItemPhoto.FileName.Trim();

                            string strHighResolutionPath = ClsCommonSettings.strServerPath + @"\Product Images\High Resolution\" + pbItemPhoto.Tag.ToString();
                            string strLowResolutionPath = ClsCommonSettings.strServerPath + @"\Product Images\Low Resolution\" + pbItemPhoto.Tag.ToString();

                            string strFilePath = ClsCommonSettings.strServerPath + @"\Product Images\High Resolution\" + pbItemPhoto.Tag.ToString();

                            if (pbItemPhoto.Image != null)
                                pbItemPhoto.Image.Dispose();

                            string[] sarFileName = MofdItemPhoto.SafeFileName.Split(Convert.ToChar("."));

                            DateTime dt = ClsCommonSettings.GetServerDateTime();

                            string strImageFileName = string.Format("{0}_{1}.{2}",
                                     (sarFileName[0].Length > 30 ? sarFileName[0].Substring(0, 30) : sarFileName[0])
                                    , dt.ToString("ddMMyy_hhmmss")
                                    , sarFileName[1]
                              );

                            if (!Directory.Exists(ClsCommonSettings.strServerPath + @"\Product Images\High Resolution"))
                                Directory.CreateDirectory(ClsCommonSettings.strServerPath + @"\Product Images\High Resolution");

                            if (!Directory.Exists(ClsCommonSettings.strServerPath + @"\Product Images\Low Resolution"))
                                Directory.CreateDirectory(ClsCommonSettings.strServerPath + @"\Product Images\Low Resolution");

                            ResizeImage(MofdItemPhoto.FileName, strImageFileName);

                            this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images.Add(new clsDTOItemImages
                            {
                                FileName = strImageFileName,
                                ViewName = string.Empty,
                                IsDefault = false
                            });
                            this.ImageIndex = this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images.Count;
                            this.EnableDisableButtons();

                            FileStream f = new FileStream(ClsCommonSettings.strServerPath + @"\Product Images\High Resolution\" + strImageFileName, FileMode.Open, FileAccess.Read);
                            pbItemPhoto.Image = Image.FromStream(f);
                            pbItemPhoto.SizeMode = PictureBoxSizeMode.StretchImage;
                            f.Close();

                            pbItemPhoto.Tag = strImageFileName;
                            ChangeStatus(pbItemPhoto, new EventArgs());
                        }
                        else
                        {
                            MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1649, out MmsgMessageBoxIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageBoxIcon);
                            lblItemMasterStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            tmItemMaster.Enabled = true;
                        }
                    }
                }

            }
        }
        //if (!Directory.Exists(ClsCommonSettings.strPicturesPath + @"\EmployeeImages"))
        //               Directory.CreateDirectory(ClsCommonSettings.strPicturesPath + @"\EmployeeImages");
        //           File.Copy(Dlg.FileName, ClsCommonSettings.strPicturesPath + @"\EmployeeImages\" + strImageFileName, true);

        public bool ResizeImage(string strSource, string strTarget)
        {
            string strLowResolutionPath = ClsCommonSettings.strServerPath + @"\Product Images\Low Resolution\" + strTarget;
            string strHighResolutionPath = ClsCommonSettings.strServerPath + @"\Product Images\High Resolution\" + strTarget;

            using (Image src = Image.FromFile(strSource, true))
            {
                if (src != null)    // Check that we have an image
                {
                    // save high resolution image
                    src.Save(strHighResolutionPath);

                    int origX, origY, newX, newY;
                    int trimX = 0, trimY = 0;
                    int MaxX = 200, MaxY = 150;

                    // Default to size of source image
                    newX = origX = src.Width;
                    newY = origY = src.Height;

                    // Does image exceed maximum dimensions?
                    if (origX > MaxX || origY > MaxY)
                    {
                        // Trim to exactly fit maximum dimensions
                        double factor = Math.Max((double)MaxX / (double)origX,
                            (double)MaxY / (double)origY);

                        newX = (int)Math.Ceiling((double)origX * factor);
                        newY = (int)Math.Ceiling((double)origY * factor);

                        trimX = newX - MaxX;
                        trimY = newY - MaxY;
                    }

                    // Create destination image
                    using (Image dest = new Bitmap(newX - trimX, newY - trimY))
                    {
                        Graphics graph = Graphics.FromImage(dest);
                        graph.InterpolationMode =
                            System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        graph.DrawImage(src, -(trimX / 2), -(trimY / 2), newX, newY);

                        dest.Save(strLowResolutionPath);
                    }
                }

                return true; // Indicate success
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                FillItemsGrid(); // Searching an item according to the category/type/item code/description
                if (dgvItems.CurrentRow != null)
                    DisplayItem();
                //else
                //    ResetForm();
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on form btnSearch_Click : " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnSearch_Click() " + Ex.Message.ToString());
            }
        }

        private void bnCancel_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void bnDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MobjClsBLLItemMaster.CheckItemExists())
                {
                    MessageBox.Show(MobjClsNotification.GetErrorMessage(MdatMessages, 1620, out MmsgMessageBoxIcon).Replace("#", "").Trim(),
                        ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageBoxIcon);
                    return;
                }
                //else if (MobjClsBLLItemMaster.FillCombos(new string[] { "ItemID", "STOpeningStockDetails", "ItemID = " + MobjClsBLLItemMaster.objClsDTOItemMaster.lngItemID }).Rows.Count > 0)
                //{
                //    MessageBox.Show(MobjClsNotification.GetErrorMessage(MdatMessages, 1620, out MmsgMessageBoxIcon).Replace("#", "").Trim(),
                //       ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageBoxIcon);
                //    return;
                //}

                if (MessageBox.Show(MobjClsNotification.GetErrorMessage(MdatMessages, 1633, out MmsgMessageBoxIcon).Replace("#", "").Trim(),
                    ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MmsgMessageBoxIcon) ==
                    DialogResult.Yes)
                {
                    if (MobjClsBLLItemMaster.DeleteItemInfo())
                    {
                        if (dgvItems.CurrentRow != null)
                            dgvItems.Rows.RemoveAt(dgvItems.CurrentRow.Index);

                        lblItemMasterStatus.Text = MobjClsNotification.GetErrorMessage(MdatMessages, 1634, out MmsgMessageBoxIcon);
                        bnAddNewItem_Click(null, null);
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on form bnDeleteItem_Click : " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on bnDeleteItem_Click() " + Ex.Message.ToString());
            }
        }

        private void btnCreateBarCode_Click(object sender, EventArgs e)
        {
            using (FrmBarCode objBarCode = new FrmBarCode())
            {
                objBarCode.PlngItemID = MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID;
                objBarCode.PstrBarcodeValue = txtItemCode.Text.Trim();
                objBarCode.ShowDialog();
                pbBarCode.Image = objBarCode.PimgBarcodeImage;
                MobjClsBLLItemMaster.objClsDTOItemMaster.strBarcodeValue = objBarCode.PstrBarcodeValue;
                objBarCode.Dispose();
            }
        }

        private void bnUom_Click(object sender, EventArgs e)
        {
            AddUOM();
        }

        private void dgvItems_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvItems.CurrentRow != null)
                {
                    //MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID = Convert.ToInt32(dgvItems.CurrentRow.Cells[dgvColItemID.Index].Value);
                    MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID = Convert.ToInt32(dgvItems.CurrentRow.Cells[1].Value);
                    DisplayItem();
                }
                MBlnChangeStatus = false;
                this.bnSaveItem.Enabled = false;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on form dgvItems_CurrentCellChanged : " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on dgvItems_CurrentCellChanged() " + Ex.Message.ToString());
            }
        }

        private void TimerTick(object sender, EventArgs e)
        {
            lblItemMasterStatus.Text = "";
            tmItemMaster.Enabled = false;
        }

        private void bnEmail_Click(object sender, EventArgs e)
        {
            using (FrmEmailPopup objEmailPopUp = new FrmEmailPopup())
            {
                objEmailPopUp.MsSubject = "Product Information";
                objEmailPopUp.EmailFormType = EmailFormID.Product;
                objEmailPopUp.EmailSource = MobjClsBLLItemMaster.GetProductReport();
                objEmailPopUp.ShowDialog();
            }
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {
            int MintItemID;
            MintItemID = Convert.ToInt32(MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID);
            if (MintItemID > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = Convert.ToInt32(MintItemID);
                ObjViewer.PiFormID = (int)FormID.ItemMaster;
                ObjViewer.ShowDialog();
            }

        }

        private void frmItemMaster1_Shown(object sender, EventArgs e)
        {
            txtItemCode.Focus();
        }

        private void mnuCreateBarcode_Click(object sender, EventArgs e)
        {
            btnBarcodeCreate.Text = "Create";
            using (FrmBarCode objBarCode = new FrmBarCode())
            {
                objBarCode.PlngItemID = MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID;
                objBarCode.PstrBarcodeValue = txtItemCode.Text.Trim();
                objBarCode.PblnIsTemplate = true;
                objBarCode.ShowDialog();
                if (objBarCode.PimgBarcodeImage != null)//if cancel
                {
                    pbBarCode.Image = objBarCode.PimgBarcodeImage;
                    ChangeStatus(null, null);
                }
                MobjClsBLLItemMaster.objClsDTOItemMaster.strBarcodeValue = objBarCode.PstrBarcodeValue;
                MobjClsBLLItemMaster.objClsDTOItemMaster.imgBarcodeImage = new ClsImages().GetImageDataStream(objBarCode.PimgBarcodeImage,
                        System.Drawing.Imaging.ImageFormat.Jpeg);

                objBarCode.Dispose();
            }
        }

        private void mnuRemoveBarcode_Click(object sender, EventArgs e)
        {
            btnBarcodeCreate.Text = "Remove";
            pbBarCode.Image = null;
            ChangeStatus(null, null);
        }

        private void btnAddBarcodeImage_Click(object sender, EventArgs e)
        {
            mnuBarcodeAddImage.Show(btnAddBarcodeImage, btnAddBarcodeImage.PointToClient(System.Windows.Forms.Cursor.Position));
        }

        private void btnAddImage_Click(object sender, EventArgs e)
        {
            mnuAddImage.Show(btnAddImage, btnAddImage.PointToClient(System.Windows.Forms.Cursor.Position));
        }

        private void BtnAttach_Click(object sender, EventArgs e)
        {
            try
            {
                if (BtnAttach.Text == "Attach")
                {
                    mnuAdd_Click(null, null);
                }
                else
                {
                    mnuRemove_Click(null, null);
                }
            }
            catch
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1649, out MmsgMessageBoxIcon);
                errorItemMaster.SetError(BtnAttach, MstrMessageCommon.Replace("#", "").Trim());
                BtnAttach.Focus();
            }
        }

        private void btnBarcodeCreate_Click(object sender, EventArgs e)
        {
            if (btnBarcodeCreate.Text == "Create")
            {
                mnuCreateBarcode_Click(null, null);
            }
            else if (btnBarcodeCreate.Text == "Print")
            {
                mnuPrintBarcode_Click(null, null);
            }
            else
            {
                mnuRemoveBarcode_Click(null, null);
            }
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                //objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)ModuleID.Inventory, (Int32)MenuID.Products, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (Int32)eModuleID.Inventory, (Int32)eMenuID.Products, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                DataTable DtPermission = objClsBLLPermissionSettings.GetMenuPermissions(ClsCommonSettings.RoleID);
                if (DtPermission.Rows.Count == 0)
                {
                    bnUom.Enabled = btnUomConversion.Enabled = btnPricingScheme.Enabled  = false;
                }
                else
                {
                    DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.UOMConversions).ToString();
                    btnUomConversion.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);

                    DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PricingScheme).ToString();
                    btnPricingScheme.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);

                    DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.UOM).ToString();
                    bnUom.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);

                }
 
                if (MblnAddPermission == true || MblnUpdatePermission == true)
                    MblnAddUpdatePermission = true;

                //if (MblnAddPermission == true && MblnUpdatePermission == true && MblnDeletePermission == true)
                //    MblnPrintAndEmailPermission = true;
            }
            else
                MblnAddPermission = MblnAddUpdatePermission = MblnCancelPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            //bnAddNewItem.Enabled = MblnAddPermission;
            //bnSaveItem.Enabled = MblnAddUpdatePermission;
            //bnDeleteItem.Enabled = MblnDeletePermission;
            ////btnSave.Enabled = MblnAddUpdatePermission;
            ////btnOk.Enabled = MblnAddUpdatePermission;
            //bnPrint.Enabled = MblnPrintEmailPermission;
            //bnEmail.Enabled = MblnPrintEmailPermission;
        }

        private void SetControlPermissions()
        {
            clsBLLPermissionSettings objBllPermissionSettings = new clsBLLPermissionSettings();
            DataTable dtControlsPermissions = new DataTable();
            dtControlsPermissions = objBllPermissionSettings.GetControlPermissions1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)ControlOperationType.CostingTab);

            MblnCostingTabPermission = false;
            if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                MblnCostingTabPermission = true;
            else
            {
                //dtControlsPermissions.DefaultView.RowFilter = "ControlName = 'CostingMethodTab'";
                if (dtControlsPermissions.Rows.Count > 0)
                    MblnCostingTabPermission = Convert.ToBoolean(dtControlsPermissions.Rows[0]["IsVisible"]);
            }
            tbpCosting.Visible = true;

        }
        private void SubcategoryChangeStatus(object sender, EventArgs e)
        {
            ChangeStatus(sender, e);
        }

        private void cboCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCategory.SelectedIndex != -1)
                LoadCombos(2);
            ChangeStatus(sender, e);
        }

        private void cboSearchCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboSearchCategory.SelectedIndex != -1)
                LoadCombos(10);
        }

        private void dgvItemLocations_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                dgvItemLocations.CommitEdit(DataGridViewDataErrorContexts.Commit);

                if (e.RowIndex >= 0 && e.ColumnIndex == dgvColLocation.Index)
                {
                    dgvItemLocations.CurrentRow.Cells["dgvColLocation"].Tag = dgvItemLocations.CurrentRow.Cells["dgvColLocation"].Value;
                    dgvItemLocations.CurrentRow.Cells["dgvColLocation"].Value = dgvItemLocations.CurrentRow.Cells["dgvColLocation"].FormattedValue;
                    dgvColRow.DataSource = null;
                    dgvColBlock.DataSource = null;
                    dgvColLot.DataSource = null;
                    dgvItemLocations.CurrentRow.Cells["dgvColRow"].Tag = 0;
                    dgvItemLocations.CurrentRow.Cells["dgvColRow"].Value = "";
                    dgvItemLocations.CurrentRow.Cells["dgvColBlock"].Tag = 0;
                    dgvItemLocations.CurrentRow.Cells["dgvColBlock"].Value = "";
                    dgvItemLocations.CurrentRow.Cells["dgvColLot"].Tag = 0;
                    dgvItemLocations.CurrentRow.Cells["dgvColLot"].Value = "";
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex == dgvColRow.Index)
                {
                    dgvItemLocations.CurrentRow.Cells["dgvColRow"].Tag = dgvItemLocations.CurrentRow.Cells["dgvColRow"].Value;
                    dgvItemLocations.CurrentRow.Cells["dgvColRow"].Value = dgvItemLocations.CurrentRow.Cells["dgvColRow"].FormattedValue;

                    dgvColBlock.DataSource = null;
                    dgvColLot.DataSource = null;
                    dgvItemLocations.CurrentRow.Cells["dgvColBlock"].Tag = 0;
                    dgvItemLocations.CurrentRow.Cells["dgvColBlock"].Value = "";
                    dgvItemLocations.CurrentRow.Cells["dgvColLot"].Tag = 0;
                    dgvItemLocations.CurrentRow.Cells["dgvColLot"].Value = "";
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex == dgvColBlock.Index)
                {
                    dgvItemLocations.CurrentRow.Cells["dgvColBlock"].Tag = dgvItemLocations.CurrentRow.Cells["dgvColBlock"].Value;
                    dgvItemLocations.CurrentRow.Cells["dgvColBlock"].Value = dgvItemLocations.CurrentRow.Cells["dgvColBlock"].FormattedValue;
                    dgvColLot.DataSource = null;
                    dgvItemLocations.CurrentRow.Cells["dgvColLot"].Tag = 0;
                    dgvItemLocations.CurrentRow.Cells["dgvColLot"].Value = "";
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex == dgvColLot.Index)
                {
                    dgvItemLocations.CurrentRow.Cells["dgvColLot"].Tag = dgvItemLocations.CurrentRow.Cells["dgvColLot"].Value;
                    dgvItemLocations.CurrentRow.Cells["dgvColLot"].Value = dgvItemLocations.CurrentRow.Cells["dgvColLot"].FormattedValue;
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        private void dgvItemLocations_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex > -1)
            {
                if (dgvItemLocations.CurrentCell != null)
                {
                    dgvItemLocations.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    if (dgvItemLocations.EditingControl != null && dgvItemLocations.EditingControl.Text != "")
                    {
                        if (dgvItemLocations.Rows[e.RowIndex].Cells["dgvColWarehouse"].Value == null || Convert.ToString(dgvItemLocations.Rows[e.RowIndex].Cells["dgvColWarehouse"].Value) == "")
                        {

                            //MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1647, out MmsgMessageBoxIcon);
                            //                            dgvItemLocations.CurrentCell = dgvItemLocations[0, e.RowIndex];

                            //MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageBoxIcon);
                            //lblItemMasterStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            //e.Cancel = true;
                            //tmItemMaster.Enabled = true;
                        }
                    }
                }
            }
        }

        private void dgvItemLocations_DataError_1(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                //if (e.ColumnIndex == 0 && e.RowIndex > -1)
                //{
                //    if (dgvItemLocations.CurrentCell != null)
                //    {
                //        dgvItemLocations.Rows[e.RowIndex].Cells["dgvColWarehouse"].Value = "";
                //        return;
                //    }
                //}
                return;
            }
            catch (Exception)
            {
                return;
            }
        }

        private void dgvItemLocations_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvItemLocations.IsCurrentCellDirty)
                {
                    if (dgvItemLocations.CurrentCell != null)
                    {
                        dgvItemLocations.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    }
                }
            }
            catch (Exception)
            { }

        }

        private void dgvUnitsOfMeasurements_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvUnitsOfMeasurements.IsCurrentCellDirty)
                {
                    if (dgvUnitsOfMeasurements.CurrentCell != null)
                    {
                        dgvUnitsOfMeasurements.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    }
                }
            }
            catch (Exception)
            { }

        }

        public void RefreshForm()
        {
            btnSearch_Click(null, null);
        }

        private void dgvItemLocations_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            this.dgvItemLocations.EndEdit();
        }

        private void frmItemMaster1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        //this.Close();
                        break;
                    case Keys.Alt | Keys.S:
                        bnSaveItem_Click(sender, new EventArgs());//save
                        break;
                    case Keys.Alt | Keys.R:
                        bnDeleteItem_Click(sender, new EventArgs());//Delete
                        break;


                }
            }
            catch (Exception)
            {
            }
        }

        private void txtReorderPoint_Enter(object sender, EventArgs e)
        {
            System.Windows.Forms.Clipboard.Clear();
        }

        private void dgvUnitsOfMeasurements_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (e.Row.ReadOnly)
                e.Cancel = true;
        }

        private void Combo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                ((ComboBox)sender).DroppedDown = false;

            }
            catch (Exception)
            {
            }
        }

        private void btnUomConversion_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmUOMConversions objUOMConversions = new FrmUOMConversions())
                {
                    objUOMConversions.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on form FrmItemMaster:FrmItemMaster " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnBaseUom_Click() " + Ex.Message.ToString());
            }
        }

        private void FillDefaultUoms()
        {
            if (cboBaseUom.SelectedValue != null)
            {
                DataTable datSalesUoms = new DataTable();
                DataTable datPurchaseUoms = new DataTable();
                datSalesUoms.Columns.Add("UomID");
                datSalesUoms.Columns.Add("Uom");
                datPurchaseUoms.Columns.Add("UomID");
                datPurchaseUoms.Columns.Add("Uom");
                DataRow drSalesRow = datSalesUoms.NewRow();
                DataRow drPurchaseRow = datPurchaseUoms.NewRow();

                drSalesRow["UomID"] = cboBaseUom.SelectedValue;
                drSalesRow["Uom"] = cboBaseUom.Text;
                drPurchaseRow["UomID"] = cboBaseUom.SelectedValue;
                drPurchaseRow["Uom"] = cboBaseUom.Text;
                datSalesUoms.Rows.Add(drSalesRow);
                datPurchaseUoms.Rows.Add(drPurchaseRow);

                foreach (DataGridViewRow dr in dgvUnitsOfMeasurements.Rows)
                {
                    if (dr.Cells[0].Value.ToBoolean())
                    {
                        DataRow drSalesRow1 = datSalesUoms.NewRow();
                        drSalesRow1["UomID"] = Convert.ToInt32(dr.Cells[1].Tag);
                        drSalesRow1["Uom"] = Convert.ToString(dr.Cells[1].Value);
                        datSalesUoms.Rows.Add(drSalesRow1);

                        DataRow drPurchaseRow1 = datPurchaseUoms.NewRow();
                        drPurchaseRow1["UomID"] = Convert.ToInt32(dr.Cells[1].Tag);
                        drPurchaseRow1["Uom"] = Convert.ToString(dr.Cells[1].Value);
                        datPurchaseUoms.Rows.Add(drPurchaseRow1);
                    }
                }
                cboDefaultSalesUom.DisplayMember = "Uom";
                cboDefaultSalesUom.ValueMember = "UomID";
                cboDefaultSalesUom.DataSource = datSalesUoms;

                cboDefaultPurchaseUom.DisplayMember = "Uom";
                cboDefaultPurchaseUom.ValueMember = "UomID";
                cboDefaultPurchaseUom.DataSource = datPurchaseUoms;
            }
            else
            {
                cboDefaultPurchaseUom.DataSource = null;
                cboDefaultSalesUom.DataSource = null;
            }
        }

        private void dgvUnitsOfMeasurements_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            FillDefaultUoms();

            if (!this.CheckInvalidUOM())
                this.chkUOMSelectAll.Checked = false;

            this.ChangeStatus(sender, new EventArgs());
        }

        private void cboBaseUom_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboDefaultPurchaseUom.DataSource = null;
            cboDefaultSalesUom.DataSource = null;
            this.dgvUnitsOfMeasurements.Rows.Clear();

            if (cboBaseUom.SelectedValue != null)
            {
                int Index = 0;

                using (DataTable datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "UM.UOMID, UM.UOMName, 0 AS Checked", "InvUOMReference UM Inner join InvUOMConversions UC on UC.OtherUOMID = UM.UOMID", "UC.UOMID = " + Convert.ToInt32(cboBaseUom.SelectedValue) + "" }))
                {
                    foreach (DataRow row in datCombos.Rows)
                    {
                        this.dgvUnitsOfMeasurements.RowCount += 1;
                        this.dgvUnitsOfMeasurements.Rows[Index].Cells[0].Value = row["Checked"].ToBoolean();
                        this.dgvUnitsOfMeasurements.Rows[Index].Cells[1].Value = row["UOMName"].ToStringCustom();
                        this.dgvUnitsOfMeasurements.Rows[Index].Cells[1].Tag = row["UOMID"].ToInt32();
                        Index += 1;
                    }
                }

                FillDefaultUoms();
            }
            ChangeStatus(sender, e);
        }

        private void dgvUnitsOfMeasurements_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            FillDefaultUoms();
        }

        private void btnWeightUom_Click(object sender, EventArgs e)
        {
            //MintUOM = cboWeightUom.SelectedValue.ToInt32();
            //AddUOM();
            //if(MintUOM > 0)
            //    cboWeightUom.SelectedValue = MintUOM;
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.MobjClsBLLItemMaster.objClsDTOItemMaster.intCompanyID = this.cboCompany.SelectedValue.ToInt32();

            //dgvItemLocations.Rows.Clear();
            //LoadCombos(11);
            //ChangeStatus(sender, e);

            //if (MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID != 0)
            //{
            //    int intCompanyId = 0;
            //    if (cboCompany.SelectedIndex != -1)
            //    {
            //        intCompanyId = cboCompany.SelectedValue.ToInt32();
            //    }
            //    else
            //    {
            //        intCompanyId = 0;
            //    }
            //    if (MobjClsBLLItemMaster.objClsDTOItemMaster.intCompanyID != intCompanyId)
            //    {
            //        GenerateItemCode();
            //    }
            //    else
            //    {
            //        txtItemCode.Text = MobjClsBLLItemMaster.objClsDTOItemMaster.strItemCode;

            //    }
            //}
            //else
            //{
            //    GenerateItemCode();
            //}
            //LoadCombos(16);
        }
        private void GetPrefix()
        {
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            
           
            DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettings(ClsCommonSettings.CompanyID);
            strItemcodePrefix = ClsCommonSettings.strProductCodePrefix;

            DataTable datCount = MobjClsBLLItemMaster.FillCombos(new string[] { "Isnull(max(SNo),0)+1 as Count", "InvItemMaster", "Isnull(CompanyID,0)= " + ClsCommonSettings.CompanyID });
            if(datCount.Rows.Count>0)
            strItemcodePrefix= strItemcodePrefix + datCount.Rows[0][0].ToString();
        }
        private void cboCostingMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCostingMethod.SelectedValue != null)
            {
                if (cboCostingMethod.SelectedValue.ToInt32() == (int)CostingMethodReference.Batch)
                    GetSaleRateForBatch();
                else
                    GetSaleRateForOthers();
            }
            cboPricingScheme.SelectedIndex = -1;
            if (cboCostingMethod.SelectedValue != null)
            {
                if (cboCostingMethod.SelectedValue.ToInt32() == (int)CostingMethodReference.Batch)
                    GetBatchDetails();
            }
            this.ChangeStatus(sender, e);
        }

        private void GetSaleRateForBatch()
        {
            lblSaleRate.Visible = false;
            txtSaleRate.Visible = false;
            BtnShow.Visible = false;
        }

        private void GetSaleRateForOthers()
        {
            lblSaleRate.Visible = true;
            txtSaleRate.Visible = true;
            BtnShow.Visible = false;

            int intTemp = 0;
            if (cboCostingMethod.SelectedValue.ToInt32() == (int)CostingMethodReference.LIFO)
                intTemp = (int)CostingMethodReference.LIFO;
            else if (cboCostingMethod.SelectedValue.ToInt32() == (int)CostingMethodReference.FIFO)
                intTemp = (int)CostingMethodReference.FIFO;
            else if (cboCostingMethod.SelectedValue.ToInt32() == (int)CostingMethodReference.AVG)
                intTemp = (int)CostingMethodReference.AVG;
            else if (cboCostingMethod.SelectedValue.ToInt32() == (int)CostingMethodReference.MIN)
                intTemp = (int)CostingMethodReference.MIN;
            else if (cboCostingMethod.SelectedValue.ToInt32() == (int)CostingMethodReference.MAX)
                intTemp = (int)CostingMethodReference.MAX;
            txtSaleRate.Text = MobjClsBLLItemMaster.GetSaleRate(intTemp, MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID, ClsCommonSettings.CompanyID);
            dblSaleRate = txtSaleRate.Text.ToDouble();
        }

        private void cboPricingScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorItemMaster.Clear();
            if (cboCostingMethod.SelectedValue != null)
            {
                if (cboCostingMethod.SelectedValue.ToInt32() == (int)CostingMethodReference.Batch)
                {
                    GetSaleRateForBatch();
                    GetBatchDetails();
                }
                else
                {
                    GetSaleRateForOthers();
                    FillwithChangePricingScheme();
                }
            }
            this.ChangeStatus(sender, e);
        }

        private void FillwithChangePricingScheme()
        {
            double Rate;
            if (cboCostingMethod.SelectedValue != null && cboPricingScheme.SelectedValue != null)
            {
                if (cboCostingMethod.SelectedValue.ToInt32() != ((int)CostingMethodReference.NONE) || cboPricingScheme.SelectedValue.ToInt32() != 1)
                {
                    int intTempPID = cboPricingScheme.SelectedValue.ToInt32();
                    DataTable datTemp = MobjClsBLLItemMaster.FillCombos(new string[] { "*", "InvPricingSchemeReference", "PricingSchemeID=" + cboPricingScheme.SelectedValue.ToInt32() });

                    if (MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails != null)
                    {

                        if (MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows.Count > 0)
                        {
                            DataTable datTempBatch = new DataTable();
                            datTempBatch = MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Copy();
                            if (datTempBatch != null)
                            {
                                if (datTempBatch.Rows.Count > 0)
                                {
                                    if (rdbPurchaseRate.Checked)
                                        Rate = datTempBatch.Rows[0]["ColPurchaseRate"].ToDouble();
                                    else if (rdbActualRate.Checked)
                                        Rate = datTempBatch.Rows[0]["ColActualRate"].ToDouble();
                                    else
                                        Rate = datTempBatch.Rows[0]["ColLandingCost"].ToDouble();


                                }
                                else
                                    Rate = 0;
                                if (datTemp.Rows.Count > 0)
                                {
                                    if (datTemp.Rows[0]["PercentOrAmount"].ToString() == "1")
                                    {
                                        double dblExRate = getExchangeRate(datTemp.Rows[0]["CurrencyID"].ToInt32());
                                        txtSaleRate.Text = (Rate +
                                                (datTemp.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                                    }
                                    else
                                    {
                                        txtSaleRate.Text = (Rate +
                                                 (Rate * datTemp.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                                    }
                                }


                            }
                        }
                    }
                    //dblSaleRate = txtSaleRate.Text.ToDouble();
                    //if (intTempPID > 0)
                    //    cboPricingScheme.SelectedValue = intTempPID;

                    //DataTable datTemp = MobjClsBLLItemMaster.FillCombos(new string[] { "*", "InvPricingSchemeReference", "PricingSchemeID=" + cboPricingScheme.SelectedValue.ToInt32() });

                    //if (datTemp != null)
                    //{
                    //    if (datTemp.Rows.Count > 0)
                    //    {
                    //        if (datTemp.Rows[0]["PercentOrAmount"].ToString() == "1")
                    //        {
                    //            double dblExRate = getExchangeRate(datTemp.Rows[0]["CurrencyID"].ToInt32());
                    //            txtSaleRate.Text = (dblSaleRate +
                    //                    (datTemp.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                    //        }
                    //        else
                    //        {
                    //            txtSaleRate.Text = (dblSaleRate +
                    //                     (dblSaleRate * datTemp.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                    //        }
                    //    }
                    //}
                    MobjClsBLLItemMaster.objClsDTOItemMaster.intCurrencyID = datTemp.Rows[0]["CurrencyID"].ToInt32();
                }
            }
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            //GetBatchDetails();
        }

        private void GetBatchDetails()
        {
            if (cboCostingMethod.SelectedValue != null && cboPricingScheme.SelectedValue != null)
            {
                DataTable datTemp = MobjClsBLLItemMaster.FillCombos(new string[] { "*", "InvPricingSchemeReference", "PricingSchemeID=" + cboPricingScheme.SelectedValue.ToInt32() });
                double dblExRate = 0;
                if (datTemp.Rows[0]["PercentOrAmount"].ToString() == "1")
                    dblExRate = getExchangeRate(datTemp.Rows[0]["CurrencyID"].ToInt32());

                if (MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails == null) return;

                for (int iCounter = 0; iCounter <= MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows.Count - 1; iCounter++)
                {
                    if (datTemp.Rows[0]["PercentOrAmount"].ToString() == "1")
                    {
                        MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows[iCounter]["ColSaleRate"] =
                            (MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows[iCounter]["ColPurchaseRate"].ToDouble() +
                            (datTemp.Rows[0]["PercOrAmtValue"].ToDouble() * dblExRate)).ToString();
                    }
                    else
                    {
                        MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows[iCounter]["ColSaleRate"] =
                            (MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows[iCounter]["ColPurchaseRate"].ToDouble() +
                            (MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows[iCounter]["ColPurchaseRate"].ToDouble() *
                            datTemp.Rows[0]["PercOrAmtValue"].ToDouble()) / 100).ToString();
                    }
                }
            }
        }

        DataTable getNewTable()
        {
            DataTable datTemp = new DataTable();
            datTemp.Columns.Add("ColBatchID");
            datTemp.Columns.Add("ColBatchNo");
            datTemp.Columns.Add("ColExpiryDate");
            datTemp.Columns.Add("ColPurchaseRate");
            datTemp.Columns.Add("ColActualRate");
            datTemp.Columns.Add("ColSaleRate");
            datTemp.Columns.Add("ColLandingCost");
            return datTemp;
        }

        private void txtPurchaseRate_TextChanged(object sender, EventArgs e)
        {
            if (txtPurchaseRate.Text == "")
                txtPurchaseRate.Text = "0";
            else
            {
                //if (rdbPurchaseRate.Checked == true)
                //{
                //   // txtActualRate.Text = "0";
                //    //cboPricingScheme.SelectedIndex = -1;
                //    //txtSaleRate.Text = txtPurchaseRate.Text;
                //}
                
               FillwithChangePricingScheme();
            }
            ChangeStatus(sender, e);
        }

        private void txtActualRate_TextChanged(object sender, EventArgs e)
        {
            if (txtActualRate.Text == "")
                txtActualRate.Text = "0";
            else
            {
                if (rdbActualRate.Checked == true)
                {
                    txtPurchaseRate.Text = "0";
                    cboPricingScheme.SelectedIndex = -1;
                    txtSaleRate.Text = txtActualRate.Text;
                }
                //FillwithChangePricingScheme();
            }
            ChangeStatus(sender, e);
        }

        private void txtSaleRate_TextChanged(object sender, EventArgs e)
        {
            if (txtSaleRate.Text == "")
                txtSaleRate.Text = "0";

            this.ChangeStatus(sender, new EventArgs());
        }

        private void dgvBatchwiseCosting_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails != null)
            {
                if (MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows.Count > 0)
                {
                    if (e.RowIndex >= 0)
                    {
                        if (e.ColumnIndex == dgvBatchwiseCosting.Columns["ColPurchaseRate"].Index || e.ColumnIndex == dgvBatchwiseCosting.Columns["ColActualRate"].Index || e.ColumnIndex == dgvBatchwiseCosting.Columns["ColSaleRate"].Index)
                        {
                            if (MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows[e.RowIndex]["ColPurchaseRate"].ToDouble() == 0)
                                MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows[e.RowIndex]["ColPurchaseRate"] = 0;
                            if (MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows[e.RowIndex]["ColActualRate"].ToDouble() == 0)
                                MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows[e.RowIndex]["ColActualRate"] = 0;
                            if (MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows[e.RowIndex]["ColSaleRate"].ToDouble() == 0)
                                MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows[e.RowIndex]["ColSaleRate"] = 0;
                            
                            MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows[e.RowIndex]["ColPurchaseRate"] = dgvBatchwiseCosting.Rows[e.RowIndex].Cells["ColPurchaseRate"].Value.ToString();
                            MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows[e.RowIndex]["ColActualRate"] = dgvBatchwiseCosting.Rows[e.RowIndex].Cells["ColActualRate"].Value.ToString();
                            MobjClsBLLItemMaster.objClsDTOItemMaster.datBatchDetails.Rows[e.RowIndex]["ColSaleRate"] = dgvBatchwiseCosting.Rows[e.RowIndex].Cells["ColSaleRate"].Value.ToString();
                        }
                    }
                }
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Products";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {

        }

        private void btnPricingScheme_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboPricingScheme.SelectedValue != null)
                    MintPricingID = Convert.ToInt32(cboPricingScheme.SelectedValue.ToString());

                using (frmPricingScheme objPricingScheme = new frmPricingScheme())
                {
                    objPricingScheme.ShowDialog();
                }
                LoadCombos(16);
                cboPricingScheme.SelectedValue = MintPricingID.ToString();
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on form FrmItemMaster:FrmItemMaster " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnBaseUom_Click() " + Ex.Message.ToString());
            }
        }

        private double getExchangeRate(int intCurrencyID)
        {
            double dblExRate = 0;
            errorItemMaster.Clear();
            DataTable datTempExRate = new DataTable();
           
            datTempExRate = MobjClsBLLItemMaster.FillCombos(new string[] { "*", "CurrencyDetails", "CompanyID=" + ClsCommonSettings.CompanyID + " AND CurrencyID=" + intCurrencyID });
            if (datTempExRate != null)
            {
                if (datTempExRate.Rows.Count > 0)
                {
                    dblExRate = Convert.ToDouble(datTempExRate.Rows[0]["ExchangeRate"].ToString());
                }
                else
                {
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1659, out MmsgMessageBoxIcon);
                    errorItemMaster.SetError(cboPricingScheme, MstrMessageCommon.Replace("#", "").Trim());
                    lblItemMasterStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    cboPricingScheme.Focus();
                    tmItemMaster.Enabled = true;
                }
            }
            else
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 1659, out MmsgMessageBoxIcon);
                errorItemMaster.SetError(cboPricingScheme, MstrMessageCommon.Replace("#", "").Trim());
                lblItemMasterStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                cboPricingScheme.Focus();
                tmItemMaster.Enabled = true;
            }

            return dblExRate;
        }

        private void rdbPurchaseRate_CheckedChanged(object sender, EventArgs e)
        {
            errorItemMaster.Clear();
            //if (rdbPurchaseRate.Checked == true)
            //    txtActualRate.Text = "0";
            //else
            //    txtPurchaseRate.Text = "0";
            //if (cboCostingMethod.SelectedValue != null)
            //{
            //    if (Convert.ToInt32(cboCostingMethod.SelectedValue) == (int)CostingMethodReference.Batch)
            //        GetBatchDetails();
            //    else
            //        FillwithChangePricingScheme();
            //}
            this.ChangeStatus(sender, e);
        }

        private void rdbActualRate_CheckedChanged(object sender, EventArgs e)
        {
            errorItemMaster.Clear();
            //if (rdbActualRate.Checked == true)
            //    txtPurchaseRate.Text = "0";
            //else
            //    txtActualRate.Text = "0";
            //if (cboCostingMethod.SelectedValue != null)
            //{
            //    if (Convert.ToInt32(cboCostingMethod.SelectedValue) == (int)CostingMethodReference.Batch)
            //        GetBatchDetails();
            //    else
            if (cboCostingMethod.SelectedValue != null && Convert.ToInt32(cboCostingMethod.SelectedValue) != (int)CostingMethodReference.Batch)
                FillwithChangePricingScheme();
            //}

            this.ChangeStatus(sender, e);
        }

        private void frmItemMaster_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.MdiFormClosing)
            {
                if (MBlnChangeStatus)
                {
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MdatMessages, 4214, out MmsgMessageBoxIcon).Replace("#", "").Trim();
                    if (MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                        DialogResult.Yes)
                    {

                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        private void lnkLabelAdvanceSearch_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DataTable datAdvanceSearchedData = new DataTable();
            using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.Products))
            {
                objFrmSearchForm.ShowDialog();
                if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                {
                    datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                    datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No", "Name");
                    datAdvanceSearchedData.Columns["ID"].ColumnName = "ItemID";
                    datAdvanceSearchedData.Columns["No"].ColumnName = "Code";
                    datAdvanceSearchedData.Columns["Name"].ColumnName = "Name";
                    dgvItems.DataSource = datAdvanceSearchedData;
                    dgvItems.Columns[0].Visible = false;
                    dgvItems.Columns[1].Visible = false;
                    dgvItems.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }

        }

        private void tabControlPanel1_Click(object sender, EventArgs e)
        {

        }


        #region Added by Ratheesh

        private void dgvDimensions_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                //if (e.RowIndex >= 0 && e.ColumnIndex > 0)
                //{
                //    this.dgvDimensions.CommitEdit(DataGridViewDataErrorContexts.Commit);

                //    if (this.dgvDimensions.Columns["clmCboDimensionUOM"].Index == e.ColumnIndex)
                //    {
                //        if (this.dgvDimensions.Rows[e.RowIndex].Cells["clmCboDimension"].Value != null)
                //        {
                //            int DimensionID = this.dgvDimensions.Rows[e.RowIndex].Cells["clmCboDimension"].Value.ToInt32();

                //            int tag = -1;

                //            if (this.dgvDimensions.CurrentRow.Cells["clmCboDimension"].Tag != null)
                //                tag = this.dgvDimensions.CurrentRow.Cells["clmCboDimension"].Tag.ToInt32();

                //            this.BindUOMSByDimensionID(DimensionID);

                //            if (tag != -1)
                //                this.dgvDimensions.CurrentRow.Cells["clmCboDimension"].Tag = tag;
                //        }
                //        else
                //            this.clmCboDimensionUOM.DataSource = null;
                //    }
                //}
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void BindUOMSByDimensionID()
        {
            //this.clmCboDimensionUOM.DataSource = null;
            this.clmCboDimensionUOM.DataSource = this.MobjClsBLLItemMaster.FillCombos(new string[] { " Distinct UOMID, UOMName", "InvUOMReference", "" });//"UOMTypeID = " + DimensionID.ToString() });
            this.clmCboDimensionUOM.ValueMember = "UOMID";
            this.clmCboDimensionUOM.DisplayMember = "UOMName";
        }

        private void dgvDimensions_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            //{
            //    if (this.dgvDimensions.Columns["clmCboDimensionUOM"].Index == e.ColumnIndex)
            //    {
            //        if (this.dgvDimensions.CurrentRow != null && this.dgvDimensions.CurrentRow.Cells["clmCboDimensionUOM"].Value != null)
            //        {
            //            this.dgvDimensions.CurrentRow.Cells["clmCboDimensionUOM"].Tag = this.dgvDimensions.CurrentRow.Cells["clmCboDimensionUOM"].Value;
            //            this.dgvDimensions.CurrentRow.Cells["clmCboDimensionUOM"].Value = this.dgvDimensions.CurrentRow.Cells["clmCboDimensionUOM"].FormattedValue;
            //        }
            //    }
            //}
        }

        private void dgvDimensions_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private void dgvDimensions_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgvDimensions.Rows.Count > 0)
            {
                //if (e.ColumnIndex == 0 && e.RowIndex > -1)
                //{
                //    clmCboDimensionUOM.DataSource = null;
                //    clmCboDimensionUOM.Items.Clear();
                //    this.dgvDimensions.Rows[e.RowIndex].Cells["clmCboDimensionUOM"].Tag = null;
                //    this.dgvDimensions.Rows[e.RowIndex].Cells["clmCboDimensionUOM"].Value = null;
                //}

                if (e.ColumnIndex == 1 && e.RowIndex > -1)
                {
                    if (this.dgvDimensions.Rows[e.RowIndex].Cells[1].Value.ToStringCustom() != string.Empty)
                        this.dgvDimensions.Rows[e.RowIndex].Cells[1].Value = this.dgvDimensions.Rows[e.RowIndex].Cells[1].Value.ToDecimal().Format(3);
                }
            }

            this.ChangeStatus(sender, e);
        }

        private void btnAddFeature_Click(object sender, EventArgs e)
        {
            using (frmCreateFeatures objFeatures = new frmCreateFeatures())
            {
                objFeatures.FormBorderStyle = FormBorderStyle.FixedSingle;
                objFeatures.StartPosition = FormStartPosition.CenterParent;
                objFeatures.MaximizeBox = false;
                objFeatures.objParentForm = this;
                objFeatures.ShowDialog();
            }
        }

        public void BindFeatureGrid(List<clsFeatureTemp> Features)
        {
            this.dgvFeatures.DataSource = null;
            this.dgvFeatures.Rows.Clear();

            int Index = 0;
            foreach (var item in Features)
            {
                this.dgvFeatures.RowCount += 1;
                this.dgvFeatures.Rows[Index].Cells[0].Value = item.FeatureName;
                this.dgvFeatures.Rows[Index].Cells[1].Value = item.FeaturesCSV;
                this.dgvFeatures.Rows[Index].Cells[0].Tag = item.FeatureID;

                Index += 1;
            }
        }

        public void BindDimensions()
        {
            this.dgvDimensions.Rows.Clear();

            int Index = 0;
            foreach (var dimension in this.MobjClsBLLItemMaster.objClsDTOItemMaster.Dimensions)
            {
                this.dgvDimensions.RowCount += 1;
                this.dgvDimensions.Rows[Index].Cells[0].Value = dimension.DimensionID;
                this.dgvDimensions.Rows[Index].Cells[0].Tag = dimension.DimensionID;
                this.dgvDimensions.Rows[Index].Cells[1].Value = dimension.DimensionValue;

                this.BindUOMSByDimensionID();

                this.dgvDimensions.Rows[Index].Cells[2].Value = dimension.UOMID;
               // this.dgvDimensions.Rows[Index].Cells[2].Tag = dimension.UOMID;
                //this.dgvDimensions.Rows[Index].Cells[2].Value = this.dgvDimensions.Rows[Index].Cells[2].FormattedValue;

                Index += 1;
            }
        }

        #endregion

        private void dgvComponents_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private void dgvFeatures_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private void dgvProductionStages_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            if (this.ImageIndex > 1)
            {
                this.ImageIndex -= 1;
                this.ShowImage();
                this.EnableDisableButtons();
            }
        }

        private void ShowImage()
        {
            if (this.ImageIndex <= this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images.Count && this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images.Count > 0)
            {
                clsDTOItemImages image = this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images[this.ImageIndex - 1];
                if (image != null)
                {
                    if (File.Exists(ClsCommonSettings.strServerPath + @"\Product Images\High Resolution\" + image.FileName))
                    {
                        FileStream f = new FileStream(ClsCommonSettings.strServerPath + @"\Product Images\High Resolution\" + image.FileName, FileMode.Open, FileAccess.Read);
                        pbItemPhoto.Image = Image.FromStream(f);
                        pbItemPhoto.SizeMode = PictureBoxSizeMode.StretchImage;
                        f.Close();

                        pbItemPhoto.Tag = image.FileName;
                    }
                    else
                    {
                        pbItemPhoto.Image = null;
                        pbItemPhoto.Tag = null;
                        //MessageBox.Show("File Not found");
                    }
                }
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (this.ImageIndex < this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images.Count)
            {
                this.ImageIndex += 1;
                this.ShowImage();
                this.EnableDisableButtons();
            }
        }

        private void EnableDisableButtons()
        {
            this.btnPrev.Enabled = this.ImageIndex > 1;
            this.btnNext.Enabled = this.ImageIndex < this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images.Count;

            if (this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images.Count > 0)
                this.lblImageText.Text = string.Format("{0} of {1}", this.ImageIndex, this.MobjClsBLLItemMaster.objClsDTOItemMaster.Images.Count);
            else
            {
                this.lblImageText.Text = "0 of 0";
                this.ImageIndex = 0;
            }
        }

        private void dgvProductionStages_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void TextboxNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
            if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
            {
                e.Handled = true;
            }
            if (((TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            {
                e.Handled = true;
            }
        }

        private void dgvDimensions_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                int ColumnIndex = Convert.ToInt32((((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex));

                e.Control.KeyPress -= this.TextboxNumeric_KeyPress;

                if (ColumnIndex == this.clmDimensionValue.Index)
                {
                    e.Control.KeyPress += this.TextboxNumeric_KeyPress;
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void dgvFeatures_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            int FeatureID = e.Row.Cells[0].Tag.ToInt32();

            if (FeatureID > 0)
                this.MobjClsBLLItemMaster.objClsDTOItemMaster.Features.RemoveAll(f => f.FeatureID == FeatureID);
        }

        private void dgvFeatures_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (this.dgvFeatures.Rows[e.RowIndex].Cells[0].Tag.ToInt32() > 0)
                {
                    using (frmCreateFeatures objFrmCreateFeatures = new frmCreateFeatures())
                    {
                        objFrmCreateFeatures.FeatureID = this.dgvFeatures.Rows[e.RowIndex].Cells[0].Tag.ToInt32();
                        objFrmCreateFeatures.objParentForm = this;
                        objFrmCreateFeatures.StartPosition = FormStartPosition.CenterParent;
                        objFrmCreateFeatures.FormBorderStyle = FormBorderStyle.FixedSingle;
                        objFrmCreateFeatures.ShowDialog();
                    }
                }
            }
        }

        private void dgvFeatures_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            this.ChangeStatus(sender, e);
        }

        private void dgvProductionStages_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            this.ChangeStatus(sender, e);
        }

        private void chkUOMSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.dgvUnitsOfMeasurements.Rows)
            {
                row.Cells[0].Value = this.chkUOMSelectAll.Checked;
            }

            this.ChangeStatus(sender, e);
        }

        private void mnuPrintBarcode_Click(object sender, EventArgs e)
        {
            btnBarcodeCreate.Text = "Print";
            using (FrmBarCode objBarCode = new FrmBarCode())
            {
                objBarCode.PlngItemID = MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID;
                objBarCode.PstrBarcodeValue = txtItemCode.Text.Trim();
                objBarCode.PblnIsTemplate = false;
                objBarCode.ShowDialog();
                //if (objBarCode.PimgBarcodeImage != null)//if cancel
                //    pbBarCode.Image = objBarCode.PimgBarcodeImage;
                //MobjClsBLLItemMaster.objClsDTOItemMaster.strBarcodeValue = objBarCode.PstrBarcodeValue;
                objBarCode.Dispose();
            }
        }

        private void cboDefaultPurchaseUom_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ChangeStatus(sender, e);
        }

        private void cboDefaultSalesUom_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ChangeStatus(sender, e);
        }

        private void dgvDimensions_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgvDimensions.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void cboCompany_KeyUp(object sender, KeyEventArgs e)
        {
            //if (MobjClsBLLItemMaster.objClsDTOItemMaster.intItemID != 0)
            //{
            //    int intCompanyId = 0;
            //    if (cboCompany.SelectedIndex != -1)
            //    {
            //        intCompanyId = cboCompany.SelectedValue.ToInt32();
            //    }
            //    else
            //    {
            //        intCompanyId = 0;
            //    }
            //    if (MobjClsBLLItemMaster.objClsDTOItemMaster.intCompanyID != intCompanyId)
            //    {
            //        GenerateItemCode();
            //    }
            //    else
            //    {
            //        txtItemCode.Text = MobjClsBLLItemMaster.objClsDTOItemMaster.strItemCode;

            //    }
            //}
            //else
            //{
            //    GenerateItemCode();
            //}
        }

        private void RowAddedEventIncludingLastRow(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjClsCommonUtility.SetSerialNo(sender as DataGridView, e.RowIndex, true);
        }

        private void RowRemovedEventIncludingLastRow(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjClsCommonUtility.SetSerialNo(sender as DataGridView, e.RowIndex, true);
        }

        private void RowAddedEventExcludingLastRow(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjClsCommonUtility.SetSerialNo(sender as DataGridView, e.RowIndex, false);
        }

        private void RowRemovedEventExcludingLastRow(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjClsCommonUtility.SetSerialNo(sender as DataGridView, e.RowIndex, false);
        }

        private void cboCostingMethod_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCostingMethod.DroppedDown = false;
        }

        private void cboPricingScheme_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboPricingScheme.DroppedDown = false;
        }
    }
}