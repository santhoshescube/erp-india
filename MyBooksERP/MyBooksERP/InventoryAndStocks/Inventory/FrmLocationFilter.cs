﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class FrmLocationFilter : DevComponents.DotNetBar.Office2007Form
    {
        public int PintFilterLocationID { get; set; }
        public int PintFilterRowID { get; set; }
        public int PintFilterBlockID { get; set; }
        public int PintFilterLotID { get; set; }
        public int PintFilterItemID { get; set; }
        public int PintFilterBatchID { get; set; }
        public bool PblnBtnOk { get; set; }

        private int MintType;
        private int MintWarehouseID;
        private bool MblnBtnOk = false;
        private bool MblnMoveDamagedQty = false;
        private clsBLLLocationTransfer MobclsBLLLocationTransfer;
       


        public FrmLocationFilter(int intType,int intWarehouseID,bool blnMoveDamagedQty)
        {
            InitializeComponent();
            MintType=intType;
            MintWarehouseID = intWarehouseID;
            MblnMoveDamagedQty = blnMoveDamagedQty;
            switch (MintType)
            {
                case 1:// from first tree(item location)
                    CboXItem.Enabled = true;
                    CboXBatch.Enabled = true;
                    break;
                case 2:// from Second tree(Warehouse)
                    CboXItem.Enabled = false;
                    CboXBatch.Enabled = false;
                    break;
                default:
                    break;

            }
            MobclsBLLLocationTransfer = new clsBLLLocationTransfer();

        }

        private void FrmLocationFilter_Load(object sender, EventArgs e)
        {
           
            if (MintType == 1)
            {
                LoadCombo(1);//Location
                LoadCombo(7);//Item
            }
            else if (MintType == 2)
            {
                LoadCombo(11);//Location warehouse
            }
              //Filterations
            //    case 1://Comapny and Warehouse
            //        DisplayOriginalLocationInfo();
            //        break;
            //    case 2://Comapny and warehouse and location
            //        break;
            //    case 3://All
            //        DisplayOriginalLocationInfoByFilter();
            //        break;
            //    case 4://Comapny and warehouse and location AND ITEM
            //        break;
            //    case 5://Comapny and warehouse anD ITEM
            //        break;
            //    case 6://Comapny and warehouse and location AND ITEM AND BATCH


            
                
        }
        private void CboXLocation_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (Convert.ToInt32(CboXLocation.SelectedValue) > 0 && MintWarehouseID > 0 )
            {
                LoadCombo(2);//ROw
                if (MintType == 1)
                {
                    LoadCombo(8);//item combo
                }
                else if (MintType == 2)
                {
                    LoadCombo(12);//Row
                }
            }
        }

        private void cboXRow_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (Convert.ToInt32(cboXRow.SelectedValue) > 0 && Convert.ToInt32(CboXLocation.SelectedValue) > 0 && MintWarehouseID > 0 )
            {
                
                if (MintType == 1)
                {
                    LoadCombo(3);//Block
                }
                else if (MintType == 2)
                {
                    LoadCombo(13);//block
                }
            }
        }

        private void CboXBlock_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (Convert.ToInt32(CboXBlock.SelectedValue) > 0 && Convert.ToInt32(cboXRow.SelectedValue) > 0 && Convert.ToInt32(CboXLocation.SelectedValue) > 0 && MintWarehouseID > 0 )
            {
                
                if (MintType == 1)
                {
                    LoadCombo(4);//Lot
                }
                else if (MintType == 2)
                {
                    LoadCombo(14);//Lot
                }
            }
        }

        private void CboXLot_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (MintType==1 &&   Convert.ToInt32(CboXLot.SelectedValue) > 0 && Convert.ToInt32(CboXBlock.SelectedValue) > 0 && Convert.ToInt32(cboXRow.SelectedValue) > 0 && Convert.ToInt32(CboXLocation.SelectedValue) > 0 && MintWarehouseID > 0 )
            {
                LoadCombo(5);//Item 
            }
        }

        private void CboXItem_SelectionChangeCommitted(object sender, EventArgs e)
        {
            bool blnFillCombo = false;
            if (blnFillCombo == false && Convert.ToInt32(CboXItem.SelectedValue) > 0 && Convert.ToInt32(CboXLot.SelectedValue) > 0 && Convert.ToInt32(CboXBlock.SelectedValue) > 0 && Convert.ToInt32(cboXRow.SelectedValue) > 0 && Convert.ToInt32(CboXLocation.SelectedValue) > 0 && MintWarehouseID > 0 )
            {
                LoadCombo(6);//Batch 
                blnFillCombo = true;
            }
            if (blnFillCombo == false && Convert.ToInt32(CboXItem.SelectedValue) > 0 && Convert.ToInt32(CboXLocation.SelectedValue) > 0 && MintWarehouseID > 0 )
            {
                LoadCombo(10);//Batch  Combo  fill by Item and warehouse and item and location
                blnFillCombo = true;
            }
            if (blnFillCombo == false && Convert.ToInt32(CboXItem.SelectedValue) > 0 && MintWarehouseID > 0 )
            {
                LoadCombo(9);//Batch  Combo  fill by Item and warehouse
                blnFillCombo = true;
            }
        }
          

        
        private bool LoadCombo(int intType)
        {

            DataTable datCombos = new DataTable();
            
            if (intType == 0 || intType == 1)//Location Combo
            {
                if (MblnMoveDamagedQty == true)
                {

                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.LocationID,WHD.Location", "STItemLocation IL inner join STWarehouseDetails WHD on IL.LocationID=WHD.LocationID and IL.WarehouseID=WHD.WarehouseID",
                                                                                "IL.WarehouseID=" + MintWarehouseID + " and isnull(IL.DamagedQuantity,0) > 0" });
                }
                else
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.LocationID,WHD.Location", "STItemLocation IL inner join STWarehouseDetails WHD on IL.LocationID=WHD.LocationID and IL.WarehouseID=WHD.WarehouseID",
                                                                                "IL.WarehouseID=" + MintWarehouseID + " and isnull(IL.Quantity,0) > 0" });
                }
                CboXLocation.ValueMember = "LocationID";
                CboXLocation.DisplayMember = "Location";
                CboXLocation.DataSource = datCombos;
                CboXLocation.SelectedIndex = -1;

            }
            if (intType == 0 || intType == 2)//Row Combo
            {
                if (MblnMoveDamagedQty == true)
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.RowID,WHD.RowNumber", "STItemLocation IL inner join STWarehouseDetails WHD on IL.LocationID=WHD.LocationID and  IL.WarehouseID=WHD.WarehouseID and IL.RowID=WHD.RowID",
                                                                                "IL.WarehouseID="+MintWarehouseID + " and IL.LocationID=" + Convert.ToInt32(CboXLocation.SelectedValue) + " and  isnull(IL.DamagedQuantity,0) > 0" });

                }
                else
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.RowID,WHD.RowNumber", "STItemLocation IL inner join STWarehouseDetails WHD on IL.LocationID=WHD.LocationID and  IL.WarehouseID=WHD.WarehouseID and IL.RowID=WHD.RowID",
                                                                                "IL.WarehouseID="+MintWarehouseID + " and IL.LocationID=" + Convert.ToInt32(CboXLocation.SelectedValue) + " and isnull(IL.Quantity,0) > 0" });


                }
                cboXRow.ValueMember = "RowID";
                cboXRow.DisplayMember = "RowNumber";
                cboXRow.DataSource = datCombos;
                cboXRow.SelectedIndex = -1;


            }
            if (intType == 0 || intType == 3)//Block Combo
            {
                if (MblnMoveDamagedQty == true)
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.BlockID,WHD.BlockNumber", "STItemLocation IL inner join STWarehouseDetails WHD on IL.LocationID=WHD.LocationID and IL.WarehouseID=WHD.WarehouseID and IL.RowID=WHD.RowID  and IL.BlockID=WHD.BlockID",
                                                                                "IL.WarehouseID="+ MintWarehouseID + " and IL.LocationID=" + Convert.ToInt32(CboXLocation.SelectedValue) + " and  IL.RowID="+Convert.ToInt32(cboXRow.SelectedValue)+" and  isnull(IL.DamagedQuantity,0) > 0" });


                }
                else
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.BlockID,WHD.BlockNumber", "STItemLocation IL inner join STWarehouseDetails WHD on IL.LocationID=WHD.LocationID and IL.WarehouseID=WHD.WarehouseID and IL.RowID=WHD.RowID  and IL.BlockID=WHD.BlockID",
                                                                                "IL.WarehouseID="+ MintWarehouseID + " and IL.LocationID=" + Convert.ToInt32(CboXLocation.SelectedValue) + " and  IL.RowID="+Convert.ToInt32(cboXRow.SelectedValue)+"  and isnull(IL.Quantity,0) > 0" });

                }
                CboXBlock.ValueMember = "BlockID";
                CboXBlock.DisplayMember = "BlockNumber";
                CboXBlock.DataSource = datCombos;
                CboXBlock.SelectedIndex = -1;

            }
            if (intType == 0 || intType == 4)//Lot Combo
            {
                if (MblnMoveDamagedQty == true)
                {

                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.LotID,WHD.LotNumber", "STItemLocation IL inner join STWarehouseDetails WHD on IL.LocationID=WHD.LocationID and IL.WarehouseID=WHD.WarehouseID and IL.RowID=WHD.RowID  and IL.BlockID=WHD.BlockID and IL.LotID=WHD.LotID",
                                                                               "IL.WarehouseID="+MintWarehouseID + " and IL.LocationID=" + Convert.ToInt32(CboXLocation.SelectedValue) + " and  IL.RowID="+Convert.ToInt32(cboXRow.SelectedValue)+" and IL.BlockID=" +Convert.ToInt32(CboXBlock.SelectedValue) +" and  isnull(IL.DamagedQuantity,0) > 0"});

                }
                else
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.LotID,WHD.LotNumber", "STItemLocation IL inner join STWarehouseDetails WHD on IL.LocationID=WHD.LocationID and IL.WarehouseID=WHD.WarehouseID and IL.RowID=WHD.RowID  and IL.BlockID=WHD.BlockID and IL.LotID=WHD.LotID",
                                                                               "IL.WarehouseID="+MintWarehouseID + " and IL.LocationID=" + Convert.ToInt32(CboXLocation.SelectedValue) + " and  IL.RowID="+Convert.ToInt32(cboXRow.SelectedValue)+" and IL.BlockID=" +Convert.ToInt32(CboXBlock.SelectedValue) +" and isnull(IL.Quantity,0) > 0"});

             
                } 
                CboXLot.ValueMember = "LotID";
                CboXLot.DisplayMember = "LotNumber";
                CboXLot.DataSource = datCombos;
                CboXLot.SelectedIndex = -1;

            }

            if (intType == 0 || intType == 5)//Item Combo filling --- wit all filter
            {

                if (MblnMoveDamagedQty == true)
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.ItemID,IM.Description", "STItemLocation IL inner join STItemMaster IM on  IL.ItemID=IM.ItemID",
                                                                               "IL.WarehouseID="+MintWarehouseID + " and IL.LocationID=" + Convert.ToInt32(CboXLocation.SelectedValue) + " and  IL.RowID="+Convert.ToInt32(cboXRow.SelectedValue)+" and IL.BlockID=" +Convert.ToInt32(CboXBlock.SelectedValue) +" and IL.LotID="+Convert.ToInt32(CboXLot.SelectedValue)+" and   isnull(IL.DamagedQuantity,0) > 0"});


                }
                else
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.ItemID,IM.Description", "STItemLocation IL inner join STItemMaster IM on  IL.ItemID=IM.ItemID",
                                                                               "IL.WarehouseID="+MintWarehouseID + " and IL.LocationID=" + Convert.ToInt32(CboXLocation.SelectedValue) + " and  IL.RowID="+Convert.ToInt32(cboXRow.SelectedValue)+" and IL.BlockID=" +Convert.ToInt32(CboXBlock.SelectedValue) +" and IL.LotID="+Convert.ToInt32(CboXLot.SelectedValue)+" and isnull(IL.Quantity,0) > 0"});


                }
                
                CboXItem.ValueMember = "ItemID";
                CboXItem.DisplayMember = "Description";
                CboXItem.DataSource = datCombos;
                CboXItem.SelectedIndex = -1;


            }
            if (intType == 0 || intType == 6)//Batch Combo 
            {
                if (MblnMoveDamagedQty == true)
                {

                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.BatchID,BD.BatchNo", "STItemLocation IL inner join STBatchDetails BD on  IL.BatchID=BD.BatchID and IL.ItemID=BD.ItemID",
                                                                               "IL.WarehouseID="+MintWarehouseID + " and IL.LocationID=" + Convert.ToInt32(CboXLocation.SelectedValue) + " and  IL.RowID="+Convert.ToInt32(cboXRow.SelectedValue)+" and IL.BlockID=" +Convert.ToInt32(CboXBlock.SelectedValue) +" and IL.LotID="+Convert.ToInt32(CboXLot.SelectedValue)+" and  IL.ItemID="+Convert.ToInt32(CboXItem.SelectedValue) +" and   isnull(IL.DamagedQuantity,0) > 0"});


                }
                else
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.BatchID,BD.BatchNo", "STItemLocation IL inner join STBatchDetails BD on  IL.BatchID=BD.BatchID and IL.ItemID=BD.ItemID",
                                                                               "IL.WarehouseID="+MintWarehouseID + " and IL.LocationID=" + Convert.ToInt32(CboXLocation.SelectedValue) + " and  IL.RowID="+Convert.ToInt32(cboXRow.SelectedValue)+" and IL.BlockID=" +Convert.ToInt32(CboXBlock.SelectedValue) +" and IL.LotID="+Convert.ToInt32(CboXLot.SelectedValue)+" and  IL.ItemID="+Convert.ToInt32(CboXItem.SelectedValue) +" and isnull(IL.Quantity,0) > 0"});


                }

                CboXBatch.ValueMember = "BatchID";
                CboXBatch.DisplayMember = "BatchNo";
                CboXBatch.DataSource = datCombos;
                CboXBatch.SelectedIndex = -1;

            }
            if (intType == 0 || intType == 7)//Item  Combo  fill by ware house
            {
                if (MblnMoveDamagedQty == true)
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.ItemID,IM.Description", "STItemLocation IL inner join STItemMaster IM on   IL.ItemID=IM.ItemID ",
                                                                               "IL.WarehouseID="+MintWarehouseID + " and isnull(IL.DamagedQuantity,0) > 0 "});

                }
                else
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.ItemID,IM.Description", "STItemLocation IL inner join STItemMaster IM on   IL.ItemID=IM.ItemID ",
                                                                               "IL.WarehouseID="+MintWarehouseID + " and isnull(IL.Quantity,0) > 0"});

                } 
                CboXItem.ValueMember = "ItemID";
                CboXItem.DisplayMember = "Description";
                CboXItem.DataSource = datCombos;
                CboXItem.SelectedIndex = -1;


            }
            if (intType == 0 || intType == 8)//Item  Combo  fill by ware house and Location
            {
                if (MblnMoveDamagedQty == true)
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.ItemID,IM.Description", "STItemLocation IL inner join STItemMaster IM on   IL.ItemID=IM.ItemID ",
                                                                               "IL.WarehouseID="+MintWarehouseID + " and IL.LocationID="+Convert.ToInt32(CboXLocation.SelectedValue) +" and isnull(IL.DamagedQuantity,0) > 0 "});

                }
                else
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.ItemID,IM.Description", "STItemLocation IL inner join STItemMaster IM on   IL.ItemID=IM.ItemID ",
                                                                               "IL.WarehouseID="+MintWarehouseID + " and IL.LocationID="+Convert.ToInt32(CboXLocation.SelectedValue) +" and isnull(IL.Quantity,0) > 0"});

  
                }
                CboXItem.ValueMember = "ItemID";
                CboXItem.DisplayMember = "Description";
                CboXItem.DataSource = datCombos;
                CboXItem.SelectedIndex = -1;
            }
            if (intType == 0 || intType == 9)//Batch  Combo  fill by Item and warehouse
            {
                if (MblnMoveDamagedQty == true)
                {

                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.BatchID,BD.BatchNo", " STItemLocation IL inner join STBatchDetails BD on  IL.BatchID=BD.BatchID and IL.ItemID=BD.ItemID ",
                                                                               "IL.WarehouseID="+MintWarehouseID + " and  IL.ItemID="+Convert.ToInt32(CboXItem.SelectedValue) +" and isnull(IL.DamagedQuantity,0) > 0"});

                }
                else
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.BatchID,BD.BatchNo", " STItemLocation IL inner join STBatchDetails BD on  IL.BatchID=BD.BatchID and IL.ItemID=BD.ItemID ",
                                                                               "IL.WarehouseID="+MintWarehouseID + " and  IL.ItemID="+Convert.ToInt32(CboXItem.SelectedValue) +" and isnull(IL.Quantity,0) > 0"});


                }
                CboXBatch.ValueMember = "BatchID";
                CboXBatch.DisplayMember = "BatchNo";
                CboXBatch.DataSource = datCombos;
                CboXBatch.SelectedIndex = -1;
            }
            if (intType == 0 || intType == 10)//Batch  Combo  fill by Item and warehouse and item and location
            {
                if (MblnMoveDamagedQty == true)
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.BatchID,BD.BatchNo", " STItemLocation IL inner join STBatchDetails BD on  IL.BatchID=BD.BatchID and IL.ItemID=BD.ItemID ",
                                                                               "IL.WarehouseID="+MintWarehouseID + "and  IL.LocationID="+Convert.ToInt32(CboXLocation.SelectedValue)+" and  IL.ItemID="+Convert.ToInt32(CboXItem.SelectedValue) +" and   isnull(IL.DamagedQuantity,0) > 0"});

                }

                else
                {
                    datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct IL.BatchID,BD.BatchNo", " STItemLocation IL inner join STBatchDetails BD on  IL.BatchID=BD.BatchID and IL.ItemID=BD.ItemID ",
                                                                               "IL.WarehouseID="+MintWarehouseID + "and  IL.LocationID="+Convert.ToInt32(CboXLocation.SelectedValue)+" and  IL.ItemID="+Convert.ToInt32(CboXItem.SelectedValue) +" and   isnull(IL.Quantity,0) > 0"});

                }
                
                CboXBatch.ValueMember = "BatchID";
                CboXBatch.DisplayMember = "BatchNo";
                CboXBatch.DataSource = datCombos;
                CboXBatch.SelectedIndex = -1;

            }
            // WareHouse combo---
            if (intType == 0 || intType == 11)//WareHouse Location
            {
                datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct   Location,LocationID", " STWarehouseDetails ","WarehouseID ="+ MintWarehouseID+""});

                CboXLocation.ValueMember = "LocationID";
                CboXLocation.DisplayMember = "Location";
                CboXLocation.DataSource = datCombos;
                CboXLocation.SelectedIndex = -1;

            }
            if (intType == 0 || intType == 12)//WareHouse ROW 
            {
                datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct RowNumber,RowID", " STWarehouseDetails ", "WarehouseID =" + MintWarehouseID + " and LocationID="+Convert.ToInt32(CboXLocation.SelectedValue)+ ""});

                cboXRow.ValueMember = "RowID";
                cboXRow.DisplayMember = "RowNumber";
                cboXRow.DataSource = datCombos;
                cboXRow.SelectedIndex = -1;

            }
            if (intType == 0 || intType == 13)//WareHouse Block 
            {
                datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct BlockNumber,BlockID", " STWarehouseDetails ",
                                                   "WarehouseID =" + MintWarehouseID + " and LocationID=" + Convert.ToInt32(CboXLocation.SelectedValue) + " and  RowID="+Convert.ToInt32(cboXRow.SelectedValue)+"" });

                CboXBlock.ValueMember = "BlockID";
                CboXBlock.DisplayMember = "BlockNumber";
                CboXBlock.DataSource = datCombos;
                CboXBlock.SelectedIndex = -1;

            }
            if (intType == 0 || intType == 14)//WareHouse Lot 
            {
                datCombos = MobclsBLLLocationTransfer.FillCombos(new string[] { "Distinct  LotNumber ,LotID", " STWarehouseDetails ",
                                                   "WarehouseID =" + MintWarehouseID + " and LocationID=" + Convert.ToInt32(CboXLocation.SelectedValue) + " and  RowID="+Convert.ToInt32(cboXRow.SelectedValue)+" and BlockID="+Convert.ToInt32(CboXBlock.SelectedValue)+"" });

                CboXLot.ValueMember = "LotID";
                CboXLot.DisplayMember = "LotNumber";
                CboXLot.DataSource = datCombos;
                CboXLot.SelectedIndex = -1;

            }

            return true;
        }

        private void btnXOk_Click(object sender, EventArgs e)
        {
            MblnBtnOk = true;
            PblnBtnOk = true;
            if (Convert.ToInt32(CboXLocation.SelectedValue) > 0)
                PintFilterLocationID = Convert.ToInt32(CboXLocation.SelectedValue);
            if (Convert.ToInt32(cboXRow.SelectedValue) > 0)
                PintFilterRowID = Convert.ToInt32(cboXRow.SelectedValue);
            if (Convert.ToInt32(CboXBlock.SelectedValue) > 0)
                PintFilterBlockID = Convert.ToInt32(CboXBlock.SelectedValue);
            if (Convert.ToInt32(CboXLot.SelectedValue) > 0)
                PintFilterLotID = Convert.ToInt32(CboXLot.SelectedValue);
            if (Convert.ToInt32(CboXItem.SelectedValue) > 0)
                PintFilterItemID = Convert.ToInt32(CboXItem.SelectedValue);
            if (Convert.ToInt32(CboXBatch.SelectedValue) > 0)
                PintFilterBatchID = Convert.ToInt32(CboXBatch.SelectedValue);

            this.Close();
        }

        private void FrmLocationFilter_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MblnBtnOk == false)
            {
                PblnBtnOk = false;
                PintFilterLocationID = 0;
                PintFilterRowID = 0;
                PintFilterBlockID = 0;
                PintFilterLotID = 0;
                PintFilterItemID = 0;
                PintFilterBatchID = 0;
            }
        }

        private void btnXCancel_Click(object sender, EventArgs e)
        {
            MblnBtnOk = false;
            PblnBtnOk = false;
            this.Close();
        }

        private void ComboBoxX_KeyDown(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
            if (e.KeyCode == Keys.Enter)
            {
                string strComboName = cbo.Name;
                switch (strComboName)
                {
                        
                    case "CboXLocation":
                       CboXLocation_SelectionChangeCommitted(sender, new EventArgs());
                        break;
                    case "cboXRow":
                        cboXRow_SelectionChangeCommitted(sender, new EventArgs());
                        break;
                    case "CboXBlock":
                        CboXBlock_SelectionChangeCommitted(sender, new EventArgs());
                        break;
                    case "CboXLot":
                        CboXLot_SelectionChangeCommitted(sender, new EventArgs());
                        break;
                    case "CboXItem":
                        CboXItem_SelectionChangeCommitted(sender, new EventArgs());
                        break;
                    case "CboXBatch":
                        break;
                     default:
                        break;
                }


            }
        }

        

    }
}
