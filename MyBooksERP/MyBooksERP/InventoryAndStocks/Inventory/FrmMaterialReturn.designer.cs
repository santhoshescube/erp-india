﻿namespace MyBooksERP
{
    partial class FrmMaterialReturn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMaterialReturn));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelMain = new DevComponents.DotNetBar.PanelEx();
            this.panelMiddle = new DevComponents.DotNetBar.PanelEx();
            this.paneltcMaterialReturn = new DevComponents.DotNetBar.PanelEx();
            this.tcMaterialReturn = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvMaterialReturn = new ClsInnerGridBar();
            this.tiItemDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.pnlBottom = new DevComponents.DotNetBar.PanelEx();
            this.pnlBottomRight = new DevComponents.DotNetBar.PanelEx();
            this.cboCurrency = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.lblNetAmount = new System.Windows.Forms.Label();
            this.txtNetAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.pnlMainBottom = new System.Windows.Forms.Panel();
            this.lblCreatedByText = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblCreatedDateValue = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblStatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.txtRemarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.expandableSplitterTop = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.tcGeneral = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.cboProject = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblProject = new System.Windows.Forms.Label();
            this.cboWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new System.Windows.Forms.Label();
            this.txtMaterialReturnNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.dtpReturnedDate = new System.Windows.Forms.DateTimePicker();
            this.lblMaterialReturnDate = new System.Windows.Forms.Label();
            this.lblMaterialReturnNo = new System.Windows.Forms.Label();
            this.tiGeneral = new DevComponents.DotNetBar.TabItem(this.components);
            this.bnMaterialReturn = new DevComponents.DotNetBar.Bar();
            this.bnAddNewItem = new DevComponents.DotNetBar.ButtonItem();
            this.bnSaveItem = new DevComponents.DotNetBar.ButtonItem();
            this.bnClear = new DevComponents.DotNetBar.ButtonItem();
            this.bnDelete = new DevComponents.DotNetBar.ButtonItem();
            this.bnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.bnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvMaterialReturnDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.epMaterialReturn = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelLeftTop = new DevComponents.DotNetBar.PanelEx();
            this.cboSProject = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSearchProject = new System.Windows.Forms.Label();
            this.cboSCreatedBy = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSCreatedBy = new System.Windows.Forms.Label();
            this.lblSMaterialReturnNo = new System.Windows.Forms.Label();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cboSWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSWarehouse = new System.Windows.Forms.Label();
            this.btnRefresh = new DevComponents.DotNetBar.ButtonX();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboSCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSCompany = new System.Windows.Forms.Label();
            this.lblSCountStatus = new DevComponents.DotNetBar.LabelX();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.tmrMaterialReturn = new System.Windows.Forms.Timer(this.components);
            this.ErrMaterialReturn = new System.Windows.Forms.ErrorProvider(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColBatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColBatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColIssuedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColReturnedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColUOM = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvColTempIssuedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColIssuedUomID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColIssuedRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColNetAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelMain.SuspendLayout();
            this.panelMiddle.SuspendLayout();
            this.paneltcMaterialReturn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcMaterialReturn)).BeginInit();
            this.tcMaterialReturn.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialReturn)).BeginInit();
            this.pnlBottom.SuspendLayout();
            this.pnlBottomRight.SuspendLayout();
            this.pnlMainBottom.SuspendLayout();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).BeginInit();
            this.tcGeneral.SuspendLayout();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnMaterialReturn)).BeginInit();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialReturnDisplay)).BeginInit();
            this.epMaterialReturn.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrMaterialReturn)).BeginInit();
            this.SuspendLayout();
            // 
            // panelMain
            // 
            this.panelMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelMain.Controls.Add(this.panelMiddle);
            this.panelMain.Controls.Add(this.expandableSplitterTop);
            this.panelMain.Controls.Add(this.panelTop);
            this.panelMain.Controls.Add(this.bnMaterialReturn);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(203, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1100, 514);
            this.panelMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelMain.Style.GradientAngle = 90;
            this.panelMain.TabIndex = 0;
            this.panelMain.Text = "panelMain";
            // 
            // panelMiddle
            // 
            this.panelMiddle.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelMiddle.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelMiddle.Controls.Add(this.paneltcMaterialReturn);
            this.panelMiddle.Controls.Add(this.pnlBottom);
            this.panelMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMiddle.Location = new System.Drawing.Point(0, 131);
            this.panelMiddle.Name = "panelMiddle";
            this.panelMiddle.Size = new System.Drawing.Size(1100, 383);
            this.panelMiddle.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelMiddle.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelMiddle.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelMiddle.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelMiddle.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelMiddle.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelMiddle.Style.GradientAngle = 90;
            this.panelMiddle.TabIndex = 1;
            this.panelMiddle.Text = "panelMiddle";
            // 
            // paneltcMaterialReturn
            // 
            this.paneltcMaterialReturn.CanvasColor = System.Drawing.SystemColors.Control;
            this.paneltcMaterialReturn.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.paneltcMaterialReturn.Controls.Add(this.tcMaterialReturn);
            this.paneltcMaterialReturn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.paneltcMaterialReturn.Location = new System.Drawing.Point(0, 0);
            this.paneltcMaterialReturn.Name = "paneltcMaterialReturn";
            this.paneltcMaterialReturn.Size = new System.Drawing.Size(1100, 274);
            this.paneltcMaterialReturn.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.paneltcMaterialReturn.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.paneltcMaterialReturn.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.paneltcMaterialReturn.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.paneltcMaterialReturn.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.paneltcMaterialReturn.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.paneltcMaterialReturn.Style.GradientAngle = 90;
            this.paneltcMaterialReturn.TabIndex = 2;
            this.paneltcMaterialReturn.Text = "panelEx1";
            // 
            // tcMaterialReturn
            // 
            this.tcMaterialReturn.BackColor = System.Drawing.Color.Transparent;
            this.tcMaterialReturn.CanReorderTabs = true;
            this.tcMaterialReturn.Controls.Add(this.tabControlPanel1);
            this.tcMaterialReturn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMaterialReturn.Location = new System.Drawing.Point(0, 0);
            this.tcMaterialReturn.Name = "tcMaterialReturn";
            this.tcMaterialReturn.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcMaterialReturn.SelectedTabIndex = 0;
            this.tcMaterialReturn.Size = new System.Drawing.Size(1100, 274);
            this.tcMaterialReturn.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcMaterialReturn.TabIndex = 1;
            this.tcMaterialReturn.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcMaterialReturn.Tabs.Add(this.tiItemDetails);
            this.tcMaterialReturn.TabStop = false;
            this.tcMaterialReturn.Text = "tabControl1";
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.dgvMaterialReturn);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1100, 252);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 1;
            this.tabControlPanel1.TabItem = this.tiItemDetails;
            // 
            // dgvMaterialReturn
            // 
            this.dgvMaterialReturn.AddNewRow = false;
            this.dgvMaterialReturn.AlphaNumericCols = new int[0];
            this.dgvMaterialReturn.BackgroundColor = System.Drawing.Color.White;
            this.dgvMaterialReturn.CapsLockCols = new int[0];
            this.dgvMaterialReturn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMaterialReturn.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColItemCode,
            this.dgvColItemName,
            this.dgvColItemID,
            this.dgvColBatchID,
            this.dgvColBatchNo,
            this.dgvColIssuedQty,
            this.dgvColReturnedQty,
            this.dgvColQuantity,
            this.dgvColUOM,
            this.dgvColTempIssuedQty,
            this.dgvColIssuedUomID,
            this.dgvColRate,
            this.dgvColIssuedRate,
            this.dgvColNetAmount});
            this.dgvMaterialReturn.DecimalCols = new int[] {
        6};
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMaterialReturn.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvMaterialReturn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMaterialReturn.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvMaterialReturn.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvMaterialReturn.HasSlNo = false;
            this.dgvMaterialReturn.LastRowIndex = 0;
            this.dgvMaterialReturn.Location = new System.Drawing.Point(1, 1);
            this.dgvMaterialReturn.Name = "dgvMaterialReturn";
            this.dgvMaterialReturn.NegativeValueCols = new int[0];
            this.dgvMaterialReturn.NumericCols = new int[0];
            this.dgvMaterialReturn.RowHeadersWidth = 50;
            this.dgvMaterialReturn.Size = new System.Drawing.Size(1098, 250);
            this.dgvMaterialReturn.TabIndex = 0;
            this.dgvMaterialReturn.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialReturn_CellValueChanged);
            this.dgvMaterialReturn.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvMaterialReturn_CellBeginEdit);
            this.dgvMaterialReturn.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvMaterialReturn_UserDeletedRow);
            this.dgvMaterialReturn.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvMaterialReturn_RowsAdded);
            this.dgvMaterialReturn.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialReturn_CellEndEdit);
            this.dgvMaterialReturn.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvMaterialReturn_Textbox_TextChanged);
            this.dgvMaterialReturn.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvMaterialReturn_EditingControlShowing);
            this.dgvMaterialReturn.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvMaterialReturn_CurrentCellDirtyStateChanged);
            this.dgvMaterialReturn.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialReturn_CellEnter);
            this.dgvMaterialReturn.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvMaterialReturn_RowsRemoved);
            // 
            // tiItemDetails
            // 
            this.tiItemDetails.AttachedControl = this.tabControlPanel1;
            this.tiItemDetails.Name = "tiItemDetails";
            this.tiItemDetails.Text = "Material Details";
            // 
            // pnlBottom
            // 
            this.pnlBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottom.Controls.Add(this.pnlBottomRight);
            this.pnlBottom.Controls.Add(this.pnlMainBottom);
            this.pnlBottom.Controls.Add(this.txtRemarks);
            this.pnlBottom.Controls.Add(this.lblRemarks);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 274);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1100, 109);
            this.pnlBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottom.Style.GradientAngle = 90;
            this.pnlBottom.TabIndex = 3;
            // 
            // pnlBottomRight
            // 
            this.pnlBottomRight.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottomRight.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottomRight.Controls.Add(this.cboCurrency);
            this.pnlBottomRight.Controls.Add(this.lblCurrency);
            this.pnlBottomRight.Controls.Add(this.lblNetAmount);
            this.pnlBottomRight.Controls.Add(this.txtNetAmount);
            this.pnlBottomRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlBottomRight.Location = new System.Drawing.Point(743, 0);
            this.pnlBottomRight.Name = "pnlBottomRight";
            this.pnlBottomRight.Size = new System.Drawing.Size(357, 83);
            this.pnlBottomRight.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottomRight.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottomRight.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottomRight.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottomRight.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottomRight.Style.BorderSide = DevComponents.DotNetBar.eBorderSide.Top;
            this.pnlBottomRight.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottomRight.Style.GradientAngle = 90;
            this.pnlBottomRight.TabIndex = 293;
            // 
            // cboCurrency
            // 
            this.cboCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCurrency.DisplayMember = "Text";
            this.cboCurrency.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCurrency.DropDownHeight = 75;
            this.cboCurrency.Enabled = false;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.IntegralHeight = false;
            this.cboCurrency.ItemHeight = 14;
            this.cboCurrency.Location = new System.Drawing.Point(136, 11);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(221, 20);
            this.cboCurrency.TabIndex = 266;
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrency.Location = new System.Drawing.Point(11, 13);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(49, 13);
            this.lblCurrency.TabIndex = 267;
            this.lblCurrency.Text = "Currency";
            // 
            // lblNetAmount
            // 
            this.lblNetAmount.AutoSize = true;
            this.lblNetAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNetAmount.Location = new System.Drawing.Point(10, 39);
            this.lblNetAmount.Name = "lblNetAmount";
            this.lblNetAmount.Size = new System.Drawing.Size(120, 24);
            this.lblNetAmount.TabIndex = 263;
            this.lblNetAmount.Text = "Net Amount";
            // 
            // txtNetAmount
            // 
            // 
            // 
            // 
            this.txtNetAmount.Border.Class = "TextBoxBorder";
            this.txtNetAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtNetAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNetAmount.Location = new System.Drawing.Point(136, 34);
            this.txtNetAmount.MaxLength = 20;
            this.txtNetAmount.Name = "txtNetAmount";
            this.txtNetAmount.Size = new System.Drawing.Size(221, 29);
            this.txtNetAmount.TabIndex = 262;
            this.txtNetAmount.TabStop = false;
            this.txtNetAmount.Text = "0.00";
            this.txtNetAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNetAmount.WatermarkEnabled = false;
            // 
            // pnlMainBottom
            // 
            this.pnlMainBottom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlMainBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMainBottom.Controls.Add(this.lblCreatedByText);
            this.pnlMainBottom.Controls.Add(this.lblCreatedDateValue);
            this.pnlMainBottom.Controls.Add(this.lblStatus);
            this.pnlMainBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlMainBottom.Location = new System.Drawing.Point(0, 83);
            this.pnlMainBottom.Name = "pnlMainBottom";
            this.pnlMainBottom.Size = new System.Drawing.Size(1100, 26);
            this.pnlMainBottom.TabIndex = 292;
            // 
            // lblCreatedByText
            // 
            this.lblCreatedByText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedByText.CloseButtonVisible = false;
            this.lblCreatedByText.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblCreatedByText.Location = new System.Drawing.Point(718, 0);
            this.lblCreatedByText.Name = "lblCreatedByText";
            this.lblCreatedByText.OptionsButtonVisible = false;
            this.lblCreatedByText.Size = new System.Drawing.Size(380, 24);
            this.lblCreatedByText.TabIndex = 258;
            this.lblCreatedByText.Text = "Created By";
            // 
            // lblCreatedDateValue
            // 
            this.lblCreatedDateValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedDateValue.CloseButtonVisible = false;
            this.lblCreatedDateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCreatedDateValue.Location = new System.Drawing.Point(387, 0);
            this.lblCreatedDateValue.Name = "lblCreatedDateValue";
            this.lblCreatedDateValue.OptionsButtonVisible = false;
            this.lblCreatedDateValue.Size = new System.Drawing.Size(711, 24);
            this.lblCreatedDateValue.TabIndex = 256;
            this.lblCreatedDateValue.Text = "Created Date";
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblStatus.CloseButtonVisible = false;
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblStatus.Image = ((System.Drawing.Image)(resources.GetObject("lblStatus.Image")));
            this.lblStatus.Location = new System.Drawing.Point(0, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.OptionsButtonVisible = false;
            this.lblStatus.Size = new System.Drawing.Size(387, 24);
            this.lblStatus.TabIndex = 20;
            // 
            // txtRemarks
            // 
            // 
            // 
            // 
            this.txtRemarks.Border.Class = "TextBoxBorder";
            this.txtRemarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtRemarks.Border.WordWrap = true;
            this.txtRemarks.Location = new System.Drawing.Point(111, 11);
            this.txtRemarks.MaxLength = 2000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(347, 64);
            this.txtRemarks.TabIndex = 0;
            this.txtRemarks.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(29, 21);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 214;
            this.lblRemarks.Text = "Remarks";
            // 
            // expandableSplitterTop
            // 
            this.expandableSplitterTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterTop.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitterTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitterTop.ExpandableControl = this.panelTop;
            this.expandableSplitterTop.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterTop.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterTop.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterTop.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterTop.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.Location = new System.Drawing.Point(0, 128);
            this.expandableSplitterTop.Name = "expandableSplitterTop";
            this.expandableSplitterTop.Size = new System.Drawing.Size(1100, 3);
            this.expandableSplitterTop.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterTop.TabIndex = 1;
            this.expandableSplitterTop.TabStop = false;
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelTop.Controls.Add(this.tcGeneral);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1100, 103);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 0;
            // 
            // tcGeneral
            // 
            this.tcGeneral.BackColor = System.Drawing.Color.Transparent;
            this.tcGeneral.CanReorderTabs = true;
            this.tcGeneral.Controls.Add(this.tabControlPanel2);
            this.tcGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcGeneral.Location = new System.Drawing.Point(0, 0);
            this.tcGeneral.Name = "tcGeneral";
            this.tcGeneral.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcGeneral.SelectedTabIndex = 0;
            this.tcGeneral.Size = new System.Drawing.Size(1100, 103);
            this.tcGeneral.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcGeneral.TabIndex = 0;
            this.tcGeneral.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcGeneral.Tabs.Add(this.tiGeneral);
            this.tcGeneral.TabStop = false;
            this.tcGeneral.Text = "tabControl1";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.cboProject);
            this.tabControlPanel2.Controls.Add(this.lblProject);
            this.tabControlPanel2.Controls.Add(this.cboWarehouse);
            this.tabControlPanel2.Controls.Add(this.lblWarehouse);
            this.tabControlPanel2.Controls.Add(this.cboCompany);
            this.tabControlPanel2.Controls.Add(this.lblCompany);
            this.tabControlPanel2.Controls.Add(this.txtMaterialReturnNo);
            this.tabControlPanel2.Controls.Add(this.dtpReturnedDate);
            this.tabControlPanel2.Controls.Add(this.lblMaterialReturnDate);
            this.tabControlPanel2.Controls.Add(this.lblMaterialReturnNo);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(1100, 81);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 1;
            this.tabControlPanel2.TabItem = this.tiGeneral;
            // 
            // cboProject
            // 
            this.cboProject.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboProject.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboProject.DisplayMember = "Text";
            this.cboProject.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboProject.DropDownHeight = 75;
            this.cboProject.FormattingEnabled = true;
            this.cboProject.IntegralHeight = false;
            this.cboProject.ItemHeight = 14;
            this.cboProject.Location = new System.Drawing.Point(126, 57);
            this.cboProject.Name = "cboProject";
            this.cboProject.Size = new System.Drawing.Size(204, 20);
            this.cboProject.TabIndex = 2;
            this.cboProject.SelectedIndexChanged += new System.EventHandler(this.cboProject_SelectedIndexChanged);
            // 
            // lblProject
            // 
            this.lblProject.AutoSize = true;
            this.lblProject.BackColor = System.Drawing.Color.Transparent;
            this.lblProject.Location = new System.Drawing.Point(30, 57);
            this.lblProject.Name = "lblProject";
            this.lblProject.Size = new System.Drawing.Size(40, 13);
            this.lblProject.TabIndex = 234;
            this.lblProject.Text = "Project";
            // 
            // cboWarehouse
            // 
            this.cboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWarehouse.DisplayMember = "Text";
            this.cboWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWarehouse.DropDownHeight = 75;
            this.cboWarehouse.FormattingEnabled = true;
            this.cboWarehouse.IntegralHeight = false;
            this.cboWarehouse.ItemHeight = 14;
            this.cboWarehouse.Location = new System.Drawing.Point(126, 33);
            this.cboWarehouse.Name = "cboWarehouse";
            this.cboWarehouse.Size = new System.Drawing.Size(204, 20);
            this.cboWarehouse.TabIndex = 1;
            this.cboWarehouse.SelectedIndexChanged += new System.EventHandler(this.cboWarehouse_SelectedIndexChanged);
            this.cboWarehouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.BackColor = System.Drawing.Color.Transparent;
            this.lblWarehouse.Location = new System.Drawing.Point(29, 35);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(62, 13);
            this.lblWarehouse.TabIndex = 230;
            this.lblWarehouse.Text = "Warehouse";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 75;
            this.cboCompany.Enabled = false;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(126, 9);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(204, 20);
            this.cboCompany.TabIndex = 0;
            this.cboCompany.TabStop = false;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.BackColor = System.Drawing.Color.Transparent;
            this.lblCompany.Location = new System.Drawing.Point(29, 11);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 229;
            this.lblCompany.Text = "Company";
            // 
            // txtMaterialReturnNo
            // 
            this.txtMaterialReturnNo.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtMaterialReturnNo.Border.Class = "TextBoxBorder";
            this.txtMaterialReturnNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMaterialReturnNo.Location = new System.Drawing.Point(464, 8);
            this.txtMaterialReturnNo.MaxLength = 20;
            this.txtMaterialReturnNo.Name = "txtMaterialReturnNo";
            this.txtMaterialReturnNo.Size = new System.Drawing.Size(105, 20);
            this.txtMaterialReturnNo.TabIndex = 3;
            this.txtMaterialReturnNo.TabStop = false;
            this.txtMaterialReturnNo.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // dtpReturnedDate
            // 
            this.dtpReturnedDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpReturnedDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpReturnedDate.Location = new System.Drawing.Point(464, 33);
            this.dtpReturnedDate.Name = "dtpReturnedDate";
            this.dtpReturnedDate.Size = new System.Drawing.Size(105, 20);
            this.dtpReturnedDate.TabIndex = 4;
            this.dtpReturnedDate.ValueChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // lblMaterialReturnDate
            // 
            this.lblMaterialReturnDate.AutoSize = true;
            this.lblMaterialReturnDate.BackColor = System.Drawing.Color.Transparent;
            this.lblMaterialReturnDate.Location = new System.Drawing.Point(369, 38);
            this.lblMaterialReturnDate.Name = "lblMaterialReturnDate";
            this.lblMaterialReturnDate.Size = new System.Drawing.Size(65, 13);
            this.lblMaterialReturnDate.TabIndex = 226;
            this.lblMaterialReturnDate.Text = "Return Date";
            // 
            // lblMaterialReturnNo
            // 
            this.lblMaterialReturnNo.AutoSize = true;
            this.lblMaterialReturnNo.BackColor = System.Drawing.Color.Transparent;
            this.lblMaterialReturnNo.Location = new System.Drawing.Point(369, 12);
            this.lblMaterialReturnNo.Name = "lblMaterialReturnNo";
            this.lblMaterialReturnNo.Size = new System.Drawing.Size(96, 13);
            this.lblMaterialReturnNo.TabIndex = 225;
            this.lblMaterialReturnNo.Text = "Material Return No";
            // 
            // tiGeneral
            // 
            this.tiGeneral.AttachedControl = this.tabControlPanel2;
            this.tiGeneral.Name = "tiGeneral";
            this.tiGeneral.Text = "General";
            // 
            // bnMaterialReturn
            // 
            this.bnMaterialReturn.AccessibleDescription = "DotNetBar Bar (bnMaterialReturn)";
            this.bnMaterialReturn.AccessibleName = "DotNetBar Bar";
            this.bnMaterialReturn.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.bnMaterialReturn.Dock = System.Windows.Forms.DockStyle.Top;
            this.bnMaterialReturn.DockLine = 1;
            this.bnMaterialReturn.DockOffset = 73;
            this.bnMaterialReturn.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.bnMaterialReturn.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.bnMaterialReturn.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnClear,
            this.bnDelete,
            this.bnPrint,
            this.bnEmail});
            this.bnMaterialReturn.Location = new System.Drawing.Point(0, 0);
            this.bnMaterialReturn.Name = "bnMaterialReturn";
            this.bnMaterialReturn.Size = new System.Drawing.Size(1100, 25);
            this.bnMaterialReturn.Stretch = true;
            this.bnMaterialReturn.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.bnMaterialReturn.TabIndex = 12;
            this.bnMaterialReturn.TabStop = false;
            this.bnMaterialReturn.Text = "bar2";
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnAddNewItem.Image = global::MyBooksERP.Properties.Resources.Add;
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlEnter);
            this.bnAddNewItem.Text = "Add";
            this.bnAddNewItem.Tooltip = "Add New Information";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnSaveItem.Image = global::MyBooksERP.Properties.Resources.save;
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Text = "Save";
            this.bnSaveItem.Tooltip = "Save";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnClear
            // 
            this.bnClear.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.bnClear.Name = "bnClear";
            this.bnClear.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.bnClear.Text = "Clear";
            this.bnClear.Tooltip = "Clear";
            this.bnClear.Visible = false;
            this.bnClear.Click += new System.EventHandler(this.bnClear_Click);
            // 
            // bnDelete
            // 
            this.bnDelete.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnDelete.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.bnDelete.Name = "bnDelete";
            this.bnDelete.Text = "Delete";
            this.bnDelete.Tooltip = "Delete";
            this.bnDelete.Click += new System.EventHandler(this.bnDelete_Click);
            // 
            // bnPrint
            // 
            this.bnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.bnPrint.Text = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // bnEmail
            // 
            this.bnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.bnEmail.Text = "Email";
            this.bnEmail.Click += new System.EventHandler(this.bnEmail_Click);
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(0, 0);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 514);
            this.dockSite1.TabIndex = 26;
            this.dockSite1.TabStop = false;
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(200, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 514);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 25;
            this.expandableSplitterLeft.TabStop = false;
            this.expandableSplitterLeft.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitterLeft_ExpandedChanged);
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.dgvMaterialReturnDisplay);
            this.PanelLeft.Controls.Add(this.epMaterialReturn);
            this.PanelLeft.Controls.Add(this.lblSCountStatus);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(200, 514);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 4;
            this.PanelLeft.Text = "panelEx1";
            // 
            // dgvMaterialReturnDisplay
            // 
            this.dgvMaterialReturnDisplay.AllowUserToAddRows = false;
            this.dgvMaterialReturnDisplay.AllowUserToDeleteRows = false;
            this.dgvMaterialReturnDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMaterialReturnDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvMaterialReturnDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMaterialReturnDisplay.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvMaterialReturnDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMaterialReturnDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvMaterialReturnDisplay.Location = new System.Drawing.Point(0, 223);
            this.dgvMaterialReturnDisplay.Name = "dgvMaterialReturnDisplay";
            this.dgvMaterialReturnDisplay.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMaterialReturnDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvMaterialReturnDisplay.RowHeadersVisible = false;
            this.dgvMaterialReturnDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMaterialReturnDisplay.Size = new System.Drawing.Size(200, 265);
            this.dgvMaterialReturnDisplay.TabIndex = 7;
            this.dgvMaterialReturnDisplay.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialReturnDisplay_CellClick);
            // 
            // epMaterialReturn
            // 
            this.epMaterialReturn.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.epMaterialReturn.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.epMaterialReturn.Controls.Add(this.panelLeftTop);
            this.epMaterialReturn.Dock = System.Windows.Forms.DockStyle.Top;
            this.epMaterialReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.epMaterialReturn.Location = new System.Drawing.Point(0, 0);
            this.epMaterialReturn.Name = "epMaterialReturn";
            this.epMaterialReturn.Size = new System.Drawing.Size(200, 223);
            this.epMaterialReturn.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.epMaterialReturn.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.epMaterialReturn.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.epMaterialReturn.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.epMaterialReturn.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.epMaterialReturn.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.epMaterialReturn.Style.GradientAngle = 90;
            this.epMaterialReturn.TabIndex = 5;
            this.epMaterialReturn.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.epMaterialReturn.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.epMaterialReturn.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.epMaterialReturn.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.epMaterialReturn.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.epMaterialReturn.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.epMaterialReturn.TitleStyle.GradientAngle = 90;
            this.epMaterialReturn.TitleText = "Search";
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelLeftTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelLeftTop.Controls.Add(this.cboSProject);
            this.panelLeftTop.Controls.Add(this.lblSearchProject);
            this.panelLeftTop.Controls.Add(this.cboSCreatedBy);
            this.panelLeftTop.Controls.Add(this.lblSCreatedBy);
            this.panelLeftTop.Controls.Add(this.lblSMaterialReturnNo);
            this.panelLeftTop.Controls.Add(this.dtpSTo);
            this.panelLeftTop.Controls.Add(this.dtpSFrom);
            this.panelLeftTop.Controls.Add(this.lblTo);
            this.panelLeftTop.Controls.Add(this.lblFrom);
            this.panelLeftTop.Controls.Add(this.cboSWarehouse);
            this.panelLeftTop.Controls.Add(this.lblSWarehouse);
            this.panelLeftTop.Controls.Add(this.btnRefresh);
            this.panelLeftTop.Controls.Add(this.txtSearch);
            this.panelLeftTop.Controls.Add(this.cboSCompany);
            this.panelLeftTop.Controls.Add(this.lblSCompany);
            this.panelLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftTop.Location = new System.Drawing.Point(0, 26);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(200, 197);
            this.panelLeftTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelLeftTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelLeftTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelLeftTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelLeftTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelLeftTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelLeftTop.Style.GradientAngle = 90;
            this.panelLeftTop.TabIndex = 6;
            // 
            // cboSProject
            // 
            this.cboSProject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSProject.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSProject.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSProject.DisplayMember = "Text";
            this.cboSProject.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSProject.DropDownHeight = 75;
            this.cboSProject.FormattingEnabled = true;
            this.cboSProject.IntegralHeight = false;
            this.cboSProject.ItemHeight = 14;
            this.cboSProject.Location = new System.Drawing.Point(78, 56);
            this.cboSProject.Name = "cboSProject";
            this.cboSProject.Size = new System.Drawing.Size(117, 20);
            this.cboSProject.TabIndex = 2;
            // 
            // lblSearchProject
            // 
            this.lblSearchProject.AutoSize = true;
            this.lblSearchProject.Location = new System.Drawing.Point(3, 59);
            this.lblSearchProject.Name = "lblSearchProject";
            this.lblSearchProject.Size = new System.Drawing.Size(40, 13);
            this.lblSearchProject.TabIndex = 265;
            this.lblSearchProject.Text = "Project";
            // 
            // cboSCreatedBy
            // 
            this.cboSCreatedBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSCreatedBy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSCreatedBy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSCreatedBy.DisplayMember = "Text";
            this.cboSCreatedBy.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSCreatedBy.DropDownHeight = 75;
            this.cboSCreatedBy.FormattingEnabled = true;
            this.cboSCreatedBy.IntegralHeight = false;
            this.cboSCreatedBy.ItemHeight = 14;
            this.cboSCreatedBy.Location = new System.Drawing.Point(78, 78);
            this.cboSCreatedBy.Name = "cboSCreatedBy";
            this.cboSCreatedBy.Size = new System.Drawing.Size(117, 20);
            this.cboSCreatedBy.TabIndex = 3;
            this.cboSCreatedBy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblSCreatedBy
            // 
            this.lblSCreatedBy.AutoSize = true;
            this.lblSCreatedBy.Location = new System.Drawing.Point(4, 81);
            this.lblSCreatedBy.Name = "lblSCreatedBy";
            this.lblSCreatedBy.Size = new System.Drawing.Size(59, 13);
            this.lblSCreatedBy.TabIndex = 263;
            this.lblSCreatedBy.Text = "Created By";
            // 
            // lblSMaterialReturnNo
            // 
            this.lblSMaterialReturnNo.AutoSize = true;
            this.lblSMaterialReturnNo.Location = new System.Drawing.Point(3, 149);
            this.lblSMaterialReturnNo.Name = "lblSMaterialReturnNo";
            this.lblSMaterialReturnNo.Size = new System.Drawing.Size(56, 13);
            this.lblSMaterialReturnNo.TabIndex = 261;
            this.lblSMaterialReturnNo.Text = "Return No";
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(78, 123);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.Size = new System.Drawing.Size(117, 20);
            this.dtpSTo.TabIndex = 5;
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(78, 100);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.Size = new System.Drawing.Size(117, 20);
            this.dtpSFrom.TabIndex = 4;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(3, 127);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 258;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(3, 103);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 257;
            this.lblFrom.Text = "From";
            // 
            // cboSWarehouse
            // 
            this.cboSWarehouse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSWarehouse.DisplayMember = "Text";
            this.cboSWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSWarehouse.DropDownHeight = 75;
            this.cboSWarehouse.FormattingEnabled = true;
            this.cboSWarehouse.IntegralHeight = false;
            this.cboSWarehouse.ItemHeight = 14;
            this.cboSWarehouse.Location = new System.Drawing.Point(78, 33);
            this.cboSWarehouse.Name = "cboSWarehouse";
            this.cboSWarehouse.Size = new System.Drawing.Size(117, 20);
            this.cboSWarehouse.TabIndex = 1;
            this.cboSWarehouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblSWarehouse
            // 
            this.lblSWarehouse.AutoSize = true;
            this.lblSWarehouse.Location = new System.Drawing.Point(3, 36);
            this.lblSWarehouse.Name = "lblSWarehouse";
            this.lblSWarehouse.Size = new System.Drawing.Size(62, 13);
            this.lblSWarehouse.TabIndex = 125;
            this.lblSWarehouse.Text = "Warehouse";
            // 
            // btnRefresh
            // 
            this.btnRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnRefresh.Location = new System.Drawing.Point(124, 169);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(71, 23);
            this.btnRefresh.TabIndex = 7;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(78, 146);
            this.txtSearch.MaxLength = 20;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(117, 20);
            this.txtSearch.TabIndex = 6;
            this.txtSearch.WatermarkEnabled = false;
            this.txtSearch.WatermarkText = "Issue No";
            // 
            // cboSCompany
            // 
            this.cboSCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSCompany.DisplayMember = "Text";
            this.cboSCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSCompany.DropDownHeight = 75;
            this.cboSCompany.FormattingEnabled = true;
            this.cboSCompany.IntegralHeight = false;
            this.cboSCompany.ItemHeight = 14;
            this.cboSCompany.Location = new System.Drawing.Point(78, 10);
            this.cboSCompany.Name = "cboSCompany";
            this.cboSCompany.Size = new System.Drawing.Size(117, 20);
            this.cboSCompany.TabIndex = 0;
            this.cboSCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblSCompany
            // 
            this.lblSCompany.AutoSize = true;
            this.lblSCompany.Location = new System.Drawing.Point(3, 13);
            this.lblSCompany.Name = "lblSCompany";
            this.lblSCompany.Size = new System.Drawing.Size(51, 13);
            this.lblSCompany.TabIndex = 109;
            this.lblSCompany.Text = "Company";
            // 
            // lblSCountStatus
            // 
            // 
            // 
            // 
            this.lblSCountStatus.BackgroundStyle.Class = "";
            this.lblSCountStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSCountStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblSCountStatus.Location = new System.Drawing.Point(0, 488);
            this.lblSCountStatus.Name = "lblSCountStatus";
            this.lblSCountStatus.Size = new System.Drawing.Size(200, 26);
            this.lblSCountStatus.TabIndex = 8;
            this.lblSCountStatus.Text = "...";
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(0, 0);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 514);
            this.dockSite5.TabIndex = 27;
            this.dockSite5.TabStop = false;
            // 
            // tmrMaterialReturn
            // 
            this.tmrMaterialReturn.Tick += new System.EventHandler(this.tmrMaterialReturn_Tick);
            // 
            // ErrMaterialReturn
            // 
            this.ErrMaterialReturn.ContainerControl = this;
            this.ErrMaterialReturn.RightToLeft = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 20;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "Item Name";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "BatchID";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Batch No";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 175;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn6.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn6.MaxInputLength = 12;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 110;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn7.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn7.MaxInputLength = 12;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 110;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn8.HeaderText = "Net Amount";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 125;
            // 
            // dgvColItemCode
            // 
            this.dgvColItemCode.HeaderText = "Item Code";
            this.dgvColItemCode.MaxInputLength = 20;
            this.dgvColItemCode.Name = "dgvColItemCode";
            this.dgvColItemCode.Width = 80;
            // 
            // dgvColItemName
            // 
            this.dgvColItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvColItemName.HeaderText = "Item Name";
            this.dgvColItemName.MaxInputLength = 100;
            this.dgvColItemName.Name = "dgvColItemName";
            // 
            // dgvColItemID
            // 
            this.dgvColItemID.HeaderText = "ItemID";
            this.dgvColItemID.Name = "dgvColItemID";
            this.dgvColItemID.Visible = false;
            // 
            // dgvColBatchID
            // 
            this.dgvColBatchID.HeaderText = "BatchID";
            this.dgvColBatchID.Name = "dgvColBatchID";
            this.dgvColBatchID.Visible = false;
            // 
            // dgvColBatchNo
            // 
            this.dgvColBatchNo.HeaderText = "Batch No";
            this.dgvColBatchNo.Name = "dgvColBatchNo";
            this.dgvColBatchNo.ReadOnly = true;
            this.dgvColBatchNo.Width = 150;
            // 
            // dgvColIssuedQty
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColIssuedQty.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvColIssuedQty.HeaderText = "Issued Qty";
            this.dgvColIssuedQty.Name = "dgvColIssuedQty";
            this.dgvColIssuedQty.ReadOnly = true;
            this.dgvColIssuedQty.Width = 90;
            // 
            // dgvColReturnedQty
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColReturnedQty.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvColReturnedQty.HeaderText = "Returned Qty";
            this.dgvColReturnedQty.Name = "dgvColReturnedQty";
            this.dgvColReturnedQty.ReadOnly = true;
            this.dgvColReturnedQty.Width = 90;
            // 
            // dgvColQuantity
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColQuantity.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvColQuantity.HeaderText = "Quantity";
            this.dgvColQuantity.MaxInputLength = 12;
            this.dgvColQuantity.Name = "dgvColQuantity";
            this.dgvColQuantity.Width = 90;
            // 
            // dgvColUOM
            // 
            this.dgvColUOM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColUOM.HeaderText = "UOM";
            this.dgvColUOM.Name = "dgvColUOM";
            this.dgvColUOM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvColUOM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dgvColUOM.Width = 75;
            // 
            // dgvColTempIssuedQty
            // 
            this.dgvColTempIssuedQty.HeaderText = "IssuedQty";
            this.dgvColTempIssuedQty.Name = "dgvColTempIssuedQty";
            this.dgvColTempIssuedQty.Visible = false;
            // 
            // dgvColIssuedUomID
            // 
            this.dgvColIssuedUomID.HeaderText = "Issued UomID";
            this.dgvColIssuedUomID.Name = "dgvColIssuedUomID";
            this.dgvColIssuedUomID.Visible = false;
            // 
            // dgvColRate
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColRate.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvColRate.HeaderText = "Rate";
            this.dgvColRate.Name = "dgvColRate";
            this.dgvColRate.ReadOnly = true;
            this.dgvColRate.Width = 90;
            // 
            // dgvColIssuedRate
            // 
            this.dgvColIssuedRate.HeaderText = "IssuedRate";
            this.dgvColIssuedRate.Name = "dgvColIssuedRate";
            this.dgvColIssuedRate.ReadOnly = true;
            this.dgvColIssuedRate.Visible = false;
            // 
            // dgvColNetAmount
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColNetAmount.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvColNetAmount.HeaderText = "Net Amount";
            this.dgvColNetAmount.Name = "dgvColNetAmount";
            this.dgvColNetAmount.ReadOnly = true;
            // 
            // FrmMaterialReturn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1303, 514);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.expandableSplitterLeft);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.dockSite5);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMaterialReturn";
            this.Text = "Material Return";
            this.Load += new System.EventHandler(this.FrmMaterialReturn_Load);
            this.panelMain.ResumeLayout(false);
            this.panelMiddle.ResumeLayout(false);
            this.paneltcMaterialReturn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcMaterialReturn)).EndInit();
            this.tcMaterialReturn.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialReturn)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.pnlBottomRight.ResumeLayout(false);
            this.pnlBottomRight.PerformLayout();
            this.pnlMainBottom.ResumeLayout(false);
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).EndInit();
            this.tcGeneral.ResumeLayout(false);
            this.tabControlPanel2.ResumeLayout(false);
            this.tabControlPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnMaterialReturn)).EndInit();
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialReturnDisplay)).EndInit();
            this.epMaterialReturn.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrMaterialReturn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx panelMain;
        private DevComponents.DotNetBar.PanelEx panelMiddle;
        private DevComponents.DotNetBar.TabControl tcMaterialReturn;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private DevComponents.DotNetBar.TabItem tiItemDetails;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterTop;
        private DevComponents.DotNetBar.PanelEx panelTop;
        private DevComponents.DotNetBar.Bar bnMaterialReturn;
        private DevComponents.DotNetBar.ButtonItem bnAddNewItem;
        private DevComponents.DotNetBar.ButtonItem bnSaveItem;
        private DevComponents.DotNetBar.ButtonItem bnClear;
        private DevComponents.DotNetBar.ButtonItem bnDelete;
        private DevComponents.DotNetBar.ButtonItem bnPrint;
        private DevComponents.DotNetBar.ButtonItem bnEmail;
        private DevComponents.DotNetBar.DockSite dockSite1;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterLeft;
        private DevComponents.DotNetBar.PanelEx PanelLeft;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvMaterialReturnDisplay;
        private DevComponents.DotNetBar.ExpandablePanel epMaterialReturn;
        private DevComponents.DotNetBar.PanelEx panelLeftTop;
        private System.Windows.Forms.Label lblSMaterialReturnNo;
        private System.Windows.Forms.DateTimePicker dtpSTo;
        private System.Windows.Forms.DateTimePicker dtpSFrom;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSWarehouse;
        private System.Windows.Forms.Label lblSWarehouse;
        private DevComponents.DotNetBar.ButtonX btnRefresh;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSCompany;
        private System.Windows.Forms.Label lblSCompany;
        private DevComponents.DotNetBar.LabelX lblSCountStatus;
        private DevComponents.DotNetBar.DockSite dockSite5;
        private DevComponents.DotNetBar.TabControl tcGeneral;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboWarehouse;
        private System.Windows.Forms.Label lblWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private System.Windows.Forms.Label lblCompany;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMaterialReturnNo;
        private System.Windows.Forms.DateTimePicker dtpReturnedDate;
        private System.Windows.Forms.Label lblMaterialReturnDate;
        private System.Windows.Forms.Label lblMaterialReturnNo;
        private DevComponents.DotNetBar.TabItem tiGeneral;
        private System.Windows.Forms.Timer tmrMaterialReturn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private ClsInnerGridBar dgvMaterialReturn;
        internal System.Windows.Forms.ErrorProvider ErrMaterialReturn;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSCreatedBy;
        private System.Windows.Forms.Label lblSCreatedBy;
        private DevComponents.DotNetBar.PanelEx paneltcMaterialReturn;
        private DevComponents.DotNetBar.PanelEx pnlBottom;
        private DevComponents.DotNetBar.PanelEx pnlBottomRight;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCurrency;
        private System.Windows.Forms.Label lblCurrency;
        private System.Windows.Forms.Label lblNetAmount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtNetAmount;
        private System.Windows.Forms.Panel pnlMainBottom;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedByText;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedDateValue;
        private DevComponents.DotNetBar.Controls.WarningBox lblStatus;
        private DevComponents.DotNetBar.Controls.TextBoxX txtRemarks;
        private System.Windows.Forms.Label lblRemarks;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboProject;
        private System.Windows.Forms.Label lblProject;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSProject;
        private System.Windows.Forms.Label lblSearchProject;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColBatchID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColBatchNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColIssuedQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColReturnedQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColQuantity;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColTempIssuedQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColIssuedUomID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColIssuedRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColNetAmount;
    }
}