﻿namespace MyBooksERP
{
    partial class frmCreateFeatures
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCreateFeatures));
            this.barStatus = new DevComponents.DotNetBar.Bar();
            this.lblSBStatus = new DevComponents.DotNetBar.LabelItem();
            this.lblStatusShow = new DevComponents.DotNetBar.LabelItem();
            this.panelEx3 = new DevComponents.DotNetBar.PanelEx();
            this.dgvValues = new ClsDataGirdViewX();
            this.cboFeatureValues = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.clmDefault = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnAddToList = new DevComponents.DotNetBar.ButtonX();
            this.btnAddFeature = new DevComponents.DotNetBar.ButtonX();
            this.cboFeatures = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.OpeningStockBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BtnFeatureValues = new System.Windows.Forms.ToolStripButton();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barStatus)).BeginInit();
            this.panelEx3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OpeningStockBindingNavigator)).BeginInit();
            this.OpeningStockBindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // barStatus
            // 
            this.barStatus.AccessibleDescription = "barStatus (barStatus)";
            this.barStatus.AccessibleName = "barStatus";
            this.barStatus.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.barStatus.AntiAlias = true;
            this.barStatus.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.barStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barStatus.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lblSBStatus,
            this.lblStatusShow});
            this.barStatus.Location = new System.Drawing.Point(0, 285);
            this.barStatus.Name = "barStatus";
            this.barStatus.Size = new System.Drawing.Size(299, 19);
            this.barStatus.Stretch = true;
            this.barStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barStatus.TabIndex = 43;
            this.barStatus.TabStop = false;
            // 
            // lblSBStatus
            // 
            this.lblSBStatus.Name = "lblSBStatus";
            this.lblSBStatus.Text = "Status : ";
            // 
            // lblStatusShow
            // 
            this.lblStatusShow.Name = "lblStatusShow";
            // 
            // panelEx3
            // 
            this.panelEx3.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx3.Controls.Add(this.dgvValues);
            this.panelEx3.Controls.Add(this.btnAddToList);
            this.panelEx3.Controls.Add(this.btnAddFeature);
            this.panelEx3.Controls.Add(this.cboFeatures);
            this.panelEx3.Controls.Add(this.labelX1);
            this.panelEx3.Location = new System.Drawing.Point(0, 25);
            this.panelEx3.Name = "panelEx3";
            this.panelEx3.Size = new System.Drawing.Size(300, 260);
            this.panelEx3.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx3.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx3.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx3.Style.GradientAngle = 90;
            this.panelEx3.TabIndex = 46;
            // 
            // dgvValues
            // 
            this.dgvValues.AddNewRow = false;
            this.dgvValues.AlphaNumericCols = new int[0];
            this.dgvValues.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvValues.CapsLockCols = new int[0];
            this.dgvValues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvValues.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cboFeatureValues,
            this.clmDefault});
            this.dgvValues.DecimalCols = new int[0];
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvValues.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvValues.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvValues.HasSlNo = false;
            this.dgvValues.LastRowIndex = 0;
            this.dgvValues.Location = new System.Drawing.Point(8, 35);
            this.dgvValues.Name = "dgvValues";
            this.dgvValues.NegativeValueCols = new int[0];
            this.dgvValues.NumericCols = new int[0];
            this.dgvValues.RowHeadersWidth = 25;
            this.dgvValues.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvValues.Size = new System.Drawing.Size(285, 193);
            this.dgvValues.TabIndex = 7;
            this.dgvValues.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvValues_CellEndEdit);
            this.dgvValues.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvValues_DataError);
            this.dgvValues.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvValues_CellContentClick);
            // 
            // cboFeatureValues
            // 
            this.cboFeatureValues.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cboFeatureValues.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboFeatureValues.HeaderText = "Values";
            this.cboFeatureValues.Name = "cboFeatureValues";
            this.cboFeatureValues.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cboFeatureValues.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // clmDefault
            // 
            this.clmDefault.HeaderText = "Default";
            this.clmDefault.Name = "clmDefault";
            this.clmDefault.Width = 45;
            // 
            // btnAddToList
            // 
            this.btnAddToList.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddToList.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddToList.Location = new System.Drawing.Point(231, 234);
            this.btnAddToList.Name = "btnAddToList";
            this.btnAddToList.Size = new System.Drawing.Size(61, 20);
            this.btnAddToList.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddToList.TabIndex = 6;
            this.btnAddToList.Text = "Save";
            this.btnAddToList.Click += new System.EventHandler(this.btnAddToList_Click);
            // 
            // btnAddFeature
            // 
            this.btnAddFeature.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddFeature.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddFeature.Location = new System.Drawing.Point(268, 6);
            this.btnAddFeature.Name = "btnAddFeature";
            this.btnAddFeature.Size = new System.Drawing.Size(22, 20);
            this.btnAddFeature.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddFeature.TabIndex = 5;
            this.btnAddFeature.Text = "...";
            this.btnAddFeature.Click += new System.EventHandler(this.btnAddFeature_Click);
            // 
            // cboFeatures
            // 
            this.cboFeatures.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFeatures.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFeatures.DisplayMember = "Text";
            this.cboFeatures.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboFeatures.ItemHeight = 14;
            this.cboFeatures.Location = new System.Drawing.Point(48, 6);
            this.cboFeatures.Name = "cboFeatures";
            this.cboFeatures.Size = new System.Drawing.Size(214, 20);
            this.cboFeatures.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboFeatures.TabIndex = 4;
            this.cboFeatures.SelectedIndexChanged += new System.EventHandler(this.cboFeatures_SelectedIndexChanged);
            this.cboFeatures.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboFeatures_KeyDown);
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(5, 5);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(44, 23);
            this.labelX1.TabIndex = 3;
            this.labelX1.Text = "Feature";
            // 
            // OpeningStockBindingNavigator
            // 
            this.OpeningStockBindingNavigator.AddNewItem = null;
            this.OpeningStockBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.OpeningStockBindingNavigator.CountItem = null;
            this.OpeningStockBindingNavigator.DeleteItem = null;
            this.OpeningStockBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnFeatureValues});
            this.OpeningStockBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.OpeningStockBindingNavigator.MoveFirstItem = null;
            this.OpeningStockBindingNavigator.MoveLastItem = null;
            this.OpeningStockBindingNavigator.MoveNextItem = null;
            this.OpeningStockBindingNavigator.MovePreviousItem = null;
            this.OpeningStockBindingNavigator.Name = "OpeningStockBindingNavigator";
            this.OpeningStockBindingNavigator.PositionItem = null;
            this.OpeningStockBindingNavigator.Size = new System.Drawing.Size(299, 25);
            this.OpeningStockBindingNavigator.TabIndex = 47;
            this.OpeningStockBindingNavigator.Text = "bindingNavigator1";
            // 
            // BtnFeatureValues
            // 
            this.BtnFeatureValues.Image = global::MyBooksERP.Properties.Resources.addFeatureTypes;
            this.BtnFeatureValues.Name = "BtnFeatureValues";
            this.BtnFeatureValues.RightToLeftAutoMirrorImage = true;
            this.BtnFeatureValues.Size = new System.Drawing.Size(23, 22);
            this.BtnFeatureValues.ToolTipText = "Add new Item";
            this.BtnFeatureValues.Click += new System.EventHandler(this.BtnFeatureValues_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Values";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // frmCreateFeatures
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 304);
            this.Controls.Add(this.OpeningStockBindingNavigator);
            this.Controls.Add(this.panelEx3);
            this.Controls.Add(this.barStatus);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCreateFeatures";
            this.Text = "Add Features";
            this.Load += new System.EventHandler(this.frmCreateFeatures_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barStatus)).EndInit();
            this.panelEx3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OpeningStockBindingNavigator)).EndInit();
            this.OpeningStockBindingNavigator.ResumeLayout(false);
            this.OpeningStockBindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.Bar barStatus;
        private DevComponents.DotNetBar.LabelItem lblSBStatus;
        private DevComponents.DotNetBar.LabelItem lblStatusShow;
        private DevComponents.DotNetBar.PanelEx panelEx3;
        private DevComponents.DotNetBar.ButtonX btnAddFeature;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboFeatures;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX btnAddToList;
        private ClsDataGirdViewX dgvValues;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingNavigator OpeningStockBindingNavigator;
        internal System.Windows.Forms.ToolStripButton BtnFeatureValues;
        private System.Windows.Forms.DataGridViewComboBoxColumn cboFeatureValues;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clmDefault;
    }
}