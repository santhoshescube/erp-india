﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;



namespace MyBooksERP
{
    public partial class frmItemGroupMaster : Form
    {
        #region Private Variables

        bool MblnShowErrorMess = false;   // Checking whether error messages are showing or not

        int MintRecordCount, MintRecordPosition;

        string MstrCommonMessage;

        DataTable datMessages, datItemDetails;

        MessageBoxIcon MmsgMessageIcon;

        clsBLLItemGroup MobjClsBLLItemGroup;
        ClsNotificationNew MobjClsNotification;
        ClsLogWriter MobjClsLogWriter;
        public Int64 PintProductGroupID;

        //private ArrayList MsarMessageArr;                  // Error Message display
        private bool MblnPrintEmailPermission = false;    // Set Print and Email Permission
        private bool MblnAddPermission = false;           //Set Add Permission
        private bool MblnUpdatePermission = false;        //Set Update Permission
        private bool MblnDeletePermission = false;        //Set Delete Permission
        private bool MblnAddUpdatePermission = false;     //Set Add Update Permission
        private bool MblnCancelPermission = true;        // Set Cancel Permission
        private bool MblnAddMode = true; 
        ClsCommonUtility MobjCommonUtility = new ClsCommonUtility();
        private string strItemgroupCode; //Generate Itemgroupcode

        #endregion

        public frmItemGroupMaster()
        {
            InitializeComponent();

            MobjClsBLLItemGroup = new clsBLLItemGroup();
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotificationNew();

            tmItemGroups.Interval = ClsCommonSettings.TimerInterval;
            MblnShowErrorMess = true;
            strItemgroupCode = ClsCommonSettings.strProductGroupCodePrefix;
            if (ClsMainSettings.BOQEnabled)
            {
                this.Text = "BOQ";
               
            }
           txtBatch.Visible= dtpDate.Visible = lblDate.Visible = ClsMainSettings.BOQEnabled;
        }

        private void FormLoad(object sender, EventArgs e)
        {
            SetPermissions();
            LoadMessage();
            LoadCombo(0);
            ResetForm();
            if (PintProductGroupID.ToInt64() > 0)
            {
                DataTable datRownumber = MobjClsBLLItemGroup.GetItemGroupRowNumber(PintProductGroupID);
                try
                {
                    if (datRownumber.Rows.Count > 0)
                    {
                        if (datRownumber.Rows[0]["RowNumber"].ToInt32() > 0)
                        {
                            MintRecordPosition = datRownumber.Rows[0]["RowNumber"].ToInt32();
                            DisplayItemGroup();
                        }
                    }
                }
                catch (Exception Ex)
                {
                }
            }
        }

        /// <summary>
        /// Loading the variables for messages
        /// </summary>
        private void LoadMessage()
        {
            datMessages = new DataTable();
            datMessages = MobjClsNotification.FillMessageArray((int)FormID.ItemMaster, 4);
        }

        private bool LoadCombo(int intType)
        {
            try
            {
                DataTable datCombos = null;

                if (intType == 0 || intType == 1)    // Item Category filling
                {
                    datCombos = MobjClsBLLItemGroup.FillCombos(new string[] { "CategoryID, CategoryName", "InvCategoryReference", "" });
                    cboCategory.DataSource = datCombos;
                    cboCategory.ValueMember = "CategoryID";
                    cboCategory.DisplayMember = "CategoryName";
                }

                if (intType == 0 || intType == 2) // Item sub category
                {
                    datCombos = null;
                    cboSubCategory.Text = string.Empty;
                    datCombos = MobjClsBLLItemGroup.FillCombos(new string[] { "SubCategoryID, SubCategoryName", "InvSubCategoryReference", "CategoryID=" + Convert.ToInt32(cboCategory.SelectedValue) });
                    cboSubCategory.DisplayMember = "SubCategoryName";
                    cboSubCategory.ValueMember = "SubCategoryID";
                    cboSubCategory.DataSource = datCombos;
                }

                if (intType == 0 || intType == 3) // Base UOM
                {
                    datCombos = MobjClsBLLItemGroup.FillCombos(new string[] { "UOMID, UOMName", "InvUOMReference", "" });
                    cboBaseUom.DisplayMember = "UOMName";
                    cboBaseUom.ValueMember = "UOMID";
                    cboBaseUom.DataSource = datCombos;
                }

                if (intType == 0 || intType == 4) // Costing Method
                {
                    datCombos = MobjClsBLLItemGroup.FillCombos(new string[] { "CostingMethodID, CostingMethod", "InvCostingMethodReference", "CostingMethodID >" + (int)CostingMethodReference.Batch + "" });
                    cboCostingMethod.DisplayMember = "CostingMethod";
                    cboCostingMethod.ValueMember = "CostingMethodID";
                    cboCostingMethod.DataSource = datCombos;
                }
                if (intType == 0 || intType == 5) // Pricing Scheme
                {
                    //datCombos = MobjClsBLLItemMaster.FillCombos(new string[] { "PricingSchemeID, PricingSchemeName", "InvPricingSchemeReference", "" });

                    cboPricingScheme.Text = "";

                    datCombos = MobjClsBLLItemGroup.FillCombos(new string[] { "PricingSchemeID, PricingSchemeName", "InvPricingSchemeReference","CurrencyID IN (0," + 
                        "(SELECT CurrencyId FROM CompanyMaster WHERE CompanyID="+ClsCommonSettings.CompanyID+"))" });
                    cboPricingScheme.DisplayMember = "PricingSchemeName";
                    cboPricingScheme.ValueMember = "PricingSchemeID";
                    cboPricingScheme.DataSource = datCombos;

                }
                //if (intType == 0 || intType == 1) // Search SubCategory
                //{
                //    datCombos = MobjClsBLLItemGroup.FillCombos(new string[] { "StatusID, Status", "CommonStatusReference", "OperationTypeID=35" });
                //    cboStatus.DisplayMember = "Status";
                //    cboStatus.ValueMember = "StatusID";
                //    cboStatus.DataSource = datCombos;
                //}
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        private void ResetForm()
        {
            try
            {
                txtDescription.Text = txtShortName.Text = "";
                txtActualAmount.Clear();
                txtSaleAmount.Clear();
                txtLabourCost.Clear();
                txtLabourCost.Clear();
                txtMachineCost.Clear();
                txtGroupCode.Tag = "0";
                txtBatch.Clear();
                dtpDate.Value = ClsCommonSettings.GetServerDate();
                dgvItem.Rows.Clear();
                dgvItem.ReadOnly = false;

                MintRecordCount = MobjClsBLLItemGroup.GetItemGroupCount();
                MintRecordPosition = MintRecordCount + 1;
                bnCountItem.Text = " of " + (MintRecordCount + 1).ToString();
                bnPositionItem.Text = (MintRecordCount + 1).ToString();
                GenerateItemGroupCode();
                btnOk.Enabled = false;
                btnSave.Enabled = false;
                bnAddNewItem.Enabled = false;
                bnDeleteItem.Enabled = false;
                bnEmail.Enabled = false;
                bnPrint.Enabled = false;
                bnSaveItem.Enabled = false;


                txtGroupCode.Focus();
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void GenerateItemGroupCode()
        {
            try
            {
                txtGroupCode.Text = strItemgroupCode + (MobjClsBLLItemGroup.GenerateItemGroupCode() + 1).ToString();

                if (ClsCommonSettings.blnProductGroupCodeAutogenerate)
                    txtGroupCode.ReadOnly = true;
                else
                    txtGroupCode.ReadOnly = false;
                //LblSCountStatus.Text = "Total Quotations " + MobjClsBLLPurchase.GetLastPurchaseNo(1, false);
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in GenerateItemGroupCode() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in GenerateItemGroupCode) " + ex.Message, 2);
            }
        }

        private void AddNewItem(object sender, EventArgs e)
        {
            bnDeleteItem.Enabled = bnSaveItem.Enabled = btnOk.Enabled = false;
            MblnAddMode = true;
            ResetForm();
        }

        private void SaveItem(object sender, EventArgs e)
        {
            try
            {
                if (ValidateFields())
                {
                    if (CheckDuplicateName())
                    {
                        if (SaveItemGroupInfo())
                            ResetForm();
                        else
                            return;

                        if (sender.GetType() == typeof(Button))
                        {
                            if (((Button)sender).Name == btnOk.Name)
                                Close();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private bool SaveItemGroupInfo()
        {
            MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (Convert.ToInt32(txtGroupCode.Tag) == 0 ? 1622 : 1623),
                out MmsgMessageIcon);
            if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                MessageBoxIcon.Information) == DialogResult.No)
                return false;


            if ((MobjClsBLLItemGroup.objClsDTOItemGroup.lngItemGroupID == 0) && txtGroupCode.ReadOnly && (MobjClsBLLItemGroup.CheckDupliateGroupCodeName(txtGroupCode.Text.Trim(),MobjClsBLLItemGroup.objClsDTOItemGroup.lngItemGroupID) != 0))
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 9002, out MmsgMessageIcon);
                MstrCommonMessage = MstrCommonMessage.Replace("* no", "Item Group Code");
                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return false;
                else
                    GenerateItemGroupCode();
            }

            MobjClsBLLItemGroup.objClsDTOItemGroup.decActualAmount = txtActualAmount.Text.Trim().Length > 0 ? Convert.ToDecimal(txtActualAmount.Text) : 0;
            MobjClsBLLItemGroup.objClsDTOItemGroup.decLabourCost = txtLabourCost.Text.ToDecimal();
            MobjClsBLLItemGroup.objClsDTOItemGroup.decSaleAmount = Convert.ToDecimal(txtSaleAmount.Text);
            MobjClsBLLItemGroup.objClsDTOItemGroup.intCreatedBy = 1;
            MobjClsBLLItemGroup.objClsDTOItemGroup.lngItemGroupID = Convert.ToInt32(txtGroupCode.Tag);
            MobjClsBLLItemGroup.objClsDTOItemGroup.intItemGroupStatusID = (int)ItemGroupStatus.Active;
            MobjClsBLLItemGroup.objClsDTOItemGroup.strItemGroupCode = txtGroupCode.Text.Trim();
            MobjClsBLLItemGroup.objClsDTOItemGroup.strDescription = txtDescription.Text.Trim();
            MobjClsBLLItemGroup.objClsDTOItemGroup.intCategoryID =Convert.ToInt32(cboCategory.SelectedValue);
            MobjClsBLLItemGroup.objClsDTOItemGroup.intSubCategoryID = Convert.ToInt32(cboSubCategory.SelectedValue);
            MobjClsBLLItemGroup.objClsDTOItemGroup.intBaseUomID = Convert.ToInt32(cboBaseUom.SelectedValue);
            MobjClsBLLItemGroup.objClsDTOItemGroup.intCostingMethodID = Convert.ToInt32(cboCostingMethod.SelectedValue);
            MobjClsBLLItemGroup.objClsDTOItemGroup.intPricingSchemeID = Convert.ToInt32(cboPricingScheme.SelectedValue);
            MobjClsBLLItemGroup.objClsDTOItemGroup.strShortName = txtShortName.Text.Trim();
            MobjClsBLLItemGroup.objClsDTOItemGroup.decMachineCost = txtMachineCost.Text.ToDecimal();
            MobjClsBLLItemGroup.objClsDTOItemGroup.strBatch = txtBatch.Text.Trim();
            MobjClsBLLItemGroup.objClsDTOItemGroup.strBatchDate = dtpDate.Value.ToString("dd MMM yyyy");

            SetItemGroupDetails();
            return MobjClsBLLItemGroup.SaveItemGroup();
        }

        private void SetItemGroupDetails()
        {
            MobjClsBLLItemGroup.objClsDTOItemGroup.objclsDTOItemGroupDetails = new List<clsDTOItemGroupDetails>();
            foreach (DataGridViewRow dgvrow in dgvItem.Rows)
            {
                if (Convert.ToInt32(dgvrow.Cells[dgvColItemID.Index].Value) > 0 &&
                    dgvrow.Cells[dgvColQuantity.Index].Value.ToDecimal() > 0)
                {
                    clsDTOItemGroupDetails objItemGroupDetails = new clsDTOItemGroupDetails();
                    objItemGroupDetails.lngItemID = dgvrow.Cells[dgvColItemID.Index].Value.ToInt32();
                    objItemGroupDetails.decQuantity = dgvrow.Cells[dgvColQuantity.Index].Value.ToDecimal();
                    objItemGroupDetails.intUOMID = dgvrow.Cells[dgvColUom.Index].Value.ToInt32();
                    objItemGroupDetails.intSerialNo = dgvrow.Cells[dgvColSerialNo.Index].Value.ToInt32();
                    objItemGroupDetails.decRate = dgvrow.Cells[dgvColItemRate.Index].Value.ToDecimal();
                    objItemGroupDetails.decItemTotal = dgvrow.Cells[dgvColItemRate.Index].Value.ToDecimal() * dgvrow.Cells[dgvColQuantity.Index].Value.ToDecimal();  

                    MobjClsBLLItemGroup.objClsDTOItemGroup.objclsDTOItemGroupDetails.Add(objItemGroupDetails);
                }
            }
        }

        private bool ValidateFields()
        {
            bool blnValid = true;
            Control cntrlFocus = null;

            errItemGroups.Clear();

            if (txtGroupCode.Text.Trim().Length == 0)
            {
                //"Please enter Group Code";
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1635, out MmsgMessageIcon);
                if (ClsMainSettings.BOQEnabled)
                    MstrCommonMessage.Replace("Group", "BOQ");
                cntrlFocus = txtGroupCode;
                blnValid = false;
            }
            else if (!txtGroupCode.ReadOnly && MobjClsBLLItemGroup.FillCombos(new string[] { "ItemGroupCode", "InvItemGroupMaster", "ItemGroupCode = '" + txtGroupCode.Text + "' And ItemGroupID <> " + MobjClsBLLItemGroup.objClsDTOItemGroup.lngItemGroupID + "" }).Rows.Count > 0)
            {
                //"Duplicate item group code
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1624, out MmsgMessageIcon);
                cntrlFocus = txtGroupCode;
                blnValid = false;
            }
            else if (txtShortName.Text.Trim().Length == 0)
            {
                //"Please enter Group Name";
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1636, out MmsgMessageIcon);
                if (ClsMainSettings.BOQEnabled)
                    MstrCommonMessage.Replace("Group", "BOQ");
                cntrlFocus = txtShortName;
                blnValid = false;
            }
           
            //else if (Convert.ToInt32(cboCategory.SelectedValue)<=0)
            //{
            //    //"Please select Category";
            //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1604, out MmsgMessageIcon);
            //    cntrlFocus = cboCategory;
            //    blnValid = false;
            //}
            //else if (Convert.ToInt32(cboSubCategory.SelectedValue) == 0)   // Validating item Subcategory
            //{
            //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1630, out MmsgMessageIcon);
            //    cntrlFocus = cboSubCategory;
            //    blnValid = false;
            //}
            //else if (Convert.ToInt32(cboBaseUom.SelectedValue) == 0)    // Validating base uom
            //{
            //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1615, out MmsgMessageIcon);
            //    cntrlFocus = cboBaseUom;
            //    blnValid = false;
            //}
            //if (cboCostingMethod.SelectedValue.ToInt32() == 0)
            //{
            //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1657, out MmsgMessageIcon);
            //    cntrlFocus = cboCostingMethod;
            //    blnValid = false;
            //}
            //if (cboPricingScheme.SelectedValue.ToInt32() == 0)
            //{
            //    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1658, out MmsgMessageIcon);
            //    cntrlFocus = cboPricingScheme;
            //    blnValid = false;
            //}
             else 
            {
                cntrlFocus = dgvItem;
                blnValid = ValidateGrid(ref cntrlFocus);
            }
             
            if (!blnValid)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                if (cntrlFocus.GetType() != typeof(ClsInnerGrid))
                    errItemGroups.SetError(cntrlFocus, MstrCommonMessage);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                cntrlFocus.Focus();
            }
            return blnValid;
        }

        private bool ValidateGrid(ref Control cntrlFocus)
        {
            dgvItem.CommitEdit(DataGridViewDataErrorContexts.Commit);

            foreach (DataGridViewRow dgvrow in dgvItem.Rows)
            {
                if (Convert.ToInt32(dgvrow.Cells[dgvColItemID.Index].Value) > 0)
                {
                    if (Convert.ToInt32(dgvrow.Cells[dgvColUom.Index].Value) == 0)
                    {
                        //"Please enter UOM";
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1640, out MmsgMessageIcon);
                        dgvItem.CurrentCell = dgvrow.Cells[dgvColUom.Index];
                        return false;
                    }
                    else if (dgvrow.Cells[dgvColQuantity.Index].Value.ToDecimal() == 0)
                    {
                        //"Please enter Quantity";
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1639, out MmsgMessageIcon);
                        dgvItem.CurrentCell = dgvrow.Cells[dgvColQuantity.Index];
                        return false;
                    }
                }
            }

            List<int> intlistItems = new List<int>();
            int intValidRowCount = 0;

            foreach (DataGridViewRow dgvrow in dgvItem.Rows)
            {
                if (Convert.ToInt32(dgvrow.Cells[dgvColItemID.Index].Value) > 0 &&
                    dgvrow.Cells[dgvColQuantity.Index].Value.ToDecimal() > 0)
                {
                    intValidRowCount++;

                    if (intlistItems.Contains(Convert.ToInt32(dgvrow.Cells[dgvColItemID.Index].Value)))
                    {
                        //"Duplicate item is not permitted";
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1641, out MmsgMessageIcon);
                        dgvItem.CurrentCell = dgvrow.Cells[dgvColItemName.Index];
                        return false;
                    }
                    else
                    {
                        intlistItems.Add(Convert.ToInt32(dgvrow.Cells[dgvColItemID.Index].Value));
                    }
                }
            }

            if (intValidRowCount == 0)
            {
                //"Please enter Item";
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1642, out MmsgMessageIcon);
                dgvItem.CurrentCell = dgvItem.Rows[0].Cells[dgvColItemName.Index];
                return false;
            }
            else if (txtSaleAmount.Text.Trim().ToDecimal()<=0)
            {
                //"Please enter Sale Amount";
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1638, out MmsgMessageIcon);
                cntrlFocus = txtSaleAmount;
                return false;
            }
           
            return true;
        }

        private bool CheckDuplicateName()
        {
            int intItemGroupID=0;
            if (txtGroupCode.Text != "")
            {
                int iGroupId=0;
                if (txtGroupCode.Tag!=null )
                    iGroupId = Convert.ToInt32(txtGroupCode.Tag);
                intItemGroupID = MobjClsBLLItemGroup.CheckDupliateGroupCodeName(txtGroupCode.Text.Trim(), iGroupId);
            }
            if (intItemGroupID > 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1624, out MmsgMessageIcon);
                if (ClsMainSettings.BOQEnabled)
                    MstrCommonMessage.Replace("Group", "BOQ");
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                errItemGroups.SetError(txtGroupCode, MstrCommonMessage);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                txtGroupCode.Focus();
                return false;
            }
            return true;
    }

        private void DisplayItemGroup()
        {
            MblnAddMode = false;
            MobjClsBLLItemGroup.objClsDTOItemGroup.intRowNumber = MintRecordPosition;
            if (MobjClsBLLItemGroup.DisplayItemGroup())
            {
                txtGroupCode.Tag = MobjClsBLLItemGroup.objClsDTOItemGroup.lngItemGroupID.ToString();
                txtActualAmount.Text = MobjClsBLLItemGroup.objClsDTOItemGroup.decActualAmount.ToString();
                txtDescription.Text = MobjClsBLLItemGroup.objClsDTOItemGroup.strDescription;
                txtGroupCode.Text = MobjClsBLLItemGroup.objClsDTOItemGroup.strItemGroupCode;
                txtLabourCost.Text = MobjClsBLLItemGroup.objClsDTOItemGroup.decLabourCost.ToString();
               
                txtShortName.Text = MobjClsBLLItemGroup.objClsDTOItemGroup.strShortName;
                cboCategory.SelectedValue =Convert.ToString(MobjClsBLLItemGroup.objClsDTOItemGroup.intCategoryID);
                cboSubCategory.SelectedValue = Convert.ToString(MobjClsBLLItemGroup.objClsDTOItemGroup.intSubCategoryID);
                cboBaseUom.SelectedValue = Convert.ToString(MobjClsBLLItemGroup.objClsDTOItemGroup.intBaseUomID);
                cboCostingMethod.SelectedValue = Convert.ToString(MobjClsBLLItemGroup.objClsDTOItemGroup.intCostingMethodID);
                cboPricingScheme.SelectedValue = Convert.ToString(MobjClsBLLItemGroup.objClsDTOItemGroup.intPricingSchemeID);
                txtMachineCost.Text = MobjClsBLLItemGroup.objClsDTOItemGroup.decMachineCost.ToString();
               // cboStatus.SelectedValue = MobjClsBLLItemGroup.objClsDTOItemGroup.intItemGroupStatusID.ToString();
                txtBatch.Text = MobjClsBLLItemGroup.objClsDTOItemGroup.strBatch;
                if(MobjClsBLLItemGroup.objClsDTOItemGroup.strBatchDate!=string.Empty)
                    dtpDate.Value = MobjClsBLLItemGroup.objClsDTOItemGroup.strBatchDate.ToDateTime();
                DisplayItemGroupDetails();
                txtSaleAmount.Text = MobjClsBLLItemGroup.objClsDTOItemGroup.decSaleAmount.ToString();
                bnPositionItem.Text = MobjClsBLLItemGroup.objClsDTOItemGroup.intRowNumber.ToString();
                bnCountItem.Text = " of " + MintRecordCount.ToString();

                bnAddNewItem.Enabled = MblnAddPermission;
                bnDeleteItem.Enabled = MblnDeletePermission;
                bnEmail.Enabled = MblnPrintEmailPermission;
                bnPrint.Enabled = MblnPrintEmailPermission;

                bnSaveItem.Enabled = false;
                btnOk.Enabled = false;
                btnSave.Enabled = false;

                if (IsItemGroupExists(false))
                {
                    dgvItem.ReadOnly = true;
                }
                else
                {
                    dgvItem.ReadOnly = false;
                }
            }
        }

        private void DisplayItemGroupDetails()
        {
            int intRowIndex = 0;

            dgvItem.Rows.Clear();

            foreach (clsDTOItemGroupDetails objItemGroupDetails in MobjClsBLLItemGroup.objClsDTOItemGroup.objclsDTOItemGroupDetails)
            {
                dgvItem.RowCount++;

                dgvItem.Rows[intRowIndex].Cells[dgvColItemID.Index].Value = objItemGroupDetails.lngItemID.ToString();
                dgvItem.Rows[intRowIndex].Cells[dgvColUom.Index].Value = (object)objItemGroupDetails.intUOMID;

                int intUomScale = 0;

                if (dgvItem.Rows[intRowIndex].Cells[dgvColUom.Index].Value.ToInt32() != 0)
                    intUomScale = MobjClsBLLItemGroup.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvItem.Rows[intRowIndex].Cells[dgvColUom.Index].Value.ToInt32() }).Rows[0]["Scale"].ToInt32();

                dgvItem.Rows[intRowIndex].Cells[dgvColQuantity.Index].Value = objItemGroupDetails.decQuantity;

                dgvItem.Rows[intRowIndex].Cells[dgvColQuantity.Index].Value = dgvItem.Rows[intRowIndex].Cells[dgvColQuantity.Index].Value.ToDecimal().ToString("F" + intUomScale);
                dgvItem.Rows[intRowIndex].Cells[dgvColSerialNo.Index].Value = objItemGroupDetails.intSerialNo.ToString();
                dgvItem.Rows[intRowIndex].Cells[dgvColItemCode.Index].Value = objItemGroupDetails.strItemCode.ToString();
                dgvItem.Rows[intRowIndex].Cells[dgvColItemName.Index].Value = objItemGroupDetails.strItemName.ToString();
                dgvItem.Rows[intRowIndex].Cells[dgvColItemRate.Index].Value = objItemGroupDetails.decRate.ToString();
                dgvItem.Rows[intRowIndex].Cells[dgvColBaseRate.Index].Value = objItemGroupDetails.decRate.ToString();

                intRowIndex++;
            }
            dgvItem.ClearSelection();
        }

        private void MoveFirstItem(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition > 1)
                {
                    MintRecordPosition = 1;
                    DisplayItemGroup();
                }
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 9, out MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void MovePreviousItem(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition > 1)
                {
                    MintRecordPosition--;
                    DisplayItemGroup();
                }
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 10, out MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void MoveNextItem(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition < MintRecordCount)
                {
                    MintRecordPosition++;
                    DisplayItemGroup();
                }
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 11, out MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void MoveLastItem(object sender, EventArgs e)
        {
            try
            {
                if (MintRecordPosition < MintRecordCount)
                {
                    MintRecordPosition = MintRecordCount;
                    DisplayItemGroup();
                }
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 12, out MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void DeleteItem(object sender, EventArgs e)
        {
            try
            {
                //Do you want to delete the Item Group ?"
                if (!IsItemGroupExists(true))
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1644, out MmsgMessageIcon);
                    if (ClsMainSettings.BOQEnabled)
                        MstrCommonMessage.Replace("Item Group", "BOQ");
                    if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                        return;

                    MobjClsBLLItemGroup.DeleteItemGroup();
                    ResetForm();
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private bool IsItemGroupExists(bool blnShowErrMsg)
        {
            DataTable dtSalesQuotationDetails = MobjClsBLLItemGroup.FillCombos(new string[] { "SalesQuotationID", "InvSalesQuotationDetail", "IsGroup = 1 And ItemID = " + MobjClsBLLItemGroup.objClsDTOItemGroup.lngItemGroupID });
            DataTable dtSalesOrderDetails = MobjClsBLLItemGroup.FillCombos(new string[] { "SalesOrderID", "InvSalesOrderDetail", "IsGroup = 1 And ItemID = " + MobjClsBLLItemGroup.objClsDTOItemGroup.lngItemGroupID });
            DataTable dtSalesInvoiceDetails = MobjClsBLLItemGroup.FillCombos(new string[] { "SalesInvoiceID", "InvSalesInvoiceDetails", "IsGroup = 1 And ItemID = " + MobjClsBLLItemGroup.objClsDTOItemGroup.lngItemGroupID });
            DataTable dtDirectDeliveryNoteDetails = MobjClsBLLItemGroup.FillCombos(new string[] { "ItemIssueID", "InvItemIssueDetails", "IsGroup = 1 And ItemID = " + MobjClsBLLItemGroup.objClsDTOItemGroup.lngItemGroupID });
            DataTable dtPOSDetails = MobjClsBLLItemGroup.FillCombos(new string[] { "POSID", "InvPOSDetails", "IsGroup = 1 And ItemID = " + MobjClsBLLItemGroup.objClsDTOItemGroup.lngItemGroupID });


            if (dtSalesQuotationDetails.Rows.Count > 0 || dtSalesOrderDetails.Rows.Count > 0 || dtSalesInvoiceDetails.Rows.Count > 0 || dtDirectDeliveryNoteDetails.Rows.Count > 0 || dtPOSDetails.Rows.Count > 0)
            {
                //"Details of this group exists
                if (blnShowErrMsg)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1652, out MmsgMessageIcon);
                    if (ClsMainSettings.BOQEnabled)
                        MstrCommonMessage.Replace("Item Group", "BOQ");
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                    txtGroupCode.Focus();
                }
                return true;
            }
            return false;
        }

        private void Cancel(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ChangeStatus(object sender, EventArgs e)
        {
            errItemGroups.Clear();
            if (MblnAddMode)
            {
                btnSave.Enabled = MblnAddPermission;
                btnOk.Enabled = MblnAddPermission;
                bnSaveItem.Enabled = MblnAddPermission;
            }
            else
            {
                btnSave.Enabled = MblnUpdatePermission;
                btnOk.Enabled = MblnUpdatePermission;
                bnSaveItem.Enabled = MblnUpdatePermission;
            }
        }

        private void Cose(object sender, EventArgs e)
        {
            Close();
        }

        private void FillItemUOMs(int intRowIndex, int intItemID)
        {
            clsBLLItemMaster objItemMaster = new clsBLLItemMaster();
            DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)dgvItem.Rows[intRowIndex].Cells[dgvColUom.Index];

            cell.DisplayMember = "ShortName";
            cell.ValueMember = "UOMID";
            datItemDetails = objItemMaster.GetItemUOM(intItemID); ;
            cell.DataSource = datItemDetails;
            cell.Value = datItemDetails.Rows[0]["UOMID"].ToInt32();

            objItemMaster = null;
        }

        private void TimerTick(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            tmItemGroups.Enabled = false;
        }

        private void Email(object sender, EventArgs e)
        {
            using (FrmEmailPopup objEmailPopUp = new FrmEmailPopup())
            {
                objEmailPopUp.MsSubject = "Product Assembly";
                objEmailPopUp.EmailFormType = EmailFormID.ItemGroup;
                objEmailPopUp.EmailSource = MobjClsBLLItemGroup.GetItemGroupingReport();
                objEmailPopUp.ShowDialog();
            }
        }

        private void FormKeyDown(object sender, KeyEventArgs e)
        {
            //if (e.Control)
            //{
            //    switch (e.KeyCode)
            //    {
            //        case Keys.Left:
            //            MovePreviousItem(sender, e);
            //            break;
            //        case Keys.Right:
            //            MoveNextItem(sender, e);
            //            break;
            //        case Keys.Up:
            //            MoveLastItem(sender, e);
            //            break;
            //        case Keys.Down:
            //            MoveFirstItem(sender, e);
            //            break;
            //    }
            //}

            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        //Dim objHelp As New VisaHelp
                        //objHelp.frmWhere = "nEmp"
                        //objHelp.Show()
                        //objHelp = Nothing
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    //case Keys.Control | Keys.S:
                    //    btnSave_Click(sender,new EventArgs());//save
                    //  break;
                    //case Keys.Control | Keys.O:
                    //    btnOk_Click(sender, new EventArgs());//OK
                    //    break; 
                    //case Keys.Control | Keys.L:
                    //    btnCancel_Click(sender, new EventArgs());//Cancel
                    //    break;
                    case Keys.Control | Keys.Enter:
                        AddNewItem(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        DeleteItem(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        Cancel(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        MovePreviousItem(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        MoveNextItem(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        MoveFirstItem(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        MoveLastItem(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        bnPrint_Click(sender, new EventArgs());//Cancel
                        break;
                    case Keys.Control | Keys.M:
                        Email(sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception)
            {
            }

        }



        private void CalCulateRate(int intRowIndex)
        {
            clsBLLItemMaster objItemMaster = new clsBLLItemMaster();
            datItemDetails = objItemMaster.GetItemUOM(Convert.ToInt32(dgvItem[dgvColItemID.Index, intRowIndex].Value));
            if (datItemDetails != null)
            {
                if (datItemDetails.Rows.Count > 0)
                {
                    int intConvertionFactor;
                    decimal decQuantity, decRate, decConversionValue, decActualQuantity, decActualRate = 0;
                    if (dgvItem[dgvColUom.Index, intRowIndex].Value != DBNull.Value && Convert.ToString(dgvItem[dgvColUom.Index, intRowIndex].Value) != "" && dgvItem[dgvColUom.Index, intRowIndex].Value != null)
                    {
                        DataRow[] dRow = datItemDetails.Select("UomID = " + Convert.ToString(dgvItem[dgvColUom.Index, intRowIndex].Value));

                        if (dRow.Length > 0)
                        {
                            decActualRate = dgvItem[dgvColBaseRate.Index, intRowIndex].Value.ToDecimal();
                            if (decActualRate == 0)
                            {
                                DataTable dtRate = null;
                                dtRate = objItemMaster.FillCombos(new string[] { "dbo.GetSaleRate(" + dgvItem[dgvColItemID.Index, intRowIndex].Value.ToInt32() + ") as SaleRate,ItemID", "InvItemMaster", "ItemID=" + dgvItem[dgvColItemID.Index, intRowIndex].Value.ToInt32() + "" });
                                if (dtRate != null && dtRate.Rows.Count > 0)
                                {
                                    decActualRate = dtRate.Rows[0]["SaleRate"].ToDecimal();
                                    dgvItem[dgvColBaseRate.Index, intRowIndex].Value = decActualRate;
                                }
                            }
                            if (!Convert.ToBoolean(dRow[0][4]))
                            {
                                intConvertionFactor = Convert.ToInt32(dRow[0][2]);
                                decConversionValue = Convert.ToDecimal(dRow[0][3]);

                                decQuantity = Convert.ToDecimal(dgvItem[dgvColQuantity.Index, intRowIndex].Value);
                                decQuantity = 1;
                                if (intConvertionFactor == 1)
                                {
                                    decActualRate = (decQuantity * (1 / decConversionValue)) * decActualRate;
                                }
                                else
                                {
                                    decActualRate = (decQuantity / (1 / decConversionValue)) * decActualRate;
                                }
                            }
                            dgvItem[dgvColItemRate.Index, intRowIndex].Value = decActualRate.ToString("F" + ClsCommonSettings.Scale);
                        }
                    }
                }
            }
        }

        private void ClosingForm(object sender, FormClosingEventArgs e)
        {
            if (btnOk.Enabled)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 8, out MmsgMessageIcon).Replace("#", "").Trim();
                if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                    DialogResult.Yes)
                {
                    MobjClsNotification = null;
                    MobjClsLogWriter = null;
                    MobjClsBLLItemGroup = null;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {
            if (MobjClsBLLItemGroup.objClsDTOItemGroup.lngItemGroupID > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = MobjClsBLLItemGroup.objClsDTOItemGroup.lngItemGroupID;
                ObjViewer.PiFormID = (int)FormID.ItemGroup;
                ObjViewer.IsBOQEnabled = ClsMainSettings.BOQEnabled;
                ObjViewer.ShowDialog();
            }

        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                //objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)ModuleID.Inventory, (Int32)MenuID.ProductGroups, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (Int32)eModuleID.Inventory, (Int32)eMenuID.ProductGroups, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                if (MblnAddPermission == true || MblnUpdatePermission == true)
                    MblnAddUpdatePermission = true;

                //if (MblnAddPermission == true && MblnUpdatePermission == true && MblnDeletePermission == true)
                //    MblnPrintAndEmailPermission = true;
            }
            else
                MblnAddPermission = MblnAddUpdatePermission = MblnCancelPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            bnAddNewItem.Enabled = MblnAddPermission;
            bnSaveItem.Enabled = MblnAddUpdatePermission;
            bnDeleteItem.Enabled = MblnDeletePermission && this.MobjClsBLLItemGroup.objClsDTOItemGroup.lngItemGroupID > 0;
            btnSave.Enabled = MblnAddUpdatePermission;
            btnOk.Enabled = MblnAddUpdatePermission;
            bnPrint.Enabled = MblnPrintEmailPermission;
            bnEmail.Enabled = MblnPrintEmailPermission;
        }

        private void dgvItem_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == 2 || e.ColumnIndex == 3 || e.ColumnIndex == 4 || e.ColumnIndex == 5 )
            {
                if (Convert.ToString(dgvItem.CurrentRow.Cells[0].Value).Trim() == "")
                {
                    //MsMessageCommon = "Please enter Item code.";
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1601, out MmsgMessageIcon);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    dgvItem.Focus();
                    e.Cancel = true;
                }
            }
        }

        private void dgvItem_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dgvItem.PServerName = ClsCommonSettings.ServerName;

                string[] First = { "dgvColItemCode", "dgvColItemName", "dgvColItemID", "dgvColItemRate", "dgvColBaseRate" };//first grid 
                string[] second = { "ItemCode", "ItemName", "ItemID", "SaleRate", "SaleRate" };//inner grid

                dgvItem.aryFirstGridParam = First;
                dgvItem.arySecondGridParam = second;
                dgvItem.PiFocusIndex = 3;
                dgvItem.iGridWidth = 350;
                dgvItem.bBothScrollBar = true;
                //if (dgvItem.CurrentCell.ColumnIndex <= 1)
                //    dgvItem.PiColumnIndex = dgvItem.CurrentCell.ColumnIndex;
                //else
                //    dgvItem.PiColumnIndex = 0;
                dgvItem.ColumnsToHide = new string[]{"ItemID"};
                //dgvItem.ColumnsToHide = new string[] { "ItemID" };

                if (dgvItem.CurrentCell.ColumnIndex == dgvColItemCode.Index)
                    dgvItem.field = "Code";
                else if (dgvItem.CurrentCell.ColumnIndex == dgvColItemName.Index)
                    dgvItem.field = "ItemName";

                if (dgvItem.TextBoxText != "")
                {
                    dgvItem.PsQuery = "select [Code] as ItemCode,[ItemName] as ItemName,InvItemMaster.[ItemID] as ItemID,CP.SaleRate " +
                        "from InvItemMaster LEFT Join InvItemDetails On InvItemMaster.ItemID = InvItemDetails.ItemID " +
                        "LEFT JOIN InvCostingAndPricingDetails CP ON InvItemMaster.ItemID = CP.ItemID " +
                        "where CP.SaleRate > 0 " +
                        " And InvItemMaster.CompanyID=" +ClsCommonSettings.CompanyID +
                        "And InvItemMaster.StatusID = 31 And UPPER(" + dgvItem.field + ") like  UPPER('%" + (dgvItem.TextBoxText) + "%') " +
                        "Order By CHARINDEX('" + (dgvItem.TextBoxText) + "',lower(" + dgvItem.field + "))";
                }
                else
                {
                    dgvItem.PsQuery = "select [Code] as ItemCode,[ItemName] as ItemName,InvItemMaster.[ItemID] as ItemID,CP.SaleRate " +
                        "from InvItemMaster LEFT Join InvItemDetails On InvItemMaster.ItemID = InvItemDetails.ItemID " +
                        "LEFT JOIN InvCostingAndPricingDetails CP ON InvItemMaster.ItemID = CP.ItemID " +
                        "where CP.SaleRate > 0 And InvItemMaster.StatusID = 31 " +
                        " And InvItemMaster.CompanyID=" + ClsCommonSettings.CompanyID +
                        "Order By " + dgvItem.field + "";
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void OnKeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (dgvItem.CurrentCell.ColumnIndex == dgvColQuantity.Index)
                {
                    if (!Char.IsNumber(e.KeyChar))
                        e.Handled = true;
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void ItemValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (e.ColumnIndex == dgvColQuantity.Index || e.ColumnIndex == dgvColItemRate.Index)
                    {
                        if (e.ColumnIndex == dgvColQuantity.Index)
                        {
                            int intUomScale = 0;

                            if (dgvItem.Rows[e.RowIndex].Cells[dgvColUom.Index].Value.ToInt32() != 0)
                                intUomScale = MobjClsBLLItemGroup.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvItem.Rows[e.RowIndex].Cells[dgvColUom.Index].Value.ToInt32() }).Rows[0]["Scale"].ToInt32();

                            dgvItem.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value = dgvItem.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value.ToDecimal().ToString("F" + intUomScale);
                            CalCulateRate(e.RowIndex);
                        }

                        decimal decItemTotal = 0, decGroupTotal = 0;

                        foreach (DataGridViewRow dgvrow in dgvItem.Rows)
                        {
                            if (Convert.ToInt32(dgvrow.Cells[dgvColItemID.Index].Value) > 0 &&
                                dgvrow.Cells[dgvColQuantity.Index].Value.ToDecimal() > 0)
                            {
                                decItemTotal = Convert.ToDecimal(dgvrow.Cells[dgvColQuantity.Index].Value) *
                                                Convert.ToDecimal(dgvrow.Cells[dgvColItemRate.Index].Value);
                                dgvrow.Cells[dgvColTotal.Index].Value = decItemTotal.ToString("F" + ClsCommonSettings.Scale);
                                decGroupTotal = decGroupTotal + decItemTotal;
                            }
                        }

                        txtActualAmount.Text = decGroupTotal.ToString();
                        txtSaleAmount.Text = (txtActualAmount.Text.Trim().ToDecimal() + txtMachineCost.Text.Trim().ToDecimal() + txtLabourCost.Text.Trim().ToDecimal()).ToString();

                    }
                    else if (e.ColumnIndex == dgvColItemID.Index)
                    {
                        FillItemUOMs(e.RowIndex, Convert.ToInt32(dgvItem.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value));
                    }
                    else if (e.ColumnIndex == dgvColUom.Index)
                    {
                        int intUomScale = 0;

                        if (dgvItem.Rows[e.RowIndex].Cells[dgvColUom.Index].Value.ToInt32() != 0)
                            intUomScale = MobjClsBLLItemGroup.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvItem.Rows[e.RowIndex].Cells[dgvColUom.Index].Value.ToInt32() }).Rows[0]["Scale"].ToInt32();

                        dgvItem.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value = dgvItem.Rows[e.RowIndex].Cells[dgvColQuantity.Index].Value.ToDecimal().ToString("F" + intUomScale);
                
                        CalCulateRate(e.RowIndex);
                    }
                    ChangeStatus(sender, new EventArgs());
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Please enter valid Quantity " + Ex.Message.ToString());

                MobjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (MblnShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void ItemCellEnter(object sender, DataGridViewCellEventArgs e)
        {
                if (e.ColumnIndex == dgvColItemCode.Index || e.ColumnIndex == dgvColItemName.Index)
            {
                dgvItem.PiColumnIndex = e.ColumnIndex;
            } 
            //else
            //    dgvItem.PiColumnIndex = 0;

        }

        private void dgvItem_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (this.dgvItem.IsCurrentCellDirty)
            {
                if (this.dgvItem.CurrentCell != null)
                    this.dgvItem.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dgvItem_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvItem.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString() == "'")
                e.Handled = true;
        }

        private void dgvItem_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (e.Row.ReadOnly)
                e.Cancel = true;
        }

        private void cboStatus_KeyDown(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void dgvItem_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            SetSerialNo();
        }

        private void dgvItem_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            SetSerialNo();
        }

        private void dgvItem_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void txtLabourCost_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus(null, null);
            txtSaleAmount.Text = (txtActualAmount.Text.Trim().ToDecimal() + txtMachineCost.Text.Trim().ToDecimal() + txtLabourCost.Text.Trim().ToDecimal()).ToString();
        }
        private void SetSerialNo()
        {
            try
            {
                int intRowNumber = 1;
                foreach (DataGridViewRow row in dgvItem.Rows)
                {
                    if (row.Index < dgvItem.Rows.Count - 1)
                    {
                        row.HeaderCell.Value = intRowNumber.ToString();
                        intRowNumber++;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SetSerialNo() " + ex.Message);
                MobjClsLogWriter.WriteLog("Error in SetSerialNo() " + ex.Message, 2);
            }
        }

        private void btnCategory_Click(object sender, EventArgs e)
        {
            try
            {
                // Calling Reference form for ItemCategory
                FrmCommonRef objCommon = new FrmCommonRef("Category", new int[] { 1, 0 }, "CategoryID, CategoryName As Category", "InvCategoryReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intCurrentSelectedValue = cboCategory.SelectedValue.ToInt32();
                LoadCombo(1);
                if (objCommon.NewID != 0)
                    cboCategory.SelectedValue = objCommon.NewID;
                else
                    cboCategory.SelectedValue = intCurrentSelectedValue;
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnCategory_Click() " + Ex.Message.ToString());
            }
        }
        private bool ItemMasterCategoryValidation()
        {
            bool bValidationOk = true;
            errItemGroups.Clear();
            if (Convert.ToInt32(cboCategory.SelectedValue) == 0)   // Validating item category
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1604, out MmsgMessageIcon);
                errItemGroups.SetError(cboCategory, MstrCommonMessage.Replace("#", "").Trim());
                cboCategory.Focus();
                bValidationOk = false;
            }
            if (!bValidationOk) // If the validation fails
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1);
                tmItemGroups.Enabled = true;
            }
            return bValidationOk;

        }
        private void btnSubCategory_Click(object sender, EventArgs e)
        {
            try
            {
                if (ItemMasterCategoryValidation())
                {
                    // Calling Reference form for ItemType
                    frmSubCategory objSubCategory = new frmSubCategory();

                    if (this.cboCategory.SelectedIndex != -1)
                        objSubCategory.CategoryID = this.cboCategory.SelectedValue.ToInt32();

                    objSubCategory.ShowDialog();
                    objSubCategory.Dispose();
                    int intCurrentComboID = cboSubCategory.SelectedValue.ToInt32();
                    LoadCombo(2);
                    cboSubCategory.SelectedValue = intCurrentComboID;
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnItemType_Click() " + Ex.Message.ToString());
            }
        }

        private void btnPricingScheme_Click(object sender, EventArgs e)
        {
            int MintPricingID=0;
            try
            {
                if (cboPricingScheme.SelectedValue != null)
                    MintPricingID = Convert.ToInt32(cboPricingScheme.SelectedValue.ToString());

                using (frmPricingScheme objPricingScheme = new frmPricingScheme())
                {
                    objPricingScheme.ShowDialog();
                }
                LoadCombo(5);
                cboPricingScheme.SelectedValue = MintPricingID.ToString();
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form FrmItemMaster:FrmItemMaster " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnBaseUom_Click() " + Ex.Message.ToString());
            }
        }

        private void btnBaseUom_Click(object sender, EventArgs e)
        {
            try
            {
                int intCurrentComboID = cboBaseUom.SelectedValue.ToInt32();
                using (FrmUnitOfMeasurement objUnitOfMeasurement = new FrmUnitOfMeasurement())
                {
                    objUnitOfMeasurement.PintUOM = intCurrentComboID;
                    objUnitOfMeasurement.ShowDialog();
                }
                LoadCombo (3);
                cboBaseUom.SelectedValue = intCurrentComboID.ToString();
            }
            catch (Exception Ex)
            {
                MobjClsLogWriter.WriteLog("Error on form FrmItemMaster:FrmItemMaster " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnBaseUom_Click() " + Ex.Message.ToString());
            }

        }

        private void btnCostingMethod_Click(object sender, EventArgs e)
        {

        }
    }
}