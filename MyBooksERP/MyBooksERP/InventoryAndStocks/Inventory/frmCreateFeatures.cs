﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace MyBooksERP
{
    /****************************************************
     * Author       : Ratheesh
     * Created On   : 12 Apr 2012
     * Purpose      : Create form for entering features
     * ***************************************************/

    public partial class frmCreateFeatures : DevComponents.DotNetBar.Office2007Form
    {
        public frmItemMaster objParentForm = null;
        public int FeatureID;

        clsBLLCreateFeatures objBLLCreateFeature = null;

        public clsBLLCreateFeatures BLLCreateFeature 
        {
            get
            {
                if (this.objBLLCreateFeature == null)
                    this.objBLLCreateFeature = new clsBLLCreateFeatures();

                return this.objBLLCreateFeature;
            }
        }
        private clsMessage objUserMessage = null;
        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.ItemMaster);

                return this.objUserMessage;
            }
        }

        public frmCreateFeatures()
        {
            InitializeComponent();
        }

        private void frmCreateFeatures_Load(object sender, EventArgs e)
        {
            this.LoadCombo(1);
            this.LoadInitial();
        }

        private void LoadInitial()
        {
            if (this.FeatureID > 0)
                this.cboFeatures.SelectedValue = this.FeatureID;
        }

        private void LoadCombo(int intCboType)
        {
            // intCboType 1 for cboFeatures
            // intCboType 2 for cboFeatureValues

            try
            {
                if (intCboType == 1)
                {
                    this.cboFeatures.DataSource = this.BLLCreateFeature.GetFeatures();
                    this.cboFeatures.DisplayMember = "FeatureName";
                    this.cboFeatures.ValueMember = "FeatureID";

                    this.cboFeatures.SelectedIndex = -1;
                }
                else if (intCboType == 2)
                {
                    if (cboFeatures.SelectedValue == null) return;
                    this.cboFeatureValues.DataSource = this.BLLCreateFeature.GetFeatureValues(cboFeatures.SelectedValue.ToInt32());
                    this.cboFeatureValues.DisplayMember = "FeatureValue";
                    this.cboFeatureValues.ValueMember = "FeatureValueID";
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void btnAddToList_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvValues.Rows.Count > 0)
                {
                    if (this.cboFeatures.SelectedIndex == -1)
                    {
                        this.UserMessage.ShowMessage(1672);
                        return;
                    }

                    bool ValueEntered = false;
                    
                    clsFeatureTemp productFeature = new clsFeatureTemp ();
                    var IsDefaultSelected = false;

                    foreach (DataGridViewRow row in this.dgvValues.Rows)
                    {
                        if (!(row.Cells[0].Value.ToStringCustom().Trim() == string.Empty))
                        {
                            ValueEntered = true;

                            if (row.Cells[1].Value.ToBoolean())
                                IsDefaultSelected = true;

                            var duplicate = productFeature.Features.Find(f => f.FeatureValueID == row.Cells[0].Tag.ToInt64());

                            if (duplicate != null)
                            {
                                this.UserMessage.ShowMessage(1669);
                                return;
                            }

                            productFeature.Features.Add(new clsFeatureValue {
                                FeatureValueID = row.Cells[0].Tag.ToInt64(),
                                FeatureValue = row.Cells[0].Value.ToString(),
                                IsDefault = row.Cells[1].Value.ToBoolean()
                            });
                        }
                    }

                    if (!ValueEntered)
                    {
                        this.UserMessage.ShowMessage(1670);
                        return;
                    }
                    else if (!IsDefaultSelected)
                    {
                        this.UserMessage.ShowMessage(1671);
                        return;
                    }
                    else
                    {
                        productFeature.FeatureID = this.cboFeatures.SelectedValue.ToInt32();
                        productFeature.FeatureName = this.cboFeatures.Text;

                        var item = this.objParentForm.Features.Find(f => f.FeatureID == productFeature.FeatureID);
                        if (item != null)
                        {
                            this.objParentForm.Features.Remove(item);
                        }

                        this.objParentForm.Features.Add(productFeature);


                        if (this.objParentForm != null)
                        {
                            this.objParentForm.BindFeatureGrid(this.objParentForm.Features);

                            this.dgvValues.DataSource = null;
                            this.dgvValues.Rows.Clear();

                            this.cboFeatures.SelectedIndex = -1;
                            this.cboFeatures.SelectedText = string.Empty;
                            this.cboFeatures.Text = string.Empty;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvValues_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 1)
            {
                this.dgvValues.EndEdit();
                bool cheked = this.dgvValues.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToBoolean();

                // check if value entered in the selected feature column
                bool valueEntered = this.dgvValues.Rows[e.RowIndex].Cells[0].Value.ToStringCustom().Length > 0;

                if (!valueEntered)
                {
                    this.dgvValues.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = false;
                    this.UserMessage.ShowMessage(1672);
                    return;
                }

                if (cheked)
                {
                    foreach (DataGridViewRow row in this.dgvValues.Rows)
                    {
                        if (row.Index != e.RowIndex)
                            row.Cells[1].Value = false;
                    }
                }
            }
        }

       
        private void cboFeatures_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.dgvValues.DataSource = null;
            this.dgvValues.Rows.Clear();

            if (this.cboFeatures.SelectedIndex != -1)
            {
                int FeatureID = this.cboFeatures.SelectedValue.ToInt32();
                //this.FeatureID = FeatureID;
                LoadCombo(2);
                var features = this.objParentForm.Features.Find(f => f.FeatureID == FeatureID);

                if (features != null)
                {
                    int Index = 0;
                    foreach (var f in features.Features)
                    {
                        this.dgvValues.RowCount += 1;
                        this.dgvValues.Rows[Index].Cells[0].Value = f.FeatureValue;
                        this.dgvValues.Rows[Index].Cells[1].Value = f.IsDefault;
                        this.dgvValues.Rows[Index].Cells[0].Tag = f.FeatureValueID;
                        Index += 1;
                    }
                }
            }
        }

        private void btnAddFeature_Click(object sender, EventArgs e)
        {
            try
            {
                // Calling Reference form for ItemCategory
                FrmCommonRef objCommon = new FrmCommonRef("Feature", new int[] { 2, 2 }, "FeatureID, IsPredefined, FeatureName", "InvFeatureReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intCurrentSelectedValue = this.cboFeatures.SelectedValue.ToInt32();
                this.LoadCombo(1);
                if (objCommon.NewID != 0)
                    this.cboFeatures.SelectedValue = objCommon.NewID;
                else
                    this.cboFeatures.SelectedValue = intCurrentSelectedValue;
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void BtnFeatureValues_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cboFeatures.SelectedIndex == -1)
                {
                    this.UserMessage.ShowMessage(1672);
                    return;
                }
                this.FeatureID = cboFeatures.SelectedValue.ToInt32();
                // Calling Reference form for ItemCategory
                FrmCommonRef objCommon = new FrmCommonRef("Feature", new int[] { 1, 1 }, "FeatureValueID, FeatureValue", "InvFeatureValueReference", "FeatureID=" + this.FeatureID.ToString());
                objCommon.MstrInsertFields = "FeatureID";
                objCommon.MstrInsertValues = this.FeatureID.ToString();
                objCommon.ShowDialog();
                objCommon.Dispose();

                this.LoadCombo(2);

            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }

        private void dgvValues_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                if (this.dgvValues.Columns["cboFeatureValues"].Index == e.ColumnIndex)
                {
                    if (this.dgvValues.CurrentRow != null && this.dgvValues.CurrentRow.Cells["cboFeatureValues"].Value != null)
                    {
                        this.dgvValues.CurrentRow.Cells["cboFeatureValues"].Tag = this.dgvValues.CurrentRow.Cells["cboFeatureValues"].Value;
                        this.dgvValues.CurrentRow.Cells["cboFeatureValues"].Value = this.dgvValues.CurrentRow.Cells["cboFeatureValues"].FormattedValue;
                    }
                }
            }
        }

        private void dgvValues_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
        }

        private void cboFeatures_KeyDown(object sender, KeyEventArgs e)
        {
            cboFeatures.DroppedDown = false;
        }
    }
}
