﻿
namespace MyBooksERP
{
    partial class FrmWarehouseTransfer
    {
        # region Designer
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
     
            
       
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmWarehouseTransfer));
            this.cmdCommandZoom = new DevComponents.DotNetBar.Command(this.components);
            this.dockContainerItem1 = new DevComponents.DotNetBar.DockContainerItem();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.DgvStockTransferDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dgvColSStockTransferID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColSStockTransferNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelLeftTop = new DevComponents.DotNetBar.PanelEx();
            this.lnkLabelAdvanceSearch = new System.Windows.Forms.LinkLabel();
            this.lblSStockTransferNo = new System.Windows.Forms.Label();
            this.cboSIncharge = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSIncharge = new System.Windows.Forms.Label();
            this.cboSToRefernce = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSDestination = new System.Windows.Forms.Label();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cboSTransferType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSTransferType = new System.Windows.Forms.Label();
            this.cboSStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSStatus = new System.Windows.Forms.Label();
            this.cboSFromWH = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSFromWh = new System.Windows.Forms.Label();
            this.btnSRefresh = new DevComponents.DotNetBar.ButtonX();
            this.txtSTransferNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboSCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.LblScompany = new System.Windows.Forms.Label();
            this.lblIndentCount = new DevComponents.DotNetBar.LabelX();
            this.lblTransferNo = new System.Windows.Forms.Label();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.dotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.dockSite4 = new DevComponents.DotNetBar.DockSite();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.dockSite2 = new DevComponents.DotNetBar.DockSite();
            this.dockSite8 = new DevComponents.DotNetBar.DockSite();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.dockSite6 = new DevComponents.DotNetBar.DockSite();
            this.dockSite3 = new DevComponents.DotNetBar.DockSite();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.lblCreatedByText = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblCreatedDateValue = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblWarehouseTransferStatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.panelRightBottom = new DevComponents.DotNetBar.PanelEx();
            this.tcTransferOrder = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvItemDetail = new ClsInnerGridBar();
            this.tpItemDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.panelGridBottom = new DevComponents.DotNetBar.PanelEx();
            this.panelExAmount = new DevComponents.DotNetBar.PanelEx();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.lblExpenseAmount = new System.Windows.Forms.Label();
            this.lblSubtotal = new System.Windows.Forms.Label();
            this.lblCurrencyAmnt = new System.Windows.Forms.Label();
            this.txtExchangeCurrencyRate = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnAddExpense = new DevComponents.DotNetBar.ButtonX();
            this.txtExpenseAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtSubtotal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtTotalAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblDiscount = new System.Windows.Forms.Label();
            this.lblAmountIn = new System.Windows.Forms.Label();
            this.lblClearenceDate = new System.Windows.Forms.Label();
            this.dtpTrnsShipmentDate = new System.Windows.Forms.DateTimePicker();
            this.dtpClearenceDate = new System.Windows.Forms.DateTimePicker();
            this.dtpProductionDate = new System.Windows.Forms.DateTimePicker();
            this.lblProductionDate = new System.Windows.Forms.Label();
            this.lblTransShipmentDate = new System.Windows.Forms.Label();
            this.txtDiscount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtLeadTime = new NumericTextBox();
            this.cboDiscount = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboIncoTerms = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblIncoTerms = new System.Windows.Forms.Label();
            this.lblLeadTime = new System.Windows.Forms.Label();
            this.btnIncoTerms = new DevComponents.DotNetBar.ButtonX();
            this.txtRemarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboPaymentTerms = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.lblPaymentTerms = new System.Windows.Forms.Label();
            this.lblAmountInWords = new System.Windows.Forms.Label();
            this.lblAdvancePayment = new System.Windows.Forms.Label();
            this.txtAdvPayment = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.expandableSplitterTop = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.tcGeneral = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.cboxIncharge = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboXVendor = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblXIncharge = new System.Windows.Forms.Label();
            this.lblFromWarehouse = new System.Windows.Forms.Label();
            this.txtDescription = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnAddressChange = new DevComponents.DotNetBar.ButtonX();
            this.cboXFromWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblVendorAddress = new System.Windows.Forms.Label();
            this.lblToWarehouse = new System.Windows.Forms.Label();
            this.txtTransferNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.cboToWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblStatusText = new System.Windows.Forms.Label();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtVendorAddress = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblxType = new System.Windows.Forms.Label();
            this.BtnTVenderAddress = new DevComponents.DotNetBar.ButtonX();
            this.lblCompany = new System.Windows.Forms.Label();
            this.cboXTransferType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.dtpDueDate = new System.Windows.Forms.DateTimePicker();
            this.btnSupplier = new DevComponents.DotNetBar.ButtonX();
            this.cboCurrency = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblIndentDate = new System.Windows.Forms.Label();
            this.dtpTransferDate = new System.Windows.Forms.DateTimePicker();
            this.btnTContextmenu = new System.Windows.Forms.Button();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.lblDueDate = new System.Windows.Forms.Label();
            this.tiGeneral = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel4 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvSuggestions = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.UserID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VerifiedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiSuggestions = new DevComponents.DotNetBar.TabItem(this.components);
            this.WarehouseTransferBindingNavigator = new DevComponents.DotNetBar.Bar();
            this.BindingNavigatorAddNewItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorSaveItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorClearItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorDeleteItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorCancelItem = new DevComponents.DotNetBar.ButtonItem();
            this.btnActions = new DevComponents.DotNetBar.ButtonItem();
            this.bnExpense = new DevComponents.DotNetBar.ButtonItem();
            this.bnSubmitForApproval = new DevComponents.DotNetBar.ButtonItem();
            this.bnSubmit = new DevComponents.DotNetBar.ButtonItem();
            this.btnApprove = new DevComponents.DotNetBar.ButtonItem();
            this.btnSuggest = new DevComponents.DotNetBar.ButtonItem();
            this.btnDeny = new DevComponents.DotNetBar.ButtonItem();
            this.btnReject = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.BtnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.tmrWarehouseTransfer = new System.Windows.Forms.Timer(this.components);
            this.errProWarehouseTransfer = new System.Windows.Forms.ErrorProvider(this.components);
            this.imgLstWarehouseTransfer = new System.Windows.Forms.ImageList(this.components);
            this.CMSVendorAddress = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tpLocationDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColBatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColBatchName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColUom = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvColRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColGrandAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColDiscount = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvcolDiscountAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColNetAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvStockTransferDisplay)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            this.panelEx1.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.panelRightBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcTransferOrder)).BeginInit();
            this.tcTransferOrder.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemDetail)).BeginInit();
            this.panelGridBottom.SuspendLayout();
            this.panelExAmount.SuspendLayout();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).BeginInit();
            this.tcGeneral.SuspendLayout();
            this.tabControlPanel3.SuspendLayout();
            this.tabControlPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSuggestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WarehouseTransferBindingNavigator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProWarehouseTransfer)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdCommandZoom
            // 
            this.cmdCommandZoom.Name = "cmdCommandZoom";
            // 
            // dockContainerItem1
            // 
            this.dockContainerItem1.Name = "dockContainerItem1";
            this.dockContainerItem1.Text = "dockContainerItem1";
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.DgvStockTransferDisplay);
            this.PanelLeft.Controls.Add(this.expandablePanel1);
            this.PanelLeft.Controls.Add(this.lblIndentCount);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(200, 514);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 9;
            this.PanelLeft.Text = "panelEx1";
            // 
            // DgvStockTransferDisplay
            // 
            this.DgvStockTransferDisplay.AllowUserToAddRows = false;
            this.DgvStockTransferDisplay.AllowUserToDeleteRows = false;
            this.DgvStockTransferDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvStockTransferDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.DgvStockTransferDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvStockTransferDisplay.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColSStockTransferID,
            this.dgvColSStockTransferNo});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvStockTransferDisplay.DefaultCellStyle = dataGridViewCellStyle12;
            this.DgvStockTransferDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvStockTransferDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvStockTransferDisplay.Location = new System.Drawing.Point(0, 271);
            this.DgvStockTransferDisplay.Name = "DgvStockTransferDisplay";
            this.DgvStockTransferDisplay.ReadOnly = true;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvStockTransferDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.DgvStockTransferDisplay.RowHeadersVisible = false;
            this.DgvStockTransferDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvStockTransferDisplay.Size = new System.Drawing.Size(200, 217);
            this.DgvStockTransferDisplay.TabIndex = 45;
            this.DgvStockTransferDisplay.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvStockTransferDisplay_CellDoubleClick);
            // 
            // dgvColSStockTransferID
            // 
            this.dgvColSStockTransferID.DataPropertyName = "StockTransferID";
            this.dgvColSStockTransferID.HeaderText = "StockTransferID";
            this.dgvColSStockTransferID.Name = "dgvColSStockTransferID";
            this.dgvColSStockTransferID.ReadOnly = true;
            this.dgvColSStockTransferID.Visible = false;
            // 
            // dgvColSStockTransferNo
            // 
            this.dgvColSStockTransferNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvColSStockTransferNo.DataPropertyName = "Stock TransferNo";
            this.dgvColSStockTransferNo.HeaderText = "Stock TransferNo";
            this.dgvColSStockTransferNo.Name = "dgvColSStockTransferNo";
            this.dgvColSStockTransferNo.ReadOnly = true;
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expandablePanel1.Controls.Add(this.panelLeftTop);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(200, 271);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 17;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Search";
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelLeftTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelLeftTop.Controls.Add(this.lnkLabelAdvanceSearch);
            this.panelLeftTop.Controls.Add(this.lblSStockTransferNo);
            this.panelLeftTop.Controls.Add(this.cboSIncharge);
            this.panelLeftTop.Controls.Add(this.lblSIncharge);
            this.panelLeftTop.Controls.Add(this.cboSToRefernce);
            this.panelLeftTop.Controls.Add(this.lblSDestination);
            this.panelLeftTop.Controls.Add(this.dtpSTo);
            this.panelLeftTop.Controls.Add(this.dtpSFrom);
            this.panelLeftTop.Controls.Add(this.lblTo);
            this.panelLeftTop.Controls.Add(this.lblFrom);
            this.panelLeftTop.Controls.Add(this.cboSTransferType);
            this.panelLeftTop.Controls.Add(this.lblSTransferType);
            this.panelLeftTop.Controls.Add(this.cboSStatus);
            this.panelLeftTop.Controls.Add(this.lblSStatus);
            this.panelLeftTop.Controls.Add(this.cboSFromWH);
            this.panelLeftTop.Controls.Add(this.lblSFromWh);
            this.panelLeftTop.Controls.Add(this.btnSRefresh);
            this.panelLeftTop.Controls.Add(this.txtSTransferNo);
            this.panelLeftTop.Controls.Add(this.cboSCompany);
            this.panelLeftTop.Controls.Add(this.LblScompany);
            this.panelLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftTop.Location = new System.Drawing.Point(0, 26);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(200, 241);
            this.panelLeftTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelLeftTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelLeftTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelLeftTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelLeftTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelLeftTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelLeftTop.Style.GradientAngle = 90;
            this.panelLeftTop.TabIndex = 34;
            // 
            // lnkLabelAdvanceSearch
            // 
            this.lnkLabelAdvanceSearch.AutoSize = true;
            this.lnkLabelAdvanceSearch.Location = new System.Drawing.Point(5, 225);
            this.lnkLabelAdvanceSearch.Name = "lnkLabelAdvanceSearch";
            this.lnkLabelAdvanceSearch.Size = new System.Drawing.Size(87, 13);
            this.lnkLabelAdvanceSearch.TabIndex = 260;
            this.lnkLabelAdvanceSearch.TabStop = true;
            this.lnkLabelAdvanceSearch.Text = "Advance Search";
            this.lnkLabelAdvanceSearch.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLabelAdvanceSearch_LinkClicked);
            // 
            // lblSStockTransferNo
            // 
            this.lblSStockTransferNo.AutoSize = true;
            this.lblSStockTransferNo.Location = new System.Drawing.Point(3, 195);
            this.lblSStockTransferNo.Name = "lblSStockTransferNo";
            this.lblSStockTransferNo.Size = new System.Drawing.Size(60, 13);
            this.lblSStockTransferNo.TabIndex = 247;
            this.lblSStockTransferNo.Text = "TransferNo";
            // 
            // cboSIncharge
            // 
            this.cboSIncharge.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSIncharge.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSIncharge.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSIncharge.DisplayMember = "EmployeeFullName";
            this.cboSIncharge.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSIncharge.DropDownHeight = 75;
            this.cboSIncharge.FormattingEnabled = true;
            this.cboSIncharge.IntegralHeight = false;
            this.cboSIncharge.ItemHeight = 14;
            this.cboSIncharge.Location = new System.Drawing.Point(82, 101);
            this.cboSIncharge.Name = "cboSIncharge";
            this.cboSIncharge.Size = new System.Drawing.Size(115, 20);
            this.cboSIncharge.TabIndex = 39;
            this.cboSIncharge.ValueMember = "EmployeeID";
            // 
            // lblSIncharge
            // 
            this.lblSIncharge.AutoSize = true;
            this.lblSIncharge.Location = new System.Drawing.Point(5, 105);
            this.lblSIncharge.Name = "lblSIncharge";
            this.lblSIncharge.Size = new System.Drawing.Size(50, 13);
            this.lblSIncharge.TabIndex = 246;
            this.lblSIncharge.Text = "InCharge";
            // 
            // cboSToRefernce
            // 
            this.cboSToRefernce.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSToRefernce.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSToRefernce.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSToRefernce.DisplayMember = "Text";
            this.cboSToRefernce.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSToRefernce.DropDownHeight = 75;
            this.cboSToRefernce.FormattingEnabled = true;
            this.cboSToRefernce.IntegralHeight = false;
            this.cboSToRefernce.ItemHeight = 14;
            this.cboSToRefernce.Location = new System.Drawing.Point(82, 78);
            this.cboSToRefernce.Name = "cboSToRefernce";
            this.cboSToRefernce.Size = new System.Drawing.Size(115, 20);
            this.cboSToRefernce.TabIndex = 38;
            // 
            // lblSDestination
            // 
            this.lblSDestination.AutoSize = true;
            this.lblSDestination.Location = new System.Drawing.Point(5, 79);
            this.lblSDestination.Name = "lblSDestination";
            this.lblSDestination.Size = new System.Drawing.Size(20, 13);
            this.lblSDestination.TabIndex = 244;
            this.lblSDestination.Text = "To";
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(82, 170);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.Size = new System.Drawing.Size(115, 20);
            this.dtpSTo.TabIndex = 42;
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(82, 147);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.Size = new System.Drawing.Size(115, 20);
            this.dtpSFrom.TabIndex = 41;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(3, 173);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 130;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(3, 150);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 129;
            this.lblFrom.Text = "From";
            // 
            // cboSTransferType
            // 
            this.cboSTransferType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSTransferType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSTransferType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSTransferType.DisplayMember = "Text";
            this.cboSTransferType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSTransferType.DropDownHeight = 75;
            this.cboSTransferType.FormattingEnabled = true;
            this.cboSTransferType.IntegralHeight = false;
            this.cboSTransferType.ItemHeight = 14;
            this.cboSTransferType.Location = new System.Drawing.Point(82, 32);
            this.cboSTransferType.Name = "cboSTransferType";
            this.cboSTransferType.Size = new System.Drawing.Size(115, 20);
            this.cboSTransferType.TabIndex = 36;
            this.cboSTransferType.SelectedIndexChanged += new System.EventHandler(this.cboSTransferType_SelectedIndexChanged);
            // 
            // lblSTransferType
            // 
            this.lblSTransferType.AutoSize = true;
            this.lblSTransferType.Location = new System.Drawing.Point(3, 36);
            this.lblSTransferType.Name = "lblSTransferType";
            this.lblSTransferType.Size = new System.Drawing.Size(73, 13);
            this.lblSTransferType.TabIndex = 128;
            this.lblSTransferType.Text = "Transfer Type";
            // 
            // cboSStatus
            // 
            this.cboSStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSStatus.DisplayMember = "Text";
            this.cboSStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSStatus.DropDownHeight = 75;
            this.cboSStatus.FormattingEnabled = true;
            this.cboSStatus.IntegralHeight = false;
            this.cboSStatus.ItemHeight = 14;
            this.cboSStatus.Location = new System.Drawing.Point(82, 124);
            this.cboSStatus.Name = "cboSStatus";
            this.cboSStatus.Size = new System.Drawing.Size(115, 20);
            this.cboSStatus.TabIndex = 40;
            // 
            // lblSStatus
            // 
            this.lblSStatus.AutoSize = true;
            this.lblSStatus.Location = new System.Drawing.Point(3, 128);
            this.lblSStatus.Name = "lblSStatus";
            this.lblSStatus.Size = new System.Drawing.Size(37, 13);
            this.lblSStatus.TabIndex = 125;
            this.lblSStatus.Text = "Status";
            // 
            // cboSFromWH
            // 
            this.cboSFromWH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSFromWH.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSFromWH.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSFromWH.DisplayMember = "Text";
            this.cboSFromWH.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSFromWH.DropDownHeight = 75;
            this.cboSFromWH.FormattingEnabled = true;
            this.cboSFromWH.IntegralHeight = false;
            this.cboSFromWH.ItemHeight = 14;
            this.cboSFromWH.Location = new System.Drawing.Point(83, 55);
            this.cboSFromWH.Name = "cboSFromWH";
            this.cboSFromWH.Size = new System.Drawing.Size(115, 20);
            this.cboSFromWH.TabIndex = 37;
            // 
            // lblSFromWh
            // 
            this.lblSFromWh.AutoSize = true;
            this.lblSFromWh.Location = new System.Drawing.Point(3, 59);
            this.lblSFromWh.Name = "lblSFromWh";
            this.lblSFromWh.Size = new System.Drawing.Size(52, 13);
            this.lblSFromWh.TabIndex = 123;
            this.lblSFromWh.Text = "From WH";
            // 
            // btnSRefresh
            // 
            this.btnSRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSRefresh.Location = new System.Drawing.Point(123, 215);
            this.btnSRefresh.Name = "btnSRefresh";
            this.btnSRefresh.Size = new System.Drawing.Size(71, 23);
            this.btnSRefresh.TabIndex = 44;
            this.btnSRefresh.Text = "Refresh";
            this.btnSRefresh.Click += new System.EventHandler(this.btnSRefresh_Click);
            // 
            // txtSTransferNo
            // 
            this.txtSTransferNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtSTransferNo.Border.Class = "TextBoxBorder";
            this.txtSTransferNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSTransferNo.Location = new System.Drawing.Point(82, 193);
            this.txtSTransferNo.MaxLength = 20;
            this.txtSTransferNo.Name = "txtSTransferNo";
            this.txtSTransferNo.Size = new System.Drawing.Size(115, 20);
            this.txtSTransferNo.TabIndex = 43;
            this.txtSTransferNo.WatermarkImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cboSCompany
            // 
            this.cboSCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSCompany.DisplayMember = "Text";
            this.cboSCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSCompany.DropDownHeight = 75;
            this.cboSCompany.FormattingEnabled = true;
            this.cboSCompany.IntegralHeight = false;
            this.cboSCompany.ItemHeight = 14;
            this.cboSCompany.Location = new System.Drawing.Point(82, 10);
            this.cboSCompany.Name = "cboSCompany";
            this.cboSCompany.Size = new System.Drawing.Size(115, 20);
            this.cboSCompany.TabIndex = 35;
            this.cboSCompany.SelectedIndexChanged += new System.EventHandler(this.cboSCompany_SelectedIndexChanged);
            // 
            // LblScompany
            // 
            this.LblScompany.AutoSize = true;
            this.LblScompany.Location = new System.Drawing.Point(3, 13);
            this.LblScompany.Name = "LblScompany";
            this.LblScompany.Size = new System.Drawing.Size(51, 13);
            this.LblScompany.TabIndex = 109;
            this.LblScompany.Text = "Company";
            // 
            // lblIndentCount
            // 
            // 
            // 
            // 
            this.lblIndentCount.BackgroundStyle.Class = "";
            this.lblIndentCount.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblIndentCount.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblIndentCount.Location = new System.Drawing.Point(0, 488);
            this.lblIndentCount.Name = "lblIndentCount";
            this.lblIndentCount.Size = new System.Drawing.Size(200, 26);
            this.lblIndentCount.TabIndex = 110;
            this.lblIndentCount.Text = "...";
            // 
            // lblTransferNo
            // 
            this.lblTransferNo.AutoSize = true;
            this.lblTransferNo.BackColor = System.Drawing.Color.Transparent;
            this.lblTransferNo.Location = new System.Drawing.Point(611, 9);
            this.lblTransferNo.Name = "lblTransferNo";
            this.lblTransferNo.Size = new System.Drawing.Size(86, 13);
            this.lblTransferNo.TabIndex = 185;
            this.lblTransferNo.Text = "Transfer Number";
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(200, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 514);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 10;
            this.expandableSplitterLeft.TabStop = false;
            this.expandableSplitterLeft.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitterLeft_ExpandedChanged);
            // 
            // dotNetBarManager1
            // 
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.dotNetBarManager1.BottomDockSite = this.dockSite4;
            this.dotNetBarManager1.EnableFullSizeDock = false;
            this.dotNetBarManager1.LeftDockSite = this.dockSite1;
            this.dotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.dotNetBarManager1.ParentForm = this;
            this.dotNetBarManager1.RightDockSite = this.dockSite2;
            this.dotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.dotNetBarManager1.ToolbarBottomDockSite = this.dockSite8;
            this.dotNetBarManager1.ToolbarLeftDockSite = this.dockSite5;
            this.dotNetBarManager1.ToolbarRightDockSite = this.dockSite6;
            this.dotNetBarManager1.TopDockSite = this.dockSite3;
            // 
            // dockSite4
            // 
            this.dockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite4.Location = new System.Drawing.Point(0, 514);
            this.dockSite4.Name = "dockSite4";
            this.dockSite4.Size = new System.Drawing.Size(1303, 0);
            this.dockSite4.TabIndex = 18;
            this.dockSite4.TabStop = false;
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(203, 0);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 514);
            this.dockSite1.TabIndex = 15;
            this.dockSite1.TabStop = false;
            // 
            // dockSite2
            // 
            this.dockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite2.Location = new System.Drawing.Point(1303, 0);
            this.dockSite2.Name = "dockSite2";
            this.dockSite2.Size = new System.Drawing.Size(0, 514);
            this.dockSite2.TabIndex = 16;
            this.dockSite2.TabStop = false;
            // 
            // dockSite8
            // 
            this.dockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite8.Location = new System.Drawing.Point(0, 514);
            this.dockSite8.Name = "dockSite8";
            this.dockSite8.Size = new System.Drawing.Size(1303, 0);
            this.dockSite8.TabIndex = 22;
            this.dockSite8.TabStop = false;
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(0, 0);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 514);
            this.dockSite5.TabIndex = 19;
            this.dockSite5.TabStop = false;
            // 
            // dockSite6
            // 
            this.dockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite6.Location = new System.Drawing.Point(1303, 0);
            this.dockSite6.Name = "dockSite6";
            this.dockSite6.Size = new System.Drawing.Size(0, 514);
            this.dockSite6.TabIndex = 20;
            this.dockSite6.TabStop = false;
            // 
            // dockSite3
            // 
            this.dockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite3.Location = new System.Drawing.Point(0, 0);
            this.dockSite3.Name = "dockSite3";
            this.dockSite3.Size = new System.Drawing.Size(1303, 0);
            this.dockSite3.TabIndex = 17;
            this.dockSite3.TabStop = false;
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx1.Controls.Add(this.pnlBottom);
            this.panelEx1.Controls.Add(this.panelRightBottom);
            this.panelEx1.Controls.Add(this.expandableSplitterTop);
            this.panelEx1.Controls.Add(this.panelTop);
            this.panelEx1.Controls.Add(this.WarehouseTransferBindingNavigator);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(203, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(1100, 514);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 0;
            this.panelEx1.Text = "panelEx1";
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.lblCreatedByText);
            this.pnlBottom.Controls.Add(this.lblCreatedDateValue);
            this.pnlBottom.Controls.Add(this.lblWarehouseTransferStatus);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 488);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1100, 26);
            this.pnlBottom.TabIndex = 246;
            // 
            // lblCreatedByText
            // 
            this.lblCreatedByText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedByText.CloseButtonVisible = false;
            this.lblCreatedByText.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblCreatedByText.Location = new System.Drawing.Point(718, 0);
            this.lblCreatedByText.Name = "lblCreatedByText";
            this.lblCreatedByText.OptionsButtonVisible = false;
            this.lblCreatedByText.Size = new System.Drawing.Size(380, 24);
            this.lblCreatedByText.TabIndex = 258;
            this.lblCreatedByText.Text = "Created By";
            // 
            // lblCreatedDateValue
            // 
            this.lblCreatedDateValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedDateValue.CloseButtonVisible = false;
            this.lblCreatedDateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCreatedDateValue.Location = new System.Drawing.Point(393, 0);
            this.lblCreatedDateValue.Name = "lblCreatedDateValue";
            this.lblCreatedDateValue.OptionsButtonVisible = false;
            this.lblCreatedDateValue.Size = new System.Drawing.Size(705, 24);
            this.lblCreatedDateValue.TabIndex = 256;
            this.lblCreatedDateValue.Text = "Created Date";
            // 
            // lblWarehouseTransferStatus
            // 
            this.lblWarehouseTransferStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblWarehouseTransferStatus.CloseButtonVisible = false;
            this.lblWarehouseTransferStatus.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblWarehouseTransferStatus.Image = ((System.Drawing.Image)(resources.GetObject("lblWarehouseTransferStatus.Image")));
            this.lblWarehouseTransferStatus.Location = new System.Drawing.Point(0, 0);
            this.lblWarehouseTransferStatus.Name = "lblWarehouseTransferStatus";
            this.lblWarehouseTransferStatus.OptionsButtonVisible = false;
            this.lblWarehouseTransferStatus.Size = new System.Drawing.Size(393, 24);
            this.lblWarehouseTransferStatus.TabIndex = 20;
            // 
            // panelRightBottom
            // 
            this.panelRightBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelRightBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelRightBottom.Controls.Add(this.tcTransferOrder);
            this.panelRightBottom.Controls.Add(this.panelGridBottom);
            this.panelRightBottom.Controls.Add(this.lblAdvancePayment);
            this.panelRightBottom.Controls.Add(this.txtAdvPayment);
            this.panelRightBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRightBottom.Location = new System.Drawing.Point(0, 178);
            this.panelRightBottom.Name = "panelRightBottom";
            this.panelRightBottom.Size = new System.Drawing.Size(1100, 336);
            this.panelRightBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelRightBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelRightBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelRightBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelRightBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelRightBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelRightBottom.Style.GradientAngle = 90;
            this.panelRightBottom.TabIndex = 18;
            this.panelRightBottom.Text = "panelEx3";
            // 
            // tcTransferOrder
            // 
            this.tcTransferOrder.BackColor = System.Drawing.Color.Transparent;
            this.tcTransferOrder.CanReorderTabs = true;
            this.tcTransferOrder.Controls.Add(this.tabControlPanel1);
            this.tcTransferOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcTransferOrder.Location = new System.Drawing.Point(0, 0);
            this.tcTransferOrder.Name = "tcTransferOrder";
            this.tcTransferOrder.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcTransferOrder.SelectedTabIndex = 0;
            this.tcTransferOrder.Size = new System.Drawing.Size(1100, 199);
            this.tcTransferOrder.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcTransferOrder.TabIndex = 13;
            this.tcTransferOrder.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcTransferOrder.Tabs.Add(this.tpItemDetails);
            this.tcTransferOrder.TabStop = false;
            this.tcTransferOrder.Text = "tabControl1";
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.dgvItemDetail);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1100, 177);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 17;
            this.tabControlPanel1.TabItem = this.tpItemDetails;
            // 
            // dgvItemDetail
            // 
            this.dgvItemDetail.AddNewRow = false;
            this.dgvItemDetail.AlphaNumericCols = new int[0];
            this.dgvItemDetail.BackgroundColor = System.Drawing.Color.White;
            this.dgvItemDetail.CapsLockCols = new int[0];
            this.dgvItemDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvItemDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColItemID,
            this.dgvColItemCode,
            this.dgvColItemName,
            this.dgvColBatchID,
            this.dgvColBatchName,
            this.dgvColQty,
            this.dgvColUom,
            this.dgvColRate,
            this.dgvColGrandAmount,
            this.dgvColDiscount,
            this.dgvcolDiscountAmount,
            this.dgvColNetAmount});
            this.dgvItemDetail.DecimalCols = new int[0];
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvItemDetail.DefaultCellStyle = dataGridViewCellStyle19;
            this.dgvItemDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvItemDetail.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvItemDetail.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvItemDetail.HasSlNo = false;
            this.dgvItemDetail.LastRowIndex = 0;
            this.dgvItemDetail.Location = new System.Drawing.Point(1, 1);
            this.dgvItemDetail.Name = "dgvItemDetail";
            this.dgvItemDetail.NegativeValueCols = new int[0];
            this.dgvItemDetail.NumericCols = new int[0];
            this.dgvItemDetail.RowHeadersWidth = 50;
            this.dgvItemDetail.Size = new System.Drawing.Size(1098, 175);
            this.dgvItemDetail.TabIndex = 13;
            this.dgvItemDetail.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItemDetail_CellValueChanged);
            this.dgvItemDetail.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvItemDetail_CellBeginEdit);
            this.dgvItemDetail.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvItemDetail_RowsAdded);
            this.dgvItemDetail.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItemDetail_CellEndEdit);
            this.dgvItemDetail.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvItemDetail_Textbox_TextChanged);
            this.dgvItemDetail.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvItemDetail_EditingControlShowing);
            this.dgvItemDetail.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvItemDetail_CurrentCellDirtyStateChanged);
            this.dgvItemDetail.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItemDetail_CellEnter);
            this.dgvItemDetail.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvItemDetail_RowsRemoved);
            // 
            // tpItemDetails
            // 
            this.tpItemDetails.AttachedControl = this.tabControlPanel1;
            this.tpItemDetails.Name = "tpItemDetails";
            this.tpItemDetails.Text = "Item Details";
            // 
            // panelGridBottom
            // 
            this.panelGridBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelGridBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelGridBottom.Controls.Add(this.panelExAmount);
            this.panelGridBottom.Controls.Add(this.lblDiscount);
            this.panelGridBottom.Controls.Add(this.lblAmountIn);
            this.panelGridBottom.Controls.Add(this.lblClearenceDate);
            this.panelGridBottom.Controls.Add(this.dtpTrnsShipmentDate);
            this.panelGridBottom.Controls.Add(this.dtpClearenceDate);
            this.panelGridBottom.Controls.Add(this.dtpProductionDate);
            this.panelGridBottom.Controls.Add(this.lblProductionDate);
            this.panelGridBottom.Controls.Add(this.lblTransShipmentDate);
            this.panelGridBottom.Controls.Add(this.txtDiscount);
            this.panelGridBottom.Controls.Add(this.txtLeadTime);
            this.panelGridBottom.Controls.Add(this.cboDiscount);
            this.panelGridBottom.Controls.Add(this.cboIncoTerms);
            this.panelGridBottom.Controls.Add(this.lblIncoTerms);
            this.panelGridBottom.Controls.Add(this.lblLeadTime);
            this.panelGridBottom.Controls.Add(this.btnIncoTerms);
            this.panelGridBottom.Controls.Add(this.txtRemarks);
            this.panelGridBottom.Controls.Add(this.cboPaymentTerms);
            this.panelGridBottom.Controls.Add(this.lblRemarks);
            this.panelGridBottom.Controls.Add(this.lblPaymentTerms);
            this.panelGridBottom.Controls.Add(this.lblAmountInWords);
            this.panelGridBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGridBottom.Location = new System.Drawing.Point(0, 199);
            this.panelGridBottom.Name = "panelGridBottom";
            this.panelGridBottom.Size = new System.Drawing.Size(1100, 137);
            this.panelGridBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelGridBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelGridBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelGridBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelGridBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelGridBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelGridBottom.Style.GradientAngle = 90;
            this.panelGridBottom.TabIndex = 106;
            // 
            // panelExAmount
            // 
            this.panelExAmount.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelExAmount.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelExAmount.Controls.Add(this.lblTotalAmount);
            this.panelExAmount.Controls.Add(this.lblExpenseAmount);
            this.panelExAmount.Controls.Add(this.lblSubtotal);
            this.panelExAmount.Controls.Add(this.lblCurrencyAmnt);
            this.panelExAmount.Controls.Add(this.txtExchangeCurrencyRate);
            this.panelExAmount.Controls.Add(this.btnAddExpense);
            this.panelExAmount.Controls.Add(this.txtExpenseAmount);
            this.panelExAmount.Controls.Add(this.txtSubtotal);
            this.panelExAmount.Controls.Add(this.txtTotalAmount);
            this.panelExAmount.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelExAmount.Location = new System.Drawing.Point(711, 0);
            this.panelExAmount.Name = "panelExAmount";
            this.panelExAmount.Size = new System.Drawing.Size(389, 137);
            this.panelExAmount.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelExAmount.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelExAmount.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelExAmount.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelExAmount.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelExAmount.Style.GradientAngle = 90;
            this.panelExAmount.TabIndex = 26;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.BackColor = System.Drawing.Color.White;
            this.lblTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(202, 99);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(45, 9);
            this.lblTotalAmount.TabIndex = 253;
            this.lblTotalAmount.Text = "Net Amount";
            // 
            // lblExpenseAmount
            // 
            this.lblExpenseAmount.AutoSize = true;
            this.lblExpenseAmount.BackColor = System.Drawing.Color.White;
            this.lblExpenseAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpenseAmount.Location = new System.Drawing.Point(202, 61);
            this.lblExpenseAmount.Name = "lblExpenseAmount";
            this.lblExpenseAmount.Size = new System.Drawing.Size(38, 9);
            this.lblExpenseAmount.TabIndex = 254;
            this.lblExpenseAmount.Text = "Expenses";
            // 
            // lblSubtotal
            // 
            this.lblSubtotal.AutoSize = true;
            this.lblSubtotal.BackColor = System.Drawing.Color.White;
            this.lblSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubtotal.Location = new System.Drawing.Point(202, 39);
            this.lblSubtotal.Name = "lblSubtotal";
            this.lblSubtotal.Size = new System.Drawing.Size(54, 9);
            this.lblSubtotal.TabIndex = 250;
            this.lblSubtotal.Text = "Gross Amount";
            // 
            // lblCurrencyAmnt
            // 
            this.lblCurrencyAmnt.AutoSize = true;
            this.lblCurrencyAmnt.BackColor = System.Drawing.Color.White;
            this.lblCurrencyAmnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrencyAmnt.Location = new System.Drawing.Point(14, 99);
            this.lblCurrencyAmnt.Name = "lblCurrencyAmnt";
            this.lblCurrencyAmnt.Size = new System.Drawing.Size(39, 9);
            this.lblCurrencyAmnt.TabIndex = 240;
            this.lblCurrencyAmnt.Text = "Amount in";
            // 
            // txtExchangeCurrencyRate
            // 
            this.txtExchangeCurrencyRate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtExchangeCurrencyRate.Border.Class = "TextBoxBorder";
            this.txtExchangeCurrencyRate.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtExchangeCurrencyRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExchangeCurrencyRate.Location = new System.Drawing.Point(13, 74);
            this.txtExchangeCurrencyRate.Name = "txtExchangeCurrencyRate";
            this.txtExchangeCurrencyRate.ReadOnly = true;
            this.txtExchangeCurrencyRate.Size = new System.Drawing.Size(185, 35);
            this.txtExchangeCurrencyRate.TabIndex = 32;
            this.txtExchangeCurrencyRate.TabStop = false;
            this.txtExchangeCurrencyRate.Text = "0.00";
            this.txtExchangeCurrencyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnAddExpense
            // 
            this.btnAddExpense.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddExpense.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddExpense.Location = new System.Drawing.Point(169, 51);
            this.btnAddExpense.Name = "btnAddExpense";
            this.btnAddExpense.Size = new System.Drawing.Size(29, 20);
            this.btnAddExpense.TabIndex = 15;
            this.btnAddExpense.Text = "...";
            this.btnAddExpense.Click += new System.EventHandler(this.btnAddExpense_Click);
            // 
            // txtExpenseAmount
            // 
            this.txtExpenseAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtExpenseAmount.Border.Class = "TextBoxBorder";
            this.txtExpenseAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtExpenseAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExpenseAmount.Location = new System.Drawing.Point(201, 51);
            this.txtExpenseAmount.Name = "txtExpenseAmount";
            this.txtExpenseAmount.ReadOnly = true;
            this.txtExpenseAmount.Size = new System.Drawing.Size(185, 20);
            this.txtExpenseAmount.TabIndex = 29;
            this.txtExpenseAmount.TabStop = false;
            this.txtExpenseAmount.Text = "0.00";
            this.txtExpenseAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSubtotal
            // 
            this.txtSubtotal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtSubtotal.Border.Class = "TextBoxBorder";
            this.txtSubtotal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubtotal.Location = new System.Drawing.Point(201, 29);
            this.txtSubtotal.MaxLength = 27;
            this.txtSubtotal.Name = "txtSubtotal";
            this.txtSubtotal.ReadOnly = true;
            this.txtSubtotal.Size = new System.Drawing.Size(185, 20);
            this.txtSubtotal.TabIndex = 27;
            this.txtSubtotal.TabStop = false;
            this.txtSubtotal.Text = "0.00";
            this.txtSubtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTotalAmount.Border.Class = "TextBoxBorder";
            this.txtTotalAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(201, 74);
            this.txtTotalAmount.MaxLength = 27;
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.Size = new System.Drawing.Size(185, 35);
            this.txtTotalAmount.TabIndex = 33;
            this.txtTotalAmount.TabStop = false;
            this.txtTotalAmount.Text = "0.00";
            this.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalAmount.TextChanged += new System.EventHandler(this.txtTotalAmount_TextChanged);
            // 
            // lblDiscount
            // 
            this.lblDiscount.AutoSize = true;
            this.lblDiscount.BackColor = System.Drawing.Color.White;
            this.lblDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscount.Location = new System.Drawing.Point(592, 29);
            this.lblDiscount.Name = "lblDiscount";
            this.lblDiscount.Size = new System.Drawing.Size(35, 9);
            this.lblDiscount.TabIndex = 251;
            this.lblDiscount.Text = "Discount";
            this.lblDiscount.Visible = false;
            // 
            // lblAmountIn
            // 
            this.lblAmountIn.AutoSize = true;
            this.lblAmountIn.Location = new System.Drawing.Point(9, 78);
            this.lblAmountIn.Name = "lblAmountIn";
            this.lblAmountIn.Size = new System.Drawing.Size(92, 13);
            this.lblAmountIn.TabIndex = 282;
            this.lblAmountIn.Text = "Amount In Words:";
            // 
            // lblClearenceDate
            // 
            this.lblClearenceDate.AutoSize = true;
            this.lblClearenceDate.Location = new System.Drawing.Point(621, 41);
            this.lblClearenceDate.Name = "lblClearenceDate";
            this.lblClearenceDate.Size = new System.Drawing.Size(81, 13);
            this.lblClearenceDate.TabIndex = 281;
            this.lblClearenceDate.Text = "Clearence Date";
            this.lblClearenceDate.Visible = false;
            // 
            // dtpTrnsShipmentDate
            // 
            this.dtpTrnsShipmentDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpTrnsShipmentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTrnsShipmentDate.Location = new System.Drawing.Point(706, 12);
            this.dtpTrnsShipmentDate.Name = "dtpTrnsShipmentDate";
            this.dtpTrnsShipmentDate.Size = new System.Drawing.Size(102, 20);
            this.dtpTrnsShipmentDate.TabIndex = 24;
            this.dtpTrnsShipmentDate.TabStop = false;
            this.dtpTrnsShipmentDate.Visible = false;
            this.dtpTrnsShipmentDate.ValueChanged += new System.EventHandler(this.dtpTrnsShipmentDate_ValueChanged);
            // 
            // dtpClearenceDate
            // 
            this.dtpClearenceDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpClearenceDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpClearenceDate.Location = new System.Drawing.Point(706, 38);
            this.dtpClearenceDate.Name = "dtpClearenceDate";
            this.dtpClearenceDate.Size = new System.Drawing.Size(102, 20);
            this.dtpClearenceDate.TabIndex = 25;
            this.dtpClearenceDate.TabStop = false;
            this.dtpClearenceDate.Visible = false;
            this.dtpClearenceDate.ValueChanged += new System.EventHandler(this.dtpClearenceDate_ValueChanged);
            // 
            // dtpProductionDate
            // 
            this.dtpProductionDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpProductionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpProductionDate.Location = new System.Drawing.Point(482, 60);
            this.dtpProductionDate.Name = "dtpProductionDate";
            this.dtpProductionDate.Size = new System.Drawing.Size(102, 20);
            this.dtpProductionDate.TabIndex = 23;
            this.dtpProductionDate.TabStop = false;
            this.dtpProductionDate.Visible = false;
            this.dtpProductionDate.ValueChanged += new System.EventHandler(this.dtpProductionDate_ValueChanged);
            // 
            // lblProductionDate
            // 
            this.lblProductionDate.AutoSize = true;
            this.lblProductionDate.Location = new System.Drawing.Point(391, 60);
            this.lblProductionDate.Name = "lblProductionDate";
            this.lblProductionDate.Size = new System.Drawing.Size(84, 13);
            this.lblProductionDate.TabIndex = 279;
            this.lblProductionDate.Text = "Production Date";
            this.lblProductionDate.Visible = false;
            // 
            // lblTransShipmentDate
            // 
            this.lblTransShipmentDate.AutoSize = true;
            this.lblTransShipmentDate.Location = new System.Drawing.Point(621, 15);
            this.lblTransShipmentDate.Name = "lblTransShipmentDate";
            this.lblTransShipmentDate.Size = new System.Drawing.Size(63, 13);
            this.lblTransShipmentDate.TabIndex = 280;
            this.lblTransShipmentDate.Text = "Trans. Date";
            this.lblTransShipmentDate.Visible = false;
            // 
            // txtDiscount
            // 
            this.txtDiscount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDiscount.Border.Class = "TextBoxBorder";
            this.txtDiscount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscount.Location = new System.Drawing.Point(591, 19);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.ReadOnly = true;
            this.txtDiscount.Size = new System.Drawing.Size(185, 20);
            this.txtDiscount.TabIndex = 31;
            this.txtDiscount.TabStop = false;
            this.txtDiscount.Text = "0.00";
            this.txtDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscount.Visible = false;
            // 
            // txtLeadTime
            // 
            this.txtLeadTime.DecimalPlaces = 4;
            this.txtLeadTime.Location = new System.Drawing.Point(482, 38);
            this.txtLeadTime.MaxLength = 12;
            this.txtLeadTime.Name = "txtLeadTime";
            this.txtLeadTime.ShortcutsEnabled = false;
            this.txtLeadTime.Size = new System.Drawing.Size(103, 20);
            this.txtLeadTime.TabIndex = 22;
            this.txtLeadTime.Text = "0.0000";
            this.txtLeadTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLeadTime.Visible = false;
            this.txtLeadTime.TextChanged += new System.EventHandler(this.txtLeadTime_TextChanged);
            // 
            // cboDiscount
            // 
            this.cboDiscount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDiscount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDiscount.DisplayMember = "Text";
            this.cboDiscount.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDiscount.DropDownHeight = 75;
            this.cboDiscount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDiscount.FormattingEnabled = true;
            this.cboDiscount.IntegralHeight = false;
            this.cboDiscount.ItemHeight = 14;
            this.cboDiscount.Location = new System.Drawing.Point(403, 19);
            this.cboDiscount.Name = "cboDiscount";
            this.cboDiscount.Size = new System.Drawing.Size(185, 20);
            this.cboDiscount.TabIndex = 30;
            this.cboDiscount.Visible = false;
            this.cboDiscount.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDiscount.WatermarkText = "Select Discount";
            this.cboDiscount.SelectedIndexChanged += new System.EventHandler(this.cboDiscount_SelectedIndexChanged);
            // 
            // cboIncoTerms
            // 
            this.cboIncoTerms.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboIncoTerms.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboIncoTerms.DisplayMember = "Text";
            this.cboIncoTerms.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboIncoTerms.DropDownHeight = 75;
            this.cboIncoTerms.FormattingEnabled = true;
            this.cboIncoTerms.IntegralHeight = false;
            this.cboIncoTerms.ItemHeight = 14;
            this.cboIncoTerms.Location = new System.Drawing.Point(482, 15);
            this.cboIncoTerms.Name = "cboIncoTerms";
            this.cboIncoTerms.Size = new System.Drawing.Size(103, 20);
            this.cboIncoTerms.TabIndex = 20;
            this.cboIncoTerms.Visible = false;
            this.cboIncoTerms.SelectedIndexChanged += new System.EventHandler(this.cboIncoTerms_SelectedIndexChanged);
            // 
            // lblIncoTerms
            // 
            this.lblIncoTerms.AutoSize = true;
            this.lblIncoTerms.Location = new System.Drawing.Point(391, 16);
            this.lblIncoTerms.Name = "lblIncoTerms";
            this.lblIncoTerms.Size = new System.Drawing.Size(60, 13);
            this.lblIncoTerms.TabIndex = 275;
            this.lblIncoTerms.Text = "Inco Terms";
            this.lblIncoTerms.Visible = false;
            // 
            // lblLeadTime
            // 
            this.lblLeadTime.AutoSize = true;
            this.lblLeadTime.Location = new System.Drawing.Point(391, 37);
            this.lblLeadTime.Name = "lblLeadTime";
            this.lblLeadTime.Size = new System.Drawing.Size(57, 13);
            this.lblLeadTime.TabIndex = 274;
            this.lblLeadTime.Text = "Lead Time";
            this.lblLeadTime.Visible = false;
            // 
            // btnIncoTerms
            // 
            this.btnIncoTerms.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnIncoTerms.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnIncoTerms.Location = new System.Drawing.Point(592, 15);
            this.btnIncoTerms.Name = "btnIncoTerms";
            this.btnIncoTerms.Size = new System.Drawing.Size(25, 20);
            this.btnIncoTerms.TabIndex = 21;
            this.btnIncoTerms.TabStop = false;
            this.btnIncoTerms.Text = "...";
            this.btnIncoTerms.Visible = false;
            // 
            // txtRemarks
            // 
            // 
            // 
            // 
            this.txtRemarks.Border.Class = "TextBoxBorder";
            this.txtRemarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtRemarks.Location = new System.Drawing.Point(103, 13);
            this.txtRemarks.MaxLength = 1000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(270, 59);
            this.txtRemarks.TabIndex = 14;
            // 
            // cboPaymentTerms
            // 
            this.cboPaymentTerms.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboPaymentTerms.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboPaymentTerms.DisplayMember = "Text";
            this.cboPaymentTerms.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboPaymentTerms.DropDownHeight = 75;
            this.cboPaymentTerms.FormattingEnabled = true;
            this.cboPaymentTerms.IntegralHeight = false;
            this.cboPaymentTerms.ItemHeight = 14;
            this.cboPaymentTerms.Location = new System.Drawing.Point(623, 93);
            this.cboPaymentTerms.Name = "cboPaymentTerms";
            this.cboPaymentTerms.Size = new System.Drawing.Size(161, 20);
            this.cboPaymentTerms.TabIndex = 252;
            this.cboPaymentTerms.Visible = false;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(9, 16);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 36;
            this.lblRemarks.Text = "Remarks";
            // 
            // lblPaymentTerms
            // 
            this.lblPaymentTerms.AutoSize = true;
            this.lblPaymentTerms.Location = new System.Drawing.Point(529, 95);
            this.lblPaymentTerms.Name = "lblPaymentTerms";
            this.lblPaymentTerms.Size = new System.Drawing.Size(80, 13);
            this.lblPaymentTerms.TabIndex = 254;
            this.lblPaymentTerms.Text = "Payment Terms";
            this.lblPaymentTerms.Visible = false;
            // 
            // lblAmountInWords
            // 
            this.lblAmountInWords.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountInWords.Location = new System.Drawing.Point(103, 78);
            this.lblAmountInWords.Name = "lblAmountInWords";
            this.lblAmountInWords.Size = new System.Drawing.Size(594, 33);
            this.lblAmountInWords.TabIndex = 283;
            // 
            // lblAdvancePayment
            // 
            this.lblAdvancePayment.AutoSize = true;
            this.lblAdvancePayment.BackColor = System.Drawing.Color.White;
            this.lblAdvancePayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdvancePayment.Location = new System.Drawing.Point(919, 183);
            this.lblAdvancePayment.Name = "lblAdvancePayment";
            this.lblAdvancePayment.Size = new System.Drawing.Size(66, 9);
            this.lblAdvancePayment.TabIndex = 303;
            this.lblAdvancePayment.Text = "Advance Payment";
            this.lblAdvancePayment.Visible = false;
            // 
            // txtAdvPayment
            // 
            this.txtAdvPayment.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtAdvPayment.Border.Class = "TextBoxBorder";
            this.txtAdvPayment.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtAdvPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdvPayment.Location = new System.Drawing.Point(918, 173);
            this.txtAdvPayment.MaxLength = 20;
            this.txtAdvPayment.Name = "txtAdvPayment";
            this.txtAdvPayment.ReadOnly = true;
            this.txtAdvPayment.Size = new System.Drawing.Size(185, 20);
            this.txtAdvPayment.TabIndex = 302;
            this.txtAdvPayment.TabStop = false;
            this.txtAdvPayment.Text = "0.00";
            this.txtAdvPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAdvPayment.Visible = false;
            // 
            // expandableSplitterTop
            // 
            this.expandableSplitterTop.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterTop.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitterTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitterTop.ExpandableControl = this.panelTop;
            this.expandableSplitterTop.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterTop.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterTop.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterTop.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterTop.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.Location = new System.Drawing.Point(0, 175);
            this.expandableSplitterTop.Name = "expandableSplitterTop";
            this.expandableSplitterTop.Size = new System.Drawing.Size(1100, 3);
            this.expandableSplitterTop.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterTop.TabIndex = 1;
            this.expandableSplitterTop.TabStop = false;
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelTop.Controls.Add(this.tcGeneral);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1100, 150);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 0;
            // 
            // tcGeneral
            // 
            this.tcGeneral.BackColor = System.Drawing.Color.Transparent;
            this.tcGeneral.CanReorderTabs = true;
            this.tcGeneral.Controls.Add(this.tabControlPanel3);
            this.tcGeneral.Controls.Add(this.tabControlPanel4);
            this.tcGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcGeneral.Location = new System.Drawing.Point(0, 0);
            this.tcGeneral.Name = "tcGeneral";
            this.tcGeneral.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcGeneral.SelectedTabIndex = 0;
            this.tcGeneral.Size = new System.Drawing.Size(1100, 150);
            this.tcGeneral.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcGeneral.TabIndex = 0;
            this.tcGeneral.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcGeneral.Tabs.Add(this.tiGeneral);
            this.tcGeneral.Tabs.Add(this.tiSuggestions);
            this.tcGeneral.TabStop = false;
            this.tcGeneral.Text = "tabControl1";
            this.tcGeneral.SelectedTabChanged += new DevComponents.DotNetBar.TabStrip.SelectedTabChangedEventHandler(this.tcGeneral_SelectedTabChanged);
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel3.Controls.Add(this.cboxIncharge);
            this.tabControlPanel3.Controls.Add(this.cboXVendor);
            this.tabControlPanel3.Controls.Add(this.lblDescription);
            this.tabControlPanel3.Controls.Add(this.lblXIncharge);
            this.tabControlPanel3.Controls.Add(this.lblFromWarehouse);
            this.tabControlPanel3.Controls.Add(this.txtDescription);
            this.tabControlPanel3.Controls.Add(this.btnAddressChange);
            this.tabControlPanel3.Controls.Add(this.cboXFromWarehouse);
            this.tabControlPanel3.Controls.Add(this.lblStatus);
            this.tabControlPanel3.Controls.Add(this.lblVendorAddress);
            this.tabControlPanel3.Controls.Add(this.lblToWarehouse);
            this.tabControlPanel3.Controls.Add(this.txtTransferNo);
            this.tabControlPanel3.Controls.Add(this.lblCustomer);
            this.tabControlPanel3.Controls.Add(this.cboToWarehouse);
            this.tabControlPanel3.Controls.Add(this.lblStatusText);
            this.tabControlPanel3.Controls.Add(this.cboCompany);
            this.tabControlPanel3.Controls.Add(this.txtVendorAddress);
            this.tabControlPanel3.Controls.Add(this.lblxType);
            this.tabControlPanel3.Controls.Add(this.BtnTVenderAddress);
            this.tabControlPanel3.Controls.Add(this.lblCompany);
            this.tabControlPanel3.Controls.Add(this.cboXTransferType);
            this.tabControlPanel3.Controls.Add(this.lblTransferNo);
            this.tabControlPanel3.Controls.Add(this.dtpDueDate);
            this.tabControlPanel3.Controls.Add(this.btnSupplier);
            this.tabControlPanel3.Controls.Add(this.cboCurrency);
            this.tabControlPanel3.Controls.Add(this.lblIndentDate);
            this.tabControlPanel3.Controls.Add(this.dtpTransferDate);
            this.tabControlPanel3.Controls.Add(this.btnTContextmenu);
            this.tabControlPanel3.Controls.Add(this.lblCurrency);
            this.tabControlPanel3.Controls.Add(this.lblDueDate);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(1100, 128);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 0;
            this.tabControlPanel3.TabItem = this.tiGeneral;
            // 
            // cboxIncharge
            // 
            this.cboxIncharge.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboxIncharge.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboxIncharge.DisplayMember = "Text";
            this.cboxIncharge.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboxIncharge.DropDownHeight = 75;
            this.cboxIncharge.FormattingEnabled = true;
            this.cboxIncharge.IntegralHeight = false;
            this.cboxIncharge.ItemHeight = 14;
            this.cboxIncharge.Location = new System.Drawing.Point(118, 101);
            this.cboxIncharge.Name = "cboxIncharge";
            this.cboxIncharge.Size = new System.Drawing.Size(209, 20);
            this.cboxIncharge.TabIndex = 4;
            this.cboxIncharge.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboxIncharge_KeyPress);
            // 
            // cboXVendor
            // 
            this.cboXVendor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboXVendor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboXVendor.DisplayMember = "Text";
            this.cboXVendor.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboXVendor.DropDownHeight = 75;
            this.cboXVendor.FormattingEnabled = true;
            this.cboXVendor.IntegralHeight = false;
            this.cboXVendor.ItemHeight = 14;
            this.cboXVendor.Location = new System.Drawing.Point(394, 7);
            this.cboXVendor.Name = "cboXVendor";
            this.cboXVendor.Size = new System.Drawing.Size(171, 20);
            this.cboXVendor.TabIndex = 5;
            this.cboXVendor.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboXVendor.WatermarkText = "Select Customer";
            this.cboXVendor.SelectedIndexChanged += new System.EventHandler(this.cboXVendor_SelectedIndexChanged);
            this.cboXVendor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboXVendor_KeyPress);
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblDescription.Location = new System.Drawing.Point(820, 37);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 13);
            this.lblDescription.TabIndex = 240;
            this.lblDescription.Text = "Description";
            // 
            // lblXIncharge
            // 
            this.lblXIncharge.AutoSize = true;
            this.lblXIncharge.BackColor = System.Drawing.Color.Transparent;
            this.lblXIncharge.Location = new System.Drawing.Point(28, 106);
            this.lblXIncharge.Name = "lblXIncharge";
            this.lblXIncharge.Size = new System.Drawing.Size(53, 13);
            this.lblXIncharge.TabIndex = 267;
            this.lblXIncharge.Text = "In Charge";
            // 
            // lblFromWarehouse
            // 
            this.lblFromWarehouse.AutoSize = true;
            this.lblFromWarehouse.BackColor = System.Drawing.Color.Transparent;
            this.lblFromWarehouse.Location = new System.Drawing.Point(28, 56);
            this.lblFromWarehouse.Name = "lblFromWarehouse";
            this.lblFromWarehouse.Size = new System.Drawing.Size(88, 13);
            this.lblFromWarehouse.TabIndex = 241;
            this.lblFromWarehouse.Text = "From Warehouse";
            // 
            // txtDescription
            // 
            // 
            // 
            // 
            this.txtDescription.Border.Class = "TextBoxBorder";
            this.txtDescription.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDescription.Enabled = false;
            this.txtDescription.Location = new System.Drawing.Point(886, 38);
            this.txtDescription.MaxLength = 200;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(251, 59);
            this.txtDescription.TabIndex = 15;
            this.txtDescription.TabStop = false;
            this.txtDescription.Visible = false;
            // 
            // btnAddressChange
            // 
            this.btnAddressChange.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddressChange.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddressChange.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnAddressChange.Location = new System.Drawing.Point(571, 30);
            this.btnAddressChange.Name = "btnAddressChange";
            this.btnAddressChange.Size = new System.Drawing.Size(29, 20);
            this.btnAddressChange.TabIndex = 8;
            this.btnAddressChange.Text = "6";
            this.btnAddressChange.Click += new System.EventHandler(this.btnAddressChange_Click);
            // 
            // cboXFromWarehouse
            // 
            this.cboXFromWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboXFromWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboXFromWarehouse.DisplayMember = "Text";
            this.cboXFromWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboXFromWarehouse.DropDownHeight = 75;
            this.cboXFromWarehouse.FormattingEnabled = true;
            this.cboXFromWarehouse.IntegralHeight = false;
            this.cboXFromWarehouse.ItemHeight = 14;
            this.cboXFromWarehouse.Location = new System.Drawing.Point(118, 54);
            this.cboXFromWarehouse.Name = "cboXFromWarehouse";
            this.cboXFromWarehouse.Size = new System.Drawing.Size(209, 20);
            this.cboXFromWarehouse.TabIndex = 2;
            this.cboXFromWarehouse.SelectedIndexChanged += new System.EventHandler(this.cboXFromWarehouse_SelectedIndexChanged);
            this.cboXFromWarehouse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboXFromWarehouse_KeyPress);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Location = new System.Drawing.Point(820, 12);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 208;
            this.lblStatus.Text = "Status";
            // 
            // lblVendorAddress
            // 
            this.lblVendorAddress.AutoSize = true;
            this.lblVendorAddress.BackColor = System.Drawing.Color.Transparent;
            this.lblVendorAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendorAddress.Location = new System.Drawing.Point(395, 31);
            this.lblVendorAddress.Name = "lblVendorAddress";
            this.lblVendorAddress.Size = new System.Drawing.Size(33, 9);
            this.lblVendorAddress.TabIndex = 265;
            this.lblVendorAddress.Text = "Address";
            // 
            // lblToWarehouse
            // 
            this.lblToWarehouse.AutoSize = true;
            this.lblToWarehouse.BackColor = System.Drawing.Color.Transparent;
            this.lblToWarehouse.Location = new System.Drawing.Point(28, 80);
            this.lblToWarehouse.Name = "lblToWarehouse";
            this.lblToWarehouse.Size = new System.Drawing.Size(78, 13);
            this.lblToWarehouse.TabIndex = 243;
            this.lblToWarehouse.Text = "To Warehouse";
            // 
            // txtTransferNo
            // 
            this.txtTransferNo.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtTransferNo.Border.Class = "TextBoxBorder";
            this.txtTransferNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTransferNo.Location = new System.Drawing.Point(708, 8);
            this.txtTransferNo.MaxLength = 20;
            this.txtTransferNo.Name = "txtTransferNo";
            this.txtTransferNo.Size = new System.Drawing.Size(101, 20);
            this.txtTransferNo.TabIndex = 10;
            this.txtTransferNo.TabStop = false;
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.BackColor = System.Drawing.Color.Transparent;
            this.lblCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomer.Location = new System.Drawing.Point(342, 10);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(45, 13);
            this.lblCustomer.TabIndex = 264;
            this.lblCustomer.Text = "Supplier";
            // 
            // cboToWarehouse
            // 
            this.cboToWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboToWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboToWarehouse.DisplayMember = "Text";
            this.cboToWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboToWarehouse.DropDownHeight = 75;
            this.cboToWarehouse.FormattingEnabled = true;
            this.cboToWarehouse.IntegralHeight = false;
            this.cboToWarehouse.ItemHeight = 14;
            this.cboToWarehouse.Location = new System.Drawing.Point(118, 77);
            this.cboToWarehouse.Name = "cboToWarehouse";
            this.cboToWarehouse.Size = new System.Drawing.Size(209, 20);
            this.cboToWarehouse.TabIndex = 3;
            this.cboToWarehouse.SelectedIndexChanged += new System.EventHandler(this.cboToWarehouse_SelectedIndexChanged);
            this.cboToWarehouse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboToWarehouse_KeyPress);
            // 
            // lblStatusText
            // 
            this.lblStatusText.AutoSize = true;
            this.lblStatusText.BackColor = System.Drawing.Color.Transparent;
            this.lblStatusText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.lblStatusText.Location = new System.Drawing.Point(882, 7);
            this.lblStatusText.Name = "lblStatusText";
            this.lblStatusText.Size = new System.Drawing.Size(62, 20);
            this.lblStatusText.TabIndex = 6;
            this.lblStatusText.Text = "Status";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 75;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(119, 6);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(209, 20);
            this.cboCompany.TabIndex = 0;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCompany_KeyPress);
            this.cboCompany.SelectedValueChanged += new System.EventHandler(this.cboCompany_SelectedValueChanged);
            // 
            // txtVendorAddress
            // 
            // 
            // 
            // 
            this.txtVendorAddress.Border.Class = "TextBoxBorder";
            this.txtVendorAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtVendorAddress.Location = new System.Drawing.Point(394, 30);
            this.txtVendorAddress.MaxLength = 200;
            this.txtVendorAddress.Multiline = true;
            this.txtVendorAddress.Name = "txtVendorAddress";
            this.txtVendorAddress.ReadOnly = true;
            this.txtVendorAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtVendorAddress.Size = new System.Drawing.Size(171, 68);
            this.txtVendorAddress.TabIndex = 7;
            this.txtVendorAddress.TabStop = false;
            // 
            // lblxType
            // 
            this.lblxType.AutoSize = true;
            this.lblxType.BackColor = System.Drawing.Color.Transparent;
            this.lblxType.Location = new System.Drawing.Point(28, 31);
            this.lblxType.Name = "lblxType";
            this.lblxType.Size = new System.Drawing.Size(73, 13);
            this.lblxType.TabIndex = 255;
            this.lblxType.Text = "Transfer Type";
            // 
            // BtnTVenderAddress
            // 
            this.BtnTVenderAddress.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnTVenderAddress.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnTVenderAddress.Location = new System.Drawing.Point(529, 59);
            this.BtnTVenderAddress.Name = "BtnTVenderAddress";
            this.BtnTVenderAddress.Size = new System.Drawing.Size(10, 10);
            this.BtnTVenderAddress.TabIndex = 266;
            this.BtnTVenderAddress.TabStop = false;
            this.BtnTVenderAddress.Text = "Permanent";
            this.BtnTVenderAddress.Visible = false;
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.BackColor = System.Drawing.Color.Transparent;
            this.lblCompany.Location = new System.Drawing.Point(28, 7);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 200;
            this.lblCompany.Text = "Company";
            // 
            // cboXTransferType
            // 
            this.cboXTransferType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboXTransferType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboXTransferType.DisplayMember = "Text";
            this.cboXTransferType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboXTransferType.DropDownHeight = 75;
            this.cboXTransferType.FormattingEnabled = true;
            this.cboXTransferType.IntegralHeight = false;
            this.cboXTransferType.ItemHeight = 14;
            this.cboXTransferType.Location = new System.Drawing.Point(118, 30);
            this.cboXTransferType.Name = "cboXTransferType";
            this.cboXTransferType.Size = new System.Drawing.Size(209, 20);
            this.cboXTransferType.TabIndex = 1;
            this.cboXTransferType.SelectionChangeCommitted += new System.EventHandler(this.cboXTransferType_SelectionChangeCommitted);
            this.cboXTransferType.SelectedIndexChanged += new System.EventHandler(this.cboXTransferType_SelectedIndexChanged);
            this.cboXTransferType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboXTransferType_KeyPress);
            // 
            // dtpDueDate
            // 
            this.dtpDueDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDueDate.Location = new System.Drawing.Point(708, 54);
            this.dtpDueDate.Name = "dtpDueDate";
            this.dtpDueDate.Size = new System.Drawing.Size(101, 20);
            this.dtpDueDate.TabIndex = 12;
            // 
            // btnSupplier
            // 
            this.btnSupplier.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSupplier.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSupplier.Location = new System.Drawing.Point(571, 7);
            this.btnSupplier.Name = "btnSupplier";
            this.btnSupplier.Size = new System.Drawing.Size(29, 20);
            this.btnSupplier.TabIndex = 6;
            this.btnSupplier.Text = "...";
            this.btnSupplier.Click += new System.EventHandler(this.btnSupplier_Click);
            // 
            // cboCurrency
            // 
            this.cboCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCurrency.DisplayMember = "Text";
            this.cboCurrency.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCurrency.DropDownHeight = 75;
            this.cboCurrency.Enabled = false;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.IntegralHeight = false;
            this.cboCurrency.ItemHeight = 14;
            this.cboCurrency.Location = new System.Drawing.Point(394, 102);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(171, 20);
            this.cboCurrency.TabIndex = 9;
            this.cboCurrency.TabStop = false;
            this.cboCurrency.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCurrency_KeyPress);
            this.cboCurrency.SelectedValueChanged += new System.EventHandler(this.cboCurrency_SelectedValueChanged);
            // 
            // lblIndentDate
            // 
            this.lblIndentDate.AutoSize = true;
            this.lblIndentDate.BackColor = System.Drawing.Color.Transparent;
            this.lblIndentDate.Location = new System.Drawing.Point(611, 33);
            this.lblIndentDate.Name = "lblIndentDate";
            this.lblIndentDate.Size = new System.Drawing.Size(30, 13);
            this.lblIndentDate.TabIndex = 188;
            this.lblIndentDate.Text = "Date";
            // 
            // dtpTransferDate
            // 
            this.dtpTransferDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpTransferDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTransferDate.Location = new System.Drawing.Point(708, 31);
            this.dtpTransferDate.Name = "dtpTransferDate";
            this.dtpTransferDate.Size = new System.Drawing.Size(101, 20);
            this.dtpTransferDate.TabIndex = 11;
            // 
            // btnTContextmenu
            // 
            this.btnTContextmenu.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnTContextmenu.Location = new System.Drawing.Point(505, 52);
            this.btnTContextmenu.Name = "btnTContextmenu";
            this.btnTContextmenu.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.btnTContextmenu.Size = new System.Drawing.Size(29, 23);
            this.btnTContextmenu.TabIndex = 262;
            this.btnTContextmenu.Text = "6";
            this.btnTContextmenu.UseVisualStyleBackColor = true;
            this.btnTContextmenu.Visible = false;
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrency.Location = new System.Drawing.Point(342, 105);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(49, 13);
            this.lblCurrency.TabIndex = 258;
            this.lblCurrency.Text = "Currency";
            // 
            // lblDueDate
            // 
            this.lblDueDate.AutoSize = true;
            this.lblDueDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDueDate.Location = new System.Drawing.Point(611, 56);
            this.lblDueDate.Name = "lblDueDate";
            this.lblDueDate.Size = new System.Drawing.Size(53, 13);
            this.lblDueDate.TabIndex = 189;
            this.lblDueDate.Text = "Due Date";
            // 
            // tiGeneral
            // 
            this.tiGeneral.AttachedControl = this.tabControlPanel3;
            this.tiGeneral.Name = "tiGeneral";
            this.tiGeneral.Text = "General";
            // 
            // tabControlPanel4
            // 
            this.tabControlPanel4.Controls.Add(this.dgvSuggestions);
            this.tabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel4.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel4.Name = "tabControlPanel4";
            this.tabControlPanel4.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel4.Size = new System.Drawing.Size(1100, 128);
            this.tabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel4.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel4.Style.GradientAngle = 90;
            this.tabControlPanel4.TabIndex = 2;
            this.tabControlPanel4.TabItem = this.tiSuggestions;
            // 
            // dgvSuggestions
            // 
            this.dgvSuggestions.AllowUserToDeleteRows = false;
            this.dgvSuggestions.BackgroundColor = System.Drawing.Color.White;
            this.dgvSuggestions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSuggestions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UserID,
            this.UserName,
            this.Status,
            this.VerifiedDate,
            this.Comment});
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSuggestions.DefaultCellStyle = dataGridViewCellStyle20;
            this.dgvSuggestions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSuggestions.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSuggestions.Location = new System.Drawing.Point(1, 1);
            this.dgvSuggestions.Name = "dgvSuggestions";
            this.dgvSuggestions.Size = new System.Drawing.Size(1098, 126);
            this.dgvSuggestions.TabIndex = 0;
            this.dgvSuggestions.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSuggestions_CellEndEdit);
            // 
            // UserID
            // 
            this.UserID.HeaderText = "UserID";
            this.UserID.Name = "UserID";
            this.UserID.Visible = false;
            // 
            // UserName
            // 
            this.UserName.HeaderText = "User Name";
            this.UserName.Name = "UserName";
            this.UserName.ReadOnly = true;
            this.UserName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.UserName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.UserName.Width = 250;
            // 
            // Status
            // 
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // VerifiedDate
            // 
            this.VerifiedDate.HeaderText = "Verified Date";
            this.VerifiedDate.Name = "VerifiedDate";
            this.VerifiedDate.ReadOnly = true;
            this.VerifiedDate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.VerifiedDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Comment
            // 
            this.Comment.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Comment.HeaderText = "Comment";
            this.Comment.Name = "Comment";
            this.Comment.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Comment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tiSuggestions
            // 
            this.tiSuggestions.AttachedControl = this.tabControlPanel4;
            this.tiSuggestions.Name = "tiSuggestions";
            this.tiSuggestions.Text = "Suggestions";
            this.tiSuggestions.Visible = false;
            // 
            // WarehouseTransferBindingNavigator
            // 
            this.WarehouseTransferBindingNavigator.AccessibleDescription = "DotNetBar Bar (WarehouseTransferBindingNavigator)";
            this.WarehouseTransferBindingNavigator.AccessibleName = "DotNetBar Bar";
            this.WarehouseTransferBindingNavigator.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.WarehouseTransferBindingNavigator.Dock = System.Windows.Forms.DockStyle.Top;
            this.WarehouseTransferBindingNavigator.DockLine = 1;
            this.WarehouseTransferBindingNavigator.DockOffset = 73;
            this.WarehouseTransferBindingNavigator.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.WarehouseTransferBindingNavigator.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.WarehouseTransferBindingNavigator.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorClearItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorCancelItem,
            this.btnActions,
            this.BtnPrint,
            this.BtnEmail});
            this.WarehouseTransferBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.WarehouseTransferBindingNavigator.Name = "WarehouseTransferBindingNavigator";
            this.WarehouseTransferBindingNavigator.Size = new System.Drawing.Size(1100, 25);
            this.WarehouseTransferBindingNavigator.Stretch = true;
            this.WarehouseTransferBindingNavigator.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.WarehouseTransferBindingNavigator.TabIndex = 12;
            this.WarehouseTransferBindingNavigator.TabStop = false;
            this.WarehouseTransferBindingNavigator.Text = "bar2";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlEnter);
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.Tooltip = "Add New Information";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Tooltip = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorClearItem
            // 
            this.BindingNavigatorClearItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorClearItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorClearItem.Image")));
            this.BindingNavigatorClearItem.Name = "BindingNavigatorClearItem";
            this.BindingNavigatorClearItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.BindingNavigatorClearItem.Text = "Clear";
            this.BindingNavigatorClearItem.Tooltip = "Clear";
            this.BindingNavigatorClearItem.Click += new System.EventHandler(this.BindingNavigatorClearItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Tooltip = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorCancelItem
            // 
            this.BindingNavigatorCancelItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorCancelItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorCancelItem.Image")));
            this.BindingNavigatorCancelItem.Name = "BindingNavigatorCancelItem";
            this.BindingNavigatorCancelItem.Text = "Cancel";
            this.BindingNavigatorCancelItem.Tooltip = "Cancel";
            this.BindingNavigatorCancelItem.Click += new System.EventHandler(this.BindingNavigatorCancelItem_Click);
            // 
            // btnActions
            // 
            this.btnActions.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnActions.Image = ((System.Drawing.Image)(resources.GetObject("btnActions.Image")));
            this.btnActions.Name = "btnActions";
            this.btnActions.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.bnExpense,
            this.bnSubmitForApproval,
            this.bnSubmit,
            this.btnApprove,
            this.btnSuggest,
            this.btnDeny,
            this.btnReject});
            this.btnActions.Text = "Actions";
            this.btnActions.Tooltip = "Actions";
            // 
            // bnExpense
            // 
            this.bnExpense.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnExpense.Image = ((System.Drawing.Image)(resources.GetObject("bnExpense.Image")));
            this.bnExpense.Name = "bnExpense";
            this.bnExpense.Text = "Expenses";
            this.bnExpense.Tooltip = "Expenses";
            this.bnExpense.Click += new System.EventHandler(this.bnExpense_Click);
            // 
            // bnSubmitForApproval
            // 
            this.bnSubmitForApproval.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnSubmitForApproval.Image = ((System.Drawing.Image)(resources.GetObject("bnSubmitForApproval.Image")));
            this.bnSubmitForApproval.Name = "bnSubmitForApproval";
            this.bnSubmitForApproval.Text = "Submit For Approval";
            this.bnSubmitForApproval.Tooltip = "Submit For Approval";
            this.bnSubmitForApproval.Visible = false;
            this.bnSubmitForApproval.Click += new System.EventHandler(this.bnSubmitForApproval_Click);
            // 
            // bnSubmit
            // 
            this.bnSubmit.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnSubmit.Image = ((System.Drawing.Image)(resources.GetObject("bnSubmit.Image")));
            this.bnSubmit.Name = "bnSubmit";
            this.bnSubmit.Text = "Submit";
            this.bnSubmit.Tooltip = "Submit";
            this.bnSubmit.Visible = false;
            this.bnSubmit.Click += new System.EventHandler(this.bnSubmit_Click);
            // 
            // btnApprove
            // 
            this.btnApprove.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnApprove.Image = ((System.Drawing.Image)(resources.GetObject("btnApprove.Image")));
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Text = "Approve";
            this.btnApprove.Tooltip = "Approve";
            this.btnApprove.Click += new System.EventHandler(this.btnApprove_Click);
            // 
            // btnSuggest
            // 
            this.btnSuggest.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSuggest.Image = ((System.Drawing.Image)(resources.GetObject("btnSuggest.Image")));
            this.btnSuggest.Name = "btnSuggest";
            this.btnSuggest.Text = "Suggest";
            this.btnSuggest.Tooltip = "Suggest";
            this.btnSuggest.Click += new System.EventHandler(this.btnSuggest_Click);
            // 
            // btnDeny
            // 
            this.btnDeny.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDeny.Image = ((System.Drawing.Image)(resources.GetObject("btnDeny.Image")));
            this.btnDeny.Name = "btnDeny";
            this.btnDeny.Text = "Deny";
            this.btnDeny.Tooltip = "Deny";
            this.btnDeny.Click += new System.EventHandler(this.btnDeny_Click);
            // 
            // btnReject
            // 
            this.btnReject.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnReject.Image = ((System.Drawing.Image)(resources.GetObject("btnReject.Image")));
            this.btnReject.Name = "btnReject";
            this.btnReject.Text = "Reject";
            this.btnReject.Tooltip = "Reject";
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // tmrWarehouseTransfer
            // 
            this.tmrWarehouseTransfer.Enabled = true;
            this.tmrWarehouseTransfer.Interval = 2000;
            // 
            // errProWarehouseTransfer
            // 
            this.errProWarehouseTransfer.ContainerControl = this;
            this.errProWarehouseTransfer.RightToLeft = true;
            // 
            // imgLstWarehouseTransfer
            // 
            this.imgLstWarehouseTransfer.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgLstWarehouseTransfer.ImageStream")));
            this.imgLstWarehouseTransfer.TransparentColor = System.Drawing.Color.Transparent;
            this.imgLstWarehouseTransfer.Images.SetKeyName(0, "Purchase Indent.ICO");
            this.imgLstWarehouseTransfer.Images.SetKeyName(1, "Purchase Order.ico");
            this.imgLstWarehouseTransfer.Images.SetKeyName(2, "Purchase Invoice.ico");
            this.imgLstWarehouseTransfer.Images.SetKeyName(3, "GRN.ICO");
            this.imgLstWarehouseTransfer.Images.SetKeyName(4, "Purchase Order Return.ico");
            // 
            // CMSVendorAddress
            // 
            this.CMSVendorAddress.Name = "ContextMenuStrip1";
            this.CMSVendorAddress.Size = new System.Drawing.Size(61, 4);
            // 
            // tpLocationDetails
            // 
            this.tpLocationDetails.Name = "tpLocationDetails";
            this.tpLocationDetails.Text = "Location Details";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "UserID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "User Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 250;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Status";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Verified Date";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.HeaderText = "Comment";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "StockTransferID";
            this.dataGridViewTextBoxColumn6.HeaderText = "StockTransferID";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Stock TransferNo";
            this.dataGridViewTextBoxColumn7.HeaderText = "Stock TransferNo";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dgvColItemID
            // 
            this.dgvColItemID.HeaderText = "ItemID";
            this.dgvColItemID.Name = "dgvColItemID";
            this.dgvColItemID.Visible = false;
            // 
            // dgvColItemCode
            // 
            this.dgvColItemCode.HeaderText = "Item Code";
            this.dgvColItemCode.Name = "dgvColItemCode";
            // 
            // dgvColItemName
            // 
            this.dgvColItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvColItemName.HeaderText = "Item Name";
            this.dgvColItemName.Name = "dgvColItemName";
            // 
            // dgvColBatchID
            // 
            this.dgvColBatchID.HeaderText = "Batch";
            this.dgvColBatchID.Name = "dgvColBatchID";
            this.dgvColBatchID.Visible = false;
            // 
            // dgvColBatchName
            // 
            this.dgvColBatchName.HeaderText = "Batch";
            this.dgvColBatchName.Name = "dgvColBatchName";
            this.dgvColBatchName.ReadOnly = true;
            this.dgvColBatchName.Visible = false;
            this.dgvColBatchName.Width = 150;
            // 
            // dgvColQty
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColQty.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvColQty.HeaderText = "Quantity";
            this.dgvColQty.MaxInputLength = 21;
            this.dgvColQty.Name = "dgvColQty";
            // 
            // dgvColUom
            // 
            this.dgvColUom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColUom.HeaderText = "Uom";
            this.dgvColUom.Name = "dgvColUom";
            // 
            // dgvColRate
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColRate.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvColRate.HeaderText = "Rate";
            this.dgvColRate.MaxInputLength = 21;
            this.dgvColRate.Name = "dgvColRate";
            this.dgvColRate.ReadOnly = true;
            // 
            // dgvColGrandAmount
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColGrandAmount.DefaultCellStyle = dataGridViewCellStyle16;
            this.dgvColGrandAmount.HeaderText = "Gross Amount";
            this.dgvColGrandAmount.MaxInputLength = 27;
            this.dgvColGrandAmount.Name = "dgvColGrandAmount";
            this.dgvColGrandAmount.ReadOnly = true;
            // 
            // dgvColDiscount
            // 
            this.dgvColDiscount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColDiscount.HeaderText = "Discount";
            this.dgvColDiscount.Name = "dgvColDiscount";
            this.dgvColDiscount.Visible = false;
            // 
            // dgvcolDiscountAmount
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvcolDiscountAmount.DefaultCellStyle = dataGridViewCellStyle17;
            this.dgvcolDiscountAmount.HeaderText = "Discount Amount";
            this.dgvcolDiscountAmount.Name = "dgvcolDiscountAmount";
            this.dgvcolDiscountAmount.ReadOnly = true;
            this.dgvcolDiscountAmount.Visible = false;
            // 
            // dgvColNetAmount
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColNetAmount.DefaultCellStyle = dataGridViewCellStyle18;
            this.dgvColNetAmount.HeaderText = "Net Amount";
            this.dgvColNetAmount.MaxInputLength = 27;
            this.dgvColNetAmount.Name = "dgvColNetAmount";
            this.dgvColNetAmount.ReadOnly = true;
            this.dgvColNetAmount.Visible = false;
            this.dgvColNetAmount.Width = 255;
            // 
            // FrmWarehouseTransfer
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.ClientSize = new System.Drawing.Size(1303, 514);
            this.Controls.Add(this.panelEx1);
            this.Controls.Add(this.dockSite2);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.expandableSplitterLeft);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.dockSite3);
            this.Controls.Add(this.dockSite4);
            this.Controls.Add(this.dockSite5);
            this.Controls.Add(this.dockSite6);
            this.Controls.Add(this.dockSite8);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmWarehouseTransfer";
            this.Text = "Stock Transfer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmWarehouseTransfer_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmWarehouseTransfer_FormClosing);
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvStockTransferDisplay)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            this.panelEx1.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            this.panelRightBottom.ResumeLayout(false);
            this.panelRightBottom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcTransferOrder)).EndInit();
            this.tcTransferOrder.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemDetail)).EndInit();
            this.panelGridBottom.ResumeLayout(false);
            this.panelGridBottom.PerformLayout();
            this.panelExAmount.ResumeLayout(false);
            this.panelExAmount.PerformLayout();
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).EndInit();
            this.tcGeneral.ResumeLayout(false);
            this.tabControlPanel3.ResumeLayout(false);
            this.tabControlPanel3.PerformLayout();
            this.tabControlPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSuggestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WarehouseTransferBindingNavigator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProWarehouseTransfer)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        //#region Windows Form Designer generated code


        public DevComponents.DotNetBar.Command cmdCommandZoom;
        private DevComponents.DotNetBar.DockContainerItem dockContainerItem1;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterLeft;
        private DevComponents.DotNetBar.PanelEx PanelLeft;

        private DevComponents.DotNetBar.DotNetBarManager dotNetBarManager1;
        private DevComponents.DotNetBar.DockSite dockSite4;
        private DevComponents.DotNetBar.DockSite dockSite1;
        private DevComponents.DotNetBar.DockSite dockSite2;
        private DevComponents.DotNetBar.DockSite dockSite3;
        private DevComponents.DotNetBar.DockSite dockSite5;
        private DevComponents.DotNetBar.DockSite dockSite6;
        //private DevComponents.DotNetBar.DockSite dockSite7;
        //private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.DockSite dockSite8;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.Bar WarehouseTransferBindingNavigator;
        private DevComponents.DotNetBar.ButtonItem BindingNavigatorAddNewItem;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterTop;
        private DevComponents.DotNetBar.PanelEx panelTop;
        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.LabelX lblIndentCount;
        private DevComponents.DotNetBar.PanelEx panelRightBottom;
        private DevComponents.DotNetBar.PanelEx panelGridBottom;
        private System.Windows.Forms.Label lblRemarks;
        private DevComponents.DotNetBar.PanelEx panelLeftTop;
        private DevComponents.DotNetBar.Controls.DataGridViewX DgvStockTransferDisplay;
        private System.Windows.Forms.Label LblScompany;
        private ClsInnerGridBar dgvItemDetail;
        private DevComponents.DotNetBar.ButtonItem BindingNavigatorSaveItem;
        private DevComponents.DotNetBar.ButtonItem BindingNavigatorClearItem;
        private DevComponents.DotNetBar.ButtonItem BindingNavigatorDeleteItem;
        private DevComponents.DotNetBar.ButtonX btnSRefresh;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSTransferNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSCompany;
        private System.Windows.Forms.Label lblCompany;
        private System.Windows.Forms.DateTimePicker dtpDueDate;
        private System.Windows.Forms.DateTimePicker dtpTransferDate;
        private System.Windows.Forms.Label lblDueDate;
        private System.Windows.Forms.Label lblIndentDate;
        private System.Windows.Forms.Label lblTransferNo;
        internal System.Windows.Forms.Timer tmrWarehouseTransfer;
        internal System.Windows.Forms.ErrorProvider errProWarehouseTransfer;
        private DevComponents.DotNetBar.ButtonItem BtnPrint;
        private DevComponents.DotNetBar.ButtonItem BtnEmail;
        private System.Windows.Forms.ImageList imgLstWarehouseTransfer;
        private DevComponents.DotNetBar.Controls.TextBoxX txtRemarks;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTransferNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        //private System.Windows.Forms.ComboBox cb;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblDescription;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSStatus;
        private System.Windows.Forms.Label lblSStatus;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSFromWH;
        private System.Windows.Forms.Label lblSFromWh;
        private System.Windows.Forms.Label lblStatusText;
        private DevComponents.DotNetBar.ButtonItem BindingNavigatorCancelItem;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSTransferType;
        private System.Windows.Forms.Label lblSTransferType;
        private System.Windows.Forms.DateTimePicker dtpSFrom;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.DateTimePicker dtpSTo;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDescription;
        private DevComponents.DotNetBar.ButtonItem btnActions;
        private DevComponents.DotNetBar.ButtonItem bnExpense;
        private DevComponents.DotNetBar.ButtonItem bnSubmitForApproval;
        private DevComponents.DotNetBar.ButtonItem btnApprove;
        private DevComponents.DotNetBar.ButtonItem btnReject;
        private System.Windows.Forms.Label lblFromWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboToWarehouse;
        private System.Windows.Forms.Label lblToWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboXFromWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboXTransferType;
        private System.Windows.Forms.Label lblxType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCurrency;
        private System.Windows.Forms.Label lblCurrency;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboPaymentTerms;
        private System.Windows.Forms.Label lblPaymentTerms;
        private DevComponents.DotNetBar.PanelEx panelExAmount;
        private System.Windows.Forms.Label lblAdvancePayment;
        private DevComponents.DotNetBar.Controls.TextBoxX txtAdvPayment;
        private System.Windows.Forms.Label lblTotalAmount;
        private System.Windows.Forms.Label lblDiscount;
        private System.Windows.Forms.Label lblExpenseAmount;
        private System.Windows.Forms.Label lblSubtotal;
        private System.Windows.Forms.Label lblCurrencyAmnt;
        private DevComponents.DotNetBar.Controls.TextBoxX txtExchangeCurrencyRate;
        private DevComponents.DotNetBar.ButtonX btnAddExpense;
        private DevComponents.DotNetBar.Controls.TextBoxX txtExpenseAmount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDiscount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSubtotal;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDiscount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTotalAmount;
        private DevComponents.DotNetBar.ButtonX btnAddressChange;
        private System.Windows.Forms.Label lblVendorAddress;
        private System.Windows.Forms.Label lblCustomer;
        public DevComponents.DotNetBar.Controls.TextBoxX txtVendorAddress;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboXVendor;
        private DevComponents.DotNetBar.ButtonX btnSupplier;
        internal System.Windows.Forms.Button btnTContextmenu;
        private NumericTextBox txtLeadTime;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboIncoTerms;
        private System.Windows.Forms.Label lblIncoTerms;
        private System.Windows.Forms.Label lblLeadTime;
        private DevComponents.DotNetBar.ButtonX btnIncoTerms;
        internal System.Windows.Forms.ContextMenuStrip CMSVendorAddress;
        private DevComponents.DotNetBar.ButtonX BtnTVenderAddress;
        private System.Windows.Forms.Label lblClearenceDate;
        private System.Windows.Forms.DateTimePicker dtpTrnsShipmentDate;
        private System.Windows.Forms.DateTimePicker dtpClearenceDate;
        private System.Windows.Forms.DateTimePicker dtpProductionDate;
        private System.Windows.Forms.Label lblProductionDate;
        private System.Windows.Forms.Label lblTransShipmentDate;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboxIncharge;
        private System.Windows.Forms.Label lblXIncharge;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSToRefernce;
        private System.Windows.Forms.Label lblSDestination;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSIncharge;
        private System.Windows.Forms.Label lblSIncharge;
        private System.Windows.Forms.Label lblSStockTransferNo;
        private DevComponents.DotNetBar.ButtonItem btnSuggest;
        private DevComponents.DotNetBar.ButtonItem btnDeny;
        private DevComponents.DotNetBar.ButtonItem bnSubmit;
        private System.Windows.Forms.Panel pnlBottom;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedByText;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedDateValue;
        private DevComponents.DotNetBar.Controls.WarningBox lblWarehouseTransferStatus;
        private DevComponents.DotNetBar.TabControl tcGeneral;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel3;
        private DevComponents.DotNetBar.TabItem tiGeneral;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel4;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvSuggestions;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn VerifiedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comment;
        private DevComponents.DotNetBar.TabItem tiSuggestions;
        private DevComponents.DotNetBar.TabControl tcTransferOrder;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private DevComponents.DotNetBar.TabItem tpItemDetails;
        private DevComponents.DotNetBar.TabItem tpLocationDetails;
        private System.Windows.Forms.Label lblAmountIn;
        private System.Windows.Forms.Label lblAmountInWords;
        private System.Windows.Forms.LinkLabel lnkLabelAdvanceSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColSStockTransferID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColSStockTransferNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColBatchID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColBatchName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColQty;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColUom;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColGrandAmount;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColDiscount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcolDiscountAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColNetAmount;

    }
}
