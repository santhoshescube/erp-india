﻿namespace MyBooksERP
{
    partial class FrmOpeningStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOpeningStock));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.lblDate = new System.Windows.Forms.Label();
            this.cboWarehouse = new System.Windows.Forms.ComboBox();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.pnlOpeningStock = new System.Windows.Forms.Panel();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.txtNetAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnWarehouse = new System.Windows.Forms.Button();
            this.dgvOpeningStockDetails = new ClsInnerGrid();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpiryDate = new DemoClsDataGridview.CalendarColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOM = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemStatusID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OpeningStockBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bnMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bnMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bnPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnClear = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.btnProduct = new System.Windows.Forms.ToolStripButton();
            this.bnPrint = new System.Windows.Forms.ToolStripButton();
            this.bnEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnHelp = new System.Windows.Forms.ToolStripButton();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tmrOpeningStock = new System.Windows.Forms.Timer(this.components);
            this.ErrOpeningStock = new System.Windows.Forms.ErrorProvider(this.components);
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.toolStripStatusLabel11 = new DevComponents.DotNetBar.LabelItem();
            this.lblStatus = new DevComponents.DotNetBar.LabelItem();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlOpeningStock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOpeningStockDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OpeningStockBindingNavigator)).BeginInit();
            this.OpeningStockBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrOpeningStock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Enabled = false;
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(346, 18);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(104, 20);
            this.dtpDate.TabIndex = 2;
            this.dtpDate.ValueChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(310, 22);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 197;
            this.lblDate.Text = "Date";
            // 
            // cboWarehouse
            // 
            this.cboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWarehouse.BackColor = System.Drawing.SystemColors.Info;
            this.cboWarehouse.DropDownHeight = 75;
            this.cboWarehouse.FormattingEnabled = true;
            this.cboWarehouse.IntegralHeight = false;
            this.cboWarehouse.Location = new System.Drawing.Point(78, 17);
            this.cboWarehouse.Name = "cboWarehouse";
            this.cboWarehouse.Size = new System.Drawing.Size(188, 21);
            this.cboWarehouse.TabIndex = 0;
            this.cboWarehouse.SelectedIndexChanged += new System.EventHandler(this.cboWarehouse_SelectedIndexChanged);
            this.cboWarehouse.SelectedValueChanged += new System.EventHandler(this.ChangeStatus);
            this.cboWarehouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(10, 20);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(62, 13);
            this.lblWarehouse.TabIndex = 196;
            this.lblWarehouse.Text = "Warehouse";
            // 
            // pnlOpeningStock
            // 
            this.pnlOpeningStock.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOpeningStock.Controls.Add(this.lblTotalAmount);
            this.pnlOpeningStock.Controls.Add(this.txtNetAmount);
            this.pnlOpeningStock.Controls.Add(this.btnWarehouse);
            this.pnlOpeningStock.Controls.Add(this.dgvOpeningStockDetails);
            this.pnlOpeningStock.Controls.Add(this.cboWarehouse);
            this.pnlOpeningStock.Controls.Add(this.lblWarehouse);
            this.pnlOpeningStock.Controls.Add(this.lblDate);
            this.pnlOpeningStock.Controls.Add(this.dtpDate);
            this.pnlOpeningStock.Location = new System.Drawing.Point(4, 28);
            this.pnlOpeningStock.Name = "pnlOpeningStock";
            this.pnlOpeningStock.Size = new System.Drawing.Size(863, 387);
            this.pnlOpeningStock.TabIndex = 101;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.BackColor = System.Drawing.Color.White;
            this.lblTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(601, 370);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(45, 9);
            this.lblTotalAmount.TabIndex = 269;
            this.lblTotalAmount.Text = "Net Amount";
            // 
            // txtNetAmount
            // 
            this.txtNetAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtNetAmount.Border.Class = "";
            this.txtNetAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtNetAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNetAmount.Location = new System.Drawing.Point(599, 352);
            this.txtNetAmount.MaxLength = 24;
            this.txtNetAmount.Name = "txtNetAmount";
            this.txtNetAmount.ReadOnly = true;
            this.txtNetAmount.Size = new System.Drawing.Size(258, 28);
            this.txtNetAmount.TabIndex = 268;
            this.txtNetAmount.TabStop = false;
            this.txtNetAmount.Text = "0.000";
            this.txtNetAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnWarehouse
            // 
            this.btnWarehouse.Location = new System.Drawing.Point(270, 17);
            this.btnWarehouse.Name = "btnWarehouse";
            this.btnWarehouse.Size = new System.Drawing.Size(26, 21);
            this.btnWarehouse.TabIndex = 1;
            this.btnWarehouse.Text = "...";
            this.btnWarehouse.UseVisualStyleBackColor = true;
            this.btnWarehouse.Visible = false;
            this.btnWarehouse.Click += new System.EventHandler(this.btnWarehouse_Click);
            // 
            // dgvOpeningStockDetails
            // 
            this.dgvOpeningStockDetails.AddNewRow = false;
            this.dgvOpeningStockDetails.AlphaNumericCols = new int[0];
            this.dgvOpeningStockDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvOpeningStockDetails.CapsLockCols = new int[0];
            this.dgvOpeningStockDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOpeningStockDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.ItemID,
            this.BatchNo,
            this.BatchID,
            this.ExpiryDate,
            this.Quantity,
            this.UOM,
            this.Rate,
            this.Total,
            this.ItemStatusID});
            this.dgvOpeningStockDetails.DecimalCols = new int[0];
            this.dgvOpeningStockDetails.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvOpeningStockDetails.HasSlNo = false;
            this.dgvOpeningStockDetails.LastRowIndex = 0;
            this.dgvOpeningStockDetails.Location = new System.Drawing.Point(4, 52);
            this.dgvOpeningStockDetails.Name = "dgvOpeningStockDetails";
            this.dgvOpeningStockDetails.NegativeValueCols = new int[0];
            this.dgvOpeningStockDetails.NumericCols = new int[0];
            this.dgvOpeningStockDetails.RowHeadersWidth = 55;
            this.dgvOpeningStockDetails.Size = new System.Drawing.Size(853, 296);
            this.dgvOpeningStockDetails.TabIndex = 3;
            this.dgvOpeningStockDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOpeningStockDetails_CellValueChanged);
            this.dgvOpeningStockDetails.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvOpeningStockDetails_CellBeginEdit);
            this.dgvOpeningStockDetails.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvOpeningStockDetails_RowsAdded);
            this.dgvOpeningStockDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOpeningStockDetails_CellEndEdit);
            this.dgvOpeningStockDetails.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvOpeningStockDetails_Textbox_TextChanged);
            this.dgvOpeningStockDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvOpeningStockDetails_EditingControlShowing);
            this.dgvOpeningStockDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvOpeningStockDetails_CurrentCellDirtyStateChanged);
            this.dgvOpeningStockDetails.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOpeningStockDetails_CellEnter);
            this.dgvOpeningStockDetails.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvOpeningStockDetails_RowsRemoved);
            // 
            // ItemCode
            // 
            this.ItemCode.HeaderText = "ItemCode";
            this.ItemCode.MaxInputLength = 20;
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.Width = 60;
            // 
            // ItemName
            // 
            this.ItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ItemName.HeaderText = "ItemName";
            this.ItemName.MaxInputLength = 200;
            this.ItemName.Name = "ItemName";
            this.ItemName.Width = 231;
            // 
            // ItemID
            // 
            this.ItemID.HeaderText = "ItemID";
            this.ItemID.Name = "ItemID";
            this.ItemID.Visible = false;
            // 
            // BatchNo
            // 
            this.BatchNo.HeaderText = "BatchNo";
            this.BatchNo.MaxInputLength = 50;
            this.BatchNo.Name = "BatchNo";
            this.BatchNo.Visible = false;
            // 
            // BatchID
            // 
            this.BatchID.HeaderText = "BatchID";
            this.BatchID.Name = "BatchID";
            this.BatchID.Visible = false;
            // 
            // ExpiryDate
            // 
            this.ExpiryDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle1.NullValue = null;
            this.ExpiryDate.DefaultCellStyle = dataGridViewCellStyle1;
            this.ExpiryDate.HeaderText = "ExpiryDate";
            this.ExpiryDate.Name = "ExpiryDate";
            this.ExpiryDate.Visible = false;
            this.ExpiryDate.Width = 80;
            // 
            // Quantity
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle2;
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.MaxInputLength = 10;
            this.Quantity.Name = "Quantity";
            this.Quantity.Width = 70;
            // 
            // UOM
            // 
            this.UOM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UOM.HeaderText = "UOM";
            this.UOM.Name = "UOM";
            this.UOM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.UOM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.UOM.Width = 70;
            // 
            // Rate
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.Rate.DefaultCellStyle = dataGridViewCellStyle3;
            this.Rate.HeaderText = "Rate";
            this.Rate.MaxInputLength = 10;
            this.Rate.Name = "Rate";
            this.Rate.Width = 85;
            // 
            // Total
            // 
            this.Total.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.Total.DefaultCellStyle = dataGridViewCellStyle4;
            this.Total.HeaderText = "Total";
            this.Total.MaxInputLength = 13;
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // ItemStatusID
            // 
            this.ItemStatusID.HeaderText = "ItemStatusID";
            this.ItemStatusID.Name = "ItemStatusID";
            this.ItemStatusID.Visible = false;
            // 
            // OpeningStockBindingNavigator
            // 
            this.OpeningStockBindingNavigator.AddNewItem = null;
            this.OpeningStockBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.OpeningStockBindingNavigator.CountItem = this.bnCountItem;
            this.OpeningStockBindingNavigator.DeleteItem = null;
            this.OpeningStockBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnMoveFirstItem,
            this.bnMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bnPositionItem,
            this.bnCountItem,
            this.bindingNavigatorSeparator1,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnClear,
            this.bnDeleteItem,
            this.ToolStripSeparator,
            this.btnProduct,
            this.bnPrint,
            this.bnEmail,
            this.toolStripSeparator1,
            this.bnHelp});
            this.OpeningStockBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.OpeningStockBindingNavigator.MoveFirstItem = null;
            this.OpeningStockBindingNavigator.MoveLastItem = null;
            this.OpeningStockBindingNavigator.MoveNextItem = null;
            this.OpeningStockBindingNavigator.MovePreviousItem = null;
            this.OpeningStockBindingNavigator.Name = "OpeningStockBindingNavigator";
            this.OpeningStockBindingNavigator.PositionItem = this.bnPositionItem;
            this.OpeningStockBindingNavigator.Size = new System.Drawing.Size(871, 25);
            this.OpeningStockBindingNavigator.TabIndex = 0;
            this.OpeningStockBindingNavigator.Text = "bindingNavigator1";
            // 
            // bnCountItem
            // 
            this.bnCountItem.Name = "bnCountItem";
            this.bnCountItem.Size = new System.Drawing.Size(35, 22);
            this.bnCountItem.Text = "of {0}";
            this.bnCountItem.ToolTipText = "Total number of items";
            // 
            // bnMoveFirstItem
            // 
            this.bnMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
            this.bnMoveFirstItem.Name = "bnMoveFirstItem";
            this.bnMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveFirstItem.Text = "Move first";
            this.bnMoveFirstItem.Click += new System.EventHandler(this.bnMoveFirstItem_Click);
            // 
            // bnMovePreviousItem
            // 
            this.bnMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem.Image")));
            this.bnMovePreviousItem.Name = "bnMovePreviousItem";
            this.bnMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bnMovePreviousItem.Text = "Move previous";
            this.bnMovePreviousItem.Click += new System.EventHandler(this.bnMovePreviousItem_Click);
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPositionItem
            // 
            this.bnPositionItem.AccessibleName = "Position";
            this.bnPositionItem.AutoSize = false;
            this.bnPositionItem.Name = "bnPositionItem";
            this.bnPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bnPositionItem.Text = "0";
            this.bnPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoveNextItem
            // 
            this.bnMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
            this.bnMoveNextItem.Name = "bnMoveNextItem";
            this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveNextItem.Text = "Move next";
            this.bnMoveNextItem.Click += new System.EventHandler(this.bnMoveNextItem_Click);
            // 
            // bnMoveLastItem
            // 
            this.bnMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
            this.bnMoveLastItem.Name = "bnMoveLastItem";
            this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveLastItem.Text = "Move last";
            this.bnMoveLastItem.Click += new System.EventHandler(this.bnMoveLastItem_Click);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bnAddNewItem.ToolTipText = "Add new Item";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bnSaveItem.ToolTipText = "Save Item";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnClear
            // 
            this.bnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.bnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnClear.Name = "bnClear";
            this.bnClear.Size = new System.Drawing.Size(23, 22);
            this.bnClear.ToolTipText = "Clear";
            this.bnClear.Visible = false;
            this.bnClear.Click += new System.EventHandler(this.bnClear_Click);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bnDeleteItem.Image")));
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteItem.ToolTipText = "Delete Item";
            this.bnDeleteItem.Click += new System.EventHandler(this.bnDeleteItem_Click);
            // 
            // ToolStripSeparator
            // 
            this.ToolStripSeparator.Name = "ToolStripSeparator";
            this.ToolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // btnProduct
            // 
            this.btnProduct.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnProduct.Image = global::MyBooksERP.Properties.Resources.Product;
            this.btnProduct.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnProduct.Name = "btnProduct";
            this.btnProduct.Size = new System.Drawing.Size(23, 22);
            this.btnProduct.Text = "Product";
            this.btnProduct.Visible = false;
            this.btnProduct.Click += new System.EventHandler(this.btnProduct_Click);
            // 
            // bnPrint
            // 
            this.bnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.bnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(23, 22);
            this.bnPrint.ToolTipText = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // bnEmail
            // 
            this.bnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.bnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Size = new System.Drawing.Size(23, 22);
            this.bnEmail.ToolTipText = "Email";
            this.bnEmail.Click += new System.EventHandler(this.bnEmail_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnHelp
            // 
            this.bnHelp.Enabled = false;
            this.bnHelp.Image = ((System.Drawing.Image)(resources.GetObject("bnHelp.Image")));
            this.bnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnHelp.Name = "bnHelp";
            this.bnHelp.Size = new System.Drawing.Size(23, 22);
            this.bnHelp.ToolTipText = "Help";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(706, 421);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(12, 421);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(787, 421);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tmrOpeningStock
            // 
            this.tmrOpeningStock.Tick += new System.EventHandler(this.tmrOpeningStock_Tick);
            // 
            // ErrOpeningStock
            // 
            this.ErrOpeningStock.ContainerControl = this;
            this.ErrOpeningStock.RightToLeft = true;
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.toolStripStatusLabel11,
            this.lblStatus});
            this.bar1.Location = new System.Drawing.Point(0, 449);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(871, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 102;
            this.bar1.TabStop = false;
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Text = "Status:";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ItemCode";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 60;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "ItemName";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "BatchNo";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "BatchID";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn6.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 80;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn7.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 80;
            // 
            // FrmOpeningStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(871, 468);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.OpeningStockBindingNavigator);
            this.Controls.Add(this.pnlOpeningStock);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmOpeningStock";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Opening Stock Entry";
            this.Load += new System.EventHandler(this.FrmOpeningStockEntry_Load);
            this.Shown += new System.EventHandler(this.FrmOpeningStockEntry_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmOpeningStockEntry_FormClosing);
            this.pnlOpeningStock.ResumeLayout(false);
            this.pnlOpeningStock.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOpeningStockDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OpeningStockBindingNavigator)).EndInit();
            this.OpeningStockBindingNavigator.ResumeLayout(false);
            this.OpeningStockBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrOpeningStock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.ComboBox cboWarehouse;
        private System.Windows.Forms.Label lblWarehouse;
        private System.Windows.Forms.Panel pnlOpeningStock;
        private System.Windows.Forms.BindingNavigator OpeningStockBindingNavigator;
        private System.Windows.Forms.ToolStripLabel bnCountItem;
        private System.Windows.Forms.ToolStripButton bnMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bnMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bnPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bnMoveNextItem;
        private System.Windows.Forms.ToolStripButton bnMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton bnAddNewItem;
        internal System.Windows.Forms.ToolStripButton bnSaveItem;
        internal System.Windows.Forms.ToolStripButton bnDeleteItem;
        private System.Windows.Forms.ToolStripButton bnClear;
        private System.Windows.Forms.ToolStripSeparator ToolStripSeparator;
        private System.Windows.Forms.ToolStripButton bnPrint;
        private System.Windows.Forms.ToolStripButton bnEmail;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton bnHelp;
        private ClsInnerGrid dgvOpeningStockDetails;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Timer tmrOpeningStock;
        private System.Windows.Forms.Button btnWarehouse;
        internal System.Windows.Forms.ErrorProvider ErrOpeningStock;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem toolStripStatusLabel11;
        private DevComponents.DotNetBar.LabelItem lblStatus;
        private System.Windows.Forms.ToolStripButton btnProduct;
        private System.Windows.Forms.Label lblTotalAmount;
        private DevComponents.DotNetBar.Controls.TextBoxX txtNetAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchID;
        private DemoClsDataGridview.CalendarColumn ExpiryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewComboBoxColumn UOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemStatusID;
    }
}