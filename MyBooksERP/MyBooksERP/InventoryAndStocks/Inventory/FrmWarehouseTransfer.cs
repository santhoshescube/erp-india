using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data;
using System.Data.SqlClient;

using Microsoft.ReportingServices.ReportRendering;
using System.Collections.Generic;


/* 
================================================= 
   Author:		<Author,,Arun>
   Create date: <Create Date,,08 nov 2011>
   Description:	<Description,,Stock Transfer Transfer>
================================================
*/
namespace MyBooksERP
{
    public partial class FrmWarehouseTransfer : DevComponents.DotNetBar.Office2007Form
    {
        //FormID=21 -Messages-1500 to 1550

        private bool MblnChangeStatus;                    //Check state of the page
        private bool MblnAddStatus;                       //Status for Addition/Updation mode 

        private bool MblnPrintEmailPermission = true;     // Set View Permission
        private bool MblnAddPermission = true;           //Set Add Permission
        private bool MblnUpdatePermission = true;        //Set Update Permission
        private bool MblnDeletePermission = true;           //Set Delete Permission
        private bool MblnAddUpdatePermission = true;        //Set Add Update Permission
        private bool MblnCancelPermission = true;           // Set Cancel Permission
        //private bool MblnSearchFlag = false;               //  Flag for identifying SearchMode /Addition Mode
        //private bool MBlnIsFromCancellation = false;      // Set To Check Save Is From Button Cancellation
        //  variable for assigning message
        private bool MblnIsFrmApproval = false;
        private bool MblnIsEditable;
        // private int MintCompanyId;
        //private int MintStockTransferID = 0;
        private int MintVendorAddID;
        private int MintExchangeCurrencyScale;
        private int MintBaseCurrencyScale;

        private string MstrMessageCommon;
        private string MstrMessageCaption;
        private MessageBoxIcon MmessageIcon;
        private ArrayList MaMessageArr;                        // Error Message display

        public Int64 PintStockTransferIDToLoad;                 // From Approval Form
        public int PintApprovePermission;                      // For Approval permission
        clsBLLWarehouseTransfer MobjclsBLLWarehouseTransfer;
        ClsLogWriter MObjLogs;                               //  Object for Class Clslogs
        ClsNotification mObjNotification;                   //  Object for Class ClsNotification
        clsBLLCommonUtility MobjClsCommonUtility;



        public FrmWarehouseTransfer()
        {
            //Constructor
            InitializeComponent();
            MObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MobjclsBLLWarehouseTransfer = new clsBLLWarehouseTransfer();
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            MobjClsCommonUtility = new clsBLLCommonUtility();
        }

        private void LoadMessage()
        {
            try
            {
                //Loading Message Function  Messages-1500 to 1550
                MaMessageArr = new ArrayList();
                MaMessageArr = mObjNotification.FillMessageArray((int)FormID.StockTransfer, ClsCommonSettings.ProductID);

            }
            catch (Exception Ex)
            {

                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());
            }
        }
        private void SetPermissions()
        {
            try
            {// Function for setting permissions
                clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
                if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, (Int32)eMenuID.StockTransfer, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                    //DataTable datTemp = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "*","STRoleFieldsDetails","RoleID=" +ClsCommonSettings.RoleID + " AND " +
                    //    "CompanyID=" + ClsCommonSettings.CompanyID + " AND ControlID=" + (int)ControlOperationType.stTranCancel });
                    //if (datTemp != null)
                    //    if (datTemp.Rows.Count > 0)
                    //        MblnCancelPermission = Convert.ToBoolean(datTemp.Rows[0]["IsVisible"].ToString());
                    if (MblnAddPermission == true || MblnUpdatePermission == true)
                        MblnAddUpdatePermission = true;

                    //DataTable DtPermission = objClsBLLPermissionSettings.GetMenuPermissions(ClsCommonSettings.RoleID);
                    //if (DtPermission.Rows.Count == 0)
                    //{
                    //    BtnItem.Enabled = BtnUom.Enabled = false;
                    //}
                    //else
                    //{
                    //    DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)MenuID.Products).ToString();
                    //    //BtnItem.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);
                    //    DtPermission.DefaultView.RowFilter = "MenuID =" + ((int)MenuID.Uom).ToString();
                    //    //BtnUom.Enabled = (DtPermission.DefaultView.ToTable().Rows.Count > 0);
                    //}
                }
                else
                    MblnAddPermission = MblnAddUpdatePermission = MblnCancelPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

                MblnCancelPermission = MblnUpdatePermission;

                //BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                //BindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
                //BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                //BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
                //if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                //{
                // 
                //}
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());
            }
        }


        private int CheckDuplicationInGrid()
        {
            // Function For Checking Duplication In Grid
            //string SearchValuefirst = "";
            //int RowIndexTemp = 0;

            //foreach (DataGridViewRow rowValue in dgvItemDetail.Rows)
            //{
            //    if (rowValue.Cells[dgvColItemID.Index].Value != null)
            //    {
            //        SearchValuefirst = Convert.ToString(rowValue.Cells[dgvColItemID.Index].Value);
            //        RowIndexTemp = rowValue.Index;
            //        foreach (DataGridViewRow row in dgvItemDetail.Rows)
            //        {
            //            if (RowIndexTemp != row.Index)
            //            {
            //                if (row.Cells[dgvColItemID.Index].Value != null)
            //                {
            //                    if ((Convert.ToString(row.Cells[dgvColItemID.Index].Value).Trim() == Convert.ToString(SearchValuefirst).Trim()))
            //                        return row.Index;
            //                }
            //            }
            //        }
            //    }
            //}
            //return -1;

            int intItemID = 0;
            long lngBatchID = 0;
            int RowIndexTemp = 0;

            foreach (DataGridViewRow rowValue in dgvItemDetail.Rows)
            {
                if (rowValue.Cells[dgvColItemID.Index].Value != null && rowValue.Cells[dgvColBatchID.Index].Value != null)
                {
                    intItemID = Convert.ToInt32(rowValue.Cells[dgvColItemID.Index].Value);
                    lngBatchID = rowValue.Cells[dgvColBatchID.Index].Value.ToInt64();
                    RowIndexTemp = rowValue.Index;
                    foreach (DataGridViewRow row in dgvItemDetail.Rows)
                    {
                        if (RowIndexTemp != row.Index)
                        {
                            if (row.Cells[dgvColItemID.Index].Value != null && row.Cells[dgvColBatchID.Index].Value.ToInt64() != 0)
                            {
                                if ((Convert.ToInt32(row.Cells[dgvColItemID.Index].Value) == intItemID) && (row.Cells[dgvColBatchID.Index].Value.ToInt64() == lngBatchID))
                                    return row.Index;
                            }
                        }
                    }
                }
            }
            return -1;
        }

        private bool LoadCombos(int intType)
        {
            try
            {

                DataTable dtControlsPermissions = new DataTable();
                clsBLLPermissionSettings objBllPermissionSettings = new clsBLLPermissionSettings();
                bool blnIsEnabled = false;

                DataTable datCombos = new DataTable();
                if (intType == 0 || intType == 1)//Compny Combo
                {
                    datCombos = null;
                    cboCompany.DataSource = null;
                    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID + "" });
                    
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;
                }
                if (intType == 0 || intType == 10)//Company
                {
                    int intTempMenuID = 0;
                    int intTempCompany = 0;
                    datCombos = null;
                    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID + "" });

                    cboSCompany.DataSource = null;
                    cboSCompany.ValueMember = "CompanyID";
                    cboSCompany.DisplayMember = "CompanyName";
                    cboSCompany.DataSource = datCombos;
                }
                if (intType == 0 || intType == 2)//stock transfer type
                {
                    datCombos = null;
                    cboXTransferType.DataSource = null;
                    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "OrderTypeID,OrderType", "CommonOrderTypeReference", " OperationTypeID=" + Convert.ToInt32(OperationType.StockTransfer) + "" });
                    
                    cboXTransferType.ValueMember = "OrderTypeID";
                    cboXTransferType.DisplayMember = "OrderType";
                    cboXTransferType.DataSource = datCombos;
                }
                if (intType == 0 || intType == 3)//From Warehouse combo
                {
                    datCombos = null;
                    cboXFromWarehouse.DataSource = null;
                    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "Distinct InvWarehouse.WarehouseID,WarehouseName", "InvWarehouse INNER JOIN InvWarehouseCompanyDetails WC ON InvWarehouse.WarehouseID=WC.WarehouseID", "WC.CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue) + " And IsActive = 1" });

                    cboXFromWarehouse.ValueMember = "WarehouseID";
                    cboXFromWarehouse.DisplayMember = "WarehouseName";
                    cboXFromWarehouse.DataSource = datCombos;
                }


                if (intType == 0 || intType == 4)//to Currency Combo
                {
                    datCombos = null;
                    cboCurrency.DataSource = null;
                    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "distinct CR.CurrencyID,CR.CurrencyName", "CurrencyReference CR INNER JOIN CurrencyDetails CD ON CR.CurrencyID=CD.CurrencyID", "CD.CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) });
                    cboCurrency.ValueMember = "CurrencyID";
                    cboCurrency.DisplayMember = "CurrencyName";
                    cboCurrency.DataSource = datCombos;
                }
                if (intType == 0 || intType == 5)//to Vendor Combo
                {
                    datCombos = null;
                    cboXVendor.DataSource = null;
                    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "V.VendorID,V.VendorName", "InvVendorInformation V INNER JOIN InvVendorCompanyDetails VC ON V.VendorID = VC.VendorID ", " V.VendorTypeID = " + (int)VendorType.Supplier + " AND VC.CompanyID = " + ClsCommonSettings.CompanyID + "  and V.StatusID=" + (int)VendorStatus.SupplierActive + "" });
                    cboXVendor.ValueMember = "VendorID";
                    cboXVendor.DisplayMember = "VendorName";
                    cboXVendor.DataSource = datCombos;
                }
                if (intType == 6)//to To warehouse Combo when Order Type is warehouse to employee
                {
                    datCombos = null;
                    cboToWarehouse.DataSource = null;
                    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "EmployeeID,EmployeeNumber+'-'+EmployeeFullName as EmployeeName", "EmployeeMaster", "" });
                    cboToWarehouse.ValueMember = "EmployeeID";
                    cboToWarehouse.DisplayMember = "EmployeeName";
                    cboToWarehouse.DataSource = datCombos;
                }
                if (intType == 0 || intType == 7)//Fill Inco Terms
                {
                }
                if (intType == 0 || intType == 8)//Fill Payment Terms
                {
                    datCombos = null;
                    cboPaymentTerms.DataSource = null;
                    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "PaymentTermsID,TermsName", "InvPaymentTerms", "" });
                    cboPaymentTerms.ValueMember = "PaymentTermsID";
                    cboPaymentTerms.DisplayMember = "TermsName";
                    cboPaymentTerms.DataSource = datCombos;
                }

                if (intType == 0 || intType == 9)//Discount 
                {
                    //if (MblnIsEditable)
                    //{
                    //    decimal decMaxSlab = 0;
                    //    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "IsNull(Max(MinSlab),0) as MinSlab", "InvDiscountReference", "DiscountMode in ('N') And IsSaleType = 'True' And IsActive = 1 and IsDiscountForAmount=1 And (IsPeriodApplicable = 0 or (IsPeriodApplicable = 1 And '" + dtpTransferDate.Value.ToString("dd-MMM-yyyy") + "' >= convert(datetime,convert(varchar,PeriodFrom,106)) And '" + dtpTransferDate.Value.ToString("dd-MMM-yyyy") + "' <= convert(datetime,convert(varchar,PeriodTo,106)))) And (IsSlabApplicable = 0 Or (IsSlabApplicable = 1 And " + txtSubtotal.Text.ToDecimal() + " >= MinSlab)) And CurrencyID = " + cboCurrency.SelectedValue.ToInt32() + " " });
                    //    if (datCombos.Rows.Count > 0)
                    //        decMaxSlab = datCombos.Rows[0]["MinSlab"].ToDecimal();
                    //    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "DiscountID,DiscountName", "InvDiscountReference", "DiscountMode in ('N') And IsSaleType = 'True' And IsActive = 1 and IsDiscountForAmount=1 And (IsPeriodApplicable = 0 or (IsPeriodApplicable = 1 And '" + dtpTransferDate.Value.ToString("dd-MMM-yyyy") + "' >= convert(datetime,convert(varchar,PeriodFrom,106)) And '" + dtpTransferDate.Value.ToString("dd-MMM-yyyy") + "' <= convert(datetime,convert(varchar,PeriodTo,106)))) And (IsSlabApplicable = 0 Or (IsSlabApplicable = 1 And " + txtSubtotal.Text.ToDecimal() + " >= MinSlab) And MinSlab >= " + decMaxSlab + " And CurrencyID = " + cboCurrency.SelectedValue.ToInt32() + ") And case PercentOrAmount When 0 Then CurrencyID Else " + cboCurrency.SelectedValue.ToInt32() + " End = " + cboCurrency.SelectedValue.ToInt32() + "" });
                    //}
                    //else
                    //{
                    //    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "DiscountID,DiscountName", "InvDiscountReference", "DiscountMode in ('N') And IsSaleType = 'True' And IsActive = 1 and IsDiscountForAmount=1 " });
                    //}


                    ////datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "DiscountTypeID,DiscountShortName", "STDiscountTypeReference", "DiscountMode in ('N') And IsSaleType = 'False' And Active = 1 and DiscountForAmount=1 ", "DiscountTypeID", "DiscountShortName" });
                    //cboDiscount.DataSource = null;
                    //cboDiscount.ValueMember = "DiscountID";
                    //cboDiscount.DisplayMember = "DiscountName";
                    //DataRow dr = datCombos.NewRow();
                    //dr["DiscountID"] = 0;
                    //dr["DiscountName"] = "No Discount";
                    //datCombos.Rows.InsertAt(dr, 0);
                    //cboDiscount.DataSource = datCombos;
                }
                //-------------------Search Combos
                
                if (intType == 11)//Transfer Ordertype
                {

                    datCombos = null;
                    
                  //  if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                        datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "Distinct STM.OrderTypeID,SCOT.OrderType",
                        " InvStockTransferMaster STM inner Join CommonOrderTypeReference SCOT on STM.OrderTypeID=SCOT.OrderTypeID And SCOT.OperationTypeID=" + Convert.ToInt32(OperationType.StockTransfer) + " ",
                            "STM.CompanyID= "+ Convert.ToInt32(cboSCompany.SelectedValue)+""});
                    //else
                    //{
                    //    try
                    //    { datCombos = MobjclsBLLWarehouseTransfer.GetOperationTypeByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.StTranType); }
                    //    catch { }
                    //}
                    cboSTransferType.DataSource = null;
                    cboSTransferType.ValueMember = "OrderTypeID";
                    cboSTransferType.DisplayMember = "OrderType";
                    cboSTransferType.DataSource = datCombos;

                }
                if (intType == 12)// From Warehosue
                {

                    datCombos = null;
                    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "Distinct STM.WarehouseID,STW.WarehouseName", "InvStockTransferMaster STM inner Join InvWarehouse STW on STM.WarehouseID=STW.WarehouseID",
                                    " STM.CompanyID= "+Convert.ToInt32(cboSCompany.SelectedValue)+" AND STM.OrderTypeID=" +Convert.ToInt32(cboSTransferType.SelectedValue)+ "" });
                    cboSFromWH.DataSource = null;
                    cboSFromWH.ValueMember = "WarehouseID";
                    cboSFromWH.DisplayMember = "WarehouseName";
                    cboSFromWH.DataSource = datCombos;

                }
                if (intType == 13)// ToWarehosue
                {
                    if (cboSTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToDepartment))
                    {
                        datCombos = null;
                        datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "Distinct STM.ReferenceID,DR.DepartmentName", "InvStockTransferMaster STM inner Join DepartmentReference DR on STM.ReferenceID=DR.DepartmentID",
                                    " STM.CompanyID= "+Convert.ToInt32(cboSCompany.SelectedValue)+" AND STM.OrderTypeID=" +Convert.ToInt32(cboSTransferType.SelectedValue)+ "" });
                        cboSToRefernce.DataSource = null;
                        cboSToRefernce.ValueMember = "ReferenceID";
                        cboSToRefernce.DisplayMember = "DepartmentName";
                        cboSToRefernce.DataSource = datCombos;
                    }
                    else if (cboSTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToEmployee) ||
                         cboSTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToEmployeeDemo))
                    {
                        datCombos = null;
                        datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "Distinct STM.ReferenceID,EM.EmployeeNumber+'-'+EM.EmployeeFullName as EmployeeName", "InvStockTransferMaster STM inner Join EmployeeMaster EM on STM.ReferenceID=EM.EmployeeID",
                                    " STM.CompanyID= "+Convert.ToInt32(cboSCompany.SelectedValue)+" AND STM.OrderTypeID=" +Convert.ToInt32(cboSTransferType.SelectedValue)+ "" });
                        cboSToRefernce.DataSource = null;
                        cboSToRefernce.ValueMember = "ReferenceID";
                        cboSToRefernce.DisplayMember = "EmployeeName";
                        cboSToRefernce.DataSource = datCombos;
                    }
                    else if (cboSTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToWareHouseTransfer) ||
                        cboSTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToWareHouseTransferDemo))
                    {
                        datCombos = null;
                        datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "Distinct STM.ReferenceID,STW.WarehouseName", "InvStockTransferMaster STM inner Join InvWarehouse STW on STM.ReferenceID=STW.WarehouseID",
                                    " STM.CompanyID= "+Convert.ToInt32(cboSCompany.SelectedValue)+" AND STM.OrderTypeID=" +Convert.ToInt32(cboSTransferType.SelectedValue)+ "" });
                        cboSToRefernce.DataSource = null;
                        cboSToRefernce.ValueMember = "ReferenceID";
                        cboSToRefernce.DisplayMember = "WarehouseName";
                        cboSToRefernce.DataSource = datCombos;
                    }
                    else if (cboSTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToProject))
                    {
                        //datCombos = null;
                        //datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "Distinct STM.ReferenceID,PR.Pro", "InvStockTransferMaster STM inner Join STProjects PR on STM.ReferenceID=PR.ProjectID",
                        //    " STM.CompanyID= "+Convert.ToInt32(cboSCompany.SelectedValue)+" AND STM.OrderTypeID=" +Convert.ToInt32(cboSTransferType.SelectedValue)+ "" });
                        //cboSToRefernce.DataSource = null;
                        //cboSToRefernce.ValueMember = "ReferenceID";
                        //cboSToRefernce.DisplayMember = "Description";
                        //cboSToRefernce.DataSource = datCombos;
                    }
                }
                if (intType == 14)//Filling Incharge Combo
                {                    
                    datCombos = null;
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //{
                    //    //datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "Distinct STM.ReferenceInchargeID AS EmployeeID,EM.FirstName", "InvStockTransferMaster STM inner Join EmployeeMaster EM on STM.ReferenceInchargeID=EM.EmployeeID",
                    //    //            " STM.CompanyID= "+Convert.ToInt32(cboSCompany.SelectedValue)+" AND STM.OrderTypeID=" +Convert.ToInt32(cboSTransferType.SelectedValue)+ "" });
                    //    try
                    //    {
                            datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "EmployeeID,FirstName", " EmployeeMaster", "CompanyID=" + Convert.ToInt32(cboSCompany.SelectedValue) });
                    //    }
                    //    catch { }
                    //}
                    //else
                    //{
                    //    try
                    //    {
                    //        DataTable datTemp = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "IsVisible", "STRoleFieldsDetails", "" + 
                    //                    "RoleID=" + ClsCommonSettings.RoleID + " AND CompanyID=" + Convert.ToInt32(cboSCompany.SelectedValue) + " " +
                    //                    "AND ControlID=" + (int)ControlOperationType.StTranEmployee + " AND IsVisible=1" });
                    //        if (datTemp != null)
                    //            if (datTemp.Rows.Count > 0)
                    //                datCombos = MobjclsBLLWarehouseTransfer.GetEmployeeByPermission(ClsCommonSettings.RoleID, Convert.ToInt32(cboSCompany.SelectedValue), (int)ControlOperationType.StTranCompany);
                    //    }
                    //    catch { }
                    //}
                    cboSIncharge.DataSource = null;
                    cboSIncharge.ValueMember = "EmployeeID";
                    cboSIncharge.DisplayMember = "EmployeeFullName";
                    cboSIncharge.DataSource = datCombos;
                }
                if (intType == 15)//Filling Status combo 
                {
                    datCombos = null;
                    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "STM.StatusID,CSR.Status", "InvStockTransferMaster STM inner join CommonStatusReference CSR on STM.StatusID=CSR.StatusID",
                                    " STM.CompanyID= "+Convert.ToInt32(cboSCompany.SelectedValue)+" AND STM.OrderTypeID=" +Convert.ToInt32(cboSTransferType.SelectedValue)+ "" });

                    cboSStatus.ValueMember = "StatusID";
                    cboSStatus.DisplayMember = "Status";
                    cboSStatus.DataSource = datCombos;
                }
                if (intType == 16)//fill toWarehouseCombo when order type is To Project
                {
                    //datCombos = null;
                    //datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "ProjectID,Description", "Stprojects",
                    //                "CompanyID= "+Convert.ToInt32(cboCompany.SelectedValue)+" " });

                    //cboToWarehouse.DataSource = null;
                    //cboToWarehouse.ValueMember = "ProjectID";
                    //cboToWarehouse.DisplayMember = "Description";
                    //cboToWarehouse.DataSource = datCombos;
                }
                if (intType == 17)//fill toWarehouseCombo when order type is To Department
                {
                    datCombos = null;
                    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "DR.DepartmentID,DR.DepartmentName", "DepartmentReference DR inner join  Employeemaster EM on EM.DepartmentID=DR.DepartmentID",
                                    " EM.CompanyID= "+Convert.ToInt32(cboCompany.SelectedValue)+" " });

                    cboToWarehouse.DataSource = null;
                    cboToWarehouse.ValueMember = "DepartmentID";
                    cboToWarehouse.DisplayMember = "DepartmentName";
                    cboToWarehouse.DataSource = datCombos;
                }
                if (intType == 18)//fill Incharge Combo
                {
                    datCombos = null;
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //{
                    //    //if (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToDepartment))
                        //    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "EmployeeID,FirstName",
                        //    " Employeemaster EM inner Join Departmentreference DR on EM.DepartmentID=DR.DepartmentID ", "DR.DepartmentID=" + cboToWarehouse.SelectedValue.ToInt32() + ")" });

                        //if (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToProject))
                        //    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "EmployeeID,FirstName",
                        //    " Employeemaster EM inner Join StProjects PR  on EM.EmployeeID=PR.ProjectInchargeID ", " where PR.ProjectID=" + cboToWarehouse.SelectedValue.ToInt32() + ")" });
                                                
                        //if (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToWareHouseTransfer) ||
                        //   cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToWareHouseTransferDemo))
                        //    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "EmployeeID,FirstName",
                        //    " Employeemaster EM inner Join InvWarehouse WH on EM.EmployeeID=WH.inchargeID ", "WH.WarehouseID=" + cboToWarehouse.SelectedValue.ToInt32() + "" });
                        //try
                        //{
                           datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "EmployeeID,FirstName", " EmployeeMaster", "CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue)+" And WorkStatusID >=6 " });
                    //    }
                    //    catch { }
                    //}
                    //else
                    //{
                    //    try
                    //    {
                    //        DataTable datTemp = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "IsVisible", "STRoleFieldsDetails", "" + 
                    //                    "RoleID=" + ClsCommonSettings.RoleID + " AND CompanyID=" + Convert.ToInt32(cboCompany.SelectedValue) + " " +
                    //                    "AND ControlID=" + (int)ControlOperationType.StTranEmployee + " AND IsVisible=1" });
                    //        if (datTemp != null)
                    //            if (datTemp.Rows.Count > 0)
                    //                datCombos = MobjclsBLLWarehouseTransfer.GetEmployeeByPermission(ClsCommonSettings.RoleID, Convert.ToInt32(cboCompany.SelectedValue), (int)ControlOperationType.StTranCompany);
                    //    }
                    //    catch { }
                    //}
                    cboxIncharge.DataSource = null;
                    cboxIncharge.ValueMember = "EmployeeID";
                    cboxIncharge.DisplayMember = "FirstName";
                    cboxIncharge.DataSource = datCombos;
                }

                if (intType == 0 || intType == 19)//to Warehouse combo
                {

                    datCombos = null;
                    cboToWarehouse.DataSource = null;
                    if(cboXTransferType.SelectedValue.ToInt32() == (int)OperationOrderType.STWareHouseToWareHouseTransferDemo)
                    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "WarehouseID,WarehouseName", "InvWarehouse", " IsActive = 1" });
                    else if(cboXTransferType.SelectedValue.ToInt32() == (int)OperationOrderType.STWareHouseToWareHouseTransfer)
                        datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "DISTINCT W.WarehouseID,W.WarehouseName", "" +
                            "InvWarehouse W INNER JOIN InvWarehouseCompanyDetails WC ON W.WarehouseID=WC.WarehouseID","" +
                            "WC.CompanyID ="+cboCompany.SelectedValue+" and W.WarehouseID <> " + cboXFromWarehouse.SelectedValue.ToInt32() + " And " +
                            "W.IsActive = 1" });

                    cboToWarehouse.ValueMember = "WarehouseID";
                    cboToWarehouse.DisplayMember = "WarehouseName";
                    cboToWarehouse.DataSource = datCombos;
                }

                return true;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());

                return false;
            }
        }
        private string GetStockTransferPrefix()
        {
            clsBLLLogin objClsBLLLogin = new clsBLLLogin();
            DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue));
            return ClsCommonSettings.strStockTransferNo;

        }
        private void GenerateOrderNo()
        {

            txtTransferNo.Text = GetStockTransferPrefix() + (MobjclsBLLWarehouseTransfer.GetLastStockTransferNo(cboCompany.SelectedValue.ToInt32()) + 1).ToString();

        }
        private bool AddToAddressMenu(int iVendorID)
        {
            try
            {
                int i;

                CMSVendorAddress.Items.Clear();
                DataTable dtAddMenu = DtGetAddressName(iVendorID);

                for (i = 0; i <= dtAddMenu.Rows.Count - 1; i++)
                {
                    ToolStripMenuItem tItem = new ToolStripMenuItem();
                    tItem.Tag = Convert.ToString(dtAddMenu.Rows[i][0]);
                    tItem.Text = Convert.ToString(dtAddMenu.Rows[i][1]);
                    CMSVendorAddress.Items.Add(tItem);
                }
                CMSVendorAddress.ItemClicked -= new ToolStripItemClickedEventHandler(this.MenuItem_Click);
                CMSVendorAddress.ItemClicked += new ToolStripItemClickedEventHandler(this.MenuItem_Click);
                return true;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());
                return false;
            }
        }
        public DataTable DtGetAddressName(int iVendorID)
        {
            try
            {
                DataTable sRecordValues;
                sRecordValues = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "VendorAddID,AddressName", "InvVendorAddress", "VendorID = " + iVendorID });
                return sRecordValues;
            }
            catch (Exception Ex)
            {

                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());

                return null;
            }
        }
        private bool DisplayAddress()
        {
            try
            {
                txtVendorAddress.Text = MobjclsBLLWarehouseTransfer.DisplayAddressInformation(MintVendorAddID);
                return true;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());

                return false;
            }
        }
        private bool FillComboColumn(int intItemID)
        {
            try
            {

                DataTable dtItemUOMs = MobjclsBLLWarehouseTransfer.GetItemUOMs(intItemID);
                dgvColUom.DataSource = null;
                dgvColUom.ValueMember = "UOMID";
                dgvColUom.DisplayMember = "ShortName";
                dgvColUom.DataSource = dtItemUOMs;
                return true;

            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());
            }
            return false;
        }
        private DataTable FillDiscountColumn(int intRowIndex, bool blnIgnoreDate)
        {
            decimal decQty, decRate = 0;
            int intItemID = 0;
            int intDiscountID = dgvItemDetail[dgvColDiscount.Index, intRowIndex].Tag.ToInt32();
            intItemID = dgvItemDetail[dgvColItemID.Index, intRowIndex].Value.ToInt32();
            decQty = dgvItemDetail[dgvColQty.Index, intRowIndex].Value.ToDecimal();
            DataTable datUomConversions = MobjclsBLLWarehouseTransfer.GetUomConversionValues(dgvItemDetail["dgvColUom", intRowIndex].Tag.ToInt32(), intItemID);
            if (datUomConversions.Rows.Count > 0)
            {
                if (datUomConversions.Rows[0]["ConvertionFactorID"].ToInt32() == 2)
                    decQty = decQty * datUomConversions.Rows[0]["ConvertionValue"].ToDecimal();
                else
                    decQty = decQty / datUomConversions.Rows[0]["ConvertionValue"].ToDecimal();
            }
            decRate = dgvItemDetail[dgvColRate.Index, intRowIndex].Value.ToDecimal();
           

            DataTable dtItemDiscounts = new DataTable();
            if (blnIgnoreDate)
                dtItemDiscounts = MobjclsBLLWarehouseTransfer.GetItemWiseDiscountDetails(intItemID, decQty, decRate, "",cboCompany.SelectedValue.ToInt32());
            else
                dtItemDiscounts = MobjclsBLLWarehouseTransfer.GetItemWiseDiscountDetails(intItemID, decQty, decRate, dtpTransferDate.Value.ToString("dd-MMM-yyyy"),cboCompany.SelectedValue.ToInt32());
            dgvColDiscount.DataSource = null;
            dgvColDiscount.ValueMember = "DiscountID";
            dgvColDiscount.DisplayMember = "DiscountShortName";
            DataRow dr = dtItemDiscounts.NewRow();
           dr["DiscountID"] = 0;
            dr["DiscountShortName"] = "No Discount";
            dtItemDiscounts.Rows.InsertAt(dr, 0);
            dgvColDiscount.DataSource = dtItemDiscounts;

            dgvItemDetail[dgvColDiscount.Index, intRowIndex].Value = intDiscountID;
            dgvItemDetail[dgvColDiscount.Index, intRowIndex].Tag = intDiscountID;
            dgvItemDetail[dgvColDiscount.Index, intRowIndex].Value = dgvItemDetail[dgvColDiscount.Index, intRowIndex].FormattedValue;
            if (string.IsNullOrEmpty(dgvItemDetail[dgvColDiscount.Index, intRowIndex].Value.ToString()))
            {
                dgvItemDetail[dgvColDiscount.Index, intRowIndex].Value = 0;
                dgvItemDetail[dgvColDiscount.Index, intRowIndex].Tag = 0;
                dgvItemDetail[dgvColDiscount.Index, intRowIndex].Value = dgvItemDetail[dgvColDiscount.Index, intRowIndex].FormattedValue;
            }
            return dtItemDiscounts;
        }
        private bool StockTransferValidation()
        {
            if (cboCompany.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1510, out MmessageIcon);//please Select a company
                errProWarehouseTransfer.SetError(cboCompany, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrWarehouseTransfer.Enabled = true;
                cboCompany.Focus();
                return false;
            }
            if (cboXTransferType.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1511, out MmessageIcon);//please Select a Transfer Type
                errProWarehouseTransfer.SetError(cboXTransferType, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrWarehouseTransfer.Enabled = true;
                cboXTransferType.Focus();
                return false;
            }
            if (cboXFromWarehouse.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1512, out MmessageIcon);//please Select a Transfer Type
                errProWarehouseTransfer.SetError(cboXFromWarehouse, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrWarehouseTransfer.Enabled = true;
                cboXTransferType.Focus();
                return false;
            }
            if (cboToWarehouse.SelectedIndex == -1)
            {
                if (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToDepartment))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1514, out MmessageIcon);//please Select a department
                }
                else if (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToEmployee) ||
                    cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToEmployeeDemo))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1516, out MmessageIcon);//please Select a employee
                }
                else if (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToWareHouseTransfer) ||
                    cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToWareHouseTransferDemo))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1513, out MmessageIcon);//please Select a to warehouse
                }
                else if (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToProject))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1515, out MmessageIcon);//please Select a project
                }
                errProWarehouseTransfer.SetError(cboToWarehouse, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrWarehouseTransfer.Enabled = true;
                cboToWarehouse.Focus();
                return false;

            }
            if (cboxIncharge.SelectedIndex == -1 && (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToEmployee) ||
                    cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToEmployeeDemo)))
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1517, out MmessageIcon);//please Select a Incharge
                errProWarehouseTransfer.SetError(cboxIncharge, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrWarehouseTransfer.Enabled = true;
                cboxIncharge.Focus();
                return false;
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1518, out MmessageIcon);//please Select a currency
                errProWarehouseTransfer.SetError(cboCurrency, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrWarehouseTransfer.Enabled = true;
                cboCurrency.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtTransferNo.Text))
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1518, out MmessageIcon);//please Enter transfer No
                errProWarehouseTransfer.SetError(cboCurrency, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrWarehouseTransfer.Enabled = true;
                cboCurrency.Focus();
                return false;
            }
            if (dtpTransferDate.Value.Date < ClsCommonSettings.GetServerDate())
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1520, out MmessageIcon);//please Enter transfer No
                errProWarehouseTransfer.SetError(dtpTransferDate, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrWarehouseTransfer.Enabled = true;
                dtpTransferDate.Focus();
                return false;
            }
            if (dtpDueDate.Value.Date < dtpTransferDate.Value.Date)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1520, out MmessageIcon);//please Enter transfer No
                errProWarehouseTransfer.SetError(dtpTransferDate, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrWarehouseTransfer.Enabled = true;
                dtpTransferDate.Focus();
                return false;
            }
            if (CheckDuplicationInGrid() > 0)
            {
                //1522 Please check duplication of values.
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1522, out MmessageIcon);//please Enter transfer No
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrWarehouseTransfer.Enabled = true;
                return false;
            }

            if (!ValidateGridDetails())
                return false;


            return true;
        }

        private bool ValidateGridDetails()
        {
            int intRowCount = 0;
            bool blnValid = true;
            int intColumnIndex =0,intRowIndex = 0;
            for (int i = 0; i < dgvItemDetail.Rows.Count; i++)
            {
                if (dgvItemDetail.Rows[i].Cells[dgvColItemID.Index].Value.ToInt32() != 0)
                {
                    if(dgvItemDetail.Rows[i].Cells[dgvColQty.Index].Value.ToDecimal() == 0)
                    {
                        blnValid = false;
                        intColumnIndex = dgvColQty.Index;
                        intRowIndex = i;
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr,1504,out MmessageIcon);
                        break;
                    }
                    if (dgvItemDetail.Rows[i].Cells[dgvColUom.Index].Tag.ToInt32() == 0)
                    {
                        blnValid = false;
                        intColumnIndex = dgvColUom.Index;
                        intRowIndex = i;
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1506, out MmessageIcon);
                        break;
                    }

                    if (MobjClsCommonUtility.ConvertQtyToBaseUnitQty(dgvItemDetail.Rows[i].Cells[dgvColUom.Index].Tag.ToInt32(),dgvItemDetail.Rows[i].Cells[dgvColItemID.Index].Value.ToInt32(),dgvItemDetail.Rows[i].Cells[dgvColQty.Index].Value.ToDecimal(),1) > MobjClsCommonUtility.GetStockQuantity(dgvItemDetail.Rows[i].Cells[dgvColItemID.Index].Value.ToInt32(), dgvItemDetail.Rows[i].Cells[dgvColBatchID.Index].Value.ToInt64(), cboCompany.SelectedValue.ToInt32(), cboXFromWarehouse.SelectedValue.ToInt32()))
                    {
                        blnValid = false;
                        intColumnIndex = dgvColQty.Index;
                        intRowIndex = i;
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1505, out MmessageIcon).Replace("@", dgvItemDetail.Rows[i].Cells[dgvColItemName.Index].Value.ToString() + " - " + dgvItemDetail.Rows[i].Cells[dgvColBatchName.Index].Value.ToString());
                        break;
                    }

                    intRowCount++;
                }
            }

            if (!blnValid)
            {
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                dgvItemDetail.Focus();
                dgvItemDetail.CurrentCell = dgvItemDetail[intColumnIndex, intRowIndex];
            }
            else if (intRowCount == 0)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1507, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                dgvItemDetail.Focus();
                dgvItemDetail.CurrentCell = dgvItemDetail[dgvColItemCode.Index, intRowIndex];
                blnValid = false;
            }
            return blnValid;
        }

        private void ArrangeControls()
        {
            if (Convert.ToInt32(cboXTransferType.SelectedValue) > 0)
            {
                if (Convert.ToInt32(cboXTransferType.SelectedValue) == Convert.ToInt32(OperationOrderType.STWareHouseToEmployee) ||
                    Convert.ToInt32(cboXTransferType.SelectedValue) == Convert.ToInt32(OperationOrderType.STWareHouseToEmployeeDemo))
                {

                }
                else if (Convert.ToInt32(cboXTransferType.SelectedValue) == Convert.ToInt32(OperationOrderType.STWareHouseToWareHouseTransfer) ||
                    Convert.ToInt32(cboXTransferType.SelectedValue) == Convert.ToInt32(OperationOrderType.STWareHouseToWareHouseTransferDemo))
                {
                    //panelExAmount.Visible = false;



                }

            }
        }
        private bool DisplayAllNoInSearchGrid()
        {
            try
            {
                DataTable dtTemp = null;
                string strFilterCondition = string.Empty;
                if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                    strFilterCondition += " CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue);
                else
                {
                    dtTemp = null;
                    dtTemp = (DataTable)cboSCompany.DataSource;
                    if (dtTemp.Rows.Count > 0)
                    {
                        strFilterCondition += "CompanyID In (";
                        foreach (DataRow dr in dtTemp.Rows)
                        {
                            strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                        }
                        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                        strFilterCondition += ")";
                    }
                }
                if (cboSTransferType.SelectedValue != null)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "OrderTypeID = " + Convert.ToInt32(cboSTransferType.SelectedValue);
                }
                if (cboSFromWH.SelectedValue != null)
                {
                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "WarehouseID = " + Convert.ToInt32(cboSFromWH.SelectedValue);
                }
                if (cboSToRefernce.SelectedValue != null && cboSTransferType.SelectedValue != null)
                {

                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "ReferenceID = " + Convert.ToInt32(cboSToRefernce.SelectedValue);
                }
                if (cboSIncharge.SelectedValue != null)
                {

                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "ReferenceInchargeID = " + Convert.ToInt32(cboSIncharge.SelectedValue);
                }
                else if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
                {
                    //if (cboSCompany.SelectedValue != null && Convert.ToInt32(cboSCompany.SelectedValue) != -2)
                    //{
                    dtTemp = null;
                    dtTemp = (DataTable)cboSIncharge.DataSource;
                    if (dtTemp.Rows.Count > 0)
                    {
                        clsBLLPermissionSettings MobjClsBLLPermissionSettings = new clsBLLPermissionSettings();
                        DataTable datTempPer = MobjClsBLLPermissionSettings.GetControlPermissions1(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.StTranEmployee);
                        if (datTempPer != null)
                        {
                            if (datTempPer.Rows.Count > 0)
                            {
                                if (datTempPer.Rows[0]["IsVisible"].ToString() == "True")
                                    strFilterCondition += "And ReferenceInchargeID In (0,";
                                else
                                    strFilterCondition += "And ReferenceInchargeID In (";
                            }
                            else
                                strFilterCondition += "And ReferenceInchargeID In (";
                        }
                        else
                            strFilterCondition += "And ReferenceInchargeID In (";

                        foreach (DataRow dr in dtTemp.Rows)
                        {
                            strFilterCondition += Convert.ToInt32(dr["EmployeeID"]) + ",";
                        }
                        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                        strFilterCondition += ")";
                    }
                    //}
                }
                if (cboSStatus.SelectedValue != null)
                {

                    if (!string.IsNullOrEmpty(strFilterCondition))
                        strFilterCondition += " And ";
                    strFilterCondition += "StatusID = " + Convert.ToInt32(cboSStatus.SelectedValue);
                }
                if (!string.IsNullOrEmpty(strFilterCondition))
                    strFilterCondition += " And ";
                strFilterCondition += " CreatedDate >= '" + dtpSFrom.Value.ToString("dd-MMM-yyyy") + "' And CreatedDate <= '" + dtpSTo.Value.ToString("dd-MMM-yyyy") + "'";

                    if (!string.IsNullOrEmpty(txtSTransferNo.Text.Trim()))
                    {

                        strFilterCondition = " StockTransferNo = '" + txtSTransferNo.Text.Trim() + "'";
                    }



                dtTemp = null;
                dtTemp = MobjclsBLLWarehouseTransfer.SearchStockTransfers(strFilterCondition);
                //DgvStockTransferDisplay.Rows.Clear();
                //if (dtTemp.Rows.Count > 0)
                //{
                //    DgvStockTransferDisplay.RowCount = 0;
                //    int intRowCount;
                //    for (int intCounter = 0; intCounter < dtTemp.Rows.Count; intCounter++)
                //    {
                //        intRowCount = DgvStockTransferDisplay.RowCount;
                //        DgvStockTransferDisplay.RowCount = DgvStockTransferDisplay.RowCount + 1;
                //        DgvStockTransferDisplay.Rows[intRowCount].Cells["dgvColSStockTransferID"].Value = dtTemp.Rows[intCounter]["StockTransferID"];
                //        DgvStockTransferDisplay.Rows[intRowCount].Cells["dgvColSStockTransferNo"].Value = dtTemp.Rows[intCounter]["StockTransferNo"];
                //    }


                //}
                dtTemp.Columns["StockTransferID"].ColumnName = "StockTransferID";
                dtTemp.Columns["StockTransferNo"].ColumnName = "Stock TransferNo";
                DgvStockTransferDisplay.DataSource = null;
                DgvStockTransferDisplay.DataSource = dtTemp;
                DgvStockTransferDisplay.Columns[0].Visible = false;
                DgvStockTransferDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                return true;

            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on" + sMethod + "-" + Ex.Message.ToString());
                return false;
            }
        }
        private void ClearAllControls()
        {
            cboCompany.SelectedIndex = -1;
            cboCompany.SelectedValue = ClsCommonSettings.CompanyID;
            SetControlEnableDisable(true);
            SetControlVisibilty(false);
            dgvItemDetail.ReadOnly = false;
            cboCompany.Enabled = true;
            cboXTransferType.SelectedValue = (int)OperationOrderType.STWareHouseToWareHouseTransfer;
            cboXFromWarehouse.SelectedIndex = -1;
            cboToWarehouse.SelectedIndex = -1;
            cboXVendor.SelectedIndex = -1;
            txtVendorAddress.Text = "";
           // cboCurrency.SelectedIndex = -1;
            txtTransferNo.Tag = 0;
            cboIncoTerms.SelectedIndex = -1;
            txtLeadTime.Text = "";
            cboPaymentTerms.SelectedIndex = -1;
            cboxIncharge.SelectedIndex = -1;
            DateTime DtServerDate = ClsCommonSettings.GetServerDate();
            //dtpCancelledDate.Value = DtServerDate;
            dtpClearenceDate.Value = DtServerDate;
            //dtpCreatedDate.Value = DtServerDate;
            dtpTransferDate.Value = DtServerDate;
            dtpTrnsShipmentDate.Value = DtServerDate;
            dtpProductionDate.Value = DtServerDate;
            dtpDueDate.Value = DtServerDate;
            dgvItemDetail.Rows.Clear();
            dgvItemDetail.ClearSelection();
            txtRemarks.Text = "";
            txtDescription.Text = "";
            //lblCancelledbyName.Text = "";
            cboDiscount.SelectedIndex = -1;
            txtSubtotal.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            txtDiscount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            txtTotalAmount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            txtExpenseAmount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            txtAdvPayment.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
            txtExchangeCurrencyRate.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);

            cboSCompany.SelectedIndex = -1;
            cboSTransferType.SelectedIndex = -1;
            cboSFromWH.SelectedIndex = -1;
            cboSToRefernce.SelectedIndex = -1;
            cboSIncharge.SelectedIndex = -1;
            cboSStatus.SelectedIndex = -1;
            txtSTransferNo.Text = "";


        }
        private void FillParameterMaster()
        {


            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID = Convert.ToInt64(txtTransferNo.Tag) > 0 ? Convert.ToInt64(txtTransferNo.Tag) : 0;
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intSlNo = 0;
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intOrderTypeID = Convert.ToInt32(cboXTransferType.SelectedValue);
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strStockTransferNo = txtTransferNo.Text;
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strOrderDate = dtpTransferDate.Value.Date.ToString("dd MMM yyyy");
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intVendorID = Convert.ToInt32(cboXVendor.SelectedValue);
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intWarehouseID = Convert.ToInt32(cboXFromWarehouse.SelectedValue);
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intReferenceID = Convert.ToInt32(cboToWarehouse.SelectedValue);//To Warehoue ID Or To EmployeeID
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strDueDate = dtpDueDate.Value.Date.ToString("dd MMM yyyy");
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intVendorAddID = MintVendorAddID;
            if (Convert.ToInt32(cboxIncharge.SelectedValue) > 0)
                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intReferenceInchargeID = Convert.ToInt32(cboxIncharge.SelectedValue);
            if (Convert.ToInt32(cboPaymentTerms.SelectedValue) > 0)
                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intPaymentTermsID = Convert.ToInt32(cboPaymentTerms.SelectedValue);
            else
                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intPaymentTermsID = 0;

            if (Convert.ToInt32(cboIncoTerms.SelectedValue) > 0)
                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intIncoTermsID = Convert.ToInt32(cboIncoTerms.SelectedValue);
            else
                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intIncoTermsID = 0;
            if (txtRemarks.Text != "")
                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strRemarks = txtRemarks.Text;
            else
                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strRemarks = "";

            if (Convert.ToInt32(cboDiscount.SelectedValue) > 0)
            {
                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intGrandDiscountID = Convert.ToInt32(cboDiscount.SelectedValue);
                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decGrandDiscountAmount = txtDiscount.Text.ToDecimal();
            }
            else
            {
                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intGrandDiscountID = 0;
                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decGrandDiscountAmount = 0.ToDecimal();
            }
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intCurrencyID = Convert.ToInt32(cboCurrency.SelectedValue);
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decExpenseAmount = txtExpenseAmount.Text.ToDecimal();
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decGrandAmount = txtSubtotal.Text.ToDecimal();
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decNetAmount = txtTotalAmount.Text.ToDecimal();
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decNetAmountRounded = Math.Ceiling(MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decNetAmount);

            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intCreatedBy = ClsCommonSettings.UserID;
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strCreatedDate = ClsCommonSettings.GetServerDate().ToString("dd MMM yyyy");

            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            // //,[ApprovedBy]
            // //,[ApprovedDate]
            // //,[VerificationDescription]
            if(MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID == 0)
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID = Convert.ToInt32(OperationStatusType.STOpened);

            //lead Time details
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decLeadTime = txtLeadTime.Text.ToDecimal();
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strProductionDate = dtpProductionDate.Value.Date.ToString("dd MMM yyyy");
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strTranshipmentDate = dtpTrnsShipmentDate.Value.Date.ToString("dd MMM yyyy");
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strClearenceDate = dtpClearenceDate.Value.Date.ToString("dd MMM yyyy");



        }
        private void FillParameterDetails()
        {
            if (dgvItemDetail.CurrentRow != null)
                dgvItemDetail.CurrentCell = dgvItemDetail["dgvColItemCode", dgvItemDetail.CurrentRow.Index];
            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.lstObjClsDTOWarehouseTransferDet = new System.Collections.Generic.List<clsDTOWarehouseTransferDet>();
            for (int iCounter = 0; iCounter < dgvItemDetail.Rows.Count; iCounter++)
            {
                if (dgvItemDetail["dgvColItemID", iCounter].Value != null && dgvItemDetail["dgvColQty", iCounter].Value != null)
                {


                    clsDTOWarehouseTransferDet objclsDTOWarehouseTransferDet = new clsDTOWarehouseTransferDet();
                    objclsDTOWarehouseTransferDet.intSerialNo = iCounter + 1;

                    objclsDTOWarehouseTransferDet.intBatchID = Convert.ToInt64(dgvItemDetail["dgvColBatchID", iCounter].Value);
                    objclsDTOWarehouseTransferDet.intItemID = Convert.ToInt32(dgvItemDetail["dgvColItemID", iCounter].Value);
                    objclsDTOWarehouseTransferDet.decQuantity = Convert.ToDecimal(dgvItemDetail["dgvColQty", iCounter].Value);

                    objclsDTOWarehouseTransferDet.intUOMID = Convert.ToInt32(dgvItemDetail["dgvColUom", iCounter].Tag);
                    objclsDTOWarehouseTransferDet.decRate = Convert.ToDecimal(dgvItemDetail["dgvColRate", iCounter].Value);

                    if (dgvItemDetail["dgvColDiscount", iCounter].Value != null)
                    {
                        objclsDTOWarehouseTransferDet.intDiscountID = Convert.ToInt32(dgvItemDetail["dgvColDiscount", iCounter].Tag);
                    }
                    if (dgvItemDetail["dgvcolDiscountAmount", iCounter].Value != DBNull.Value)
                    {
                        objclsDTOWarehouseTransferDet.decDiscountAmount = Convert.ToDecimal(dgvItemDetail["dgvcolDiscountAmount", iCounter].Value);
                    }
                    objclsDTOWarehouseTransferDet.decGrandAmount = Convert.ToDecimal(dgvItemDetail["dgvColGrandAmount", iCounter].Value);
                    objclsDTOWarehouseTransferDet.decNetAmount = Convert.ToDecimal(dgvItemDetail["dgvColNetAmount", iCounter].Value);
                    //if (dgvPurchase["Batchnumber", i].Value != null)
                    //    objClsDtoPurchaseDetails.strBatchNumber = Convert.ToString(dgvPurchase["Batchnumber", i].Value);

                    MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.lstObjClsDTOWarehouseTransferDet.Add(objclsDTOWarehouseTransferDet);
                }
            }

        }
        private bool SaveStockTransferInfo()
        {
            if (StockTransferValidation())
            {
                if (MblnAddStatus)
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);
                else
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;

               
                   
                
                if(!MobjclsBLLWarehouseTransfer.ExistCurrencyReference(cboXFromWarehouse.SelectedValue.ToInt32(),cboToWarehouse.SelectedValue.ToInt32()))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1539, out MmessageIcon);//Duplicate Stock Transfer No.
                    errProWarehouseTransfer.SetError(cboToWarehouse, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrWarehouseTransfer.Enabled = true;
                    //txtSTransferNo.Focus();
                    return false;
                }

                //if(ClsCommonSettings.blnPOSAutogenerate  Autogenerate StockTransferNo ?
                if (MblnAddStatus && txtTransferNo.Text!="")
                {
                    if (MobjclsBLLWarehouseTransfer.IsDuplicateStockTranferNo(cboCompany.SelectedValue.ToInt64(), txtTransferNo.Text))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1536, out MmessageIcon);//Duplicate Stock Transfer No.
                        errProWarehouseTransfer.SetError(txtSTransferNo, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        tmrWarehouseTransfer.Enabled = true;
                        txtSTransferNo.Focus();
                        return false;
                    }

                   
                }
                if (!MblnAddStatus && txtTransferNo.Tag.ToInt64() > 0)
                {
                    if (!MobjclsBLLWarehouseTransfer.IsValidStockTransferID(cboCompany.SelectedValue.ToInt64(), txtTransferNo.Tag.ToInt64()))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1538, out MmessageIcon);//Duplicate Stock Transfer No.
                        errProWarehouseTransfer.SetError(txtSTransferNo, MstrMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        tmrWarehouseTransfer.Enabled = true;
                        txtSTransferNo.Focus();
                        return false;
                    }
                }
                FillParameterMaster();
                FillParameterDetails();
                if (MobjclsBLLWarehouseTransfer.SaveStockTransfer(MblnAddStatus))
                {
                    return true;
                }
            }

            return false;

        }
        private void CalculateTotalAmt()
        {
            try
            {

                double dGrandAmount = 0;
                if (dgvItemDetail.RowCount > 0)
                {
                    int iGridRowCount = dgvItemDetail.RowCount;
                    iGridRowCount = iGridRowCount - 1;

                    for (int i = 0; i < iGridRowCount; i++)
                    {
                        double dGridNetAmount = 0;
                        if (dgvItemDetail.Rows[i].Cells["dgvColItemID"].Value.ToInt32() != 0)
                        {
                            int iItemID = dgvItemDetail.Rows[i].Cells["dgvColItemID"].Value.ToInt32();
                            //int iDiscountID = dgvItemDetail.Rows[i].Cells["dgvColDiscount"].Tag.ToInt32();
                            double dQty = 0;
                            double dRate = 0, decDiscountAmount = 0;
                            dQty = dgvItemDetail.Rows[i].Cells["dgvColQty"].Value.ToDouble();
                            double dDirectRate = dgvItemDetail.Rows[i].Cells["dgvColRate"].Value.ToDouble();
                            double dGridGrandAmount = dQty * dDirectRate;
                            if (dQty > 0 && dDirectRate > 0)
                            {
                                bool blnIsDiscountForAmount = false;//MobjclsBLLWarehouseTransfer.IsDiscountForAmount(iDiscountID);
                                if (!blnIsDiscountForAmount)
                                {
                                    //if (iDiscountID > 0 && dDirectRate > 0 && iItemID > 0)
                                    //    dRate = MobjclsBLLWarehouseTransfer.GetItemDiscountAmount(3, iItemID, iDiscountID, dDirectRate);
                                    //else
                                        dRate = dDirectRate;
                                    //decDiscountAmount = dDirectRate - dRate;
                                    //decDiscountAmount = decDiscountAmount * dQty;
                                    dGridNetAmount = dRate * dQty;
                                }
                                else
                                {
                                    dGridNetAmount = dDirectRate * dQty;
                                    //if (iDiscountID > 0 && dDirectRate > 0 && iItemID > 0)
                                    //    dGridNetAmount = MobjclsBLLWarehouseTransfer.GetItemDiscountAmount(3, iItemID, iDiscountID, dGridNetAmount);
                                    //decDiscountAmount = (dDirectRate * dQty) - dGridNetAmount;
                                }
                            }


                            dgvItemDetail.Rows[i].Cells["dgvcolGrandAmount"].Value = dGridGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                            dgvItemDetail.Rows[i].Cells["dgvColNetAmount"].Value = dGridNetAmount.ToString("F" + MintExchangeCurrencyScale);
                            //dgvItemDetail.Rows[i].Cells["dgvcolDiscountAmount"].Value = decDiscountAmount.ToString("F" + MintExchangeCurrencyScale);
                            dGrandAmount += dGridNetAmount;

                        }
                        else
                        {
                            dGrandAmount += 0;
                            dgvItemDetail.Rows[i].Cells["dgvcolGrandAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                            dgvItemDetail.Rows[i].Cells["dgvColNetAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                            //dgvItemDetail.Rows[i].Cells["dgvcolDiscountAmount"].Value = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                        }
                    }
                }

                txtSubtotal.Text = dGrandAmount.ToString("F" + MintExchangeCurrencyScale);
                CalculateNetTotal();

            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CalculateTotalAmt() " + ex.Message);
                MObjLogs.WriteLog("Error in CalculateTotalAmt() " + ex.Message, 2);
            }
        }
        private void CalculateNetTotal()
        {
            try
            {
                //double dDiscountAmt = 0;
                double dSubTotal = 0;
                double dNetAmount = 0;
                double dExpenseAmount = 0;

                if (!string.IsNullOrEmpty(txtExpenseAmount.Text))
                    dExpenseAmount = Convert.ToDouble(txtExpenseAmount.Text);
                dSubTotal = txtSubtotal.Text.ToDouble();
                //int intDiscountID = Convert.ToInt32(cboDiscount.SelectedValue);


                double dBTotal = dSubTotal;

                //if (intDiscountID > 0)
                //{
                //    dBTotal = MobjclsBLLWarehouseTransfer.GetItemDiscountAmount(4, 0, intDiscountID, dSubTotal);
                //    dDiscountAmt = (dSubTotal) - dBTotal;
                //    txtDiscount.Text = dDiscountAmt.ToString("F" + MintExchangeCurrencyScale);
                //}
                //else
                //{
                //    dDiscountAmt = 0;
                //    txtDiscount.Text = Convert.ToDecimal(0).ToString("F" + MintExchangeCurrencyScale);
                //}

                dNetAmount = (dSubTotal + dExpenseAmount);// -dDiscountAmt;
                txtTotalAmount.Text = dNetAmount.ToString("F" + MintExchangeCurrencyScale);
                txtExpenseAmount.Text = Convert.ToDecimal(txtExpenseAmount.Text).ToString("F" + MintExchangeCurrencyScale);
                CalculateExchangeCurrencyRate();
            }

            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in CalculateBNetNotal() " + ex.Message);
                MObjLogs.WriteLog("Error in CalculateBNetTotal() " + ex.Message, 2);
            }
        }
        private void CalculateExchangeCurrencyRate()
        {
            int intCompanyID = 0, intCurrencyID = 0;

            intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
            intCurrencyID = Convert.ToInt32(cboCurrency.SelectedValue);

            if (MobjclsBLLWarehouseTransfer.GetExchangeCurrencyRate(intCompanyID, intCurrencyID))
            {
                txtExchangeCurrencyRate.Text = Convert.ToDecimal(MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decExchangeCurrencyRate * Convert.ToDecimal(txtTotalAmount.Text)).ToString("F" + MintBaseCurrencyScale);
            }
        }
        private bool DisplayOrderInfo(Int64 iOrderID)
        {
            try
            {


                if (MobjclsBLLWarehouseTransfer.DisplayStockTransferInfo(iOrderID))
                {
                    MblnAddStatus = false;

                    BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                    txtTransferNo.Tag = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID;

                    cboCompany.SelectedValue = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intCompanyID;
                    clsBLLLogin objClsBLLLogin = new clsBLLLogin();
                    DataTable dtCompanySettingsInfo = objClsBLLLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue));
                    cboXTransferType.SelectedValue = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intOrderTypeID;
                    cboXFromWarehouse.SelectedValue = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intWarehouseID;

                    if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intOrderTypeID == Convert.ToInt32(OperationOrderType.STWareHouseToEmployee) ||
                        MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intOrderTypeID == Convert.ToInt32(OperationOrderType.STWareHouseToEmployeeDemo))
                    {
                        LoadCombos(6);


                    }
                    else if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intOrderTypeID == Convert.ToInt32(OperationOrderType.STWareHouseToDepartment))
                    {
                        LoadCombos(17);



                    }
                    else if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intOrderTypeID == Convert.ToInt32(OperationOrderType.STWareHouseToProject))
                    {
                        LoadCombos(16);


                    }
                    else if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intOrderTypeID == Convert.ToInt32(OperationOrderType.STWareHouseToWareHouseTransfer) ||
                        MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intOrderTypeID == Convert.ToInt32(OperationOrderType.STWareHouseToWareHouseTransferDemo))
                    {
                        LoadCombos(19);



                    }

                    cboToWarehouse.SelectedValue = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intReferenceID;
                    LoadCombos(18);
                    cboxIncharge.SelectedValue = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intReferenceInchargeID;

                    if (cboxIncharge.SelectedValue.ToInt32() == 0)
                    {
                        DataTable datEmployeeDetails = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "FirstName", "EmployeeMaster", "EmployeeID = " + MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intReferenceInchargeID });
                        if(datEmployeeDetails.Rows.Count>0)
                        cboxIncharge.Text = datEmployeeDetails.Rows[0]["FirstName"].ToStringCustom();
                    }

                    cboCurrency.SelectedValue = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intCurrencyID;
                    cboXVendor.SelectedValue = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intVendorID;
                    txtTransferNo.Text = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strStockTransferNo;
                    dtpTransferDate.Value = Convert.ToDateTime(MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strOrderDate);
                    dtpDueDate.Value = Convert.ToDateTime(MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strDueDate);

                    cboPaymentTerms.SelectedValue = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intPaymentTermsID;
                    //cboIncoTerms.SelectedValue = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intIncoTermsID;
                    

                    //if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strProductionDate != null && MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strProductionDate != "")
                    //    dtpProductionDate.Value = Convert.ToDateTime(MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strProductionDate);
                    //if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strTranshipmentDate != "")
                    //    dtpTrnsShipmentDate.Value = Convert.ToDateTime(MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strTranshipmentDate);
                    //if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strClearenceDate != "" && MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strClearenceDate != null)
                    //    dtpClearenceDate.Value = Convert.ToDateTime(MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strClearenceDate);
                    //txtLeadTime.Text = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decLeadTime.ToString();
                   
                    txtRemarks.Text = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strRemarks;
                    lblStatusText.Text = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strStatus;

                    //LoadCombos(9);
                    //cboDiscount.SelectedValue = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intGrandDiscountID;
                   
                    if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strVerificationDescription != "")
                    { txtDescription.Text = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strVerificationDescription; }

                    
                        lblCreatedDateValue.Text = "Created Date : " + MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strCreatedDate;
                        lblCreatedByText.Text = "Created By : " + MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strEmployeeName;

                    if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID == (int)OperationStatusType.STApproved || MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID == (int)OperationStatusType.STRejected || MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID == (int)OperationStatusType.STCancelled)
                    {
                       // dtpCancelledDate.Value = Convert.ToDateTime(MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strApprovedDate);
                        lblCreatedDateValue.Text = lblCreatedDateValue.Text + "    | " + MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strStatus + " Date : " + MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strApprovedDate;
                        lblCreatedByText.Text = lblCreatedByText.Text + "       |     " + MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strStatus + " By : " + MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strApprovedBy; 
                    }

                    //lblCreatedByName.Text = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strEmployeeName;
                    //if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strApprovedBy != "")
                    //{ lblCancelledbyName.Text = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strApprovedBy; }
                   
                 
                   

                    //clsBLLLogin mobjClsBllLogin = new clsBLLLogin();
                    //mobjClsBllLogin.GetCompanySettings(Convert.ToInt32(cboCompany.SelectedValue.ToInt32()));
                    DispalyItemDetails();
                    txtSubtotal.Text = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decGrandAmount.ToString();
                    txtExpenseAmount.Text = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decExpenseAmount.ToString();
                    txtDiscount.Text = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decGrandDiscountAmount.ToString();
                    txtTotalAmount.Text = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decNetAmount.ToString();
                    CalculateExchangeCurrencyRate();
                    SetStockTransferStatus();
                    btnActions.Enabled = btnAddExpense.Enabled = true;

                }
                return true;

            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());
                return false;
            }
        }
        private void DispalyItemDetails()
        {
            DataTable dtItemDetails = new DataTable();
            dtItemDetails = MobjclsBLLWarehouseTransfer.GetStockTransferItemDetails();
            if (dtItemDetails.Rows.Count > 0)
            {


                for (int iCounter = 0; iCounter < dtItemDetails.Rows.Count; iCounter++)
                {
                    dgvItemDetail.RowCount = dgvItemDetail.RowCount + 1;
                    dgvItemDetail.Rows[iCounter].Cells["dgvColItemCode"].Value = dtItemDetails.Rows[iCounter]["ItemCode"];
                    dgvItemDetail.Rows[iCounter].Cells["dgvColItemName"].Value = dtItemDetails.Rows[iCounter]["Itemname"];
                    //  dgvPurchase.Rows[i].Cells["Quantity"].Value = DtPurOrderDetail.Rows[i]["Quantity"];
                    dgvItemDetail.Rows[iCounter].Cells["dgvColItemID"].Value = dtItemDetails.Rows[iCounter]["ItemID"];
                    dgvItemDetail.Rows[iCounter].Cells["dgvColBatchID"].Value = dtItemDetails.Rows[iCounter]["BatchID"];
                    dgvItemDetail.Rows[iCounter].Cells["dgvColBatchName"].Value = dtItemDetails.Rows[iCounter]["BatchNo"];
                    FillComboColumn(Convert.ToInt32(dtItemDetails.Rows[iCounter]["ItemID"]));
                    dgvItemDetail.Rows[iCounter].Cells["dgvColUom"].Value = dtItemDetails.Rows[iCounter]["UOMID"];
                    dgvItemDetail.Rows[iCounter].Cells["dgvColUom"].Tag = dtItemDetails.Rows[iCounter]["UOMID"];
                    dgvItemDetail.Rows[iCounter].Cells["dgvColUom"].Value = dgvItemDetail.Rows[iCounter].Cells["dgvColUom"].FormattedValue;

                    int intUomScale = 0;
                    if (dgvItemDetail[dgvColUom.Index, iCounter].Tag.ToInt32() != 0)
                        intUomScale = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "Scale", "InvUOMReference", "UOMID = " + dgvItemDetail[dgvColUom.Index, iCounter].Tag.ToInt32() }).Rows[0]["Scale"].ToInt32();

                    dgvItemDetail.Rows[iCounter].Cells["dgvColQty"].Value = dtItemDetails.Rows[iCounter]["Quantity"].ToDecimal().ToString("F" + intUomScale);

                    dgvItemDetail.Rows[iCounter].Cells["dgvColRate"].Value = dtItemDetails.Rows[iCounter]["Rate"];
                    dgvItemDetail.Rows[iCounter].Cells["dgvColGrandAmount"].Value = dtItemDetails.Rows[iCounter]["GrandAmount"];
                    //FillDiscountColumn(iCounter, false);
                    //if (dtItemDetails.Rows[iCounter]["DiscountID"] != DBNull.Value && Convert.ToInt32(dtItemDetails.Rows[iCounter]["DiscountID"]) != 0)
                    //{
                    //    dgvItemDetail.Rows[iCounter].Cells["dgvColDiscount"].Value = dtItemDetails.Rows[iCounter]["DiscountID"];
                    //    dgvItemDetail.Rows[iCounter].Cells["dgvColDiscount"].Tag = dtItemDetails.Rows[iCounter]["DiscountID"];
                    //    dgvItemDetail.Rows[iCounter].Cells["dgvColDiscount"].Value = dgvItemDetail.Rows[iCounter].Cells["dgvColDiscount"].FormattedValue;
                    //}
                    dgvItemDetail.Rows[iCounter].Cells["dgvcolDiscountAmount"].Value = dtItemDetails.Rows[iCounter]["DiscountAmount"];

                    dgvItemDetail.Rows[iCounter].Cells["dgvColNetAmount"].Value = dtItemDetails.Rows[iCounter]["NetAmount"];
                }
            }

        }
        private void SetControlVisibilty(bool blnIsvisibile)
        {
            //lblCancelledDate.Visible = blnIsvisibile;
            lblDescription.Visible = blnIsvisibile;
            txtDescription.Visible = blnIsvisibile;
            //dtpCancelledDate.Visible = blnIsvisibile;
            //lblCancelledBy.Visible = blnIsvisibile;
            //lblCancelledbyName.Visible = blnIsvisibile;
        }
        private void SetControlEnableDisable(bool blnIsEnabled)
        {
                    cboCompany.Enabled = cboXTransferType.Enabled = cboXFromWarehouse.Enabled = cboToWarehouse.Enabled = cboxIncharge.Enabled = blnIsEnabled;
                    cboXVendor.Enabled = txtVendorAddress.Enabled = txtTransferNo.Enabled = dtpTransferDate.Enabled = dtpDueDate.Enabled = blnIsEnabled;
                    txtRemarks.Enabled = cboIncoTerms.Enabled = dtpTrnsShipmentDate.Enabled = dtpClearenceDate.Enabled = dtpProductionDate.Enabled = blnIsEnabled;
                    cboDiscount.Enabled = cboPaymentTerms.Enabled = txtLeadTime.Enabled = blnIsEnabled;

                    SetGridEditability(blnIsEnabled);
        }

        private void TextboxNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
            if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
            {
                e.Handled = true;
            }
            if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            {
                e.Handled = true;
            }
        }

        private void SetGridEditability(bool blnEditable)
        {
            dgvColItemCode.ReadOnly = dgvColItemName.ReadOnly = dgvColQty.ReadOnly = dgvColUom.ReadOnly = dgvColDiscount.ReadOnly = !blnEditable;                       
        }

        private void SetStockTransferStatus()
        {

            //Stock Transfer Status buttons 

            btnActions.Enabled = true;
            bnExpense.Visible = true;
            btnApprove.Visible = btnReject.Visible = tiSuggestions.Visible = btnSuggest.Visible = btnDeny.Visible = false;
            bnSubmit.Visible = bnSubmitForApproval.Visible = false;
            BindingNavigatorCancelItem.Text = "Cancel";

            if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID == Convert.ToInt32(OperationStatusType.STOpened))
            {

                SetControlVisibilty(false);
                SetControlEnableDisable(true);

                if (!MblnIsFrmApproval)
                {
                    if (ClsCommonSettings.StockTransferApproval)
                    {
                        bnSubmitForApproval.Visible = MblnUpdatePermission;
                    }
                    else
                    {
                        bnSubmit.Visible = MblnUpdatePermission;
                    }
                }
                lblStatusText.ForeColor = Color.Brown;
                MblnIsEditable = true;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BindingNavigatorClearItem.Enabled = true;
                BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
            }

            else if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID == Convert.ToInt32(OperationStatusType.STSubmittedForApproval))
            {
                if (MblnIsFrmApproval)
                {
                    if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID == PintStockTransferIDToLoad)
                    {
                        if (MblnIsFrmApproval && (PintApprovePermission == (int)ApprovePermission.Suggest || PintApprovePermission == (int)ApprovePermission.Both))
                        {
                            btnSuggest.Visible = btnDeny.Visible = true;
                            tiSuggestions.Visible = true;
                        }
                        if (MblnIsFrmApproval && (PintApprovePermission == (int)ApprovePermission.Approve || PintApprovePermission == (int)ApprovePermission.Both))
                            btnApprove.Visible = btnReject.Visible = true;
                    }
                }
                BindingNavigatorCancelItem.Enabled = MblnCancelPermission;

                BindingNavigatorDeleteItem.Enabled = false;
                SetControlEnableDisable(false);
                BindingNavigatorClearItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;

                lblStatusText.ForeColor = Color.BlueViolet;
               
            }
            else if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID == Convert.ToInt32(OperationStatusType.STSubmitted))
            {
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorClearItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
                SetControlEnableDisable(false);

                lblStatusText.ForeColor = Color.BlueViolet;
            }
            else if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID == Convert.ToInt32(OperationStatusType.STApproved))
            {
                SetControlVisibilty(true);

                if (MblnIsFrmApproval)
                {
                    if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID == PintStockTransferIDToLoad)
                    {
                        if (MblnIsFrmApproval && (PintApprovePermission == (int)ApprovePermission.Approve || PintApprovePermission == (int)ApprovePermission.Both))
                            btnReject.Visible = true;
                    }
                }
               

                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorClearItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorCancelItem.Enabled = false;
                SetControlEnableDisable(false);

                lblStatusText.ForeColor = Color.Green;
            }
            else if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID == Convert.ToInt32(OperationStatusType.STRejected))
            {
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BindingNavigatorClearItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorCancelItem.Enabled = MblnCancelPermission;

                if (MblnIsFrmApproval)
                {
                    if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID == PintStockTransferIDToLoad)
                    {
                        if (MblnIsFrmApproval && (PintApprovePermission == (int)ApprovePermission.Suggest || PintApprovePermission == (int)ApprovePermission.Both))
                        {
                            btnSuggest.Visible = btnDeny.Visible = true;
                            tiSuggestions.Visible = true;
                        }
                        if (MblnIsFrmApproval && (PintApprovePermission == (int)ApprovePermission.Approve || PintApprovePermission == (int)ApprovePermission.Both))
                            btnApprove.Visible = true;
                    }
                }
                else
                    bnSubmitForApproval.Visible = MblnUpdatePermission;
                SetControlVisibilty(true);
                SetControlEnableDisable(true);

                lblStatusText.ForeColor = Color.Red;
            }
            else if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID == Convert.ToInt32(OperationStatusType.STCancelled))
            {
                BindingNavigatorCancelItem.Text = "Reopen";
                BindingNavigatorCancelItem.Enabled = MblnCancelPermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                
                SetControlVisibilty(true);
                SetControlEnableDisable(false);

                lblStatusText.ForeColor = Color.Red;
            }
            else if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID == Convert.ToInt32(OperationStatusType.STIssued))
            {
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorCancelItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;

                SetControlEnableDisable(false);

                lblStatusText.ForeColor = Color.DarkBlue;

            }
            else if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID == Convert.ToInt32(OperationStatusType.STClosed))
            {
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorCancelItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                SetControlEnableDisable(false);

                lblStatusText.ForeColor = Color.Purple;
            }
            if (MblnIsFrmApproval)
            {
                BindingNavigatorAddNewItem.Enabled = BindingNavigatorCancelItem.Enabled = BindingNavigatorClearItem.Enabled = BindingNavigatorDeleteItem.Enabled = BindingNavigatorSaveItem.Enabled = false;
            }
        }
        private void AddNewStockTransfer()
        {
            try
            {
                MblnAddStatus = true;
                txtTransferNo.Tag = 0;
                txtTransferNo.Text = "";
                ClearAllControls();
                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID = 0;

                btnActions.Enabled = false;
                btnApprove.Visible = false;
                btnReject.Visible = false;
                bnSubmitForApproval.Visible = false;
                btnSuggest.Visible = btnDeny.Visible = false;
                tiSuggestions.Visible = false;
                GenerateOrderNo();

                DisplayAllNoInSearchGrid();

                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1533, out MmessageIcon); //"Add new Stock Transfer";
                btnAddExpense.Enabled = false;

                lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                lblStatusText.Text = "New";
                lblStatusText.ForeColor = Color.Black;
                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID = (int)OperationStatusType.STOpened;
                tmrWarehouseTransfer.Enabled = true;

               // lblCancelledDate.Visible = false;
                lblDescription.Visible = false;
                txtDescription.Visible = false;
               // dtpCancelledDate.Visible = false;
               // lblCancelledBy.Visible = false;
               // lblCreatedByName.Visible = false;

                errProWarehouseTransfer.Clear();
                MblnChangeStatus = false;
                BtnPrint.Enabled = false;
                BtnEmail.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                //  MblnIsFromCancellation = false;
                BindingNavigatorCancelItem.Enabled = false;
                //BindingNavigatorSaveItem.Enabled = mb;
                BindingNavigatorClearItem.Enabled = false;
                lblCreatedByText.Text = "Created By : " + ClsCommonSettings.strEmployeeName;
                lblCreatedDateValue.Text = "Created Date : " + ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");

                dgvItemDetail.Rows.Clear();
                SetGridEditability(true);

                BindingNavigatorAddNewItem.Enabled = false;
                cboCompany.Focus();

            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());
            }
        }

        private void Changestatus()
        {
            //Changing status
            MblnChangeStatus = true;
            if (MblnAddStatus && PintStockTransferIDToLoad ==0)
            {
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;//MblnAddPermission;
                BindingNavigatorClearItem.Enabled = true;
            }
            else
            {
                if (PintStockTransferIDToLoad == 0)
                {
                    if (MobjclsBLLWarehouseTransfer != null && (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID == (Int32)OperationStatusType.STOpened))
                        BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;//MblnUpdatePermission;
                    else
                        BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                    BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                    if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID == Convert.ToInt32(OperationStatusType.STSubmittedForApproval) ||
                        MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID == Convert.ToInt32(OperationStatusType.STRejected))
                    {
                        BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                        //btnActions.Enabled = false;
                    }
                }
            }

            errProWarehouseTransfer.Clear();
        }
        private bool UpdateStockTransferStatus(int intStatus, string strDescription, int intApprovedBy, string strApproveddate)
        {
            try
            {
                if (strDescription != "")
                    MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strVerificationDescription = strDescription;
                else
                    MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strVerificationDescription = "";
                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID = intStatus;

                if (intApprovedBy > 0)
                    MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intApprovedBy = intApprovedBy;
                else
                    MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intApprovedBy = 0;
                if (strApproveddate != "")
                    MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strApprovedDate = strApproveddate;
                else
                    MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strApprovedDate = "";


                if (MobjclsBLLWarehouseTransfer.UpdateStockTransferStatus())
                {

                    return true;
                }

                return false;
            }

            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());
                return false;
            }

        }
        private DataTable GetVerificationHistoryStatus(bool blnAllUsers)
        {
            //Show Comments
            string strCondition = "";
                      
                    strCondition += "VH.OperationTypeID = " + (int)OperationType.StockTransfer + " And VH.ReferenceID = " + MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID + " And VH.StatusID In (" + (int)OperationStatusType.STSuggested + "," + (int)OperationStatusType.STDenied + ")";
                    if (!blnAllUsers)
                        strCondition += " And VH.VerifiedBy = " + ClsCommonSettings.UserID;
                   
           
            return MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "VH.StatusID,Convert(varchar(10),VH.VerifiedDate,103) as VerifiedDate,UM.UserID,UM.UserName,ST.Description as Status,VH.Remarks as Comment", "STVerificationHistory VH Inner Join UserMaster UM On UM.UserID = VH.VerifiedBy Inner Join STCommonStatusReference ST On ST.StatusID = VH.StatusID", strCondition });
        }
        //Control   Events----------------------------------
        private void FrmWarehouseTransfer_Load(object sender, EventArgs e)
        {
            LoadMessage();
            SetPermissions();
            LoadCombos(0);

            dtpSFrom.Value = dtpSTo.Value = ClsCommonSettings.GetServerDate();
            AddNewStockTransfer();
            
            if (PintStockTransferIDToLoad != 0)
            {
                BindingNavigatorCancelItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                MblnAddStatus = false;
                MblnIsFrmApproval = true;
                DisplayOrderInfo(PintStockTransferIDToLoad);
            }
         
            //GenerateOrderNo();
        }
        private void FrmWarehouseTransfer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.MdiFormClosing)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4214, out MmessageIcon).Replace("#", "").Trim();
                if (!MblnChangeStatus || MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                    DialogResult.Yes)
                {
                }
                else
                {
                    e.Cancel = true;
                }
            }

        }

        private void cboXVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
           
                if (Convert.ToInt32(cboXVendor.SelectedValue) > 0)
                {
                    AddToAddressMenu(Convert.ToInt32(cboXVendor.SelectedValue));
                    BtnTVenderAddress.Text = CMSVendorAddress.Items[0].Text;
                    lblVendorAddress.Text = CMSVendorAddress.Items[0].Text + " Address";
                    MintVendorAddID = Convert.ToInt32(CMSVendorAddress.Items[0].Tag);
                    DisplayAddress();
                }
            
            Changestatus();

        }

        private void btnAddressChange_Click(object sender, EventArgs e)
        {
            CMSVendorAddress.Show(btnTContextmenu, btnTContextmenu.PointToClient(System.Windows.Forms.Cursor.Position));
        }

        private void cboXTransferType_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }

        private void dgvItemDetail_Textbox_TextChanged(object sender, EventArgs e)
        {
            //Item Selection Event 
            try
            {
                //if (dgvItemDetail.CurrentCell.ColumnIndex == 0 || dgvItemDetail.CurrentCell.ColumnIndex == 1)
                //{
                //    for (int i = 0; i < dgvItemDetail.Columns.Count; i++)
                //        dgvItemDetail.Rows[dgvItemDetail.CurrentCell.RowIndex].Cells[i].Value = null;
                //}
                dgvItemDetail.PServerName = ClsCommonSettings.ServerName;
                string[] First = null;
                string[] second = null;

                First = new string[] { "dgvColItemCode", "dgvColItemName", "dgvColItemID", "dgvColBatchID", "dgvColBatchName", "dgvColRate" };//first grid 
                second = new string[] { "ItemCode", "Description", "ItemID", "BatchID", "BatchNo","Rate" };//inner grid

                dgvItemDetail.aryFirstGridParam = First;
                dgvItemDetail.arySecondGridParam = second;
                dgvItemDetail.PiFocusIndex = dgvColQty.Index;
                dgvItemDetail.iGridWidth = 500;
                dgvItemDetail.bBothScrollBar = true;
                dgvItemDetail.ColumnsToHide = new string[] { "ItemID", "BatchID", "BatchNo","ExpiryDate" };

                DataTable dtDatasource = new DataTable();
                if(cboXTransferType.SelectedValue.ToInt32() == (int)OperationOrderType.STWareHouseToWareHouseTransfer || cboXTransferType.SelectedValue.ToInt32() == (int)OperationOrderType.STWareHouseToWareHouseTransferDemo)
                    dtDatasource = MobjclsBLLWarehouseTransfer.GetDataForItemSelection(ClsCommonSettings.CompanyID, Convert.ToInt32(cboCurrency.SelectedValue),cboXFromWarehouse.SelectedValue.ToInt32(),true);
                else
                    dtDatasource = MobjclsBLLWarehouseTransfer.GetDataForItemSelection(ClsCommonSettings.CompanyID, Convert.ToInt32(cboCurrency.SelectedValue), cboXFromWarehouse.SelectedValue.ToInt32(), false);

                if (dgvItemDetail.CurrentCell.ColumnIndex == dgvColItemCode.Index)
                {
                    dgvItemDetail.CurrentCell.Value = dgvItemDetail.TextBoxText;
                    dgvItemDetail.field = "ItemCode";
                }
                else if (dgvItemDetail.CurrentCell.ColumnIndex == dgvColItemName.Index)
                {
                    dgvItemDetail.CurrentCell.Value = dgvItemDetail.TextBoxText;
                    dgvItemDetail.field = "Description";
                }
                string strFilterString = dgvItemDetail.field + " Like '" + dgvItemDetail.TextBoxText.ToUpper() + "%'";
                dtDatasource.DefaultView.RowFilter = strFilterString;
                DataTable datTemp = dtDatasource.DefaultView.ToTable();
                datTemp.DefaultView.Sort = dgvItemDetail.field;

                dgvItemDetail.dtDataSource = datTemp.DefaultView.ToTable();
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on dgvItemDetail_Textbox_TextChanged() -" + Ex.Message.ToString());
            }
        }

        private void dgvItemDetail_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == dgvColItemCode.Index) || (e.ColumnIndex == dgvColItemName.Index))
            {
                dgvItemDetail.PiColumnIndex = e.ColumnIndex;
            }
        }

        private void dgvItemDetail_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (cboXTransferType.SelectedValue.ToInt32() == 0 || cboXFromWarehouse.SelectedValue.ToInt32() == 0 || cboCompany.SelectedValue.ToInt32() == 0)
                e.Cancel = true;
            else
            {
                if (e.ColumnIndex == dgvColUom.Index || e.ColumnIndex == dgvColDiscount.Index)
                {
                    if (dgvItemDetail.CurrentRow.Cells["dgvColItemID"].Value != null)
                    {
                        int iItemID = Convert.ToInt32(dgvItemDetail.CurrentRow.Cells["dgvColItemID"].Value);
                        int tag = -1;
                        if (e.ColumnIndex == dgvColUom.Index)
                        {
                            if (dgvItemDetail.CurrentRow.Cells["dgvColItemID"].Tag != null)
                            {
                                tag = Convert.ToInt32(dgvItemDetail.CurrentRow.Cells["dgvColItemID"].Tag);
                            }
                            FillComboColumn(iItemID);
                            if (tag != -1)
                                dgvItemDetail.CurrentRow.Cells["dgvColUom"].Tag = tag;
                        }
                        else if (e.ColumnIndex == dgvColDiscount.Index)
                        {
                            if (dgvItemDetail.CurrentRow.Cells["dgvColDiscount"].Tag != null)
                            {
                                tag = Convert.ToInt32(dgvItemDetail.CurrentRow.Cells["dgvColDiscount"].Tag);
                            }
                            FillDiscountColumn(e.RowIndex, false);

                            if (tag != -1)
                                dgvItemDetail.CurrentRow.Cells["dgvColDiscount"].Tag = tag;
                        }
                    }
                    else
                    {
                        dgvColUom.DataSource = null;
                        dgvColDiscount.DataSource = null;
                    }

                }
                Changestatus();
            }
        }

        private void dgvItemDetail_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    dgvItemDetail.EndEdit();
                    if (e.ColumnIndex == dgvColUom.Index)
                    {
                        dgvItemDetail.CurrentRow.Cells["dgvColUom"].Tag = dgvItemDetail.CurrentRow.Cells["dgvColUom"].Value;
                        dgvItemDetail.CurrentRow.Cells["dgvColUom"].Value = dgvItemDetail.CurrentRow.Cells["dgvColUom"].FormattedValue;
                        dgvItemDetail.Rows[e.RowIndex].Cells[dgvColRate.Index].Value = MobjClsCommonUtility.GetSaleRate(dgvItemDetail.CurrentRow.Cells[dgvColItemID.Index].Value.ToInt32(), dgvItemDetail.CurrentRow.Cells[dgvColUom.Index].Tag.ToInt32(), dgvItemDetail.CurrentRow.Cells[dgvColBatchID.Index].Value.ToInt64(), cboCompany.SelectedValue.ToInt32());
                        FillDiscountColumn(e.RowIndex, false);
                    }
                    if (e.ColumnIndex == dgvColDiscount.Index)
                    {
                        dgvItemDetail.CurrentRow.Cells["dgvColDiscount"].Tag = dgvItemDetail.CurrentRow.Cells["dgvColDiscount"].Value;
                        dgvItemDetail.CurrentRow.Cells["dgvColDiscount"].Value = dgvItemDetail.CurrentRow.Cells["dgvColDiscount"].FormattedValue;
                    }
                }
                if (e.ColumnIndex == dgvColQty.Index || e.ColumnIndex == dgvColRate.Index || e.ColumnIndex == dgvColDiscount.Index)
                {
                    CalculateTotalAmt();
                    CalculateNetTotal();
                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in dgvItemDetail_CellEndEdit() " + Ex.Message);
                MObjLogs.WriteLog("Error in dgvItemDetail_CellEndEdit() " + Ex.Message, 2);
            }
        }
        private void DgvStockTransferDisplay_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DgvStockTransferDisplay.Rows.Count > 0)
                {
                    if (Convert.ToInt64(DgvStockTransferDisplay.CurrentRow.Cells[0].Value) != 0)
                    {
                        MblnAddStatus = false;
                        ClearAllControls();
                        //MblnIsFrmApproval = false;
                        DisplayOrderInfo(Convert.ToInt64(DgvStockTransferDisplay.CurrentRow.Cells[0].Value));
                        BtnPrint.Enabled = MblnPrintEmailPermission;
                        BtnEmail.Enabled = MblnPrintEmailPermission;

                        string strPurchaseNo = " StockTransfer ";
                        strPurchaseNo += DgvStockTransferDisplay.CurrentRow.Cells[1].Value.ToString();
                        lblWarehouseTransferStatus.Text = " Details of " + strPurchaseNo;
                    }
                }
            }
            catch (Exception Ex)
            {
                MObjLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " in " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + "-" + Ex.Message.ToString());
            }
        }
        private void cboCurrency_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cboCurrency.SelectedValue != null)
            {
                DataTable datCurrency = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "CurrencyID,Scale", "CurrencyReference", "CurrencyID = " + Convert.ToInt32(cboCurrency.SelectedValue) });
                if (datCurrency.Rows.Count > 0)
                    MintExchangeCurrencyScale = Convert.ToInt32(datCurrency.Rows[0]["Scale"]);
            }
            CalculateTotalAmt();
            CalculateNetTotal();
            CalculateExchangeCurrencyRate();
            Changestatus();
        }

        private void cboCompany_SelectedValueChanged(object sender, EventArgs e)
        {
          
        }
        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {

            try
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1527, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;

                if (Convert.ToInt32(txtTransferNo.Tag) > 0)
                {
                    if (MobjclsBLLWarehouseTransfer.IsReferenceExistsInReceipt(Convert.ToInt64(txtTransferNo.Tag)))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1544, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        tmrWarehouseTransfer.Enabled = true;
                    }
                    else
                    {
                        MobjclsBLLWarehouseTransfer.DeleteStockTransferInfo();
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1528, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        tmrWarehouseTransfer.Enabled = true;
                        AddNewStockTransfer();
                        DisplayAllNoInSearchGrid();
                    }
                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());
            }
        }
        private void BindingNavigatorClearItem_Click(object sender, EventArgs e)
        {
            ClearAllControls();
        }
        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewStockTransfer();
        }
        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveStockTransferInfo())
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrWarehouseTransfer.Enabled = true;
                AddNewStockTransfer();
                DisplayAllNoInSearchGrid();
            }
        }
        private void bnSubmitForApproval_Click(object sender, EventArgs e)
        {
            try
            {

                if (dtpDueDate.Value < ClsCommonSettings.GetServerDate().Date)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 37, out MmessageIcon);
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    {

                        return;
                    }
                }
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1531, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID = Convert.ToInt32(txtTransferNo.Tag);
                    if (UpdateStockTransferStatus(Convert.ToInt32(OperationStatusType.STSubmittedForApproval), "", ClsCommonSettings.UserID, ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")))
                    {  int intOperationTypeID = Convert.ToInt32(OperationType.StockTransfer);
                        MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intCancelledBy = ClsCommonSettings.UserID;
                        MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strCancellationDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                        if (MobjclsBLLWarehouseTransfer.Suggest(intOperationTypeID))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1534, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            tmrWarehouseTransfer.Enabled = true;
                            DisplayAllNoInSearchGrid();
                            AddNewStockTransfer();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());
            }


        }

        private void bnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1526, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {

                    if (UpdateStockTransferStatus(Convert.ToInt32(OperationStatusType.STSubmitted), "", ClsCommonSettings.UserID, ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy"))) //1526
                    {
                        int intOperationTypeID = Convert.ToInt32(OperationType.StockTransfer);
                        MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intCancelledBy = ClsCommonSettings.UserID;
                        MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strCancellationDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                        if (MobjclsBLLWarehouseTransfer.Suggest(intOperationTypeID))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1534, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            tmrWarehouseTransfer.Enabled = true;
                            DisplayAllNoInSearchGrid();
                            AddNewStockTransfer();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());
            }
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1525, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                    {
                        objFrmCancellationDetails.Text = "Approval Details";
                        objFrmCancellationDetails.lblCancelledDate.Text = "Approval Date";
                        objFrmCancellationDetails.ShowDialog();
                        //dtpCancelledDate.Value = objFrmCancellationDetails.PDtCancellationDate;
                        if (!objFrmCancellationDetails.PBlnIsCanelled)
                        {
                            txtDescription.Text = objFrmCancellationDetails.PStrDescription;
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID = Convert.ToInt32(txtTransferNo.Tag);
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strDescription = objFrmCancellationDetails.PStrDescription;
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID = Convert.ToInt32(OperationStatusType.STApproved);
                            int intOperationTypeID = Convert.ToInt32(OperationType.StockTransfer);
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intCancelledBy = ClsCommonSettings.UserID;
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strCancellationDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");

                            if (MobjclsBLLWarehouseTransfer.Suggest(intOperationTypeID))
                            {


                                if (UpdateStockTransferStatus(Convert.ToInt32(OperationStatusType.STApproved), objFrmCancellationDetails.PStrDescription,
                                                        ClsCommonSettings.UserID, ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")))
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    tmrWarehouseTransfer.Enabled = true;
                                    DisplayAllNoInSearchGrid();
                                    AddNewStockTransfer();

                                }//1525
                            }
                        }
                    }

                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());
            }

        }

        private void btnSuggest_Click(object sender, EventArgs e)
        {
            try
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1523, out MmessageIcon);


                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {

                    using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                    {
                        int intOperationTypeID = 0;
                        objFrmCancellationDetails.Text = "Suggestion Details";
                        objFrmCancellationDetails.lblCancelledDate.Text = "Suggested Date";
                        objFrmCancellationDetails.ShowDialog();
                        if (!objFrmCancellationDetails.PBlnIsCanelled)
                        {
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strDescription = objFrmCancellationDetails.PStrDescription;
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID = Convert.ToInt32(OperationStatusType.STSuggested);
                            intOperationTypeID = Convert.ToInt32(OperationType.StockTransfer);
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intCancelledBy = ClsCommonSettings.UserID;
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strCancellationDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID = Convert.ToInt32(txtTransferNo.Tag);
                            if (MobjclsBLLWarehouseTransfer.Suggest(intOperationTypeID))
                            {
                                AddNewStockTransfer();
                                DisplayAllNoInSearchGrid();
                            }
                            else
                            {
                            }
                        }
                    }
                }
            }

            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());
            }

        }

        private void btnDeny_Click(object sender, EventArgs e)
        {
            try
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1524, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                    {
                        objFrmCancellationDetails.Text = "Suggestion Details";
                        objFrmCancellationDetails.lblCancelledDate.Text = "Suggested Date";
                        objFrmCancellationDetails.ShowDialog();
                        if (!objFrmCancellationDetails.PBlnIsCanelled)
                        {
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strDescription = objFrmCancellationDetails.PStrDescription;
                            int intOperationTypeID = 0;
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID = Convert.ToInt32(OperationStatusType.STDenied);
                            intOperationTypeID = Convert.ToInt32(OperationType.StockTransfer);
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intCancelledBy = ClsCommonSettings.UserID;
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strCancellationDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID = Convert.ToInt32(txtTransferNo.Tag);
                            if (MobjclsBLLWarehouseTransfer.Suggest(intOperationTypeID))
                            {
                                AddNewStockTransfer();
                                DisplayAllNoInSearchGrid();
                            }
                            else
                            {
                            }
                        }
                    }
                }
            }

            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());
            }
        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1532, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                    {
                        int intOperationTypeID = 0;
                        objFrmCancellationDetails.Text = "Reject Details";
                        objFrmCancellationDetails.lblCancelledDate.Text = "Rejected Date";
                        objFrmCancellationDetails.ShowDialog();
                        if (!objFrmCancellationDetails.PBlnIsCanelled)
                        {
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strDescription = objFrmCancellationDetails.PStrDescription;
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID = Convert.ToInt32(OperationStatusType.STRejected);
                            intOperationTypeID = Convert.ToInt32(OperationType.StockTransfer);
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intCancelledBy = ClsCommonSettings.UserID;
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strCancellationDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
                            MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID = Convert.ToInt32(txtTransferNo.Tag);
                            if (MobjclsBLLWarehouseTransfer.Suggest(intOperationTypeID))
                            {
                                if (UpdateStockTransferStatus(Convert.ToInt32(OperationStatusType.STRejected), objFrmCancellationDetails.PStrDescription,
                                    ClsCommonSettings.UserID, ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy")))
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    tmrWarehouseTransfer.Enabled = true;
                                    DisplayAllNoInSearchGrid();
                                    AddNewStockTransfer();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());

            }
        }
      
        private void BtnPrint_Click(object sender, EventArgs e)
        {
            if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID;
                ObjViewer.PiFormID = (int)FormID.StockTransfer;
                ObjViewer.ShowDialog();
            }
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
            {
                ObjEmailPopUp.MsSubject = "Stock Transfer";
                ObjEmailPopUp.EmailFormType = EmailFormID.StockTransfer;
                ObjEmailPopUp.EmailSource = MobjclsBLLWarehouseTransfer.GetStockTransferReport();
                ObjEmailPopUp.ShowDialog();
            }
        }

        private void BindingNavigatorCancelItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (BindingNavigatorCancelItem.Text.ToUpper() == "CANCEL")
                {

                    if (MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "M.ReceiptAndPaymentID", "AccReceiptAndPaymentMaster M INNER JOIN AccReceiptAndPaymentDetails D ON M.ReceiptAndPaymentID = D.ReceiptAndPaymentID", "M.OperationTypeID = " + (int)OperationType.StockTransfer + " And D.ReferenceID =" + Convert.ToInt32( txtTransferNo.Tag) + " AND M.ReceiptAndPaymentTypeID = " + (int)PaymentTypes.InvoiceExpense + "" }).Rows.Count > 0)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1535, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            tmrWarehouseTransfer.Enabled = true;
                            return;
                        }
                    
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1529, out MmessageIcon);
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (FrmCancellationDetails objFrmCancellationDetails = new FrmCancellationDetails(false))
                        {
                            objFrmCancellationDetails.ShowDialog();
                            //dtpCancelledDate.Value = objFrmCancellationDetails.PDtCancellationDate;
                            if (!objFrmCancellationDetails.PBlnIsCanelled)
                            {
                                txtDescription.Text = objFrmCancellationDetails.PStrDescription;
                                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strDescription = objFrmCancellationDetails.PStrDescription;
                                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.strCancellationDate = objFrmCancellationDetails.PDtCancellationDate.ToString("dd MMM yyyy");
                                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID = Convert.ToInt32(txtTransferNo.Tag);
                                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intCancelledBy = ClsCommonSettings.UserID;
                                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                               // MobjclsBLLWarehouseTransfer.SaveCancellationDetails();

                                MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID = Convert.ToInt32(OperationStatusType.STCancelled);
                                if (MobjclsBLLWarehouseTransfer.UpdateStockTransferStatus())
                                {
                                    //MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                                    //MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                    tmrWarehouseTransfer.Enabled = true;
                                    DisplayAllNoInSearchGrid();
                                    AddNewStockTransfer();
                                }
                            }
                        }

                    }
                }

                else//Reopen
                {

                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1530, out MmessageIcon);
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID = Convert.ToInt32(txtTransferNo.Tag);
                        MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStatusID = Convert.ToInt32(OperationStatusType.STOpened);
                        if (MobjclsBLLWarehouseTransfer.UpdateStockTransferStatus())
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblWarehouseTransferStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            tmrWarehouseTransfer.Enabled = true;
                            DisplayAllNoInSearchGrid();
                            AddNewStockTransfer();
                        }


                    }
                }


            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());
            }
        }
        private void MenuItem_Click(System.Object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(cboXVendor.SelectedValue) > 0)
                {
                    BtnTVenderAddress.Text = e.ClickedItem.Text;
                    lblVendorAddress.Text = e.ClickedItem.Text + " Address";
                    MintVendorAddID = Convert.ToInt32(e.ClickedItem.Tag);
                    DisplayAddress();
                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on " + sMethod + "-" + Ex.Message.ToString());

            }
        }
        private void cboXTransferType_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvItemDetail.Rows.Clear();
            if (cboXTransferType.SelectedValue.ToInt32() > 0)
            {
                lblXIncharge.Visible = true;
                cboxIncharge.Visible = true;
              
                if (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToEmployee) ||
                    cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToEmployeeDemo))
                {
                    lblToWarehouse.Text = "To Employee";
                    lblXIncharge.Visible = false;
                    cboxIncharge.Visible = false;
                    LoadCombos(6);
                }
                else if (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToWareHouseTransfer) ||
                    cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToWareHouseTransferDemo))
                {
                    lblToWarehouse.Text = "To Warehouse";
                    LoadCombos(19);
                }
                else if (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToProject))
                {
                    lblToWarehouse.Text = "To Project";
                    LoadCombos(16);//Load Project Combo
                }
                else if (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToDepartment))
                {
                    lblToWarehouse.Text = "To Department";
                    LoadCombos(17);//Load Department
                }
                Changestatus();
            }
            // Changestatus();
        }
        private void cboSCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboSCompany.SelectedValue.ToInt32() > 0)
                LoadCombos(11);

            DataTable datCombos = null;
            //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
            //{
                if (cboSCompany.SelectedValue == null || Convert.ToInt32(cboSCompany.SelectedValue) == -2)
                {
                    DataTable datCompany = (DataTable)cboSCompany.DataSource;
                    string strFilterCondition = "";
                    if (datCompany.Rows.Count > 0)
                    {
                        strFilterCondition = "CompanyID In (";
                        foreach (DataRow dr in datCompany.Rows)
                            strFilterCondition += Convert.ToInt32(dr["CompanyID"]) + ",";
                        strFilterCondition = strFilterCondition.Remove(strFilterCondition.Length - 1);
                        strFilterCondition += ")";
                    }
                    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "EmployeeID,EmployeeFullName", "EmployeeMaster", strFilterCondition });
                }
                else
                {
                    datCombos = MobjclsBLLWarehouseTransfer.FillCombos(new string[] { "EmployeeID,EmployeeFullName", "EmployeeMaster", "CompanyID = " + Convert.ToInt32(cboSCompany.SelectedValue) });
                }
            cboSIncharge.ValueMember = "EmployeeID";
            cboSIncharge.DisplayMember = "EmployeeFullName";
            cboSIncharge.DataSource = datCombos;
        }
        private void cboSTransferType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboSCompany.SelectedValue.ToInt32() > 0 && cboSTransferType.SelectedValue.ToInt32() > 0)
            {
                LoadCombos(12);
                LoadCombos(13);
               // LoadCombos(14);
                LoadCombos(15);
            }
        }
        private void btnSRefresh_Click(object sender, EventArgs e)
        {
            DisplayAllNoInSearchGrid();

        }

        private void txtLeadTime_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void dtpProductionDate_ValueChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void dtpTrnsShipmentDate_ValueChanged(object sender, EventArgs e)
        {
            Changestatus();

        }

        private void cboIncoTerms_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void dtpClearenceDate_ValueChanged(object sender, EventArgs e)
        {
            Changestatus();

        }
        
        private void cboToWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCombos(18);
        }
        private void cboDiscount_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalculateNetTotal();
            Changestatus();
        }

        private void btnAddExpense_Click(object sender, EventArgs e)
        {

            try
            {
                bool blnShow = false;
                bool blnCalculateExpense = true;

                if (MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID !=0 || txtExpenseAmount.Text.ToDecimal() != 0)
                {
                    blnShow = true;
                }
                if (blnShow)
                {
                    using (frmExpense objfrmExpense = new frmExpense(false))
                    {
                        objfrmExpense.intModuleID = 3;
                            objfrmExpense.PintOperationType = Convert.ToInt32(OperationType.StockTransfer) ;
                            objfrmExpense.PintOperationID = MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID;
                        objfrmExpense.PintCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                        if (PintStockTransferIDToLoad != 0)
                            objfrmExpense.PBlnIsEditable = false;

                        objfrmExpense.ShowDialog();
                        if (blnCalculateExpense)
                            txtExpenseAmount.Text = objfrmExpense.GetExpenseAmount(Convert.ToInt32(OperationType.StockTransfer), MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID, 0).ToString("F" + MintExchangeCurrencyScale);
                        CalculateNetTotal();
                        MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decGrandAmount = Convert.ToDecimal(txtSubtotal.Text);
                        MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decNetAmount = Convert.ToDecimal(txtTotalAmount.Text);
                        MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decGrandDiscountAmount = Convert.ToDecimal(txtDiscount.Text);
                        MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decNetAmountRounded = Math.Ceiling(MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decNetAmount);
                        MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.decExpenseAmount = txtExpenseAmount.Text.ToDecimal();
                        MobjclsBLLWarehouseTransfer.UpdateGrandTotalAmount();
                    }
                }
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnAddExpense_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in btnAddExpense_Click() " + ex.Message, 2);
            }
        }

        private void bnExpense_Click(object sender, EventArgs e)
        {
            btnAddExpense_Click(null, null);
        }

        private void dgvItemDetail_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            int ColumnIndex = Convert.ToInt32((((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex));

            e.Control.KeyPress -= this.TextboxNumeric_KeyPress;
            if (ColumnIndex == dgvColQty.Index)
            {
                e.Control.KeyPress += this.TextboxNumeric_KeyPress;
            }
        }

        private void dgvItemDetail_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvItemDetail.IsCurrentCellDirty)
            {
                if (dgvItemDetail.CurrentCell != null)
                    dgvItemDetail.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
        }

        private void cboXFromWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvItemDetail.Rows.Clear();
            if (cboXTransferType.SelectedValue.ToInt32() == (int)OperationOrderType.STWareHouseToWareHouseTransfer)
                LoadCombos(19);
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (MblnAddStatus)
                GenerateOrderNo();
            LoadCombos(3);
            if (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToEmployee) ||
                cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToEmployeeDemo))
            {
                LoadCombos(6);
            }
            else if (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToWareHouseTransfer) ||
                cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToWareHouseTransferDemo))
            {
                LoadCombos(19);
            }
            else if (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToProject))
            {
                LoadCombos(16);//Load Project Combo
            }
            else if (cboXTransferType.SelectedValue.ToInt32() == Convert.ToInt32(OperationOrderType.STWareHouseToDepartment))
            {
                LoadCombos(17);//Load Department
            }


            if (Convert.ToInt32(cboCompany.SelectedValue) > 0)
            {
                int intCurrencyID = 0;
                LoadCombos(4);
                LoadCombos(18);
                cboCurrency.Text = null;
                lblCurrencyAmnt.Text = "Amount in " + MobjclsBLLWarehouseTransfer.GetCompanyCurrency(Convert.ToInt32(cboCompany.SelectedValue), out MintBaseCurrencyScale, out intCurrencyID);
                cboCurrency.SelectedValue = intCurrencyID;
            }
            else
            {
                cboCurrency.DataSource = null;
                cboCurrency.SelectedIndex = -1;
                cboCurrency.Text = null;
                lblCurrencyAmnt.Text = "Amount in company currency";
                cboCurrency.SelectedIndex = -1;
            }
            Changestatus();

        }

        private void tcGeneral_SelectedTabChanged(object sender, TabStripTabChangedEventArgs e)
        {
            if (tcGeneral.SelectedTab == tiSuggestions)
            {
                DataTable datVerification = new DataTable();
                if (PintApprovePermission == (int)ApprovePermission.Approve || PintApprovePermission == (int)ApprovePermission.Both)
                    datVerification = GetVerificationHistoryStatus(true);
                else
                    datVerification = GetVerificationHistoryStatus(false);
                dgvSuggestions.Rows.Clear();
                for (int i = 0; i < datVerification.Rows.Count; i++)
                {
                    dgvSuggestions.Rows.Add();
                    dgvSuggestions[UserID.Index, i].Value = datVerification.Rows[i]["UserID"];
                    dgvSuggestions[UserName.Index, i].Value = datVerification.Rows[i]["UserName"];
                    dgvSuggestions[Status.Index, i].Value = datVerification.Rows[i]["Status"];
                    dgvSuggestions[VerifiedDate.Index, i].Value = datVerification.Rows[i]["VerifiedDate"];
                    dgvSuggestions[Comment.Index, i].Value = datVerification.Rows[i]["Comment"];
                    if (datVerification.Rows[i]["UserID"].ToInt32() != ClsCommonSettings.UserID)
                        dgvSuggestions[Comment.Index, i].ReadOnly = true;
                }
            }
        }

        private void dgvSuggestions_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == Comment.Index)
            {
                if (dgvSuggestions.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                    MobjclsBLLWarehouseTransfer.UpdationVerificationTableRemarks(MobjclsBLLWarehouseTransfer.PobjClsDTOWarehouseTransfer.intStockTransferID, dgvSuggestions.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
            }

        }

        private void dgvItemDetail_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvColItemID.Index && dgvItemDetail.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value !=null )
                {
                    int tag = -1;
                    if (dgvItemDetail.Rows[e.RowIndex].Cells[dgvColUom.Index].Tag != null)
                    {
                        tag = Convert.ToInt32(dgvItemDetail.Rows[e.RowIndex].Cells[dgvColUom.Index].Tag);
                    }
                    if (dgvItemDetail.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value.ToInt32() != 0)
                    {
                        FillComboColumn(dgvItemDetail.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value.ToInt32());
                        if (tag == -1)
                        {
                            DataTable datDefaultUom = MobjClsCommonUtility.FillCombos(new string[] { "BaseUomID", "InvItemMaster", "ItemID = " + dgvItemDetail.Rows[e.RowIndex].Cells[dgvColItemID.Index].Value.ToInt32() + "" });
                            tag = Convert.ToInt32(datDefaultUom.Rows[0]["BaseUomID"]);
                        }
                    }
                    dgvItemDetail.Rows[e.RowIndex].Cells[dgvColUom.Index].Tag = tag;
                    dgvItemDetail.Rows[e.RowIndex].Cells[dgvColUom.Index].Value = tag;
                    dgvItemDetail.Rows[e.RowIndex].Cells[dgvColUom.Index].Value = dgvItemDetail.Rows[e.RowIndex].Cells[dgvColUom.Index].FormattedValue;
                }
                else if (e.ColumnIndex == dgvColRate.Index || e.ColumnIndex == dgvColQty.Index)
                {
                    CalculateTotalAmt();
                }
            }
        }

        private void txtTotalAmount_TextChanged(object sender, EventArgs e)
        {
            lblAmountInWords.Text = MobjClsCommonUtility.ConvertToWord(Convert.ToString(txtTotalAmount.Text), Convert.ToInt32(cboCurrency.SelectedValue));
              
        }

        private void lnkLabelAdvanceSearch_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DataTable datAdvanceSearchedData = new DataTable();
            using (FrmSearchForm objFrmSearchForm = new FrmSearchForm((int)OperationType.StockTransfer))
            {
                objFrmSearchForm.ShowDialog();
                if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
                {
                    datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
                    datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
                    datAdvanceSearchedData.Columns["ID"].ColumnName = "StockTransferID";
                    datAdvanceSearchedData.Columns["No"].ColumnName = "Stock TransferNo";
                    DgvStockTransferDisplay.DataSource = datAdvanceSearchedData;
                   DgvStockTransferDisplay.Columns[0].Visible = false;
                    DgvStockTransferDisplay.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

        private void btnSupplier_Click(object sender, EventArgs e)
        {
            try
            {
                int intVendorID = cboXVendor.SelectedValue.ToInt32();

                using (FrmVendor objVendor = new FrmVendor(2))
                {
                    objVendor.PintVendorID = intVendorID;
                    objVendor.ShowDialog();
                    LoadCombos(5);
                    if (objVendor.PintVendorID != 0 && cboxIncharge.Enabled)
                        cboXVendor.SelectedValue = objVendor.PintVendorID;
                    else
                        cboXVendor.SelectedValue = intVendorID;
                }

                if (Convert.ToInt32(cboXVendor.SelectedValue) > 0)
                        AddToAddressMenu(Convert.ToInt32(cboXVendor.SelectedValue));

                Changestatus();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in btnSupplier_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in btnSupplier_Click() " + ex.Message, 2);
            }
        }

        private void expandableSplitterLeft_ExpandedChanged(object sender, ExpandedChangeEventArgs e)
        {
            if (expandableSplitterLeft.Expanded)
            {
                if (DgvStockTransferDisplay.Columns.Count > 0)
                    DgvStockTransferDisplay.Columns[0].Visible = false;
            }
        }

        private void dgvItemDetail_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjClsCommonUtility.SetSerialNo(dgvItemDetail, e.RowIndex, false);
        }

        private void dgvItemDetail_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjClsCommonUtility.SetSerialNo(dgvItemDetail, e.RowIndex, false);
        }

        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void cboXTransferType_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboXTransferType.DroppedDown = false;
        }

        private void cboXFromWarehouse_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboXFromWarehouse.DroppedDown = false;
        }

        private void cboToWarehouse_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboToWarehouse.DroppedDown = false;
        }

        private void cboxIncharge_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboxIncharge.DroppedDown = false;
        }

        private void cboXVendor_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboXVendor.DroppedDown = false;
        }

        private void cboCurrency_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCurrency.DroppedDown = false;
        }


    }
}
