﻿namespace MyBooksERP
{
    partial class FrmStockAdjustment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmStockAdjustment));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.TmrStatus = new System.Windows.Forms.Timer(this.components);
            this.ErrorStatus = new System.Windows.Forms.ErrorProvider(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.panelRight = new DevComponents.DotNetBar.PanelEx();
            this.panelBottom = new DevComponents.DotNetBar.PanelEx();
            this.expandableSplitter1 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.tcGeneral = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblAdjustmentDate = new System.Windows.Forms.Label();
            this.lblReason = new System.Windows.Forms.Label();
            this.lblAdjustmentNo = new System.Windows.Forms.Label();
            this.btnReason = new DevComponents.DotNetBar.ButtonX();
            this.dtpAdjustmentDate = new System.Windows.Forms.DateTimePicker();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.lblCompany = new System.Windows.Forms.Label();
            this.cboWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtAdjustmentNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboReason = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.tabItem1 = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel4 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvSuggestions = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.UserID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VerifiedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabItem2 = new DevComponents.DotNetBar.TabItem(this.components);
            this.tcStockAdjustment = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvStockAdjustment = new ClsInnerGridBar();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboBatchNo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ActualQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NewQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Difference = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LIFORate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.dgvLocationDetails = new ClsInnerGridBar();
            this.LItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LBatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LBatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LLocation = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LRow = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LBlock = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LLot = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LActualQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LCurrentQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LNewQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LDifference = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiItemDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.tiLocationDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.panelGridBottom = new DevComponents.DotNetBar.PanelEx();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.lblCreatedByText = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblCreatedDateValue = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblStockAdjustmentStatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.txtRemarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.BindingNavigatorStockAdjustment = new DevComponents.DotNetBar.Bar();
            this.BindingNavigatorAddNewItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorSaveItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorClearItem = new DevComponents.DotNetBar.ButtonItem();
            this.BindingNavigatorDeleteItem = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.BtnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.BtnHelp = new DevComponents.DotNetBar.ButtonItem();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvStockAdjustmentDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelLeftTop = new DevComponents.DotNetBar.PanelEx();
            this.lnkLabelAdvanceSearch = new System.Windows.Forms.LinkLabel();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblSAdjustmentNo = new System.Windows.Forms.Label();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cboSEmployee = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblEmployee = new System.Windows.Forms.Label();
            this.btnSRefresh = new DevComponents.DotNetBar.ButtonX();
            this.txtSAdjustmentNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboSCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSCompany = new System.Windows.Forms.Label();
            this.lblStockAdjustmentCount = new DevComponents.DotNetBar.LabelX();
            this.tiGeneral = new DevComponents.DotNetBar.TabItem(this.components);
            this.tiSuggestions = new DevComponents.DotNetBar.TabItem(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorStatus)).BeginInit();
            this.panelEx1.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelBottom.SuspendLayout();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).BeginInit();
            this.tcGeneral.SuspendLayout();
            this.tabControlPanel3.SuspendLayout();
            this.tabControlPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSuggestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcStockAdjustment)).BeginInit();
            this.tcStockAdjustment.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStockAdjustment)).BeginInit();
            this.tabControlPanel2.SuspendLayout();
            this.panelEx2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocationDetails)).BeginInit();
            this.panelGridBottom.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigatorStockAdjustment)).BeginInit();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStockAdjustmentDisplay)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnSave
            // 
            this.BtnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSave.Location = new System.Drawing.Point(360, 526);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 10;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnCancel.Location = new System.Drawing.Point(1065, 526);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 12;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnOk.Enabled = false;
            this.BtnOk.Location = new System.Drawing.Point(987, 526);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 11;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // TmrStatus
            // 
            this.TmrStatus.Interval = 2000;
            this.TmrStatus.Tick += new System.EventHandler(this.TmrStatus_Tick);
            // 
            // ErrorStatus
            // 
            this.ErrorStatus.ContainerControl = this;
            this.ErrorStatus.RightToLeft = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.Frozen = true;
            this.dataGridViewTextBoxColumn2.HeaderText = "Item Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn3.Frozen = true;
            this.dataGridViewTextBoxColumn3.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Visible = false;
            this.dataGridViewTextBoxColumn3.Width = 120;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle21.Format = "N3";
            dataGridViewCellStyle21.NullValue = null;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewTextBoxColumn4.Frozen = true;
            this.dataGridViewTextBoxColumn4.HeaderText = "Current Quantity";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 120;
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewTextBoxColumn5.Frozen = true;
            this.dataGridViewTextBoxColumn5.HeaderText = "New Quantity";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 75;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewTextBoxColumn6.Frozen = true;
            this.dataGridViewTextBoxColumn6.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "ItemID";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewTextBoxColumn8.Frozen = true;
            this.dataGridViewTextBoxColumn8.HeaderText = "ActualQty";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx1.Controls.Add(this.panelRight);
            this.panelEx1.Controls.Add(this.expandableSplitterLeft);
            this.panelEx1.Controls.Add(this.PanelLeft);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(0, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(1153, 582);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 101;
            this.panelEx1.Text = "panelEx1";
            // 
            // panelRight
            // 
            this.panelRight.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelRight.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelRight.Controls.Add(this.panelBottom);
            this.panelRight.Controls.Add(this.panelTop);
            this.panelRight.Controls.Add(this.BindingNavigatorStockAdjustment);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRight.Location = new System.Drawing.Point(203, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(950, 582);
            this.panelRight.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelRight.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelRight.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelRight.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelRight.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelRight.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelRight.Style.GradientAngle = 90;
            this.panelRight.TabIndex = 1;
            // 
            // panelBottom
            // 
            this.panelBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelBottom.Controls.Add(this.expandableSplitter1);
            this.panelBottom.Controls.Add(this.tcStockAdjustment);
            this.panelBottom.Controls.Add(this.panelGridBottom);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBottom.Location = new System.Drawing.Point(0, 133);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(950, 449);
            this.panelBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelBottom.Style.GradientAngle = 90;
            this.panelBottom.TabIndex = 1;
            this.panelBottom.Text = "panelEx3";
            // 
            // expandableSplitter1
            // 
            this.expandableSplitter1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitter1.ExpandableControl = this.panelTop;
            this.expandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitter1.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.Location = new System.Drawing.Point(0, 0);
            this.expandableSplitter1.Name = "expandableSplitter1";
            this.expandableSplitter1.Size = new System.Drawing.Size(950, 3);
            this.expandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitter1.TabIndex = 216;
            this.expandableSplitter1.TabStop = false;
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelTop.Controls.Add(this.tcGeneral);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(950, 108);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 106;
            // 
            // tcGeneral
            // 
            this.tcGeneral.BackColor = System.Drawing.Color.Transparent;
            this.tcGeneral.CanReorderTabs = true;
            this.tcGeneral.Controls.Add(this.tabControlPanel3);
            this.tcGeneral.Controls.Add(this.tabControlPanel4);
            this.tcGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcGeneral.Location = new System.Drawing.Point(0, 0);
            this.tcGeneral.Name = "tcGeneral";
            this.tcGeneral.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcGeneral.SelectedTabIndex = 0;
            this.tcGeneral.Size = new System.Drawing.Size(950, 108);
            this.tcGeneral.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcGeneral.TabIndex = 0;
            this.tcGeneral.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcGeneral.Tabs.Add(this.tabItem1);
            this.tcGeneral.Tabs.Add(this.tabItem2);
            this.tcGeneral.TabStop = false;
            this.tcGeneral.Text = "tabControl1";
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel3.Controls.Add(this.cboCompany);
            this.tabControlPanel3.Controls.Add(this.lblAdjustmentDate);
            this.tabControlPanel3.Controls.Add(this.lblReason);
            this.tabControlPanel3.Controls.Add(this.lblAdjustmentNo);
            this.tabControlPanel3.Controls.Add(this.btnReason);
            this.tabControlPanel3.Controls.Add(this.dtpAdjustmentDate);
            this.tabControlPanel3.Controls.Add(this.lblWarehouse);
            this.tabControlPanel3.Controls.Add(this.lblCompany);
            this.tabControlPanel3.Controls.Add(this.cboWarehouse);
            this.tabControlPanel3.Controls.Add(this.txtAdjustmentNo);
            this.tabControlPanel3.Controls.Add(this.cboReason);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(950, 86);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 1;
            this.tabControlPanel3.TabItem = this.tabItem1;
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 75;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(107, 8);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(190, 20);
            this.cboCompany.TabIndex = 2;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCompany_KeyPress);
            // 
            // lblAdjustmentDate
            // 
            this.lblAdjustmentDate.AutoSize = true;
            this.lblAdjustmentDate.BackColor = System.Drawing.Color.Transparent;
            this.lblAdjustmentDate.Location = new System.Drawing.Point(330, 38);
            this.lblAdjustmentDate.Name = "lblAdjustmentDate";
            this.lblAdjustmentDate.Size = new System.Drawing.Size(85, 13);
            this.lblAdjustmentDate.TabIndex = 210;
            this.lblAdjustmentDate.Text = "Adjustment Date";
            // 
            // lblReason
            // 
            this.lblReason.AutoSize = true;
            this.lblReason.BackColor = System.Drawing.Color.Transparent;
            this.lblReason.Location = new System.Drawing.Point(23, 60);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(44, 13);
            this.lblReason.TabIndex = 212;
            this.lblReason.Text = "Reason";
            // 
            // lblAdjustmentNo
            // 
            this.lblAdjustmentNo.AutoSize = true;
            this.lblAdjustmentNo.BackColor = System.Drawing.Color.Transparent;
            this.lblAdjustmentNo.Location = new System.Drawing.Point(330, 12);
            this.lblAdjustmentNo.Name = "lblAdjustmentNo";
            this.lblAdjustmentNo.Size = new System.Drawing.Size(76, 13);
            this.lblAdjustmentNo.TabIndex = 211;
            this.lblAdjustmentNo.Text = "Adjustment No";
            // 
            // btnReason
            // 
            this.btnReason.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnReason.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnReason.Location = new System.Drawing.Point(302, 61);
            this.btnReason.Name = "btnReason";
            this.btnReason.Size = new System.Drawing.Size(25, 20);
            this.btnReason.TabIndex = 5;
            this.btnReason.Text = "...";
            this.btnReason.Click += new System.EventHandler(this.btnReason_Click);
            // 
            // dtpAdjustmentDate
            // 
            this.dtpAdjustmentDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpAdjustmentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAdjustmentDate.Location = new System.Drawing.Point(421, 35);
            this.dtpAdjustmentDate.Name = "dtpAdjustmentDate";
            this.dtpAdjustmentDate.Size = new System.Drawing.Size(103, 20);
            this.dtpAdjustmentDate.TabIndex = 7;
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.BackColor = System.Drawing.Color.Transparent;
            this.lblWarehouse.Location = new System.Drawing.Point(23, 37);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(62, 13);
            this.lblWarehouse.TabIndex = 214;
            this.lblWarehouse.Text = "Warehouse";
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.BackColor = System.Drawing.Color.Transparent;
            this.lblCompany.Location = new System.Drawing.Point(23, 12);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 213;
            this.lblCompany.Text = "Company";
            // 
            // cboWarehouse
            // 
            this.cboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWarehouse.DisplayMember = "Text";
            this.cboWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWarehouse.DropDownHeight = 75;
            this.cboWarehouse.FormattingEnabled = true;
            this.cboWarehouse.IntegralHeight = false;
            this.cboWarehouse.ItemHeight = 14;
            this.cboWarehouse.Location = new System.Drawing.Point(107, 35);
            this.cboWarehouse.Name = "cboWarehouse";
            this.cboWarehouse.Size = new System.Drawing.Size(190, 20);
            this.cboWarehouse.TabIndex = 3;
            this.cboWarehouse.SelectedIndexChanged += new System.EventHandler(this.cboWarehouse_SelectedIndexChanged);
            this.cboWarehouse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboWarehouse_KeyPress);
            this.cboWarehouse.TextChanged += new System.EventHandler(this.cboWarehouse_TextChanged);
            // 
            // txtAdjustmentNo
            // 
            this.txtAdjustmentNo.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtAdjustmentNo.Border.Class = "TextBoxBorder";
            this.txtAdjustmentNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtAdjustmentNo.Location = new System.Drawing.Point(421, 8);
            this.txtAdjustmentNo.MaxLength = 20;
            this.txtAdjustmentNo.Name = "txtAdjustmentNo";
            this.txtAdjustmentNo.Size = new System.Drawing.Size(103, 20);
            this.txtAdjustmentNo.TabIndex = 6;
            this.txtAdjustmentNo.TabStop = false;
            // 
            // cboReason
            // 
            this.cboReason.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboReason.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboReason.DisplayMember = "Text";
            this.cboReason.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboReason.DropDownHeight = 75;
            this.cboReason.FormattingEnabled = true;
            this.cboReason.IntegralHeight = false;
            this.cboReason.ItemHeight = 14;
            this.cboReason.Location = new System.Drawing.Point(107, 60);
            this.cboReason.Name = "cboReason";
            this.cboReason.Size = new System.Drawing.Size(190, 20);
            this.cboReason.TabIndex = 4;
            this.cboReason.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboReason_KeyPress);
            // 
            // tabItem1
            // 
            this.tabItem1.AttachedControl = this.tabControlPanel3;
            this.tabItem1.Name = "tabItem1";
            this.tabItem1.Text = "General";
            // 
            // tabControlPanel4
            // 
            this.tabControlPanel4.Controls.Add(this.dgvSuggestions);
            this.tabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel4.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel4.Name = "tabControlPanel4";
            this.tabControlPanel4.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel4.Size = new System.Drawing.Size(950, 86);
            this.tabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel4.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel4.Style.GradientAngle = 90;
            this.tabControlPanel4.TabIndex = 2;
            this.tabControlPanel4.TabItem = this.tabItem2;
            // 
            // dgvSuggestions
            // 
            this.dgvSuggestions.AllowUserToDeleteRows = false;
            this.dgvSuggestions.BackgroundColor = System.Drawing.Color.White;
            this.dgvSuggestions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSuggestions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UserID,
            this.UserName,
            this.Status,
            this.VerifiedDate,
            this.Comment});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSuggestions.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSuggestions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSuggestions.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSuggestions.Location = new System.Drawing.Point(1, 1);
            this.dgvSuggestions.Name = "dgvSuggestions";
            this.dgvSuggestions.Size = new System.Drawing.Size(948, 84);
            this.dgvSuggestions.TabIndex = 0;
            // 
            // UserID
            // 
            this.UserID.HeaderText = "UserID";
            this.UserID.Name = "UserID";
            this.UserID.Visible = false;
            // 
            // UserName
            // 
            this.UserName.HeaderText = "User Name";
            this.UserName.Name = "UserName";
            this.UserName.ReadOnly = true;
            this.UserName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.UserName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.UserName.Width = 250;
            // 
            // Status
            // 
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // VerifiedDate
            // 
            this.VerifiedDate.HeaderText = "Verified Date";
            this.VerifiedDate.Name = "VerifiedDate";
            this.VerifiedDate.ReadOnly = true;
            this.VerifiedDate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.VerifiedDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Comment
            // 
            this.Comment.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Comment.HeaderText = "Comment";
            this.Comment.Name = "Comment";
            this.Comment.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Comment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabItem2
            // 
            this.tabItem2.AttachedControl = this.tabControlPanel4;
            this.tabItem2.Name = "tabItem2";
            this.tabItem2.Text = "Suggestions";
            this.tabItem2.Visible = false;
            // 
            // tcStockAdjustment
            // 
            this.tcStockAdjustment.BackColor = System.Drawing.Color.Transparent;
            this.tcStockAdjustment.CanReorderTabs = true;
            this.tcStockAdjustment.Controls.Add(this.tabControlPanel1);
            this.tcStockAdjustment.Controls.Add(this.tabControlPanel2);
            this.tcStockAdjustment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcStockAdjustment.Location = new System.Drawing.Point(0, 0);
            this.tcStockAdjustment.Name = "tcStockAdjustment";
            this.tcStockAdjustment.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcStockAdjustment.SelectedTabIndex = 0;
            this.tcStockAdjustment.Size = new System.Drawing.Size(950, 359);
            this.tcStockAdjustment.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcStockAdjustment.TabIndex = 8;
            this.tcStockAdjustment.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.MultilineNoNavigationBox;
            this.tcStockAdjustment.Tabs.Add(this.tiItemDetails);
            this.tcStockAdjustment.Tabs.Add(this.tiLocationDetails);
            this.tcStockAdjustment.Text = "tabControl1";
            this.tcStockAdjustment.SelectedTabChanged += new DevComponents.DotNetBar.TabStrip.SelectedTabChangedEventHandler(this.tcStockAdjustment_SelectedTabChanged);
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.dgvStockAdjustment);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(950, 337);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 9;
            // 
            // dgvStockAdjustment
            // 
            this.dgvStockAdjustment.AddNewRow = false;
            this.dgvStockAdjustment.AlphaNumericCols = new int[0];
            this.dgvStockAdjustment.BackgroundColor = System.Drawing.Color.White;
            this.dgvStockAdjustment.CapsLockCols = new int[0];
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvStockAdjustment.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvStockAdjustment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvStockAdjustment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.cboBatchNo,
            this.ActualQty,
            this.CurrentQty,
            this.NewQty,
            this.Difference,
            this.LIFORate,
            this.ItemID});
            this.dgvStockAdjustment.DecimalCols = new int[0];
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvStockAdjustment.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvStockAdjustment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvStockAdjustment.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvStockAdjustment.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvStockAdjustment.HasSlNo = false;
            this.dgvStockAdjustment.LastRowIndex = 0;
            this.dgvStockAdjustment.Location = new System.Drawing.Point(1, 1);
            this.dgvStockAdjustment.Name = "dgvStockAdjustment";
            this.dgvStockAdjustment.NegativeValueCols = new int[0];
            this.dgvStockAdjustment.NumericCols = new int[0];
            this.dgvStockAdjustment.RowHeadersWidth = 50;
            this.dgvStockAdjustment.Size = new System.Drawing.Size(948, 335);
            this.dgvStockAdjustment.TabIndex = 10;
            this.dgvStockAdjustment.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStockAdjustment_CellValueChanged);
            this.dgvStockAdjustment.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvStockAdjustment_CellBeginEdit);
            this.dgvStockAdjustment.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStockAdjustment_CellLeave);
            this.dgvStockAdjustment.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvStockAdjustment_RowsAdded);
            this.dgvStockAdjustment.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStockAdjustment_CellEndEdit);
            this.dgvStockAdjustment.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvStockAdjustment_Textbox_TextChanged);
            this.dgvStockAdjustment.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvStockAdjustment_EditingControlShowing);
            this.dgvStockAdjustment.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvStockAdjustment_CurrentCellDirtyStateChanged);
            this.dgvStockAdjustment.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvStockAdjustment_DataError);
            this.dgvStockAdjustment.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStockAdjustment_CellEnter);
            this.dgvStockAdjustment.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvStockAdjustment_RowsRemoved);
            this.dgvStockAdjustment.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStockAdjustment_CellContentClick);
            // 
            // ItemCode
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ItemCode.DefaultCellStyle = dataGridViewCellStyle3;
            this.ItemCode.HeaderText = "Item Code";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.Width = 216;
            // 
            // ItemName
            // 
            this.ItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ItemName.DefaultCellStyle = dataGridViewCellStyle4;
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.Name = "ItemName";
            // 
            // cboBatchNo
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.cboBatchNo.DefaultCellStyle = dataGridViewCellStyle5;
            this.cboBatchNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboBatchNo.HeaderText = "Batch No.";
            this.cboBatchNo.Name = "cboBatchNo";
            this.cboBatchNo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cboBatchNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cboBatchNo.Visible = false;
            this.cboBatchNo.Width = 5;
            // 
            // ActualQty
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ActualQty.DefaultCellStyle = dataGridViewCellStyle6;
            this.ActualQty.HeaderText = "Actual Qty";
            this.ActualQty.Name = "ActualQty";
            this.ActualQty.ReadOnly = true;
            this.ActualQty.Visible = false;
            // 
            // CurrentQty
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.CurrentQty.DefaultCellStyle = dataGridViewCellStyle7;
            this.CurrentQty.HeaderText = "Current Qty";
            this.CurrentQty.Name = "CurrentQty";
            this.CurrentQty.ReadOnly = true;
            this.CurrentQty.Width = 120;
            // 
            // NewQty
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.NullValue = null;
            this.NewQty.DefaultCellStyle = dataGridViewCellStyle8;
            this.NewQty.HeaderText = "New Qty";
            this.NewQty.MaxInputLength = 10;
            this.NewQty.Name = "NewQty";
            this.NewQty.Width = 120;
            // 
            // Difference
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Difference.DefaultCellStyle = dataGridViewCellStyle9;
            this.Difference.HeaderText = "Difference";
            this.Difference.Name = "Difference";
            this.Difference.ReadOnly = true;
            this.Difference.Width = 120;
            // 
            // LIFORate
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.LIFORate.DefaultCellStyle = dataGridViewCellStyle10;
            this.LIFORate.HeaderText = "Rate";
            this.LIFORate.Name = "LIFORate";
            this.LIFORate.ReadOnly = true;
            this.LIFORate.Width = 120;
            // 
            // ItemID
            // 
            this.ItemID.HeaderText = "ItemID";
            this.ItemID.Name = "ItemID";
            this.ItemID.Visible = false;
            this.ItemID.Width = 5;
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.panelEx2);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(950, 337);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            // 
            // panelEx2
            // 
            this.panelEx2.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx2.Controls.Add(this.dgvLocationDetails);
            this.panelEx2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx2.Location = new System.Drawing.Point(1, 1);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(948, 335);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 150;
            this.panelEx2.Text = "panelEx2";
            // 
            // dgvLocationDetails
            // 
            this.dgvLocationDetails.AddNewRow = false;
            this.dgvLocationDetails.AlphaNumericCols = new int[0];
            this.dgvLocationDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvLocationDetails.CapsLockCols = new int[0];
            this.dgvLocationDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocationDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LItemCode,
            this.LItemName,
            this.LItemID,
            this.LBatchID,
            this.LBatchNo,
            this.LLocation,
            this.LRow,
            this.LBlock,
            this.LLot,
            this.LActualQty,
            this.LCurrentQty,
            this.LNewQty,
            this.LDifference});
            this.dgvLocationDetails.DecimalCols = new int[0];
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLocationDetails.DefaultCellStyle = dataGridViewCellStyle16;
            this.dgvLocationDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLocationDetails.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvLocationDetails.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvLocationDetails.HasSlNo = false;
            this.dgvLocationDetails.LastRowIndex = 0;
            this.dgvLocationDetails.Location = new System.Drawing.Point(0, 0);
            this.dgvLocationDetails.Name = "dgvLocationDetails";
            this.dgvLocationDetails.NegativeValueCols = new int[0];
            this.dgvLocationDetails.NumericCols = new int[0];
            this.dgvLocationDetails.RowHeadersWidth = 30;
            this.dgvLocationDetails.Size = new System.Drawing.Size(948, 335);
            this.dgvLocationDetails.TabIndex = 0;
            this.dgvLocationDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLocationDetails_CellValueChanged);
            this.dgvLocationDetails.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvLocationDetails_UserDeletingRow);
            this.dgvLocationDetails.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvLocationDetails_CellBeginEdit);
            this.dgvLocationDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLocationDetails_CellEndEdit);
            this.dgvLocationDetails.Textbox_TextChanged += new System.EventHandler<System.EventArgs>(this.dgvLocationDetails_Textbox_TextChanged);
            this.dgvLocationDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvLocationDetails_EditingControlShowing);
            this.dgvLocationDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvLocationDetails_CurrentCellDirtyStateChanged);
            this.dgvLocationDetails.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLocationDetails_CellEnter);
            // 
            // LItemCode
            // 
            this.LItemCode.HeaderText = "Item Code";
            this.LItemCode.Name = "LItemCode";
            this.LItemCode.Width = 60;
            // 
            // LItemName
            // 
            this.LItemName.HeaderText = "Item Name";
            this.LItemName.Name = "LItemName";
            this.LItemName.Width = 200;
            // 
            // LItemID
            // 
            this.LItemID.HeaderText = "ItemID";
            this.LItemID.Name = "LItemID";
            this.LItemID.Visible = false;
            // 
            // LBatchID
            // 
            this.LBatchID.HeaderText = "BatchID";
            this.LBatchID.Name = "LBatchID";
            this.LBatchID.Visible = false;
            // 
            // LBatchNo
            // 
            this.LBatchNo.HeaderText = "Batch No";
            this.LBatchNo.Name = "LBatchNo";
            this.LBatchNo.ReadOnly = true;
            this.LBatchNo.Width = 130;
            // 
            // LLocation
            // 
            this.LLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LLocation.HeaderText = "Location";
            this.LLocation.Name = "LLocation";
            this.LLocation.Width = 125;
            // 
            // LRow
            // 
            this.LRow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LRow.HeaderText = "Row";
            this.LRow.Name = "LRow";
            this.LRow.Width = 125;
            // 
            // LBlock
            // 
            this.LBlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LBlock.HeaderText = "Block";
            this.LBlock.Name = "LBlock";
            this.LBlock.Width = 125;
            // 
            // LLot
            // 
            this.LLot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LLot.HeaderText = "Lot";
            this.LLot.Name = "LLot";
            // 
            // LActualQty
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.LActualQty.DefaultCellStyle = dataGridViewCellStyle12;
            this.LActualQty.HeaderText = "Actual Qty";
            this.LActualQty.Name = "LActualQty";
            this.LActualQty.ReadOnly = true;
            this.LActualQty.Visible = false;
            this.LActualQty.Width = 90;
            // 
            // LCurrentQty
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.LCurrentQty.DefaultCellStyle = dataGridViewCellStyle13;
            this.LCurrentQty.HeaderText = "Current Qty";
            this.LCurrentQty.Name = "LCurrentQty";
            this.LCurrentQty.ReadOnly = true;
            this.LCurrentQty.Width = 90;
            // 
            // LNewQty
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.LNewQty.DefaultCellStyle = dataGridViewCellStyle14;
            this.LNewQty.HeaderText = "NewQty";
            this.LNewQty.Name = "LNewQty";
            this.LNewQty.Width = 78;
            // 
            // LDifference
            // 
            this.LDifference.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.LDifference.DefaultCellStyle = dataGridViewCellStyle15;
            this.LDifference.HeaderText = "Difference";
            this.LDifference.Name = "LDifference";
            this.LDifference.ReadOnly = true;
            // 
            // tiItemDetails
            // 
            this.tiItemDetails.AttachedControl = this.tabControlPanel1;
            this.tiItemDetails.Name = "tiItemDetails";
            this.tiItemDetails.Text = "Item Details";
            // 
            // tiLocationDetails
            // 
            this.tiLocationDetails.AttachedControl = this.tabControlPanel2;
            this.tiLocationDetails.Name = "tiLocationDetails";
            this.tiLocationDetails.Text = "Location Details";
            // 
            // panelGridBottom
            // 
            this.panelGridBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelGridBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelGridBottom.Controls.Add(this.pnlBottom);
            this.panelGridBottom.Controls.Add(this.txtRemarks);
            this.panelGridBottom.Controls.Add(this.lblRemarks);
            this.panelGridBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGridBottom.Location = new System.Drawing.Point(0, 359);
            this.panelGridBottom.Name = "panelGridBottom";
            this.panelGridBottom.Size = new System.Drawing.Size(950, 90);
            this.panelGridBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelGridBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelGridBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelGridBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelGridBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelGridBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelGridBottom.Style.GradientAngle = 90;
            this.panelGridBottom.TabIndex = 25;
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.lblCreatedByText);
            this.pnlBottom.Controls.Add(this.lblCreatedDateValue);
            this.pnlBottom.Controls.Add(this.lblStockAdjustmentStatus);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 64);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(950, 26);
            this.pnlBottom.TabIndex = 292;
            // 
            // lblCreatedByText
            // 
            this.lblCreatedByText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedByText.CloseButtonVisible = false;
            this.lblCreatedByText.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblCreatedByText.Location = new System.Drawing.Point(568, 0);
            this.lblCreatedByText.Name = "lblCreatedByText";
            this.lblCreatedByText.OptionsButtonVisible = false;
            this.lblCreatedByText.Size = new System.Drawing.Size(380, 24);
            this.lblCreatedByText.TabIndex = 258;
            this.lblCreatedByText.Text = "Created By";
            // 
            // lblCreatedDateValue
            // 
            this.lblCreatedDateValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedDateValue.CloseButtonVisible = false;
            this.lblCreatedDateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCreatedDateValue.Location = new System.Drawing.Point(393, 0);
            this.lblCreatedDateValue.Name = "lblCreatedDateValue";
            this.lblCreatedDateValue.OptionsButtonVisible = false;
            this.lblCreatedDateValue.Size = new System.Drawing.Size(555, 24);
            this.lblCreatedDateValue.TabIndex = 256;
            this.lblCreatedDateValue.Text = "Created Date";
            // 
            // lblStockAdjustmentStatus
            // 
            this.lblStockAdjustmentStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblStockAdjustmentStatus.CloseButtonVisible = false;
            this.lblStockAdjustmentStatus.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblStockAdjustmentStatus.Image = ((System.Drawing.Image)(resources.GetObject("lblStockAdjustmentStatus.Image")));
            this.lblStockAdjustmentStatus.Location = new System.Drawing.Point(0, 0);
            this.lblStockAdjustmentStatus.Name = "lblStockAdjustmentStatus";
            this.lblStockAdjustmentStatus.OptionsButtonVisible = false;
            this.lblStockAdjustmentStatus.Size = new System.Drawing.Size(393, 24);
            this.lblStockAdjustmentStatus.TabIndex = 20;
            // 
            // txtRemarks
            // 
            // 
            // 
            // 
            this.txtRemarks.Border.Class = "TextBoxBorder";
            this.txtRemarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtRemarks.Location = new System.Drawing.Point(58, 8);
            this.txtRemarks.MaxLength = 1000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(519, 49);
            this.txtRemarks.TabIndex = 12;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(3, 10);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 36;
            this.lblRemarks.Text = "Remarks";
            // 
            // BindingNavigatorStockAdjustment
            // 
            this.BindingNavigatorStockAdjustment.AccessibleDescription = "DotNetBar Bar (BindingNavigatorStockAdjustment)";
            this.BindingNavigatorStockAdjustment.AccessibleName = "DotNetBar Bar";
            this.BindingNavigatorStockAdjustment.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.BindingNavigatorStockAdjustment.Dock = System.Windows.Forms.DockStyle.Top;
            this.BindingNavigatorStockAdjustment.DockLine = 1;
            this.BindingNavigatorStockAdjustment.DockOffset = 73;
            this.BindingNavigatorStockAdjustment.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.BindingNavigatorStockAdjustment.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.BindingNavigatorStockAdjustment.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorClearItem,
            this.BindingNavigatorDeleteItem,
            this.BtnPrint,
            this.BtnEmail,
            this.BtnHelp});
            this.BindingNavigatorStockAdjustment.Location = new System.Drawing.Point(0, 0);
            this.BindingNavigatorStockAdjustment.Name = "BindingNavigatorStockAdjustment";
            this.BindingNavigatorStockAdjustment.Size = new System.Drawing.Size(950, 25);
            this.BindingNavigatorStockAdjustment.Stretch = true;
            this.BindingNavigatorStockAdjustment.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.BindingNavigatorStockAdjustment.TabIndex = 105;
            this.BindingNavigatorStockAdjustment.TabStop = false;
            this.BindingNavigatorStockAdjustment.Text = "bar2";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.Tooltip = "Add New Information";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Tooltip = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorClearItem
            // 
            this.BindingNavigatorClearItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorClearItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorClearItem.Image")));
            this.BindingNavigatorClearItem.Name = "BindingNavigatorClearItem";
            this.BindingNavigatorClearItem.Text = "Clear";
            this.BindingNavigatorClearItem.Tooltip = "Clear";
            this.BindingNavigatorClearItem.Click += new System.EventHandler(this.BindingNavigatorClearItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Tooltip = "Delete";
            this.BindingNavigatorDeleteItem.Visible = false;
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // BtnHelp
            // 
            this.BtnHelp.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(200, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 582);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 122;
            this.expandableSplitterLeft.TabStop = false;
            this.expandableSplitterLeft.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitterLeft_ExpandedChanged);
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.dgvStockAdjustmentDisplay);
            this.PanelLeft.Controls.Add(this.expandablePanel1);
            this.PanelLeft.Controls.Add(this.lblStockAdjustmentCount);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(200, 582);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 121;
            this.PanelLeft.Text = "panelEx1";
            // 
            // dgvStockAdjustmentDisplay
            // 
            this.dgvStockAdjustmentDisplay.AllowUserToAddRows = false;
            this.dgvStockAdjustmentDisplay.AllowUserToDeleteRows = false;
            this.dgvStockAdjustmentDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvStockAdjustmentDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dgvStockAdjustmentDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvStockAdjustmentDisplay.DefaultCellStyle = dataGridViewCellStyle18;
            this.dgvStockAdjustmentDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvStockAdjustmentDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvStockAdjustmentDisplay.Location = new System.Drawing.Point(0, 187);
            this.dgvStockAdjustmentDisplay.Name = "dgvStockAdjustmentDisplay";
            this.dgvStockAdjustmentDisplay.ReadOnly = true;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvStockAdjustmentDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dgvStockAdjustmentDisplay.RowHeadersVisible = false;
            this.dgvStockAdjustmentDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStockAdjustmentDisplay.Size = new System.Drawing.Size(200, 369);
            this.dgvStockAdjustmentDisplay.TabIndex = 20;
            this.dgvStockAdjustmentDisplay.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStockAdjustmentDisplay_CellDoubleClick);
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expandablePanel1.Controls.Add(this.panelLeftTop);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(200, 187);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 101;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Search";
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelLeftTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelLeftTop.Controls.Add(this.lnkLabelAdvanceSearch);
            this.panelLeftTop.Controls.Add(this.dtpSTo);
            this.panelLeftTop.Controls.Add(this.dtpSFrom);
            this.panelLeftTop.Controls.Add(this.lblSAdjustmentNo);
            this.panelLeftTop.Controls.Add(this.lblTo);
            this.panelLeftTop.Controls.Add(this.lblFrom);
            this.panelLeftTop.Controls.Add(this.cboSEmployee);
            this.panelLeftTop.Controls.Add(this.lblEmployee);
            this.panelLeftTop.Controls.Add(this.btnSRefresh);
            this.panelLeftTop.Controls.Add(this.txtSAdjustmentNo);
            this.panelLeftTop.Controls.Add(this.cboSCompany);
            this.panelLeftTop.Controls.Add(this.lblSCompany);
            this.panelLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLeftTop.Location = new System.Drawing.Point(0, 26);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(200, 159);
            this.panelLeftTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelLeftTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelLeftTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelLeftTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelLeftTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelLeftTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelLeftTop.Style.GradientAngle = 90;
            this.panelLeftTop.TabIndex = 122;
            // 
            // lnkLabelAdvanceSearch
            // 
            this.lnkLabelAdvanceSearch.AutoSize = true;
            this.lnkLabelAdvanceSearch.Location = new System.Drawing.Point(1, 140);
            this.lnkLabelAdvanceSearch.Name = "lnkLabelAdvanceSearch";
            this.lnkLabelAdvanceSearch.Size = new System.Drawing.Size(87, 13);
            this.lnkLabelAdvanceSearch.TabIndex = 261;
            this.lnkLabelAdvanceSearch.TabStop = true;
            this.lnkLabelAdvanceSearch.Text = "Advance Search";
            this.lnkLabelAdvanceSearch.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLabelAdvanceSearch_LinkClicked);
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(79, 82);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.Size = new System.Drawing.Size(115, 20);
            this.dtpSTo.TabIndex = 17;
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(79, 59);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.Size = new System.Drawing.Size(115, 20);
            this.dtpSFrom.TabIndex = 16;
            // 
            // lblSAdjustmentNo
            // 
            this.lblSAdjustmentNo.AutoSize = true;
            this.lblSAdjustmentNo.Location = new System.Drawing.Point(3, 107);
            this.lblSAdjustmentNo.Name = "lblSAdjustmentNo";
            this.lblSAdjustmentNo.Size = new System.Drawing.Size(76, 13);
            this.lblSAdjustmentNo.TabIndex = 245;
            this.lblSAdjustmentNo.Text = "Adjustment No";
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(1, 85);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 244;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(1, 62);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 243;
            this.lblFrom.Text = "From";
            // 
            // cboSEmployee
            // 
            this.cboSEmployee.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSEmployee.DisplayMember = "Text";
            this.cboSEmployee.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSEmployee.DropDownHeight = 75;
            this.cboSEmployee.FormattingEnabled = true;
            this.cboSEmployee.IntegralHeight = false;
            this.cboSEmployee.ItemHeight = 14;
            this.cboSEmployee.Location = new System.Drawing.Point(79, 33);
            this.cboSEmployee.Name = "cboSEmployee";
            this.cboSEmployee.Size = new System.Drawing.Size(115, 20);
            this.cboSEmployee.TabIndex = 15;
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            this.lblEmployee.Location = new System.Drawing.Point(1, 36);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(54, 13);
            this.lblEmployee.TabIndex = 124;
            this.lblEmployee.Text = "Executive";
            // 
            // btnSRefresh
            // 
            this.btnSRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSRefresh.Location = new System.Drawing.Point(123, 130);
            this.btnSRefresh.Name = "btnSRefresh";
            this.btnSRefresh.Size = new System.Drawing.Size(71, 23);
            this.btnSRefresh.TabIndex = 19;
            this.btnSRefresh.Text = "Refresh";
            this.btnSRefresh.Click += new System.EventHandler(this.btnSRefresh_Click);
            // 
            // txtSAdjustmentNo
            // 
            this.txtSAdjustmentNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtSAdjustmentNo.Border.Class = "TextBoxBorder";
            this.txtSAdjustmentNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSAdjustmentNo.Location = new System.Drawing.Point(79, 105);
            this.txtSAdjustmentNo.MaxLength = 20;
            this.txtSAdjustmentNo.Name = "txtSAdjustmentNo";
            this.txtSAdjustmentNo.Size = new System.Drawing.Size(115, 20);
            this.txtSAdjustmentNo.TabIndex = 18;
            this.txtSAdjustmentNo.WatermarkEnabled = false;
            // 
            // cboSCompany
            // 
            this.cboSCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSCompany.DisplayMember = "Text";
            this.cboSCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSCompany.DropDownHeight = 75;
            this.cboSCompany.FormattingEnabled = true;
            this.cboSCompany.IntegralHeight = false;
            this.cboSCompany.ItemHeight = 14;
            this.cboSCompany.Location = new System.Drawing.Point(79, 10);
            this.cboSCompany.Name = "cboSCompany";
            this.cboSCompany.Size = new System.Drawing.Size(115, 20);
            this.cboSCompany.TabIndex = 14;
            this.cboSCompany.SelectedIndexChanged += new System.EventHandler(this.cboSCompany_SelectedIndexChanged);
            // 
            // lblSCompany
            // 
            this.lblSCompany.AutoSize = true;
            this.lblSCompany.Location = new System.Drawing.Point(1, 13);
            this.lblSCompany.Name = "lblSCompany";
            this.lblSCompany.Size = new System.Drawing.Size(51, 13);
            this.lblSCompany.TabIndex = 109;
            this.lblSCompany.Text = "Company";
            // 
            // lblStockAdjustmentCount
            // 
            // 
            // 
            // 
            this.lblStockAdjustmentCount.BackgroundStyle.Class = "";
            this.lblStockAdjustmentCount.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblStockAdjustmentCount.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblStockAdjustmentCount.Location = new System.Drawing.Point(0, 556);
            this.lblStockAdjustmentCount.Name = "lblStockAdjustmentCount";
            this.lblStockAdjustmentCount.Size = new System.Drawing.Size(200, 26);
            this.lblStockAdjustmentCount.TabIndex = 105;
            this.lblStockAdjustmentCount.Text = "...";
            // 
            // tiGeneral
            // 
            this.tiGeneral.Name = "tiGeneral";
            this.tiGeneral.Text = "General";
            // 
            // tiSuggestions
            // 
            this.tiSuggestions.Name = "tiSuggestions";
            this.tiSuggestions.Text = "Suggestions";
            this.tiSuggestions.Visible = false;
            // 
            // FrmStockAdjustment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1153, 582);
            this.Controls.Add(this.panelEx1);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.BtnCancel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmStockAdjustment";
            this.Text = "Stock Adjustment";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmStockAdjustment_Load);
            this.Shown += new System.EventHandler(this.FrmStockAdjustment_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmStockAdjustment_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorStatus)).EndInit();
            this.panelEx1.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelBottom.ResumeLayout(false);
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).EndInit();
            this.tcGeneral.ResumeLayout(false);
            this.tabControlPanel3.ResumeLayout(false);
            this.tabControlPanel3.PerformLayout();
            this.tabControlPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSuggestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcStockAdjustment)).EndInit();
            this.tcStockAdjustment.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStockAdjustment)).EndInit();
            this.tabControlPanel2.ResumeLayout(false);
            this.panelEx2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocationDetails)).EndInit();
            this.panelGridBottom.ResumeLayout(false);
            this.panelGridBottom.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigatorStockAdjustment)).EndInit();
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStockAdjustmentDisplay)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button BtnOk;
        internal System.Windows.Forms.Timer TmrStatus;
        internal System.Windows.Forms.ErrorProvider ErrorStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.TabItem tiSuggestions;
        private DevComponents.DotNetBar.PanelEx PanelLeft;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvStockAdjustmentDisplay;
        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.PanelEx panelLeftTop;
        private System.Windows.Forms.DateTimePicker dtpSTo;
        private System.Windows.Forms.DateTimePicker dtpSFrom;
        private System.Windows.Forms.Label lblSAdjustmentNo;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSEmployee;
        private System.Windows.Forms.Label lblEmployee;
        private DevComponents.DotNetBar.ButtonX btnSRefresh;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSAdjustmentNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSCompany;
        private System.Windows.Forms.Label lblSCompany;
        private DevComponents.DotNetBar.LabelX lblStockAdjustmentCount;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterLeft;
        private DevComponents.DotNetBar.PanelEx panelRight;
        private DevComponents.DotNetBar.TabItem tiGeneral;
        private DevComponents.DotNetBar.Bar BindingNavigatorStockAdjustment;
        private DevComponents.DotNetBar.ButtonItem BindingNavigatorAddNewItem;
        private DevComponents.DotNetBar.ButtonItem BindingNavigatorSaveItem;
        private DevComponents.DotNetBar.ButtonItem BindingNavigatorClearItem;
        private DevComponents.DotNetBar.ButtonItem BindingNavigatorDeleteItem;
        private DevComponents.DotNetBar.ButtonItem BtnPrint;
        private DevComponents.DotNetBar.ButtonItem BtnEmail;
        private DevComponents.DotNetBar.PanelEx panelBottom;
        private DevComponents.DotNetBar.TabControl tcStockAdjustment;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private ClsInnerGridBar dgvStockAdjustment;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private DevComponents.DotNetBar.PanelEx panelEx2;
        private ClsInnerGridBar dgvLocationDetails;
        private DevComponents.DotNetBar.TabItem tiItemDetails;
        private DevComponents.DotNetBar.TabItem tiLocationDetails;
        private DevComponents.DotNetBar.PanelEx panelGridBottom;
        private System.Windows.Forms.Panel pnlBottom;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedByText;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedDateValue;
        private DevComponents.DotNetBar.Controls.WarningBox lblStockAdjustmentStatus;
        private DevComponents.DotNetBar.Controls.TextBoxX txtRemarks;
        private System.Windows.Forms.Label lblRemarks;
        private DevComponents.DotNetBar.PanelEx panelTop;
        internal System.Windows.Forms.Label lblAdjustmentDate;
        internal System.Windows.Forms.Label lblAdjustmentNo;
        private System.Windows.Forms.DateTimePicker dtpAdjustmentDate;
        private System.Windows.Forms.Label lblCompany;
        private DevComponents.DotNetBar.Controls.TextBoxX txtAdjustmentNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboReason;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboWarehouse;
        private System.Windows.Forms.Label lblWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.ButtonX btnReason;
        private System.Windows.Forms.Label lblReason;
        private DevComponents.DotNetBar.ButtonItem BtnHelp;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter1;
        private DevComponents.DotNetBar.TabControl tcGeneral;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel3;
        private DevComponents.DotNetBar.TabItem tabItem1;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel4;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvSuggestions;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn VerifiedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comment;
        private DevComponents.DotNetBar.TabItem tabItem2;
        private System.Windows.Forms.DataGridViewTextBoxColumn LItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn LItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LItemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn LBatchID;
        private System.Windows.Forms.DataGridViewTextBoxColumn LBatchNo;
        private System.Windows.Forms.DataGridViewComboBoxColumn LLocation;
        private System.Windows.Forms.DataGridViewComboBoxColumn LRow;
        private System.Windows.Forms.DataGridViewComboBoxColumn LBlock;
        private System.Windows.Forms.DataGridViewComboBoxColumn LLot;
        private System.Windows.Forms.DataGridViewTextBoxColumn LActualQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn LCurrentQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn LNewQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn LDifference;
        private System.Windows.Forms.LinkLabel lnkLabelAdvanceSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewComboBoxColumn cboBatchNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActualQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn NewQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn Difference;
        private System.Windows.Forms.DataGridViewTextBoxColumn LIFORate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemID;
    }
}