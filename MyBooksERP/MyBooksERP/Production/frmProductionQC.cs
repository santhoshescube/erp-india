﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class frmProductionQC : Form
    {
        clsBLLCommonUtility mobjClsBllCommonUtility;
        clsBLLProductionQC objclsBLLProductionQC;
        MessageBoxIcon MmsgMessageIcon;
        string MstrCommonMessage;
        bool MblnAddStatus, MblnAddPermission, MblnUpdatePermission, MblnDeletePermission, MblnPrintEmailPermission;
        ClsNotificationNew MobjClsNotification;
        DataTable datMessages;

        public frmProductionQC()
        {
            InitializeComponent();
            mobjClsBllCommonUtility = new clsBLLCommonUtility();
            objclsBLLProductionQC = new clsBLLProductionQC();
            MobjClsNotification = new ClsNotificationNew();
        }

        private void frmProductionProcess_Load(object sender, EventArgs e)
        {
            LoadMessage();
            SetPermissions();
            LoadCombos(0);
            ClearControls();
            FillDisplayGrid();
        }
        private void ClearControls()
        {
           
            txtCode.Tag = 0;
            txtCode.Text = objclsBLLProductionQC.GetQCNo();
           txtRemarks.Text = "";
            cboProductionNo.SelectedIndex = -1;
            dtpOrderDate.Value = ClsCommonSettings.GetServerDate();
            txtProducedQty.Text=txtSampleQuantity.Text=txtPassedQty.Text = "";
          cboQCNo.SelectedIndex= cboSPrdNO.SelectedIndex= cboIncharge.SelectedIndex = -1;
            dgvQCDetails.Rows.Clear();
            cboProductionNo.Enabled = true;
            //dgvMaterialIssueDisplay.Rows.Clear();
           // err.Clear();
            //tmrTemplate.Enabled = false;
            //getSearchTemplate();
            EnableDisableControls();
        }
        private void EnableDisableControls()
        {
            bnAddNewItem.Enabled = MblnAddPermission;

            if (MblnAddStatus)
            {
                bnSaveItem.Enabled = MblnAddPermission;
                bnDelete.Enabled = bnPrint.Enabled = bnEmail.Enabled = false;
            }
            else
            {
                bnSaveItem.Enabled = MblnUpdatePermission;
                bnDelete.Enabled = MblnDeletePermission;
                bnPrint.Enabled = bnEmail.Enabled = MblnPrintEmailPermission;
            }
        }
        private void LoadMessage()
        {
            datMessages = new DataTable();
            datMessages = MobjClsNotification.FillMessageArray((int)FormID.ProductionTemplate, 4);
        }
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory,
                    (Int32)eMenuID.QC, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission,
                    out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }
        private void LoadCombos(int intType)
        {
            DataTable datTemp = new DataTable();

            if (intType == 0 || intType == 1)
            {
                if (txtCode.Tag.ToInt64() > 0)
                {
                    datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "ProductionID,ProductionNo,ProducedQuantity,IM.ItemName,IM.BaseUomID", "InvProductionMaster PM Inner join InvItemMaster IM ON PM.ProductID=IM.ItemID ", "" });
                    cboProductionNo.Enabled = false;
                }
                else
                {
                    datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "ProductionID,ProductionNo,ProducedQuantity,IM.ItemName,IM.BaseUomID", "InvProductionMaster PM Inner join InvItemMaster IM ON PM.ProductID=IM.ItemID ", "PM.ProductionID NOT IN( SELECT ProductionID FROM invProductionQCMaster)" });
                    cboProductionNo.Enabled = true;
                }



                cboProductionNo.DataSource = datTemp;
                cboProductionNo.ValueMember = "ProductionID";
                cboProductionNo.DisplayMember = "ProductionNo";
            }
            if (intType == 0 || intType == 2)
            {
                datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "EmployeeID,EmployeeFullName", "EmployeeMaster", "" });

                cboIncharge.DataSource = datTemp;
                cboIncharge.ValueMember = "EmployeeID";
                cboIncharge.DisplayMember = "EmployeeFullName";
            }
            if (intType == 0 || intType == 3)
            {
                datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "BatchNo,BatchNo", "InvProductionDetails", "ProductionID = " + cboProductionNo.SelectedValue.ToInt64()});

                BatchNo.DataSource = datTemp;
                BatchNo.ValueMember = "BatchNo";
                BatchNo.DisplayMember = "BatchNo";
            }
            if (intType == 0 || intType == 4)
            {
                datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "ProductionID,ProductionNo", "InvProductionMaster", "" });

                cboSPrdNO.DataSource = datTemp;
                cboSPrdNO.ValueMember = "ProductionID";
                cboSPrdNO.DisplayMember = "ProductionNo";
            }
            if (intType == 0 || intType == 4)
            {
                datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "QCID,QCNo", "invProductionQCMaster" ,""});

                cboQCNo.DataSource = datTemp;
                cboQCNo.ValueMember = "QCID";
                cboQCNo.DisplayMember = "QCNo";
            }
            
        }

        private void cboProductionNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboProductionNo.SelectedIndex >= 0)
            {
                LoadCombos(3);
                txtProducedQty.Text = ((System.Data.DataRowView)(cboProductionNo.SelectedItem)).Row.ItemArray[2].ToStringCustom();
                lblProduct.Text = ((System.Data.DataRowView)(cboProductionNo.SelectedItem)).Row.ItemArray[3].ToStringCustom();
                FillGrid();
            }
        }
        private void FillGrid()
        {
            if (cboProductionNo.SelectedIndex >=0)
            {
                dgvQCDetails.Rows.Clear();
                
               // MessageBox.Show(Math.Ceiling(txtSampleQuantity.Text.ToDecimal()).ToString());
                if (Math.Ceiling(txtSampleQuantity.Text.ToDecimal())> 0)
                {
                    int count = Math.Ceiling(txtSampleQuantity.Text.ToDecimal()).ToInt32();
                    dgvQCDetails.Rows.Add(count);
                }
            }
        }

        private void txtSampleQuantity_TextChanged(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void txtProducedQty_TextChanged(object sender, EventArgs e)
        {

        }
        private void SetSerialNo()
        {
            try
            {
                int intRowNumber = 1;
                foreach (DataGridViewRow row in dgvQCDetails.Rows)
                {
                    if (row.Index <= dgvQCDetails.Rows.Count - 1)
                    {
                        row.Cells["SerialNo"].Value = intRowNumber.ToString();
                        intRowNumber++;
                    }
                }
            }
            catch { }
        }


        private void bnSaveItem_Click(object sender, EventArgs e)
        {
            dgvQCDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
            Save();
            FillDisplayGrid();
        }
        private bool FormValidation()
        {
         errTemplate.Clear();
         if (txtSampleQuantity.Text == "")
         {
             MstrCommonMessage = "Please enter sample quantity";
             MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
             lblStatus.Text = MstrCommonMessage.Split('#').Last();
             tmrTemplate.Enabled = true;
             txtSampleQuantity.Focus();
             return false;
         }
          if (txtPassedQty.Text == "")
         {
            MstrCommonMessage="Please enter passed quantity";
             MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
             lblStatus.Text = MstrCommonMessage.Split('#').Last();
             tmrTemplate.Enabled = true;
             txtPassedQty.Focus();
             return false;
         }
          if (cboProductionNo.SelectedIndex == -1)
          {
              MstrCommonMessage = "Please select production number";
              MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
              lblStatus.Text = MstrCommonMessage.Split('#').Last();
              tmrTemplate.Enabled = true;
              txtPassedQty.Focus();
              return false;
          }
          if (txtProducedQty.Text.ToDecimal()<txtPassedQty.Text.ToDecimal())
          {
              MstrCommonMessage = "Passed quantity exceeds produced quantity";
              MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
              lblStatus.Text = MstrCommonMessage.Split('#').Last();
              tmrTemplate.Enabled = true;
              txtPassedQty.Focus();
              return false;
          }
            return true;
        }
        private DataTable FillDetailParameters()
        {
            DataTable datTemp = new DataTable();
            datTemp.Columns.Add("BatchNo");          
            datTemp.Columns.Add("QCStatus");
            datTemp.Columns.Add("Remarks");
            datTemp.Columns.Add("SerialNo");
            

            for (int iCounter = 0; iCounter <= dgvQCDetails.Rows.Count - 1; iCounter++)
            {
                datTemp.Rows.Add();
                datTemp.Rows[iCounter]["BatchNo"] = dgvQCDetails.Rows[iCounter].Cells["BatchNo"].Value.ToStringCustom();
                datTemp.Rows[iCounter]["SerialNo"] = dgvQCDetails.Rows[iCounter].Cells["SerialNo"].Value.ToInt32();
                datTemp.Rows[iCounter]["QCStatus"] =Convert.ToBoolean(dgvQCDetails.Rows[iCounter].Cells["Status"].Value);
                datTemp.Rows[iCounter]["Remarks"] = dgvQCDetails.Rows[iCounter].Cells["Remarks"].Value.ToStringCustom();
            }

            return datTemp;
        }



        private void FillDisplayGrid()
        {
            dgvMaterialIssueDisplay.DataSource = objclsBLLProductionQC.GetAllQC(dtpSFrom.Value,dtpSTo.Value,cboSPrdNO.SelectedValue.ToInt64(),cboQCNo.SelectedValue.ToInt64());
            dgvMaterialIssueDisplay.Columns[2].Visible = false;
        }
        private bool Save()
        {
            bool blnSave = false;

            if (FormValidation())
            {
                long lngID = txtCode.Tag.ToInt64();

                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (lngID == 0 ? 1 : 3), out MmsgMessageIcon);

                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information) == DialogResult.No)
                    return false;

               
               txtCode.Tag=objclsBLLProductionQC.SaveQC(FillDetailParameters(),lngID,txtCode.Text,dtpOrderDate.Value.ToString("dd/MMM/yyyy"),cboProductionNo.SelectedValue.ToInt64(),txtRemarks.Text,cboIncharge.SelectedValue.ToInt32(),txtSampleQuantity.Text.ToDecimal(),txtPassedQty.Text.ToDecimal());

                if (blnSave)
                {
                    if (lngID == 0)
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 2, out MmsgMessageIcon);
                    else
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 21, out MmsgMessageIcon);

                    lblStatus.Text = MstrCommonMessage.Split('#').Last();
                    tmrTemplate.Enabled = true;
                    MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                }
            }
            else
                blnSave = false;

            return blnSave;
        }

        private void bnDelete_Click(object sender, EventArgs e)
        {
            DeleteQC();
            ClearControls();
            FillDisplayGrid();
        }
        private bool DeleteQC()
        {
            bool blnDelete = false;

            if (txtCode.Tag.ToInt64() > 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 13, out MmsgMessageIcon);

                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information) == DialogResult.No)
                    return false;

                blnDelete = objclsBLLProductionQC.DeleteQC(txtCode.Tag.ToInt64());

                if (blnDelete)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4, out MmsgMessageIcon);
                    lblStatus.Text = MstrCommonMessage.Split('#').Last();
                    tmrTemplate.Enabled = true;
                    MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                }
            }

            return blnDelete;
        }
        private void DisplayQC(int intRowIndex)
        {
            

            DataTable dsTemp = objclsBLLProductionQC.GetQCDetails(dgvMaterialIssueDisplay.Rows[intRowIndex].Cells["QCID"].Value.ToInt64());



            if (dsTemp != null)
                    {
                        if (dsTemp.Rows.Count > 0)
                        {
                            txtCode.Tag = dsTemp.Rows[0]["QCID"].ToInt64();
                            LoadCombos(1);
                           
                            txtCode.Text = dsTemp.Rows[0]["QCNo"].ToStringCustom();
                            cboProductionNo.SelectedValue = dsTemp.Rows[0]["ProductionID"].ToStringCustom();
                            cboIncharge.SelectedValue = dsTemp.Rows[0]["InchargeID"].ToInt32();
                            txtRemarks.Text = dsTemp.Rows[0]["MainRemarks"].ToStringCustom();
                            dtpOrderDate.Value = dsTemp.Rows[0]["QCDate"].ToDateTime();
                            txtSampleQuantity.Text = dsTemp.Rows[0]["SampleQty"].ToStringCustom();
                            txtPassedQty.Text = dsTemp.Rows[0]["PassedQty"].ToStringCustom();
                            dgvQCDetails.Rows.Clear();
                            for (int iCounter = 0; iCounter <= dsTemp.Rows.Count - 1; iCounter++)
                            {
                                dgvQCDetails.Rows.Add();
                                dgvQCDetails.Rows[iCounter].Cells["BatchNo"].Value = dsTemp.Rows[iCounter]["BatchNo"].ToStringCustom();
                                dgvQCDetails.Rows[iCounter].Cells["Status"].Value =Convert.ToBoolean( dsTemp.Rows[iCounter]["QCStatus"]);
                                dgvQCDetails.Rows[iCounter].Cells["SerialNo"].Value = dsTemp.Rows[iCounter]["SerialNo"].ToDecimal();
                                dgvQCDetails.Rows[iCounter].Cells["Remarks"].Value = dsTemp.Rows[iCounter]["Remarks"].ToString();
                            }
                            
                           
                        
                    

                   

                   
                }

                EnableDisableControls();
            }
        }


        private void dgvQCDetails_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            SetSerialNo();
        }

        private void dgvMaterialIssueDisplay_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
                DisplayQC(e.RowIndex);
        }

        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
          
            ClearControls();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
           
            FillDisplayGrid();
        }
       

        private void dgvQCDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void cboQCNo_DropDown(object sender, EventArgs e)
        {
            cboQCNo.DroppedDown = false;
        }
    }
}
