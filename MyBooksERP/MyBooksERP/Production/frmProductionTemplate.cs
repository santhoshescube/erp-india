﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace MyBooksERP
{
    public partial class frmProductionTemplate : Form
    {
        ClsCommonUtility objCommonUtility;
        clsBLLCommonUtility objclsBLLCommonUtility;
        ClsNotificationNew MobjClsNotification;
        clsBLLProductionTemplate MobjclsBLLProductionTemplate;
                
        DataTable datMessages, datItemDetails;
        MessageBoxIcon MmsgMessageIcon;
        string MstrCommonMessage;
        bool MblnAddPermission, MblnUpdatePermission, MblnDeletePermission, MblnPrintEmailPermission;

        public frmProductionTemplate()
        {
            InitializeComponent();
            objCommonUtility = new ClsCommonUtility();
            objclsBLLCommonUtility = new clsBLLCommonUtility();
            MobjClsNotification = new ClsNotificationNew();
            MobjclsBLLProductionTemplate = new clsBLLProductionTemplate();
        }

        private void frmProductionTemplate_Load(object sender, EventArgs e)
        {
            LoadMessage();
            SetPermissions();
            LoadCombos(0, 0);
            ClearControls();            
        }

        private void LoadCombos(int intType, int intItemID)
        {
            DataTable datTemp = new DataTable();

            if (intType == 0 || intType == 1)
            {
                datTemp = objCommonUtility.FillCombos(new string[] { "ItemID,ItemName + ' - ' + Code AS ItemName", "InvItemMaster", "" +
                    "StatusID = " + (int)OperationStatusType.ProductActive });

                cboProduct.DataSource = datTemp;
                cboProduct.ValueMember = "ItemID";
                cboProduct.DisplayMember = "ItemName";
            }

            if (intType == 2)
            {
                UOMID.DataSource = objCommonUtility.FillCombos(new string[] { "IU.UOMID,UR.Code as ShortName" , "" +
                    "InvItemUOMs IU INNER JOIN InvUOMReference UR ON IU.UOMID = UR.UOMID", "IU.ItemID = " + intItemID});
                UOMID.ValueMember = "UOMID";
                UOMID.DisplayMember = "ShortName";
            }
        }

        private void ClearControls()
        {            
            txtCode.Tag = 0;
            txtCode.Text = MobjclsBLLProductionTemplate.getTemplateCode();
            txtName.Text = txtRemarks.Text = "";
            cboProduct.SelectedIndex = -1;
            lblTotal.Text = "0.000";
            dgvItem.Rows.Clear();
            dgvParticulars.Rows.Clear();
            errTemplate.Clear();
            tmrTemplate.Enabled = false;
            getSearchTemplate();
            EnableDisableControls();
        }

        private void getSearchTemplate()
        {
            dgvSearch.DataSource = MobjclsBLLProductionTemplate.getSearchTemplate();
        }

        private void EnableDisableControls()
        {
            btnAdd.Enabled = MblnAddPermission;

            if (txtCode.Tag.ToInt64() == 0)
            {
                btnSave.Enabled = MblnAddPermission;
                btnDelete.Enabled = btnPrint.Enabled = btnEmail.Enabled = false;
            }
            else
            {
                btnSave.Enabled = MblnUpdatePermission;
                btnDelete.Enabled = MblnDeletePermission;
                btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
            }
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory, 
                    (Int32)eMenuID.ProductionTemplate, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, 
                    out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void LoadMessage()
        {
            datMessages = new DataTable();
            datMessages = MobjClsNotification.FillMessageArray((int)FormID.ProductionTemplate, 4);
        }

        private void dgvItem_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == UOMID.Index || e.ColumnIndex == Quantity.Index || e.ColumnIndex == Rate.Index || e.ColumnIndex == Total.Index)
            {
                if (Convert.ToString(dgvItem.CurrentRow.Cells["ItemCode"].Value).Trim() == "")
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1601, out MmsgMessageIcon);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    dgvItem.Focus();
                    e.Cancel = true;
                }
            }
        }

        private void CalculateTotal()
        {
            lblTotal.Text = "0.000";
            decimal decTotal = 0;

            for (int iCounter = 0; iCounter <= dgvItem.Rows.Count - 2; iCounter++)
                decTotal += dgvItem.Rows[iCounter].Cells["Total"].Value.ToDecimal();

            for (int jCounter = 0; jCounter <= dgvParticulars.Rows.Count - 2; jCounter++)
                decTotal += dgvParticulars.Rows[jCounter].Cells["Amount"].Value.ToDecimal();

            lblTotal.Text = decTotal.ToStringCustom();
        }

        private void dgvItem_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (e.ColumnIndex == Quantity.Index || e.ColumnIndex == Rate.Index)
                    {
                        if (e.ColumnIndex == Quantity.Index) 
                            CalCulateRate(e.RowIndex);

                        decimal decItemTotal = 0, decGroupTotal = 0;

                        foreach (DataGridViewRow dgvrow in dgvItem.Rows)
                        {
                            if (Convert.ToInt32(dgvrow.Cells[ItemID.Index].Value) > 0 &&
                            Convert.ToInt32(dgvrow.Cells[Quantity.Index].Value) > 0)
                            {
                                decItemTotal = Convert.ToDecimal(dgvrow.Cells[Quantity.Index].Value) *
                                                Convert.ToDecimal(dgvrow.Cells[Rate.Index].Value);
                                dgvrow.Cells[Total.Index].Value = decItemTotal.ToString("F" + ClsCommonSettings.Scale);
                                decGroupTotal = decGroupTotal + decItemTotal;
                            }
                        }
                    }
                    else if (e.ColumnIndex == ItemID.Index)
                        FillItemUOMs(e.RowIndex, Convert.ToInt32(dgvItem.Rows[e.RowIndex].Cells[ItemID.Index].Value));
                    else if (e.ColumnIndex == UOMID.Index)
                        CalCulateRate(e.RowIndex);

                    CalculateTotal();
                }
            }
            catch { }
        }

        private void FillItemUOMs(int intRowIndex, int intItemID)
        {
            clsBLLItemMaster objItemMaster = new clsBLLItemMaster();
            DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)dgvItem.Rows[intRowIndex].Cells[UOMID.Index];

            cell.DisplayMember = "ShortName";
            cell.ValueMember = "UOMID";
            datItemDetails = objItemMaster.GetItemUOM(intItemID); ;
            cell.DataSource = datItemDetails;
            cell.Value = datItemDetails.Rows[0]["UOMID"].ToInt32();

            objItemMaster = null;
        }

        private void CalCulateRate(int intRowIndex)
        {
            clsBLLItemMaster objItemMaster = new clsBLLItemMaster();
            datItemDetails = objItemMaster.GetItemUOM(Convert.ToInt32(dgvItem[ItemID.Index, intRowIndex].Value));

            if (datItemDetails != null)
            {
                if (datItemDetails.Rows.Count > 0)
                {
                    int intConvertionFactor;
                    decimal decQuantity, decRate, decConversionValue, decActualQuantity, decActualRate = 0;

                    if (dgvItem[UOMID.Index, intRowIndex].Value != DBNull.Value && 
                        Convert.ToString(dgvItem[UOMID.Index, intRowIndex].Value) != "" && 
                        dgvItem[UOMID.Index, intRowIndex].Value != null)
                    {
                        DataRow[] dRow = datItemDetails.Select("UomID = " + Convert.ToString(dgvItem[UOMID.Index, intRowIndex].Value));

                        if (dRow.Length > 0)
                        {
                            decActualRate = dgvItem[BaseRate.Index, intRowIndex].Value.ToDecimal();

                            if (!Convert.ToBoolean(dRow[0][4]))
                            {
                                intConvertionFactor = Convert.ToInt32(dRow[0][2]);
                                decConversionValue = Convert.ToDecimal(dRow[0][3]);
                                decQuantity = Convert.ToDecimal(dgvItem[Quantity.Index, intRowIndex].Value);

                                if (intConvertionFactor == 1)
                                    decActualRate = (decQuantity * (1 / decConversionValue)) * decActualRate;
                                else
                                    decActualRate = (decQuantity / (1 / decConversionValue)) * decActualRate;
                            }

                            dgvItem[Rate.Index, intRowIndex].Value = decActualRate.ToString("F" + ClsCommonSettings.Scale);
                        }
                    }
                }
            }
        }

        private void dgvItem_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvItem.IsCurrentCellDirty)
            {
                if (dgvItem.CurrentCell != null)
                    dgvItem.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dgvItem_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvItem.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString() == "'")
                e.Handled = true;
        }

        private void dgvItem_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == ItemCode.Index || e.ColumnIndex == ItemName.Index)
                dgvItem.PiColumnIndex = e.ColumnIndex;
        }

        private void SetSerialNo()
        {
            try
            {
                int intRowNumber = 1;
                foreach (DataGridViewRow row in dgvItem.Rows)
                {
                    if (row.Index < dgvItem.Rows.Count - 1)
                    {
                        row.HeaderCell.Value = intRowNumber.ToString();
                        intRowNumber++;
                    }
                }
            }
            catch { }
        }

        private void dgvItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (dgvItem.CurrentCell.ColumnIndex == Quantity.Index)
                {
                    if (!Char.IsNumber(e.KeyChar))
                        e.Handled = true;
                }
            }
            catch { }
        }

        private void dgvItem_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            SetSerialNo();
        }

        private void dgvItem_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            SetSerialNo();
        }

        private void dgvItem_Textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvItem.CurrentCell.ColumnIndex == ItemCode.Index || dgvItem.CurrentCell.ColumnIndex == ItemName.Index)
                {
                    dgvItem.PServerName = ClsCommonSettings.ServerName;

                    string[] First = { "ItemCode", "ItemName", "ItemID", "Rate", "BaseRate" };//first grid 
                    string[] second = { "ItemCode", "ItemName", "ItemID", "SaleRate", "SaleRate" };//inner grid

                    dgvItem.aryFirstGridParam = First;
                    dgvItem.arySecondGridParam = second;
                    dgvItem.PiFocusIndex = 3;
                    dgvItem.iGridWidth = 350;
                    dgvItem.bBothScrollBar = true;
                    dgvItem.ColumnsToHide = new string[] { "ItemID" };

                    if (dgvItem.CurrentCell.ColumnIndex == ItemCode.Index)
                        dgvItem.field = "Code";
                    else if (dgvItem.CurrentCell.ColumnIndex == ItemName.Index)
                        dgvItem.field = "ItemName";

                    if (dgvItem.TextBoxText != "")
                    {
                        dgvItem.PsQuery = "SELECT IM.Code AS ItemCode,IM.ItemName,IM.ItemID,ISNULL(dbo.GetSaleRate(IM.ItemID),0) AS SaleRate " +
                            "FROM InvItemMaster IM JOIN InvItemDetails ID ON IM.ItemID = ID.ItemID " +
                            "WHERE /*ISNULL(dbo.GetSaleRate(IM.ItemID),0) > 0 AND */" +
                            "IM.StatusID = " + (int)OperationStatusType.ProductActive + " AND " +
                            "UPPER(" + dgvItem.field + ") LIKE UPPER('%" + (dgvItem.TextBoxText) + "%') AND " +
                            "IM.ItemID <> " + cboProduct.SelectedValue.ToInt32() + " " +
                            "ORDER BY CHARINDEX('" + (dgvItem.TextBoxText) + "',LOWER(" + dgvItem.field + "))";
                    }
                    else
                    {
                        dgvItem.PsQuery = "SELECT IM.Code AS ItemCode,IM.ItemName,IM.ItemID,ISNULL(dbo.GetSaleRate(IM.ItemID),0) AS SaleRate " +
                            "FROM InvItemMaster IM JOIN InvItemDetails ID ON IM.ItemID = ID.ItemID " +
                            "WHERE /*ISNULL(dbo.GetSaleRate(IM.ItemID),0) > 0 AND */" +
                            "IM.StatusID = " + (int)OperationStatusType.ProductActive + " AND " +
                            "IM.ItemID <> " + cboProduct.SelectedValue.ToInt32() + " " +
                            "ORDER BY " + dgvItem.field + "";
                    }
                }
            }
            catch { }
        }

        private void dgvItem_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (e.Row.ReadOnly)
                e.Cancel = true;
        }

        private void dgvParticulars_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvParticulars.IsCurrentCellDirty)
            {
                if (dgvParticulars.CurrentCell != null)
                    dgvParticulars.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dgvParticulars_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            CalculateTotal();
        }

        private void dgvParticulars_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvParticulars.EditingControl.KeyPress += new KeyPressEventHandler(EditingControlParticulars_KeyPress);
        }

        void EditingControlParticulars_KeyPress(object sender, KeyPressEventArgs e)
        {            
            if (dgvParticulars.CurrentCell.ColumnIndex == Amount.Index)
            {
                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                    e.Handled = true;
            }
        }

        private void dgvParticulars_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            SetSerialNoOther();
        }

        private void dgvParticulars_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            SetSerialNoOther();
        }

        private void SetSerialNoOther()
        {
            try
            {
                int intRowNumber = 1;
                foreach (DataGridViewRow row in dgvParticulars.Rows)
                {
                    if (row.Index < dgvParticulars.Rows.Count - 1)
                    {
                        row.HeaderCell.Value = intRowNumber.ToString();
                        intRowNumber++;
                    }
                }
            }
            catch { }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void dgvSearch_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
                DisplayProductTemplate(e.RowIndex);
        }

        private void DisplayProductTemplate(int intRowIndex)
        {
            dgvItem.Rows.Clear();
            dgvParticulars.Rows.Clear();
            MobjclsBLLProductionTemplate.PobjClsDTOProductionTemplate.lngTemplateID = dgvSearch.Rows[intRowIndex].Cells["TemplateID"].Value.ToInt64();
            DataSet dsTemp = MobjclsBLLProductionTemplate.getTemplateDetails();

            if (dsTemp != null)
            {
                if (dsTemp.Tables.Count > 0)
                {
                    DataTable datTemp = dsTemp.Tables[0];
                    DataTable datTempDetails = dsTemp.Tables[1];
                    DataTable datTempOthers = dsTemp.Tables[2];

                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            txtCode.Tag = datTemp.Rows[0]["TemplateID"].ToInt64();
                            txtCode.Text = datTemp.Rows[0]["Code"].ToStringCustom();
                            txtName.Text = datTemp.Rows[0]["Template"].ToStringCustom();
                            cboProduct.SelectedValue = datTemp.Rows[0]["ProductID"].ToInt32();
                            txtRemarks.Text = datTemp.Rows[0]["Remarks"].ToStringCustom();
                            lblTotal.Text = datTemp.Rows[0]["Total"].ToStringCustom();
                        }
                    }

                    if (datTempDetails != null)
                    {
                        if (datTempDetails.Rows.Count > 0)
                        {
                            for (int iCounter = 0; iCounter <= datTempDetails.Rows.Count - 1; iCounter++)
                            {
                                dgvItem.Rows.Add();
                                dgvItem.Rows[iCounter].Cells["ItemID"].Value = datTempDetails.Rows[iCounter]["ItemID"].ToInt32();
                                dgvItem.Rows[iCounter].Cells["ItemCode"].Value = datTempDetails.Rows[iCounter]["Code"].ToStringCustom();
                                dgvItem.Rows[iCounter].Cells["ItemName"].Value = datTempDetails.Rows[iCounter]["ItemName"].ToStringCustom();
                                dgvItem.Rows[iCounter].Cells["UOMID"].Value = datTempDetails.Rows[iCounter]["UOMID"].ToInt32();
                                dgvItem.Rows[iCounter].Cells["Quantity"].Value = datTempDetails.Rows[iCounter]["Quantity"].ToDecimal();
                                dgvItem.Rows[iCounter].Cells["Rate"].Value = datTempDetails.Rows[iCounter]["Rate"].ToDecimal();
                                dgvItem.Rows[iCounter].Cells["Total"].Value = datTempDetails.Rows[iCounter]["ItemTotal"].ToDecimal();
                                dgvItem.Rows[iCounter].Cells["SerialNo"].Value = datTempDetails.Rows[iCounter]["SerialNo"].ToDecimal();
                                dgvItem.Rows[iCounter].Cells["BaseRate"].Value = datTempDetails.Rows[iCounter]["Rate"].ToDecimal();
                            }
                        }
                    }

                    if (datTempOthers != null)
                    {
                        if (datTempOthers.Rows.Count > 0)
                        {
                            for (int jCounter = 0; jCounter <= datTempOthers.Rows.Count - 1; jCounter++)
                            {
                                dgvParticulars.Rows.Add();
                                dgvParticulars.Rows[jCounter].Cells["Particulars"].Value = datTempOthers.Rows[jCounter]["Particulars"].ToStringCustom();
                                dgvParticulars.Rows[jCounter].Cells["Amount"].Value = datTempOthers.Rows[jCounter]["Amount"].ToDecimal();
                            }
                        }
                    }
                }

                EnableDisableControls();
            }              
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveProductionTemplate())
                ClearControls();
        }

        private bool SaveProductionTemplate()
        {
            bool blnSave = false;

            MobjclsBLLProductionTemplate.PobjClsDTOProductionTemplate.lngTemplateID = txtCode.Tag.ToInt64();

            // Checking Reference is Exists or not
            if (MobjclsBLLProductionTemplate.PobjClsDTOProductionTemplate.lngTemplateID > 0)
            {
                DataTable datTemp = objclsBLLCommonUtility.FillCombos(new string[] { "MaterialIssueID", "InvMaterialIssueMaster", "" +
                "TemplateID = " + MobjclsBLLProductionTemplate.PobjClsDTOProductionTemplate.lngTemplateID });

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 25, out MmsgMessageIcon);
                        lblStatus.Text = MstrCommonMessage.Split('#').Last();
                        tmrTemplate.Enabled = true;
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        return false;
                    }
                }
            }
            // End

            if (FormValidation())
            {
                long lngID = txtCode.Tag.ToInt64();

                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (lngID == 0 ? 1 : 3), out MmsgMessageIcon);

                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information) == DialogResult.No)
                    return false;

                FillParameters();
                blnSave = MobjclsBLLProductionTemplate.SaveProductionTemplate(FillDetailParameters(), FillOtherDetailParameters());

                if (blnSave)
                {
                    if (lngID == 0)
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 2, out MmsgMessageIcon);
                    else
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 21, out MmsgMessageIcon);

                    lblStatus.Text = MstrCommonMessage.Split('#').Last();
                    tmrTemplate.Enabled = true;
                    MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                }
            }
            else
                blnSave = false;

            return blnSave;
        }

        private bool FormValidation()
        {
            errTemplate.Clear();
            tmrTemplate.Enabled = false;

            if (txtCode.Text == "")
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 24, out MmsgMessageIcon);
                errTemplate.SetError(txtCode, MstrCommonMessage.Replace("#", "").Trim());
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Split('#').Last();
                tmrTemplate.Enabled = true;
                txtCode.Focus();
                return false;
            }

            if (MobjclsBLLProductionTemplate.checkTemplateCodeDuplication(txtCode.Tag.ToInt64(), txtCode.Text.Trim()))
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 25, out MmsgMessageIcon);
                errTemplate.SetError(txtCode, MstrCommonMessage.Replace("#", "").Trim());
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Split('#').Last();
                tmrTemplate.Enabled = true;
                txtCode.Focus();
                return false;
            }

            if (txtName.Text == "")
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 28, out MmsgMessageIcon);
                errTemplate.SetError(txtName, MstrCommonMessage.Replace("#", "").Trim());
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Split('#').Last();
                tmrTemplate.Enabled = true;
                txtName.Focus();
                return false;
            }

            if (cboProduct.SelectedIndex == -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 9120, out MmsgMessageIcon);
                errTemplate.SetError(cboProduct, MstrCommonMessage.Replace("#", "").Trim());
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Split('#').Last();
                tmrTemplate.Enabled = true;
                cboProduct.Focus();
                return false;
            }

            if (dgvItem.Rows.Count == 1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 26, out MmsgMessageIcon);
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Split('#').Last();
                tmrTemplate.Enabled = true;
                return false;
            }
            else
            {
                for (int iCounter = 0; iCounter <= dgvItem.Rows.Count - 2; iCounter++)
                {
                    if (dgvItem.Rows[iCounter].Cells["ItemID"].Value.ToInt32() == 0)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 9120, out MmsgMessageIcon);
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        lblStatus.Text = MstrCommonMessage.Split('#').Last();
                        tmrTemplate.Enabled = true;                        
                        return false;
                    }

                    if (dgvItem.Rows[iCounter].Cells["Quantity"].Value.ToDecimal() == 0)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 29, out MmsgMessageIcon);
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        lblStatus.Text = MstrCommonMessage.Split('#').Last();
                        tmrTemplate.Enabled = true;
                        return false;
                    }

                    if (dgvItem.Rows[iCounter].Cells["Rate"].Value.ToDecimal() == 0)
                    {
                        MstrCommonMessage = "Please enter item rate.";
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        lblStatus.Text = MstrCommonMessage.Split('#').Last();
                        tmrTemplate.Enabled = true;
                        return false;
                    }
                }
            }

            return true;
        }
        
        private void FillParameters()
        {
            MobjclsBLLProductionTemplate.PobjClsDTOProductionTemplate.lngTemplateID = txtCode.Tag.ToInt64();
            MobjclsBLLProductionTemplate.PobjClsDTOProductionTemplate.strCode = txtCode.Text.Trim();
            MobjclsBLLProductionTemplate.PobjClsDTOProductionTemplate.strName = txtName.Text.Trim();
            MobjclsBLLProductionTemplate.PobjClsDTOProductionTemplate.intProductID = cboProduct.SelectedValue.ToInt32();
            MobjclsBLLProductionTemplate.PobjClsDTOProductionTemplate.strRemarks = txtRemarks.Text.Trim();
            MobjclsBLLProductionTemplate.PobjClsDTOProductionTemplate.decTotal = lblTotal.Text.ToDecimal();
        }

        private DataTable FillDetailParameters()
        {
            DataTable datTemp = new DataTable();
            datTemp.Columns.Add("ItemID");
            datTemp.Columns.Add("UOMID");
            datTemp.Columns.Add("Quantity");
            datTemp.Columns.Add("Rate");
            datTemp.Columns.Add("Total");

            for (int iCounter = 0; iCounter <= dgvItem.Rows.Count - 2; iCounter++)
            {
                datTemp.Rows.Add();
                datTemp.Rows[iCounter]["ItemID"] = dgvItem.Rows[iCounter].Cells["ItemID"].Value.ToInt32();
                datTemp.Rows[iCounter]["UOMID"] = dgvItem.Rows[iCounter].Cells["UOMID"].Value.ToInt32();
                datTemp.Rows[iCounter]["Quantity"] = dgvItem.Rows[iCounter].Cells["Quantity"].Value.ToDecimal();
                datTemp.Rows[iCounter]["Rate"] = dgvItem.Rows[iCounter].Cells["Rate"].Value.ToDecimal();
                datTemp.Rows[iCounter]["Total"] = dgvItem.Rows[iCounter].Cells["Total"].Value.ToDecimal();
            }

            return datTemp;
        }

        private DataTable FillOtherDetailParameters()
        {
            DataTable datTemp = new DataTable();
            datTemp.Columns.Add("Particulars");
            datTemp.Columns.Add("Amount");

            for (int iCounter = 0; iCounter <= dgvParticulars.Rows.Count - 2; iCounter++)
            {
                datTemp.Rows.Add();
                datTemp.Rows[iCounter]["Particulars"] = dgvParticulars.Rows[iCounter].Cells["Particulars"].Value.ToStringCustom();
                datTemp.Rows[iCounter]["Amount"] = dgvParticulars.Rows[iCounter].Cells["Amount"].Value.ToDecimal();
            }

            return datTemp;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (DeleteProductionTemplate())
                ClearControls();
        }

        private bool DeleteProductionTemplate()
        {
            bool blnDelete = false;
            MobjclsBLLProductionTemplate.PobjClsDTOProductionTemplate.lngTemplateID = txtCode.Tag.ToInt64();

            // Checking Reference is Exists or not
            DataTable datTemp = objclsBLLCommonUtility.FillCombos(new string[] { "MaterialIssueID", "InvMaterialIssueMaster", "" +
                "TemplateID = " + MobjclsBLLProductionTemplate.PobjClsDTOProductionTemplate.lngTemplateID });

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 25, out MmsgMessageIcon);
                    lblStatus.Text = MstrCommonMessage.Split('#').Last();
                    tmrTemplate.Enabled = true;
                    MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    return false;
                }
            }
            // End

            if (MobjclsBLLProductionTemplate.PobjClsDTOProductionTemplate.lngTemplateID > 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 13, out MmsgMessageIcon);

                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information) == DialogResult.No)
                    return false;

                blnDelete = MobjclsBLLProductionTemplate.DeleteProductionTemplate();

                if (blnDelete)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4, out MmsgMessageIcon);
                    lblStatus.Text = MstrCommonMessage.Split('#').Last();
                    tmrTemplate.Enabled = true;
                    MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                }
            }

            return blnDelete;
        }

        private void tmrTemplate_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            tmrTemplate.Enabled = false;
        }

        private void cboProduct_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboProduct.DroppedDown = false;
        }
    }
}