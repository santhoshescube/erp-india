﻿namespace MyBooksERP
{
    partial class frmProductionQC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProductionQC));
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvMaterialIssueDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.epMaterialIssue = new DevComponents.DotNetBar.ExpandablePanel();
            this.cboQCNo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboSPrdNO = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.btnRefresh = new DevComponents.DotNetBar.ButtonX();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblSCountStatus = new DevComponents.DotNetBar.LabelX();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelMain = new DevComponents.DotNetBar.PanelEx();
            this.tcMaterialIssue = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvQCDetails = new ClsInnerGridBar();
            this.SerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchNo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiItemDetails = new DevComponents.DotNetBar.TabItem(this.components);
            this.pnlBottom = new DevComponents.DotNetBar.PanelEx();
            this.txtRemarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.lblIssuedBy = new System.Windows.Forms.Label();
            this.cboIncharge = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.pnlMainBottom = new System.Windows.Forms.Panel();
            this.lblStatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblCreatedDateValue = new DevComponents.DotNetBar.Controls.WarningBox();
            this.lblCreatedByText = new DevComponents.DotNetBar.Controls.WarningBox();
            this.expandableSplitterTop = new DevComponents.DotNetBar.ExpandableSplitter();
            this.tcGeneral = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.lblProduct = new DevComponents.DotNetBar.LabelX();
            this.txtPassedQty = new DemoClsDataGridview.DecimalTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpOrderDate = new System.Windows.Forms.DateTimePicker();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboProductionNo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblIssueNo = new System.Windows.Forms.Label();
            this.txtCode = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblProdutionNo = new System.Windows.Forms.Label();
            this.txtSampleQuantity = new DemoClsDataGridview.DecimalTextBox();
            this.txtProducedQty = new DemoClsDataGridview.DecimalTextBox();
            this.tiGeneral = new DevComponents.DotNetBar.TabItem(this.components);
            this.bnMaterialIssue = new DevComponents.DotNetBar.Bar();
            this.bnAddNewItem = new DevComponents.DotNetBar.ButtonItem();
            this.bnSaveItem = new DevComponents.DotNetBar.ButtonItem();
            this.bnClear = new DevComponents.DotNetBar.ButtonItem();
            this.bnDelete = new DevComponents.DotNetBar.ButtonItem();
            this.bnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.bnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.errTemplate = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrTemplate = new System.Windows.Forms.Timer(this.components);
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialIssueDisplay)).BeginInit();
            this.epMaterialIssue.SuspendLayout();
            this.panelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcMaterialIssue)).BeginInit();
            this.tcMaterialIssue.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvQCDetails)).BeginInit();
            this.pnlBottom.SuspendLayout();
            this.pnlMainBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).BeginInit();
            this.tcGeneral.SuspendLayout();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnMaterialIssue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.dgvMaterialIssueDisplay);
            this.PanelLeft.Controls.Add(this.epMaterialIssue);
            this.PanelLeft.Controls.Add(this.lblSCountStatus);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(200, 474);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 5;
            this.PanelLeft.Text = "panelEx1";
            // 
            // dgvMaterialIssueDisplay
            // 
            this.dgvMaterialIssueDisplay.AllowUserToAddRows = false;
            this.dgvMaterialIssueDisplay.AllowUserToDeleteRows = false;
            this.dgvMaterialIssueDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMaterialIssueDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMaterialIssueDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMaterialIssueDisplay.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvMaterialIssueDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMaterialIssueDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvMaterialIssueDisplay.Location = new System.Drawing.Point(0, 176);
            this.dgvMaterialIssueDisplay.Name = "dgvMaterialIssueDisplay";
            this.dgvMaterialIssueDisplay.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMaterialIssueDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvMaterialIssueDisplay.RowHeadersVisible = false;
            this.dgvMaterialIssueDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMaterialIssueDisplay.Size = new System.Drawing.Size(200, 272);
            this.dgvMaterialIssueDisplay.TabIndex = 7;
            this.dgvMaterialIssueDisplay.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialIssueDisplay_CellDoubleClick);
            // 
            // epMaterialIssue
            // 
            this.epMaterialIssue.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.epMaterialIssue.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.epMaterialIssue.Controls.Add(this.cboQCNo);
            this.epMaterialIssue.Controls.Add(this.label1);
            this.epMaterialIssue.Controls.Add(this.label4);
            this.epMaterialIssue.Controls.Add(this.cboSPrdNO);
            this.epMaterialIssue.Controls.Add(this.dtpSTo);
            this.epMaterialIssue.Controls.Add(this.btnRefresh);
            this.epMaterialIssue.Controls.Add(this.dtpSFrom);
            this.epMaterialIssue.Controls.Add(this.lblTo);
            this.epMaterialIssue.Controls.Add(this.lblFrom);
            this.epMaterialIssue.Dock = System.Windows.Forms.DockStyle.Top;
            this.epMaterialIssue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.epMaterialIssue.Location = new System.Drawing.Point(0, 0);
            this.epMaterialIssue.Name = "epMaterialIssue";
            this.epMaterialIssue.Size = new System.Drawing.Size(200, 176);
            this.epMaterialIssue.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.epMaterialIssue.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.epMaterialIssue.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.epMaterialIssue.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.epMaterialIssue.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.epMaterialIssue.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.epMaterialIssue.Style.GradientAngle = 90;
            this.epMaterialIssue.TabIndex = 5;
            this.epMaterialIssue.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.epMaterialIssue.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.epMaterialIssue.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.epMaterialIssue.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.epMaterialIssue.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.epMaterialIssue.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.epMaterialIssue.TitleStyle.GradientAngle = 90;
            this.epMaterialIssue.TitleText = "Search";
            // 
            // cboQCNo
            // 
            this.cboQCNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboQCNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboQCNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboQCNo.DisplayMember = "Text";
            this.cboQCNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboQCNo.DropDownHeight = 75;
            this.cboQCNo.FormattingEnabled = true;
            this.cboQCNo.IntegralHeight = false;
            this.cboQCNo.ItemHeight = 14;
            this.cboQCNo.Location = new System.Drawing.Point(77, 108);
            this.cboQCNo.Name = "cboQCNo";
            this.cboQCNo.Size = new System.Drawing.Size(117, 20);
            this.cboQCNo.TabIndex = 266;
            this.cboQCNo.DropDown += new System.EventHandler(this.cboQCNo_DropDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(4, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 265;
            this.label1.Text = "Production No";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(4, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 264;
            this.label4.Text = "QC No";
            // 
            // cboSPrdNO
            // 
            this.cboSPrdNO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSPrdNO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSPrdNO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSPrdNO.DisplayMember = "Text";
            this.cboSPrdNO.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSPrdNO.DropDownHeight = 75;
            this.cboSPrdNO.FormattingEnabled = true;
            this.cboSPrdNO.IntegralHeight = false;
            this.cboSPrdNO.ItemHeight = 14;
            this.cboSPrdNO.Location = new System.Drawing.Point(79, 32);
            this.cboSPrdNO.Name = "cboSPrdNO";
            this.cboSPrdNO.Size = new System.Drawing.Size(117, 20);
            this.cboSPrdNO.TabIndex = 2;
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(79, 82);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.Size = new System.Drawing.Size(117, 20);
            this.dtpSTo.TabIndex = 5;
            // 
            // btnRefresh
            // 
            this.btnRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnRefresh.Location = new System.Drawing.Point(126, 134);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(71, 23);
            this.btnRefresh.TabIndex = 7;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(79, 56);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.Size = new System.Drawing.Size(117, 20);
            this.dtpSFrom.TabIndex = 4;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(4, 60);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 258;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(4, 83);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 257;
            this.lblFrom.Text = "From";
            // 
            // lblSCountStatus
            // 
            // 
            // 
            // 
            this.lblSCountStatus.BackgroundStyle.Class = "";
            this.lblSCountStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSCountStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblSCountStatus.Location = new System.Drawing.Point(0, 448);
            this.lblSCountStatus.Name = "lblSCountStatus";
            this.lblSCountStatus.Size = new System.Drawing.Size(200, 26);
            this.lblSCountStatus.TabIndex = 8;
            this.lblSCountStatus.Text = "...";
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(200, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 474);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 26;
            this.expandableSplitterLeft.TabStop = false;
            // 
            // panelMain
            // 
            this.panelMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelMain.Controls.Add(this.tcMaterialIssue);
            this.panelMain.Controls.Add(this.pnlBottom);
            this.panelMain.Controls.Add(this.pnlMainBottom);
            this.panelMain.Controls.Add(this.expandableSplitterTop);
            this.panelMain.Controls.Add(this.tcGeneral);
            this.panelMain.Controls.Add(this.bnMaterialIssue);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(203, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(691, 474);
            this.panelMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelMain.Style.GradientAngle = 90;
            this.panelMain.TabIndex = 27;
            this.panelMain.Text = "panelMain";
            // 
            // tcMaterialIssue
            // 
            this.tcMaterialIssue.BackColor = System.Drawing.Color.Transparent;
            this.tcMaterialIssue.CanReorderTabs = true;
            this.tcMaterialIssue.Controls.Add(this.tabControlPanel1);
            this.tcMaterialIssue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMaterialIssue.Location = new System.Drawing.Point(0, 165);
            this.tcMaterialIssue.Name = "tcMaterialIssue";
            this.tcMaterialIssue.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcMaterialIssue.SelectedTabIndex = 0;
            this.tcMaterialIssue.Size = new System.Drawing.Size(691, 203);
            this.tcMaterialIssue.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcMaterialIssue.TabIndex = 0;
            this.tcMaterialIssue.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcMaterialIssue.Tabs.Add(this.tiItemDetails);
            this.tcMaterialIssue.TabStop = false;
            this.tcMaterialIssue.Text = "tabControl1";
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.dgvQCDetails);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(691, 181);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 0;
            this.tabControlPanel1.TabItem = this.tiItemDetails;
            // 
            // dgvQCDetails
            // 
            this.dgvQCDetails.AddNewRow = false;
            this.dgvQCDetails.AllowUserToAddRows = false;
            this.dgvQCDetails.AllowUserToDeleteRows = false;
            this.dgvQCDetails.AlphaNumericCols = new int[0];
            this.dgvQCDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvQCDetails.CapsLockCols = new int[0];
            this.dgvQCDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvQCDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SerialNo,
            this.BatchNo,
            this.Status,
            this.Remarks});
            this.dgvQCDetails.DecimalCols = new int[] {
        6};
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvQCDetails.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvQCDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvQCDetails.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvQCDetails.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvQCDetails.HasSlNo = false;
            this.dgvQCDetails.LastRowIndex = 0;
            this.dgvQCDetails.Location = new System.Drawing.Point(1, 1);
            this.dgvQCDetails.Name = "dgvQCDetails";
            this.dgvQCDetails.NegativeValueCols = new int[0];
            this.dgvQCDetails.NumericCols = new int[0];
            this.dgvQCDetails.RowHeadersWidth = 50;
            this.dgvQCDetails.Size = new System.Drawing.Size(689, 179);
            this.dgvQCDetails.TabIndex = 0;
            this.dgvQCDetails.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvQCDetails_RowsAdded);
            this.dgvQCDetails.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvQCDetails_DataError);
            // 
            // SerialNo
            // 
            this.SerialNo.HeaderText = "SerialNo";
            this.SerialNo.Name = "SerialNo";
            this.SerialNo.ReadOnly = true;
            // 
            // BatchNo
            // 
            this.BatchNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BatchNo.HeaderText = "BatchNo";
            this.BatchNo.Name = "BatchNo";
            // 
            // Status
            // 
            this.Status.HeaderText = "Passed/Failed";
            this.Status.Name = "Status";
            this.Status.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Remarks
            // 
            this.Remarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.Name = "Remarks";
            // 
            // tiItemDetails
            // 
            this.tiItemDetails.AttachedControl = this.tabControlPanel1;
            this.tiItemDetails.Name = "tiItemDetails";
            this.tiItemDetails.Text = "QC Details";
            // 
            // pnlBottom
            // 
            this.pnlBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottom.Controls.Add(this.txtRemarks);
            this.pnlBottom.Controls.Add(this.lblRemarks);
            this.pnlBottom.Controls.Add(this.lblIssuedBy);
            this.pnlBottom.Controls.Add(this.cboIncharge);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 368);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(691, 80);
            this.pnlBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottom.Style.GradientAngle = 90;
            this.pnlBottom.TabIndex = 1;
            // 
            // txtRemarks
            // 
            // 
            // 
            // 
            this.txtRemarks.Border.Class = "TextBoxBorder";
            this.txtRemarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtRemarks.Border.WordWrap = true;
            this.txtRemarks.Location = new System.Drawing.Point(119, 31);
            this.txtRemarks.MaxLength = 1000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(546, 45);
            this.txtRemarks.TabIndex = 215;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(25, 33);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 216;
            this.lblRemarks.Text = "Remarks";
            // 
            // lblIssuedBy
            // 
            this.lblIssuedBy.AutoSize = true;
            this.lblIssuedBy.BackColor = System.Drawing.Color.Transparent;
            this.lblIssuedBy.Location = new System.Drawing.Point(25, 9);
            this.lblIssuedBy.Name = "lblIssuedBy";
            this.lblIssuedBy.Size = new System.Drawing.Size(49, 13);
            this.lblIssuedBy.TabIndex = 228;
            this.lblIssuedBy.Text = "Incharge";
            // 
            // cboIncharge
            // 
            this.cboIncharge.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboIncharge.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboIncharge.DisplayMember = "Text";
            this.cboIncharge.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboIncharge.DropDownHeight = 75;
            this.cboIncharge.FormattingEnabled = true;
            this.cboIncharge.IntegralHeight = false;
            this.cboIncharge.ItemHeight = 14;
            this.cboIncharge.Location = new System.Drawing.Point(119, 5);
            this.cboIncharge.Name = "cboIncharge";
            this.cboIncharge.Size = new System.Drawing.Size(215, 20);
            this.cboIncharge.TabIndex = 2;
            // 
            // pnlMainBottom
            // 
            this.pnlMainBottom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlMainBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMainBottom.Controls.Add(this.lblStatus);
            this.pnlMainBottom.Controls.Add(this.lblCreatedDateValue);
            this.pnlMainBottom.Controls.Add(this.lblCreatedByText);
            this.pnlMainBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlMainBottom.Location = new System.Drawing.Point(0, 448);
            this.pnlMainBottom.Name = "pnlMainBottom";
            this.pnlMainBottom.Size = new System.Drawing.Size(691, 26);
            this.pnlMainBottom.TabIndex = 293;
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblStatus.CloseButtonVisible = false;
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.Image = ((System.Drawing.Image)(resources.GetObject("lblStatus.Image")));
            this.lblStatus.Location = new System.Drawing.Point(0, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.OptionsButtonVisible = false;
            this.lblStatus.Size = new System.Drawing.Size(289, 24);
            this.lblStatus.TabIndex = 20;
            // 
            // lblCreatedDateValue
            // 
            this.lblCreatedDateValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedDateValue.CloseButtonVisible = false;
            this.lblCreatedDateValue.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblCreatedDateValue.Location = new System.Drawing.Point(289, 0);
            this.lblCreatedDateValue.Name = "lblCreatedDateValue";
            this.lblCreatedDateValue.OptionsButtonVisible = false;
            this.lblCreatedDateValue.Size = new System.Drawing.Size(200, 24);
            this.lblCreatedDateValue.TabIndex = 256;
            this.lblCreatedDateValue.Text = "Created Date";
            // 
            // lblCreatedByText
            // 
            this.lblCreatedByText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblCreatedByText.CloseButtonVisible = false;
            this.lblCreatedByText.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblCreatedByText.Location = new System.Drawing.Point(489, 0);
            this.lblCreatedByText.Name = "lblCreatedByText";
            this.lblCreatedByText.OptionsButtonVisible = false;
            this.lblCreatedByText.Size = new System.Drawing.Size(200, 24);
            this.lblCreatedByText.TabIndex = 258;
            this.lblCreatedByText.Text = "Created By";
            // 
            // expandableSplitterTop
            // 
            this.expandableSplitterTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterTop.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitterTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitterTop.ExpandableControl = this.tcGeneral;
            this.expandableSplitterTop.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterTop.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterTop.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterTop.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterTop.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.Location = new System.Drawing.Point(0, 162);
            this.expandableSplitterTop.Name = "expandableSplitterTop";
            this.expandableSplitterTop.Size = new System.Drawing.Size(691, 3);
            this.expandableSplitterTop.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterTop.TabIndex = 3;
            this.expandableSplitterTop.TabStop = false;
            // 
            // tcGeneral
            // 
            this.tcGeneral.BackColor = System.Drawing.Color.Transparent;
            this.tcGeneral.CanReorderTabs = true;
            this.tcGeneral.Controls.Add(this.tabControlPanel2);
            this.tcGeneral.Dock = System.Windows.Forms.DockStyle.Top;
            this.tcGeneral.Location = new System.Drawing.Point(0, 25);
            this.tcGeneral.Name = "tcGeneral";
            this.tcGeneral.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcGeneral.SelectedTabIndex = 0;
            this.tcGeneral.Size = new System.Drawing.Size(691, 137);
            this.tcGeneral.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcGeneral.TabIndex = 0;
            this.tcGeneral.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcGeneral.Tabs.Add(this.tiGeneral);
            this.tcGeneral.TabStop = false;
            this.tcGeneral.Text = "tabControl1";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.lblProduct);
            this.tabControlPanel2.Controls.Add(this.txtPassedQty);
            this.tabControlPanel2.Controls.Add(this.label2);
            this.tabControlPanel2.Controls.Add(this.dtpOrderDate);
            this.tabControlPanel2.Controls.Add(this.lblDate);
            this.tabControlPanel2.Controls.Add(this.lblQuantity);
            this.tabControlPanel2.Controls.Add(this.label5);
            this.tabControlPanel2.Controls.Add(this.cboProductionNo);
            this.tabControlPanel2.Controls.Add(this.lblIssueNo);
            this.tabControlPanel2.Controls.Add(this.txtCode);
            this.tabControlPanel2.Controls.Add(this.lblProdutionNo);
            this.tabControlPanel2.Controls.Add(this.txtSampleQuantity);
            this.tabControlPanel2.Controls.Add(this.txtProducedQty);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(691, 115);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 1;
            this.tabControlPanel2.TabItem = this.tiGeneral;
            // 
            // lblProduct
            // 
            this.lblProduct.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblProduct.BackgroundStyle.Class = "";
            this.lblProduct.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblProduct.Location = new System.Drawing.Point(9, 88);
            this.lblProduct.Name = "lblProduct";
            this.lblProduct.Size = new System.Drawing.Size(634, 23);
            this.lblProduct.TabIndex = 248;
            this.lblProduct.Text = "labelX1";
            // 
            // txtPassedQty
            // 
            this.txtPassedQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPassedQty.Location = new System.Drawing.Point(543, 60);
            this.txtPassedQty.MaxLength = 15;
            this.txtPassedQty.Name = "txtPassedQty";
            this.txtPassedQty.ShortcutsEnabled = false;
            this.txtPassedQty.Size = new System.Drawing.Size(100, 20);
            this.txtPassedQty.TabIndex = 247;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(442, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 246;
            this.label2.Text = "Passed Quantity";
            // 
            // dtpOrderDate
            // 
            this.dtpOrderDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOrderDate.Location = new System.Drawing.Point(104, 59);
            this.dtpOrderDate.Name = "dtpOrderDate";
            this.dtpOrderDate.Size = new System.Drawing.Size(112, 20);
            this.dtpOrderDate.TabIndex = 243;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Location = new System.Drawing.Point(6, 63);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(48, 13);
            this.lblDate.TabIndex = 244;
            this.lblDate.Text = "QC Date";
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.BackColor = System.Drawing.Color.Transparent;
            this.lblQuantity.Location = new System.Drawing.Point(442, 36);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(84, 13);
            this.lblQuantity.TabIndex = 241;
            this.lblQuantity.Text = "Sample Quantity";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(442, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 238;
            this.label5.Text = "Produced Quantity";
            // 
            // cboProductionNo
            // 
            this.cboProductionNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboProductionNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboProductionNo.DisplayMember = "Text";
            this.cboProductionNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboProductionNo.DropDownHeight = 75;
            this.cboProductionNo.FormattingEnabled = true;
            this.cboProductionNo.IntegralHeight = false;
            this.cboProductionNo.ItemHeight = 14;
            this.cboProductionNo.Location = new System.Drawing.Point(104, 33);
            this.cboProductionNo.Name = "cboProductionNo";
            this.cboProductionNo.Size = new System.Drawing.Size(215, 20);
            this.cboProductionNo.TabIndex = 0;
            this.cboProductionNo.TabStop = false;
            this.cboProductionNo.SelectedIndexChanged += new System.EventHandler(this.cboProductionNo_SelectedIndexChanged);
            // 
            // lblIssueNo
            // 
            this.lblIssueNo.AutoSize = true;
            this.lblIssueNo.BackColor = System.Drawing.Color.Transparent;
            this.lblIssueNo.Location = new System.Drawing.Point(6, 36);
            this.lblIssueNo.Name = "lblIssueNo";
            this.lblIssueNo.Size = new System.Drawing.Size(78, 13);
            this.lblIssueNo.TabIndex = 229;
            this.lblIssueNo.Text = "Production No.";
            // 
            // txtCode
            // 
            this.txtCode.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtCode.Border.Class = "TextBoxBorder";
            this.txtCode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCode.Location = new System.Drawing.Point(104, 6);
            this.txtCode.MaxLength = 50;
            this.txtCode.Name = "txtCode";
            this.txtCode.ReadOnly = true;
            this.txtCode.Size = new System.Drawing.Size(100, 20);
            this.txtCode.TabIndex = 3;
            this.txtCode.TabStop = false;
            // 
            // lblProdutionNo
            // 
            this.lblProdutionNo.AutoSize = true;
            this.lblProdutionNo.BackColor = System.Drawing.Color.Transparent;
            this.lblProdutionNo.Location = new System.Drawing.Point(6, 10);
            this.lblProdutionNo.Name = "lblProdutionNo";
            this.lblProdutionNo.Size = new System.Drawing.Size(39, 13);
            this.lblProdutionNo.TabIndex = 225;
            this.lblProdutionNo.Text = "QC No";
            // 
            // txtSampleQuantity
            // 
            this.txtSampleQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSampleQuantity.Location = new System.Drawing.Point(543, 33);
            this.txtSampleQuantity.MaxLength = 15;
            this.txtSampleQuantity.Name = "txtSampleQuantity";
            this.txtSampleQuantity.ShortcutsEnabled = false;
            this.txtSampleQuantity.Size = new System.Drawing.Size(100, 20);
            this.txtSampleQuantity.TabIndex = 242;
            this.txtSampleQuantity.TextChanged += new System.EventHandler(this.txtSampleQuantity_TextChanged);
            // 
            // txtProducedQty
            // 
            this.txtProducedQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProducedQty.Location = new System.Drawing.Point(543, 6);
            this.txtProducedQty.Name = "txtProducedQty";
            this.txtProducedQty.ReadOnly = true;
            this.txtProducedQty.ShortcutsEnabled = false;
            this.txtProducedQty.Size = new System.Drawing.Size(100, 20);
            this.txtProducedQty.TabIndex = 239;
            this.txtProducedQty.TextChanged += new System.EventHandler(this.txtProducedQty_TextChanged);
            // 
            // tiGeneral
            // 
            this.tiGeneral.AttachedControl = this.tabControlPanel2;
            this.tiGeneral.Name = "tiGeneral";
            this.tiGeneral.Text = "General";
            // 
            // bnMaterialIssue
            // 
            this.bnMaterialIssue.AccessibleDescription = "DotNetBar Bar (bnMaterialIssue)";
            this.bnMaterialIssue.AccessibleName = "DotNetBar Bar";
            this.bnMaterialIssue.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.bnMaterialIssue.Dock = System.Windows.Forms.DockStyle.Top;
            this.bnMaterialIssue.DockLine = 1;
            this.bnMaterialIssue.DockOffset = 73;
            this.bnMaterialIssue.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.bnMaterialIssue.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.bnMaterialIssue.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnClear,
            this.bnDelete,
            this.bnPrint,
            this.bnEmail});
            this.bnMaterialIssue.Location = new System.Drawing.Point(0, 0);
            this.bnMaterialIssue.Name = "bnMaterialIssue";
            this.bnMaterialIssue.Size = new System.Drawing.Size(691, 25);
            this.bnMaterialIssue.Stretch = true;
            this.bnMaterialIssue.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.bnMaterialIssue.TabIndex = 12;
            this.bnMaterialIssue.TabStop = false;
            this.bnMaterialIssue.Text = "bar2";
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnAddNewItem.Image = global::MyBooksERP.Properties.Resources.Add;
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlEnter);
            this.bnAddNewItem.Text = "Add";
            this.bnAddNewItem.Tooltip = "Add New Information";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnSaveItem.Image = global::MyBooksERP.Properties.Resources.save;
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Text = "Save";
            this.bnSaveItem.Tooltip = "Save";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnClear
            // 
            this.bnClear.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.bnClear.Name = "bnClear";
            this.bnClear.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.bnClear.Text = "Clear";
            this.bnClear.Tooltip = "Clear";
            this.bnClear.Visible = false;
            // 
            // bnDelete
            // 
            this.bnDelete.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnDelete.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.bnDelete.Name = "bnDelete";
            this.bnDelete.Text = "Delete";
            this.bnDelete.Tooltip = "Delete";
            this.bnDelete.Click += new System.EventHandler(this.bnDelete_Click);
            // 
            // bnPrint
            // 
            this.bnPrint.BeginGroup = true;
            this.bnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.bnPrint.Text = "Print";
            this.bnPrint.Visible = false;
            // 
            // bnEmail
            // 
            this.bnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.bnEmail.Text = "Email";
            this.bnEmail.Visible = false;
            // 
            // errTemplate
            // 
            this.errTemplate.ContainerControl = this;
            // 
            // tmrTemplate
            // 
            this.tmrTemplate.Interval = 1000;
            // 
            // frmProductionQC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 474);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.expandableSplitterLeft);
            this.Controls.Add(this.PanelLeft);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProductionQC";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Quality Check";
            this.Load += new System.EventHandler(this.frmProductionProcess_Load);
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialIssueDisplay)).EndInit();
            this.epMaterialIssue.ResumeLayout(false);
            this.epMaterialIssue.PerformLayout();
            this.panelMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcMaterialIssue)).EndInit();
            this.tcMaterialIssue.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvQCDetails)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.pnlMainBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).EndInit();
            this.tcGeneral.ResumeLayout(false);
            this.tabControlPanel2.ResumeLayout(false);
            this.tabControlPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnMaterialIssue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errTemplate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx PanelLeft;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvMaterialIssueDisplay;
        private DevComponents.DotNetBar.ExpandablePanel epMaterialIssue;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSPrdNO;
        private System.Windows.Forms.DateTimePicker dtpSTo;
        private System.Windows.Forms.DateTimePicker dtpSFrom;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private DevComponents.DotNetBar.ButtonX btnRefresh;
        private DevComponents.DotNetBar.LabelX lblSCountStatus;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterLeft;
        private DevComponents.DotNetBar.PanelEx panelMain;
        private DevComponents.DotNetBar.TabControl tcMaterialIssue;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private DevComponents.DotNetBar.TabItem tiItemDetails;
        private System.Windows.Forms.Panel pnlMainBottom;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedByText;
        private DevComponents.DotNetBar.Controls.WarningBox lblCreatedDateValue;
        private DevComponents.DotNetBar.Controls.WarningBox lblStatus;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterTop;
        private DevComponents.DotNetBar.TabControl tcGeneral;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboIncharge;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboProductionNo;
        private System.Windows.Forms.Label lblIssueNo;
        private System.Windows.Forms.Label lblIssuedBy;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCode;
        private System.Windows.Forms.Label lblProdutionNo;
        private DevComponents.DotNetBar.TabItem tiGeneral;
        private DevComponents.DotNetBar.Bar bnMaterialIssue;
        private DevComponents.DotNetBar.ButtonItem bnAddNewItem;
        private DevComponents.DotNetBar.ButtonItem bnSaveItem;
        private DevComponents.DotNetBar.ButtonItem bnClear;
        private DevComponents.DotNetBar.ButtonItem bnDelete;
        private DevComponents.DotNetBar.ButtonItem bnPrint;
        private DevComponents.DotNetBar.ButtonItem bnEmail;
        private DevComponents.DotNetBar.PanelEx pnlBottom;
        private DevComponents.DotNetBar.Controls.TextBoxX txtRemarks;
        private System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.Label label4;
        private DemoClsDataGridview.DecimalTextBox txtProducedQty;
        private System.Windows.Forms.Label label5;
        private DemoClsDataGridview.DecimalTextBox txtSampleQuantity;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpOrderDate;
        private System.Windows.Forms.Label lblDate;
        private DemoClsDataGridview.DecimalTextBox txtPassedQty;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ErrorProvider errTemplate;
        private System.Windows.Forms.Timer tmrTemplate;
        private ClsInnerGridBar dgvQCDetails;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboQCNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SerialNo;
        private System.Windows.Forms.DataGridViewComboBoxColumn BatchNo;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private DevComponents.DotNetBar.LabelX lblProduct;
    }
}