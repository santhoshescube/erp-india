﻿namespace MyBooksERP
{
    partial class frmProductionProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProductionProcess));
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvProcessDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ProductionID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductionNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOMNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.epMaterialIssue = new DevComponents.DotNetBar.ExpandablePanel();
            this.label4 = new System.Windows.Forms.Label();
            this.cboSBOMNo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblSearchBOMNo = new System.Windows.Forms.Label();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.btnRefresh = new DevComponents.DotNetBar.ButtonX();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblSCountStatus = new DevComponents.DotNetBar.LabelX();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelMain = new DevComponents.DotNetBar.PanelEx();
            this.tcMaterialIssue = new DevComponents.DotNetBar.TabControl();
            this.tcpMaterial = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvMaterialDetails = new ClsInnerGridBar();
            this.ItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActualQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RequiredQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WastageQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiMaterial = new DevComponents.DotNetBar.TabItem(this.components);
            this.pnlBottom = new DevComponents.DotNetBar.PanelEx();
            this.txtRemarks = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.lblIssuedBy = new System.Windows.Forms.Label();
            this.cboIncharge = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.pnlMainBottom = new System.Windows.Forms.Panel();
            this.lblStatus = new DevComponents.DotNetBar.Controls.WarningBox();
            this.expandableSplitterTop = new DevComponents.DotNetBar.ExpandableSplitter();
            this.tcGeneral = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblBatchNo = new System.Windows.Forms.Label();
            this.txtBatchNo = new System.Windows.Forms.TextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.cboWarehouse = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtQuantity = new DemoClsDataGridview.DecimalTextBox();
            this.txtDetails = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblMaterialIssueDetails = new System.Windows.Forms.Label();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.txtProductionNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblProdutionNo = new System.Windows.Forms.Label();
            this.lblBOMNo = new System.Windows.Forms.Label();
            this.cboBOMNo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.tiGeneral = new DevComponents.DotNetBar.TabItem(this.components);
            this.bnMaterialIssue = new DevComponents.DotNetBar.Bar();
            this.btnAdd = new DevComponents.DotNetBar.ButtonItem();
            this.btnSave = new DevComponents.DotNetBar.ButtonItem();
            this.bnClear = new DevComponents.DotNetBar.ButtonItem();
            this.btnDelete = new DevComponents.DotNetBar.ButtonItem();
            this.btnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.btnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.tmrProcess = new System.Windows.Forms.Timer(this.components);
            this.errProcess = new System.Windows.Forms.ErrorProvider(this.components);
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcessDisplay)).BeginInit();
            this.epMaterialIssue.SuspendLayout();
            this.panelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcMaterialIssue)).BeginInit();
            this.tcMaterialIssue.SuspendLayout();
            this.tcpMaterial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialDetails)).BeginInit();
            this.pnlBottom.SuspendLayout();
            this.pnlMainBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).BeginInit();
            this.tcGeneral.SuspendLayout();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnMaterialIssue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.dgvProcessDisplay);
            this.PanelLeft.Controls.Add(this.epMaterialIssue);
            this.PanelLeft.Controls.Add(this.lblSCountStatus);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(200, 474);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 5;
            this.PanelLeft.Text = "panelEx1";
            // 
            // dgvProcessDisplay
            // 
            this.dgvProcessDisplay.AllowUserToAddRows = false;
            this.dgvProcessDisplay.AllowUserToDeleteRows = false;
            this.dgvProcessDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProcessDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvProcessDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProcessDisplay.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductionID,
            this.ProductionNo,
            this.BOMNo});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProcessDisplay.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvProcessDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProcessDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvProcessDisplay.Location = new System.Drawing.Point(0, 158);
            this.dgvProcessDisplay.Name = "dgvProcessDisplay";
            this.dgvProcessDisplay.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProcessDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvProcessDisplay.RowHeadersVisible = false;
            this.dgvProcessDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProcessDisplay.Size = new System.Drawing.Size(200, 290);
            this.dgvProcessDisplay.TabIndex = 7;
            this.dgvProcessDisplay.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialIssueDisplay_CellDoubleClick);
            // 
            // ProductionID
            // 
            this.ProductionID.DataPropertyName = "ProductionID";
            this.ProductionID.HeaderText = "ProductionID";
            this.ProductionID.Name = "ProductionID";
            this.ProductionID.ReadOnly = true;
            this.ProductionID.Visible = false;
            // 
            // ProductionNo
            // 
            this.ProductionNo.DataPropertyName = "ProductionNo";
            this.ProductionNo.HeaderText = "Production No";
            this.ProductionNo.Name = "ProductionNo";
            this.ProductionNo.ReadOnly = true;
            // 
            // BOMNo
            // 
            this.BOMNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BOMNo.DataPropertyName = "BOMNo";
            this.BOMNo.HeaderText = "BOM No";
            this.BOMNo.Name = "BOMNo";
            this.BOMNo.ReadOnly = true;
            // 
            // epMaterialIssue
            // 
            this.epMaterialIssue.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.epMaterialIssue.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.epMaterialIssue.Controls.Add(this.label4);
            this.epMaterialIssue.Controls.Add(this.cboSBOMNo);
            this.epMaterialIssue.Controls.Add(this.lblSearchBOMNo);
            this.epMaterialIssue.Controls.Add(this.txtSearch);
            this.epMaterialIssue.Controls.Add(this.dtpSTo);
            this.epMaterialIssue.Controls.Add(this.btnRefresh);
            this.epMaterialIssue.Controls.Add(this.dtpSFrom);
            this.epMaterialIssue.Controls.Add(this.lblTo);
            this.epMaterialIssue.Controls.Add(this.lblFrom);
            this.epMaterialIssue.Dock = System.Windows.Forms.DockStyle.Top;
            this.epMaterialIssue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.epMaterialIssue.Location = new System.Drawing.Point(0, 0);
            this.epMaterialIssue.Name = "epMaterialIssue";
            this.epMaterialIssue.Size = new System.Drawing.Size(200, 158);
            this.epMaterialIssue.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.epMaterialIssue.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.epMaterialIssue.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.epMaterialIssue.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.epMaterialIssue.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.epMaterialIssue.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.epMaterialIssue.Style.GradientAngle = 90;
            this.epMaterialIssue.TabIndex = 5;
            this.epMaterialIssue.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.epMaterialIssue.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.epMaterialIssue.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.epMaterialIssue.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.epMaterialIssue.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.epMaterialIssue.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.epMaterialIssue.TitleStyle.GradientAngle = 90;
            this.epMaterialIssue.TitleText = "Search";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(4, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 264;
            this.label4.Text = "Production No";
            // 
            // cboSBOMNo
            // 
            this.cboSBOMNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSBOMNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSBOMNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSBOMNo.DisplayMember = "Text";
            this.cboSBOMNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSBOMNo.DropDownHeight = 75;
            this.cboSBOMNo.FormattingEnabled = true;
            this.cboSBOMNo.IntegralHeight = false;
            this.cboSBOMNo.ItemHeight = 14;
            this.cboSBOMNo.Location = new System.Drawing.Point(79, 32);
            this.cboSBOMNo.Name = "cboSBOMNo";
            this.cboSBOMNo.Size = new System.Drawing.Size(117, 20);
            this.cboSBOMNo.TabIndex = 2;
            this.cboSBOMNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboBOMNo_KeyPress);
            // 
            // lblSearchBOMNo
            // 
            this.lblSearchBOMNo.AutoSize = true;
            this.lblSearchBOMNo.Location = new System.Drawing.Point(4, 35);
            this.lblSearchBOMNo.Name = "lblSearchBOMNo";
            this.lblSearchBOMNo.Size = new System.Drawing.Size(48, 13);
            this.lblSearchBOMNo.TabIndex = 263;
            this.lblSearchBOMNo.Text = "BOM No";
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(79, 99);
            this.txtSearch.MaxLength = 20;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(117, 20);
            this.txtSearch.TabIndex = 6;
            this.txtSearch.WatermarkEnabled = false;
            this.txtSearch.WatermarkText = "Issue No";
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(79, 77);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.Size = new System.Drawing.Size(117, 20);
            this.dtpSTo.TabIndex = 5;
            // 
            // btnRefresh
            // 
            this.btnRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnRefresh.Location = new System.Drawing.Point(124, 128);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(71, 23);
            this.btnRefresh.TabIndex = 7;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(79, 54);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.Size = new System.Drawing.Size(117, 20);
            this.dtpSFrom.TabIndex = 4;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(4, 58);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 258;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(4, 82);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 257;
            this.lblFrom.Text = "From";
            // 
            // lblSCountStatus
            // 
            // 
            // 
            // 
            this.lblSCountStatus.BackgroundStyle.Class = "";
            this.lblSCountStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSCountStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblSCountStatus.Location = new System.Drawing.Point(0, 448);
            this.lblSCountStatus.Name = "lblSCountStatus";
            this.lblSCountStatus.Size = new System.Drawing.Size(200, 26);
            this.lblSCountStatus.TabIndex = 8;
            this.lblSCountStatus.Text = "...";
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(200, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 474);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 26;
            this.expandableSplitterLeft.TabStop = false;
            // 
            // panelMain
            // 
            this.panelMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelMain.Controls.Add(this.tcMaterialIssue);
            this.panelMain.Controls.Add(this.pnlBottom);
            this.panelMain.Controls.Add(this.pnlMainBottom);
            this.panelMain.Controls.Add(this.expandableSplitterTop);
            this.panelMain.Controls.Add(this.tcGeneral);
            this.panelMain.Controls.Add(this.bnMaterialIssue);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(203, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(691, 474);
            this.panelMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelMain.Style.GradientAngle = 90;
            this.panelMain.TabIndex = 27;
            this.panelMain.Text = "panelMain";
            // 
            // tcMaterialIssue
            // 
            this.tcMaterialIssue.BackColor = System.Drawing.Color.Transparent;
            this.tcMaterialIssue.CanReorderTabs = true;
            this.tcMaterialIssue.Controls.Add(this.tcpMaterial);
            this.tcMaterialIssue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMaterialIssue.Location = new System.Drawing.Point(0, 161);
            this.tcMaterialIssue.Name = "tcMaterialIssue";
            this.tcMaterialIssue.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcMaterialIssue.SelectedTabIndex = 0;
            this.tcMaterialIssue.Size = new System.Drawing.Size(691, 207);
            this.tcMaterialIssue.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcMaterialIssue.TabIndex = 0;
            this.tcMaterialIssue.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcMaterialIssue.Tabs.Add(this.tiMaterial);
            this.tcMaterialIssue.TabStop = false;
            this.tcMaterialIssue.Text = "tabControl1";
            // 
            // tcpMaterial
            // 
            this.tcpMaterial.Controls.Add(this.dgvMaterialDetails);
            this.tcpMaterial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcpMaterial.Location = new System.Drawing.Point(0, 22);
            this.tcpMaterial.Name = "tcpMaterial";
            this.tcpMaterial.Padding = new System.Windows.Forms.Padding(1);
            this.tcpMaterial.Size = new System.Drawing.Size(691, 185);
            this.tcpMaterial.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tcpMaterial.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tcpMaterial.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tcpMaterial.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tcpMaterial.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tcpMaterial.Style.GradientAngle = 90;
            this.tcpMaterial.TabIndex = 1;
            this.tcpMaterial.TabItem = this.tiMaterial;
            // 
            // dgvMaterialDetails
            // 
            this.dgvMaterialDetails.AddNewRow = false;
            this.dgvMaterialDetails.AllowUserToAddRows = false;
            this.dgvMaterialDetails.AllowUserToDeleteRows = false;
            this.dgvMaterialDetails.AlphaNumericCols = new int[0];
            this.dgvMaterialDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvMaterialDetails.CapsLockCols = new int[0];
            this.dgvMaterialDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMaterialDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemID,
            this.ItemCode,
            this.ItemName,
            this.ActualQuantity,
            this.RequiredQuantity,
            this.WastageQuantity,
            this.UOMID,
            this.UOM});
            this.dgvMaterialDetails.DecimalCols = new int[] {
        6};
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMaterialDetails.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvMaterialDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMaterialDetails.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvMaterialDetails.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvMaterialDetails.HasSlNo = false;
            this.dgvMaterialDetails.LastRowIndex = 0;
            this.dgvMaterialDetails.Location = new System.Drawing.Point(1, 1);
            this.dgvMaterialDetails.Name = "dgvMaterialDetails";
            this.dgvMaterialDetails.NegativeValueCols = new int[0];
            this.dgvMaterialDetails.NumericCols = new int[0];
            this.dgvMaterialDetails.RowHeadersWidth = 50;
            this.dgvMaterialDetails.Size = new System.Drawing.Size(689, 183);
            this.dgvMaterialDetails.TabIndex = 1;
            this.dgvMaterialDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialDetails_CellValueChanged);
            this.dgvMaterialDetails.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvMaterialDetails_RowsAdded);
            this.dgvMaterialDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvMaterialDetails_EditingControlShowing);
            this.dgvMaterialDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvMaterialDetails_CurrentCellDirtyStateChanged);
            this.dgvMaterialDetails.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvMaterialDetails_RowsRemoved);
            // 
            // ItemID
            // 
            this.ItemID.DataPropertyName = "ItemID";
            this.ItemID.HeaderText = "ItemID";
            this.ItemID.Name = "ItemID";
            this.ItemID.Visible = false;
            // 
            // ItemCode
            // 
            this.ItemCode.DataPropertyName = "ItemCode";
            this.ItemCode.HeaderText = "Item Code";
            this.ItemCode.MaxInputLength = 20;
            this.ItemCode.Name = "ItemCode";
            // 
            // ItemName
            // 
            this.ItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ItemName.DataPropertyName = "ItemName";
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.MaxInputLength = 100;
            this.ItemName.Name = "ItemName";
            // 
            // ActualQuantity
            // 
            this.ActualQuantity.DataPropertyName = "ActualQuantity";
            this.ActualQuantity.HeaderText = "ActualQuantity";
            this.ActualQuantity.Name = "ActualQuantity";
            this.ActualQuantity.ReadOnly = true;
            this.ActualQuantity.Visible = false;
            // 
            // RequiredQuantity
            // 
            this.RequiredQuantity.DataPropertyName = "RequiredQuantity";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.RequiredQuantity.DefaultCellStyle = dataGridViewCellStyle4;
            this.RequiredQuantity.HeaderText = "Required Qty";
            this.RequiredQuantity.MaxInputLength = 15;
            this.RequiredQuantity.Name = "RequiredQuantity";
            // 
            // WastageQuantity
            // 
            this.WastageQuantity.DataPropertyName = "WastageQuantity";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.WastageQuantity.DefaultCellStyle = dataGridViewCellStyle5;
            this.WastageQuantity.HeaderText = "Wastage Qty";
            this.WastageQuantity.MaxInputLength = 15;
            this.WastageQuantity.Name = "WastageQuantity";
            this.WastageQuantity.Width = 125;
            // 
            // UOMID
            // 
            this.UOMID.DataPropertyName = "UOMID";
            this.UOMID.HeaderText = "UOMID";
            this.UOMID.Name = "UOMID";
            this.UOMID.ReadOnly = true;
            this.UOMID.Visible = false;
            // 
            // UOM
            // 
            this.UOM.DataPropertyName = "UOM";
            this.UOM.HeaderText = "UOM";
            this.UOM.Name = "UOM";
            this.UOM.ReadOnly = true;
            this.UOM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // tiMaterial
            // 
            this.tiMaterial.AttachedControl = this.tcpMaterial;
            this.tiMaterial.Name = "tiMaterial";
            this.tiMaterial.Text = "Wastage Details";
            // 
            // pnlBottom
            // 
            this.pnlBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottom.Controls.Add(this.txtRemarks);
            this.pnlBottom.Controls.Add(this.lblRemarks);
            this.pnlBottom.Controls.Add(this.lblIssuedBy);
            this.pnlBottom.Controls.Add(this.cboIncharge);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 368);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(691, 80);
            this.pnlBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottom.Style.GradientAngle = 90;
            this.pnlBottom.TabIndex = 1;
            // 
            // txtRemarks
            // 
            // 
            // 
            // 
            this.txtRemarks.Border.Class = "TextBoxBorder";
            this.txtRemarks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtRemarks.Border.WordWrap = true;
            this.txtRemarks.Location = new System.Drawing.Point(119, 31);
            this.txtRemarks.MaxLength = 2000;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(546, 45);
            this.txtRemarks.TabIndex = 215;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(25, 33);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 216;
            this.lblRemarks.Text = "Remarks";
            // 
            // lblIssuedBy
            // 
            this.lblIssuedBy.AutoSize = true;
            this.lblIssuedBy.BackColor = System.Drawing.Color.Transparent;
            this.lblIssuedBy.Location = new System.Drawing.Point(25, 9);
            this.lblIssuedBy.Name = "lblIssuedBy";
            this.lblIssuedBy.Size = new System.Drawing.Size(49, 13);
            this.lblIssuedBy.TabIndex = 228;
            this.lblIssuedBy.Text = "Incharge";
            // 
            // cboIncharge
            // 
            this.cboIncharge.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboIncharge.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboIncharge.DisplayMember = "Text";
            this.cboIncharge.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboIncharge.DropDownHeight = 75;
            this.cboIncharge.FormattingEnabled = true;
            this.cboIncharge.IntegralHeight = false;
            this.cboIncharge.ItemHeight = 14;
            this.cboIncharge.Location = new System.Drawing.Point(119, 5);
            this.cboIncharge.Name = "cboIncharge";
            this.cboIncharge.Size = new System.Drawing.Size(215, 20);
            this.cboIncharge.TabIndex = 2;
            this.cboIncharge.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboBOMNo_KeyPress);
            // 
            // pnlMainBottom
            // 
            this.pnlMainBottom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlMainBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMainBottom.Controls.Add(this.lblStatus);
            this.pnlMainBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlMainBottom.Location = new System.Drawing.Point(0, 448);
            this.pnlMainBottom.Name = "pnlMainBottom";
            this.pnlMainBottom.Size = new System.Drawing.Size(691, 26);
            this.pnlMainBottom.TabIndex = 293;
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.lblStatus.CloseButtonVisible = false;
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.Image = ((System.Drawing.Image)(resources.GetObject("lblStatus.Image")));
            this.lblStatus.Location = new System.Drawing.Point(0, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.OptionsButtonVisible = false;
            this.lblStatus.Size = new System.Drawing.Size(689, 24);
            this.lblStatus.TabIndex = 20;
            // 
            // expandableSplitterTop
            // 
            this.expandableSplitterTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterTop.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitterTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandableSplitterTop.ExpandableControl = this.tcGeneral;
            this.expandableSplitterTop.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterTop.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterTop.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterTop.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterTop.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterTop.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterTop.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterTop.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterTop.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterTop.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterTop.Location = new System.Drawing.Point(0, 158);
            this.expandableSplitterTop.Name = "expandableSplitterTop";
            this.expandableSplitterTop.Size = new System.Drawing.Size(691, 3);
            this.expandableSplitterTop.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterTop.TabIndex = 3;
            this.expandableSplitterTop.TabStop = false;
            // 
            // tcGeneral
            // 
            this.tcGeneral.BackColor = System.Drawing.Color.Transparent;
            this.tcGeneral.CanReorderTabs = true;
            this.tcGeneral.Controls.Add(this.tabControlPanel2);
            this.tcGeneral.Dock = System.Windows.Forms.DockStyle.Top;
            this.tcGeneral.Location = new System.Drawing.Point(0, 25);
            this.tcGeneral.Name = "tcGeneral";
            this.tcGeneral.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcGeneral.SelectedTabIndex = 0;
            this.tcGeneral.Size = new System.Drawing.Size(691, 133);
            this.tcGeneral.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tcGeneral.TabIndex = 0;
            this.tcGeneral.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tcGeneral.Tabs.Add(this.tiGeneral);
            this.tcGeneral.TabStop = false;
            this.tcGeneral.Text = "tabControl1";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.lblDate);
            this.tabControlPanel2.Controls.Add(this.lblBatchNo);
            this.tabControlPanel2.Controls.Add(this.txtBatchNo);
            this.tabControlPanel2.Controls.Add(this.dtpDate);
            this.tabControlPanel2.Controls.Add(this.lblWarehouse);
            this.tabControlPanel2.Controls.Add(this.cboWarehouse);
            this.tabControlPanel2.Controls.Add(this.txtQuantity);
            this.tabControlPanel2.Controls.Add(this.txtDetails);
            this.tabControlPanel2.Controls.Add(this.lblMaterialIssueDetails);
            this.tabControlPanel2.Controls.Add(this.lblQuantity);
            this.tabControlPanel2.Controls.Add(this.txtProductionNo);
            this.tabControlPanel2.Controls.Add(this.lblProdutionNo);
            this.tabControlPanel2.Controls.Add(this.lblBOMNo);
            this.tabControlPanel2.Controls.Add(this.cboBOMNo);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(691, 111);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 1;
            this.tabControlPanel2.TabItem = this.tiGeneral;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Location = new System.Drawing.Point(220, 8);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 249;
            this.lblDate.Text = "Date";
            // 
            // lblBatchNo
            // 
            this.lblBatchNo.AutoSize = true;
            this.lblBatchNo.BackColor = System.Drawing.Color.Transparent;
            this.lblBatchNo.Location = new System.Drawing.Point(12, 61);
            this.lblBatchNo.Name = "lblBatchNo";
            this.lblBatchNo.Size = new System.Drawing.Size(52, 13);
            this.lblBatchNo.TabIndex = 248;
            this.lblBatchNo.Text = "Batch No";
            // 
            // txtBatchNo
            // 
            this.txtBatchNo.Location = new System.Drawing.Point(87, 58);
            this.txtBatchNo.Name = "txtBatchNo";
            this.txtBatchNo.Size = new System.Drawing.Size(100, 20);
            this.txtBatchNo.TabIndex = 247;
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(272, 6);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(106, 20);
            this.dtpDate.TabIndex = 246;
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.BackColor = System.Drawing.Color.Transparent;
            this.lblWarehouse.Location = new System.Drawing.Point(12, 88);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(62, 13);
            this.lblWarehouse.TabIndex = 244;
            this.lblWarehouse.Text = "Warehouse";
            // 
            // cboWarehouse
            // 
            this.cboWarehouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWarehouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWarehouse.DisplayMember = "Text";
            this.cboWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWarehouse.DropDownHeight = 75;
            this.cboWarehouse.FormattingEnabled = true;
            this.cboWarehouse.IntegralHeight = false;
            this.cboWarehouse.ItemHeight = 14;
            this.cboWarehouse.Location = new System.Drawing.Point(87, 84);
            this.cboWarehouse.Name = "cboWarehouse";
            this.cboWarehouse.Size = new System.Drawing.Size(291, 20);
            this.cboWarehouse.TabIndex = 243;
            this.cboWarehouse.TabStop = false;
            this.cboWarehouse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboBOMNo_KeyPress);
            // 
            // txtQuantity
            // 
            this.txtQuantity.BackColor = System.Drawing.Color.White;
            this.txtQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQuantity.Location = new System.Drawing.Point(272, 58);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.ShortcutsEnabled = false;
            this.txtQuantity.Size = new System.Drawing.Size(106, 20);
            this.txtQuantity.TabIndex = 242;
            this.txtQuantity.TextChanged += new System.EventHandler(this.txtQuantity_TextChanged);
            // 
            // txtDetails
            // 
            this.txtDetails.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDetails.Border.Class = "TextBoxBorder";
            this.txtDetails.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDetails.Border.WordWrap = true;
            this.txtDetails.Location = new System.Drawing.Point(459, 7);
            this.txtDetails.MaxLength = 2000;
            this.txtDetails.Multiline = true;
            this.txtDetails.Name = "txtDetails";
            this.txtDetails.ReadOnly = true;
            this.txtDetails.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDetails.Size = new System.Drawing.Size(220, 97);
            this.txtDetails.TabIndex = 240;
            // 
            // lblMaterialIssueDetails
            // 
            this.lblMaterialIssueDetails.AutoSize = true;
            this.lblMaterialIssueDetails.BackColor = System.Drawing.Color.Transparent;
            this.lblMaterialIssueDetails.Location = new System.Drawing.Point(414, 8);
            this.lblMaterialIssueDetails.Name = "lblMaterialIssueDetails";
            this.lblMaterialIssueDetails.Size = new System.Drawing.Size(39, 13);
            this.lblMaterialIssueDetails.TabIndex = 233;
            this.lblMaterialIssueDetails.Text = "Details";
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.BackColor = System.Drawing.Color.Transparent;
            this.lblQuantity.Location = new System.Drawing.Point(220, 61);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(46, 13);
            this.lblQuantity.TabIndex = 241;
            this.lblQuantity.Text = "Quantity";
            // 
            // txtProductionNo
            // 
            this.txtProductionNo.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtProductionNo.Border.Class = "TextBoxBorder";
            this.txtProductionNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtProductionNo.Location = new System.Drawing.Point(87, 6);
            this.txtProductionNo.MaxLength = 20;
            this.txtProductionNo.Name = "txtProductionNo";
            this.txtProductionNo.Size = new System.Drawing.Size(100, 20);
            this.txtProductionNo.TabIndex = 3;
            this.txtProductionNo.TabStop = false;
            // 
            // lblProdutionNo
            // 
            this.lblProdutionNo.AutoSize = true;
            this.lblProdutionNo.BackColor = System.Drawing.Color.Transparent;
            this.lblProdutionNo.Location = new System.Drawing.Point(12, 9);
            this.lblProdutionNo.Name = "lblProdutionNo";
            this.lblProdutionNo.Size = new System.Drawing.Size(69, 13);
            this.lblProdutionNo.TabIndex = 225;
            this.lblProdutionNo.Text = "Prodution No";
            // 
            // lblBOMNo
            // 
            this.lblBOMNo.AutoSize = true;
            this.lblBOMNo.BackColor = System.Drawing.Color.Transparent;
            this.lblBOMNo.Location = new System.Drawing.Point(12, 34);
            this.lblBOMNo.Name = "lblBOMNo";
            this.lblBOMNo.Size = new System.Drawing.Size(48, 13);
            this.lblBOMNo.TabIndex = 229;
            this.lblBOMNo.Text = "BOM No";
            // 
            // cboBOMNo
            // 
            this.cboBOMNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBOMNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBOMNo.DisplayMember = "Text";
            this.cboBOMNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboBOMNo.DropDownHeight = 75;
            this.cboBOMNo.FormattingEnabled = true;
            this.cboBOMNo.IntegralHeight = false;
            this.cboBOMNo.ItemHeight = 14;
            this.cboBOMNo.Location = new System.Drawing.Point(87, 32);
            this.cboBOMNo.Name = "cboBOMNo";
            this.cboBOMNo.Size = new System.Drawing.Size(291, 20);
            this.cboBOMNo.TabIndex = 0;
            this.cboBOMNo.TabStop = false;
            this.cboBOMNo.SelectedIndexChanged += new System.EventHandler(this.cboBOMNo_SelectedIndexChanged);
            this.cboBOMNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboBOMNo_KeyPress);
            // 
            // tiGeneral
            // 
            this.tiGeneral.AttachedControl = this.tabControlPanel2;
            this.tiGeneral.Name = "tiGeneral";
            this.tiGeneral.Text = "General";
            // 
            // bnMaterialIssue
            // 
            this.bnMaterialIssue.AccessibleDescription = "DotNetBar Bar (bnMaterialIssue)";
            this.bnMaterialIssue.AccessibleName = "DotNetBar Bar";
            this.bnMaterialIssue.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.bnMaterialIssue.Dock = System.Windows.Forms.DockStyle.Top;
            this.bnMaterialIssue.DockLine = 1;
            this.bnMaterialIssue.DockOffset = 73;
            this.bnMaterialIssue.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.bnMaterialIssue.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.bnMaterialIssue.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnAdd,
            this.btnSave,
            this.bnClear,
            this.btnDelete,
            this.btnPrint,
            this.btnEmail});
            this.bnMaterialIssue.Location = new System.Drawing.Point(0, 0);
            this.bnMaterialIssue.Name = "bnMaterialIssue";
            this.bnMaterialIssue.Size = new System.Drawing.Size(691, 25);
            this.bnMaterialIssue.Stretch = true;
            this.bnMaterialIssue.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.bnMaterialIssue.TabIndex = 12;
            this.bnMaterialIssue.TabStop = false;
            this.bnMaterialIssue.Text = "bar2";
            // 
            // btnAdd
            // 
            this.btnAdd.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAdd.Image = global::MyBooksERP.Properties.Resources.Add;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlEnter);
            this.btnAdd.Text = "Add";
            this.btnAdd.Tooltip = "Add New Information";
            this.btnAdd.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // btnSave
            // 
            this.btnSave.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSave.Image = global::MyBooksERP.Properties.Resources.save;
            this.btnSave.Name = "btnSave";
            this.btnSave.Text = "Save";
            this.btnSave.Tooltip = "Save";
            this.btnSave.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnClear
            // 
            this.bnClear.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.bnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.bnClear.Name = "bnClear";
            this.bnClear.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.bnClear.Text = "Clear";
            this.bnClear.Tooltip = "Clear";
            this.bnClear.Visible = false;
            this.bnClear.Click += new System.EventHandler(this.bnClear_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDelete.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Text = "Delete";
            this.btnDelete.Tooltip = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.bnDelete_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BeginGroup = true;
            this.btnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.btnPrint.Text = "Print";
            this.btnPrint.Visible = false;
            // 
            // btnEmail
            // 
            this.btnEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlM);
            this.btnEmail.Text = "Email";
            this.btnEmail.Visible = false;
            // 
            // tmrProcess
            // 
            this.tmrProcess.Interval = 1000;
            this.tmrProcess.Tick += new System.EventHandler(this.tmrProcess_Tick);
            // 
            // errProcess
            // 
            this.errProcess.ContainerControl = this;
            // 
            // frmProductionProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 474);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.expandableSplitterLeft);
            this.Controls.Add(this.PanelLeft);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProductionProcess";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Production Process";
            this.Load += new System.EventHandler(this.frmProductionProcess_Load);
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcessDisplay)).EndInit();
            this.epMaterialIssue.ResumeLayout(false);
            this.epMaterialIssue.PerformLayout();
            this.panelMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcMaterialIssue)).EndInit();
            this.tcMaterialIssue.ResumeLayout(false);
            this.tcpMaterial.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialDetails)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.pnlMainBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcGeneral)).EndInit();
            this.tcGeneral.ResumeLayout(false);
            this.tabControlPanel2.ResumeLayout(false);
            this.tabControlPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnMaterialIssue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProcess)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx PanelLeft;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvProcessDisplay;
        private DevComponents.DotNetBar.ExpandablePanel epMaterialIssue;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSBOMNo;
        private System.Windows.Forms.Label lblSearchBOMNo;
        private System.Windows.Forms.DateTimePicker dtpSTo;
        private System.Windows.Forms.DateTimePicker dtpSFrom;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private DevComponents.DotNetBar.ButtonX btnRefresh;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelX lblSCountStatus;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterLeft;
        private DevComponents.DotNetBar.PanelEx panelMain;
        private DevComponents.DotNetBar.TabControl tcMaterialIssue;
        private System.Windows.Forms.Panel pnlMainBottom;
        private DevComponents.DotNetBar.Controls.WarningBox lblStatus;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterTop;
        private DevComponents.DotNetBar.TabControl tcGeneral;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboIncharge;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboBOMNo;
        private System.Windows.Forms.Label lblBOMNo;
        private System.Windows.Forms.Label lblIssuedBy;
        private DevComponents.DotNetBar.Controls.TextBoxX txtProductionNo;
        private System.Windows.Forms.Label lblProdutionNo;
        private DevComponents.DotNetBar.TabItem tiGeneral;
        private DevComponents.DotNetBar.Bar bnMaterialIssue;
        private DevComponents.DotNetBar.ButtonItem btnAdd;
        private DevComponents.DotNetBar.ButtonItem btnSave;
        private DevComponents.DotNetBar.ButtonItem bnClear;
        private DevComponents.DotNetBar.ButtonItem btnDelete;
        private DevComponents.DotNetBar.ButtonItem btnPrint;
        private DevComponents.DotNetBar.ButtonItem btnEmail;
        private System.Windows.Forms.Label lblMaterialIssueDetails;
        private DevComponents.DotNetBar.PanelEx pnlBottom;
        private DevComponents.DotNetBar.Controls.TextBoxX txtRemarks;
        private System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.Label label4;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDetails;
        private DemoClsDataGridview.DecimalTextBox txtQuantity;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.Timer tmrProcess;
        private System.Windows.Forms.ErrorProvider errProcess;
        private System.Windows.Forms.Label lblWarehouse;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboWarehouse;
        private DevComponents.DotNetBar.TabControlPanel tcpMaterial;
        private DevComponents.DotNetBar.TabItem tiMaterial;
        private ClsInnerGridBar dgvMaterialDetails;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label lblBatchNo;
        private System.Windows.Forms.TextBox txtBatchNo;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductionID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductionNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BOMNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActualQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn RequiredQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn WastageQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOM;
    }
}