﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class frmProductionProcess : Form
    {
        clsBLLCommonUtility mobjClsBllCommonUtility;
        clsBLLProductionProcess MobjclsBLLProductionProcess;
        ClsNotificationNew MobjClsNotification;
        clsBLLMaterialIssueMaster MobjClsBLLMaterialIssueMaster;

        DataTable datMessages;
        MessageBoxIcon MmsgMessageIcon;
        string MstrCommonMessage;
        bool MblnAddPermission, MblnUpdatePermission, MblnDeletePermission, MblnPrintEmailPermission;

        long lngTemplateID = 0, lngMaterialIssueID = 0;
        int intProductID = 0;
        decimal decBOMTotal = 0;
        bool blnIsAutoIssue = false;
        DataTable datBOMDetails = new DataTable();

        public frmProductionProcess()
        {
            InitializeComponent();
            mobjClsBllCommonUtility = new clsBLLCommonUtility();
            MobjclsBLLProductionProcess = new clsBLLProductionProcess();
            MobjClsNotification = new ClsNotificationNew();
            MobjClsBLLMaterialIssueMaster = new clsBLLMaterialIssueMaster();
        }

        private void frmProductionProcess_Load(object sender, EventArgs e)
        {
            LoadMessage();
            SetPermissions();
            LoadCombos(0);
            ClearControls();
        }

        private void LoadMessage()
        {
            datMessages = new DataTable();
            datMessages = MobjClsNotification.FillMessageArray((int)FormID.ProductionProcess, 4);
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Inventory,
                    (Int32)eMenuID.ProductionTemplate, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission,
                    out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void EnableDisableControls()
        {
            btnAdd.Enabled = MblnAddPermission;

            if (txtProductionNo.Tag.ToInt64() == 0)
            {
                btnSave.Enabled = MblnAddPermission;
                btnDelete.Enabled = btnPrint.Enabled = btnEmail.Enabled = false;
                cboBOMNo.Enabled = cboWarehouse.Enabled = true;
            }
            else
            {
                btnSave.Enabled = MblnUpdatePermission;
                btnDelete.Enabled = MblnDeletePermission;
                btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
                cboBOMNo.Enabled = cboWarehouse.Enabled = false;
            }
        }

        private void LoadCombos(int intType)
        {
            DataTable datTemp = new DataTable();

            if (intType == 0 || intType == 1)
            {
                datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "PTM.TemplateID,PTM.Template + ' - ' + PTM.Code AS Template," +
                        "PTM.ProductID,IM.ItemName + ' - ' + IM.Code AS Product,PTM.Total AS TotalCost,PTM.IsAutoIssue", "InvProductionTemplateMaster PTM " +
                        "LEFT JOIN InvItemMaster IM ON PTM.ProductID = IM.ItemID","PTM.CompanyID = " + ClsCommonSettings.LoginCompanyID });

                cboBOMNo.DataSource = datTemp;
                cboBOMNo.ValueMember = "TemplateID";
                cboBOMNo.DisplayMember = "Template";
            }

            if (intType == 0 || intType == 3)
            {
                datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "TemplateID,Template + ' - ' + Code AS Template", "" +
                    "InvProductionTemplateMaster", "" });
                cboSBOMNo.DataSource = datTemp;
                cboSBOMNo.ValueMember = "TemplateID";
                cboSBOMNo.DisplayMember = "Template";
            }

            if (intType == 0 || intType == 4)
            {
                datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "Distinct IW.WarehouseID,IW.WarehouseName", "" +
                        "InvWarehouse IW INNER JOIN InvWarehouseCompanyDetails WC ON IW.WarehouseID=WC.WarehouseID", "" +
                        "IsActive=1 AND WC.CompanyID = " + ClsCommonSettings.LoginCompanyID });
                cboWarehouse.DataSource = datTemp;
                cboWarehouse.ValueMember = "WarehouseID";
                cboWarehouse.DisplayMember = "WarehouseName";
            }

            if (intType == 0 || intType == 5)
            {
                if (txtProductionNo.Tag.ToInt64() == 0)
                    datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "EmployeeID,EmployeeFullName + ' - ' + EmployeeNumber AS Employee", "" +
                            "EmployeeMaster", "WorkStatusID >= 6" });
                else
                    datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "EmployeeID,EmployeeFullName + ' - ' + EmployeeNumber AS Employee", "" +
                            "EmployeeMaster", "" });
                cboIncharge.DataSource = datTemp;
                cboIncharge.ValueMember = "EmployeeID";
                cboIncharge.DisplayMember = "Employee";
            }
        }

        private void ClearControls()
        {            
            txtProductionNo.Tag = 0;
            LoadCombos(0);
            txtDetails.Text = txtRemarks.Text = txtQuantity.Text = txtBatchNo.Text = txtSearch.Text = "";
            cboBOMNo.SelectedIndex = cboIncharge.SelectedIndex = cboSBOMNo.SelectedIndex = cboWarehouse.SelectedIndex = -1;
            dtpDate.Value = dtpSFrom.Value = dtpSTo.Value = ClsCommonSettings.GetServerDate();
            dgvMaterialDetails.Rows.Clear();
            errProcess.Clear();
            tmrProcess.Enabled = false;

            lngTemplateID = 0;
            intProductID = 0;
            blnIsAutoIssue = false;
            datBOMDetails = null;

            txtProductionNo.Text = MobjclsBLLProductionProcess.getProductionProcessNo();
            getSearchProductionProcess();
            EnableDisableControls();
        }

        private void getSearchProductionProcess()
        {
            dgvProcessDisplay.DataSource = MobjclsBLLProductionProcess.getSearchProductionProcess(txtSearch.Text.Trim(),
                dtpSFrom.Value.ToString("dd MMM yyyy").ToDateTime(), dtpSTo.Value.ToString("dd MMM yyyy").ToDateTime(), 
                cboSBOMNo.SelectedValue.ToInt64());
        }

        private void cboBOMNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboBOMNo.SelectedItem != null)
            {
                lngTemplateID = ((System.Data.DataRowView)(cboBOMNo.SelectedItem)).Row.ItemArray[0].ToInt64();
                intProductID = ((System.Data.DataRowView)(cboBOMNo.SelectedItem)).Row.ItemArray[2].ToInt32();
                blnIsAutoIssue = Convert.ToBoolean(((System.Data.DataRowView)(cboBOMNo.SelectedItem)).Row.ItemArray[5]);

                txtDetails.Text = "Product : " + ((System.Data.DataRowView)(cboBOMNo.SelectedItem)).Row.ItemArray[3].ToStringCustom() + Environment.NewLine +
                    "Total Cost : " + ((System.Data.DataRowView)(cboBOMNo.SelectedItem)).Row.ItemArray[4].ToStringCustom();

                datBOMDetails = MobjclsBLLProductionProcess.getBOMDetails(cboBOMNo.SelectedValue.ToInt64());
                dgvMaterialDetails.Rows.Clear();

                for (int iCounter = 0; iCounter <= datBOMDetails.Rows.Count - 1; iCounter++)
                {
                    dgvMaterialDetails.Rows.Add();
                    dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["ItemID"].Value = datBOMDetails.Rows[iCounter]["ItemID"].ToInt32();
                    dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["ItemName"].Value = datBOMDetails.Rows[iCounter]["ItemName"].ToStringCustom();
                    dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["ItemCode"].Value = datBOMDetails.Rows[iCounter]["ItemCode"].ToStringCustom();
                    dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["RequiredQuantity"].Value = datBOMDetails.Rows[iCounter]["Quantity"].ToDecimal();
                    dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["ActualQuantity"].Value = datBOMDetails.Rows[iCounter]["ActualQuantity"].ToDecimal();
                    dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["WastageQuantity"].Value = 0;
                    dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["UOMID"].Value = datBOMDetails.Rows[iCounter]["UOMID"].ToInt32();
                    dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["UOM"].Value = datBOMDetails.Rows[iCounter]["UOM"].ToStringCustom();
                }
            }
        }

        private void bnSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveProductionProcess())
                ClearControls();
        }

        private bool SaveProductionProcess()
        {
            bool blnSave = false;
            long lngID = txtProductionNo.Tag.ToInt64();

            // Checking Reference is Exists or not
            if (lngID > 0)
            {
                DataTable datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "QCID", "invProductionQCMaster", "ProductionID = " + lngID });

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 25, out MmsgMessageIcon);
                        lblStatus.Text = MstrCommonMessage.Split('#').Last();
                        tmrProcess.Enabled = true;
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        return false;
                    }
                }
            }
            // End

            if (FormValidation())
            {                
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, (lngID == 0 ? 1 : 3), out MmsgMessageIcon);

                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information) == DialogResult.No)
                    return false;
                                
                FillParameters();
                blnSave = MobjclsBLLProductionProcess.SaveProductionProcess(FillDetailParameters());

                if (blnIsAutoIssue)
                {
                    FillMaterialIssueParameter();
                    MobjClsBLLMaterialIssueMaster.SaveMaterialIssue();
                }

                if (blnSave)
                {
                    if (lngID == 0)
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 2, out MmsgMessageIcon);
                    else
                        MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 21, out MmsgMessageIcon);

                    lblStatus.Text = MstrCommonMessage.Split('#').Last();
                    tmrProcess.Enabled = true;
                    MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                }
            }
            else
                blnSave = false;

            return blnSave;
        }

        private bool FormValidation()
        {
            errProcess.Clear();
            tmrProcess.Enabled = false;

            if (txtProductionNo.Text == "")
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 24, out MmsgMessageIcon);
                errProcess.SetError(txtProductionNo, MstrCommonMessage.Replace("#", "").Trim());
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Split('#').Last();
                tmrProcess.Enabled = true;
                txtProductionNo.Focus();
                return false;
            }

            if (MobjclsBLLProductionProcess.checkProductionProcessNoDuplication(txtProductionNo.Tag.ToInt64(), txtProductionNo.Text.Trim()))
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 25, out MmsgMessageIcon);
                errProcess.SetError(txtProductionNo, MstrCommonMessage.Replace("#", "").Trim());
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Split('#').Last();
                tmrProcess.Enabled = true;
                txtProductionNo.Focus();
                return false;
            }

            if (cboBOMNo.SelectedIndex == -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 9106, out MmsgMessageIcon);
                errProcess.SetError(cboBOMNo, MstrCommonMessage.Replace("#", "").Trim());
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Split('#').Last();
                tmrProcess.Enabled = true;
                cboBOMNo.Focus();
                return false;
            }

            if (txtBatchNo.Text.Trim() == "")
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 9121, out MmsgMessageIcon);
                MstrCommonMessage = MstrCommonMessage.Replace("select", "enter").Trim();
                errProcess.SetError(txtBatchNo, MstrCommonMessage.Replace("#", "").Trim());
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Split('#').Last();
                tmrProcess.Enabled = true;
                txtBatchNo.Focus();
                return false;
            }

            if (txtQuantity.Text.ToDecimal() == 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 29, out MmsgMessageIcon);
                errProcess.SetError(txtQuantity, MstrCommonMessage.Replace("#", "").Trim());
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Split('#').Last();
                tmrProcess.Enabled = true;
                txtQuantity.Focus();
                return false;
            }

            if (cboWarehouse.SelectedIndex == -1)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 9105, out MmsgMessageIcon);
                errProcess.SetError(cboWarehouse, MstrCommonMessage.Replace("#", "").Trim());
                MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = MstrCommonMessage.Split('#').Last();
                tmrProcess.Enabled = true;
                cboWarehouse.Focus();
                return false;
            }

            return true;
        }

        private void FillMaterialIssueParameter()
        {
            string strMaterialIssueNo = "";

            if (lngMaterialIssueID == 0)
            {
                strMaterialIssueNo = (mobjClsBllCommonUtility.FillCombos(new string[] { "(IsNull(Max(SlNo),0) + 1) as SlNo", "" +
                    "InvMaterialIssueMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID })).Rows[0]["SlNo"].ToString();
                MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueNo = "MI" + strMaterialIssueNo;
            }
            else
            {
                strMaterialIssueNo = (mobjClsBllCommonUtility.FillCombos(new string[] { "MaterialIssueNo", "" +
                    "InvMaterialIssueMaster", "MaterialIssueID = " + lngMaterialIssueID })).Rows[0]["MaterialIssueNo"].ToString();
                MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueNo = strMaterialIssueNo;
            }

            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueDate = dtpDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueID = lngMaterialIssueID;
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.WarehouseID = cboWarehouse.SelectedValue.ToInt32();
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.TemplateID = cboBOMNo.SelectedValue.ToInt64();
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.Quantity = txtQuantity.Text.ToDecimal();
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.CompanyID = ClsCommonSettings.LoginCompanyID;
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.NetAmount = decBOMTotal;
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.NetAmountRounded = Math.Ceiling(decBOMTotal);
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.CreatedBy = ClsCommonSettings.UserID;
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.CreatedDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.Remarks = txtRemarks.Text.Trim();
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.blnAutoMaterialIssue = blnIsAutoIssue;
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.lngProductionID = MobjclsBLLProductionProcess.PobjClsDTOProductionProcess.lngProductionID;
            MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueDetails = new List<clsDTOMaterialIssueDetails>();
            int intSerialNo = 0;

            for (int iCounter = 0; iCounter <= datBOMDetails.Rows.Count - 1; iCounter ++)
            {
                if (datBOMDetails.Rows[iCounter]["ItemID"].ToInt32() > 0)
                {
                    clsDTOMaterialIssueDetails objDTOMaterialDetails = new clsDTOMaterialIssueDetails();
                    objDTOMaterialDetails.SerialNo = ++intSerialNo;
                    objDTOMaterialDetails.ItemID = datBOMDetails.Rows[iCounter]["ItemID"].ToInt32();
                    objDTOMaterialDetails.BatchID = 0;
                    //objDTOMaterialDetails.Quantity = datBOMDetails.Rows[iCounter]["Quantity"].ToDecimal();
                    objDTOMaterialDetails.Quantity = dgvMaterialDetails.Rows[iCounter].Cells["RequiredQuantity"].Value.ToDecimal();
                    objDTOMaterialDetails.ActualQuantity = datBOMDetails.Rows[iCounter]["ActualQuantity"].ToDecimal();
                    objDTOMaterialDetails.UOMID = datBOMDetails.Rows[iCounter]["UOMID"].ToInt32();
                    objDTOMaterialDetails.Rate = datBOMDetails.Rows[iCounter]["Rate"].ToDecimal();
                    objDTOMaterialDetails.NetAmount = datBOMDetails.Rows[iCounter]["ItemTotal"].ToDecimal();
                    MobjClsBLLMaterialIssueMaster.objClsDTOMaterialIssueMaster.MaterialIssueDetails.Add(objDTOMaterialDetails);
                }
            }
        }

        private void FillParameters()
        {
            MobjclsBLLProductionProcess.PobjClsDTOProductionProcess.lngProductionID = txtProductionNo.Tag.ToInt64();
            MobjclsBLLProductionProcess.PobjClsDTOProductionProcess.strProductionNo = txtProductionNo.Text.Trim();
            MobjclsBLLProductionProcess.PobjClsDTOProductionProcess.dtProductionDate = (dtpDate.Value.ToString("dd MMM yyyy")).ToDateTime();
            MobjclsBLLProductionProcess.PobjClsDTOProductionProcess.lngTemplateID = cboBOMNo.SelectedValue.ToInt64();
            MobjclsBLLProductionProcess.PobjClsDTOProductionProcess.strBatchNo = txtBatchNo.Text.Trim();
            MobjclsBLLProductionProcess.PobjClsDTOProductionProcess.intProductID = intProductID;
            MobjclsBLLProductionProcess.PobjClsDTOProductionProcess.decQuantity = txtQuantity.Text.ToDecimal();
            MobjclsBLLProductionProcess.PobjClsDTOProductionProcess.intWarehouseID = cboWarehouse.SelectedValue.ToInt32();
            MobjclsBLLProductionProcess.PobjClsDTOProductionProcess.intInChargeID = cboIncharge.SelectedValue.ToInt32();
            MobjclsBLLProductionProcess.PobjClsDTOProductionProcess.strRemarks = txtRemarks.Text.Trim();
        }

        private DataTable FillDetailParameters()
        {
            DataTable datTemp = new DataTable();
            datTemp.Columns.Add("ItemID");
            datTemp.Columns.Add("UOMID");
            datTemp.Columns.Add("ActualQuantity");
            datTemp.Columns.Add("RequiredQuantity");
            datTemp.Columns.Add("WastageQuantity");

            for (int iCounter = 0; iCounter <= dgvMaterialDetails.Rows.Count - 1; iCounter++)
            {
                datTemp.Rows.Add();
                datTemp.Rows[iCounter]["ItemID"] = dgvMaterialDetails.Rows[iCounter].Cells["ItemID"].Value.ToInt32();
                datTemp.Rows[iCounter]["UOMID"] = dgvMaterialDetails.Rows[iCounter].Cells["UOMID"].Value.ToInt32();
                datTemp.Rows[iCounter]["ActualQuantity"] = dgvMaterialDetails.Rows[iCounter].Cells["ActualQuantity"].Value.ToDecimal();
                datTemp.Rows[iCounter]["RequiredQuantity"] = dgvMaterialDetails.Rows[iCounter].Cells["RequiredQuantity"].Value.ToDecimal();
                datTemp.Rows[iCounter]["WastageQuantity"] = dgvMaterialDetails.Rows[iCounter].Cells["WastageQuantity"].Value.ToDecimal();                
            }

            return datTemp;
        }

        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void bnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void bnDelete_Click(object sender, EventArgs e)
        {
            if (DeleteProductionProcess())
                ClearControls();
        }

        private bool DeleteProductionProcess()
        {
            bool blnDelete = false;
            MobjclsBLLProductionProcess.PobjClsDTOProductionProcess.lngProductionID = txtProductionNo.Tag.ToInt64();

            // Checking Reference is Exists or not
            DataTable datTemp = mobjClsBllCommonUtility.FillCombos(new string[] { "QCID", "invProductionQCMaster", "" +
                    "ProductionID = " + MobjclsBLLProductionProcess.PobjClsDTOProductionProcess.lngProductionID });

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 25, out MmsgMessageIcon);
                    lblStatus.Text = MstrCommonMessage.Split('#').Last();
                    tmrProcess.Enabled = true;
                    MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    return false;
                }
            }
            // End

            if (MobjclsBLLProductionProcess.PobjClsDTOProductionProcess.lngProductionID > 0)
            {
                MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 13, out MmsgMessageIcon);

                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information) == DialogResult.No)
                    return false;

                blnDelete = MobjclsBLLProductionProcess.DeleteProductionProcess();

                if (blnDelete)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 4, out MmsgMessageIcon);
                    lblStatus.Text = MstrCommonMessage.Split('#').Last();
                    tmrProcess.Enabled = true;
                    MessageBox.Show(MstrCommonMessage.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                }
            }

            return blnDelete;
        }

        private void tmrProcess_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            tmrProcess.Enabled = false;
        }

        private void dgvMaterialIssueDisplay_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
                DisplayProductProcess(e.RowIndex);
        }

        private void DisplayProductProcess(int intRowIndex)
        {
            dgvMaterialDetails.Rows.Clear();
            MobjclsBLLProductionProcess.PobjClsDTOProductionProcess.lngProductionID = dgvProcessDisplay.Rows[intRowIndex].Cells["ProductionID"].Value.ToInt64();
            DataSet dsTemp = MobjclsBLLProductionProcess.getProductionProcessDetails();

            if (dsTemp != null)
            {
                if (dsTemp.Tables.Count > 0)
                {
                    DataTable datTemp = dsTemp.Tables[0];
                    DataTable datTempDetails = dsTemp.Tables[1];
                    DataTable datBOMDetails = dsTemp.Tables[2];

                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {                            
                            txtProductionNo.Tag = datTemp.Rows[0]["ProductionID"].ToInt64();
                            txtProductionNo.Text = datTemp.Rows[0]["ProductionNo"].ToStringCustom();
                            dtpDate.Value = datTemp.Rows[0]["ProductionDate"].ToDateTime();
                            LoadCombos(1);
                            cboBOMNo.SelectedValue = datTemp.Rows[0]["TemplateID"].ToInt32();
                            txtBatchNo.Text = datTemp.Rows[0]["BatchNo"].ToStringCustom();
                            txtQuantity.Text = datTemp.Rows[0]["Quantity"].ToStringCustom();
                            LoadCombos(5);
                            cboIncharge.SelectedValue = datTemp.Rows[0]["InChargeID"].ToInt32();
                            cboWarehouse.SelectedValue = datTemp.Rows[0]["WarehouseID"].ToInt32();
                            txtRemarks.Text = datTemp.Rows[0]["Remarks"].ToStringCustom();
                            lngTemplateID = datTemp.Rows[0]["TemplateID"].ToInt32();
                            intProductID = datTemp.Rows[0]["ProductID"].ToInt32();
                            lngMaterialIssueID = datTemp.Rows[0]["MaterialIssueID"].ToInt32();
                        }
                    }

                    dgvMaterialDetails.Rows.Clear();

                    if (datTempDetails != null)
                    {
                        if (datTempDetails.Rows.Count > 0)
                        {
                            for (int iCounter = 0; iCounter <= datTempDetails.Rows.Count - 1; iCounter++)
                            {
                                dgvMaterialDetails.Rows.Add();
                                dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["ItemID"].Value = datTempDetails.Rows[iCounter]["ItemID"].ToInt32();
                                dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["ItemName"].Value = datTempDetails.Rows[iCounter]["ItemName"].ToStringCustom();
                                dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["ItemCode"].Value = datTempDetails.Rows[iCounter]["ItemCode"].ToStringCustom();
                                dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["RequiredQuantity"].Value = datTempDetails.Rows[iCounter]["RequiredQuantity"].ToDecimal();
                                dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["ActualQuantity"].Value = datTempDetails.Rows[iCounter]["ActualQuantity"].ToDecimal();
                                dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["WastageQuantity"].Value = datTempDetails.Rows[iCounter]["WastageQuantity"].ToDecimal();
                                dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["UOMID"].Value = datTempDetails.Rows[iCounter]["UOMID"].ToInt32();
                                dgvMaterialDetails.Rows[dgvMaterialDetails.Rows.Count - 1].Cells["UOM"].Value = datTempDetails.Rows[iCounter]["UOM"].ToStringCustom();
                            }
                        }
                    }
                }

                EnableDisableControls();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            getSearchProductionProcess();
        }

        void EditingControlParticulars_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvMaterialDetails.CurrentCell.ColumnIndex == RequiredQuantity.Index ||
                dgvMaterialDetails.CurrentCell.ColumnIndex == WastageQuantity.Index)
            {
                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                    e.Handled = true;
            }
        }

        private void dgvMaterialDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvMaterialDetails.EditingControl.KeyPress += new KeyPressEventHandler(EditingControlParticulars_KeyPress);
        }

        private void dgvMaterialDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvMaterialDetails.Columns["RequiredQuantity"].Index)
                {
                    int intUomScale = 0;
                    if (dgvMaterialDetails[UOMID.Index, e.RowIndex].Value.ToInt32() != 0)
                        intUomScale = mobjClsBllCommonUtility.FillCombos(new string[] { "Scale", "InvUOMReference", "" +
                            "UOMID = " + dgvMaterialDetails[UOMID.Index, e.RowIndex].Value.ToInt32() }).Rows[0]["Scale"].ToInt32();
                    dgvMaterialDetails.Rows[e.RowIndex].Cells[RequiredQuantity.Index].Value =
                        dgvMaterialDetails.Rows[e.RowIndex].Cells[RequiredQuantity.Index].Value.ToDecimal().ToString("F" + intUomScale);
                }
            }
        }

        private void dgvMaterialDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvMaterialDetails.IsCurrentCellDirty)
            {
                if (dgvMaterialDetails.CurrentCell != null)
                    dgvMaterialDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void SetSerialNo()
        {
            try
            {
                int intRowNumber = 1;
                foreach (DataGridViewRow row in dgvMaterialDetails.Rows)
                {
                    if (row.Index < dgvMaterialDetails.Rows.Count - 1)
                    {
                        row.HeaderCell.Value = intRowNumber.ToString();
                        intRowNumber++;
                    }
                }
            }
            catch { }
        }

        private void dgvMaterialDetails_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            SetSerialNo();
        }

        private void dgvMaterialDetails_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            SetSerialNo();
        }

        private void cboBOMNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            cb.DroppedDown = false;
        }

        private void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            decimal decQty = txtQuantity.Text.ToDecimal();
            decBOMTotal = 0;

            if (decQty > 0)
            {
                for (int iCounter = 0; iCounter <= dgvMaterialDetails.Rows.Count - 1; iCounter++)
                {
                    // Wastage Details
                    dgvMaterialDetails.Rows[iCounter].Cells["RequiredQuantity"].Value = decQty * 
                        dgvMaterialDetails.Rows[iCounter].Cells["ActualQuantity"].Value.ToDecimal();

                    // Material Issue Details
                    datBOMDetails.Rows[iCounter]["Quantity"] = decQty * datBOMDetails.Rows[iCounter]["ActualQuantity"].ToDecimal();

                    decBOMTotal = decBOMTotal + 
                        (datBOMDetails.Rows[iCounter]["Quantity"].ToDecimal() * datBOMDetails.Rows[iCounter]["Rate"].ToDecimal());
                }
            }
        }
    }
}