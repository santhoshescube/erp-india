﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlTypes;

namespace MyBooksERP
{
    public partial class frmGroupJV : DevComponents.DotNetBar.Office2007Form
    {
        clsBLLJournalVoucher MobjclsBLLJournalVoucher;
        ClsNotification mObjNotification;

        private ArrayList MaMessageArr;                 // Error Message display
        private ArrayList MaStatusMessage;
        private string MsMessageCaption;                //  Showdialog Message caption from MDI
        private string MsMessageCommon;                 //  variable for assigning message
        private MessageBoxIcon MmessageIcon;

        bool blnNavigateStatus = false;
        bool MblnAddPermission, MblnAddUpdatePermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission;
        bool blnIsValidated = true;

        int intJVSerialNo = 0;
        DataTable datGroupJVDetails = new DataTable();
        DataTable datLedgerAccounts = new DataTable();
        DataTable datGroupJVDetDetails = new DataTable();
        bool blnIsSelected = false;

        int intDebitRowCount = 0, intCreditRowCount = 0, intDrAccountID = 0, intCrAccountID = 0;
        string strDrCode = "", strCrCode = "", strDrName = "", strCrName = "", strDrRemarks = "", strCrRemarks = "";
        decimal decDrAmount = 0, decCrAmount = 0;

        public frmGroupJV()
        {
            InitializeComponent();
            MobjclsBLLJournalVoucher = new clsBLLJournalVoucher();
            mObjNotification = new ClsNotification();
        }

        private void frmGroupJV_Load(object sender, EventArgs e)
        {
            tmGeneralReceiptsAndPayments.Enabled = false;
            LoadMessage();
            LoadCombos();
            SetPermissions();
            clearControls();
        }

        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = mObjNotification.FillMessageArray((int)FormID.GeneralReceiptsAndPayments, 4);
            MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.GeneralReceiptsAndPayments, 4);
            MsMessageCaption = ClsCommonSettings.MessageCaption;
        }

        private void LoadCombos()
        {
            cboCompany.DataSource = MobjclsBLLJournalVoucher.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" +
                "CompanyID = " + ClsCommonSettings.LoginCompanyID + " ORDER BY CompanyName" });
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DisplayMember = "CompanyName";
            cboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;

            CboSearchCompany.DataSource = MobjclsBLLJournalVoucher.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" +
                "CompanyID = " + ClsCommonSettings.LoginCompanyID + " ORDER BY CompanyName" });
            CboSearchCompany.ValueMember = "CompanyID";
            CboSearchCompany.DisplayMember = "CompanyName";
            CboSearchCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;

            datLedgerAccounts = MobjclsBLLJournalVoucher.FillCombos(new string[] { "AccountID AS LedgerAccountID,Code AS LedgerCode," +
                "AccountName AS LedgerName", "AccAccountMaster", "" +
                "ISNULL(AccountGroupID,0) NOT IN (0," + (int)AccountGroups.Stockinhand + ") AND AccountID NOT IN (4,5) ORDER BY AccountName"});
            dgvLedgerAccounts.DataSource = datLedgerAccounts.DefaultView.ToTable();
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 2)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Accounts, (int)eMenuID.JournalVoucher, 
                    out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                if (MblnAddPermission == true || MblnUpdatePermission == true)
                    MblnAddUpdatePermission = true;
            }
            else
                MblnAddPermission = MblnAddUpdatePermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void clearControls()
        {
            cboCompany.Enabled = dtpDate.Enabled = true;
            dtpDate.Value = ClsCommonSettings.GetServerDate();
            txtNarration.Text = "";
            getVocherNo();
            lblSelectedAccountBalance.Text = txtAddTotalDebit.Text = txtAddTotalCredit.Text = txtDebitTotal.Text = txtCreditTotal.Text = "0";
            cboCompany.Tag = 0;
            dgvAddAccounts.Rows.Clear();
            dgvJournalVoucher.Rows.Clear();
            dgvGroupJV.Rows.Clear();
            getVoucherNosForSearch();
            GetRecordCountwithValues();
            btnAddNewItem.Enabled = MblnAddPermission;
            btnSaveItem.Enabled = false;
            btnDeleteItem.Enabled = btnPrint.Enabled = btnEmail.Enabled = false;
            dgvJournalVoucher.ReadOnly = false;
            dgvGroupJV.ReadOnly = false;
            MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID = 0;
            txtVoucherNo.Tag = 0;

            pnlLedgerAccounts.SendToBack();
            pnlLedgerAccounts.Visible = false;

            intDebitRowCount = intCreditRowCount = intDrAccountID = intCrAccountID = 0;
            strDrCode = strCrCode = strDrName = strCrName = strDrRemarks = strCrRemarks = "";
            decDrAmount = decCrAmount = 0;
        }

        private void getVocherNo()
        {
            txtVoucherNo.Text = MobjclsBLLJournalVoucher.GetGroupJVNo();
        }

        private void getVoucherNosForSearch()
        {
            DataTable datTemp = new DataTable();
            if (dtpSFrom.Checked == true && dtpSTo.Checked == true)
                datTemp = MobjclsBLLJournalVoucher.getGroupJVNos(TxtSsearch.Text, dtpSFrom.Value.ToString("dd MMM yyyy"), dtpSTo.Value.ToString("dd MMM yyyy"));
            else if (dtpSFrom.Checked == false && dtpSTo.Checked == false)
                datTemp = MobjclsBLLJournalVoucher.getGroupJVNos(TxtSsearch.Text, "", "");
            else if (dtpSFrom.Checked == true && dtpSTo.Checked == false)
                datTemp = MobjclsBLLJournalVoucher.getGroupJVNos(TxtSsearch.Text, dtpSFrom.Value.ToString("dd MMM yyyy"), "");
            else if (dtpSFrom.Checked == false && dtpSTo.Checked == true)
                datTemp = MobjclsBLLJournalVoucher.getGroupJVNos(TxtSsearch.Text, "", dtpSTo.Value.ToString("dd MMM yyyy"));

            dgvVNoDisplay.DataSource = datTemp;
            dgvVNoDisplay.Columns["VoucherID"].Visible = false;
        }

        private void GetRecordCountwithValues()
        {
            LblSCountStatus.Text = dgvVNoDisplay == null ? "" : dgvVNoDisplay.Rows.Count.ToStringCustom() == "0" ? "" : dgvVNoDisplay.Rows.Count.ToStringCustom();
        }

        private void BtnSRefresh_Click(object sender, EventArgs e)
        {
            getVoucherNosForSearch();
            GetRecordCountwithValues();
        }

        private void dgvVNoDisplay_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtVoucherNo.Tag = dgvVNoDisplay.Rows[e.RowIndex].Cells["VoucherID"].Value.ToDecimal();
                DisplayJournalVoucher();
            }
            catch { }
        }

        private void DisplayJournalVoucher()
        {
            try
            {
                if (MobjclsBLLJournalVoucher.DisplayGroupJV(txtVoucherNo.Tag.ToDecimal()))
                {
                    txtVoucherNo.Tag = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID;
                    cboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;
                    cboCompany.Enabled = false;
                    dtpDate.Value = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dtpVoucherDate;
                    txtVoucherNo.Text = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.strVoucherNo;
                    txtNarration.Text = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.strRemarks;

                    if (MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID > 0)
                    {
                        datGroupJVDetails = MobjclsBLLJournalVoucher.DisplayGroupJVDetails(MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID);
                        datGroupJVDetDetails = MobjclsBLLJournalVoucher.DisplayGroupJVDetDetails(MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID);

                        try { dgvAddAccounts.Rows.Clear(); }
                        catch { }
                        try { dgvJournalVoucher.Rows.Clear(); }
                        catch { }
                        try { dgvGroupJV.Rows.Clear(); }
                        catch { }

                        for (int iCounter = 0; iCounter <= datGroupJVDetDetails.Rows.Count - 1; iCounter++)
                        {
                            dgvGroupJV.Rows.Add();
                            dgvGroupJV.Rows[iCounter].Cells["GJVAccountID"].Value = datGroupJVDetDetails.Rows[iCounter]["AccountID"].ToInt32();
                            dgvGroupJV.Rows[iCounter].Cells["GJVCode"].Value = datGroupJVDetDetails.Rows[iCounter]["Code"].ToStringCustom();
                            dgvGroupJV.Rows[iCounter].Cells["GJVAccountName"].Value = datGroupJVDetDetails.Rows[iCounter]["AccountName"].ToStringCustom();
                            dgvGroupJV.Rows[iCounter].Cells["GJVSerialNo"].Value = datGroupJVDetDetails.Rows[iCounter]["SerialNo"].ToInt32();
                            dgvGroupJV.Rows[iCounter].Cells["GJVRemarks"].Value = datGroupJVDetDetails.Rows[iCounter]["Remarks"].ToStringCustom();
                            dgvGroupJV.Rows[iCounter].Cells["GJVDebitAmount"].Value = null;
                            dgvGroupJV.Rows[iCounter].Cells["GJVCreditAmount"].Value = null;

                            if (datGroupJVDetDetails.Rows[iCounter]["DebitAmount"].ToDecimal() > 0)
                                dgvGroupJV.Rows[iCounter].Cells["GJVDebitAmount"].Value = datGroupJVDetDetails.Rows[iCounter]["DebitAmount"].ToDecimal();

                            if (datGroupJVDetDetails.Rows[iCounter]["CreditAmount"].ToDecimal() > 0)
                                dgvGroupJV.Rows[iCounter].Cells["GJVCreditAmount"].Value = datGroupJVDetDetails.Rows[iCounter]["CreditAmount"].ToDecimal();

                            intJVSerialNo = datGroupJVDetDetails.Rows[iCounter]["JVSerialNo"].ToInt32();
                            dgvGroupJV.Rows[iCounter].Cells["GJVJVSerialNo"].Value = intJVSerialNo;
                            dgvGroupJV.Rows[iCounter].Cells["GJVVoucherID"].Value = datGroupJVDetDetails.Rows[iCounter]["VoucherID"].ToDecimal();
                        }

                        for (int iCounter = 0; iCounter <= datGroupJVDetails.Rows.Count - 1; iCounter++)
                        {
                            dgvJournalVoucher.Rows.Add();
                            dgvJournalVoucher.Rows[iCounter].Cells["AccountID"].Value = datGroupJVDetails.Rows[iCounter]["AccountID"].ToInt32();
                            dgvJournalVoucher.Rows[iCounter].Cells["Code"].Value = datGroupJVDetails.Rows[iCounter]["Code"].ToStringCustom();
                            dgvJournalVoucher.Rows[iCounter].Cells["AccountName"].Value = datGroupJVDetails.Rows[iCounter]["AccountName"].ToStringCustom();
                            dgvJournalVoucher.Rows[iCounter].Cells["SerialNo"].Value = datGroupJVDetails.Rows[iCounter]["SerialNo"].ToInt32();
                            dgvJournalVoucher.Rows[iCounter].Cells["Remarks"].Value = datGroupJVDetails.Rows[iCounter]["Remarks"].ToStringCustom();
                            dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value = null;
                            dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value = null;

                            if (datGroupJVDetails.Rows[iCounter]["DebitAmount"].ToDecimal() > 0)
                                dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value = datGroupJVDetails.Rows[iCounter]["DebitAmount"].ToDecimal();

                            if (datGroupJVDetails.Rows[iCounter]["CreditAmount"].ToDecimal() > 0)
                                dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value = datGroupJVDetails.Rows[iCounter]["CreditAmount"].ToDecimal();

                            intJVSerialNo = datGroupJVDetails.Rows[iCounter]["JVSerialNo"].ToInt32();
                            dgvJournalVoucher.Rows[iCounter].Cells["JVSerialNo"].Value = intJVSerialNo;
                            dgvJournalVoucher.Rows[iCounter].Cells["EditVoucherID"].Value = datGroupJVDetails.Rows[iCounter]["VoucherID"].ToDecimal();
                        }

                        CalcTotal();
                    }

                    dgvJournalVoucher.ReadOnly = false;
                    dgvGroupJV.ReadOnly = false;
                    btnSaveItem.Enabled = MblnAddUpdatePermission;
                    btnDeleteItem.Enabled = MblnDeletePermission;
                    btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
                }
            }
            catch { }
        }

        private void CalcTotal()
        {
            txtDebitTotal.Text = txtCreditTotal.Text = "0";

            for (int iCounter = 0; iCounter <= dgvGroupJV.Rows.Count - 1; iCounter++)
            {
                if (dgvGroupJV.Rows[iCounter].Cells["GJVDebitAmount"].Value != null)
                    txtDebitTotal.Text = (txtDebitTotal.Text.ToDecimal() + (dgvGroupJV.Rows[iCounter].Cells["GJVDebitAmount"].Value.ToDecimal())).ToString();
                else if (dgvGroupJV.Rows[iCounter].Cells["GJVCreditAmount"].Value != null)
                    txtCreditTotal.Text = (txtCreditTotal.Text.ToDecimal() + (dgvGroupJV.Rows[iCounter].Cells["GJVCreditAmount"].Value.ToDecimal())).ToString();
            }
        }

        private void btnAddNewItem_Click(object sender, EventArgs e)
        {
            tmGeneralReceiptsAndPayments.Enabled = false;
            clearControls();
        }

        private void tmGeneralReceiptsAndPayments_Tick(object sender, EventArgs e)
        {
            tslStatus.Text = string.Empty;
            tmGeneralReceiptsAndPayments.Enabled = false;
        }

        private void btnChartofAccounts_Click(object sender, EventArgs e)
        {
            using (FrmChartOfAccounts objFrmChartOfAccounts = new FrmChartOfAccounts())
                objFrmChartOfAccounts.ShowDialog();
        }

        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            if (FormValidation())
            {
                bool blnTempSave = false; // want to save or not.
                bool blnTempUpdate = false; // want to save or not.

                if (MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID.ToDecimal() == 0)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        blnTempSave = true;
                }
                else
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        blnTempSave = true;
                }

                if (blnTempSave == true || blnTempUpdate == true)
                {
                    // for Save and Update Voucher
                    MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dtpVoucherDate = dtpDate.Value.ToString("dd MMM yyyy").ToDateTime();
                    MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.strRemarks = txtNarration.Text;

                    int itemCount = dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["JVSerialNo"].Value.ToInt32();
                    int iterator = 1;
                    int intCounter = 0;
                    int intTempJVSerialNo = 0;

                    while (itemCount >= iterator)
                    {
                        for (int iCounter = intCounter; iCounter <= dgvJournalVoucher.Rows.Count - 1; iCounter++)
                        {
                            intTempJVSerialNo = dgvJournalVoucher.Rows[intCounter].Cells["JVSerialNo"].Value.ToInt32();

                            if (intTempJVSerialNo == dgvJournalVoucher.Rows[iCounter].Cells["JVSerialNo"].Value.ToInt32())
                            {
                                if (dgvJournalVoucher.Rows[iCounter].Cells["EditVoucherID"].Value.ToDecimal() > 0)
                                {
                                    MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID =
                                        (SqlDecimal)dgvJournalVoucher.Rows[iCounter].Cells["EditVoucherID"].Value.ToDecimal();
                                    MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.strVoucherNo = MobjclsBLLJournalVoucher.getMainVoucherNo();
                                }
                                else
                                    MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID = 0;

                                break;
                            }
                        }

                        DataTable datJVDetails = new DataTable();
                        datJVDetails.Columns.Add("AccountID");
                        datJVDetails.Columns.Add("JVSerialNo");
                        datJVDetails.Columns.Add("SerialNo");
                        datJVDetails.Columns.Add("DebitAmount");
                        datJVDetails.Columns.Add("CreditAmount");
                        datJVDetails.Columns.Add("Remarks");

                        for (int jCounter = 0; jCounter <= dgvJournalVoucher.Rows.Count - 1; jCounter++)
                        {
                            if (intTempJVSerialNo == dgvJournalVoucher.Rows[jCounter].Cells["JVSerialNo"].Value.ToInt32())
                            {
                                datJVDetails.Rows.Add();
                                datJVDetails.Rows[datJVDetails.Rows.Count - 1]["AccountID"] = dgvJournalVoucher.Rows[jCounter].Cells["AccountID"].Value;
                                datJVDetails.Rows[datJVDetails.Rows.Count - 1]["JVSerialNo"] = dgvJournalVoucher.Rows[jCounter].Cells["JVSerialNo"].Value;
                                datJVDetails.Rows[datJVDetails.Rows.Count - 1]["SerialNo"] = dgvJournalVoucher.Rows[jCounter].Cells["SerialNo"].Value;
                                datJVDetails.Rows[datJVDetails.Rows.Count - 1]["DebitAmount"] = dgvJournalVoucher.Rows[jCounter].Cells["DebitAmount"].Value;
                                datJVDetails.Rows[datJVDetails.Rows.Count - 1]["CreditAmount"] = dgvJournalVoucher.Rows[jCounter].Cells["CreditAmount"].Value;
                                datJVDetails.Rows[datJVDetails.Rows.Count - 1]["Remarks"] = dgvJournalVoucher.Rows[jCounter].Cells["Remarks"].Value;

                                intCounter = jCounter + 1;
                            }
                        }

                        MobjclsBLLJournalVoucher.SaveVoucherMaster(datJVDetails);

                        for (int kCounter = 0; kCounter <= dgvJournalVoucher.Rows.Count - 1; kCounter++)
                        {
                            if (intTempJVSerialNo == dgvJournalVoucher.Rows[kCounter].Cells["JVSerialNo"].Value.ToInt32())
                                dgvJournalVoucher.Rows[kCounter].Cells["EditVoucherID"].Value =
                                    MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID;
                        }

                        iterator++;
                    }
                    // End

                    // For Save or Update Group JV                
                    if (txtVoucherNo.Tag.ToDecimal() > 0)
                        MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID = (SqlDecimal)txtVoucherNo.Tag;
                    else
                        MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID = 0;

                    MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.strVoucherNo = txtVoucherNo.Text;
                    MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.strRemarks = txtNarration.Text;

                    DataTable datDetails = new DataTable();
                    datDetails.Columns.Add("JVSerialNo");
                    datDetails.Columns.Add("AccountID");
                    datDetails.Columns.Add("SerialNo");
                    datDetails.Columns.Add("DebitAmount");
                    datDetails.Columns.Add("CreditAmount");
                    datDetails.Columns.Add("Remarks");
                    datDetails.Columns.Add("VoucherID");

                    for (int iCounter = 0; iCounter <= dgvJournalVoucher.Rows.Count - 1; iCounter++)
                    {
                        datDetails.Rows.Add();
                        datDetails.Rows[datDetails.Rows.Count - 1]["JVSerialNo"] = dgvJournalVoucher.Rows[iCounter].Cells["JVSerialNo"].Value;
                        datDetails.Rows[datDetails.Rows.Count - 1]["AccountID"] = dgvJournalVoucher.Rows[iCounter].Cells["AccountID"].Value;
                        datDetails.Rows[datDetails.Rows.Count - 1]["SerialNo"] = dgvJournalVoucher.Rows[iCounter].Cells["SerialNo"].Value;
                        datDetails.Rows[datDetails.Rows.Count - 1]["DebitAmount"] = dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value;
                        datDetails.Rows[datDetails.Rows.Count - 1]["CreditAmount"] = dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value;
                        datDetails.Rows[datDetails.Rows.Count - 1]["Remarks"] = dgvJournalVoucher.Rows[iCounter].Cells["Remarks"].Value;
                        datDetails.Rows[datDetails.Rows.Count - 1]["VoucherID"] = dgvJournalVoucher.Rows[iCounter].Cells["EditVoucherID"].Value;
                    }

                    DataTable datGJVDetails = new DataTable();
                    datGJVDetails.Columns.Add("JVSerialNo");
                    datGJVDetails.Columns.Add("AccountID");
                    datGJVDetails.Columns.Add("SerialNo");
                    datGJVDetails.Columns.Add("DebitAmount");
                    datGJVDetails.Columns.Add("CreditAmount");
                    datGJVDetails.Columns.Add("Remarks");
                    datGJVDetails.Columns.Add("VoucherID");

                    for (int iCounter = 0; iCounter <= dgvGroupJV.Rows.Count - 1; iCounter++)
                    {
                        datGJVDetails.Rows.Add();
                        datGJVDetails.Rows[datGJVDetails.Rows.Count - 1]["JVSerialNo"] = dgvGroupJV.Rows[iCounter].Cells["GJVJVSerialNo"].Value;
                        datGJVDetails.Rows[datGJVDetails.Rows.Count - 1]["AccountID"] = dgvGroupJV.Rows[iCounter].Cells["GJVAccountID"].Value;
                        datGJVDetails.Rows[datGJVDetails.Rows.Count - 1]["SerialNo"] = dgvGroupJV.Rows[iCounter].Cells["GJVSerialNo"].Value;
                        datGJVDetails.Rows[datGJVDetails.Rows.Count - 1]["DebitAmount"] = dgvGroupJV.Rows[iCounter].Cells["GJVDebitAmount"].Value;
                        datGJVDetails.Rows[datGJVDetails.Rows.Count - 1]["CreditAmount"] = dgvGroupJV.Rows[iCounter].Cells["GJVCreditAmount"].Value;
                        datGJVDetails.Rows[datGJVDetails.Rows.Count - 1]["Remarks"] = dgvGroupJV.Rows[iCounter].Cells["GJVRemarks"].Value;
                        datGJVDetails.Rows[datGJVDetails.Rows.Count - 1]["VoucherID"] = dgvGroupJV.Rows[iCounter].Cells["GJVVoucherID"].Value;
                    }

                    MobjclsBLLJournalVoucher.SaveGroupJVMaster(datDetails, datGJVDetails);
                    // End

                    // Delete Removed Vouchers
                    if (txtVoucherNo.Tag.ToDecimal() > 0)
                    {
                        itemCount = datGroupJVDetails.Rows[datGroupJVDetails.Rows.Count - 1]["JVSerialNo"].ToInt32();
                        iterator = 1;
                        intCounter = 0;
                        intTempJVSerialNo = 0;

                        while (itemCount >= iterator)
                        {
                            for (int iCounter = intCounter; iCounter <= datGroupJVDetails.Rows.Count - 1; iCounter++)
                            {
                                intTempJVSerialNo = datGroupJVDetails.Rows[intCounter]["JVSerialNo"].ToInt32();

                                if (intTempJVSerialNo == datGroupJVDetails.Rows[iCounter]["JVSerialNo"].ToInt32())
                                {
                                    if (datGroupJVDetails.Rows[iCounter]["VoucherID"].ToDecimal() > 0)
                                    {
                                        MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID =
                                            (SqlDecimal)datGroupJVDetails.Rows[iCounter]["VoucherID"].ToDecimal();
                                    }

                                    break;
                                }
                            }

                            for (int jCounter = 0; jCounter <= datGroupJVDetails.Rows.Count - 1; jCounter++)
                            {
                                if (intTempJVSerialNo == datGroupJVDetails.Rows[jCounter]["JVSerialNo"].ToInt32())
                                    intCounter = jCounter + 1;
                            }

                            bool blnVIDExists = false;

                            for (int kCounter = 0; kCounter <= dgvJournalVoucher.Rows.Count - 1; kCounter++)
                            {
                                if (MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID ==
                                    (SqlDecimal)dgvJournalVoucher.Rows[kCounter].Cells["EditVoucherID"].Value.ToDecimal())
                                    blnVIDExists = true;
                            }

                            if (!blnVIDExists)
                                MobjclsBLLJournalVoucher.DeleteJournalVoucher();

                            iterator++;
                        }
                    }
                    // End

                    if (blnTempSave == true)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                        tslStatus.Text = MsMessageCommon.Split('#').Last();
                        tmGeneralReceiptsAndPayments.Enabled = true;
                        MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon); // Saved Successfully
                    }
                    else if (blnTempUpdate == true)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);
                        tslStatus.Text = MsMessageCommon.Split('#').Last();
                        tmGeneralReceiptsAndPayments.Enabled = true;
                        MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon); // Saved Successfully
                    }

                    clearControls();
                }
            }
        }

        private bool FormValidation()
        {
            if (dtpDate.Value.Date > ClsCommonSettings.GetServerDate())
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9006, out MmessageIcon);
                tslStatus.Text = MsMessageCommon.Split('#').Last();
                tmGeneralReceiptsAndPayments.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                blnIsValidated = false;
                return false;
            }

            if (cboCompany.SelectedIndex == -1)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 14, out MmessageIcon);
                tslStatus.Text = MsMessageCommon.Split('#').Last();
                tmGeneralReceiptsAndPayments.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                blnIsValidated = false;
                return false;
            }

            if (txtVoucherNo.Text == "")
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 102, out MmessageIcon);
                tslStatus.Text = MsMessageCommon.Split('#').Last();
                tmGeneralReceiptsAndPayments.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                blnIsValidated = false;
                return false;
            }

            if (txtVoucherNo.Tag.ToDecimal() == 0 && txtVoucherNo.Text != "")
            {
                DataTable datVouchersNos = MobjclsBLLJournalVoucher.getGroupJVNos(txtVoucherNo.Text);
                if (datVouchersNos != null)
                {
                    if (datVouchersNos.Rows.Count > 0)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 109, out MmessageIcon);
                        tslStatus.Text = MsMessageCommon.Split('#').Last();
                        tmGeneralReceiptsAndPayments.Enabled = true;
                        MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        blnIsValidated = false;
                        return false;
                    }
                }
            }

            if (dgvJournalVoucher.Rows.Count == 0)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 103, out MmessageIcon);
                tslStatus.Text = MsMessageCommon.Split('#').Last();
                tmGeneralReceiptsAndPayments.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                blnIsValidated = false;
                return false;
            }

            for (int iCounter = 0; iCounter <= dgvJournalVoucher.Rows.Count - 1; iCounter++)
            {
                if (dgvJournalVoucher.Rows[iCounter].Cells["AccountID"].Value == null)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 104, out MmessageIcon);
                    tslStatus.Text = MsMessageCommon.Split('#').Last();
                    tmGeneralReceiptsAndPayments.Enabled = true;
                    MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    blnIsValidated = false;
                    return false;
                }

                if (dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value.ToDouble() == 0 &&
                    dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value.ToDouble() == 0)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 105, out MmessageIcon);
                    tslStatus.Text = MsMessageCommon.Split('#').Last();
                    tmGeneralReceiptsAndPayments.Enabled = true;
                    MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    blnIsValidated = false;
                    return false;
                }
            }

            if (txtDebitTotal.Text.ToDecimal() != txtCreditTotal.Text.ToDecimal())
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 105, out MmessageIcon);
                tslStatus.Text = MsMessageCommon.Split('#').Last();
                tmGeneralReceiptsAndPayments.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                blnIsValidated = false;
                return false;
            }

            blnIsValidated = true;
            return true;
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
            if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                int itemCount = dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["JVSerialNo"].Value.ToInt32();
                int iterator = 1;
                int intCounter = 0;
                int intTempJVSerialNo = 0;

                while (itemCount >= iterator)
                {
                    for (int iCounter = intCounter; iCounter <= datGroupJVDetails.Rows.Count - 1; iCounter++)
                    {
                        intTempJVSerialNo = datGroupJVDetails.Rows[intCounter]["JVSerialNo"].ToInt32();

                        if (intTempJVSerialNo == datGroupJVDetails.Rows[iCounter]["JVSerialNo"].ToInt32())
                        {
                            if (datGroupJVDetails.Rows[iCounter]["VoucherID"].ToDecimal() > 0)
                            {
                                MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID =
                                    (SqlDecimal)datGroupJVDetails.Rows[iCounter]["VoucherID"].ToDecimal();
                            }

                            break;
                        }
                    }

                    for (int jCounter = 0; jCounter <= datGroupJVDetails.Rows.Count - 1; jCounter++)
                    {
                        if (intTempJVSerialNo == datGroupJVDetails.Rows[jCounter]["JVSerialNo"].ToInt32())
                            intCounter = jCounter + 1;
                    }

                    MobjclsBLLJournalVoucher.DeleteJournalVoucher();

                    iterator++;
                }

                MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID = (SqlDecimal)txtVoucherNo.Tag;

                if (MobjclsBLLJournalVoucher.DeleteGroupJV() == true)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                    tslStatus.Text = MsMessageCommon.Split('#').Last();
                    tmGeneralReceiptsAndPayments.Enabled = true;
                    MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    clearControls();
                }
            }
        }

        private void dgvAddAccounts_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if ((e.ColumnIndex == dgvAddAccounts.Columns["AddCode"].Index || e.ColumnIndex == dgvAddAccounts.Columns["AddAccountName"].Index) &&
                    blnIsSelected == false)
                {
                    dgvLedgerAccounts.Visible = true;
                    pnlLedgerAccounts.BringToFront();
                    pnlLedgerAccounts.Visible = true;
                    SetPanel(e.RowIndex, e.ColumnIndex);

                    if (e.ColumnIndex == dgvAddAccounts.Columns["AddCode"].Index)
                    {
                        try { datLedgerAccounts.DefaultView.RowFilter = "LedgerCode like '%" + dgvAddAccounts.Rows[e.RowIndex].Cells["AddCode"].Value.ToStringCustom() + "%'"; }
                        catch { }                        
                    }
                    else if (e.ColumnIndex == dgvAddAccounts.Columns["AddAccountName"].Index)
                    {
                        try { datLedgerAccounts.DefaultView.RowFilter = "LedgerName like '%" + dgvAddAccounts.Rows[e.RowIndex].Cells["AddAccountName"].Value.ToStringCustom() + "%'"; }
                        catch { }
                    }

                    dgvLedgerAccounts.DataSource = datLedgerAccounts.DefaultView.ToTable();
                }
                else if (blnIsSelected == false)
                {
                    pnlLedgerAccounts.SendToBack();
                    pnlLedgerAccounts.Visible = false;
                }               

                ////Account Duplication
                //if (e.ColumnIndex == dgvAddAccounts.Columns["AddAccountID"].Index)
                //{
                //    for (int iCounter = 0; iCounter <= dgvAddAccounts.Rows.Count - 2; iCounter++)
                //    {
                //        if (iCounter != e.RowIndex && dgvAddAccounts.Rows[e.RowIndex].Cells["AddAccountID"].Value != null && 
                //            dgvAddAccounts.Rows[iCounter].Cells["AddAccountID"].Value.ToInt32() == dgvAddAccounts.Rows[e.RowIndex].Cells["AddAccountID"].Value.ToInt32())
                //        {
                //            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 110, out MmessageIcon);
                //            tslStatus.Text = MsMessageCommon.Split('#').Last();
                //            tmGeneralReceiptsAndPayments.Enabled = true;
                //            MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //            dgvAddAccounts.Rows[e.RowIndex].Cells["AddAccountID"].Value = null;
                //        }
                //    }
                //}

                if (e.ColumnIndex == dgvAddAccounts.Columns["AddAccountID"].Index)
                {
                    if (e.RowIndex > 0)
                    {
                        if ((txtAddTotalDebit.Text.ToDecimal() - txtAddTotalCredit.Text.ToDecimal()) > 0)
                            dgvAddAccounts.Rows[e.RowIndex].Cells["AddCreditAmount"].Value = 
                                Math.Abs(txtAddTotalDebit.Text.ToDecimal() - txtAddTotalCredit.Text.ToDecimal());
                        else
                            dgvAddAccounts.Rows[e.RowIndex].Cells["AddDebitAmount"].Value =
                                Math.Abs(txtAddTotalDebit.Text.ToDecimal() - txtAddTotalCredit.Text.ToDecimal());
                    }
                }
                else if ((e.ColumnIndex == dgvAddAccounts.Columns["AddDebitAmount"].Index ||
                    e.ColumnIndex == dgvAddAccounts.Columns["AddCreditAmount"].Index))
                {
                    if (dgvAddAccounts.Rows[e.RowIndex].Cells["AddDebitAmount"].Value.ToDecimal() > 0)
                        dgvAddAccounts.Rows[e.RowIndex].Cells["AddCreditAmount"].Value = null;
                    else if (dgvAddAccounts.Rows[e.RowIndex].Cells["AddCreditAmount"].Value.ToDecimal() > 0)
                        dgvAddAccounts.Rows[e.RowIndex].Cells["AddDebitAmount"].Value = null;
                }

                CalcJVTotal();

                if (e.ColumnIndex == dgvAddAccounts.Columns["AddDebitAmount"].Index)
                {
                    if (dgvAddAccounts.Rows[e.RowIndex].Cells["AddDebitAmount"].Value.ToDecimal() > 0 && 
                        txtDebitTotal.Text.ToDecimal() > txtCreditTotal.Text.ToDecimal())
                    {
                        if (dgvAddAccounts.Rows[0].Cells["AddCreditAmount"].Value.ToDecimal() > 0)
                            dgvAddAccounts.Rows[0].Cells["AddCreditAmount"].Value = dgvAddAccounts.Rows[0].Cells["AddCreditAmount"].Value.ToDecimal() + 
                                (Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal()));
                    }
                }
                else if (e.ColumnIndex == dgvAddAccounts.Columns["AddCreditAmount"].Index)
                {
                    if (dgvAddAccounts.Rows[e.RowIndex].Cells["AddCreditAmount"].Value.ToDecimal() > 0 && 
                        txtDebitTotal.Text.ToDecimal() < txtCreditTotal.Text.ToDecimal())
                    {
                        if (dgvAddAccounts.Rows[0].Cells["AddDebitAmount"].Value.ToDecimal() > 0)
                            dgvAddAccounts.Rows[0].Cells["AddDebitAmount"].Value = dgvAddAccounts.Rows[0].Cells["AddDebitAmount"].Value.ToDecimal() + 
                                (Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal()));
                    }
                }

                if (e.ColumnIndex == dgvAddAccounts.Columns["AddAccountID"].Index)
                {
                    if (cboCompany.SelectedIndex >= 0)
                    {
                        decimal dlTempAmt = MobjclsBLLJournalVoucher.getAccountBalance(dgvAddAccounts.Rows[e.RowIndex].Cells["AddAccountID"].Value.ToInt32(), 
                            cboCompany.SelectedValue.ToInt32());

                        if (dlTempAmt > 0)
                            lblSelectedAccountBalance.Text = Math.Abs(dlTempAmt).ToString() + " (Dr)";
                        else
                            lblSelectedAccountBalance.Text = Math.Abs(dlTempAmt).ToString() + " (Cr)";
                    }
                }
            }
        }

        private void SetPanel(int intRowIndex, int intColumnIndex)
        {
            int intX = 0;

            if (expnlLeft.Expanded)
                intX = PanelLeft.Width + expnlLeft.Width + dgvAddAccounts.RowHeadersWidth;
            else
                intX = dgvAddAccounts.RowHeadersWidth;

            if (intRowIndex > 4)
            {
                pnlLedgerAccounts.Location = new Point(intX, btnAddJV.Location.Y + 5);
            }
            else
            {
                pnlLedgerAccounts.Location = new Point(intX, 
                    dgvAddAccounts.Location.Y + dgvAddAccounts.ColumnHeadersHeight + dgvAddAccounts.CurrentRow.Height * (intRowIndex + 2));
            }
        }

        private void CalcJVTotal()
        {
            txtAddTotalDebit.Text = txtAddTotalCredit.Text = "0";

            for (int iCounter = 0; iCounter <= dgvAddAccounts.Rows.Count - 1; iCounter++)
            {
                if (dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value != null)
                    txtAddTotalDebit.Text = (txtAddTotalDebit.Text.ToDecimal() +
                        (dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value.ToString()).ToDecimal()).ToString();
                else if (dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value != null)
                    txtAddTotalCredit.Text = (txtAddTotalCredit.Text.ToDecimal() +
                        (dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value.ToString()).ToDecimal()).ToString();
            }
        }

        private void dgvAddAccounts_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private void dgvAddAccounts_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvAddAccounts.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);

            if (e.Control is DataGridViewComboBoxEditingControl)
                ((DataGridViewComboBoxEditingControl)e.Control).BackColor = Color.White;
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvAddAccounts.CurrentCell.OwningColumn.Index == AddDebitAmount.Index ||
                dgvAddAccounts.CurrentCell.OwningColumn.Index == AddCreditAmount.Index)
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    int dotIndex = -1;
                    if (txt.Text.Contains("."))
                    {
                        dotIndex = txt.Text.IndexOf('.');
                    }
                    if (e.KeyChar == '.')
                    {
                        if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                        {
                            e.Handled = true;
                        }
                    }
                }
            }            
        }

        private void dgvAddAccounts_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            CalcJVTotal();
        }

        private void dgvAddAccounts_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgvAddAccounts.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dgvAddAccounts_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            pnlLedgerAccounts.SendToBack();
            pnlLedgerAccounts.Visible = false;
        }

        private void dgvAddAccounts_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvLedgerAccounts != null && dgvLedgerAccounts.Rows.Count > 0)
                {
                    dgvLedgerAccounts.Focus();
                    dgvLedgerAccounts.CurrentCell = dgvLedgerAccounts["LedgerCode", 0];
                }
            }
            catch { }
        }

        private void btnAddJV_Click(object sender, EventArgs e)
        {
            bool IsNotValid = false;

            if (txtAddTotalDebit.Text.ToDecimal() == 0 || txtAddTotalCredit.Text.ToDecimal() == 0)
                IsNotValid = true;
            else
            {
                if (txtAddTotalDebit.Text.ToDecimal() == txtAddTotalCredit.Text.ToDecimal())
                {
                    if (dgvAddAccounts.Rows[0].Cells["AddJVSerialNo"].Value.ToInt32() == 0) // Add Mode
                    {
                        if (dgvJournalVoucher.Rows.Count == 0)
                            intJVSerialNo = 1;
                        else
                            intJVSerialNo = dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["JVSerialNo"].Value.ToInt32() + 1;
                    }

                    intDebitRowCount = intCreditRowCount = intDrAccountID = intCrAccountID = 0;
                    strDrCode = strCrCode = strDrName = strCrName = strDrRemarks = strCrRemarks = "";
                    decDrAmount = decCrAmount = 0;

                    for (int iCounter = 0; iCounter <= dgvAddAccounts.Rows.Count - 2; iCounter++)
                    {
                        if (dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value.ToDecimal() > 0)
                        {
                            intDebitRowCount++;
                            intDrAccountID = dgvAddAccounts.Rows[iCounter].Cells["AddAccountID"].Value.ToInt32();
                            strDrCode = dgvAddAccounts.Rows[iCounter].Cells["AddCode"].Value.ToStringCustom();
                            strDrName = dgvAddAccounts.Rows[iCounter].Cells["AddAccountName"].Value.ToStringCustom();
                            strDrRemarks = dgvAddAccounts.Rows[iCounter].Cells["AddRemarks"].Value.ToStringCustom();
                            decDrAmount = dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value.ToDecimal();
                        }
                        else if (dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value.ToDecimal() > 0)
                        {
                            intCreditRowCount++;
                            intCrAccountID = dgvAddAccounts.Rows[iCounter].Cells["AddAccountID"].Value.ToInt32();
                            strCrCode = dgvAddAccounts.Rows[iCounter].Cells["AddCode"].Value.ToStringCustom();
                            strCrName = dgvAddAccounts.Rows[iCounter].Cells["AddAccountName"].Value.ToStringCustom();
                            strCrRemarks = dgvAddAccounts.Rows[iCounter].Cells["AddRemarks"].Value.ToStringCustom();
                            decCrAmount = dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value.ToDecimal();
                        }
                    }

                    if (intDebitRowCount > 1 && intCreditRowCount > 1)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 112, out MmessageIcon);
                        tslStatus.Text = MsMessageCommon.Split('#').Last();
                        tmGeneralReceiptsAndPayments.Enabled = true;
                        MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    }
                    else
                    {
                        AddJV();
                        AddGJV();
                    }
                }
                else
                    IsNotValid = true;
            }

            if (IsNotValid)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 105, out MmessageIcon);
                tslStatus.Text = MsMessageCommon.Split('#').Last();
                tmGeneralReceiptsAndPayments.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
            }
        }

        private void AddJV()
        {
            if (dgvAddAccounts.Rows[0].Cells["AddJVSerialNo"].Value.ToInt32() == 0)
            {
                for (int iCounter = 0; iCounter <= dgvAddAccounts.Rows.Count - 2; iCounter++)
                {
                    if (intDebitRowCount > 1)
                    {
                        if (dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value.ToDecimal() > 0)
                        {
                            dgvJournalVoucher.Rows.Add();
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountID"].Value = intCrAccountID;
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Code"].Value = strCrCode;
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountName"].Value = strCrName;
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["SerialNo"].Value = 1;
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Remarks"].Value = strCrRemarks;
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["CreditAmount"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value.ToDecimal();
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["JVSerialNo"].Value = intJVSerialNo;

                            dgvJournalVoucher.Rows.Add();
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountID"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddAccountID"].Value.ToInt32();

                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Code"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddCode"].Value.ToStringCustom();

                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountName"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddAccountName"].Value.ToStringCustom();

                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["SerialNo"].Value = 2;

                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Remarks"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddRemarks"].Value.ToStringCustom();

                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["DebitAmount"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value.ToDecimal();

                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["JVSerialNo"].Value = intJVSerialNo;

                            intJVSerialNo++;
                        }
                    }
                    else
                    {
                        if (dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value.ToDecimal() > 0)
                        {
                            dgvJournalVoucher.Rows.Add();
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountID"].Value = intDrAccountID;
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Code"].Value = strDrCode;
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountName"].Value = strDrName;
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["SerialNo"].Value = 1;
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Remarks"].Value = strDrRemarks;
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["DebitAmount"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value.ToDecimal();
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["JVSerialNo"].Value = intJVSerialNo;

                            dgvJournalVoucher.Rows.Add();
                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountID"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddAccountID"].Value.ToInt32();

                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Code"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddCode"].Value.ToStringCustom();

                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountName"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddAccountName"].Value.ToStringCustom();

                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["SerialNo"].Value = 2;

                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Remarks"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddRemarks"].Value.ToStringCustom();

                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["CreditAmount"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value.ToDecimal();

                            dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["JVSerialNo"].Value = intJVSerialNo;

                            intJVSerialNo++;
                        }
                    }
                }
            }
            else
            {
                for (int iCounter = 0; iCounter <= dgvAddAccounts.Rows.Count - 2; iCounter++)
                {
                    if (dgvAddAccounts.Rows[iCounter].Cells["AddJVSerialNo"].Value.ToInt32() > 0)
                    {
                        for (int jCounter = 0; jCounter <= dgvJournalVoucher.Rows.Count - 1; jCounter++)
                        {
                            if (dgvAddAccounts.Rows[iCounter].Cells["AddJVSerialNo"].Value.ToInt32() ==
                                dgvJournalVoucher.Rows[jCounter].Cells["JVSerialNo"].Value.ToInt32() &&
                                dgvAddAccounts.Rows[iCounter].Cells["AddSerialNo"].Value.ToInt32() ==
                                dgvJournalVoucher.Rows[jCounter].Cells["SerialNo"].Value.ToInt32())
                            {
                                dgvJournalVoucher.Rows[jCounter].Cells["AccountID"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddAccountID"].Value.ToInt32();

                                dgvJournalVoucher.Rows[jCounter].Cells["Code"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddCode"].Value.ToStringCustom();

                                dgvJournalVoucher.Rows[jCounter].Cells["AccountName"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddAccountName"].Value.ToStringCustom();

                                dgvJournalVoucher.Rows[jCounter].Cells["SerialNo"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddSerialNo"].Value.ToInt32();

                                dgvJournalVoucher.Rows[jCounter].Cells["Remarks"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddRemarks"].Value.ToStringCustom();

                                if (dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value.ToDecimal() > 0)
                                    dgvJournalVoucher.Rows[jCounter].Cells["DebitAmount"].Value =
                                        dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value.ToDecimal();

                                if (dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value.ToDecimal() > 0)
                                    dgvJournalVoucher.Rows[jCounter].Cells["CreditAmount"].Value =
                                        dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value.ToDecimal();

                                dgvJournalVoucher.Rows[jCounter].Cells["JVSerialNo"].Value = intJVSerialNo;
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (intDebitRowCount > 1)
                        {
                            if (dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value.ToDecimal() > 0)
                            {
                                dgvJournalVoucher.Rows.Add();
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountID"].Value = intCrAccountID;
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Code"].Value = strCrCode;
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountName"].Value = strCrName;
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["SerialNo"].Value = 1;
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Remarks"].Value = strCrRemarks;
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["CreditAmount"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value.ToDecimal();
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["JVSerialNo"].Value = intJVSerialNo;

                                dgvJournalVoucher.Rows.Add();
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountID"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddAccountID"].Value.ToInt32();

                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Code"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddCode"].Value.ToStringCustom();

                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountName"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddAccountName"].Value.ToStringCustom();

                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["SerialNo"].Value = 2;

                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Remarks"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddRemarks"].Value.ToStringCustom();

                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["DebitAmount"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value.ToDecimal();

                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["JVSerialNo"].Value = intJVSerialNo;

                                intJVSerialNo++;
                            }
                        }
                        else
                        {
                            if (dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value.ToDecimal() > 0)
                            {
                                dgvJournalVoucher.Rows.Add();
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountID"].Value = intDrAccountID;
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Code"].Value = strDrCode;
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountName"].Value = strDrName;
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["SerialNo"].Value = 1;
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Remarks"].Value = strDrRemarks;
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["DebitAmount"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value.ToDecimal();
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["JVSerialNo"].Value = intJVSerialNo;

                                dgvJournalVoucher.Rows.Add();
                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountID"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddAccountID"].Value.ToInt32();

                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Code"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddCode"].Value.ToStringCustom();

                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["AccountName"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddAccountName"].Value.ToStringCustom();

                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["SerialNo"].Value = 2;

                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["Remarks"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddRemarks"].Value.ToStringCustom();

                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["CreditAmount"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value.ToDecimal();

                                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].Cells["JVSerialNo"].Value = intJVSerialNo;

                                intJVSerialNo++;
                            }
                        }
                    }
                }
            }

            CalcTotal();
        }

        private void AddGJV()
        {
            if (dgvAddAccounts.Rows[0].Cells["AddJVSerialNo"].Value.ToInt32() == 0) // Add Mode
            {
                if (dgvGroupJV.Rows.Count == 0)
                    intJVSerialNo = 1;
                else
                    intJVSerialNo = dgvGroupJV.Rows[dgvGroupJV.Rows.Count - 1].Cells["GJVJVSerialNo"].Value.ToInt32() + 1;
            }

            for (int iCounter = 0; iCounter <= dgvAddAccounts.Rows.Count - 2; iCounter++)
            {
                if (dgvAddAccounts.Rows[0].Cells["AddJVSerialNo"].Value.ToInt32() == 0)
                {
                    dgvGroupJV.Rows.Add();
                    dgvGroupJV.Rows[iCounter].Cells["GJVAccountID"].Value =
                        dgvAddAccounts.Rows[iCounter].Cells["AddAccountID"].Value.ToInt32();

                    dgvGroupJV.Rows[iCounter].Cells["GJVCode"].Value =
                        dgvAddAccounts.Rows[iCounter].Cells["AddCode"].Value.ToStringCustom();

                    dgvGroupJV.Rows[iCounter].Cells["GJVAccountName"].Value =
                        dgvAddAccounts.Rows[iCounter].Cells["AddAccountName"].Value.ToStringCustom();

                    dgvGroupJV.Rows[dgvGroupJV.Rows.Count - 1].Cells["GJVSerialNo"].Value = iCounter + 1;

                    dgvGroupJV.Rows[dgvGroupJV.Rows.Count - 1].Cells["GJVRemarks"].Value =
                        dgvAddAccounts.Rows[iCounter].Cells["AddRemarks"].Value.ToStringCustom();

                    if (dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value.ToDecimal() > 0)
                        dgvGroupJV.Rows[dgvGroupJV.Rows.Count - 1].Cells["GJVDebitAmount"].Value =
                            dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value.ToDecimal();

                    if (dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value.ToDecimal() > 0)
                        dgvGroupJV.Rows[dgvGroupJV.Rows.Count - 1].Cells["GJVCreditAmount"].Value =
                            dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value.ToDecimal();

                    dgvGroupJV.Rows[dgvGroupJV.Rows.Count - 1].Cells["GJVJVSerialNo"].Value = intJVSerialNo;
                }
                else
                {
                    for (int jCounter = 0; jCounter <= dgvGroupJV.Rows.Count - 1; jCounter++)
                    {
                        if (dgvAddAccounts.Rows[iCounter].Cells["AddJVSerialNo"].Value.ToInt32() == dgvGroupJV.Rows[jCounter].Cells["GJVJVSerialNo"].Value.ToInt32() &&
                            dgvAddAccounts.Rows[iCounter].Cells["AddSerialNo"].Value.ToInt32() == dgvGroupJV.Rows[jCounter].Cells["GJVSerialNo"].Value.ToInt32())
                        {
                            dgvGroupJV.Rows[jCounter].Cells["GJVAccountID"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddAccountID"].Value.ToInt32();

                            dgvGroupJV.Rows[jCounter].Cells["GJVCode"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddCode"].Value.ToStringCustom();

                            dgvGroupJV.Rows[jCounter].Cells["GJVAccountName"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddAccountName"].Value.ToStringCustom();

                            dgvGroupJV.Rows[jCounter].Cells["GJVSerialNo"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddSerialNo"].Value.ToInt32();

                            dgvGroupJV.Rows[jCounter].Cells["GJVRemarks"].Value =
                                dgvAddAccounts.Rows[iCounter].Cells["AddRemarks"].Value.ToStringCustom();

                            if (dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value.ToDecimal() > 0)
                                dgvGroupJV.Rows[jCounter].Cells["GJVDebitAmount"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddDebitAmount"].Value.ToDecimal();

                            if (dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value.ToDecimal() > 0)
                                dgvGroupJV.Rows[jCounter].Cells["GJVCreditAmount"].Value =
                                    dgvAddAccounts.Rows[iCounter].Cells["AddCreditAmount"].Value.ToDecimal();

                            dgvGroupJV.Rows[jCounter].Cells["GJVJVSerialNo"].Value = intJVSerialNo;
                        }
                    }
                }
            }

            dgvAddAccounts.Rows.Clear();
            CalcTotal();
        }

        private void txtAddTotalDebit_TextChanged(object sender, EventArgs e)
        {
            if (dgvAddAccounts.Rows.Count > 0)
            {
                dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 1].ReadOnly = false;
                if (txtAddTotalDebit.Text != "0" && txtAddTotalCredit.Text != "0")
                    if (dgvAddAccounts.Rows.Count > 2 && txtAddTotalDebit.Text == txtAddTotalCredit.Text)
                        dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 1].ReadOnly = true;
            }
        }

        private void txtDebitTotal_TextChanged(object sender, EventArgs e)
        {
            if (txtDebitTotal.Text != "0" || txtCreditTotal.Text != "0")
            {
                btnAddNewItem.Enabled = MblnAddPermission;
                btnSaveItem.Enabled = MblnAddUpdatePermission;

                if (txtVoucherNo.Tag.ToDecimal() > 0)
                {
                    btnSaveItem.Enabled = MblnUpdatePermission;
                    btnDeleteItem.Enabled = MblnDeletePermission;
                    btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
                }
                else
                    btnDeleteItem.Enabled = btnPrint.Enabled = btnEmail.Enabled = false;
            }
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            getVocherNo();
        }

        private void dgvGroupJV_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
                EditJV(e.RowIndex);
        }

        private void EditJV(int intRowIndex)
        {
            dgvAddAccounts.Rows.Clear();
            intJVSerialNo = dgvGroupJV.Rows[intRowIndex].Cells["GJVJVSerialNo"].Value.ToInt32();

            if (intJVSerialNo > 0)
            {
                for (int iCounter = 0; iCounter <= dgvGroupJV.Rows.Count - 1; iCounter++)
                {
                    if (intJVSerialNo == dgvGroupJV.Rows[iCounter].Cells["GJVJVSerialNo"].Value.ToInt32())
                    {
                        dgvAddAccounts.Rows.Add();
                        dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddAccountID"].Value = dgvGroupJV.Rows[iCounter].Cells["GJVAccountID"].Value.ToInt32();
                        dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddCode"].Value = dgvGroupJV.Rows[iCounter].Cells["GJVCode"].Value.ToStringCustom();
                        dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddAccountName"].Value = dgvGroupJV.Rows[iCounter].Cells["GJVAccountName"].Value.ToStringCustom();
                        dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddSerialNo"].Value = dgvGroupJV.Rows[iCounter].Cells["GJVSerialNo"].Value.ToInt32();
                        dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddRemarks"].Value = dgvGroupJV.Rows[iCounter].Cells["GJVRemarks"].Value.ToStringCustom();

                        if (dgvGroupJV.Rows[iCounter].Cells["GJVDebitAmount"].Value.ToDecimal() > 0)
                            dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddDebitAmount"].Value = dgvGroupJV.Rows[iCounter].Cells["GJVDebitAmount"].Value.ToDecimal();

                        if (dgvGroupJV.Rows[iCounter].Cells["GJVCreditAmount"].Value.ToDecimal() > 0)
                            dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddCreditAmount"].Value = dgvGroupJV.Rows[iCounter].Cells["GJVCreditAmount"].Value.ToDecimal();

                        dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddJVSerialNo"].Value = intJVSerialNo;
                        dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddVoucherID"].Value = dgvGroupJV.Rows[iCounter].Cells["GJVVoucherID"].Value.ToDecimal();
                    }
                }
            }
            //intJVSerialNo = dgvJournalVoucher.Rows[intRowIndex].Cells["JVSerialNo"].Value.ToInt32();

            //if (intJVSerialNo > 0)
            //{
            //    for (int iCounter = 0; iCounter <= dgvJournalVoucher.Rows.Count - 1; iCounter++)
            //    {
            //        if (intJVSerialNo == dgvJournalVoucher.Rows[iCounter].Cells["JVSerialNo"].Value.ToInt32())
            //        {
            //            dgvAddAccounts.Rows.Add();
            //            dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddAccountID"].Value = dgvJournalVoucher.Rows[iCounter].Cells["AccountID"].Value.ToInt32();
            //            dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddCode"].Value = dgvJournalVoucher.Rows[iCounter].Cells["Code"].Value.ToStringCustom();
            //            dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddAccountName"].Value = dgvJournalVoucher.Rows[iCounter].Cells["AccountName"].Value.ToStringCustom();
            //            dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddSerialNo"].Value = dgvJournalVoucher.Rows[iCounter].Cells["SerialNo"].Value.ToInt32();
            //            dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddRemarks"].Value = dgvJournalVoucher.Rows[iCounter].Cells["Remarks"].Value.ToStringCustom();

            //            if (dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value.ToDecimal() > 0)
            //                dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddDebitAmount"].Value = dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value.ToDecimal();

            //            if (dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value.ToDecimal() > 0)
            //                dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddCreditAmount"].Value = dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value.ToDecimal();

            //            dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddJVSerialNo"].Value = intJVSerialNo;
            //            dgvAddAccounts.Rows[dgvAddAccounts.Rows.Count - 2].Cells["AddVoucherID"].Value = dgvJournalVoucher.Rows[iCounter].Cells["EditVoucherID"].Value.ToDecimal();
            //        }
            //    }
            //}
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void LoadReport()
        {
            long VoucherID = txtVoucherNo.Tag.ToInt64();
            if (VoucherID > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = VoucherID;
                ObjViewer.PiFormID = (int)FormID.GroupJV;
                ObjViewer.ShowDialog();
            }
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedIndex >= 0)
            {
                clsBLLAccountSummary MobjclsBLLAccountSummary = new clsBLLAccountSummary();
                lblCurrency.Text = MobjclsBLLAccountSummary.GetCompanyCurrency(cboCompany.SelectedValue.ToInt32());
            }
            else
                lblCurrency.Text = "Currency : ";
        }
        
        private void dgvGroupJV_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            CalcTotal();
        }
                
        private void btnJVEdit_Click(object sender, EventArgs e)
        {
            if (dgvJournalVoucher.CurrentRow.Index >= 0)
                EditJV(dgvJournalVoucher.CurrentRow.Index);
        }

        private void btnJVRemove_Click(object sender, EventArgs e)
        {
            bool blnIsValidRemove = true;

            if (dgvJournalVoucher.CurrentRow.Index >= 0)
            {
                int intTempJVSerialNo = dgvJournalVoucher.Rows[dgvJournalVoucher.CurrentRow.Index].Cells["JVSerialNo"].Value.ToInt32();

                for (int iCounter = 0; iCounter <= dgvJournalVoucher.Rows.Count - 1; iCounter++)
                {
                    if (!dgvJournalVoucher.Rows[iCounter].Selected)
                    {
                        if (intTempJVSerialNo == dgvJournalVoucher.Rows[iCounter].Cells["JVSerialNo"].Value.ToInt32())
                        {
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 105, out MmessageIcon);
                            tslStatus.Text = MsMessageCommon.Split('#').Last();
                            tmGeneralReceiptsAndPayments.Enabled = true;
                            MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            blnIsValidRemove = false;
                            break;
                        }
                    }                    
                }
            }

            if (blnIsValidRemove)
            {
                for (int iCounter = dgvJournalVoucher.Rows.Count - 1; iCounter >= 0 ; iCounter--)
                {
                    if (dgvJournalVoucher.Rows[iCounter].Selected)
                        dgvJournalVoucher.Rows.RemoveAt(iCounter);
                }

                dgvAddAccounts.Rows.Clear();
            }
        }

        private void btnAddAutofill_Click(object sender, EventArgs e)
        {
            if (dgvAddAccounts.Rows.Count > 0 && dgvAddAccounts.CurrentRow.Index >= 0)
            {
                if (dgvAddAccounts.CurrentCell.ColumnIndex == dgvAddAccounts.Columns["AddRemarks"].Index)
                {
                    for (int iCounter = dgvAddAccounts.CurrentRow.Index; iCounter <= dgvAddAccounts.Rows.Count - 2; iCounter++)
                        dgvAddAccounts.Rows[iCounter].Cells["AddRemarks"].Value = dgvAddAccounts.Rows[dgvAddAccounts.CurrentRow.Index].Cells["AddRemarks"].Value;
                }
            }
        }

        private void btnAutofill_Click(object sender, EventArgs e)
        {
            if (dgvJournalVoucher.Rows.Count > 0 && dgvJournalVoucher.CurrentRow.Index >= 0)
            {
                if (dgvJournalVoucher.CurrentCell.ColumnIndex == dgvJournalVoucher.Columns["Remarks"].Index)
                {
                    for (int iCounter = dgvJournalVoucher.CurrentRow.Index; iCounter <= dgvJournalVoucher.Rows.Count - 1; iCounter++)
                        dgvJournalVoucher.Rows[iCounter].Cells["Remarks"].Value = dgvJournalVoucher.Rows[dgvJournalVoucher.CurrentRow.Index].Cells["Remarks"].Value;
                }
            }
        }

        private void dgvLedgerAccounts_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvLedgerAccounts.CurrentRow != null && dgvLedgerAccounts.Focused)
                SelectLedgerAccounts(e.RowIndex);

            pnlLedgerAccounts.SendToBack();
            pnlLedgerAccounts.Visible = false;
        }

        private void SelectLedgerAccounts(int intRowindex)
        {
            try
            {
                if (dgvLedgerAccounts != null && dgvLedgerAccounts.Rows.Count > 0)
                {
                    blnIsSelected = true;
                    int intrIndex = dgvAddAccounts.CurrentRow.Index;

                    if (Convert.ToString(dgvAddAccounts.Rows[intrIndex].Cells["AddCode"].Value) == "" &&
                        Convert.ToString(dgvAddAccounts.Rows[intrIndex].Cells["AddAccountName"].Value) == "")
                        intrIndex = intrIndex - 1;

                    dgvAddAccounts.Rows[intrIndex].Cells["AddAccountID"].Value = dgvLedgerAccounts.Rows[intRowindex].Cells["LedgerAccountID"].Value;
                    dgvAddAccounts.Rows[intrIndex].Cells["AddCode"].Value = dgvLedgerAccounts.Rows[intRowindex].Cells["LedgerCode"].Value;
                    dgvAddAccounts.Rows[intrIndex].Cells["AddAccountName"].Value = dgvLedgerAccounts.Rows[intRowindex].Cells["LedgerName"].Value;
                    blnIsSelected = false;
                }
            }
            catch { }
        }

        private void expnlLeft_ExpandedChanged(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
        {
            try
            {
                if (dgvAddAccounts.CurrentRow != null)
                    SetPanel(dgvAddAccounts.CurrentRow.Index, 1);
            }
            catch { }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            long lngVoucherID = txtVoucherNo.Tag.ToInt64();
            if (lngVoucherID > 0)
            {
                using (FrmEmailPopup objEmailPopUp = new FrmEmailPopup())
                {
                    objEmailPopUp.MsSubject = "Journal Voucher";
                    objEmailPopUp.EmailFormType = EmailFormID.GroupJV;
                    objEmailPopUp.EmailSource = MobjclsBLLJournalVoucher.DisplayEmailGroupJV(lngVoucherID);
                    objEmailPopUp.ShowDialog();
                }
            }
        }
    }
}