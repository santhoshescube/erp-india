﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlTypes;

namespace MyBooksERP
{
    public partial class frmJournalVoucher : DevComponents.DotNetBar.Office2007Form
    {
        bool blnNavigateStatus = false;        
        bool MblnAddPermission, MblnAddUpdatePermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission;
        bool blnIsValidated = true;

        clsBLLJournalVoucher MobjclsBLLJournalVoucher = new clsBLLJournalVoucher();
        public int intOpTypeID, intRefID, intJTypeID = 1;   // Journal Type --> Direct = 1, Recurring = 2, Automatted = 3
        public int RecurringJournalSetUpID = 0;
        public DateTime dtRecurringDate;
        private clsMessage objUserMessage = null;

        public decimal decDrilVoucherID = 0;                // For Account Summary Dril Down don't Change
        public int TempMenuID { get; set; }                 // For which Voucher Type want to display

        private ArrayList MaMessageArr;                 // Error Message display
        private ArrayList MaStatusMessage;
        ClsNotification mObjNotification;

        private string MsMessageCaption;                //  Showdialog Message caption from MDI
        private string MsMessageCommon;                 //  variable for assigning message
        private MessageBoxIcon MmessageIcon;

        DataTable datLedgerAccounts = new DataTable();
        bool blnIsSelected = false;

        public frmJournalVoucher()
        {
            InitializeComponent();
            mObjNotification = new ClsNotification();
        }

        private void frmJournalVoucher_Load(object sender, EventArgs e)
        {
            tmGeneralReceiptsAndPayments.Enabled = false;
            LoadMessage();
            LoadCombos();

            switch (TempMenuID)
            {
                case (int)eMenuID.PaymentVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.Payment;
                    break;
                case (int)eMenuID.ReceiptVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.Receipt;
                    break;
                case (int)eMenuID.ContraVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.Contra;
                    break;
                case (int)eMenuID.JournalVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.Journal;
                    break;
                case (int)eMenuID.PurchaseVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.Purchase;
                    break;
                case (int)eMenuID.DebitNoteVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.DebitNote;
                    break;
                case (int)eMenuID.SalesVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.Sale;
                    break;
                case (int)eMenuID.CreditNoteVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.CreditNote;
                    break;
            }

            SetPermissions();
            if (decDrilVoucherID > 0)
            {
                txtVoucherNo.Tag = decDrilVoucherID;
                DisplayJournalVoucher();
                expnlLeft.Expanded = false;
            }
            else if (RecurringJournalSetUpID > 0)
            {   
                tmGeneralReceiptsAndPayments.Enabled = false;
                clearControls();
                DataTable datTemp = MobjclsBLLJournalVoucher.getRecurringVoucherMasterDetails(RecurringJournalSetUpID);
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        cboCompany.SelectedValue = datTemp.Rows[0]["CompanyID"].ToInt32();
                        cboVoucherType.SelectedValue = datTemp.Rows[0]["VoucherTypeID"].ToInt32();
                        dtpDate.Value = dtRecurringDate.ToString("dd MMM yyyy").ToDateTime();
                        DisplayRecurringJournalVoucher();
                    }
                }
                cboCompany.Enabled = cboVoucherType.Enabled = dtpDate.Enabled = false;
                expnlLeft.Expanded = false;
            }
            else
                clearControls();

            tcVoucherEntry.SelectedTab = stVoucherEntry;

            switch (TempMenuID)
            {
                case (int)eMenuID.PaymentVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.Payment;
                    ButtonVisibility(true);
                    break;
                case (int)eMenuID.ReceiptVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.Receipt;
                    ButtonVisibility(true);
                    break;
                case (int)eMenuID.ContraVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.Contra;
                    ButtonVisibility(true);
                    break;
                case (int)eMenuID.JournalVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.Journal;
                    ButtonVisibility(true);
                    break;
                case (int)eMenuID.PurchaseVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.Purchase;
                    ButtonVisibility(false);
                    break;
                case (int)eMenuID.DebitNoteVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.DebitNote;
                    ButtonVisibility(false);
                    break;
                case (int)eMenuID.SalesVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.Sale;
                    ButtonVisibility(false);
                    break;
                case (int)eMenuID.CreditNoteVoucher:
                    cboVoucherType.SelectedValue = (int)VoucherType.CreditNote;
                    ButtonVisibility(false);
                    break;
            }
        }

        private void ButtonVisibility(bool blnEnable)
        {
            btnAddNewItem.Visible = btnSaveItem.Visible = btnDeleteItem.Visible = btnClear.Visible =
                btnChartofAccounts.Visible = btnCostCenterReference.Visible = blnEnable;
            toolStripSeparator1.Visible = toolStripSeparator7.Visible = blnEnable;
            dgvJournalVoucher.ReadOnly = !blnEnable;
            panelTop.Enabled = blnEnable;
            pnlBottom.Enabled = blnEnable;
        }

        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = mObjNotification.FillMessageArray((int)FormID.GeneralReceiptsAndPayments, 4);
            MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.GeneralReceiptsAndPayments, 4);
            MsMessageCaption = ClsCommonSettings.MessageCaption;
        }

        private void LoadCombos()
        {
            cboCompany.DataSource = MobjclsBLLJournalVoucher.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID + " ORDER BY CompanyName" });
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DisplayMember = "CompanyName";
            cboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;

            CboSearchCompany.DataSource = MobjclsBLLJournalVoucher.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID + " ORDER BY CompanyName" });
            CboSearchCompany.ValueMember = "CompanyID";
            CboSearchCompany.DisplayMember = "CompanyName";
            CboSearchCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;

            cboVoucherType.DataSource = MobjclsBLLJournalVoucher.FillCombos(new string[] { "VoucherTypeID,VoucherType", "AccVoucherTypeReference", "" });
            cboVoucherType.ValueMember = "VoucherTypeID";
            cboVoucherType.DisplayMember = "VoucherType";
            cboVoucherType.SelectedIndex = -1;

            cboBankAccount.DataSource = MobjclsBLLJournalVoucher.FillCombos(new string[] { "AccountID,AccountName", "AccAccountMaster", "AccountGroupID = " + (int)AccountGroups.BankAccounts });
            cboBankAccount.DisplayMember = "AccountName";
            cboBankAccount.ValueMember = "AccountID";
                        
            LoadAccounts("ISNULL(AccountGroupID,0) <> 0 ORDER BY AccountName");
            LoadCostCenter();            
        }

        private void LoadAccounts(string stTemp)
        {
            //AccountID.DataSource = MobjclsBLLJournalVoucher.FillCombos(new string[] { "AccountID,CASE WHEN ISNULL(Code,'') = '' THEN AccountName ELSE (AccountName + ' - ' + Code) END AS AccountName", "AccAccountMaster", stTemp });
            //AccountID.ValueMember = "AccountID";
            //AccountID.DisplayMember = "AccountName";  

            datLedgerAccounts = MobjclsBLLJournalVoucher.FillCombos(new string[] { "AccountID AS LedgerAccountID,Code AS LedgerCode," +
                "AccountName AS LedgerName", "AccAccountMaster", "(CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL) AND " + stTemp });
            dgvLedgerAccounts.DataSource = datLedgerAccounts;

            CostCenterAccountID.DataSource = MobjclsBLLJournalVoucher.FillCombos(new string[] { "AccountID,AccountName", "AccAccountMaster", "" +
                "(CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL) AND ISNULL(AccountGroupID,0) <> 0 ORDER BY AccountName" });
            CostCenterAccountID.ValueMember = "AccountID";
            CostCenterAccountID.DisplayMember = "AccountName";
        }

        private void LoadCostCenter()
        {
            CostCenterID.DataSource = MobjclsBLLJournalVoucher.FillCombos(new string[] { "CostCenterID,CostCenter", "AccCostCenterReference", "ISNULL(CostCenterGroupID,0) <> " + (int)CostCenterGroupReference.Employee + " ORDER BY CostCenter" });
            CostCenterID.ValueMember = "CostCenterID";
            CostCenterID.DisplayMember = "CostCenter";
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 2)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Accounts, (int)eMenuID.JournalVoucher, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                if (MblnAddPermission == true || MblnUpdatePermission == true)
                    MblnAddUpdatePermission = true;
            }
            else
                MblnAddPermission = MblnAddUpdatePermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void cboSearchCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboSearchCompany.SelectedIndex >= 0)
                cboCompany.SelectedValue = CboSearchCompany.SelectedValue.ToInt32();

            clearControls();
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            getVocherNo();            
            dgvJournalVoucher.Rows.Clear();
            dgvCostCenter.Rows.Clear();
            txtDebitTotal.Text = txtCreditTotal.Text = "0";

            if (cboCompany.SelectedIndex >= 0)
            {
                clsBLLAccountSummary MobjclsBLLAccountSummary = new clsBLLAccountSummary();
                lblCurrency.Text = MobjclsBLLAccountSummary.GetCompanyCurrency(cboCompany.SelectedValue.ToInt32());
            }
            else
                lblCurrency.Text = "Currency : ";
        }

        private void cboVoucherType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboVoucherType.SelectedIndex >= 0)
            {                
                if (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Payment)
                    lblVoucherNo.Text = "Payment No";
                else if (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Receipt)
                    lblVoucherNo.Text = "Receipt No";
                else if (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Contra)
                    lblVoucherNo.Text = "Contra No";
                else if (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Journal)
                    lblVoucherNo.Text = "Journal No";
                else if (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Purchase)
                    lblVoucherNo.Text = "Purchase No";
                else if (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Sale)
                    lblVoucherNo.Text = "Sales No";
                else if (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.DebitNote)
                    lblVoucherNo.Text = "Debit Note No";
                else if (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.CreditNote)
                    lblVoucherNo.Text = "Credit Note No";

                getVocherNo();
            }           

            dgvJournalVoucher.Rows.Clear();
            dgvCostCenter.Rows.Clear();
            txtDebitTotal.Text = txtCreditTotal.Text = "0";

            dgvJournalVoucher.ReadOnly = false;
            dgvCostCenter.ReadOnly = false;
            
            btnAddNewItem.Enabled = MblnAddPermission;
            btnSaveItem.Enabled = false;
            btnDeleteItem.Enabled = btnPrint.Enabled = btnEmail.Enabled = false;
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            //getVocherNo();
        }

        private void getVocherNo()
        {
            if (cboCompany.SelectedIndex >= 0 && cboVoucherType.SelectedIndex >= 0)
                txtVoucherNo.Text = MobjclsBLLJournalVoucher.GetVoucherNo(cboCompany.SelectedValue.ToInt32(), 
                    cboVoucherType.SelectedValue.ToInt32(), (dtpDate.Value.ToString("dd MMM yyy")).ToDateTime());
            else
                txtVoucherNo.Text = "";
        }

        private void clearControls()
        {
            tcVoucherEntry.SelectedTab = stVoucherEntry;
            cboCompany.Enabled = cboVoucherType.Enabled = dtpDate.Enabled = true;
            dtpDate.Value = dtpChequeDate.Value = MobjclsBLLJournalVoucher.GetServerDate();
            LoadAccounts("ISNULL(AccountGroupID,0) <> 0 ORDER BY AccountName");
            txtChequeNo.Text = txtNarration.Text = "";
            getVocherNo();
            lblSelectedAccountBalance.Text = txtDebitTotal.Text = txtCreditTotal.Text = "0";
            txtVoucherNo.Tag = 0;
            txtChequeNo.Enabled = dtpChequeDate.Enabled = false;            
            dgvJournalVoucher.Rows.Clear();
            dgvCostCenter.Rows.Clear();
            LoadCostCenter();

            blnNavigateStatus = false;
            getVoucherNosForSearch();
            GetRecordCountwithValues();

            btnAddNewItem.Enabled = MblnAddPermission;
            btnSaveItem.Enabled = false;
            btnDeleteItem.Enabled = btnPrint.Enabled = btnEmail.Enabled = false;

            dgvJournalVoucher.ReadOnly = false;
            dgvCostCenter.ReadOnly = false;
            if (cboVoucherType.SelectedIndex >= 0)
            {
                if (cboVoucherType.SelectedValue.ToInt32() > 4)
                {
                    //dgvJournalVoucher.ReadOnly = true;
                    //dgvCostCenter.ReadOnly = true;
                }
            }
            cboBankAccount.SelectedIndex = -1;

            pnlLedgerAccounts.SendToBack();
            pnlLedgerAccounts.Visible = false;
        }

        private void BtnSRefresh_Click(object sender, EventArgs e)
        {
            getVoucherNosForSearch();
            GetRecordCountwithValues();
        }

        private void getVoucherNosForSearch()
        {
            DataTable datTemp = new DataTable();
            if (dtpSFrom.Checked == true && dtpSTo.Checked == true)
                datTemp = MobjclsBLLJournalVoucher.getVoucherNos(cboVoucherType.SelectedValue.ToInt32(),
                    CboSearchCompany.SelectedValue.ToInt32(), TxtSsearch.Text, dtpSFrom.Value.ToString("dd MMM yyyy"), dtpSTo.Value.ToString("dd MMM yyyy"));
            else if (dtpSFrom.Checked == false && dtpSTo.Checked == false)
                datTemp = MobjclsBLLJournalVoucher.getVoucherNos(cboVoucherType.SelectedValue.ToInt32(), 
                    CboSearchCompany.SelectedValue.ToInt32(), TxtSsearch.Text, "", "");
            else if (dtpSFrom.Checked == true && dtpSTo.Checked == false)
                datTemp = MobjclsBLLJournalVoucher.getVoucherNos(cboVoucherType.SelectedValue.ToInt32(),
                    CboSearchCompany.SelectedValue.ToInt32(), TxtSsearch.Text, dtpSFrom.Value.ToString("dd MMM yyyy"), "");
            else if (dtpSFrom.Checked == false && dtpSTo.Checked == true)
                datTemp = MobjclsBLLJournalVoucher.getVoucherNos(cboVoucherType.SelectedValue.ToInt32(),
                    CboSearchCompany.SelectedValue.ToInt32(), TxtSsearch.Text, "", dtpSTo.Value.ToString("dd MMM yyyy"));

            dgvVNoDisplay.DataSource = datTemp;
            dgvVNoDisplay.Columns["VoucherID"].Visible = false;
        }

        private void dgvVNoDisplay_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtVoucherNo.Tag = dgvVNoDisplay.Rows[e.RowIndex].Cells["VoucherID"].Value.ToDecimal();
                DisplayJournalVoucher();
            }
            catch
            {
            }
        }

        private void GetRecordCountwithValues()
        {
            LblSCountStatus.Text = dgvVNoDisplay == null ? "" : dgvVNoDisplay.Rows.Count.ToStringCustom() == "0" ? "" : dgvVNoDisplay.Rows.Count.ToStringCustom();
        }

        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            if (FormValidation())
            {
                bool blnTempSave = false; // want to save or not.
                bool blnTempUpdate = false; // want to save or not.
                if ((txtVoucherNo.Tag).ToDecimal() > 0)
                    MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID = (SqlDecimal)txtVoucherNo.Tag;
                else
                    MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID = 0;
                MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.strVoucherNo = txtVoucherNo.Text;
                MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.intVoucherTypeID = cboVoucherType.SelectedValue.ToInt32();
                MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.intCompanyID = cboCompany.SelectedValue.ToInt32();
                MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dtpVoucherDate = dtpDate.Value.ToString("dd MMM yyyy").ToDateTime();
                if (txtChequeNo.Enabled)
                    MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.strChNo = txtChequeNo.Text.Trim();
                else
                    MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.strChNo = null;
                MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dtpChDate = dtpChequeDate.Value.ToString("dd MMM yyyy").ToDateTime();
                MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.strRemarks = txtNarration.Text;
                if (RecurringJournalSetUpID > 0)
                {
                    intJTypeID = (int)JournalTypes.Recurring;
                    intOpTypeID = (int)OperationType.RecurringJournal;
                    intRefID = RecurringJournalSetUpID;
                }
                MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.intJournalTypeID = intJTypeID;
                MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.intOperationTypeID = intOpTypeID;
                MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.intReferenceID = intRefID;
                MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.intCreatedBy = ClsCommonSettings.UserID;
                MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dtpCreatedDate = MobjclsBLLJournalVoucher.GetServerDate();

                if (MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID == 0)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        blnTempSave = true;
                }
                else
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        blnTempSave = true;
                }

                if (blnTempSave == true || blnTempUpdate == true)
                {
                    DataTable datJVDetails = new DataTable();
                    datJVDetails.Columns.Add("AccountID");
                    datJVDetails.Columns.Add("SerialNo");
                    datJVDetails.Columns.Add("DebitAmount");
                    datJVDetails.Columns.Add("CreditAmount");
                    datJVDetails.Columns.Add("Remarks");


                    DataTable datCCDetails = new DataTable();
                    datCCDetails.Columns.Add("CostCenterAccountID");
                    datCCDetails.Columns.Add("CostCenterID");
                    datCCDetails.Columns.Add("Amount");

                    for (int iCounter = 0; iCounter <= dgvJournalVoucher.Rows.Count - 2; iCounter++)
                    {
                        datJVDetails.Rows.Add();
                        datJVDetails.Rows[iCounter]["AccountID"] = dgvJournalVoucher.Rows[iCounter].Cells["AccountID"].Value;
                        datJVDetails.Rows[iCounter]["SerialNo"] = dgvJournalVoucher.Rows[iCounter].Cells["SerialNo"].Value;
                        datJVDetails.Rows[iCounter]["DebitAmount"] = dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value;
                        datJVDetails.Rows[iCounter]["CreditAmount"] = dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value;
                        datJVDetails.Rows[iCounter]["Remarks"] = dgvJournalVoucher.Rows[iCounter].Cells["Remarks"].Value;
                    }

                    for (int iCounter = 0; iCounter <= dgvCostCenter.Rows.Count - 2; iCounter++)
                    {
                        datCCDetails.Rows.Add();
                        datCCDetails.Rows[iCounter]["CostCenterAccountID"] = dgvCostCenter.Rows[iCounter].Cells["CostCenterAccountID"].Value;
                        datCCDetails.Rows[iCounter]["CostCenterID"] = dgvCostCenter.Rows[iCounter].Cells["CostCenterID"].Value;
                        datCCDetails.Rows[iCounter]["Amount"] = dgvCostCenter.Rows[iCounter].Cells["Amount"].Value;
                    }

                    if (MobjclsBLLJournalVoucher.SaveJournalVoucher(datJVDetails, datCCDetails) == true)
                    {
                        if (blnTempSave == true)
                        {
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                            tslStatus.Text = MsMessageCommon.Split('#').Last();
                            tmGeneralReceiptsAndPayments.Enabled = true;
                            MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon); // Saved Successfully
                        }
                        else if (blnTempUpdate == true)
                        {
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);
                            tslStatus.Text = MsMessageCommon.Split('#').Last();
                            tmGeneralReceiptsAndPayments.Enabled = true;
                            MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon); // Saved Successfully
                        }
                        clearControls();
                    }
                }
            }
        }

        private bool FormValidation()
        {
            if (dtpDate.Value.Date > ClsCommonSettings.GetServerDate())
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9006, out MmessageIcon);
                tslStatus.Text = MsMessageCommon.Split('#').Last();
                tmGeneralReceiptsAndPayments.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                blnIsValidated = false;
                return false;
            }
            if (cboCompany.SelectedIndex == -1)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 14, out MmessageIcon);
                tslStatus.Text = MsMessageCommon.Split('#').Last();
                tmGeneralReceiptsAndPayments.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                blnIsValidated = false;
                return false;
            }
            if (cboVoucherType.SelectedIndex == -1)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 101, out MmessageIcon);
                tslStatus.Text = MsMessageCommon.Split('#').Last();
                tmGeneralReceiptsAndPayments.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                blnIsValidated = false;
                return false;
            }
            if (txtVoucherNo.Text == "")
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 102, out MmessageIcon);
                tslStatus.Text = MsMessageCommon.Split('#').Last();
                tmGeneralReceiptsAndPayments.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                blnIsValidated = false;
                return false;
            }
            if (txtVoucherNo.Tag.ToDecimal() == 0 && txtVoucherNo.Text != "")
            {
                DataTable datVouchersNos = MobjclsBLLJournalVoucher.getVoucherNos(cboVoucherType.SelectedValue.ToInt32(), 
                    cboCompany.SelectedValue.ToInt32(), txtVoucherNo.Text, "", "");
                if (datVouchersNos != null)
                {
                    if (datVouchersNos.Rows.Count > 0)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 109, out MmessageIcon);
                        tslStatus.Text = MsMessageCommon.Split('#').Last();
                        tmGeneralReceiptsAndPayments.Enabled = true;
                        MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        blnIsValidated = false;
                        return false;
                    }
                }
            }
            if (dgvJournalVoucher.Rows.Count == 1)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 103, out MmessageIcon);
                tslStatus.Text = MsMessageCommon.Split('#').Last();
                tmGeneralReceiptsAndPayments.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                blnIsValidated = false;
                return false;
            }

            for (int iCounter = 0; iCounter <= dgvJournalVoucher.Rows.Count - 2; iCounter++)
            {
                if (dgvJournalVoucher.Rows[iCounter].Cells["AccountID"].Value == null)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 104, out MmessageIcon);
                    tslStatus.Text = MsMessageCommon.Split('#').Last();
                    tmGeneralReceiptsAndPayments.Enabled = true;
                    MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    blnIsValidated = false;
                    return false;
                }

                if (dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value.ToDouble() == 0 && 
                    dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value.ToDouble() == 0)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 105, out MmessageIcon);
                    tslStatus.Text = MsMessageCommon.Split('#').Last();
                    tmGeneralReceiptsAndPayments.Enabled = true;
                    MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    blnIsValidated = false;
                    return false;
                }

                //if (dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value.ToDouble() > 0)
                //{
                //    decimal dlTempAmt = MobjclsBLLJournalVoucher.getAccountBalance(dgvJournalVoucher.Rows[iCounter].Cells["AccountID"].Value.ToInt32(),
                //        cboCompany.SelectedValue.ToInt32());

                //    if ((dlTempAmt - dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value.ToDecimal()) < 0)
                //    {
                //        MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 111, out MmessageIcon);
                //        tslStatus.Text = MsMessageCommon.Split('#').Last();
                //        tmGeneralReceiptsAndPayments.Enabled = true;
                //        MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //        blnIsValidated = false;
                //        return false;
                //    }
                //}
            }

            if (txtDebitTotal.Text.ToDecimal() != txtCreditTotal.Text.ToDecimal())
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 105, out MmessageIcon);
                tslStatus.Text = MsMessageCommon.Split('#').Last();
                tmGeneralReceiptsAndPayments.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                blnIsValidated = false;
                return false;
            }
            if (MobjclsBLLJournalVoucher.CheckAccountForCheque(dgvJournalVoucher.Rows[0].Cells["AccountID"].Value.ToInt32()) == true && 
                (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Payment || 
                cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Receipt))
            {
                if (txtChequeNo.Text == "")
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 106, out MmessageIcon);
                    tslStatus.Text = MsMessageCommon.Split('#').Last();
                    tmGeneralReceiptsAndPayments.Enabled = true;
                    MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    blnIsValidated = false;
                    return false;
                }
            }
            if (dgvCostCenter.Rows.Count > 1)
            {
                for (int iCounter = 0; iCounter <= dgvCostCenter.Rows.Count - 2; iCounter++)
                {
                    if (dgvCostCenter.Rows[iCounter].Cells["CostCenterAccountID"].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 104, out MmessageIcon);
                        tslStatus.Text = MsMessageCommon.Split('#').Last();
                        tmGeneralReceiptsAndPayments.Enabled = true;
                        MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        blnIsValidated = false;
                        return false;
                    }
                    if (dgvCostCenter.Rows[iCounter].Cells["CostCenterID"].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 107, out MmessageIcon);
                        tslStatus.Text = MsMessageCommon.Split('#').Last();
                        tmGeneralReceiptsAndPayments.Enabled = true;
                        MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        blnIsValidated = false;
                        return false;
                    }
                }
                                
                for (int iCounter = 0; iCounter <= dgvJournalVoucher.Rows.Count - 2; iCounter++)
                {
                    decimal dlTempCCAmt = 0;
                    if (MobjclsBLLJournalVoucher.CheckAccountCostCenterApplicable(dgvJournalVoucher.Rows[iCounter].Cells["AccountID"].Value.ToInt32()) == true)
                    {
                        for (int jCounter = 0; jCounter <= dgvCostCenter.Rows.Count - 2; jCounter++)
                            if (dgvCostCenter.Rows[jCounter].Cells["CostCenterAccountID"].Value.ToInt32() == dgvJournalVoucher.Rows[iCounter].Cells["AccountID"].Value.ToInt32())
                                dlTempCCAmt += (dgvCostCenter.Rows[jCounter].Cells["Amount"].Value.ToDecimal());

                        if ((dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value.ToDecimal() + dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value.ToDecimal()) != dlTempCCAmt)
                        {
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 108, out MmessageIcon);
                            tslStatus.Text = MsMessageCommon.Split('#').Last();
                            tmGeneralReceiptsAndPayments.Enabled = true;
                            MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            blnIsValidated = false;
                            return false;
                        }
                    }
                }
            }
            blnIsValidated = true;
            return true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            btnSaveItem_Click(sender, e);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            btnSaveItem_Click(sender, e);
            if (blnIsValidated == true)
                this.Close();
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
            if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                if (MobjclsBLLJournalVoucher.DeleteJournalVoucher() == true)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                    tslStatus.Text = MsMessageCommon.Split('#').Last();
                    tmGeneralReceiptsAndPayments.Enabled = true;
                    MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    clearControls();
                }
            }
        }

        private void DisplayJournalVoucher()
        {
            if (MobjclsBLLJournalVoucher.DisplayJournalVoucher(txtVoucherNo.Tag.ToDecimal()))
            {
                txtVoucherNo.Tag = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID;
                cboCompany.SelectedValue = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.intCompanyID;
                cboCompany.Enabled = false;                
                cboVoucherType.SelectedValue = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.intVoucherTypeID;
                cboVoucherType.Enabled = false;
                
                dtpDate.Value = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dtpVoucherDate;
                txtVoucherNo.Text = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.strVoucherNo;
                txtNarration.Text = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.strRemarks;
                intJTypeID = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.intJournalTypeID;
                intOpTypeID = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.intOperationTypeID;
                intRefID = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.intReferenceID;

                if (intJTypeID == (int)JournalTypes.Recurring)
                    dtpDate.Enabled = false;
                else
                    dtpDate.Enabled = true;

                DataTable datTemp = new DataTable();
                if (MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID > 0)
                {
                    datTemp = MobjclsBLLJournalVoucher.DisplayJournalVoucherDetails(MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID);
                    dgvJournalVoucher.Rows.Clear();
                    for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                    {
                        dgvJournalVoucher.Rows.Add();
                        dgvJournalVoucher.Rows[iCounter].Cells["AccountID"].Value = (datTemp.Rows[iCounter]["AccountID"].ToString()).ToInt32();
                        dgvJournalVoucher.Rows[iCounter].Cells["Code"].Value = datTemp.Rows[iCounter]["Code"].ToString();
                        dgvJournalVoucher.Rows[iCounter].Cells["AccountName"].Value = datTemp.Rows[iCounter]["AccountName"].ToString();
                        dgvJournalVoucher.Rows[iCounter].Cells["SerialNo"].Value = (datTemp.Rows[iCounter]["SerialNo"].ToString()).ToInt32();
                        dgvJournalVoucher.Rows[iCounter].Cells["Remarks"].Value = datTemp.Rows[iCounter]["Remarks"].ToString();
                        dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value = null;
                        dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value = null;
                        if (datTemp.Rows[iCounter]["DebitAmount"].ToString() != "")
                            if (datTemp.Rows[iCounter]["DebitAmount"].ToDecimal() > 0)
                                dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value = (datTemp.Rows[iCounter]["DebitAmount"].ToString()).ToDecimal();
                        if (datTemp.Rows[iCounter]["CreditAmount"].ToString() != "")
                            if (datTemp.Rows[iCounter]["CreditAmount"].ToDecimal() > 0)
                                dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value = (datTemp.Rows[iCounter]["CreditAmount"].ToString()).ToDecimal();
                    }
                    CalcTotal();

                    datTemp = MobjclsBLLJournalVoucher.DisplayCostCenterDetails(MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dlVoucherID);
                    dgvCostCenter.Rows.Clear();

                    CostCenterID.DataSource = MobjclsBLLJournalVoucher.FillCombos(new string[] { "CostCenterID,CostCenter", "AccCostCenterReference", "ISNULL(CostCenterGroupID,0) <> 0 ORDER BY CostCenter" });
                    CostCenterID.ValueMember = "CostCenterID";
                    CostCenterID.DisplayMember = "CostCenter";

                    for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                    {
                        dgvCostCenter.Rows.Add();
                        dgvCostCenter.Rows[iCounter].Cells["CostCenterAccountID"].Value = (datTemp.Rows[iCounter]["CostCenterAccountID"].ToString()).ToInt32();
                        dgvCostCenter.Rows[iCounter].Cells["CostCenterID"].Value = (datTemp.Rows[iCounter]["CostCenterID"].ToString()).ToInt32();
                        dgvCostCenter.Rows[iCounter].Cells["Amount"].Value = (datTemp.Rows[iCounter]["Amount"].ToString()).ToDecimal();
                    }
                }

                if (MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.intBankAccountID > 0)
                    cboBankAccount.SelectedValue = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.intBankAccountID;
                else
                    cboBankAccount.SelectedIndex = -1;
                txtChequeNo.Text = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.strChNo;
                if (MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.strChNo != "")
                    dtpChequeDate.Value = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dtpChDate;
                else
                    dtpChequeDate.Value = MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.dtpVoucherDate;

                dgvJournalVoucher.ReadOnly = false;
                dgvCostCenter.ReadOnly = false;
                if (intJTypeID == (int)JournalTypes.Automated)
                {
                    btnSaveItem.Enabled = btnDeleteItem.Enabled = false;
                    btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
                    dgvJournalVoucher.ReadOnly = true;
                    dgvCostCenter.ReadOnly = true;
                }
                else if (intJTypeID == (int)JournalTypes.Recurring)
                {
                    btnSaveItem.Enabled = MblnAddUpdatePermission;
                    btnDeleteItem.Enabled = false;
                    btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
                }
                else
                {
                    if (MobjclsBLLJournalVoucher.PobjClsDTOJournalVoucher.blnIsPDC == true)
                    {
                        btnSaveItem.Enabled = false;
                        dgvJournalVoucher.ReadOnly = true;
                        dgvCostCenter.ReadOnly = true;
                    }
                    else
                    {
                        btnSaveItem.Enabled = MblnAddUpdatePermission;
                        dgvJournalVoucher.ReadOnly = false;
                        dgvCostCenter.ReadOnly = false;
                    }
                    btnDeleteItem.Enabled = MblnDeletePermission;
                    btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
                }
            }
        }

        private void CalcTotal()
        {
            txtDebitTotal.Text = txtCreditTotal.Text = "0";
            for (int iCounter = 0; iCounter <= dgvJournalVoucher.Rows.Count - 1; iCounter++)
            {
                if (dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value != null)
                    txtDebitTotal.Text = (txtDebitTotal.Text.ToDecimal() + (dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value.ToString()).ToDecimal()).ToString();
                else if (dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value != null)
                    txtCreditTotal.Text = (txtCreditTotal.Text.ToDecimal() + (dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value.ToString()).ToDecimal()).ToString();
            }
        }

        private void btnAddNewItem_Click(object sender, EventArgs e)
        {
            tmGeneralReceiptsAndPayments.Enabled = false;
            clearControls();

            intOpTypeID = 0;
            intRefID = 0;
            intJTypeID = 1;
            RecurringJournalSetUpID = 0;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            btnAddNewItem_Click(sender, e);
        }

        private void tmGeneralReceiptsAndPayments_Tick(object sender, EventArgs e)
        {
            tmGeneralReceiptsAndPayments.Enabled = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnChartofAccounts_Click(object sender, EventArgs e)
        {
            using (FrmChartOfAccounts objFrmChartOfAccounts = new FrmChartOfAccounts())
                objFrmChartOfAccounts.ShowDialog();
            //LoadAccounts();
            //dgvJournalVoucher.Rows.Clear();
            //dgvCostCenter.Rows.Clear();
        }

        private void dgvJournalVoucher_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (cboVoucherType.SelectedIndex >= 0)
                {
                    if (e.ColumnIndex == dgvJournalVoucher.Columns["AccountID"].Index)
                    {
                        DataTable datTemp = new DataTable();
                        string strTemp = "";
                        switch (cboVoucherType.SelectedValue.ToInt32())
                        {
                            case (int)VoucherType.Payment:
                                strTemp = "ISNULL(AccountGroupID,0) NOT IN (0," + (int)AccountGroups.Stockinhand + "," + (int)AccountGroups.SalesAccounts + "," +
                                "" + (int)AccountGroups.PurchaseAccounts + ") AND dbo.fnGetGroupAccountId(AccountID,0,0)<>" + (int)AccountGroups.Income +
                                " /*AND AccountID NOT IN (4,5)*/ AND AccountID <> 5 ORDER BY AccountName";
                                break;
                            case (int)VoucherType.Receipt:
                                strTemp = "ISNULL(AccountGroupID,0) NOT IN (0," + (int)AccountGroups.Stockinhand + "," + (int)AccountGroups.SalesAccounts + "," +
                               "" + (int)AccountGroups.PurchaseAccounts + ") AND dbo.fnGetGroupAccountId(AccountID,0,0)<>" + (int)AccountGroups.Expense +
                               " /*AND AccountID NOT IN (4,5)*/ AND AccountID <> 4 ORDER BY AccountName";
                                break;
                            case (int)VoucherType.Contra:
                                strTemp = "ISNULL(AccountGroupID,0) IN (" + (int)AccountGroups.Cashinhand + "," + (int)AccountGroups.BankAccounts + ") ORDER BY AccountName";
                                break;
                            case (int)VoucherType.Journal:
                                if (ClsMainSettings.JVAccDuplication)
                                    strTemp = "ISNULL(AccountGroupID,0) NOT IN (0," + (int)AccountGroups.Stockinhand + ") AND AccountID NOT IN (4,5) ORDER BY AccountName";
                                else
                                    strTemp = "ISNULL(AccountGroupID,0) NOT IN (0," + (int)AccountGroups.Cashinhand + "," + (int)AccountGroups.BankAccounts + "," +
                                    "" + (int)AccountGroups.Stockinhand + "," + (int)AccountGroups.SalesAccounts + "," +
                                    "" + (int)AccountGroups.PurchaseAccounts + ") AND AccountID NOT IN (4,5) ORDER BY AccountName";                                
                                break;
                            case (int)VoucherType.Purchase:
                            case (int)VoucherType.DebitNote:
                                strTemp = "ISNULL(AccountGroupID,0) IN (0," + (int)AccountGroups.Cashinhand + "," + (int)AccountGroups.BankAccounts + "," +
                                "" + (int)AccountGroups.PurchaseAccounts + "," +
                                "" + (int)AccountGroups.SundryCreditors + "," + (int)AccountGroups.SundryDebtors + ") ORDER BY AccountName";
                                break;
                            case (int)VoucherType.Sale:
                            case (int)VoucherType.CreditNote:
                                strTemp = "ISNULL(AccountGroupID,0) IN (0," + (int)AccountGroups.Cashinhand + "," + (int)AccountGroups.BankAccounts + "," +
                                "" + (int)AccountGroups.SalesAccounts + "," +
                                "" + (int)AccountGroups.SundryCreditors + "," + (int)AccountGroups.SundryDebtors + ") ORDER BY AccountName";
                                break;
                        }

                        LoadAccounts(strTemp);

                        if (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Contra)
                        {
                            dgvJournalVoucher.Rows[0].Cells["DebitAmount"].ReadOnly = false;
                            dgvJournalVoucher.Rows[0].Cells["CreditAmount"].ReadOnly = true;
                            if (e.RowIndex > 0)
                            {
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].ReadOnly = true;
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].ReadOnly = false;
                            }
                        }
                    }
                }
            }
        }

        private void dgvJournalVoucher_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (cboVoucherType.SelectedIndex >= 0)
                {
                    if ((e.ColumnIndex == dgvJournalVoucher.Columns["Code"].Index || 
                        e.ColumnIndex == dgvJournalVoucher.Columns["AccountName"].Index) &&
                        blnIsSelected == false)
                    {
                        dgvLedgerAccounts.Visible = true;
                        pnlLedgerAccounts.BringToFront();
                        pnlLedgerAccounts.Visible = true;
                        SetPanel(e.RowIndex, e.ColumnIndex);

                        if (e.ColumnIndex == dgvJournalVoucher.Columns["Code"].Index)
                        {
                            try { datLedgerAccounts.DefaultView.RowFilter = "LedgerCode like '%" + dgvJournalVoucher.Rows[e.RowIndex].Cells["Code"].Value.ToStringCustom() + "%'"; }
                            catch { }
                        }
                        else if (e.ColumnIndex == dgvJournalVoucher.Columns["AccountName"].Index)
                        {
                            try { datLedgerAccounts.DefaultView.RowFilter = "LedgerName like '%" + dgvJournalVoucher.Rows[e.RowIndex].Cells["AccountName"].Value.ToStringCustom() + "%'"; }
                            catch { }
                        }

                        dgvLedgerAccounts.DataSource = datLedgerAccounts.DefaultView.ToTable();
                    }
                    else if (blnIsSelected == false)
                    {
                        pnlLedgerAccounts.SendToBack();
                        pnlLedgerAccounts.Visible = false;
                    }

                    if (!ClsMainSettings.JVAccDuplication)
                    {
                        if (e.ColumnIndex == dgvJournalVoucher.Columns["AccountID"].Index)
                        {
                            for (int iCounter = 0; iCounter <= dgvJournalVoucher.Rows.Count - 2; iCounter++)
                            {
                                if (iCounter != e.RowIndex && dgvJournalVoucher.Rows[e.RowIndex].Cells["AccountID"].Value != null && dgvJournalVoucher.Rows[iCounter].Cells["AccountID"].Value.ToInt32() == dgvJournalVoucher.Rows[e.RowIndex].Cells["AccountID"].Value.ToInt32())
                                {
                                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 110, out MmessageIcon);
                                    tslStatus.Text = MsMessageCommon.Split('#').Last();
                                    tmGeneralReceiptsAndPayments.Enabled = true;
                                    MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    dgvJournalVoucher.Rows[e.RowIndex].Cells["AccountID"].Value = null;
                                }
                            }
                        }
                    }

                    if (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Payment && e.ColumnIndex == dgvJournalVoucher.Columns["AccountID"].Index)
                    {// Party A/c Dr to Cash A/c
                        if (MobjclsBLLJournalVoucher.CheckAccount(dgvJournalVoucher.Rows[e.RowIndex].Cells["AccountID"].Value.ToInt32()) == true)
                        {
                            if (e.RowIndex > 0)
                            {
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].Value = dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].Value = null;
                                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 110, out MmessageIcon);
                                tslStatus.Text = MsMessageCommon.Split('#').Last();
                                tmGeneralReceiptsAndPayments.Enabled = true;
                                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["AccountID"].Value = null;
                            }
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].ReadOnly = true;
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].ReadOnly = false;
                        }
                        else
                        {
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].ReadOnly = false;
                            if (Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal()) > 0)
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].Value = Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal());
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].ReadOnly = true;
                            if (e.RowIndex == 0)
                            {
                                if (dgvJournalVoucher.Rows[e.RowIndex].Cells["AccountID"].Value.ToInt32() != (int)Accounts.PDCIssued)
                                    dgvJournalVoucher.Rows[e.RowIndex].Cells["AccountID"].Value = (int)Accounts.CashAccount;
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].ReadOnly = true;
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].ReadOnly = false;
                                if (Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal()) > 0)
                                {
                                    dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].Value = Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal());
                                    dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].Value = null;
                                }
                            }
                        }
                    }
                    else if (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Receipt && e.ColumnIndex == dgvJournalVoucher.Columns["AccountID"].Index)
                    {// Cash A/c Dr to Party A/c
                        if (MobjclsBLLJournalVoucher.CheckAccount(dgvJournalVoucher.Rows[e.RowIndex].Cells["AccountID"].Value.ToInt32()) == true)
                        {
                            if (e.RowIndex > 0)
                            {
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].Value = dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].Value = null;
                                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 110, out MmessageIcon);
                                tslStatus.Text = MsMessageCommon.Split('#').Last();
                                tmGeneralReceiptsAndPayments.Enabled = true;
                                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["AccountID"].Value = null;
                            }
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].ReadOnly = false;
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].ReadOnly = true;
                        }
                        else
                        {
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].ReadOnly = true;
                            if (Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal()) > 0)
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].Value = Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal());
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].ReadOnly = false;
                            if (e.RowIndex == 0)
                            {
                                if (dgvJournalVoucher.Rows[e.RowIndex].Cells["AccountID"].Value.ToInt32() != (int)Accounts.PDCReceived)
                                    dgvJournalVoucher.Rows[e.RowIndex].Cells["AccountID"].Value = (int)Accounts.CashAccount;
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].ReadOnly = false;
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].ReadOnly = true;
                                if (Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal()) > 0)
                                {
                                    dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].Value = Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal());
                                    dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].Value = null;
                                }
                            }
                        }
                    }
                    else if (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Contra && e.ColumnIndex == dgvJournalVoucher.Columns["AccountID"].Index)
                    {
                        if (e.RowIndex > 0)
                        {
                            if (Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal()) > 0)
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].Value = Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal());
                        }
                    }
                    else if (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Journal && e.ColumnIndex == dgvJournalVoucher.Columns["AccountID"].Index)
                    {
                        if (e.RowIndex > 0)
                        {
                            if ((txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal()) > 0)
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].Value = Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal());
                            else
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].Value = Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal());
                        }
                    }
                    else if (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Journal && (e.ColumnIndex == dgvJournalVoucher.Columns["DebitAmount"].Index || e.ColumnIndex == dgvJournalVoucher.Columns["CreditAmount"].Index))
                    {
                        if (dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].Value != null)
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].Value = null;
                        else if (dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].Value != null)
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].Value = null;
                    }
                    else if ((cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Purchase || cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.CreditNote)
                        && e.ColumnIndex == dgvJournalVoucher.Columns["AccountID"].Index)
                    {// Purchase A/c or Sales Ret. A/c Dr to Cash A/c or Bank A/c or Party A/c
                        if (e.RowIndex == 0)
                        {
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].ReadOnly = false;
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].ReadOnly = true;
                        }
                        else
                        {
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].ReadOnly = true;
                            if (Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal()) > 0)
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].Value = Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal());
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].ReadOnly = false;
                        }
                    }
                    else if ((cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Sale || cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.DebitNote)
                        && e.ColumnIndex == dgvJournalVoucher.Columns["AccountID"].Index)
                    {// Cash A/c or Bank A/c or Party A/c Dr to Sale A/c or Purchase Ret. A/c
                        if (e.RowIndex == 0)
                        {
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].ReadOnly = true;
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].ReadOnly = false;
                        }
                        else
                        {
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].ReadOnly = false;
                            if (Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal()) > 0)
                                dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].Value = Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal());
                            dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].ReadOnly = true;
                        }
                    }
                }

                CalcTotal();

                if (cboVoucherType.SelectedIndex >= 0) // This step perform only after Total Calculation
                {
                    if (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Payment ||
                        cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Receipt ||
                        cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Contra ||
                        cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Journal)
                    {
                        if (e.ColumnIndex == dgvJournalVoucher.Columns["DebitAmount"].Index)
                        {
                            if (dgvJournalVoucher.Rows[e.RowIndex].Cells["DebitAmount"].Value != null && txtDebitTotal.Text.ToDecimal() > txtCreditTotal.Text.ToDecimal())
                            {
                                if (dgvJournalVoucher.Rows[0].Cells["CreditAmount"].Value != null)
                                    dgvJournalVoucher.Rows[0].Cells["CreditAmount"].Value = dgvJournalVoucher.Rows[0].Cells["CreditAmount"].Value.ToDecimal() + (Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal()));
                            }
                        }
                        else if (e.ColumnIndex == dgvJournalVoucher.Columns["CreditAmount"].Index)
                        {
                            if (dgvJournalVoucher.Rows[e.RowIndex].Cells["CreditAmount"].Value != null && txtDebitTotal.Text.ToDecimal() < txtCreditTotal.Text.ToDecimal())
                            {
                                if (dgvJournalVoucher.Rows[0].Cells["DebitAmount"].Value != null)
                                    dgvJournalVoucher.Rows[0].Cells["DebitAmount"].Value = dgvJournalVoucher.Rows[0].Cells["DebitAmount"].Value.ToDecimal() + (Math.Abs(txtDebitTotal.Text.ToDecimal() - txtCreditTotal.Text.ToDecimal()));
                            }
                        }
                    }
                }

                if (e.ColumnIndex == dgvJournalVoucher.Columns["AccountID"].Index)
                {
                    if (cboCompany.SelectedIndex >= 0)
                    {
                        decimal dlTempAmt = MobjclsBLLJournalVoucher.getAccountBalance(dgvJournalVoucher.Rows[e.RowIndex].Cells["AccountID"].Value.ToInt32(), cboCompany.SelectedValue.ToInt32());
                        if (dlTempAmt > 0)
                            lblSelectedAccountBalance.Text = Math.Abs(dlTempAmt).ToString() + " (Dr)";
                        else
                            lblSelectedAccountBalance.Text = Math.Abs(dlTempAmt).ToString() + " (Cr)";
                    }

                    dgvCostCenter.Rows.Clear();
                    string strTempAccounts = "";
                    for (int iCounter = 0; iCounter <= dgvJournalVoucher.Rows.Count - 1; iCounter++)
                        if (MobjclsBLLJournalVoucher.CheckAccountCostCenterApplicable(dgvJournalVoucher.Rows[iCounter].Cells["AccountID"].Value.ToInt32()) == true)
                            strTempAccounts += dgvJournalVoucher.Rows[iCounter].Cells["AccountID"].Value.ToString() + ",";
                    if (strTempAccounts.Length > 0)
                    {
                        strTempAccounts = strTempAccounts.Remove(strTempAccounts.Length - 1);
                        CostCenterAccountID.DataSource = MobjclsBLLJournalVoucher.FillCombos(new string[] { "AccountID,AccountName", "" +
                            "AccAccountMaster", "AccountID IN (" + strTempAccounts + ") AND ISNULL(AccountGroupID,0) <> 0 ORDER BY AccountName" });
                        CostCenterAccountID.ValueMember = "AccountID";
                        CostCenterAccountID.DisplayMember = "AccountName";
                    }

                    if (MobjclsBLLJournalVoucher.CheckAccountForCheque(dgvJournalVoucher.Rows[0].Cells["AccountID"].Value.ToInt32()) == true && 
                        (cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Payment || 
                        cboVoucherType.SelectedValue.ToInt32() == (int)VoucherType.Receipt))
                    {
                        txtChequeNo.Enabled = dtpChequeDate.Enabled = true;
                        txtChequeNo.BackColor = SystemColors.Info;
                    }
                    else
                    {
                        txtChequeNo.Enabled = dtpChequeDate.Enabled = false;
                        txtChequeNo.Text = "";
                        dtpChequeDate.Value = MobjclsBLLJournalVoucher.GetServerDate();
                        txtChequeNo.BackColor = SystemColors.Window;
                    }
                }
            }
        }

        private void SetPanel(int intRowIndex, int intColumnIndex)
        {
            int intX = 0;

            if (expnlLeft.Expanded)
                intX = PanelLeft.Width + expnlLeft.Width + dgvJournalVoucher.RowHeadersWidth;
            else
                intX = dgvJournalVoucher.RowHeadersWidth;

            if (intRowIndex > 10)
            {
                pnlLedgerAccounts.Location = new Point(intX, pnlBottom.Location.Y);
            }
            else
            {
                pnlLedgerAccounts.Location = new Point(intX,
                    bnGeneralReceiptsAndPayments.Location.Y + panelTop.Height + dgvJournalVoucher.ColumnHeadersHeight + 
                    dgvJournalVoucher.CurrentRow.Height * (intRowIndex + 3) + 6);
            }
        }

        private void dgvJournalVoucher_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private void dgvJournalVoucher_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (dgvJournalVoucher.ReadOnly == false)
            {
                CalcTotal();
                if (dgvJournalVoucher.Rows.Count > 0)
                    dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].ReadOnly = false;
            }
        }

        private void txtDebitTotal_TextChanged(object sender, EventArgs e)
        {
            if (dgvJournalVoucher.Rows.Count > 0)
            {
                dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].ReadOnly = false;
                if (txtDebitTotal.Text != "0" && txtCreditTotal.Text != "0")
                    if (dgvJournalVoucher.Rows.Count > 2 && txtDebitTotal.Text == txtCreditTotal.Text)
                        dgvJournalVoucher.Rows[dgvJournalVoucher.Rows.Count - 1].ReadOnly = true;
            }

            if (txtDebitTotal.Text != "0" || txtCreditTotal.Text != "0")
            {
                btnAddNewItem.Enabled = MblnAddPermission;
                btnSaveItem.Enabled = MblnAddUpdatePermission;
                if (txtVoucherNo.Tag.ToString() != "0")
                {
                    btnSaveItem.Enabled = MblnUpdatePermission;
                    btnDeleteItem.Enabled = MblnDeletePermission;
                    btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
                }
                else
                    btnDeleteItem.Enabled = btnPrint.Enabled = btnEmail.Enabled = false;
            }
        }

        private void dgvCostCenter_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                decimal dlTempAmount = 0;
                for (int iCounter = 0; iCounter <= dgvCostCenter.Rows.Count - 1; iCounter++)
                {
                    if (dgvCostCenter.Rows[iCounter].Cells["CostCenterAccountID"].Value.ToInt32() == dgvCostCenter.Rows[e.RowIndex].Cells["CostCenterAccountID"].Value.ToInt32())
                    {
                        dlTempAmount += (dgvCostCenter.Rows[iCounter].Cells["Amount"].Value.ToDecimal());
                    }
                }

                if (e.ColumnIndex == dgvCostCenter.Columns["CostCenterAccountID"].Index)
                {   
                    for (int iCounter = 0; iCounter <= dgvJournalVoucher.Rows.Count - 1; iCounter++)
                    {
                        if (dgvJournalVoucher.Rows[iCounter].Cells["AccountID"].Value.ToInt32() == dgvCostCenter.Rows[e.RowIndex].Cells["CostCenterAccountID"].Value.ToInt32())
                        {
                            if (dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value != null)
                                dgvCostCenter.Rows[e.RowIndex].Cells["Amount"].Value = dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value.ToDecimal() - dlTempAmount;
                            else if (dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value != null)
                                dgvCostCenter.Rows[e.RowIndex].Cells["Amount"].Value = dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value.ToDecimal() - dlTempAmount;
                        }
                    }
                }
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }
        private void LoadReport()
        {
            long VoucherID = txtVoucherNo.Tag.ToInt64();
            if (VoucherID > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = VoucherID;
                ObjViewer.PiFormID = (int)FormID.GeneralReceiptsAndPayments;
                ObjViewer.ShowDialog();
            }
        }

        private void DisplayRecurringJournalVoucher()
        {
            if (RecurringJournalSetUpID > 0)
            {
                DataTable datTemp = MobjclsBLLJournalVoucher.getRecurringVoucherDetails(RecurringJournalSetUpID);
                dgvJournalVoucher.Rows.Clear();
                for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                {
                    dgvJournalVoucher.Rows.Add();
                    dgvJournalVoucher.Rows[iCounter].Cells["AccountID"].Value = (datTemp.Rows[iCounter]["AccountID"].ToString()).ToInt32();
                    dgvJournalVoucher.Rows[iCounter].Cells["SerialNo"].Value = (datTemp.Rows[iCounter]["SerialNo"].ToString()).ToInt32();
                    dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value = null;
                    dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value = null;
                    if (datTemp.Rows[iCounter]["DebitAmount"].ToString() != "")
                        if (datTemp.Rows[iCounter]["DebitAmount"].ToDecimal() > 0)
                            dgvJournalVoucher.Rows[iCounter].Cells["DebitAmount"].Value = (datTemp.Rows[iCounter]["DebitAmount"].ToString()).ToDecimal();
                    if (datTemp.Rows[iCounter]["CreditAmount"].ToString() != "")
                        if (datTemp.Rows[iCounter]["CreditAmount"].ToDecimal() > 0)
                            dgvJournalVoucher.Rows[iCounter].Cells["CreditAmount"].Value = (datTemp.Rows[iCounter]["CreditAmount"].ToString()).ToDecimal();
                }
                CalcTotal();

                dgvJournalVoucher.Columns["AccountID"].ReadOnly = true;

                btnSaveItem.Enabled = MblnAddUpdatePermission;
                btnDeleteItem.Enabled = false;
                btnPrint.Enabled = btnEmail.Enabled = false;
            }
        }

        private void cboCompany_KeyDown(object sender, KeyEventArgs e)
        {
            ((ComboBox)sender).DroppedDown = false;
        }

        private void cboVoucherType_KeyDown(object sender, KeyEventArgs e)
        {
            ((ComboBox)sender).DroppedDown = false;
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            ShowEmail();
        }
        private void ShowEmail()
        {
            long lngVoucherID = txtVoucherNo.Tag.ToInt64();
            if (lngVoucherID > 0)
            {
                using (FrmEmailPopup objEmailPopUp = new FrmEmailPopup())
                {

                    objEmailPopUp.MsSubject = "Accounting Voucher";
                    objEmailPopUp.EmailFormType = EmailFormID.GeneralReceiptsAndPayments;
                    objEmailPopUp.EmailSource = MobjclsBLLJournalVoucher.DisplayGeneralReceiptsAndPayements(lngVoucherID);
                    objEmailPopUp.ShowDialog();
                }
            }
        }

        private void dgvJournalVoucher_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvJournalVoucher.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);            
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                ((DataGridViewComboBoxEditingControl)e.Control).BackColor = Color.White;
            }
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvJournalVoucher.CurrentCell.OwningColumn.Index == DebitAmount.Index ||
                dgvJournalVoucher.CurrentCell.OwningColumn.Index == CreditAmount.Index)
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
                
                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    int dotIndex = -1;
                    if (txt.Text.Contains("."))
                    {
                        dotIndex = txt.Text.IndexOf('.');
                    }
                    if (e.KeyChar == '.')
                    {
                        if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                        {
                            e.Handled = true;
                        }
                    }
                }
            }
        }

        private void CboSearchCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            ((ComboBox)sender).DroppedDown = false;
        }

        private void btnCostCenterReference_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Cost Center", new int[] { 2, 2 }, "CostCenterID,CostCenterGroupID,CostCenter", "AccCostCenterReference", "CostCenterGroupID = " + (int)CostCenterGroupReference.General);
                objCommon.ShowDialog();
                objCommon.Dispose();
            }
            catch
            { }
        }

        private void dgvLedgerAccounts_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvLedgerAccounts.CurrentRow != null && dgvLedgerAccounts.Focused)
                SelectLedgerAccounts(dgvLedgerAccounts.CurrentRow.Index);

            pnlLedgerAccounts.SendToBack();
            pnlLedgerAccounts.Visible = false;
                        
            dgvJournalVoucher.CurrentCell = dgvJournalVoucher[Remarks.Index, dgvJournalVoucher.Rows.Count - 2];
            DataGridViewCellEventArgs e1 = new DataGridViewCellEventArgs(Code.Index, dgvJournalVoucher.Rows.Count - 2);
            dgvJournalVoucher_CellValueChanged(sender, e1);
            pnlLedgerAccounts.SendToBack();
            pnlLedgerAccounts.Visible = false;
            dgvJournalVoucher.Focus();
        }

        private void SelectLedgerAccounts(int intRowindex)
        {
            try
            {
                if (dgvLedgerAccounts != null && dgvLedgerAccounts.Rows.Count > 0)
                {
                    blnIsSelected = true;
                    int intrIndex = dgvJournalVoucher.CurrentRow.Index;

                    if (Convert.ToString(dgvJournalVoucher.Rows[intrIndex].Cells["Code"].Value) == "" &&
                        Convert.ToString(dgvJournalVoucher.Rows[intrIndex].Cells["AccountName"].Value) == "")
                        intrIndex = intrIndex - 1;

                    dgvJournalVoucher.Rows[intrIndex].Cells["AccountID"].Value = dgvLedgerAccounts.Rows[intRowindex].Cells["LedgerAccountID"].Value;
                    dgvJournalVoucher.Rows[intrIndex].Cells["Code"].Value = dgvLedgerAccounts.Rows[intRowindex].Cells["LedgerCode"].Value;
                    dgvJournalVoucher.Rows[intrIndex].Cells["AccountName"].Value = dgvLedgerAccounts.Rows[intRowindex].Cells["LedgerName"].Value;
                    blnIsSelected = false;
                }
            }
            catch { }
        }

        private void dgvJournalVoucher_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvLedgerAccounts != null && dgvLedgerAccounts.Rows.Count > 0)
                {
                    dgvLedgerAccounts.Focus();
                    dgvLedgerAccounts.CurrentCell = dgvLedgerAccounts["LedgerCode", 0];
                }
            }
            catch { }
        }

        private void dgvJournalVoucher_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgvJournalVoucher.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dgvJournalVoucher_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            pnlLedgerAccounts.SendToBack();
            pnlLedgerAccounts.Visible = false;
        }

        private void expnlLeft_ExpandedChanged(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
        {
            try
            {
                if (dgvJournalVoucher.CurrentRow != null)
                    SetPanel(dgvJournalVoucher.CurrentRow.Index, 1);
            }
            catch { }
        }

        private void btnAddAutofill_Click(object sender, EventArgs e)
        {
            if (dgvJournalVoucher.Rows.Count > 0 && dgvJournalVoucher.CurrentRow.Index >= 0)
            {
                if (dgvJournalVoucher.CurrentCell.ColumnIndex == dgvJournalVoucher.Columns["Remarks"].Index)
                {
                    for (int iCounter = dgvJournalVoucher.CurrentRow.Index; iCounter <= dgvJournalVoucher.Rows.Count - 2; iCounter++)
                        dgvJournalVoucher.Rows[iCounter].Cells["Remarks"].Value = dgvJournalVoucher.Rows[dgvJournalVoucher.CurrentRow.Index].Cells["Remarks"].Value;
                }
            }
        }

        private void dgvLedgerAccounts_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                dgvLedgerAccounts_CellDoubleClick(sender, null);
        }
    }
}