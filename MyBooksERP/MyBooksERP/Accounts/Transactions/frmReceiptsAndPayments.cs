﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class frmReceiptsAndPayments : Form
    {
        public FrmMain objFrmTradingMain;
        int intCurrencyID = 0;
        long TotalRecordCnt, CurrentRecCnt;
        bool blnDeletePosssile = true;
        bool MblnAddPermission, MblnAddUpdatePermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission;

        public int intMenuID = 0;
        public int intModuleID = 0;
        int intCompID = 0, intOpTypeID = 0, intRAndPTypeID = 0, intVenID = 0;
        clsBLLReceiptsAndPayments MobjclsBLLReceiptsAndPayments = new clsBLLReceiptsAndPayments();
        clsBLLCommonUtility MobjClsBLLCommonUtility = new clsBLLCommonUtility();
        private clsMessage objUserMessage = null;
        public long lngRPID = 0;
        public bool blnTempIsReceipt = false;
        bool blnClearSearchData = false;
        bool blnOk = false;
        //public DataTable datAdvanceSearchedData = new DataTable();

        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.Payments);
                return this.objUserMessage;
            }
        }

        public frmReceiptsAndPayments()
        {
            InitializeComponent();
        }

        public frmReceiptsAndPayments(int intMode)
        {
            InitializeComponent();
            intMenuID = intMode;
        }

        public frmReceiptsAndPayments(int intMode, int intModule)
        {
            InitializeComponent();
            intMenuID = intMode;
            intModuleID = intModule;
        }

        public frmReceiptsAndPayments(int intMode, int intCmpID, int intOType)
        {
            InitializeComponent();
            intMenuID = intMode;
            intCompID = intCmpID;
            intOpTypeID = intOType;
        }

        public frmReceiptsAndPayments(int intMode, int intCmpID, int intOType, int intPType, int intCusID)
        {
            InitializeComponent();
            intMenuID = intMode;
            intCompID = intCmpID;
            intOpTypeID = intOType;
            intRAndPTypeID = intPType;
            intVenID = intCusID;
        }

        private void frmReceiptsAndPayments_Load(object sender, EventArgs e)
        {
            object objICO;
            blnClearSearchData = true;
            if (intMenuID == (int)eMenuID.Payment)
            {
                this.Text = "Payments";
                objICO = Properties.Resources.Payments1;
                lblCustomer.Text = "Supplier";
                MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.blnIsReceipt = false;
                SetPermissions((int)eMenuID.Payment);
                lblPaymentDate.Text = "Payment Date";
                lblPaymentNo.Text = "Payment No.";
                lblPaymentType.Text = "Payment Type";
            }
            else
            {
                this.Text = "Receipts";
                objICO = Properties.Resources.Receipts1;
                lblCustomer.Text = "Customer";
                MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.blnIsReceipt = true;
                SetPermissions((int)eMenuID.Receipt);
                lblPaymentDate.Text = "Receipt Date";
                lblPaymentNo.Text = "Receipt No.";
                lblPaymentType.Text = "Receipt Type";
            }

            tmrReceiptsPayments.Enabled = false;
            LoadCombos();
            this.Icon = ((System.Drawing.Icon)(objICO));
            ClearControls();

            if (lngRPID == 0)
            {
                if (intCompID > 0 && intOpTypeID > 0 && intRAndPTypeID > 0)
                {
                    cboCompany.SelectedValue = intCompID;
                    cboOperationType.SelectedValue = intOpTypeID;
                    cboPaymentType.SelectedValue = intRAndPTypeID;
                    if (intVenID > 0)
                        cboCustomer.SelectedValue = intVenID;
                    cboCompany.Enabled = cboOperationType.Enabled = cboPaymentType.Enabled = cboCustomer.Enabled = false;
                }
                else if (intCompID > 0 &&
                    intOpTypeID > 0)
                {
                    cboCompany.SelectedValue = intCompID;
                    cboOperationType.SelectedValue = intOpTypeID;
                    cboCompany.Enabled = cboOperationType.Enabled = false;
                }

                if (intModuleID > 0)
                {
                    LoadModuleOperationTypes();
                    cboCompany.Enabled = cboOperationType.Enabled = cboPaymentType.Enabled = cboCustomer.Enabled = true;
                }
            }
            else if (lngRPID > 0)
            {
                bindingNavigatorPositionItem.Text = MobjclsBLLReceiptsAndPayments.GetRowNumber(lngRPID, blnTempIsReceipt).ToString();
                bindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
                DisplayReceiptAndPayments();
                FillCurrentBalanceInTextBox();
            }
        }

        private void SetPermissions(int intMenuID)
        {
            // Function for setting permissions
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 2)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Accounts, intMenuID, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                if (MblnAddPermission == true || MblnUpdatePermission == true)
                    MblnAddUpdatePermission = true;
            }
            else
                MblnAddPermission = MblnAddUpdatePermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void LoadCombos()
        {
            cboCompany.DataSource = MobjclsBLLReceiptsAndPayments.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID + " ORDER BY CompanyName" });
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DisplayMember = "CompanyName";
            cboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;

            LoadOperationType();

            if (intMenuID == (int)eMenuID.Payment)
                cboCustomer.DataSource = MobjclsBLLReceiptsAndPayments.FillCombos(new string[] { "distinct V.VendorID,V.VendorName", "InvVendorInformation V INNER JOIN InvVendorCompanyDetails VC ON V.VendorID = VC.VendorID ", " V.VendorTypeID = " + (int)VendorType.Supplier + " AND VC.CompanyID = " + ClsCommonSettings.CompanyID + "  and V.StatusID=" + (int)VendorStatus.SupplierActive });
            else
                cboCustomer.DataSource = MobjclsBLLReceiptsAndPayments.FillCombos(new string[] { "distinct V.VendorID,V.VendorName", "InvVendorInformation V INNER JOIN InvVendorCompanyDetails VC ON V.VendorID = VC.VendorID ", " V.VendorTypeID = " + (int)VendorType.Customer + " AND VC.CompanyID = " + ClsCommonSettings.CompanyID + "  and V.StatusID=" + (int)VendorStatus.CustomerActive });
            cboCustomer.ValueMember = "VendorID";
            cboCustomer.DisplayMember = "VendorName";

            cboPaymentMode.DataSource = MobjclsBLLReceiptsAndPayments.FillCombos(new string[] { "TransactionTypeID,TransactionType", "TransactionTypeReference", "" });
            cboPaymentMode.DisplayMember = "TransactionType";
            cboPaymentMode.ValueMember = "TransactionTypeID";
            cboPaymentMode.SelectedValue = (int)PaymentModes.Cash;

            cboBankAccount.DataSource = MobjclsBLLReceiptsAndPayments.FillCombos(new string[] { "AccountID,AccountName", "AccAccountMaster", "AccountGroupID = " + (int)AccountGroups.BankAccounts });
            cboBankAccount.DisplayMember = "AccountName";
            cboBankAccount.ValueMember = "AccountID";
        }

        private void LoadOperationType()
        {
            if (intMenuID == (int)eMenuID.Payment)
            {
                cboOperationType.DataSource = MobjclsBLLReceiptsAndPayments.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference",
                "OperationTypeID In (" + (int)OperationType.PurchaseOrder + "," + (int)OperationType.PurchaseInvoice + "," + (int)OperationType.StockTransfer + ")"});
            }
            else
            {
                cboOperationType.DataSource = MobjclsBLLReceiptsAndPayments.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference",
                "OperationTypeID In (" + (int)OperationType.SalesOrder + "," + (int)OperationType.SalesInvoice + ")"});
            }
            cboOperationType.ValueMember = "OperationTypeID";
            cboOperationType.DisplayMember = "OperationType";
        }

        private void LoadModuleOperationTypes()
        {
            if (intModuleID == (int)ReceiptsAndPaymentsModes.PurchasePayments && intMenuID == (int)eMenuID.Payment)
            {
                cboOperationType.DataSource = MobjclsBLLReceiptsAndPayments.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference",
                "OperationTypeID In (" + (int)OperationType.PurchaseOrder + "," + (int)OperationType.PurchaseInvoice + ")"});
            }
            else if (intModuleID == (int)ReceiptsAndPaymentsModes.InventoryPayments && intMenuID == (int)eMenuID.Payment)
            {
                cboOperationType.DataSource = MobjclsBLLReceiptsAndPayments.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference",
                "OperationTypeID In (" + (int)OperationType.StockTransfer + ")"});
            }
            else if (intModuleID == (int)ReceiptsAndPaymentsModes.SalesReceipts && intMenuID == (int)eMenuID.Receipt)
            {
                cboOperationType.DataSource = MobjclsBLLReceiptsAndPayments.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference",
                "OperationTypeID In (" + (int)OperationType.SalesOrder + "," + (int)OperationType.SalesInvoice + ")"});
            }
            cboOperationType.ValueMember = "OperationTypeID";
            cboOperationType.DisplayMember = "OperationType";
        }

        private void ClearControls()
        {
            if (!blnClearSearchData)
            //datAdvanceSearchedData = new DataTable();
            blnClearSearchData = false;

            if (intModuleID > 0)
                LoadModuleOperationTypes();
            else
                LoadOperationType();
            cboOperationType.SelectedIndex = cboPaymentType.SelectedIndex = cboCustomer.SelectedIndex = -1;
            dtpPaymentDate.Value = dtpChequeDate.Value = MobjclsBLLReceiptsAndPayments.GetServerDate();
            GetPaymentNo();
            txtPaymentNo.Tag = 0;
            cboBankAccount.SelectedIndex = -1;
            txtAmount.Text = "0";
            txtBillAmount.Text = "0";
            txtCurrentBalance.Text = "0";
            PositionTextBoxes(306, 411, 516);
            //PositionTextBoxes(282, 348, 441, 516);
            txtChequeNo.Text = lblAmountinWords.Text = txtDescription.Text = "";
            RecordCount();
            bindingNavigatorCountItem.Text = "of " + (TotalRecordCnt + 1).ToString();
            bindingNavigatorPositionItem.Text = (TotalRecordCnt + 1).ToString();
            cboCompany.Enabled = cboOperationType.Enabled = cboPaymentType.Enabled = cboCustomer.Enabled = btnCustomerSearch.Enabled = dtpPaymentDate.Enabled = true;

            dgvPaymentDetails.ReadOnly = false;
            bnAddNewItem.Enabled = MblnAddPermission;
            btnSave.Enabled = bnSaveItem.Enabled = btnOk.Enabled = false;
            bnDeleteItem.Enabled = bnPrint.Enabled = bnEmail.Enabled = false;

            blnDeletePosssile = true;
            tmrReceiptsPayments.Enabled = false;
            erproRP.Clear();
        }

        private void ClearControlsPartially()
        {
            cboCustomer.SelectedIndex = -1;
            txtAmount.Text = "0";
            txtCurrentBalance.Text = "0";
            txtBillAmount.Text = "0";
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearControlsPartially();
            intCurrencyID = 0;
            if (cboCompany.SelectedIndex >= 0)
            {
                MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intCompanyID = cboCompany.SelectedValue.ToInt32();
                GetPaymentNo();
                intCurrencyID = MobjclsBLLReceiptsAndPayments.GetCompanyCurrency();

                if (intMenuID == (int)eMenuID.Payment)
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.blnIsReceipt = false;
                else
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.blnIsReceipt = true;
            }
        }

        private void GetPaymentNo()
        {
            txtPaymentNo.Text = "";
            if (cboCompany.SelectedIndex >= 0)
                txtPaymentNo.Text = MobjclsBLLReceiptsAndPayments.GetPaymentNo();
        }

        private void cboOperationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearControlsPartially();
            cboPaymentType.DataSource = null;
            DataTable datTemp = new DataTable();
            if (cboOperationType.SelectedIndex >= 0)
            {
                int intSelectedValue = cboOperationType.SelectedValue.ToInt32();
                string strTemp = "";
                cboCustomer.Enabled = true;
                switch (intSelectedValue)
                {
                    case (Int32)OperationType.PurchaseOrder:
                        //strTemp = "ReceiptAndPaymentTypeID IN (" + (int)PaymentTypes.AdvancePayment + "," + (int)PaymentTypes.DirectExpense + ")";
                        strTemp = "ReceiptAndPaymentTypeID IN (" + (int)PaymentTypes.AdvancePayment + ")";
                        break;
                    case (Int32)OperationType.PurchaseInvoice:
                        //strTemp = "ReceiptAndPaymentTypeID IN (" + (int)PaymentTypes.InvoicePayment + "," + (int)PaymentTypes.DirectExpense + ")";
                        strTemp = "ReceiptAndPaymentTypeID IN (" + (int)PaymentTypes.InvoicePayment + "," + (int)PaymentTypes.DirectExpense + ")";
                        break;
                    case (Int32)OperationType.SalesOrder:
                        strTemp = "ReceiptAndPaymentTypeID IN (" + (int)PaymentTypes.AdvancePayment + ")";
                        break;
                    case (Int32)OperationType.SalesInvoice:
                        //strTemp = "ReceiptAndPaymentTypeID IN (" + (int)PaymentTypes.InvoicePayment + "," + (int)PaymentTypes.DirectExpense + ")";
                        strTemp = "ReceiptAndPaymentTypeID IN (" + (int)PaymentTypes.InvoicePayment + "," + (int)PaymentTypes.DirectExpense + ")";
                        break;
                    case (Int32)OperationType.POS:
                        strTemp = "ReceiptAndPaymentTypeID = " + (int)PaymentTypes.InvoicePayment;
                        cboCustomer.Enabled = false;
                        break;
                    case (Int32)OperationType.StockTransfer:
                        //strTemp = "ReceiptAndPaymentTypeID IN (" + (int)PaymentTypes.InvoiceExpense + "," + (int)PaymentTypes.DirectExpense + ")";
                        strTemp = "ReceiptAndPaymentTypeID IN (" + (int)PaymentTypes.InvoiceExpense + ")";
                        break;
                }
                if (strTemp != "")
                {
                    datTemp = MobjclsBLLReceiptsAndPayments.FillCombos(new string[] { "ReceiptAndPaymentTypeID,ReceiptAndPaymentType", "InvReceiptAndPaymentTypeReference", strTemp });
                    switch (intSelectedValue)
                    {
                        case (Int32)OperationType.PurchaseInvoice:
                            datTemp.Rows[0]["ReceiptAndPaymentType"] = "Purchase Payment";
                            datTemp.Rows[1]["ReceiptAndPaymentType"] = "Expense";
                           
                            break;
                        case (Int32)OperationType.SalesOrder:
                            datTemp.Rows[0]["ReceiptAndPaymentType"] = "Advance Receipt";
                            break;
                        case (Int32)OperationType.SalesInvoice:
                            datTemp.Rows[0]["ReceiptAndPaymentType"] = "Sales Receipt";
                            datTemp.Rows[1]["ReceiptAndPaymentType"] = "Expense";                           
                            break;
                    }
                   
                }
            }
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {

                    if (!ClsMainSettings.ExpensePosted)
                        if (datTemp.Rows.Count > 1)
                            datTemp.Rows.RemoveAt(1);

                    cboPaymentType.DataSource = datTemp;
                }
            }
            cboPaymentType.DisplayMember = "ReceiptAndPaymentType";
            cboPaymentType.ValueMember = "ReceiptAndPaymentTypeID";
        }

        private void cboPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearControlsPartially();
            if (cboOperationType.SelectedIndex >= 0)
            {
                if (cboOperationType.SelectedValue.ToInt32() == (int)OperationType.StockTransfer)
                {
                    dgvPaymentDetails.Rows.Clear();
                    FillGrid();
                }
            }
        }

        private void dtpPaymentDate_ValueChanged(object sender, EventArgs e)
        {
            ClearControlsPartially();
        }

        private void cboCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvPaymentDetails.Rows.Clear();
            if (cboOperationType.SelectedIndex >= 0)
            {
                int intSelectedValue = cboOperationType.SelectedValue.ToInt32();
                if (cboCustomer.SelectedIndex >= 0)
                {
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intVendorID = cboCustomer.SelectedValue.ToInt32();
                    switch (intSelectedValue)
                    {
                        case (Int32)OperationType.PurchaseOrder:
                        case (Int32)OperationType.PurchaseInvoice:
                            intCurrencyID = 0;
                            intCurrencyID = MobjclsBLLReceiptsAndPayments.GetSupplierCurrency();

                            if (cboPaymentType.SelectedValue.ToInt32() == (int)PaymentTypes.DirectExpense && cboCompany.SelectedIndex >= 0)
                            {
                                MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intCompanyID = cboCompany.SelectedValue.ToInt32();
                                intCurrencyID = MobjclsBLLReceiptsAndPayments.GetCompanyCurrency();
                            }
                            break;
                    }

                    FillGrid();
                    FillCurrentBalanceInTextBox();

                    if (dgvPaymentDetails.Rows.Count > 5)
                    {
                        PositionTextBoxes(286, 391, 496);
                        //PositionTextBoxes(262, 328, 421, 496);
                    }
                    else
                    {
                        PositionTextBoxes(306, 411, 516);
                        //PositionTextBoxes(282, 348, 441, 516);
                    }                    
                }
            }
        }

        private void FillCurrentBalanceInTextBox()
        {
            decimal decCurrentBalance = 0;
            decimal decBillAmount = 0;
            if (dgvPaymentDetails.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvPaymentDetails.Rows)
                {
                    decCurrentBalance += row.Cells["CurrentBalance"].Value.ToDecimal();
                    decBillAmount += row.Cells["BillAmount"].Value.ToDecimal();
                }
                txtCurrentBalance.Text = decCurrentBalance.ToString();
                txtBillAmount.Text = decBillAmount.ToString();
            }
            else
            {
                txtCurrentBalance.Text = 0.ToString();
                txtBillAmount.Text = 0.ToString();
            }
        }

        private void PositionTextBoxes(int BillAmountPostion,int BalanceAmountPosition,int AmountPosition)
        {
            txtBillAmount.Left = BillAmountPostion;
            txtCurrentBalance.Left = BalanceAmountPosition;
            txtAmount.Left = AmountPosition;
        }

        private void PositionTextBoxes(int lblChNoPostion, int txtChNoPosition, int lblChDatePosition,int dtpChDatePosition)
        {
            lblChequeNo.Left = lblChNoPostion;
            txtChequeNo.Left = txtChNoPosition;
            lblChequeDate.Left = lblChDatePosition;
            dtpChequeDate.Left = dtpChDatePosition;
        }

        private void cboPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboBankAccount.Enabled = txtChequeNo.Enabled = dtpChequeDate.Enabled = false;
            cboBankAccount.BackColor = txtChequeNo.BackColor = System.Drawing.SystemColors.Window;
            if (cboPaymentMode.SelectedIndex >= 0)
            {
                if (cboPaymentMode.SelectedValue.ToInt32() == (int)PaymentModes.Bank)
                {
                    cboBankAccount.Enabled = txtChequeNo.Enabled = dtpChequeDate.Enabled = true;
                    cboBankAccount.BackColor = txtChequeNo.BackColor = System.Drawing.SystemColors.Info;
                }
                else
                {
                    cboBankAccount.SelectedIndex = -1;
                    txtChequeNo.Text = "";
                    dtpChequeDate.Value = ClsCommonSettings.GetServerDate();
                }
            }
        }

        private void FillGrid()        
        {
            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intCompanyID = cboCompany.SelectedValue.ToInt32();
            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intOperationTypeID = cboOperationType.SelectedValue.ToInt32();
            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReceiptAndPaymentTypeID = cboPaymentType.SelectedValue.ToInt32();
            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.dtpPaymentDate = dtpPaymentDate.Value.ToString("dd MMM yyyy").ToDateTime();
            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intVendorID = cboCustomer.SelectedValue.ToInt32();
            
            DataTable datTemp = MobjclsBLLReceiptsAndPayments.getReferenceIDDetails();
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                    {
                        MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReferenceID = datTemp.Rows[iCounter]["ReferenceID"].ToInt64();
                        decimal decTempAmt = MobjclsBLLReceiptsAndPayments.getBalanceAmount();

                        //Sales Invoice & Purchase Invoice
                        if (cboOperationType.SelectedValue.ToInt32() == 9 || cboOperationType.SelectedValue.ToInt32()==5)
                        {                            
                            DataTable datTempforIDsandAmount = MobjclsBLLReceiptsAndPayments.GetAllInvoiceIDandAmount();
                          
                            if (datTempforIDsandAmount.Rows.Count > 0)
                            {
                                decimal decAdvanceAmt = MobjclsBLLReceiptsAndPayments.GetAdvanceAmountByInvoiceID();
                                for (int jCounter = 0; jCounter <= datTempforIDsandAmount.Rows.Count - 1; jCounter++)
                                {
                                    if (datTempforIDsandAmount.Rows[jCounter]["ReferenceID"].ToInt64() != MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReferenceID)
                                        decAdvanceAmt = decAdvanceAmt - datTempforIDsandAmount.Rows[jCounter]["Amount"].ToDecimal();
                                    else
                                    {
                                        if (decAdvanceAmt > 0)
                                        {
                                            decTempAmt = decTempAmt - decAdvanceAmt;
                                            decAdvanceAmt = decAdvanceAmt - datTempforIDsandAmount.Rows[jCounter]["Amount"].ToDecimal();
                                        }
                                        break;
                                    }
                                }
                            }
                        }                        

                        if (decTempAmt > 0)
                        {
                            dgvPaymentDetails.Rows.Add();
                            dgvPaymentDetails.Rows[dgvPaymentDetails.Rows.Count - 1].Cells["ReferenceID"].Value = datTemp.Rows[iCounter]["ReferenceID"].ToInt64();
                            dgvPaymentDetails.Rows[dgvPaymentDetails.Rows.Count - 1].Cells["ReferenceNo"].Value = datTemp.Rows[iCounter]["ReferenceNo"];
                            dgvPaymentDetails.Rows[dgvPaymentDetails.Rows.Count - 1].Cells["BillAmount"].Value = MobjclsBLLReceiptsAndPayments.getBillAmount();
                            dgvPaymentDetails.Rows[dgvPaymentDetails.Rows.Count - 1].Cells["CurrentBalance"].Value = decTempAmt;
                            dgvPaymentDetails.Rows[dgvPaymentDetails.Rows.Count - 1].Cells["ActualAmount"].Value = 0;
                            dgvPaymentDetails.Rows[dgvPaymentDetails.Rows.Count - 1].Cells["Amount"].Value = 0;
                        }
                    }
                }
            }
        }

        private void bnSaveItem_Click(object sender, EventArgs e)
        {
            blnOk = false;
            if (SaveReceiptAndPayment())
            {
                tsStatus.Text = this.UserMessage.GetMessageByCode(2);
                tmrReceiptsPayments.Enabled = true;
                this.UserMessage.ShowMessage(2); // Saved Successfully

                RecordCount();
                DisplayReceiptAndPayments();
                FillCurrentBalanceInTextBox();
                blnOk = true;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            bnSaveItem_Click(sender, e);
            if (blnOk == true)
                this.Close();
        }

        private bool SaveReceiptAndPayment()
        {
            bool blnStatus = false;
            if (FormValidation())
            {
                if (this.UserMessage.ShowMessage(1) == true) // Save
                {
                    if (txtPaymentNo.Text != "")
                    {
                        MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.strPaymentNumber = txtPaymentNo.Text;
                        if (txtPaymentNo.Tag.ToInt64() == 0)
                        {   
                            if (MobjclsBLLReceiptsAndPayments.DuplicatePaymentNo() > 0)
                            {
                                tsStatus.Text = this.UserMessage.GetMessageByCode(31);
                                tmrReceiptsPayments.Enabled = true;
                                erproRP.SetError(txtPaymentNo, tsStatus.Text);
                                this.UserMessage.ShowMessage(31, txtPaymentNo);
                                return false;
                            }
                        }
                        else
                        {
                            if (MobjclsBLLReceiptsAndPayments.DuplicatePaymentNo(txtPaymentNo.Tag.ToInt64()) > 0)
                            {
                                tsStatus.Text = this.UserMessage.GetMessageByCode(31);
                                tmrReceiptsPayments.Enabled = true;
                                erproRP.SetError(txtPaymentNo, tsStatus.Text);
                                this.UserMessage.ShowMessage(31, txtPaymentNo);
                                return false;
                            }
                        }
                    }
                    //DataTable datReferenceID;
                    //switch (cboOperationType.SelectedValue.ToInt32())
                    //{
                    //    case (int)OperationType.PurchaseOrder:
                    //        {
                    //            for (int iCounter = 0; iCounter <= dgvPaymentDetails.Rows.Count - 1; iCounter++)
                    //            {
                    //                if (dgvPaymentDetails.Rows[iCounter].Cells["Amount"].Value.ToDecimal() > 0)
                    //                {
                    //                    datReferenceID = MobjclsBLLReceiptsAndPayments.FillCombos(new string[] { "PurchaseInvoiceID", "InvPurchaseInvoiceMaster", "PurchaseInvoiceID = " + dgvPaymentDetails.Rows[iCounter].Cells["ReferenceID"].Value.ToInt32() });
                    //                }
                    //            }
                    //            break;
                    //        }
                    //    case (int)OperationType.PurchaseInvoice:
                    //        {
                    //            break;
                    //        }
                    //    case (int)OperationType.SalesOrder:
                    //        {
                    //            break;
                    //        }
                    //    case (int)OperationType.SalesInvoice:
                    //        {
                    //            break;
                    //        }
                    //}

                    if (txtPaymentNo.Tag.ToInt64() > 0)
                        MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReceiptAndPaymentID = txtPaymentNo.Tag.ToInt64();
                    else
                        MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReceiptAndPaymentID = 0;
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.strPaymentNumber = txtPaymentNo.Text;
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.dtpPaymentDate = dtpPaymentDate.Value.ToString("dd MMM yyyy").ToDateTime();
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intOperationTypeID = cboOperationType.SelectedValue.ToInt32();
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReceiptAndPaymentTypeID = cboPaymentType.SelectedValue.ToInt32();
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intTransactionTypeID = cboPaymentMode.SelectedValue.ToInt32();
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.strChequeNumber = txtChequeNo.Text;
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.dtpChequeDate = dtpChequeDate.Value.ToString("dd MMM yyyy").ToDateTime();
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.decTotalAmount = txtAmount.Text.ToDecimal();
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intCurrencyID = intCurrencyID;
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intCompanyID = cboCompany.SelectedValue.ToInt32();
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intCreatedBy = ClsCommonSettings.UserID;
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.dtpCreatedDate = MobjclsBLLReceiptsAndPayments.GetServerDate();
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.strRemarks = txtDescription.Text;
                    if (intMenuID == (int)eMenuID.Payment)
                        MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.blnIsReceipt = false;
                    else
                        MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.blnIsReceipt = true;
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.blnIsDirect = false;
                    MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intAccountID = cboBankAccount.SelectedValue.ToInt32();
                    if (cboCustomer.SelectedIndex >= 0)
                        MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intVendorID = cboCustomer.SelectedValue.ToInt32();

                    DataTable datDetails = new DataTable();
                    datDetails.Columns.Add("ReferenceID");
                    datDetails.Columns.Add("Amount");

                    for (int iCounter = 0; iCounter <= dgvPaymentDetails.Rows.Count - 1; iCounter++)
                    {
                        if (dgvPaymentDetails.Rows[iCounter].Cells["Amount"].Value.ToDecimal() > 0)
                        {
                            datDetails.Rows.Add();
                            datDetails.Rows[datDetails.Rows.Count - 1]["ReferenceID"] = dgvPaymentDetails.Rows[iCounter].Cells["ReferenceID"].Value;
                            datDetails.Rows[datDetails.Rows.Count - 1]["Amount"] = dgvPaymentDetails.Rows[iCounter].Cells["Amount"].Value;
                        }
                    }

                    if (!MobjclsBLLReceiptsAndPayments.IsExistAllReferenceIDs(datDetails))
                    {
                        tsStatus.Text = this.UserMessage.GetMessageByCode(9016);
                        tmrReceiptsPayments.Enabled = true;
                        this.UserMessage.ShowMessage(9016); // Not Saved.
                        return false;
                    }

                    if (MobjclsBLLReceiptsAndPayments.SaveReceiptsAndPayments(datDetails))
                    {
                        blnStatus = true;

                        if (this.cboOperationType.SelectedValue.ToInt32() == (Int32)OperationType.SalesOrder)
                        {
                            objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.SalesOrderAdvancePaid;
                            objFrmTradingMain.tmrAlert.Enabled = true;
                        }
                        if (this.cboOperationType.SelectedValue.ToInt32() == (Int32)OperationType.SalesInvoice && this.cboPaymentType.SelectedValue.ToInt32() == 2)//Sales receipt
                        {
                            objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.Receipt;
                            objFrmTradingMain.tmrAlert.Enabled = true; 
                        }
                    }
                }
            }
            return blnStatus;
        }

        private bool FormValidation()
        {
            erproRP.Clear();
            bool blnStatus = true;
            if (cboCompany.SelectedIndex == -1)
            {
                tsStatus.Text = this.UserMessage.GetMessageByCode(14);
                tmrReceiptsPayments.Enabled = true;
                erproRP.SetError(cboCompany, tsStatus.Text);
                this.UserMessage.ShowMessage(14, cboCompany);
                return false;
            }
            if (cboOperationType.SelectedIndex == -1)
            {
                tsStatus.Text = this.UserMessage.GetMessageByCode(7302);
                tmrReceiptsPayments.Enabled = true;
                erproRP.SetError(cboOperationType, tsStatus.Text);
                this.UserMessage.ShowMessage(7302, cboOperationType);
                return false;
            }
            if (cboPaymentType.SelectedIndex == -1)
            {
                tsStatus.Text = this.UserMessage.GetMessageByCode(7303);
                tmrReceiptsPayments.Enabled = true;
                erproRP.SetError(cboPaymentType, tsStatus.Text);
                this.UserMessage.ShowMessage(7303, cboPaymentType);
                return false;
            }
            if (dtpPaymentDate.Value.Date > ClsCommonSettings.GetServerDate())
            {
                tsStatus.Text = this.UserMessage.GetMessageByCode(9006);
                tmrReceiptsPayments.Enabled = true;
                erproRP.SetError(dtpPaymentDate, tsStatus.Text);
                this.UserMessage.ShowMessage(9006, dtpPaymentDate);
                return false;
            }
            //clsBLLJournalVoucher MobjclsBLLJournalVoucher = new clsBLLJournalVoucher();
            //if (cboPaymentMode.SelectedValue.ToInt32() == (int)PaymentModes.Cash && intMenuID == (int)eMenuID.Payment)
            //{

            //    if (((MobjclsBLLJournalVoucher.getAccountBalance((int)Accounts.CashAccount, cboCompany.SelectedValue.ToInt32())) - Convert.ToDecimal(txtAmount.Text)) < 0 && cboPaymentMode.SelectedValue.ToInt32() == (int)PaymentModes.Cash)
            //    {
            //        //MsMessageCommon = "There is no Enough cash balance";

            //        tsStatus.Text = this.UserMessage.GetMessageByCode(2278);
            //        tmrReceiptsPayments.Enabled = true;
            //        erproRP.SetError(cboPaymentMode, tsStatus.Text);
            //        this.UserMessage.ShowMessage(2278, cboPaymentMode);
            //        return false;


            //    }

            //}
            //else if (cboPaymentMode.SelectedValue.ToInt32() == (int)PaymentModes.Bank && intMenuID == (int)eMenuID.Payment) 
            //{
            //    if (((MobjclsBLLJournalVoucher.getAccountBalance(cboBankAccount.SelectedValue.ToInt32(), cboCompany.SelectedValue.ToInt32())) - Convert.ToDecimal(txtAmount.Text)) < 0)
            //    {
            //        if (Convert.ToDateTime(dtpChequeDate.Text) <= Convert.ToDateTime(dtpPaymentDate.Text))
            //        {
            //            //MsMessageCommon = "There is no Enough cash balance";

            //            tsStatus.Text = this.UserMessage.GetMessageByCode(2278);
            //            tmrReceiptsPayments.Enabled = true;
            //            erproRP.SetError(cboPaymentMode, tsStatus.Text);
            //            this.UserMessage.ShowMessage(2278, cboPaymentMode);
            //            return false;
            //        }
            //    }
            //}
            if (txtPaymentNo.Text == "")
            {
                tsStatus.Text = this.UserMessage.GetMessageByCode(7301);
                tmrReceiptsPayments.Enabled = true;
                erproRP.SetError(txtPaymentNo, tsStatus.Text);
                this.UserMessage.ShowMessage(7301, txtPaymentNo);
                return false;
            }
            else
            {
                MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.strPaymentNumber = txtPaymentNo.Text;
                if (txtPaymentNo.Tag.ToInt64() == 0)
                {
                    if (MobjclsBLLReceiptsAndPayments.DuplicatePaymentNo() > 0)
                    {
                        tsStatus.Text = this.UserMessage.GetMessageByCode(31);
                        tmrReceiptsPayments.Enabled = true;
                        erproRP.SetError(txtPaymentNo, tsStatus.Text);
                        this.UserMessage.ShowMessage(31, txtPaymentNo);
                        return false;
                    }
                }
                else
                {
                    if (MobjclsBLLReceiptsAndPayments.DuplicatePaymentNo(txtPaymentNo.Tag.ToInt64()) > 0)
                    {
                        tsStatus.Text = this.UserMessage.GetMessageByCode(31);
                        tmrReceiptsPayments.Enabled = true;
                        erproRP.SetError(txtPaymentNo, tsStatus.Text);
                        this.UserMessage.ShowMessage(31, txtPaymentNo);
                        return false;
                    }
                }
            }
            if (cboOperationType.SelectedValue.ToInt32() != (int)OperationType.StockTransfer &&
                cboCustomer.SelectedIndex == -1)
            {
                tsStatus.Text = this.UserMessage.GetMessageByCode(7305);
                tmrReceiptsPayments.Enabled = true;
                erproRP.SetError(cboCustomer, tsStatus.Text);
                this.UserMessage.ShowMessage(7305, cboCustomer);
                return false;
            }
            if (dgvPaymentDetails.Rows.Count == 0)
            {
                tsStatus.Text = this.UserMessage.GetMessageByCode(7310);
                tmrReceiptsPayments.Enabled = true;
                this.UserMessage.ShowMessage(7310);
                return false;
            }
            else
            {
                if (txtAmount.Text.ToDecimal() == 0)
                {
                    tsStatus.Text = this.UserMessage.GetMessageByCode(7310);
                    tmrReceiptsPayments.Enabled = true;
                    this.UserMessage.ShowMessage(7310);
                    return false;
                }
            }
            if (cboPaymentMode.SelectedIndex == -1)
            {
                tsStatus.Text = this.UserMessage.GetMessageByCode(7304);
                tmrReceiptsPayments.Enabled = true;
                erproRP.SetError(cboPaymentMode, tsStatus.Text);
                this.UserMessage.ShowMessage(7304, cboPaymentMode);
                return false;
            }
            else
            {
                if (cboPaymentMode.SelectedValue.ToInt32() == (int)PaymentModes.Bank)
                {
                    if (cboBankAccount.SelectedIndex == -1)
                    {
                        tsStatus.Text = this.UserMessage.GetMessageByCode(18);
                        tmrReceiptsPayments.Enabled = true;
                        erproRP.SetError(cboBankAccount, tsStatus.Text);
                        this.UserMessage.ShowMessage(18, cboBankAccount);
                        return false;
                    }

                    if (txtChequeNo.Text == "")
                    {
                        tsStatus.Text = this.UserMessage.GetMessageByCode(7306);
                        tmrReceiptsPayments.Enabled = true;
                        erproRP.SetError(txtChequeNo, tsStatus.Text);
                        this.UserMessage.ShowMessage(7306, txtChequeNo);
                        return false;
                    }
                    else
                    {
                        MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.strChequeNumber = txtChequeNo.Text;
                        if (txtPaymentNo.Tag.ToInt64() == 0)
                        {
                            if (MobjclsBLLReceiptsAndPayments.DuplicateChequeNo(0) > 0)
                            {
                                tsStatus.Text = this.UserMessage.GetMessageByCode(31);
                                tmrReceiptsPayments.Enabled = true;
                                erproRP.SetError(txtChequeNo, tsStatus.Text);
                                this.UserMessage.ShowMessage(31, txtChequeNo);
                                return false;
                            }
                        }
                        else
                        {
                            if (MobjclsBLLReceiptsAndPayments.DuplicateChequeNo(txtPaymentNo.Tag.ToInt64()) > 0)
                            {
                                tsStatus.Text = this.UserMessage.GetMessageByCode(31);
                                tmrReceiptsPayments.Enabled = true;
                                erproRP.SetError(txtChequeNo, tsStatus.Text);
                                this.UserMessage.ShowMessage(31, txtChequeNo);
                                return false;
                            }
                        }
                    }
                }
            }

            clsBLLPurchase MobjClsBLLPurchase = new clsBLLPurchase();
            MobjClsBLLPurchase.PobjClsDTOPurchase.intCompanyID = cboCompany.SelectedValue.ToInt32();
            //if (intMenuID == (int)eMenuID.Payment)
            //{
            //    if (!MobjClsBLLPurchase.GetCompanyAccount(Convert.ToInt32(TransactionTypes.Payment)))
            //    {
            //        tsStatus.Text = this.UserMessage.GetMessageByCode(7321);
            //        tmrReceiptsPayments.Enabled = true;
            //        this.UserMessage.ShowMessage(7321);
            //        return false;
            //    }
            //}
            //else if (intMenuID == (int)eMenuID.Receipt)
            //{
            //    if (!MobjClsBLLPurchase.GetCompanyAccount(Convert.ToInt32(TransactionTypes.Receipts)))
            //    {
            //        tsStatus.Text = this.UserMessage.GetMessageByCode(7322);
            //        tmrReceiptsPayments.Enabled = true;
            //        this.UserMessage.ShowMessage(7322);
            //        return false;
            //    }
            //}
            if (intMenuID == (int)eMenuID.Payment)
            {
                if (txtPaymentNo.Tag.ToInt64() > 0)
                {
                    DataTable datPreviousDetails = MobjclsBLLReceiptsAndPayments.GetPreviousAmountAndType(txtPaymentNo.Tag.ToInt64());
                    if (datPreviousDetails != null && datPreviousDetails.Rows.Count > 0)
                    {
                        if (cboPaymentMode.SelectedValue.ToInt32() == datPreviousDetails.Rows[0]["TransactionTypeID"].ToInt32())
                        {
                            if (!UpdateAccountBalanceValidation(datPreviousDetails))
                                return false;
                        }
                        else
                        {
                            if (!InsertAccountBalanceValidation())
                                return false;
                        }
                    }
                }
                else
                {
                    if (!InsertAccountBalanceValidation())
                        return false;
                }
            }
            
            return blnStatus;
        }

        private bool InsertAccountBalanceValidation()
        {
            clsBLLJournalVoucher MobjclsBLLJournalVoucher = new clsBLLJournalVoucher();
            if (cboPaymentMode.SelectedValue.ToInt32() == (int)PaymentModes.Cash)
            {
                if (((MobjclsBLLJournalVoucher.getAccountBalance((int)Accounts.CashAccount, cboCompany.SelectedValue.ToInt32())) - Convert.ToDecimal(txtAmount.Text)) < 0 && cboPaymentMode.SelectedValue.ToInt32() == (int)PaymentModes.Cash)
                {
                   // MsMessageCommon = "There is no Enough cash balance";
                    //this.UserMessage.ShowMessage(2278, cboPaymentMode) 
                    string strMessage = "There is no Enough cash balance. Do you want to continue?";

                    if (MessageBox.Show(strMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        tsStatus.Text = this.UserMessage.GetMessageByCode(2278);
                        tmrReceiptsPayments.Enabled = true;
                        erproRP.SetError(cboPaymentMode, tsStatus.Text);
                        return false;
                    }
                }
            }
            else if (cboPaymentMode.SelectedValue.ToInt32() == (int)PaymentModes.Bank)
            {
                if (((MobjclsBLLJournalVoucher.getAccountBalance(cboBankAccount.SelectedValue.ToInt32(), cboCompany.SelectedValue.ToInt32())) - Convert.ToDecimal(txtAmount.Text)) < 0)
                {
                    if (Convert.ToDateTime(dtpChequeDate.Text) <= Convert.ToDateTime(dtpPaymentDate.Text))
                    {
                        // MsMessageCommon = "There is no Enough cash balance";
                        //this.UserMessage.ShowMessage(2278, cboPaymentMode);
                        string strMessage = "There is no Enough cash balance. Do you want to continue?";

                        if (MessageBox.Show(strMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            tsStatus.Text = this.UserMessage.GetMessageByCode(2291);
                            tmrReceiptsPayments.Enabled = true;
                            erproRP.SetError(cboPaymentMode, tsStatus.Text);
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        private bool UpdateAccountBalanceValidation(DataTable datPreviousDetails)
        {
            clsBLLJournalVoucher MobjclsBLLJournalVoucher = new clsBLLJournalVoucher();

            if (cboPaymentMode.SelectedValue.ToInt32() == (int)PaymentModes.Cash)
            {
                if (txtAmount.Text.ToDecimal() > datPreviousDetails.Rows[0]["Amount"].ToDecimal())
                {
                    if (datPreviousDetails.Rows[0]["Amount"].ToDecimal() + (MobjclsBLLJournalVoucher.getAccountBalance((int)Accounts.CashAccount, cboCompany.SelectedValue.ToInt32())) < Convert.ToDecimal(txtAmount.Text))
                    {
                        // MsMessageCommon = "There is no Enough cash balance";

                        tsStatus.Text = this.UserMessage.GetMessageByCode(2278);
                        tmrReceiptsPayments.Enabled = true;
                        erproRP.SetError(cboPaymentMode, tsStatus.Text);
                        this.UserMessage.ShowMessage(2278, cboPaymentMode);
                        return false;
                    }
                }
            }
            else
            {
                if (datPreviousDetails.Rows[0]["AccountID"].ToInt32() == cboBankAccount.SelectedValue.ToInt32())
                {
                    if (txtAmount.Text.ToDecimal() > datPreviousDetails.Rows[0]["Amount"].ToDecimal())
                    {
                        if (datPreviousDetails.Rows[0]["Amount"].ToDecimal() + ((MobjclsBLLJournalVoucher.getAccountBalance(cboBankAccount.SelectedValue.ToInt32(), cboCompany.SelectedValue.ToInt32())) - Convert.ToDecimal(txtAmount.Text)) < 0)
                        {
                            if (Convert.ToDateTime(dtpChequeDate.Text) <= Convert.ToDateTime(dtpPaymentDate.Text) && Convert.ToDateTime(dtpChequeDate.Text) <= datPreviousDetails.Rows[0]["ChequeDate"].ToDateTime())
                            {
                                // MsMessageCommon = "There is no Enough cash balance";

                                tsStatus.Text = this.UserMessage.GetMessageByCode(2291);
                                tmrReceiptsPayments.Enabled = true;
                                erproRP.SetError(cboPaymentMode, tsStatus.Text);
                                this.UserMessage.ShowMessage(2278, cboPaymentMode);
                                return false;
                            }
                        }
                    }
                    else
                    {
                        if (Convert.ToDateTime(dtpChequeDate.Text) <= Convert.ToDateTime(dtpPaymentDate.Text) && Convert.ToDateTime(dtpChequeDate.Text) <= datPreviousDetails.Rows[0]["ChequeDate"].ToDateTime())
                        {
                            if ((MobjclsBLLJournalVoucher.getAccountBalance(cboBankAccount.SelectedValue.ToInt32(), cboCompany.SelectedValue.ToInt32()) - Convert.ToDecimal(txtAmount.Text)) < 0)
                            {
                                // MsMessageCommon = "There is no Enough cash balance";

                                tsStatus.Text = this.UserMessage.GetMessageByCode(2291);
                                tmrReceiptsPayments.Enabled = true;
                                erproRP.SetError(cboPaymentMode, tsStatus.Text);
                                this.UserMessage.ShowMessage(2278, cboPaymentMode);
                                return false;
                            }
                        }
                    }
                }
                else
                {
                    if (((MobjclsBLLJournalVoucher.getAccountBalance(cboBankAccount.SelectedValue.ToInt32(), cboCompany.SelectedValue.ToInt32())) - Convert.ToDecimal(txtAmount.Text)) < 0)
                    {
                        if (Convert.ToDateTime(dtpChequeDate.Text) <= Convert.ToDateTime(dtpPaymentDate.Text))
                        {
                            // MsMessageCommon = "There is no Enough cash balance";

                            tsStatus.Text = this.UserMessage.GetMessageByCode(2291);
                            tmrReceiptsPayments.Enabled = true;
                            erproRP.SetError(cboPaymentMode, tsStatus.Text);
                            this.UserMessage.ShowMessage(2278, cboPaymentMode);
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        private void bnDeleteItem_Click(object sender, EventArgs e)
        {
            int intReceiptPaymentID = txtPaymentNo.Tag.ToInt32();
            if (this.UserMessage.ShowMessage(13) == true)
            {
                if (MobjclsBLLReceiptsAndPayments.DeleteReceiptAndPayment() == true)
                {
                    clsBLLAlertMoment objclsBLLAlertMoment = new clsBLLAlertMoment();
                    if (this.cboOperationType.SelectedValue.ToInt32() == (Int32)OperationType.SalesOrder)
                    {
                        objclsBLLAlertMoment.DeleteAlert(intReceiptPaymentID, (int)AlertSettingsTypes.SalesOrderAdvancePaid);
                    }
                    if (this.cboOperationType.SelectedValue.ToInt32() == (Int32)OperationType.SalesInvoice && this.cboPaymentType.SelectedValue.ToInt32() == 2)//Sales receipt
                    {
                        objclsBLLAlertMoment.DeleteAlert(intReceiptPaymentID, (int)AlertSettingsTypes.Receipt);
                    }
                    
                    objFrmTradingMain.lngAlertID = (long)AlertSettingsTypes.SalesOrderAdvancePaid;
                    objFrmTradingMain.tmrAlert.Enabled = true;

                    tsStatus.Text = this.UserMessage.GetMessageByCode(4);
                    tmrReceiptsPayments.Enabled = true;
                    this.UserMessage.ShowMessage(4); // Deleted Successfully

                    bnAddNewItem_Click(sender, e);
                }
            }
        }

        private void dgvPaymentDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvPaymentDetails.Columns["Amount"].Index)
                {
                    if (dgvPaymentDetails.Rows[e.RowIndex].Cells["Amount"].Value != null)
                    {
                        if (dgvPaymentDetails.Rows[e.RowIndex].Cells["Amount"].Value.ToDecimal() > 0)
                        {
                            int intTempSacle = MobjclsBLLReceiptsAndPayments.GetCurrencyScale(intCurrencyID);
                            dgvPaymentDetails.Rows[e.RowIndex].Cells["Amount"].Value = dgvPaymentDetails.Rows[e.RowIndex].Cells["Amount"].Value.ToDecimal().CurrencyFormat(intTempSacle).ToDecimal();
                            if ((dgvPaymentDetails.Rows[e.RowIndex].Cells["CurrentBalance"].Value.ToDecimal() +
                                    dgvPaymentDetails.Rows[e.RowIndex].Cells["ActualAmount"].Value.ToDecimal()) <
                                    dgvPaymentDetails.Rows[e.RowIndex].Cells["Amount"].Value.ToDecimal())
                            {
                                dgvPaymentDetails.Rows[e.RowIndex].Cells["Amount"].Value =
                                    (dgvPaymentDetails.Rows[e.RowIndex].Cells["CurrentBalance"].Value.ToDecimal() +
                                    dgvPaymentDetails.Rows[e.RowIndex].Cells["ActualAmount"].Value.ToDecimal());
                            }
                        }
                        else
                            dgvPaymentDetails.Rows[e.RowIndex].Cells["Amount"].Value = 0;
                    }
                    CalcTotal();
                }
            }
        }

        private void CalcTotal()
        {
            txtAmount.Text = "0";
            for (int iCounter = 0; iCounter <= dgvPaymentDetails.Rows.Count - 1; iCounter++)
            {
                if (dgvPaymentDetails.Rows[iCounter].Cells["Amount"].Value != null)
                    txtAmount.Text = (txtAmount.Text.ToDecimal() + (dgvPaymentDetails.Rows[iCounter].Cells["Amount"].Value.ToString()).ToDecimal()).ToString();
            }
        }

        private void dgvPaymentDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgvPaymentDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void RecordCount()
        {
            TotalRecordCnt = MobjclsBLLReceiptsAndPayments.GetRecordCount(intModuleID);
            CurrentRecCnt = TotalRecordCnt;
            bindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
            bindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();

            if (TotalRecordCnt == 0)
            {
                bindingNavigatorCountItem.Text = "of 0";
                CurrentRecCnt = 0;
                TotalRecordCnt = 0;
            }
            BindingEnableDisable();
        }

        private void BindingEnableDisable()
        {
            bindingNavigatorMoveFirstItem.Enabled = true;
            bindingNavigatorMovePreviousItem.Enabled = true;
            bindingNavigatorMoveNextItem.Enabled = true;
            bindingNavigatorMoveLastItem.Enabled = true;

            if (TotalRecordCnt == 0)
            {
                bindingNavigatorMoveFirstItem.Enabled = false;
                bindingNavigatorMovePreviousItem.Enabled = false;
                bindingNavigatorMoveNextItem.Enabled = false;
                bindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                if (Convert.ToDouble(bindingNavigatorPositionItem.Text) == Convert.ToDouble(TotalRecordCnt))
                {
                    bindingNavigatorMoveNextItem.Enabled = false;
                    bindingNavigatorMoveLastItem.Enabled = false;
                }
                else if (Convert.ToDouble(bindingNavigatorPositionItem.Text) == 1)
                {
                    bindingNavigatorMoveFirstItem.Enabled = false;
                    bindingNavigatorMovePreviousItem.Enabled = false;
                }
            }
        }

        private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            bindingNavigatorPositionItem.Text = "1";
            if (TotalRecordCnt > 1)
                bindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
            else
                bindingNavigatorCountItem.Text = "of 1";
            
            //if (datAdvanceSearchedData.Rows.Count > 0)
            //{
            //    bool blnIsReceipt = false;
            //    if (intMenuID == (int)eMenuID.Receipt)
            //        blnIsReceipt = true;
            //    string strCurrentCnt = bindingNavigatorPositionItem.Text;
            //    bindingNavigatorPositionItem.Text = MobjclsBLLReceiptsAndPayments.GetRowNumber(datAdvanceSearchedData.Rows[strCurrentCnt.ToInt32() - 1]["ID"].ToInt64(), blnIsReceipt).ToString();
                
            //    DisplayReceiptAndPayments();
            //    bindingNavigatorPositionItem.Text = strCurrentCnt;
            //    bindingNavigatorCountItem.Text = "of " + datAdvanceSearchedData.Rows.Count;
            //    BindingEnableDisable();
            //}
            //else
                DisplayReceiptAndPayments();
            FillCurrentBalanceInTextBox();
        }

        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            bindingNavigatorPositionItem.Text = (Convert.ToInt32(bindingNavigatorPositionItem.Text) - 1).ToString();
            if (Convert.ToInt32(bindingNavigatorPositionItem.Text) <= 0)
                bindingNavigatorPositionItem.Text = "1";
            if (TotalRecordCnt > 1)
                bindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
            else
                bindingNavigatorCountItem.Text = "of 1";

            //if (datAdvanceSearchedData.Rows.Count > 0)
            //{
            //    bool blnIsReceipt = false;
            //    if (intMenuID == (int)eMenuID.Receipt)
            //        blnIsReceipt = true;
            //    string strCurrentCnt = bindingNavigatorPositionItem.Text;
            //    bindingNavigatorPositionItem.Text = MobjclsBLLReceiptsAndPayments.GetRowNumber(datAdvanceSearchedData.Rows[strCurrentCnt.ToInt32() - 1]["ID"].ToInt64(), blnIsReceipt).ToString();
               
            //    DisplayReceiptAndPayments();
            //    bindingNavigatorPositionItem.Text = strCurrentCnt;
            //    bindingNavigatorCountItem.Text = "of "+datAdvanceSearchedData.Rows.Count ;
            //    BindingEnableDisable();
            //}
            //else
                DisplayReceiptAndPayments();
            FillCurrentBalanceInTextBox();
        }

        private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            bindingNavigatorPositionItem.Text = (Convert.ToInt32(bindingNavigatorPositionItem.Text) + 1).ToString();
            if (TotalRecordCnt < Convert.ToInt32(bindingNavigatorPositionItem.Text))
            {
                if (TotalRecordCnt > 1)
                    bindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                else
                    bindingNavigatorPositionItem.Text = "1";
            }
            if (TotalRecordCnt > 1)
                bindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
            else
                bindingNavigatorCountItem.Text = "of 1";
            //if (datAdvanceSearchedData.Rows.Count > 0)
            //{
            //    bool blnIsReceipt = false;
            //    if (intMenuID == (int)eMenuID.Receipt)
            //        blnIsReceipt = true;
            //    string strCurrentCnt = bindingNavigatorPositionItem.Text;
            //    bindingNavigatorPositionItem.Text = MobjclsBLLReceiptsAndPayments.GetRowNumber(datAdvanceSearchedData.Rows[strCurrentCnt.ToInt32() - 1]["ID"].ToInt64(), blnIsReceipt).ToString();
                
            //    DisplayReceiptAndPayments();
            //    bindingNavigatorPositionItem.Text = strCurrentCnt;
            //    bindingNavigatorCountItem.Text = "of " + datAdvanceSearchedData.Rows.Count;
            //    BindingEnableDisable();
            //}
            //else
                DisplayReceiptAndPayments();
            FillCurrentBalanceInTextBox();
        }

        private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            if (TotalRecordCnt > 1)
            {
                //if (datAdvanceSearchedData.Rows.Count == 0)
                    bindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                //else
                //    bindingNavigatorPositionItem.Text = datAdvanceSearchedData.Rows.Count.ToString();

                bindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
            }
            else
            {
                bindingNavigatorPositionItem.Text = "1";
                bindingNavigatorCountItem.Text = "of 1";
            }
            //if (datAdvanceSearchedData.Rows.Count > 0)
            //{
            //    bool blnIsReceipt = false;
            //    if (intMenuID == (int)eMenuID.Receipt)
            //        blnIsReceipt = true;
            //    string strCurrentCnt = bindingNavigatorPositionItem.Text;
            //    bindingNavigatorPositionItem.Text = MobjclsBLLReceiptsAndPayments.GetRowNumber(datAdvanceSearchedData.Rows[strCurrentCnt.ToInt32() - 1]["ID"].ToInt64(), blnIsReceipt).ToString();
                
            //    DisplayReceiptAndPayments();
            //    bindingNavigatorPositionItem.Text = strCurrentCnt;
            //    bindingNavigatorCountItem.Text = "of " + datAdvanceSearchedData.Rows.Count;
            //    BindingEnableDisable();
            //}
            //else
                DisplayReceiptAndPayments();
            FillCurrentBalanceInTextBox();
        }

        private void DisplayReceiptAndPayments()
        {
            //cboOperationType.DataSource = MobjclsBLLReceiptsAndPayments.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference", "" });
            if (MobjclsBLLReceiptsAndPayments.DisplayReceiptsAndPayments(bindingNavigatorPositionItem.Text.ToInt64(), intModuleID))
            {
                blnDeletePosssile = true;
                int intTempVID = MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intVendorID;
                int intTempRPTID = MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReceiptAndPaymentTypeID;
                txtPaymentNo.Tag = MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReceiptAndPaymentID;
                cboCompany.SelectedValue = MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intCompanyID;
                intCurrencyID = MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intCurrencyID;
                cboOperationType.SelectedValue = MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intOperationTypeID;
                cboPaymentType.SelectedValue = intTempRPTID;
                dtpPaymentDate.Value = MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.dtpPaymentDate;
                txtPaymentNo.Text = MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.strPaymentNumber;

                cboPaymentMode.SelectedValue = MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intTransactionTypeID;
                if (cboPaymentMode.SelectedValue.ToInt32() == (int)PaymentModes.Bank)
                {
                    cboBankAccount.SelectedValue = MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intAccountID;
                    txtChequeNo.Text = MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.strChequeNumber;
                    dtpChequeDate.Value = MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.dtpChequeDate;
                }
                txtDescription.Text = MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.strRemarks;

                cboCompany.Enabled = cboOperationType.Enabled = cboPaymentType.Enabled = cboCustomer.Enabled = btnCustomerSearch.Enabled= dtpPaymentDate.Enabled = false;

                if (intTempVID > 0)
                    cboCustomer.SelectedValue = intTempVID;

                if(cboCustomer.SelectedValue.ToInt32() <= 0)
                {
                    if(cboOperationType.SelectedValue.ToInt32() == (int)OperationType.POS)
                    {
                        cboCustomer.Text = "General Customer";
                    }
                }

                DataTable datTemp = MobjclsBLLReceiptsAndPayments.DisplayReceiptsAndPaymentsDetails();

                dgvPaymentDetails.Rows.Clear();
                dgvPaymentDetails.ReadOnly = false;

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                        {
                            dgvPaymentDetails.Rows.Add();
                            dgvPaymentDetails.Rows[iCounter].Cells["ReferenceID"].Value = datTemp.Rows[iCounter]["ReferenceID"].ToInt64();
                            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReferenceID = datTemp.Rows[iCounter]["ReferenceID"].ToInt64();
                            decimal decBillAmount, decBalanceAmount, decadvanceadjustment, decAdvanceAmt=(0.0).ToDecimal();
                            int intReferenceID = datTemp.Rows[iCounter]["ReferenceID"].ToInt32();
                            decBillAmount = MobjclsBLLReceiptsAndPayments.getBillAmount();
                            decBalanceAmount = MobjclsBLLReceiptsAndPayments.getBalanceAmount();

                            //Sales Invoice & Purchase Invoice
                            //if (cboOperationType.SelectedValue.ToInt32() == 9 || cboOperationType.SelectedValue.ToInt32() == 5)
                            //{

                            //    DataTable datTempforIDsandAmount = MobjclsBLLReceiptsAndPayments.GetAllInvoiceIDandAmount();


                            //    if (datTempforIDsandAmount.Rows.Count > 0)
                            //    {
                            //        decAdvanceAmt = MobjclsBLLReceiptsAndPayments.GetAdvanceAmountByInvoiceID();
                            //        for (int jCounter = 0; jCounter <= datTempforIDsandAmount.Rows.Count - 1; jCounter++)
                            //        {
                            //            if (datTempforIDsandAmount.Rows[jCounter]["ReferenceID"].ToInt64() != MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReferenceID)
                            //            {
                            //                //decTempAmt = decTempAmt - decAdvanceAmt;
                            //                decAdvanceAmt = decAdvanceAmt - datTempforIDsandAmount.Rows[jCounter]["Amount"].ToDecimal();
                            //            }
                            //            else
                            //            {
                            //                if (decAdvanceAmt > 0)
                            //                {
                            //                    //decBalanceAmount = decBalanceAmount - decAdvanceAmt;
                            //                    //decAdvanceAmt = decAdvanceAmt - datTempforIDsandAmount.Rows[jCounter]["Amount"].ToDecimal();
                            //                }
                            //                break;
                            //            }
                            //        }
                            //    }
                            //}



                            /***************************************************************************************************************************/


                            if (cboOperationType.SelectedValue.ToInt32() == 5)
                            {

                                DataTable datTempforIDsandAmount = MobjclsBLLReceiptsAndPayments.GetAllInvoiceIDandAmount();


                                if (datTempforIDsandAmount.Rows.Count > 0)
                                {
                                    decAdvanceAmt = MobjclsBLLReceiptsAndPayments.GetAdvanceAmountByInvoiceID();
                                    //for (int jCounter = 0; jCounter <= datTempforIDsandAmount.Rows.Count - 1; jCounter++)
                                    for (int jCounter = 0; jCounter <= datTempforIDsandAmount.Rows.Count - 1; jCounter++)
                                    {
                                        if (datTempforIDsandAmount.Rows[jCounter]["ReferenceID"].ToInt64() != MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReferenceID)
                                        {
                                            //decTempAmt = decTempAmt - decAdvanceAmt;
                                            decAdvanceAmt = decAdvanceAmt - datTempforIDsandAmount.Rows[jCounter]["Amount"].ToDecimal();

                                        }
                                        else
                                        {
                                            if (decAdvanceAmt > 0)
                                            {
                                                decBalanceAmount = decBalanceAmount - decAdvanceAmt;
                                                decAdvanceAmt = decAdvanceAmt - datTempforIDsandAmount.Rows[jCounter]["Amount"].ToDecimal();

                                            }
                                            break;
                                        }
                                    }
                                    //{
                                    //    if (datTempforIDsandAmount.Rows[jCounter]["ReferenceID"].ToInt64() != MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReferenceID)
                                    //    {
                                    //        //decTempAmt = decTempAmt - decAdvanceAmt;
                                    //        decAdvanceAmt = decAdvanceAmt - datTempforIDsandAmount.Rows[jCounter]["Amount"].ToDecimal();
                                    //    }
                                    //    else
                                    //    {
                                    //        if (decAdvanceAmt > 0)
                                    //        {
                                    //            //decBalanceAmount = decBalanceAmount - decAdvanceAmt;
                                    //            //decAdvanceAmt = decAdvanceAmt - datTempforIDsandAmount.Rows[jCounter]["Amount"].ToDecimal();
                                    //        }
                                    //        break;
                                    //    }
                                    //}
                                }
                            }

                            //Sales Invoice
                            if (cboOperationType.SelectedValue.ToInt32() == 9)
                            {

                                DataTable datTempforIDsandAmount = MobjclsBLLReceiptsAndPayments.GetAllInvoiceIDandAmount();


                                if (datTempforIDsandAmount.Rows.Count > 0)
                                {
                                    decAdvanceAmt = MobjclsBLLReceiptsAndPayments.GetAdvanceAmountByInvoiceID();
                                    for (int jCounter = 0; jCounter <= datTempforIDsandAmount.Rows.Count - 1; jCounter++)
                                    {
                                        if (datTempforIDsandAmount.Rows[jCounter]["ReferenceID"].ToInt64() != MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReferenceID)
                                        {

                                            //decTempAmt = decTempAmt - decAdvanceAmt;
                                            decAdvanceAmt = decAdvanceAmt - datTempforIDsandAmount.Rows[jCounter]["Amount"].ToDecimal();
                                        }
                                        else
                                        {
                                            if (decAdvanceAmt > 0)
                                            {
                                                decimal decReceitAmount = MobjclsBLLReceiptsAndPayments.GetReceiptAmount(intReferenceID);
                                                //decBalanceAmount = decBalanceAmount - decAdvanceAmt;
                                                //decAdvanceAmt = decAdvanceAmt - datTempforIDsandAmount.Rows[jCounter]["Amount"].ToDecimal();
                                                decAdvanceAmt = decAdvanceAmt - decReceitAmount;
                                                decBalanceAmount = decAdvanceAmt;
                                            }
                                            break;

                                        }
                                    }
                                    //decBalanceAmount = decAdvanceAmt- decReceitAmount;
                                }
                            }


                            dgvPaymentDetails.Rows[iCounter].Cells["ReferenceNo"].Value = MobjclsBLLReceiptsAndPayments.getReferenceNo();
                            dgvPaymentDetails.Rows[iCounter].Cells["BillAmount"].Value = decBillAmount;// +decadvanceadjustment;
                            //dgvPaymentDetails.Rows[iCounter].Cells["CurrentBalance"].Value = ((this.MobjclsBLLReceiptsAndPayments.GetPaymentTerms(intReferenceID) == 2) ? decBalanceAmount : decBalanceAmount - decAdvanceAmt);
                            dgvPaymentDetails.Rows[iCounter].Cells["CurrentBalance"].Value = decBalanceAmount;
                            dgvPaymentDetails.Rows[iCounter].Cells["ActualAmount"].Value = datTemp.Rows[iCounter]["Amount"].ToDecimal();
                            dgvPaymentDetails.Rows[iCounter].Cells["Amount"].Value = datTemp.Rows[iCounter]["Amount"].ToDecimal();

                            if (MobjclsBLLReceiptsAndPayments.CheckReferenceNoStatus() == 1) // Delete Possible
                            {
                                dgvPaymentDetails.Rows[iCounter].ReadOnly = true;
                                blnDeletePosssile = false;
                            }
                        }
                    }
                }

                FillGridinUpdate();
                BindingEnableDisable();

                if (txtPaymentNo.Tag.ToInt64() > 0)
                    btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = MblnUpdatePermission;
                else
                    btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = MblnAddPermission;
                bnDeleteItem.Enabled = MblnDeletePermission;
                if (MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.blnIsDirect == true || blnDeletePosssile == false)
                {
                    dgvPaymentDetails.ReadOnly = true;
                    btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = bnDeleteItem.Enabled = false;
                }
            }
        }

        private void FillGridinUpdate()
        {
            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intCompanyID = cboCompany.SelectedValue.ToInt32();
            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intOperationTypeID = cboOperationType.SelectedValue.ToInt32();
            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReceiptAndPaymentTypeID = cboPaymentType.SelectedValue.ToInt32();
            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intVendorID = cboCustomer.SelectedValue.ToInt32();
            DataTable datTemp = MobjclsBLLReceiptsAndPayments.getReferenceIDDetailstoDisplay();
            DataTable datTempRPDetails = MobjclsBLLReceiptsAndPayments.DisplayReceiptsAndPaymentsDetails();
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    bool blnStatus = false;
                    for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                    {
                        for (int jCounter = 0; jCounter <= datTempRPDetails.Rows.Count - 1; jCounter++)
                        {
                            if (datTemp.Rows[iCounter]["ReferenceID"].ToInt32() == datTempRPDetails.Rows[jCounter]["ReferenceID"].ToInt32())
                            {
                                blnStatus = true;
                                break;
                            }
                        }
                        if (blnStatus == false)
                        {
                            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReferenceID = datTemp.Rows[iCounter]["ReferenceID"].ToInt64();
                            decimal decTempAmt = MobjclsBLLReceiptsAndPayments.getBalanceAmount();
                            if (decTempAmt > 0)
                            {
                                dgvPaymentDetails.Rows.Add();
                                dgvPaymentDetails.Rows[dgvPaymentDetails.Rows.Count - 1].Cells["ReferenceID"].Value = datTemp.Rows[iCounter]["ReferenceID"].ToInt64();
                                dgvPaymentDetails.Rows[dgvPaymentDetails.Rows.Count - 1].Cells["ReferenceNo"].Value = datTemp.Rows[iCounter]["ReferenceNo"];
                                dgvPaymentDetails.Rows[dgvPaymentDetails.Rows.Count - 1].Cells["BillAmount"].Value = MobjclsBLLReceiptsAndPayments.getBillAmount();
                                dgvPaymentDetails.Rows[dgvPaymentDetails.Rows.Count - 1].Cells["CurrentBalance"].Value = decTempAmt;
                                dgvPaymentDetails.Rows[dgvPaymentDetails.Rows.Count - 1].Cells["ActualAmount"].Value = 0;
                                dgvPaymentDetails.Rows[dgvPaymentDetails.Rows.Count - 1].Cells["Amount"].Value = 0;
                            }
                        }
                        blnStatus = false;
                    }
                }
            }
        }

        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            tmrReceiptsPayments.Enabled = false;
            ClearControls();

            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intOperationTypeID = 0;
            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReceiptAndPaymentTypeID = 0;
            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intTransactionTypeID = 0;
            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intVendorID = 0;
            MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.intReferenceID = 0;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
            clsBLLCommonUtility MobjclsBLLCommonUtility = new clsBLLCommonUtility();
            lblAmountinWords.Text = MobjclsBLLCommonUtility.ConvertToWord(txtAmount.Text, intCurrencyID);

            if (txtPaymentNo.Tag.ToInt64() > 0)
            {
                if (txtPaymentNo.Tag.ToInt64() > 0)
                    btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = MblnUpdatePermission;
                else
                    btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = MblnAddPermission;
                if (blnDeletePosssile == true)
                    bnDeleteItem.Enabled = MblnDeletePermission;
                else
                    bnDeleteItem.Enabled = false;
                bnPrint.Enabled = bnEmail.Enabled = MblnPrintEmailPermission;
            }
            else
            {
                if (txtPaymentNo.Tag.ToInt64() > 0)
                    btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = MblnUpdatePermission;
                else
                    btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = MblnAddPermission;
                bnDeleteItem.Enabled = bnPrint.Enabled = bnEmail.Enabled = false;
            }
        }

        private void tmrReceiptsPayments_Tick(object sender, EventArgs e)
        {
            tmrReceiptsPayments.Enabled = false;
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {
            if (txtPaymentNo.Tag.ToInt64() > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = txtPaymentNo.Tag.ToInt64();

                if (MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.blnIsReceipt == true)
                    ObjViewer.PiFormID = (int)FormID.Receipts;
                else
                    ObjViewer.PiFormID = (int)FormID.Payments;
                ObjViewer.AlMajdalPrePrintedFormat = ClsMainSettings.AlMajdalPrePrintedFormat;
                ObjViewer.ShowDialog();
            }
        }

        private void bnEmail_Click(object sender, EventArgs e)
        {
            using (FrmEmailPopup objEmailPopUp = new FrmEmailPopup())
            {
                if (txtPaymentNo.Tag.ToInt64() > 0)
                {
                    if (MobjclsBLLReceiptsAndPayments.PobjClsDTOReceiptsAndPayments.blnIsReceipt == true)
                    {
                        objEmailPopUp.MsSubject = "Receipt Details";
                        objEmailPopUp.EmailFormType = EmailFormID.Receipts;
                    }
                    else
                    {
                        objEmailPopUp.MsSubject = "Payment Details";
                        objEmailPopUp.EmailFormType = EmailFormID.Payments;
                    }
                    objEmailPopUp.EmailSource = MobjclsBLLReceiptsAndPayments.DisplayPaymentDetailsReport(txtPaymentNo.Tag.ToInt64());
                    objEmailPopUp.ShowDialog();
                }
            }
        }

        private void dgvPaymentDetails_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjClsBLLCommonUtility.SetSerialNo(dgvPaymentDetails, e.RowIndex, true);
        }

        private void dgvPaymentDetails_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            MobjClsBLLCommonUtility.SetSerialNo(dgvPaymentDetails, e.RowIndex, true);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            FrmSearchForm objFrmSearchForm = null;

            if (intMenuID == (int)eMenuID.Payment)
                objFrmSearchForm = new FrmSearchForm((int)OperationType.Payment);
            else if (intMenuID == (int)eMenuID.Receipt)
                objFrmSearchForm = new FrmSearchForm((int)OperationType.Receipt);

            objFrmSearchForm.ShowDialog();

            //if (objFrmSearchForm.datSerachedData != null && objFrmSearchForm.datSerachedData.Rows.Count > 0)
            //{
            //    datAdvanceSearchedData = objFrmSearchForm.datSerachedData;
            //    datAdvanceSearchedData = datAdvanceSearchedData.DefaultView.ToTable(true, "ID", "No");
            //}
            AutofillfromForm();
        }

        private void AutofillfromForm()
        {
            //if (datAdvanceSearchedData.Rows.Count > 0)
            //{
            //    bool blnIsReceipt = false;
            //    if (intMenuID == (int)eMenuID.Receipt)
            //        blnIsReceipt = true;

            //    bindingNavigatorPositionItem.Text = MobjclsBLLReceiptsAndPayments.GetRowNumber(datAdvanceSearchedData.Rows[0]["ID"].ToInt64(), blnIsReceipt).ToString();
            //    DisplayReceiptAndPayments();
            //    FillCurrentBalanceInTextBox();

            //    bindingNavigatorPositionItem.Text = datAdvanceSearchedData.Rows.Count.ToString();
            //    bindingNavigatorCountItem.Text = "of " + datAdvanceSearchedData.Rows.Count.ToString();
            //}
            //else
            //{
                //bindingNavigatorPositionItem.Text = "1";
                //bindingNavigatorCountItem.Text = "of 1";
            //}

            //TotalRecordCnt = datAdvanceSearchedData.Rows.Count;
            //CurrentRecCnt = TotalRecordCnt;
            //if (TotalRecordCnt == 0)
            //{
            //    bindingNavigatorCountItem.Text = "of 0";
            //    CurrentRecCnt = 0;
            //    TotalRecordCnt = 0;
            //}
            //BindingEnableDisable();
        }

        private void btnCustomerSearch_Click(object sender, EventArgs e)
        {
            int intVendor = Convert.ToInt32(cboCustomer.SelectedValue);
            int intVendorType=0;
            if (intMenuID == (int)eMenuID.Payment)  
                intVendorType = (int)VendorType.Supplier;
            else if (intMenuID == (int)eMenuID.Receipt)
                intVendorType = (int)VendorType.Customer;

            using (FrmVendor objVendor = new FrmVendor(intVendorType))
            {
                objVendor.PintVendorID = Convert.ToInt32(cboCustomer.SelectedValue);
                objVendor.ShowDialog();

                if (intMenuID == (int)eMenuID.Payment)
                    cboCustomer.DataSource = MobjclsBLLReceiptsAndPayments.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", "VendorTypeID=" + (int)VendorType.Supplier });
                else
                    cboCustomer.DataSource = MobjclsBLLReceiptsAndPayments.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", "VendorTypeID=" + (int)VendorType.Customer });
                cboCustomer.ValueMember = "VendorID";
                cboCustomer.DisplayMember = "VendorName";

                if (objVendor.PintVendorID != 0 && cboCustomer.Enabled)
                    cboCustomer.SelectedValue = objVendor.PintVendorID;
                else
                    cboCustomer.SelectedValue = intVendor;
            }
        }
    }
}