﻿using DemoClsDataGridview;
namespace MyBooksERP
{
    partial class frmJournalVoucher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmJournalVoucher));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bnGeneralReceiptsAndPayments = new System.Windows.Forms.BindingNavigator(this.components);
            this.btnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.btnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.btnChartofAccounts = new System.Windows.Forms.ToolStripButton();
            this.btnCostCenterReference = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.txtVoucherNo = new System.Windows.Forms.TextBox();
            this.lblVoucherNo = new System.Windows.Forms.Label();
            this.lblSelectedAccountBalance = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDebitTotal = new System.Windows.Forms.TextBox();
            this.txtCreditTotal = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.txtChequeNo = new System.Windows.Forms.TextBox();
            this.dtpChequeDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboVoucherType = new System.Windows.Forms.ComboBox();
            this.lblVoucherType = new System.Windows.Forms.Label();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.lblCompany = new System.Windows.Forms.Label();
            this.lblNarration = new System.Windows.Forms.Label();
            this.txtNarration = new System.Windows.Forms.TextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.lblDate = new System.Windows.Forms.Label();
            this.tmGeneralReceiptsAndPayments = new System.Windows.Forms.Timer(this.components);
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvVNoDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.VoucherID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VoucherNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VoucherDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelLeftTop = new DevComponents.DotNetBar.PanelEx();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblRFQNo = new System.Windows.Forms.Label();
            this.BtnSRefresh = new DevComponents.DotNetBar.ButtonX();
            this.TxtSsearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.CboSearchCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.LblScompany = new System.Windows.Forms.Label();
            this.LblSCountStatus = new DevComponents.DotNetBar.LabelX();
            this.pnlMain = new DevComponents.DotNetBar.PanelEx();
            this.panelMiddle = new DevComponents.DotNetBar.PanelEx();
            this.tcVoucherEntry = new DevComponents.DotNetBar.SuperTabControl();
            this.tpVoucher1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.dgvJournalVoucher = new ClsInnerGridBar();
            this.AccountID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DebitAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreditAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsGroupJVAdd = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnAddAutofill = new System.Windows.Forms.ToolStripMenuItem();
            this.stVoucherEntry = new DevComponents.DotNetBar.SuperTabItem();
            this.tpCostCenter1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.dgvCostCenter = new ClsInnerGridBar();
            this.CostCenterAccountID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.CostCenterID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stCostCenter = new DevComponents.DotNetBar.SuperTabItem();
            this.pnlBottom = new DevComponents.DotNetBar.PanelEx();
            this.cboBankAccount = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.expnlLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlLedgerAccounts = new DevComponents.DotNetBar.PanelEx();
            this.dgvLedgerAccounts = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.LedgerAccountID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LedgerCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LedgerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bnGeneralReceiptsAndPayments)).BeginInit();
            this.bnGeneralReceiptsAndPayments.SuspendLayout();
            this.ssStatus.SuspendLayout();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVNoDisplay)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.panelMiddle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcVoucherEntry)).BeginInit();
            this.tcVoucherEntry.SuspendLayout();
            this.tpVoucher1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvJournalVoucher)).BeginInit();
            this.cmsGroupJVAdd.SuspendLayout();
            this.tpCostCenter1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCostCenter)).BeginInit();
            this.pnlBottom.SuspendLayout();
            this.panelTop.SuspendLayout();
            this.pnlLedgerAccounts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLedgerAccounts)).BeginInit();
            this.SuspendLayout();
            // 
            // bnGeneralReceiptsAndPayments
            // 
            this.bnGeneralReceiptsAndPayments.AddNewItem = null;
            this.bnGeneralReceiptsAndPayments.BackColor = System.Drawing.Color.Transparent;
            this.bnGeneralReceiptsAndPayments.CountItem = null;
            this.bnGeneralReceiptsAndPayments.DeleteItem = null;
            this.bnGeneralReceiptsAndPayments.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddNewItem,
            this.btnSaveItem,
            this.btnDeleteItem,
            this.btnClear,
            this.toolStripSeparator7,
            this.btnChartofAccounts,
            this.btnCostCenterReference,
            this.toolStripSeparator1,
            this.btnPrint,
            this.btnEmail,
            this.toolStripSeparator8,
            this.btnHelp});
            this.bnGeneralReceiptsAndPayments.Location = new System.Drawing.Point(3, 0);
            this.bnGeneralReceiptsAndPayments.MoveFirstItem = null;
            this.bnGeneralReceiptsAndPayments.MoveLastItem = null;
            this.bnGeneralReceiptsAndPayments.MoveNextItem = null;
            this.bnGeneralReceiptsAndPayments.MovePreviousItem = null;
            this.bnGeneralReceiptsAndPayments.Name = "bnGeneralReceiptsAndPayments";
            this.bnGeneralReceiptsAndPayments.PositionItem = null;
            this.bnGeneralReceiptsAndPayments.Size = new System.Drawing.Size(1001, 25);
            this.bnGeneralReceiptsAndPayments.TabIndex = 1041;
            // 
            // btnAddNewItem
            // 
            this.btnAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNewItem.Image")));
            this.btnAddNewItem.Name = "btnAddNewItem";
            this.btnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.btnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.btnAddNewItem.Text = "Add";
            this.btnAddNewItem.ToolTipText = "Add";
            this.btnAddNewItem.Click += new System.EventHandler(this.btnAddNewItem_Click);
            // 
            // btnSaveItem
            // 
            this.btnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveItem.Image")));
            this.btnSaveItem.Name = "btnSaveItem";
            this.btnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.btnSaveItem.Text = "Save";
            this.btnSaveItem.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteItem.Image")));
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.RightToLeftAutoMirrorImage = true;
            this.btnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.btnDeleteItem.Text = "Delete";
            this.btnDeleteItem.ToolTipText = "Delete";
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.ToolTipText = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // btnChartofAccounts
            // 
            this.btnChartofAccounts.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnChartofAccounts.Image = global::MyBooksERP.Properties.Resources.Chart_Of_Accounts;
            this.btnChartofAccounts.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnChartofAccounts.Name = "btnChartofAccounts";
            this.btnChartofAccounts.Size = new System.Drawing.Size(23, 22);
            this.btnChartofAccounts.Text = "Chart of Accounts";
            this.btnChartofAccounts.Click += new System.EventHandler(this.btnChartofAccounts_Click);
            // 
            // btnCostCenterReference
            // 
            this.btnCostCenterReference.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCostCenterReference.Image = global::MyBooksERP.Properties.Resources.Collection;
            this.btnCostCenterReference.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCostCenterReference.Name = "btnCostCenterReference";
            this.btnCostCenterReference.Size = new System.Drawing.Size(23, 22);
            this.btnCostCenterReference.Text = "Cost Center";
            this.btnCostCenterReference.ToolTipText = "Cost Center";
            this.btnCostCenterReference.Click += new System.EventHandler(this.btnCostCenterReference_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = ((System.Drawing.Image)(resources.GetObject("btnEmail.Image")));
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 22);
            this.btnHelp.Text = "He&lp";
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.Location = new System.Drawing.Point(333, 14);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(58, 13);
            this.lblCurrency.TabIndex = 1071;
            this.lblCurrency.Text = "Currency : ";
            // 
            // txtVoucherNo
            // 
            this.txtVoucherNo.BackColor = System.Drawing.SystemColors.Info;
            this.txtVoucherNo.Location = new System.Drawing.Point(747, 11);
            this.txtVoucherNo.Name = "txtVoucherNo";
            this.txtVoucherNo.ReadOnly = true;
            this.txtVoucherNo.Size = new System.Drawing.Size(138, 20);
            this.txtVoucherNo.TabIndex = 1069;
            // 
            // lblVoucherNo
            // 
            this.lblVoucherNo.AutoSize = true;
            this.lblVoucherNo.Location = new System.Drawing.Point(658, 14);
            this.lblVoucherNo.Name = "lblVoucherNo";
            this.lblVoucherNo.Size = new System.Drawing.Size(64, 13);
            this.lblVoucherNo.TabIndex = 1068;
            this.lblVoucherNo.Text = "Voucher No";
            // 
            // lblSelectedAccountBalance
            // 
            this.lblSelectedAccountBalance.AutoSize = true;
            this.lblSelectedAccountBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectedAccountBalance.Location = new System.Drawing.Point(721, 47);
            this.lblSelectedAccountBalance.Name = "lblSelectedAccountBalance";
            this.lblSelectedAccountBalance.Size = new System.Drawing.Size(10, 12);
            this.lblSelectedAccountBalance.TabIndex = 1071;
            this.lblSelectedAccountBalance.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(656, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 1070;
            this.label4.Text = "Acc Cur Bal :";
            // 
            // txtDebitTotal
            // 
            this.txtDebitTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDebitTotal.BackColor = System.Drawing.Color.White;
            this.txtDebitTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDebitTotal.Enabled = false;
            this.txtDebitTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDebitTotal.Location = new System.Drawing.Point(697, 3);
            this.txtDebitTotal.MaxLength = 25;
            this.txtDebitTotal.Name = "txtDebitTotal";
            this.txtDebitTotal.ReadOnly = true;
            this.txtDebitTotal.Size = new System.Drawing.Size(150, 22);
            this.txtDebitTotal.TabIndex = 0;
            this.txtDebitTotal.Text = "0";
            this.txtDebitTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDebitTotal.TextChanged += new System.EventHandler(this.txtDebitTotal_TextChanged);
            // 
            // txtCreditTotal
            // 
            this.txtCreditTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreditTotal.BackColor = System.Drawing.Color.White;
            this.txtCreditTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCreditTotal.Enabled = false;
            this.txtCreditTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditTotal.Location = new System.Drawing.Point(848, 3);
            this.txtCreditTotal.MaxLength = 25;
            this.txtCreditTotal.Name = "txtCreditTotal";
            this.txtCreditTotal.ReadOnly = true;
            this.txtCreditTotal.Size = new System.Drawing.Size(150, 22);
            this.txtCreditTotal.TabIndex = 1045;
            this.txtCreditTotal.Text = "0";
            this.txtCreditTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCreditTotal.TextChanged += new System.EventHandler(this.txtDebitTotal_TextChanged);
            // 
            // lblTotal
            // 
            this.lblTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(655, 7);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(36, 13);
            this.lblTotal.TabIndex = 1050;
            this.lblTotal.Text = "Total";
            // 
            // txtChequeNo
            // 
            this.txtChequeNo.Location = new System.Drawing.Point(382, 4);
            this.txtChequeNo.Name = "txtChequeNo";
            this.txtChequeNo.Size = new System.Drawing.Size(70, 20);
            this.txtChequeNo.TabIndex = 1066;
            // 
            // dtpChequeDate
            // 
            this.dtpChequeDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpChequeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpChequeDate.Location = new System.Drawing.Point(513, 4);
            this.dtpChequeDate.Name = "dtpChequeDate";
            this.dtpChequeDate.Size = new System.Drawing.Size(106, 20);
            this.dtpChequeDate.TabIndex = 1064;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(458, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 1063;
            this.label3.Text = "Ch. Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(333, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 1061;
            this.label1.Text = "Ch. No.";
            // 
            // cboVoucherType
            // 
            this.cboVoucherType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboVoucherType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboVoucherType.DropDownWidth = 106;
            this.cboVoucherType.FormattingEnabled = true;
            this.cboVoucherType.Location = new System.Drawing.Point(898, 11);
            this.cboVoucherType.Name = "cboVoucherType";
            this.cboVoucherType.Size = new System.Drawing.Size(100, 21);
            this.cboVoucherType.TabIndex = 1;
            this.cboVoucherType.Visible = false;
            this.cboVoucherType.SelectedIndexChanged += new System.EventHandler(this.cboVoucherType_SelectedIndexChanged);
            this.cboVoucherType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboVoucherType_KeyDown);
            // 
            // lblVoucherType
            // 
            this.lblVoucherType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblVoucherType.AutoSize = true;
            this.lblVoucherType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVoucherType.Location = new System.Drawing.Point(904, 14);
            this.lblVoucherType.Name = "lblVoucherType";
            this.lblVoucherType.Size = new System.Drawing.Size(74, 13);
            this.lblVoucherType.TabIndex = 1058;
            this.lblVoucherType.Text = "Voucher Type";
            this.lblVoucherType.Visible = false;
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboCompany.DropDownHeight = 105;
            this.cboCompany.DropDownWidth = 250;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 13;
            this.cboCompany.Location = new System.Drawing.Point(77, 11);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(250, 21);
            this.cboCompany.TabIndex = 0;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCompany_KeyDown);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Location = new System.Drawing.Point(20, 14);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 1056;
            this.lblCompany.Text = "Company";
            // 
            // lblNarration
            // 
            this.lblNarration.AutoSize = true;
            this.lblNarration.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNarration.Location = new System.Drawing.Point(6, 31);
            this.lblNarration.Name = "lblNarration";
            this.lblNarration.Size = new System.Drawing.Size(60, 13);
            this.lblNarration.TabIndex = 1050;
            this.lblNarration.Text = "Narration";
            // 
            // txtNarration
            // 
            this.txtNarration.Location = new System.Drawing.Point(23, 47);
            this.txtNarration.MaxLength = 2000;
            this.txtNarration.Multiline = true;
            this.txtNarration.Name = "txtNarration";
            this.txtNarration.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtNarration.Size = new System.Drawing.Size(596, 79);
            this.txtNarration.TabIndex = 4;
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(513, 11);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(106, 20);
            this.dtpDate.TabIndex = 2;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(458, 14);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 1035;
            this.lblDate.Text = "Date";
            // 
            // tmGeneralReceiptsAndPayments
            // 
            this.tmGeneralReceiptsAndPayments.Tick += new System.EventHandler(this.tmGeneralReceiptsAndPayments_Tick);
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tslStatus});
            this.ssStatus.Location = new System.Drawing.Point(3, 442);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(1001, 22);
            this.ssStatus.TabIndex = 1048;
            this.ssStatus.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(45, 17);
            this.toolStripStatusLabel1.Text = "Status: ";
            // 
            // tslStatus
            // 
            this.tslStatus.Name = "tslStatus";
            this.tslStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.dgvVNoDisplay);
            this.PanelLeft.Controls.Add(this.expandablePanel1);
            this.PanelLeft.Controls.Add(this.LblSCountStatus);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(250, 464);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 1044;
            this.PanelLeft.Text = "panelEx1";
            // 
            // dgvVNoDisplay
            // 
            this.dgvVNoDisplay.AllowUserToAddRows = false;
            this.dgvVNoDisplay.AllowUserToDeleteRows = false;
            this.dgvVNoDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVNoDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvVNoDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVNoDisplay.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VoucherID,
            this.VoucherNo,
            this.VoucherDate});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvVNoDisplay.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvVNoDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvVNoDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvVNoDisplay.Location = new System.Drawing.Point(0, 169);
            this.dgvVNoDisplay.Name = "dgvVNoDisplay";
            this.dgvVNoDisplay.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVNoDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvVNoDisplay.RowHeadersVisible = false;
            this.dgvVNoDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVNoDisplay.Size = new System.Drawing.Size(250, 269);
            this.dgvVNoDisplay.TabIndex = 41;
            this.dgvVNoDisplay.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVNoDisplay_CellDoubleClick);
            // 
            // VoucherID
            // 
            this.VoucherID.DataPropertyName = "VoucherID";
            this.VoucherID.HeaderText = "VoucherID";
            this.VoucherID.Name = "VoucherID";
            this.VoucherID.ReadOnly = true;
            this.VoucherID.Visible = false;
            // 
            // VoucherNo
            // 
            this.VoucherNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.VoucherNo.DataPropertyName = "VoucherNo";
            this.VoucherNo.HeaderText = "VoucherNo";
            this.VoucherNo.Name = "VoucherNo";
            this.VoucherNo.ReadOnly = true;
            // 
            // VoucherDate
            // 
            this.VoucherDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.VoucherDate.DataPropertyName = "VoucherDate";
            this.VoucherDate.HeaderText = "VoucherDate";
            this.VoucherDate.Name = "VoucherDate";
            this.VoucherDate.ReadOnly = true;
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expandablePanel1.Controls.Add(this.panelLeftTop);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(250, 169);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 101;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Search";
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelLeftTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelLeftTop.Controls.Add(this.dtpSTo);
            this.panelLeftTop.Controls.Add(this.dtpSFrom);
            this.panelLeftTop.Controls.Add(this.lblTo);
            this.panelLeftTop.Controls.Add(this.lblFrom);
            this.panelLeftTop.Controls.Add(this.lblRFQNo);
            this.panelLeftTop.Controls.Add(this.BtnSRefresh);
            this.panelLeftTop.Controls.Add(this.TxtSsearch);
            this.panelLeftTop.Controls.Add(this.CboSearchCompany);
            this.panelLeftTop.Controls.Add(this.LblScompany);
            this.panelLeftTop.Location = new System.Drawing.Point(0, 26);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(250, 143);
            this.panelLeftTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelLeftTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelLeftTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelLeftTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelLeftTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelLeftTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelLeftTop.Style.GradientAngle = 90;
            this.panelLeftTop.TabIndex = 105;
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(74, 62);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.ShowCheckBox = true;
            this.dtpSTo.Size = new System.Drawing.Size(119, 20);
            this.dtpSTo.TabIndex = 246;
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(74, 36);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.ShowCheckBox = true;
            this.dtpSFrom.Size = new System.Drawing.Size(119, 20);
            this.dtpSFrom.TabIndex = 245;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(3, 62);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 244;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(3, 39);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 243;
            this.lblFrom.Text = "From";
            // 
            // lblRFQNo
            // 
            this.lblRFQNo.AutoSize = true;
            this.lblRFQNo.Location = new System.Drawing.Point(3, 90);
            this.lblRFQNo.Name = "lblRFQNo";
            this.lblRFQNo.Size = new System.Drawing.Size(67, 13);
            this.lblRFQNo.TabIndex = 125;
            this.lblRFQNo.Text = "Voucher No.";
            // 
            // BtnSRefresh
            // 
            this.BtnSRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnSRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BtnSRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnSRefresh.Location = new System.Drawing.Point(172, 114);
            this.BtnSRefresh.Name = "BtnSRefresh";
            this.BtnSRefresh.Size = new System.Drawing.Size(71, 23);
            this.BtnSRefresh.TabIndex = 40;
            this.BtnSRefresh.Text = "Refresh";
            this.BtnSRefresh.Click += new System.EventHandler(this.BtnSRefresh_Click);
            // 
            // TxtSsearch
            // 
            this.TxtSsearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.TxtSsearch.Border.Class = "TextBoxBorder";
            this.TxtSsearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtSsearch.Location = new System.Drawing.Point(74, 88);
            this.TxtSsearch.MaxLength = 20;
            this.TxtSsearch.Name = "TxtSsearch";
            this.TxtSsearch.Size = new System.Drawing.Size(169, 20);
            this.TxtSsearch.TabIndex = 35;
            this.TxtSsearch.WatermarkEnabled = false;
            this.TxtSsearch.WatermarkText = "RFQNo.";
            // 
            // CboSearchCompany
            // 
            this.CboSearchCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CboSearchCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSearchCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSearchCompany.DisplayMember = "Text";
            this.CboSearchCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboSearchCompany.DropDownHeight = 75;
            this.CboSearchCompany.Enabled = false;
            this.CboSearchCompany.FormattingEnabled = true;
            this.CboSearchCompany.IntegralHeight = false;
            this.CboSearchCompany.ItemHeight = 14;
            this.CboSearchCompany.Location = new System.Drawing.Point(74, 10);
            this.CboSearchCompany.Name = "CboSearchCompany";
            this.CboSearchCompany.Size = new System.Drawing.Size(169, 20);
            this.CboSearchCompany.TabIndex = 36;
            this.CboSearchCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboSearchCompany_KeyPress);
            // 
            // LblScompany
            // 
            this.LblScompany.AutoSize = true;
            this.LblScompany.Location = new System.Drawing.Point(3, 13);
            this.LblScompany.Name = "LblScompany";
            this.LblScompany.Size = new System.Drawing.Size(51, 13);
            this.LblScompany.TabIndex = 109;
            this.LblScompany.Text = "Company";
            // 
            // LblSCountStatus
            // 
            // 
            // 
            // 
            this.LblSCountStatus.BackgroundStyle.Class = "";
            this.LblSCountStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblSCountStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LblSCountStatus.Location = new System.Drawing.Point(0, 438);
            this.LblSCountStatus.Name = "LblSCountStatus";
            this.LblSCountStatus.Size = new System.Drawing.Size(250, 26);
            this.LblSCountStatus.TabIndex = 105;
            this.LblSCountStatus.Text = "...";
            // 
            // pnlMain
            // 
            this.pnlMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlMain.Controls.Add(this.panelMiddle);
            this.pnlMain.Controls.Add(this.pnlBottom);
            this.pnlMain.Controls.Add(this.ssStatus);
            this.pnlMain.Controls.Add(this.panelTop);
            this.pnlMain.Controls.Add(this.bnGeneralReceiptsAndPayments);
            this.pnlMain.Controls.Add(this.expnlLeft);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(250, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1004, 464);
            this.pnlMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlMain.Style.GradientAngle = 90;
            this.pnlMain.TabIndex = 1045;
            // 
            // panelMiddle
            // 
            this.panelMiddle.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelMiddle.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelMiddle.Controls.Add(this.tcVoucherEntry);
            this.panelMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMiddle.Location = new System.Drawing.Point(3, 66);
            this.panelMiddle.Name = "panelMiddle";
            this.panelMiddle.Size = new System.Drawing.Size(1001, 246);
            this.panelMiddle.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelMiddle.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelMiddle.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelMiddle.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelMiddle.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelMiddle.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelMiddle.Style.GradientAngle = 90;
            this.panelMiddle.TabIndex = 1069;
            // 
            // tcVoucherEntry
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            this.tcVoucherEntry.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.tcVoucherEntry.ControlBox.MenuBox.Name = "";
            this.tcVoucherEntry.ControlBox.Name = "";
            this.tcVoucherEntry.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.tcVoucherEntry.ControlBox.MenuBox,
            this.tcVoucherEntry.ControlBox.CloseBox});
            this.tcVoucherEntry.Controls.Add(this.tpVoucher1);
            this.tcVoucherEntry.Controls.Add(this.tpCostCenter1);
            this.tcVoucherEntry.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcVoucherEntry.Location = new System.Drawing.Point(0, 0);
            this.tcVoucherEntry.Name = "tcVoucherEntry";
            this.tcVoucherEntry.ReorderTabsEnabled = true;
            this.tcVoucherEntry.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tcVoucherEntry.SelectedTabIndex = 1;
            this.tcVoucherEntry.Size = new System.Drawing.Size(1001, 246);
            this.tcVoucherEntry.TabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcVoucherEntry.TabIndex = 0;
            this.tcVoucherEntry.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.stVoucherEntry,
            this.stCostCenter});
            this.tcVoucherEntry.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.VisualStudio2008Document;
            this.tcVoucherEntry.Text = "superTabControl1";
            // 
            // tpVoucher1
            // 
            this.tpVoucher1.Controls.Add(this.dgvJournalVoucher);
            this.tpVoucher1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tpVoucher1.Location = new System.Drawing.Point(0, 25);
            this.tpVoucher1.Name = "tpVoucher1";
            this.tpVoucher1.Size = new System.Drawing.Size(1001, 221);
            this.tpVoucher1.TabIndex = 1;
            this.tpVoucher1.TabItem = this.stVoucherEntry;
            // 
            // dgvJournalVoucher
            // 
            this.dgvJournalVoucher.AddNewRow = false;
            this.dgvJournalVoucher.AllowUserToResizeColumns = false;
            this.dgvJournalVoucher.AllowUserToResizeRows = false;
            this.dgvJournalVoucher.AlphaNumericCols = new int[0];
            this.dgvJournalVoucher.BackgroundColor = System.Drawing.Color.White;
            this.dgvJournalVoucher.CapsLockCols = new int[0];
            this.dgvJournalVoucher.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvJournalVoucher.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AccountID,
            this.Code,
            this.AccountName,
            this.CurrentBalance,
            this.SerialNo,
            this.Remarks,
            this.DebitAmount,
            this.CreditAmount});
            this.dgvJournalVoucher.ContextMenuStrip = this.cmsGroupJVAdd;
            this.dgvJournalVoucher.DecimalCols = new int[0];
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvJournalVoucher.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvJournalVoucher.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvJournalVoucher.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvJournalVoucher.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvJournalVoucher.HasSlNo = false;
            this.dgvJournalVoucher.LastRowIndex = 0;
            this.dgvJournalVoucher.Location = new System.Drawing.Point(0, 0);
            this.dgvJournalVoucher.Name = "dgvJournalVoucher";
            this.dgvJournalVoucher.NegativeValueCols = new int[0];
            this.dgvJournalVoucher.NumericCols = new int[0];
            this.dgvJournalVoucher.RowHeadersWidth = 50;
            this.dgvJournalVoucher.Size = new System.Drawing.Size(1001, 221);
            this.dgvJournalVoucher.TabIndex = 22;
            this.dgvJournalVoucher.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvJournalVoucher_CellValueChanged);
            this.dgvJournalVoucher.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvJournalVoucher_CellMouseClick);
            this.dgvJournalVoucher.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvJournalVoucher_CellBeginEdit);
            this.dgvJournalVoucher.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvJournalVoucher_EditingControlShowing);
            this.dgvJournalVoucher.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvJournalVoucher_CurrentCellDirtyStateChanged);
            this.dgvJournalVoucher.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvJournalVoucher_DataError);
            this.dgvJournalVoucher.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvJournalVoucher_RowsRemoved);
            this.dgvJournalVoucher.SelectionChanged += new System.EventHandler(this.dgvJournalVoucher_SelectionChanged);
            // 
            // AccountID
            // 
            this.AccountID.DataPropertyName = "AccountID";
            this.AccountID.HeaderText = "Particulars";
            this.AccountID.Name = "AccountID";
            this.AccountID.ReadOnly = true;
            this.AccountID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AccountID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AccountID.Visible = false;
            this.AccountID.Width = 300;
            // 
            // Code
            // 
            this.Code.DataPropertyName = "Code";
            this.Code.HeaderText = "Code";
            this.Code.Name = "Code";
            this.Code.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Code.Width = 150;
            // 
            // AccountName
            // 
            this.AccountName.DataPropertyName = "AccountName";
            this.AccountName.HeaderText = "Account";
            this.AccountName.Name = "AccountName";
            this.AccountName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AccountName.Width = 250;
            // 
            // CurrentBalance
            // 
            this.CurrentBalance.DataPropertyName = "CurrentBalance";
            this.CurrentBalance.HeaderText = "CurrentBalance";
            this.CurrentBalance.Name = "CurrentBalance";
            this.CurrentBalance.ReadOnly = true;
            this.CurrentBalance.Visible = false;
            // 
            // SerialNo
            // 
            this.SerialNo.DataPropertyName = "SerialNo";
            this.SerialNo.HeaderText = "SerialNo";
            this.SerialNo.Name = "SerialNo";
            this.SerialNo.ReadOnly = true;
            this.SerialNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SerialNo.Visible = false;
            // 
            // Remarks
            // 
            this.Remarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.MaxInputLength = 500;
            this.Remarks.Name = "Remarks";
            // 
            // DebitAmount
            // 
            this.DebitAmount.DataPropertyName = "DebitAmount";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DebitAmount.DefaultCellStyle = dataGridViewCellStyle4;
            this.DebitAmount.HeaderText = "Debit";
            this.DebitAmount.MaxInputLength = 9;
            this.DebitAmount.MinimumWidth = 150;
            this.DebitAmount.Name = "DebitAmount";
            this.DebitAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DebitAmount.Width = 150;
            // 
            // CreditAmount
            // 
            this.CreditAmount.DataPropertyName = "CreditAmount";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.CreditAmount.DefaultCellStyle = dataGridViewCellStyle5;
            this.CreditAmount.HeaderText = "Credit";
            this.CreditAmount.MaxInputLength = 9;
            this.CreditAmount.MinimumWidth = 150;
            this.CreditAmount.Name = "CreditAmount";
            this.CreditAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CreditAmount.Width = 150;
            // 
            // cmsGroupJVAdd
            // 
            this.cmsGroupJVAdd.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddAutofill});
            this.cmsGroupJVAdd.Name = "cmsGroupJV";
            this.cmsGroupJVAdd.Size = new System.Drawing.Size(114, 26);
            // 
            // btnAddAutofill
            // 
            this.btnAddAutofill.Name = "btnAddAutofill";
            this.btnAddAutofill.Size = new System.Drawing.Size(113, 22);
            this.btnAddAutofill.Text = "Autofill";
            this.btnAddAutofill.Click += new System.EventHandler(this.btnAddAutofill_Click);
            // 
            // stVoucherEntry
            // 
            this.stVoucherEntry.AttachedControl = this.tpVoucher1;
            this.stVoucherEntry.GlobalItem = false;
            this.stVoucherEntry.Name = "stVoucherEntry";
            this.stVoucherEntry.Text = "Voucher";
            // 
            // tpCostCenter1
            // 
            this.tpCostCenter1.Controls.Add(this.dgvCostCenter);
            this.tpCostCenter1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tpCostCenter1.Location = new System.Drawing.Point(0, 0);
            this.tpCostCenter1.Name = "tpCostCenter1";
            this.tpCostCenter1.Size = new System.Drawing.Size(1001, 246);
            this.tpCostCenter1.TabIndex = 0;
            this.tpCostCenter1.TabItem = this.stCostCenter;
            // 
            // dgvCostCenter
            // 
            this.dgvCostCenter.AddNewRow = false;
            this.dgvCostCenter.AllowUserToResizeColumns = false;
            this.dgvCostCenter.AllowUserToResizeRows = false;
            this.dgvCostCenter.AlphaNumericCols = new int[0];
            this.dgvCostCenter.BackgroundColor = System.Drawing.Color.White;
            this.dgvCostCenter.CapsLockCols = new int[0];
            this.dgvCostCenter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCostCenter.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CostCenterAccountID,
            this.CostCenterID,
            this.Amount});
            this.dgvCostCenter.DecimalCols = new int[0];
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCostCenter.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvCostCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCostCenter.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvCostCenter.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvCostCenter.HasSlNo = false;
            this.dgvCostCenter.LastRowIndex = 0;
            this.dgvCostCenter.Location = new System.Drawing.Point(0, 0);
            this.dgvCostCenter.Name = "dgvCostCenter";
            this.dgvCostCenter.NegativeValueCols = new int[0];
            this.dgvCostCenter.NumericCols = new int[0];
            this.dgvCostCenter.RowHeadersWidth = 50;
            this.dgvCostCenter.Size = new System.Drawing.Size(1001, 246);
            this.dgvCostCenter.TabIndex = 23;
            this.dgvCostCenter.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCostCenter_CellValueChanged);
            // 
            // CostCenterAccountID
            // 
            this.CostCenterAccountID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CostCenterAccountID.DataPropertyName = "CostCenterAccountID";
            this.CostCenterAccountID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CostCenterAccountID.HeaderText = "Particulars";
            this.CostCenterAccountID.Name = "CostCenterAccountID";
            this.CostCenterAccountID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CostCenterAccountID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // CostCenterID
            // 
            this.CostCenterID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CostCenterID.DataPropertyName = "CostCenterID";
            this.CostCenterID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CostCenterID.HeaderText = "CostCenter";
            this.CostCenterID.Name = "CostCenterID";
            this.CostCenterID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CostCenterID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Amount
            // 
            this.Amount.DataPropertyName = "Amount";
            this.Amount.HeaderText = "Amount";
            this.Amount.MaxInputLength = 9;
            this.Amount.MinimumWidth = 150;
            this.Amount.Name = "Amount";
            this.Amount.Width = 150;
            // 
            // stCostCenter
            // 
            this.stCostCenter.AttachedControl = this.tpCostCenter1;
            this.stCostCenter.GlobalItem = false;
            this.stCostCenter.Name = "stCostCenter";
            this.stCostCenter.Text = "CostCenter";
            // 
            // pnlBottom
            // 
            this.pnlBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottom.Controls.Add(this.cboBankAccount);
            this.pnlBottom.Controls.Add(this.label2);
            this.pnlBottom.Controls.Add(this.lblSelectedAccountBalance);
            this.pnlBottom.Controls.Add(this.label4);
            this.pnlBottom.Controls.Add(this.txtChequeNo);
            this.pnlBottom.Controls.Add(this.txtDebitTotal);
            this.pnlBottom.Controls.Add(this.txtCreditTotal);
            this.pnlBottom.Controls.Add(this.lblTotal);
            this.pnlBottom.Controls.Add(this.txtNarration);
            this.pnlBottom.Controls.Add(this.lblNarration);
            this.pnlBottom.Controls.Add(this.label1);
            this.pnlBottom.Controls.Add(this.label3);
            this.pnlBottom.Controls.Add(this.dtpChequeDate);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(3, 312);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1001, 130);
            this.pnlBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottom.Style.GradientAngle = 90;
            this.pnlBottom.TabIndex = 1068;
            // 
            // cboBankAccount
            // 
            this.cboBankAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBankAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBankAccount.DropDownHeight = 105;
            this.cboBankAccount.DropDownWidth = 250;
            this.cboBankAccount.Enabled = false;
            this.cboBankAccount.FormattingEnabled = true;
            this.cboBankAccount.IntegralHeight = false;
            this.cboBankAccount.ItemHeight = 13;
            this.cboBankAccount.Location = new System.Drawing.Point(77, 4);
            this.cboBankAccount.Name = "cboBankAccount";
            this.cboBankAccount.Size = new System.Drawing.Size(250, 21);
            this.cboBankAccount.TabIndex = 1072;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 1072;
            this.label2.Text = "Bank A/c";
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelTop.Controls.Add(this.lblCompany);
            this.panelTop.Controls.Add(this.lblCurrency);
            this.panelTop.Controls.Add(this.lblDate);
            this.panelTop.Controls.Add(this.dtpDate);
            this.panelTop.Controls.Add(this.cboCompany);
            this.panelTop.Controls.Add(this.lblVoucherType);
            this.panelTop.Controls.Add(this.cboVoucherType);
            this.panelTop.Controls.Add(this.txtVoucherNo);
            this.panelTop.Controls.Add(this.lblVoucherNo);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(3, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1001, 41);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 1044;
            // 
            // expnlLeft
            // 
            this.expnlLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expnlLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlLeft.ExpandableControl = this.PanelLeft;
            this.expnlLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expnlLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expnlLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expnlLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expnlLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expnlLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expnlLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expnlLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.expnlLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(179)))), ((int)(((byte)(219)))));
            this.expnlLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expnlLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expnlLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expnlLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expnlLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expnlLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expnlLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expnlLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expnlLeft.Location = new System.Drawing.Point(0, 0);
            this.expnlLeft.Name = "expnlLeft";
            this.expnlLeft.Size = new System.Drawing.Size(3, 464);
            this.expnlLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expnlLeft.TabIndex = 0;
            this.expnlLeft.TabStop = false;
            this.expnlLeft.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expnlLeft_ExpandedChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "SerialNo";
            this.dataGridViewTextBoxColumn1.HeaderText = "SerialNo";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "DebitAmount";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn2.HeaderText = "Debit";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 9;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CreditAmount";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn3.HeaderText = "Credit";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 9;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Amount";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn4.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 150;
            // 
            // pnlLedgerAccounts
            // 
            this.pnlLedgerAccounts.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlLedgerAccounts.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlLedgerAccounts.Controls.Add(this.dgvLedgerAccounts);
            this.pnlLedgerAccounts.Location = new System.Drawing.Point(352, 132);
            this.pnlLedgerAccounts.Name = "pnlLedgerAccounts";
            this.pnlLedgerAccounts.Size = new System.Drawing.Size(550, 130);
            this.pnlLedgerAccounts.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlLedgerAccounts.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlLedgerAccounts.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlLedgerAccounts.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlLedgerAccounts.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlLedgerAccounts.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlLedgerAccounts.Style.GradientAngle = 90;
            this.pnlLedgerAccounts.TabIndex = 1048;
            this.pnlLedgerAccounts.Text = "panelEx1";
            // 
            // dgvLedgerAccounts
            // 
            this.dgvLedgerAccounts.AllowUserToAddRows = false;
            this.dgvLedgerAccounts.AllowUserToDeleteRows = false;
            this.dgvLedgerAccounts.AllowUserToOrderColumns = true;
            this.dgvLedgerAccounts.AllowUserToResizeColumns = false;
            this.dgvLedgerAccounts.AllowUserToResizeRows = false;
            this.dgvLedgerAccounts.BackgroundColor = System.Drawing.Color.White;
            this.dgvLedgerAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLedgerAccounts.ColumnHeadersVisible = false;
            this.dgvLedgerAccounts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LedgerAccountID,
            this.LedgerCode,
            this.LedgerName});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLedgerAccounts.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvLedgerAccounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLedgerAccounts.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvLedgerAccounts.Location = new System.Drawing.Point(0, 0);
            this.dgvLedgerAccounts.Name = "dgvLedgerAccounts";
            this.dgvLedgerAccounts.ReadOnly = true;
            this.dgvLedgerAccounts.RowHeadersVisible = false;
            this.dgvLedgerAccounts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLedgerAccounts.Size = new System.Drawing.Size(550, 130);
            this.dgvLedgerAccounts.TabIndex = 1079;
            this.dgvLedgerAccounts.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLedgerAccounts_CellDoubleClick);
            this.dgvLedgerAccounts.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvLedgerAccounts_KeyDown);
            // 
            // LedgerAccountID
            // 
            this.LedgerAccountID.DataPropertyName = "LedgerAccountID";
            this.LedgerAccountID.HeaderText = "LedgerAccountID";
            this.LedgerAccountID.Name = "LedgerAccountID";
            this.LedgerAccountID.ReadOnly = true;
            this.LedgerAccountID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LedgerAccountID.Visible = false;
            // 
            // LedgerCode
            // 
            this.LedgerCode.DataPropertyName = "LedgerCode";
            this.LedgerCode.HeaderText = "Code";
            this.LedgerCode.Name = "LedgerCode";
            this.LedgerCode.ReadOnly = true;
            this.LedgerCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LedgerCode.Width = 150;
            // 
            // LedgerName
            // 
            this.LedgerName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LedgerName.DataPropertyName = "LedgerName";
            this.LedgerName.HeaderText = "Account";
            this.LedgerName.Name = "LedgerName";
            this.LedgerName.ReadOnly = true;
            this.LedgerName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // frmJournalVoucher
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1254, 464);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.pnlLedgerAccounts);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmJournalVoucher";
            this.Text = "Accounting Voucher Creation";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmJournalVoucher_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bnGeneralReceiptsAndPayments)).EndInit();
            this.bnGeneralReceiptsAndPayments.ResumeLayout(false);
            this.bnGeneralReceiptsAndPayments.PerformLayout();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVNoDisplay)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.panelMiddle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcVoucherEntry)).EndInit();
            this.tcVoucherEntry.ResumeLayout(false);
            this.tpVoucher1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvJournalVoucher)).EndInit();
            this.cmsGroupJVAdd.ResumeLayout(false);
            this.tpCostCenter1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCostCenter)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.pnlLedgerAccounts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLedgerAccounts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator bnGeneralReceiptsAndPayments;
        internal System.Windows.Forms.ToolStripButton btnAddNewItem;
        internal System.Windows.Forms.ToolStripButton btnSaveItem;
        internal System.Windows.Forms.ToolStripButton btnDeleteItem;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        private System.Windows.Forms.TextBox txtChequeNo;
        internal System.Windows.Forms.DateTimePicker dtpChequeDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboVoucherType;
        private System.Windows.Forms.Label lblVoucherType;
        private System.Windows.Forms.ComboBox cboCompany;
        private System.Windows.Forms.Label lblCompany;
        internal System.Windows.Forms.Label lblNarration;
        internal System.Windows.Forms.TextBox txtNarration;
        internal System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label lblDate;
        internal System.Windows.Forms.TextBox txtDebitTotal;
        internal System.Windows.Forms.TextBox txtCreditTotal;
        internal System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Timer tmGeneralReceiptsAndPayments;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.StatusStrip ssStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tslStatus;
        private System.Windows.Forms.TextBox txtVoucherNo;
        private System.Windows.Forms.Label lblVoucherNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblSelectedAccountBalance;
        private System.Windows.Forms.ToolStripButton btnChartofAccounts;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Label lblCurrency;
        private DevComponents.DotNetBar.PanelEx PanelLeft;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvVNoDisplay;
        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.PanelEx panelLeftTop;
        private System.Windows.Forms.DateTimePicker dtpSTo;
        private System.Windows.Forms.DateTimePicker dtpSFrom;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblRFQNo;
        private DevComponents.DotNetBar.ButtonX BtnSRefresh;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtSsearch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboSearchCompany;
        private System.Windows.Forms.Label LblScompany;
        private DevComponents.DotNetBar.LabelX LblSCountStatus;
        private DevComponents.DotNetBar.PanelEx pnlMain;
        private DevComponents.DotNetBar.ExpandableSplitter expnlLeft;
        private DevComponents.DotNetBar.PanelEx pnlBottom;
        private DevComponents.DotNetBar.PanelEx panelTop;
        private DevComponents.DotNetBar.PanelEx panelMiddle;
        private DevComponents.DotNetBar.SuperTabControl tcVoucherEntry;
        private DevComponents.DotNetBar.SuperTabControlPanel tpVoucher1;
        private DevComponents.DotNetBar.SuperTabItem stVoucherEntry;
        private ClsInnerGridBar dgvJournalVoucher;
        private ClsInnerGridBar dgvCostCenter;
        private System.Windows.Forms.DataGridViewComboBoxColumn CostCenterAccountID;
        private System.Windows.Forms.DataGridViewComboBoxColumn CostCenterID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoucherID;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoucherNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoucherDate;
        private DevComponents.DotNetBar.SuperTabControlPanel tpCostCenter1;
        private DevComponents.DotNetBar.SuperTabItem stCostCenter;
        private System.Windows.Forms.ComboBox cboBankAccount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripButton btnCostCenterReference;
        private DevComponents.DotNetBar.PanelEx pnlLedgerAccounts;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvLedgerAccounts;
        private System.Windows.Forms.DataGridViewTextBoxColumn LedgerAccountID;
        private System.Windows.Forms.DataGridViewTextBoxColumn LedgerCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn LedgerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn SerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn DebitAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreditAmount;
        private System.Windows.Forms.ContextMenuStrip cmsGroupJVAdd;
        private System.Windows.Forms.ToolStripMenuItem btnAddAutofill;
    }
}