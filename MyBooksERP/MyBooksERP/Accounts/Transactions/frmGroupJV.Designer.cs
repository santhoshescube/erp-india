﻿namespace MyBooksERP
{
    partial class frmGroupJV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGroupJV));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvVNoDisplay = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.VoucherID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VoucherNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VoucherDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.panelLeftTop = new DevComponents.DotNetBar.PanelEx();
            this.lblRFQNo = new System.Windows.Forms.Label();
            this.TxtSsearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.BtnSRefresh = new DevComponents.DotNetBar.ButtonX();
            this.CboSearchCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.LblScompany = new System.Windows.Forms.Label();
            this.LblSCountStatus = new DevComponents.DotNetBar.LabelX();
            this.pnlMain = new DevComponents.DotNetBar.PanelEx();
            this.panelMiddle = new DevComponents.DotNetBar.PanelEx();
            this.dgvJournalVoucher = new ClsInnerGridBar();
            this.AccountID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DebitAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreditAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JVSerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditVoucherID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvGroupJV = new ClsInnerGridBar();
            this.GJVAccountID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GJVCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GJVAccountName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GJVSerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GJVRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GJVDebitAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GJVCreditAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GJVJVSerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GJVVoucherID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsGroupJV = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnJVEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.btnJVRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAutofill = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlBottom = new DevComponents.DotNetBar.PanelEx();
            this.lblSelectedAccountBalance = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDebitTotal = new System.Windows.Forms.TextBox();
            this.txtCreditTotal = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.txtNarration = new System.Windows.Forms.TextBox();
            this.lblNarration = new System.Windows.Forms.Label();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.panelTop = new DevComponents.DotNetBar.PanelEx();
            this.txtVoucherNo = new System.Windows.Forms.TextBox();
            this.lblVoucherNo = new System.Windows.Forms.Label();
            this.txtAddTotalDebit = new System.Windows.Forms.TextBox();
            this.txtAddTotalCredit = new System.Windows.Forms.TextBox();
            this.lblAddTotal = new System.Windows.Forms.Label();
            this.btnAddJV = new DevComponents.DotNetBar.ButtonX();
            this.dgvAddAccounts = new ClsInnerGridBar();
            this.AddAccountID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddAccountName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddSerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddDebitAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddCreditAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddJVSerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddVoucherID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsGroupJVAdd = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnAddAutofill = new System.Windows.Forms.ToolStripMenuItem();
            this.lblCompany = new System.Windows.Forms.Label();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.bnGeneralReceiptsAndPayments = new System.Windows.Forms.BindingNavigator(this.components);
            this.btnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.btnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.btnChartofAccounts = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.expnlLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.dgvLedgerAccounts = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.LedgerAccountID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LedgerCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LedgerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stCostCenter = new DevComponents.DotNetBar.SuperTabItem();
            this.tmGeneralReceiptsAndPayments = new System.Windows.Forms.Timer(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlLedgerAccounts = new DevComponents.DotNetBar.PanelEx();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVNoDisplay)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            this.panelLeftTop.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.panelMiddle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvJournalVoucher)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroupJV)).BeginInit();
            this.cmsGroupJV.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.ssStatus.SuspendLayout();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddAccounts)).BeginInit();
            this.cmsGroupJVAdd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnGeneralReceiptsAndPayments)).BeginInit();
            this.bnGeneralReceiptsAndPayments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLedgerAccounts)).BeginInit();
            this.pnlLedgerAccounts.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelLeft.Controls.Add(this.dgvVNoDisplay);
            this.PanelLeft.Controls.Add(this.expandablePanel1);
            this.PanelLeft.Controls.Add(this.LblSCountStatus);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(250, 464);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 1045;
            this.PanelLeft.Text = "panelEx1";
            // 
            // dgvVNoDisplay
            // 
            this.dgvVNoDisplay.AllowUserToAddRows = false;
            this.dgvVNoDisplay.AllowUserToDeleteRows = false;
            this.dgvVNoDisplay.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVNoDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvVNoDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVNoDisplay.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VoucherID,
            this.VoucherNo,
            this.VoucherDate});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvVNoDisplay.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvVNoDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvVNoDisplay.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvVNoDisplay.Location = new System.Drawing.Point(0, 171);
            this.dgvVNoDisplay.Name = "dgvVNoDisplay";
            this.dgvVNoDisplay.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVNoDisplay.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvVNoDisplay.RowHeadersVisible = false;
            this.dgvVNoDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVNoDisplay.Size = new System.Drawing.Size(250, 267);
            this.dgvVNoDisplay.TabIndex = 41;
            this.dgvVNoDisplay.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVNoDisplay_CellDoubleClick);
            // 
            // VoucherID
            // 
            this.VoucherID.DataPropertyName = "VoucherID";
            this.VoucherID.HeaderText = "VoucherID";
            this.VoucherID.Name = "VoucherID";
            this.VoucherID.ReadOnly = true;
            this.VoucherID.Visible = false;
            // 
            // VoucherNo
            // 
            this.VoucherNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.VoucherNo.DataPropertyName = "VoucherNo";
            this.VoucherNo.HeaderText = "VoucherNo";
            this.VoucherNo.Name = "VoucherNo";
            this.VoucherNo.ReadOnly = true;
            // 
            // VoucherDate
            // 
            this.VoucherDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.VoucherDate.DataPropertyName = "VoucherDate";
            this.VoucherDate.HeaderText = "VoucherDate";
            this.VoucherDate.Name = "VoucherDate";
            this.VoucherDate.ReadOnly = true;
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.InactiveCaption;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expandablePanel1.Controls.Add(this.panelLeftTop);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(250, 171);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 101;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Search";
            // 
            // panelLeftTop
            // 
            this.panelLeftTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelLeftTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelLeftTop.Controls.Add(this.lblRFQNo);
            this.panelLeftTop.Controls.Add(this.TxtSsearch);
            this.panelLeftTop.Controls.Add(this.dtpSTo);
            this.panelLeftTop.Controls.Add(this.dtpSFrom);
            this.panelLeftTop.Controls.Add(this.lblTo);
            this.panelLeftTop.Controls.Add(this.lblFrom);
            this.panelLeftTop.Controls.Add(this.BtnSRefresh);
            this.panelLeftTop.Controls.Add(this.CboSearchCompany);
            this.panelLeftTop.Controls.Add(this.LblScompany);
            this.panelLeftTop.Location = new System.Drawing.Point(0, 26);
            this.panelLeftTop.Name = "panelLeftTop";
            this.panelLeftTop.Size = new System.Drawing.Size(250, 145);
            this.panelLeftTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelLeftTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelLeftTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelLeftTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelLeftTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelLeftTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelLeftTop.Style.GradientAngle = 90;
            this.panelLeftTop.TabIndex = 105;
            // 
            // lblRFQNo
            // 
            this.lblRFQNo.AutoSize = true;
            this.lblRFQNo.Location = new System.Drawing.Point(3, 90);
            this.lblRFQNo.Name = "lblRFQNo";
            this.lblRFQNo.Size = new System.Drawing.Size(67, 13);
            this.lblRFQNo.TabIndex = 248;
            this.lblRFQNo.Text = "Voucher No.";
            // 
            // TxtSsearch
            // 
            this.TxtSsearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.TxtSsearch.Border.Class = "TextBoxBorder";
            this.TxtSsearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtSsearch.Location = new System.Drawing.Point(74, 88);
            this.TxtSsearch.MaxLength = 20;
            this.TxtSsearch.Name = "TxtSsearch";
            this.TxtSsearch.Size = new System.Drawing.Size(169, 20);
            this.TxtSsearch.TabIndex = 247;
            this.TxtSsearch.WatermarkEnabled = false;
            this.TxtSsearch.WatermarkText = "RFQNo.";
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(74, 62);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.ShowCheckBox = true;
            this.dtpSTo.Size = new System.Drawing.Size(119, 20);
            this.dtpSTo.TabIndex = 246;
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(74, 36);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.ShowCheckBox = true;
            this.dtpSFrom.Size = new System.Drawing.Size(119, 20);
            this.dtpSFrom.TabIndex = 245;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(3, 62);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 244;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(3, 39);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 243;
            this.lblFrom.Text = "From";
            // 
            // BtnSRefresh
            // 
            this.BtnSRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnSRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BtnSRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnSRefresh.Location = new System.Drawing.Point(173, 114);
            this.BtnSRefresh.Name = "BtnSRefresh";
            this.BtnSRefresh.Size = new System.Drawing.Size(71, 23);
            this.BtnSRefresh.TabIndex = 40;
            this.BtnSRefresh.Text = "Refresh";
            this.BtnSRefresh.Click += new System.EventHandler(this.BtnSRefresh_Click);
            // 
            // CboSearchCompany
            // 
            this.CboSearchCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CboSearchCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSearchCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSearchCompany.DisplayMember = "Text";
            this.CboSearchCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboSearchCompany.DropDownHeight = 75;
            this.CboSearchCompany.Enabled = false;
            this.CboSearchCompany.FormattingEnabled = true;
            this.CboSearchCompany.IntegralHeight = false;
            this.CboSearchCompany.ItemHeight = 14;
            this.CboSearchCompany.Location = new System.Drawing.Point(74, 10);
            this.CboSearchCompany.Name = "CboSearchCompany";
            this.CboSearchCompany.Size = new System.Drawing.Size(169, 20);
            this.CboSearchCompany.TabIndex = 36;
            // 
            // LblScompany
            // 
            this.LblScompany.AutoSize = true;
            this.LblScompany.Location = new System.Drawing.Point(3, 13);
            this.LblScompany.Name = "LblScompany";
            this.LblScompany.Size = new System.Drawing.Size(51, 13);
            this.LblScompany.TabIndex = 109;
            this.LblScompany.Text = "Company";
            // 
            // LblSCountStatus
            // 
            // 
            // 
            // 
            this.LblSCountStatus.BackgroundStyle.Class = "";
            this.LblSCountStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblSCountStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LblSCountStatus.Location = new System.Drawing.Point(0, 438);
            this.LblSCountStatus.Name = "LblSCountStatus";
            this.LblSCountStatus.Size = new System.Drawing.Size(250, 26);
            this.LblSCountStatus.TabIndex = 105;
            this.LblSCountStatus.Text = "...";
            // 
            // pnlMain
            // 
            this.pnlMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlMain.Controls.Add(this.panelMiddle);
            this.pnlMain.Controls.Add(this.pnlBottom);
            this.pnlMain.Controls.Add(this.ssStatus);
            this.pnlMain.Controls.Add(this.panelTop);
            this.pnlMain.Controls.Add(this.bnGeneralReceiptsAndPayments);
            this.pnlMain.Controls.Add(this.expnlLeft);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(250, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1004, 464);
            this.pnlMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlMain.Style.GradientAngle = 90;
            this.pnlMain.TabIndex = 1046;
            // 
            // panelMiddle
            // 
            this.panelMiddle.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelMiddle.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelMiddle.Controls.Add(this.dgvJournalVoucher);
            this.panelMiddle.Controls.Add(this.dgvGroupJV);
            this.panelMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMiddle.Location = new System.Drawing.Point(3, 215);
            this.panelMiddle.Name = "panelMiddle";
            this.panelMiddle.Size = new System.Drawing.Size(1001, 149);
            this.panelMiddle.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelMiddle.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelMiddle.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelMiddle.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelMiddle.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelMiddle.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelMiddle.Style.GradientAngle = 90;
            this.panelMiddle.TabIndex = 1069;
            // 
            // dgvJournalVoucher
            // 
            this.dgvJournalVoucher.AddNewRow = false;
            this.dgvJournalVoucher.AllowUserToAddRows = false;
            this.dgvJournalVoucher.AllowUserToDeleteRows = false;
            this.dgvJournalVoucher.AllowUserToResizeColumns = false;
            this.dgvJournalVoucher.AllowUserToResizeRows = false;
            this.dgvJournalVoucher.AlphaNumericCols = new int[0];
            this.dgvJournalVoucher.BackgroundColor = System.Drawing.Color.White;
            this.dgvJournalVoucher.CapsLockCols = new int[0];
            this.dgvJournalVoucher.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvJournalVoucher.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AccountID,
            this.Code,
            this.AccountName,
            this.SerialNo,
            this.Remarks,
            this.DebitAmount,
            this.CreditAmount,
            this.JVSerialNo,
            this.EditVoucherID});
            this.dgvJournalVoucher.DecimalCols = new int[0];
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvJournalVoucher.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvJournalVoucher.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvJournalVoucher.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvJournalVoucher.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvJournalVoucher.HasSlNo = false;
            this.dgvJournalVoucher.LastRowIndex = 0;
            this.dgvJournalVoucher.Location = new System.Drawing.Point(0, 96);
            this.dgvJournalVoucher.Name = "dgvJournalVoucher";
            this.dgvJournalVoucher.NegativeValueCols = new int[0];
            this.dgvJournalVoucher.NumericCols = new int[0];
            this.dgvJournalVoucher.RowHeadersWidth = 50;
            this.dgvJournalVoucher.Size = new System.Drawing.Size(1001, 53);
            this.dgvJournalVoucher.TabIndex = 22;
            // 
            // AccountID
            // 
            this.AccountID.DataPropertyName = "AccountID";
            this.AccountID.HeaderText = "AccountID";
            this.AccountID.Name = "AccountID";
            this.AccountID.ReadOnly = true;
            this.AccountID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AccountID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Code
            // 
            this.Code.DataPropertyName = "Code";
            this.Code.HeaderText = "Code";
            this.Code.Name = "Code";
            this.Code.ReadOnly = true;
            this.Code.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Code.Width = 150;
            // 
            // AccountName
            // 
            this.AccountName.DataPropertyName = "AccountName";
            this.AccountName.HeaderText = "Account";
            this.AccountName.Name = "AccountName";
            this.AccountName.ReadOnly = true;
            this.AccountName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AccountName.Width = 250;
            // 
            // SerialNo
            // 
            this.SerialNo.DataPropertyName = "SerialNo";
            this.SerialNo.HeaderText = "SerialNo";
            this.SerialNo.Name = "SerialNo";
            this.SerialNo.ReadOnly = true;
            this.SerialNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Remarks
            // 
            this.Remarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Remarks.DataPropertyName = "Remarks";
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.MaxInputLength = 500;
            this.Remarks.Name = "Remarks";
            this.Remarks.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // DebitAmount
            // 
            this.DebitAmount.DataPropertyName = "DebitAmount";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DebitAmount.DefaultCellStyle = dataGridViewCellStyle4;
            this.DebitAmount.HeaderText = "Debit";
            this.DebitAmount.MaxInputLength = 9;
            this.DebitAmount.MinimumWidth = 150;
            this.DebitAmount.Name = "DebitAmount";
            this.DebitAmount.ReadOnly = true;
            this.DebitAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DebitAmount.Width = 150;
            // 
            // CreditAmount
            // 
            this.CreditAmount.DataPropertyName = "CreditAmount";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.CreditAmount.DefaultCellStyle = dataGridViewCellStyle5;
            this.CreditAmount.HeaderText = "Credit";
            this.CreditAmount.MaxInputLength = 9;
            this.CreditAmount.MinimumWidth = 150;
            this.CreditAmount.Name = "CreditAmount";
            this.CreditAmount.ReadOnly = true;
            this.CreditAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CreditAmount.Width = 150;
            // 
            // JVSerialNo
            // 
            this.JVSerialNo.DataPropertyName = "JVSerialNo";
            this.JVSerialNo.HeaderText = "JVSerialNo";
            this.JVSerialNo.Name = "JVSerialNo";
            this.JVSerialNo.ReadOnly = true;
            this.JVSerialNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // EditVoucherID
            // 
            this.EditVoucherID.DataPropertyName = "EditVoucherID";
            this.EditVoucherID.HeaderText = "EditVoucherID";
            this.EditVoucherID.Name = "EditVoucherID";
            this.EditVoucherID.ReadOnly = true;
            this.EditVoucherID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvGroupJV
            // 
            this.dgvGroupJV.AddNewRow = false;
            this.dgvGroupJV.AllowUserToAddRows = false;
            this.dgvGroupJV.AllowUserToDeleteRows = false;
            this.dgvGroupJV.AllowUserToResizeColumns = false;
            this.dgvGroupJV.AllowUserToResizeRows = false;
            this.dgvGroupJV.AlphaNumericCols = new int[0];
            this.dgvGroupJV.BackgroundColor = System.Drawing.Color.White;
            this.dgvGroupJV.CapsLockCols = new int[0];
            this.dgvGroupJV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGroupJV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GJVAccountID,
            this.GJVCode,
            this.GJVAccountName,
            this.GJVSerialNo,
            this.GJVRemarks,
            this.GJVDebitAmount,
            this.GJVCreditAmount,
            this.GJVJVSerialNo,
            this.GJVVoucherID});
            this.dgvGroupJV.ContextMenuStrip = this.cmsGroupJV;
            this.dgvGroupJV.DecimalCols = new int[0];
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGroupJV.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgvGroupJV.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvGroupJV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvGroupJV.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvGroupJV.HasSlNo = false;
            this.dgvGroupJV.LastRowIndex = 0;
            this.dgvGroupJV.Location = new System.Drawing.Point(0, 0);
            this.dgvGroupJV.Name = "dgvGroupJV";
            this.dgvGroupJV.NegativeValueCols = new int[0];
            this.dgvGroupJV.NumericCols = new int[0];
            this.dgvGroupJV.RowHeadersWidth = 50;
            this.dgvGroupJV.Size = new System.Drawing.Size(1001, 96);
            this.dgvGroupJV.TabIndex = 23;
            this.dgvGroupJV.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGroupJV_CellDoubleClick);
            this.dgvGroupJV.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvGroupJV_RowsRemoved);
            // 
            // GJVAccountID
            // 
            this.GJVAccountID.DataPropertyName = "GJVAccountID";
            this.GJVAccountID.HeaderText = "AccountID";
            this.GJVAccountID.Name = "GJVAccountID";
            this.GJVAccountID.ReadOnly = true;
            this.GJVAccountID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.GJVAccountID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // GJVCode
            // 
            this.GJVCode.DataPropertyName = "GJVCode";
            this.GJVCode.HeaderText = "Code";
            this.GJVCode.Name = "GJVCode";
            this.GJVCode.ReadOnly = true;
            this.GJVCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.GJVCode.Width = 150;
            // 
            // GJVAccountName
            // 
            this.GJVAccountName.DataPropertyName = "GJVAccountName";
            this.GJVAccountName.HeaderText = "Account";
            this.GJVAccountName.Name = "GJVAccountName";
            this.GJVAccountName.ReadOnly = true;
            this.GJVAccountName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.GJVAccountName.Width = 250;
            // 
            // GJVSerialNo
            // 
            this.GJVSerialNo.DataPropertyName = "GJVSerialNo";
            this.GJVSerialNo.HeaderText = "SerialNo";
            this.GJVSerialNo.Name = "GJVSerialNo";
            this.GJVSerialNo.ReadOnly = true;
            this.GJVSerialNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // GJVRemarks
            // 
            this.GJVRemarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.GJVRemarks.DataPropertyName = "GJVRemarks";
            this.GJVRemarks.HeaderText = "Remarks";
            this.GJVRemarks.MaxInputLength = 500;
            this.GJVRemarks.Name = "GJVRemarks";
            this.GJVRemarks.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // GJVDebitAmount
            // 
            this.GJVDebitAmount.DataPropertyName = "GJVDebitAmount";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.GJVDebitAmount.DefaultCellStyle = dataGridViewCellStyle7;
            this.GJVDebitAmount.HeaderText = "Debit";
            this.GJVDebitAmount.MaxInputLength = 9;
            this.GJVDebitAmount.MinimumWidth = 150;
            this.GJVDebitAmount.Name = "GJVDebitAmount";
            this.GJVDebitAmount.ReadOnly = true;
            this.GJVDebitAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.GJVDebitAmount.Width = 150;
            // 
            // GJVCreditAmount
            // 
            this.GJVCreditAmount.DataPropertyName = "GJVCreditAmount";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.GJVCreditAmount.DefaultCellStyle = dataGridViewCellStyle8;
            this.GJVCreditAmount.HeaderText = "Credit";
            this.GJVCreditAmount.MaxInputLength = 9;
            this.GJVCreditAmount.MinimumWidth = 150;
            this.GJVCreditAmount.Name = "GJVCreditAmount";
            this.GJVCreditAmount.ReadOnly = true;
            this.GJVCreditAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.GJVCreditAmount.Width = 150;
            // 
            // GJVJVSerialNo
            // 
            this.GJVJVSerialNo.DataPropertyName = "GJVJVSerialNo";
            this.GJVJVSerialNo.HeaderText = "JVSerialNo";
            this.GJVJVSerialNo.Name = "GJVJVSerialNo";
            this.GJVJVSerialNo.ReadOnly = true;
            this.GJVJVSerialNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // GJVVoucherID
            // 
            this.GJVVoucherID.DataPropertyName = "GJVVoucherID";
            this.GJVVoucherID.HeaderText = "EditVoucherID";
            this.GJVVoucherID.Name = "GJVVoucherID";
            this.GJVVoucherID.ReadOnly = true;
            this.GJVVoucherID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cmsGroupJV
            // 
            this.cmsGroupJV.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnJVEdit,
            this.btnJVRemove,
            this.btnAutofill});
            this.cmsGroupJV.Name = "cmsGroupJV";
            this.cmsGroupJV.Size = new System.Drawing.Size(118, 70);
            // 
            // btnJVEdit
            // 
            this.btnJVEdit.Name = "btnJVEdit";
            this.btnJVEdit.Size = new System.Drawing.Size(117, 22);
            this.btnJVEdit.Text = "Edit";
            this.btnJVEdit.Click += new System.EventHandler(this.btnJVEdit_Click);
            // 
            // btnJVRemove
            // 
            this.btnJVRemove.Name = "btnJVRemove";
            this.btnJVRemove.Size = new System.Drawing.Size(117, 22);
            this.btnJVRemove.Text = "Remove";
            this.btnJVRemove.Click += new System.EventHandler(this.btnJVRemove_Click);
            // 
            // btnAutofill
            // 
            this.btnAutofill.Name = "btnAutofill";
            this.btnAutofill.Size = new System.Drawing.Size(117, 22);
            this.btnAutofill.Text = "Autofill";
            this.btnAutofill.Click += new System.EventHandler(this.btnAutofill_Click);
            // 
            // pnlBottom
            // 
            this.pnlBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottom.Controls.Add(this.lblSelectedAccountBalance);
            this.pnlBottom.Controls.Add(this.label4);
            this.pnlBottom.Controls.Add(this.txtDebitTotal);
            this.pnlBottom.Controls.Add(this.txtCreditTotal);
            this.pnlBottom.Controls.Add(this.lblTotal);
            this.pnlBottom.Controls.Add(this.txtNarration);
            this.pnlBottom.Controls.Add(this.lblNarration);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(3, 364);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1001, 78);
            this.pnlBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottom.Style.GradientAngle = 90;
            this.pnlBottom.TabIndex = 1068;
            // 
            // lblSelectedAccountBalance
            // 
            this.lblSelectedAccountBalance.AutoSize = true;
            this.lblSelectedAccountBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectedAccountBalance.Location = new System.Drawing.Point(721, 47);
            this.lblSelectedAccountBalance.Name = "lblSelectedAccountBalance";
            this.lblSelectedAccountBalance.Size = new System.Drawing.Size(10, 12);
            this.lblSelectedAccountBalance.TabIndex = 1071;
            this.lblSelectedAccountBalance.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(656, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 1070;
            this.label4.Text = "Acc Cur Bal :";
            // 
            // txtDebitTotal
            // 
            this.txtDebitTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDebitTotal.BackColor = System.Drawing.Color.White;
            this.txtDebitTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDebitTotal.Enabled = false;
            this.txtDebitTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDebitTotal.Location = new System.Drawing.Point(697, 3);
            this.txtDebitTotal.MaxLength = 25;
            this.txtDebitTotal.Name = "txtDebitTotal";
            this.txtDebitTotal.ReadOnly = true;
            this.txtDebitTotal.Size = new System.Drawing.Size(150, 22);
            this.txtDebitTotal.TabIndex = 0;
            this.txtDebitTotal.Text = "0";
            this.txtDebitTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDebitTotal.TextChanged += new System.EventHandler(this.txtDebitTotal_TextChanged);
            // 
            // txtCreditTotal
            // 
            this.txtCreditTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreditTotal.BackColor = System.Drawing.Color.White;
            this.txtCreditTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCreditTotal.Enabled = false;
            this.txtCreditTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditTotal.Location = new System.Drawing.Point(848, 3);
            this.txtCreditTotal.MaxLength = 25;
            this.txtCreditTotal.Name = "txtCreditTotal";
            this.txtCreditTotal.ReadOnly = true;
            this.txtCreditTotal.Size = new System.Drawing.Size(150, 22);
            this.txtCreditTotal.TabIndex = 1045;
            this.txtCreditTotal.Text = "0";
            this.txtCreditTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotal
            // 
            this.lblTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(655, 7);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(36, 13);
            this.lblTotal.TabIndex = 1050;
            this.lblTotal.Text = "Total";
            // 
            // txtNarration
            // 
            this.txtNarration.Location = new System.Drawing.Point(77, 3);
            this.txtNarration.MaxLength = 2000;
            this.txtNarration.Multiline = true;
            this.txtNarration.Name = "txtNarration";
            this.txtNarration.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtNarration.Size = new System.Drawing.Size(542, 71);
            this.txtNarration.TabIndex = 4;
            // 
            // lblNarration
            // 
            this.lblNarration.AutoSize = true;
            this.lblNarration.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNarration.Location = new System.Drawing.Point(6, 7);
            this.lblNarration.Name = "lblNarration";
            this.lblNarration.Size = new System.Drawing.Size(60, 13);
            this.lblNarration.TabIndex = 1050;
            this.lblNarration.Text = "Narration";
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tslStatus});
            this.ssStatus.Location = new System.Drawing.Point(3, 442);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(1001, 22);
            this.ssStatus.TabIndex = 1048;
            this.ssStatus.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(45, 17);
            this.toolStripStatusLabel1.Text = "Status: ";
            // 
            // tslStatus
            // 
            this.tslStatus.Name = "tslStatus";
            this.tslStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // panelTop
            // 
            this.panelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelTop.Controls.Add(this.txtVoucherNo);
            this.panelTop.Controls.Add(this.lblVoucherNo);
            this.panelTop.Controls.Add(this.txtAddTotalDebit);
            this.panelTop.Controls.Add(this.txtAddTotalCredit);
            this.panelTop.Controls.Add(this.lblAddTotal);
            this.panelTop.Controls.Add(this.btnAddJV);
            this.panelTop.Controls.Add(this.dgvAddAccounts);
            this.panelTop.Controls.Add(this.lblCompany);
            this.panelTop.Controls.Add(this.lblCurrency);
            this.panelTop.Controls.Add(this.lblDate);
            this.panelTop.Controls.Add(this.dtpDate);
            this.panelTop.Controls.Add(this.cboCompany);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(3, 25);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1001, 190);
            this.panelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelTop.Style.GradientAngle = 90;
            this.panelTop.TabIndex = 1044;
            // 
            // txtVoucherNo
            // 
            this.txtVoucherNo.BackColor = System.Drawing.SystemColors.Info;
            this.txtVoucherNo.Location = new System.Drawing.Point(740, 11);
            this.txtVoucherNo.Name = "txtVoucherNo";
            this.txtVoucherNo.Size = new System.Drawing.Size(138, 20);
            this.txtVoucherNo.TabIndex = 1078;
            // 
            // lblVoucherNo
            // 
            this.lblVoucherNo.AutoSize = true;
            this.lblVoucherNo.Location = new System.Drawing.Point(651, 14);
            this.lblVoucherNo.Name = "lblVoucherNo";
            this.lblVoucherNo.Size = new System.Drawing.Size(64, 13);
            this.lblVoucherNo.TabIndex = 1077;
            this.lblVoucherNo.Text = "Voucher No";
            // 
            // txtAddTotalDebit
            // 
            this.txtAddTotalDebit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAddTotalDebit.BackColor = System.Drawing.Color.White;
            this.txtAddTotalDebit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddTotalDebit.Enabled = false;
            this.txtAddTotalDebit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddTotalDebit.Location = new System.Drawing.Point(697, 165);
            this.txtAddTotalDebit.MaxLength = 25;
            this.txtAddTotalDebit.Name = "txtAddTotalDebit";
            this.txtAddTotalDebit.ReadOnly = true;
            this.txtAddTotalDebit.Size = new System.Drawing.Size(150, 22);
            this.txtAddTotalDebit.TabIndex = 1074;
            this.txtAddTotalDebit.Text = "0";
            this.txtAddTotalDebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAddTotalDebit.TextChanged += new System.EventHandler(this.txtAddTotalDebit_TextChanged);
            // 
            // txtAddTotalCredit
            // 
            this.txtAddTotalCredit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAddTotalCredit.BackColor = System.Drawing.Color.White;
            this.txtAddTotalCredit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddTotalCredit.Enabled = false;
            this.txtAddTotalCredit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddTotalCredit.Location = new System.Drawing.Point(848, 165);
            this.txtAddTotalCredit.MaxLength = 25;
            this.txtAddTotalCredit.Name = "txtAddTotalCredit";
            this.txtAddTotalCredit.ReadOnly = true;
            this.txtAddTotalCredit.Size = new System.Drawing.Size(150, 22);
            this.txtAddTotalCredit.TabIndex = 1075;
            this.txtAddTotalCredit.Text = "0";
            this.txtAddTotalCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblAddTotal
            // 
            this.lblAddTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAddTotal.AutoSize = true;
            this.lblAddTotal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddTotal.Location = new System.Drawing.Point(655, 169);
            this.lblAddTotal.Name = "lblAddTotal";
            this.lblAddTotal.Size = new System.Drawing.Size(36, 13);
            this.lblAddTotal.TabIndex = 1076;
            this.lblAddTotal.Text = "Total";
            // 
            // btnAddJV
            // 
            this.btnAddJV.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddJV.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddJV.Image = global::MyBooksERP.Properties.Resources.Add1;
            this.btnAddJV.Location = new System.Drawing.Point(9, 165);
            this.btnAddJV.Name = "btnAddJV";
            this.btnAddJV.Size = new System.Drawing.Size(63, 23);
            this.btnAddJV.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddJV.TabIndex = 1073;
            this.btnAddJV.Text = "Add";
            this.btnAddJV.Click += new System.EventHandler(this.btnAddJV_Click);
            // 
            // dgvAddAccounts
            // 
            this.dgvAddAccounts.AddNewRow = false;
            this.dgvAddAccounts.AllowUserToResizeColumns = false;
            this.dgvAddAccounts.AllowUserToResizeRows = false;
            this.dgvAddAccounts.AlphaNumericCols = new int[0];
            this.dgvAddAccounts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAddAccounts.BackgroundColor = System.Drawing.Color.White;
            this.dgvAddAccounts.CapsLockCols = new int[0];
            this.dgvAddAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAddAccounts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AddAccountID,
            this.AddCode,
            this.AddAccountName,
            this.AddSerialNo,
            this.AddRemarks,
            this.AddDebitAmount,
            this.AddCreditAmount,
            this.AddJVSerialNo,
            this.AddVoucherID});
            this.dgvAddAccounts.ContextMenuStrip = this.cmsGroupJVAdd;
            this.dgvAddAccounts.DecimalCols = new int[0];
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAddAccounts.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvAddAccounts.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvAddAccounts.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvAddAccounts.HasSlNo = false;
            this.dgvAddAccounts.LastRowIndex = 0;
            this.dgvAddAccounts.Location = new System.Drawing.Point(0, 38);
            this.dgvAddAccounts.Name = "dgvAddAccounts";
            this.dgvAddAccounts.NegativeValueCols = new int[0];
            this.dgvAddAccounts.NumericCols = new int[0];
            this.dgvAddAccounts.RowHeadersWidth = 50;
            this.dgvAddAccounts.Size = new System.Drawing.Size(1001, 126);
            this.dgvAddAccounts.TabIndex = 1072;
            this.dgvAddAccounts.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAddAccounts_CellValueChanged);
            this.dgvAddAccounts.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvAddAccounts_CellMouseClick);
            this.dgvAddAccounts.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvAddAccounts_EditingControlShowing);
            this.dgvAddAccounts.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvAddAccounts_CurrentCellDirtyStateChanged);
            this.dgvAddAccounts.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvAddAccounts_DataError);
            this.dgvAddAccounts.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvAddAccounts_RowsRemoved);
            this.dgvAddAccounts.SelectionChanged += new System.EventHandler(this.dgvAddAccounts_SelectionChanged);
            // 
            // AddAccountID
            // 
            this.AddAccountID.DataPropertyName = "AddAccountID";
            this.AddAccountID.HeaderText = "AddAccountID";
            this.AddAccountID.Name = "AddAccountID";
            this.AddAccountID.ReadOnly = true;
            this.AddAccountID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AddAccountID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AddAccountID.Visible = false;
            // 
            // AddCode
            // 
            this.AddCode.DataPropertyName = "AddCode";
            this.AddCode.HeaderText = "Code";
            this.AddCode.Name = "AddCode";
            this.AddCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AddCode.Width = 150;
            // 
            // AddAccountName
            // 
            this.AddAccountName.DataPropertyName = "AddAccountName";
            this.AddAccountName.HeaderText = "Account";
            this.AddAccountName.Name = "AddAccountName";
            this.AddAccountName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AddAccountName.Width = 250;
            // 
            // AddSerialNo
            // 
            this.AddSerialNo.DataPropertyName = "AddSerialNo";
            this.AddSerialNo.HeaderText = "AddSerialNo";
            this.AddSerialNo.Name = "AddSerialNo";
            this.AddSerialNo.ReadOnly = true;
            this.AddSerialNo.Visible = false;
            // 
            // AddRemarks
            // 
            this.AddRemarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AddRemarks.DataPropertyName = "AddRemarks";
            this.AddRemarks.HeaderText = "Remarks";
            this.AddRemarks.MaxInputLength = 500;
            this.AddRemarks.Name = "AddRemarks";
            // 
            // AddDebitAmount
            // 
            this.AddDebitAmount.DataPropertyName = "AddDebitAmount";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.AddDebitAmount.DefaultCellStyle = dataGridViewCellStyle10;
            this.AddDebitAmount.HeaderText = "Debit";
            this.AddDebitAmount.MaxInputLength = 9;
            this.AddDebitAmount.MinimumWidth = 150;
            this.AddDebitAmount.Name = "AddDebitAmount";
            this.AddDebitAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AddDebitAmount.Width = 150;
            // 
            // AddCreditAmount
            // 
            this.AddCreditAmount.DataPropertyName = "AddCreditAmount";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.AddCreditAmount.DefaultCellStyle = dataGridViewCellStyle11;
            this.AddCreditAmount.HeaderText = "Credit";
            this.AddCreditAmount.MaxInputLength = 9;
            this.AddCreditAmount.MinimumWidth = 150;
            this.AddCreditAmount.Name = "AddCreditAmount";
            this.AddCreditAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AddCreditAmount.Width = 150;
            // 
            // AddJVSerialNo
            // 
            this.AddJVSerialNo.DataPropertyName = "AddJVSerialNo";
            this.AddJVSerialNo.HeaderText = "AddJVSerialNo";
            this.AddJVSerialNo.Name = "AddJVSerialNo";
            this.AddJVSerialNo.ReadOnly = true;
            this.AddJVSerialNo.Visible = false;
            // 
            // AddVoucherID
            // 
            this.AddVoucherID.DataPropertyName = "AddVoucherID";
            this.AddVoucherID.HeaderText = "AddVoucherID";
            this.AddVoucherID.Name = "AddVoucherID";
            this.AddVoucherID.ReadOnly = true;
            this.AddVoucherID.Visible = false;
            // 
            // cmsGroupJVAdd
            // 
            this.cmsGroupJVAdd.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddAutofill});
            this.cmsGroupJVAdd.Name = "cmsGroupJV";
            this.cmsGroupJVAdd.Size = new System.Drawing.Size(114, 26);
            // 
            // btnAddAutofill
            // 
            this.btnAddAutofill.Name = "btnAddAutofill";
            this.btnAddAutofill.Size = new System.Drawing.Size(113, 22);
            this.btnAddAutofill.Text = "Autofill";
            this.btnAddAutofill.Click += new System.EventHandler(this.btnAddAutofill_Click);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Location = new System.Drawing.Point(20, 14);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 1056;
            this.lblCompany.Text = "Company";
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.Location = new System.Drawing.Point(333, 14);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(58, 13);
            this.lblCurrency.TabIndex = 1071;
            this.lblCurrency.Text = "Currency : ";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(458, 14);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 1035;
            this.lblDate.Text = "Date";
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(513, 11);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(106, 20);
            this.dtpDate.TabIndex = 2;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboCompany.DropDownHeight = 105;
            this.cboCompany.DropDownWidth = 250;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 13;
            this.cboCompany.Location = new System.Drawing.Point(77, 11);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(250, 21);
            this.cboCompany.TabIndex = 0;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            // 
            // bnGeneralReceiptsAndPayments
            // 
            this.bnGeneralReceiptsAndPayments.AddNewItem = null;
            this.bnGeneralReceiptsAndPayments.BackColor = System.Drawing.Color.Transparent;
            this.bnGeneralReceiptsAndPayments.CountItem = null;
            this.bnGeneralReceiptsAndPayments.DeleteItem = null;
            this.bnGeneralReceiptsAndPayments.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddNewItem,
            this.btnSaveItem,
            this.btnDeleteItem,
            this.btnClear,
            this.toolStripSeparator7,
            this.btnChartofAccounts,
            this.toolStripSeparator1,
            this.btnPrint,
            this.btnEmail});
            this.bnGeneralReceiptsAndPayments.Location = new System.Drawing.Point(3, 0);
            this.bnGeneralReceiptsAndPayments.MoveFirstItem = null;
            this.bnGeneralReceiptsAndPayments.MoveLastItem = null;
            this.bnGeneralReceiptsAndPayments.MoveNextItem = null;
            this.bnGeneralReceiptsAndPayments.MovePreviousItem = null;
            this.bnGeneralReceiptsAndPayments.Name = "bnGeneralReceiptsAndPayments";
            this.bnGeneralReceiptsAndPayments.PositionItem = null;
            this.bnGeneralReceiptsAndPayments.Size = new System.Drawing.Size(1001, 25);
            this.bnGeneralReceiptsAndPayments.TabIndex = 1041;
            // 
            // btnAddNewItem
            // 
            this.btnAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNewItem.Image")));
            this.btnAddNewItem.Name = "btnAddNewItem";
            this.btnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.btnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.btnAddNewItem.Text = "Add";
            this.btnAddNewItem.ToolTipText = "Add";
            this.btnAddNewItem.Click += new System.EventHandler(this.btnAddNewItem_Click);
            // 
            // btnSaveItem
            // 
            this.btnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveItem.Image")));
            this.btnSaveItem.Name = "btnSaveItem";
            this.btnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.btnSaveItem.Text = "Save";
            this.btnSaveItem.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteItem.Image")));
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.RightToLeftAutoMirrorImage = true;
            this.btnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.btnDeleteItem.Text = "Delete";
            this.btnDeleteItem.ToolTipText = "Delete";
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.ToolTipText = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnAddNewItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // btnChartofAccounts
            // 
            this.btnChartofAccounts.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnChartofAccounts.Image = global::MyBooksERP.Properties.Resources.Chart_Of_Accounts;
            this.btnChartofAccounts.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnChartofAccounts.Name = "btnChartofAccounts";
            this.btnChartofAccounts.Size = new System.Drawing.Size(23, 22);
            this.btnChartofAccounts.Text = "Chart of Accounts";
            this.btnChartofAccounts.Click += new System.EventHandler(this.btnChartofAccounts_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = ((System.Drawing.Image)(resources.GetObject("btnEmail.Image")));
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // expnlLeft
            // 
            this.expnlLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expnlLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlLeft.ExpandableControl = this.PanelLeft;
            this.expnlLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expnlLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expnlLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expnlLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expnlLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expnlLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expnlLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expnlLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expnlLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expnlLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expnlLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expnlLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expnlLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expnlLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expnlLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expnlLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expnlLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expnlLeft.Location = new System.Drawing.Point(0, 0);
            this.expnlLeft.Name = "expnlLeft";
            this.expnlLeft.Size = new System.Drawing.Size(3, 464);
            this.expnlLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expnlLeft.TabIndex = 0;
            this.expnlLeft.TabStop = false;
            this.expnlLeft.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expnlLeft_ExpandedChanged);
            // 
            // dgvLedgerAccounts
            // 
            this.dgvLedgerAccounts.AllowUserToAddRows = false;
            this.dgvLedgerAccounts.AllowUserToDeleteRows = false;
            this.dgvLedgerAccounts.AllowUserToOrderColumns = true;
            this.dgvLedgerAccounts.AllowUserToResizeColumns = false;
            this.dgvLedgerAccounts.AllowUserToResizeRows = false;
            this.dgvLedgerAccounts.BackgroundColor = System.Drawing.Color.White;
            this.dgvLedgerAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLedgerAccounts.ColumnHeadersVisible = false;
            this.dgvLedgerAccounts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LedgerAccountID,
            this.LedgerCode,
            this.LedgerName});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLedgerAccounts.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgvLedgerAccounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLedgerAccounts.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvLedgerAccounts.Location = new System.Drawing.Point(0, 0);
            this.dgvLedgerAccounts.Name = "dgvLedgerAccounts";
            this.dgvLedgerAccounts.ReadOnly = true;
            this.dgvLedgerAccounts.RowHeadersVisible = false;
            this.dgvLedgerAccounts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLedgerAccounts.Size = new System.Drawing.Size(550, 150);
            this.dgvLedgerAccounts.TabIndex = 1079;
            this.dgvLedgerAccounts.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLedgerAccounts_CellDoubleClick);
            // 
            // LedgerAccountID
            // 
            this.LedgerAccountID.DataPropertyName = "LedgerAccountID";
            this.LedgerAccountID.HeaderText = "LedgerAccountID";
            this.LedgerAccountID.Name = "LedgerAccountID";
            this.LedgerAccountID.ReadOnly = true;
            this.LedgerAccountID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LedgerAccountID.Visible = false;
            // 
            // LedgerCode
            // 
            this.LedgerCode.DataPropertyName = "LedgerCode";
            this.LedgerCode.HeaderText = "Code";
            this.LedgerCode.Name = "LedgerCode";
            this.LedgerCode.ReadOnly = true;
            this.LedgerCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LedgerCode.Width = 150;
            // 
            // LedgerName
            // 
            this.LedgerName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LedgerName.DataPropertyName = "LedgerName";
            this.LedgerName.HeaderText = "Account";
            this.LedgerName.Name = "LedgerName";
            this.LedgerName.ReadOnly = true;
            this.LedgerName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // stCostCenter
            // 
            this.stCostCenter.GlobalItem = false;
            this.stCostCenter.Name = "stCostCenter";
            this.stCostCenter.Text = "CostCenter";
            // 
            // tmGeneralReceiptsAndPayments
            // 
            this.tmGeneralReceiptsAndPayments.Tick += new System.EventHandler(this.tmGeneralReceiptsAndPayments_Tick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "VoucherID";
            this.dataGridViewTextBoxColumn1.HeaderText = "VoucherID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "VoucherNo";
            this.dataGridViewTextBoxColumn2.HeaderText = "VoucherNo";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "VoucherDate";
            this.dataGridViewTextBoxColumn3.HeaderText = "VoucherDate";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // pnlLedgerAccounts
            // 
            this.pnlLedgerAccounts.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlLedgerAccounts.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlLedgerAccounts.Controls.Add(this.dgvLedgerAccounts);
            this.pnlLedgerAccounts.Location = new System.Drawing.Point(0, 0);
            this.pnlLedgerAccounts.Name = "pnlLedgerAccounts";
            this.pnlLedgerAccounts.Size = new System.Drawing.Size(550, 150);
            this.pnlLedgerAccounts.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlLedgerAccounts.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlLedgerAccounts.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlLedgerAccounts.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlLedgerAccounts.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlLedgerAccounts.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlLedgerAccounts.Style.GradientAngle = 90;
            this.pnlLedgerAccounts.TabIndex = 1047;
            this.pnlLedgerAccounts.Text = "panelEx1";
            // 
            // frmGroupJV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1254, 464);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.pnlLedgerAccounts);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmGroupJV";
            this.Text = "Group Journal";
            this.Load += new System.EventHandler(this.frmGroupJV_Load);
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVNoDisplay)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            this.panelLeftTop.ResumeLayout(false);
            this.panelLeftTop.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.panelMiddle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvJournalVoucher)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroupJV)).EndInit();
            this.cmsGroupJV.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddAccounts)).EndInit();
            this.cmsGroupJVAdd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bnGeneralReceiptsAndPayments)).EndInit();
            this.bnGeneralReceiptsAndPayments.ResumeLayout(false);
            this.bnGeneralReceiptsAndPayments.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLedgerAccounts)).EndInit();
            this.pnlLedgerAccounts.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx PanelLeft;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvVNoDisplay;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoucherID;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoucherNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoucherDate;
        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.PanelEx panelLeftTop;
        private System.Windows.Forms.DateTimePicker dtpSTo;
        private System.Windows.Forms.DateTimePicker dtpSFrom;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private DevComponents.DotNetBar.ButtonX BtnSRefresh;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboSearchCompany;
        private System.Windows.Forms.Label LblScompany;
        private DevComponents.DotNetBar.LabelX LblSCountStatus;
        private DevComponents.DotNetBar.PanelEx pnlMain;
        private DevComponents.DotNetBar.PanelEx panelMiddle;
        private ClsInnerGridBar dgvJournalVoucher;
        private DevComponents.DotNetBar.SuperTabItem stCostCenter;
        private DevComponents.DotNetBar.PanelEx pnlBottom;
        private System.Windows.Forms.Label lblSelectedAccountBalance;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.TextBox txtDebitTotal;
        internal System.Windows.Forms.TextBox txtCreditTotal;
        internal System.Windows.Forms.Label lblTotal;
        internal System.Windows.Forms.TextBox txtNarration;
        internal System.Windows.Forms.Label lblNarration;
        private System.Windows.Forms.StatusStrip ssStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tslStatus;
        private DevComponents.DotNetBar.PanelEx panelTop;
        private ClsInnerGridBar dgvAddAccounts;
        private System.Windows.Forms.Label lblCompany;
        private System.Windows.Forms.Label lblCurrency;
        private System.Windows.Forms.Label lblDate;
        internal System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.ComboBox cboCompany;
        internal System.Windows.Forms.BindingNavigator bnGeneralReceiptsAndPayments;
        internal System.Windows.Forms.ToolStripButton btnAddNewItem;
        internal System.Windows.Forms.ToolStripButton btnSaveItem;
        internal System.Windows.Forms.ToolStripButton btnDeleteItem;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton btnChartofAccounts;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        private DevComponents.DotNetBar.ExpandableSplitter expnlLeft;
        private DevComponents.DotNetBar.ButtonX btnAddJV;
        internal System.Windows.Forms.TextBox txtAddTotalDebit;
        internal System.Windows.Forms.TextBox txtAddTotalCredit;
        internal System.Windows.Forms.Label lblAddTotal;
        private System.Windows.Forms.TextBox txtVoucherNo;
        private System.Windows.Forms.Label lblVoucherNo;
        private System.Windows.Forms.Label lblRFQNo;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtSsearch;
        private System.Windows.Forms.Timer tmGeneralReceiptsAndPayments;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.ContextMenuStrip cmsGroupJV;
        private System.Windows.Forms.ToolStripMenuItem btnJVEdit;
        private System.Windows.Forms.ToolStripMenuItem btnJVRemove;
        private System.Windows.Forms.ContextMenuStrip cmsGroupJVAdd;
        private System.Windows.Forms.ToolStripMenuItem btnAddAutofill;
        private System.Windows.Forms.ToolStripMenuItem btnAutofill;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvLedgerAccounts;
        private System.Windows.Forms.DataGridViewTextBoxColumn LedgerAccountID;
        private System.Windows.Forms.DataGridViewTextBoxColumn LedgerCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn LedgerName;
        private DevComponents.DotNetBar.PanelEx pnlLedgerAccounts;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddAccountID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddAccountName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddSerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddRemarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddDebitAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddCreditAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddJVSerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddVoucherID;
        private ClsInnerGridBar dgvGroupJV;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn DebitAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreditAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn JVSerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn EditVoucherID;
        private System.Windows.Forms.DataGridViewTextBoxColumn GJVAccountID;
        private System.Windows.Forms.DataGridViewTextBoxColumn GJVCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn GJVAccountName;
        private System.Windows.Forms.DataGridViewTextBoxColumn GJVSerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn GJVRemarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn GJVDebitAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn GJVCreditAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn GJVJVSerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn GJVVoucherID;
    }
}