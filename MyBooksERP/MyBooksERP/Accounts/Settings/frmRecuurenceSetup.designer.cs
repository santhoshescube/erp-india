﻿namespace MyBooksERP
{
    partial class frmRecuurenceSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRecuurenceSetup));
            this.Label7 = new System.Windows.Forms.Label();
            this.nudMonthIntervalIsOn = new System.Windows.Forms.NumericUpDown();
            this.Label6 = new System.Windows.Forms.Label();
            this.cboDays = new System.Windows.Forms.ComboBox();
            this.cboWeekDayOrder = new System.Windows.Forms.ComboBox();
            this.rdbIsOnMonthly = new System.Windows.Forms.RadioButton();
            this.nudMonthIntervalIsDays = new System.Windows.Forms.NumericUpDown();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.pnlDaily = new System.Windows.Forms.Panel();
            this.rdbIsWeeklyDaily = new System.Windows.Forms.RadioButton();
            this.nudDayInterval = new System.Windows.Forms.NumericUpDown();
            this.rdbIsDaysDaily = new System.Windows.Forms.RadioButton();
            this.Label1 = new System.Windows.Forms.Label();
            this.pnlWeekly = new System.Windows.Forms.Panel();
            this.chkSaturday = new System.Windows.Forms.CheckBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.chkFriday = new System.Windows.Forms.CheckBox();
            this.nudWeekInterval = new System.Windows.Forms.NumericUpDown();
            this.chkWednesday = new System.Windows.Forms.CheckBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.chkThursday = new System.Windows.Forms.CheckBox();
            this.chkSunday = new System.Windows.Forms.CheckBox();
            this.chkTuesDay = new System.Windows.Forms.CheckBox();
            this.chkMonday = new System.Windows.Forms.CheckBox();
            this.pnlMonthly = new System.Windows.Forms.Panel();
            this.rdbIsDaysMOnthly = new System.Windows.Forms.RadioButton();
            this.Label5 = new System.Windows.Forms.Label();
            this.nudDayofMonth = new System.Windows.Forms.NumericUpDown();
            this.Label4 = new System.Windows.Forms.Label();
            this.rdbDaily = new System.Windows.Forms.RadioButton();
            this.rdbWeekly = new System.Windows.Forms.RadioButton();
            this.rdbMonthly = new System.Windows.Forms.RadioButton();
            this.txtRecurranceType = new System.Windows.Forms.TextBox();
            this.lblReccurranceType = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorClearItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPrint = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorHelp = new System.Windows.Forms.ToolStripButton();
            this.SsRecurranceSetup = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.LblSStripRecurranceSetup = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmrRecurranceSetup = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.nudMonthIntervalIsOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMonthIntervalIsDays)).BeginInit();
            this.GroupBox1.SuspendLayout();
            this.pnlDaily.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDayInterval)).BeginInit();
            this.pnlWeekly.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudWeekInterval)).BeginInit();
            this.pnlMonthly.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDayofMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            this.SsRecurranceSetup.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Label7.Location = new System.Drawing.Point(362, 50);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(47, 13);
            this.Label7.TabIndex = 17;
            this.Label7.Text = "month(s)";
            // 
            // nudMonthIntervalIsOn
            // 
            this.nudMonthIntervalIsOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.nudMonthIntervalIsOn.Location = new System.Drawing.Point(293, 48);
            this.nudMonthIntervalIsOn.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.nudMonthIntervalIsOn.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMonthIntervalIsOn.Name = "nudMonthIntervalIsOn";
            this.nudMonthIntervalIsOn.Size = new System.Drawing.Size(61, 20);
            this.nudMonthIntervalIsOn.TabIndex = 16;
            this.nudMonthIntervalIsOn.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Label6.Location = new System.Drawing.Point(240, 50);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(45, 13);
            this.Label6.TabIndex = 15;
            this.Label6.Text = "of every";
            // 
            // cboDays
            // 
            this.cboDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cboDays.FormattingEnabled = true;
            this.cboDays.Location = new System.Drawing.Point(152, 47);
            this.cboDays.Name = "cboDays";
            this.cboDays.Size = new System.Drawing.Size(80, 21);
            this.cboDays.TabIndex = 14;
            // 
            // cboWeekDayOrder
            // 
            this.cboWeekDayOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cboWeekDayOrder.FormattingEnabled = true;
            this.cboWeekDayOrder.Location = new System.Drawing.Point(83, 47);
            this.cboWeekDayOrder.Name = "cboWeekDayOrder";
            this.cboWeekDayOrder.Size = new System.Drawing.Size(61, 21);
            this.cboWeekDayOrder.TabIndex = 13;
            // 
            // rdbIsOnMonthly
            // 
            this.rdbIsOnMonthly.AutoSize = true;
            this.rdbIsOnMonthly.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.rdbIsOnMonthly.Location = new System.Drawing.Point(19, 48);
            this.rdbIsOnMonthly.Name = "rdbIsOnMonthly";
            this.rdbIsOnMonthly.Size = new System.Drawing.Size(39, 17);
            this.rdbIsOnMonthly.TabIndex = 12;
            this.rdbIsOnMonthly.Text = "On";
            this.rdbIsOnMonthly.UseVisualStyleBackColor = true;
            this.rdbIsOnMonthly.CheckedChanged += new System.EventHandler(this.rdbIsOnMonthly_CheckedChanged);
            // 
            // nudMonthIntervalIsDays
            // 
            this.nudMonthIntervalIsDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.nudMonthIntervalIsDays.Location = new System.Drawing.Point(205, 14);
            this.nudMonthIntervalIsDays.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.nudMonthIntervalIsDays.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMonthIntervalIsDays.Name = "nudMonthIntervalIsDays";
            this.nudMonthIntervalIsDays.Size = new System.Drawing.Size(61, 20);
            this.nudMonthIntervalIsDays.TabIndex = 11;
            this.nudMonthIntervalIsDays.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.pnlDaily);
            this.GroupBox1.Controls.Add(this.pnlWeekly);
            this.GroupBox1.Controls.Add(this.pnlMonthly);
            this.GroupBox1.Controls.Add(this.rdbDaily);
            this.GroupBox1.Controls.Add(this.rdbWeekly);
            this.GroupBox1.Controls.Add(this.rdbMonthly);
            this.GroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox1.Location = new System.Drawing.Point(3, 67);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(433, 132);
            this.GroupBox1.TabIndex = 4;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Recurring Pattern";
            // 
            // pnlDaily
            // 
            this.pnlDaily.Controls.Add(this.rdbIsWeeklyDaily);
            this.pnlDaily.Controls.Add(this.nudDayInterval);
            this.pnlDaily.Controls.Add(this.rdbIsDaysDaily);
            this.pnlDaily.Controls.Add(this.Label1);
            this.pnlDaily.Location = new System.Drawing.Point(4, 42);
            this.pnlDaily.Name = "pnlDaily";
            this.pnlDaily.Size = new System.Drawing.Size(414, 81);
            this.pnlDaily.TabIndex = 5;
            // 
            // rdbIsWeeklyDaily
            // 
            this.rdbIsWeeklyDaily.AutoSize = true;
            this.rdbIsWeeklyDaily.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.rdbIsWeeklyDaily.Location = new System.Drawing.Point(160, 36);
            this.rdbIsWeeklyDaily.Name = "rdbIsWeeklyDaily";
            this.rdbIsWeeklyDaily.Size = new System.Drawing.Size(73, 17);
            this.rdbIsWeeklyDaily.TabIndex = 3;
            this.rdbIsWeeklyDaily.Text = "Workdays";
            this.rdbIsWeeklyDaily.UseVisualStyleBackColor = true;
            // 
            // nudDayInterval
            // 
            this.nudDayInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.nudDayInterval.Location = new System.Drawing.Point(83, 12);
            this.nudDayInterval.Maximum = new decimal(new int[] {
            365,
            0,
            0,
            0});
            this.nudDayInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDayInterval.Name = "nudDayInterval";
            this.nudDayInterval.Size = new System.Drawing.Size(61, 20);
            this.nudDayInterval.TabIndex = 1;
            this.nudDayInterval.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // rdbIsDaysDaily
            // 
            this.rdbIsDaysDaily.AutoSize = true;
            this.rdbIsDaysDaily.Checked = true;
            this.rdbIsDaysDaily.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.rdbIsDaysDaily.Location = new System.Drawing.Point(160, 13);
            this.rdbIsDaysDaily.Name = "rdbIsDaysDaily";
            this.rdbIsDaysDaily.Size = new System.Drawing.Size(49, 17);
            this.rdbIsDaysDaily.TabIndex = 2;
            this.rdbIsDaysDaily.TabStop = true;
            this.rdbIsDaysDaily.Text = "Days";
            this.rdbIsDaysDaily.UseVisualStyleBackColor = true;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Label1.Location = new System.Drawing.Point(16, 14);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(66, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Recur Every";
            // 
            // pnlWeekly
            // 
            this.pnlWeekly.Controls.Add(this.chkSaturday);
            this.pnlWeekly.Controls.Add(this.Label2);
            this.pnlWeekly.Controls.Add(this.chkFriday);
            this.pnlWeekly.Controls.Add(this.nudWeekInterval);
            this.pnlWeekly.Controls.Add(this.chkWednesday);
            this.pnlWeekly.Controls.Add(this.Label3);
            this.pnlWeekly.Controls.Add(this.chkThursday);
            this.pnlWeekly.Controls.Add(this.chkSunday);
            this.pnlWeekly.Controls.Add(this.chkTuesDay);
            this.pnlWeekly.Controls.Add(this.chkMonday);
            this.pnlWeekly.Location = new System.Drawing.Point(7, 42);
            this.pnlWeekly.Name = "pnlWeekly";
            this.pnlWeekly.Size = new System.Drawing.Size(414, 81);
            this.pnlWeekly.TabIndex = 4;
            // 
            // chkSaturday
            // 
            this.chkSaturday.AutoSize = true;
            this.chkSaturday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkSaturday.Location = new System.Drawing.Point(224, 62);
            this.chkSaturday.Name = "chkSaturday";
            this.chkSaturday.Size = new System.Drawing.Size(68, 17);
            this.chkSaturday.TabIndex = 9;
            this.chkSaturday.Text = "Saturday";
            this.chkSaturday.UseVisualStyleBackColor = true;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Label2.Location = new System.Drawing.Point(16, 14);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(66, 13);
            this.Label2.TabIndex = 0;
            this.Label2.Text = "Recur Every";
            // 
            // chkFriday
            // 
            this.chkFriday.AutoSize = true;
            this.chkFriday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkFriday.Location = new System.Drawing.Point(155, 62);
            this.chkFriday.Name = "chkFriday";
            this.chkFriday.Size = new System.Drawing.Size(54, 17);
            this.chkFriday.TabIndex = 8;
            this.chkFriday.Text = "Friday";
            this.chkFriday.UseVisualStyleBackColor = true;
            // 
            // nudWeekInterval
            // 
            this.nudWeekInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.nudWeekInterval.Location = new System.Drawing.Point(83, 12);
            this.nudWeekInterval.Maximum = new decimal(new int[] {
            52,
            0,
            0,
            0});
            this.nudWeekInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudWeekInterval.Name = "nudWeekInterval";
            this.nudWeekInterval.Size = new System.Drawing.Size(61, 20);
            this.nudWeekInterval.TabIndex = 1;
            this.nudWeekInterval.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chkWednesday
            // 
            this.chkWednesday.AutoSize = true;
            this.chkWednesday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkWednesday.Location = new System.Drawing.Point(296, 39);
            this.chkWednesday.Name = "chkWednesday";
            this.chkWednesday.Size = new System.Drawing.Size(83, 17);
            this.chkWednesday.TabIndex = 7;
            this.chkWednesday.Text = "Wednesday";
            this.chkWednesday.UseVisualStyleBackColor = true;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Label3.Location = new System.Drawing.Point(157, 14);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(62, 13);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "week(s) on:";
            // 
            // chkThursday
            // 
            this.chkThursday.AutoSize = true;
            this.chkThursday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkThursday.Location = new System.Drawing.Point(83, 62);
            this.chkThursday.Name = "chkThursday";
            this.chkThursday.Size = new System.Drawing.Size(70, 17);
            this.chkThursday.TabIndex = 6;
            this.chkThursday.Text = "Thursday";
            this.chkThursday.UseVisualStyleBackColor = true;
            // 
            // chkSunday
            // 
            this.chkSunday.AutoSize = true;
            this.chkSunday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkSunday.Location = new System.Drawing.Point(83, 39);
            this.chkSunday.Name = "chkSunday";
            this.chkSunday.Size = new System.Drawing.Size(62, 17);
            this.chkSunday.TabIndex = 3;
            this.chkSunday.Text = "Sunday";
            this.chkSunday.UseVisualStyleBackColor = true;
            // 
            // chkTuesDay
            // 
            this.chkTuesDay.AutoSize = true;
            this.chkTuesDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkTuesDay.Location = new System.Drawing.Point(224, 39);
            this.chkTuesDay.Name = "chkTuesDay";
            this.chkTuesDay.Size = new System.Drawing.Size(67, 17);
            this.chkTuesDay.TabIndex = 5;
            this.chkTuesDay.Text = "Tuesday";
            this.chkTuesDay.UseVisualStyleBackColor = true;
            // 
            // chkMonday
            // 
            this.chkMonday.AutoSize = true;
            this.chkMonday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkMonday.Location = new System.Drawing.Point(155, 39);
            this.chkMonday.Name = "chkMonday";
            this.chkMonday.Size = new System.Drawing.Size(64, 17);
            this.chkMonday.TabIndex = 4;
            this.chkMonday.Text = "Monday";
            this.chkMonday.UseVisualStyleBackColor = true;
            // 
            // pnlMonthly
            // 
            this.pnlMonthly.Controls.Add(this.Label7);
            this.pnlMonthly.Controls.Add(this.rdbIsDaysMOnthly);
            this.pnlMonthly.Controls.Add(this.Label6);
            this.pnlMonthly.Controls.Add(this.Label5);
            this.pnlMonthly.Controls.Add(this.cboDays);
            this.pnlMonthly.Controls.Add(this.nudDayofMonth);
            this.pnlMonthly.Controls.Add(this.nudMonthIntervalIsOn);
            this.pnlMonthly.Controls.Add(this.Label4);
            this.pnlMonthly.Controls.Add(this.cboWeekDayOrder);
            this.pnlMonthly.Controls.Add(this.nudMonthIntervalIsDays);
            this.pnlMonthly.Controls.Add(this.rdbIsOnMonthly);
            this.pnlMonthly.Location = new System.Drawing.Point(10, 42);
            this.pnlMonthly.Name = "pnlMonthly";
            this.pnlMonthly.Size = new System.Drawing.Size(414, 81);
            this.pnlMonthly.TabIndex = 3;
            // 
            // rdbIsDaysMOnthly
            // 
            this.rdbIsDaysMOnthly.AutoSize = true;
            this.rdbIsDaysMOnthly.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.rdbIsDaysMOnthly.Location = new System.Drawing.Point(19, 14);
            this.rdbIsDaysMOnthly.Name = "rdbIsDaysMOnthly";
            this.rdbIsDaysMOnthly.Size = new System.Drawing.Size(44, 17);
            this.rdbIsDaysMOnthly.TabIndex = 10;
            this.rdbIsDaysMOnthly.Text = "Day";
            this.rdbIsDaysMOnthly.UseVisualStyleBackColor = true;
            this.rdbIsDaysMOnthly.CheckedChanged += new System.EventHandler(this.rdbIsDaysMOnthly_CheckedChanged);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Label5.Location = new System.Drawing.Point(274, 16);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(47, 13);
            this.Label5.TabIndex = 0;
            this.Label5.Text = "month(s)";
            // 
            // nudDayofMonth
            // 
            this.nudDayofMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.nudDayofMonth.Location = new System.Drawing.Point(83, 14);
            this.nudDayofMonth.Maximum = new decimal(new int[] {
            28,
            0,
            0,
            0});
            this.nudDayofMonth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDayofMonth.Name = "nudDayofMonth";
            this.nudDayofMonth.Size = new System.Drawing.Size(61, 20);
            this.nudDayofMonth.TabIndex = 1;
            this.nudDayofMonth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Label4.Location = new System.Drawing.Point(152, 16);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(45, 13);
            this.Label4.TabIndex = 2;
            this.Label4.Text = "of every";
            // 
            // rdbDaily
            // 
            this.rdbDaily.AutoSize = true;
            this.rdbDaily.Checked = true;
            this.rdbDaily.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbDaily.Location = new System.Drawing.Point(29, 19);
            this.rdbDaily.Name = "rdbDaily";
            this.rdbDaily.Size = new System.Drawing.Size(48, 17);
            this.rdbDaily.TabIndex = 0;
            this.rdbDaily.TabStop = true;
            this.rdbDaily.Text = "&Daily";
            this.rdbDaily.UseVisualStyleBackColor = true;
            this.rdbDaily.CheckedChanged += new System.EventHandler(this.rdbDaily_CheckedChanged);
            // 
            // rdbWeekly
            // 
            this.rdbWeekly.AutoSize = true;
            this.rdbWeekly.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbWeekly.Location = new System.Drawing.Point(93, 19);
            this.rdbWeekly.Name = "rdbWeekly";
            this.rdbWeekly.Size = new System.Drawing.Size(61, 17);
            this.rdbWeekly.TabIndex = 1;
            this.rdbWeekly.Text = "&Weekly";
            this.rdbWeekly.UseVisualStyleBackColor = true;
            this.rdbWeekly.CheckedChanged += new System.EventHandler(this.rdbWeekly_CheckedChanged);
            // 
            // rdbMonthly
            // 
            this.rdbMonthly.AutoSize = true;
            this.rdbMonthly.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbMonthly.Location = new System.Drawing.Point(170, 19);
            this.rdbMonthly.Name = "rdbMonthly";
            this.rdbMonthly.Size = new System.Drawing.Size(62, 17);
            this.rdbMonthly.TabIndex = 2;
            this.rdbMonthly.Text = "&Monthly";
            this.rdbMonthly.UseVisualStyleBackColor = true;
            this.rdbMonthly.CheckedChanged += new System.EventHandler(this.rdbMonthly_CheckedChanged);
            // 
            // txtRecurranceType
            // 
            this.txtRecurranceType.BackColor = System.Drawing.SystemColors.Info;
            this.txtRecurranceType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRecurranceType.Location = new System.Drawing.Point(106, 34);
            this.txtRecurranceType.Name = "txtRecurranceType";
            this.txtRecurranceType.Size = new System.Drawing.Size(321, 20);
            this.txtRecurranceType.TabIndex = 20;
            this.txtRecurranceType.TextChanged += new System.EventHandler(this.txtRecurranceType_TextChanged);
            // 
            // lblReccurranceType
            // 
            this.lblReccurranceType.AutoSize = true;
            this.lblReccurranceType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReccurranceType.Location = new System.Drawing.Point(10, 37);
            this.lblReccurranceType.Name = "lblReccurranceType";
            this.lblReccurranceType.Size = new System.Drawing.Size(90, 13);
            this.lblReccurranceType.TabIndex = 19;
            this.lblReccurranceType.Text = "Recurrence Type";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(12, 205);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(352, 205);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "&Cancel";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(271, 205);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 21;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bindingNavigator1.BackColor = System.Drawing.Color.Transparent;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorSaveItem,
            this.bindingNavigatorDeleteItem,
            this.bindingNavigatorClearItem,
            this.toolStripSeparator1,
            this.bindingNavigatorPrint,
            this.bindingNavigatorEmail,
            this.toolStripSeparator2,
            this.bindingNavigatorHelp});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.Size = new System.Drawing.Size(438, 25);
            this.bindingNavigator1.TabIndex = 22;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            this.bindingNavigatorDeleteItem.Click += new System.EventHandler(this.bindingNavigatorDeleteItem_Click);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            this.bindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.bindingNavigatorMoveFirstItem_Click);
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            this.bindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.bindingNavigatorMovePreviousItem_Click);
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            this.bindingNavigatorMoveNextItem.Click += new System.EventHandler(this.bindingNavigatorMoveNextItem_Click);
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            this.bindingNavigatorMoveLastItem.Click += new System.EventHandler(this.bindingNavigatorMoveLastItem_Click);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorSaveItem
            // 
            this.bindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorSaveItem.Image = global::MyBooksERP.Properties.Resources.save;
            this.bindingNavigatorSaveItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorSaveItem.Name = "bindingNavigatorSaveItem";
            this.bindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorSaveItem.Text = "Save";
            this.bindingNavigatorSaveItem.Click += new System.EventHandler(this.bindingNavigatorSaveItem_Click);
            // 
            // bindingNavigatorClearItem
            // 
            this.bindingNavigatorClearItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorClearItem.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.bindingNavigatorClearItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorClearItem.Name = "bindingNavigatorClearItem";
            this.bindingNavigatorClearItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorClearItem.Text = "Clear";
            this.bindingNavigatorClearItem.Click += new System.EventHandler(this.bindingNavigatorClearItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPrint
            // 
            this.bindingNavigatorPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.bindingNavigatorPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorPrint.Name = "bindingNavigatorPrint";
            this.bindingNavigatorPrint.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorPrint.Text = "Print";
            this.bindingNavigatorPrint.Click += new System.EventHandler(this.bindingNavigatorPrint_Click);
            // 
            // bindingNavigatorEmail
            // 
            this.bindingNavigatorEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.bindingNavigatorEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorEmail.Name = "bindingNavigatorEmail";
            this.bindingNavigatorEmail.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorEmail.Text = "Email";
            this.bindingNavigatorEmail.Click += new System.EventHandler(this.bindingNavigatorEmail_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorHelp
            // 
            this.bindingNavigatorHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorHelp.Image = global::MyBooksERP.Properties.Resources.help;
            this.bindingNavigatorHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorHelp.Name = "bindingNavigatorHelp";
            this.bindingNavigatorHelp.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorHelp.Text = "Help";
            // 
            // SsRecurranceSetup
            // 
            this.SsRecurranceSetup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.LblSStripRecurranceSetup});
            this.SsRecurranceSetup.Location = new System.Drawing.Point(0, 231);
            this.SsRecurranceSetup.Name = "SsRecurranceSetup";
            this.SsRecurranceSetup.Size = new System.Drawing.Size(438, 22);
            this.SsRecurranceSetup.TabIndex = 23;
            this.SsRecurranceSetup.Text = "StatusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(45, 17);
            this.toolStripStatusLabel1.Text = "Status: ";
            // 
            // LblSStripRecurranceSetup
            // 
            this.LblSStripRecurranceSetup.Name = "LblSStripRecurranceSetup";
            this.LblSStripRecurranceSetup.Size = new System.Drawing.Size(0, 17);
            // 
            // tmrRecurranceSetup
            // 
            this.tmrRecurranceSetup.Interval = 3000;
            this.tmrRecurranceSetup.Tick += new System.EventHandler(this.tmrRecurranceSetup_Tick);
            // 
            // frmRecuurenceSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 253);
            this.Controls.Add(this.SsRecurranceSetup);
            this.Controls.Add(this.bindingNavigator1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.txtRecurranceType);
            this.Controls.Add(this.lblReccurranceType);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRecuurenceSetup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Recurrence Setup";
            this.Load += new System.EventHandler(this.frmRecuurenceSetup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudMonthIntervalIsOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMonthIntervalIsDays)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.pnlDaily.ResumeLayout(false);
            this.pnlDaily.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDayInterval)).EndInit();
            this.pnlWeekly.ResumeLayout(false);
            this.pnlWeekly.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudWeekInterval)).EndInit();
            this.pnlMonthly.ResumeLayout(false);
            this.pnlMonthly.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDayofMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            this.SsRecurranceSetup.ResumeLayout(false);
            this.SsRecurranceSetup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.NumericUpDown nudMonthIntervalIsOn;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.ComboBox cboDays;
        internal System.Windows.Forms.ComboBox cboWeekDayOrder;
        internal System.Windows.Forms.RadioButton rdbIsOnMonthly;
        internal System.Windows.Forms.NumericUpDown nudMonthIntervalIsDays;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.RadioButton rdbIsDaysMOnthly;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.NumericUpDown nudDayofMonth;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.CheckBox chkSaturday;
        internal System.Windows.Forms.CheckBox chkFriday;
        internal System.Windows.Forms.CheckBox chkWednesday;
        internal System.Windows.Forms.CheckBox chkThursday;
        internal System.Windows.Forms.CheckBox chkTuesDay;
        internal System.Windows.Forms.CheckBox chkMonday;
        internal System.Windows.Forms.CheckBox chkSunday;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.NumericUpDown nudWeekInterval;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.RadioButton rdbIsWeeklyDaily;
        internal System.Windows.Forms.RadioButton rdbIsDaysDaily;
        internal System.Windows.Forms.NumericUpDown nudDayInterval;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.RadioButton rdbMonthly;
        internal System.Windows.Forms.RadioButton rdbWeekly;
        internal System.Windows.Forms.RadioButton rdbDaily;
        internal System.Windows.Forms.TextBox txtRecurranceType;
        internal System.Windows.Forms.Label lblReccurranceType;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnClose;
        internal System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorSaveItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorPrint;
        private System.Windows.Forms.ToolStripButton bindingNavigatorEmail;
        private System.Windows.Forms.ToolStripButton bindingNavigatorClearItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorHelp;
        private System.Windows.Forms.Panel pnlDaily;
        private System.Windows.Forms.Panel pnlWeekly;
        private System.Windows.Forms.Panel pnlMonthly;
        internal System.Windows.Forms.StatusStrip SsRecurranceSetup;
        internal System.Windows.Forms.ToolStripStatusLabel LblSStripRecurranceSetup;
        private System.Windows.Forms.Timer tmrRecurranceSetup;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;

    }
}