﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Windows.Forms;



namespace MyBooksERP
{
    /// <summary>
    /// Created On      : 15.09.2010
    /// Created By      : Devi
    /// Description     : Form for Chart of Accounts
    /// 
    /// Modified By     : Junaid
    /// Modified Date   : 4 APRL 2012
    /// </summary>
    public partial class FrmChartOfAccounts : DevComponents.DotNetBar.Office2007Form
    {
        #region Private variables
        /// <summary>
        /// 
        /// </summary>
        private bool MbAddStatus; //Add/Update mode 
        private bool MbChangeStatus;    // Change status

        private Int64 MiAccountID;      // Accounts identification
        private int MiTimerInterval=1000;    // Time interval of the timer
        private int MiCompanyId;        // Company identification
        private int MiUserId;           // User identification

        private string MsMessageCommon; // Common messages to display
        private string MsMessageCaption;    //Message caption
        char BSOrPL;
        // Permissin
        private ArrayList MaMessageArray;   // Error Message display
        private ArrayList MaStatusMessage;
        private bool MblnPrintEmailPermission = false;//To Set Print/Email Permission
        private bool MblnAddPermission = false;///To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnAddUpdatePermission = false;
        //
        //private bool mbPrintPermission = false;
        public bool MbCalledFromAccountSettings = false;
        public eMode Mode = eMode.Insert;
        private string strAcCaption = "Account Properties of ";
        private string StrGrpCaption = "Group Properties of ";

        private bool blnGrp = false;
        clsBLLChartOfAccounts MObjclsBLLChartOfAccounts; // Object of the ChartOfAccounts class
        ClsLogWriter MObjLogs;      // Object of the LogWriter class
        ClsNotification MObjNotification; // Object of the Notification class
        public Accounts SelectedAccount { get; set; }//For Selecting single Account Node
        public AccountGroups PermittedAccountGroup { get; set; }//For Selecting single Group Node
        private clsMessage objUserMessage = null;
        public int intGroupID;

        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.ChartOfAccounts );

                return this.objUserMessage;
            }
        }

        #endregion // Private variables

        public FrmChartOfAccounts()
        {
            InitializeComponent();

            // account group has to be set from the calling form

            this.PermittedAccountGroup = AccountGroups.NotSet;
            this.SelectedAccount = Accounts.NotSelect;

            //  Setting values to the variables
            MiCompanyId = ClsCommonSettings.CompanyID; // Current Companyid
            MiUserId = ClsCommonSettings.UserID; // Current Userid
            MsMessageCaption = ClsCommonSettings.MessageCaption; // Message caption
            MObjclsBLLChartOfAccounts = new clsBLLChartOfAccounts(); // New object for Accounts
            MObjLogs = new ClsLogWriter(Application.StartupPath);   // Application startup path
            MObjNotification = new ClsNotification();
            TmAccounts.Interval = ClsCommonSettings.TimerInterval;
            TmAccounts.Enabled = false;
        }

        private void FrmChartOfAccounts_Load(object sender, EventArgs e)
        {
            try
            {
                this.SetPermissions();
                FillAccountsTree(); // Filling treeview
                LoadAccountGroup();
                AddNewItem();   // Set the for for adding new item 

                if (intGroupID > 0)
                {
                    MnuAddAccount_Click(sender, e);
                    cboGroup.SelectedValue = intGroupID;
                    rbtnCreateLedger.Checked = true;
                    rbtnCreateGroup.Checked = false;
                    intGroupID = 0;
                }                
            }
            catch (Exception Ex) 
            {
                //throw Ex; 
            }
        }

        private void LoadAccountGroup()
        {
            ClsCommonUtility objCommonUtility = new ClsCommonUtility();
            DataTable datTemp = objCommonUtility.FillCombos(new string[] { "AccountGroupID,AccountGroupName", "AccAccountGroup", "" +
                "(CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL)" });
            cboGroup.ValueMember = "AccountGroupID";
            cboGroup.DisplayMember = "AccountGroupName";
            cboGroup.DataSource = datTemp;
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {

                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Accounts, (Int32)eMenuID.AccountSettings, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                if (MblnAddPermission == true || MblnUpdatePermission == true)
                    MblnAddUpdatePermission = true;
                else
                    MblnAddUpdatePermission = false;
            }
            else
                MblnAddPermission = MblnAddUpdatePermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            BtnNew.Enabled = MblnAddPermission;
            BtnDelete.Enabled = MblnDeletePermission;
            BtnBottomSave.Enabled = BtnOk.Enabled = BtnSave.Enabled = MblnAddUpdatePermission;

        }

        /// <summary>
        /// Setting the form for adding new Cheque receipt
        /// </summary>
        /// <returns></returns>
        private bool AddNewItem()
        {
            SetEnable();    // Clear all fields in the form
            MbAddStatus = MblnAddPermission;
            MiAccountID = 0;
            Mode = eMode.Insert;// Initializing the AccountID            
            return true;
        }

        /// <summary>
        /// Clearing fields in the form
        /// </summary>
        private void SetEnable()
        {
            // Clearing controls
            TxtDescription.Clear();
            txtTaxValue.Clear();
            TxtShortName.Clear();
            this.TxtDescription.Enabled = this.TxtShortName.Enabled = true;
            ErrAccounts.Clear();
            MnuAddNew.Visible = false;
            SetEnableDisable(true);
            TxtDescription.Focus(); // Set default focus 
             
        }
        private void SetEnableCheckBox(bool blnEnable)
        {
            blnGrp = blnEnable;
            chkCostCenter.Visible = true ;
            chkCostCenter.BringToFront();
            if (blnGrp)
            {
                chkCostCenter.Checked = false;
                chkCostCenter.Visible = false;
            }
        }
        /// <summary>
        /// Enable/disable buttons according to the action
        /// </summary>
        private void SetEnableDisable(bool bEnable)
        {
            BtnNew.Enabled = MblnAddPermission;
            BtnDelete.Enabled = MblnDeletePermission;

            if (bEnable)
            {
                BtnBottomSave.Enabled = !bEnable;
                BtnOk.Enabled = !bEnable;
                BtnSave.Enabled = !bEnable;
                MbChangeStatus = !bEnable;
            }
            else
            {
                BtnBottomSave.Enabled = bEnable;
                BtnOk.Enabled = bEnable;
                BtnSave.Enabled = bEnable;
                MbChangeStatus = bEnable;
            }
            //BtnDelete.Enabled = !this.MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.Predefined;
            //BtnNew.Enabled = blnGrp;

            
        }

        private void ClearErrorProvider(object sender, EventArgs e)
        {
            ErrAccounts.Clear();    // Clearing error provider control
            EnableButtons(sender, e);
        }
        /// <summary>
        /// Enables the buttons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnableButtons(object sender, EventArgs e)
       {
           BtnSave.Enabled = BtnOk.Enabled = BtnBottomSave.Enabled = (this.MblnAddPermission || this.MbAddStatus) || (this.MblnUpdatePermission && !this.MbAddStatus);

            MbChangeStatus = true;
           
        }
        /// <summary>
        /// Filling treeview with groups and accounts
        /// </summary>
        private void FillAccountsTree()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                TvAccountsHead.Nodes.Clear();   // First clear the treeview

                // Looping through with the account heads
                foreach (DataRow dRow in MObjclsBLLChartOfAccounts.FillAccountsHead().Rows)
                {
                    // New node for Account head
                    TreeNode TnAccount = new TreeNode();
                    TnAccount.Text = dRow["AccountGroupName"].ToString();
                    TnAccount.Tag = dRow["AccountGroupID"];
                    TnAccount.ImageIndex = 0;
                    TnAccount.SelectedImageIndex = 0;
                    TvAccountsHead.Nodes.Add(TnAccount);    // Add account head into the treeview
                    FillAccounts(TnAccount);
                    FillChildGroups(TnAccount);
                }
                TvAccountsHead.ExpandAll(); // Callapsing all treeview nodes
                TvAccountsHead.SelectedNode = TvAccountsHead.Nodes[0];  // Set focus to the first node
            }
            catch (Exception ex)
            {
                this.MObjLogs.WriteLog(string.Format("Error occured in SetNodeSelection() Form Name :{0}, Exception :{1}", this.Name, ex.ToString()), 3);
            }
            finally 
            { 
                Cursor.Current = Cursors.Default;
                this.SetNodeSelection();// Set focus to group node or account node 
               
                
            }
        }

        private void FillChildGroups(TreeNode TnParent)
        {
            DataTable datChildGroups = null;

            try
            {
                datChildGroups = this.MObjclsBLLChartOfAccounts.GetChildAccountHead(Convert.ToInt32(TnParent.Tag));
                if (datChildGroups == null || datChildGroups.Rows.Count == 0) return;

                foreach (DataRow dtRow in datChildGroups.Rows)
                {
                    TreeNode TnAccount = new TreeNode();
                    TnAccount.Text = dtRow[1].ToString();
                    TnAccount.Tag = dtRow[0];
                    TnAccount.ImageIndex = 0;
                    TnAccount.SelectedImageIndex = 0;
                    TnParent.Nodes.Add(TnAccount);    // Add account head into the treeview
                    FillAccounts(TnAccount);
                    FillChildGroups(TnAccount);
                }
            }
            catch (Exception ex) { throw ex; }
        }

        private void FillAccounts(TreeNode TnParentGroup)
        {
            DataTable datChildGroups = null;

            try
            {
                datChildGroups = this.MObjclsBLLChartOfAccounts.GetChildAccounts(Convert.ToInt32(TnParentGroup.Tag), ClsCommonSettings.CompanyID);

                if (datChildGroups == null || datChildGroups.Rows.Count == 0) return;

                foreach (DataRow dtRow in datChildGroups.Rows)
                {
                    TreeNode TnAccount = new TreeNode();
                    TnAccount.Text = dtRow["AccountName"].ToString();
                    TnAccount.Tag = "@" + dtRow["AccountID"].ToString();
                    TnAccount.ImageIndex = 1;
                    TnAccount.SelectedImageIndex = 1;
                    TnParentGroup.Nodes.Add(TnAccount);    // Add account head into the treeview
                }

            }
            catch (Exception ex) { throw ex; }
        }


        /// <summary>
        /// Finding root node of the given node
        /// </summary>
        /// <param name="TnChild"></param>
        /// <returns></returns>
        private char FindRootNode(TreeNode TnChild)
        {
            if (TnChild.Text.Trim() == "Assets" || TnChild.Text.Trim() == "Liabilities")
            {
                BSOrPL = Convert.ToChar("B");
            }
            else if (TnChild.Text.Trim() == "Income" || TnChild.Text.Trim() == "Expense" || TnChild.Text.Trim() == "Trading Account")
            {
                BSOrPL = Convert.ToChar("P");
            }
            else if (BSOrPL.ToString() != "B" || BSOrPL.ToString() != "P")
            {
                FindRootNode(TnChild.Parent);
            }
            return BSOrPL;
        }

        private bool ValidateFields()
        {
            bool bReturnValue = true;
            try
            {
                if (MObjclsBLLChartOfAccounts.CheckIsPredefined(MiAccountID, blnGrp))
                {
                    this.UserMessage.ShowMessage(Convert.ToInt32(blnGrp == true ? 2378 : 2377));
                    bReturnValue = false;
                    return false;
                }
                if (TxtShortName.Text.Trim().Length == 0) // Code validation
                {
                    this.UserMessage.ShowMessage(2369, TxtShortName);
                    lblStatusMsg.Text = this.UserMessage.GetMessageByCode(2352);
                    TmAccounts.Start();
                    bReturnValue = false;

                    return false;
                }
                if (cboGroup.SelectedValue.ToInt32()==33 && txtTaxValue.Text.Trim().Length == 0) // Duties & Tax Validation validation
                {
                    this.UserMessage.ShowMessage(2352, txtTaxValue);
                    lblStatusMsg.Text = this.UserMessage.GetMessageByCode(2360);
                    TmAccounts.Start();
                    bReturnValue = false;

                    return false;
                }
                if (TxtDescription.Text.Trim().Length == 0) // Description validation
                {
                    this.UserMessage.ShowMessage(2352,TxtDescription );
                    lblStatusMsg.Text = this.UserMessage.GetMessageByCode(2352);
                    TmAccounts.Start();
                    bReturnValue = false;
                 
                    return false;
                }
                else if (MObjclsBLLChartOfAccounts.CheckDuplication((MiAccountID > 0 ? false : true),
                    new string[] { TxtDescription.Text.Trim().Replace("'", "`").Trim() }, MiAccountID, ClsCommonSettings.CompanyID, blnGrp ))    // Validating duplicate account head/account
                {
                    this.UserMessage.ShowMessage (2367, TxtDescription);
                    lblStatusMsg.Text = this.UserMessage.GetMessageByCode(2367);
                    TmAccounts.Start();
                    bReturnValue = false;
                    return false;
                }

                if (rbtnCreateLedger.Checked == true && TxtShortName.Enabled == true) // Only checking in Account and also is not autogenarated
                {
                    if (TxtShortName.Text.Trim().Length == 0)
                    {
                        this.UserMessage.ShowMessage(27, TxtShortName);
                        lblStatusMsg.Text = this.UserMessage.GetMessageByCode(27);
                        TmAccounts.Start();
                        bReturnValue = false;
                        return false;
                    }
                    else if (MObjclsBLLChartOfAccounts.CheckAccountCodeExists((TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") == -1 ?
                            TvAccountsHead.SelectedNode.Tag.ToInt32() :
                            (TvAccountsHead.SelectedNode.Tag.ToString().Remove(0, 1)).ToInt32()), TxtShortName.Text.Trim()))
                    {
                        this.UserMessage.ShowMessage(2375, TxtShortName);
                        lblStatusMsg.Text = this.UserMessage.GetMessageByCode(2375);
                        TmAccounts.Start();
                        bReturnValue = false;
                        return false;
                    }
                }

                if (!MbAddStatus)
                {
                    long lngAcID = (TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") == -1 ?
                        Convert.ToInt32(TvAccountsHead.SelectedNode.Tag) :
                        Convert.ToInt32(TvAccountsHead.SelectedNode.Tag.ToString().Remove(0, 1)));
                     
                    if (cboGroup.SelectedValue.ToInt64() != GetAccountGroupID(lngAcID, rbtnCreateGroup.Checked))
                    {
                        if (MObjclsBLLChartOfAccounts.CheckExistReferences(rbtnCreateGroup.Checked, lngAcID.ToInt32(), false))
                        {
                            this.UserMessage.ShowMessage(Convert.ToInt32(rbtnCreateGroup.Checked == true ? 2378 : 2377));
                            bReturnValue = false;
                            return false;
                        }
                        else
                        {
                            if (MObjclsBLLChartOfAccounts.CheckExistReferences(rbtnCreateGroup.Checked, lngAcID.ToInt32(), true))
                            {
                                this.UserMessage.ShowMessage(Convert.ToInt32(rbtnCreateGroup.Checked == true ? 2378 : 2377));
                                bReturnValue = false;
                                return false;
                            }
                        }
                    }                    
                }

                return bReturnValue;
            }
            catch (Exception Ex) { throw Ex; }
        }

        private bool SaveAccounts()
        {
            try
            {
                if (!ValidateFields())
                    return false;

                bool saveConfirmation= false ;

                if (MbAddStatus)
                {
                    saveConfirmation = this.UserMessage.ShowMessage(1);
                        
                }
                else
                  saveConfirmation =  this.UserMessage.ShowMessage(3);

                if (!saveConfirmation )
                    return false;
                else  
                {
                    if (!MbAddStatus)
                    {
                        MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.ParentID = cboGroup.SelectedValue.ToInt32();
                        MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.GLAccountID = (TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") == -1 ?
                        Convert.ToInt32(TvAccountsHead.SelectedNode.Tag) :
                        Convert.ToInt32(TvAccountsHead.SelectedNode.Tag.ToString().Remove(0, 1)));                        
                    }
                    else
                        MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.GLAccountID = cboGroup.SelectedValue.ToInt32();

                    GetGroupNodeSelection(cboGroup.SelectedValue.ToInt32());
                    MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.BSOrPL = FindRootNode(TvAccountsHead.SelectedNode);
                    MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.Code = TxtShortName.Text.Trim().Replace("'", "`");
                    MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.Description = TxtDescription.Text.Trim().Replace("'", "`");
                    MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.IsCostCenterApplicable = chkCostCenter.Checked;
                    MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.AccValue =txtTaxValue.Text.ToDecimal();
                }

                blnGrp = rbtnCreateGroup.Checked;

                if (MObjclsBLLChartOfAccounts.SaveAccount(blnGrp , MbAddStatus))
                {

                    if (MbAddStatus)
                        this.UserMessage.ShowMessage(2);
                    else
                        this.UserMessage.ShowMessage(21);

                    Mode = eMode.Unknown;
                    FillAccountsTree();
                    LoadAccountGroup();
                    MbAddStatus = false;
                    return true;
                }
                return false;
            }
            catch (Exception Ex) { throw Ex; }
        }

        private void GetGroupNodeSelection(int intGrID)
        {
            try
            {
                if (intGrID != (int)AccountGroups.NotSet)
                {
                    int GroupID = intGrID;
                    TreeNode NodeToBeSelected = null;
                    if (this.TvAccountsHead.Nodes.Count <= 0)
                        return;
                    foreach (TreeNode node in this.TvAccountsHead.Nodes)
                    {
                        TreeNode childNode = node;
                        NodeToBeSelected = this.FindGroupNode(childNode, GroupID);

                        if (NodeToBeSelected != null)
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                this.MObjLogs.WriteLog(string.Format("Error occured in SetNodeSelection() Form Name :{0}, Exception :{1}", this.Name, ex.ToString()), 3);
            }

        }

        private TreeNode FindGroupNode(TreeNode node, int GroupID)
        {
            TreeNode NodeToBeSelected = null;

            if (node.Nodes.Count > 0)
            {
                foreach (TreeNode nd in node.Nodes)
                {
                    if (nd.Tag.ToInt32() == GroupID)
                        return nd;
                    else
                    {
                        NodeToBeSelected = FindNode(nd, GroupID);
                        if (NodeToBeSelected != null)
                            break;
                    }
                }
            }

            return NodeToBeSelected;
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {            
            AddNewItem();
            GetAccountCode();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SaveAccounts();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DeleteAccount();
        }

        private void BtnBottomSave_Click(object sender, EventArgs e)
        {
            SaveAccounts();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (SaveAccounts())
            {
                MbChangeStatus = false;
                Close();
            }
        }

        private int GetAccountGroupID(long lngAcID, bool blnGrp)
        {
            int intGID = 0;
            ClsCommonUtility objCommonUtility = new ClsCommonUtility();
            DataTable datTemp = new DataTable();
            
            if (blnGrp)
            {
                datTemp = objCommonUtility.FillCombos(new string[] { "ParentID", "AccAccountGroup", "AccountGroupID = " + lngAcID });
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        intGID = datTemp.Rows[0]["ParentID"].ToInt32();
                }
            }
            else
            {
                datTemp = objCommonUtility.FillCombos(new string[] { "AccountGroupID", "AccAccountMaster", "AccountID = " + lngAcID });
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        intGID = datTemp.Rows[0]["AccountGroupID"].ToInt32();
                }
            }
            return intGID;
        }

        /// <summary>
        /// Getting account/group information
        /// </summary>
        private void GetAccountInfo()
        {
            try
            {
                int iAccountID = (TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") == -1 ?
                    Convert.ToInt32(TvAccountsHead.SelectedNode.Tag) :
                    Convert.ToInt32(TvAccountsHead.SelectedNode.Tag.ToString().Remove(0, 1)));

                if (MObjclsBLLChartOfAccounts.GetAccountInfo(TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") == -1 ? true : false, iAccountID))
                {
                    MiAccountID = iAccountID;//                    
                    TxtDescription.Text = MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.Description;

                    blnGrp  = (TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") == -1 ? true : false);
                    txtTaxValue.Text = MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.AccValue.ToString();
                    cboGroup.SelectedValue = GetAccountGroupID(MiAccountID, blnGrp);
                    if (blnGrp)
                    {
                        rbtnCreateGroup.Checked = true;
                        rbtnCreateLedger.Checked = false;
                    }
                    else
                    {
                        rbtnCreateGroup.Checked = false;
                        rbtnCreateLedger.Checked = true;
                    }

                    TxtShortName.Text = MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.Code;
                    SetEnableCheckBox(blnGrp );
                    chkCostCenter.Checked  = MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.IsCostCenterApplicable;
                    //(blnGrp  == true ? COAErrorCode.CouldNotDeleteAccountGroup : COAErrorCode.CouldNotDeleteAccount )


                    LblAccountsProperty.Text = (blnGrp ==true ?StrGrpCaption :strAcCaption)  + TxtDescription.Text.Trim();
                    SetEnableDisable(false);  // Disable buttons
                    this.MbAddStatus =  false;
                    BtnDelete.Enabled = MblnDeletePermission;
                    if (MblnDeletePermission )
                        BtnDelete.Enabled = this.TxtDescription.Enabled = this.TxtShortName.Enabled = !this.MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.Predefined;

                    TxtShortName.Enabled = (!Convert.ToBoolean(MObjclsBLLChartOfAccounts.CheckAccountCodeAutogenerated(cboGroup.SelectedValue.ToInt32())));
                }
            }
            catch (Exception Ex) { throw Ex; }
        }

        private void TvAccountsHead_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                EventArgs Ev = new EventArgs();
                EnableButtons(sender, Ev);

                GetAccountInfo();

                lblStatusMsg .Text  = "Please right click to add new account or group";
                TmAccounts.Start();
            }
            catch (Exception Ex)
            {
                this.MObjLogs.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                //MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void MnuAddGroup_Click(object sender, EventArgs e)
        {
            try
            {
                EnableButtons(sender, e);
                AddNewItem();

                this.TxtDescription.Enabled = this.TxtShortName.Enabled = true;

                SetEnableCheckBox(true);
                rbtnCreateGroup.Checked = true;
                rbtnCreateLedger.Checked = false;
            }
            catch (Exception Ex)
            {
                this.MObjLogs.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

            }
        }

        private void MnuAddAccount_Click(object sender, EventArgs e)
        {
            this.CreateAccount();
            rbtnCreateLedger.Checked = true;
            GetAccountCode();
        }

        private void CreateAccount()
        {
            try
            {
                SetEnableCheckBox(false);
                EnableButtons(new object(), new EventArgs());

                this.TxtDescription.Enabled = this.TxtShortName.Enabled = true;

                AddNewItem();
            }
            catch (Exception Ex)
            {
                this.MObjLogs.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

            }
        }

        /// <summary>
        /// Delete account/group
        /// </summary>
        /// <returns></returns>
        private bool DeleteAccount()
        {
            try
            {
                int iAccountID = 0;                
                if (TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") == -1)
                    iAccountID = Convert.ToInt32(TvAccountsHead.SelectedNode.Tag);
                else
                    iAccountID = Convert.ToInt32(TvAccountsHead.SelectedNode.Tag.ToString().Substring(TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") + 1));

                if (MObjclsBLLChartOfAccounts.CheckExistReferences(blnGrp, iAccountID, false))
                    this.UserMessage.ShowMessage(Convert.ToInt32(blnGrp == true ? COAErrorCode.CouldNotDeleteAccountGroup : COAErrorCode.CouldNotDeleteAccount));
                else
                {
                    if (MObjclsBLLChartOfAccounts.CheckExistReferences(blnGrp, iAccountID, true))
                        this.UserMessage.ShowMessage(Convert.ToInt32(blnGrp == true ? COAErrorCode.CouldNotDeleteAccountGroup : COAErrorCode.CouldNotDeleteAccount));
                    else
                    {
                        if (this.UserMessage.ShowMessage(13))
                        {
                            if (MObjclsBLLChartOfAccounts.DeleteAccount(TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") == -1 ? true : false, iAccountID))
                            {
                                this.UserMessage.ShowMessage(4);
                                FillAccountsTree();
                            }
                        }
                    }                    
                }

                LoadAccountGroup();

                return true;
            }
            catch (Exception Ex) { throw Ex; }
        }

        private void MnuDeleteAccount_Click(object sender, EventArgs e)
        {
            try
            {
                if (MblnDeletePermission)
                    DeleteAccount();
                else
                    this.LblShowStatus.Text = "Sorry!!, You do not have privilege to delete!";
            }
            catch (Exception Ex)
            {
                this.MObjLogs.WriteLog("Error on deleting account:MnuDeleteAccount_Click" + this.Name + " " + Ex.Message.ToString(), 2);

            }
        }

        private void ChkCompany_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ChkMain_CheckedChanged(object sender, EventArgs e)
        {
            if (blnGrp )
            {
                LblShortName.Text = "Short Name";
                LblDescription.Text = "Group Head";
            }
            else
            {
                LblShortName.Text = "Code";
                LblDescription.Text = "Account Head";
            }


            EnableButtons(sender, e);
        }

        private void TvAccountsHead_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    TvAccountsHead.SelectedNode = e.Node; 

                    int GroupID = TvAccountsHead.SelectedNode.Tag.ToInt32();
                    if (GroupID == 0)
                    {
                        int iAccountID = (TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") == -1 ?
                            Convert.ToInt32(TvAccountsHead.SelectedNode.Tag) :
                            Convert.ToInt32(TvAccountsHead.SelectedNode.Tag.ToString().Remove(0, 1)));

                        bool blnGrp = (TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") == -1 ? true : false);

                        GroupID = GetAccountGroupID(iAccountID, blnGrp);
                    }
                    cboGroup.SelectedValue = GroupID;
                    
                    if (!MblnAddPermission)
                    {
                        this.lblStatusMsg.Text = "Sorry!! You do not have privilege to add account or group";
                        TmAccounts.Enabled = true;
                        return;
                    }
                    else
                    {
                        this.lblStatusMsg.Text = "Please right click to add new account or group";
                        TmAccounts.Start();
                    }
                    

                    if (GroupID > 0 && this.PermittedAccountGroup != AccountGroups.NotSet)
                    {
                        bool HasPermission = false;

                        if (GroupID == (int)this.PermittedAccountGroup)
                               HasPermission = true;

                        if (!HasPermission)
                        {
                            TreeNode node = e.Node;

                            // check if user has permission to create new account/group under the node the user just clicked
                            while (node.Parent != null)
                            {
                                if (node.Parent.Tag.ToInt32() == (int)this.PermittedAccountGroup)
                                {
                                    HasPermission = true;
                                    break;
                                }
                                node = node.Parent;
                            }
                        }

                        if (!HasPermission)
                        {
                            if (this.MbCalledFromAccountSettings)
                            {
                                this.lblStatusMsg.Text = string.Format("Please create account group or head under {0}", this.PermittedAccountGroup.ToString());
                                TmAccounts.Start();
                                return;
                            }

                            object ErrorCode = null;
                            switch (this.PermittedAccountGroup)
                            {
                                case AccountGroups.SundryDebtors :
                                    // customer
                                    ErrorCode = (object)COAErrorCode.InvalidCustomerAccountHeadSelected;
                                    break;

                                case AccountGroups.SundryCreditors :
                                    // supplier
                                    ErrorCode = (object)COAErrorCode.InvalidSupplierAccountHeadSelected;
                                    break;

                                case AccountGroups.BankAccounts:
                                    ErrorCode = (object)COAErrorCode.InvalidCustomerAccountHeadSelected;
                                    break;
                            }

                            if (ErrorCode != null)
                            {


                                //MessageBox.Show(this.MsMessageCommon.Replace("#", string.Empty), ClsCommonSettings.MessageCaption);
                            }
                            return;
                        }
                    }

                    if (TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") == -1 ? true : false)
                    {
                        MnuAddAccount.Enabled = true;
                        MnuAddGroup.Enabled = true;
                    }
                    else
                    {
                        MnuAddAccount.Enabled = false;
                        MnuAddGroup.Enabled = false;
                    }
                    MnuDeleteAccount.Enabled = !MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.Predefined;
                    MnuAddNew.Show(TvAccountsHead, e.Location);
                }
            }
            catch (Exception Ex)
            {
                this.MObjLogs.WriteLog("Error on filling TvAccountsHead_NodeMouseClick " + this.Name + " " + Ex.Message.ToString(), 2);
                //MessageBox.Show("Error on TvAccountsHead_NodeMouseClick() " + Ex.Message.ToString());
            }
        }

        private void CboCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                //if (Convert.ToInt32(CboCompany.SelectedValue) > 0)//Changed
                //{
                //    MiCompanyId = CboCompany.SelectedValue.ToInt32();
                //}
                FillAccountsTree();
            }
            catch (Exception Ex)
            {
                this.MObjLogs.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                //MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void BtnSummary_Click(object sender, EventArgs e)
        {
            try
            {
                MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.CompanyID = Convert.ToInt32(1);
                MObjclsBLLChartOfAccounts.GetReport(TvAccountsHead.Nodes, true);
                LoadReport();
            }
            catch (Exception Ex)
            {
                this.MObjLogs.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                //MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void LoadReport()
        {
            if (MiCompanyId > 0)
            {
                //FrmAccountsReportViewer ObjViewer = new FrmAccountsReportViewer();
                //ObjViewer.PsFormName = this.Text;
                //ObjViewer.PiRecId = MiCompanyId;
                //ObjViewer.PiFormID = (int)FormID.AccountsEntry;
                //ObjViewer.ShowDialog();
            }
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                //using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                //{
                //    MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.CompanyID = Convert.ToInt32(CboCompany.SelectedValue);
                //    MObjclsBLLChartOfAccounts.GetReport(TvAccountsHead.Nodes, false);

                //    ObjEmailPopUp.MsSubject = "Chart Of Accounts";
                //    ObjEmailPopUp.MiRecordID = Convert.ToInt32(CboCompany.SelectedValue);
                //    ObjEmailPopUp.EmailFormType = EmailFormID.ChartOfAccounts;
                //    ObjEmailPopUp.ShowDialog();
                //}
            }
            catch (Exception Ex)
            {
                this.MObjLogs.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                //MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
            }
        }

        private void SetNodeSelection()
        {
            try
            {
                // first make sure that PermittedAccountGroup has value
                if (this.PermittedAccountGroup != AccountGroups.NotSet || this.SelectedAccount != Accounts.NotSelect)
                {
                    int GroupID = (int)this.PermittedAccountGroup;
                    int AccountID = (int)this.SelectedAccount;
                    TreeNode NodeToBeSelected = null;
                    TreeNode NodeForAccount = null;
                    if (this.TvAccountsHead.Nodes.Count <= 0)
                        return;
                    foreach (TreeNode node in this.TvAccountsHead.Nodes)
                    {
                        TreeNode childNode = node;
                        NodeToBeSelected = this.FindNode(childNode, GroupID);
                       
                        if (NodeToBeSelected != null )
                            break;
                    }
                    foreach (TreeNode node in this.TvAccountsHead.Nodes)
                    {
                        TreeNode childNode = node;

                        NodeForAccount = this.FindAccNode(childNode, AccountID);
                       
                        if ( NodeForAccount != null)
                            break;
                    }
                    if (NodeToBeSelected != null)
                    {
                        this.TvAccountsHead.CollapseAll();
                        this.TvAccountsHead.SelectedNode = NodeToBeSelected;
                        this.TvAccountsHead.LabelEdit = true;
                        this.TvAccountsHead.SelectedNode.BeginEdit();
                        this.TvAccountsHead.LabelEdit = false;
                        this.TvAccountsHead.SelectedNode.ExpandAll();
                    }
                    if (NodeForAccount != null)
                    {
                        this.TvAccountsHead.CollapseAll();
                        this.TvAccountsHead.SelectedNode = NodeForAccount;
                        this.TvAccountsHead.LabelEdit = true;
                        this.TvAccountsHead.SelectedNode.BeginEdit();
                        this.TvAccountsHead.LabelEdit = false;
                        this.TvAccountsHead.SelectedNode.ExpandAll();
                    }
                }
            }
            catch (Exception ex)
            {
                this.MObjLogs.WriteLog(string.Format("Error occured in SetNodeSelection() Form Name :{0}, Exception :{1}", this.Name, ex.ToString()), 3);
            }

        }

        private TreeNode FindNode(TreeNode node, int GroupID)
        {
            TreeNode NodeToBeSelected = null;

            if (node.Nodes.Count > 0)
            {
                foreach (TreeNode nd in node.Nodes)
                {
                    if (nd.Tag.ToInt32() == GroupID)
                        return nd;
                    else
                    {
                        NodeToBeSelected = FindNode(nd, GroupID);
                        if (NodeToBeSelected != null)
                            break;
                    }
                }
            }

           return NodeToBeSelected;
        }
        /// <summary>
        /// To Find Accout Node From TreeView
        /// </summary>
        /// <param name="node"></param>
        /// <param name="AccoutID"></param>
        /// <returns></returns>
        private TreeNode FindAccNode(TreeNode node, int AccoutID)
        {
            TreeNode NodeForAccount = null;

            if (node.Nodes.Count > 0)
            {
                foreach (TreeNode nd in node.Nodes)
                {
                    if (nd.Tag.ToString().IndexOf("@") != -1)
                    {
                        if (Convert.ToInt32(nd.Tag.ToString().Remove(0, 1))==AccoutID)
                        return nd;
                    }
                    else
                    {
                        NodeForAccount = FindAccNode(nd, AccoutID);
                        if (NodeForAccount != null)
                            break;
                    }
                }
            }

            return NodeForAccount;
        }

        private void CboCompany_KeyDown(object sender, KeyEventArgs e)
        {
            //CboCompany.DroppedDown = false;
            if (e.KeyCode == Keys.Enter)
                CboCompany_SelectionChangeCommitted(sender, new EventArgs());
        }

        private void chkCostCenter_CheckedChanged(object sender, EventArgs e)
        {
            EnableButtons(sender, e);
        }

        private void TmAccounts_Tick(object sender, EventArgs e)
        {
            lblStatusMsg.Text = "";
            TmAccounts.Enabled = false;
        }

        private void btnOpeningBalance_Click(object sender, EventArgs e)
        {
            using (frmOpeningBalance objfrmOpeningBalance = new frmOpeningBalance ())
            {
               if(TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") != -1)
                   objfrmOpeningBalance .intAccountID = Convert.ToInt32(TvAccountsHead.SelectedNode.Tag.ToString().Remove(0, 1));
                objfrmOpeningBalance.ShowDialog ();
            }

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (MiCompanyId > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PintCompany = MiCompanyId;
                ObjViewer.PiRecId = 1;
                ObjViewer.PiFormID = (int)FormID.JournalRecurrenceSetup ;
                ObjViewer.ShowDialog();
            }
        }

        private void cboGroup_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboGroup.DroppedDown = false;
        }

        private void cboGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableButtons(sender, e);
            GetAccountCode();
            if (cboGroup.SelectedValue.ToInt32() == 33)
                lblTaxValue.Visible = txtTaxValue.Visible = true;
            else
                lblTaxValue.Visible = txtTaxValue.Visible = false;
        }

        private void rbtnCreateLedger_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnCreateLedger.Checked)
                chkCostCenter.Visible = true;
            else
                chkCostCenter.Visible = false;

            GetAccountCode();
        }

        private void rbtnCreateGroup_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnCreateGroup.Checked)
                chkCostCenter.Visible = false;
            else
                chkCostCenter.Visible = true;

            GetAccountCode();
        }

        private void GetAccountCode()
        {
            if (rbtnCreateLedger.Checked)
            {
                int intGrID = cboGroup.SelectedValue.ToInt32();
                TxtShortName.Text = MObjclsBLLChartOfAccounts.GetAccountCode(intGrID);
                TxtShortName.Enabled = (!Convert.ToBoolean(MObjclsBLLChartOfAccounts.CheckAccountCodeAutogenerated(intGrID)));
            }
            else
            {
                TxtShortName.Text = "";
                TxtShortName.Enabled = true;
            }
        }

        private void txtTaxValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars;

            strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            strInvalidChars = strInvalidChars + ".";
            e.Handled = false;
            if (((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))))
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 )
            {
                e.Handled = true;
            }
        }
    }
}