﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlTypes;

namespace MyBooksERP
{
    public partial class frmReccurigJournalSetup : Form
    {
        #region Decleration
            private ClsCommonUtility objCommonUtility = null;
            private clsBLLJournalVoucher objclsBLLJournalVoucher = null;
            private clsBLLReurringJournalSetup objBllLReurringJournalSetup = null;

            private clsMessage objUserMessage = null;
            clsDTOJournalVoucher objclsDTOJournalVoucher = null;

            clsDTOReurringJournalSetup objDTOReurringJournalSetup = null;

            private int RecurringJournalSetupId = 0;
            private eMode Mode = eMode.Insert;
            private long RowIndex;
            private bool MblnAddPermission = true;           //To set Add Permission
            private bool MblnUpdatePermission = true;      //To set Update Permission
            private bool MblnDeletePermission = true;
            private bool MblnAddUpdatePermission = true;   //To set Add Update Permission
            private bool MblnPrintEmailPermission = true;        //To set Print Email Permission

            private bool MblnChangeStatus = true;
            private int RecurrenceCount = 0;

        #endregion

        #region Property Setting
            private clsBLLReurringJournalSetup BLLReurringJournalSetup
            {
                get
                {
                    
                    if (this.objBllLReurringJournalSetup == null)
                        this.objBllLReurringJournalSetup = new clsBLLReurringJournalSetup();
                    return this.objBllLReurringJournalSetup;
                }
                set
                {
                    this.BLLReurringJournalSetup = value;
                }
            }
            private clsBLLJournalVoucher BLLJournalVoucher
            {
                get
                {
                    if (this.objclsBLLJournalVoucher == null)
                        this.objclsBLLJournalVoucher = new clsBLLJournalVoucher();
                    return this.objclsBLLJournalVoucher;
                }
                set
                {
                    this.BLLJournalVoucher = value;
                }
            }
            private ClsCommonUtility CommonUtility
            {
                get
                {
                    if (this.objCommonUtility == null)
                        this.objCommonUtility = new ClsCommonUtility();
                    return this.objCommonUtility;
                }
            }
            private clsMessage UserMessage
            {
                get
                {
                    if (this.objUserMessage == null)
                        this.objUserMessage = new clsMessage(FormID.JournalRecurrenceSetup);

                    return this.objUserMessage;
                }
            }



        #endregion

        #region Constructor
        public frmReccurigJournalSetup()
        {
            InitializeComponent();
        }
        #endregion

        #region Functions
       
        private void SwitchToNewMode()
        {
            try
            {
                this.RecurringJournalSetupId = 0;
                cboCompany.SelectedValue =ClsCommonSettings.CompanyID;
                this.Mode = eMode.Insert;
                this.RowIndex = 1;
                LoadCombos();
                dgvVoucherDetails.DataSource=null;
                this.BindNavigator();
                bindingNavigatorAddNewItem.Enabled=btnSaveItem.Enabled= btnOk.Enabled=btnSave.Enabled = false;
                MblnChangeStatus = false;
                groupBox2.Enabled = false;
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }

        }
        private void ClearControls()
        {
            cboCompany.SelectedValue = ClsCommonSettings.CompanyID;
            cboVoucherType.SelectedIndex = -1;
            cboRecurrenceType.SelectedIndex = -1;
            cboVoucherNumber.SelectedIndex = -1;
            cboVoucherNumber.Text=string.Empty;
            txtNoofOccurence.Text = string.Empty;
            rdbActive.Checked = true;
            dtpStartDate.Enabled = true;
        }
        private void BindNavigator()
        {
            try
            {
                this.errorProviderRecurringJournalSetup.Clear();
                this.ClearControls();

                BLLReurringJournalSetup.objPager.RowIndex = this.RowIndex;
                BLLReurringJournalSetup.BindRecurrenceSetupInfo();
                SetNavigatorText();

                if (this.Mode == eMode.Update)
                {
                    this.BindControls();
                }
                else
                {
                    this.RowIndex = BLLReurringJournalSetup.objPager.TotalRecords + 1;
                }
                BindingNavigatorEnabledDisabled();
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }
        private void OnInputChanged(object sender, EventArgs e)
        {
            
                MblnChangeStatus = true;
                btnOk.Enabled = btnSave.Enabled =btnSaveItem.Enabled= MblnAddUpdatePermission;
            
        }
       
        /// <summary>
        /// 
        /// </summary>
        private void BindingNavigatorEnabledDisabled()
        {
            try
            {
                this.bindingNavigatorMoveFirstItem.Enabled = this.bindingNavigatorMovePreviousItem.Enabled = (this.RowIndex > 1);
                this.bindingNavigatorMoveNextItem.Enabled = this.bindingNavigatorMoveLastItem.Enabled = this.RowIndex < BLLReurringJournalSetup.objPager.TotalRecords;

                if (this.RowIndex <= this.BLLReurringJournalSetup.objPager.TotalRecords)
                    this.bindingNavigatorAddNewItem.Enabled = MblnAddPermission;

                bindingNavigatorDeleteItem.Enabled = (this.Mode == eMode.Update ? MblnDeletePermission : false);
                BtnPrint.Enabled = (this.Mode == eMode.Update ? MblnPrintEmailPermission : false);

                btnOk.Enabled =btnSaveItem.Enabled=  btnSave.Enabled = false;
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void SetNavigatorText()
        {
            try
            
            {
                if (BLLReurringJournalSetup.objPager.TotalRecords > 0)
                {
                    this.bindingNavigatorCountItem.Text = string.Format("of {0}", (this.Mode == eMode.Insert ? BLLReurringJournalSetup.objPager.TotalRecords + 1 : BLLReurringJournalSetup.objPager.TotalRecords));
                    this.bindingNavigatorPositionItem.Text = string.Format("{0}", (this.Mode == eMode.Insert ? BLLReurringJournalSetup.objPager.TotalRecords + 1 : this.RowIndex));
                }
                else
                {
                    this.bindingNavigatorCountItem.Text = string.Format("of {0}", 1);
                    this.bindingNavigatorPositionItem.Text = string.Format("{0}", 1);
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void BindControls()
        {
            try
            {
                SqlDecimal VoucherID = BLLReurringJournalSetup.DTOReurringJournalSetup.VoucherID;
                this.RecurringJournalSetupId = BLLReurringJournalSetup.DTOReurringJournalSetup.RecurringJournalSetupID;
                cboCompany.SelectedValue = BLLReurringJournalSetup.DTOReurringJournalSetup.CompanyId;
                cboVoucherType.SelectedValue = BLLReurringJournalSetup.DTOReurringJournalSetup.VoucherTypeId;
                cboRecurrenceType.SelectedValue = BLLReurringJournalSetup.DTOReurringJournalSetup.RecurrenceSetupID;
                cboVoucherNumber.SelectedValue = BLLReurringJournalSetup.DTOReurringJournalSetup.VoucherID.ToDecimal();
                txtNoofOccurence.Text = BLLReurringJournalSetup.DTOReurringJournalSetup.NoofOccurance.ToString();
                dtpStartDate.Value = BLLReurringJournalSetup.DTOReurringJournalSetup.StartDate;
                rdbActive.Checked= BLLReurringJournalSetup.DTOReurringJournalSetup.Status;
                rdbInactive.Checked=!(BLLReurringJournalSetup.DTOReurringJournalSetup.Status);
                cboVoucherNumber.SelectedValue = VoucherID.ToDecimal();
                RecurrenceCount = objBllLReurringJournalSetup.GetRecurredCount();
                dtpStartDate.Enabled =cboRecurrenceType.Enabled = RecurrenceCount == 0;
                //if (this.RecurringJournalSetupId > 0)
                //{
                //    DataTable dt = BLLSiteVisit.BindJobOrderInfo();
                //    BindDataGrid(dt);
                //}
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
            }
        }
        public void LoadCombos()
            {
                try
                {
                    DataTable dt = this.CommonUtility.FillCombos(new string[] { "CompanyID, CompanyName", "CompanyMaster", "1=1 Order by CompanyName" });
                    this.BindCombo(this.cboCompany, "CompanyID", "CompanyName", dt);

                    
                    dt = this.CommonUtility.FillCombos(new string[] { "VoucherTypeID, VoucherType", "AccVoucherTypeReference", "1=1 Order by VoucherType" });
                    this.BindCombo(this.cboVoucherType, "VoucherTypeID", "VoucherType", dt);

                    dt = this.CommonUtility.FillCombos(new string[] { "RecurrenceSetupID, RecurrenceType", "AccRecurranceSetup", "1=1 Order by RecurrenceType" });
                    this.BindCombo(this.cboRecurrenceType, "RecurrenceSetupID", "RecurrenceType", dt);

                }
                catch (Exception ex)
                {
                    ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
                }
            }

        public void BindCombo(ComboBox cbo, string ValueMember, string DisplayMember, object dataSource)
            {
                try
                {
                    cbo.DataSource = dataSource;
                    cbo.DisplayMember = DisplayMember;
                    cbo.ValueMember = ValueMember;
                    
                }
                catch (Exception ex)
                {
                    ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
                }
            }
        /// <summary>
        /// user input validations
        /// </summary>
        /// <returns></returns>
        private bool Validations()
        {
            if (cboCompany.SelectedIndex == -1)
            {
                UserMessage.ShowMessage((int)eRecurringJournalSetupMessageCodes.CompanyRequired, cboCompany, errorProviderRecurringJournalSetup);
                return false;
            }
            if (cboVoucherType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage((int)eRecurringJournalSetupMessageCodes.VoucherTypeRequired, cboVoucherType, errorProviderRecurringJournalSetup);
                return false;
            }
            if (cboVoucherNumber.SelectedIndex == -1)
            {
                UserMessage.ShowMessage((int)eRecurringJournalSetupMessageCodes.VoucherRequired, cboVoucherNumber, errorProviderRecurringJournalSetup);
                return false;
            }
            else
            {
                this.BLLReurringJournalSetup.DTOReurringJournalSetup.VoucherID = (SqlDecimal)(cboVoucherNumber.SelectedValue.ToDecimal());
                this.BLLReurringJournalSetup.DTOReurringJournalSetup.RecurringJournalSetupID = RecurringJournalSetupId;
                if (BLLReurringJournalSetup.IsRecurringExists())
                {
                    UserMessage.ShowMessage((int)eRecurringJournalSetupMessageCodes.Recurrencealreadyexists, cboVoucherNumber, errorProviderRecurringJournalSetup);
                    return false;
                }

            }
            if (cboRecurrenceType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage((int)eRecurringJournalSetupMessageCodes.RecurrensesetupRequired, cboRecurrenceType, errorProviderRecurringJournalSetup);
                return false;
            }
            if (txtNoofOccurence.Text == string.Empty || txtNoofOccurence.Text == "0")
            {
                UserMessage.ShowMessage((int)eRecurringJournalSetupMessageCodes.NoofOccurencerequired, txtNoofOccurence, errorProviderRecurringJournalSetup);
                return false;
            }
            else
            {
                 if (this.Mode == eMode.Update && txtNoofOccurence.Text.ToInt32()<RecurrenceCount )
                {
                    UserMessage.ShowMessage((int)eRecurringJournalSetupMessageCodes.RdcuurenceOccured, txtNoofOccurence, errorProviderRecurringJournalSetup);
                    return false;
                }
            }
           
       

            return true;
        }
        /// <summary>
        /// set message codes for user input validations
        /// </summary>
        private enum eRecurringJournalSetupMessageCodes
        {
            
            CompanyRequired = 6100,
            VoucherTypeRequired = 6101,
            VoucherRequired = 6102,
            RecurrensesetupRequired=6103,
            StartDateRequired =6104,
            NoofOccurencerequired=6105,
            Recurrencealreadyexists = 6106,
            IsAlertGiven=6107,
            RdcuurenceOccured=6108

        }
        private void LoadReport()
        {
            FrmReportviewer ObjViewer = new FrmReportviewer();
            ObjViewer.PsFormName = this.Text;
            ObjViewer.PintCompany = cboCompany.SelectedValue.ToInt32();
            ObjViewer.PiRecId = this.RecurringJournalSetupId;
            ObjViewer.PiFormID = (int)FormID.JournalRecurrenceSetup;
            ObjViewer.ShowDialog();
        }
        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 2)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Accounts, (int)eMenuID.RecurringJournalSetup, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                if (MblnAddPermission == true || MblnUpdatePermission == true)
                    MblnAddUpdatePermission = true;
            }
            else
                MblnAddPermission = MblnAddUpdatePermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }
        #endregion

        #region Events
            private void frmReccurigJournalSetup_Load(object sender, EventArgs e)
            {
                this.SetPermissions();
                this.SwitchToNewMode();
            }

            private void cboVoucherType_SelectedIndexChanged(object sender, EventArgs e)
            {
                if (cboVoucherType.SelectedIndex != -1 && cboCompany.SelectedIndex != -1)
                {
                    BLLReurringJournalSetup.DTOReurringJournalSetup.CompanyId = cboCompany.SelectedValue.ToInt32();
                    BLLReurringJournalSetup.DTOReurringJournalSetup.VoucherTypeId = cboVoucherType.SelectedValue.ToInt32();
                    BindCombo(cboVoucherNumber, "VoucherID", "VoucherNo", BLLReurringJournalSetup.FillComboVoucherNo());
                    OnInputChanged(sender, e);
                    if (cboVoucherNumber.DataSource == null)
                        cboVoucherNumber.Text = string.Empty;
                }
            }

            private void cboVoucherNumber_SelectedIndexChanged(object sender, EventArgs e)
            {
                if(cboVoucherNumber.SelectedIndex !=-1)
                {
                    OnInputChanged(sender, e);
                    BLLReurringJournalSetup.DTOReurringJournalSetup.VoucherID=(SqlDecimal)(cboVoucherNumber.SelectedValue.ToDecimal());
                    dgvVoucherDetails.DataSource = BLLReurringJournalSetup.GetVoucherDetails();
                    dgvVoucherDetails.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                }
            }
            private void btnOk_Click(object sender, EventArgs e)
            {
                this.Save();
                this.Close();                    
                     
            }
            /// <summary>
            /// Save information
            /// </summary>
            private void Save()
            {
                try
                {
                    bool SaveConfirmation;
                    if (Validations())
                    {
                        if (!this.BLLReurringJournalSetup.IsAlertUserSettingsExists())
                        {
                            if (this.UserMessage.ShowMessage((int)eRecurringJournalSetupMessageCodes.IsAlertGiven))
                            {
                                using (FrmAlertSettings objAlertSettings = new FrmAlertSettings())
                                {
                                    objAlertSettings.ModuleID =(int) eModuleID.Accounts;
                                    objAlertSettings.ShowDialog();
                                }
                            }
                        }
                        if (this.Mode == eMode.Insert)
                            SaveConfirmation = UserMessage.ShowMessage(1);
                        else
                            SaveConfirmation = UserMessage.ShowMessage(3);
                        if (SaveConfirmation)
                        {
                            this.BLLReurringJournalSetup.DTOReurringJournalSetup.RecurringJournalSetupID = this.RecurringJournalSetupId;
                            this.BLLReurringJournalSetup.DTOReurringJournalSetup.RecurrenceSetupID = cboRecurrenceType.SelectedValue.ToInt32();
                            this.BLLReurringJournalSetup.DTOReurringJournalSetup.VoucherID = (SqlDecimal)(cboVoucherNumber.SelectedValue.ToDecimal());
                            this.BLLReurringJournalSetup.DTOReurringJournalSetup.StartDate = dtpStartDate.Value.Date;
                            this.BLLReurringJournalSetup.DTOReurringJournalSetup.NoofOccurance = txtNoofOccurence.Text.ToInt32();
                            this.BLLReurringJournalSetup.DTOReurringJournalSetup.Status = rdbActive.Checked;
                            this.RecurringJournalSetupId = this.BLLReurringJournalSetup.SaveRecurrenceFournalSetup();
                            this.UserMessage.ShowMessage(2);
                            SwitchToNewMode();
                            MovePrevious();

                        }
                    }
                }


                catch (Exception ex)
                {
                    ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
                }

            }
            private void cboCompany_KeyDown(object sender, KeyEventArgs e)
            {
                ((ComboBox)sender).DroppedDown = false;
            }

        #endregion

            private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
            {
                try
                {
                    MblnChangeStatus = false;
                    this.Mode = eMode.Update;
                    this.RowIndex = 1;

                    BindNavigator();
                }
                catch (Exception ex)
                {
                    ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
                }
            }

            private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
            {
                try
                {
                  
                    MovePrevious();
                }
                catch (Exception ex)
                {
                    ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
                }
            }
            private void MovePrevious()
            {
                MblnChangeStatus = false;
                this.Mode = eMode.Update;
                this.RowIndex = this.RowIndex - 1;

                BindNavigator();
            }

            private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
            {
                try
                {
                    this.Mode = eMode.Update;
                    this.RowIndex = this.RowIndex + 1;

                    BindNavigator();
                }
                catch (Exception ex)
                {
                    ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
                }
            }

            private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
            {
                try
                {
                    this.Mode = eMode.Update;
                    this.RowIndex = BLLReurringJournalSetup.objPager.TotalRecords;

                    this.BindNavigator();
                }
                catch (Exception ex)
                {
                    ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
                }
            }

            private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
            {
                
                SwitchToNewMode();
                // to set the grid with null table

                BLLReurringJournalSetup.DTOReurringJournalSetup.VoucherID = 0;
                dgvVoucherDetails.DataSource = BLLReurringJournalSetup.GetVoucherDetails();
                dgvVoucherDetails.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            private void BtnClear_Click(object sender, EventArgs e)
            {
                try
                {
                    ClearControls();
                }
                catch (Exception ex)
                {
                    ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
                }
            }

            private void btnSave_Click(object sender, EventArgs e)
            {
                Save();
            }

            private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
            {
                try
                {
                    if (this.UserMessage.ShowMessage(13) == true)
                    {
                        if (BLLReurringJournalSetup.DeleteRecuuringJournalSetup())
                        {
                            tslStatus.Text = this.UserMessage.GetMessageByCode(4);
                            tmRecurringJournalSetup.Enabled = true;
                            this.UserMessage.ShowMessage(4);
                            this.SwitchToNewMode();
                        }
                    }

                }
                catch (Exception ex)
                {
                    ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
                }
            }

            private void BtnPrint_Click(object sender, EventArgs e)
            {
                LoadReport();
            }

            private void btnRecurrenceSetup_Click(object sender, EventArgs e)
            {
                try
                {
                    using (frmRecuurenceSetup obj = new frmRecuurenceSetup())
                    {
                        int RecurrenseTypeId = 0;                        
                        RecurrenseTypeId = cboRecurrenceType.SelectedValue.ToInt32();
                        obj.ShowDialog();
                        DataTable dt = this.CommonUtility.FillCombos(new string[] { "RecurrenceSetupID, RecurrenceType", "AccRecurranceSetup", "1=1 Order by RecurrenceType" });
                        this.BindCombo(this.cboRecurrenceType, "RecurrenceSetupID", "RecurrenceType", dt);
                        this.cboRecurrenceType.SelectedValue = RecurrenseTypeId;
                    }
                }
                catch (Exception ex)
                {
                    ClsLogWriter.WriteLog(ex,Log.LogSeverity.Error);
                }
            }

            private void dgvVoucherDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
            {
                try
                {
                }
                catch
                {
                }
            }

            private void btnAlertSettings_Click(object sender, EventArgs e)
            {
                using (FrmAlertSettings objAlertSettings=new FrmAlertSettings())
                {
                    objAlertSettings.ModuleID =(int) eModuleID.Accounts;
                    objAlertSettings.ShowDialog();
                }
            }

            private void btnCancel_Click(object sender, EventArgs e)
            {
                this.Close();
            }
        
    }
}
