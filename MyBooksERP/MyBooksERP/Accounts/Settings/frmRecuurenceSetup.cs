﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class frmRecuurenceSetup : Form
    {
        clsBLLRecurrenceSetup MobjclsBLLRecurrenceSetup = new clsBLLRecurrenceSetup();
        int TotalRecordCnt, CurrentRecCnt;
        bool MblnAddPermission, MblnAddUpdatePermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission;
        private clsMessage objUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.RecurrentSetup);

                return this.objUserMessage;
            }
        }

        public frmRecuurenceSetup()
        {
            InitializeComponent();
        }

        private void rdbDaily_CheckedChanged(object sender, EventArgs e)
        {
            pnlDaily.Visible = true;
            pnlMonthly.Visible = pnlWeekly.Visible = false;
        }

        private void rdbWeekly_CheckedChanged(object sender, EventArgs e)
        {
            pnlWeekly.Visible = true;
            pnlMonthly.Visible = pnlDaily.Visible = false;
        }

        private void rdbMonthly_CheckedChanged(object sender, EventArgs e)
        {
            pnlMonthly.Visible = true;
            pnlDaily.Visible = pnlWeekly.Visible = false;

            rdbIsDaysMOnthly.Checked = true;
            rdbIsOnMonthly.Checked = false;
        }

        private void rdbIsDaysMOnthly_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbIsDaysMOnthly.Checked == true)
            {
                nudDayofMonth.Enabled = nudMonthIntervalIsDays.Enabled = true;
                cboWeekDayOrder.Enabled = cboDays.Enabled = nudMonthIntervalIsOn.Enabled = false;
            }
            else
            {
                nudDayofMonth.Enabled = nudMonthIntervalIsDays.Enabled = false;
                cboWeekDayOrder.Enabled = cboDays.Enabled = nudMonthIntervalIsOn.Enabled = true;
            }
        }

        private void rdbIsOnMonthly_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbIsOnMonthly.Checked == true)
            {
                cboWeekDayOrder.Enabled = cboDays.Enabled = nudMonthIntervalIsOn.Enabled = true;
                nudDayofMonth.Enabled = nudMonthIntervalIsDays.Enabled = false;                
            }
            else
            {                
                cboWeekDayOrder.Enabled = cboDays.Enabled = nudMonthIntervalIsOn.Enabled = false;
                nudDayofMonth.Enabled = nudMonthIntervalIsDays.Enabled = true;
            }
        }
        
        private void frmRecuurenceSetup_Load(object sender, EventArgs e)
        {
            tmrRecurranceSetup.Enabled = false;
            LoadCombos();
            SetPermissions();
            clearControls();         
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 2)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Accounts, (int)eMenuID.RecurenceSetup, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                if (MblnAddPermission == true || MblnUpdatePermission == true)
                    MblnAddUpdatePermission = true;
            }
            else
                MblnAddPermission = MblnAddUpdatePermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

         private void txtRecurranceType_TextChanged(object sender, EventArgs e)
         {
             bindingNavigatorAddNewItem.Enabled = MblnAddPermission;
             btnSave.Enabled = btnOk.Enabled = bindingNavigatorSaveItem.Enabled = MblnAddUpdatePermission;
             if (txtRecurranceType.Tag.ToInt32() > 0)
             {
                 btnSave.Enabled = btnOk.Enabled = bindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                 bindingNavigatorDeleteItem.Enabled = MblnDeletePermission; 
                 bindingNavigatorPrint.Enabled = bindingNavigatorEmail.Enabled = MblnPrintEmailPermission;
             }                 
             else
                 bindingNavigatorDeleteItem.Enabled = bindingNavigatorPrint.Enabled = bindingNavigatorEmail.Enabled = false;
         }

        private void LoadCombos()
        {
            cboWeekDayOrder.DataSource = MobjclsBLLRecurrenceSetup.FillCombos(new string[] { "WeekDayOrderID,WeekDayOrder", "AccWorkDayOrderReference", "" });
            cboWeekDayOrder.ValueMember = "WeekDayOrderID";
            cboWeekDayOrder.DisplayMember = "WeekDayOrder";

            cboDays.DataSource = MobjclsBLLRecurrenceSetup.FillCombos(new string[] { "DayID,DayName", "DayReference", "" });
            cboDays.ValueMember = "DayID";
            cboDays.DisplayMember = "DayName";
        }

        private void clearControls()
        {
            txtRecurranceType.Tag = 0;
            txtRecurranceType.Text = "";
            nudDayInterval.Value = 1;
            rdbIsDaysDaily.Checked = true;
            rdbDaily.Checked = true;
            pnlDaily.Visible = true;            
            pnlMonthly.Visible = pnlWeekly.Visible = false;
            nudWeekInterval.Value = 1;
            chkSunday.Checked = false;
            chkMonday.Checked = false;
            chkTuesDay.Checked = false;
            chkWednesday.Checked = false;
            chkThursday.Checked = false;
            chkFriday.Checked = false;
            chkSaturday.Checked = false;
            rdbIsDaysMOnthly.Checked = true;
            nudDayofMonth.Value = 1;
            nudMonthIntervalIsDays.Value = 1;
            nudMonthIntervalIsOn.Value = 1;
            cboWeekDayOrder.SelectedIndex = 0;
            cboDays.SelectedIndex = 0;

            RecordCount();
            bindingNavigatorCountItem.Text = "of " + (TotalRecordCnt + 1).ToString();
            bindingNavigatorPositionItem.Text = (TotalRecordCnt + 1).ToString();
            
            bindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            btnSave.Enabled = btnOk.Enabled = bindingNavigatorSaveItem.Enabled = false;
            bindingNavigatorDeleteItem.Enabled = bindingNavigatorPrint.Enabled = bindingNavigatorEmail.Enabled = false;
        }

        private void RecordCount()
        {
            TotalRecordCnt = MobjclsBLLRecurrenceSetup.GetRecordCount();
            CurrentRecCnt = TotalRecordCnt;
            bindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
            bindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();

            if (TotalRecordCnt == 0)
            {
                bindingNavigatorCountItem.Text = "of 0";
                CurrentRecCnt = 0;
                TotalRecordCnt = 0;
            }
            BindingEnableDisable();
        }

        private void BindingEnableDisable()
        {
            bindingNavigatorMoveFirstItem.Enabled = true;
            bindingNavigatorMovePreviousItem.Enabled = true;
            bindingNavigatorMoveNextItem.Enabled = true;
            bindingNavigatorMoveLastItem.Enabled = true;

            if (TotalRecordCnt == 0)
            {
                bindingNavigatorMoveFirstItem.Enabled = false;
                bindingNavigatorMovePreviousItem.Enabled = false;
                bindingNavigatorMoveNextItem.Enabled = false;
                bindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                if (Convert.ToDouble(bindingNavigatorPositionItem.Text) == Convert.ToDouble(TotalRecordCnt))
                {
                    bindingNavigatorMoveNextItem.Enabled = false;
                    bindingNavigatorMoveLastItem.Enabled = false;
                }
                else if (Convert.ToDouble(bindingNavigatorPositionItem.Text) == 1)
                {
                    bindingNavigatorMoveFirstItem.Enabled = false;
                    bindingNavigatorMovePreviousItem.Enabled = false;
                }
            }
        }

        private bool FormValidation()
        {
            if (txtRecurranceType.Text == "")
            {                
                LblSStripRecurranceSetup.Text = this.UserMessage.GetMessageByCode(400);
                tmrRecurranceSetup.Enabled = true;
                this.UserMessage.ShowMessage(400, txtRecurranceType);
                return false;
            }
            return true;
        }
        
        private void bindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (FormValidation() == true)
            {
                bool blnTempSave = false; // want to save or not.
                bool blnTempUpdate = false; // want to save or not.
                MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intRecurrenceSetupID = txtRecurranceType.Tag.ToInt32();
                MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.strRecurrenceType = txtRecurranceType.Text;
                if (MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intRecurrenceSetupID == 0)
                {
                    if (this.UserMessage.ShowMessage(1) == true) // Save
                        blnTempSave = true;
                }
                else
                {
                    if (this.UserMessage.ShowMessage(3) == true) // Update
                        blnTempUpdate = true;
                }

                if (blnTempSave == true || blnTempUpdate == true)
                {
                    if (rdbDaily.Checked == true)
                    {
                        MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intRecurringPatternID = 1;
                        MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intDayInterval = nudDayInterval.Value.ToInt32();
                        MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsDaysDaily = rdbIsDaysDaily.Checked;
                    }
                    else if (rdbWeekly.Checked == true)
                    {
                        MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intRecurringPatternID = 2;
                        MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intWeekInterval = nudWeekInterval.Value.ToInt32();
                        MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsSunday = chkSunday.Checked;
                        MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsMonday = chkMonday.Checked;
                        MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsTuesday = chkTuesDay.Checked;
                        MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsWednesday = chkWednesday.Checked;
                        MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsThursday = chkThursday.Checked;
                        MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsFriday = chkFriday.Checked;
                        MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsSaturday = chkSaturday.Checked;
                    }
                    else if (rdbMonthly.Checked == true)
                    {
                        MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intRecurringPatternID = 3;
                        MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsDaysMonthly = rdbIsDaysMOnthly.Checked;
                        if (rdbIsDaysMOnthly.Checked == true)
                        {
                            MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intDayofMonth = nudDayofMonth.Value.ToInt32();
                            MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intMonthInterval = nudMonthIntervalIsDays.Value.ToInt32();
                        }
                        else
                        {
                            MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intWeekDayOrderID = cboWeekDayOrder.SelectedValue.ToInt32();
                            MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intDayID = cboDays.SelectedValue.ToInt32();
                            MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intMonthInterval = nudMonthIntervalIsOn.Value.ToInt32();
                        }                        
                    }
                    if (MobjclsBLLRecurrenceSetup.SaveRecurrenceSetup() == true)
                    {
                        if (blnTempSave == true)
                        {                            
                            LblSStripRecurranceSetup.Text = this.UserMessage.GetMessageByCode(2);
                            tmrRecurranceSetup.Enabled = true;
                            this.UserMessage.ShowMessage(2); // Saved Successfully
                        }
                        else if (blnTempUpdate == true)
                        {                            
                            LblSStripRecurranceSetup.Text = this.UserMessage.GetMessageByCode(21);
                            tmrRecurranceSetup.Enabled = true;
                            this.UserMessage.ShowMessage(21); // Updated Successfully
                        }
                        clearControls();
                    }
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bindingNavigatorSaveItem_Click(sender, e);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            bindingNavigatorSaveItem_Click(sender, e);
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            bindingNavigatorPositionItem.Text = "1";
            if (TotalRecordCnt > 1)
                bindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
            else
                bindingNavigatorCountItem.Text = "of 1";
            DiplayRecurranceSetup();
        }

        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            bindingNavigatorPositionItem.Text = (Convert.ToInt32(bindingNavigatorPositionItem.Text) - 1).ToString();
            if (Convert.ToInt32(bindingNavigatorPositionItem.Text) <= 0)
                bindingNavigatorPositionItem.Text = "1";
            if (TotalRecordCnt > 1)
                bindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
            else
                bindingNavigatorCountItem.Text = "of 1";
            DiplayRecurranceSetup();
        }

        private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            bindingNavigatorPositionItem.Text = (Convert.ToInt32(bindingNavigatorPositionItem.Text) + 1).ToString();
            if (TotalRecordCnt < Convert.ToInt32(bindingNavigatorPositionItem.Text))
            {
                if (TotalRecordCnt > 1)
                    bindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                else
                    bindingNavigatorPositionItem.Text = "1";
            }
            if (TotalRecordCnt > 1)
                bindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
            else
                bindingNavigatorCountItem.Text = "of 1";
            DiplayRecurranceSetup();
        }

        private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            if (TotalRecordCnt > 1)
            {
                bindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                bindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
            }
            else
            {
                bindingNavigatorPositionItem.Text = "1";
                bindingNavigatorCountItem.Text = "of 1";
            }
            DiplayRecurranceSetup();
        }

        private void DiplayRecurranceSetup()
        {
            MobjclsBLLRecurrenceSetup.DisplayRecurranceSetup(bindingNavigatorPositionItem.Text.ToInt32());
            
            txtRecurranceType.Tag = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intRecurrenceSetupID;
            txtRecurranceType.Text = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.strRecurrenceType;
            if (MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intRecurringPatternID == 1)
                rdbDaily.Checked = true;
            else if (MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intRecurringPatternID == 2)
                rdbWeekly.Checked = true;
            else if (MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intRecurringPatternID == 3)
                rdbMonthly.Checked = true;

            if (rdbDaily.Checked == true)
            {
                nudDayInterval.Value = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intDayInterval;
                if (MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsDaysDaily == true)
                {
                    rdbIsDaysDaily.Checked = true;
                    rdbIsWeeklyDaily.Checked = false;
                }
                else
                {
                    rdbIsDaysDaily.Checked = false;
                    rdbIsWeeklyDaily.Checked = true;
                }
            }
            else if (rdbWeekly.Checked == true)
            {
                nudWeekInterval.Value = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intWeekInterval;
                chkSunday.Checked = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsSunday;
                chkMonday.Checked = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsMonday;
                chkTuesDay.Checked = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsTuesday;
                chkWednesday.Checked = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsWednesday;
                chkThursday.Checked = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsThursday;
                chkFriday.Checked = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsFriday;
                chkSaturday.Checked = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsSaturday;
            }
            else if (rdbMonthly.Checked == true)
            {
                if (MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.blnIsDaysMonthly == true)
                {
                    rdbIsDaysMOnthly.Checked = true;
                    rdbIsOnMonthly.Checked = false;
                }
                else
                {
                    rdbIsDaysMOnthly.Checked = false;
                    rdbIsOnMonthly.Checked = true;
                }
                if (MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intDayofMonth > 0)
                    nudDayofMonth.Value = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intDayofMonth;
                else
                    nudDayofMonth.Value = 1;
                if (rdbIsDaysMOnthly.Checked == true)
                {
                    nudMonthIntervalIsDays.Value = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intMonthInterval;
                    nudMonthIntervalIsOn.Value = 1;
                }
                else
                {
                    nudMonthIntervalIsDays.Value = 1;
                    nudMonthIntervalIsOn.Value = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intMonthInterval;
                }
                cboWeekDayOrder.SelectedValue = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intWeekDayOrderID;
                cboDays.SelectedValue = MobjclsBLLRecurrenceSetup.PobjClsDTORecurrenceSetup.intDayID;
            }
            BindingEnableDisable();
            bindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnAddUpdatePermission;
            bindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            bindingNavigatorPrint.Enabled = bindingNavigatorEmail.Enabled = MblnPrintEmailPermission;            
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            tmrRecurranceSetup.Enabled = false;
            clearControls();
        }

        private void bindingNavigatorClearItem_Click(object sender, EventArgs e)
        {
            bindingNavigatorAddNewItem_Click(sender, e);
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (this.UserMessage.ShowMessage(13) == true)
            {
                if (MobjclsBLLRecurrenceSetup.DeleteRecurranceSetup() == true)
                {                    
                    LblSStripRecurranceSetup.Text = this.UserMessage.GetMessageByCode(4);
                    tmrRecurranceSetup.Enabled = true;
                    this.UserMessage.ShowMessage(4);
                    clearControls();
                }
            }
        }

        private void tmrRecurranceSetup_Tick(object sender, EventArgs e)
        {
            LblSStripRecurranceSetup.Text = "";
            tmrRecurranceSetup.Enabled = false;
        }

        private void bindingNavigatorPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void bindingNavigatorEmail_Click(object sender, EventArgs e)
        {

        }
        private void LoadReport()
        {
            int intRecurrentID = Convert.ToInt32(txtRecurranceType.Tag);
            if (intRecurrentID > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = intRecurrentID;
                ObjViewer.PintCompany = ClsCommonSettings.CompanyID;
                ObjViewer.PiFormID = (int)FormID.RecurrentSetup;
                ObjViewer.ShowDialog();
            }
        }
    }
}
