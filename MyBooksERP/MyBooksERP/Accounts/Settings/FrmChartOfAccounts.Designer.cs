﻿namespace MyBooksERP
{
    partial class FrmChartOfAccounts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmChartOfAccounts));
            this.StripAccounts = new System.Windows.Forms.ToolStrip();
            this.BtnNew = new System.Windows.Forms.ToolStripButton();
            this.BtnSave = new System.Windows.Forms.ToolStripButton();
            this.BtnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnSummary = new System.Windows.Forms.ToolStripButton();
            this.btnOpeningBalance = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.SsAccountsHead = new System.Windows.Forms.StatusStrip();
            this.LblSStripAccountsHead = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatusMsg = new System.Windows.Forms.ToolStripStatusLabel();
            this.chkCostCenter = new System.Windows.Forms.CheckBox();
            this.TvAccountsHead = new System.Windows.Forms.TreeView();
            this.ImlAccounts = new System.Windows.Forms.ImageList(this.components);
            this.TxtDescription = new System.Windows.Forms.TextBox();
            this.LblShowStatus = new System.Windows.Forms.Label();
            this.LblDescription = new System.Windows.Forms.Label();
            this.TxtShortName = new System.Windows.Forms.TextBox();
            this.LblAccountsProperty = new System.Windows.Forms.Label();
            this.LblShortName = new System.Windows.Forms.Label();
            this.BtnBottomSave = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.ErrAccounts = new System.Windows.Forms.ErrorProvider(this.components);
            this.MnuAddNew = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MnuAddGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.MnuAddAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.MnuDeleteAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.TmAccounts = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lblTaxValue = new System.Windows.Forms.Label();
            this.txtTaxValue = new System.Windows.Forms.TextBox();
            this.rbtnCreateGroup = new System.Windows.Forms.RadioButton();
            this.rbtnCreateLedger = new System.Windows.Forms.RadioButton();
            this.cboGroup = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.LblAccountsHead = new System.Windows.Forms.Label();
            this.StripAccounts.SuspendLayout();
            this.SsAccountsHead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrAccounts)).BeginInit();
            this.MnuAddNew.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // StripAccounts
            // 
            this.StripAccounts.BackColor = System.Drawing.Color.Transparent;
            this.StripAccounts.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnNew,
            this.BtnSave,
            this.BtnDelete,
            this.toolStripSeparator2,
            this.BtnSummary,
            this.btnOpeningBalance,
            this.toolStripSeparator1,
            this.BtnHelp,
            this.BtnEmail});
            this.StripAccounts.Location = new System.Drawing.Point(0, 0);
            this.StripAccounts.Name = "StripAccounts";
            this.StripAccounts.Size = new System.Drawing.Size(744, 25);
            this.StripAccounts.TabIndex = 5;
            this.StripAccounts.Text = "ToolStrip1";
            // 
            // BtnNew
            // 
            this.BtnNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnNew.Image = ((System.Drawing.Image)(resources.GetObject("BtnNew.Image")));
            this.BtnNew.Name = "BtnNew";
            this.BtnNew.RightToLeftAutoMirrorImage = true;
            this.BtnNew.Size = new System.Drawing.Size(23, 22);
            this.BtnNew.Text = "Add new";
            this.BtnNew.Click += new System.EventHandler(this.BtnNew_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnSave.Image = ((System.Drawing.Image)(resources.GetObject("BtnSave.Image")));
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(23, 22);
            this.BtnSave.Text = "Save Data";
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("BtnDelete.Image")));
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.RightToLeftAutoMirrorImage = true;
            this.BtnDelete.Size = new System.Drawing.Size(23, 22);
            this.BtnDelete.Text = "Delete";
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnSummary
            // 
            this.BtnSummary.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnSummary.Image = global::MyBooksERP.Properties.Resources.Summory_Report;
            this.BtnSummary.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnSummary.Name = "BtnSummary";
            this.BtnSummary.Size = new System.Drawing.Size(23, 22);
            this.BtnSummary.Visible = false;
            this.BtnSummary.Click += new System.EventHandler(this.BtnSummary_Click);
            // 
            // btnOpeningBalance
            // 
            this.btnOpeningBalance.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnOpeningBalance.Image = global::MyBooksERP.Properties.Resources.Opening_Stock;
            this.btnOpeningBalance.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOpeningBalance.Name = "btnOpeningBalance";
            this.btnOpeningBalance.Size = new System.Drawing.Size(23, 22);
            this.btnOpeningBalance.Text = "Accounts Opening Balance";
            this.btnOpeningBalance.Click += new System.EventHandler(this.btnOpeningBalance_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Enabled = false;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Visible = false;
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // SsAccountsHead
            // 
            this.SsAccountsHead.BackColor = System.Drawing.Color.Transparent;
            this.SsAccountsHead.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblSStripAccountsHead,
            this.lblStatusMsg});
            this.SsAccountsHead.Location = new System.Drawing.Point(0, 482);
            this.SsAccountsHead.Name = "SsAccountsHead";
            this.SsAccountsHead.Size = new System.Drawing.Size(744, 22);
            this.SsAccountsHead.TabIndex = 15;
            this.SsAccountsHead.Text = "StatusStrip1";
            // 
            // LblSStripAccountsHead
            // 
            this.LblSStripAccountsHead.Name = "LblSStripAccountsHead";
            this.LblSStripAccountsHead.Size = new System.Drawing.Size(45, 17);
            this.LblSStripAccountsHead.Text = "Status: ";
            // 
            // lblStatusMsg
            // 
            this.lblStatusMsg.Name = "lblStatusMsg";
            this.lblStatusMsg.Size = new System.Drawing.Size(0, 17);
            // 
            // chkCostCenter
            // 
            this.chkCostCenter.AutoSize = true;
            this.chkCostCenter.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkCostCenter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCostCenter.Location = new System.Drawing.Point(56, 270);
            this.chkCostCenter.Name = "chkCostCenter";
            this.chkCostCenter.Size = new System.Drawing.Size(163, 17);
            this.chkCostCenter.TabIndex = 5;
            this.chkCostCenter.Text = "Cost Centers are applicable?";
            this.chkCostCenter.UseVisualStyleBackColor = true;
            this.chkCostCenter.CheckedChanged += new System.EventHandler(this.chkCostCenter_CheckedChanged);
            // 
            // TvAccountsHead
            // 
            this.TvAccountsHead.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TvAccountsHead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TvAccountsHead.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TvAccountsHead.HideSelection = false;
            this.TvAccountsHead.ImageIndex = 1;
            this.TvAccountsHead.ImageList = this.ImlAccounts;
            this.TvAccountsHead.Location = new System.Drawing.Point(0, 0);
            this.TvAccountsHead.Name = "TvAccountsHead";
            this.TvAccountsHead.SelectedImageKey = "OpenFolder";
            this.TvAccountsHead.Size = new System.Drawing.Size(382, 401);
            this.TvAccountsHead.TabIndex = 0;
            this.TvAccountsHead.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TvAccountsHead_AfterSelect);
            this.TvAccountsHead.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.TvAccountsHead_NodeMouseClick);
            // 
            // ImlAccounts
            // 
            this.ImlAccounts.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImlAccounts.ImageStream")));
            this.ImlAccounts.TransparentColor = System.Drawing.Color.Transparent;
            this.ImlAccounts.Images.SetKeyName(0, "ExpandAll.png");
            this.ImlAccounts.Images.SetKeyName(1, "Accounts.png");
            // 
            // TxtDescription
            // 
            this.TxtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtDescription.BackColor = System.Drawing.SystemColors.Info;
            this.TxtDescription.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDescription.Location = new System.Drawing.Point(56, 140);
            this.TxtDescription.MaxLength = 30;
            this.TxtDescription.Name = "TxtDescription";
            this.TxtDescription.Size = new System.Drawing.Size(270, 21);
            this.TxtDescription.TabIndex = 2;
            this.TxtDescription.TextChanged += new System.EventHandler(this.ClearErrorProvider);
            // 
            // LblShowStatus
            // 
            this.LblShowStatus.AutoSize = true;
            this.LblShowStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblShowStatus.Location = new System.Drawing.Point(83, 294);
            this.LblShowStatus.Name = "LblShowStatus";
            this.LblShowStatus.Size = new System.Drawing.Size(0, 13);
            this.LblShowStatus.TabIndex = 10;
            // 
            // LblDescription
            // 
            this.LblDescription.AutoSize = true;
            this.LblDescription.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDescription.Location = new System.Drawing.Point(9, 143);
            this.LblDescription.Name = "LblDescription";
            this.LblDescription.Size = new System.Drawing.Size(34, 13);
            this.LblDescription.TabIndex = 3;
            this.LblDescription.Text = "Name";
            // 
            // TxtShortName
            // 
            this.TxtShortName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtShortName.BackColor = System.Drawing.SystemColors.Info;
            this.TxtShortName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShortName.Location = new System.Drawing.Point(56, 113);
            this.TxtShortName.MaxLength = 30;
            this.TxtShortName.Name = "TxtShortName";
            this.TxtShortName.Size = new System.Drawing.Size(270, 21);
            this.TxtShortName.TabIndex = 3;
            this.TxtShortName.TextChanged += new System.EventHandler(this.EnableButtons);
            // 
            // LblAccountsProperty
            // 
            this.LblAccountsProperty.AutoSize = true;
            this.LblAccountsProperty.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAccountsProperty.ForeColor = System.Drawing.Color.DarkGreen;
            this.LblAccountsProperty.Location = new System.Drawing.Point(9, 10);
            this.LblAccountsProperty.Name = "LblAccountsProperty";
            this.LblAccountsProperty.Size = new System.Drawing.Size(132, 13);
            this.LblAccountsProperty.TabIndex = 9;
            this.LblAccountsProperty.Text = "Account Properties of ";
            // 
            // LblShortName
            // 
            this.LblShortName.AutoSize = true;
            this.LblShortName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblShortName.Location = new System.Drawing.Point(9, 116);
            this.LblShortName.Name = "LblShortName";
            this.LblShortName.Size = new System.Drawing.Size(32, 13);
            this.LblShortName.TabIndex = 6;
            this.LblShortName.Text = "Code";
            // 
            // BtnBottomSave
            // 
            this.BtnBottomSave.Location = new System.Drawing.Point(9, 450);
            this.BtnBottomSave.Name = "BtnBottomSave";
            this.BtnBottomSave.Size = new System.Drawing.Size(75, 23);
            this.BtnBottomSave.TabIndex = 0;
            this.BtnBottomSave.Text = "&Save";
            this.BtnBottomSave.UseVisualStyleBackColor = true;
            this.BtnBottomSave.Click += new System.EventHandler(this.BtnBottomSave_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(659, 449);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 2;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(578, 450);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 1;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // ErrAccounts
            // 
            this.ErrAccounts.ContainerControl = this;
            this.ErrAccounts.RightToLeft = true;
            // 
            // MnuAddNew
            // 
            this.MnuAddNew.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MnuAddGroup,
            this.MnuAddAccount,
            this.MnuDeleteAccount});
            this.MnuAddNew.Name = "MnuAddNew";
            this.MnuAddNew.Size = new System.Drawing.Size(145, 70);
            // 
            // MnuAddGroup
            // 
            this.MnuAddGroup.Name = "MnuAddGroup";
            this.MnuAddGroup.Size = new System.Drawing.Size(144, 22);
            this.MnuAddGroup.Text = "Add Group";
            this.MnuAddGroup.Click += new System.EventHandler(this.MnuAddGroup_Click);
            // 
            // MnuAddAccount
            // 
            this.MnuAddAccount.Name = "MnuAddAccount";
            this.MnuAddAccount.Size = new System.Drawing.Size(144, 22);
            this.MnuAddAccount.Text = "Add Account";
            this.MnuAddAccount.Click += new System.EventHandler(this.MnuAddAccount_Click);
            // 
            // MnuDeleteAccount
            // 
            this.MnuDeleteAccount.Name = "MnuDeleteAccount";
            this.MnuDeleteAccount.Size = new System.Drawing.Size(144, 22);
            this.MnuDeleteAccount.Text = "Delete";
            this.MnuDeleteAccount.Click += new System.EventHandler(this.MnuDeleteAccount_Click);
            // 
            // TmAccounts
            // 
            this.TmAccounts.Interval = 3000;
            this.TmAccounts.Tick += new System.EventHandler(this.TmAccounts_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Location = new System.Drawing.Point(9, 41);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lblTaxValue);
            this.splitContainer1.Panel1.Controls.Add(this.txtTaxValue);
            this.splitContainer1.Panel1.Controls.Add(this.rbtnCreateGroup);
            this.splitContainer1.Panel1.Controls.Add(this.rbtnCreateLedger);
            this.splitContainer1.Panel1.Controls.Add(this.cboGroup);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.LblAccountsProperty);
            this.splitContainer1.Panel1.Controls.Add(this.chkCostCenter);
            this.splitContainer1.Panel1.Controls.Add(this.LblDescription);
            this.splitContainer1.Panel1.Controls.Add(this.TxtDescription);
            this.splitContainer1.Panel1.Controls.Add(this.LblShortName);
            this.splitContainer1.Panel1.Controls.Add(this.TxtShortName);
            this.splitContainer1.Panel1.Controls.Add(this.shapeContainer1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.LblShowStatus);
            this.splitContainer1.Panel2.Controls.Add(this.TvAccountsHead);
            this.splitContainer1.Size = new System.Drawing.Size(726, 403);
            this.splitContainer1.SplitterDistance = 338;
            this.splitContainer1.TabIndex = 21;
            // 
            // lblTaxValue
            // 
            this.lblTaxValue.AutoSize = true;
            this.lblTaxValue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaxValue.Location = new System.Drawing.Point(6, 300);
            this.lblTaxValue.Name = "lblTaxValue";
            this.lblTaxValue.Size = new System.Drawing.Size(82, 13);
            this.lblTaxValue.TabIndex = 18;
            this.lblTaxValue.Text = "Tax Value ( % )";
            this.lblTaxValue.Visible = false;
            // 
            // txtTaxValue
            // 
            this.txtTaxValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTaxValue.BackColor = System.Drawing.SystemColors.Info;
            this.txtTaxValue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxValue.Location = new System.Drawing.Point(92, 297);
            this.txtTaxValue.MaxLength = 30;
            this.txtTaxValue.Name = "txtTaxValue";
            this.txtTaxValue.Size = new System.Drawing.Size(127, 21);
            this.txtTaxValue.TabIndex = 17;
            this.txtTaxValue.Visible = false;
            this.txtTaxValue.TextChanged += new System.EventHandler(this.ClearErrorProvider);
            this.txtTaxValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTaxValue_KeyPress);
            // 
            // rbtnCreateGroup
            // 
            this.rbtnCreateGroup.AutoSize = true;
            this.rbtnCreateGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnCreateGroup.Location = new System.Drawing.Point(156, 73);
            this.rbtnCreateGroup.Name = "rbtnCreateGroup";
            this.rbtnCreateGroup.Size = new System.Drawing.Size(90, 17);
            this.rbtnCreateGroup.TabIndex = 1;
            this.rbtnCreateGroup.Text = "Create Group";
            this.rbtnCreateGroup.UseVisualStyleBackColor = true;
            this.rbtnCreateGroup.CheckedChanged += new System.EventHandler(this.rbtnCreateGroup_CheckedChanged);
            // 
            // rbtnCreateLedger
            // 
            this.rbtnCreateLedger.AutoSize = true;
            this.rbtnCreateLedger.Checked = true;
            this.rbtnCreateLedger.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnCreateLedger.Location = new System.Drawing.Point(56, 73);
            this.rbtnCreateLedger.Name = "rbtnCreateLedger";
            this.rbtnCreateLedger.Size = new System.Drawing.Size(94, 17);
            this.rbtnCreateLedger.TabIndex = 0;
            this.rbtnCreateLedger.TabStop = true;
            this.rbtnCreateLedger.Text = "Create Ledger";
            this.rbtnCreateLedger.UseVisualStyleBackColor = true;
            this.rbtnCreateLedger.CheckedChanged += new System.EventHandler(this.rbtnCreateLedger_CheckedChanged);
            // 
            // cboGroup
            // 
            this.cboGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboGroup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboGroup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboGroup.BackColor = System.Drawing.SystemColors.Info;
            this.cboGroup.DropDownHeight = 120;
            this.cboGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboGroup.FormattingEnabled = true;
            this.cboGroup.IntegralHeight = false;
            this.cboGroup.Location = new System.Drawing.Point(56, 225);
            this.cboGroup.Name = "cboGroup";
            this.cboGroup.Size = new System.Drawing.Size(270, 21);
            this.cboGroup.TabIndex = 4;
            this.cboGroup.SelectedIndexChanged += new System.EventHandler(this.cboGroup_SelectedIndexChanged);
            this.cboGroup.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboGroup_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 228);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Under";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2});
            this.shapeContainer1.Size = new System.Drawing.Size(336, 401);
            this.shapeContainer1.TabIndex = 16;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lineShape2.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 0;
            this.lineShape2.X2 = 340;
            this.lineShape2.Y1 = 198;
            this.lineShape2.Y2 = 198;
            // 
            // LblAccountsHead
            // 
            this.LblAccountsHead.AutoSize = true;
            this.LblAccountsHead.BackColor = System.Drawing.Color.Transparent;
            this.LblAccountsHead.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAccountsHead.ForeColor = System.Drawing.Color.DarkGreen;
            this.LblAccountsHead.Location = new System.Drawing.Point(348, 26);
            this.LblAccountsHead.Name = "LblAccountsHead";
            this.LblAccountsHead.Size = new System.Drawing.Size(117, 13);
            this.LblAccountsHead.TabIndex = 22;
            this.LblAccountsHead.Text = "Accounts Hierarchy";
            // 
            // FrmChartOfAccounts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 504);
            this.Controls.Add(this.LblAccountsHead);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.BtnBottomSave);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.SsAccountsHead);
            this.Controls.Add(this.StripAccounts);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmChartOfAccounts";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chart of Accounts";
            this.Load += new System.EventHandler(this.FrmChartOfAccounts_Load);
            this.StripAccounts.ResumeLayout(false);
            this.StripAccounts.PerformLayout();
            this.SsAccountsHead.ResumeLayout(false);
            this.SsAccountsHead.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrAccounts)).EndInit();
            this.MnuAddNew.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolStrip StripAccounts;
        internal System.Windows.Forms.ToolStripButton BtnNew;
        internal System.Windows.Forms.ToolStripButton BtnSave;
        internal System.Windows.Forms.ToolStripButton BtnDelete;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.StatusStrip SsAccountsHead;
        internal System.Windows.Forms.ToolStripStatusLabel LblSStripAccountsHead;
        internal System.Windows.Forms.TreeView TvAccountsHead;
        internal System.Windows.Forms.TextBox TxtDescription;
        internal System.Windows.Forms.Label LblShowStatus;
        internal System.Windows.Forms.Label LblDescription;
        internal System.Windows.Forms.TextBox TxtShortName;
        internal System.Windows.Forms.Label LblAccountsProperty;
        internal System.Windows.Forms.Label LblShortName;
        internal System.Windows.Forms.Button BtnBottomSave;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.ToolStripButton BtnSummary;
        private System.Windows.Forms.ErrorProvider ErrAccounts;
        private System.Windows.Forms.ImageList ImlAccounts;
        private System.Windows.Forms.ToolStripMenuItem MnuAddGroup;
        private System.Windows.Forms.ToolStripMenuItem MnuAddAccount;
        private System.Windows.Forms.ToolStripMenuItem MnuDeleteAccount;
        internal System.Windows.Forms.ContextMenuStrip MnuAddNew;
        private System.Windows.Forms.Timer TmAccounts;
        private System.Windows.Forms.ToolStripButton BtnEmail;
        private System.Windows.Forms.CheckBox chkCostCenter;
        private System.Windows.Forms.ToolStripStatusLabel lblStatusMsg;
        private System.Windows.Forms.ToolStripButton btnOpeningBalance;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RadioButton rbtnCreateGroup;
        private System.Windows.Forms.RadioButton rbtnCreateLedger;
        private System.Windows.Forms.ComboBox cboGroup;
        private System.Windows.Forms.Label label1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        internal System.Windows.Forms.Label LblAccountsHead;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        internal System.Windows.Forms.Label lblTaxValue;
        internal System.Windows.Forms.TextBox txtTaxValue;
    }
}