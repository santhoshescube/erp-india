﻿namespace MyBooksERP
{
    partial class frmAccountsFormReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource4 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource5 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource6 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource7 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource8 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAccountsFormReport));
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.chkQtywise = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.lblVendors = new DevComponents.DotNetBar.LabelX();
            this.cboVendors = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblFromDate = new DevComponents.DotNetBar.LabelX();
            this.lblToDate = new DevComponents.DotNetBar.LabelX();
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.reportViewer2 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.reportViewer3 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.reportViewer4 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.panelEx1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.chkQtywise);
            this.panelEx1.Controls.Add(this.lblVendors);
            this.panelEx1.Controls.Add(this.cboVendors);
            this.panelEx1.Controls.Add(this.lblFromDate);
            this.panelEx1.Controls.Add(this.lblToDate);
            this.panelEx1.Controls.Add(this.BtnShow);
            this.panelEx1.Controls.Add(this.dtpToDate);
            this.panelEx1.Controls.Add(this.dtpFromDate);
            this.panelEx1.Controls.Add(this.lblCompany);
            this.panelEx1.Controls.Add(this.cboCompany);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEx1.Location = new System.Drawing.Point(0, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(1238, 64);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 1;
            // 
            // chkQtywise
            // 
            // 
            // 
            // 
            this.chkQtywise.BackgroundStyle.Class = "";
            this.chkQtywise.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkQtywise.Location = new System.Drawing.Point(360, 34);
            this.chkQtywise.Name = "chkQtywise";
            this.chkQtywise.Size = new System.Drawing.Size(123, 23);
            this.chkQtywise.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkQtywise.TabIndex = 64;
            this.chkQtywise.Text = "Item Quantitywise";
            this.chkQtywise.Visible = false;
            // 
            // lblVendors
            // 
            // 
            // 
            // 
            this.lblVendors.BackgroundStyle.Class = "";
            this.lblVendors.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblVendors.Location = new System.Drawing.Point(16, 36);
            this.lblVendors.Name = "lblVendors";
            this.lblVendors.Size = new System.Drawing.Size(53, 21);
            this.lblVendors.TabIndex = 63;
            this.lblVendors.Text = "Vendor";
            this.lblVendors.Visible = false;
            // 
            // cboVendors
            // 
            this.cboVendors.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboVendors.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboVendors.DisplayMember = "Text";
            this.cboVendors.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboVendors.FormattingEnabled = true;
            this.cboVendors.ItemHeight = 14;
            this.cboVendors.Location = new System.Drawing.Point(75, 36);
            this.cboVendors.Name = "cboVendors";
            this.cboVendors.Size = new System.Drawing.Size(260, 20);
            this.cboVendors.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboVendors.TabIndex = 62;
            this.cboVendors.Visible = false;
            this.cboVendors.SelectedIndexChanged += new System.EventHandler(this.cboVendors_SelectedIndexChanged);
            this.cboVendors.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCompany_KeyDown);
            // 
            // lblFromDate
            // 
            // 
            // 
            // 
            this.lblFromDate.BackgroundStyle.Class = "";
            this.lblFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFromDate.Location = new System.Drawing.Point(360, 10);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(31, 21);
            this.lblFromDate.TabIndex = 61;
            this.lblFromDate.Text = "From";
            // 
            // lblToDate
            // 
            // 
            // 
            // 
            this.lblToDate.BackgroundStyle.Class = "";
            this.lblToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblToDate.Location = new System.Drawing.Point(520, 10);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(23, 20);
            this.lblToDate.TabIndex = 60;
            this.lblToDate.Text = "To";
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(580, 37);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(75, 20);
            this.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnShow.TabIndex = 59;
            this.BtnShow.Text = "Show";
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(549, 11);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(106, 20);
            this.dtpToDate.TabIndex = 58;
            this.dtpToDate.ValueChanged += new System.EventHandler(this.dtpToDate_ValueChanged);
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(397, 10);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(106, 20);
            this.dtpFromDate.TabIndex = 57;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            // 
            // lblCompany
            // 
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(16, 10);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(53, 21);
            this.lblCompany.TabIndex = 1;
            this.lblCompany.Text = "Company";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(75, 10);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(260, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 0;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCompany_KeyDown);
            // 
            // ReportViewer1
            // 
            this.ReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DtSetSettingsFormReport_STPayment";
            reportDataSource1.Value = null;
            reportDataSource2.Name = "DtSetCompanyFormReport_CompanyHeader";
            reportDataSource2.Value = null;
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.ReportViewer1.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.MainReports.RptMISPaymentReceipt.rdlc";
            this.ReportViewer1.Location = new System.Drawing.Point(0, 64);
            this.ReportViewer1.Name = "ReportViewer1";
            this.ReportViewer1.Size = new System.Drawing.Size(1238, 437);
            this.ReportViewer1.TabIndex = 8;
            // 
            // reportViewer2
            // 
            this.reportViewer2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reportViewer2.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource3.Name = "DtSetSettingsFormReport_STPayment";
            reportDataSource3.Value = null;
            reportDataSource4.Name = "DtSetCompanyFormReport_CompanyHeader";
            reportDataSource4.Value = null;
            this.reportViewer2.LocalReport.DataSources.Add(reportDataSource3);
            this.reportViewer2.LocalReport.DataSources.Add(reportDataSource4);
            this.reportViewer2.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.MainReports.RptMISPaymentReceipt.rdlc";
            this.reportViewer2.Location = new System.Drawing.Point(0, 64);
            this.reportViewer2.Name = "reportViewer2";
            this.reportViewer2.Size = new System.Drawing.Size(1238, 437);
            this.reportViewer2.TabIndex = 9;
            // 
            // reportViewer3
            // 
            this.reportViewer3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reportViewer3.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource5.Name = "DtSetSettingsFormReport_STPayment";
            reportDataSource5.Value = null;
            reportDataSource6.Name = "DtSetCompanyFormReport_CompanyHeader";
            reportDataSource6.Value = null;
            this.reportViewer3.LocalReport.DataSources.Add(reportDataSource5);
            this.reportViewer3.LocalReport.DataSources.Add(reportDataSource6);
            this.reportViewer3.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.MainReports.RptMISPaymentReceipt.rdlc";
            this.reportViewer3.Location = new System.Drawing.Point(0, 64);
            this.reportViewer3.Name = "reportViewer3";
            this.reportViewer3.Size = new System.Drawing.Size(1238, 437);
            this.reportViewer3.TabIndex = 10;
            // 
            // reportViewer4
            // 
            this.reportViewer4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reportViewer4.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource7.Name = "DtSetSettingsFormReport_STPayment";
            reportDataSource7.Value = null;
            reportDataSource8.Name = "DtSetCompanyFormReport_CompanyHeader";
            reportDataSource8.Value = null;
            this.reportViewer4.LocalReport.DataSources.Add(reportDataSource7);
            this.reportViewer4.LocalReport.DataSources.Add(reportDataSource8);
            this.reportViewer4.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.MainReports.RptMISPaymentReceipt.rdlc";
            this.reportViewer4.Location = new System.Drawing.Point(0, 64);
            this.reportViewer4.Name = "reportViewer4";
            this.reportViewer4.Size = new System.Drawing.Size(1238, 437);
            this.reportViewer4.TabIndex = 11;
            // 
            // frmAccountsFormReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1238, 501);
            this.Controls.Add(this.reportViewer4);
            this.Controls.Add(this.reportViewer3);
            this.Controls.Add(this.reportViewer2);
            this.Controls.Add(this.ReportViewer1);
            this.Controls.Add(this.panelEx1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAccountsFormReport";
            this.Text = "frmAccountsFormReport";
            this.Load += new System.EventHandler(this.frmAccountsFormReport_Load);
            this.panelEx1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.ButtonX BtnShow;
        internal System.Windows.Forms.DateTimePicker dtpFromDate;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private Microsoft.Reporting.WinForms.ReportViewer ReportViewer1;
        private DevComponents.DotNetBar.LabelX lblFromDate;
        private DevComponents.DotNetBar.LabelX lblToDate;
        internal System.Windows.Forms.DateTimePicker dtpToDate;
        private DevComponents.DotNetBar.LabelX lblVendors;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboVendors;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer2;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkQtywise;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer3;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer4;
    }
}