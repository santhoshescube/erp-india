﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Reporting.WinForms;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
namespace MyBooksERP
{
    /***********************************************************
    * Author       : Junaid
    * Created On   : 24 APRL 2012
    * Purpose      : Create Accounts Form Report 
    * Modified     :
    * Modified Date:               
    * **********************************************************/
    public partial class frmAccountsFormReport : DevComponents.DotNetBar.Office2007Form
    {
        public string strAccHeader;
        string PsReportFooter;
        public string strPeriod;
        int PiScale;
        public DataSet  CompanyHeader;
        private string MsReportPath;
        private clsMessage objUserMessage = null;
        private clsBLLAccountsFormReport objclsBLLAccountsFormReport = null;
        public eMenuID TempMenuID { get; set; }
        public FormID TempFormID { get; set; }
        clsBLLAccountSummary MobjclsBLLAccountSummary = new clsBLLAccountSummary();

        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.AccountsFormReport );
                return this.objUserMessage;
            }
        }

        public frmAccountsFormReport()
        {
            InitializeComponent();
            PsReportFooter = ClsCommonSettings.ReportFooter;
            PiScale = ClsCommonSettings.Scale;
            objclsBLLAccountsFormReport = new clsBLLAccountsFormReport();
        }

        private void LoadReport()
        {
            this.Cursor = Cursors.WaitCursor;

            if (TempMenuID == eMenuID.StockSummaryItemwise)
            {                
                this.reportViewer2.ProcessingMode = ProcessingMode.Local;
                this.reportViewer2.Clear();
                ReportViewer1.Visible = false;
                reportViewer2.Visible = true;
                reportViewer3.Visible = false;
                reportViewer4.Visible = false;
            }
            else if (TempMenuID == eMenuID.StockSummaryItemQtywise)
            {
                this.reportViewer3.ProcessingMode = ProcessingMode.Local;
                this.reportViewer3.Clear();
                ReportViewer1.Visible = false;
                reportViewer2.Visible = false;
                reportViewer3.Visible = true;
                reportViewer4.Visible = false;
            }
            else if (TempMenuID == eMenuID.StockLedger)
            {
                this.reportViewer4.ProcessingMode = ProcessingMode.Local;
                this.reportViewer4.Clear();
                ReportViewer1.Visible = false;
                reportViewer2.Visible = false;
                reportViewer3.Visible = false;
                reportViewer4.Visible = true;
            }
            else
            {
                this.ReportViewer1.ProcessingMode = ProcessingMode.Local;
                this.ReportViewer1.Clear();
                ReportViewer1.Visible = true;
                reportViewer2.Visible = false;
                reportViewer3.Visible = false;
                reportViewer4.Visible = false;
            }

            switch ((int)TempMenuID)
            {
                case (int)eMenuID.BalanceSheet:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptAccBalanceSheet.rdlc";
                        this.ReportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Period", strPeriod, true );
                        this.ReportViewer1.LocalReport.SetParameters(ReportParameter);
                        CompanyHeader = objclsBLLAccountsFormReport.DisplayCompanyHeader();

                        this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.ReportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = objclsBLLAccountsFormReport.DisplayBalanceSheet();

                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", CompanyHeader.Tables[0]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_BalanceSheet_Liabilities", dtSet.Tables[0]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_BalanceSheet_Asset", dtSet.Tables[1]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_BalanceSheet_GrandTotal", dtSet.Tables[2]));
                        break;
                    }
                case (int)eMenuID.ProfitAndLossAccount:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptAccProfitAndLossAccounts.rdlc";
                        this.ReportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Period", strPeriod, true);
                        this.ReportViewer1.LocalReport.SetParameters(ReportParameter);
                        this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportProcessingEventHandler);

                        this.ReportViewer1.LocalReport.DataSources.Clear();
                        CompanyHeader = objclsBLLAccountsFormReport.DisplayCompanyHeader();
                        DataSet dtSet = objclsBLLAccountsFormReport.DisplayProfitAndLossAccounts();
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", CompanyHeader.Tables[0]));

                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_Trading_Dr", dtSet.Tables[0]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_Trading_Cr", dtSet.Tables[1]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_ProfitAndLossAccouts_Dr", dtSet.Tables[2]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_ProfitAndLossAccouts_Cr", dtSet.Tables[3]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_ProfitAndLossAccouts_GrandTotal", dtSet.Tables[4]));
                        break;
                    }
                case (int)eMenuID.CashFlow:
                    {
                        strAccHeader = "Cash Flow";
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptAccCashAndFundFlow.rdlc";
                        this.ReportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[3];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Period", strPeriod, true);
                        ReportParameter[2] = new ReportParameter("Header", strAccHeader, true);
                        this.ReportViewer1.LocalReport.SetParameters(ReportParameter);
                        CompanyHeader = objclsBLLAccountsFormReport.DisplayCompanyHeader();

                        this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportProcessingEventHandler);

                        this.ReportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = objclsBLLAccountsFormReport.DisplayCashFlow();
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", CompanyHeader.Tables[0]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_CashFlow_InFlow", dtSet.Tables[0]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_CashFlow_OutFlow", dtSet.Tables[1]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_CashFlow_Total", dtSet.Tables[2]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_CashFlow_Net", dtSet.Tables[3]));
                        break;
                    }
                case (int)eMenuID.StockSummary:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptAccStockSummary.rdlc";
                        this.ReportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[3];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Item", cboVendors.Text, true);
                        ReportParameter[2] = new ReportParameter("Period", strPeriod, true);
                        this.ReportViewer1.LocalReport.SetParameters(ReportParameter);
                        CompanyHeader = objclsBLLAccountsFormReport.DisplayCompanyHeader();
                        this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.ReportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = objclsBLLAccountsFormReport.DisplayStockSummary();

                        DataTable datTemp = new DataTable();
                        datTemp.Columns.Add("AccDate");
                        datTemp.Columns.Add("ReferenceNo");
                        datTemp.Columns.Add("Description");
                        datTemp.Columns.Add("Inwards");
                        datTemp.Columns.Add("Outwards");
                        datTemp.Columns.Add("ClosingStock");

                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", CompanyHeader.Tables[0]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_StockSammary", dtSet.Tables[0]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_StockSammary_Totals", dtSet.Tables[1]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_StockSammaryItemwise", datTemp));
                        break;
                    }
                case (int)eMenuID.StockSummaryItemwise:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptAccStockSummaryItemwise.rdlc";
                        this.reportViewer2.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[3];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Item", cboVendors.Text, true);
                        ReportParameter[2] = new ReportParameter("Period", strPeriod, true);
                        this.reportViewer2.LocalReport.SetParameters(ReportParameter);
                        CompanyHeader = objclsBLLAccountsFormReport.DisplayCompanyHeader();
                        this.reportViewer2.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.reportViewer2.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.reportViewer2.LocalReport.DataSources.Clear();

                        DataSet dtSet = objclsBLLAccountsFormReport.DisplayStockSummaryItemwise();
                        DataTable datDetails = dtSet.Tables[0];
                        DataTable datSummary = new DataTable();

                        if (cboVendors.SelectedIndex > -1)
                        {
                            datDetails.DefaultView.RowFilter = "ItemID IN (0," + cboVendors.SelectedValue.ToInt32() + ")";

                            DataTable datTemp = new DataTable();
                            datTemp.Columns.Add("AccDate");
                            datTemp.Columns.Add("ReferenceNo");
                            datTemp.Columns.Add("Description");
                            datTemp.Columns.Add("Inwards");
                            datTemp.Columns.Add("Outwards");
                            datTemp.Columns.Add("ClosingStock");

                            DataTable datTempDetails = datDetails.DefaultView.ToTable();
                            decimal decBalance = 0, decTotalInwardBalance = 0, decTotalOutwardBalance = 0, decTotalBalance = 0;

                            for (int iCounter = 0; iCounter <= datTempDetails.Rows.Count - 1; iCounter++)
                            {
                                datTemp.Rows.Add();
                                datTemp.Rows[datTemp.Rows.Count - 1]["AccDate"] = datTempDetails.Rows[iCounter]["AccDate"];
                                datTemp.Rows[datTemp.Rows.Count - 1]["ReferenceNo"] = datTempDetails.Rows[iCounter]["VoucherNo"];
                                datTemp.Rows[datTemp.Rows.Count - 1]["Description"] = datTempDetails.Rows[iCounter]["Particulars"];

                                if (datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.GRN ||
                                    datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.PurchaseInvoice ||
                                    datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.CreditNote ||
                                    datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.OpeningStock ||
                                    datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.MaterialReturn)
                                {
                                    datTemp.Rows[datTemp.Rows.Count - 1]["Inwards"] = datTempDetails.Rows[iCounter]["StockValue"];
                                    decTotalInwardBalance += datTempDetails.Rows[iCounter]["StockValue"].ToDecimal();
                                }
                                else if (datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.DebitNote ||
                                    datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.SalesInvoice ||
                                    datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.POS ||
                                    datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.MaterialIssue)
                                {
                                    datTemp.Rows[datTemp.Rows.Count - 1]["Outwards"] = datTempDetails.Rows[iCounter]["StockValue"];
                                    decTotalOutwardBalance += datTempDetails.Rows[iCounter]["StockValue"].ToDecimal();
                                }

                                decBalance = datTemp.Rows[datTemp.Rows.Count - 1]["Inwards"].ToDecimal() -
                                    datTemp.Rows[datTemp.Rows.Count - 1]["Outwards"].ToDecimal();

                                if (datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.OpeningStock)
                                    datTemp.Rows[datTemp.Rows.Count - 1]["Inwards"] = "";

                                datTemp.Rows[datTemp.Rows.Count - 1]["ClosingStock"] = (Math.Abs(decTotalBalance + decBalance)).ToStringCustom();
                                decTotalBalance += decBalance;
                            }

                            DataTable datTempSummary = new DataTable();
                            datTempSummary.Columns.Add("Particulars");
                            datTempSummary.Columns.Add("OpStockValue");
                            datTempSummary.Columns.Add("InStockValue");
                            datTempSummary.Columns.Add("OutStockValue");
                            datTempSummary.Columns.Add("ClStockValue");

                            datTempSummary.Rows.Add();
                            datTempSummary.Rows[0]["Particulars"] = "Total";
                            datTempSummary.Rows[0]["OpStockValue"] = "";
                            datTempSummary.Rows[0]["InStockValue"] = decTotalInwardBalance;
                            datTempSummary.Rows[0]["OutStockValue"] = decTotalOutwardBalance;
                            datTempSummary.Rows[0]["ClStockValue"] = decTotalBalance;

                            datDetails = datTemp;
                            datSummary = datTempSummary;
                        }

                        this.reportViewer2.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", CompanyHeader.Tables[0]));
                        this.reportViewer2.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_StockSammary", dtSet.Tables[0]));
                        this.reportViewer2.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_StockSammaryItemwise", datDetails));
                        this.reportViewer2.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_StockSammary_Totals", datSummary));
                        break;
                    }
                case (int)eMenuID.StockSummaryItemQtywise:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptAccStockSummaryItemQtywise.rdlc";
                        this.reportViewer3.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[3];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Item", cboVendors.Text, true);
                        ReportParameter[2] = new ReportParameter("Period", strPeriod, true);
                        this.reportViewer3.LocalReport.SetParameters(ReportParameter);
                        CompanyHeader = objclsBLLAccountsFormReport.DisplayCompanyHeader();
                        this.reportViewer3.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.reportViewer3.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.reportViewer3.LocalReport.DataSources.Clear();

                        DataSet dtSet = objclsBLLAccountsFormReport.DisplayStockSummaryQtywise();
                        DataTable datDetails = dtSet.Tables[0];
                        DataTable datSummary = new DataTable();

                        if (cboVendors.SelectedIndex > -1)
                        {
                            datDetails.DefaultView.RowFilter = "ItemID IN (0," + cboVendors.SelectedValue.ToInt32() + ")";

                            DataTable datTemp = new DataTable();
                            datTemp.Columns.Add("AccDate");
                            datTemp.Columns.Add("ReferenceNo");
                            datTemp.Columns.Add("Description");
                            datTemp.Columns.Add("Inwards");
                            datTemp.Columns.Add("Outwards");
                            datTemp.Columns.Add("ClosingStock");

                            DataTable datTempDetails = datDetails.DefaultView.ToTable();
                            decimal decBalance = 0, decTotalInwardBalance = 0, decTotalOutwardBalance = 0, decTotalBalance = 0;

                            for (int iCounter = 0; iCounter <= datTempDetails.Rows.Count - 1; iCounter++)
                            {
                                datTemp.Rows.Add();
                                datTemp.Rows[datTemp.Rows.Count - 1]["AccDate"] = datTempDetails.Rows[iCounter]["AccDate"];
                                datTemp.Rows[datTemp.Rows.Count - 1]["ReferenceNo"] = datTempDetails.Rows[iCounter]["VoucherNo"];
                                datTemp.Rows[datTemp.Rows.Count - 1]["Description"] = datTempDetails.Rows[iCounter]["Particulars"];

                                if (datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.GRN ||
                                    datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.PurchaseInvoice ||
                                    datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.CreditNote ||
                                    datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.OpeningStock ||
                                    datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.MaterialReturn)
                                {
                                    datTemp.Rows[datTemp.Rows.Count - 1]["Inwards"] = datTempDetails.Rows[iCounter]["StockValue"];
                                    decTotalInwardBalance += datTempDetails.Rows[iCounter]["StockValue"].ToDecimal();
                                }
                                else if (datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.DebitNote ||
                                    datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.SalesInvoice ||
                                    datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.POS ||
                                    datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.MaterialIssue)
                                {
                                    datTemp.Rows[datTemp.Rows.Count - 1]["Outwards"] = datTempDetails.Rows[iCounter]["StockValue"];
                                    decTotalOutwardBalance += datTempDetails.Rows[iCounter]["StockValue"].ToDecimal();
                                }

                                decBalance = datTemp.Rows[datTemp.Rows.Count - 1]["Inwards"].ToDecimal() -
                                    datTemp.Rows[datTemp.Rows.Count - 1]["Outwards"].ToDecimal();

                                if (datTempDetails.Rows[iCounter]["OperationTypeID"].ToInt32() == (int)OperationType.OpeningStock)
                                    datTemp.Rows[datTemp.Rows.Count - 1]["Inwards"] = "";

                                datTemp.Rows[datTemp.Rows.Count - 1]["ClosingStock"] = (Math.Abs(decTotalBalance + decBalance)).ToStringCustom();
                                decTotalBalance += decBalance;
                            }

                            DataTable datTempSummary = new DataTable();
                            datTempSummary.Columns.Add("Particulars");
                            datTempSummary.Columns.Add("OpStockValue");
                            datTempSummary.Columns.Add("InStockValue");
                            datTempSummary.Columns.Add("OutStockValue");
                            datTempSummary.Columns.Add("ClStockValue");

                            datTempSummary.Rows.Add();
                            datTempSummary.Rows[0]["Particulars"] = "Total";
                            datTempSummary.Rows[0]["OpStockValue"] = "";
                            datTempSummary.Rows[0]["InStockValue"] = decTotalInwardBalance;
                            datTempSummary.Rows[0]["OutStockValue"] = decTotalOutwardBalance;
                            datTempSummary.Rows[0]["ClStockValue"] = decTotalBalance;

                            datDetails = datTemp;
                            datSummary = datTempSummary;
                        }

                        this.reportViewer3.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", CompanyHeader.Tables[0]));
                        this.reportViewer3.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_StockSammary", dtSet.Tables[0]));
                        this.reportViewer3.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_StockSammaryItemwise", datDetails));
                        this.reportViewer3.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_StockSammary_Totals", datSummary));
                        break;
                    }
                case (int)eMenuID.FundFlow:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptAccFundFlow.rdlc";
                        this.ReportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Period", strPeriod, true);
                        this.ReportViewer1.LocalReport.SetParameters(ReportParameter);
                        CompanyHeader = objclsBLLAccountsFormReport.DisplayCompanyHeader();

                        this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.ReportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = objclsBLLAccountsFormReport.DisplayFundFlow();
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", CompanyHeader.Tables[0]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_FundFlow_Sources", dtSet.Tables[0]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_FundFlow_Application", dtSet.Tables[1]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_FundFlow_GrandTotal", dtSet.Tables[2]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_FundFlow_WorkingCapital", dtSet.Tables[3]));
                        break;
                    }
                case (int)eMenuID.IncomeStatement:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptAccIncomeStatement.rdlc";
                        this.ReportViewer1.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Period", strPeriod, true);
                        this.ReportViewer1.LocalReport.SetParameters(ReportParameter);
                        CompanyHeader = objclsBLLAccountsFormReport.DisplayCompanyHeader();

                        this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.ReportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSet = objclsBLLAccountsFormReport.DisplayIncomeStatement();
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", CompanyHeader.Tables[0]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_IncomeStatement_TR_Dr", dtSet.Tables[0]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_IncomeStatement_TR_Cr", dtSet.Tables[1]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_IncomeStatement_Gross", dtSet.Tables[2]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_IncomeStatement_PL_Dr", dtSet.Tables[3]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_IncomeStatement_PL_Cr", dtSet.Tables[4]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountsFormReport_IncomeStatement_Net", dtSet.Tables[5]));
                        break;
                    }
                case (int)eMenuID.StatementofAccounts:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptAccStatementofAccounts.rdlc";
                        ReportViewer1.LocalReport.ReportPath = MsReportPath;
                        
                        ReportViewer1.LocalReport.DataSources.Clear();
                        DataSet dtSetCompany = objclsBLLAccountsFormReport.DisplayCompanyHeader();
                        ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtSetCompany.Tables[0]));

                        DataSet dtSet = objclsBLLAccountsFormReport.DisplayStatementofAccounts();
                        DataTable datOpening = dtSet.Tables[1];
                        try
                        {
                            datOpening.Rows.RemoveAt(1);
                            datOpening.Rows.RemoveAt(1);
                        }
                        catch { }
                        ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountSummary_GeneralLedger_ClosingBalance", datOpening));
                                                
                        DataTable datDetails = dtSet.Tables[0];
                        datDetails.Columns.Add("Balance");
                        decimal decBalance = 0, decTotalBalance = 0;
                        string strBalance = "", strAmountInWords = "";

                        try
                        {
                            if (datOpening.Rows.Count > 0)
                            {
                                DataRow dr = datDetails.NewRow();
                                dr["Particulars"] = "Opening Balance As On " + dtpFromDate.Value.ToString("dd MMM yyyy");
                                dr["DebitAmount"] = datOpening.Rows[0]["DebitAmount"];
                                dr["CreditAmount"] = datOpening.Rows[0]["CreditAmount"];
                                datDetails.Rows.InsertAt(dr, 0);

                                decBalance = datDetails.Rows[0]["DebitAmount"].ToDecimal() - datDetails.Rows[0]["CreditAmount"].ToDecimal();

                                if (decTotalBalance + decBalance > 0)
                                    datDetails.Rows[0]["Balance"] = (decTotalBalance + decBalance).ToStringCustom() + " Dr";
                                else
                                    datDetails.Rows[0]["Balance"] = (Math.Abs(decTotalBalance + decBalance)).ToStringCustom() + " Cr";

                                decTotalBalance += decBalance;
                            }
                        }
                        catch { }

                        for (int iCounter = 1; iCounter <= datDetails.Rows.Count - 1; iCounter++)
                        {
                            decBalance = datDetails.Rows[iCounter]["DebitAmount"].ToDecimal() - datDetails.Rows[iCounter]["CreditAmount"].ToDecimal();

                            if (decTotalBalance + decBalance > 0)
                                datDetails.Rows[iCounter]["Balance"] = (decTotalBalance + decBalance).ToStringCustom() + " Dr";                                
                            else
                                datDetails.Rows[iCounter]["Balance"] = (Math.Abs(decTotalBalance + decBalance)).ToStringCustom() + " Cr";                                

                            decTotalBalance += decBalance;
                        }

                        try
                        {
                            if (datDetails.Rows.Count > 0)
                            {
                                strBalance = (Math.Abs(decTotalBalance)).ToStringCustom();
                                strAmountInWords = new clsBLLCommonUtility().ConvertToWord(strBalance, ClsCommonSettings.CurrencyID);

                                if (decTotalBalance > 0)
                                {                                    
                                    strAmountInWords += " (Dr)";
                                    strBalance += " Dr";
                                }
                                else
                                {
                                    strAmountInWords += " (Cr)";
                                    strBalance += " Cr";
                                }
                            }
                        }
                        catch { }

                        ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_AccountSummary_GeneralLedger", datDetails));

                        ReportParameter[] ReportParameter = new ReportParameter[7];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Vendor", cboVendors.Text, true);
                        ReportParameter[2] = new ReportParameter("StatementDate", ClsCommonSettings.GetServerDate().ToString("dd MMM yyyy"), true);
                        ReportParameter[3] = new ReportParameter("Period", dtpFromDate.Value.ToString("dd MMM yyyy") + " - " + dtpToDate.Value.ToString("dd MMM yyyy"), true);
                        ReportParameter[4] = new ReportParameter("FromDate", dtpFromDate.Value.ToString("dd MMM yyyy"), true);
    
                        if (datDetails.Rows.Count > 0)
                        {
                            ReportParameter[5] = new ReportParameter("FooterValue", strBalance, true);
                            ReportParameter[6] = new ReportParameter("AmountInWords", strAmountInWords, true);
                        }
                        else
                        {
                            ReportParameter[5] = new ReportParameter("FooterValue", "", true);
                            ReportParameter[6] = new ReportParameter("AmountInWords", "", true);
                        }

                        ReportViewer1.LocalReport.SetParameters(ReportParameter);
                        break;
                    }
                case (int)eMenuID.StockLedger:
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptAccStockLedger.rdlc";
                        this.reportViewer4.LocalReport.ReportPath = MsReportPath;
                        ReportParameter[] ReportParameter = new ReportParameter[2];
                        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                        ReportParameter[1] = new ReportParameter("Header", cboVendors.Text + " : Stock Ledger " + strPeriod, true);
                        this.reportViewer4.LocalReport.SetParameters(ReportParameter);
                        CompanyHeader = objclsBLLAccountsFormReport.DisplayCompanyHeader();

                        this.reportViewer4.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.reportViewer4.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportProcessingEventHandler);
                        this.reportViewer4.LocalReport.DataSources.Clear();
                        DataSet dtSet = objclsBLLAccountsFormReport.DisplayStockLedger();
                        this.reportViewer4.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", CompanyHeader.Tables[0]));
                        this.reportViewer4.LocalReport.DataSources.Add(new ReportDataSource("DtSetAccounts_StockLedger", dtSet.Tables[0]));
                        break;
                    }
            }

            if (TempMenuID == eMenuID.StockSummaryItemwise)
            {
                this.reportViewer2.SetDisplayMode(DisplayMode.PrintLayout);
                this.reportViewer2.ZoomMode = ZoomMode.Percent;                
            }
            else if (TempMenuID == eMenuID.StockSummaryItemQtywise)
            {
                this.reportViewer3.SetDisplayMode(DisplayMode.PrintLayout);
                this.reportViewer3.ZoomMode = ZoomMode.Percent;
            }
            else if (TempMenuID == eMenuID.StockLedger)
            {
                this.reportViewer4.SetDisplayMode(DisplayMode.PrintLayout);
                this.reportViewer4.ZoomMode = ZoomMode.Percent;
            }
            else
            {
                this.ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                this.ReportViewer1.ZoomMode = ZoomMode.Percent;
            }

            this.Cursor = Cursors.Default;
        }
        
        private void LoadCombos()
        {
            DataTable datCompanyCombos = new DataTable();
            datCompanyCombos = objclsBLLAccountsFormReport.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID + "" });
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DisplayMember = "CompanyName";
            cboCompany.DataSource = datCompanyCombos;

            if (TempMenuID == eMenuID.StatementofAccounts)
                datCompanyCombos = objclsBLLAccountsFormReport.FillCombos(new string[] { "AccountID AS ReferenceID," +
                    "AccountName + ' - ' + Code AS ReferenceName", "AccAccountMaster", "AccountGroupID IN (17,21)" });
            else
                datCompanyCombos = objclsBLLAccountsFormReport.FillCombos(new string[] { "IM.ItemID AS ReferenceID," +
                    "IM.ItemName + ' - ' + IM.Code AS ReferenceName", "InvItemMaster IM INNER JOIN InvItemDetails ID ON IM.ItemID = ID.ItemID", "" +
                    "ID.ProductTypeID IN (1,2)" });

            cboVendors.ValueMember = "ReferenceID";
            cboVendors.DisplayMember = "ReferenceName";
            cboVendors.DataSource = datCompanyCombos;
        }

        private void FillParameters()
        {
            objclsBLLAccountsFormReport.objclsDTOAccountsFormReport.intTempCompanyID = cboCompany.SelectedValue.ToInt32();
            objclsBLLAccountsFormReport .objclsDTOAccountsFormReport .dtpTempFromDate = dtpFromDate.Value.ToString("dd MMM yyyy").ToDateTime();
            objclsBLLAccountsFormReport.objclsDTOAccountsFormReport.dtpTempToDate =(TempMenuID == eMenuID.BalanceSheet ?
                dtpFromDate.Value.ToString("dd MMM yyyy").ToDateTime() :
                dtpToDate.Value.ToString("dd MMM yyyy").ToDateTime());
            strPeriod = (TempMenuID == eMenuID.BalanceSheet ?
                " As on "+ dtpFromDate.Value.ToString("dd MMM yyyy") :
                "From " + dtpFromDate.Value.ToString("dd MMM yyyy") + " To " + dtpToDate.Value.ToString("dd MMM yyyy"));
            objclsBLLAccountsFormReport.objclsDTOAccountsFormReport.intTempAccountID = cboVendors.SelectedValue.ToInt32();
        }

        private void GetBookstartDate()
        {
            //dtpFromDate.MinDate = objclsBLLAccountsFormReport.GetBookstartDate();
        }

        public void SubReportProcessingEventHandler(Object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", CompanyHeader.Tables[0]));
        }

        private void frmAccountsFormReport_Load(object sender, EventArgs e)
        {            
            if (TempMenuID == eMenuID.BalanceSheet)
            {
                BtnShow.Location = new System.Drawing.Point(lblToDate.Location.X, lblToDate.Location.Y);
                dtpToDate.Visible = false;
                lblToDate.Visible = false;
                lblFromDate.Text = "On";
            }
            this.BtnShow.Enabled = false;
            LoadCombos();

            if (TempMenuID == eMenuID.BalanceSheet)
                dtpFromDate.Value = ClsCommonSettings.GetServerDate();
            else
            {
                if (cboCompany.SelectedIndex >= 0)
                    dtpFromDate.Value = MobjclsBLLAccountSummary.GetFinancialStartDate(cboCompany.SelectedValue.ToInt32(), ClsCommonSettings.GetServerDate());
            }

            if (TempMenuID == eMenuID.StatementofAccounts || TempMenuID == eMenuID.StockSummary ||
                TempMenuID == eMenuID.StockSummaryItemwise || TempMenuID == eMenuID.StockSummaryItemQtywise || TempMenuID == eMenuID.StockLedger)
            {
                lblVendors.Visible = cboVendors.Visible = true;
                chkQtywise.Visible = false;

                if (TempMenuID == eMenuID.StatementofAccounts)
                    lblVendors.Text = "Vendor";
                else if (TempMenuID == eMenuID.StockLedger)
                    lblVendors.Text = "Item";
                else
                {
                    lblVendors.Text = "Item";
                    chkQtywise.Visible = true;
                }
            }
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            if (TempMenuID == eMenuID.StockSummary || TempMenuID == eMenuID.StockSummaryItemwise || TempMenuID == eMenuID.StockSummaryItemQtywise)
            {
                if (cboVendors.SelectedIndex == -1)
                    TempMenuID = eMenuID.StockSummary;
                else
                {
                    if (chkQtywise.Checked)
                        TempMenuID = eMenuID.StockSummaryItemQtywise;
                    else
                        TempMenuID = eMenuID.StockSummaryItemwise;
                }
            }

            if (FormValidation() == true)
            {
                FillParameters();
                LoadReport();
            }
        }

        private bool FormValidation()
        {
            if (TempMenuID != eMenuID.BalanceSheet)
            {
                if (dtpFromDate.Value.Date > dtpToDate.Value.Date)
                {
                    this.UserMessage.ShowMessage(19, dtpFromDate);
                    return false;
                }
            }

            if (TempMenuID == eMenuID.StatementofAccounts)
            {
                if (cboVendors.SelectedIndex == -1)
                {
                    this.UserMessage.ShowMessage(9125, cboVendors);
                    return false;
                }
            }

            if (TempMenuID == eMenuID.StockLedger)
            {
                if (cboVendors.SelectedIndex == -1)
                {
                    this.UserMessage.ShowMessage(28, cboVendors);
                    return false;
                }
            }
            return true;
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedValue.ToInt32() > 0)
            {
                objclsBLLAccountsFormReport.objclsDTOAccountsFormReport.intTempCompanyID = cboCompany.SelectedValue.ToInt32();
                this.BtnShow.Enabled=true ;
                GetBookstartDate();
            }
        }

        private void cboCompany_KeyDown(object sender, KeyEventArgs e)
        {
            ((ComboBox)sender).DroppedDown = false;
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedIndex >= 0)
            {
                DateTime dtpFDate = MobjclsBLLAccountSummary.GetCompanyBookStartDate(cboCompany.SelectedValue.ToInt32());
                if (dtpFromDate.Value < dtpFDate)
                    dtpFromDate.Value = dtpFDate;

                DateTime dtpTDate = MobjclsBLLAccountSummary.GetFinancialStartDate(cboCompany.SelectedValue.ToInt32(),
                    dtpFromDate.Value.AddYears(1)).AddDays(-1);
                if (dtpToDate.Value > dtpTDate || dtpFromDate.Value > dtpToDate.Value)
                    dtpToDate.Value = dtpTDate;
            }
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedIndex >= 0)
            {
                DateTime dtpFDate = MobjclsBLLAccountSummary.GetFinancialStartDate(cboCompany.SelectedValue.ToInt32(), dtpToDate.Value);
                if (dtpFromDate.Value < dtpFDate || dtpFromDate.Value > dtpToDate.Value)
                    dtpFromDate.Value = dtpFDate;
            }
        }

        private void cboVendors_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (TempMenuID == eMenuID.StockSummary || TempMenuID == eMenuID.StockSummaryItemwise)
            {
                if (cboVendors.SelectedIndex == -1)
                    TempMenuID = eMenuID.StockSummary;
                else
                    TempMenuID = eMenuID.StockSummaryItemwise;
            }
        }
    }
}
