﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class frmAccountSummary : DevComponents.DotNetBar.Office2007Form
    {
        clsBLLAccountSummary MobjclsBLLAccountSummary = new clsBLLAccountSummary();
        clsBLLChartOfAccounts MObjclsBLLChartOfAccounts = new clsBLLChartOfAccounts();
        int intTempRefID = 0, intSelectedCriteria = 0;
        string strTempDescription = "";
        bool blnGrp = false;
        DataSet dtsTemp = new DataSet();
        DataTable datDrilDown = new DataTable();
        int intDrilDown = 2;
        bool MblnAddPermission, MblnAddUpdatePermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission;
        private clsMessage objUserMessage = null;
        public int SummaryId = 68;

        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.ChartOfAccounts);
                return this.objUserMessage;
            }
        }

        public frmAccountSummary()
        {
            InitializeComponent();
        }

        private void frmAccountSummary_Load(object sender, EventArgs e)
        {            
            expnlChartofAccounts.Visible = false;
            ClearControls();
            LoadCombos();
            cboDisplayFilter.Enabled = false;
            cboDisplayFilter.SelectedIndex = 0;           

            datDrilDown.Columns.Add("Description");
            datDrilDown.Columns.Add("MenuID");
            datDrilDown.Columns.Add("AccountID");
            datDrilDown.Columns.Add("IsGroup");

            lblSearchTitle.Text = "Search";
            if (cboSearchCompany.SelectedIndex >= 0)
                dtpFromDate.Value = MobjclsBLLAccountSummary.GetFinancialStartDate(cboSearchCompany.SelectedValue.ToInt32(), ClsCommonSettings.GetServerDate());
            
            SetPermissions();
            btnPrint.Enabled = false;            
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 2)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Accounts, (int)eMenuID.AccountsSummary, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                if (MblnAddPermission == true || MblnUpdatePermission == true)
                    MblnAddUpdatePermission = true;
            }
            else
                MblnAddPermission = MblnAddUpdatePermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            btnPrint.Enabled = MblnPrintEmailPermission;
        }

        private void ClearControls()
        {
            DataTable datTemp = new DataTable();
            dgvAccountDetails.DataSource = datTemp;
            lblSearchTitle.Text = "";
            pnlLedgerSummary.Visible = false;            
        }

        private void LoadCombos()
        {
            cboSearchCompany.DataSource = MobjclsBLLAccountSummary.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID + " ORDER BY CompanyName" });
            cboSearchCompany.ValueMember = "CompanyID";
            cboSearchCompany.DisplayMember = "CompanyName";
            cboSearchCompany.SelectedValue = ClsCommonSettings.CompanyID;

            cboSearchAccount.DataSource = MobjclsBLLAccountSummary.FillCombos(new string[] { "AccountID,AccountName", "AccAccountMaster", "" +
                "(CompanyID = " + ClsCommonSettings.CompanyID + " OR CompanyID IS NULL) AND ISNULL(AccountGroupID,0) <> 0 ORDER BY AccountName" });
            cboSearchAccount.ValueMember = "AccountID";
            cboSearchAccount.DisplayMember = "AccountName";
            cboSearchAccount.SelectedIndex = -1;
        }

        private void lvDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvDisplay.SelectedItems.Count < 1)
                return;
            ClearControls();
            GetLvSelectedID();

            if (cboSearchCompany.SelectedIndex >= 0)
            {
                if (lvDisplay.SelectedItems[0].Tag.ToInt32() == (int)eMenuID.DayBook)
                    dtpFromDate.Value = ClsCommonSettings.GetServerDate();
                else
                    dtpFromDate.Value = MobjclsBLLAccountSummary.GetFinancialStartDate(cboSearchCompany.SelectedValue.ToInt32(), ClsCommonSettings.GetServerDate());
            }

            chkShowVoucher.Visible = false;
            chkShowVoucher.Checked = false;

            switch (lvDisplay.SelectedItems[0].Tag.ToInt32())
            {
                case ((int)eMenuID.AccountsSummary):
                    FillAccountsTree();
                    expnlChartofAccounts.Visible = true;
                    grpnlSorting.Visible = false;
                    blnGrp = true;
                    intTempRefID = 0;
                    btnSearch.Enabled = false;
                    break;
                case ((int)eMenuID.GeneralLedger):
                    FillAccountsTree();
                    grpnlSorting.Visible = true;
                    expnlChartofAccounts.Visible = true;
                    chkShowVoucher.Visible = true;
                    chkShowVoucher.Checked = false;
                    cboSearchAccount.SelectedIndex = -1;
                    blnGrp = false;
                    intTempRefID = 0;
                    btnSearch.Enabled = false;
                    chkShowVoucher.Visible = true;
                    chkShowVoucher.Checked = false;
                    break;
                case ((int)eMenuID.DayBook):
                    chkShowVoucher.Visible = true;
                    chkShowVoucher.Checked = false;
                    expnlChartofAccounts.Visible = false;
                    btnSearch.Enabled = true;
                    dtpFromDate.Value = ClsCommonSettings.GetServerDate();
                    break;
                case ((int)eMenuID.BalanceSheet):
                    expnlChartofAccounts.Visible = false;
                    btnSearch.Enabled = true;
                    dtpFromDate.Value = ClsCommonSettings.GetServerDate();
                    break;
                case ((int)eMenuID.CashBook):
                case ((int)eMenuID.TrialBalance):
                case ((int)eMenuID.ProfitAndLossAccount):
                    expnlChartofAccounts.Visible = false;
                    btnSearch.Enabled = true;
                    break;
            }
            dtpToDate.Value = ClsCommonSettings.GetServerDate();
        }

        private void GetLvSelectedID()
        {            
            if (lvDisplay.SelectedItems[0].ToString().Contains("Trial Balance"))
                lvDisplay.SelectedItems[0].Tag = (int)eMenuID.TrialBalance;
            else if (lvDisplay.SelectedItems[0].ToString().Contains("General Ledger"))
                lvDisplay.SelectedItems[0].Tag = (int)eMenuID.GeneralLedger;
            else if (lvDisplay.SelectedItems[0].ToString().Contains("Day Book"))
                lvDisplay.SelectedItems[0].Tag = (int)eMenuID.DayBook;
            else if (lvDisplay.SelectedItems[0].ToString().Contains("Group Summary"))
                lvDisplay.SelectedItems[0].Tag = (int)eMenuID.AccountsSummary;
            else if (lvDisplay.SelectedItems[0].ToString().Contains("Cash Book"))
                lvDisplay.SelectedItems[0].Tag = (int)eMenuID.CashBook;
            else if (lvDisplay.SelectedItems[0].ToString().Contains("Profit and Loss"))
                lvDisplay.SelectedItems[0].Tag = (int)eMenuID.ProfitAndLossAccount;
            else if (lvDisplay.SelectedItems[0].ToString().Contains("Balance Sheet"))
                lvDisplay.SelectedItems[0].Tag = (int)eMenuID.BalanceSheet;
        }

        private void FillAccountsTree()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                TvAccountsHead.Nodes.Clear();   // First clear the treeview
                DataTable datTemp = MObjclsBLLChartOfAccounts.FillAccountsHead();
                // Looping through with the account heads
                foreach (DataRow dRow in MObjclsBLLChartOfAccounts.FillAccountsHead().Rows)
                {
                    // New node for Account head
                    DevComponents.AdvTree.Node TnAccount = new DevComponents.AdvTree.Node();
                    TnAccount.Text = dRow["AccountGroupName"].ToString();
                    TnAccount.Tag = dRow["AccountGroupID"];
                    TnAccount.ImageIndex = 0;
                    TvAccountsHead.Nodes.Add(TnAccount);    // Add account head into the treeview
                    cboDisplayFilter.Enabled = false;
                    if (lvDisplay.SelectedItems[0].Tag.ToInt32() == (int)eMenuID.GeneralLedger)
                    {
                        FillAccounts(TnAccount);
                        cboDisplayFilter.Enabled = true;
                    }
                    FillChildGroups(TnAccount);
                    TvAccountsHead.ExpandAll();
                }
            }
            catch { }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void FillAccounts(DevComponents.AdvTree.Node TnParentGroup)
        {
            DataTable datChildGroups = null;
            datChildGroups = this.MobjclsBLLAccountSummary.GetChildAccounts(Convert.ToInt32(TnParentGroup.Tag), intSelectedCriteria);
            if (datChildGroups == null || datChildGroups.Rows.Count == 0) return;
            foreach (DataRow dtRow in datChildGroups.Rows)
            {
                DevComponents.AdvTree.Node TnAccount = new DevComponents.AdvTree.Node();
                TnAccount.Text = dtRow["AccountName"].ToString();
                TnAccount.Tag = "@" + dtRow["AccountID"].ToString();
                TnAccount.ImageIndex = 1;
                TnParentGroup.Nodes.Add(TnAccount);    // Add account head into the treeview
            }
        }

        private void FillChildGroups(DevComponents.AdvTree.Node TnParent)
        {
            DataTable datChildGroups = null;
            datChildGroups = this.MObjclsBLLChartOfAccounts.GetChildAccountHead(Convert.ToInt32(TnParent.Tag));
            if (datChildGroups == null || datChildGroups.Rows.Count == 0) return;
            foreach (DataRow dtRow in datChildGroups.Rows)
            {
                DevComponents.AdvTree.Node TnAccount = new DevComponents.AdvTree.Node();
                TnAccount.Text = dtRow[1].ToString();
                TnAccount.Tag = dtRow[0];
                TnAccount.ImageIndex = 0;
                TnParent.Nodes.Add(TnAccount);    // Add account head into the treeview
                if (lvDisplay.SelectedItems[0].Tag.ToInt32() == (int)eMenuID.GeneralLedger)
                    FillAccounts(TnAccount);
                FillChildGroups(TnAccount);
            }
        }

        private void cboDisplayFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cboDisplayFilter.Text)
            {
                case "A/c Name":
                    intSelectedCriteria = 0;
                    break;
                case "A/c Code":
                    intSelectedCriteria = 1;
                    break;
                case "A/c Name & A/c Code":
                    intSelectedCriteria = 2;
                    break;
            }
            if (lvDisplay.SelectedItems.Count < 1)
                return;
            FillAccountsTree();
        }

        private void TvAccountsHead_AfterNodeSelect(object sender, DevComponents.AdvTree.AdvTreeNodeEventArgs e)
        {
            if (lvDisplay.SelectedItems.Count < 1 || TvAccountsHead.SelectedNode == null || TvAccountsHead.SelectedNode.Tag == null)
                return;
            intTempRefID = (TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") == -1 ?
                    Convert.ToInt32(TvAccountsHead.SelectedNode.Tag) :
                    Convert.ToInt32(TvAccountsHead.SelectedNode.Tag.ToString().Remove(0, 1)));

            if (MObjclsBLLChartOfAccounts.GetAccountInfo(TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") == -1 ? true : false, intTempRefID))
            {                
                strTempDescription = MObjclsBLLChartOfAccounts.objMclsDTOChartOfAccounts.Description;
                blnGrp = (TvAccountsHead.SelectedNode.Tag.ToString().IndexOf("@") == -1 ? true : false);
            }

            btnSearch.Enabled = true;
            if (blnGrp == false)
                cboSearchAccount.SelectedValue = intTempRefID;
        }

        private void cboSearchAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboSearchAccount.SelectedIndex >= 0)
                SetNodeSelection();
        }
        private void cboSearchCompany_KeyDown(object sender, KeyEventArgs e)
        {
            ((ComboBox)sender).DroppedDown = false;
        }
        private void SetNodeSelection()
        {
            DevComponents.AdvTree.Node NodeForAccount = null;
            if (this.TvAccountsHead.Nodes.Count <= 0)
                return;
            foreach (DevComponents.AdvTree.Node node in this.TvAccountsHead.Nodes)
            {
                DevComponents.AdvTree.Node childNode = node;
                NodeForAccount = this.FindAccNode(childNode, cboSearchAccount.SelectedValue.ToInt32());
                if (NodeForAccount != null)
                    break;
            }
            if (NodeForAccount != null)
                this.TvAccountsHead.SelectedNode = NodeForAccount;
        }

        private DevComponents.AdvTree.Node FindAccNode(DevComponents.AdvTree.Node node, int AccountID)
        {
            DevComponents.AdvTree.Node NodeForAccount = null;
            if (node.Nodes.Count > 0)
            {
                foreach (DevComponents.AdvTree.Node nd in node.Nodes)
                {
                    if (nd.Tag.ToString().IndexOf("@") != -1)
                    {
                        if (Convert.ToInt32(nd.Tag.ToString().Remove(0, 1)) == AccountID)
                            return nd;
                    }
                    else
                    {
                        NodeForAccount = FindAccNode(nd, AccountID);
                        if (NodeForAccount != null)
                            break;
                    }
                }
            }
            return NodeForAccount;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (lvDisplay.SelectedItems.Count > 0)
            {
                GetLvSelectedID();
                chkShowVoucher.Visible = false;
                chkShowVoucher.Checked = false;

                if (this.Text == "General Ledger" && lvDisplay.SelectedItems[0].Tag.ToInt32() == (int)eMenuID.GeneralLedger && blnGrp == true)
                {
                    ClearControls();
                    return;
                }
                if (intTempRefID == 0 && (this.Text == "General Ledger" || this.Text == "Group Summary") &&
                    (lvDisplay.SelectedItems[0].Tag.ToInt32() == (int)eMenuID.GeneralLedger ||
                    lvDisplay.SelectedItems[0].Tag.ToInt32() == (int)eMenuID.AccountsSummary))
                    return;

                if (lvDisplay.SelectedItems[0].Tag.ToInt32() == (int)eMenuID.GeneralLedger &&
                    (intTempRefID == (int)Accounts.OpeningStock || intTempRefID == (int)Accounts.GoodsInTransit || intTempRefID == (int)Accounts.StockAccount))
                {
                    ClearControls();
                    return;
                }

                if (lvDisplay.SelectedItems[0].Tag.ToInt32() == (int)eMenuID.GeneralLedger && blnGrp == true)
                {
                    ClearControls();
                    return;
                }

                LoadSearch();
            }
        }

        private void LoadSearch()
        {
            string strTitleText ="";

            if (lvDisplay.SelectedItems.Count < 1)
                return;

            ClearControls();
            FillGrid();
            GridFieldVisibility(lvDisplay.SelectedItems[0].Tag.ToInt32());
            pnlLedgerSummary.Visible = true;
            btnPrint.Enabled = true;

            switch (lvDisplay.SelectedItems[0].Tag.ToInt32())
            {
                case ((int)eMenuID.AccountsSummary):
                    strTitleText = strTempDescription + " - Group Summary";
                    DisplaySummary();
                    break;
                case ((int)eMenuID.GeneralLedger):
                    strTitleText = strTempDescription + " - Ledger";
                    DisplayGeneralLedger();
                    break;
                case ((int)eMenuID.DayBook):
                    strTitleText = "Day Book";
                    pnlLedgerSummary.Visible = false;
                    chkShowVoucher.Visible = true;
                    chkShowVoucher.Checked = false;
                    break;
                case ((int)eMenuID.CashBook):
                    strTitleText = "Cash Book";
                    DisplaySummary();
                    break;
                case ((int)eMenuID.TrialBalance):
                    strTitleText = "Trial Balance";
                    DisplaySummary();
                    break;
                case ((int)eMenuID.ProfitAndLossAccount):
                    strTitleText = "Profit And Loss Account";
                    pnlLedgerSummary.Visible = false;
                    break;
                case ((int)eMenuID.BalanceSheet):
                    strTitleText = "Balance Sheet";
                    pnlLedgerSummary.Visible = false;
                    break;
            }

            switch (lvDisplay.SelectedItems[0].Tag.ToInt32())
            {
                case ((int)eMenuID.AccountsSummary):
                case ((int)eMenuID.GeneralLedger):
                case ((int)eMenuID.DayBook):
                case ((int)eMenuID.CashBook):
                case ((int)eMenuID.TrialBalance):
                    if (dtpFromDate.Value == dtpToDate.Value)
                        lblSearchTitle.Text = strTitleText + " For " + dtpFromDate.Value.ToString("dd MMM yyyy");
                    else
                        lblSearchTitle.Text = strTitleText + " From " + dtpFromDate.Value.ToString("dd MMM yyyy") + " To " + dtpToDate.Value.ToString("dd MMM yyyy");
                    break;
                case ((int)eMenuID.ProfitAndLossAccount):
                    lblSearchTitle.Text = strTitleText + " For the period " + dtpFromDate.Value.ToString("dd MMM yyyy") + " - " + dtpToDate.Value.ToString("dd MMM yyyy");
                    break;
                case ((int)eMenuID.BalanceSheet):
                    lblSearchTitle.Text = strTitleText + " As On " + dtpFromDate.Value.ToString("dd MMM yyyy");
                    break;
            }

            intDrilDown = 2;
            datDrilDown.Rows.Clear();
            datDrilDown.Rows.Add();
            datDrilDown.Rows[datDrilDown.Rows.Count - 1]["MenuID"] = lvDisplay.SelectedItems[0].Tag.ToInt32();

            if (lvDisplay.SelectedItems[0].Tag.ToInt32() == (int)eMenuID.CashBook)
                datDrilDown.Rows[datDrilDown.Rows.Count - 1]["AccountID"] = (int)AccountGroups.CurrentAssets;
            else
                datDrilDown.Rows[datDrilDown.Rows.Count - 1]["AccountID"] = intTempRefID;

            datDrilDown.Rows[datDrilDown.Rows.Count - 1]["IsGroup"] = blnGrp;
            datDrilDown.Rows[datDrilDown.Rows.Count - 1]["Description"] = lblSearchTitle.Text;
        }

        private void FillGrid()
        {
            MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID = lvDisplay.SelectedItems[0].Tag.ToInt32();
            if (lvDisplay.SelectedItems[0].Tag.ToInt32() == (int)eMenuID.CashBook)
                MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempAccountID = (int)AccountGroups.CurrentAssets;
            else
                MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempAccountID = intTempRefID;
            MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempCompanyID = cboSearchCompany.SelectedValue.ToInt32();
            MobjclsBLLAccountSummary .MobjclsDTOAccountSummary .dtpTempFromDate = dtpFromDate.Value.ToString("dd MMM yyyy").ToDateTime();
            MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.dtpTempToDate = dtpToDate.Value.ToString("dd MMM yyyy").ToDateTime();
            MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.IsTempGroup = blnGrp;

            if (MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID != (int)eMenuID.DayBook &&
                MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID != (int)eMenuID.ProfitAndLossAccount &&
                MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID != (int)eMenuID.BalanceSheet &&
                MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempAccountID == 0)
            {
                MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID = (int)eMenuID.TrialBalance;
                lvDisplay.SelectedItems[0].Tag = (int)eMenuID.TrialBalance;
            }

            if (MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.ProfitAndLossAccount &&
                MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempAccountID == 0)
            {
                MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID = (int)eMenuID.ProfitAndLossAccount;
                lvDisplay.SelectedItems[0].Tag = (int)eMenuID.ProfitAndLossAccount;
            }

            if (MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.BalanceSheet &&
                MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempAccountID == 0)
            {
                MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID = (int)eMenuID.BalanceSheet;
                lvDisplay.SelectedItems[0].Tag = (int)eMenuID.BalanceSheet;
            }

            if (MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.GeneralLedger &&
                MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.IsTempGroup == true)
            {
                MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID = (int)eMenuID.AccountsSummary;
                lvDisplay.SelectedItems[0].Tag = (int)eMenuID.AccountsSummary;
            }

            dtsTemp.Clear();
            dtsTemp = MobjclsBLLAccountSummary.DisplayAccountsSummaryReoprt();
            dgvAccountDetails.DataSource = dtsTemp.Tables[0];

            DataGridViewCellStyle gvStyle = new DataGridViewCellStyle();
            
            gvStyle.Font = new Font(DataGridView.DefaultFont, FontStyle.Italic);
            //gvStyle.ForeColor = Color.Black;

            if (MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.ProfitAndLossAccount ||
                 MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.BalanceSheet)
            {
                dgvAccountDetails.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
                
                foreach(DataGridViewRow row in dgvAccountDetails.Rows)
                {
                    if (row.Cells["DebitIsGroup"].Value.ToBoolean() || row.Cells["DebitAccountID"].Value.ToInt32() == (int)Accounts.OpeningStock)
                    {
                        foreach(DataGridViewCell cell in row.Cells)
                        {
                            if (cell.ColumnIndex == 1 || cell.ColumnIndex == 2)
                                cell.Style.ApplyStyle(gvStyle);
                        }
                    }
                    if (row.Cells["CreditIsGroup"].Value.ToBoolean())
                    {
                        foreach(DataGridViewCell cell in row.Cells)
                        {
                            if (cell.ColumnIndex == 6 || cell.ColumnIndex == 7)
                                cell.Style.ApplyStyle(gvStyle);
                        }
                    }
                }
            }
            else if (MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.DayBook)
            {
                dgvAccountDetails.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

                foreach (DataGridViewRow row in dgvAccountDetails.Rows)
                {
                    if (row.Cells["VoucherDate"].Value.ToStringCustom() == "" && row.Cells["VoucherType"].Value.ToStringCustom() == "" &&
                        row.Cells["VoucherNo"].Value.ToStringCustom() == "")
                    {
                        foreach (DataGridViewCell cell in row.Cells)
                            cell.Style.ApplyStyle(gvStyle);
                    }
                }
            }
            else if (MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.AccountsSummary)
            {
                dgvAccountDetails.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

                foreach (DataGridViewRow row in dgvAccountDetails.Rows)
                {
                    if (row.Cells["ParentID"].Value.ToInt32() == (int)AccountGroups.Stockinhand)
                    {
                        foreach (DataGridViewCell cell in row.Cells)
                            cell.Style.ApplyStyle(gvStyle);
                    }
                }
            }
            else
                dgvAccountDetails.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            
            if (dtsTemp.Tables.Count > 0)
            {
                dgvAccountDetails.DataSource = dtsTemp.Tables[0];

                if (dgvAccountDetails.Columns.Contains("VoucherDate"))
                {
                    dgvAccountDetails.Columns["VoucherDate"].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dgvAccountDetails.Columns["VoucherDate"].ReadOnly = true;
                }
                if (dgvAccountDetails.Columns.Contains("VoucherType"))
                {
                    dgvAccountDetails.Columns["VoucherType"].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dgvAccountDetails.Columns["VoucherType"].ReadOnly = true;
                }
                if (dgvAccountDetails.Columns.Contains("VoucherNo"))
                {
                    dgvAccountDetails.Columns["VoucherNo"].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dgvAccountDetails.Columns["VoucherNo"].ReadOnly = true;
                }
                if (dgvAccountDetails.Columns.Contains("Particulars"))
                {
                    dgvAccountDetails.Columns["Particulars"].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dgvAccountDetails.Columns["Particulars"].ReadOnly = true;
                }
                if (dgvAccountDetails.Columns.Contains("DebitAmount"))
                {
                    dgvAccountDetails.Columns["DebitAmount"].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dgvAccountDetails.Columns["DebitAmount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvAccountDetails.Columns["DebitAmount"].ReadOnly = true;
                }
                if (dgvAccountDetails.Columns.Contains("CreditAmount"))
                {
                    dgvAccountDetails.Columns["CreditAmount"].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dgvAccountDetails.Columns["CreditAmount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvAccountDetails.Columns["CreditAmount"].ReadOnly = true;
                }
                if (dgvAccountDetails.Columns.Contains("Debit"))
                {
                    dgvAccountDetails.Columns["Debit"].ReadOnly = true;
                    dgvAccountDetails.Columns["Debit"].HeaderText = "Particulars";
                }
                if (dgvAccountDetails.Columns.Contains("Credit"))
                {
                    dgvAccountDetails.Columns["Credit"].ReadOnly = true;
                    dgvAccountDetails.Columns["Credit"].HeaderText = "Particulars";
                }
                if (dgvAccountDetails.Columns.Contains("ForSeperation"))
                {
                    dgvAccountDetails.Columns["ForSeperation"].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dgvAccountDetails.Columns["ForSeperation"].HeaderText = "";
                    dgvAccountDetails.Columns["ForSeperation"].ReadOnly = true;
                }

                if (MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.ProfitAndLossAccount ||
                 MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.BalanceSheet)
                {
                    if (dgvAccountDetails.Columns.Contains("DebitAmount"))
                        dgvAccountDetails.Columns["DebitAmount"].HeaderText = "";
                    if (dgvAccountDetails.Columns.Contains("CreditAmount"))
                        dgvAccountDetails.Columns["CreditAmount"].HeaderText = "";
                }

                if (MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.BalanceSheet)
                {
                    if (dgvAccountDetails.Columns.Contains("Debit"))
                        dgvAccountDetails.Columns["Debit"].HeaderText = "Liabilities";
                    if (dgvAccountDetails.Columns.Contains("Credit"))
                        dgvAccountDetails.Columns["Credit"].HeaderText = "Assets";
                }
                
                dgvAccountDetails.ClearSelection();
                btnPrint.Enabled = true;
            }
        }
               
        private void GridFieldVisibility(int intMenuID)
        {
            if (dgvAccountDetails.Columns.Contains("AccountID"))
                dgvAccountDetails.Columns["AccountID"].Visible = false;
            if (dgvAccountDetails.Columns.Contains("CompanyID"))
                dgvAccountDetails.Columns["CompanyID"].Visible = false;
            if (dgvAccountDetails.Columns.Contains("VoucherID"))
                dgvAccountDetails.Columns["VoucherID"].Visible = false;
            if (dgvAccountDetails.Columns.Contains("VoucherTypeID"))
                dgvAccountDetails.Columns["VoucherTypeID"].Visible = false;
            if (dgvAccountDetails.Columns.Contains("IsCostCenter"))
                dgvAccountDetails.Columns["IsCostCenter"].Visible = false;
            if (dgvAccountDetails.Columns.Contains("ParentID"))
                dgvAccountDetails.Columns["ParentID"].Visible = false;
            if (dgvAccountDetails.Columns.Contains("IsGroup"))
                dgvAccountDetails.Columns["IsGroup"].Visible = false;
            if (dgvAccountDetails.Columns.Contains("OperationTypeID"))
                dgvAccountDetails.Columns["OperationTypeID"].Visible = false;
            if (dgvAccountDetails.Columns.Contains("ReferenceID"))
                dgvAccountDetails.Columns["ReferenceID"].Visible = false;
            if (dgvAccountDetails.Columns.Contains("DebitAccountID"))
                dgvAccountDetails.Columns["DebitAccountID"].Visible = false;
            if (dgvAccountDetails.Columns.Contains("DebitIsGroup"))
                dgvAccountDetails.Columns["DebitIsGroup"].Visible = false;
            if (dgvAccountDetails.Columns.Contains("CreditAccountID"))
                dgvAccountDetails.Columns["CreditAccountID"].Visible = false;
            if (dgvAccountDetails.Columns.Contains("CreditIsGroup"))
                dgvAccountDetails.Columns["CreditIsGroup"].Visible = false;

            if (dgvAccountDetails.DataSource != null)
            {
                if (dgvAccountDetails.Columns.Contains("Particulars"))
                    dgvAccountDetails.Columns["Particulars"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                if (dgvAccountDetails.Columns.Contains("Remarks"))
                    dgvAccountDetails.Columns["Remarks"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                if (dgvAccountDetails.Columns.Contains("Debit"))
                    dgvAccountDetails.Columns["Debit"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                if (dgvAccountDetails.Columns.Contains("Credit"))
                    dgvAccountDetails.Columns["Credit"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void DisplayGeneralLedger()
        {
            if (dtsTemp.Tables[1].Rows.Count > 0)
            {
                pnlLedgerSummary.Visible = true;
                chkShowVoucher.Visible = true;
                chkShowVoucher.Checked = false;

                if (dtsTemp.Tables[1].Rows.Count > 0)
                {
                    lblOpeningBalance.Enabled = lblDebitOpening.Enabled = lblCreditOpening.Enabled = true;
                    lblOpeningBalance.Text = dtsTemp.Tables[1].Rows[0]["Particulars"].ToString();
                    lblDebitOpening.Text = dtsTemp.Tables[1].Rows[0]["DebitAmount"].ToString();
                    lblCreditOpening.Text = dtsTemp.Tables[1].Rows[0]["CreditAmount"].ToString();
                }
                if (dtsTemp.Tables[1].Rows.Count > 1)
                {
                    lblCurrentTotal.Enabled = lblDebitTotal.Enabled = lblCreditTotal.Enabled = true;
                    lblCurrentTotal.Text = dtsTemp.Tables[1].Rows[1]["Particulars"].ToString();
                    lblDebitTotal.Text = dtsTemp.Tables[1].Rows[1]["DebitAmount"].ToString();
                    lblCreditTotal.Text = dtsTemp.Tables[1].Rows[1]["CreditAmount"].ToString();
                }
                if (dtsTemp.Tables[1].Rows.Count > 2)
                {
                    lblClosingBalance.Enabled = lblDebitClosing.Enabled = lblCreditClosing.Enabled = true;
                    lblClosingBalance.Text = dtsTemp.Tables[1].Rows[2]["Particulars"].ToString();
                    lblDebitClosing.Text = dtsTemp.Tables[1].Rows[2]["DebitAmount"].ToString();
                    lblCreditClosing.Text = dtsTemp.Tables[1].Rows[2]["CreditAmount"].ToString();
                }
                this.pnlLedgerSummary.Size = new System.Drawing.Size(632, 78);
            }
        }

        private void DisplaySummary()
        {
            if (dtsTemp.Tables[1].Rows.Count > 0)
            {
                pnlLedgerSummary.Visible = true;

                lblOpeningBalance.Enabled = lblDebitOpening.Enabled = lblCreditOpening.Enabled = false;
                lblCurrentTotal.Enabled = lblDebitTotal.Enabled = lblCreditTotal.Enabled = false;

                lblClosingBalance.Enabled = lblDebitClosing.Enabled = lblCreditClosing.Enabled = true;
                lblClosingBalance.Text = dtsTemp.Tables[1].Rows[0]["Particulars"].ToString();
                lblDebitClosing.Text = dtsTemp.Tables[1].Rows[0]["DebitAmount"].ToString();
                lblCreditClosing.Text = dtsTemp.Tables[1].Rows[0]["CreditAmount"].ToString();

                this.pnlLedgerSummary.Size = new System.Drawing.Size(632, 35);
            }
            else
            {
                lblDebitOpening.Text = lblDebitTotal.Text = lblDebitClosing.Text = "";
                lblCreditOpening.Text = lblCreditTotal.Text = lblCreditClosing.Text = "";
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void LoadReport()
        {
            using (FrmReportviewer ObjViewer = new FrmReportviewer())
            {
                ObjViewer.Text = this.Text;
                ObjViewer.PintCompany = cboSearchCompany.SelectedValue.ToInt32();
                ObjViewer.PiFormID = (int)FormID.AccountSummary;
                ObjViewer.strAccHeader = lblSearchTitle.Text;
                ObjViewer.intMenuID = lvDisplay.SelectedItems[0].Tag.ToInt32();
                ObjViewer.intAccountID = MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempAccountID;
                ObjViewer.dteFromDate = dtpFromDate.Value.ToString("dd MMM yyyy").ToDateTime();
                ObjViewer.dteToDate = dtpToDate.Value.ToString("dd MMM yyyy").ToDateTime();
                ObjViewer.blnIsGroup = MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.IsTempGroup;
                ObjViewer.ShowDialog();
            }
        }

        private void dgvAccountDetails_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {            
            if (datDrilDown.Rows.Count - intDrilDown <= 0 && intDrilDown > 2)
            {
                string strTempDesc = datDrilDown.Rows[datDrilDown.Rows.Count - (intDrilDown - 1)]["Description"].ToString();
                int intTempMID = datDrilDown.Rows[datDrilDown.Rows.Count - (intDrilDown - 1)]["MenuID"].ToInt32();
                int intTempAID = datDrilDown.Rows[datDrilDown.Rows.Count - (intDrilDown - 1)]["AccountID"].ToInt32();
                bool blnTempIG = datDrilDown.Rows[datDrilDown.Rows.Count - (intDrilDown - 1)]["IsGroup"].ToBoolean();

                datDrilDown.Rows.Clear();
                intDrilDown = 2;

                datDrilDown.Rows.Add();
                datDrilDown.Rows[datDrilDown.Rows.Count - 1]["Description"] = strTempDesc;
                datDrilDown.Rows[datDrilDown.Rows.Count - 1]["MenuID"] = intTempMID;
                datDrilDown.Rows[datDrilDown.Rows.Count - 1]["AccountID"] = intTempAID;
                datDrilDown.Rows[datDrilDown.Rows.Count - 1]["IsGroup"] = blnTempIG;
            }

            if (lvDisplay.SelectedItems.Count < 1)
                return;

            try
            {
                switch (lvDisplay.SelectedItems[0].Tag.ToInt32())
                {
                    case ((int)eMenuID.ProfitAndLossAccount):
                    case ((int)eMenuID.BalanceSheet):
                        if (dgvAccountDetails.CurrentRow != null)
                        {
                            if (dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["DebitAccountID"].Value.ToString() == "" &&
                                dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["CreditAccountID"].Value.ToString() == "")
                                return;

                            else if ((dgvAccountDetails.CurrentCell.ColumnIndex == 1/*DebitAccount*/ || dgvAccountDetails.CurrentCell.ColumnIndex == 2/*Debit*/) &&
                                dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["DebitAccountID"].Value.ToInt32() == (int)Accounts.OpeningStock)
                                return;
                        }
                        break;
                    case ((int)eMenuID.AccountsSummary):
                        if (dgvAccountDetails.CurrentRow != null)
                        {
                            if (dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["AccountID"].Value.ToString() == "")
                                return;

                            else if (dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["ParentID"].Value.ToInt32() == (int)AccountGroups.Stockinhand)
                                return;
                        }
                        break;
                    case ((int)eMenuID.TrialBalance):
                    case ((int)eMenuID.CashBook):
                    case ((int)eMenuID.GeneralLedger):
                    case ((int)eMenuID.DayBook):
                        if (dgvAccountDetails.CurrentRow != null)
                            if (dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["AccountID"].Value.ToString() == "")
                                return;
                        break;
                }                
            }
            catch 
            {
                return;
            }

            switch (lvDisplay.SelectedItems[0].Tag.ToInt32())
            {
                case ((int)eMenuID.TrialBalance):
                case ((int)eMenuID.AccountsSummary):
                case ((int)eMenuID.CashBook):
                    if (dgvAccountDetails.Columns.Contains("VoucherNo"))
                    {
                        try
                        {
                            if (dgvAccountDetails.CurrentRow != null)
                                if (dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["VoucherNo"].Value.ToString() != "")
                                    GetJournalVoucher();
                        }
                        catch { }
                    }
                    else
                        if (dgvAccountDetails.CurrentRow != null)
                            DrilDownIsGroupOrLedger(dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["IsGroup"].Value.ToBoolean());
                    break;

                case ((int)eMenuID.ProfitAndLossAccount):
                case ((int)eMenuID.BalanceSheet):
                    if (dgvAccountDetails.Columns.Contains("VoucherNo"))
                    {
                        try
                        {
                            if (dgvAccountDetails.CurrentRow != null)
                                if (dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["VoucherNo"].Value.ToString() != "")
                                    GetJournalVoucher();
                        }
                        catch { }
                    }
                    else
                        if (dgvAccountDetails.CurrentRow != null)
                        {
                            if ((dgvAccountDetails.CurrentCell.ColumnIndex == 1/*Debit*/ ||
                                dgvAccountDetails.CurrentCell.ColumnIndex == 2/*DebitAmount*/) &&
                               dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["DebitAccountID"].Value.ToInt32() > 0)
                                DrilDownIsGroupOrLedger(dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["DebitIsGroup"].Value.ToBoolean());
                            else if ((dgvAccountDetails.CurrentCell.ColumnIndex == 6/*Credit*/ ||
                                dgvAccountDetails.CurrentCell.ColumnIndex == 7/*CreditAmount*/)&&
                               dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["CreditAccountID"].Value.ToInt32() > 0)
                                DrilDownIsGroupOrLedger(dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["CreditIsGroup"].Value.ToBoolean());
                        }
                    break;

                case ((int)eMenuID.GeneralLedger):
                case ((int)eMenuID.DayBook):
                    if (dgvAccountDetails.Columns.Contains("VoucherNo"))
                    {
                        try
                        {
                            if (dgvAccountDetails.CurrentRow != null)
                                if (dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["VoucherNo"].Value.ToString() != "")
                                    GetJournalVoucher();
                        }
                        catch { }
                    }
                    break;
            }
        }

        private void GetJournalVoucher()
        {
            switch (dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["VoucherTypeID"].Value.ToInt32())
            {
                case (int)VoucherType.Payment:
                    CallVoucher((int)eMenuID.PaymentVoucher, "Payment Voucher", Properties.Resources.Extra_Charges1);
                    break;
                case (int)VoucherType.Receipt:
                    CallVoucher((int)eMenuID.ReceiptVoucher, "Receipt Voucher", Properties.Resources.costEstimation3);
                    break;
                case (int)VoucherType.Contra:
                    CallVoucher((int)eMenuID.ContraVoucher, "Contra Voucher", Properties.Resources.Check_Receipt1);
                    break;
                case (int)VoucherType.Journal:
                    CallVoucher((int)eMenuID.JournalVoucher, "Journal Voucher", Properties.Resources.GereralReceipts_Paymrnts);
                    break;
                case (int)VoucherType.Purchase:
                    if (chkShowVoucher.Checked)
                        CallVoucher((int)eMenuID.PurchaseVoucher, "Purchase Voucher", Properties.Resources.Purchase_Invoice1);
                    else
                        CallPurchaseInvoice();
                    break;
                case (int)VoucherType.DebitNote:
                    if (chkShowVoucher.Checked)
                        CallVoucher((int)eMenuID.DebitNoteVoucher, "Debit Note Voucher", Properties.Resources.DebitNote);
                    else
                        CallDebitNote();
                    break;
                case (int)VoucherType.Sale:
                    if (chkShowVoucher.Checked)
                        CallVoucher((int)eMenuID.SalesVoucher, "Sales Voucher", Properties.Resources.Sales_invoice1);
                    else
                    {
                        if (dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["OperationTypeID"].Value.ToInt32() == (int)OperationType.POS)
                            CallPOS();
                        else if (dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["OperationTypeID"].Value.ToInt32() == (int)OperationType.SalesInvoice)
                            CallSalesInvoice();
                    }
                    break;
                case (int)VoucherType.CreditNote:
                    if (chkShowVoucher.Checked)
                        CallVoucher((int)eMenuID.CreditNoteVoucher, "Credit Note Voucher", Properties.Resources.Sales_Return1);
                    else
                        CallCreditNote();
                    break;
            }

            // Fill Changed Details
            switch (lvDisplay.SelectedItems[0].Tag.ToInt32())
            {
                case ((int)eMenuID.GeneralLedger):
                    FillGridDrillDown((int)eMenuID.GeneralLedger, datDrilDown.Rows[datDrilDown.Rows.Count - 1]["AccountID"].ToInt32(), false);
                    DisplayGeneralLedger();
                    GridFieldVisibility((int)eMenuID.GeneralLedger);
                    break;
                case ((int)eMenuID.DayBook):
                    FillGridDrillDown((int)eMenuID.DayBook, datDrilDown.Rows[datDrilDown.Rows.Count - 1]["AccountID"].ToInt32(), false);
                    pnlLedgerSummary.Visible = false;
                    GridFieldVisibility((int)eMenuID.DayBook);
                    break;
            }
            // End
        }

        private void CallVoucher(int intMenuID, string strHeader, object objICO)
        {
            frmJournalVoucher objJournalVoucher = new frmJournalVoucher();
            objJournalVoucher.Icon = ((System.Drawing.Icon)(objICO));
            objJournalVoucher.decDrilVoucherID = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["VoucherID"].Value.ToDecimal();
            switch (dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["VoucherTypeID"].Value.ToInt32())
            {
                case (int)VoucherType.Payment:
                    objJournalVoucher.TempMenuID = (int)eMenuID.PaymentVoucher;
                    break;
                case (int)VoucherType.Receipt:
                    objJournalVoucher.TempMenuID = (int)eMenuID.ReceiptVoucher;
                    break;
                case (int)VoucherType.Contra:
                    objJournalVoucher.TempMenuID = (int)eMenuID.ContraVoucher;
                    break;
                case (int)VoucherType.Journal:
                    objJournalVoucher.TempMenuID = (int)eMenuID.JournalVoucher;
                    break;
                case (int)VoucherType.Purchase:
                    objJournalVoucher.TempMenuID = (int)eMenuID.PurchaseVoucher;
                    break;
                case (int)VoucherType.DebitNote:
                    objJournalVoucher.TempMenuID = (int)eMenuID.DebitNoteVoucher;
                    break;
                case (int)VoucherType.Sale:
                    objJournalVoucher.TempMenuID = (int)eMenuID.SalesVoucher;
                    break;
                case (int)VoucherType.CreditNote:
                    objJournalVoucher.TempMenuID = (int)eMenuID.CreditNoteVoucher;
                    break;
            }
            objJournalVoucher.Text = strHeader;
            objJournalVoucher.MdiParent = this.MdiParent;//ClsMainSettings.objFrmMain;
            objJournalVoucher.WindowState = FormWindowState.Maximized;
            objJournalVoucher.Show();
        }

        private void DrilDownIsGroupOrLedger(bool blnIsGroup)
        {
            string strTitleText = "";
            int intMenuID = 0;

            switch (lvDisplay.SelectedItems[0].Tag.ToInt32())
            {
                case ((int)eMenuID.ProfitAndLossAccount):
                case ((int)eMenuID.BalanceSheet):
                    if (blnIsGroup == true)
                    {
                        if (dgvAccountDetails.CurrentCell.ColumnIndex == 1/*Debit*/ ||
                                dgvAccountDetails.CurrentCell.ColumnIndex == 2/*DebitAmount*/)
                            strTitleText = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["Debit"].Value.ToString() + " - Group Summary";
                        else if (dgvAccountDetails.CurrentCell.ColumnIndex == 6/*Credit*/ ||
                                dgvAccountDetails.CurrentCell.ColumnIndex == 7/*CreditAmount*/)
                            strTitleText = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["Credit"].Value.ToString() + " - Group Summary";

                        datDrilDown.Rows.Add();
                        datDrilDown.Rows[datDrilDown.Rows.Count - 1]["MenuID"] = (int)eMenuID.AccountsSummary;

                        if (dgvAccountDetails.CurrentCell.ColumnIndex == 1/*Debit*/ ||
                                dgvAccountDetails.CurrentCell.ColumnIndex == 2/*DebitAmount*/)
                        {
                            datDrilDown.Rows[datDrilDown.Rows.Count - 1]["AccountID"] = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["DebitAccountID"].Value.ToInt32();
                            datDrilDown.Rows[datDrilDown.Rows.Count - 1]["IsGroup"] = "true";
                        }
                        else if (dgvAccountDetails.CurrentCell.ColumnIndex == 6/*Credit*/ ||
                                dgvAccountDetails.CurrentCell.ColumnIndex == 7/*CreditAmount*/)
                            strTitleText = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["Credit"].Value.ToString() + " - Group Summary";
                        {
                            datDrilDown.Rows[datDrilDown.Rows.Count - 1]["AccountID"] = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["CreditAccountID"].Value.ToInt32();
                            datDrilDown.Rows[datDrilDown.Rows.Count - 1]["IsGroup"] = "true";
                        }

                        if ((dgvAccountDetails.CurrentCell.ColumnIndex == 1/*Debit*/ ||
                                dgvAccountDetails.CurrentCell.ColumnIndex == 2/*DebitAmount*/) &&
                               dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["DebitAccountID"].Value.ToInt32() > 0)
                            FillGridDrillDown((int)eMenuID.AccountsSummary,
                                            dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["DebitAccountID"].Value.ToInt32(), true);
                        else if ((dgvAccountDetails.CurrentCell.ColumnIndex == 6/*Credit*/ ||
                                dgvAccountDetails.CurrentCell.ColumnIndex == 7/*CreditAmount*/)&&
                               dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["CreditAccountID"].Value.ToInt32() > 0)
                            FillGridDrillDown((int)eMenuID.AccountsSummary,
                                            dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["CreditAccountID"].Value.ToInt32(), true);

                        DisplaySummary();
                        intMenuID = (int)eMenuID.AccountsSummary;
                    }
                    else
                    {
                        if (dgvAccountDetails.CurrentCell.ColumnIndex == 1/*Debit*/ ||
                                dgvAccountDetails.CurrentCell.ColumnIndex == 2/*DebitAmount*/)
                            strTitleText = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["Debit"].Value.ToString() + " - Ledger";
                        else if (dgvAccountDetails.CurrentCell.ColumnIndex == 6/*Credit*/ ||
                                dgvAccountDetails.CurrentCell.ColumnIndex == 7/*CreditAmount*/)
                            strTitleText = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["Credit"].Value.ToString() + " - Ledger";

                        datDrilDown.Rows.Add();
                        datDrilDown.Rows[datDrilDown.Rows.Count - 1]["MenuID"] = (int)eMenuID.GeneralLedger;

                        if (dgvAccountDetails.CurrentCell.ColumnIndex == 1/*Debit*/ ||
                                dgvAccountDetails.CurrentCell.ColumnIndex == 2/*DebitAmount*/)
                            datDrilDown.Rows[datDrilDown.Rows.Count - 1]["AccountID"] = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["DebitAccountID"].Value.ToInt32();
                        else if (dgvAccountDetails.CurrentCell.ColumnIndex == 6/*Credit*/ ||
                                dgvAccountDetails.CurrentCell.ColumnIndex == 7/*CreditAmount*/)
                            datDrilDown.Rows[datDrilDown.Rows.Count - 1]["AccountID"] = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["CreditAccountID"].Value.ToInt32();
                        
                        datDrilDown.Rows[datDrilDown.Rows.Count - 1]["IsGroup"] = "false";

                        if ((dgvAccountDetails.CurrentCell.ColumnIndex == 1/*Debit*/ ||
                                dgvAccountDetails.CurrentCell.ColumnIndex == 2/*DebitAmount*/) &&
                               dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["DebitAccountID"].Value.ToInt32() > 0)
                            FillGridDrillDown((int)eMenuID.GeneralLedger,
                                            dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["DebitAccountID"].Value.ToInt32(), false);
                        else if ((dgvAccountDetails.CurrentCell.ColumnIndex == 6/*Credit*/ ||
                                dgvAccountDetails.CurrentCell.ColumnIndex == 7/*CreditAmount*/) &&
                               dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["CreditAccountID"].Value.ToInt32() > 0)
                            FillGridDrillDown((int)eMenuID.GeneralLedger,
                                            dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["CreditAccountID"].Value.ToInt32(), false);

                        DisplayGeneralLedger();
                        intMenuID = (int)eMenuID.GeneralLedger;
                    }
                    break;

                default:
                    if (blnIsGroup == true)
                    {
                        strTitleText = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["Particulars"].Value.ToString() + " - Group Summary";

                        datDrilDown.Rows.Add();
                        datDrilDown.Rows[datDrilDown.Rows.Count - 1]["MenuID"] = (int)eMenuID.AccountsSummary;
                        datDrilDown.Rows[datDrilDown.Rows.Count - 1]["AccountID"] = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["AccountID"].Value.ToInt32();
                        datDrilDown.Rows[datDrilDown.Rows.Count - 1]["IsGroup"] = "true";

                        FillGridDrillDown((int)eMenuID.AccountsSummary,
                                        dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["AccountID"].Value.ToInt32(), true);
                        DisplaySummary();
                        intMenuID = (int)eMenuID.AccountsSummary;
                    }
                    else
                    {
                        strTitleText = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["Particulars"].Value.ToString() + " - Ledger";

                        datDrilDown.Rows.Add();
                        datDrilDown.Rows[datDrilDown.Rows.Count - 1]["MenuID"] = (int)eMenuID.GeneralLedger;
                        datDrilDown.Rows[datDrilDown.Rows.Count - 1]["AccountID"] = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["AccountID"].Value.ToInt32();
                        datDrilDown.Rows[datDrilDown.Rows.Count - 1]["IsGroup"] = "false";

                        FillGridDrillDown((int)eMenuID.GeneralLedger,
                                        dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["AccountID"].Value.ToInt32(), false);
                        DisplayGeneralLedger();
                        intMenuID = (int)eMenuID.GeneralLedger;
                    }
                    break;
            }
            
            lvDisplay.SelectedItems[0].Tag = intMenuID;
            GridFieldVisibility(intMenuID);
            pnlLedgerSummary.Visible = true;

            if (dtpFromDate.Value == dtpToDate.Value)
                lblSearchTitle.Text = strTitleText + " For " + dtpFromDate.Value.ToString("dd MMM yyyy");
            else
                lblSearchTitle.Text = strTitleText + " From " + dtpFromDate.Value.ToString("dd MMM yyyy") + " To " + dtpToDate.Value.ToString("dd MMM yyyy");

            datDrilDown.Rows[datDrilDown.Rows.Count - 1]["Description"] = lblSearchTitle.Text;
        }

        private void FillGridDrillDown(int intMenuID, int intRefID, bool blnIsGroup)
        {            
            MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID = intMenuID;
            MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempAccountID = intRefID;
            MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempCompanyID = cboSearchCompany.SelectedValue.ToInt32();

            if (MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.GeneralLedger &&
                intMenuID == (int)eMenuID.GeneralLedger && cboSearchCompany.SelectedIndex >= 0)
                dtpFromDate.Value = MobjclsBLLAccountSummary.GetFinancialStartDate(cboSearchCompany.SelectedValue.ToInt32(), ClsCommonSettings.GetServerDate());
            
            MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.dtpTempFromDate = dtpFromDate.Value.ToString("dd MMM yyyy").ToDateTime();
            MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.dtpTempToDate = dtpToDate.Value.ToString("dd MMM yyyy").ToDateTime();
            MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.IsTempGroup = blnIsGroup;

            ClearControls();
            dtsTemp.Clear();
            dtsTemp = MobjclsBLLAccountSummary.DisplayAccountsSummaryReoprt();
            dgvAccountDetails.DataSource = dtsTemp.Tables[0];

            DataGridViewCellStyle gvStyle = new DataGridViewCellStyle();
            gvStyle.Font = new Font(DataGridView.DefaultFont, FontStyle.Italic);
            //gvStyle.ForeColor = Color.Black;

            if (MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.ProfitAndLossAccount ||
                 MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.BalanceSheet)
            {
                dgvAccountDetails.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;

                foreach (DataGridViewRow row in dgvAccountDetails.Rows)
                {
                    if (row.Cells["DebitIsGroup"].Value.ToBoolean() || row.Cells["DebitAccountID"].Value.ToInt32() == (int)Accounts.OpeningStock)
                    {
                        foreach (DataGridViewCell cell in row.Cells)
                        {
                            if (cell.ColumnIndex == 1 || cell.ColumnIndex == 2)
                                cell.Style.ApplyStyle(gvStyle);
                        }
                    }
                    if (row.Cells["CreditIsGroup"].Value.ToBoolean())
                    {
                        foreach (DataGridViewCell cell in row.Cells)
                        {
                            if (cell.ColumnIndex == 6 || cell.ColumnIndex == 7)
                                cell.Style.ApplyStyle(gvStyle);
                        }
                    }
                }
            }
            else if (MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.DayBook)
            {
                dgvAccountDetails.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

                foreach (DataGridViewRow row in dgvAccountDetails.Rows)
                {
                    if (row.Cells["VoucherDate"].Value.ToStringCustom() == "" && row.Cells["VoucherType"].Value.ToStringCustom() == "" &&
                        row.Cells["VoucherNo"].Value.ToStringCustom() == "")
                    {
                        foreach (DataGridViewCell cell in row.Cells)
                            cell.Style.ApplyStyle(gvStyle);
                    }
                }
            }
            else if (MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.AccountsSummary)
            {
                dgvAccountDetails.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

                foreach (DataGridViewRow row in dgvAccountDetails.Rows)
                {
                    if (row.Cells["ParentID"].Value.ToInt32() == (int)AccountGroups.Stockinhand)
                    {
                        foreach (DataGridViewCell cell in row.Cells)
                            cell.Style.ApplyStyle(gvStyle);
                    }
                }
            }
            else
                dgvAccountDetails.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            if (dgvAccountDetails.Columns.Contains("VoucherDate"))
            {
                dgvAccountDetails.Columns["VoucherDate"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvAccountDetails.Columns["VoucherDate"].ReadOnly = true;
            }
            if (dgvAccountDetails.Columns.Contains("VoucherType"))
            {
                dgvAccountDetails.Columns["VoucherType"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvAccountDetails.Columns["VoucherType"].ReadOnly = true;
            }
            if (dgvAccountDetails.Columns.Contains("VoucherNo"))
            {
                dgvAccountDetails.Columns["VoucherNo"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvAccountDetails.Columns["VoucherNo"].ReadOnly = true;
            }
            if (dgvAccountDetails.Columns.Contains("Particulars"))
            {
                dgvAccountDetails.Columns["Particulars"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvAccountDetails.Columns["Particulars"].ReadOnly = true;
            }
            if (dgvAccountDetails.Columns.Contains("DebitAmount"))
            {
                dgvAccountDetails.Columns["DebitAmount"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvAccountDetails.Columns["DebitAmount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvAccountDetails.Columns["DebitAmount"].ReadOnly = true;
            }
            if (dgvAccountDetails.Columns.Contains("CreditAmount"))
            {
                dgvAccountDetails.Columns["CreditAmount"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvAccountDetails.Columns["CreditAmount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvAccountDetails.Columns["CreditAmount"].ReadOnly = true;
            }
            if (dgvAccountDetails.Columns.Contains("Debit"))
            {
                dgvAccountDetails.Columns["Debit"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvAccountDetails.Columns["Debit"].ReadOnly = true;
                dgvAccountDetails.Columns["Debit"].HeaderText = "Particulars";
            }
            if (dgvAccountDetails.Columns.Contains("Credit"))
            {
                dgvAccountDetails.Columns["Credit"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvAccountDetails.Columns["Credit"].ReadOnly = true;
                dgvAccountDetails.Columns["Credit"].HeaderText = "Particulars";
            }
            if (dgvAccountDetails.Columns.Contains("ForSeperation"))
            {
                dgvAccountDetails.Columns["ForSeperation"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvAccountDetails.Columns["ForSeperation"].HeaderText = "";
                dgvAccountDetails.Columns["ForSeperation"].ReadOnly = true;
            }

            if (MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.ProfitAndLossAccount ||
             MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.BalanceSheet)
            {
                if (dgvAccountDetails.Columns.Contains("DebitAmount"))
                    dgvAccountDetails.Columns["DebitAmount"].HeaderText = "";
                if (dgvAccountDetails.Columns.Contains("CreditAmount"))
                    dgvAccountDetails.Columns["CreditAmount"].HeaderText = "";
            }

            if (MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.BalanceSheet)
            {
                if (dgvAccountDetails.Columns.Contains("Debit"))
                    dgvAccountDetails.Columns["Debit"].HeaderText = "Liabilities";
                if (dgvAccountDetails.Columns.Contains("Credit"))
                    dgvAccountDetails.Columns["Credit"].HeaderText = "Assets";
            }

            dgvAccountDetails.ClearSelection();
            btnPrint.Enabled = true;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (datDrilDown.Rows.Count - intDrilDown >= 0)
            {
                LoadDrilDownSearch();                
                intDrilDown++;
            }
        }

        private void LoadDrilDownSearch()
        {
            string strTitleText = "";
            
            ClearControls();
            lvDisplay.SelectedItems[0].Tag = datDrilDown.Rows[datDrilDown.Rows.Count - intDrilDown]["MenuID"].ToInt32();

            if (datDrilDown.Rows[datDrilDown.Rows.Count - intDrilDown]["MenuID"].ToInt32() == (int)eMenuID.BalanceSheet)
                dtpFromDate.Value = ClsCommonSettings.GetServerDate();

            FillGridDrillDown(datDrilDown.Rows[datDrilDown.Rows.Count - intDrilDown]["MenuID"].ToInt32(),
                    datDrilDown.Rows[datDrilDown.Rows.Count - intDrilDown]["AccountID"].ToInt32(),
                    datDrilDown.Rows[datDrilDown.Rows.Count - intDrilDown]["IsGroup"].ToBoolean());
            GridFieldVisibility(datDrilDown.Rows[datDrilDown.Rows.Count - intDrilDown]["MenuID"].ToInt32());
            pnlLedgerSummary.Visible = true;

            chkShowVoucher.Visible = false;
            chkShowVoucher.Checked = false;

            switch (datDrilDown.Rows[datDrilDown.Rows.Count - intDrilDown]["MenuID"].ToInt32())
            {
                case ((int)eMenuID.AccountsSummary):
                    strTitleText = strTempDescription + " - Group Summary";
                    DisplaySummary();
                    break;
                case ((int)eMenuID.GeneralLedger):
                    strTitleText = strTempDescription + " - Ledger";
                    DisplayGeneralLedger();
                    break;
                case ((int)eMenuID.DayBook):
                    strTitleText = "Day Book";
                    pnlLedgerSummary.Visible = false;
                    break;
                case ((int)eMenuID.CashBook):
                    strTitleText = "Cash Book";
                    DisplaySummary();
                    break;
                case ((int)eMenuID.TrialBalance):
                    strTitleText = "Trial Balance";
                    DisplaySummary();
                    break;
                case ((int)eMenuID.ProfitAndLossAccount):
                    strTitleText = "Profit And Loss Account";
                    pnlLedgerSummary.Visible = false;
                    break;
                case ((int)eMenuID.BalanceSheet):
                    strTitleText = "Balance Sheet";
                    pnlLedgerSummary.Visible = false;                    
                    break;
            }
            lblSearchTitle.Text = datDrilDown.Rows[datDrilDown.Rows.Count - intDrilDown]["Description"].ToString();
        }

        private void cboSearchCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboSearchCompany.SelectedIndex >= 0)
                lblCurrency.Text = MobjclsBLLAccountSummary.GetCompanyCurrency(cboSearchCompany.SelectedValue.ToInt32());
            else
                lblCurrency.Text = "Currency : ";
            btnPrint.Enabled = false;
        }

        private void frmAccountSummary_Shown(object sender, EventArgs e)
        {            
            switch (SummaryId)
            {
                case ((int)eMenuID.AccountsSummary):                                      
                    lvDisplay.Items[1].Selected = true;
                    break;
                case ((int)eMenuID.GeneralLedger):
                    lvDisplay.Items[2].Selected = true;
                    break;
                case ((int)eMenuID.DayBook):
                    lvDisplay.Items[3].Selected = true;
                    break;
                case ((int)eMenuID.CashBook):                               
                    lvDisplay.Items[4].Selected = true;
                    break;
                case ((int)eMenuID.TrialBalance):                    
                    lvDisplay.Items[0].Selected = true;
                    break;
                case ((int)eMenuID.ProfitAndLossAccount):
                    lvDisplay.Items[5].Selected = true;
                    break;
                case ((int)eMenuID.BalanceSheet):
                    lvDisplay.Items[6].Selected = true;
                    break;
            }
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (cboSearchCompany.SelectedIndex >= 0)
            {
                if (lvDisplay.SelectedItems.Count < 1)
                return;

                if (lvDisplay.SelectedItems[0].Tag.ToInt32() == (int)eMenuID.DayBook)
                    dtpToDate.Value = dtpFromDate.Value;
                else
                {
                    DateTime dtpFDate = MobjclsBLLAccountSummary.GetCompanyBookStartDate(cboSearchCompany.SelectedValue.ToInt32());
                    if (dtpFromDate.Value < dtpFDate)
                        dtpFromDate.Value = dtpFDate;

                    DateTime dtpTDate = MobjclsBLLAccountSummary.GetFinancialStartDate(cboSearchCompany.SelectedValue.ToInt32(),
                        dtpFromDate.Value.AddYears(1)).AddDays(-1);
                    if (dtpToDate.Value > dtpTDate || dtpFromDate.Value > dtpToDate.Value)
                        dtpToDate.Value = dtpTDate;
                }
            }
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            if (cboSearchCompany.SelectedIndex >= 0)
            {
                if (lvDisplay.SelectedItems[0].Tag.ToInt32() == (int)eMenuID.BalanceSheet ||
                    MobjclsBLLAccountSummary.MobjclsDTOAccountSummary.intTempMenuID == (int)eMenuID.BalanceSheet)
                    dtpFromDate.Value = dtpToDate.Value;
                else
                {
                    DateTime dtpFDate = MobjclsBLLAccountSummary.GetFinancialStartDate(cboSearchCompany.SelectedValue.ToInt32(), dtpToDate.Value);
                    if (dtpFromDate.Value < dtpFDate || dtpFromDate.Value > dtpToDate.Value)
                        dtpFromDate.Value = dtpFDate;
                }
            }
        }

        private void CallPurchaseInvoice()
        {
            FrmPurchaseInvoice objPurchaseInvoice = null;
            try
            {
                objPurchaseInvoice = new FrmPurchaseInvoice(4);
                objPurchaseInvoice.Text = "Purchase Invoice   " + (this.MdiChildren.Length + 1);
                objPurchaseInvoice.PlngReferenceID = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["ReferenceID"].Value.ToInt64();
                objPurchaseInvoice.MdiParent = this.MdiParent;
                objPurchaseInvoice.WindowState = FormWindowState.Maximized;
                objPurchaseInvoice.Show();
                objPurchaseInvoice.Update();
            }
            catch (OutOfMemoryException ex)
            {
                objPurchaseInvoice.Dispose();
                System.GC.Collect();
                CallPurchaseInvoice();
            }
        }

        private void CallDebitNote()
        {
            FrmDebitNote objDebitNote = null;
            try
            {
                objDebitNote = new FrmDebitNote();
                objDebitNote.Text = "Debit Note " + (this.MdiChildren.Length + 1);
                objDebitNote.PlngReferenceID = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["ReferenceID"].Value.ToInt64();
                objDebitNote.WindowState = FormWindowState.Maximized;
                objDebitNote.MdiParent = this.MdiParent;
                objDebitNote.Update();
                objDebitNote.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objDebitNote.Dispose();
                System.GC.Collect();
                CallDebitNote();
            }
        }

        private void CallPOS()
        {
            FrmPOS objPOS = null;
            try
            {
                objPOS = new FrmPOS();
                objPOS.Text = "POS " + (this.MdiChildren.Length + 1);
                objPOS.PlngReferenceID = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["ReferenceID"].Value.ToInt64();
                objPOS.WindowState = FormWindowState.Maximized;
                objPOS.MdiParent = this.MdiParent;
                objPOS.Update();
                objPOS.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objPOS.Dispose();
                System.GC.Collect();
                CallPOS();
            }
        }

        private void CallSalesInvoice()
        {
            FrmSalesInvoice objSalesInvoice = null;
            try
            {
                objSalesInvoice = new FrmSalesInvoice(3, 0);
                objSalesInvoice.Text = "Sales Invoice" + (this.MdiChildren.Length + 1);
                objSalesInvoice.plngReferenceID = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["ReferenceID"].Value.ToInt64();
                objSalesInvoice.WindowState = FormWindowState.Maximized;
                objSalesInvoice.MdiParent = this.MdiParent;
                objSalesInvoice.Update();
                objSalesInvoice.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objSalesInvoice.Dispose();
                System.GC.Collect();
                CallSalesInvoice();
            }
        }

        private void CallCreditNote()
        {
            FrmCreditNote objSalesReturn = null;
            try
            {
                objSalesReturn = new FrmCreditNote();
                objSalesReturn.Text = "Credit Note " + (this.MdiChildren.Length + 1);
                objSalesReturn.PlngReferenceID = dgvAccountDetails.Rows[dgvAccountDetails.CurrentRow.Index].Cells["ReferenceID"].Value.ToInt64();
                objSalesReturn.WindowState = FormWindowState.Maximized;
                objSalesReturn.MdiParent = this.MdiParent;
                objSalesReturn.Update();
                objSalesReturn.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objSalesReturn.Dispose();
                System.GC.Collect();
                CallCreditNote();
            }
        }

        private void dgvAccountDetails_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > 0)
            {
                if (dgvAccountDetails.Columns.Contains("ForSeperation"))
                {
                    if (e.ColumnIndex == dgvAccountDetails.Columns["ForSeperation"].Index)
                        dgvAccountDetails.ClearSelection();
                }
            }
        }

        private void dgvAccountDetails_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > 0)
            {
                if (dgvAccountDetails.Columns.Contains("ForSeperation"))
                {
                    if (e.ColumnIndex == dgvAccountDetails.Columns["ForSeperation"].Index)
                        dgvAccountDetails.ClearSelection();
                }
            }
        }
    }
}
