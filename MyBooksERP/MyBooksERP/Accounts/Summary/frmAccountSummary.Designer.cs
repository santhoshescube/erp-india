﻿namespace MyBooksERP
{
    partial class frmAccountSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAccountSummary));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("Trial Balance", "Trial Balance.png");
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("Group Summary", "Acconut summary.png");
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("General Ledger", "General Ledger.png");
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("Day Book", "Day book.png");
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("Cash Book", "Cash Book (report).png");
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("Profit and Loss", "Profot & Loss Report.png");
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("Balance Sheet", "Balancesheet.png");
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.imglstDisplay = new System.Windows.Forms.ImageList(this.components);
            this.pnlChart = new DevComponents.DotNetBar.PanelEx();
            this.ImlAccounts = new System.Windows.Forms.ImageList(this.components);
            this.nodeConnector1 = new DevComponents.AdvTree.NodeConnector();
            this.elementStyle1 = new DevComponents.DotNetBar.ElementStyle();
            this.lblCreditClosing = new DevComponents.DotNetBar.LabelX();
            this.lblCreditTotal = new DevComponents.DotNetBar.LabelX();
            this.lblCreditOpening = new DevComponents.DotNetBar.LabelX();
            this.lblDebitClosing = new DevComponents.DotNetBar.LabelX();
            this.lblDebitTotal = new DevComponents.DotNetBar.LabelX();
            this.lblDebitOpening = new DevComponents.DotNetBar.LabelX();
            this.lblCurrency = new DevComponents.DotNetBar.LabelX();
            this.lblClosingBalance = new DevComponents.DotNetBar.LabelX();
            this.lblCurrentTotal = new DevComponents.DotNetBar.LabelX();
            this.lblOpeningBalance = new DevComponents.DotNetBar.LabelX();
            this.btnSearch = new DevComponents.DotNetBar.ButtonX();
            this.btnPrint = new DevComponents.DotNetBar.ButtonX();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.dgvAccountDetails = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.TvAccountsHead = new DevComponents.AdvTree.AdvTree();
            this.node1 = new DevComponents.AdvTree.Node();
            this.nodeConnector2 = new DevComponents.AdvTree.NodeConnector();
            this.elementStyle2 = new DevComponents.DotNetBar.ElementStyle();
            this.lvDisplay = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.btnDisplay = new DevComponents.DotNetBar.ButtonItem();
            this.lblToDate = new DevComponents.DotNetBar.LabelX();
            this.lblFromDate = new DevComponents.DotNetBar.LabelX();
            this.lblCompanySearch = new DevComponents.DotNetBar.LabelX();
            this.pnlLeftChart = new DevComponents.DotNetBar.PanelEx();
            this.expnlChartofAccounts = new DevComponents.DotNetBar.ExpandablePanel();
            this.grpnlSorting = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.cboDisplayFilter = new System.Windows.Forms.ComboBox();
            this.lblFilter = new DevComponents.DotNetBar.LabelX();
            this.cboSearchAccount = new System.Windows.Forms.ComboBox();
            this.lblSearchAc = new DevComponents.DotNetBar.LabelX();
            this.expnlSearch = new DevComponents.DotNetBar.ExpandablePanel();
            this.chkShowVoucher = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cboSearchCompany = new System.Windows.Forms.ComboBox();
            this.expnlGatewayofAccounts = new DevComponents.DotNetBar.ExpandablePanel();
            this.pnlReport = new DevComponents.DotNetBar.PanelEx();
            this.pnlGridWithSummary = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.groupPanel5 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnBack = new DevComponents.DotNetBar.ButtonX();
            this.lblSearchTitle = new DevComponents.DotNetBar.LabelX();
            this.pnlLedgerSummary = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.esLeftChart = new DevComponents.DotNetBar.ExpandableSplitter();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccountDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TvAccountsHead)).BeginInit();
            this.pnlLeftChart.SuspendLayout();
            this.expnlChartofAccounts.SuspendLayout();
            this.grpnlSorting.SuspendLayout();
            this.expnlSearch.SuspendLayout();
            this.expnlGatewayofAccounts.SuspendLayout();
            this.pnlReport.SuspendLayout();
            this.pnlGridWithSummary.SuspendLayout();
            this.groupPanel5.SuspendLayout();
            this.pnlLedgerSummary.SuspendLayout();
            this.SuspendLayout();
            // 
            // imglstDisplay
            // 
            this.imglstDisplay.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imglstDisplay.ImageStream")));
            this.imglstDisplay.TransparentColor = System.Drawing.Color.Transparent;
            this.imglstDisplay.Images.SetKeyName(0, "Trial Balance.png");
            this.imglstDisplay.Images.SetKeyName(1, "Acconut summary.png");
            this.imglstDisplay.Images.SetKeyName(2, "General Ledger.png");
            this.imglstDisplay.Images.SetKeyName(3, "Day book.png");
            this.imglstDisplay.Images.SetKeyName(4, "Cash Book (report).png");
            this.imglstDisplay.Images.SetKeyName(5, "Profot & Loss Report.png");
            this.imglstDisplay.Images.SetKeyName(6, "Balancesheet.png");
            // 
            // pnlChart
            // 
            this.pnlChart.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlChart.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlChart.Location = new System.Drawing.Point(46, 143);
            this.pnlChart.Name = "pnlChart";
            this.pnlChart.Size = new System.Drawing.Size(194, 153);
            this.pnlChart.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlChart.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlChart.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlChart.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlChart.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlChart.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlChart.Style.GradientAngle = 90;
            this.pnlChart.TabIndex = 54;
            // 
            // ImlAccounts
            // 
            this.ImlAccounts.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImlAccounts.ImageStream")));
            this.ImlAccounts.TransparentColor = System.Drawing.Color.Transparent;
            this.ImlAccounts.Images.SetKeyName(0, "ExpandAll.png");
            this.ImlAccounts.Images.SetKeyName(1, "Accounts.png");
            // 
            // nodeConnector1
            // 
            this.nodeConnector1.LineColor = System.Drawing.SystemColors.ControlText;
            // 
            // elementStyle1
            // 
            this.elementStyle1.Class = "";
            this.elementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.elementStyle1.Name = "elementStyle1";
            this.elementStyle1.TextColor = System.Drawing.SystemColors.ControlText;
            // 
            // lblCreditClosing
            // 
            this.lblCreditClosing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblCreditClosing.BackgroundStyle.Class = "";
            this.lblCreditClosing.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCreditClosing.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreditClosing.Location = new System.Drawing.Point(842, 51);
            this.lblCreditClosing.Name = "lblCreditClosing";
            this.lblCreditClosing.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblCreditClosing.Size = new System.Drawing.Size(120, 18);
            this.lblCreditClosing.TabIndex = 74;
            this.lblCreditClosing.Text = "0";
            // 
            // lblCreditTotal
            // 
            this.lblCreditTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblCreditTotal.BackgroundStyle.Class = "";
            this.lblCreditTotal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCreditTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreditTotal.Location = new System.Drawing.Point(842, 27);
            this.lblCreditTotal.Name = "lblCreditTotal";
            this.lblCreditTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblCreditTotal.Size = new System.Drawing.Size(120, 18);
            this.lblCreditTotal.TabIndex = 73;
            this.lblCreditTotal.Text = "0";
            // 
            // lblCreditOpening
            // 
            this.lblCreditOpening.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblCreditOpening.BackgroundStyle.Class = "";
            this.lblCreditOpening.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCreditOpening.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreditOpening.Location = new System.Drawing.Point(842, 5);
            this.lblCreditOpening.Name = "lblCreditOpening";
            this.lblCreditOpening.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblCreditOpening.Size = new System.Drawing.Size(120, 18);
            this.lblCreditOpening.TabIndex = 72;
            this.lblCreditOpening.Text = "0";
            // 
            // lblDebitClosing
            // 
            this.lblDebitClosing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblDebitClosing.BackgroundStyle.Class = "";
            this.lblDebitClosing.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDebitClosing.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDebitClosing.Location = new System.Drawing.Point(712, 51);
            this.lblDebitClosing.Name = "lblDebitClosing";
            this.lblDebitClosing.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDebitClosing.Size = new System.Drawing.Size(120, 18);
            this.lblDebitClosing.TabIndex = 71;
            this.lblDebitClosing.Text = "0";
            // 
            // lblDebitTotal
            // 
            this.lblDebitTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblDebitTotal.BackgroundStyle.Class = "";
            this.lblDebitTotal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDebitTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDebitTotal.Location = new System.Drawing.Point(712, 27);
            this.lblDebitTotal.Name = "lblDebitTotal";
            this.lblDebitTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDebitTotal.Size = new System.Drawing.Size(120, 18);
            this.lblDebitTotal.TabIndex = 70;
            this.lblDebitTotal.Text = "0";
            // 
            // lblDebitOpening
            // 
            this.lblDebitOpening.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblDebitOpening.BackgroundStyle.Class = "";
            this.lblDebitOpening.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDebitOpening.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDebitOpening.Location = new System.Drawing.Point(712, 5);
            this.lblDebitOpening.Name = "lblDebitOpening";
            this.lblDebitOpening.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDebitOpening.Size = new System.Drawing.Size(120, 18);
            this.lblDebitOpening.TabIndex = 69;
            this.lblDebitOpening.Text = "0";
            // 
            // lblCurrency
            // 
            this.lblCurrency.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblCurrency.AutoSize = true;
            // 
            // 
            // 
            this.lblCurrency.BackgroundStyle.Class = "";
            this.lblCurrency.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCurrency.Location = new System.Drawing.Point(227, 35);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(59, 15);
            this.lblCurrency.TabIndex = 68;
            this.lblCurrency.Text = "Currency : ";
            // 
            // lblClosingBalance
            // 
            this.lblClosingBalance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblClosingBalance.BackgroundStyle.Class = "";
            this.lblClosingBalance.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblClosingBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClosingBalance.Location = new System.Drawing.Point(600, 51);
            this.lblClosingBalance.Name = "lblClosingBalance";
            this.lblClosingBalance.Size = new System.Drawing.Size(106, 18);
            this.lblClosingBalance.TabIndex = 67;
            this.lblClosingBalance.Text = "Closing Balance";
            // 
            // lblCurrentTotal
            // 
            this.lblCurrentTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblCurrentTotal.BackgroundStyle.Class = "";
            this.lblCurrentTotal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCurrentTotal.Location = new System.Drawing.Point(600, 27);
            this.lblCurrentTotal.Name = "lblCurrentTotal";
            this.lblCurrentTotal.Size = new System.Drawing.Size(106, 18);
            this.lblCurrentTotal.TabIndex = 66;
            this.lblCurrentTotal.Text = "Current Total";
            // 
            // lblOpeningBalance
            // 
            this.lblOpeningBalance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblOpeningBalance.BackgroundStyle.Class = "";
            this.lblOpeningBalance.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblOpeningBalance.Location = new System.Drawing.Point(600, 5);
            this.lblOpeningBalance.Name = "lblOpeningBalance";
            this.lblOpeningBalance.Size = new System.Drawing.Size(106, 18);
            this.lblOpeningBalance.TabIndex = 65;
            this.lblOpeningBalance.Text = "Opening Balance";
            // 
            // btnSearch
            // 
            this.btnSearch.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSearch.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSearch.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSearch.Image = global::MyBooksERP.Properties.Resources.Search;
            this.btnSearch.Location = new System.Drawing.Point(201, 82);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(80, 25);
            this.btnSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSearch.TabIndex = 59;
            this.btnSearch.Text = "&Search";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.btnPrint.Location = new System.Drawing.Point(831, 0);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(65, 19);
            this.btnPrint.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPrint.TabIndex = 58;
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // dtpToDate
            // 
            this.dtpToDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpToDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(71, 87);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(106, 20);
            this.dtpToDate.TabIndex = 56;
            this.dtpToDate.ValueChanged += new System.EventHandler(this.dtpToDate_ValueChanged);
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpFromDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(71, 61);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(106, 20);
            this.dtpFromDate.TabIndex = 54;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            // 
            // dgvAccountDetails
            // 
            this.dgvAccountDetails.AllowUserToAddRows = false;
            this.dgvAccountDetails.AllowUserToDeleteRows = false;
            this.dgvAccountDetails.AllowUserToResizeRows = false;
            this.dgvAccountDetails.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvAccountDetails.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvAccountDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAccountDetails.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAccountDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAccountDetails.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvAccountDetails.Location = new System.Drawing.Point(0, 25);
            this.dgvAccountDetails.Name = "dgvAccountDetails";
            this.dgvAccountDetails.RowHeadersVisible = false;
            this.dgvAccountDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvAccountDetails.Size = new System.Drawing.Size(969, 412);
            this.dgvAccountDetails.TabIndex = 1;
            this.dgvAccountDetails.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAccountDetails_CellClick);
            this.dgvAccountDetails.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvAccountDetails_CellMouseDoubleClick);
            this.dgvAccountDetails.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAccountDetails_CellEnter);
            // 
            // TvAccountsHead
            // 
            this.TvAccountsHead.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline;
            this.TvAccountsHead.AllowDrop = true;
            this.TvAccountsHead.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.TvAccountsHead.BackgroundStyle.Class = "TreeBorderKey";
            this.TvAccountsHead.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TvAccountsHead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TvAccountsHead.ImageIndex = 0;
            this.TvAccountsHead.ImageList = this.ImlAccounts;
            this.TvAccountsHead.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.TvAccountsHead.Location = new System.Drawing.Point(0, 89);
            this.TvAccountsHead.Name = "TvAccountsHead";
            this.TvAccountsHead.Nodes.AddRange(new DevComponents.AdvTree.Node[] {
            this.node1});
            this.TvAccountsHead.NodesConnector = this.nodeConnector2;
            this.TvAccountsHead.NodeStyle = this.elementStyle2;
            this.TvAccountsHead.PathSeparator = ";";
            this.TvAccountsHead.Size = new System.Drawing.Size(310, 138);
            this.TvAccountsHead.Styles.Add(this.elementStyle2);
            this.TvAccountsHead.TabIndex = 2;
            this.TvAccountsHead.Text = "advTree1";
            this.TvAccountsHead.AfterNodeSelect += new DevComponents.AdvTree.AdvTreeNodeEventHandler(this.TvAccountsHead_AfterNodeSelect);
            // 
            // node1
            // 
            this.node1.Expanded = true;
            this.node1.Name = "node1";
            this.node1.Text = "node1";
            // 
            // nodeConnector2
            // 
            this.nodeConnector2.LineColor = System.Drawing.SystemColors.ControlText;
            // 
            // elementStyle2
            // 
            this.elementStyle2.Class = "";
            this.elementStyle2.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.elementStyle2.Name = "elementStyle2";
            this.elementStyle2.TextColor = System.Drawing.SystemColors.ControlText;
            // 
            // lvDisplay
            // 
            this.lvDisplay.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.lvDisplay.Border.Class = "ListViewBorder";
            this.lvDisplay.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lvDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvDisplay.HideSelection = false;
            listViewItem1.Tag = "68";
            listViewItem2.Tag = "65";
            listViewItem3.Tag = "66";
            listViewItem4.Tag = "67";
            listViewItem5.Tag = "69";
            this.lvDisplay.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6,
            listViewItem7});
            this.lvDisplay.LargeImageList = this.imglstDisplay;
            this.lvDisplay.Location = new System.Drawing.Point(0, 26);
            this.lvDisplay.MultiSelect = false;
            this.lvDisplay.Name = "lvDisplay";
            this.lvDisplay.Scrollable = false;
            this.lvDisplay.Size = new System.Drawing.Size(310, 147);
            this.lvDisplay.TabIndex = 1;
            this.lvDisplay.UseCompatibleStateImageBehavior = false;
            this.lvDisplay.SelectedIndexChanged += new System.EventHandler(this.lvDisplay_SelectedIndexChanged);
            // 
            // btnDisplay
            // 
            this.btnDisplay.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDisplay.Checked = true;
            this.btnDisplay.GlobalName = "btnDisplay";
            this.btnDisplay.Image = ((System.Drawing.Image)(resources.GetObject("btnDisplay.Image")));
            this.btnDisplay.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.OptionGroup = "navBar";
            this.btnDisplay.Text = "Outline";
            // 
            // lblToDate
            // 
            this.lblToDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lblToDate.BackgroundStyle.Class = "";
            this.lblToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblToDate.Location = new System.Drawing.Point(9, 84);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(47, 23);
            this.lblToDate.TabIndex = 62;
            this.lblToDate.Text = "To Date";
            // 
            // lblFromDate
            // 
            this.lblFromDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lblFromDate.BackgroundStyle.Class = "";
            this.lblFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFromDate.Location = new System.Drawing.Point(9, 58);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(53, 23);
            this.lblFromDate.TabIndex = 61;
            this.lblFromDate.Text = "From Date";
            // 
            // lblCompanySearch
            // 
            this.lblCompanySearch.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lblCompanySearch.BackgroundStyle.Class = "";
            this.lblCompanySearch.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompanySearch.Location = new System.Drawing.Point(9, 31);
            this.lblCompanySearch.Name = "lblCompanySearch";
            this.lblCompanySearch.Size = new System.Drawing.Size(53, 23);
            this.lblCompanySearch.TabIndex = 60;
            this.lblCompanySearch.Text = "Company";
            // 
            // pnlLeftChart
            // 
            this.pnlLeftChart.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlLeftChart.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlLeftChart.Controls.Add(this.expnlChartofAccounts);
            this.pnlLeftChart.Controls.Add(this.expnlSearch);
            this.pnlLeftChart.Controls.Add(this.expnlGatewayofAccounts);
            this.pnlLeftChart.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeftChart.Location = new System.Drawing.Point(0, 0);
            this.pnlLeftChart.Name = "pnlLeftChart";
            this.pnlLeftChart.Size = new System.Drawing.Size(310, 514);
            this.pnlLeftChart.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlLeftChart.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlLeftChart.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlLeftChart.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlLeftChart.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlLeftChart.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlLeftChart.Style.GradientAngle = 90;
            this.pnlLeftChart.TabIndex = 103;
            // 
            // expnlChartofAccounts
            // 
            this.expnlChartofAccounts.CanvasColor = System.Drawing.SystemColors.Control;
            this.expnlChartofAccounts.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expnlChartofAccounts.Controls.Add(this.TvAccountsHead);
            this.expnlChartofAccounts.Controls.Add(this.grpnlSorting);
            this.expnlChartofAccounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.expnlChartofAccounts.ExpandButtonVisible = false;
            this.expnlChartofAccounts.Location = new System.Drawing.Point(0, 287);
            this.expnlChartofAccounts.Name = "expnlChartofAccounts";
            this.expnlChartofAccounts.Size = new System.Drawing.Size(310, 227);
            this.expnlChartofAccounts.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlChartofAccounts.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlChartofAccounts.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlChartofAccounts.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expnlChartofAccounts.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expnlChartofAccounts.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expnlChartofAccounts.Style.GradientAngle = 90;
            this.expnlChartofAccounts.TabIndex = 3;
            this.expnlChartofAccounts.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlChartofAccounts.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlChartofAccounts.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlChartofAccounts.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expnlChartofAccounts.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlChartofAccounts.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expnlChartofAccounts.TitleStyle.GradientAngle = 90;
            this.expnlChartofAccounts.TitleText = "Chart of Accounts";
            // 
            // grpnlSorting
            // 
            this.grpnlSorting.CanvasColor = System.Drawing.SystemColors.Control;
            this.grpnlSorting.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpnlSorting.Controls.Add(this.cboDisplayFilter);
            this.grpnlSorting.Controls.Add(this.lblFilter);
            this.grpnlSorting.Controls.Add(this.cboSearchAccount);
            this.grpnlSorting.Controls.Add(this.lblSearchAc);
            this.grpnlSorting.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpnlSorting.Location = new System.Drawing.Point(0, 26);
            this.grpnlSorting.Name = "grpnlSorting";
            this.grpnlSorting.Size = new System.Drawing.Size(310, 63);
            // 
            // 
            // 
            this.grpnlSorting.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpnlSorting.Style.BackColorGradientAngle = 90;
            this.grpnlSorting.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpnlSorting.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpnlSorting.Style.BorderBottomWidth = 1;
            this.grpnlSorting.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpnlSorting.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpnlSorting.Style.BorderLeftWidth = 1;
            this.grpnlSorting.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpnlSorting.Style.BorderRightWidth = 1;
            this.grpnlSorting.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpnlSorting.Style.BorderTopWidth = 1;
            this.grpnlSorting.Style.Class = "";
            this.grpnlSorting.Style.CornerDiameter = 4;
            this.grpnlSorting.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpnlSorting.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpnlSorting.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpnlSorting.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpnlSorting.StyleMouseDown.Class = "";
            this.grpnlSorting.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpnlSorting.StyleMouseOver.Class = "";
            this.grpnlSorting.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpnlSorting.TabIndex = 3;
            // 
            // cboDisplayFilter
            // 
            this.cboDisplayFilter.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cboDisplayFilter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboDisplayFilter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDisplayFilter.DropDownHeight = 105;
            this.cboDisplayFilter.FormattingEnabled = true;
            this.cboDisplayFilter.IntegralHeight = false;
            this.cboDisplayFilter.Items.AddRange(new object[] {
            "A/c Name",
            "A/c Code",
            "A/c Name & A/c Code"});
            this.cboDisplayFilter.Location = new System.Drawing.Point(85, 5);
            this.cboDisplayFilter.Name = "cboDisplayFilter";
            this.cboDisplayFilter.Size = new System.Drawing.Size(213, 21);
            this.cboDisplayFilter.TabIndex = 64;
            this.cboDisplayFilter.SelectedIndexChanged += new System.EventHandler(this.cboDisplayFilter_SelectedIndexChanged);
            this.cboDisplayFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboSearchCompany_KeyDown);
            // 
            // lblFilter
            // 
            this.lblFilter.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lblFilter.BackgroundStyle.Class = "";
            this.lblFilter.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFilter.Location = new System.Drawing.Point(6, 3);
            this.lblFilter.Name = "lblFilter";
            this.lblFilter.Size = new System.Drawing.Size(73, 23);
            this.lblFilter.TabIndex = 63;
            this.lblFilter.Text = "Displayed by";
            // 
            // cboSearchAccount
            // 
            this.cboSearchAccount.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cboSearchAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboSearchAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchAccount.DropDownHeight = 105;
            this.cboSearchAccount.DropDownWidth = 200;
            this.cboSearchAccount.FormattingEnabled = true;
            this.cboSearchAccount.IntegralHeight = false;
            this.cboSearchAccount.Location = new System.Drawing.Point(85, 32);
            this.cboSearchAccount.Name = "cboSearchAccount";
            this.cboSearchAccount.Size = new System.Drawing.Size(213, 21);
            this.cboSearchAccount.TabIndex = 62;
            this.cboSearchAccount.SelectedIndexChanged += new System.EventHandler(this.cboSearchAccount_SelectedIndexChanged);
            this.cboSearchAccount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboSearchCompany_KeyDown);
            // 
            // lblSearchAc
            // 
            this.lblSearchAc.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lblSearchAc.BackgroundStyle.Class = "";
            this.lblSearchAc.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSearchAc.Location = new System.Drawing.Point(6, 30);
            this.lblSearchAc.Name = "lblSearchAc";
            this.lblSearchAc.Size = new System.Drawing.Size(80, 23);
            this.lblSearchAc.TabIndex = 61;
            this.lblSearchAc.Text = "Search Ledger";
            // 
            // expnlSearch
            // 
            this.expnlSearch.CanvasColor = System.Drawing.SystemColors.Control;
            this.expnlSearch.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expnlSearch.Controls.Add(this.chkShowVoucher);
            this.expnlSearch.Controls.Add(this.lblCompanySearch);
            this.expnlSearch.Controls.Add(this.cboSearchCompany);
            this.expnlSearch.Controls.Add(this.btnSearch);
            this.expnlSearch.Controls.Add(this.lblFromDate);
            this.expnlSearch.Controls.Add(this.dtpToDate);
            this.expnlSearch.Controls.Add(this.lblToDate);
            this.expnlSearch.Controls.Add(this.dtpFromDate);
            this.expnlSearch.Controls.Add(this.lblCurrency);
            this.expnlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.expnlSearch.ExpandButtonVisible = false;
            this.expnlSearch.Location = new System.Drawing.Point(0, 173);
            this.expnlSearch.Name = "expnlSearch";
            this.expnlSearch.Size = new System.Drawing.Size(310, 114);
            this.expnlSearch.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlSearch.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlSearch.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlSearch.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expnlSearch.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expnlSearch.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expnlSearch.Style.GradientAngle = 90;
            this.expnlSearch.TabIndex = 2;
            this.expnlSearch.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlSearch.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlSearch.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlSearch.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expnlSearch.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlSearch.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expnlSearch.TitleStyle.GradientAngle = 90;
            this.expnlSearch.TitleText = "Search";
            // 
            // chkShowVoucher
            // 
            // 
            // 
            // 
            this.chkShowVoucher.BackgroundStyle.Class = "";
            this.chkShowVoucher.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkShowVoucher.Location = new System.Drawing.Point(201, 58);
            this.chkShowVoucher.Name = "chkShowVoucher";
            this.chkShowVoucher.Size = new System.Drawing.Size(100, 23);
            this.chkShowVoucher.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkShowVoucher.TabIndex = 69;
            this.chkShowVoucher.Text = "Show Voucher";
            this.chkShowVoucher.Visible = false;
            // 
            // cboSearchCompany
            // 
            this.cboSearchCompany.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cboSearchCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboSearchCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchCompany.DropDownHeight = 105;
            this.cboSearchCompany.DropDownWidth = 200;
            this.cboSearchCompany.FormattingEnabled = true;
            this.cboSearchCompany.IntegralHeight = false;
            this.cboSearchCompany.Location = new System.Drawing.Point(71, 33);
            this.cboSearchCompany.Name = "cboSearchCompany";
            this.cboSearchCompany.Size = new System.Drawing.Size(150, 21);
            this.cboSearchCompany.TabIndex = 0;
            this.cboSearchCompany.SelectedIndexChanged += new System.EventHandler(this.cboSearchCompany_SelectedIndexChanged);
            this.cboSearchCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboSearchCompany_KeyDown);
            // 
            // expnlGatewayofAccounts
            // 
            this.expnlGatewayofAccounts.CanvasColor = System.Drawing.SystemColors.Control;
            this.expnlGatewayofAccounts.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expnlGatewayofAccounts.Controls.Add(this.lvDisplay);
            this.expnlGatewayofAccounts.Dock = System.Windows.Forms.DockStyle.Top;
            this.expnlGatewayofAccounts.Location = new System.Drawing.Point(0, 0);
            this.expnlGatewayofAccounts.Name = "expnlGatewayofAccounts";
            this.expnlGatewayofAccounts.Size = new System.Drawing.Size(310, 173);
            this.expnlGatewayofAccounts.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlGatewayofAccounts.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlGatewayofAccounts.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlGatewayofAccounts.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expnlGatewayofAccounts.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expnlGatewayofAccounts.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expnlGatewayofAccounts.Style.GradientAngle = 90;
            this.expnlGatewayofAccounts.TabIndex = 0;
            this.expnlGatewayofAccounts.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlGatewayofAccounts.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlGatewayofAccounts.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlGatewayofAccounts.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expnlGatewayofAccounts.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlGatewayofAccounts.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expnlGatewayofAccounts.TitleStyle.GradientAngle = 90;
            this.expnlGatewayofAccounts.TitleText = "Gateway of Accounts";
            // 
            // pnlReport
            // 
            this.pnlReport.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlReport.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlReport.Controls.Add(this.pnlGridWithSummary);
            this.pnlReport.Controls.Add(this.esLeftChart);
            this.pnlReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlReport.Location = new System.Drawing.Point(310, 0);
            this.pnlReport.Name = "pnlReport";
            this.pnlReport.Size = new System.Drawing.Size(974, 514);
            this.pnlReport.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlReport.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlReport.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlReport.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlReport.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlReport.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlReport.TabIndex = 104;
            // 
            // pnlGridWithSummary
            // 
            this.pnlGridWithSummary.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlGridWithSummary.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.pnlGridWithSummary.Controls.Add(this.dgvAccountDetails);
            this.pnlGridWithSummary.Controls.Add(this.groupPanel5);
            this.pnlGridWithSummary.Controls.Add(this.pnlLedgerSummary);
            this.pnlGridWithSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGridWithSummary.Location = new System.Drawing.Point(3, 0);
            this.pnlGridWithSummary.Name = "pnlGridWithSummary";
            this.pnlGridWithSummary.Size = new System.Drawing.Size(971, 514);
            // 
            // 
            // 
            this.pnlGridWithSummary.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlGridWithSummary.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlGridWithSummary.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pnlGridWithSummary.Style.BorderBottomWidth = 1;
            this.pnlGridWithSummary.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlGridWithSummary.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pnlGridWithSummary.Style.BorderLeftWidth = 1;
            this.pnlGridWithSummary.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pnlGridWithSummary.Style.BorderRightWidth = 1;
            this.pnlGridWithSummary.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pnlGridWithSummary.Style.BorderTopWidth = 1;
            this.pnlGridWithSummary.Style.Class = "";
            this.pnlGridWithSummary.Style.CornerDiameter = 4;
            this.pnlGridWithSummary.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.pnlGridWithSummary.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.pnlGridWithSummary.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlGridWithSummary.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.pnlGridWithSummary.StyleMouseDown.Class = "";
            this.pnlGridWithSummary.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.pnlGridWithSummary.StyleMouseOver.Class = "";
            this.pnlGridWithSummary.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.pnlGridWithSummary.TabIndex = 102;
            // 
            // groupPanel5
            // 
            this.groupPanel5.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel5.Controls.Add(this.btnBack);
            this.groupPanel5.Controls.Add(this.lblSearchTitle);
            this.groupPanel5.Controls.Add(this.btnPrint);
            this.groupPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupPanel5.Location = new System.Drawing.Point(0, 0);
            this.groupPanel5.Name = "groupPanel5";
            this.groupPanel5.Size = new System.Drawing.Size(969, 25);
            // 
            // 
            // 
            this.groupPanel5.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel5.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel5.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderBottomWidth = 1;
            this.groupPanel5.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel5.Style.BorderGradientAngle = 0;
            this.groupPanel5.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderLeftWidth = 1;
            this.groupPanel5.Style.BorderLightGradientAngle = 0;
            this.groupPanel5.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderRightWidth = 1;
            this.groupPanel5.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderTopWidth = 1;
            this.groupPanel5.Style.Class = "";
            this.groupPanel5.Style.CornerDiameter = 4;
            this.groupPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel5.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel5.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel5.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseDown.Class = "";
            this.groupPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseOver.Class = "";
            this.groupPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel5.TabIndex = 66;
            // 
            // btnBack
            // 
            this.btnBack.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Image = global::MyBooksERP.Properties.Resources.ArrowLeft;
            this.btnBack.Location = new System.Drawing.Point(902, 0);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(65, 19);
            this.btnBack.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnBack.TabIndex = 63;
            this.btnBack.Tooltip = "Back";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lblSearchTitle
            // 
            this.lblSearchTitle.AutoSize = true;
            // 
            // 
            // 
            this.lblSearchTitle.BackgroundStyle.Class = "";
            this.lblSearchTitle.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSearchTitle.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblSearchTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchTitle.Location = new System.Drawing.Point(0, 0);
            this.lblSearchTitle.Name = "lblSearchTitle";
            this.lblSearchTitle.Size = new System.Drawing.Size(39, 15);
            this.lblSearchTitle.TabIndex = 63;
            this.lblSearchTitle.Text = "Search";
            // 
            // pnlLedgerSummary
            // 
            this.pnlLedgerSummary.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlLedgerSummary.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.pnlLedgerSummary.Controls.Add(this.lblCreditClosing);
            this.pnlLedgerSummary.Controls.Add(this.lblDebitClosing);
            this.pnlLedgerSummary.Controls.Add(this.lblCreditTotal);
            this.pnlLedgerSummary.Controls.Add(this.lblClosingBalance);
            this.pnlLedgerSummary.Controls.Add(this.lblDebitOpening);
            this.pnlLedgerSummary.Controls.Add(this.lblCurrentTotal);
            this.pnlLedgerSummary.Controls.Add(this.lblDebitTotal);
            this.pnlLedgerSummary.Controls.Add(this.lblCreditOpening);
            this.pnlLedgerSummary.Controls.Add(this.lblOpeningBalance);
            this.pnlLedgerSummary.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlLedgerSummary.Location = new System.Drawing.Point(0, 437);
            this.pnlLedgerSummary.Name = "pnlLedgerSummary";
            this.pnlLedgerSummary.Size = new System.Drawing.Size(969, 75);
            // 
            // 
            // 
            this.pnlLedgerSummary.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlLedgerSummary.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlLedgerSummary.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pnlLedgerSummary.Style.BorderBottomWidth = 1;
            this.pnlLedgerSummary.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlLedgerSummary.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pnlLedgerSummary.Style.BorderLeftWidth = 1;
            this.pnlLedgerSummary.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pnlLedgerSummary.Style.BorderRightWidth = 1;
            this.pnlLedgerSummary.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pnlLedgerSummary.Style.BorderTopWidth = 1;
            this.pnlLedgerSummary.Style.Class = "";
            this.pnlLedgerSummary.Style.CornerDiameter = 4;
            this.pnlLedgerSummary.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.pnlLedgerSummary.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.pnlLedgerSummary.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlLedgerSummary.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.pnlLedgerSummary.StyleMouseDown.Class = "";
            this.pnlLedgerSummary.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.pnlLedgerSummary.StyleMouseOver.Class = "";
            this.pnlLedgerSummary.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.pnlLedgerSummary.TabIndex = 104;
            // 
            // esLeftChart
            // 
            this.esLeftChart.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.esLeftChart.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.esLeftChart.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.esLeftChart.ExpandableControl = this.pnlLeftChart;
            this.esLeftChart.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.esLeftChart.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.esLeftChart.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.esLeftChart.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.esLeftChart.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.esLeftChart.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.esLeftChart.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.esLeftChart.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.esLeftChart.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.esLeftChart.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.esLeftChart.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.esLeftChart.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.esLeftChart.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.esLeftChart.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.esLeftChart.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.esLeftChart.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.esLeftChart.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.esLeftChart.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.esLeftChart.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.esLeftChart.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.esLeftChart.Location = new System.Drawing.Point(0, 0);
            this.esLeftChart.Name = "esLeftChart";
            this.esLeftChart.Size = new System.Drawing.Size(3, 514);
            this.esLeftChart.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.esLeftChart.TabIndex = 0;
            this.esLeftChart.TabStop = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "VoucherID";
            this.dataGridViewTextBoxColumn1.HeaderText = "VoucherID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CompanyID";
            this.dataGridViewTextBoxColumn2.HeaderText = "CompanyID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "VoucherTypeID";
            this.dataGridViewTextBoxColumn3.HeaderText = "VoucherTypeID";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "VoucherDate";
            dataGridViewCellStyle2.Format = "dd/MMM/yyyy";
            dataGridViewCellStyle2.NullValue = null;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn4.HeaderText = "Date";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Particulars";
            dataGridViewCellStyle3.Format = "dd/MMM/yyyy";
            dataGridViewCellStyle3.NullValue = null;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn5.HeaderText = "Particulars";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "VoucherType";
            dataGridViewCellStyle4.Format = "dd/MMM/yyyy";
            dataGridViewCellStyle4.NullValue = null;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn6.HeaderText = "Voucher Type";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 80;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "VoucherNo";
            this.dataGridViewTextBoxColumn7.HeaderText = "Voucher No";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "DebitAmount";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn8.HeaderText = "Debit";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 125;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "CreditAmount";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn9.HeaderText = "Credit";
            this.dataGridViewTextBoxColumn9.MinimumWidth = 125;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "IsCostCenter";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn10.HeaderText = "IsCostCenter";
            this.dataGridViewTextBoxColumn10.MinimumWidth = 125;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Visible = false;
            this.dataGridViewTextBoxColumn10.Width = 125;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "IsCostCenter";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn11.HeaderText = "IsCostCenter";
            this.dataGridViewTextBoxColumn11.MinimumWidth = 125;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Visible = false;
            this.dataGridViewTextBoxColumn11.Width = 125;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "IsCostCenter";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn12.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn12.HeaderText = "IsCostCenter";
            this.dataGridViewTextBoxColumn12.MinimumWidth = 125;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Visible = false;
            this.dataGridViewTextBoxColumn12.Width = 125;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "IsGroup";
            this.dataGridViewTextBoxColumn13.HeaderText = "IsGroup";
            this.dataGridViewTextBoxColumn13.MinimumWidth = 125;
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Visible = false;
            this.dataGridViewTextBoxColumn13.Width = 125;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "IsCostCenter";
            this.dataGridViewTextBoxColumn14.HeaderText = "IsCostCenter";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "IsGroup";
            this.dataGridViewTextBoxColumn15.HeaderText = "IsGroup";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Visible = false;
            // 
            // frmAccountSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 514);
            this.Controls.Add(this.pnlReport);
            this.Controls.Add(this.pnlLeftChart);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAccountSummary";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Accounts Summary";
            this.Load += new System.EventHandler(this.frmAccountSummary_Load);
            this.Shown += new System.EventHandler(this.frmAccountSummary_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccountDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TvAccountsHead)).EndInit();
            this.pnlLeftChart.ResumeLayout(false);
            this.expnlChartofAccounts.ResumeLayout(false);
            this.grpnlSorting.ResumeLayout(false);
            this.expnlSearch.ResumeLayout(false);
            this.expnlSearch.PerformLayout();
            this.expnlGatewayofAccounts.ResumeLayout(false);
            this.pnlReport.ResumeLayout(false);
            this.pnlGridWithSummary.ResumeLayout(false);
            this.groupPanel5.ResumeLayout(false);
            this.groupPanel5.PerformLayout();
            this.pnlLedgerSummary.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.DateTimePicker dtpToDate;
        internal System.Windows.Forms.DateTimePicker dtpFromDate;
        public System.Windows.Forms.ImageList imglstDisplay;
        private System.Windows.Forms.ImageList ImlAccounts;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private DevComponents.DotNetBar.PanelEx pnlChart;
        private DevComponents.AdvTree.NodeConnector nodeConnector1;
        private DevComponents.DotNetBar.ElementStyle elementStyle1;
        private DevComponents.DotNetBar.ButtonX btnPrint;
        private DevComponents.DotNetBar.ButtonX btnSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvAccountDetails;
        private DevComponents.AdvTree.AdvTree TvAccountsHead;
        private DevComponents.AdvTree.Node node1;
        private DevComponents.AdvTree.NodeConnector nodeConnector2;
        private DevComponents.DotNetBar.ElementStyle elementStyle2;
        private DevComponents.DotNetBar.Controls.ListViewEx lvDisplay;
        private DevComponents.DotNetBar.ButtonItem btnDisplay;
        private DevComponents.DotNetBar.PanelEx pnlLeftChart;
        private DevComponents.DotNetBar.PanelEx pnlReport;
        private DevComponents.DotNetBar.ExpandableSplitter esLeftChart;
        private DevComponents.DotNetBar.LabelX lblToDate;
        private DevComponents.DotNetBar.LabelX lblFromDate;
        private DevComponents.DotNetBar.LabelX lblCompanySearch;
        private DevComponents.DotNetBar.LabelX lblClosingBalance;
        private DevComponents.DotNetBar.LabelX lblCurrentTotal;
        private DevComponents.DotNetBar.LabelX lblOpeningBalance;
        private DevComponents.DotNetBar.LabelX lblDebitOpening;
        private DevComponents.DotNetBar.LabelX lblCurrency;
        private DevComponents.DotNetBar.LabelX lblDebitTotal;
        private DevComponents.DotNetBar.LabelX lblDebitClosing;
        private DevComponents.DotNetBar.LabelX lblCreditClosing;
        private DevComponents.DotNetBar.LabelX lblCreditTotal;
        private DevComponents.DotNetBar.LabelX lblCreditOpening;
        private DevComponents.DotNetBar.Controls.GroupPanel grpnlSorting;
        private System.Windows.Forms.ComboBox cboSearchCompany;
        private DevComponents.DotNetBar.Controls.GroupPanel pnlLedgerSummary;
        private DevComponents.DotNetBar.Controls.GroupPanel pnlGridWithSummary;
        private System.Windows.Forms.ComboBox cboSearchAccount;
        private DevComponents.DotNetBar.LabelX lblSearchAc;
        private System.Windows.Forms.ComboBox cboDisplayFilter;
        private DevComponents.DotNetBar.LabelX lblFilter;
        private DevComponents.DotNetBar.ButtonX btnBack;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel5;
        private DevComponents.DotNetBar.LabelX lblSearchTitle;
        private DevComponents.DotNetBar.ExpandablePanel expnlGatewayofAccounts;
        private DevComponents.DotNetBar.ExpandablePanel expnlSearch;
        private DevComponents.DotNetBar.ExpandablePanel expnlChartofAccounts;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkShowVoucher;

    }
}