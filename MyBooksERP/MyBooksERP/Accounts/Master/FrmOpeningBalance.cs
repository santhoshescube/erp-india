﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    public partial class frmOpeningBalance : Form
    {
        /***********************************************************
        * Author       : Junaid
        * Created On   : 13 APRL 2012
        * Purpose      : Create Opening Balance for Accounts
        *                
        * **********************************************************/
        #region Declarations
        //permission
        private bool blnPrintEmailPermission = false;   // To Set Print Email Permission
        private bool blnAddPermission = false;          // To Set Add Permission
        private bool blnUpdatePermission = false;       // To Set Update Permission
        private bool blnDeletePermission = false;       // To Set Delete Permission
        private bool blnAddUpdatePermission = false;    // To Set Add Update Perm
        private bool blnAddStatus = false;              // To Add/Update mode 
        private bool blnDeleteMode = false;
        clsBLLCommonUtility MobjBLLCommonUtility = new clsBLLCommonUtility();
        //
        public  int  intAccountID;                      // To Reference Display
        #endregion
        #region Properties
        private clsMessage objUserMessage = null;
        private clsBLLOpenigBalance ObjclsBLLOpenigBalance = null;
        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.OpeningBalance );

                return this.objUserMessage;
            }
        }
        #endregion
        public frmOpeningBalance()
        {
            InitializeComponent();
            ObjclsBLLOpenigBalance = new clsBLLOpenigBalance();
        }

        private void frmOpeningBalance_Load(object sender, EventArgs e)
        {
            SetPermissions();
            LoadCombo();
            AddNew();
            if (intAccountID > 0)
            {
                CboCompanyName.SelectedValue = ClsCommonSettings.CompanyID;
                CboAccount.SelectedValue = intAccountID;
            }
        }
        #region Funtion
        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Accounts, (Int32)eMenuID.OpeningBalance , out blnPrintEmailPermission, out blnAddPermission, out blnUpdatePermission, out blnDeletePermission);

                if (blnAddPermission == true || blnUpdatePermission == true)
                    blnAddUpdatePermission = true;
                else
                    blnAddUpdatePermission = false;
            }
            else
                blnAddPermission = blnAddUpdatePermission = blnPrintEmailPermission = blnUpdatePermission = blnDeletePermission = true;
            
            btnSave.Enabled = btnSaveItem.Enabled = blnAddUpdatePermission;
            
        }
        private void AddNew()
        {
            blnAddStatus = true;
            clear();
            BtnPrint.Enabled = false;
            BtnEmail.Enabled = false;
            btnDeleteItem.Enabled = false;
            bindingNavigatorAddNewItem.Enabled = false;
            blnAddStatus = true;

        }
        private void clear()
        {
            errorProvider.Clear();
            txtOpeningBalance.Text = "";
            CboAccount.SelectedIndex = -1;
          
            cboDebitOrCredit.SelectedIndex = -1;
            lblBookStartDate.Text = "";
            CboAccount.Enabled = true;
            CboCompanyName.Enabled = true;
            //dgvOpeningBalance.Rows.Clear();
            dgvOpeningBalance.DataSource = null;
            if (CboCompanyName.Items.Count > 0)
            {
                CboCompanyName.SelectedIndex = 0;
                CboCompanyName_SelectedIndexChanged(null, null);
            }
        }
        private void LoadCombo()
        {
            LoadAccountCombo();
            DataTable datCompanyCombos = new DataTable();
            datCompanyCombos = ObjclsBLLOpenigBalance.LoadComboboxes(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID = " + ClsCommonSettings.LoginCompanyID });
            CboCompanyName.ValueMember = "CompanyID";
            CboCompanyName.DisplayMember = "CompanyName";
            CboCompanyName.DataSource = datCompanyCombos;
        }
        private void SetdataSource(DataTable datTemp)
        {
            CboAccount.ValueMember = "AccountID";
            CboAccount.DisplayMember = "AccountName";
            CboAccount.DataSource = datTemp;
        }
        private void LoadAccountCombo()
        {
            SetdataSource(ObjclsBLLOpenigBalance.FillcboAccount());
        }
        private void LoadAccountCombo(int CompanyID)
        {
            SetdataSource(ObjclsBLLOpenigBalance.FillcboAccount(CompanyID));
        }
        private void LoadAccountCombo(int CompanyID,int AccountID)
        {
            SetdataSource(ObjclsBLLOpenigBalance.FillcboAccount(CompanyID, AccountID));
        }
        private void ShowBookStartDate(int CompanyID)
        {
            lblBookStartDate.Text = ObjclsBLLOpenigBalance.BookStartDate(CompanyID);
        }
        private void DisplayGridInfo()
        {
            if (blnAddStatus == true || blnDeleteMode == true)
            {
                 lblAccOpBal.Text=string.Empty;
                Decimal decDebitTotal = 0;
                Decimal decCreditTotal = 0;
                Decimal decTotal = 0;
                ObjclsBLLOpenigBalance.clsDTOOpeningBalance.CompanyID = this.CboCompanyName.SelectedValue.ToInt32();
                DataTable DtGrid = ObjclsBLLOpenigBalance.DisplayGridInformation();
                dgvOpeningBalance.DataSource = DtGrid;
                dgvOpeningBalance.Columns["CompanyID"].Visible = false;
                dgvOpeningBalance.Columns["AccountID"].Visible = false;
                dgvOpeningBalance.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                if (DtGrid != null)
                {
                    if (DtGrid.Rows.Count > 0)
                    {
                        decDebitTotal = Convert.ToDecimal(DtGrid.Compute("Sum(DebitAmount)", "1=1"));
                        decCreditTotal = Convert.ToDecimal(DtGrid.Compute("Sum(CreditAmount)", "1=1"));

                        decTotal = decDebitTotal - decCreditTotal;
                        if (decTotal > 0)
                            lblAccOpBal.Text = "Diff. in Opening Balance: " + Convert.ToString(decTotal) + "(Dr)";
                        else if (decTotal < 0)
                            lblAccOpBal.Text = "Diff. in Opening Balance: " + Convert.ToString(decTotal * -1) + "(Cr)";
                    }
                }
            }
        }
        private bool Save()
        {
            try
            {
                if (this.ValidateInputs())
                {
                    bool SaveConfirmation;
                    bool blnDuplicate=false  ;
                    if (blnAddStatus == true ? SaveConfirmation = this.UserMessage.ShowMessage((int)MessageCode.DoYouWishToSave) : SaveConfirmation = this.UserMessage.ShowMessage((int)MessageCode.DoYouWishToUpdate))
                    {
                        if (SaveConfirmation)
                        {
                            FillParameters();
                            //if (blnAddStatus || (!blnAddStatus && (CboAccount.Tag.ToInt32 () != CboAccount.SelectedValue.ToInt32 ())))

                            //   blnDuplicate= ObjclsBLLOpenigBalance.Checkduplicate();// duplicate checking for add new mode
                            //if (!blnDuplicate)
                            //{
                                if (ObjclsBLLOpenigBalance.SaveOpeningBalance(blnAddStatus, Convert.ToBoolean(cboDebitOrCredit.SelectedIndex),CboAccount.Tag .ToInt32 ()))
                                {
                                    this.UserMessage.ShowMessage(blnAddStatus == true ? (int)MessageCode.SavedSuccessfully : (int)MessageCode.UpdatedSuccessfully);
                                    blnAddStatus = true;// displaying grid on new mode and update mode
                                    DisplayGridInfo();
                                    blnAddStatus = false;
                                    int CompanyID = Convert.ToInt32(CboCompanyName.SelectedValue);
                                    LoadAccountCombo(Convert.ToInt32(CboCompanyName.SelectedValue)); 
                                    AddNew();
                                    CboCompanyName.SelectedValue = CompanyID;
                                    CboCompanyName_SelectedIndexChanged(null, null);
                                    return true;
                                }

                            //}
                            //this.UserMessage.ShowMessage(2380);//Message for duplicate
                            //return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;

            }
            catch (Exception ex)
            {
                //write EX
                return false;
            }
        }
        private bool ValidateInputs()
        {
            try
            {
                this.errorProvider.Clear();
                if (this.CboCompanyName.SelectedIndex == -1)
                    return this.UserMessage.ShowMessage(2376,this.CboCompanyName ,this.errorProvider );

                if (this.CboAccount.SelectedIndex == -1)
                    return this.UserMessage.ShowMessage(2377, this.CboAccount, this.errorProvider);

                if (this.txtOpeningBalance.Text.Trim() == string.Empty)
                    return this.UserMessage.ShowMessage(2378, this.txtOpeningBalance, this.errorProvider);

                if (this.txtOpeningBalance.Text.ToDouble() == 0)
                    return this.UserMessage.ShowMessage(2378, this.txtOpeningBalance, this.errorProvider);

                if (this.cboDebitOrCredit.SelectedIndex == -1)
                    return this.UserMessage.ShowMessage(2379, this.cboDebitOrCredit);

                return true;
            }
            catch (Exception ex)
            {
              //ClsLogWriter.WriteLog(ex, Log.LogSeverity.Fatal);
                return false;
            }
        }
        private void FillParameters()
        {
             ObjclsBLLOpenigBalance.clsDTOOpeningBalance.CompanyID = this.CboCompanyName.SelectedValue.ToInt32();
             ObjclsBLLOpenigBalance.clsDTOOpeningBalance.AccountID = this.CboAccount.SelectedValue.ToInt32();
             ObjclsBLLOpenigBalance.clsDTOOpeningBalance.OpeningBalance =Convert.ToDecimal ( this.txtOpeningBalance.Text.Trim ());
             ObjclsBLLOpenigBalance.clsDTOOpeningBalance.BookStartDate = Convert.ToDateTime(this.lblBookStartDate.Text);
        }
        private bool Delete()
        {
            bool DeleteConfirmation;
            if (DeleteConfirmation = this.UserMessage.ShowMessage((int)MessageCode.DoYouWishToDelete))
            {
                if (ObjclsBLLOpenigBalance.DeleteOpeningBalance(CboAccount.SelectedValue.ToInt32(), CboCompanyName.SelectedValue.ToInt32()))
                {
                    blnDeleteMode = true;
                    this.UserMessage.ShowMessage((int)MessageCode.DeletedSuccessfully);
                    DisplayGridInfo();
                    AddNew();
                    blnDeleteMode = false;
                    return true;
                }
            }
           return false;
        }
        private void DisplayGridInfoToCombos()
        {
            blnAddStatus = false;
            CboCompanyName.SelectedValue = Convert.ToInt32(dgvOpeningBalance.CurrentRow.Cells["CompanyID"].Value);
            LoadAccountCombo(Convert.ToInt32(dgvOpeningBalance.CurrentRow.Cells["CompanyID"].Value),Convert.ToInt32( dgvOpeningBalance.CurrentRow.Cells["AccountID"].Value));
            CboAccount.SelectedValue = Convert.ToInt32(dgvOpeningBalance.CurrentRow.Cells["AccountID"].Value);
            CboAccount.Tag = Convert.ToInt32(dgvOpeningBalance.CurrentRow.Cells["AccountID"].Value);
            txtOpeningBalance.Text = getOpeningBalance();
            bindingNavigatorAddNewItem.Enabled = blnAddPermission;
            btnDeleteItem.Enabled = blnDeletePermission ;
            BtnEmail.Enabled= BtnPrint.Enabled = blnPrintEmailPermission ;
            btnSaveItem.Enabled =btnSave.Enabled = blnAddUpdatePermission;
            CboCompanyName.Enabled = false;
        }
        private string getOpeningBalance()
        {
            string strOpenBl = null;
            cboDebitOrCredit.SelectedIndex = 1;
            strOpenBl = Convert.ToString(dgvOpeningBalance.CurrentRow.Cells["CreditAmount"].Value);
            if (Convert.ToInt32(dgvOpeningBalance.CurrentRow.Cells["DebitAmount"].Value) > 0)
            {
                cboDebitOrCredit.SelectedIndex = 0;
                strOpenBl = Convert.ToString(dgvOpeningBalance.CurrentRow.Cells["DebitAmount"].Value);
            }
            return strOpenBl;
        }
        private void LoadReport()
        {
            int intCompanyID= CboCompanyName .SelectedValue .ToInt32 ();
            int intAccoutID= CboAccount .SelectedValue .ToInt32 ();
            if (intCompanyID > 0 && intAccoutID > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PintCompany = intCompanyID;//ComapnyID
                ObjViewer.PiRecId = intAccoutID;//AccoutID
                ObjViewer.PiFormID = (int)FormID.OpeningBalance;//FormID
                ObjViewer.ShowDialog();
            }
        }
        #endregion
        #region Events
        private void CboCompanyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboCompanyName.SelectedIndex != -1 && CboCompanyName.SelectedValue != null)
            {
                errorProvider.Clear();
                ShowBookStartDate((int)CboCompanyName.SelectedValue);
                LoadAccountCombo((int)CboCompanyName.SelectedValue);
                CboAccount.SelectedIndex = -1;
                DisplayGridInfo();
            }
        }
        private void LaodAccountCombo()
        {

        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }
        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            Save();
        }
        private void dgvOpeningBalance_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex > -1 && e.RowIndex > -1)
            {
               if(dgvOpeningBalance.CurrentRow.Cells["AccountID"].Value.ToInt32()!=14) //checking for opening stock account

                DisplayGridInfoToCombos();
            }
        }
        private void CboAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            //txtOpeningBalance.Text = "";
            //cboDebitOrCredit.SelectedIndex = -1;
            if (CboAccount.SelectedIndex != -1 && CboCompanyName .SelectedIndex != -1 )
            {
                errorProvider.Clear();
                //fillBalance();
            }
        }
        private void fillBalance()
        {
            ObjclsBLLOpenigBalance.clsDTOOpeningBalance.CompanyID = this.CboCompanyName.SelectedValue.ToInt32();
            ObjclsBLLOpenigBalance.clsDTOOpeningBalance.AccountID = this.CboAccount.SelectedValue.ToInt32();
            blnAddStatus = true;
            DataTable datTemp = ObjclsBLLOpenigBalance.fillBalance();
            if( datTemp != null   && datTemp.Rows.Count >0 )
            {
                cboDebitOrCredit.SelectedIndex = 1;
                txtOpeningBalance.Text = (datTemp.Rows[0]["DebitAmount"].ToDouble() + datTemp.Rows[0]["CreditAmount"].ToDouble()).ToString();
                if (datTemp.Rows[0]["DebitAmount"].ToDouble() > 0)
                {
                    cboDebitOrCredit.SelectedIndex = 0;
                }
                blnAddStatus = false;
            }
        }
        private void CancelToolStripButton_Click(object sender, EventArgs e)
        {
            //clear();
            AddNew();
        }
        private void CboCompanyName_KeyDown(object sender, KeyEventArgs e)
        {
            ((ComboBox)sender).DroppedDown = false;

        }
        private void BtnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }
        private void BtnCompany_Click(object sender, EventArgs e)
        {
            using (FrmCompany objCom = new FrmCompany())
            {
                objCom.ShowDialog();
            }
            int intComboID = CboCompanyName.SelectedValue.ToInt32();
            CboCompanyName.SelectedValue = intComboID;
        }
        private void BtnAccount_Click(object sender, EventArgs e)
        {
            if (CboCompanyName.SelectedIndex != -1)
            {
                using (FrmChartOfAccounts objChartOfAccounts = new FrmChartOfAccounts())
                {
                    objChartOfAccounts.MbCalledFromAccountSettings = true;
                    objChartOfAccounts.ShowDialog();
                }
            }
        }
        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            Delete();
        }
        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNew();
        }
        private void dgvOpeningBalance_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            MobjBLLCommonUtility.SetSerialNo(dgvOpeningBalance , e.RowIndex, true);
        }

        private void dgvOpeningBalance_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
             MobjBLLCommonUtility.SetSerialNo(dgvOpeningBalance , e.RowIndex, true);
        }
        #endregion

        private void btnChartofAccounts_Click(object sender, EventArgs e)
        {
            using (FrmChartOfAccounts objFrmChartOfAccounts = new FrmChartOfAccounts())
                objFrmChartOfAccounts.ShowDialog();
            LoadAccountCombo();
        }

       
        
    }
}
