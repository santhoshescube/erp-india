﻿namespace MyBooksERP
{
    partial class frmOpeningBalance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOpeningBalance));
            this.OpeningBalancebindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.btnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.btnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.CancelToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.btnChartofAccounts = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboDebitOrCredit = new System.Windows.Forms.ComboBox();
            this.lblBookStartDate = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.dgvOpeningBalance = new System.Windows.Forms.DataGridView();
            this.txtOpeningBalance = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CboAccount = new System.Windows.Forms.ComboBox();
            this.lblAccount = new System.Windows.Forms.Label();
            this.CboCompanyName = new System.Windows.Forms.ComboBox();
            this.lblCompanyName = new System.Windows.Forms.Label();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatusMsg = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblAccOpBal = new System.Windows.Forms.Label();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.OpeningBalancebindingNavigator)).BeginInit();
            this.OpeningBalancebindingNavigator.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOpeningBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.ssStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // OpeningBalancebindingNavigator
            // 
            this.OpeningBalancebindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.OpeningBalancebindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.OpeningBalancebindingNavigator.CountItem = null;
            this.OpeningBalancebindingNavigator.DeleteItem = this.btnDeleteItem;
            this.OpeningBalancebindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorAddNewItem,
            this.btnDeleteItem,
            this.btnSaveItem,
            this.CancelToolStripButton,
            this.toolStripSeparator1,
            this.btnChartofAccounts,
            this.toolStripSeparator3,
            this.BtnPrint,
            this.BtnEmail,
            this.toolStripSeparator2,
            this.BtnHelp});
            this.OpeningBalancebindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.OpeningBalancebindingNavigator.MoveFirstItem = null;
            this.OpeningBalancebindingNavigator.MoveLastItem = null;
            this.OpeningBalancebindingNavigator.MoveNextItem = null;
            this.OpeningBalancebindingNavigator.MovePreviousItem = null;
            this.OpeningBalancebindingNavigator.Name = "OpeningBalancebindingNavigator";
            this.OpeningBalancebindingNavigator.PositionItem = null;
            this.OpeningBalancebindingNavigator.Size = new System.Drawing.Size(438, 25);
            this.OpeningBalancebindingNavigator.TabIndex = 0;
            this.OpeningBalancebindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDeleteItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.RightToLeftAutoMirrorImage = true;
            this.btnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.btnDeleteItem.Text = "Delete";
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // btnSaveItem
            // 
            this.btnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveItem.Image = global::MyBooksERP.Properties.Resources.save;
            this.btnSaveItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSaveItem.Name = "btnSaveItem";
            this.btnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.btnSaveItem.Text = "Save";
            this.btnSaveItem.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // CancelToolStripButton
            // 
            this.CancelToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CancelToolStripButton.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.CancelToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CancelToolStripButton.Name = "CancelToolStripButton";
            this.CancelToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.CancelToolStripButton.Text = "Clear";
            this.CancelToolStripButton.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Visible = false;
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = global::MyBooksERP.Properties.Resources.help;
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "Help";
            this.BtnHelp.Visible = false;
            // 
            // btnChartofAccounts
            // 
            this.btnChartofAccounts.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnChartofAccounts.Image = global::MyBooksERP.Properties.Resources.Chart_Of_Accounts;
            this.btnChartofAccounts.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnChartofAccounts.Name = "btnChartofAccounts";
            this.btnChartofAccounts.Size = new System.Drawing.Size(23, 22);
            this.btnChartofAccounts.Text = "Chart of Accounts";
            this.btnChartofAccounts.Click += new System.EventHandler(this.btnChartofAccounts_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cboDebitOrCredit);
            this.groupBox1.Controls.Add(this.lblBookStartDate);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.dgvOpeningBalance);
            this.groupBox1.Controls.Add(this.txtOpeningBalance);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.CboAccount);
            this.groupBox1.Controls.Add(this.lblAccount);
            this.groupBox1.Controls.Add(this.CboCompanyName);
            this.groupBox1.Controls.Add(this.lblCompanyName);
            this.groupBox1.Location = new System.Drawing.Point(7, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(424, 331);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Opening Balance Details";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Amount";
            // 
            // cboDebitOrCredit
            // 
            this.cboDebitOrCredit.BackColor = System.Drawing.SystemColors.Info;
            this.cboDebitOrCredit.FormattingEnabled = true;
            this.cboDebitOrCredit.Items.AddRange(new object[] {
            "Dr",
            "Cr"});
            this.cboDebitOrCredit.Location = new System.Drawing.Point(289, 97);
            this.cboDebitOrCredit.Name = "cboDebitOrCredit";
            this.cboDebitOrCredit.Size = new System.Drawing.Size(38, 21);
            this.cboDebitOrCredit.TabIndex = 4;
            // 
            // lblBookStartDate
            // 
            this.lblBookStartDate.AutoSize = true;
            this.lblBookStartDate.Location = new System.Drawing.Point(133, 77);
            this.lblBookStartDate.Name = "lblBookStartDate";
            this.lblBookStartDate.Size = new System.Drawing.Size(0, 13);
            this.lblBookStartDate.TabIndex = 19;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(333, 96);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dgvOpeningBalance
            // 
            this.dgvOpeningBalance.AllowUserToAddRows = false;
            this.dgvOpeningBalance.AllowUserToDeleteRows = false;
            this.dgvOpeningBalance.AllowUserToResizeColumns = false;
            this.dgvOpeningBalance.AllowUserToResizeRows = false;
            this.dgvOpeningBalance.BackgroundColor = System.Drawing.Color.White;
            this.dgvOpeningBalance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOpeningBalance.Location = new System.Drawing.Point(6, 127);
            this.dgvOpeningBalance.MultiSelect = false;
            this.dgvOpeningBalance.Name = "dgvOpeningBalance";
            this.dgvOpeningBalance.ReadOnly = true;
            this.dgvOpeningBalance.RowHeadersWidth = 50;
            this.dgvOpeningBalance.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvOpeningBalance.Size = new System.Drawing.Size(410, 197);
            this.dgvOpeningBalance.TabIndex = 17;
            this.dgvOpeningBalance.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvOpeningBalance_RowsAdded);
            this.dgvOpeningBalance.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOpeningBalance_CellClick);
            this.dgvOpeningBalance.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvOpeningBalance_RowsRemoved);
            // 
            // txtOpeningBalance
            // 
            this.txtOpeningBalance.BackColor = System.Drawing.SystemColors.Info;
            this.txtOpeningBalance.Location = new System.Drawing.Point(129, 98);
            this.txtOpeningBalance.MaxLength = 9;
            this.txtOpeningBalance.Name = "txtOpeningBalance";
            this.txtOpeningBalance.ShortcutsEnabled = false;
            this.txtOpeningBalance.Size = new System.Drawing.Size(135, 20);
            this.txtOpeningBalance.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Opening Balance As On";
            // 
            // CboAccount
            // 
            this.CboAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboAccount.BackColor = System.Drawing.SystemColors.Info;
            this.CboAccount.DropDownHeight = 134;
            this.CboAccount.FormattingEnabled = true;
            this.CboAccount.IntegralHeight = false;
            this.CboAccount.Location = new System.Drawing.Point(129, 48);
            this.CboAccount.MaxDropDownItems = 10;
            this.CboAccount.Name = "CboAccount";
            this.CboAccount.Size = new System.Drawing.Size(281, 21);
            this.CboAccount.TabIndex = 2;
            this.CboAccount.SelectedIndexChanged += new System.EventHandler(this.CboAccount_SelectedIndexChanged);
            this.CboAccount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboCompanyName_KeyDown);
            // 
            // lblAccount
            // 
            this.lblAccount.AutoSize = true;
            this.lblAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccount.Location = new System.Drawing.Point(11, 52);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(47, 13);
            this.lblAccount.TabIndex = 6;
            this.lblAccount.Text = "Account";
            // 
            // CboCompanyName
            // 
            this.CboCompanyName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompanyName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompanyName.BackColor = System.Drawing.SystemColors.Info;
            this.CboCompanyName.DropDownHeight = 134;
            this.CboCompanyName.FormattingEnabled = true;
            this.CboCompanyName.IntegralHeight = false;
            this.CboCompanyName.Location = new System.Drawing.Point(129, 21);
            this.CboCompanyName.MaxDropDownItems = 10;
            this.CboCompanyName.Name = "CboCompanyName";
            this.CboCompanyName.Size = new System.Drawing.Size(281, 21);
            this.CboCompanyName.TabIndex = 1;
            this.CboCompanyName.SelectedIndexChanged += new System.EventHandler(this.CboCompanyName_SelectedIndexChanged);
            this.CboCompanyName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboCompanyName_KeyDown);
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.AutoSize = true;
            this.lblCompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompanyName.Location = new System.Drawing.Point(11, 24);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(51, 13);
            this.lblCompanyName.TabIndex = 3;
            this.lblCompanyName.Text = "Company";
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // ssStatus
            // 
            this.ssStatus.BackColor = System.Drawing.Color.Transparent;
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lblStatusMsg});
            this.ssStatus.Location = new System.Drawing.Point(0, 383);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(438, 22);
            this.ssStatus.TabIndex = 2;
            this.ssStatus.Text = "Status";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(45, 17);
            this.toolStripStatusLabel1.Text = "Status: ";
            // 
            // lblStatusMsg
            // 
            this.lblStatusMsg.Name = "lblStatusMsg";
            this.lblStatusMsg.Size = new System.Drawing.Size(0, 17);
            // 
            // lblAccOpBal
            // 
            this.lblAccOpBal.AutoSize = true;
            this.lblAccOpBal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccOpBal.Location = new System.Drawing.Point(181, 366);
            this.lblAccOpBal.Name = "lblAccOpBal";
            this.lblAccOpBal.Size = new System.Drawing.Size(0, 13);
            this.lblAccOpBal.TabIndex = 250;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            this.toolStripSeparator2.Visible = false;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // frmOpeningBalance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 405);
            this.Controls.Add(this.lblAccOpBal);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.OpeningBalancebindingNavigator);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmOpeningBalance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Opening Balance";
            this.Load += new System.EventHandler(this.frmOpeningBalance_Load);
            ((System.ComponentModel.ISupportInitialize)(this.OpeningBalancebindingNavigator)).EndInit();
            this.OpeningBalancebindingNavigator.ResumeLayout(false);
            this.OpeningBalancebindingNavigator.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOpeningBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator OpeningBalancebindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripButton btnDeleteItem;
        private System.Windows.Forms.ToolStripButton btnSaveItem;
        private System.Windows.Forms.ToolStripButton CancelToolStripButton;
        private System.Windows.Forms.ToolStripButton BtnPrint;
        private System.Windows.Forms.ToolStripButton BtnEmail;
        private System.Windows.Forms.ToolStripButton BtnHelp;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.ComboBox CboCompanyName;
        private System.Windows.Forms.Label lblCompanyName;
        internal System.Windows.Forms.ComboBox CboAccount;
        private System.Windows.Forms.Label lblAccount;
        private System.Windows.Forms.TextBox txtOpeningBalance;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvOpeningBalance;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblBookStartDate;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.ComboBox cboDebitOrCredit;
        private System.Windows.Forms.StatusStrip ssStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatusMsg;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripButton btnChartofAccounts;
        private System.Windows.Forms.Label lblAccOpBal;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}