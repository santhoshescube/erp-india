﻿namespace MyBooksERP//.GUI.FingerTech
{
    partial class FrmAttendanceDeviceSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAttendanceDeviceSettings));
            this.GrpMain = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTimeOut = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtDelay = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtModel = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtCommKey = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtDeviceNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtPort = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtIPAddress = new DevComponents.Editors.IpAddressInput();
            this.DgvDeviceSettings = new System.Windows.Forms.DataGridView();
            this.ColSILD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDevicename = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColIPAdress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPort = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDeviceNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCommKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColModel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDelay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTimeOut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnSave = new System.Windows.Forms.Button();
            this.Label11 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.DeviceSettingsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripbtnHelp = new System.Windows.Forms.ToolStripButton();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label10 = new System.Windows.Forms.Label();
            this.BtnTest = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ProgressBarTest = new System.Windows.Forms.ToolStripProgressBar();
            this.ToolStripStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmrClearlabels = new System.Windows.Forms.Timer(this.components);
            this.errorProSettings = new System.Windows.Forms.ErrorProvider(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GrpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIPAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvDeviceSettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeviceSettingsBindingNavigator)).BeginInit();
            this.DeviceSettingsBindingNavigator.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProSettings)).BeginInit();
            this.SuspendLayout();
            // 
            // GrpMain
            // 
            this.GrpMain.BackColor = System.Drawing.SystemColors.Control;
            this.GrpMain.Controls.Add(this.label12);
            this.GrpMain.Controls.Add(this.label4);
            this.GrpMain.Controls.Add(this.txtTimeOut);
            this.GrpMain.Controls.Add(this.txtDelay);
            this.GrpMain.Controls.Add(this.txtModel);
            this.GrpMain.Controls.Add(this.txtCommKey);
            this.GrpMain.Controls.Add(this.txtDeviceNo);
            this.GrpMain.Controls.Add(this.txtPort);
            this.GrpMain.Controls.Add(this.txtName);
            this.GrpMain.Controls.Add(this.txtIPAddress);
            this.GrpMain.Controls.Add(this.DgvDeviceSettings);
            this.GrpMain.Controls.Add(this.BtnSave);
            this.GrpMain.Controls.Add(this.Label11);
            this.GrpMain.Controls.Add(this.Label8);
            this.GrpMain.Controls.Add(this.Label1);
            this.GrpMain.Controls.Add(this.Label2);
            this.GrpMain.Controls.Add(this.Label3);
            this.GrpMain.Controls.Add(this.Label9);
            this.GrpMain.Controls.Add(this.Label6);
            this.GrpMain.Controls.Add(this.Label7);
            this.GrpMain.Controls.Add(this.ShapeContainer1);
            this.GrpMain.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrpMain.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GrpMain.Location = new System.Drawing.Point(5, 28);
            this.GrpMain.Name = "GrpMain";
            this.GrpMain.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.GrpMain.Size = new System.Drawing.Size(535, 309);
            this.GrpMain.TabIndex = 0;
            this.GrpMain.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(5, 148);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 14);
            this.label12.TabIndex = 1035;
            this.label12.Text = "Settings Info";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1, -1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 14);
            this.label4.TabIndex = 1034;
            this.label4.Text = "Device Connector";
            // 
            // txtTimeOut
            // 
            this.txtTimeOut.BackColor = System.Drawing.SystemColors.HighlightText;
            // 
            // 
            // 
            this.txtTimeOut.Border.Class = "TextBoxBorder";
            this.txtTimeOut.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTimeOut.Location = new System.Drawing.Point(375, 98);
            this.txtTimeOut.MaxLength = 4;
            this.txtTimeOut.Name = "txtTimeOut";
            this.txtTimeOut.Size = new System.Drawing.Size(152, 20);
            this.txtTimeOut.TabIndex = 7;
            this.txtTimeOut.Text = "10";
            this.txtTimeOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTimeOut.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            this.txtTimeOut.TextChanged += new System.EventHandler(this.txtTimeOut_TextChanged);
            // 
            // txtDelay
            // 
            this.txtDelay.BackColor = System.Drawing.SystemColors.HighlightText;
            // 
            // 
            // 
            this.txtDelay.Border.Class = "TextBoxBorder";
            this.txtDelay.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDelay.Location = new System.Drawing.Point(375, 73);
            this.txtDelay.MaxLength = 5;
            this.txtDelay.Name = "txtDelay";
            this.txtDelay.Size = new System.Drawing.Size(152, 20);
            this.txtDelay.TabIndex = 6;
            this.txtDelay.Text = "1000";
            this.txtDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDelay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            this.txtDelay.TextChanged += new System.EventHandler(this.txtDelay_TextChanged);
            // 
            // txtModel
            // 
            this.txtModel.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtModel.Border.Class = "TextBoxBorder";
            this.txtModel.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtModel.Location = new System.Drawing.Point(375, 47);
            this.txtModel.MaxLength = 40;
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(152, 20);
            this.txtModel.TabIndex = 5;
            this.txtModel.TextChanged += new System.EventHandler(this.txtModel_TextChanged);
            // 
            // txtCommKey
            // 
            this.txtCommKey.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtCommKey.Border.Class = "TextBoxBorder";
            this.txtCommKey.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCommKey.Location = new System.Drawing.Point(375, 20);
            this.txtCommKey.MaxLength = 7;
            this.txtCommKey.Name = "txtCommKey";
            this.txtCommKey.Size = new System.Drawing.Size(152, 20);
            this.txtCommKey.TabIndex = 4;
            this.txtCommKey.TextChanged += new System.EventHandler(this.txtCommKey_TextChanged);
            // 
            // txtDeviceNo
            // 
            this.txtDeviceNo.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtDeviceNo.Border.Class = "TextBoxBorder";
            this.txtDeviceNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDeviceNo.Location = new System.Drawing.Point(84, 97);
            this.txtDeviceNo.MaxLength = 6;
            this.txtDeviceNo.Name = "txtDeviceNo";
            this.txtDeviceNo.Size = new System.Drawing.Size(135, 20);
            this.txtDeviceNo.TabIndex = 3;
            this.txtDeviceNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            this.txtDeviceNo.TextChanged += new System.EventHandler(this.txtDeviceNo_TextChanged);
            // 
            // txtPort
            // 
            this.txtPort.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtPort.Border.Class = "TextBoxBorder";
            this.txtPort.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPort.Location = new System.Drawing.Point(84, 72);
            this.txtPort.MaxLength = 6;
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(135, 20);
            this.txtPort.TabIndex = 2;
            this.txtPort.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            this.txtPort.TextChanged += new System.EventHandler(this.txtPort_TextChanged);
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtName.Border.Class = "TextBoxBorder";
            this.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtName.Location = new System.Drawing.Point(84, 20);
            this.txtName.MaxLength = 40;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(180, 20);
            this.txtName.TabIndex = 0;
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // txtIPAddress
            // 
            this.txtIPAddress.AutoOverwrite = true;
            this.txtIPAddress.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.txtIPAddress.BackgroundStyle.BackColor = System.Drawing.SystemColors.Info;
            this.txtIPAddress.BackgroundStyle.Class = "DateTimeInputBackground";
            this.txtIPAddress.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtIPAddress.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.txtIPAddress.Location = new System.Drawing.Point(84, 46);
            this.txtIPAddress.Name = "txtIPAddress";
            this.txtIPAddress.Size = new System.Drawing.Size(180, 20);
            this.txtIPAddress.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtIPAddress.TabIndex = 1;
            this.txtIPAddress.TextChanged += new System.EventHandler(this.txtIPAddress_TextChanged);
            // 
            // DgvDeviceSettings
            // 
            this.DgvDeviceSettings.AllowUserToAddRows = false;
            this.DgvDeviceSettings.AllowUserToDeleteRows = false;
            this.DgvDeviceSettings.AllowUserToResizeColumns = false;
            this.DgvDeviceSettings.AllowUserToResizeRows = false;
            this.DgvDeviceSettings.BackgroundColor = System.Drawing.Color.White;
            this.DgvDeviceSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvDeviceSettings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColSILD,
            this.ColDevicename,
            this.ColIPAdress,
            this.ColPort,
            this.ColDeviceNo,
            this.ColCommKey,
            this.ColModel,
            this.ColDelay,
            this.ColTimeOut});
            this.DgvDeviceSettings.Location = new System.Drawing.Point(10, 165);
            this.DgvDeviceSettings.MultiSelect = false;
            this.DgvDeviceSettings.Name = "DgvDeviceSettings";
            this.DgvDeviceSettings.ReadOnly = true;
            this.DgvDeviceSettings.RowHeadersVisible = false;
            this.DgvDeviceSettings.RowHeadersWidth = 25;
            this.DgvDeviceSettings.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DgvDeviceSettings.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DgvDeviceSettings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvDeviceSettings.Size = new System.Drawing.Size(516, 138);
            this.DgvDeviceSettings.TabIndex = 71;
            this.DgvDeviceSettings.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvDeviceSettings_CellClick);
            // 
            // ColSILD
            // 
            this.ColSILD.HeaderText = "SlID";
            this.ColSILD.Name = "ColSILD";
            this.ColSILD.ReadOnly = true;
            this.ColSILD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColSILD.Visible = false;
            // 
            // ColDevicename
            // 
            this.ColDevicename.HeaderText = "Device Name";
            this.ColDevicename.Name = "ColDevicename";
            this.ColDevicename.ReadOnly = true;
            this.ColDevicename.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColDevicename.Width = 145;
            // 
            // ColIPAdress
            // 
            this.ColIPAdress.HeaderText = "IP Address";
            this.ColIPAdress.Name = "ColIPAdress";
            this.ColIPAdress.ReadOnly = true;
            this.ColIPAdress.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColIPAdress.Width = 145;
            // 
            // ColPort
            // 
            this.ColPort.HeaderText = "Port";
            this.ColPort.Name = "ColPort";
            this.ColPort.ReadOnly = true;
            this.ColPort.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColPort.Width = 75;
            // 
            // ColDeviceNo
            // 
            this.ColDeviceNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColDeviceNo.HeaderText = "Device Number";
            this.ColDeviceNo.Name = "ColDeviceNo";
            this.ColDeviceNo.ReadOnly = true;
            this.ColDeviceNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColCommKey
            // 
            this.ColCommKey.HeaderText = "CommKey";
            this.ColCommKey.Name = "ColCommKey";
            this.ColCommKey.ReadOnly = true;
            this.ColCommKey.Visible = false;
            // 
            // ColModel
            // 
            this.ColModel.HeaderText = "Model";
            this.ColModel.Name = "ColModel";
            this.ColModel.ReadOnly = true;
            this.ColModel.Visible = false;
            // 
            // ColDelay
            // 
            this.ColDelay.HeaderText = "Delay";
            this.ColDelay.Name = "ColDelay";
            this.ColDelay.ReadOnly = true;
            this.ColDelay.Visible = false;
            // 
            // ColTimeOut
            // 
            this.ColTimeOut.HeaderText = "TimeOut";
            this.ColTimeOut.Name = "ColTimeOut";
            this.ColTimeOut.ReadOnly = true;
            this.ColTimeOut.Visible = false;
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(457, 125);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(69, 24);
            this.BtnSave.TabIndex = 8;
            this.BtnSave.Text = "&Save";
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(10, 20);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(34, 14);
            this.Label11.TabIndex = 27;
            this.Label11.Text = "Name";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(295, 99);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(72, 23);
            this.Label8.TabIndex = 26;
            this.Label8.Text = "TimeOut (s):";
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.SystemColors.Control;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.Location = new System.Drawing.Point(10, 46);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(65, 17);
            this.Label1.TabIndex = 23;
            this.Label1.Text = "IP Address";
            // 
            // Label2
            // 
            this.Label2.BackColor = System.Drawing.SystemColors.Control;
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label2.Location = new System.Drawing.Point(10, 72);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(32, 17);
            this.Label2.TabIndex = 22;
            this.Label2.Text = "Port";
            // 
            // Label3
            // 
            this.Label3.BackColor = System.Drawing.SystemColors.Control;
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label3.Location = new System.Drawing.Point(295, 21);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(64, 16);
            this.Label3.TabIndex = 20;
            this.Label3.Text = "Comm Key";
            // 
            // Label9
            // 
            this.Label9.BackColor = System.Drawing.SystemColors.Control;
            this.Label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label9.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label9.Location = new System.Drawing.Point(10, 98);
            this.Label9.Name = "Label9";
            this.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label9.Size = new System.Drawing.Size(65, 19);
            this.Label9.TabIndex = 19;
            this.Label9.Text = "Device No";
            // 
            // Label6
            // 
            this.Label6.BackColor = System.Drawing.SystemColors.Control;
            this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label6.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label6.Location = new System.Drawing.Point(295, 47);
            this.Label6.Name = "Label6";
            this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label6.Size = new System.Drawing.Size(40, 17);
            this.Label6.TabIndex = 18;
            this.Label6.Text = "Model";
            // 
            // Label7
            // 
            this.Label7.BackColor = System.Drawing.SystemColors.Control;
            this.Label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label7.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label7.Location = new System.Drawing.Point(295, 73);
            this.Label7.Name = "Label7";
            this.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label7.Size = new System.Drawing.Size(65, 17);
            this.Label7.TabIndex = 17;
            this.Label7.Text = "Delay (ms)";
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.ShapeContainer1.Size = new System.Drawing.Size(529, 290);
            this.ShapeContainer1.TabIndex = 28;
            this.ShapeContainer1.TabStop = false;
            // 
            // LineShape1
            // 
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 6;
            this.LineShape1.X2 = 523;
            this.LineShape1.Y1 = 141;
            this.LineShape1.Y2 = 141;
            // 
            // DeviceSettingsBindingNavigator
            // 
            this.DeviceSettingsBindingNavigator.AddNewItem = null;
            this.DeviceSettingsBindingNavigator.CountItem = null;
            this.DeviceSettingsBindingNavigator.DeleteItem = null;
            this.DeviceSettingsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.toolStripSeparator2,
            this.BindingNavigatorDeleteItem,
            this.btnClear,
            this.ToolStripbtnHelp});
            this.DeviceSettingsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.DeviceSettingsBindingNavigator.MoveFirstItem = null;
            this.DeviceSettingsBindingNavigator.MoveLastItem = null;
            this.DeviceSettingsBindingNavigator.MoveNextItem = null;
            this.DeviceSettingsBindingNavigator.MovePreviousItem = null;
            this.DeviceSettingsBindingNavigator.Name = "DeviceSettingsBindingNavigator";
            this.DeviceSettingsBindingNavigator.PositionItem = null;
            this.DeviceSettingsBindingNavigator.Size = new System.Drawing.Size(549, 25);
            this.DeviceSettingsBindingNavigator.TabIndex = 50;
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Remove";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // ToolStripbtnHelp
            // 
            this.ToolStripbtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripbtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripbtnHelp.Image")));
            this.ToolStripbtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripbtnHelp.Name = "ToolStripbtnHelp";
            this.ToolStripbtnHelp.Size = new System.Drawing.Size(23, 22);
            this.ToolStripbtnHelp.Text = "He&lp";
            this.ToolStripbtnHelp.Visible = false;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.Label5);
            this.GroupBox1.Controls.Add(this.Label10);
            this.GroupBox1.Controls.Add(this.BtnTest);
            this.GroupBox1.Location = new System.Drawing.Point(8, 342);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(532, 55);
            this.GroupBox1.TabIndex = 1;
            this.GroupBox1.TabStop = false;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.BackColor = System.Drawing.SystemColors.Control;
            this.Label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(-1, 0);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(151, 13);
            this.Label5.TabIndex = 26;
            this.Label5.Text = "Test your Device settings";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(6, 19);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(358, 29);
            this.Label10.TabIndex = 27;
            this.Label10.Text = "After filling the above information, we recommend you to test with the entered va" +
                "lues whether the connection is successful or not,";
            // 
            // BtnTest
            // 
            this.BtnTest.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTest.Location = new System.Drawing.Point(429, 19);
            this.BtnTest.Name = "BtnTest";
            this.BtnTest.Size = new System.Drawing.Size(94, 27);
            this.BtnTest.TabIndex = 0;
            this.BtnTest.Text = "Test Settings";
            this.BtnTest.UseVisualStyleBackColor = true;
            this.BtnTest.Click += new System.EventHandler(this.BtnTest_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(396, 403);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(69, 24);
            this.BtnOk.TabIndex = 0;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(471, 403);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(69, 24);
            this.BtnCancel.TabIndex = 1;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProgressBarTest,
            this.ToolStripStatus});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 432);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(549, 22);
            this.StatusStrip1.TabIndex = 62;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // ProgressBarTest
            // 
            this.ProgressBarTest.Name = "ProgressBarTest";
            this.ProgressBarTest.Overflow = System.Windows.Forms.ToolStripItemOverflow.Always;
            this.ProgressBarTest.Size = new System.Drawing.Size(100, 16);
            this.ProgressBarTest.Step = 1;
            this.ProgressBarTest.Visible = false;
            // 
            // ToolStripStatus
            // 
            this.ToolStripStatus.Name = "ToolStripStatus";
            this.ToolStripStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // tmrClearlabels
            // 
            this.tmrClearlabels.Tick += new System.EventHandler(this.tmrClearlabels_Tick);
            // 
            // errorProSettings
            // 
            this.errorProSettings.ContainerControl = this;
            this.errorProSettings.RightToLeft = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "SlID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Device Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 145;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "IP Address";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 145;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Port";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 75;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.HeaderText = "Device Number";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "CommKey";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Model";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Delay";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "TimeOut";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // FrmAttendanceDeviceSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 454);
            this.Controls.Add(this.GrpMain);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.DeviceSettingsBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAttendanceDeviceSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Device Settings";
            this.Load += new System.EventHandler(this.FrmFTEchDeviceSettings_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmFTEchDeviceSettings_FormClosing);
            this.GrpMain.ResumeLayout(false);
            this.GrpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIPAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvDeviceSettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeviceSettingsBindingNavigator)).EndInit();
            this.DeviceSettingsBindingNavigator.ResumeLayout(false);
            this.DeviceSettingsBindingNavigator.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProSettings)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.GroupBox GrpMain;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Label Label8;
        public System.Windows.Forms.Label Label1;
        public System.Windows.Forms.Label Label2;
        public System.Windows.Forms.Label Label3;
        public System.Windows.Forms.Label Label9;
        public System.Windows.Forms.Label Label6;
        public System.Windows.Forms.Label Label7;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.DataGridView DgvDeviceSettings;
        internal System.Windows.Forms.BindingNavigator DeviceSettingsBindingNavigator;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripButton ToolStripbtnHelp;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Button BtnTest;
        internal System.Windows.Forms.Button BtnOk;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripProgressBar ProgressBarTest;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColSILD;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDevicename;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColIPAdress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPort;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDeviceNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCommKey;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColModel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDelay;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTimeOut;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.Timer tmrClearlabels;
        private DevComponents.Editors.IpAddressInput txtIPAddress;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDeviceNo;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPort;
        private DevComponents.DotNetBar.Controls.TextBoxX txtName;
        private DevComponents.DotNetBar.Controls.TextBoxX txtModel;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCommKey;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTimeOut;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDelay;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ErrorProvider errorProSettings;
    }
}