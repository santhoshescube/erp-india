﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    /*****************************************************
* Created By       : Arun
* Creation Date    : 19 Apr 2012
* Description      : Handle Attendance Ot Template
* FormID           : 126
* ***************************************************/
    public partial class FrmOtTemplate : Form
    {

        private clsBLLOtTemplate MobjclsBLLOtTemplate = null;
        private clsMessage ObjUserMessage = null;

        public FrmOtTemplate()
        {
            InitializeComponent();
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}


            
        }


        //#region SetArabicControls
        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.AttendanceOtTemplate, this);
        //}
        //#endregion SetArabicControls


        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.AbsentPolicy);
                return this.ObjUserMessage;
            }
        }
        private clsBLLOtTemplate BLLOtTemplate
        {
            get
            {
                if (this.MobjclsBLLOtTemplate == null)
                    this.MobjclsBLLOtTemplate = new clsBLLOtTemplate();

                return this.MobjclsBLLOtTemplate;
            }
        }







        private bool GetOtTemplateDetails()
        {
            bool blnRetValue = false;
            try
            {


                DataTable dtOtDetails = BLLOtTemplate.GetOtTemplateDetails();
                if (dtOtDetails != null)
                {
                    if (dtOtDetails.Rows.Count > 0)
                    {
                        int iCounter = 0;
                        foreach (DataRow row in dtOtDetails.Rows)
                        {
                            dgvOtTemplate.RowCount = dgvOtTemplate.RowCount + 1;
                            dgvOtTemplate.Rows[iCounter].Cells["ColOrderNo"].Value = row["OrderNo"];
                            dgvOtTemplate.Rows[iCounter].Cells["ColStartTime"].Value = row["StartMin"];
                            dgvOtTemplate.Rows[iCounter].Cells["ColEndTime"].Value = row["EndMin"];
                            dgvOtTemplate.Rows[iCounter].Cells["ColOtTime"].Value = row["OTMin"];
                            iCounter = iCounter + 1;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }
            return blnRetValue;
        }

        private bool FillParameter()
        {
            bool blnRetValue = false;
            bool blnFlag = false;
            int iCheck = 0;
            BLLOtTemplate.DTOOtTemplate.DTOOtTemplateDetails = new List<clsDTOOtTemplateDetails>();
            for (int iCounter = 0; iCounter <= dgvOtTemplate.Rows.Count - 1; iCounter++)
            {
                
                if (dgvOtTemplate.Rows[iCounter].Cells["ColStartTime"].Value != DBNull.Value &&
                        dgvOtTemplate.Rows[iCounter].Cells["ColEndTime"].Value != DBNull.Value &&
                        dgvOtTemplate.Rows[iCounter].Cells["ColOtTime"].Value != DBNull.Value)
                {

                    if (Convert.ToString(dgvOtTemplate.Rows[iCounter].Cells["ColStartTime"].Value) != "" &&
                         Convert.ToString(dgvOtTemplate.Rows[iCounter].Cells["ColEndTime"].Value) != "" &&
                         Convert.ToString(dgvOtTemplate.Rows[iCounter].Cells["ColOtTime"].Value) != "")
                    {
                        blnFlag = false;
                        if (Int32.TryParse(Convert.ToString(dgvOtTemplate.Rows[iCounter].Cells["ColStartTime"].Value), out iCheck))
                            blnFlag = true;

                        if (blnFlag && Int32.TryParse(Convert.ToString(dgvOtTemplate.Rows[iCounter].Cells["ColEndTime"].Value), out iCheck) == false)
                            blnFlag = false;

                        if (blnFlag && Int32.TryParse(Convert.ToString(dgvOtTemplate.Rows[iCounter].Cells["ColOtTime"].Value), out iCheck) == false)
                            blnFlag = false;

                        if (blnFlag)
                        {



                            clsDTOOtTemplateDetails objOtTemplateDetails = new clsDTOOtTemplateDetails();
                            objOtTemplateDetails.StartMin = dgvOtTemplate.Rows[iCounter].Cells["ColStartTime"].Value.ToInt32();
                            objOtTemplateDetails.EndMin = dgvOtTemplate.Rows[iCounter].Cells["ColEndTime"].Value.ToInt32();
                            objOtTemplateDetails.OTMin = dgvOtTemplate.Rows[iCounter].Cells["ColOtTime"].Value.ToInt32();
                            BLLOtTemplate.DTOOtTemplate.DTOOtTemplateDetails.Add(objOtTemplateDetails);
                            
                                blnRetValue = true;
                        }


                    }
                }

            }
            return blnRetValue;
        }
        private bool SaveOtTempalteDetails()
        {
            bool blnRetValue = false;
            try
            {
                if (dgvOtTemplate.Rows.Count > 0)
                {
                    
                    DeleteOTDetails();
                    FillParameter();
                    if (BLLOtTemplate.SaveOtTemplateDetails())
                        blnRetValue = true;
                   
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }
            return blnRetValue;
        }

        private bool DeleteOTDetails()
        {
            bool blnRetValue = false;
            try
            {
                
                if (BLLOtTemplate.DeleteOtTemplate() )
                    blnRetValue = true;

            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);

            }
            return blnRetValue;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveOtTempalteDetails())
            {
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvOtTemplate_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                TextBox txtNumeric = (TextBox)e.Control;
                txtNumeric.ShortcutsEnabled = false;
                if (dgvOtTemplate.CurrentCell != null)
                {
                    if (dgvOtTemplate.CurrentCell.ColumnIndex >= -1 && dgvOtTemplate.CurrentCell.RowIndex >= -1)
                        e.Control.KeyPress += new KeyPressEventHandler(TextboxNumeric_KeyPress);
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }
        }
        private void TextboxNumeric_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            try
            {

                if (!(Char.IsDigit(e.KeyChar) || (e.KeyChar == 8) || (e.KeyChar == 13)))
                {
                    e.Handled = true;
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }
        }
        private void FrmOtTemplate_Shown(object sender, EventArgs e)
        {
            GetOtTemplateDetails();
        }

        private void FrmOtTemplate_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void dgvOtTemplate_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                return;
            }
            catch (Exception)
            {
                return;
            }
        }

        private void dgvOtTemplate_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                dgvOtTemplate.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }
        }





    }
}
