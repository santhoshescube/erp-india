﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{ 
/*****************************************************
 * Created By       : Arun
 * Creation Date    : 17 Apr 2012
 * Description      : Handle Attendance Device Settings
 * FromID           : 124
 * Message Codes    : 5630-5642
 * ***************************************************/

    public partial class FrmAttendanceDeviceSettings : Form
    {

        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission

        private bool MblnbtnOk = false;
        private string MstrMessageCaption =""; 
        private string MstrMessageCommon;
        private MessageBoxIcon MmessageIcon;
        private bool MblnIsEditMode = false;
        private bool MblnChangeStatus = false;

        clsBLLAttendanceDeviceSettings MobjclsBLLAttendanceDeviceSettings;
        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjNotification;
        private clsMessage ObjUserMessage = null;
       
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.AttendanceDeviceSttings);
                return this.ObjUserMessage;
            }
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                //objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.AttendanceDeviceSettings, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.ChangePassword, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);// Since this form is not used, Temperorly changed the eMenuID to ChangePassword in order to avoid bugs.

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }

        private void Changestatus()
        {
            //function for changing status


            if (!MblnIsEditMode)
            {
                BtnOk.Enabled = MblnAddPermission;
                BtnSave.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
            else
            {
                BtnOk.Enabled = MblnUpdatePermission;
                BtnSave.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            errorProSettings.Clear();
            MblnChangeStatus = true;

        }
        public FrmAttendanceDeviceSettings()
        {
            InitializeComponent();
            MobjclsBLLAttendanceDeviceSettings = new clsBLLAttendanceDeviceSettings();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            MstrMessageCaption = ClsCommonSettings.MessageCaption=="" || ClsCommonSettings.MessageCaption==null? "ActivRolls":ClsCommonSettings.MessageCaption;
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }
        //#region SetArabicControls
        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.AttendanceDeviceSttings, this);
        //}
        //#endregion SetArabicControls

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (SaveSettings())
            {
                MessageBox.Show(UserMessage.GetMessageByCode(2),MstrMessageCaption,MessageBoxButtons.OK,MessageBoxIcon.Information );
                ClearAllControls();
                FillGrid();
            }
        }

        private void FrmFTEchDeviceSettings_Load(object sender, EventArgs e)
        {
            SetPermissions();
            AddNewSettings();
            FillGrid();
        }

        private void FrmFTEchDeviceSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MblnbtnOk == false && MblnChangeStatus == true)
            {
                if (UserMessage.ShowMessage(8))
                {
                    e.Cancel = false;
                }
                else
                { e.Cancel = true; }
            }
        }
        private void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!((Char.IsDigit(e.KeyChar)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace --((e.KeyChar == 46)) || 
            {
                e.Handled = true;
            }
            //if (((TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            //{
            //    e.Handled = true;
            //}

        }
        private void AddNewSettings()
        {
            ClearAllControls();
            BtnOk.Enabled = false;
            BtnSave.Enabled = false;
            BindingNavigatorSaveItem.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = false;
            MblnChangeStatus = false;
            txtName.Focus();

        }
        private void ClearAllControls()
        {
            MblnIsEditMode = false;
            txtCommKey.Text = "";
            txtDelay.Text = "";
            txtIPAddress.Value = "";
            txtIPAddress.Text= "";
            txtModel.Text = "";
            txtName.Text = "";
            txtPort.Text = "";
            txtDeviceNo.Text = "";
            txtTimeOut.Text = "";
            GrpMain.Tag = 0;
            txtName.Focus();

        }
        private void FillParameters()
        {
            if (Convert.ToInt32(GrpMain.Tag) > 0)
            {
                MobjclsBLLAttendanceDeviceSettings.clsDTOAttendanceDeviceSettings.intDeviceID = Convert.ToInt32(GrpMain.Tag);
            }
            else
            {
                MobjclsBLLAttendanceDeviceSettings.clsDTOAttendanceDeviceSettings.intDeviceID = 0;
            }

            MobjclsBLLAttendanceDeviceSettings.clsDTOAttendanceDeviceSettings.intDeviceTypeID = Convert.ToInt32(AttendnaceDevices.FingerTech);//for Finger Tech
            MobjclsBLLAttendanceDeviceSettings.clsDTOAttendanceDeviceSettings.strDevicename = txtName.Text.ToStringCustom();
            MobjclsBLLAttendanceDeviceSettings.clsDTOAttendanceDeviceSettings.strIPAddress = txtIPAddress.Value.ToStringCustom().Trim();
            MobjclsBLLAttendanceDeviceSettings.clsDTOAttendanceDeviceSettings.dblPort = txtPort.Text.ToDouble();
            MobjclsBLLAttendanceDeviceSettings.clsDTOAttendanceDeviceSettings.intDeviceNo = txtDeviceNo.Text.ToInt32();
            MobjclsBLLAttendanceDeviceSettings.clsDTOAttendanceDeviceSettings.intCommKey = txtCommKey.Text.ToInt32();
            MobjclsBLLAttendanceDeviceSettings.clsDTOAttendanceDeviceSettings.strModel = txtModel.Text.ToStringCustom();
            MobjclsBLLAttendanceDeviceSettings.clsDTOAttendanceDeviceSettings.intDelay = txtDelay.Text != "" ? txtDelay.Text.ToInt32() : 0;
            MobjclsBLLAttendanceDeviceSettings.clsDTOAttendanceDeviceSettings.intTimeOut = txtTimeOut.Text != "" ? txtTimeOut.Text.ToInt32() : 0;

        }

        private bool DeviceSettingsValidation()
        {


            if (txtName.Text.ToStringCustom() == string.Empty)
            {
                ////MsMessageCommon = "Please enter a valid Group";

                MstrMessageCommon = UserMessage.GetMessageByCode(5630);//mObjNotification.GetErrorMessage(MaMessageArr, 3953, out MmessageIcon);
                errorProSettings.SetError(txtName, MstrMessageCommon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                txtName.Focus();
                return false;
                ////MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                //ErrorProviderAccountSettings.SetError(CboGoup, MstrMessageCommon.Replace("#", "").Trim());
                //lblAccountSettingsstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //TmAccountSettings.Enabled = true;

            }
            if (txtIPAddress.Value.ToStringCustom() == string.Empty )
            {
                MstrMessageCommon = UserMessage.GetMessageByCode(5631);// "Please Enter IP Address";//mObjNotification.GetErrorMessage(MaMessageArr, 3953, out MmessageIcon);
                errorProSettings.SetError(txtIPAddress, MstrMessageCommon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                txtIPAddress.Focus();
                return false;

            }
            if (txtPort.Text.ToStringCustom() == string.Empty)
            {

                MstrMessageCommon = UserMessage.GetMessageByCode(5632);//"Please Enter Port";//mObjNotification.GetErrorMessage(MaMessageArr, 3953, out MmessageIcon);
                errorProSettings.SetError(txtPort, MstrMessageCommon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                txtPort.Focus();
                return false;
            }
            if (txtDeviceNo.Text.ToStringCustom() == string.Empty)
            {
                MstrMessageCommon = UserMessage.GetMessageByCode(5633);//"Please Enter Device No";//mObjNotification.GetErrorMessage(MaMessageArr, 3953, out MmessageIcon);
                errorProSettings.SetError(txtDeviceNo, MstrMessageCommon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                txtDeviceNo.Focus();
                return false;
            }
            if (txtCommKey.Text.ToStringCustom() == string.Empty)
            {
                MstrMessageCommon = UserMessage.GetMessageByCode(5634);//"Please Enter Comm key";//mObjNotification.GetErrorMessage(MaMessageArr, 3953, out MmessageIcon);
                errorProSettings.SetError(txtCommKey, MstrMessageCommon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                txtCommKey.Focus();
                return false;
            }
            if (txtModel.Text.ToStringCustom() == string.Empty)
            {
                MstrMessageCommon = UserMessage.GetMessageByCode(5635);//"Please Enter Model";//mObjNotification.GetErrorMessage(MaMessageArr, 3953, out MmessageIcon);
                errorProSettings.SetError(txtModel, MstrMessageCommon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                txtModel.Focus();
                return false;

            }
            
            return true;
        }
        private bool SaveSettings()
        {
            bool blnRetvalue = false;
            try
            {

                if (DeviceSettingsValidation())
                {
                    int intMessageCode = 0;
                    if (GrpMain.Tag.ToInt32() > 0)
                        intMessageCode = 5642;
                    else
                        intMessageCode = 5641;

                    if (UserMessage.ShowMessage(intMessageCode))
                    {
                        FillParameters();
                        blnRetvalue = MobjclsBLLAttendanceDeviceSettings.SaveDeviceSettings();
                    }
                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MObjClsLogWriter.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

            }
            return blnRetvalue;
        }
        private bool FillGrid()
        {
            int intRow;
            MobjclsBLLAttendanceDeviceSettings.clsDTOAttendanceDeviceSettings.intDeviceTypeID = Convert.ToInt32(AttendnaceDevices.FingerTech);//for finger tech
            DataTable DT = MobjclsBLLAttendanceDeviceSettings.DisplayDeviceSettings();
            DgvDeviceSettings.Rows.Clear();
            DgvDeviceSettings.RowCount = 0;
            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i <= DT.Rows.Count - 1; i++)
                {
                    intRow = DgvDeviceSettings.RowCount;
                    DgvDeviceSettings.RowCount = DgvDeviceSettings.RowCount + 1;
                    DgvDeviceSettings.Rows[intRow].Cells["ColSILD"].Value = Convert.ToString(DT.Rows[i]["DeviceID"]);
                    DgvDeviceSettings.Rows[intRow].Cells["ColDevicename"].Value = Convert.ToString(DT.Rows[i]["DeviceName"]);

                    DgvDeviceSettings.Rows[intRow].Cells["ColIPAdress"].Value = Convert.ToString(DT.Rows[i]["IPAddress"]);
                    DgvDeviceSettings.Rows[intRow].Cells["ColPort"].Value = Convert.ToString(DT.Rows[i]["Port"]);
                    DgvDeviceSettings.Rows[intRow].Cells["ColDeviceNo"].Value = Convert.ToString(DT.Rows[i]["DeviceNo"]);
                    DgvDeviceSettings.Rows[intRow].Cells["ColCommKey"].Value = Convert.ToString(DT.Rows[i]["CommKey"]);
                    DgvDeviceSettings.Rows[intRow].Cells["ColModel"].Value = Convert.ToString(DT.Rows[i]["Model"]);
                    DgvDeviceSettings.Rows[intRow].Cells["ColDelay"].Value = Convert.ToString(DT.Rows[i]["Delay"]);
                    DgvDeviceSettings.Rows[intRow].Cells["ColTimeOut"].Value = Convert.ToString(DT.Rows[i]["TimeOut"]);
                    DgvDeviceSettings.Rows[intRow].Cells["ColDeviceNo"].Value = Convert.ToString(DT.Rows[i]["DeviceNo"]);
                }
            }
            return true;
        }

        private void BtnTest_Click(object sender, EventArgs e)
        {
            TestSetting();
        }
        private bool TestSetting()
        {
            if (DeviceSettingsValidation())
            {
                ProgressBarTest.Visible = true;
                ProgressBarTest.Value = 20;
                string strIPAddress = txtIPAddress.Text;
                int intPort = Convert.ToInt32(txtPort.Text);
                int intCommkey = Convert.ToInt32(txtCommKey.Text);
                string strModel = txtModel.Text;
                int intDeviceNo = Convert.ToInt32(txtDeviceNo.Text);
                ProgressBarTest.Value = 30;
                //if (BioBridgeSDK.Connect_TCPIP(strModel, intDeviceNo, strIPAddress, intPort, intCommkey) == 0)
                //{
                //    ProgressBarTest.Value = 60;
                //    BioBridgeSDK.Disconnect();
                //    ProgressBarTest.Value = 100;
                //    MessageBox.Show(UserMessage.GetMessageByCode(5636),MstrMessageCaption,MessageBoxButtons.OK,MessageBoxIcon.Information);
                //    ToolStripStatus.Text = UserMessage.GetMessageByCode(5636);
                //    tmrClearlabels.Enabled = true;
                //}
                //else
                //{

                    MessageBox.Show(UserMessage.GetMessageByCode(5637), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ToolStripStatus.Text = UserMessage.GetMessageByCode(5637);
                    tmrClearlabels.Enabled = true;
                    ProgressBarTest.Value = 100;
                //}
                ProgressBarTest.Visible = false;

            }
            return true;
        }
        private void FillGridToFields(int intRowIndex)
        {
            ClearAllControls();
            GrpMain.Tag = DgvDeviceSettings.Rows[intRowIndex].Cells["ColSILD"].Value.ToInt32();
            txtName.Text = DgvDeviceSettings.Rows[intRowIndex].Cells["ColDevicename"].Value.ToStringCustom();
            txtIPAddress.Value = DgvDeviceSettings.Rows[intRowIndex].Cells["ColIPAdress"].Value.ToStringCustom();
            txtPort.Text = DgvDeviceSettings.Rows[intRowIndex].Cells["ColPort"].Value.ToStringCustom();
            txtDeviceNo.Text = DgvDeviceSettings.Rows[intRowIndex].Cells["ColDeviceNo"].Value.ToStringCustom();
            txtCommKey.Text = DgvDeviceSettings.Rows[intRowIndex].Cells["ColCommKey"].Value.ToStringCustom();
            txtModel.Text = DgvDeviceSettings.Rows[intRowIndex].Cells["ColModel"].Value.ToStringCustom();
            txtDelay.Text = DgvDeviceSettings.Rows[intRowIndex].Cells["ColDelay"].Value.ToStringCustom();
            txtTimeOut.Text = DgvDeviceSettings.Rows[intRowIndex].Cells["ColTimeOut"].Value.ToStringCustom();
            txtDeviceNo.Text = DgvDeviceSettings.Rows[intRowIndex].Cells["ColDeviceNo"].Value.ToStringCustom();
            MblnIsEditMode = true;
            MblnChangeStatus = false;
            BtnOk.Enabled = MblnChangeStatus;
            BtnSave.Enabled = MblnChangeStatus;
            BindingNavigatorSaveItem.Enabled = MblnChangeStatus;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;


        }
        private void DgvDeviceSettings_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                FillGridToFields(e.RowIndex);
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewSettings();
        }
        private bool DeleteDeviceSettings()
        {
            MobjclsBLLAttendanceDeviceSettings.clsDTOAttendanceDeviceSettings.intDeviceID = Convert.ToInt32(GrpMain.Tag);
            MobjclsBLLAttendanceDeviceSettings.DeleteDeviceSettings();
            return true;
        }
        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(GrpMain.Tag) > 0)
            {

                //MstrMessageCommon = "Do you wish to delete this device settings?";
                if (UserMessage.ShowMessage(5638))
                {
                    if (DeleteDeviceSettings())
                    {
                        UserMessage.ShowMessage(5639);
                        ClearAllControls();
                        FillGrid();
                    }
                }
                else
                {

                    return;
                }



            }
            else
            {  if (DgvDeviceSettings.RowCount>0)
                   UserMessage.ShowMessage(5640);
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            BtnSave_Click(sender, e);
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (SaveSettings())
            {
                MblnbtnOk = true;
                //MessageBox.Show(UserMessage.GetMessageByCode(2), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //ClearAllControls  ();
                //FillGrid();
                this.Close();
            }
            
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearAllControls();
        }

        private void txtTimeOut_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void tmrClearlabels_Tick(object sender, EventArgs e)
        {
            tmrClearlabels.Enabled = false;
            ToolStripStatus.Text = "";

        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtIPAddress_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtPort_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtDeviceNo_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtCommKey_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtModel_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtDelay_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

    }
}
