﻿

#region USING

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;
using System.Collections;
#endregion USING

namespace MyBooksERP
{
    /***************************************************** 
    * Created By       : Arun
    * Creation Date    : 17 Apr 2012
    * Description      : Handle Employee WorkCode Mapping for Attendance
    * FormID           : 125
    * *****************************************************/

    public partial class FrmAttendanceMapping : Form
    {
        #region DECLARTIONS

        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission

        private int MintTemplateID = 0;
        private int MintTemplateFileType = 0;
        private string MstrMessageCommon = "";

        clsBLLAttendance MobjclsBLLAttendance;
        clsBLLAttendanceMapping MobjclsBLLAtnMapping;
        private clsMessage ObjUserMessage = null;
        ClsNotification MobjClsNotification;
        ClsLogWriter ClsLog;
        private MessageBoxIcon MmessageIcon;
        private ArrayList MsarMessageArr;
        #endregion DECLARTIONS

        #region CONTRUCTOR

        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.AttendanceMapping);
                return this.ObjUserMessage;
            }
        }

        public FrmAttendanceMapping()
        {
            InitializeComponent();
            MobjclsBLLAttendance = new clsBLLAttendance();
            MobjclsBLLAtnMapping = new clsBLLAttendanceMapping();
            MobjClsNotification = new ClsNotification();
            ClsLog = new ClsLogWriter(Application.StartupPath);
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        #endregion CONSTRUCTOR

        #region METHODS
        private void LoadMessage()
        {
            // Loading Message
            try
            {
                MsarMessageArr = new ArrayList();
                MsarMessageArr = MobjClsNotification.FillMessageArray((int)FormID.AttendanceMapping, ClsCommonSettings.ProductID);
            }
            catch (Exception Ex)
            {
                ClsLog.WriteLog("Error on LoadMessage " + this.Name + " " + Ex.Message.ToString(), 1);
            }

        }
        private void ResetForm()
        {
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    if (rdbTemplates.Checked)
            //    {
            //        lblDevice.Text = "قالب";
            //        LoadCombo(2);
            //        btnTemplate.Enabled = true;
            //    }
            //    else if (rdbDevice.Checked)
            //    {
            //        lblDevice.Text = "جهاز";
            //        LoadCombo(1);
            //        btnTemplate.Enabled = false;
            //    }
            //}
            //else
            //{
                if (rdbTemplates.Checked)
                {
                    lblDevice.Text = "Templates";
                    LoadCombo(2);
                    btnTemplate.Enabled = true;
                }
                else if (rdbDevice.Checked)
                {
                    lblDevice.Text = "Device";
                    LoadCombo(1);
                    btnTemplate.Enabled = false;
                }
            //}
            MintTemplateID = 0;
            MintTemplateFileType = 0;
            DgvAtnMapping.Rows.Clear();
            ColPunchID.DataSource = null;

        }

        private bool FillTemplates()
        {
            //Filling Attendance Templates for csv,accessdb,excel
            try
            {
                DataTable datTemplates = new DataTable();

                datTemplates = MobjclsBLLAttendance.FillCombos(new string[] { "TemplateMasterID,TemplateName", "PayAttendanceTemplateMaster", "FileTypeID IN(" + (int)AttendanceFiletype.Csv + "," + (int)AttendanceFiletype.Excel + "," + (int)AttendanceFiletype.AccessDB + ")" });
                if (datTemplates.Rows.Count > 0)
                {
                    for (int intCounter = 0; intCounter <= datTemplates.Rows.Count - 1; intCounter++)
                    {
                        ToolStripMenuItem btnItem = new ToolStripMenuItem();
                        btnItem.Text = Convert.ToString(datTemplates.Rows[intCounter]["TemplateName"]);
                        btnItem.Name = Convert.ToString(datTemplates.Rows[intCounter]["TemplateName"]);
                        btnItem.Tag = Convert.ToString(datTemplates.Rows[intCounter]["TemplateMasterID"]);
                        btnItem.Click += new EventHandler(TemplateSubItems_Click);
                        btnTemplate.DropDownItems.Add(btnItem);
                    }
                }


                return true;
            }
            catch (Exception Ex)
            {
                ClsLog.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }

        private void TemplateSubItems_Click(Object sender, EventArgs e)
        {

            try
            {
                if (CboDevice.SelectedValue.ToInt32() > 0)
                {

                    ToolStripMenuItem btnTemplateItem = (ToolStripMenuItem)sender;
                    MintTemplateID = Convert.ToInt32(btnTemplateItem.Tag);
                    MintTemplateFileType = MobjclsBLLAttendance.GetTemplateFileType(MintTemplateID);
                    if (MintTemplateFileType == MobjclsBLLAttendance.GetTemplateFileType(CboDevice.SelectedValue.ToInt32()))
                    {

                        OpenFileDialog dlgbox = new OpenFileDialog();
                        string sFileName = "";
                        switch (MintTemplateFileType)
                        {
                            case 1:
                                dlgbox.Filter = "CSV Files (*.csv)|*.csv| Supported Files (*.csv)|*.csv";
                                break;
                            case 2:
                                dlgbox.Filter = " Excel File (*.xls;*.xlsx)|*.xls;*.xlsx|  Supported Files (*.xls;*.xlsx;)|*.xls;*.xlsx;";
                                break;
                            case 3:
                                dlgbox.Filter = "Access File (*.accdb;*.mdb)|*.accdb;*.mdb| Supported Files (*.accdb;*.mdb)|*.accdb;*.mdb";
                                break;

                        }
                        if (MintTemplateFileType <= 3)
                        {
                            if (dlgbox.ShowDialog() == DialogResult.OK)
                            {
                                sFileName = dlgbox.FileName;
                            }
                            dlgbox.Dispose();
                            dlgbox = null;
                        }
                        Cursor.Current = Cursors.WaitCursor;

                        DataTable dtEmpNo = FetchData(sFileName);
                        if (dtEmpNo != null)
                        {
                            if (dtEmpNo.Rows.Count > 0)
                            {
                                //ColPunchID.Items.Clear();
                                ColPunchID.DataSource = null;

                                ColPunchID.ValueMember = "Column1";
                                ColPunchID.DisplayMember = "Column1";
                                ColPunchID.DataSource = dtEmpNo;

                            }
                        }



                    }
                    else
                    {
                        //UserMessage.ShowMessage(5554,null,2);
                        MstrMessageCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 5554, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }

                }
                else
                {
                    //UserMessage.ShowMessage(5553,null,2);
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 5553, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);


                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }
            Cursor.Current = Cursors.Default;

        }

        private bool GetTemplatedata(ref  string strEmpNo)
        {
            bool blnRetValue = false;

            DataTable DtFileDetils = MobjclsBLLAttendance.GetTemplateFieldDetails(MintTemplateID);
            if (DtFileDetils != null)
            {
                if (DtFileDetils.Rows.Count > 0)
                {
                    for (int iCounter = 0; iCounter <= DtFileDetils.Rows.Count - 1; iCounter++)
                    {

                        if (DtFileDetils.Rows[iCounter]["Templatefield"].ToStringCustom() == "EmployeeNumber")
                            strEmpNo = DtFileDetils.Rows[iCounter]["Actualfield"].ToStringCustom();
                    }
                    blnRetValue = true;
                }
            }
            return blnRetValue;

        }

        private DataTable FetchData(string sFileName)
        {

            bool blnIsHeaderExists = false;
            int intPunchingCount = 1;
            string strDateFormat = "";
            bool blnIsTimeWithDate = false;

            string strEmpno = "";
            string strExtention = "";

            DataTable dtData = new DataTable();
            if (MintTemplateFileType <= 3)
            {
                if (sFileName == "")
                    return dtData;
                FileInfo fInfo = new FileInfo(sFileName);
                if (fInfo.Exists)
                    strExtention = fInfo.Extension;
                else
                    return dtData;

            }

            MobjclsBLLAttendance.GetTemplateDetails(MintTemplateID, ref blnIsHeaderExists, ref intPunchingCount, ref strDateFormat, ref blnIsTimeWithDate);
            GetTemplatedata(ref strEmpno);



            if (MintTemplateFileType == 1)
            {
                //Getting the Id,time,date from the file
                dtData.Columns.Add("Column1");

                using (Microsoft.VisualBasic.FileIO.TextFieldParser myreader = new Microsoft.VisualBasic.FileIO.TextFieldParser(sFileName))
                {

                    myreader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                    myreader.SetDelimiters(",");
                    string[] currentrow;

                    while (!myreader.EndOfData)
                    {
                        if (blnIsHeaderExists)
                        {
                            blnIsHeaderExists = false;
                            continue;
                        }


                        currentrow = myreader.ReadFields();
                        string sCurrent = "";
                        try
                        {
                            sCurrent = currentrow[strEmpno.Replace("Column", "").Trim().ToInt32() - 1].ToStringCustom();
                            dtData.Rows.Add(sCurrent);


                        }
                        catch (Exception Ex)
                        {
                            ClsLog.WriteLog("Error on FetchData Reading data from csv:FrmAttendance. TimeCount=1" + Ex.Message.ToString() + "", 2);
                        }


                    }//End While
                    dtData.DefaultView.Sort = "column1";
                    dtData = dtData.DefaultView.ToTable(true); //'Distinct Value Selecting 




                }//End Using
            }//File type=1
            else if (MintTemplateFileType == 2)
            {
                dtData.Columns.Add("Column1");


                DataSet ds = new DataSet();
                string strconn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + sFileName + ";Extended Properties=Excel 12.0;";
                OleDbDataAdapter mydata = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", strconn);
                mydata.TableMappings.Add("Table1", "ExcelTest");
                mydata.Fill(ds);

                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    string sCurrent = "";

                    try
                    {

                        sCurrent = ds.Tables[0].Rows[i][strEmpno.Replace("Column", "").Trim().ToInt32() - 1].ToStringCustom();
                        dtData.Rows.Add(sCurrent);


                    }
                    catch (Exception Ex)
                    {
                        ClsLog.WriteLog("Error on FetchData Reading data from Excel:FrmAttendance. TimeCount=1" + Ex.Message.ToString() + "", 2);
                    }


                }
                dtData.DefaultView.Sort = "column1";
                dtData = dtData.DefaultView.ToTable(true); //'Distinct Value Selecting 

            }
            else if (MintTemplateFileType == 3)
            {
                TestAccessDbConnection(sFileName);
            }

            return dtData;
        }
        private void TestAccessDbConnection(string sFileName)
        {
            try
            {
                string sUserName = "";
                string sPassword = "";
                string sTableName = "";
                string sTableField = "";
                string strCon = "";
                string strSelectQuery = "";
                string sCriteria = "";
                string sServerName = "";
                string sDatabaseName = "";
                DataTable dtMyData = new DataTable();

                // GetAccessDbTableDetails(sUserName, sPassword, sTableName, sTableField, sCriteria) '//getting password
                GetAccessDbAndSqlDbTableDetails(MintTemplateID, ref sUserName, ref sPassword, ref sTableName, ref sServerName, ref sCriteria, ref sDatabaseName,ref sTableField);// '//getting password
                if (sFileName != "" && sTableName != "" && sTableField != "")
                {
                    if (sPassword != "")
                    {
                        strCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + sFileName + "; Jet OLEDB:Database Password=" + sPassword;
                    }
                    else
                    {
                        strCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + sFileName;
                    }
                    OleDbConnection MobjOleDbConnection = new OleDbConnection();
                    MobjOleDbConnection.ConnectionString = strCon;
                    MobjOleDbConnection.Open();
                    if (sCriteria != "")
                    {
                        string sQuery = sCriteria.Substring(0, sCriteria.IndexOf(","));
                        string sTable = sQuery.Substring(sQuery.IndexOf(" "), sQuery.IndexOf(".") - sQuery.Substring(0, sQuery.IndexOf(" ")).Length);
                        sTableField = sQuery.Remove(0, sQuery.IndexOf(".") + 1);
                        sQuery = sQuery.Insert(7, " Distinct ");
                        //'Select USERINFO.Badgenumber
                        strSelectQuery = sQuery + " From " + sTable;
                    }
                    else
                    {
                        strSelectQuery = "Select Distinct " + sTableField + " From " + sTableName;
                    }
                    //Dim mydata As OleDb.OleDbDataAdapter = New System.Data.OleDb.OleDbDataAdapter(strSelectQuery, MobjOleDbConnection)
                    OleDbDataAdapter mydata = new OleDbDataAdapter(strSelectQuery, MobjOleDbConnection);
                    mydata.Fill(dtMyData);
                    MobjOleDbConnection.Close();
                    if (dtMyData.Rows.Count > 0)
                    {
                        ColPunchID.DataSource = dtMyData;
                        ColPunchID.DisplayMember = sTableField;
                        ColPunchID.ValueMember = sTableField;
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLog.WriteLog("Error on TestAccessDbConnection:FrmFetchdata." + ex.Message.ToString() + "", 2);
                //MessageBox.Show("Error on TestAccessDbConnection:FrmFetchdata." & Ex.Message.ToString(), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
            }
        }
        private bool GetAccessDbAndSqlDbTableDetails(int iTemplateId, ref string sUserName, ref string sPassword, ref string sTableName, ref string sServerName, ref string sCriteria, ref string sDatabaseName, ref string sTableField)
        {
            sUserName = "";
            sPassword = "";
            sTableName = "";
            sCriteria = "";
            sServerName = "";
            sDatabaseName = "";
            try
            {

                DataTable datData = MobjclsBLLAttendance.FillCombos(new string[] { "[UserName],[Password],[TableName],isnull(ServerName,'')as ServerName,isnull(Criteria,'') as Criteria, isnull (DatabaseName,'') as DatabaseName", "[PayAttendanceTemplateFileDetails]", "[TemplateMasterID]= " + iTemplateId });
                DataTable DtFileDetils = MobjclsBLLAttendance.GetTemplateFieldDetails(iTemplateId);
                if (DtFileDetils != null)
                {
                    if (DtFileDetils.Rows.Count > 0)
                    {
                        for (int iCounter = 0; iCounter <= DtFileDetils.Rows.Count - 1; iCounter++)
                        {

                            if (DtFileDetils.Rows[iCounter]["Templatefield"].ToStringCustom() == "EmployeeNumber")
                                sTableField = DtFileDetils.Rows[iCounter]["Actualfield"].ToStringCustom();

                            //if (DtFileDetils.Rows[iCounter]["Templatefield"].ToStringCustom() == "Date")
                            //    strDate = DtFileDetils.Rows[iCounter]["Actualfield"].ToStringCustom();
                            //if (intTimeCount == 1)
                            //{
                            //    if (DtFileDetils.Rows[iCounter]["Templatefield"].ToStringCustom() == "Time")
                            //        strTime1 = DtFileDetils.Rows[iCounter]["Actualfield"].ToStringCustom();

                            //}
                            //else if (intTimeCount == 2)
                            //{
                            //    if (strTime1 == "")
                            //    {
                            //        if (DtFileDetils.Rows[iCounter]["Templatefield"].ToStringCustom() == "Time")
                            //            strTime1 = DtFileDetils.Rows[iCounter]["Actualfield"].ToStringCustom();
                            //    }


                            //    if (strTime1 != "")
                            //    {
                            //        if (DtFileDetils.Rows[iCounter]["Templatefield"].ToStringCustom() == "Time")
                            //            strTime2 = DtFileDetils.Rows[iCounter]["Actualfield"].ToStringCustom();
                            //    }
                            //}


                        }
                    }
                }
                //string sQuery = "SELECT [UserName],[Password],[TableName],isnull(ServerName,'')as ServerName,isnull(Criteria,'') as Criteria, isnull (DatabaseName,'') as DatabaseName FROM [AttendanceFileDetails] where [TemplateMasterID]= " + iTemplateId;
                //SqlDataReader sdrTemplate As SqlDataReader = mObjAttenCon.ExecutesReader(sQuery)

                if (datData.Rows.Count > 0)
                {
                    sUserName = Convert.ToString(datData.Rows[0]["UserName"]);
                    sPassword = Convert.ToString(datData.Rows[0]["Password"]);
                    sTableName = Convert.ToString(datData.Rows[0]["TableName"]);
                    sCriteria = Convert.ToString(datData.Rows[0]["Criteria"]);
                    sServerName = Convert.ToString(datData.Rows[0]["ServerName"]);
                    sDatabaseName = Convert.ToString(datData.Rows[0]["DatabaseName"]);
                }

                return true;
            }
            catch (Exception ex)
            {
                ClsLog.WriteLog("Error on GetAccessDbAndSqlDbTableDetails: " + this.Name + " " + ex.Message.ToString(), 2);
                return false;
            }
        }

        private string GetMonth(int iMonth)
        {

            string sMonth = "";
            if (iMonth >= 1 && iMonth <= 12)
                return Microsoft.VisualBasic.DateAndTime.MonthName(iMonth, true);

            return sMonth;
        }

        private bool GetAlluserInfo()//Get All userInfo From Device
        {

            bool blnRetValue = false;
            //try
            //{
            //    this.Cursor = Cursors.WaitCursor;
            //    int i = 0;
            //    DataTable dtData = MobjclsBLLAtnMapping.GetFingerTechDeviceSettings(Convert.ToInt32(AttendnaceDevices.FingerTech));// 1 for finger tech
            //    string[] sCurrent = new string[2];
            //    DataTable dtGetData = new DataTable();
            //    dtGetData.Columns.Add("EnrollNo");
            //    dtGetData.Columns.Add("Name");

            //    if (dtData.Rows.Count > 0)
            //    {
            //        for (int iRwCnt = 0; iRwCnt <= dtData.Rows.Count - 1; iRwCnt++)
            //        {
            //            string strIPAddress = Convert.ToString(dtData.Rows[iRwCnt]["IPAddress"]);
            //            int intPort = Convert.ToInt32(dtData.Rows[iRwCnt]["Port"]);
            //            int intCommKey = Convert.ToInt32(dtData.Rows[iRwCnt]["CommKey"]);
            //            string strModel = Convert.ToString(dtData.Rows[iRwCnt]["Model"]);
            //            int intDelay = Convert.ToInt32(dtData.Rows[iRwCnt]["Delay"]);
            //            int intTimeout = Convert.ToInt32(dtData.Rows[iRwCnt]["Timeout"]);
            //            int intDeviceNo = Convert.ToInt32(dtData.Rows[iRwCnt]["DeviceNo"]);
            //            int intTemplateID = Convert.ToInt32(dtData.Rows[iRwCnt]["DeviceID"]);

            //            ToolStripStatus.Text = "Connecting to Device..";
            //            if (BioBridgeSDK.Connect_TCPIP(strModel, intDeviceNo, strIPAddress, intPort, intCommKey) == 0)
            //            {
            //                if (BioBridgeSDK.GetDeviceStatus(6, ref i) == 0)
            //                {
            //                    int intEnrollNo = 0;
            //                    string sName = "";
            //                    string sPassWord = "";
            //                    int intPrivilage = 0;
            //                    bool blnIsEnabled = false;
            //                    int intLogSize = 0;

            //                    if (BioBridgeSDK.ReadGeneralLog(ref intLogSize) == 0)
            //                    {

            //                        while ((BioBridgeSDK.GetAllUserInfo(ref intEnrollNo, ref sName, ref sPassWord, ref intPrivilage, ref blnIsEnabled) == 0))
            //                        {
            //                            sCurrent[0] = intEnrollNo.ToString();
            //                            sCurrent[1] = sName;

            //                            dtGetData.Rows.Add(sCurrent);
            //                            ToolStripStatus.Text = "Getting Id  from Device..";
            //                        }


            //                        ToolStripStatus.Text = "Getting Id  from Device..";

            //                    }
            //                }

            //                BioBridgeSDK.Disconnect();
            //            }
            //            else
            //            {
            //                ToolStripStatus.Text = "Please Check Device Settings ";
            //            }


            //        }//for

            //        if (dtGetData != null)
            //        {
            //            if (dtGetData.Rows.Count > 0)
            //            {
            //                blnRetValue = true;
            //                ColPunchID.Items.Clear();
            //                ColPunchID.DataSource = null;
            //                ColPunchID.ValueMember = "EnrollNo";
            //                ColPunchID.DisplayMember = "EnrollNo";
            //                ColPunchID.DataSource = dtGetData;
            //            }
            //        }

            //        ToolStripStatus.Text = "";

            //    }
            //    this.Cursor = Cursors.Default;
            //}
            //catch (Exception Ex)
            //{
            //    this.Cursor = Cursors.Default;
            //    string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
            //    Clslog. WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            //    blnRetValue = false;
            //}
            return blnRetValue;

        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.AttendanceMapping, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            if (MblnUpdatePermission || MblnAddPermission)
                MblnUpdatePermission = true;

            btnOk.Enabled = MblnUpdatePermission;
            BtnSaveItem.Enabled = MblnUpdatePermission;
            btnDelete.Enabled = MblnDeletePermission;



        }

        private void Changestatus()
        {
            //function for changing status



            btnOk.Enabled = MblnUpdatePermission;
            BtnSaveItem.Enabled = MblnUpdatePermission;


            errproMapping.Clear();
            //MblnChangeStatus = true;

        }

        private bool LoadCombo(int intType)
        {
            bool blnRetValue = false;
            try
            {
                if (intType == 1)
                {
                    CboDevice.DataSource = null;
                    CboDevice.Items.Clear();

                    DataTable datCombos = new DataTable();
                    datCombos = MobjclsBLLAttendance.FillCombos(new string[] { "DeviceID,DeviceName", "PayAttendanceDeviceSettings", "DeviceTypeID in(" + Convert.ToInt32(AttendnaceDevices.FingerTech) + ")", "DeviceID", "DeviceName" });
                    CboDevice.ValueMember = "DeviceID";
                    CboDevice.DisplayMember = "DeviceName";
                    CboDevice.DataSource = datCombos;
                    blnRetValue = true;
                }
                if (intType == 2)
                {
                    CboDevice.DataSource = null;
                    CboDevice.Items.Clear();

                    DataTable datCombos = new DataTable();
                    datCombos = MobjclsBLLAttendance.FillCombos(new string[] { "TemplateMasterID,TemplateName", "PayAttendanceTemplateMaster", "FileTypeID IN(" + (int)AttendanceFiletype.Csv + "," + (int)AttendanceFiletype.Excel + "," + (int)AttendanceFiletype.AccessDB + ")" });
                    CboDevice.ValueMember = "TemplateMasterID";
                    CboDevice.DisplayMember = "TemplateName";
                    CboDevice.DataSource = datCombos;
                    blnRetValue = true;
                }
                if (intType == 3)
                {
                    // ltrim(rtrim([WorkID])) WorkID , ltrim(rtrim([WorkID])) WorkID
                    ColPunchID.DataSource = null;
                    //ColPunchID.Items.Clear();

                    string strCondition = rdbDevice.Checked ? " SettingsID=" + CboDevice.SelectedValue.ToInt32() + "" : "TemplateMasterID=" + CboDevice.SelectedValue.ToInt32() + "";
                    DataTable datCombos = new DataTable();
                    ColPunchID.DataSource = null;
                    // datCombos = MobjclsBLLAttendance.FillCombos(new string[] { "ltrim(rtrim([WorkID])) WorkID , ltrim(rtrim([WorkID])) WorkID", "PayEmployeeAttendanceMapping", "TemplateMasterID=" + CboDevice.SelectedValue.ToInt32() + "" });
                    datCombos = MobjclsBLLAttendance.FillCombos(new string[] { "ltrim(rtrim([WorkID])) Column1 , ltrim(rtrim([WorkID])) Column1", "PayEmployeeAttendanceMapping", strCondition });

                    ColPunchID.ValueMember = "Column1";
                    ColPunchID.DisplayMember = "Column1";
                    ColPunchID.DataSource = datCombos;
                    blnRetValue = true;
                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                ClsLog.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                blnRetValue = false;
            }
            return blnRetValue;

        }

        private bool FillEmployeesToGrid()
        {
            try
            {
                int iSettingsID = 0;
                int iEquipmentID = 0;
                int iTemplateID = 0;
                if (rdbDevice.Checked)
                {
                    iSettingsID = Convert.ToInt32(CboDevice.SelectedValue);
                    iEquipmentID = (int)AttendnaceDevices.FingerTech;

                }
                else
                {
                    iTemplateID = Convert.ToInt32(CboDevice.SelectedValue);
                }
                DataTable dtEmployees = MobjclsBLLAtnMapping.GetEmployees(iEquipmentID, iSettingsID, iTemplateID);
                if (dtEmployees.Rows.Count > 0)
                {
                    DgvAtnMapping.Rows.Clear();
                    DgvAtnMapping.RowCount = 0;
                    int intRw = 0;
                    ColPunchID.ValueMember = "WorkID";
                    ColPunchID.DisplayMember = "WorkID";
                    ColPunchID.DataSource = dtEmployees.DefaultView.ToTable(true,"WorkID");
                    for (int i = 0; i <= dtEmployees.Rows.Count - 1; i++)
                    {
                        intRw = DgvAtnMapping.Rows.Count;
                        DgvAtnMapping.RowCount = DgvAtnMapping.RowCount + 1;
                        DgvAtnMapping.Rows[intRw].Cells["ColEmpName"].Tag = dtEmployees.Rows[i]["EmployeeID"].ToInt32();
                        DgvAtnMapping.Rows[intRw].Cells["ColEmpName"].Value = dtEmployees.Rows[i]["FirstName"].ToStringCustom();
                        DgvAtnMapping.Rows[intRw].Cells["ColEmpNumber"].Value = dtEmployees.Rows[i]["EmployeeNumber"].ToStringCustom();
                        DgvAtnMapping.Rows[intRw].Cells["ColCompanyID"].Value = dtEmployees.Rows[i]["CompanyID"].ToInt32();
                        if (dtEmployees.Rows[i]["WorkID"].ToStringCustom() != string.Empty)
                        {
                            DgvAtnMapping.Rows[intRw].Cells["ColPunchID"].Value = dtEmployees.Rows[i]["WorkID"].ToStringCustom();
                        }
                        DgvAtnMapping.CurrentCell = DgvAtnMapping[1, intRw];
                        DgvAtnMapping.Refresh();
                    }

                }
                return true;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                ClsLog.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }


        }

        private bool DeleteGridRow(int intRowIndex)
        {
            bool blnRetvalue = false;
            try
            {
                int intEmpID = DgvAtnMapping.Rows[intRowIndex].Cells["ColEmpName"].Tag.ToInt32();
                int intSettingsID = rdbDevice.Checked ? CboDevice.SelectedValue.ToInt32() : 0;//from Device Settings
                int intTemplateID = rdbDevice.Checked ? 0 : CboDevice.SelectedValue.ToInt32();
                blnRetvalue = MobjclsBLLAtnMapping.DeleteEmployeeMapping(intEmpID, intSettingsID, intTemplateID);
            }
            catch (Exception Ex)
            {
                ClsLog.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

            }

            return blnRetvalue;
        }

        private bool FillParameter()
        {
            try
            {
                bool blnRetValue = false;
                MobjclsBLLAtnMapping.lstclsDTOAtnMapping = new List<clsDTOAttendanceMapping>();
                for (int i = 0; i <= DgvAtnMapping.Rows.Count - 1; i++)
                {
                    if (DgvAtnMapping.Rows[i].Cells["ColPunchID"].Value.ToStringCustom() == string.Empty)
                        continue;
                    clsDTOAttendanceMapping objClsDtoAtnMapping = new clsDTOAttendanceMapping();

                    objClsDtoAtnMapping.intEmployeeID = Convert.ToInt32(DgvAtnMapping.Rows[i].Cells["ColEmpName"].Tag);
                    objClsDtoAtnMapping.intEquipmentID = rdbDevice.Checked ? Convert.ToInt32(AttendnaceDevices.FingerTech) : 0;//For Fingertech
                    objClsDtoAtnMapping.strWorkID = Convert.ToString(DgvAtnMapping.Rows[i].Cells["ColPunchID"].Value);
                    objClsDtoAtnMapping.blnActive = rdbDevice.Checked ? true : false;
                    objClsDtoAtnMapping.intTemplateMasterID = rdbDevice.Checked ? 0 : CboDevice.SelectedValue.ToInt32();//for Device =0  fro template=template ID
                    objClsDtoAtnMapping.intsettingsID = rdbDevice.Checked ? Convert.ToInt32(CboDevice.SelectedValue) : 0;//from Device Settings
                    //objClsDtoAtnMapping.intType = 2;//for device =2,for template =1
                    objClsDtoAtnMapping.intComapnyID = Convert.ToInt32(DgvAtnMapping.Rows[i].Cells["ColCompanyID"].Value);
                    MobjclsBLLAtnMapping.lstclsDTOAtnMapping.Add(objClsDtoAtnMapping);
                    blnRetValue = true;
                }
                return blnRetValue;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                ClsLog.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }

        }

        private bool FormValidation()
        {
            if (CboDevice.SelectedValue.ToInt32() <= 0)
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 5552, out MmessageIcon); ;// UserMessage.GetMessageByCode(5552, 2);
                MstrMessageCommon = rdbTemplates.Checked ? MstrMessageCommon.Replace("device", "template") : MstrMessageCommon;
                errproMapping.SetError(CboDevice, MstrMessageCommon);
                MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //UserMessage.ShowMessage(5552);
                CboDevice.Focus();
                return false;
            }
            return true;

        }

        private bool SaveAttendanceMapping()
        {
            bool blnRetvlaue = false;
            if (FormValidation())
            {
                if (DgvAtnMapping.Rows.Count > 0)
                {
                    if (CheckDuplicatePunchID(2) == -1)
                    {
                        int iSettingsId = Convert.ToInt32(CboDevice.SelectedValue);
                        MobjclsBLLAtnMapping.DeleteMappings(Convert.ToInt32(AttendnaceDevices.FingerTech), iSettingsId, MintTemplateID);//DELETE EXISTING DETAILS
                        if (FillParameter())
                        {
                            if (MobjclsBLLAtnMapping.SaveAtnMapping())
                            {
                                //UserMessage.ShowMessage(2,null,2); ;
                                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                                blnRetvlaue = true;
                            }
                        }
                    }
                    else
                    {
                        //UserMessage.ShowMessage(5550,null,2);
                        MstrMessageCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 5550, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
            }

            return blnRetvlaue;
        }

        private bool DeleteMappings()
        {
            bool retValue = false;
            try
            {

                if (FormValidation())
                {
                    if (Convert.ToInt32(CboDevice.SelectedValue) > 0)
                    {
                        int iEquipmentID = 0;//For Finger Tech  
                        int iSettingsID = 0;
                        int iTemplateID = 0;
                        if (rdbDevice.Checked)
                        {
                            iEquipmentID = Convert.ToInt32(AttendnaceDevices.FingerTech);
                            iSettingsID = Convert.ToInt32(CboDevice.SelectedValue);
                        }
                        else
                        {
                            iTemplateID = Convert.ToInt32(CboDevice.SelectedValue);
                        }

                        retValue = MobjclsBLLAtnMapping.DeleteMappings(iEquipmentID, iSettingsID, iTemplateID);
                    }
                }
                return retValue;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                ClsLog.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return retValue;
            }
        }

        private int CheckDuplicatePunchID(int ColIndex)
        {
            string SearchValue = "";

            int RowIndexTemp = 0;


            int CheckDuplicatePunchID = -1;
            try
            {

                foreach (DataGridViewRow rowValue in DgvAtnMapping.Rows)
                {
                    if (rowValue.Cells[ColIndex].Value.ToStringCustom() != string.Empty)
                    {


                        SearchValue = rowValue.Cells[ColIndex].Value.ToString();
                        RowIndexTemp = rowValue.Index;
                        foreach (DataGridViewRow row in DgvAtnMapping.Rows)
                        {
                            if (RowIndexTemp != row.Index)
                            {
                                if (row.Cells[ColIndex].Value != null)
                                {
                                    if (row.Cells[ColIndex].Value.ToString() == SearchValue)
                                    {
                                        CheckDuplicatePunchID = row.Index;
                                        DgvAtnMapping.CurrentCell = DgvAtnMapping[ColIndex, CheckDuplicatePunchID];
                                        return CheckDuplicatePunchID;
                                    }
                                }
                            }



                        }
                    }


                }
                return CheckDuplicatePunchID;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                ClsLog.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return CheckDuplicatePunchID;
            }


        }

        #endregion METHODS

        #region CONTROL EVENTS

        private void FrmAttendanceMapping_Load(object sender, EventArgs e)
        {

            SetPermissions();
            ResetForm();
            FillTemplates();
            CboDevice.SelectedIndex = -1;
            LoadMessage();

       
        }
        //#region SetArabicControls
        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.AttendanceMapping, this);
        //}
        //#endregion SetArabicControls

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (DgvAtnMapping.RowCount > 0)
                {
                    if (DeleteMappings())
                    {
                        //UserMessage.ShowMessage(5551,null,2);
                        MstrMessageCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 5551, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                        DgvAtnMapping.Rows.Clear();

                    }
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Error on : " + this.Name.ToString() + " " + Ex.Message.ToString());
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                ClsLog.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }
        }

        private void CboDevice_SelectionChangeCommitted(object sender, EventArgs e)
        {
            DgvAtnMapping.Rows.Clear();
        }

        private void CboDevice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            { CboDevice_SelectionChangeCommitted(sender, new EventArgs()); }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveAttendanceMapping())
                this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (DgvAtnMapping.Rows.Count > 0)
                    SaveAttendanceMapping();
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                ClsLog.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }

        }

        private void BtnFillEmployees_Click(object sender, EventArgs e)
        {
            try
            {
                if (FormValidation())
                {
                    this.Cursor = Cursors.WaitCursor;
                    if (rdbDevice.Checked)
                    {

                        GetAlluserInfo();//filling Punchid Combobox --wokid taken From device

                    }
                    else
                    {
                        LoadCombo(3);
                    }
                    FillEmployeesToGrid();
                    this.Cursor = Cursors.Default;

                }
            }
            catch (Exception Ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("Error: " + Ex.ToString());
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                ClsLog.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

            }



        }

        private void CboDevice_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus();
            errproMapping.Clear();
        }

        private void rdbTemplates_CheckedChanged(object sender, EventArgs e)
        {
            Changestatus();
            ResetForm();
        }

        private void rdbDevice_CheckedChanged(object sender, EventArgs e)
        {
            Changestatus();
            ResetForm();
        }

        private void DgvAtnMapping_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void DgvAtnMapping_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Changestatus();
        }

        private void DgvAtnMapping_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (DgvAtnMapping.IsCurrentCellDirty)
                {
                    if (DgvAtnMapping.CurrentCell != null)
                        DgvAtnMapping.CommitEdit(DataGridViewDataErrorContexts.Commit);

                }
            }
            catch
            {
            }
        }

        private void DgvAtnMapping_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && MblnDeletePermission)
            {
                if (DgvAtnMapping.Rows.Count > 0)
                {
                    if (DgvAtnMapping.SelectedRows.Count > 0)
                    {
                        int intRowIndex = 0;
                        foreach (DataGridViewRow Row in DgvAtnMapping.SelectedRows)
                        {
                            intRowIndex = Row.Index;

                            if (DeleteGridRow(intRowIndex))
                            {
                            }
                            else
                            {
                                //e.Handled = true;
                            }
                        }


                    }
                }
            }
        }



        #endregion CONTROL EVENTS


    }//Class
}//Name Space
