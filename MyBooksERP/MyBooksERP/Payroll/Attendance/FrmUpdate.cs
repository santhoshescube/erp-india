﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Microsoft.VisualBasic;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;

namespace MyBooksERP
{
    /*****************************************************
    * Created By       : Arun
    * Creation Date    : 17 Apr 2012
    * Description      : Handle Attendance Device Settings
    * FromID           : 127
    * ***************************************************/
    public partial class FrmUpdate : Form
    {
        public ArrayList RowListData = new ArrayList();
        public string[] pDetailData;
        //Dim MaMessageArr As ArrayList ' Error Message display
        //Dim MaStatusMessage As ArrayList
        private string MstrMessCommon = "";
        string GlStrMessageCaption = "";

        public ArrayList punchingCollection = new ArrayList();
        public int piCurrentIndex = 0;
        public bool PblnOkbutton = false;

        public bool PblnChange = false; //For auto fill cheking
        private string MstrCellTime = "";

        private int iMaxColumCount = 0;
        private int iCompanyID = 0;
        private ColorDialog ColDigBox;
        bool Glbln24HrFormat = ClsCommonSettings.Glb24HourFormat;
        bool MblnPayExists = true;
        private clsMessage ObjUserMessage = null;
        clsBLLAttendance MobjclsBLLAttendance;
        ClsLogWriter MobjClsLogs;
        private MessageBoxIcon MmessageIcon;
        //private ArrayList MsarMessageArr;
        ClsNotification MobjClsNotification;
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.AttendanceUpdate);
                return this.ObjUserMessage;
            }
        }

        public FrmUpdate(ArrayList RowList, string[] DetailData)
        {
            InitializeComponent();
            ColDigBox = new ColorDialog();
            MobjclsBLLAttendance = new clsBLLAttendance();
            MobjClsLogs = new ClsLogWriter(Application.StartupPath);
            RowListData = RowList;
            pDetailData = DetailData;
            iMaxColumCount = RowList.Count;
            GlStrMessageCaption = ClsCommonSettings.MessageCaption == "" ? "PayRoll" : ClsCommonSettings.MessageCaption;
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}


        }


        //#region SetArabicControls
        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.AttendanceUpdate, this);
        //}
        //#endregion SetArabicControls


        private void FrmUpdate_Load(object sender, EventArgs e)
        {
            try
            {
                FillGrid(true);
                TimeScheduleChart();
                tmrClearlabels.Interval = ClsCommonSettings.TimerInterval;
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    this.Text = "الحضور وقت " + pDetailData[0];//'EmployeeName
                //}
                //else
                //{
                    this.Text = "Time attendance of " + pDetailData[0];//'EmployeeName
                //}

                lblDateDisplay.Text = pDetailData[5]; //'Date
                TxtBreakTime.Text = pDetailData[6];
                TxtBreakTime.Tag = pDetailData[6];
                if (pDetailData[7] == "1")
                {
                    lblLateComing.Visible = true;
                    lblLateComing.Tag = 1;
                }
                else
                {
                    lblLateComing.Visible = false;
                    lblLateComing.Tag = 0;
                }

                if (pDetailData[8] == "1")
                {
                    LblEarlyGoing.Visible = true;
                    LblEarlyGoing.Tag = 1;
                }
                else
                {
                    LblEarlyGoing.Visible = false;
                    LblEarlyGoing.Tag = 0;
                }
                TxtFoodBreak.Text = pDetailData[10]; //'FoodBreakTime
                TxtFoodBreak.Tag = pDetailData[10];
                TxtOverTime.Text = pDetailData[11];// 'OverTime
                TxtOverTime.Tag = pDetailData[11];//
                txtWorkTime.Text = pDetailData[9];//; 'WorkTime
                txtWorkTime.Tag = pDetailData[9];//
                LblShiftDesc.Text = pDetailData[3];//'Shift Descr
                iCompanyID = Convert.ToInt32(pDetailData[2]);//'CompanyID
                DgvTimeScheuleGraph.ClearSelection();//
            }
            catch (Exception Ex)
            {

                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

            }
        }
        private void FrmUpdate_FormClosing(object sender, FormClosingEventArgs e)
        {
            MobjclsBLLAttendance = null;
            MobjClsLogs = null;

        }


        private bool TimeDiffCalculation()
        {
            try
            {


                int tmDiffBreak = 0;
                int tmDiffWork = 0;
                //int tmWorkTotal = 0;
                //int tmBreaktime = 0;

                for (int i = 0; i <= dgvUpdate.Rows.Count - 2; i++)
                {
                    dgvUpdate.Rows[i].Cells["ColSlNo"].Value = i + 1;
                    if (Convert.ToString(dgvUpdate.Rows[i].Cells["ColPunching"].Value) == "")
                        return false;
                    dgvUpdate.Rows[i].Cells["ColBreakTime"].Value = "";
                    dgvUpdate.Rows[i].Cells["ColWorkTime"].Value = "";

                    if (i % 2 == 0)
                    {
                        dgvUpdate.Rows[i].Cells["ColEntryExit"].Value = "Entry";
                        if (i != 0)
                        {

                            tmDiffBreak = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(dgvUpdate.Rows[i - 1].Cells["ColPunching"].Value), Convert.ToDateTime(dgvUpdate.Rows[i].Cells["ColPunching"].Value));
                            string sWorkTime = Convert.ToString(tmDiffBreak);
                            sWorkTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sWorkTime), DateTime.Today));
                            sWorkTime = Convert.ToDateTime(sWorkTime).ToString("HH:mm:ss");
                            dgvUpdate.Rows[i].Cells["ColBreakTime"].Value = sWorkTime;
                        }
                    }
                    else
                    {
                        dgvUpdate.Rows[i].Cells["ColEntryExit"].Value = "Exit";
                        tmDiffWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(dgvUpdate.Rows[i - 1].Cells["ColPunching"].Value), Convert.ToDateTime(dgvUpdate.Rows[i].Cells["ColPunching"].Value));


                        string sWorkTime = Convert.ToString(tmDiffWork);
                        sWorkTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sWorkTime), DateTime.Today));
                        sWorkTime = Convert.ToDateTime(sWorkTime).ToString("HH:mm:ss");
                        dgvUpdate.Rows[i].Cells["ColWorkTime"].Value = sWorkTime;


                    }

                }
                return true;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }

        private bool FillGrid(bool bLoad)
        {
            try
            {
                dgvUpdate.Rows.Clear();
                int tmDiffBreak = 0;
                int tmDiffWork = 0;
                for (int i = 0; i <= RowListData.Count - 1; i++)
                {
                    dgvUpdate.Rows.Add();
                    dgvUpdate.Rows[i].Cells["ColSlNo"].Value = i + 1;
                    dgvUpdate.Rows[i].Cells["ColPunching"].Value = RowListData[i];
                    if (i % 2 == 0)
                    {
                        dgvUpdate.Rows[i].Cells["ColEntryExit"].Value = "Entry";
                        if (i != 0)
                        {
                            tmDiffBreak = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(RowListData[i - 1]), Convert.ToDateTime(RowListData[i]));
                            string sWorkTime = Convert.ToString(tmDiffBreak);
                            sWorkTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sWorkTime), DateTime.Today));
                            sWorkTime = Convert.ToDateTime(sWorkTime).ToString("HH:mm:ss");
                            dgvUpdate.Rows[i].Cells["ColBreakTime"].Value = sWorkTime;
                        }
                    }
                    else
                    {
                        dgvUpdate.Rows[i].Cells["ColEntryExit"].Value = "Exit";
                        tmDiffWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(RowListData[i - 1]), Convert.ToDateTime(RowListData[i]));
                        string sWorkTime = Convert.ToString(tmDiffWork);
                        sWorkTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sWorkTime), DateTime.Today));
                        sWorkTime = Convert.ToDateTime(sWorkTime).ToString("HH:mm:ss");
                        dgvUpdate.Rows[i].Cells["ColWorkTime"].Value = sWorkTime;


                    }
                    // ' dgvUpdate.Rows(i).Cells("WorkTime").Value = RowListData(i)

                }
                //'WorkTimeCalculation(pDetailData)

                return true;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }


        private void TimeScheduleChart()
        {
            try
            {
                DgvTimeScheuleGraph.Columns.Clear();
                int iRowCount = dgvUpdate.RowCount;
                for (int i = 0; i <= dgvUpdate.RowCount - 1; i++)//'ColBreakTime(ColWorkTime)
                {
                    if (i != 0)
                    {
                        if (i % 2 == 0)
                        {
                            if (Convert.ToString(dgvUpdate.Rows[i].Cells["ColBreakTime"].Value) != "")
                            {
                                DateTime sBreakTime = Convert.ToDateTime(dgvUpdate.Rows[i].Cells["ColBreakTime"].Value);
                                int iHour = sBreakTime.Hour;
                                iHour = sBreakTime.Minute + iHour * 60;
                                string str = "Col" + i.ToString();
                                DgvTimeScheuleGraph.Columns.Add(str, "Col");
                                DgvTimeScheuleGraph.Columns[str].MinimumWidth = 2;
                                DgvTimeScheuleGraph.Columns[str].Width = iHour;
                                DgvTimeScheuleGraph.Rows[0].Cells[i - 1].ToolTipText = dgvUpdate.Rows[i - 1].Cells["ColPunching"].Value + "-" + dgvUpdate.Rows[i].Cells["ColPunching"].Value; //'Format(sBreakTime, "HH:mm ss")
                                DgvTimeScheuleGraph.Rows[0].Cells[i - 1].Style.BackColor = PicBoxBreakTime.BackColor; //'Color.Red
                                DgvTimeScheuleGraph.Rows[0].Cells[i - 1].Style.SelectionBackColor = PicBoxBreakTime.BackColor; //'Color.Red
                            }
                        }
                        else
                        {
                            if (Convert.ToString(dgvUpdate.Rows[i].Cells["ColWorkTime"].Value) != "")
                            {
                                DateTime sWorktime = Convert.ToDateTime(dgvUpdate.Rows[i].Cells["ColWorkTime"].Value);
                                int iHour = sWorktime.Hour;
                                iHour = sWorktime.Minute + iHour * 60;
                                string str = "Col" + i.ToString();
                                DgvTimeScheuleGraph.Columns.Add(str, "Col");
                                DgvTimeScheuleGraph.Columns[str].MinimumWidth = 2;
                                DgvTimeScheuleGraph.Columns[str].Width = iHour;
                                DgvTimeScheuleGraph.Rows[0].Cells[i - 1].ToolTipText = dgvUpdate.Rows[i - 1].Cells["ColPunching"].Value + "-" + dgvUpdate.Rows[i].Cells["ColPunching"].Value; //'Format(sWorktime, "HH:mm ss")
                                DgvTimeScheuleGraph.Rows[0].Cells[i - 1].Style.BackColor = PicBoxWorkTime.BackColor;//'Color.Green
                                DgvTimeScheuleGraph.Rows[0].Cells[i - 1].Style.SelectionBackColor = PicBoxWorkTime.BackColor;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

            }
        }
        private int GetShiftOrderNo(int iShiftId, string sFirstTime, ref  string sFromTime, ref string sToTime, ref string sDur) // 'Finding shift OrderNo from First Punch time
        {
            int iShiftOrderNo = 0;
            int iBufferTime = 0;
            sFromTime = "";
            sToTime = "";
            // string sPrevToTime = "";
            bool blnNightshiftFlag = false;
            try
            {

                DataTable DtShiftDetails = MobjclsBLLAttendance.GetDynamicShiftDetails(iShiftId);
                if (DtShiftDetails.Rows.Count > 0)
                {
                    int iDuration = 0;

                    for (int i = 0; i <= DtShiftDetails.Rows.Count - 1; i++)
                    {

                        iShiftOrderNo = 0;
                        iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNO"]);
                        sDur = Convert.ToString(DtShiftDetails.Rows[i]["Duration"]);
                        iDuration = GetDurationInMinutes(sDur);

                        iBufferTime = Convert.ToInt32(DtShiftDetails.Rows[i]["BufferTime"]);
                        if (Glbln24HrFormat == true)
                        {
                            sFromTime = Convert.ToString((DtShiftDetails.Rows[i]["FromTime"]));
                            sToTime = Convert.ToString((DtShiftDetails.Rows[i]["ToTime"]));
                        }
                        else
                        {
                            //sFromTime = ConvertDate12((DtShiftDetails.Rows[i]["FromTime"]));
                            //sToTime = ConvertDate12((DtShiftDetails.Rows[i]["ToTime"]));
                        }
                        int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iDuration), Convert.ToDateTime(sToTime));
                        if (tmDurationDiff < 0)
                        {
                            blnNightshiftFlag = true;
                            sFromTime = Convert.ToDateTime(sFromTime).ToString();
                            sToTime = Convert.ToDateTime(sToTime).AddDays(1).ToString();
                        }

                        if (Convert.ToDateTime(sFirstTime) >= Convert.ToDateTime(sFromTime).AddMinutes(-iBufferTime) && Convert.ToDateTime(sFirstTime) <= Convert.ToDateTime(sFromTime).AddMinutes(iBufferTime))
                        {
                            iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNO"]);
                            break;
                        }

                    } //for i
                    if (iShiftOrderNo <= 0)
                    {
                        int iTime = 0;
                        //int iorde = 0;
                        iShiftOrderNo = 0;
                        string sFrom = "";
                        string sTo = "";
                        string sShiftDur = "";
                        for (int j = 0; j <= DtShiftDetails.Rows.Count - 1; j++)
                        {
                            iDuration = 0;
                            blnNightshiftFlag = false;
                            sDur = Convert.ToString(DtShiftDetails.Rows[j]["Duration"]);
                            iDuration = GetDurationInMinutes(sDur);
                            iBufferTime = Convert.ToInt32(DtShiftDetails.Rows[j]["BufferTime"]);
                            if (Glbln24HrFormat == false)
                            {
                                sFromTime = Convert.ToString(DtShiftDetails.Rows[j]["FromTime"]);
                                DateTime dt = Convert.ToDateTime(sFromTime);
                                sFromTime = ConvertDate12(dt);
                                sToTime = Convert.ToString(DtShiftDetails.Rows[j]["ToTime"]);
                                dt = Convert.ToDateTime(sToTime);
                                sToTime = ConvertDate12(dt);
                            }
                            else
                            {
                                sFromTime = Convert.ToString(DtShiftDetails.Rows[j]["FromTime"]);
                                sToTime = Convert.ToString(DtShiftDetails.Rows[j]["ToTime"]);
                            }
                            int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iDuration), Convert.ToDateTime(sToTime));
                            if (tmDurationDiff < 0)
                            {
                                blnNightshiftFlag = true;
                                sFromTime = Convert.ToDateTime(sFromTime).ToString();
                                sToTime = Convert.ToDateTime(sToTime).AddDays(1).ToString();
                            }
                            if (j == 0)
                            {
                                iTime = Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)));
                                iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[j]["OrderNO"]);
                                sShiftDur = sDur;
                                sFrom = sFromTime;
                                sTo = sToTime;
                            }
                            else
                            {
                                if (blnNightshiftFlag)
                                {
                                    bool flag = false;
                                    if (Convert.ToDateTime(sFirstTime) >= Convert.ToDateTime("12:00 AM") && Convert.ToDateTime(sFirstTime) <= Convert.ToDateTime("12:00 PM"))
                                    {
                                        flag = true;
                                    }
                                    if (Convert.ToDateTime(sFirstTime) >= Convert.ToDateTime(sFromTime) && Convert.ToDateTime(sFirstTime) <= Convert.ToDateTime("11:59 PM"))
                                    {
                                        if (Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime))) < iTime)
                                        {
                                            iTime = Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)));
                                            iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[j]["OrderNO"]);
                                            sShiftDur = sDur;
                                            sFrom = sFromTime;
                                            sTo = sToTime;
                                        }
                                    }
                                    else if (Convert.ToDateTime(sFirstTime) <= Convert.ToDateTime(sFromTime))
                                    {
                                        if (Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime))) < iTime)
                                        {
                                            iTime = Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)));
                                            iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[j]["OrderNO"]);
                                            sShiftDur = sDur;
                                            sFrom = sFromTime;
                                            sTo = sToTime;
                                        }
                                    }
                                    else
                                    {
                                        if (Math.Abs(1440 - Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)))) < iTime)
                                        {
                                            iTime = Math.Abs(1440 - Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime))));
                                            iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[j]["OrderNO"]);
                                            sShiftDur = sDur;
                                            sFrom = sFromTime;
                                            sTo = sToTime;
                                        }
                                    }
                                    if (flag == true)
                                    {
                                        if (Math.Abs(1440 - Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)))) < iTime)
                                        {
                                            iTime = Math.Abs(1440 - Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime))));
                                            iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[j]["OrderNO"]);
                                            sShiftDur = sDur;
                                            sFrom = sFromTime;
                                            sTo = sToTime;
                                        }
                                    }
                                }
                                else
                                {
                                    if (Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime))) < iTime)
                                    {
                                        iTime = Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)));
                                        iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[j]["OrderNO"]);
                                        sShiftDur = sDur;
                                        sFrom = sFromTime;
                                        sTo = sToTime;
                                    }
                                }
                            }// else
                        } //for (int j  = 0 ;j<= DtShiftDetails.Rows.Count - 1;j++)
                        sFromTime = sFrom;
                        sToTime = sTo;
                        sDur = sShiftDur;

                    }//if (iShiftOrderNo <= 0 )
                    if (blnNightshiftFlag)
                    {
                        if (Glbln24HrFormat == false)
                        {
                            DateTime dt = Convert.ToDateTime(sFromTime);
                            sFromTime = ConvertDate12(dt);
                            dt = Convert.ToDateTime(sToTime);
                            sToTime = ConvertDate12(dt);
                        }
                        else
                        {
                            sFromTime = ConvertDate24New(sFromTime);
                            sToTime = ConvertDate24New(sToTime);
                        }
                    }
                }
                return iShiftOrderNo;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString());
                return iShiftOrderNo;
            }
        }
        private bool WorkTimeCalculation(string[] details)
        {
            try
            {

                string sAllowedBreakTime = details[10];

                int k = dgvUpdate.Rows.Count - 2;


                int iEmpId = Convert.ToInt32(details[1]);//Employee Id
                int iCmpId = Convert.ToInt32(details[2]);//'Company id

                int iShiftId = Convert.ToInt32(details[4]); //ShiftID
                DateTime atnDate = Convert.ToDateTime(details[5]);// Date
                string strAtnDate = atnDate.ToString("dd MMM yyyy");

                TxtBreakTime.Tag = "";
                TxtOverTime.Text = "";
                TxtOverTime.Tag = "";

                txtWorkTime.Tag = "";
                lblLateComing.Visible = false;
                lblLateComing.Tag = 0;
                LblEarlyGoing.Visible = false;
                LblEarlyGoing.Tag = 0;



                string sWorkTime = "0";
                string sBreakTime = "0";
                //string sTBreakTime = "0";
                string sOTBreakTime = "0";


                string sOt = "0";
                string sPreviousTime = "";
                bool bShiftChangeTime = true;
                bool bShiftChangeTimeBrkOT = true;
                bool bShiftChangeTimeEarlyWRk = true;
                bool bShiftChangeTimeEarlyBRK = true;

                int intDurationInMinutes = 0;
                int intMinWorkingHrsMin = 0;
                bool blnIsMinWrkHRsAsOT = false;

                int iShiftOrderNo = 0;
                //int iDayID = Convert.ToInt32(atnDate.DayOfWeek) + 2 > 7 ? 1 : Convert.ToInt32(atnDate.DayOfWeek) + 2;
                int iDayID = Convert.ToInt32(atnDate.DayOfWeek) + 1;
                string sFromDate = "";
                string sToDate = "";

                // ShiftDesTextBox.Text = DgvFetchData.Rows[iRowIndex].Cells["ShiftTimeDisplay"].Value;
                // int iAbsentTime = 0;
                string sFromTime = "";
                string sToTime = "";
                string sDuration = "";

                int iShiftType = 0;
                int iNoOfTimings = 0;
                string sMinWorkHours = "0";

                int iLate = 0;
                int iEarly = 0;
                bool bOffDay = true;
                string sBeforeShiftTime = "";
                string sAfterShiftTime = "";

                DataTable dtShiftInfo;
                dtShiftInfo = MobjclsBLLAttendance.GetShiftInfo(4, iEmpId, iShiftId, iDayID, sFromDate, sToDate, iCmpId);
                if (dtShiftInfo.Rows.Count > 0)
                {
                    bOffDay = false;
                    sFromTime = Convert.ToString(dtShiftInfo.Rows[0]["FromTime"]);
                    sToTime = Convert.ToString(dtShiftInfo.Rows[0]["ToTime"]);
                    sDuration = Convert.ToString(dtShiftInfo.Rows[0]["Duration"]);
                    iShiftType = Convert.ToInt32(dtShiftInfo.Rows[0]["ShiftTypeID"]);
                    iNoOfTimings = Convert.ToInt32(dtShiftInfo.Rows[0]["NoOfTimings"]);
                    sMinWorkHours = Convert.ToString(dtShiftInfo.Rows[0]["MinWorkingHours"]);
                    iLate = Convert.ToInt32(dtShiftInfo.Rows[0]["LateAfter"]);
                    iEarly = Convert.ToInt32(dtShiftInfo.Rows[0]["EarlyBefore"]);
                }
                if (bOffDay == true)
                {
                    dtShiftInfo = MobjclsBLLAttendance.GetShiftInfo(5, iEmpId, iShiftId, iDayID, sFromDate, sToDate, iCmpId);
                    if (dtShiftInfo.Rows.Count > 0)
                    {
                        sFromTime = Convert.ToString(dtShiftInfo.Rows[0]["FromTime"]);
                        sToTime = Convert.ToString(dtShiftInfo.Rows[0]["ToTime"]);
                        sDuration = Convert.ToString(dtShiftInfo.Rows[0]["Duration"]);
                        iShiftType = Convert.ToInt32(dtShiftInfo.Rows[0]["ShiftTypeID"]);
                        iNoOfTimings = Convert.ToInt32(dtShiftInfo.Rows[0]["NoOfTimings"]);
                        sMinWorkHours = Convert.ToString(dtShiftInfo.Rows[0]["MinWorkingHours"]);
                    }
                }


                dtShiftInfo = MobjclsBLLAttendance.GetShiftInfo(3, iEmpId, iShiftId, iDayID, sFromDate, sToDate, iCmpId);
                if (dtShiftInfo.Rows.Count > 0)
                {
                    sBeforeShiftTime = Convert.ToString(dtShiftInfo.Rows[0]["BeforeShiftTime"]);
                    sAfterShiftTime = Convert.ToString(dtShiftInfo.Rows[0]["AfterShiftTime"]);

                }

                if (iShiftType == 3)// Then 'for Dynamic Shift
                {
                    string dFirstTime = Convert.ToString(dgvUpdate.Rows[0].Cells["ColPunching"].Value);
                    string sFirstTime = Convert.ToDateTime(dFirstTime).ToString("HH:mm");

                    iShiftOrderNo = GetShiftOrderNo(iShiftId, sFirstTime, ref sFromTime, ref  sToTime, ref sDuration);
                    sMinWorkHours = "0";
                    sAllowedBreakTime = "0";
                    MobjclsBLLAttendance.GetDynamicShiftDuration(iShiftId, iShiftOrderNo, ref sMinWorkHours, ref sAllowedBreakTime);

                }

                if (sBeforeShiftTime != "")
                {
                    int iBeforeShiftTime = GetDurationInMinutes(sBeforeShiftTime);
                    sFromTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, (-iBeforeShiftTime), Convert.ToDateTime(sFromTime)));
                    DateTime dt = Convert.ToDateTime(sFromTime);
                    //if (Glbln24HrFormat == false)
                    //    //sFromTime = ConvertDate12(dt);
                    //else
                    //sFromTime = ConvertDate24(dt);
                }

                if (sAfterShiftTime != "")
                {
                    int iAfterShiftTime = GetDurationInMinutes(sAfterShiftTime);
                    sToTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, iAfterShiftTime, Convert.ToDateTime(sToTime)));
                    DateTime Dt2 = Convert.ToDateTime(sToTime);
                    //if (Glbln24HrFormat == false)

                    //    sToTime = ConvertDate12(Dt2);
                    //else
                    //    sToTime = ConvertDate24(Dt2);
                }



                intDurationInMinutes = GetDurationInMinutes(sDuration);//Getting Duration In mintes
                intMinWorkingHrsMin = GetDurationInMinutes(sMinWorkHours);//Getting Minworkhrs in minutes

                int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(intDurationInMinutes), Convert.ToDateTime(sToTime));
                if (tmDurationDiff < 0) //For Night Shift
                {
                    sFromTime = (Convert.ToDateTime(sFromTime)).ToString();
                    sToTime = Convert.ToString(Convert.ToDateTime(sToTime).AddDays(1));
                }

                int iAdditionalMinutes = 0;
                bool bEarly = true;
                bool bLate = true;
                if (MblnPayExists)
                {
                    iAdditionalMinutes = MobjclsBLLAttendance.GetLeaveExAddMinutes(iEmpId, Convert.ToDateTime(strAtnDate));//(iEmpId, strAtnDate);
                }
                if (iAdditionalMinutes > 0)
                {
                    sAllowedBreakTime = Convert.ToString(Convert.ToInt32(sAllowedBreakTime) + Convert.ToInt32(iAdditionalMinutes));
                    bEarly = false;
                    bLate = false;
                }
                //bool bLateComing = false;


                blnIsMinWrkHRsAsOT = MobjclsBLLAttendance.isAfterMinWrkHrsAsOT(iShiftId);



                if (iShiftType <= 3)
                {
                    for (int j = 0; j <= k; j++)
                    {
                        //if (dgvUpdate.Rows[j].Cells["ColPunching"].Value == "")
                        //{
                        //    return false;
                        //}
                        DateTime dResult = Convert.ToDateTime(dgvUpdate.Rows[j].Cells["ColPunching"].Value);
                        string sTimeValue = Convert.ToDateTime(dResult).ToString("HH:mm");




                        if (j == 0)
                        {
                            if (bLate)
                            {


                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate) && iShiftType == 1)
                                {
                                    //bLateComing = true;
                                    lblLateComing.Tag = 1;
                                    lblLateComing.Visible = true;
                                }
                                else
                                {
                                    //bLateComing = false;
                                    lblLateComing.Tag = 0;
                                    lblLateComing.Visible = false;
                                }
                            }
                        }
                        else
                        {

                            int tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue));
                            string sPrevCurrent = tmDiff.ToString();
                            if (tmDiff < 0)
                            {
                                tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1));
                                sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString();
                            }
                            if (j % 2 == 1)
                            {


                                if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly) && iShiftType == 1 && bEarly == true)
                                {
                                    LblEarlyGoing.Tag = 1;
                                    LblEarlyGoing.Visible = true;// 'Earlygoing yes'Earlygoing yes
                                }

                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                {

                                    if (bShiftChangeTime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime))
                                    {
                                        bShiftChangeTime = false;
                                        int idiffPrevShitWork = 0;
                                        if (Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sFromTime))
                                        {// idiffPrevShitWork = DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sToTime))
                                            idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sToTime));
                                        }
                                        else
                                        {
                                            //idiffPrevShitWork = DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime))
                                            idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime));
                                        }
                                        //int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime));
                                        int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                        sOt = Convert.ToString(Convert.ToDouble(sOt) + idiffCurtShiftOT);
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                    }

                                    else
                                    {
                                        sOt = Convert.ToString(Convert.ToDouble(sOt) + Convert.ToDouble(tmDiff));

                                    }
                                }
                                else
                                {

                                    bool bflag = false;

                                    if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sFromTime))
                                        bflag = true;

                                    if (bShiftChangeTimeEarlyWRk && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime))
                                    {
                                        bShiftChangeTimeEarlyWRk = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sTimeValue));
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                        bflag = true;
                                    }
                                    if (bflag == false)
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff));


                                }

                            }
                            else//brek time
                            {


                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                {
                                    if (bShiftChangeTimeBrkOT = true && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                    {
                                        bShiftChangeTimeBrkOT = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                        int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(idiffPrevShitWork));
                                        sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(idiffCurtShiftOT));
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                            sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(tmDiff));
                                    }

                                }
                                else
                                {


                                    bool bflag = false;
                                    if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sFromTime).AddMinutes(iLate))
                                    {
                                        bflag = true;
                                    }
                                    if (bShiftChangeTimeEarlyBRK && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sFromTime).AddMinutes(iLate) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate))
                                    {
                                        bShiftChangeTimeEarlyBRK = false;
                                        int idiffPrevShitBreak = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + idiffPrevShitBreak);
                                        bflag = true;
                                    }
                                    if (bflag == false)
                                    {
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(tmDiff));
                                    }

                                    LblEarlyGoing.Tag = 0;
                                    LblEarlyGoing.Visible = false;
                                }



                            }


                        }


                        sPreviousTime = sTimeValue;
                        if (Glbln24HrFormat == false)
                        {
                            sTimeValue = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                            dgvUpdate.Rows[j].Cells["ColPunching"].Value = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                        }
                        else
                        {
                            dgvUpdate.Rows[j].Cells["ColPunching"].Value = Convert.ToDateTime(dResult).ToString("HH:mm");
                        }



                    }//for
                }
                else if (iShiftType == 4)
                {
                    sWorkTime = "0";
                    sBreakTime = "0";
                    sOt = "0";
                    WorktimeCalculationForSplitShift(iShiftId, k, sFromTime, sToTime, iLate, iEarly, bLate, bEarly, "",
                                                    blnIsMinWrkHRsAsOT, ref sWorkTime, ref sBreakTime, ref sOt);

                }


                if (blnIsMinWrkHRsAsOT == true)// For OT
                {
                    if (Convert.ToDouble(sWorkTime) == Convert.ToDouble(intMinWorkingHrsMin))
                    { }//'
                    else if (Convert.ToDouble(sWorkTime) < Convert.ToDouble(intMinWorkingHrsMin))
                    { //'sOt = "0"
                        if (intDurationInMinutes > intMinWorkingHrsMin)
                        { sOt = "0"; }
                        else// 'duration and minWorkhours are same then Consider Convert.ToInt32(sAllowedBreak) + iEarly + iLate)
                        {
                            if (Convert.ToInt32(sWorkTime) + Convert.ToInt32(sAllowedBreakTime) + iEarly + iLate < intMinWorkingHrsMin)
                            {
                                sOt = "0";
                            }
                        }
                    }
                    else if (Convert.ToDouble(sWorkTime) > Convert.ToDouble(intMinWorkingHrsMin))
                    {
                        int Ot = Convert.ToInt32(sWorkTime) - Convert.ToInt32(intMinWorkingHrsMin);
                        sOt = Convert.ToString(Convert.ToInt32(sOt) + Ot);
                        sWorkTime = intMinWorkingHrsMin.ToString();
                    }
                }





                //if (intDurationInMinutes > intMinWorkingHrsMin)
                //  {
                //      iAbsentTime = intDurationInMinutes - Convert.ToInt32(sWorkTime);
                //  }
                //  else
                //  {
                //      iAbsentTime = intDurationInMinutes - (Convert.ToInt32(sWorkTime) + Convert.ToInt32(sAllowedBreakTime) + iEarly + iLate);
                //  }


                //  int iShortage = Convert.ToInt32(sBreakTime == "0" ? "0" : sBreakTime) - Convert.ToInt32(sAllowedBreakTime);
                //  if (iShortage > iAbsentTime)
                //  {
                //      iAbsentTime = iShortage;
                //  }
                //DgvFetchData.Rows[iRowIndex].Cells["ColAbsentTime"].Value = iAbsentTime > 0 ? iAbsentTime : 0;

                //int iAbsent = iAbsentTime > 0 ? iAbsentTime : 0;
                //string sShortage = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iAbsent), DateTime.Today));
                //lblShoratgeTime.Text = Convert.ToDateTime(sShortage).ToString("HH:mm:ss");











                //iAbsentTime = intDurationInMinutes - (Convert.ToInt32(sWorkTime) + Convert.ToInt32(sAllowedBreakTime) + iLate + iEarly);
                //DgvFetchData.Rows[iRowIndex].Cells["ColAbsentTime"].Value = iAbsentTime > 0 ? iAbsentTime : 0;


                txtWorkTime.Tag = Convert.ToDouble(sWorkTime);
                sWorkTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sWorkTime), DateTime.Today));
                sWorkTime = Convert.ToDateTime(sWorkTime).ToString("HH:mm:ss");
                txtWorkTime.Text = sWorkTime;




                if (sBreakTime != "" && sBreakTime != "0")
                {

                    TxtBreakTime.Tag = sBreakTime;
                    sBreakTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sBreakTime), DateTime.Today));
                    sBreakTime = Convert.ToDateTime(sBreakTime).ToString("HH:mm:ss");
                    TxtBreakTime.Text = sBreakTime;


                }
                else
                {

                    TxtBreakTime.Tag = 0;
                    TxtBreakTime.Text = "00:00:00";
                }

                if (Convert.ToString(txtWorkTime.Tag) != null && Convert.ToDouble(sOt) > 0)
                {
                    TxtOverTime.Tag = sOt;
                    sOt = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sOt), DateTime.Today));
                    sOt = Convert.ToDateTime(sOt).ToString("HH:mm:ss");
                    TxtOverTime.Text = sOt;
                }
                else
                {
                    TxtOverTime.Tag = 0;
                    TxtOverTime.Text = "00:00:00";
                }


                return true;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;

            }

        }
        private bool WorktimeCalculationForDynamicShift(ref string sWorkTime, ref string sBreakTime, int k)
        {
            try
            {

                string sPreviousTime = "";
                string CurrentDateDisplay = "";
                string FirstOne = "";
                CurrentDateDisplay = Microsoft.VisualBasic.DateAndTime.Now.Date.ToString("dd-MMM-yyyy");
                FirstOne = CurrentDateDisplay.Trim() + " " + dgvUpdate.Rows[0].Cells["ColPunching"].FormattedValue.ToString();
                string strStart = FirstOne;
                //string DayChange = "";
                //bool flag = false;
                sWorkTime = "0";
                sBreakTime = "0";
                LblEarlyGoing.Tag = 0;
                LblEarlyGoing.Visible = false;
                lblLateComing.Tag = 0;
                lblLateComing.Visible = false;


                for (int j = 0; j <= k; j++)
                {
                    //If dgvUpdate.Rows(j).Cells("ColPunching").Value = "" Then Exit Function
                    DateTime dResult = Convert.ToDateTime(dgvUpdate.Rows[j].Cells["ColPunching"].Value);
                    string sTimeValue = Convert.ToDateTime(dResult).ToString("HH:mm");

                    if (j == 0)
                    {
                    }
                    else
                    {

                        int tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue));
                        if (tmDiff < 0)
                        {
                            tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1));
                            sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString();
                        }

                        if (j % 2 == 1)
                            sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff));

                        else
                            sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(tmDiff));

                    }



                    sPreviousTime = sTimeValue;
                    if (Glbln24HrFormat == false)
                    {
                        sTimeValue = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                    }
                    dgvUpdate.Rows[j].Cells["ColPunching"].Value = sTimeValue;
                }//next

                return true;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }


        private bool WorktimeCalculationForSplitShift(int iShiftID, int k, string sFromTime, string sToTime,
                                                 int iLate, int iEarly, bool bLate, bool bEarly, string sEmpName,
                                                 bool isAfterMinWrkHrsAsOT, ref string sWorkTime, ref string sBreakTime, ref string sOt)
        {//' Modified On 21 jul 2011'Worktime Calculation for Split shift  ByVal sFromTime As String, ByVal sToTime As String,
            try
            {
                TxtBreakTime.Tag = "";
                TxtOverTime.Text = "";
                TxtOverTime.Tag = "";

                txtWorkTime.Tag = "";
                lblLateComing.Visible = false;
                lblLateComing.Tag = 0;
                LblEarlyGoing.Visible = false;
                LblEarlyGoing.Tag = 0;

                sWorkTime = "0";
                sBreakTime = "0";
                sOt = "0";
                string sOvertime = "0";

                bool nightFlag = false;
                //Dim sTsql As String = "Select [OrderNO],[FromTime],[ToTime],[Duration] from ShiftDetails where ShiftID=" & iShiftID & " order by OrderNO"
                DataTable DtShiftDetails = MobjclsBLLAttendance.GetDynamicShiftDetails(iShiftID);
                if (DtShiftDetails.Rows.Count > 0)
                {
                    for (int i = 0; i <= DtShiftDetails.Rows.Count - 1; i++)
                    {
                        string sSplitFromTime = Convert.ToString(DtShiftDetails.Rows[i]["FromTime"]);
                        string sSplitToTime = Convert.ToString(DtShiftDetails.Rows[i]["ToTime"]);
                        string sDuration = Convert.ToString(DtShiftDetails.Rows[i]["Duration"]);
                        int iMinWorkingHrsMin = GetDurationInMinutes(sDuration);

                        if (nightFlag == true)
                        {
                            sSplitFromTime = Convert.ToDateTime(sSplitFromTime).AddDays(1).ToString();
                            sSplitToTime = Convert.ToDateTime(sSplitToTime).AddDays(1).ToString();
                        }

                        int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime).AddMinutes(iMinWorkingHrsMin), Convert.ToDateTime(sSplitToTime));
                        if (tmDurationDiff < 0)
                        {
                            nightFlag = true;
                            sSplitFromTime = Convert.ToDateTime(sSplitFromTime).ToString();
                            sSplitToTime = Convert.ToDateTime(sSplitToTime).AddDays(1).ToString();
                        }

                        string sPreviousTime = "";
                        bool bShiftChangeTime = true;
                        bool bShiftChangeOvertime = true;

                        bool bShiftChangeTimeBrkOT = true;
                        bool bShiftChangeTimeEarlyWRk = true;
                        bool bShiftChangeTimeEarlyBRK = true;
                        string sOTBreakTime = "0";


                        for (int j = 0; j <= k; j++)//'Time selecting filter wit date and id 
                        {
                            if (Convert.ToString(dgvUpdate.Rows[j].Cells["ColPunching"].Value) == "")
                            { return true; }
                            DateTime dResult = Convert.ToDateTime(dgvUpdate.Rows[j].Cells["ColPunching"].Value);
                            string sTimeValue = Convert.ToDateTime(dResult).ToString("HH:mm");

                            //'Modified for Dynamic Shift
                            //'If nightFlag = True Then
                            //'    If Convert.ToDateTime(sTimeValue) >= Convert.ToDateTime("12:00 AM") And _
                            //'            Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime("12:00 PM") Then
                            //'        sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString("dd MMM yyyy HH:mm tt")
                            //'    End If
                            //'End If



                            if (j == 0)//Then ''first time
                            {  //'----------------------------------'LateComing checking
                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate))//Then 'bFlexi = False
                                {
                                    lblLateComing.Tag = 1;
                                    lblLateComing.Visible = true;
                                }
                                else
                                {
                                    lblLateComing.Tag = 0;
                                    lblLateComing.Visible = false;
                                }
                                //'----------------------------------'LateComing checking
                            }
                            else
                            {
                                int tmDiff = SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue));
                                string sPrevCurrent = tmDiff.ToString();

                                if (tmDiff < 0)// 'if date diff less than Zero then adding day to the StimeValue
                                {
                                    tmDiff = SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1));
                                    sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString();
                                }


                                if (j % 2 == 1)//Then 'in here Mode 1=WorkTime 'and Mode=0 for BreakTime
                                {// ''''''''''''''''''''''''''''''''''''''''''''''
                                    //  '-------------------------------------Early Going validating 
                                    if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly))//Then 'bFlexi = False
                                    {
                                        LblEarlyGoing.Tag = 1;
                                        LblEarlyGoing.Visible = true; // 'Earlygoing yes
                                    }// '-------------------------------------Early Going 

                                    //'---------------------------------------------Split Over Time-------------------------------------------
                                    if (i == DtShiftDetails.Rows.Count - 1)
                                    {
                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                        {
                                            if (bShiftChangeOvertime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime))
                                            {
                                                bShiftChangeOvertime = false;
                                                int idiffCurtShiftOT = SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                                if (i == DtShiftDetails.Rows.Count - 1)
                                                { sOvertime = Convert.ToString(Convert.ToDouble(sOvertime) + idiffCurtShiftOT); }

                                            }
                                            else
                                            {
                                                if (i == DtShiftDetails.Rows.Count - 1)
                                                { sOvertime = Convert.ToString(Convert.ToDouble(sOvertime) + Convert.ToDouble(tmDiff)); } //'WorkTime Calculation 
                                            }
                                        }
                                    }//'--------
                                    if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime))
                                    {//' DgvAttendanceFetch.Rows(gCount).Cells("EarlyGoing").Value = 0
                                        if (bShiftChangeTime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitToTime))
                                        {
                                            bShiftChangeTime = false;
                                            // 'Previous Time less than From time ,Previous Time Replaced as SfromTime---------------------
                                            int idiffPrevShitWork = 0;
                                            if (Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime))
                                            {
                                                idiffPrevShitWork = SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sSplitToTime));
                                            }
                                            else
                                            {
                                                idiffPrevShitWork = SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sSplitToTime));
                                            }
                                            //'-------------------------------------------------------------------------------------------------
                                            //'Dim idiffPrevShitWork As Integer = DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime))
                                            int idiffCurtShiftOT = SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitToTime), Convert.ToDateTime(sTimeValue));
                                            if (i == DtShiftDetails.Rows.Count - 1)
                                            { sOt = Convert.ToString(Convert.ToDouble(sOt) + idiffCurtShiftOT); }

                                            sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                        }
                                        else
                                        {
                                            if (i == DtShiftDetails.Rows.Count - 1)
                                            { sOt = Convert.ToString(Convert.ToDouble(sOt) + Convert.ToDouble(tmDiff)); } //'WorkTime Calculation 
                                        }
                                    }
                                    else
                                    {
                                        //'Modification -----------------------------12-11-2010
                                        bool bflag = false;
                                        if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sSplitFromTime))
                                        {
                                            bflag = true;
                                        }// 'sWorkTime = Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff)
                                        if (bShiftChangeTimeEarlyWRk && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitFromTime))
                                        {
                                            bShiftChangeTimeEarlyWRk = false;
                                            int idiffPrevShitWork = SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sTimeValue));
                                            sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                            bflag = true;
                                        }
                                        if (bflag == false)
                                        {
                                            sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff));
                                        }

                                        // '-----------------------------------------------------Break Time Calculating-----------------------------------------------------------------------------
                                    }
                                }
                                else //'Mode=0 for BreakTime
                                {  //'''''''''''''''''''''''''''''''''''''''''''''
                                    if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime))
                                    {
                                        if (bShiftChangeTimeBrkOT = true && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitToTime))
                                        {
                                            bShiftChangeTimeBrkOT = false;
                                            int idiffPrevShitWork = SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sSplitToTime));
                                            int idiffCurtShiftOT = SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitToTime), Convert.ToDateTime(sTimeValue));
                                            sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(idiffPrevShitWork));//'Break Time Calculation
                                            sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(idiffCurtShiftOT));

                                        }
                                        else
                                        {
                                            if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime))
                                                sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(tmDiff)); //'Break Time Calculation
                                        }
                                    }
                                    else
                                    {
                                        //'Modification -----------------------------12-11-2010
                                        bool bflag = false;

                                        if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sSplitFromTime))
                                        {
                                            bflag = true;
                                        } //'sWorkTime = Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff)
                                        if (bShiftChangeTimeEarlyBRK && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitFromTime))
                                        {
                                            bShiftChangeTimeEarlyBRK = false;
                                            int idiffPrevShitBreak = SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sTimeValue));
                                            sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + idiffPrevShitBreak);
                                            bflag = true;
                                        }
                                        if (bflag == false)
                                        {
                                            sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(tmDiff));
                                        }
                                        //'Modification ------------------------------End 12-11-2010
                                        //'-------------------------------------Early Going validating
                                        LblEarlyGoing.Tag = 0;
                                        LblEarlyGoing.Visible = false; //'Earlygoing No
                                        // '-------------------------------------Early Going 
                                    }
                                    //'''''''''''''''''''''''''''''''


                                }
                            } // if j=0
                            sPreviousTime = sTimeValue;
                            if (Glbln24HrFormat == false)
                            {
                                sTimeValue = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                            }
                            dgvUpdate.Rows[j].Cells["ColPunching"].Value = sTimeValue;
                        } //' for j row Count
                    }//'For i As Integer = 0 To DtShiftDetails.Rows.Count - 1
                } //'  If DtShiftDetails.Rows.Count > 0

                return true;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }



        private int GetSplitShiftTotalDuration(int ishiftID)
        {
            //'in split Shift Total Duration is Sum of  details shift duration
            int intTotalSplitShiftDurInMinutes = 0;
            try
            {

                DataTable DtShiftDetails = MobjclsBLLAttendance.GetDynamicShiftDetails(ishiftID);
                if (DtShiftDetails.Rows.Count > 0)
                {
                    for (int i = 0; i <= DtShiftDetails.Rows.Count - 1; i++)
                    {
                        string strDuration = Convert.ToString(DtShiftDetails.Rows[i]["Duration"]);
                        int intDurInMin = GetDurationInMinutes(strDuration);
                        intTotalSplitShiftDurInMinutes += intDurInMin;
                    }
                }
                return intTotalSplitShiftDurInMinutes;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return 0;
            }
        }


        private int GetDurationInMinutes(string sDuration)// 'Getting Duration In minutes
        {
            int iDurationInMinutes = 0;
            try
            {
                string[] sHourMinute = new string[2];

                if (sDuration.Contains('.'))
                {
                    sHourMinute = sDuration.Split('.');
                    iDurationInMinutes = Convert.ToInt32(sHourMinute[0]) * 60 + Convert.ToInt32(sHourMinute[1]);

                }
                else if (sDuration.Contains(':'))
                {
                    sHourMinute = sDuration.Split(':');
                    iDurationInMinutes = Convert.ToInt32(sHourMinute[0]) * 60 + Convert.ToInt32(sHourMinute[1]);
                }
                else if (sDuration != "")
                {
                    iDurationInMinutes = Convert.ToInt32(sDuration) * 60;
                }
                return iDurationInMinutes;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return iDurationInMinutes;
            }
        }

        private void dgvUpdate_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                return;
            }
            catch
            {
                return;
            }
        }

        private void dgvUpdate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {


                if (e.KeyCode == Keys.Tab)
                {
                    SendKeys.Send("Enter");

                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (dgvUpdate.Rows.Count < 0)
                        return;

                    if (dgvUpdate.CurrentRow.IsNewRow)
                        return;

                    dgvUpdate.Rows.RemoveAt(dgvUpdate.CurrentRow.Index);

                    for (int i = 0; i <= dgvUpdate.Rows.Count - 2; i++)
                    {
                        dgvUpdate.Rows[i].Cells["ColEntryExit"].Value = "";
                        if (i % 2 == 0)
                            dgvUpdate.Rows[i].Cells["ColEntryExit"].Value = "Entry";
                        else
                            dgvUpdate.Rows[i].Cells["ColEntryExit"].Value = "Exit";

                        dgvUpdate.Rows[i].Cells["ColSlNo"].Value = i + 1;
                    }
                    if (TimeDiffCalculation())
                    {
                        WorkTimeCalculation(pDetailData);
                        TimeScheduleChart();
                    }
                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {

            try
            {


                bool bUpadteData = true;

                if (dgvUpdate.RowCount >= 0)
                {
                    if (Convert.ToString(dgvUpdate.Rows[0].Cells[0].Value).Trim() == "" || Convert.ToString(dgvUpdate.Rows[0].Cells[1].Value).Trim() == "" ||
                        Convert.ToString(dgvUpdate.Rows[0].Cells[2].Value).Trim() == "")
                    {
                        MessageBox.Show(UserMessage.GetMessageByCode(5600), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        TLSPStatusLblDisplaying.Text = UserMessage.GetMessageByCode(5600);
                        tmrClearlabels.Enabled = true;
                        bUpadteData = false;
                        return;
                    }
                    int iRowcount = dgvUpdate.Rows.Count - 1;
                    if (iRowcount % 2 == 1)
                    {
                        MessageBox.Show(UserMessage.GetMessageByCode(5600), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        TLSPStatusLblDisplaying.Text = UserMessage.GetMessageByCode(5600);
                        tmrClearlabels.Enabled = true;
                        bUpadteData = false;
                        return;
                    }


                }

                if (dgvUpdate.RowCount > 2)
                {
                    int iRowcount = dgvUpdate.Rows.Count - 1;
                    if (iRowcount % 2 == 0)
                    {
                        //if (iMaxColumCount >= dgvUpdate.Rows.Count - 2)
                        //{
                        for (int iRow = 0; iRow <= dgvUpdate.Rows.Count - 2; iRow++)
                        {
                            if (Convert.ToString(dgvUpdate.Rows[iRow].Cells["ColPunching"].Value) == "")
                            {
                                //MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 5602, out MmessageIcon); //
                                ;
                                //MessageBox.Show(New ClsNotification().GetErrorMessage(MaMessageArr, 1582, True), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                MessageBox.Show(UserMessage.GetMessageByCode(5602), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                TLSPStatusLblDisplaying.Text = UserMessage.GetMessageByCode(5602);
                                tmrClearlabels.Enabled = true;
                                bUpadteData = false;
                                return;
                            }
                            punchingCollection.Add(dgvUpdate.Rows[iRow].Cells["ColPunching"].Value);
                        }
                        //punchingCollection = punchingCollection;

                        if (CellValueChange() == true && bUpadteData == true)
                        {
                            //MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 5603, out MmessageIcon);//

                            MessageBox.Show(UserMessage.GetMessageByCode(5603), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            TLSPStatusLblDisplaying.Text = UserMessage.GetMessageByCode(5603);
                            tmrClearlabels.Enabled = true;

                        }
                        if (punchingCollection.Count != iMaxColumCount)
                        {
                            PblnChange = true;
                        }

                        PblnOkbutton = true;
                        this.Close();


                        //}
                        //else
                        //{

                        //    MstrMessCommon = "Punching Data Should not be greater than Fetch Punching Data,Max Punching Column: ";
                        //    // MessageBox.Show(New ClsNotification().GetErrorMessage(MaMessageArr, 1585, True) + Convert.ToString(iMaxColumCount), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        //    MessageBox.Show(MstrMessCommon + Convert.ToString(iMaxColumCount), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //    bUpadteData = false;
                        //    return;
                        //}


                    }
                }
                bUpadteData = true;
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }


        }
        private bool CellValueChange()  //Handles dgvUpdate.CellValueChanged, dgvUpdate.CellEndEdit
        {
            bool CellValueChange = true;
            if (CellValueChange)
                return true;
            else
                return false;

        }
        private void dgvUpdate_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {

            try
            {
                if (e.ColumnIndex == ColPunching.Index && e.RowIndex > -1)
                {
                    if (Convert.ToString(dgvUpdate.CurrentCell.Value).Trim() != "")
                    {
                        MstrCellTime = Convert.ToString(dgvUpdate.CurrentCell.Value).Trim();
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }

        }
        private void dgvUpdate_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            CellValueChange();

            try
            {
                dgvUpdate.CommitEdit(DataGridViewDataErrorContexts.Commit);
                if (TimeDiffCalculation())
                {
                    WorkTimeCalculation(pDetailData);
                    TimeScheduleChart();
                }
                if (e.ColumnIndex == ColPunching.Index && e.RowIndex > -1)
                {
                    if (Convert.ToString(dgvUpdate.CurrentCell.Value).Trim() != MstrCellTime)
                    {
                        PblnChange = true;
                    }
                }


            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }






        }

        private void dgvUpdate_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            CellValueChange();
            try
            {
                if (dgvUpdate.Rows.Count > 0)
                {
                    dgvUpdate.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }

            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }
        }

        private void InsertARecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvUpdate.CurrentRow.IsNewRow)
                {
                    if (dgvUpdate.RowCount == 1)
                        dgvUpdate.Rows.Add();
                    else
                        return;

                }
                else
                {

                    bool iRowInsert = true;
                    for (int iRows = 0; iRows <= dgvUpdate.Rows.Count - 2; iRows++)
                    {
                        if (Convert.ToString(dgvUpdate.Rows[iRows].Cells["ColPunching"].Value) == "")
                            iRowInsert = false;

                    }
                    if (iRowInsert)
                        dgvUpdate.Rows.Insert(dgvUpdate.CurrentRow.Index + 1);

                }

                for (int i = 0; i <= dgvUpdate.Rows.Count - 2; i++)
                {
                    if (i % 2 == 0)
                    {
                        dgvUpdate.Rows[i].Cells["ColEntryExit"].Value = "Entry";
                    }
                    else
                    {
                        dgvUpdate.Rows[i].Cells["ColEntryExit"].Value = "Exit";
                    }
                    dgvUpdate.Rows[i].Cells["ColSlNo"].Value = i + 1;
                }
            }

            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }
        }

        private void RemoveARecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //'Remove
            try
            {

                if (dgvUpdate.Rows.Count < 0)
                    return;
                if (dgvUpdate.CurrentRow.IsNewRow)
                    return;

                dgvUpdate.Rows.RemoveAt(dgvUpdate.CurrentRow.Index);

                for (int i = 0; i <= dgvUpdate.Rows.Count - 2; i++)
                {
                    dgvUpdate.Rows[i].Cells["ColEntryExit"].Value = "";
                    if (i % 2 == 0)

                        dgvUpdate.Rows[i].Cells["ColEntryExit"].Value = "Entry";
                    else
                        dgvUpdate.Rows[i].Cells["ColEntryExit"].Value = "Exit";

                    dgvUpdate.Rows[i].Cells["ColSlNo"].Value = i + 1;
                }
                if (TimeDiffCalculation())
                {
                    WorkTimeCalculation(pDetailData);
                    TimeScheduleChart();
                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }
        }


        private bool IsValidTime(string sTime)
        {
            try
            {
                //'Sample  Format  "13:14"
                //DateTime TheDate ;
                //TheDate = sTime;
                string Format1 = "^*(1[0-2]|[1-9]):[0-5][0-9]*(a|p|A|P)(m|M)*$";
                System.Text.RegularExpressions.Regex TryValidateFormat1 = new System.Text.RegularExpressions.Regex(Format1);
                string Format2 = "([0-1][0-9]|2[0-3]):([0-5][0-9])";
                System.Text.RegularExpressions.Regex TryValidateFormat2 = new System.Text.RegularExpressions.Regex(Format2);
                return TryValidateFormat2.IsMatch(sTime) || TryValidateFormat1.IsMatch(sTime);
            }
            catch (Exception)
            {

                return false;
            }
        }
        private string FormatString(string strTime)
        {
            if (strTime.ToStringCustom() == string.Empty)
                return "00:00";

            string strTT = "";
            if (strTime.ToLower().Contains('a'))
                strTT = "AM";
            else if (strTime.ToLower().Contains('p'))
                strTT = "PM";

            strTime = strTime.ToLower();
            strTime = strTime.Replace(":", ".");
            strTime = strTime.Replace("a", "");
            strTime = strTime.Replace("p", "");
            strTime = strTime.Replace("m", "");

            string strOutput = "00:00";
            decimal decOutput = 0M;

            if (Decimal.TryParse(strTime, out decOutput))
            {
                strOutput = String.Format("{0:N2}", decOutput).Replace(".", ":").Replace(",", "");

                if (decOutput > 0)
                {
                    string[] arr = strOutput.Split(':');
                    if (arr.Length >= 2)
                    {
                        int intLeft = Convert.ToInt32(arr[0]);
                        int intRight = Convert.ToInt32(arr[1]);
                        intLeft = intLeft > 23 ? 0 : intLeft;
                        intRight = intRight > 59 ? 0 : intRight;
                        if (intRight < 10)
                        {
                            string strRight = "0" + intRight.ToString();
                            strOutput = String.Format("{0:N2}", Convert.ToDecimal(intLeft.ToString() + "." + strRight)).Replace(".", ":").Replace(",", "");

                        }
                        else
                        {
                            strOutput = String.Format("{0:N2}", Convert.ToDecimal(intLeft.ToString() + "." + intRight.ToString())).Replace(".", ":").Replace(",", "");

                        }

                    }
                }
            }
            strOutput = strOutput + " " + strTT;
            strOutput = strOutput.TrimEnd().TrimStart();
            return strOutput;

        }

        private string AMPMConvert(string StrValue)
        {

            try
            {
                StrValue = FormatString(StrValue);

                if (StrValue.IndexOf(".") != -1)
                    StrValue = StrValue.Replace('.', ':');

                if (Microsoft.VisualBasic.Information.IsNumeric(StrValue))
                {
                    if ((StrValue).Length == 1)
                    {
                        if (Convert.ToInt32(StrValue) < 12)
                            StrValue = StrValue + "A";
                        else
                            StrValue = StrValue + "P";

                    }
                    else if (StrValue.Length == 2)
                    {
                        if (Convert.ToInt32(StrValue) < 12)
                            StrValue = StrValue + "A";
                        else
                            StrValue = StrValue + "P";

                    }
                }

                if (StrValue.IndexOf(":") != -1)
                {
                    string LeftTime = "";
                    string RightTime = "";
                    string[] strar = new string[2];

                    strar = StrValue.Split(':');
                    LeftTime = strar[0];
                    RightTime = strar[1];
                    if (Microsoft.VisualBasic.Information.IsNumeric(RightTime))
                    {
                        if (RightTime.Length > 2)
                            return Microsoft.VisualBasic.Strings.Left(StrValue, 2);

                    }
                }


                DateTime CForDate;
                if (StrValue.IndexOf("AM") != -1)
                {
                    CForDate = Convert.ToDateTime(StrValue.Replace("AM", "AM"));
                    if (Glbln24HrFormat == false)
                        return CForDate.ToString("hh:mm tt").Trim();
                    else
                        return CForDate.ToString("HH:mm").Trim();

                    // return AMPMConvert("");
                }
                else if (StrValue.IndexOf("A") != -1)
                {
                    CForDate = Convert.ToDateTime(StrValue.Replace("A", "AM"));
                    if (Glbln24HrFormat == false)
                        return CForDate.ToString("hh:mm tt").Trim();
                    else
                        return CForDate.ToString("HH:mm").Trim();

                    //Return AMPMConvert
                }
                else if (StrValue.IndexOf("PM") != -1)
                {
                    CForDate = Convert.ToDateTime(StrValue.Replace("PM", "PM"));
                    if (Glbln24HrFormat == false)
                        return CForDate.ToString("hh:mm tt").Trim();
                    else
                        return CForDate.ToString("HH:mm").Trim();

                    //Return AMPMConvert
                }
                else if (StrValue.IndexOf("P") != -1)
                {
                    CForDate = Convert.ToDateTime(StrValue.Replace("P", "PM"));
                    if (Glbln24HrFormat == false)
                        return CForDate.ToString("hh:mm tt").Trim();
                    else
                        return CForDate.ToString("HH:mm").Trim();

                    //Return AMPMConvert
                }
                else
                {
                    if (StrValue.Length > 3)
                        return Microsoft.VisualBasic.Strings.Left(StrValue, 2);

                    return StrValue;
                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return StrValue;
            }

        }

        private string Convert24HrFormat(string StrValue)
        {
            try
            {
                StrValue = FormatString(StrValue);
                if (StrValue.IndexOf(".") != -1)
                    StrValue = StrValue.Replace(".", ":");

                string ReturnTime = "";
                if (StrValue.IndexOf(":") != -1)
                {
                    string LeftTime = "";
                    string RightTime = "";
                    string[] strar = new string[2];
                    strar = StrValue.Split(':');
                    LeftTime = strar[0];
                    RightTime = strar[1];
                    if (Microsoft.VisualBasic.Information.IsNumeric(RightTime))
                    {
                        if ((RightTime.Length) > 2)
                            return Microsoft.VisualBasic.Strings.Left(StrValue, 2);

                    }
                }
                if (StrValue.IndexOf(":") != -1)
                {
                    string LeftTime = "";
                    string RightTime = "";
                    string Time1 = "";
                    string time2 = "";
                    string[] strar = new string[2];
                    strar = StrValue.Split(':');
                    LeftTime = strar[0];
                    RightTime = strar[1];
                    if (Microsoft.VisualBasic.Information.IsNumeric(LeftTime))
                    {
                        if (Convert.ToInt32(LeftTime) > 23)
                            return StrValue;
                        else
                        {
                            if (LeftTime.Length == 2)
                                Time1 = LeftTime;
                            else if (LeftTime.Length == 1)
                                Time1 = "0" + LeftTime;

                        }
                    }
                    else
                        return "10p";

                    if (Microsoft.VisualBasic.Information.IsNumeric(RightTime))
                        if (Convert.ToInt32(RightTime) > 59)
                            return StrValue;
                        else
                        {
                            if (RightTime.Length == 2)
                                time2 = RightTime;
                            else if (RightTime.Length == 1)
                                time2 = "0" + RightTime;

                        }
                    else
                        return "10p";

                    ReturnTime = Time1 + ":" + time2;
                    return ReturnTime;
                }
                else
                {

                    if (Microsoft.VisualBasic.Information.IsNumeric(StrValue))
                    {
                        if (Convert.ToInt32(StrValue) > 23)
                            return StrValue;
                        else
                        {
                            if (StrValue.Length == 2)
                            {
                                ReturnTime = StrValue + ":" + "00";
                                return ReturnTime;
                            }
                            else if (StrValue.Length == 1)
                            {
                                ReturnTime = "0" + StrValue + ":" + "00";
                                return ReturnTime;
                            }
                            else
                                return StrValue;

                        }
                    }
                    else
                    {
                        return StrValue;
                    }




                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return StrValue;
            }
        }
        private string ConvertDate24(DateTime dateString)
        {
            try
            {
                DTPFormatTime.CustomFormat = "HH:mm";
                DTPFormatTime.Text = dateString.ToString();
                return DTPFormatTime.Text;
            }
            catch (Exception)
            {
                return " ";
            }
        }
        private string ConvertDate24New(string dateString)
        {
            try
            {
                DTPFormatTime.CustomFormat = "HH:mm";
                DTPFormatTime.Text = dateString;
                return DTPFormatTime.Text.ToString();
                //Return FromTimeDateTimePicker.Text
                //Return Nothing
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
                return DTPFormatTime.Text.ToString();
            }

        }
        private string ConvertDate12(DateTime dateString)
        {
            try
            {
                DTPFormatTime.CustomFormat = "hh:mm tt";
                DTPFormatTime.Text = dateString.ToString();
                return DTPFormatTime.Text;
            }
            catch (Exception)
            {
                return " ";
            }
        }

        private void dgvUpdate_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (dgvUpdate.Columns[e.ColumnIndex].HeaderText == "Punching")
                {
                    if (e.Value == null)
                        return;
                    if (String.IsNullOrEmpty(e.Value.ToString()))
                        return;

                    DateTime dtThedate;
                    System.Text.StringBuilder sdateString = new System.Text.StringBuilder();

                    if (!DateTime.TryParse(e.Value.ToString(), out dtThedate))//&& dtThedate==#12:00:00 AM#
                    {

                    }
                    else
                    {
                        if (Glbln24HrFormat != false)
                        {
                            e.Value = dtThedate.ToString("HH:mm");
                            e.FormattingApplied = true;
                        }
                        else
                        {
                            e.Value = dtThedate.ToString("hh:mm tt");
                            e.FormattingApplied = true;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }
        }

        private void dgvUpdate_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvUpdate.IsCurrentCellDirty)
                {
                    if (dgvUpdate.CurrentCell != null)
                    {
                        dgvUpdate.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    }
                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }
        }

        private void dgvUpdate_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            dgvUpdate.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void PicBoxWorkTime_Click(object sender, EventArgs e)
        {
            if (ColDigBox.ShowDialog() == DialogResult.OK)
            {
                PicBoxWorkTime.BackColor = ColDigBox.Color;
            }
            else
            {
                PicBoxWorkTime.BackColor = Color.Green;
            }
            TimeScheduleChart();
        }

        private void PicBoxBreakTime_Click(object sender, EventArgs e)
        {
            if (ColDigBox.ShowDialog() == DialogResult.OK)
            {
                PicBoxBreakTime.BackColor = ColDigBox.Color;
            }
            else
            {
                PicBoxBreakTime.BackColor = Color.Red;
            }
            TimeScheduleChart();
        }

        private void DgvTimeScheuleGraph_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int icurtCell = DgvTimeScheuleGraph.CurrentCell.ColumnIndex + 1;
                dgvUpdate.CurrentCell = dgvUpdate[2, icurtCell];//'e.RowIndex
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }
        }

        private void dgvUpdate_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {

            dgvUpdate.CommitEdit(DataGridViewDataErrorContexts.Commit);

            try
            {


                string date2 = "";
                DateTime ConvertTime;
                // if blnDelete = True Then Exit Sub

                // DgvFetchData.CommitEdit();
                dgvUpdate.CommitEdit(DataGridViewDataErrorContexts.Commit);


                if (e.RowIndex != -1)
                {
                    if (e.ColumnIndex == 2)
                    {
                        if (Convert.ToString(dgvUpdate.Rows[e.RowIndex].Cells[e.ColumnIndex].Value) == "" || dgvUpdate.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null)
                            return;
                        else
                        {

                            if (Glbln24HrFormat == false)
                            {
                                dgvUpdate.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = AMPMConvert(Microsoft.VisualBasic.Strings.UCase(dgvUpdate.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()));

                            }
                            else
                            {
                                dgvUpdate.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = Convert24HrFormat(Microsoft.VisualBasic.Strings.UCase(dgvUpdate.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()));


                            }
                            date2 = dgvUpdate.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                        }
                        if (IsValidTime(dgvUpdate.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()) == false)
                        {
                            if (Glbln24HrFormat == false)
                            {
                                //message
                                MstrMessCommon = UserMessage.GetMessageByCode(5604);
                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                TLSPStatusLblDisplaying.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                tmrClearlabels.Enabled = true;
                            }
                            else
                            {
                                //message
                                MstrMessCommon = UserMessage.GetMessageByCode(5604);
                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                TLSPStatusLblDisplaying.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                tmrClearlabels.Enabled = true;

                            }
                            dgvUpdate.CurrentCell = dgvUpdate[e.ColumnIndex, e.RowIndex];
                            e.Cancel = true;
                            return;
                        }
                        else
                        {
                            ConvertTime = Convert.ToDateTime(date2);
                        }

                        if (IsValidTime(dgvUpdate.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()) == true)
                        {
                            string STimeIn = dgvUpdate.CurrentRow.Cells[e.ColumnIndex].Value.ToString();
                            if (Glbln24HrFormat == false)
                            {
                                STimeIn = ConvertDate24New(STimeIn);

                            }
                        }


                        string CurrentDateDisplay = "";
                        string FirstOne = "";
                        //string LastOne = "";
                        //string PreviousOne = "";
                        //string ComVal = "";


                        if (e.ColumnIndex == 2)
                        {
                            //'CurrentDateDisplay = DTPDate.Value.ToString("dd-MMM-yyyy")
                            CurrentDateDisplay = Microsoft.VisualBasic.DateAndTime.Now.Date.ToString("dd MMM yyyy");
                            if (dgvUpdate.Rows[e.RowIndex].Cells[2].Value != null)
                            {
                                if (Convert.ToString(dgvUpdate.Rows[e.RowIndex].Cells[2].Value) != "")
                                {
                                    FirstOne = CurrentDateDisplay.Trim() + " " + dgvUpdate.Rows[0].Cells[2].FormattedValue.ToString();
                                }
                            }


                            string strStart = FirstOne;
                            string strLast = Convert.ToDateTime(strStart).AddDays(1).ToString();
                            string DayChange = "";
                            bool flag = false;
                            string strCurrent = "";
                            string sPreviousTime = "";
                            int iDiff = 0;

                            for (int k = 0; k <= dgvUpdate.RowCount - 1; k++)
                            {
                                if (Convert.ToString(dgvUpdate.Rows[k].Cells[2].Value) == "")
                                {
                                    break;
                                }
                                if (k > 0)
                                {
                                    if (flag == true)
                                    {
                                        strCurrent = DayChange + " " + dgvUpdate.Rows[e.RowIndex].Cells[2].FormattedValue;
                                        iDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(strCurrent));
                                        if (iDiff < 0)
                                        {
                                            strCurrent = Convert.ToDateTime(strCurrent).AddDays(1).ToString();
                                            DayChange = Strings.Format(Convert.ToDateTime(
                                                strCurrent), "dd MMM yyyy");
                                            if (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(strStart), Convert.ToDateTime(strCurrent)) >= 1440)
                                            {
                                                //MbCorrectTime = false;
                                                MstrMessCommon = UserMessage.GetMessageByCode(5605);//objNotification.GetErrorMessage(MaMessageArr, 1562)
                                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                TLSPStatusLblDisplaying.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                                tmrClearlabels.Enabled = true;
                                                e.Cancel = true;
                                                return;
                                            }
                                        }
                                        if (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(strStart), Convert.ToDateTime(strCurrent)) >= 1440)
                                        {
                                            //MbCorrectTime = false;
                                            MstrMessCommon = UserMessage.GetMessageByCode(5605); //objNotification.GetErrorMessage(MaMessageArr, 1562)
                                            MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            TLSPStatusLblDisplaying.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();

                                            tmrClearlabels.Enabled = true;
                                            e.Cancel = true;
                                            //'AttendanceDataGridView.CurrentCell = AttendanceDataGridView(k, e.RowIndex)


                                            return;
                                        }
                                    }
                                    else
                                    {

                                        strCurrent = Convert.ToString(dgvUpdate.Rows[k].Cells[2].FormattedValue);
                                        iDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(strCurrent));
                                        if (iDiff < 0)
                                        {
                                            strCurrent = Convert.ToDateTime(strCurrent).AddDays(1).ToString();
                                            DayChange = Strings.Format(Convert.ToDateTime(strCurrent), "dd MMM yyyy");
                                            flag = true;
                                            if (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(strStart), Convert.ToDateTime(strCurrent)) >= 1440)
                                            {

                                                // MbCorrectTime = false;
                                                MstrMessCommon = UserMessage.GetMessageByCode(5605);//objNotification.GetErrorMessage(MaMessageArr, 1562)
                                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                TLSPStatusLblDisplaying.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                                tmrClearlabels.Enabled = true;

                                                e.Cancel = true;
                                                return;
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    strCurrent = Convert.ToString(dgvUpdate.Rows[0].Cells[2].Value);
                                }
                                sPreviousTime = strCurrent;
                            }

                        }//(e.ColumnIndex >= 3 )

                    }

                }//rowindex =-1

            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }

        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvUpdate_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (dgvUpdate.CurrentCell.ColumnIndex == 2)
                {
                    if (dgvUpdate.CurrentCell is DataGridViewTextBoxCell)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtInt_KeyPress);

                    }
                }
            }
            catch (Exception Ex)
            {
                string sMethod = new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name;
                MobjClsLogs.WriteLog("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }

        }
        void txtInt_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " bcdefghijklnoqrstuvwxyz~!@#$%^&*()_+|}{?></,`-=\\[]";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
                return;
            }
            string strValidChars = " amp.:";
            if ((strValidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 && ((TextBox)sender).Text.ToLower().Contains(e.KeyChar.ToString())))
            {
                e.Handled = true;
                return;
            }
            if ((strValidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 && ((TextBox)sender).Text.ToLower().Equals("") && ((TextBox)sender).SelectedText.Equals("")))
            {
                e.Handled = true;
                return;
            }
            if ((strValidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 && ((TextBox)sender).Text.Length == ((TextBox)sender).SelectedText.Length))
            {
                e.Handled = true;
                return;
            }
            if ((e.KeyChar.ToString().ToLower() == "p" && ((TextBox)sender).Text.ToLower().Contains('a')))
            {
                e.Handled = true;
                return;
            }
            if ((e.KeyChar.ToString().ToLower() == "a" && ((TextBox)sender).Text.ToLower().Contains('p')))
            {
                e.Handled = true;
                return;
            }



        }

        private void tmrClearlabels_Tick(object sender, EventArgs e)
        {
            tmrClearlabels.Enabled = false;
            TLSPStatusLblDisplaying.Text = "";
        }
    }
}

