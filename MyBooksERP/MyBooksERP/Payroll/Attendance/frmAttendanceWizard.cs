﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Globalization;
using System.Configuration;
using System.IO;
using Microsoft.Win32;
using BLL;
using DTO;


namespace MyBooksERP
{
    public enum FileTypeID
    {
        CSV = 1,
        EXCEL = 2,
        MDB = 3
    }

    public partial class FrmAttendancewizard : Form
    {
        string mMappingEmployee;// variable mapping with employeenumber or code
        string mMappingDate;    // variable mapping with date 
        string sFilename;       // Name of selected file
        string sExtention;      // Extension of selected file (csv,xls)
        bool mBlnConnectionStatus = false;

        DataTable dtFetchData;
        clsBLLMapping MobjclsBLLMapping;
        ClsLogWriter mObjLogs;
        OleDbConnection mCon;
        string strCommon = "";
        string strCom = "";
        public FrmAttendancewizard()
        {
            InitializeComponent();
            //string strServer = "VBC-PC7";
            ////new clsRegistry().ReadFromRegistry("SOFTWARE\\Mypayserver", "Mypayservername", out this.strServer);
            //clsCommonSettings.ServerName = strServer;

            mCon = new OleDbConnection();
            MobjclsBLLMapping = new clsBLLMapping();
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}


        }



        //#region SetArabicControls
        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.AttendanceWizard, this);
        //}
        //#endregion SetArabicControls


        private void FrmAttendancewizard_Load(object sender, EventArgs e)
        {

        }

        private void WzPageTemplatename_NextButtonClick(object sender, CancelEventArgs e)
        {
            MobjclsBLLMapping.clsDTOMapping.TemplateName = TxtTemplateName.Text.Trim();

            if (TxtTemplateName.Text == string.Empty)
            {
                WarningTempname.Visible = true;
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    strCommon = "يرجى توفير اسم للقالب كما يحلو لك أن يظهر بعد التكوين."; 
                //}
                //else
                //{
                    strCommon = "Please provide a name for the template as you wish it to appear after configuration.";
                //}
                WarningTempname.Text =strCommon;

                TxtTemplateName.Focus();
                e.Cancel = true;
            }
            else if (MobjclsBLLMapping.IsExists())
            {
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    strCommon = "اسم القالب موجود بالفعل. هل تريد استبداله؟";
                //    strCom = "معالج الحضور";
                //}
                //else
                //{
                    strCommon = "Template Name already Exists. Do you want to Replace it?";
                    strCom = "Attendance Wizard";
                //}

                if (MessageBox.Show(strCommon, strCom, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    WarningTempname.Visible = true;

                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    strCommon = "  اسم قالب موجود بالفعل  ";
                    //}
                    //else
                    //{
                        strCommon = "Template Name already Exists.";
                    //}

                    WarningTempname.Text = strCommon;
                    TxtTemplateName.Focus();
                    e.Cancel = true;
                }
            }
            else
                WarningTempname.Visible = false;

        }

        private void WzPageTemplatename_AfterPageDisplayed(object sender, DevComponents.DotNetBar.WizardPageChangeEventArgs e)
        {
            TxtTemplateName.Focus();
        }

        private void WzPageFilebrowse_AfterPageDisplayed(object sender, DevComponents.DotNetBar.WizardPageChangeEventArgs e)
        {
            BtnFilebrowse.Focus();
        }

        private void WzPageFilebrowse_NextButtonClick(object sender, CancelEventArgs e)
        {

            if (TxtFileBrowse.Text == string.Empty)
            {
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    strCommon = "يرجى تحديد المسار لاستخراج البيانات للملف الحضور.";
                //}
                //else
                //{
                    strCommon = "Please select the path to extract the data for the attendance file.";
                //}


                WarningFilebrowse.Visible = true;
                WarningFilebrowse.Text = strCommon;
                TxtFileBrowse.Focus();
                e.Cancel = true;
            }
            else
                WarningFilebrowse.Visible = false;

            //if (sExtention == ".mdb")
            //{
            //    ProgressBarProcess.Visible = false;
            //    wizardPageLogin.Visible = true;
            //    AttendanceWizard.SelectedPage = AttendanceWizard.WizardPages[2];
            //}
            //else
            //{
            //    ProgressBarProcess.Visible = true;
            //    //wizardPageLogin.Visible = false;
            //    AttendanceWizard.SelectedPage = AttendanceWizard.WizardPages[3];
            //}
        }

        private void WzPageGriddisplay_AfterPageDisplayed(object sender, DevComponents.DotNetBar.WizardPageChangeEventArgs e)
        {
            RbHeaderYes.Focus();
            WzPageGriddisplay.Show();
        }

        private void WzPageGriddisplay_BackButtonClick(object sender, CancelEventArgs e)
        {
            prgBarSetPassword.Value = ProgressBarProcess.Value = 0;

            //if (sExtention == ".mdb")
            //{
            //    AttendanceWizard.SelectedPage = AttendanceWizard.WizardPages[2];
            //}
            //else
            //{
            //    //wizardPageLogin.Visible = false;
            //    AttendanceWizard.SelectedPage = AttendanceWizard.WizardPages[1];
            //}
        }

        private void WzPageGriddisplay_NextButtonClick(object sender, CancelEventArgs e)
        {
            if (DgvAttendanceFetch.Rows.Count <= 0)
            {
                e.Cancel = true;
            }
        }

        private void WzPageFormatdisplay_NextButtonClick(object sender, CancelEventArgs e)
        {
            if (Rbsingle.Checked == true)
            {
                AttendanceWizard.SelectedPage = AttendanceWizard.WizardPages[5];
            }

        }

        private void WzPageTimeselection_AfterPageDisplayed(object sender, DevComponents.DotNetBar.WizardPageChangeEventArgs e)
        {
            NumUpDpwnTime.Focus();
        }

        private void WzPageTimeselection_NextButtonClick(object sender, CancelEventArgs e)
        {

            if (Rbsingle.Checked == false)
            {
                if (NumUpDpwnTime.Text == string.Empty)
                {
                    Wartimecolumn.Visible = true;

                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    strCommon = "يرجى تحديد الوقت.";
                    //}
                    //else
                    //{
                        strCommon = "Please select time.";
                    //}

                    Wartimecolumn.Text = strCommon;
                    NumUpDpwnTime.Focus();
                    e.Cancel = true;
                }
                else
                {
                    Wartimecolumn.Visible = false;

                }
            }
        }

        private void WzPagemapping1_NextButtonClick(object sender, CancelEventArgs e)
        {
            if (CbomappingEmp.Text == string.Empty)
            {
                Warmappingemp.Visible = true;
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    Warmappingemp.Text = "الرجاء اختيار رئيس العمود الذي يتوافق مع الموظف رقم / الرمز.";
                //}
                //else
                //{
                    Warmappingemp.Text = "Please select the Column head that corresponds to the Employee No./Code.";
                //}
                CbomappingEmp.Focus();
                e.Cancel = true;
            }
            else
                Warmappingemp.Visible = false;

            mMappingEmployee = CbomappingEmp.Text;

        }

        private void WzPagemapping1_AfterPageDisplayed(object sender, DevComponents.DotNetBar.WizardPageChangeEventArgs e)
        {
            CbomappingEmp.Focus();
            Fillmappingemp();
        }

        private void Fillmappingemp()
        {
            try
            {
                CbomappingEmp.Items.Clear();
                for (int i = 0; i < DgvAttendanceFetch.Columns.Count; i++)
                {
                    CbomappingEmp.Items.Add(new clsListItem(DgvAttendanceFetch.Columns[i].Index, DgvAttendanceFetch.Columns[i].HeaderText));
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on Fillmappingemp :" + ex.Message + "()  " + this.Name + "", 0);
            }


        }
        private void BtnFilebrowse_Click(object sender, EventArgs e)
        {
            ProgressBarProcess.Visible = true;
            ProgressBarProcess.Minimum = 0;
            ProgressBarProcess.Maximum = 100;
            ProgressBarProcess.Value = 0;

            try
            {

                BtnFilebrowse.Enabled = false;

                if (FetchData(GetFilename()) && MobjclsBLLMapping.clsDTOMapping.intFileTypeID != Convert.ToInt32(FileTypeID.MDB))
                {
                    ProgressBarProcess.Value = 25;
                    //SaveData();
                    //dtFetchData = MobjclsBLLMapping.GetAllData();

                    DgvAttendanceFetch.DataSource = null;
                    DgvAttendancemapping1.DataSource = null;
                    DgvAttendanceFetchmapping2.DataSource = null;
                    DgvAttendancefetchmapping3.DataSource = null;
                    DgvAttendanceFetchTimeselection.DataSource = null;

                    DgvAttendanceFetch.DataSource = dtFetchData;
                    DgvAttendancemapping1.DataSource = dtFetchData;
                    DgvAttendancefetchmapping3.DataSource = dtFetchData;
                    DgvAttendanceFetchmapping2.DataSource = dtFetchData;
                    DgvAttendanceFetchTimeselection.DataSource = dtFetchData;

                    ProgressBarProcess.Value = 50;
                    TmProgress.Enabled = false;
                }

                //if (MobjclsBLLMapping.clsDTOMapping.intFileTypeID != Convert.ToInt32(FileTypeID.MDB))
                //    SaveData();

                ProgressBarProcess.Value = 100;
                BtnFilebrowse.Enabled = true;
            }
            catch
            {
                mObjLogs.WriteLog("Error on BtnFilebrowse_Click :Wizard()  " + this.Name + "", 0);
            }
        }

        private void SaveData()
        {
            try
            {
                CreateTable("AttendanceFetch");
                string sQuery = string.Empty;

                if (sExtention == ".xls" || sExtention == ".xlsx")
                {
                    for (int i = 0; i < DgvAttendanceFetch.Rows.Count; i++)
                    {
                        sQuery = "INSERT INTO AttendanceFetch values (";
                        string Columns = string.Empty;
                        for (int j = 0; j < DgvAttendanceFetch.Columns.Count; i++)
                        {
                            Columns = Columns + "'" + Convert.ToString(DgvAttendanceFetch.Rows[i].Cells[j].Value) + "'" + ",";
                            //Columns = Columns + "'" + GetName(Convert.ToString(DgvAttendanceFetch.Rows[i].Cells[j].Value)) + "'" + ",";
                        }
                        if (Columns != string.Empty)
                        {
                            Columns = Columns.Substring(0, Columns.Length - 1);
                            sQuery = sQuery + Columns + ")";
                        }
                        MobjclsBLLMapping.Execute(sQuery);
                    }

                }
                else
                {

                    for (int i = 1; i < DgvAttendanceFetch.Rows.Count; i++)
                    {
                        sQuery = "INSERT INTO AttendanceFetch values (";
                        string Columns = string.Empty;
                        for (int j = 0; j < DgvAttendanceFetch.Columns.Count; j++)
                        {
                            Columns = Columns + "'" + Convert.ToString(DgvAttendanceFetch.Rows[i].Cells[j].Value) + "'" + ",";
                            //Columns = Columns + "'" + GetName(Convert.ToString(DgvAttendanceFetch.Rows[i].Cells[j].Value)) + "'" + ",";
                        }
                        if (Columns != string.Empty)
                        {
                            Columns = Columns.Substring(0, Columns.Length - 1);
                            sQuery = sQuery + Columns + ")";
                        }
                        MobjclsBLLMapping.Execute(sQuery);
                    }

                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, Log.LogSeverity.Error);
            }
        }

        private void CreateTable(string sTableName)
        {
            try
            {
                string sqlCreate = string.Empty;
                MobjclsBLLMapping.Execute("IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AttendanceFetch]') AND type in (N'U'))DROP TABLE [AttendanceFetch]");
                sqlCreate = "CREATE TABLE " + sTableName + " ( ";
                string Columns = string.Empty;
                if (sExtention == ".mdb")
                {
                    for (int i = 0; i < DgvAttendanceFetch.Columns.Count; i++)
                    {
                        Columns = Columns + DgvAttendanceFetch.Columns[i].HeaderText + " VARCHAR(150) NULL,";
                    }
                }
                else
                {
                    for (int i = 0; i < DgvAttendanceFetch.Columns.Count; i++)
                    {
                        Columns = Columns + "Column" + i + " VARCHAR(150) NULL,";
                    }
                }
                if (Columns != string.Empty)
                {
                    Columns = Columns.Substring(0, Columns.Length - 1);
                    sqlCreate = sqlCreate + Columns + ")";
                }
                MobjclsBLLMapping.Execute(sqlCreate);
            }
            catch
            {
                mObjLogs.WriteLog("Error on CreateTable :Wizard()  " + this.Name + "", 0);
            }

        }
        private string GetName(string sName)
        {
            string sValue;
            sValue = sName.Replace(",", "").Trim();
            try
            {
                sValue = sValue.Replace("/", "").Trim();
                sValue = sValue.Replace("\"", "").Trim();

                return sValue;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on GetName :" + ex.Message + "  " + this.Name + "", 0);
                return sValue;
            }
        }

        private string GetFilename()
        {
            //DgvAttendanceFetch.ColumnCount = 0;
            //DgvAttendanceFetch.Rows.Clear();


            OpenFileDialog dlgbox = new OpenFileDialog();
            dlgbox.Filter = "CSV Files (*.csv)|*.csv| Excel File (*.xls;*.xlsx)|*.xls;*.xlsx| Access File (*.accdb;*.mdb)|*.accdb;*.mdb| Supported Files (*.csv;*.xls;*.xlsx;*.accdb;*.mdb)|*.csv;*.xls;*.xlsx;*.accdb;*.mdb";
            if (dlgbox.ShowDialog() == DialogResult.OK)
            {
                this.Cursor = Cursors.WaitCursor;
                sFilename = dlgbox.FileName;
                TxtFileBrowse.Text = sFilename;
            }
            else
                this.Cursor = Cursors.Default;
            dlgbox.Dispose();

            return sFilename;


        }
        private bool FetchData(string sFileName)
        {

            try
            {
                //wizardPageLogin.Visible = false;
                dtFetchData = new DataTable();

                FileInfo fInfo = new FileInfo(sFilename);
                if (fInfo.Exists == true)
                    sExtention = fInfo.Extension;
                else
                    return false;
                if (sFilename == "") return false;

                if (sExtention == ".CSV" || sExtention == ".csv") // CSV File 
                {
                    MobjclsBLLMapping.clsDTOMapping.intFileTypeID = Convert.ToInt32(FileTypeID.CSV);
                    string[] lines = File.ReadAllLines(sFilename);
                    string[] fields;
                    fields = lines[0].Split(',');

                    int cols = fields.GetLength(0);
                    DataTable dt = new DataTable();

                    for (int i = 0; i < cols; i++)
                        dt.Columns.Add("Column" + (i + 1), typeof(string));
                    DataRow Row;
                    for (int i = 0; i < lines.
                        GetLength(0); i++)
                    {
                        fields = lines[i].Split(',');

                        Row = dt.NewRow();

                        for (int f = 0; f < cols; f++)
                            Row[f] = fields[f];
                        dt.Rows.Add(Row);
                    }
                    DgvAttendanceFetch.DataSource = dt;
                    dtFetchData = dt;

                }
                else if (sExtention == ".xls" || sExtention == ".xlsx") // Excel file
                {
                    MobjclsBLLMapping.clsDTOMapping.intFileTypeID = Convert.ToInt32(FileTypeID.EXCEL);
                    DataSet ds = new DataSet();
                    string strCon = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + sFilename + ";Extended Properties=Excel 12.0;";
                    OleDbDataAdapter mydata = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", strCon);
                    mydata.TableMappings.Add("Table1", "ExcelTest");
                    mydata.Fill(ds);
                    DgvAttendanceFetch.DataSource = ds.Tables[0];
                    dtFetchData = ds.Tables[0];

                }
                else if (sExtention == ".mdb")
                {
                    MobjclsBLLMapping.clsDTOMapping.intFileTypeID = Convert.ToInt32(FileTypeID.MDB);
                    ProgressBarProcess.Visible = false;
                    wizardPageLogin.Visible = true;
                }
                return true;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on FetchData :" + Ex.Message + "  " + this.Name + "", 0);
                return false;
            }
        }

        private void WzPagemapping2_AfterPageDisplayed(object sender, DevComponents.DotNetBar.WizardPageChangeEventArgs e)
        {
            CbomappingDate.Focus();
            Fillmappingdate();
        }

        private void Fillmappingdate()
        {
            CbomappingDate.Items.Clear();

            for (int i = 0; i < DgvAttendanceFetch.Columns.Count; i++)
            {
                if (DgvAttendanceFetch.Columns[i].HeaderText != mMappingEmployee)
                    CbomappingDate.Items.Add(new clsListItem(DgvAttendanceFetch.Columns[i].Index, DgvAttendanceFetch.Columns[i].HeaderText));
            }


            Cbomapdateformat.Items.Clear();

            Cbomapdateformat.Items.Add(new clsListItem("dd/MMM/yyyy", "'dd/MMM/yyyy'  Or  'dd-MMM-yyyy'  Or  'dd\\MMM\\yyyy'"));
            Cbomapdateformat.Items.Add(new clsListItem("dd/MM/yyyy", "'dd/MM/yyyy'  Or  'dd-MM-yyyy'  Or  'dd\\MM\\yyyy'"));
            Cbomapdateformat.Items.Add(new clsListItem("MM/dd/yyyy", "'MM/dd/yyyy'  Or  'MM-dd-yyyy'  Or  'MM\\dd\\yyyy'"));
            Cbomapdateformat.Items.Add(new clsListItem("dd/MM/yy", "'dd/MM/yy'  Or  'dd-MM-yy'  Or  'dd\\MM\\yy'"));
            Cbomapdateformat.Items.Add(new clsListItem("MM/dd/yy", "'MM/dd/yy'  Or  'MM-dd-yy'  Or  'MM\\dd\\yy'"));
            Cbomapdateformat.Items.Add(new clsListItem("d/M/yyyy", "'d/M/yyyy'  Or  'd-M-yyyy'  Or  'd\\M\\yyyy'"));
            Cbomapdateformat.Items.Add(new clsListItem("M/d/yyyy", "'M/d/yyyy'  Or  'M-d-yyyy'  Or  'M\\d\\yyyy'"));
            Cbomapdateformat.Items.Add(new clsListItem("d/M/yy", "'d/M/yy'  Or  'd-M-yy'  Or  'd\\M\\yy'"));
            Cbomapdateformat.Items.Add(new clsListItem("M/d/yy", "'M/d/yy'  Or  'M-d-yy'  Or  'M\\d\\yy'"));
            Cbomapdateformat.Items.Add(new clsListItem("yyyy/MM/dd", "'yyyy/MM/dd'  Or  'yyyy-MM-dd'  Or  'yyyy\\MM\\dd'"));
            Cbomapdateformat.Items.Add(new clsListItem("yyyy/M/d", "'yyyy/M/d'  Or  'yyyy-M-d'  Or  'yyyy\\M\\d'"));
            Cbomapdateformat.Items.Add(new clsListItem("yy/MM/dd", "'yy/MM/dd'  Or  'yy-MM-dd'  Or  'yy\\MM\\dd'"));
            Cbomapdateformat.Items.Add(new clsListItem("yy/M/d", "'yy/M/d'  Or  'yy-M-d'  Or  'yy\\M\\d'"));
        }

        private void WzPagemapping2_NextButtonClick(object sender, CancelEventArgs e)
        {
            if (CbomappingDate.Text == string.Empty)
            {
                Warmappingdate.Visible = true;
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    Warmappingdate.Text = "الرجاء اختيار رئيس العمود الذي يتوافق مع تاريخ دخول الحضور.";
                //}
                //else
                //{
                    Warmappingdate.Text = "Please select the Column head that corresponds to the Date of attendance entry.";
                //}
                CbomappingDate.Focus();
                e.Cancel = true;
            }
            else
            {
                Warmappingdate.Visible = false;
                mMappingDate = CbomappingDate.Text;
                if (Cbomapdateformat.Text == string.Empty)
                {
                    Warmappingdate.Visible = true;
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    Warmappingdate.Text = "الرجاء اختيار تنسيق التاريخ الذي يتوافق مع تاريخ دخول الحضور.";
                    //}
                    //else
                    //{
                        Warmappingdate.Text = "Please select date format that corresponds to the Date of attendance entry.";
                    //}

                    Cbomapdateformat.Focus();
                    e.Cancel = true;
                }
                else
                    Warmappingdate.Visible = false;
            }

        }

        private void WzPagemapping3_AfterPageDisplayed(object sender, DevComponents.DotNetBar.WizardPageChangeEventArgs e)
        {
            LstMappingtime.Focus();
            Fillmappingtime();
        }

        private void Fillmappingtime()
        {
            try
            {
                LstMappingtime.Items.Clear();

                for (int i = 0; i < DgvAttendanceFetch.Columns.Count; i++)
                {
                    //if (DgvAttendanceFetch.Columns[i].HeaderText != mMappingEmployee && DgvAttendanceFetch.Columns[i].HeaderText != mMappingDate)
                        if (DgvAttendanceFetch.Columns[i].HeaderText != mMappingEmployee)
                            LstMappingtime.Items.Add(DgvAttendanceFetch.Columns[i].HeaderText);
                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Fillmappingtime" + Ex.Message + "  " + this.Name + "", 0);
            }
        }

        private void WzPagemapping3_NextButtonClick(object sender, CancelEventArgs e)
        {
            if (LstMappingtime.SelectedItems.Count == 0)
            {
                Warmappingtime.Visible = true;
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    Warmappingtime.Text = "الرجاء اختيار رئيس العمود الذي يتوافق مع الوقت.";
                //}
                //else
                //{
                    Warmappingtime.Text = "Please select the column head that corresponds to time.";
                //}

                LstMappingtime.Focus();
                e.Cancel = true;
            }
            else
            {
                Warmappingtime.Visible = false;
                if (NumUpDpwnTime.Text != string.Empty)
                {
                    int iTotalmappingcount;

                    if (Rbsingle.Checked == true)
                        iTotalmappingcount = 1;
                    else
                        iTotalmappingcount = Convert.ToInt32(NumUpDpwnTime.Text);
                    if (LstMappingtime.SelectedItems.Count != iTotalmappingcount)
                    {
                        Warmappingtime.Visible = true;
                        //if (ClsCommonSettings.IsArabicView)
                        //{
                        //    Warmappingtime.Text = "يرجى التأكد من اختيار قمت بها يتوافق مع العمود الوقت.";
                        //}
                        //else
                        //{
                            Warmappingtime.Text = "Please ensure that the selection you have made corresponds to the time column.";
                        //}
                        e.Cancel = true;
                    }
                }
            }

        }

        private void WzPageFinalmapping_AfterPageDisplayed(object sender, DevComponents.DotNetBar.WizardPageChangeEventArgs e)
        {
            LoadCombos();
            FillMappingGrid();
        }

        private void LoadCombos()
        {
            //DataTable  datCombos = MobjclsBLLMapping.FillCombos(new string[] { "CompanyIndustryID,Description", "IndustryReference", "", "CompanyIndustryID", "Description" });
            try
            {
                //DataTable datCombos = MobjclsBLLMapping.FillCombos(new string[] { "" + mMappingEmployee + "," + mMappingEmployee + "", "Attendancefetch", "", "" + mMappingEmployee + "", "" + mMappingEmployee + "" });
                
                CboPunch.DataSource = dtFetchData.DefaultView.ToTable(true, "" + mMappingEmployee + "");
                CboPunch.ValueMember = "" + mMappingEmployee + "";
                CboPunch.DisplayMember = "" + mMappingEmployee + "";
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("LoadCombos" + Ex.Message + "  " + this.Name + "", 0);
            }
        }


        private void FillMappingGrid()
        {
            try
            {
                DgvAttendanceMapping.RowCount = 0;

                DataTable dtEmployee = MobjclsBLLMapping.FillCombos(new string[] { "EmployeeID,isnull(FirstName,'') + ' ' + isnull(LastName,'')as Firstname", "EmployeeMaster", "", " EmployeeID ", " Firstname +" });
                CboEmployee.ValueMember = "EmployeeID";
                CboEmployee.DisplayMember = "Firstname";
                CboEmployee.DataSource = dtEmployee;


                dtEmployee = null;
                MobjclsBLLMapping.clsDTOMapping.TemplateName = TxtTemplateName.Text.ToUpper();
                dtEmployee = MobjclsBLLMapping.GetMappingEmployeeList();
                if (dtEmployee.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEmployee.Rows.Count; i++)
                    {
                        DgvAttendanceMapping.RowCount = DgvAttendanceMapping.RowCount + 1;
                        DgvAttendanceMapping.Rows[i].Cells[0].Value = Convert.ToInt64(dtEmployee.Rows[i]["EmployeeID"]);
                        DgvAttendanceMapping.Rows[i].Cells[1].Value = Convert.ToString(dtEmployee.Rows[i]["EmployeeNumber"]);
                        if (dtEmployee.Rows[i]["Workid"] != DBNull.Value && dtEmployee.Rows[i]["Workid"].ToStringCustom() != "")
                            DgvAttendanceMapping.Rows[i].Cells[2].Value = Convert.ToString(dtEmployee.Rows[i]["Workid"]);
                        DgvAttendanceMapping.Rows[i].Cells[4].Value = Convert.ToString(dtEmployee.Rows[i]["Companyid"]);
                    }
                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("FillMappingGrid" + Ex.Message + "  " + this.Name + "", 0);
            }
        }

        private void DgvAttendanceMapping_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
        /// <summary>
        /// FINAL SAVING OF TEMPLATE
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WzPageFinalmapping_NextButtonClick(object sender, CancelEventArgs e)
        {

            Warmappingfinal.Text = string.Empty;
            if (CheckValidation())
            {
                SaveTemplate();
                Warmappingfinal.Visible = true;
            }
            else
            {
                Warmappingfinal.Visible = true;
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    Warmappingfinal.Text = "وجود ازدواجية.";
                //}
                //else
                //{
                    Warmappingfinal.Text = "Duplication exists.";
                //}

                e.Cancel = true;
            }
        }

        private void Savemappeddata(int iTemplateID)
        {
            try
            {
                string sQuery;
                if (DgvAttendanceMapping.RowCount >= 1)
                {

                    for (int i = 0; i < DgvAttendanceMapping.RowCount; i++)
                    {
                        sQuery = "INSERT INTO PayEmployeeAttendanceMapping(EmployeeID,EquipmentID,WorkID,IsActive,TemplateMasterID,SettingsID,CompanyID) values (" + Convert.ToInt32(DgvAttendanceMapping.Rows[i].Cells[0].Value) + ",0,'" + DgvAttendanceMapping.Rows[i].Cells[2].Value + "',1," + iTemplateID + ",0," + Convert.ToInt32(DgvAttendanceMapping.Rows[i].Cells[4].Value) + ")";
                        MobjclsBLLMapping.Execute(sQuery);
                    }

                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Savemappeddata" + Ex.Message + "  " + this.Name + "", 0);
            }
        }

        private void SaveAttendanceFileDetails(int iTemplateID)
        {
            try
            {
                string sQuery;
                sQuery = "DELETE PayAttendanceTemplateFileDetails WHERE templatemasterID=" + iTemplateID;
                MobjclsBLLMapping.Execute(sQuery);

                sQuery = "INSERT INTO PayAttendanceTemplateFileDetails(TemplateMasterID,UserName,Password,TableName,ServerName,Criteria) values (" + iTemplateID + ",'','" + txtPassword.Text + "','" + cboTableNames.Text + "','','')";
                MobjclsBLLMapping.Execute(sQuery);
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("SaveAttendanceFileDetails" + Ex.Message + "  " + this.Name + "", 0);
            }
        }

        private bool CheckValidation()
        {
            try
            {
                Warmappingfinal.Visible = false;
                if (DgvAttendanceMapping.Rows.Count >= 1)
                {
                    for (int i = 0; i < DgvAttendanceMapping.Rows.Count - 1; i++)
                    {
                        int iTempID = 0;
                        iTempID = CheckDuplicationInGrid(DgvAttendanceMapping, 2);
                        if (iTempID != -1)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("CheckValidation" + Ex.Message + "  " + this.Name + "", 0);
                return false;
            }
        }

        private int CheckDuplicationInGrid(DataGridView Grd, int ColIndexfirst)
        {
            string SearchValuefirst = "";
            int RowIndexTemp = 0;

            foreach (DataGridViewRow rowValue in Grd.Rows)
            {
                if (rowValue.Cells[ColIndexfirst].Value != null)
                {
                    SearchValuefirst = Convert.ToString(rowValue.Cells[ColIndexfirst].Value);
                    RowIndexTemp = rowValue.Index;
                    foreach (DataGridViewRow row in Grd.Rows)
                    {
                        if (RowIndexTemp != row.Index)
                        {

                            if (row.Cells[ColIndexfirst].Value != null)
                            {
                                if ((Convert.ToString(row.Cells[ColIndexfirst].Value).Trim() == Convert.ToString(SearchValuefirst).Trim()))
                                    return row.Index;

                            }

                        }
                    }
                }
            }
            return -1;
        }

        private void SaveTemplate()
        {
            try
            {
                int iTemplateID = 0;
                MobjclsBLLMapping.clsDTOMapping.TemplateName = TxtTemplateName.Text.ToUpper();
                MobjclsBLLMapping.clsDTOMapping.HeaderExists = RbHeaderYes.Checked;

                string iPunchingcount;

                if (Rbsingle.Checked)
                    iPunchingcount = "1";
                else
                    iPunchingcount = NumUpDpwnTime.Text;

                MobjclsBLLMapping.clsDTOMapping.Punchingcount = iPunchingcount;
                MobjclsBLLMapping.clsDTOMapping.Dateformat = ((clsListItem)(Cbomapdateformat.SelectedItem)).Value.ToString();
                MobjclsBLLMapping.clsDTOMapping.Timewithdate = rbtimedate.Checked;
                iTemplateID = MobjclsBLLMapping.SaveTemplate();

                if (iTemplateID > 0)
                {
                    if (LstMappingtime.Items.Count > 0)
                    {
                        string sQuery = "INSERT INTO PayAttendanceTemplateDetail (TemplateMasterID,Templatefield,Actualfield) values(" + iTemplateID + ",'EmployeeNumber','" + mMappingEmployee + "')";
                        MobjclsBLLMapping.Execute(sQuery);
                        sQuery = "INSERT INTO PayAttendanceTemplateDetail (TemplateMasterID,Templatefield,Actualfield) values(" + iTemplateID + ",'Date','" + mMappingDate + "')";
                        MobjclsBLLMapping.Execute(sQuery);

                        for (int i = 0; i < LstMappingtime.SelectedItems.Count; i++)
                        {
                            string sListitem1 = LstMappingtime.SelectedItems[i].Text;
                            sQuery = "INSERT INTO PayAttendanceTemplateDetail (TemplateMasterID,Templatefield,Actualfield) values(" + iTemplateID + ",'Time','" + sListitem1 + "')";
                            MobjclsBLLMapping.Execute(sQuery);
                        }

                    }
                    Savemappeddata(iTemplateID);

                    if (MobjclsBLLMapping.clsDTOMapping.intFileTypeID == Convert.ToInt32(FileTypeID.MDB))
                        SaveAttendanceFileDetails(iTemplateID);
                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("SaveTemplate" + Ex.Message + "  " + this.Name + "", 0);

            }
        }

        private void WizardPage1_AfterPageDisplayed(object sender, DevComponents.DotNetBar.WizardPageChangeEventArgs e)
        {
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    lblfinalmsg.Text = "تهاني! تم بنجاح تكوين إعدادات الحضور للاندماج مع ورقة وقتك.";
            //}
            //else
            //{
                lblfinalmsg.Text = "Congrats! You have successfully configured the attendance settings to merge with your time sheet.";
            //}
            WizardPage1.BackButtonEnabled = DevComponents.DotNetBar.eWizardButtonState.False;
            WizardPage1.CancelButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            WizardPage1.HelpButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;

        }

        private void WizardPage1_FinishButtonClick(object sender, CancelEventArgs e)
        {
            this.Close();
        }

        private void AttendanceWizard_CancelButtonClick(object sender, CancelEventArgs e)
        {
            this.Close();
        }

        private void AttendanceWizard_HelpButtonClick(object sender, CancelEventArgs e)
        {
            using (FrmHelp Help = new FrmHelp())
            { Help.strFormName = "AttendanceWizard"; Help.ShowDialog(); }
        }

        private bool TestConnection()
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                string strCon = "";
                mBlnConnectionStatus = false;

                if (txtPassword.Text != string.Empty)
                    strCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + sFilename + "; Jet OLEDB:Database Password=" + txtPassword.Text;
                else
                    strCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + sFilename;

                mCon.ConnectionString = strCon;
                mCon.Open();

                dt = mCon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, null });
                dt.DefaultView.RowFilter = "TABLE_TYPE='TABLE'";
                cboTableNames.DataSource = dt.DefaultView.ToTable();
                cboTableNames.DisplayMember = "TABLE_NAME";
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    warningSetPassword.Text = "نجح اختبار الاتصال.";
                //}
                //else
                //{
                    warningSetPassword.Text = "Test connection succeeded.";
                //}

                mBlnConnectionStatus = true;
                return true;
            }
            catch (Exception Ex)
            {
                warningSetPassword.Text = "Test connection failed.";
                mObjLogs.WriteLog("Error on TestConnection :" + Ex.Message + "  " + this.Name + "", 0);
                return false;
            }
        }

        private void btnTestConnection_Click(object sender, EventArgs e)
        {
            TestConnection();
        }

        private bool FillTableNames()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                prgBarSetPassword.Visible = true;
                prgBarSetPassword.Minimum = 0;
                prgBarSetPassword.Maximum = 100;
                prgBarSetPassword.Value = 0;

                DataSet ds = new DataSet();
                OleDbDataAdapter mydata = new OleDbDataAdapter("Select * From " + cboTableNames.Text, mCon);
                mydata.Fill(ds);
                DgvAttendanceFetch.DataSource = ds.Tables[0];
                dtFetchData = ds.Tables[0];

                prgBarSetPassword.Value = 25;
                //SaveData();
                //dtFetchData = MobjclsBLLMapping.GetAllData();
                DgvAttendancemapping1.DataSource = null;
                DgvAttendanceFetchmapping2.DataSource = null;
                DgvAttendancefetchmapping3.DataSource = null;
                DgvAttendanceFetchTimeselection.DataSource = null;

                DgvAttendancemapping1.DataSource = dtFetchData;
                DgvAttendancefetchmapping3.DataSource = dtFetchData;
                DgvAttendanceFetchmapping2.DataSource = dtFetchData;
                DgvAttendanceFetchTimeselection.DataSource = dtFetchData;
                prgBarSetPassword.Value = 50;
                //SaveData();

                prgBarSetPassword.Value = 100;
                TmProgress.Enabled = false;
                this.Cursor = Cursors.Default;
                return true;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on FillTableNames :" + Ex.Message + "  " + this.Name + "", 0);
                return false;
            }
        }

        private void wizardPageLogin_NextButtonClick(object sender, CancelEventArgs e)
        {
            if (sExtention == ".mdb")
            {
                if (mBlnConnectionStatus == false)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    warningSetPassword.Text = "يرجى الاتصال بقاعدة بيانات.";
                    //}
                    //else
                    //{
                        warningSetPassword.Text = "Please connect to the database.";
                    //}
                    btnTestconnection.Focus();
                    e.Cancel = true;
                }
                if (cboTableNames.Text == string.Empty)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    warningSetPassword.Text = "Please select the table name.";
                    //}
                    ////else
                    ////{
                    //    warningSetPassword.Text = "يرجى تحديد اسم الجدول.";
                    //}
                    cboTableNames.Focus();
                    e.Cancel = true;
                }
                FillTableNames();
            }
        }

        private void wizardPageLogin_AfterPageDisplayed(object sender, DevComponents.DotNetBar.WizardPageChangeEventArgs e)
        {
            if (sExtention == ".mdb")
            {
                wizardPageLogin.Enabled = true;
                txtPassword.Focus();
            }
            else
            {
                wizardPageLogin.Enabled = false;
            }
        }
    }
}
