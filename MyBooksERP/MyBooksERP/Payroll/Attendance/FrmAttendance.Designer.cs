﻿using DemoClsDataGridview;
namespace MyBooksERP
{
    partial class FrmAttendance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAttendance));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.DgvEmployee = new ClsDataGirdViewX();
            this.Column1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.ExpPanelEmployee = new DevComponents.DotNetBar.ExpandablePanel();
            this.CboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.grpSearchLeft = new System.Windows.Forms.GroupBox();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnSearch = new DevComponents.DotNetBar.ButtonX();
            this.lblShowBy = new DevComponents.DotNetBar.LabelX();
            this.cboEmpWorkstatus = new DevComponents.DotNetBar.Controls.ComboTree();
            this.ImageListEmployee = new System.Windows.Forms.ImageList(this.components);
            this.CboEmpFilterType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboEmpFilter = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.expandableSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelRightMain = new DevComponents.DotNetBar.PanelEx();
            this.DgvAttendance = new ClsDataGirdViewX();
            this.pnlConnectionStatus = new DevComponents.DotNetBar.PanelEx();
            this.lblAnimation = new DevComponents.DotNetBar.LabelX();
            this.pgrsBarConnection = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.DgvPunchingData = new ClsDataGirdViewX();
            this.ColDgvWorkID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvEmployeeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvPunchingDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvTimings = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvInOutStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvPunchingResult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelBottom = new DevComponents.DotNetBar.PanelEx();
            this.lblEarlyGoing = new DevComponents.DotNetBar.LabelX();
            this.lblLateComing = new DevComponents.DotNetBar.LabelX();
            this.lblPrgressPercentage = new DevComponents.DotNetBar.LabelX();
            this.lblProgressShow = new DevComponents.DotNetBar.LabelX();
            this.lblWorkTime = new DevComponents.DotNetBar.LabelX();
            this.lblAllowedBreak = new DevComponents.DotNetBar.LabelX();
            this.lblAllowedBreakTime = new DevComponents.DotNetBar.LabelX();
            this.lblShiftName = new DevComponents.DotNetBar.LabelX();
            this.lblBreak = new DevComponents.DotNetBar.LabelX();
            this.lblShift = new DevComponents.DotNetBar.LabelX();
            this.lblBreakTime = new DevComponents.DotNetBar.LabelX();
            this.lblExcessTime = new DevComponents.DotNetBar.LabelX();
            this.lblShortageTime = new DevComponents.DotNetBar.LabelX();
            this.lblWork = new DevComponents.DotNetBar.LabelX();
            this.lblShortage = new DevComponents.DotNetBar.LabelX();
            this.lblExcess = new DevComponents.DotNetBar.LabelX();
            this.lblConsequence = new DevComponents.DotNetBar.LabelX();
            this.ExpandablepnlSearch = new DevComponents.DotNetBar.ExpandablePanel();
            this.grpMain = new System.Windows.Forms.GroupBox();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.ChkLate = new System.Windows.Forms.CheckBox();
            this.ChkEarly = new System.Windows.Forms.CheckBox();
            this.ChkExtendedBreak = new System.Windows.Forms.CheckBox();
            this.ChkOverTime = new System.Windows.Forms.CheckBox();
            this.ChkAbsent = new System.Windows.Forms.CheckBox();
            this.ChkLeave = new System.Windows.Forms.CheckBox();
            this.ChkConsequence = new System.Windows.Forms.CheckBox();
            this.ChkShortage = new System.Windows.Forms.CheckBox();
            this.btnShow = new System.Windows.Forms.Button();
            this.rbtnMonthly = new System.Windows.Forms.RadioButton();
            this.rbtnDaily = new System.Windows.Forms.RadioButton();
            this.grpDaySummary = new System.Windows.Forms.GroupBox();
            this.lblShortageCountDispaly = new DevComponents.DotNetBar.LabelX();
            this.lblShortageCount = new DevComponents.DotNetBar.LabelX();
            this.lblLeave = new DevComponents.DotNetBar.LabelX();
            this.lblLeaveCountDispaly = new DevComponents.DotNetBar.LabelX();
            this.lblAbsentDispaly = new DevComponents.DotNetBar.LabelX();
            this.lblPresent = new DevComponents.DotNetBar.LabelX();
            this.lblPresentDisplay = new DevComponents.DotNetBar.LabelX();
            this.lblAbsent = new DevComponents.DotNetBar.LabelX();
            this.dockSite7 = new DevComponents.DotNetBar.DockSite();
            this.BarFunctions = new DevComponents.DotNetBar.Bar();
            this.btnDevices = new DevComponents.DotNetBar.ButtonItem();
            this.btnTemplates = new DevComponents.DotNetBar.ButtonItem();
            this.btnMapping = new DevComponents.DotNetBar.ButtonItem();
            this.btnLive = new DevComponents.DotNetBar.ButtonItem();
            this.btnOptions = new DevComponents.DotNetBar.ButtonItem();
            this.btnFetchData = new DevComponents.DotNetBar.ButtonItem();
            this.btnViewValidTimings = new DevComponents.DotNetBar.ButtonItem();
            this.btnInvalidTimings = new DevComponents.DotNetBar.ButtonItem();
            this.btnViewAllTimings = new DevComponents.DotNetBar.ButtonItem();
            this.btnViewFirstandLastTimings = new DevComponents.DotNetBar.ButtonItem();
            this.btnViewDetailInfo = new DevComponents.DotNetBar.ButtonItem();
            this.btnViewDay = new DevComponents.DotNetBar.ButtonItem();
            this.btnViewWorkTime = new DevComponents.DotNetBar.ButtonItem();
            this.btnViewBreakTime = new DevComponents.DotNetBar.ButtonItem();
            this.btnViewExcessTime = new DevComponents.DotNetBar.ButtonItem();
            this.btnViewLateComing = new DevComponents.DotNetBar.ButtonItem();
            this.btnViewEarlyGoing = new DevComponents.DotNetBar.ButtonItem();
            this.btnviewLogsFromHandpunch = new DevComponents.DotNetBar.ButtonItem();
            this.CustomizeItem2 = new DevComponents.DotNetBar.CustomizeItem();
            this.BarView = new DevComponents.DotNetBar.Bar();
            this.btnSave = new DevComponents.DotNetBar.ButtonItem();
            this.btnDelete = new DevComponents.DotNetBar.ButtonItem();
            this.btnClear = new DevComponents.DotNetBar.ButtonItem();
            this.btnAutoFill = new DevComponents.DotNetBar.ButtonItem();
            this.btnExportToExcel = new DevComponents.DotNetBar.ButtonItem();
            this.btnLeaveOpening = new DevComponents.DotNetBar.ButtonItem();
            this.btnHelp = new DevComponents.DotNetBar.ButtonItem();
            this.CustomizeItem1 = new DevComponents.DotNetBar.CustomizeItem();
            this.barStatusBottom = new DevComponents.DotNetBar.Bar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.lblToolStripStatus = new DevComponents.DotNetBar.LabelItem();
            this.lblAttendnaceStatus = new DevComponents.DotNetBar.LabelItem();
            this.barProgressBarAttendance = new DevComponents.DotNetBar.ProgressBarItem();
            this.lblEmployeeSelected = new DevComponents.DotNetBar.LabelItem();
            this.lblEmployeeSelectedValue = new DevComponents.DotNetBar.LabelItem();
            this.DTPFormatTime = new System.Windows.Forms.DateTimePicker();
            this.tmrReadLogs = new System.Windows.Forms.Timer(this.components);
            this.tmrClearlabels = new System.Windows.Forms.Timer(this.components);
            this.dotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.dockSite4 = new DevComponents.DotNetBar.DockSite();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.dockSite2 = new DevComponents.DotNetBar.DockSite();
            this.dockSite8 = new DevComponents.DotNetBar.DockSite();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.dockSite6 = new DevComponents.DotNetBar.DockSite();
            this.dockSite3 = new DevComponents.DotNetBar.DockSite();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.ColDgvAtnDate = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvEmployee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvStatus = new DevComponents.DotNetBar.Controls.DataGridViewComboBoxExColumn();
            this.ColDgvAddMore = new DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn();
            this.ColDgvRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvShift = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvDay = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvBreakTime = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvWorkTime = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvLateComing = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvEarlyGoing = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvExcessTime = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvCompanyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvConsequenceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvPolicyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvConseqAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvValidFlag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvShiftTimeDisplay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvAllowedBreakTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvHolidayFlag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvLeaveFlag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvAbsentTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvShiftOrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvLeaveInfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvLeaveId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvLeaveType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvPunchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvProjectID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvEmployee)).BeginInit();
            this.ExpPanelEmployee.SuspendLayout();
            this.grpSearchLeft.SuspendLayout();
            this.panelRightMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendance)).BeginInit();
            this.pnlConnectionStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvPunchingData)).BeginInit();
            this.panelBottom.SuspendLayout();
            this.ExpandablepnlSearch.SuspendLayout();
            this.grpMain.SuspendLayout();
            this.grpDaySummary.SuspendLayout();
            this.dockSite7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarFunctions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barStatusBottom)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.PanelLeft.Controls.Add(this.DgvEmployee);
            this.PanelLeft.Controls.Add(this.ExpPanelEmployee);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(232, 664);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 0;
            this.PanelLeft.Text = "panelEx1";
            // 
            // DgvEmployee
            // 
            this.DgvEmployee.AddNewRow = false;
            this.DgvEmployee.AllowUserToAddRows = false;
            this.DgvEmployee.AllowUserToDeleteRows = false;
            this.DgvEmployee.AllowUserToResizeColumns = false;
            this.DgvEmployee.AllowUserToResizeRows = false;
            this.DgvEmployee.AlphaNumericCols = new int[0];
            this.DgvEmployee.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.DgvEmployee.CapsLockCols = new int[0];
            this.DgvEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvEmployee.ColumnHeadersVisible = false;
            this.DgvEmployee.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.DgvEmployee.DecimalCols = new int[0];
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvEmployee.DefaultCellStyle = dataGridViewCellStyle1;
            this.DgvEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvEmployee.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvEmployee.HasSlNo = false;
            this.DgvEmployee.LastRowIndex = 0;
            this.DgvEmployee.Location = new System.Drawing.Point(0, 182);
            this.DgvEmployee.Name = "DgvEmployee";
            this.DgvEmployee.NegativeValueCols = new int[0];
            this.DgvEmployee.NumericCols = new int[0];
            this.DgvEmployee.ReadOnly = true;
            this.DgvEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvEmployee.Size = new System.Drawing.Size(232, 482);
            this.DgvEmployee.TabIndex = 1;
            this.DgvEmployee.Sorted += new System.EventHandler(this.DgvEmployee_Sorted);
            this.DgvEmployee.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvEmployee_CellClick);
            this.DgvEmployee.Click += new System.EventHandler(this.DgvEmployee_Click);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.MinimumWidth = 20;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 20;
            // 
            // ExpPanelEmployee
            // 
            this.ExpPanelEmployee.CanvasColor = System.Drawing.SystemColors.Control;
            this.ExpPanelEmployee.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ExpPanelEmployee.Controls.Add(this.CboCompany);
            this.ExpPanelEmployee.Controls.Add(this.grpSearchLeft);
            this.ExpPanelEmployee.Controls.Add(this.lblShowBy);
            this.ExpPanelEmployee.Controls.Add(this.cboEmpWorkstatus);
            this.ExpPanelEmployee.Controls.Add(this.CboEmpFilterType);
            this.ExpPanelEmployee.Controls.Add(this.CboEmpFilter);
            this.ExpPanelEmployee.Dock = System.Windows.Forms.DockStyle.Top;
            this.ExpPanelEmployee.Location = new System.Drawing.Point(0, 0);
            this.ExpPanelEmployee.Name = "ExpPanelEmployee";
            this.ExpPanelEmployee.Size = new System.Drawing.Size(232, 182);
            this.ExpPanelEmployee.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.ExpPanelEmployee.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.ExpPanelEmployee.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.ExpPanelEmployee.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.ExpPanelEmployee.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.ExpPanelEmployee.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.ExpPanelEmployee.Style.GradientAngle = 90;
            this.ExpPanelEmployee.TabIndex = 0;
            this.ExpPanelEmployee.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.ExpPanelEmployee.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.ExpPanelEmployee.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.ExpPanelEmployee.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.ExpPanelEmployee.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.ExpPanelEmployee.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.ExpPanelEmployee.TitleStyle.GradientAngle = 90;
            this.ExpPanelEmployee.TitleText = "Company";
            this.ExpPanelEmployee.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.ExpPanelEmployee_ExpandedChanged);
            this.ExpPanelEmployee.ExpandedChanging += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.ExpPanelEmployee_ExpandedChanging);
            // 
            // CboCompany
            // 
            this.CboCompany.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompany.DisplayMember = "Text";
            this.CboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboCompany.FormattingEnabled = true;
            this.CboCompany.ItemHeight = 14;
            this.CboCompany.Location = new System.Drawing.Point(11, 3);
            this.CboCompany.Name = "CboCompany";
            this.CboCompany.Size = new System.Drawing.Size(178, 20);
            this.CboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboCompany.TabIndex = 0;
            this.CboCompany.WatermarkText = "Company";
            this.CboCompany.SelectedIndexChanged += new System.EventHandler(this.CboCompany_SelectedIndexChanged);
            this.CboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboCompany_KeyDown);
            // 
            // grpSearchLeft
            // 
            this.grpSearchLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSearchLeft.Controls.Add(this.txtSearch);
            this.grpSearchLeft.Controls.Add(this.btnSearch);
            this.grpSearchLeft.Location = new System.Drawing.Point(12, 113);
            this.grpSearchLeft.Name = "grpSearchLeft";
            this.grpSearchLeft.Size = new System.Drawing.Size(204, 62);
            this.grpSearchLeft.TabIndex = 5;
            this.grpSearchLeft.TabStop = false;
            this.grpSearchLeft.Text = "Search";
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(6, 14);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(188, 20);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // btnSearch
            // 
            this.btnSearch.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSearch.Image = global::MyBooksERP.Properties.Resources.Search;
            this.btnSearch.Location = new System.Drawing.Point(160, 36);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(34, 20);
            this.btnSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblShowBy
            // 
            // 
            // 
            // 
            this.lblShowBy.BackgroundStyle.Class = "";
            this.lblShowBy.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblShowBy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShowBy.Location = new System.Drawing.Point(12, 64);
            this.lblShowBy.Name = "lblShowBy";
            this.lblShowBy.Size = new System.Drawing.Size(53, 23);
            this.lblShowBy.TabIndex = 4;
            this.lblShowBy.Text = "Show by";
            // 
            // cboEmpWorkstatus
            // 
            this.cboEmpWorkstatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboEmpWorkstatus.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.cboEmpWorkstatus.BackgroundStyle.Class = "TextBoxBorder";
            this.cboEmpWorkstatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cboEmpWorkstatus.ButtonDropDown.Visible = true;
            this.cboEmpWorkstatus.ColumnsVisible = false;
            this.cboEmpWorkstatus.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboEmpWorkstatus.GridColumnLines = false;
            this.cboEmpWorkstatus.ImageIndex = 0;
            this.cboEmpWorkstatus.ImageList = this.ImageListEmployee;
            this.cboEmpWorkstatus.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.cboEmpWorkstatus.Location = new System.Drawing.Point(11, 39);
            this.cboEmpWorkstatus.Name = "cboEmpWorkstatus";
            this.cboEmpWorkstatus.Size = new System.Drawing.Size(207, 20);
            this.cboEmpWorkstatus.TabIndex = 1;
            this.cboEmpWorkstatus.WatermarkText = "Work Status";
            this.cboEmpWorkstatus.SelectedIndexChanged += new System.EventHandler(this.cboEmpWorkstatus_SelectedIndexChanged);
            // 
            // ImageListEmployee
            // 
            this.ImageListEmployee.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageListEmployee.ImageStream")));
            this.ImageListEmployee.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageListEmployee.Images.SetKeyName(0, "Employee_Absconding.png");
            this.ImageListEmployee.Images.SetKeyName(1, "Employee_Expired.PNG");
            this.ImageListEmployee.Images.SetKeyName(2, "Employee_InService.PNG");
            this.ImageListEmployee.Images.SetKeyName(3, "Employee_Probation.PNG");
            this.ImageListEmployee.Images.SetKeyName(4, "Employee_Resigned.PNG");
            this.ImageListEmployee.Images.SetKeyName(5, "Employee_Retired.PNG");
            this.ImageListEmployee.Images.SetKeyName(6, "Employee_Terminated.png");
            this.ImageListEmployee.Images.SetKeyName(7, "female_Employee_Absconding.PNG");
            this.ImageListEmployee.Images.SetKeyName(8, "Employee_female_Expired.PNG");
            this.ImageListEmployee.Images.SetKeyName(9, "Employee_female_InService.png");
            this.ImageListEmployee.Images.SetKeyName(10, "Employee_female_Probation.png");
            this.ImageListEmployee.Images.SetKeyName(11, "Employee_female_Resigned.PNG");
            this.ImageListEmployee.Images.SetKeyName(12, "female_Employee_Retired.png");
            this.ImageListEmployee.Images.SetKeyName(13, "female_Employee_Terminated.PNG");
            this.ImageListEmployee.Images.SetKeyName(14, "Employee_All.PNG");
            this.ImageListEmployee.Images.SetKeyName(15, "Male_Others.PNG");
            // 
            // CboEmpFilterType
            // 
            this.CboEmpFilterType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CboEmpFilterType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboEmpFilterType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboEmpFilterType.DisplayMember = "Text";
            this.CboEmpFilterType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboEmpFilterType.DropDownHeight = 134;
            this.CboEmpFilterType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboEmpFilterType.FormattingEnabled = true;
            this.CboEmpFilterType.IntegralHeight = false;
            this.CboEmpFilterType.ItemHeight = 15;
            this.CboEmpFilterType.Location = new System.Drawing.Point(11, 92);
            this.CboEmpFilterType.Name = "CboEmpFilterType";
            this.CboEmpFilterType.Size = new System.Drawing.Size(207, 21);
            this.CboEmpFilterType.TabIndex = 3;
            this.CboEmpFilterType.SelectedIndexChanged += new System.EventHandler(this.CboEmpFilterType_SelectedIndexChanged);
            this.CboEmpFilterType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboEmpFilterType_KeyDown);
            // 
            // CboEmpFilter
            // 
            this.CboEmpFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CboEmpFilter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboEmpFilter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboEmpFilter.DisplayMember = "Text";
            this.CboEmpFilter.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboEmpFilter.DropDownHeight = 134;
            this.CboEmpFilter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboEmpFilter.FormattingEnabled = true;
            this.CboEmpFilter.IntegralHeight = false;
            this.CboEmpFilter.ItemHeight = 15;
            this.CboEmpFilter.Location = new System.Drawing.Point(70, 65);
            this.CboEmpFilter.Name = "CboEmpFilter";
            this.CboEmpFilter.Size = new System.Drawing.Size(148, 21);
            this.CboEmpFilter.TabIndex = 2;
            this.CboEmpFilter.WatermarkText = "D";
            this.CboEmpFilter.SelectedIndexChanged += new System.EventHandler(this.CboEmpFilter_SelectedIndexChanged);
            this.CboEmpFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboEmpFilter_KeyDown);
            // 
            // expandableSplitterLeft
            // 
            this.expandableSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitterLeft.ExpandableControl = this.PanelLeft;
            this.expandableSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitterLeft.Location = new System.Drawing.Point(232, 0);
            this.expandableSplitterLeft.Name = "expandableSplitterLeft";
            this.expandableSplitterLeft.Size = new System.Drawing.Size(3, 664);
            this.expandableSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitterLeft.TabIndex = 1;
            this.expandableSplitterLeft.TabStop = false;
            this.expandableSplitterLeft.ExpandedChanging += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitterLeft_ExpandedChanging);
            // 
            // panelRightMain
            // 
            this.panelRightMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelRightMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelRightMain.Controls.Add(this.DgvAttendance);
            this.panelRightMain.Controls.Add(this.pnlConnectionStatus);
            this.panelRightMain.Controls.Add(this.DgvPunchingData);
            this.panelRightMain.Controls.Add(this.panelBottom);
            this.panelRightMain.Controls.Add(this.ExpandablepnlSearch);
            this.panelRightMain.Controls.Add(this.dockSite7);
            this.panelRightMain.Controls.Add(this.barStatusBottom);
            this.panelRightMain.Controls.Add(this.DTPFormatTime);
            this.panelRightMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRightMain.Location = new System.Drawing.Point(235, 0);
            this.panelRightMain.Name = "panelRightMain";
            this.panelRightMain.Size = new System.Drawing.Size(1115, 664);
            this.panelRightMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelRightMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelRightMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelRightMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelRightMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelRightMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelRightMain.Style.GradientAngle = 90;
            this.panelRightMain.TabIndex = 2;
            this.panelRightMain.Text = "panelEx1";
            // 
            // DgvAttendance
            // 
            this.DgvAttendance.AddNewRow = false;
            this.DgvAttendance.AllowUserToAddRows = false;
            this.DgvAttendance.AllowUserToResizeColumns = false;
            this.DgvAttendance.AllowUserToResizeRows = false;
            this.DgvAttendance.AlphaNumericCols = new int[0];
            this.DgvAttendance.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvAttendance.CapsLockCols = new int[0];
            this.DgvAttendance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvAttendance.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColDgvAtnDate,
            this.ColDgvEmployee,
            this.ColDgvStatus,
            this.ColDgvAddMore,
            this.ColDgvRemarks,
            this.ColDgvShift,
            this.ColDgvDay,
            this.ColDgvBreakTime,
            this.ColDgvWorkTime,
            this.ColDgvLateComing,
            this.ColDgvEarlyGoing,
            this.ColDgvExcessTime,
            this.ColDgvCompanyID,
            this.ColDgvConsequenceID,
            this.ColDgvPolicyID,
            this.ColDgvConseqAmt,
            this.ColDgvValidFlag,
            this.ColDgvShiftTimeDisplay,
            this.ColDgvAllowedBreakTime,
            this.ColDgvHolidayFlag,
            this.ColDgvLeaveFlag,
            this.ColDgvAbsentTime,
            this.ColDgvShiftOrderNo,
            this.ColDgvLeaveInfo,
            this.ColDgvLeaveId,
            this.ColDgvLeaveType,
            this.ColDgvPunchID,
            this.ColDgvProjectID});
            this.DgvAttendance.DecimalCols = new int[0];
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvAttendance.DefaultCellStyle = dataGridViewCellStyle2;
            this.DgvAttendance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvAttendance.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvAttendance.HasSlNo = false;
            this.DgvAttendance.LastRowIndex = 0;
            this.DgvAttendance.Location = new System.Drawing.Point(0, 130);
            this.DgvAttendance.Name = "DgvAttendance";
            this.DgvAttendance.NegativeValueCols = new int[0];
            this.DgvAttendance.NumericCols = new int[0];
            this.DgvAttendance.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DgvAttendance.Size = new System.Drawing.Size(1115, 466);
            this.DgvAttendance.TabIndex = 1;
            this.DgvAttendance.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DgvAttendance_CellBeginEdit);
            this.DgvAttendance.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvAttendance_CellDoubleClick);
            this.DgvAttendance.Leave += new System.EventHandler(this.DgvAttendance_Leave);
            this.DgvAttendance.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DgvAttendance_CellValidating);
            this.DgvAttendance.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvAttendance_CellEndEdit);
            this.DgvAttendance.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvAttendance_CellClick);
            this.DgvAttendance.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DgvAttendance_EditingControlShowing);
            this.DgvAttendance.CurrentCellDirtyStateChanged += new System.EventHandler(this.DgvAttendance_CurrentCellDirtyStateChanged);
            this.DgvAttendance.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DgvAttendance_DataError);
            this.DgvAttendance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DgvAttendance_KeyDown);
            this.DgvAttendance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DgvAttendance_KeyPress);
            this.DgvAttendance.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DgvAttendance_RowHeaderMouseClick);
            this.DgvAttendance.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DgvAttendance_KeyUp);
            // 
            // pnlConnectionStatus
            // 
            this.pnlConnectionStatus.CanvasColor = System.Drawing.Color.Transparent;
            this.pnlConnectionStatus.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlConnectionStatus.Controls.Add(this.lblAnimation);
            this.pnlConnectionStatus.Controls.Add(this.pgrsBarConnection);
            this.pnlConnectionStatus.Location = new System.Drawing.Point(284, 222);
            this.pnlConnectionStatus.Name = "pnlConnectionStatus";
            this.pnlConnectionStatus.Size = new System.Drawing.Size(388, 67);
            this.pnlConnectionStatus.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlConnectionStatus.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlConnectionStatus.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlConnectionStatus.Style.Border = DevComponents.DotNetBar.eBorderType.DoubleLine;
            this.pnlConnectionStatus.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlConnectionStatus.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlConnectionStatus.Style.GradientAngle = 90;
            this.pnlConnectionStatus.TabIndex = 4;
            this.pnlConnectionStatus.Visible = false;
            // 
            // lblAnimation
            // 
            this.lblAnimation.AutoSize = true;
            // 
            // 
            // 
            this.lblAnimation.BackgroundStyle.Class = "";
            this.lblAnimation.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAnimation.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnimation.Location = new System.Drawing.Point(42, 12);
            this.lblAnimation.Name = "lblAnimation";
            this.lblAnimation.Size = new System.Drawing.Size(0, 0);
            this.lblAnimation.TabIndex = 1;
            // 
            // pgrsBarConnection
            // 
            // 
            // 
            // 
            this.pgrsBarConnection.BackgroundStyle.Class = "";
            this.pgrsBarConnection.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.pgrsBarConnection.Location = new System.Drawing.Point(8, 11);
            this.pgrsBarConnection.Name = "pgrsBarConnection";
            this.pgrsBarConnection.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Dot;
            this.pgrsBarConnection.Size = new System.Drawing.Size(31, 23);
            this.pgrsBarConnection.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.pgrsBarConnection.TabIndex = 0;
            this.pgrsBarConnection.Value = 40;
            // 
            // DgvPunchingData
            // 
            this.DgvPunchingData.AddNewRow = false;
            this.DgvPunchingData.AllowUserToAddRows = false;
            this.DgvPunchingData.AllowUserToDeleteRows = false;
            this.DgvPunchingData.AllowUserToOrderColumns = true;
            this.DgvPunchingData.AllowUserToResizeColumns = false;
            this.DgvPunchingData.AlphaNumericCols = new int[0];
            this.DgvPunchingData.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvPunchingData.CapsLockCols = new int[0];
            this.DgvPunchingData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvPunchingData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColDgvWorkID,
            this.ColDgvEmployeeName,
            this.ColDgvPunchingDate,
            this.ColDgvTimings,
            this.ColDgvInOutStatus,
            this.ColDgvPunchingResult});
            this.DgvPunchingData.DecimalCols = new int[0];
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvPunchingData.DefaultCellStyle = dataGridViewCellStyle3;
            this.DgvPunchingData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvPunchingData.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvPunchingData.HasSlNo = false;
            this.DgvPunchingData.LastRowIndex = 0;
            this.DgvPunchingData.Location = new System.Drawing.Point(0, 130);
            this.DgvPunchingData.Name = "DgvPunchingData";
            this.DgvPunchingData.NegativeValueCols = new int[0];
            this.DgvPunchingData.NumericCols = new int[0];
            this.DgvPunchingData.Size = new System.Drawing.Size(1115, 466);
            this.DgvPunchingData.TabIndex = 2;
            this.DgvPunchingData.Visible = false;
            this.DgvPunchingData.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DgvPunchingData_DataError);
            // 
            // ColDgvWorkID
            // 
            this.ColDgvWorkID.HeaderText = "WorkID";
            this.ColDgvWorkID.Name = "ColDgvWorkID";
            this.ColDgvWorkID.ReadOnly = true;
            // 
            // ColDgvEmployeeName
            // 
            this.ColDgvEmployeeName.HeaderText = "Employee";
            this.ColDgvEmployeeName.Name = "ColDgvEmployeeName";
            this.ColDgvEmployeeName.ReadOnly = true;
            // 
            // ColDgvPunchingDate
            // 
            this.ColDgvPunchingDate.HeaderText = "Date";
            this.ColDgvPunchingDate.Name = "ColDgvPunchingDate";
            this.ColDgvPunchingDate.ReadOnly = true;
            // 
            // ColDgvTimings
            // 
            this.ColDgvTimings.HeaderText = "Time";
            this.ColDgvTimings.Name = "ColDgvTimings";
            this.ColDgvTimings.ReadOnly = true;
            // 
            // ColDgvInOutStatus
            // 
            this.ColDgvInOutStatus.HeaderText = "Status";
            this.ColDgvInOutStatus.Name = "ColDgvInOutStatus";
            this.ColDgvInOutStatus.ReadOnly = true;
            // 
            // ColDgvPunchingResult
            // 
            this.ColDgvPunchingResult.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColDgvPunchingResult.HeaderText = "Result";
            this.ColDgvPunchingResult.Name = "ColDgvPunchingResult";
            this.ColDgvPunchingResult.ReadOnly = true;
            // 
            // panelBottom
            // 
            this.panelBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelBottom.Controls.Add(this.lblEarlyGoing);
            this.panelBottom.Controls.Add(this.lblLateComing);
            this.panelBottom.Controls.Add(this.lblPrgressPercentage);
            this.panelBottom.Controls.Add(this.lblProgressShow);
            this.panelBottom.Controls.Add(this.lblWorkTime);
            this.panelBottom.Controls.Add(this.lblAllowedBreak);
            this.panelBottom.Controls.Add(this.lblAllowedBreakTime);
            this.panelBottom.Controls.Add(this.lblShiftName);
            this.panelBottom.Controls.Add(this.lblBreak);
            this.panelBottom.Controls.Add(this.lblShift);
            this.panelBottom.Controls.Add(this.lblBreakTime);
            this.panelBottom.Controls.Add(this.lblExcessTime);
            this.panelBottom.Controls.Add(this.lblShortageTime);
            this.panelBottom.Controls.Add(this.lblWork);
            this.panelBottom.Controls.Add(this.lblShortage);
            this.panelBottom.Controls.Add(this.lblExcess);
            this.panelBottom.Controls.Add(this.lblConsequence);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 596);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1115, 49);
            this.panelBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelBottom.Style.GradientAngle = 90;
            this.panelBottom.TabIndex = 153;
            // 
            // lblEarlyGoing
            // 
            this.lblEarlyGoing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEarlyGoing.AutoSize = true;
            // 
            // 
            // 
            this.lblEarlyGoing.BackgroundStyle.Class = "";
            this.lblEarlyGoing.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblEarlyGoing.ForeColor = System.Drawing.Color.DarkRed;
            this.lblEarlyGoing.Location = new System.Drawing.Point(962, 27);
            this.lblEarlyGoing.Name = "lblEarlyGoing";
            this.lblEarlyGoing.Size = new System.Drawing.Size(146, 15);
            this.lblEarlyGoing.TabIndex = 1035;
            this.lblEarlyGoing.Text = "This person went early today!";
            // 
            // lblLateComing
            // 
            this.lblLateComing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLateComing.AutoSize = true;
            // 
            // 
            // 
            this.lblLateComing.BackgroundStyle.Class = "";
            this.lblLateComing.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblLateComing.ForeColor = System.Drawing.Color.DarkRed;
            this.lblLateComing.Location = new System.Drawing.Point(813, 27);
            this.lblLateComing.Name = "lblLateComing";
            this.lblLateComing.Size = new System.Drawing.Size(143, 15);
            this.lblLateComing.TabIndex = 1034;
            this.lblLateComing.Text = "This person came late today!";
            // 
            // lblPrgressPercentage
            // 
            this.lblPrgressPercentage.AutoSize = true;
            // 
            // 
            // 
            this.lblPrgressPercentage.BackgroundStyle.Class = "";
            this.lblPrgressPercentage.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPrgressPercentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrgressPercentage.Location = new System.Drawing.Point(154, 27);
            this.lblPrgressPercentage.Name = "lblPrgressPercentage";
            this.lblPrgressPercentage.Size = new System.Drawing.Size(137, 17);
            this.lblPrgressPercentage.TabIndex = 1033;
            this.lblPrgressPercentage.Text = "0 Records processed ";
            this.lblPrgressPercentage.Visible = false;
            // 
            // lblProgressShow
            // 
            this.lblProgressShow.AutoSize = true;
            // 
            // 
            // 
            this.lblProgressShow.BackgroundStyle.Class = "";
            this.lblProgressShow.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblProgressShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgressShow.Location = new System.Drawing.Point(3, 27);
            this.lblProgressShow.Name = "lblProgressShow";
            this.lblProgressShow.Size = new System.Drawing.Size(146, 17);
            this.lblProgressShow.TabIndex = 1032;
            this.lblProgressShow.Text = "Gathering Information ....";
            this.lblProgressShow.Visible = false;
            // 
            // lblWorkTime
            // 
            this.lblWorkTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWorkTime.AutoSize = true;
            // 
            // 
            // 
            this.lblWorkTime.BackgroundStyle.Class = "";
            this.lblWorkTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWorkTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWorkTime.Location = new System.Drawing.Point(814, 7);
            this.lblWorkTime.Name = "lblWorkTime";
            this.lblWorkTime.Size = new System.Drawing.Size(47, 15);
            this.lblWorkTime.TabIndex = 39;
            this.lblWorkTime.Text = "00:00:00";
            // 
            // lblAllowedBreak
            // 
            this.lblAllowedBreak.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAllowedBreak.AutoSize = true;
            // 
            // 
            // 
            this.lblAllowedBreak.BackgroundStyle.Class = "";
            this.lblAllowedBreak.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAllowedBreak.Location = new System.Drawing.Point(501, 7);
            this.lblAllowedBreak.Name = "lblAllowedBreak";
            this.lblAllowedBreak.Size = new System.Drawing.Size(73, 15);
            this.lblAllowedBreak.TabIndex = 34;
            this.lblAllowedBreak.Text = "Allowed Break";
            // 
            // lblAllowedBreakTime
            // 
            this.lblAllowedBreakTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAllowedBreakTime.AutoSize = true;
            // 
            // 
            // 
            this.lblAllowedBreakTime.BackgroundStyle.Class = "";
            this.lblAllowedBreakTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAllowedBreakTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllowedBreakTime.Location = new System.Drawing.Point(576, 7);
            this.lblAllowedBreakTime.Name = "lblAllowedBreakTime";
            this.lblAllowedBreakTime.Size = new System.Drawing.Size(47, 15);
            this.lblAllowedBreakTime.TabIndex = 35;
            this.lblAllowedBreakTime.Text = "00:00:00";
            // 
            // lblShiftName
            // 
            this.lblShiftName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblShiftName.AutoSize = true;
            // 
            // 
            // 
            this.lblShiftName.BackgroundStyle.Class = "";
            this.lblShiftName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblShiftName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShiftName.Location = new System.Drawing.Point(37, 5);
            this.lblShiftName.Name = "lblShiftName";
            this.lblShiftName.Size = new System.Drawing.Size(59, 15);
            this.lblShiftName.TabIndex = 33;
            this.lblShiftName.Text = "Shift Name";
            // 
            // lblBreak
            // 
            this.lblBreak.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBreak.AutoSize = true;
            // 
            // 
            // 
            this.lblBreak.BackgroundStyle.Class = "";
            this.lblBreak.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblBreak.Location = new System.Drawing.Point(632, 7);
            this.lblBreak.Name = "lblBreak";
            this.lblBreak.Size = new System.Drawing.Size(59, 15);
            this.lblBreak.TabIndex = 36;
            this.lblBreak.Text = "Break Time";
            // 
            // lblShift
            // 
            this.lblShift.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblShift.AutoSize = true;
            // 
            // 
            // 
            this.lblShift.BackgroundStyle.Class = "";
            this.lblShift.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblShift.Location = new System.Drawing.Point(3, 5);
            this.lblShift.Name = "lblShift";
            this.lblShift.Size = new System.Drawing.Size(25, 15);
            this.lblShift.TabIndex = 32;
            this.lblShift.Text = "Shift";
            // 
            // lblBreakTime
            // 
            this.lblBreakTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBreakTime.AutoSize = true;
            // 
            // 
            // 
            this.lblBreakTime.BackgroundStyle.Class = "";
            this.lblBreakTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblBreakTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBreakTime.Location = new System.Drawing.Point(693, 7);
            this.lblBreakTime.Name = "lblBreakTime";
            this.lblBreakTime.Size = new System.Drawing.Size(47, 15);
            this.lblBreakTime.TabIndex = 37;
            this.lblBreakTime.Text = "00:00:00";
            // 
            // lblExcessTime
            // 
            this.lblExcessTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblExcessTime.AutoSize = true;
            // 
            // 
            // 
            this.lblExcessTime.BackgroundStyle.Class = "";
            this.lblExcessTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblExcessTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExcessTime.Location = new System.Drawing.Point(1065, 7);
            this.lblExcessTime.Name = "lblExcessTime";
            this.lblExcessTime.Size = new System.Drawing.Size(47, 15);
            this.lblExcessTime.TabIndex = 43;
            this.lblExcessTime.Text = "00:00:00";
            // 
            // lblShortageTime
            // 
            this.lblShortageTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblShortageTime.AutoSize = true;
            // 
            // 
            // 
            this.lblShortageTime.BackgroundStyle.Class = "";
            this.lblShortageTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblShortageTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShortageTime.Location = new System.Drawing.Point(928, 7);
            this.lblShortageTime.Name = "lblShortageTime";
            this.lblShortageTime.Size = new System.Drawing.Size(47, 15);
            this.lblShortageTime.TabIndex = 41;
            this.lblShortageTime.Text = "00:00:00";
            // 
            // lblWork
            // 
            this.lblWork.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWork.AutoSize = true;
            // 
            // 
            // 
            this.lblWork.BackgroundStyle.Class = "";
            this.lblWork.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWork.Location = new System.Drawing.Point(756, 7);
            this.lblWork.Name = "lblWork";
            this.lblWork.Size = new System.Drawing.Size(56, 15);
            this.lblWork.TabIndex = 38;
            this.lblWork.Text = "Work Time";
            // 
            // lblShortage
            // 
            this.lblShortage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblShortage.AutoSize = true;
            // 
            // 
            // 
            this.lblShortage.BackgroundStyle.Class = "";
            this.lblShortage.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblShortage.Location = new System.Drawing.Point(875, 7);
            this.lblShortage.Name = "lblShortage";
            this.lblShortage.Size = new System.Drawing.Size(47, 15);
            this.lblShortage.TabIndex = 40;
            this.lblShortage.Text = "Shortage";
            // 
            // lblExcess
            // 
            this.lblExcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblExcess.AutoSize = true;
            // 
            // 
            // 
            this.lblExcess.BackgroundStyle.Class = "";
            this.lblExcess.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblExcess.Location = new System.Drawing.Point(994, 7);
            this.lblExcess.Name = "lblExcess";
            this.lblExcess.Size = new System.Drawing.Size(65, 15);
            this.lblExcess.TabIndex = 42;
            this.lblExcess.Text = "Excess Time";
            // 
            // lblConsequence
            // 
            this.lblConsequence.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsequence.AutoSize = true;
            // 
            // 
            // 
            this.lblConsequence.BackgroundStyle.Class = "";
            this.lblConsequence.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblConsequence.ForeColor = System.Drawing.Color.DarkRed;
            this.lblConsequence.Location = new System.Drawing.Point(3, 29);
            this.lblConsequence.Name = "lblConsequence";
            this.lblConsequence.Size = new System.Drawing.Size(73, 15);
            this.lblConsequence.TabIndex = 44;
            this.lblConsequence.Text = " Consequence";
            this.lblConsequence.Visible = false;
            // 
            // ExpandablepnlSearch
            // 
            this.ExpandablepnlSearch.CanvasColor = System.Drawing.SystemColors.Control;
            this.ExpandablepnlSearch.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ExpandablepnlSearch.Controls.Add(this.grpMain);
            this.ExpandablepnlSearch.Controls.Add(this.grpDaySummary);
            this.ExpandablepnlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.ExpandablepnlSearch.Location = new System.Drawing.Point(0, 25);
            this.ExpandablepnlSearch.Name = "ExpandablepnlSearch";
            this.ExpandablepnlSearch.Size = new System.Drawing.Size(1115, 105);
            this.ExpandablepnlSearch.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.ExpandablepnlSearch.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.ExpandablepnlSearch.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.ExpandablepnlSearch.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.ExpandablepnlSearch.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.ExpandablepnlSearch.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.ExpandablepnlSearch.Style.GradientAngle = 90;
            this.ExpandablepnlSearch.TabIndex = 4;
            this.ExpandablepnlSearch.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.ExpandablepnlSearch.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.ExpandablepnlSearch.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.ExpandablepnlSearch.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.ExpandablepnlSearch.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.ExpandablepnlSearch.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.ExpandablepnlSearch.TitleStyle.GradientAngle = 90;
            this.ExpandablepnlSearch.TitleText = "Search";
            this.ExpandablepnlSearch.ExpandedChanging += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.ExpandablepnlSearch_ExpandedChanging);
            // 
            // grpMain
            // 
            this.grpMain.Controls.Add(this.dtpFromDate);
            this.grpMain.Controls.Add(this.ChkLate);
            this.grpMain.Controls.Add(this.ChkEarly);
            this.grpMain.Controls.Add(this.ChkExtendedBreak);
            this.grpMain.Controls.Add(this.ChkOverTime);
            this.grpMain.Controls.Add(this.ChkAbsent);
            this.grpMain.Controls.Add(this.ChkLeave);
            this.grpMain.Controls.Add(this.ChkConsequence);
            this.grpMain.Controls.Add(this.ChkShortage);
            this.grpMain.Controls.Add(this.btnShow);
            this.grpMain.Controls.Add(this.rbtnMonthly);
            this.grpMain.Controls.Add(this.rbtnDaily);
            this.grpMain.Location = new System.Drawing.Point(7, 27);
            this.grpMain.Name = "grpMain";
            this.grpMain.Size = new System.Drawing.Size(642, 72);
            this.grpMain.TabIndex = 0;
            this.grpMain.TabStop = false;
            this.grpMain.Text = "General Info";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "MMM yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(6, 44);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(125, 20);
            this.dtpFromDate.TabIndex = 2;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            this.dtpFromDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpFromDate_KeyDown);
            // 
            // ChkLate
            // 
            this.ChkLate.AutoSize = true;
            this.ChkLate.Location = new System.Drawing.Point(178, 19);
            this.ChkLate.Name = "ChkLate";
            this.ChkLate.Size = new System.Drawing.Size(65, 17);
            this.ChkLate.TabIndex = 3;
            this.ChkLate.Text = "Late - In";
            this.ChkLate.UseVisualStyleBackColor = true;
            // 
            // ChkEarly
            // 
            this.ChkEarly.AutoSize = true;
            this.ChkEarly.Location = new System.Drawing.Point(178, 48);
            this.ChkEarly.Name = "ChkEarly";
            this.ChkEarly.Size = new System.Drawing.Size(75, 17);
            this.ChkEarly.TabIndex = 4;
            this.ChkEarly.Text = "Early - Out";
            this.ChkEarly.UseVisualStyleBackColor = true;
            // 
            // ChkExtendedBreak
            // 
            this.ChkExtendedBreak.AutoSize = true;
            this.ChkExtendedBreak.Location = new System.Drawing.Point(287, 19);
            this.ChkExtendedBreak.Name = "ChkExtendedBreak";
            this.ChkExtendedBreak.Size = new System.Drawing.Size(105, 17);
            this.ChkExtendedBreak.TabIndex = 5;
            this.ChkExtendedBreak.Text = "Break Exceeded";
            this.ChkExtendedBreak.UseVisualStyleBackColor = true;
            // 
            // ChkOverTime
            // 
            this.ChkOverTime.AutoSize = true;
            this.ChkOverTime.Location = new System.Drawing.Point(287, 48);
            this.ChkOverTime.Name = "ChkOverTime";
            this.ChkOverTime.Size = new System.Drawing.Size(75, 17);
            this.ChkOverTime.TabIndex = 6;
            this.ChkOverTime.Text = "Over Time";
            this.ChkOverTime.UseVisualStyleBackColor = true;
            // 
            // ChkAbsent
            // 
            this.ChkAbsent.AutoSize = true;
            this.ChkAbsent.Location = new System.Drawing.Point(396, 48);
            this.ChkAbsent.Name = "ChkAbsent";
            this.ChkAbsent.Size = new System.Drawing.Size(59, 17);
            this.ChkAbsent.TabIndex = 8;
            this.ChkAbsent.Text = "Absent";
            this.ChkAbsent.UseVisualStyleBackColor = true;
            // 
            // ChkLeave
            // 
            this.ChkLeave.AutoSize = true;
            this.ChkLeave.Location = new System.Drawing.Point(396, 19);
            this.ChkLeave.Name = "ChkLeave";
            this.ChkLeave.Size = new System.Drawing.Size(56, 17);
            this.ChkLeave.TabIndex = 7;
            this.ChkLeave.Text = "Leave";
            this.ChkLeave.UseVisualStyleBackColor = true;
            // 
            // ChkConsequence
            // 
            this.ChkConsequence.AutoSize = true;
            this.ChkConsequence.Location = new System.Drawing.Point(468, 48);
            this.ChkConsequence.Name = "ChkConsequence";
            this.ChkConsequence.Size = new System.Drawing.Size(92, 17);
            this.ChkConsequence.TabIndex = 10;
            this.ChkConsequence.Text = "Consequence";
            this.ChkConsequence.UseVisualStyleBackColor = true;
            // 
            // ChkShortage
            // 
            this.ChkShortage.AutoSize = true;
            this.ChkShortage.Location = new System.Drawing.Point(468, 19);
            this.ChkShortage.Name = "ChkShortage";
            this.ChkShortage.Size = new System.Drawing.Size(69, 17);
            this.ChkShortage.TabIndex = 9;
            this.ChkShortage.Text = "Shortage";
            this.ChkShortage.UseVisualStyleBackColor = true;
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(575, 45);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(61, 23);
            this.btnShow.TabIndex = 11;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // rbtnMonthly
            // 
            this.rbtnMonthly.AutoSize = true;
            this.rbtnMonthly.Checked = true;
            this.rbtnMonthly.Location = new System.Drawing.Point(6, 19);
            this.rbtnMonthly.Name = "rbtnMonthly";
            this.rbtnMonthly.Size = new System.Drawing.Size(62, 17);
            this.rbtnMonthly.TabIndex = 0;
            this.rbtnMonthly.TabStop = true;
            this.rbtnMonthly.Text = "Monthly";
            this.rbtnMonthly.UseVisualStyleBackColor = true;
            this.rbtnMonthly.CheckedChanged += new System.EventHandler(this.rbtnMonthly_CheckedChanged);
            // 
            // rbtnDaily
            // 
            this.rbtnDaily.AutoSize = true;
            this.rbtnDaily.Location = new System.Drawing.Point(83, 19);
            this.rbtnDaily.Name = "rbtnDaily";
            this.rbtnDaily.Size = new System.Drawing.Size(48, 17);
            this.rbtnDaily.TabIndex = 1;
            this.rbtnDaily.Text = "Daily";
            this.rbtnDaily.UseVisualStyleBackColor = true;
            // 
            // grpDaySummary
            // 
            this.grpDaySummary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpDaySummary.AutoSize = true;
            this.grpDaySummary.Controls.Add(this.lblShortageCountDispaly);
            this.grpDaySummary.Controls.Add(this.lblShortageCount);
            this.grpDaySummary.Controls.Add(this.lblLeave);
            this.grpDaySummary.Controls.Add(this.lblLeaveCountDispaly);
            this.grpDaySummary.Controls.Add(this.lblAbsentDispaly);
            this.grpDaySummary.Controls.Add(this.lblPresent);
            this.grpDaySummary.Controls.Add(this.lblPresentDisplay);
            this.grpDaySummary.Controls.Add(this.lblAbsent);
            this.grpDaySummary.Location = new System.Drawing.Point(884, 28);
            this.grpDaySummary.Name = "grpDaySummary";
            this.grpDaySummary.Size = new System.Drawing.Size(225, 71);
            this.grpDaySummary.TabIndex = 1;
            this.grpDaySummary.TabStop = false;
            this.grpDaySummary.Text = "Month Summary";
            // 
            // lblShortageCountDispaly
            // 
            this.lblShortageCountDispaly.AutoSize = true;
            // 
            // 
            // 
            this.lblShortageCountDispaly.BackgroundStyle.Class = "";
            this.lblShortageCountDispaly.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblShortageCountDispaly.Location = new System.Drawing.Point(204, 37);
            this.lblShortageCountDispaly.Name = "lblShortageCountDispaly";
            this.lblShortageCountDispaly.Size = new System.Drawing.Size(9, 15);
            this.lblShortageCountDispaly.TabIndex = 7;
            this.lblShortageCountDispaly.Text = "0";
            // 
            // lblShortageCount
            // 
            this.lblShortageCount.AutoSize = true;
            // 
            // 
            // 
            this.lblShortageCount.BackgroundStyle.Class = "";
            this.lblShortageCount.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblShortageCount.Location = new System.Drawing.Point(138, 37);
            this.lblShortageCount.Name = "lblShortageCount";
            this.lblShortageCount.Size = new System.Drawing.Size(47, 15);
            this.lblShortageCount.TabIndex = 6;
            this.lblShortageCount.Text = "Shortage";
            // 
            // lblLeave
            // 
            this.lblLeave.AutoSize = true;
            // 
            // 
            // 
            this.lblLeave.BackgroundStyle.Class = "";
            this.lblLeave.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblLeave.Location = new System.Drawing.Point(138, 17);
            this.lblLeave.Name = "lblLeave";
            this.lblLeave.Size = new System.Drawing.Size(32, 15);
            this.lblLeave.TabIndex = 2;
            this.lblLeave.Text = "Leave";
            // 
            // lblLeaveCountDispaly
            // 
            this.lblLeaveCountDispaly.AutoSize = true;
            // 
            // 
            // 
            this.lblLeaveCountDispaly.BackgroundStyle.Class = "";
            this.lblLeaveCountDispaly.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblLeaveCountDispaly.Location = new System.Drawing.Point(204, 17);
            this.lblLeaveCountDispaly.Name = "lblLeaveCountDispaly";
            this.lblLeaveCountDispaly.Size = new System.Drawing.Size(9, 15);
            this.lblLeaveCountDispaly.TabIndex = 3;
            this.lblLeaveCountDispaly.Text = "0";
            // 
            // lblAbsentDispaly
            // 
            this.lblAbsentDispaly.AutoSize = true;
            // 
            // 
            // 
            this.lblAbsentDispaly.BackgroundStyle.Class = "";
            this.lblAbsentDispaly.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAbsentDispaly.Location = new System.Drawing.Point(78, 37);
            this.lblAbsentDispaly.Name = "lblAbsentDispaly";
            this.lblAbsentDispaly.Size = new System.Drawing.Size(9, 15);
            this.lblAbsentDispaly.TabIndex = 5;
            this.lblAbsentDispaly.Text = "0";
            // 
            // lblPresent
            // 
            this.lblPresent.AutoSize = true;
            // 
            // 
            // 
            this.lblPresent.BackgroundStyle.Class = "";
            this.lblPresent.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPresent.Location = new System.Drawing.Point(18, 17);
            this.lblPresent.Name = "lblPresent";
            this.lblPresent.Size = new System.Drawing.Size(40, 15);
            this.lblPresent.TabIndex = 0;
            this.lblPresent.Text = "Present";
            // 
            // lblPresentDisplay
            // 
            this.lblPresentDisplay.AutoSize = true;
            // 
            // 
            // 
            this.lblPresentDisplay.BackgroundStyle.Class = "";
            this.lblPresentDisplay.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPresentDisplay.Location = new System.Drawing.Point(78, 17);
            this.lblPresentDisplay.Name = "lblPresentDisplay";
            this.lblPresentDisplay.Size = new System.Drawing.Size(9, 15);
            this.lblPresentDisplay.TabIndex = 1;
            this.lblPresentDisplay.Text = "0";
            // 
            // lblAbsent
            // 
            this.lblAbsent.AutoSize = true;
            // 
            // 
            // 
            this.lblAbsent.BackgroundStyle.Class = "";
            this.lblAbsent.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAbsent.Location = new System.Drawing.Point(18, 37);
            this.lblAbsent.Name = "lblAbsent";
            this.lblAbsent.Size = new System.Drawing.Size(37, 15);
            this.lblAbsent.TabIndex = 4;
            this.lblAbsent.Text = "Absent";
            // 
            // dockSite7
            // 
            this.dockSite7.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite7.Controls.Add(this.BarFunctions);
            this.dockSite7.Controls.Add(this.BarView);
            this.dockSite7.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite7.Location = new System.Drawing.Point(0, 0);
            this.dockSite7.Name = "dockSite7";
            this.dockSite7.Size = new System.Drawing.Size(1115, 25);
            this.dockSite7.TabIndex = 6;
            this.dockSite7.TabStop = false;
            // 
            // BarFunctions
            // 
            this.BarFunctions.AccessibleDescription = "DotNetBar Bar (BarFunctions)";
            this.BarFunctions.AccessibleName = "DotNetBar Bar";
            this.BarFunctions.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.BarFunctions.CanUndock = false;
            this.BarFunctions.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.BarFunctions.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.BarFunctions.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnDevices,
            this.btnTemplates,
            this.btnMapping,
            this.btnLive,
            this.btnOptions,
            this.CustomizeItem2});
            this.BarFunctions.Location = new System.Drawing.Point(0, 0);
            this.BarFunctions.Name = "BarFunctions";
            this.BarFunctions.Size = new System.Drawing.Size(287, 25);
            this.BarFunctions.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.BarFunctions.TabIndex = 0;
            this.BarFunctions.TabStop = false;
            this.BarFunctions.Text = "Bar1";
            // 
            // btnDevices
            // 
            this.btnDevices.Enabled = false;
            this.btnDevices.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleIfRecentlyUsed;
            this.btnDevices.Name = "btnDevices";
            this.btnDevices.Text = "Connect To";
            this.btnDevices.Tooltip = "Connect To Device";
            this.btnDevices.Visible = false;
            // 
            // btnTemplates
            // 
            this.btnTemplates.BeginGroup = true;
            this.btnTemplates.FixedSize = new System.Drawing.Size(100, 10);
            this.btnTemplates.Name = "btnTemplates";
            this.btnTemplates.Text = "Template";
            // 
            // btnMapping
            // 
            this.btnMapping.BeginGroup = true;
            this.btnMapping.Image = global::MyBooksERP.Properties.Resources.Attendance_Mapping;
            this.btnMapping.Name = "btnMapping";
            this.btnMapping.Text = "&Mapping";
            this.btnMapping.Tooltip = "Attendance Mapping";
            this.btnMapping.Click += new System.EventHandler(this.btnMapping_Click);
            // 
            // btnLive
            // 
            this.btnLive.BeginGroup = true;
            this.btnLive.Image = ((System.Drawing.Image)(resources.GetObject("btnLive.Image")));
            this.btnLive.Name = "btnLive";
            this.btnLive.Text = "Live";
            this.btnLive.Tooltip = "Live";
            this.btnLive.Visible = false;
            this.btnLive.Click += new System.EventHandler(this.btnLive_Click);
            // 
            // btnOptions
            // 
            this.btnOptions.BeginGroup = true;
            this.btnOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnOptions.Image")));
            this.btnOptions.Name = "btnOptions";
            this.btnOptions.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnFetchData,
            this.btnViewValidTimings,
            this.btnInvalidTimings,
            this.btnViewAllTimings,
            this.btnViewFirstandLastTimings,
            this.btnViewDetailInfo,
            this.btnviewLogsFromHandpunch});
            this.btnOptions.Text = "Options";
            this.btnOptions.Tooltip = "Filter Options";
            // 
            // btnFetchData
            // 
            this.btnFetchData.Name = "btnFetchData";
            this.btnFetchData.Text = "FetchData";
            this.btnFetchData.Visible = false;
            this.btnFetchData.Click += new System.EventHandler(this.btnFetchData_Click);
            // 
            // btnViewValidTimings
            // 
            this.btnViewValidTimings.Name = "btnViewValidTimings";
            this.btnViewValidTimings.Text = "View Valid Timings";
            this.btnViewValidTimings.Click += new System.EventHandler(this.btnViewValidTimings_Click);
            // 
            // btnInvalidTimings
            // 
            this.btnInvalidTimings.Name = "btnInvalidTimings";
            this.btnInvalidTimings.Text = "View Invalid Timings";
            this.btnInvalidTimings.Click += new System.EventHandler(this.btnInvalidTimings_Click);
            // 
            // btnViewAllTimings
            // 
            this.btnViewAllTimings.Name = "btnViewAllTimings";
            this.btnViewAllTimings.Text = "All Timings";
            this.btnViewAllTimings.Click += new System.EventHandler(this.btnViewAllTimings_Click);
            // 
            // btnViewFirstandLastTimings
            // 
            this.btnViewFirstandLastTimings.Name = "btnViewFirstandLastTimings";
            this.btnViewFirstandLastTimings.Text = "First and Last Timings";
            this.btnViewFirstandLastTimings.Click += new System.EventHandler(this.btnViewFirstandLastTimings_Click);
            // 
            // btnViewDetailInfo
            // 
            this.btnViewDetailInfo.Name = "btnViewDetailInfo";
            this.btnViewDetailInfo.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnViewDay,
            this.btnViewWorkTime,
            this.btnViewBreakTime,
            this.btnViewExcessTime,
            this.btnViewLateComing,
            this.btnViewEarlyGoing});
            this.btnViewDetailInfo.Text = "View Detail info";
            // 
            // btnViewDay
            // 
            this.btnViewDay.Checked = true;
            this.btnViewDay.Name = "btnViewDay";
            this.btnViewDay.Text = "Day";
            this.btnViewDay.Click += new System.EventHandler(this.btnViewDay_Click);
            // 
            // btnViewWorkTime
            // 
            this.btnViewWorkTime.Checked = true;
            this.btnViewWorkTime.Name = "btnViewWorkTime";
            this.btnViewWorkTime.Text = "WorkTime";
            this.btnViewWorkTime.Click += new System.EventHandler(this.btnViewWorkTime_Click);
            // 
            // btnViewBreakTime
            // 
            this.btnViewBreakTime.Checked = true;
            this.btnViewBreakTime.Name = "btnViewBreakTime";
            this.btnViewBreakTime.Text = "Break Time";
            this.btnViewBreakTime.Click += new System.EventHandler(this.btnViewBreakTime_Click);
            // 
            // btnViewExcessTime
            // 
            this.btnViewExcessTime.Checked = true;
            this.btnViewExcessTime.Name = "btnViewExcessTime";
            this.btnViewExcessTime.Text = "Excess Time";
            this.btnViewExcessTime.Click += new System.EventHandler(this.btnViewExcessTime_Click);
            // 
            // btnViewLateComing
            // 
            this.btnViewLateComing.Checked = true;
            this.btnViewLateComing.Name = "btnViewLateComing";
            this.btnViewLateComing.Text = "Late";
            this.btnViewLateComing.Click += new System.EventHandler(this.btnViewLateComing_Click);
            // 
            // btnViewEarlyGoing
            // 
            this.btnViewEarlyGoing.Checked = true;
            this.btnViewEarlyGoing.Name = "btnViewEarlyGoing";
            this.btnViewEarlyGoing.Text = "Early";
            this.btnViewEarlyGoing.Click += new System.EventHandler(this.btnViewEarlyGoing_Click);
            // 
            // btnviewLogsFromHandpunch
            // 
            this.btnviewLogsFromHandpunch.Name = "btnviewLogsFromHandpunch";
            this.btnviewLogsFromHandpunch.Text = "View Device Logs";
            this.btnviewLogsFromHandpunch.Visible = false;
            this.btnviewLogsFromHandpunch.Click += new System.EventHandler(this.btnviewLogsFromHandpunch_Click);
            // 
            // CustomizeItem2
            // 
            this.CustomizeItem2.CustomizeItemVisible = false;
            this.CustomizeItem2.Name = "CustomizeItem2";
            this.CustomizeItem2.Text = "&Add or Remove Buttons";
            this.CustomizeItem2.Tooltip = "Bar Options";
            // 
            // BarView
            // 
            this.BarView.AccessibleDescription = "DotNetBar Bar (BarView)";
            this.BarView.AccessibleName = "DotNetBar Bar";
            this.BarView.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.BarView.CanUndock = false;
            this.BarView.DockOffset = 147;
            this.BarView.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.BarView.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.BarView.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSave,
            this.btnDelete,
            this.btnClear,
            this.btnAutoFill,
            this.btnExportToExcel,
            this.btnLeaveOpening,
            this.btnHelp,
            this.CustomizeItem1});
            this.BarView.Location = new System.Drawing.Point(289, 0);
            this.BarView.Name = "BarView";
            this.BarView.Size = new System.Drawing.Size(195, 25);
            this.BarView.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.BarView.TabIndex = 2;
            this.BarView.TabStop = false;
            this.BarView.Text = "Bar2";
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Name = "btnSave";
            this.btnSave.Text = "&Save";
            this.btnSave.Tooltip = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Text = "Delete";
            this.btnDelete.Tooltip = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.btnClear.Name = "btnClear";
            this.btnClear.Text = "&Clear";
            this.btnClear.Tooltip = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnAutoFill
            // 
            this.btnAutoFill.BeginGroup = true;
            this.btnAutoFill.Image = ((System.Drawing.Image)(resources.GetObject("btnAutoFill.Image")));
            this.btnAutoFill.Name = "btnAutoFill";
            this.btnAutoFill.Text = "&AutoFill";
            this.btnAutoFill.Tooltip = "Auto Fill";
            this.btnAutoFill.Click += new System.EventHandler(this.btnAutoFill_Click);
            // 
            // btnExportToExcel
            // 
            this.btnExportToExcel.Image = global::MyBooksERP.Properties.Resources.ExportToExcel;
            this.btnExportToExcel.Name = "btnExportToExcel";
            this.btnExportToExcel.Text = "Excel";
            this.btnExportToExcel.Tooltip = "Excel";
            this.btnExportToExcel.Click += new System.EventHandler(this.btnExportToExcel_Click);
            // 
            // btnLeaveOpening
            // 
            this.btnLeaveOpening.Image = global::MyBooksERP.Properties.Resources.Leave1;
            this.btnLeaveOpening.Name = "btnLeaveOpening";
            this.btnLeaveOpening.Text = "&Leave Opening";
            this.btnLeaveOpening.Tooltip = "Leave Opening";
            this.btnLeaveOpening.Visible = false;
            this.btnLeaveOpening.Click += new System.EventHandler(this.btnLeaveOpening_Click);
            // 
            // btnHelp
            // 
            this.btnHelp.BeginGroup = true;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.btnHelp.Text = "Help";
            this.btnHelp.Tooltip = "Help";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // CustomizeItem1
            // 
            this.CustomizeItem1.CustomizeItemVisible = false;
            this.CustomizeItem1.Name = "CustomizeItem1";
            this.CustomizeItem1.Text = "&Add or Remove Buttons";
            this.CustomizeItem1.Tooltip = "Bar Options";
            // 
            // barStatusBottom
            // 
            this.barStatusBottom.AccessibleDescription = "bar1 (bar1)";
            this.barStatusBottom.AccessibleName = "bar1";
            this.barStatusBottom.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.barStatusBottom.AntiAlias = true;
            this.barStatusBottom.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.barStatusBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barStatusBottom.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.barStatusBottom.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.lblToolStripStatus,
            this.lblAttendnaceStatus,
            this.barProgressBarAttendance,
            this.lblEmployeeSelected,
            this.lblEmployeeSelectedValue});
            this.barStatusBottom.Location = new System.Drawing.Point(0, 645);
            this.barStatusBottom.Name = "barStatusBottom";
            this.barStatusBottom.Size = new System.Drawing.Size(1115, 19);
            this.barStatusBottom.Stretch = true;
            this.barStatusBottom.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barStatusBottom.TabIndex = 1084;
            this.barStatusBottom.TabStop = false;
            this.barStatusBottom.Text = "bar1";
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "Status : ";
            this.labelItem1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblToolStripStatus
            // 
            this.lblToolStripStatus.Name = "lblToolStripStatus";
            // 
            // lblAttendnaceStatus
            // 
            this.lblAttendnaceStatus.Name = "lblAttendnaceStatus";
            this.lblAttendnaceStatus.Text = "AttendanceStatus";
            // 
            // barProgressBarAttendance
            // 
            // 
            // 
            // 
            this.barProgressBarAttendance.BackStyle.Class = "";
            this.barProgressBarAttendance.BackStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.barProgressBarAttendance.ChunkGradientAngle = 0F;
            this.barProgressBarAttendance.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.barProgressBarAttendance.Name = "barProgressBarAttendance";
            this.barProgressBarAttendance.RecentlyUsed = false;
            // 
            // lblEmployeeSelected
            // 
            this.lblEmployeeSelected.Name = "lblEmployeeSelected";
            this.lblEmployeeSelected.PaddingLeft = 250;
            this.lblEmployeeSelected.Text = "Employee :";
            // 
            // lblEmployeeSelectedValue
            // 
            this.lblEmployeeSelectedValue.Name = "lblEmployeeSelectedValue";
            // 
            // DTPFormatTime
            // 
            this.DTPFormatTime.CustomFormat = "HH:mm";
            this.DTPFormatTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTPFormatTime.Location = new System.Drawing.Point(174, 234);
            this.DTPFormatTime.Name = "DTPFormatTime";
            this.DTPFormatTime.ShowUpDown = true;
            this.DTPFormatTime.Size = new System.Drawing.Size(68, 20);
            this.DTPFormatTime.TabIndex = 3;
            this.DTPFormatTime.TabStop = false;
            // 
            // tmrReadLogs
            // 
            this.tmrReadLogs.Interval = 800;
            // 
            // tmrClearlabels
            // 
            this.tmrClearlabels.Tick += new System.EventHandler(this.tmrClearlabels_Tick);
            // 
            // dotNetBarManager1
            // 
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.dotNetBarManager1.BottomDockSite = this.dockSite4;
            this.dotNetBarManager1.EnableFullSizeDock = false;
            this.dotNetBarManager1.LeftDockSite = this.dockSite1;
            this.dotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.dotNetBarManager1.ParentForm = this;
            this.dotNetBarManager1.RightDockSite = this.dockSite2;
            this.dotNetBarManager1.ShowCustomizeContextMenu = false;
            this.dotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.dotNetBarManager1.ToolbarBottomDockSite = this.dockSite8;
            this.dotNetBarManager1.ToolbarLeftDockSite = this.dockSite5;
            this.dotNetBarManager1.ToolbarRightDockSite = this.dockSite6;
            this.dotNetBarManager1.ToolbarTopDockSite = this.dockSite7;
            this.dotNetBarManager1.TopDockSite = this.dockSite3;
            // 
            // dockSite4
            // 
            this.dockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite4.Location = new System.Drawing.Point(0, 664);
            this.dockSite4.Name = "dockSite4";
            this.dockSite4.Size = new System.Drawing.Size(1350, 0);
            this.dockSite4.TabIndex = 6;
            this.dockSite4.TabStop = false;
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(235, 0);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 664);
            this.dockSite1.TabIndex = 3;
            this.dockSite1.TabStop = false;
            // 
            // dockSite2
            // 
            this.dockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite2.Location = new System.Drawing.Point(1350, 0);
            this.dockSite2.Name = "dockSite2";
            this.dockSite2.Size = new System.Drawing.Size(0, 664);
            this.dockSite2.TabIndex = 4;
            this.dockSite2.TabStop = false;
            // 
            // dockSite8
            // 
            this.dockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite8.Location = new System.Drawing.Point(0, 664);
            this.dockSite8.Name = "dockSite8";
            this.dockSite8.Size = new System.Drawing.Size(1350, 0);
            this.dockSite8.TabIndex = 9;
            this.dockSite8.TabStop = false;
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(0, 0);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 664);
            this.dockSite5.TabIndex = 7;
            this.dockSite5.TabStop = false;
            // 
            // dockSite6
            // 
            this.dockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite6.Location = new System.Drawing.Point(1350, 0);
            this.dockSite6.Name = "dockSite6";
            this.dockSite6.Size = new System.Drawing.Size(0, 664);
            this.dockSite6.TabIndex = 8;
            this.dockSite6.TabStop = false;
            // 
            // dockSite3
            // 
            this.dockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite3.Location = new System.Drawing.Point(0, 0);
            this.dockSite3.Name = "dockSite3";
            this.dockSite3.Size = new System.Drawing.Size(1350, 0);
            this.dockSite3.TabIndex = 5;
            this.dockSite3.TabStop = false;
            // 
            // buttonItem1
            // 
            this.buttonItem1.BeginGroup = true;
            this.buttonItem1.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem1.Image")));
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.Text = "AutoFill";
            this.buttonItem1.Tooltip = "Auto Fill";
            // 
            // ColDgvAtnDate
            // 
            this.ColDgvAtnDate.FillWeight = 80F;
            this.ColDgvAtnDate.Frozen = true;
            this.ColDgvAtnDate.HeaderText = "Date";
            this.ColDgvAtnDate.Name = "ColDgvAtnDate";
            this.ColDgvAtnDate.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ColDgvAtnDate.Width = 80;
            // 
            // ColDgvEmployee
            // 
            this.ColDgvEmployee.FillWeight = 250F;
            this.ColDgvEmployee.Frozen = true;
            this.ColDgvEmployee.HeaderText = "Employee";
            this.ColDgvEmployee.Name = "ColDgvEmployee";
            this.ColDgvEmployee.ReadOnly = true;
            this.ColDgvEmployee.Visible = false;
            this.ColDgvEmployee.Width = 250;
            // 
            // ColDgvStatus
            // 
            this.ColDgvStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ColDgvStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ColDgvStatus.DisplayMember = "Text";
            this.ColDgvStatus.DropDownHeight = 106;
            this.ColDgvStatus.DropDownWidth = 121;
            this.ColDgvStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ColDgvStatus.Frozen = true;
            this.ColDgvStatus.HeaderText = "Status";
            this.ColDgvStatus.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ColDgvStatus.IntegralHeight = false;
            this.ColDgvStatus.ItemHeight = 15;
            this.ColDgvStatus.Name = "ColDgvStatus";
            this.ColDgvStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ColDgvStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColDgvStatus.WatermarkText = "Status";
            // 
            // ColDgvAddMore
            // 
            this.ColDgvAddMore.FillWeight = 35F;
            this.ColDgvAddMore.HeaderText = "";
            this.ColDgvAddMore.Name = "ColDgvAddMore";
            this.ColDgvAddMore.Text = null;
            this.ColDgvAddMore.Width = 35;
            // 
            // ColDgvRemarks
            // 
            this.ColDgvRemarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColDgvRemarks.FillWeight = 150F;
            this.ColDgvRemarks.HeaderText = "Remarks";
            this.ColDgvRemarks.MaxInputLength = 190;
            this.ColDgvRemarks.MinimumWidth = 150;
            this.ColDgvRemarks.Name = "ColDgvRemarks";
            this.ColDgvRemarks.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColDgvShift
            // 
            this.ColDgvShift.HeaderText = "Shift";
            this.ColDgvShift.Name = "ColDgvShift";
            this.ColDgvShift.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // ColDgvDay
            // 
            this.ColDgvDay.HeaderText = "Day";
            this.ColDgvDay.Name = "ColDgvDay";
            this.ColDgvDay.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ColDgvDay.Visible = false;
            // 
            // ColDgvBreakTime
            // 
            this.ColDgvBreakTime.HeaderText = "BreakTime";
            this.ColDgvBreakTime.Name = "ColDgvBreakTime";
            this.ColDgvBreakTime.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ColDgvBreakTime.Visible = false;
            // 
            // ColDgvWorkTime
            // 
            this.ColDgvWorkTime.HeaderText = "WorkTime";
            this.ColDgvWorkTime.Name = "ColDgvWorkTime";
            this.ColDgvWorkTime.Visible = false;
            // 
            // ColDgvLateComing
            // 
            this.ColDgvLateComing.HeaderText = "LateComing";
            this.ColDgvLateComing.Name = "ColDgvLateComing";
            this.ColDgvLateComing.Visible = false;
            // 
            // ColDgvEarlyGoing
            // 
            this.ColDgvEarlyGoing.HeaderText = "EarlyGoing";
            this.ColDgvEarlyGoing.Name = "ColDgvEarlyGoing";
            this.ColDgvEarlyGoing.Visible = false;
            // 
            // ColDgvExcessTime
            // 
            this.ColDgvExcessTime.HeaderText = "ExcessTime";
            this.ColDgvExcessTime.Name = "ColDgvExcessTime";
            this.ColDgvExcessTime.Visible = false;
            // 
            // ColDgvCompanyID
            // 
            this.ColDgvCompanyID.HeaderText = "CompanyID";
            this.ColDgvCompanyID.Name = "ColDgvCompanyID";
            this.ColDgvCompanyID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColDgvCompanyID.Visible = false;
            // 
            // ColDgvConsequenceID
            // 
            this.ColDgvConsequenceID.HeaderText = "ConsequenceID";
            this.ColDgvConsequenceID.Name = "ColDgvConsequenceID";
            this.ColDgvConsequenceID.Visible = false;
            // 
            // ColDgvPolicyID
            // 
            this.ColDgvPolicyID.HeaderText = "PolicyID";
            this.ColDgvPolicyID.Name = "ColDgvPolicyID";
            this.ColDgvPolicyID.Visible = false;
            // 
            // ColDgvConseqAmt
            // 
            this.ColDgvConseqAmt.HeaderText = "ConseqAmt";
            this.ColDgvConseqAmt.Name = "ColDgvConseqAmt";
            this.ColDgvConseqAmt.Visible = false;
            // 
            // ColDgvValidFlag
            // 
            this.ColDgvValidFlag.HeaderText = "ValidFlag";
            this.ColDgvValidFlag.Name = "ColDgvValidFlag";
            this.ColDgvValidFlag.Visible = false;
            // 
            // ColDgvShiftTimeDisplay
            // 
            this.ColDgvShiftTimeDisplay.HeaderText = "ShiftTimeDisplay";
            this.ColDgvShiftTimeDisplay.Name = "ColDgvShiftTimeDisplay";
            this.ColDgvShiftTimeDisplay.Visible = false;
            // 
            // ColDgvAllowedBreakTime
            // 
            this.ColDgvAllowedBreakTime.HeaderText = "AllowedBreakTime";
            this.ColDgvAllowedBreakTime.Name = "ColDgvAllowedBreakTime";
            this.ColDgvAllowedBreakTime.Visible = false;
            // 
            // ColDgvHolidayFlag
            // 
            this.ColDgvHolidayFlag.HeaderText = "HolidayFlag";
            this.ColDgvHolidayFlag.Name = "ColDgvHolidayFlag";
            this.ColDgvHolidayFlag.Visible = false;
            // 
            // ColDgvLeaveFlag
            // 
            this.ColDgvLeaveFlag.HeaderText = "LeaveFlag";
            this.ColDgvLeaveFlag.Name = "ColDgvLeaveFlag";
            this.ColDgvLeaveFlag.Visible = false;
            // 
            // ColDgvAbsentTime
            // 
            this.ColDgvAbsentTime.HeaderText = "AbsentTime";
            this.ColDgvAbsentTime.Name = "ColDgvAbsentTime";
            this.ColDgvAbsentTime.Visible = false;
            // 
            // ColDgvShiftOrderNo
            // 
            this.ColDgvShiftOrderNo.HeaderText = "ShiftOrderNo";
            this.ColDgvShiftOrderNo.Name = "ColDgvShiftOrderNo";
            this.ColDgvShiftOrderNo.Visible = false;
            // 
            // ColDgvLeaveInfo
            // 
            this.ColDgvLeaveInfo.HeaderText = "LeaveInfo";
            this.ColDgvLeaveInfo.Name = "ColDgvLeaveInfo";
            this.ColDgvLeaveInfo.Visible = false;
            // 
            // ColDgvLeaveId
            // 
            this.ColDgvLeaveId.HeaderText = "LeaveId";
            this.ColDgvLeaveId.Name = "ColDgvLeaveId";
            this.ColDgvLeaveId.Visible = false;
            // 
            // ColDgvLeaveType
            // 
            this.ColDgvLeaveType.HeaderText = "LeaveType";
            this.ColDgvLeaveType.Name = "ColDgvLeaveType";
            this.ColDgvLeaveType.Visible = false;
            // 
            // ColDgvPunchID
            // 
            this.ColDgvPunchID.HeaderText = "PunchID";
            this.ColDgvPunchID.Name = "ColDgvPunchID";
            this.ColDgvPunchID.Visible = false;
            // 
            // ColDgvProjectID
            // 
            this.ColDgvProjectID.HeaderText = "ProjectID";
            this.ColDgvProjectID.Name = "ColDgvProjectID";
            this.ColDgvProjectID.Visible = false;
            // 
            // FrmAttendance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 664);
            this.Controls.Add(this.dockSite2);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.panelRightMain);
            this.Controls.Add(this.expandableSplitterLeft);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.dockSite3);
            this.Controls.Add(this.dockSite4);
            this.Controls.Add(this.dockSite5);
            this.Controls.Add(this.dockSite6);
            this.Controls.Add(this.dockSite8);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmAttendance";
            this.Text = "Attendance";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmAttendance_Load);
            this.Shown += new System.EventHandler(this.FrmAttendance_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmAttendance_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmAttendance_KeyDown);
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvEmployee)).EndInit();
            this.ExpPanelEmployee.ResumeLayout(false);
            this.grpSearchLeft.ResumeLayout(false);
            this.panelRightMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendance)).EndInit();
            this.pnlConnectionStatus.ResumeLayout(false);
            this.pnlConnectionStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvPunchingData)).EndInit();
            this.panelBottom.ResumeLayout(false);
            this.panelBottom.PerformLayout();
            this.ExpandablepnlSearch.ResumeLayout(false);
            this.ExpandablepnlSearch.PerformLayout();
            this.grpMain.ResumeLayout(false);
            this.grpMain.PerformLayout();
            this.grpDaySummary.ResumeLayout(false);
            this.grpDaySummary.PerformLayout();
            this.dockSite7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BarFunctions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barStatusBottom)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx PanelLeft;
        private DevComponents.DotNetBar.ExpandablePanel ExpPanelEmployee;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitterLeft;
        private DevComponents.DotNetBar.PanelEx panelRightMain;
        private DevComponents.DotNetBar.ExpandablePanel ExpandablepnlSearch;
        internal DevComponents.DotNetBar.Bar BarFunctions;
        internal DevComponents.DotNetBar.ButtonItem btnDevices;
        internal DevComponents.DotNetBar.ButtonItem btnMapping;
        internal DevComponents.DotNetBar.ButtonItem btnOptions;
        internal DevComponents.DotNetBar.ButtonItem btnViewValidTimings;
        internal DevComponents.DotNetBar.ButtonItem btnInvalidTimings;
        internal DevComponents.DotNetBar.ButtonItem btnViewAllTimings;
        internal DevComponents.DotNetBar.ButtonItem btnViewFirstandLastTimings;
        internal DevComponents.DotNetBar.ButtonItem btnViewDetailInfo;
        internal DevComponents.DotNetBar.ButtonItem btnViewDay;
        internal DevComponents.DotNetBar.ButtonItem btnViewWorkTime;
        internal DevComponents.DotNetBar.ButtonItem btnViewBreakTime;
        internal DevComponents.DotNetBar.ButtonItem btnViewExcessTime;
        internal DevComponents.DotNetBar.ButtonItem btnViewLateComing;
        internal DevComponents.DotNetBar.ButtonItem btnViewEarlyGoing;
        internal DevComponents.DotNetBar.CustomizeItem CustomizeItem2;
        internal DevComponents.DotNetBar.Bar BarView;
        internal DevComponents.DotNetBar.ButtonItem btnSave;
        internal DevComponents.DotNetBar.ButtonItem btnDelete;
        internal DevComponents.DotNetBar.ButtonItem btnClear;
        internal DevComponents.DotNetBar.ButtonItem btnAutoFill;
        internal DevComponents.DotNetBar.CustomizeItem CustomizeItem1;
        private DevComponents.DotNetBar.PanelEx panelBottom;
        private DevComponents.DotNetBar.Bar barStatusBottom;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.LabelItem lblToolStripStatus;
        private System.Windows.Forms.GroupBox grpDaySummary;
        internal DevComponents.DotNetBar.LabelX lblWorkTime;
        internal DevComponents.DotNetBar.LabelX lblAllowedBreak;
        internal DevComponents.DotNetBar.LabelX lblConsequence;
        internal DevComponents.DotNetBar.LabelX lblAllowedBreakTime;
        internal DevComponents.DotNetBar.LabelX lblShiftName;
        internal DevComponents.DotNetBar.LabelX lblBreak;
        internal DevComponents.DotNetBar.LabelX lblShift;
        internal DevComponents.DotNetBar.LabelX lblBreakTime;
        internal DevComponents.DotNetBar.LabelX lblExcessTime;
        internal DevComponents.DotNetBar.LabelX lblShortageTime;
        internal DevComponents.DotNetBar.LabelX lblWork;
        internal DevComponents.DotNetBar.LabelX lblShortage;
        internal DevComponents.DotNetBar.LabelX lblExcess;
        internal DevComponents.DotNetBar.LabelX lblShortageCountDispaly;
        internal DevComponents.DotNetBar.LabelX lblShortageCount;
        internal DevComponents.DotNetBar.LabelX lblLeave;
        internal DevComponents.DotNetBar.LabelX lblLeaveCountDispaly;
        internal DevComponents.DotNetBar.LabelX lblAbsentDispaly;
        internal DevComponents.DotNetBar.LabelX lblPresent;
        internal DevComponents.DotNetBar.LabelX lblPresentDisplay;
        internal DevComponents.DotNetBar.LabelX lblAbsent;
        internal System.Windows.Forms.RadioButton rbtnMonthly;
        internal System.Windows.Forms.RadioButton rbtnDaily;
        internal DevComponents.DotNetBar.LabelX lblShowBy;
        internal DevComponents.DotNetBar.Controls.ComboTree cboEmpWorkstatus;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx CboCompany;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx CboEmpFilterType;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx CboEmpFilter;
        private DevComponents.DotNetBar.LabelItem lblAttendnaceStatus;
        private DevComponents.DotNetBar.ProgressBarItem barProgressBarAttendance;
        internal System.Windows.Forms.DateTimePicker DTPFormatTime;
        private DevComponents.DotNetBar.PanelEx pnlConnectionStatus;
        private DevComponents.DotNetBar.Controls.CircularProgress pgrsBarConnection;
        private DevComponents.DotNetBar.LabelX lblAnimation;

        private DevComponents.DotNetBar.ButtonItem btnFetchData;
        private System.Windows.Forms.Timer tmrReadLogs;
        private System.Windows.Forms.Timer tmrClearlabels;
        internal System.Windows.Forms.ImageList ImageListEmployee;
        internal System.Windows.Forms.GroupBox grpSearchLeft;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        internal DevComponents.DotNetBar.ButtonX btnSearch;
        private System.Windows.Forms.GroupBox grpMain;
        private DevComponents.DotNetBar.ButtonItem btnviewLogsFromHandpunch;
        private DevComponents.DotNetBar.LabelX lblPrgressPercentage;
        private DevComponents.DotNetBar.LabelX lblProgressShow;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.CheckBox ChkLate;
        private System.Windows.Forms.CheckBox ChkEarly;
        private System.Windows.Forms.CheckBox ChkExtendedBreak;
        private System.Windows.Forms.CheckBox ChkOverTime;
        private System.Windows.Forms.CheckBox ChkAbsent;
        private System.Windows.Forms.CheckBox ChkLeave;
        private System.Windows.Forms.CheckBox ChkConsequence;
        private System.Windows.Forms.CheckBox ChkShortage;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private DevComponents.DotNetBar.DotNetBarManager dotNetBarManager1;
        private DevComponents.DotNetBar.DockSite dockSite4;
        private DevComponents.DotNetBar.DockSite dockSite1;
        private DevComponents.DotNetBar.DockSite dockSite2;
        private DevComponents.DotNetBar.DockSite dockSite3;
        private DevComponents.DotNetBar.DockSite dockSite5;
        private DevComponents.DotNetBar.DockSite dockSite6;
        private DevComponents.DotNetBar.DockSite dockSite8;
        private DevComponents.DotNetBar.DockSite dockSite7;
        private ClsDataGirdViewX DgvEmployee;
        private System.Windows.Forms.DataGridViewImageColumn Column1;
        private ClsDataGirdViewX DgvPunchingData;
        private ClsDataGirdViewX DgvAttendance;
        private DevComponents.DotNetBar.ButtonItem btnTemplates;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvWorkID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvEmployeeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvPunchingDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvTimings;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvInOutStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvPunchingResult;
        private DevComponents.DotNetBar.LabelX lblEarlyGoing;
        private DevComponents.DotNetBar.LabelX lblLateComing;
        private DevComponents.DotNetBar.ButtonItem btnLeaveOpening;
        internal DevComponents.DotNetBar.ButtonItem btnLive;
        private DevComponents.DotNetBar.LabelItem lblEmployeeSelected;
        private DevComponents.DotNetBar.LabelItem lblEmployeeSelectedValue;
        internal DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonItem btnHelp;
        internal DevComponents.DotNetBar.ButtonItem btnExportToExcel;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvAtnDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvEmployee;
        private DevComponents.DotNetBar.Controls.DataGridViewComboBoxExColumn ColDgvStatus;
        private DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn ColDgvAddMore;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvRemarks;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvShift;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvDay;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvBreakTime;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvWorkTime;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvLateComing;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvEarlyGoing;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvExcessTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvCompanyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvConsequenceID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvPolicyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvConseqAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvValidFlag;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvShiftTimeDisplay;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvAllowedBreakTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvHolidayFlag;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvLeaveFlag;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvAbsentTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvShiftOrderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvLeaveInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvLeaveId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvLeaveType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvPunchID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvProjectID;
        
    }
}