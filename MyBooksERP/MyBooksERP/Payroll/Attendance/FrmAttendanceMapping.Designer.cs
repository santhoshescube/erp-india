﻿namespace MyBooksERP//.GUI.FingerTech
{
    partial class FrmAttendanceMapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAttendanceMapping));
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnTemplate = new System.Windows.Forms.ToolStripDropDownButton();
            this.BtnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.CboEmployee = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.CboPunch = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.btnOk = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ToolStripStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.CboDevice = new System.Windows.Forms.ComboBox();
            this.lblDevice = new System.Windows.Forms.Label();
            this.BtnFillEmployees = new System.Windows.Forms.Button();
            this.lblHeader = new System.Windows.Forms.Label();
            this.grpMain = new System.Windows.Forms.GroupBox();
            this.lblType = new System.Windows.Forms.Label();
            this.rdbTemplates = new System.Windows.Forms.RadioButton();
            this.rdbDevice = new System.Windows.Forms.RadioButton();
            this.DgvAtnMapping = new DemoClsDataGridview.ClsDataGirdView();
            this.ColEmpName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEmpNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPunchID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColDeviceNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCompanyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errproMapping = new System.Windows.Forms.ErrorProvider(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStrip1.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            this.grpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAtnMapping)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errproMapping)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnTemplate,
            this.toolStripSeparator3,
            this.BtnSaveItem,
            this.BindingNavigatorSeparator1,
            this.btnDelete,
            this.toolStripSeparator2,
            this.BtnHelp});
            this.ToolStrip1.Location = new System.Drawing.Point(0, 0);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(506, 25);
            this.ToolStrip1.TabIndex = 66;
            this.ToolStrip1.Text = "ToolStrip1";
            // 
            // btnTemplate
            // 
            this.btnTemplate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTemplate.Name = "btnTemplate";
            this.btnTemplate.Size = new System.Drawing.Size(70, 22);
            this.btnTemplate.Text = "Template";
            // 
            // BtnSaveItem
            // 
            this.BtnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BtnSaveItem.Image")));
            this.BtnSaveItem.Name = "BtnSaveItem";
            this.BtnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BtnSaveItem.ToolTipText = "Save";
            this.BtnSaveItem.Click += new System.EventHandler(this.BtnSaveItem_Click);
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(23, 22);
            this.btnDelete.ToolTipText = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            // 
            // CboEmployee
            // 
            this.CboEmployee.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.CboEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CboEmployee.HeaderText = "Employee Name";
            this.CboEmployee.Name = "CboEmployee";
            this.CboEmployee.ReadOnly = true;
            this.CboEmployee.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CboEmployee.Width = 250;
            // 
            // CboPunch
            // 
            this.CboPunch.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CboPunch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CboPunch.HeaderText = "PunchID";
            this.CboPunch.Name = "CboPunch";
            this.CboPunch.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CboPunch.ToolTipText = "Please select Punch ID";
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.dataGridViewComboBoxColumn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn1.HeaderText = "Employee Name";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.ReadOnly = true;
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.Width = 250;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn2.HeaderText = "PunchID";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.ToolTipText = "Please select Punch ID";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(342, 391);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(423, 391);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 1;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.ToolStripStatus});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 417);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(506, 22);
            this.StatusStrip1.TabIndex = 66;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(39, 17);
            this.StatusLabel.Text = "Status";
            // 
            // ToolStripStatus
            // 
            this.ToolStripStatus.Name = "ToolStripStatus";
            this.ToolStripStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // CboDevice
            // 
            this.CboDevice.BackColor = System.Drawing.SystemColors.Info;
            this.CboDevice.FormattingEnabled = true;
            this.CboDevice.Location = new System.Drawing.Point(70, 42);
            this.CboDevice.Name = "CboDevice";
            this.CboDevice.Size = new System.Drawing.Size(149, 21);
            this.CboDevice.TabIndex = 0;
            this.CboDevice.SelectionChangeCommitted += new System.EventHandler(this.CboDevice_SelectionChangeCommitted);
            this.CboDevice.SelectedIndexChanged += new System.EventHandler(this.CboDevice_SelectedIndexChanged);
            this.CboDevice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboDevice_KeyDown);
            // 
            // lblDevice
            // 
            this.lblDevice.AutoSize = true;
            this.lblDevice.Location = new System.Drawing.Point(8, 45);
            this.lblDevice.Name = "lblDevice";
            this.lblDevice.Size = new System.Drawing.Size(41, 13);
            this.lblDevice.TabIndex = 1031;
            this.lblDevice.Text = "Device";
            // 
            // BtnFillEmployees
            // 
            this.BtnFillEmployees.Location = new System.Drawing.Point(225, 40);
            this.BtnFillEmployees.Name = "BtnFillEmployees";
            this.BtnFillEmployees.Size = new System.Drawing.Size(105, 23);
            this.BtnFillEmployees.TabIndex = 1;
            this.BtnFillEmployees.Text = "Fill Employees";
            this.BtnFillEmployees.UseVisualStyleBackColor = true;
            this.BtnFillEmployees.Click += new System.EventHandler(this.BtnFillEmployees_Click);
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblHeader.Location = new System.Drawing.Point(5, 1);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(84, 13);
            this.lblHeader.TabIndex = 1034;
            this.lblHeader.Text = "Mapping  Info";
            // 
            // grpMain
            // 
            this.grpMain.Controls.Add(this.lblType);
            this.grpMain.Controls.Add(this.rdbTemplates);
            this.grpMain.Controls.Add(this.rdbDevice);
            this.grpMain.Controls.Add(this.lblDevice);
            this.grpMain.Controls.Add(this.BtnFillEmployees);
            this.grpMain.Controls.Add(this.CboDevice);
            this.grpMain.Controls.Add(this.lblHeader);
            this.grpMain.Controls.Add(this.DgvAtnMapping);
            this.grpMain.Location = new System.Drawing.Point(8, 23);
            this.grpMain.Name = "grpMain";
            this.grpMain.Size = new System.Drawing.Size(490, 365);
            this.grpMain.TabIndex = 0;
            this.grpMain.TabStop = false;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(8, 19);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(31, 13);
            this.lblType.TabIndex = 1037;
            this.lblType.Text = "Type";
            // 
            // rdbTemplates
            // 
            this.rdbTemplates.AutoSize = true;
            this.rdbTemplates.Checked = true;
            this.rdbTemplates.Location = new System.Drawing.Point(70, 19);
            this.rdbTemplates.Name = "rdbTemplates";
            this.rdbTemplates.Size = new System.Drawing.Size(74, 17);
            this.rdbTemplates.TabIndex = 1036;
            this.rdbTemplates.TabStop = true;
            this.rdbTemplates.Text = "Templates";
            this.rdbTemplates.UseVisualStyleBackColor = true;
            this.rdbTemplates.CheckedChanged += new System.EventHandler(this.rdbTemplates_CheckedChanged);
            // 
            // rdbDevice
            // 
            this.rdbDevice.AutoSize = true;
            this.rdbDevice.Location = new System.Drawing.Point(150, 19);
            this.rdbDevice.Name = "rdbDevice";
            this.rdbDevice.Size = new System.Drawing.Size(59, 17);
            this.rdbDevice.TabIndex = 1035;
            this.rdbDevice.Text = "Device";
            this.rdbDevice.UseVisualStyleBackColor = true;
            this.rdbDevice.Visible = false;
            this.rdbDevice.CheckedChanged += new System.EventHandler(this.rdbDevice_CheckedChanged);
            // 
            // DgvAtnMapping
            // 
            this.DgvAtnMapping.AddNewRow = true;
            this.DgvAtnMapping.AllowUserToAddRows = false;
            this.DgvAtnMapping.AlphaNumericCols = new int[0];
            this.DgvAtnMapping.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvAtnMapping.CapsLockCols = new int[0];
            this.DgvAtnMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvAtnMapping.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColEmpName,
            this.ColEmpNumber,
            this.ColPunchID,
            this.ColDeviceNo,
            this.ColCompanyID});
            this.DgvAtnMapping.DecimalCols = new int[0];
            this.DgvAtnMapping.HasSlNo = false;
            this.DgvAtnMapping.LastRowIndex = 0;
            this.DgvAtnMapping.Location = new System.Drawing.Point(6, 69);
            this.DgvAtnMapping.Name = "DgvAtnMapping";
            this.DgvAtnMapping.NegativeValueCols = new int[0];
            this.DgvAtnMapping.NumericCols = new int[0];
            this.DgvAtnMapping.RowHeadersWidth = 35;
            this.DgvAtnMapping.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DgvAtnMapping.Size = new System.Drawing.Size(478, 287);
            this.DgvAtnMapping.TabIndex = 2;
            this.DgvAtnMapping.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvAtnMapping_CellValueChanged);
            this.DgvAtnMapping.CurrentCellDirtyStateChanged += new System.EventHandler(this.DgvAtnMapping_CurrentCellDirtyStateChanged);
            this.DgvAtnMapping.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DgvAtnMapping_DataError);
            this.DgvAtnMapping.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DgvAtnMapping_KeyDown);
            // 
            // ColEmpName
            // 
            this.ColEmpName.HeaderText = "Name";
            this.ColEmpName.Name = "ColEmpName";
            this.ColEmpName.ReadOnly = true;
            this.ColEmpName.Width = 199;
            // 
            // ColEmpNumber
            // 
            this.ColEmpNumber.HeaderText = "Employee Number";
            this.ColEmpNumber.Name = "ColEmpNumber";
            this.ColEmpNumber.ReadOnly = true;
            this.ColEmpNumber.Width = 120;
            // 
            // ColPunchID
            // 
            this.ColPunchID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColPunchID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.ColPunchID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ColPunchID.HeaderText = "PunchID";
            this.ColPunchID.Name = "ColPunchID";
            this.ColPunchID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ColDeviceNo
            // 
            this.ColDeviceNo.HeaderText = "Device No";
            this.ColDeviceNo.MaxInputLength = 15;
            this.ColDeviceNo.Name = "ColDeviceNo";
            this.ColDeviceNo.Visible = false;
            // 
            // ColCompanyID
            // 
            this.ColCompanyID.HeaderText = "CompanyID";
            this.ColCompanyID.MaxInputLength = 5;
            this.ColCompanyID.Name = "ColCompanyID";
            this.ColCompanyID.Visible = false;
            // 
            // errproMapping
            // 
            this.errproMapping.ContainerControl = this;
            this.errproMapping.RightToLeft = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "EmployeeNumber";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 200;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Equipment No";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.ToolTipText = "Please enter Equipment No";
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Device No";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "CompanyID";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "EmployeeNumber";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.HeaderText = "Equipment No";
            this.Column2.MaxInputLength = 10;
            this.Column2.Name = "Column2";
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.ToolTipText = "Please enter Equipment No";
            this.Column2.Visible = false;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 6);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // FrmAttendanceMapping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 439);
            this.Controls.Add(this.grpMain);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.ToolStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAttendanceMapping";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Attendance Mapping";
            this.Load += new System.EventHandler(this.FrmAttendanceMapping_Load);
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.grpMain.ResumeLayout(false);
            this.grpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAtnMapping)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errproMapping)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolStrip ToolStrip1;
        internal System.Windows.Forms.ToolStripButton BtnSaveItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.DataGridViewComboBoxColumn CboEmployee;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        internal System.Windows.Forms.DataGridViewComboBoxColumn CboPunch;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        internal System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        internal System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DemoClsDataGridview.ClsDataGirdView DgvAtnMapping;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.ComboBox CboDevice;
        private System.Windows.Forms.Label lblDevice;
        private System.Windows.Forms.Button BtnFillEmployees;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        internal System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.GroupBox grpMain;
        private System.Windows.Forms.ErrorProvider errproMapping;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.RadioButton rdbTemplates;
        private System.Windows.Forms.RadioButton rdbDevice;
        internal System.Windows.Forms.ToolStripDropDownButton btnTemplate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEmpName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEmpNumber;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColPunchID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDeviceNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCompanyID;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}