﻿namespace MyBooksERP
{
    partial class FrmOtTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOtTemplate));
            this.dgvOtTemplate = new System.Windows.Forms.DataGridView();
            this.ColOrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColStartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColOtTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpMain = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOtTemplate)).BeginInit();
            this.grpMain.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvOtTemplate
            // 
            this.dgvOtTemplate.AllowUserToResizeColumns = false;
            this.dgvOtTemplate.AllowUserToResizeRows = false;
            this.dgvOtTemplate.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvOtTemplate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOtTemplate.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColOrderNo,
            this.ColStartTime,
            this.ColEndTime,
            this.ColOtTime});
            this.dgvOtTemplate.Location = new System.Drawing.Point(3, 9);
            this.dgvOtTemplate.Name = "dgvOtTemplate";
            this.dgvOtTemplate.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvOtTemplate.Size = new System.Drawing.Size(401, 191);
            this.dgvOtTemplate.TabIndex = 0;
            this.dgvOtTemplate.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvOtTemplate_EditingControlShowing);
            this.dgvOtTemplate.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvOtTemplate_CurrentCellDirtyStateChanged);
            this.dgvOtTemplate.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvOtTemplate_DataError);
            // 
            // ColOrderNo
            // 
            this.ColOrderNo.HeaderText = "OrderNo";
            this.ColOrderNo.MaxInputLength = 2;
            this.ColOrderNo.Name = "ColOrderNo";
            this.ColOrderNo.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColOrderNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColOrderNo.Visible = false;
            // 
            // ColStartTime
            // 
            this.ColStartTime.HeaderText = "StartTime(Min)";
            this.ColStartTime.MaxInputLength = 4;
            this.ColStartTime.Name = "ColStartTime";
            this.ColStartTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColStartTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColStartTime.Width = 125;
            // 
            // ColEndTime
            // 
            this.ColEndTime.HeaderText = "EndTime(Min)";
            this.ColEndTime.MaxInputLength = 4;
            this.ColEndTime.Name = "ColEndTime";
            this.ColEndTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColEndTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColEndTime.Width = 125;
            // 
            // ColOtTime
            // 
            this.ColOtTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColOtTime.HeaderText = "Ot Time(Min)";
            this.ColOtTime.MaxInputLength = 4;
            this.ColOtTime.Name = "ColOtTime";
            this.ColOtTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColOtTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // grpMain
            // 
            this.grpMain.Controls.Add(this.dgvOtTemplate);
            this.grpMain.Location = new System.Drawing.Point(3, 1);
            this.grpMain.Name = "grpMain";
            this.grpMain.Size = new System.Drawing.Size(407, 204);
            this.grpMain.TabIndex = 1;
            this.grpMain.TabStop = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 231);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(411, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(42, 17);
            this.lblStatus.Text = "Status:";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(254, 205);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(332, 205);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "OrderNo";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 2;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "StartTime(Min)";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 4;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 125;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "EndTime(Min)";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 4;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 125;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.HeaderText = "Ot Time(Min)";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 4;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FrmOtTemplate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 253);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.grpMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmOtTemplate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "OT Template";
            this.Shown += new System.EventHandler(this.FrmOtTemplate_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmOtTemplate_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOtTemplate)).EndInit();
            this.grpMain.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvOtTemplate;
        private System.Windows.Forms.GroupBox grpMain;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColOrderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColStartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEndTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColOtTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
    }
}