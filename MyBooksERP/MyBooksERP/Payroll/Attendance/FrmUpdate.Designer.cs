﻿namespace MyBooksERP//.GUI.HandPunch
{
    partial class FrmUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUpdate));
            this.TxtFoodBreak = new System.Windows.Forms.Label();
            this.LblShift = new System.Windows.Forms.Label();
            this.LblShiftDesc = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.cmsRecord = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.InsertARecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RemoveARecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TLSPStatusLbl = new System.Windows.Forms.ToolStripStatusLabel();
            this.TLSPStatusLblDisplaying = new System.Windows.Forms.ToolStripStatusLabel();
            this.Label7 = new System.Windows.Forms.Label();
            this.lblDateDisplay = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.TxtBreakTime = new System.Windows.Forms.TextBox();
            this.LblEarlyGoing = new System.Windows.Forms.Label();
            this.TxtOverTime = new System.Windows.Forms.TextBox();
            this.lblLateComing = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.txtWorkTime = new System.Windows.Forms.TextBox();
            this.PicBoxBreakTime = new System.Windows.Forms.Button();
            this.PicBoxWorkTime = new System.Windows.Forms.Button();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.DgvTimeScheuleGraph = new System.Windows.Forms.DataGridView();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.DTPFormatTime = new System.Windows.Forms.DateTimePicker();
            this.dgvUpdate = new DemoClsDataGridview.ClsDataGirdView();
            this.ColSlNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEntryExit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPunching = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColWorkTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColBreakTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tmrClearlabels = new System.Windows.Forms.Timer(this.components);
            this.cmsRecord.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTimeScheuleGraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpdate)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtFoodBreak
            // 
            this.TxtFoodBreak.AutoSize = true;
            this.TxtFoodBreak.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFoodBreak.Location = new System.Drawing.Point(117, 35);
            this.TxtFoodBreak.Name = "TxtFoodBreak";
            this.TxtFoodBreak.Size = new System.Drawing.Size(38, 13);
            this.TxtFoodBreak.TabIndex = 1063;
            this.TxtFoodBreak.Text = "Label6";
            // 
            // LblShift
            // 
            this.LblShift.AutoSize = true;
            this.LblShift.Location = new System.Drawing.Point(12, 13);
            this.LblShift.Name = "LblShift";
            this.LblShift.Size = new System.Drawing.Size(28, 13);
            this.LblShift.TabIndex = 1062;
            this.LblShift.Text = "Shift";
            // 
            // LblShiftDesc
            // 
            this.LblShiftDesc.AutoSize = true;
            this.LblShiftDesc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblShiftDesc.Location = new System.Drawing.Point(116, 13);
            this.LblShiftDesc.Name = "LblShiftDesc";
            this.LblShiftDesc.Size = new System.Drawing.Size(44, 13);
            this.LblShiftDesc.TabIndex = 1061;
            this.LblShiftDesc.Text = "Label6";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(13, 35);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(101, 13);
            this.Label4.TabIndex = 1060;
            this.Label4.Text = "Allowed Break Time";
            // 
            // cmsRecord
            // 
            this.cmsRecord.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.InsertARecordToolStripMenuItem,
            this.RemoveARecordToolStripMenuItem});
            this.cmsRecord.Name = "cmsRecord";
            this.cmsRecord.Size = new System.Drawing.Size(167, 48);
            // 
            // InsertARecordToolStripMenuItem
            // 
            this.InsertARecordToolStripMenuItem.Image = global::MyBooksERP.Properties.Resources.Add;
            this.InsertARecordToolStripMenuItem.Name = "InsertARecordToolStripMenuItem";
            this.InsertARecordToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.InsertARecordToolStripMenuItem.Text = "Insert a Record";
            this.InsertARecordToolStripMenuItem.Click += new System.EventHandler(this.InsertARecordToolStripMenuItem_Click);
            // 
            // RemoveARecordToolStripMenuItem
            // 
            this.RemoveARecordToolStripMenuItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.RemoveARecordToolStripMenuItem.Name = "RemoveARecordToolStripMenuItem";
            this.RemoveARecordToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.RemoveARecordToolStripMenuItem.Text = "Remove a Record";
            this.RemoveARecordToolStripMenuItem.Click += new System.EventHandler(this.RemoveARecordToolStripMenuItem_Click);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel1,
            this.TLSPStatusLbl,
            this.TLSPStatusLblDisplaying});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 506);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(480, 22);
            this.StatusStrip1.TabIndex = 1065;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // ToolStripStatusLabel1
            // 
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // TLSPStatusLbl
            // 
            this.TLSPStatusLbl.BackColor = System.Drawing.SystemColors.Control;
            this.TLSPStatusLbl.Name = "TLSPStatusLbl";
            this.TLSPStatusLbl.Size = new System.Drawing.Size(42, 17);
            this.TLSPStatusLbl.Text = "Status:";
            // 
            // TLSPStatusLblDisplaying
            // 
            this.TLSPStatusLblDisplaying.BackColor = System.Drawing.SystemColors.Control;
            this.TLSPStatusLblDisplaying.Name = "TLSPStatusLblDisplaying";
            this.TLSPStatusLblDisplaying.Size = new System.Drawing.Size(13, 17);
            this.TLSPStatusLblDisplaying.Text = "  ";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.Location = new System.Drawing.Point(12, 55);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(140, 13);
            this.Label7.TabIndex = 1067;
            this.Label7.Text = "Time Attendance as on ";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblDateDisplay
            // 
            this.lblDateDisplay.AutoSize = true;
            this.lblDateDisplay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateDisplay.Location = new System.Drawing.Point(149, 55);
            this.lblDateDisplay.Name = "lblDateDisplay";
            this.lblDateDisplay.Size = new System.Drawing.Size(44, 13);
            this.lblDateDisplay.TabIndex = 1066;
            this.lblDateDisplay.Text = "Label6";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(239, 446);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(69, 13);
            this.Label9.TabIndex = 1070;
            this.Label9.Text = "Early Status :";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(239, 425);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(67, 13);
            this.Label8.TabIndex = 1072;
            this.Label8.Text = "Late Status :";
            // 
            // TxtBreakTime
            // 
            this.TxtBreakTime.Location = new System.Drawing.Point(117, 446);
            this.TxtBreakTime.Name = "TxtBreakTime";
            this.TxtBreakTime.ReadOnly = true;
            this.TxtBreakTime.Size = new System.Drawing.Size(100, 20);
            this.TxtBreakTime.TabIndex = 1069;
            // 
            // LblEarlyGoing
            // 
            this.LblEarlyGoing.AutoSize = true;
            this.LblEarlyGoing.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEarlyGoing.Location = new System.Drawing.Point(304, 446);
            this.LblEarlyGoing.Name = "LblEarlyGoing";
            this.LblEarlyGoing.Size = new System.Drawing.Size(174, 13);
            this.LblEarlyGoing.TabIndex = 1077;
            this.LblEarlyGoing.Tag = "0";
            this.LblEarlyGoing.Text = "This person went early today!";
            // 
            // TxtOverTime
            // 
            this.TxtOverTime.Location = new System.Drawing.Point(117, 469);
            this.TxtOverTime.Name = "TxtOverTime";
            this.TxtOverTime.ReadOnly = true;
            this.TxtOverTime.Size = new System.Drawing.Size(100, 20);
            this.TxtOverTime.TabIndex = 1075;
            // 
            // lblLateComing
            // 
            this.lblLateComing.AutoSize = true;
            this.lblLateComing.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLateComing.Location = new System.Drawing.Point(304, 425);
            this.lblLateComing.Name = "lblLateComing";
            this.lblLateComing.Size = new System.Drawing.Size(170, 13);
            this.lblLateComing.TabIndex = 1076;
            this.lblLateComing.Tag = "0";
            this.lblLateComing.Text = "This person came late today!";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(12, 469);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(56, 13);
            this.Label5.TabIndex = 1074;
            this.Label5.Text = "Over Time";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(12, 447);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(61, 13);
            this.Label3.TabIndex = 1073;
            this.Label3.Text = "Break Time";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(12, 423);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(59, 13);
            this.Label2.TabIndex = 1071;
            this.Label2.Text = "Work Time";
            // 
            // txtWorkTime
            // 
            this.txtWorkTime.Location = new System.Drawing.Point(117, 422);
            this.txtWorkTime.Name = "txtWorkTime";
            this.txtWorkTime.ReadOnly = true;
            this.txtWorkTime.Size = new System.Drawing.Size(100, 20);
            this.txtWorkTime.TabIndex = 1068;
            // 
            // PicBoxBreakTime
            // 
            this.PicBoxBreakTime.BackColor = System.Drawing.Color.Red;
            this.PicBoxBreakTime.Location = new System.Drawing.Point(186, 387);
            this.PicBoxBreakTime.Name = "PicBoxBreakTime";
            this.PicBoxBreakTime.Size = new System.Drawing.Size(26, 23);
            this.PicBoxBreakTime.TabIndex = 1081;
            this.PicBoxBreakTime.UseVisualStyleBackColor = false;
            this.PicBoxBreakTime.Click += new System.EventHandler(this.PicBoxBreakTime_Click);
            // 
            // PicBoxWorkTime
            // 
            this.PicBoxWorkTime.BackColor = System.Drawing.Color.Green;
            this.PicBoxWorkTime.Location = new System.Drawing.Point(81, 387);
            this.PicBoxWorkTime.Name = "PicBoxWorkTime";
            this.PicBoxWorkTime.Size = new System.Drawing.Size(26, 23);
            this.PicBoxWorkTime.TabIndex = 1080;
            this.PicBoxWorkTime.UseVisualStyleBackColor = false;
            this.PicBoxWorkTime.Click += new System.EventHandler(this.PicBoxWorkTime_Click);
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(115, 391);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(71, 13);
            this.Label6.TabIndex = 1079;
            this.Label6.Text = "Break Time";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(12, 391);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(68, 13);
            this.Label1.TabIndex = 1078;
            this.Label1.Text = "Work Time";
            // 
            // DgvTimeScheuleGraph
            // 
            this.DgvTimeScheuleGraph.AllowUserToDeleteRows = false;
            this.DgvTimeScheuleGraph.AllowUserToResizeColumns = false;
            this.DgvTimeScheuleGraph.AllowUserToResizeRows = false;
            this.DgvTimeScheuleGraph.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.DgvTimeScheuleGraph.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DgvTimeScheuleGraph.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.DgvTimeScheuleGraph.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvTimeScheuleGraph.ColumnHeadersVisible = false;
            this.DgvTimeScheuleGraph.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DgvTimeScheuleGraph.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DgvTimeScheuleGraph.Location = new System.Drawing.Point(6, 341);
            this.DgvTimeScheuleGraph.MultiSelect = false;
            this.DgvTimeScheuleGraph.Name = "DgvTimeScheuleGraph";
            this.DgvTimeScheuleGraph.ReadOnly = true;
            this.DgvTimeScheuleGraph.RowHeadersVisible = false;
            this.DgvTimeScheuleGraph.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DgvTimeScheuleGraph.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.DgvTimeScheuleGraph.Size = new System.Drawing.Size(469, 36);
            this.DgvTimeScheuleGraph.TabIndex = 1082;
            this.DgvTimeScheuleGraph.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvTimeScheuleGraph_CellClick);
            // 
            // LineShape1
            // 
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 8;
            this.LineShape1.X2 = 474;
            this.LineShape1.Y1 = 62;
            this.LineShape1.Y2 = 62;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.LineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(480, 528);
            this.shapeContainer1.TabIndex = 1083;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 7;
            this.lineShape2.X2 = 473;
            this.lineShape2.Y1 = 413;
            this.lineShape2.Y2 = 413;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(400, 476);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 1086;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(319, 476);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 1085;
            this.BtnOk.Text = "Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // DTPFormatTime
            // 
            this.DTPFormatTime.CustomFormat = "HH:mm";
            this.DTPFormatTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTPFormatTime.Location = new System.Drawing.Point(199, 218);
            this.DTPFormatTime.Name = "DTPFormatTime";
            this.DTPFormatTime.ShowUpDown = true;
            this.DTPFormatTime.Size = new System.Drawing.Size(68, 20);
            this.DTPFormatTime.TabIndex = 1087;
            this.DTPFormatTime.TabStop = false;
            // 
            // dgvUpdate
            // 
            this.dgvUpdate.AddNewRow = false;
            this.dgvUpdate.AlphaNumericCols = new int[0];
            this.dgvUpdate.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvUpdate.CapsLockCols = new int[0];
            this.dgvUpdate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUpdate.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColSlNo,
            this.ColEntryExit,
            this.ColPunching,
            this.ColWorkTime,
            this.ColBreakTime});
            this.dgvUpdate.ContextMenuStrip = this.cmsRecord;
            this.dgvUpdate.DecimalCols = new int[0];
            this.dgvUpdate.HasSlNo = false;
            this.dgvUpdate.LastRowIndex = 0;
            this.dgvUpdate.Location = new System.Drawing.Point(6, 71);
            this.dgvUpdate.Name = "dgvUpdate";
            this.dgvUpdate.NegativeValueCols = new int[0];
            this.dgvUpdate.NumericCols = new int[0];
            this.dgvUpdate.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvUpdate.Size = new System.Drawing.Size(469, 263);
            this.dgvUpdate.TabIndex = 1084;
            this.dgvUpdate.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUpdate_CellValueChanged);
            this.dgvUpdate.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvUpdate_CellBeginEdit);
            this.dgvUpdate.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUpdate_CellLeave);
            this.dgvUpdate.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvUpdate_CellFormatting);
            this.dgvUpdate.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvUpdate_CellValidating);
            this.dgvUpdate.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUpdate_CellEndEdit);
            this.dgvUpdate.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvUpdate_EditingControlShowing);
            this.dgvUpdate.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvUpdate_CurrentCellDirtyStateChanged);
            this.dgvUpdate.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvUpdate_DataError);
            this.dgvUpdate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvUpdate_KeyDown);
            // 
            // ColSlNo
            // 
            this.ColSlNo.HeaderText = "SlNo";
            this.ColSlNo.Name = "ColSlNo";
            this.ColSlNo.ReadOnly = true;
            this.ColSlNo.Width = 56;
            // 
            // ColEntryExit
            // 
            this.ColEntryExit.HeaderText = "Entry/Exit";
            this.ColEntryExit.Name = "ColEntryExit";
            this.ColEntryExit.ReadOnly = true;
            this.ColEntryExit.Width = 75;
            // 
            // ColPunching
            // 
            this.ColPunching.HeaderText = "Punching";
            this.ColPunching.Name = "ColPunching";
            // 
            // ColWorkTime
            // 
            this.ColWorkTime.HeaderText = "WorkTime";
            this.ColWorkTime.Name = "ColWorkTime";
            this.ColWorkTime.ReadOnly = true;
            this.ColWorkTime.Width = 90;
            // 
            // ColBreakTime
            // 
            this.ColBreakTime.HeaderText = "BreakTime";
            this.ColBreakTime.Name = "ColBreakTime";
            this.ColBreakTime.ReadOnly = true;
            this.ColBreakTime.Width = 119;
            // 
            // tmrClearlabels
            // 
            this.tmrClearlabels.Tick += new System.EventHandler(this.tmrClearlabels_Tick);
            // 
            // FrmUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 528);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.dgvUpdate);
            this.Controls.Add(this.DgvTimeScheuleGraph);
            this.Controls.Add(this.PicBoxBreakTime);
            this.Controls.Add(this.PicBoxWorkTime);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Label9);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.TxtBreakTime);
            this.Controls.Add(this.LblEarlyGoing);
            this.Controls.Add(this.TxtOverTime);
            this.Controls.Add(this.lblLateComing);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.txtWorkTime);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.lblDateDisplay);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.TxtFoodBreak);
            this.Controls.Add(this.LblShift);
            this.Controls.Add(this.LblShiftDesc);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.DTPFormatTime);
            this.Controls.Add(this.shapeContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmUpdate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Update";
            this.Load += new System.EventHandler(this.FrmUpdate_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmUpdate_FormClosing);
            this.cmsRecord.ResumeLayout(false);
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTimeScheuleGraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpdate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label TxtFoodBreak;
        internal System.Windows.Forms.Label LblShift;
        internal System.Windows.Forms.Label LblShiftDesc;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.ContextMenuStrip cmsRecord;
        internal System.Windows.Forms.ToolStripMenuItem InsertARecordToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem RemoveARecordToolStripMenuItem;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel1;
        internal System.Windows.Forms.ToolStripStatusLabel TLSPStatusLbl;
        internal System.Windows.Forms.ToolStripStatusLabel TLSPStatusLblDisplaying;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label lblDateDisplay;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox TxtBreakTime;
        internal System.Windows.Forms.Label LblEarlyGoing;
        internal System.Windows.Forms.TextBox TxtOverTime;
        internal System.Windows.Forms.Label lblLateComing;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox txtWorkTime;
        internal System.Windows.Forms.Button PicBoxBreakTime;
        internal System.Windows.Forms.Button PicBoxWorkTime;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label1;
        private System.Windows.Forms.DataGridView DgvTimeScheuleGraph;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private DemoClsDataGridview.ClsDataGirdView dgvUpdate;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button BtnOk;
        internal System.Windows.Forms.DateTimePicker DTPFormatTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColSlNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEntryExit;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPunching;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColWorkTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColBreakTime;
        private System.Windows.Forms.Timer tmrClearlabels;
    }
}