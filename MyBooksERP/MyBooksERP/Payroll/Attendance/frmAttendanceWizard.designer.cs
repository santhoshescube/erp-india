﻿namespace MyBooksERP
{
    partial class FrmAttendancewizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAttendancewizard));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TmProgress = new System.Windows.Forms.Timer(this.components);
            this.AttendanceWizard = new DevComponents.DotNetBar.Wizard();
            this.WzPageOverview = new DevComponents.DotNetBar.WizardPage();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.WzPageTemplatename = new DevComponents.DotNetBar.WizardPage();
            this.WarningTempname = new DevComponents.DotNetBar.Controls.WarningBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.TxtTemplateName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.Label4 = new System.Windows.Forms.Label();
            this.WzPageFilebrowse = new DevComponents.DotNetBar.WizardPage();
            this.ProgressBarProcess = new DevComponents.DotNetBar.Controls.ProgressBarX();
            this.WarningFilebrowse = new DevComponents.DotNetBar.Controls.WarningBox();
            this.BtnFilebrowse = new DevComponents.DotNetBar.ButtonX();
            this.TxtFileBrowse = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.Label6 = new System.Windows.Forms.Label();
            this.wizardPageLogin = new DevComponents.DotNetBar.WizardPage();
            this.label14 = new System.Windows.Forms.Label();
            this.prgBarSetPassword = new DevComponents.DotNetBar.Controls.ProgressBarX();
            this.warningSetPassword = new DevComponents.DotNetBar.Controls.WarningBox();
            this.cboTableNames = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnTestconnection = new DevComponents.DotNetBar.ButtonX();
            this.txtPassword = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.WzPageGriddisplay = new DevComponents.DotNetBar.WizardPage();
            this.Lnkgobackbrowse = new System.Windows.Forms.LinkLabel();
            this.RbHeaderNo = new System.Windows.Forms.RadioButton();
            this.RbHeaderYes = new System.Windows.Forms.RadioButton();
            this.Label7 = new System.Windows.Forms.Label();
            this.DgvAttendanceFetch = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.WzPageFormatdisplay = new DevComponents.DotNetBar.WizardPage();
            this.Label8 = new System.Windows.Forms.Label();
            this.BubbleBar3 = new DevComponents.DotNetBar.BubbleBar();
            this.BubbleBarTab3 = new DevComponents.DotNetBar.BubbleBarTab(this.components);
            this.Bubanother = new DevComponents.DotNetBar.BubbleButton();
            this.BubbleBar2 = new DevComponents.DotNetBar.BubbleBar();
            this.BubbleBarTab2 = new DevComponents.DotNetBar.BubbleBarTab(this.components);
            this.Bubmultiple = new DevComponents.DotNetBar.BubbleButton();
            this.BubbleBar1 = new DevComponents.DotNetBar.BubbleBar();
            this.BubbleBarTab1 = new DevComponents.DotNetBar.BubbleBarTab(this.components);
            this.Bubsingle = new DevComponents.DotNetBar.BubbleButton();
            this.LnkGoback = new System.Windows.Forms.LinkLabel();
            this.Rbanother = new System.Windows.Forms.RadioButton();
            this.Rbmultiple = new System.Windows.Forms.RadioButton();
            this.Rbsingle = new System.Windows.Forms.RadioButton();
            this.WzPageTimeselection = new DevComponents.DotNetBar.WizardPage();
            this.Wartimecolumn = new DevComponents.DotNetBar.Controls.WarningBox();
            this.NumUpDpwnTime = new DevComponents.Editors.IntegerInput();
            this.Label9 = new System.Windows.Forms.Label();
            this.DgvAttendanceFetchTimeselection = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.WzPagemapping1 = new DevComponents.DotNetBar.WizardPage();
            this.Warmappingemp = new DevComponents.DotNetBar.Controls.WarningBox();
            this.CbomappingEmp = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.DgvAttendancemapping1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Label10 = new System.Windows.Forms.Label();
            this.WzPagemapping2 = new DevComponents.DotNetBar.WizardPage();
            this.Cbomapdateformat = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.Label15 = new System.Windows.Forms.Label();
            this.Warmappingdate = new DevComponents.DotNetBar.Controls.WarningBox();
            this.CbomappingDate = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.DgvAttendanceFetchmapping2 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Label11 = new System.Windows.Forms.Label();
            this.WzPagemapping3 = new DevComponents.DotNetBar.WizardPage();
            this.rbtimedate1 = new System.Windows.Forms.RadioButton();
            this.rbtimedate = new System.Windows.Forms.RadioButton();
            this.Label16 = new System.Windows.Forms.Label();
            this.Warmappingtime = new DevComponents.DotNetBar.Controls.WarningBox();
            this.LstMappingtime = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.DgvAttendancefetchmapping3 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Label12 = new System.Windows.Forms.Label();
            this.WzPagemappingdesc = new DevComponents.DotNetBar.WizardPage();
            this.Label13 = new System.Windows.Forms.Label();
            this.WzPageFinalmapping = new DevComponents.DotNetBar.WizardPage();
            this.DgvAttendanceMapping = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.CboEmployee = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CboPunch = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Warmappingfinal = new DevComponents.DotNetBar.Controls.WarningBox();
            this.WizardPage1 = new DevComponents.DotNetBar.WizardPage();
            this.lblfinalmsg = new System.Windows.Forms.Label();
            this.AttendanceWizard.SuspendLayout();
            this.WzPageOverview.SuspendLayout();
            this.WzPageTemplatename.SuspendLayout();
            this.WzPageFilebrowse.SuspendLayout();
            this.wizardPageLogin.SuspendLayout();
            this.WzPageGriddisplay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendanceFetch)).BeginInit();
            this.WzPageFormatdisplay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BubbleBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BubbleBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BubbleBar1)).BeginInit();
            this.WzPageTimeselection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumUpDpwnTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendanceFetchTimeselection)).BeginInit();
            this.WzPagemapping1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendancemapping1)).BeginInit();
            this.WzPagemapping2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendanceFetchmapping2)).BeginInit();
            this.WzPagemapping3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendancefetchmapping3)).BeginInit();
            this.WzPagemappingdesc.SuspendLayout();
            this.WzPageFinalmapping.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendanceMapping)).BeginInit();
            this.WizardPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // AttendanceWizard
            // 
            this.AttendanceWizard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(229)))), ((int)(((byte)(253)))));
            this.AttendanceWizard.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("AttendanceWizard.BackgroundImage")));
            this.AttendanceWizard.ButtonStyle = DevComponents.DotNetBar.eWizardStyle.Office2007;
            this.AttendanceWizard.CancelButtonText = "Cancel";
            this.AttendanceWizard.Cursor = System.Windows.Forms.Cursors.Default;
            this.AttendanceWizard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AttendanceWizard.FinishButtonTabIndex = 3;
            this.AttendanceWizard.FooterHeight = 35;
            // 
            // 
            // 
            this.AttendanceWizard.FooterStyle.BackColor = System.Drawing.Color.Transparent;
            this.AttendanceWizard.FooterStyle.Class = "";
            this.AttendanceWizard.FooterStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.AttendanceWizard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            this.AttendanceWizard.HeaderCaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.AttendanceWizard.HeaderDescriptionIndent = 62;
            this.AttendanceWizard.HeaderDescriptionVisible = false;
            this.AttendanceWizard.HeaderHeight = 90;
            this.AttendanceWizard.HeaderImageAlignment = DevComponents.DotNetBar.eWizardTitleImageAlignment.Left;
            // 
            // 
            // 
            this.AttendanceWizard.HeaderStyle.BackColor = System.Drawing.Color.Transparent;
            this.AttendanceWizard.HeaderStyle.BackColorGradientAngle = 90;
            this.AttendanceWizard.HeaderStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.AttendanceWizard.HeaderStyle.BorderBottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(157)))), ((int)(((byte)(182)))));
            this.AttendanceWizard.HeaderStyle.BorderBottomWidth = 1;
            this.AttendanceWizard.HeaderStyle.BorderColor = System.Drawing.SystemColors.Control;
            this.AttendanceWizard.HeaderStyle.BorderLeftWidth = 1;
            this.AttendanceWizard.HeaderStyle.BorderRightWidth = 1;
            this.AttendanceWizard.HeaderStyle.BorderTopWidth = 1;
            this.AttendanceWizard.HeaderStyle.Class = "";
            this.AttendanceWizard.HeaderStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.AttendanceWizard.HeaderStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.AttendanceWizard.HeaderStyle.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.AttendanceWizard.HeaderTitleIndent = 62;
            this.AttendanceWizard.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.AttendanceWizard.Location = new System.Drawing.Point(0, 0);
            this.AttendanceWizard.Name = "AttendanceWizard";
            this.AttendanceWizard.Size = new System.Drawing.Size(537, 423);
            this.AttendanceWizard.TabIndex = 1;
            this.AttendanceWizard.WizardPages.AddRange(new DevComponents.DotNetBar.WizardPage[] {
            this.WzPageOverview,
            this.WzPageTemplatename,
            this.WzPageFilebrowse,
            this.wizardPageLogin,
            this.WzPageGriddisplay,
            this.WzPageFormatdisplay,
            this.WzPageTimeselection,
            this.WzPagemapping1,
            this.WzPagemapping2,
            this.WzPagemapping3,
            this.WzPagemappingdesc,
            this.WzPageFinalmapping,
            this.WizardPage1});
            this.AttendanceWizard.CancelButtonClick += new System.ComponentModel.CancelEventHandler(this.AttendanceWizard_CancelButtonClick);
            this.AttendanceWizard.HelpButtonClick += new System.ComponentModel.CancelEventHandler(this.AttendanceWizard_HelpButtonClick);
            // 
            // WzPageOverview
            // 
            this.WzPageOverview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WzPageOverview.BackColor = System.Drawing.Color.Transparent;
            this.WzPageOverview.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.WzPageOverview.Controls.Add(this.Label1);
            this.WzPageOverview.Controls.Add(this.Label2);
            this.WzPageOverview.Controls.Add(this.Label3);
            this.WzPageOverview.InteriorPage = false;
            this.WzPageOverview.Location = new System.Drawing.Point(0, 0);
            this.WzPageOverview.Name = "WzPageOverview";
            this.WzPageOverview.Size = new System.Drawing.Size(537, 388);
            // 
            // 
            // 
            this.WzPageOverview.Style.BackgroundImagePosition = DevComponents.DotNetBar.eStyleBackgroundImage.TopLeft;
            this.WzPageOverview.Style.Class = "";
            this.WzPageOverview.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WzPageOverview.Style.MarginLeft = 3;
            this.WzPageOverview.Style.MarginTop = 4;
            // 
            // 
            // 
            this.WzPageOverview.StyleMouseDown.Class = "";
            this.WzPageOverview.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPageOverview.StyleMouseOver.Class = "";
            this.WzPageOverview.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WzPageOverview.TabIndex = 7;
            // 
            // Label1
            // 
            this.Label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Label1.BackColor = System.Drawing.Color.Transparent;
            this.Label1.Font = new System.Drawing.Font("Tahoma", 16F);
            this.Label1.Location = new System.Drawing.Point(210, 18);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(333, 66);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Welcome to the Data Migration Wizard";
            // 
            // Label2
            // 
            this.Label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Label2.BackColor = System.Drawing.Color.Transparent;
            this.Label2.Location = new System.Drawing.Point(210, 100);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(283, 231);
            this.Label2.TabIndex = 1;
            this.Label2.Text = resources.GetString("Label2.Text");
            // 
            // Label3
            // 
            this.Label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label3.BackColor = System.Drawing.Color.Transparent;
            this.Label3.Location = new System.Drawing.Point(210, 356);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(120, 13);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "To continue, click Next.";
            // 
            // WzPageTemplatename
            // 
            this.WzPageTemplatename.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WzPageTemplatename.AntiAlias = false;
            this.WzPageTemplatename.BackColor = System.Drawing.Color.Transparent;
            this.WzPageTemplatename.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.WzPageTemplatename.Controls.Add(this.WarningTempname);
            this.WzPageTemplatename.Controls.Add(this.Label5);
            this.WzPageTemplatename.Controls.Add(this.TxtTemplateName);
            this.WzPageTemplatename.Controls.Add(this.Label4);
            this.WzPageTemplatename.Location = new System.Drawing.Point(7, 102);
            this.WzPageTemplatename.Name = "WzPageTemplatename";
            this.WzPageTemplatename.PageDescription = "You may give your access control brand name";
            this.WzPageTemplatename.PageHeaderImage = global::MyBooksERP.Properties.Resources.wizard_setup_3;
            this.WzPageTemplatename.PageTitle = "Specify your Template name";
            this.WzPageTemplatename.Size = new System.Drawing.Size(523, 274);
            // 
            // 
            // 
            this.WzPageTemplatename.Style.Class = "";
            this.WzPageTemplatename.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPageTemplatename.StyleMouseDown.Class = "";
            this.WzPageTemplatename.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPageTemplatename.StyleMouseOver.Class = "";
            this.WzPageTemplatename.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WzPageTemplatename.TabIndex = 8;
            this.WzPageTemplatename.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.WzPageTemplatename_NextButtonClick);
            this.WzPageTemplatename.AfterPageDisplayed += new DevComponents.DotNetBar.WizardPageChangeEventHandler(this.WzPageTemplatename_AfterPageDisplayed);
            // 
            // WarningTempname
            // 
            this.WarningTempname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.WarningTempname.CloseButtonVisible = false;
            this.WarningTempname.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.WarningTempname.Image = ((System.Drawing.Image)(resources.GetObject("WarningTempname.Image")));
            this.WarningTempname.Location = new System.Drawing.Point(0, 242);
            this.WarningTempname.Name = "WarningTempname";
            this.WarningTempname.OptionsButtonVisible = false;
            this.WarningTempname.Size = new System.Drawing.Size(523, 32);
            this.WarningTempname.TabIndex = 23;
            this.WarningTempname.Text = "<b>Warning Box</b> control with <i>text-markup</i> support.";
            this.WarningTempname.Visible = false;
            // 
            // Label5
            // 
            this.Label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label5.BackColor = System.Drawing.Color.Transparent;
            this.Label5.Location = new System.Drawing.Point(69, 182);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(387, 30);
            this.Label5.TabIndex = 6;
            this.Label5.Text = "This name is used for saving your template only. Your access control device will " +
                "not be accessed directly by this wizard at any point of time.";
            // 
            // TxtTemplateName
            // 
            // 
            // 
            // 
            this.TxtTemplateName.Border.Class = "TextBoxBorder";
            this.TxtTemplateName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtTemplateName.Location = new System.Drawing.Point(72, 79);
            this.TxtTemplateName.MaxLength = 50;
            this.TxtTemplateName.Name = "TxtTemplateName";
            this.TxtTemplateName.Size = new System.Drawing.Size(223, 20);
            this.TxtTemplateName.TabIndex = 0;
            // 
            // Label4
            // 
            this.Label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label4.BackColor = System.Drawing.Color.Transparent;
            this.Label4.Location = new System.Drawing.Point(69, 20);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(387, 30);
            this.Label4.TabIndex = 4;
            this.Label4.Text = "What is the name of the Biometric /RFID/Access Control System you are using\r\n in " +
                "your organization?";
            // 
            // WzPageFilebrowse
            // 
            this.WzPageFilebrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WzPageFilebrowse.AntiAlias = false;
            this.WzPageFilebrowse.BackColor = System.Drawing.Color.Transparent;
            this.WzPageFilebrowse.Controls.Add(this.ProgressBarProcess);
            this.WzPageFilebrowse.Controls.Add(this.WarningFilebrowse);
            this.WzPageFilebrowse.Controls.Add(this.BtnFilebrowse);
            this.WzPageFilebrowse.Controls.Add(this.TxtFileBrowse);
            this.WzPageFilebrowse.Controls.Add(this.Label6);
            this.WzPageFilebrowse.Location = new System.Drawing.Point(7, 102);
            this.WzPageFilebrowse.Name = "WzPageFilebrowse";
            this.WzPageFilebrowse.PageDescription = "This file may be optained from time attendance software shipped with your access " +
                "control device";
            this.WzPageFilebrowse.PageHeaderImage = global::MyBooksERP.Properties.Resources.wizard_setup_3;
            this.WzPageFilebrowse.PageTitle = "Data File Identification";
            this.WzPageFilebrowse.Size = new System.Drawing.Size(523, 274);
            // 
            // 
            // 
            this.WzPageFilebrowse.Style.Class = "";
            this.WzPageFilebrowse.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPageFilebrowse.StyleMouseDown.Class = "";
            this.WzPageFilebrowse.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPageFilebrowse.StyleMouseOver.Class = "";
            this.WzPageFilebrowse.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WzPageFilebrowse.TabIndex = 9;
            this.WzPageFilebrowse.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.WzPageFilebrowse_NextButtonClick);
            this.WzPageFilebrowse.AfterPageDisplayed += new DevComponents.DotNetBar.WizardPageChangeEventHandler(this.WzPageFilebrowse_AfterPageDisplayed);
            // 
            // ProgressBarProcess
            // 
            // 
            // 
            // 
            this.ProgressBarProcess.BackgroundStyle.Class = "";
            this.ProgressBarProcess.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ProgressBarProcess.ColorTable = DevComponents.DotNetBar.eProgressBarItemColor.Paused;
            this.ProgressBarProcess.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ProgressBarProcess.Location = new System.Drawing.Point(0, 231);
            this.ProgressBarProcess.Name = "ProgressBarProcess";
            this.ProgressBarProcess.Size = new System.Drawing.Size(523, 10);
            this.ProgressBarProcess.TabIndex = 26;
            this.ProgressBarProcess.Text = "ProgressBarX1";
            this.ProgressBarProcess.Visible = false;
            // 
            // WarningFilebrowse
            // 
            this.WarningFilebrowse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.WarningFilebrowse.CloseButtonVisible = false;
            this.WarningFilebrowse.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.WarningFilebrowse.Image = ((System.Drawing.Image)(resources.GetObject("WarningFilebrowse.Image")));
            this.WarningFilebrowse.Location = new System.Drawing.Point(0, 241);
            this.WarningFilebrowse.Name = "WarningFilebrowse";
            this.WarningFilebrowse.OptionsButtonVisible = false;
            this.WarningFilebrowse.Size = new System.Drawing.Size(523, 33);
            this.WarningFilebrowse.TabIndex = 24;
            this.WarningFilebrowse.Text = "<b>Warning Box</b> control with <i>text-markup</i> support.";
            this.WarningFilebrowse.Visible = false;
            // 
            // BtnFilebrowse
            // 
            this.BtnFilebrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnFilebrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnFilebrowse.Location = new System.Drawing.Point(369, 148);
            this.BtnFilebrowse.Name = "BtnFilebrowse";
            this.BtnFilebrowse.Size = new System.Drawing.Size(52, 20);
            this.BtnFilebrowse.TabIndex = 7;
            this.BtnFilebrowse.Text = "&Browse";
            this.BtnFilebrowse.Click += new System.EventHandler(this.BtnFilebrowse_Click);
            // 
            // TxtFileBrowse
            // 
            // 
            // 
            // 
            this.TxtFileBrowse.Border.Class = "TextBoxBorder";
            this.TxtFileBrowse.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtFileBrowse.Location = new System.Drawing.Point(72, 148);
            this.TxtFileBrowse.Name = "TxtFileBrowse";
            this.TxtFileBrowse.ReadOnly = true;
            this.TxtFileBrowse.Size = new System.Drawing.Size(291, 20);
            this.TxtFileBrowse.TabIndex = 6;
            // 
            // Label6
            // 
            this.Label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label6.BackColor = System.Drawing.Color.Transparent;
            this.Label6.Location = new System.Drawing.Point(69, 40);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(359, 42);
            this.Label6.TabIndex = 5;
            this.Label6.Text = "MyPayfriend auto attendance system requires an attendance data file from which th" +
                "e attendance details of the employees are extracted.\r\nPlease specify the data fi" +
                "le needed :\r\n";
            // 
            // wizardPageLogin
            // 
            this.wizardPageLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wizardPageLogin.BackColor = System.Drawing.Color.Transparent;
            this.wizardPageLogin.Controls.Add(this.label14);
            this.wizardPageLogin.Controls.Add(this.prgBarSetPassword);
            this.wizardPageLogin.Controls.Add(this.warningSetPassword);
            this.wizardPageLogin.Controls.Add(this.cboTableNames);
            this.wizardPageLogin.Controls.Add(this.btnTestconnection);
            this.wizardPageLogin.Controls.Add(this.txtPassword);
            this.wizardPageLogin.Controls.Add(this.labelX2);
            this.wizardPageLogin.Controls.Add(this.labelX1);
            this.wizardPageLogin.Location = new System.Drawing.Point(7, 102);
            this.wizardPageLogin.Name = "wizardPageLogin";
            this.wizardPageLogin.PageDescription = "Set Password";
            this.wizardPageLogin.PageTitle = "Connect to the Databse";
            this.wizardPageLogin.Size = new System.Drawing.Size(523, 274);
            // 
            // 
            // 
            this.wizardPageLogin.Style.Class = "";
            this.wizardPageLogin.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wizardPageLogin.StyleMouseDown.Class = "";
            this.wizardPageLogin.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wizardPageLogin.StyleMouseOver.Class = "";
            this.wizardPageLogin.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wizardPageLogin.TabIndex = 19;
            this.wizardPageLogin.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.wizardPageLogin_NextButtonClick);
            this.wizardPageLogin.AfterPageDisplayed += new DevComponents.DotNetBar.WizardPageChangeEventHandler(this.wizardPageLogin_AfterPageDisplayed);
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(58, 14);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(359, 42);
            this.label14.TabIndex = 28;
            this.label14.Text = "Please enter the database password and connect to the database.Then select the ta" +
                "ble name from which the data to be fetched.";
            // 
            // prgBarSetPassword
            // 
            // 
            // 
            // 
            this.prgBarSetPassword.BackgroundStyle.Class = "";
            this.prgBarSetPassword.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.prgBarSetPassword.ColorTable = DevComponents.DotNetBar.eProgressBarItemColor.Paused;
            this.prgBarSetPassword.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.prgBarSetPassword.Location = new System.Drawing.Point(0, 231);
            this.prgBarSetPassword.Name = "prgBarSetPassword";
            this.prgBarSetPassword.Size = new System.Drawing.Size(523, 10);
            this.prgBarSetPassword.TabIndex = 27;
            this.prgBarSetPassword.Text = "ProgressBarX1";
            this.prgBarSetPassword.Visible = false;
            // 
            // warningSetPassword
            // 
            this.warningSetPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.warningSetPassword.CloseButtonVisible = false;
            this.warningSetPassword.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.warningSetPassword.Image = ((System.Drawing.Image)(resources.GetObject("warningSetPassword.Image")));
            this.warningSetPassword.Location = new System.Drawing.Point(0, 241);
            this.warningSetPassword.Name = "warningSetPassword";
            this.warningSetPassword.OptionsButtonVisible = false;
            this.warningSetPassword.Size = new System.Drawing.Size(523, 33);
            this.warningSetPassword.TabIndex = 25;
            this.warningSetPassword.Text = "<b>Warning Box</b> control with <i>text-markup</i> support.";
            this.warningSetPassword.Visible = false;
            // 
            // cboTableNames
            // 
            this.cboTableNames.DisplayMember = "Text";
            this.cboTableNames.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboTableNames.FormattingEnabled = true;
            this.cboTableNames.ItemHeight = 14;
            this.cboTableNames.Location = new System.Drawing.Point(172, 143);
            this.cboTableNames.Name = "cboTableNames";
            this.cboTableNames.Size = new System.Drawing.Size(178, 20);
            this.cboTableNames.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboTableNames.TabIndex = 16;
            // 
            // btnTestconnection
            // 
            this.btnTestconnection.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnTestconnection.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnTestconnection.Location = new System.Drawing.Point(172, 105);
            this.btnTestconnection.Name = "btnTestconnection";
            this.btnTestconnection.Size = new System.Drawing.Size(98, 23);
            this.btnTestconnection.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnTestconnection.TabIndex = 15;
            this.btnTestconnection.Text = "Connect";
            this.btnTestconnection.Tooltip = "Connect to database";
            this.btnTestconnection.Click += new System.EventHandler(this.btnTestConnection_Click);
            // 
            // txtPassword
            // 
            // 
            // 
            // 
            this.txtPassword.Border.Class = "TextBoxBorder";
            this.txtPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPassword.Location = new System.Drawing.Point(172, 79);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(178, 20);
            this.txtPassword.TabIndex = 14;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.Class = "";
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(59, 143);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 13;
            this.labelX2.Text = "Select Table";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(59, 76);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(107, 23);
            this.labelX1.TabIndex = 12;
            this.labelX1.Text = "Database Password";
            // 
            // WzPageGriddisplay
            // 
            this.WzPageGriddisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WzPageGriddisplay.AntiAlias = false;
            this.WzPageGriddisplay.BackColor = System.Drawing.Color.Transparent;
            this.WzPageGriddisplay.Controls.Add(this.Lnkgobackbrowse);
            this.WzPageGriddisplay.Controls.Add(this.RbHeaderNo);
            this.WzPageGriddisplay.Controls.Add(this.RbHeaderYes);
            this.WzPageGriddisplay.Controls.Add(this.Label7);
            this.WzPageGriddisplay.Controls.Add(this.DgvAttendanceFetch);
            this.WzPageGriddisplay.Location = new System.Drawing.Point(7, 102);
            this.WzPageGriddisplay.Name = "WzPageGriddisplay";
            this.WzPageGriddisplay.PageDescription = "Identify your data files content";
            this.WzPageGriddisplay.PageHeaderImage = global::MyBooksERP.Properties.Resources.wizard_setup_3;
            this.WzPageGriddisplay.PageTitle = "Header Identification";
            this.WzPageGriddisplay.Size = new System.Drawing.Size(523, 274);
            // 
            // 
            // 
            this.WzPageGriddisplay.Style.Class = "";
            this.WzPageGriddisplay.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPageGriddisplay.StyleMouseDown.Class = "";
            this.WzPageGriddisplay.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPageGriddisplay.StyleMouseOver.Class = "";
            this.WzPageGriddisplay.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WzPageGriddisplay.TabIndex = 10;
            this.WzPageGriddisplay.BackButtonClick += new System.ComponentModel.CancelEventHandler(this.WzPageGriddisplay_BackButtonClick);
            this.WzPageGriddisplay.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.WzPageGriddisplay_NextButtonClick);
            this.WzPageGriddisplay.AfterPageDisplayed += new DevComponents.DotNetBar.WizardPageChangeEventHandler(this.WzPageGriddisplay_AfterPageDisplayed);
            // 
            // Lnkgobackbrowse
            // 
            this.Lnkgobackbrowse.AutoSize = true;
            this.Lnkgobackbrowse.Location = new System.Drawing.Point(5, 260);
            this.Lnkgobackbrowse.Name = "Lnkgobackbrowse";
            this.Lnkgobackbrowse.Size = new System.Drawing.Size(158, 13);
            this.Lnkgobackbrowse.TabIndex = 1;
            this.Lnkgobackbrowse.TabStop = true;
            this.Lnkgobackbrowse.Text = "I can\'t see any data in the table!";
            // 
            // RbHeaderNo
            // 
            this.RbHeaderNo.AutoSize = true;
            this.RbHeaderNo.Location = new System.Drawing.Point(389, 236);
            this.RbHeaderNo.Name = "RbHeaderNo";
            this.RbHeaderNo.Size = new System.Drawing.Size(39, 17);
            this.RbHeaderNo.TabIndex = 7;
            this.RbHeaderNo.Text = "No";
            this.RbHeaderNo.UseVisualStyleBackColor = true;
            // 
            // RbHeaderYes
            // 
            this.RbHeaderYes.AutoSize = true;
            this.RbHeaderYes.Checked = true;
            this.RbHeaderYes.Location = new System.Drawing.Point(334, 236);
            this.RbHeaderYes.Name = "RbHeaderYes";
            this.RbHeaderYes.Size = new System.Drawing.Size(43, 17);
            this.RbHeaderYes.TabIndex = 6;
            this.RbHeaderYes.TabStop = true;
            this.RbHeaderYes.Text = "Yes";
            this.RbHeaderYes.UseVisualStyleBackColor = true;
            // 
            // Label7
            // 
            this.Label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label7.BackColor = System.Drawing.Color.Transparent;
            this.Label7.Location = new System.Drawing.Point(5, 225);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(449, 30);
            this.Label7.TabIndex = 5;
            this.Label7.Text = "Your data file may contain header description for the columns.\r\nAre you able to s" +
                "ee any column headers inside the above table?";
            // 
            // DgvAttendanceFetch
            // 
            this.DgvAttendanceFetch.AllowUserToAddRows = false;
            this.DgvAttendanceFetch.AllowUserToDeleteRows = false;
            this.DgvAttendanceFetch.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvAttendanceFetch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DgvAttendanceFetch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvAttendanceFetch.DefaultCellStyle = dataGridViewCellStyle2;
            this.DgvAttendanceFetch.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvAttendanceFetch.Location = new System.Drawing.Point(10, 0);
            this.DgvAttendanceFetch.Name = "DgvAttendanceFetch";
            this.DgvAttendanceFetch.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvAttendanceFetch.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DgvAttendanceFetch.RowHeadersVisible = false;
            this.DgvAttendanceFetch.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.DgvAttendanceFetch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvAttendanceFetch.Size = new System.Drawing.Size(503, 208);
            this.DgvAttendanceFetch.TabIndex = 8;
            // 
            // WzPageFormatdisplay
            // 
            this.WzPageFormatdisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WzPageFormatdisplay.AntiAlias = false;
            this.WzPageFormatdisplay.BackColor = System.Drawing.Color.Transparent;
            this.WzPageFormatdisplay.Controls.Add(this.Label8);
            this.WzPageFormatdisplay.Controls.Add(this.BubbleBar3);
            this.WzPageFormatdisplay.Controls.Add(this.BubbleBar2);
            this.WzPageFormatdisplay.Controls.Add(this.BubbleBar1);
            this.WzPageFormatdisplay.Controls.Add(this.LnkGoback);
            this.WzPageFormatdisplay.Controls.Add(this.Rbanother);
            this.WzPageFormatdisplay.Controls.Add(this.Rbmultiple);
            this.WzPageFormatdisplay.Controls.Add(this.Rbsingle);
            this.WzPageFormatdisplay.Location = new System.Drawing.Point(7, 102);
            this.WzPageFormatdisplay.Name = "WzPageFormatdisplay";
            this.WzPageFormatdisplay.PageDescription = "< Wizard step description >";
            this.WzPageFormatdisplay.PageHeaderImage = global::MyBooksERP.Properties.Resources.wizard_setup_3;
            this.WzPageFormatdisplay.PageTitle = "Sample Files";
            this.WzPageFormatdisplay.Size = new System.Drawing.Size(523, 274);
            // 
            // 
            // 
            this.WzPageFormatdisplay.Style.Class = "";
            this.WzPageFormatdisplay.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPageFormatdisplay.StyleMouseDown.Class = "";
            this.WzPageFormatdisplay.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPageFormatdisplay.StyleMouseOver.Class = "";
            this.WzPageFormatdisplay.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WzPageFormatdisplay.TabIndex = 11;
            this.WzPageFormatdisplay.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.WzPageFormatdisplay_NextButtonClick);
            // 
            // Label8
            // 
            this.Label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label8.BackColor = System.Drawing.Color.Transparent;
            this.Label8.Location = new System.Drawing.Point(12, 2);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(294, 22);
            this.Label8.TabIndex = 10;
            this.Label8.Text = "Which of the below shown samples resembles your table?";
            // 
            // BubbleBar3
            // 
            this.BubbleBar3.Alignment = DevComponents.DotNetBar.eBubbleButtonAlignment.Bottom;
            this.BubbleBar3.AntiAlias = true;
            // 
            // 
            // 
            this.BubbleBar3.BackgroundStyle.Class = "";
            this.BubbleBar3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.BubbleBar3.ButtonBackAreaStyle.BackColor = System.Drawing.Color.Transparent;
            this.BubbleBar3.ButtonBackAreaStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.BubbleBar3.ButtonBackAreaStyle.BorderBottomWidth = 1;
            this.BubbleBar3.ButtonBackAreaStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
            this.BubbleBar3.ButtonBackAreaStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.BubbleBar3.ButtonBackAreaStyle.BorderLeftWidth = 1;
            this.BubbleBar3.ButtonBackAreaStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.BubbleBar3.ButtonBackAreaStyle.BorderRightWidth = 1;
            this.BubbleBar3.ButtonBackAreaStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.BubbleBar3.ButtonBackAreaStyle.BorderTopWidth = 1;
            this.BubbleBar3.ButtonBackAreaStyle.Class = "";
            this.BubbleBar3.ButtonBackAreaStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.BubbleBar3.ButtonBackAreaStyle.PaddingBottom = 3;
            this.BubbleBar3.ButtonBackAreaStyle.PaddingLeft = 3;
            this.BubbleBar3.ButtonBackAreaStyle.PaddingRight = 3;
            this.BubbleBar3.ButtonBackAreaStyle.PaddingTop = 3;
            this.BubbleBar3.ImageSizeLarge = new System.Drawing.Size(330, 150);
            this.BubbleBar3.ImageSizeNormal = new System.Drawing.Size(300, 110);
            this.BubbleBar3.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.BubbleBar3.Location = new System.Drawing.Point(201, 171);
            this.BubbleBar3.MouseOverTabColors.BorderColor = System.Drawing.SystemColors.Highlight;
            this.BubbleBar3.Name = "BubbleBar3";
            this.BubbleBar3.SelectedTab = this.BubbleBarTab3;
            this.BubbleBar3.SelectedTabColors.BorderColor = System.Drawing.Color.Black;
            this.BubbleBar3.Size = new System.Drawing.Size(326, 119);
            this.BubbleBar3.TabIndex = 17;
            this.BubbleBar3.Tabs.Add(this.BubbleBarTab3);
            this.BubbleBar3.Text = "BubbleBar3";
            // 
            // BubbleBarTab3
            // 
            this.BubbleBarTab3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(230)))), ((int)(((byte)(247)))));
            this.BubbleBarTab3.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(168)))), ((int)(((byte)(228)))));
            this.BubbleBarTab3.Buttons.AddRange(new DevComponents.DotNetBar.BubbleButton[] {
            this.Bubanother});
            this.BubbleBarTab3.DarkBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.BubbleBarTab3.LightBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.BubbleBarTab3.Name = "BubbleBarTab3";
            this.BubbleBarTab3.PredefinedColor = DevComponents.DotNetBar.eTabItemColor.Blue;
            this.BubbleBarTab3.Text = "BubbleBarTab1";
            this.BubbleBarTab3.TextColor = System.Drawing.Color.Black;
            // 
            // Bubanother
            // 
            this.Bubanother.Image = ((System.Drawing.Image)(resources.GetObject("Bubanother.Image")));
            this.Bubanother.Name = "Bubanother";
            // 
            // BubbleBar2
            // 
            this.BubbleBar2.Alignment = DevComponents.DotNetBar.eBubbleButtonAlignment.Bottom;
            this.BubbleBar2.AntiAlias = true;
            // 
            // 
            // 
            this.BubbleBar2.BackgroundStyle.Class = "";
            this.BubbleBar2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.BubbleBar2.ButtonBackAreaStyle.BackColor = System.Drawing.Color.Transparent;
            this.BubbleBar2.ButtonBackAreaStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.BubbleBar2.ButtonBackAreaStyle.BorderBottomWidth = 1;
            this.BubbleBar2.ButtonBackAreaStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
            this.BubbleBar2.ButtonBackAreaStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.BubbleBar2.ButtonBackAreaStyle.BorderLeftWidth = 1;
            this.BubbleBar2.ButtonBackAreaStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.BubbleBar2.ButtonBackAreaStyle.BorderRightWidth = 1;
            this.BubbleBar2.ButtonBackAreaStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.BubbleBar2.ButtonBackAreaStyle.BorderTopWidth = 1;
            this.BubbleBar2.ButtonBackAreaStyle.Class = "";
            this.BubbleBar2.ButtonBackAreaStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.BubbleBar2.ButtonBackAreaStyle.PaddingBottom = 3;
            this.BubbleBar2.ButtonBackAreaStyle.PaddingLeft = 3;
            this.BubbleBar2.ButtonBackAreaStyle.PaddingRight = 3;
            this.BubbleBar2.ButtonBackAreaStyle.PaddingTop = 3;
            this.BubbleBar2.ImageSizeLarge = new System.Drawing.Size(270, 150);
            this.BubbleBar2.ImageSizeNormal = new System.Drawing.Size(250, 110);
            this.BubbleBar2.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.BubbleBar2.Location = new System.Drawing.Point(274, 43);
            this.BubbleBar2.MouseOverTabColors.BorderColor = System.Drawing.SystemColors.Highlight;
            this.BubbleBar2.Name = "BubbleBar2";
            this.BubbleBar2.SelectedTab = this.BubbleBarTab2;
            this.BubbleBar2.SelectedTabColors.BorderColor = System.Drawing.Color.Black;
            this.BubbleBar2.Size = new System.Drawing.Size(249, 119);
            this.BubbleBar2.TabIndex = 16;
            this.BubbleBar2.Tabs.Add(this.BubbleBarTab2);
            this.BubbleBar2.Text = "BubbleBar2";
            // 
            // BubbleBarTab2
            // 
            this.BubbleBarTab2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(230)))), ((int)(((byte)(247)))));
            this.BubbleBarTab2.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(168)))), ((int)(((byte)(228)))));
            this.BubbleBarTab2.Buttons.AddRange(new DevComponents.DotNetBar.BubbleButton[] {
            this.Bubmultiple});
            this.BubbleBarTab2.DarkBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.BubbleBarTab2.LightBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.BubbleBarTab2.Name = "BubbleBarTab2";
            this.BubbleBarTab2.PredefinedColor = DevComponents.DotNetBar.eTabItemColor.Blue;
            this.BubbleBarTab2.Text = "BubbleBarTab1";
            this.BubbleBarTab2.TextColor = System.Drawing.Color.Black;
            // 
            // Bubmultiple
            // 
            this.Bubmultiple.Image = ((System.Drawing.Image)(resources.GetObject("Bubmultiple.Image")));
            this.Bubmultiple.Name = "Bubmultiple";
            // 
            // BubbleBar1
            // 
            this.BubbleBar1.Alignment = DevComponents.DotNetBar.eBubbleButtonAlignment.Bottom;
            this.BubbleBar1.AntiAlias = true;
            // 
            // 
            // 
            this.BubbleBar1.BackgroundStyle.Class = "";
            this.BubbleBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.BubbleBar1.ButtonBackAreaStyle.BackColor = System.Drawing.Color.Transparent;
            this.BubbleBar1.ButtonBackAreaStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.BubbleBar1.ButtonBackAreaStyle.BorderBottomWidth = 1;
            this.BubbleBar1.ButtonBackAreaStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
            this.BubbleBar1.ButtonBackAreaStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.BubbleBar1.ButtonBackAreaStyle.BorderLeftWidth = 1;
            this.BubbleBar1.ButtonBackAreaStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.BubbleBar1.ButtonBackAreaStyle.BorderRightWidth = 1;
            this.BubbleBar1.ButtonBackAreaStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.BubbleBar1.ButtonBackAreaStyle.BorderTopWidth = 1;
            this.BubbleBar1.ButtonBackAreaStyle.Class = "";
            this.BubbleBar1.ButtonBackAreaStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.BubbleBar1.ButtonBackAreaStyle.PaddingBottom = 3;
            this.BubbleBar1.ButtonBackAreaStyle.PaddingLeft = 3;
            this.BubbleBar1.ButtonBackAreaStyle.PaddingRight = 3;
            this.BubbleBar1.ButtonBackAreaStyle.PaddingTop = 3;
            this.BubbleBar1.ImageSizeLarge = new System.Drawing.Size(270, 150);
            this.BubbleBar1.ImageSizeNormal = new System.Drawing.Size(250, 110);
            this.BubbleBar1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.BubbleBar1.Location = new System.Drawing.Point(12, 43);
            this.BubbleBar1.MouseOverTabColors.BorderColor = System.Drawing.SystemColors.Highlight;
            this.BubbleBar1.Name = "BubbleBar1";
            this.BubbleBar1.SelectedTab = this.BubbleBarTab1;
            this.BubbleBar1.SelectedTabColors.BorderColor = System.Drawing.Color.Black;
            this.BubbleBar1.Size = new System.Drawing.Size(249, 119);
            this.BubbleBar1.TabIndex = 15;
            this.BubbleBar1.Tabs.Add(this.BubbleBarTab1);
            this.BubbleBar1.Text = "BubbleBar1";
            // 
            // BubbleBarTab1
            // 
            this.BubbleBarTab1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(230)))), ((int)(((byte)(247)))));
            this.BubbleBarTab1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(168)))), ((int)(((byte)(228)))));
            this.BubbleBarTab1.Buttons.AddRange(new DevComponents.DotNetBar.BubbleButton[] {
            this.Bubsingle});
            this.BubbleBarTab1.DarkBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.BubbleBarTab1.LightBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.BubbleBarTab1.Name = "BubbleBarTab1";
            this.BubbleBarTab1.PredefinedColor = DevComponents.DotNetBar.eTabItemColor.Blue;
            this.BubbleBarTab1.Text = "BubbleBarTab1";
            this.BubbleBarTab1.TextColor = System.Drawing.Color.Black;
            // 
            // Bubsingle
            // 
            this.Bubsingle.Image = ((System.Drawing.Image)(resources.GetObject("Bubsingle.Image")));
            this.Bubsingle.Name = "Bubsingle";
            // 
            // LnkGoback
            // 
            this.LnkGoback.AutoSize = true;
            this.LnkGoback.Location = new System.Drawing.Point(3, 258);
            this.LnkGoback.Name = "LnkGoback";
            this.LnkGoback.Size = new System.Drawing.Size(114, 13);
            this.LnkGoback.TabIndex = 14;
            this.LnkGoback.TabStop = true;
            this.LnkGoback.Text = "I want to see my Table";
            // 
            // Rbanother
            // 
            this.Rbanother.AutoSize = true;
            this.Rbanother.Location = new System.Drawing.Point(123, 220);
            this.Rbanother.Name = "Rbanother";
            this.Rbanother.Size = new System.Drawing.Size(66, 17);
            this.Rbanother.TabIndex = 13;
            this.Rbanother.Text = "Sample3";
            this.Rbanother.UseVisualStyleBackColor = true;
            // 
            // Rbmultiple
            // 
            this.Rbmultiple.AutoSize = true;
            this.Rbmultiple.Location = new System.Drawing.Point(360, 24);
            this.Rbmultiple.Name = "Rbmultiple";
            this.Rbmultiple.Size = new System.Drawing.Size(66, 17);
            this.Rbmultiple.TabIndex = 12;
            this.Rbmultiple.Text = "Sample2";
            this.Rbmultiple.UseVisualStyleBackColor = true;
            // 
            // Rbsingle
            // 
            this.Rbsingle.AutoSize = true;
            this.Rbsingle.Checked = true;
            this.Rbsingle.Location = new System.Drawing.Point(96, 25);
            this.Rbsingle.Name = "Rbsingle";
            this.Rbsingle.Size = new System.Drawing.Size(66, 17);
            this.Rbsingle.TabIndex = 11;
            this.Rbsingle.TabStop = true;
            this.Rbsingle.Text = "Sample1";
            this.Rbsingle.UseVisualStyleBackColor = true;
            // 
            // WzPageTimeselection
            // 
            this.WzPageTimeselection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WzPageTimeselection.BackColor = System.Drawing.Color.Transparent;
            this.WzPageTimeselection.Controls.Add(this.Wartimecolumn);
            this.WzPageTimeselection.Controls.Add(this.NumUpDpwnTime);
            this.WzPageTimeselection.Controls.Add(this.Label9);
            this.WzPageTimeselection.Controls.Add(this.DgvAttendanceFetchTimeselection);
            this.WzPageTimeselection.Location = new System.Drawing.Point(7, 102);
            this.WzPageTimeselection.Name = "WzPageTimeselection";
            this.WzPageTimeselection.PageHeaderImage = global::MyBooksERP.Properties.Resources.wizard_setup_3;
            this.WzPageTimeselection.PageTitle = "Specify Time Columns";
            this.WzPageTimeselection.Size = new System.Drawing.Size(523, 274);
            // 
            // 
            // 
            this.WzPageTimeselection.Style.Class = "";
            this.WzPageTimeselection.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPageTimeselection.StyleMouseDown.Class = "";
            this.WzPageTimeselection.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPageTimeselection.StyleMouseOver.Class = "";
            this.WzPageTimeselection.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WzPageTimeselection.TabIndex = 12;
            this.WzPageTimeselection.Text = "WizardPage6";
            this.WzPageTimeselection.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.WzPageTimeselection_NextButtonClick);
            this.WzPageTimeselection.AfterPageDisplayed += new DevComponents.DotNetBar.WizardPageChangeEventHandler(this.WzPageTimeselection_AfterPageDisplayed);
            // 
            // Wartimecolumn
            // 
            this.Wartimecolumn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.Wartimecolumn.CloseButtonVisible = false;
            this.Wartimecolumn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Wartimecolumn.Image = ((System.Drawing.Image)(resources.GetObject("Wartimecolumn.Image")));
            this.Wartimecolumn.Location = new System.Drawing.Point(0, 237);
            this.Wartimecolumn.Name = "Wartimecolumn";
            this.Wartimecolumn.OptionsButtonVisible = false;
            this.Wartimecolumn.Size = new System.Drawing.Size(523, 37);
            this.Wartimecolumn.TabIndex = 17;
            this.Wartimecolumn.Text = "<b>Warning Box</b> control with <i>text-markup</i> support.";
            this.Wartimecolumn.Visible = false;
            // 
            // NumUpDpwnTime
            // 
            // 
            // 
            // 
            this.NumUpDpwnTime.BackgroundStyle.Class = "DateTimeInputBackground";
            this.NumUpDpwnTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.NumUpDpwnTime.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.NumUpDpwnTime.Location = new System.Drawing.Point(258, 214);
            this.NumUpDpwnTime.MaxValue = 20;
            this.NumUpDpwnTime.MinValue = 1;
            this.NumUpDpwnTime.Name = "NumUpDpwnTime";
            this.NumUpDpwnTime.ShowUpDown = true;
            this.NumUpDpwnTime.Size = new System.Drawing.Size(40, 20);
            this.NumUpDpwnTime.TabIndex = 16;
            this.NumUpDpwnTime.Value = 1;
            // 
            // Label9
            // 
            this.Label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label9.BackColor = System.Drawing.Color.Transparent;
            this.Label9.Location = new System.Drawing.Point(11, 216);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(251, 18);
            this.Label9.TabIndex = 12;
            this.Label9.Text = "How many columns represent Time in the table?\r\n\r\n";
            // 
            // DgvAttendanceFetchTimeselection
            // 
            this.DgvAttendanceFetchTimeselection.AllowUserToAddRows = false;
            this.DgvAttendanceFetchTimeselection.AllowUserToDeleteRows = false;
            this.DgvAttendanceFetchTimeselection.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvAttendanceFetchTimeselection.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DgvAttendanceFetchTimeselection.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvAttendanceFetchTimeselection.DefaultCellStyle = dataGridViewCellStyle5;
            this.DgvAttendanceFetchTimeselection.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvAttendanceFetchTimeselection.Location = new System.Drawing.Point(10, 0);
            this.DgvAttendanceFetchTimeselection.Name = "DgvAttendanceFetchTimeselection";
            this.DgvAttendanceFetchTimeselection.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvAttendanceFetchTimeselection.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.DgvAttendanceFetchTimeselection.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.DgvAttendanceFetchTimeselection.Size = new System.Drawing.Size(503, 205);
            this.DgvAttendanceFetchTimeselection.TabIndex = 11;
            // 
            // WzPagemapping1
            // 
            this.WzPagemapping1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WzPagemapping1.BackColor = System.Drawing.Color.Transparent;
            this.WzPagemapping1.Controls.Add(this.Warmappingemp);
            this.WzPagemapping1.Controls.Add(this.CbomappingEmp);
            this.WzPagemapping1.Controls.Add(this.DgvAttendancemapping1);
            this.WzPagemapping1.Controls.Add(this.Label10);
            this.WzPagemapping1.Location = new System.Drawing.Point(7, 102);
            this.WzPagemapping1.Name = "WzPagemapping1";
            this.WzPagemapping1.PageHeaderImage = global::MyBooksERP.Properties.Resources.wizard_setup_3;
            this.WzPagemapping1.PageTitle = "Employee No./Code Identification";
            this.WzPagemapping1.Size = new System.Drawing.Size(523, 274);
            // 
            // 
            // 
            this.WzPagemapping1.Style.Class = "";
            this.WzPagemapping1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPagemapping1.StyleMouseDown.Class = "";
            this.WzPagemapping1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPagemapping1.StyleMouseOver.Class = "";
            this.WzPagemapping1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WzPagemapping1.TabIndex = 13;
            this.WzPagemapping1.Text = "WizardPage7";
            this.WzPagemapping1.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.WzPagemapping1_NextButtonClick);
            this.WzPagemapping1.AfterPageDisplayed += new DevComponents.DotNetBar.WizardPageChangeEventHandler(this.WzPagemapping1_AfterPageDisplayed);
            // 
            // Warmappingemp
            // 
            this.Warmappingemp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.Warmappingemp.CloseButtonVisible = false;
            this.Warmappingemp.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Warmappingemp.Image = ((System.Drawing.Image)(resources.GetObject("Warmappingemp.Image")));
            this.Warmappingemp.Location = new System.Drawing.Point(0, 237);
            this.Warmappingemp.Name = "Warmappingemp";
            this.Warmappingemp.OptionsButtonVisible = false;
            this.Warmappingemp.Size = new System.Drawing.Size(523, 37);
            this.Warmappingemp.TabIndex = 18;
            this.Warmappingemp.Text = "<b>Warning Box</b> control with <i>text-markup</i> support.";
            this.Warmappingemp.Visible = false;
            // 
            // CbomappingEmp
            // 
            this.CbomappingEmp.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CbomappingEmp.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CbomappingEmp.DisplayMember = "Text";
            this.CbomappingEmp.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CbomappingEmp.DropDownHeight = 134;
            this.CbomappingEmp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbomappingEmp.FormattingEnabled = true;
            this.CbomappingEmp.IntegralHeight = false;
            this.CbomappingEmp.ItemHeight = 14;
            this.CbomappingEmp.Location = new System.Drawing.Point(379, 214);
            this.CbomappingEmp.Name = "CbomappingEmp";
            this.CbomappingEmp.Size = new System.Drawing.Size(121, 20);
            this.CbomappingEmp.TabIndex = 14;
            // 
            // DgvAttendancemapping1
            // 
            this.DgvAttendancemapping1.AllowUserToAddRows = false;
            this.DgvAttendancemapping1.AllowUserToDeleteRows = false;
            this.DgvAttendancemapping1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvAttendancemapping1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.DgvAttendancemapping1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvAttendancemapping1.DefaultCellStyle = dataGridViewCellStyle8;
            this.DgvAttendancemapping1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvAttendancemapping1.Location = new System.Drawing.Point(10, 0);
            this.DgvAttendancemapping1.Name = "DgvAttendancemapping1";
            this.DgvAttendancemapping1.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvAttendancemapping1.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.DgvAttendancemapping1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.DgvAttendancemapping1.Size = new System.Drawing.Size(503, 205);
            this.DgvAttendancemapping1.TabIndex = 13;
            // 
            // Label10
            // 
            this.Label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label10.BackColor = System.Drawing.Color.Transparent;
            this.Label10.Location = new System.Drawing.Point(12, 216);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(394, 18);
            this.Label10.TabIndex = 13;
            this.Label10.Text = "Which of the above listed column is having your Employee number or code?";
            // 
            // WzPagemapping2
            // 
            this.WzPagemapping2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WzPagemapping2.BackColor = System.Drawing.Color.Transparent;
            this.WzPagemapping2.Controls.Add(this.Cbomapdateformat);
            this.WzPagemapping2.Controls.Add(this.Label15);
            this.WzPagemapping2.Controls.Add(this.Warmappingdate);
            this.WzPagemapping2.Controls.Add(this.CbomappingDate);
            this.WzPagemapping2.Controls.Add(this.DgvAttendanceFetchmapping2);
            this.WzPagemapping2.Controls.Add(this.Label11);
            this.WzPagemapping2.Location = new System.Drawing.Point(7, 102);
            this.WzPagemapping2.Name = "WzPagemapping2";
            this.WzPagemapping2.PageHeaderImage = global::MyBooksERP.Properties.Resources.wizard_setup_3;
            this.WzPagemapping2.PageTitle = "Date Identification";
            this.WzPagemapping2.Size = new System.Drawing.Size(523, 274);
            // 
            // 
            // 
            this.WzPagemapping2.Style.Class = "";
            this.WzPagemapping2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPagemapping2.StyleMouseDown.Class = "";
            this.WzPagemapping2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPagemapping2.StyleMouseOver.Class = "";
            this.WzPagemapping2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WzPagemapping2.TabIndex = 14;
            this.WzPagemapping2.Text = "WizardPage8";
            this.WzPagemapping2.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.WzPagemapping2_NextButtonClick);
            this.WzPagemapping2.AfterPageDisplayed += new DevComponents.DotNetBar.WizardPageChangeEventHandler(this.WzPagemapping2_AfterPageDisplayed);
            // 
            // Cbomapdateformat
            // 
            this.Cbomapdateformat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Cbomapdateformat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cbomapdateformat.DisplayMember = "Text";
            this.Cbomapdateformat.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.Cbomapdateformat.DropDownHeight = 134;
            this.Cbomapdateformat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cbomapdateformat.FormattingEnabled = true;
            this.Cbomapdateformat.IntegralHeight = false;
            this.Cbomapdateformat.ItemHeight = 14;
            this.Cbomapdateformat.Location = new System.Drawing.Point(223, 213);
            this.Cbomapdateformat.Name = "Cbomapdateformat";
            this.Cbomapdateformat.Size = new System.Drawing.Size(290, 20);
            this.Cbomapdateformat.TabIndex = 20;
            // 
            // Label15
            // 
            this.Label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label15.BackColor = System.Drawing.Color.Transparent;
            this.Label15.Location = new System.Drawing.Point(5, 215);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(221, 18);
            this.Label15.TabIndex = 19;
            this.Label15.Text = "Which date format is displayed in your table?\r\n\r\n\r\n";
            // 
            // Warmappingdate
            // 
            this.Warmappingdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.Warmappingdate.CloseButtonVisible = false;
            this.Warmappingdate.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Warmappingdate.Image = ((System.Drawing.Image)(resources.GetObject("Warmappingdate.Image")));
            this.Warmappingdate.Location = new System.Drawing.Point(0, 248);
            this.Warmappingdate.Name = "Warmappingdate";
            this.Warmappingdate.OptionsButtonVisible = false;
            this.Warmappingdate.Size = new System.Drawing.Size(523, 26);
            this.Warmappingdate.TabIndex = 18;
            this.Warmappingdate.Text = "<b>Warning Box</b> control with <i>text-markup</i> support.";
            this.Warmappingdate.Visible = false;
            // 
            // CbomappingDate
            // 
            this.CbomappingDate.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CbomappingDate.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CbomappingDate.DisplayMember = "Text";
            this.CbomappingDate.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CbomappingDate.DropDownHeight = 134;
            this.CbomappingDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbomappingDate.FormattingEnabled = true;
            this.CbomappingDate.IntegralHeight = false;
            this.CbomappingDate.ItemHeight = 14;
            this.CbomappingDate.Location = new System.Drawing.Point(377, 187);
            this.CbomappingDate.Name = "CbomappingDate";
            this.CbomappingDate.Size = new System.Drawing.Size(136, 20);
            this.CbomappingDate.TabIndex = 17;
            // 
            // DgvAttendanceFetchmapping2
            // 
            this.DgvAttendanceFetchmapping2.AllowUserToAddRows = false;
            this.DgvAttendanceFetchmapping2.AllowUserToDeleteRows = false;
            this.DgvAttendanceFetchmapping2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvAttendanceFetchmapping2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.DgvAttendanceFetchmapping2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvAttendanceFetchmapping2.DefaultCellStyle = dataGridViewCellStyle11;
            this.DgvAttendanceFetchmapping2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvAttendanceFetchmapping2.Location = new System.Drawing.Point(10, 0);
            this.DgvAttendanceFetchmapping2.Name = "DgvAttendanceFetchmapping2";
            this.DgvAttendanceFetchmapping2.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvAttendanceFetchmapping2.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.DgvAttendanceFetchmapping2.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.DgvAttendanceFetchmapping2.Size = new System.Drawing.Size(503, 179);
            this.DgvAttendanceFetchmapping2.TabIndex = 16;
            // 
            // Label11
            // 
            this.Label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label11.BackColor = System.Drawing.Color.Transparent;
            this.Label11.Location = new System.Drawing.Point(5, 189);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(302, 18);
            this.Label11.TabIndex = 15;
            this.Label11.Text = "Which column in the table displays the date?\r\n\r\n";
            // 
            // WzPagemapping3
            // 
            this.WzPagemapping3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WzPagemapping3.BackColor = System.Drawing.Color.Transparent;
            this.WzPagemapping3.Controls.Add(this.rbtimedate1);
            this.WzPagemapping3.Controls.Add(this.rbtimedate);
            this.WzPagemapping3.Controls.Add(this.Label16);
            this.WzPagemapping3.Controls.Add(this.Warmappingtime);
            this.WzPagemapping3.Controls.Add(this.LstMappingtime);
            this.WzPagemapping3.Controls.Add(this.DgvAttendancefetchmapping3);
            this.WzPagemapping3.Controls.Add(this.Label12);
            this.WzPagemapping3.Location = new System.Drawing.Point(7, 102);
            this.WzPagemapping3.Name = "WzPagemapping3";
            this.WzPagemapping3.PageHeaderImage = global::MyBooksERP.Properties.Resources.wizard_setup_3;
            this.WzPagemapping3.PageTitle = "Date && Time Stamp Identification";
            this.WzPagemapping3.Size = new System.Drawing.Size(523, 274);
            // 
            // 
            // 
            this.WzPagemapping3.Style.Class = "";
            this.WzPagemapping3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPagemapping3.StyleMouseDown.Class = "";
            this.WzPagemapping3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPagemapping3.StyleMouseOver.Class = "";
            this.WzPagemapping3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WzPagemapping3.TabIndex = 15;
            this.WzPagemapping3.Text = "WizardPage9";
            this.WzPagemapping3.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.WzPagemapping3_NextButtonClick);
            this.WzPagemapping3.AfterPageDisplayed += new DevComponents.DotNetBar.WizardPageChangeEventHandler(this.WzPagemapping3_AfterPageDisplayed);
            // 
            // rbtimedate1
            // 
            this.rbtimedate1.AutoSize = true;
            this.rbtimedate1.Location = new System.Drawing.Point(316, 218);
            this.rbtimedate1.Name = "rbtimedate1";
            this.rbtimedate1.Size = new System.Drawing.Size(39, 17);
            this.rbtimedate1.TabIndex = 27;
            this.rbtimedate1.Text = "No";
            this.rbtimedate1.UseVisualStyleBackColor = true;
            // 
            // rbtimedate
            // 
            this.rbtimedate.AutoSize = true;
            this.rbtimedate.Checked = true;
            this.rbtimedate.Location = new System.Drawing.Point(261, 218);
            this.rbtimedate.Name = "rbtimedate";
            this.rbtimedate.Size = new System.Drawing.Size(43, 17);
            this.rbtimedate.TabIndex = 26;
            this.rbtimedate.TabStop = true;
            this.rbtimedate.Text = "Yes";
            this.rbtimedate.UseVisualStyleBackColor = true;
            // 
            // Label16
            // 
            this.Label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label16.BackColor = System.Drawing.Color.Transparent;
            this.Label16.Location = new System.Drawing.Point(7, 219);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(423, 20);
            this.Label16.TabIndex = 25;
            this.Label16.Text = "Does the time display time and date?\r\n\r\n\r\n";
            // 
            // Warmappingtime
            // 
            this.Warmappingtime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.Warmappingtime.CloseButtonVisible = false;
            this.Warmappingtime.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Warmappingtime.Image = ((System.Drawing.Image)(resources.GetObject("Warmappingtime.Image")));
            this.Warmappingtime.Location = new System.Drawing.Point(0, 248);
            this.Warmappingtime.Name = "Warmappingtime";
            this.Warmappingtime.OptionsButtonVisible = false;
            this.Warmappingtime.Size = new System.Drawing.Size(523, 26);
            this.Warmappingtime.TabIndex = 20;
            this.Warmappingtime.Text = "<b>Warning Box</b> control with <i>text-markup</i> support.";
            this.Warmappingtime.Visible = false;
            // 
            // LstMappingtime
            // 
            // 
            // 
            // 
            this.LstMappingtime.Border.Class = "ListViewBorder";
            this.LstMappingtime.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LstMappingtime.FullRowSelect = true;
            this.LstMappingtime.Location = new System.Drawing.Point(261, 152);
            this.LstMappingtime.Name = "LstMappingtime";
            this.LstMappingtime.Size = new System.Drawing.Size(252, 60);
            this.LstMappingtime.TabIndex = 19;
            this.LstMappingtime.UseCompatibleStateImageBehavior = false;
            this.LstMappingtime.View = System.Windows.Forms.View.List;
            // 
            // DgvAttendancefetchmapping3
            // 
            this.DgvAttendancefetchmapping3.AllowUserToAddRows = false;
            this.DgvAttendancefetchmapping3.AllowUserToDeleteRows = false;
            this.DgvAttendancefetchmapping3.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvAttendancefetchmapping3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.DgvAttendancefetchmapping3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvAttendancefetchmapping3.DefaultCellStyle = dataGridViewCellStyle14;
            this.DgvAttendancefetchmapping3.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvAttendancefetchmapping3.Location = new System.Drawing.Point(10, 0);
            this.DgvAttendancefetchmapping3.Name = "DgvAttendancefetchmapping3";
            this.DgvAttendancefetchmapping3.ReadOnly = true;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvAttendancefetchmapping3.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.DgvAttendancefetchmapping3.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.DgvAttendancefetchmapping3.Size = new System.Drawing.Size(503, 146);
            this.DgvAttendancefetchmapping3.TabIndex = 18;
            // 
            // Label12
            // 
            this.Label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label12.BackColor = System.Drawing.Color.Transparent;
            this.Label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.Location = new System.Drawing.Point(6, 152);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(220, 28);
            this.Label12.TabIndex = 17;
            this.Label12.Text = "Which of these columns represents the date/time stamps?";
            // 
            // WzPagemappingdesc
            // 
            this.WzPagemappingdesc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WzPagemappingdesc.BackColor = System.Drawing.Color.Transparent;
            this.WzPagemappingdesc.Controls.Add(this.Label13);
            this.WzPagemappingdesc.Location = new System.Drawing.Point(7, 102);
            this.WzPagemappingdesc.Name = "WzPagemappingdesc";
            this.WzPagemappingdesc.PageHeaderImage = global::MyBooksERP.Properties.Resources.wizard_setup_3;
            this.WzPagemappingdesc.PageTitle = "Employee Code Mapping (Contd...)";
            this.WzPagemappingdesc.Size = new System.Drawing.Size(523, 274);
            // 
            // 
            // 
            this.WzPagemappingdesc.Style.Class = "";
            this.WzPagemappingdesc.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPagemappingdesc.StyleMouseDown.Class = "";
            this.WzPagemappingdesc.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPagemappingdesc.StyleMouseOver.Class = "";
            this.WzPagemappingdesc.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WzPagemappingdesc.TabIndex = 16;
            this.WzPagemappingdesc.Text = "WizardPage10";
            // 
            // Label13
            // 
            this.Label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label13.BackColor = System.Drawing.Color.Transparent;
            this.Label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label13.Location = new System.Drawing.Point(60, 16);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(421, 238);
            this.Label13.TabIndex = 18;
            this.Label13.Text = resources.GetString("Label13.Text");
            // 
            // WzPageFinalmapping
            // 
            this.WzPageFinalmapping.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WzPageFinalmapping.BackColor = System.Drawing.Color.Transparent;
            this.WzPageFinalmapping.Controls.Add(this.DgvAttendanceMapping);
            this.WzPageFinalmapping.Controls.Add(this.Warmappingfinal);
            this.WzPageFinalmapping.Location = new System.Drawing.Point(7, 102);
            this.WzPageFinalmapping.Name = "WzPageFinalmapping";
            this.WzPageFinalmapping.PageHeaderImage = global::MyBooksERP.Properties.Resources.wizard_setup_3;
            this.WzPageFinalmapping.PageTitle = "Employee Code Mapping";
            this.WzPageFinalmapping.Size = new System.Drawing.Size(523, 274);
            // 
            // 
            // 
            this.WzPageFinalmapping.Style.Class = "";
            this.WzPageFinalmapping.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPageFinalmapping.StyleMouseDown.Class = "";
            this.WzPageFinalmapping.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WzPageFinalmapping.StyleMouseOver.Class = "";
            this.WzPageFinalmapping.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WzPageFinalmapping.TabIndex = 17;
            this.WzPageFinalmapping.Text = "WizardPage11";
            this.WzPageFinalmapping.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.WzPageFinalmapping_NextButtonClick);
            this.WzPageFinalmapping.AfterPageDisplayed += new DevComponents.DotNetBar.WizardPageChangeEventHandler(this.WzPageFinalmapping_AfterPageDisplayed);
            // 
            // DgvAttendanceMapping
            // 
            this.DgvAttendanceMapping.AllowUserToAddRows = false;
            this.DgvAttendanceMapping.AllowUserToOrderColumns = true;
            this.DgvAttendanceMapping.AllowUserToResizeColumns = false;
            this.DgvAttendanceMapping.AllowUserToResizeRows = false;
            this.DgvAttendanceMapping.BackgroundColor = System.Drawing.Color.White;
            this.DgvAttendanceMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvAttendanceMapping.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CboEmployee,
            this.Column2,
            this.CboPunch,
            this.Column1,
            this.Column3});
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvAttendanceMapping.DefaultCellStyle = dataGridViewCellStyle16;
            this.DgvAttendanceMapping.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvAttendanceMapping.Location = new System.Drawing.Point(10, 3);
            this.DgvAttendanceMapping.Name = "DgvAttendanceMapping";
            this.DgvAttendanceMapping.RowHeadersWidth = 30;
            this.DgvAttendanceMapping.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DgvAttendanceMapping.Size = new System.Drawing.Size(498, 229);
            this.DgvAttendanceMapping.TabIndex = 23;
            this.DgvAttendanceMapping.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DgvAttendanceMapping_DataError);
            // 
            // CboEmployee
            // 
            this.CboEmployee.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.CboEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CboEmployee.HeaderText = "Employee Name";
            this.CboEmployee.Name = "CboEmployee";
            this.CboEmployee.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CboEmployee.Width = 246;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "EmployeeNumber";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CboPunch
            // 
            this.CboPunch.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CboPunch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CboPunch.HeaderText = "PunchID";
            this.CboPunch.Name = "CboPunch";
            this.CboPunch.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "employeeid";
            this.Column1.HeaderText = "Equipment No";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "CompanyID";
            this.Column3.Name = "Column3";
            this.Column3.Visible = false;
            // 
            // Warmappingfinal
            // 
            this.Warmappingfinal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.Warmappingfinal.CloseButtonVisible = false;
            this.Warmappingfinal.Image = ((System.Drawing.Image)(resources.GetObject("Warmappingfinal.Image")));
            this.Warmappingfinal.Location = new System.Drawing.Point(10, 238);
            this.Warmappingfinal.Name = "Warmappingfinal";
            this.Warmappingfinal.OptionsButtonVisible = false;
            this.Warmappingfinal.Size = new System.Drawing.Size(503, 33);
            this.Warmappingfinal.TabIndex = 22;
            this.Warmappingfinal.Text = "<b>Warning Box</b> control with <i>text-markup</i> support.";
            this.Warmappingfinal.Visible = false;
            // 
            // WizardPage1
            // 
            this.WizardPage1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WizardPage1.AntiAlias = false;
            this.WizardPage1.BackColor = System.Drawing.Color.Transparent;
            this.WizardPage1.Controls.Add(this.lblfinalmsg);
            this.WizardPage1.Location = new System.Drawing.Point(7, 102);
            this.WizardPage1.Name = "WizardPage1";
            this.WizardPage1.PageDescription = "< Wizard step description >";
            this.WizardPage1.PageHeaderImage = global::MyBooksERP.Properties.Resources.wizard_setup_3;
            this.WizardPage1.Size = new System.Drawing.Size(523, 274);
            // 
            // 
            // 
            this.WizardPage1.Style.Class = "";
            this.WizardPage1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WizardPage1.StyleMouseDown.Class = "";
            this.WizardPage1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WizardPage1.StyleMouseOver.Class = "";
            this.WizardPage1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WizardPage1.TabIndex = 18;
            this.WizardPage1.FinishButtonClick += new System.ComponentModel.CancelEventHandler(this.WizardPage1_FinishButtonClick);
            this.WizardPage1.AfterPageDisplayed += new DevComponents.DotNetBar.WizardPageChangeEventHandler(this.WizardPage1_AfterPageDisplayed);
            // 
            // lblfinalmsg
            // 
            this.lblfinalmsg.Location = new System.Drawing.Point(86, 56);
            this.lblfinalmsg.Name = "lblfinalmsg";
            this.lblfinalmsg.Size = new System.Drawing.Size(373, 61);
            this.lblfinalmsg.TabIndex = 0;
            // 
            // FrmAttendancewizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 423);
            this.Controls.Add(this.AttendanceWizard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAttendancewizard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Automatic Attendance System";
            this.Load += new System.EventHandler(this.FrmAttendancewizard_Load);
            this.AttendanceWizard.ResumeLayout(false);
            this.WzPageOverview.ResumeLayout(false);
            this.WzPageTemplatename.ResumeLayout(false);
            this.WzPageFilebrowse.ResumeLayout(false);
            this.wizardPageLogin.ResumeLayout(false);
            this.WzPageGriddisplay.ResumeLayout(false);
            this.WzPageGriddisplay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendanceFetch)).EndInit();
            this.WzPageFormatdisplay.ResumeLayout(false);
            this.WzPageFormatdisplay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BubbleBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BubbleBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BubbleBar1)).EndInit();
            this.WzPageTimeselection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NumUpDpwnTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendanceFetchTimeselection)).EndInit();
            this.WzPagemapping1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendancemapping1)).EndInit();
            this.WzPagemapping2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendanceFetchmapping2)).EndInit();
            this.WzPagemapping3.ResumeLayout(false);
            this.WzPagemapping3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendancefetchmapping3)).EndInit();
            this.WzPagemappingdesc.ResumeLayout(false);
            this.WzPageFinalmapping.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendanceMapping)).EndInit();
            this.WizardPage1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Timer TmProgress;
        internal DevComponents.DotNetBar.WizardPage WzPageOverview;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label3;
        internal DevComponents.DotNetBar.WizardPage WzPageTemplatename;
        internal DevComponents.DotNetBar.Controls.WarningBox WarningTempname;
        internal System.Windows.Forms.Label Label5;
        internal DevComponents.DotNetBar.Controls.TextBoxX TxtTemplateName;
        internal System.Windows.Forms.Label Label4;
        internal DevComponents.DotNetBar.WizardPage WzPageFilebrowse;
        internal DevComponents.DotNetBar.Controls.ProgressBarX ProgressBarProcess;
        internal DevComponents.DotNetBar.Controls.WarningBox WarningFilebrowse;
        internal DevComponents.DotNetBar.ButtonX BtnFilebrowse;
        internal DevComponents.DotNetBar.Controls.TextBoxX TxtFileBrowse;
        internal System.Windows.Forms.Label Label6;
        internal DevComponents.DotNetBar.WizardPage WzPageGriddisplay;
        internal System.Windows.Forms.LinkLabel Lnkgobackbrowse;
        internal System.Windows.Forms.RadioButton RbHeaderNo;
        internal System.Windows.Forms.RadioButton RbHeaderYes;
        internal System.Windows.Forms.Label Label7;
        internal DevComponents.DotNetBar.Controls.DataGridViewX DgvAttendanceFetch;
        internal DevComponents.DotNetBar.WizardPage WzPageFormatdisplay;
        internal System.Windows.Forms.Label Label8;
        internal DevComponents.DotNetBar.BubbleBar BubbleBar3;
        internal DevComponents.DotNetBar.BubbleBarTab BubbleBarTab3;
        internal DevComponents.DotNetBar.BubbleButton Bubanother;
        internal DevComponents.DotNetBar.BubbleBar BubbleBar2;
        internal DevComponents.DotNetBar.BubbleBarTab BubbleBarTab2;
        internal DevComponents.DotNetBar.BubbleButton Bubmultiple;
        internal DevComponents.DotNetBar.BubbleBar BubbleBar1;
        internal DevComponents.DotNetBar.BubbleBarTab BubbleBarTab1;
        internal DevComponents.DotNetBar.BubbleButton Bubsingle;
        internal System.Windows.Forms.LinkLabel LnkGoback;
        internal System.Windows.Forms.RadioButton Rbanother;
        internal System.Windows.Forms.RadioButton Rbmultiple;
        internal System.Windows.Forms.RadioButton Rbsingle;
        internal DevComponents.DotNetBar.WizardPage WzPageTimeselection;
        internal DevComponents.DotNetBar.Controls.WarningBox Wartimecolumn;
        internal DevComponents.Editors.IntegerInput NumUpDpwnTime;
        internal System.Windows.Forms.Label Label9;
        internal DevComponents.DotNetBar.Controls.DataGridViewX DgvAttendanceFetchTimeselection;
        internal DevComponents.DotNetBar.WizardPage WzPagemapping1;
        internal DevComponents.DotNetBar.Controls.WarningBox Warmappingemp;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx CbomappingEmp;
        internal DevComponents.DotNetBar.Controls.DataGridViewX DgvAttendancemapping1;
        internal System.Windows.Forms.Label Label10;
        internal DevComponents.DotNetBar.WizardPage WzPagemapping2;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx Cbomapdateformat;
        internal System.Windows.Forms.Label Label15;
        internal DevComponents.DotNetBar.Controls.WarningBox Warmappingdate;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx CbomappingDate;
        internal DevComponents.DotNetBar.Controls.DataGridViewX DgvAttendanceFetchmapping2;
        internal System.Windows.Forms.Label Label11;
        internal DevComponents.DotNetBar.WizardPage WzPagemapping3;
        internal System.Windows.Forms.RadioButton rbtimedate1;
        internal System.Windows.Forms.RadioButton rbtimedate;
        internal System.Windows.Forms.Label Label16;
        internal DevComponents.DotNetBar.Controls.WarningBox Warmappingtime;
        internal DevComponents.DotNetBar.Controls.ListViewEx LstMappingtime;
        internal DevComponents.DotNetBar.Controls.DataGridViewX DgvAttendancefetchmapping3;
        internal System.Windows.Forms.Label Label12;
        internal DevComponents.DotNetBar.WizardPage WzPagemappingdesc;
        internal System.Windows.Forms.Label Label13;
        internal DevComponents.DotNetBar.WizardPage WzPageFinalmapping;
        internal DevComponents.DotNetBar.Controls.DataGridViewX DgvAttendanceMapping;
        internal DevComponents.DotNetBar.Controls.WarningBox Warmappingfinal;
        internal DevComponents.DotNetBar.WizardPage WizardPage1;
        internal System.Windows.Forms.Label lblfinalmsg;
        private System.Windows.Forms.DataGridViewComboBoxColumn CboEmployee;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewComboBoxColumn CboPunch;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private DevComponents.DotNetBar.WizardPage wizardPageLogin;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboTableNames;
        private DevComponents.DotNetBar.ButtonX btnTestconnection;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPassword;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        internal DevComponents.DotNetBar.Controls.WarningBox warningSetPassword;
        internal DevComponents.DotNetBar.Controls.ProgressBarX prgBarSetPassword;
        public DevComponents.DotNetBar.Wizard AttendanceWizard;
        internal System.Windows.Forms.Label label14;
    }
}

