﻿namespace MyBooksERP
{
    partial class FrmLeaveFromAttendance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLeaveFromAttendance));
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.PictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.ChkHalfDay = new System.Windows.Forms.CheckBox();
            this.ChkPaid = new System.Windows.Forms.CheckBox();
            this.cboLeaveType = new System.Windows.Forms.ComboBox();
            this.lblLeaveType = new System.Windows.Forms.Label();
            this.lblDateDisplay = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblEmployee = new System.Windows.Forms.Label();
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).BeginInit();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox1
            // 
            this.GroupBox1.BackColor = System.Drawing.Color.White;
            this.GroupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.GroupBox1.Controls.Add(this.PictureBox2);
            this.GroupBox1.Controls.Add(this.lblStatus);
            this.GroupBox1.Location = new System.Drawing.Point(6, 3);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(263, 40);
            this.GroupBox1.TabIndex = 68;
            this.GroupBox1.TabStop = false;
            // 
            // PictureBox2
            // 
            this.PictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.PictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PictureBox2.BackgroundImage")));
            this.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PictureBox2.Location = new System.Drawing.Point(-5, 0);
            this.PictureBox2.Name = "PictureBox2";
            this.PictureBox2.Size = new System.Drawing.Size(32, 33);
            this.PictureBox2.TabIndex = 61;
            this.PictureBox2.TabStop = false;
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(27, 11);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(218, 26);
            this.lblStatus.TabIndex = 60;
            this.lblStatus.Text = "To Affect the leave you have to save the Attendnace form also !\r\n\r\n";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(219, 171);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(50, 23);
            this.btnCancel.TabIndex = 67;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(163, 171);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(50, 23);
            this.btnOk.TabIndex = 66;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.ChkHalfDay);
            this.pnlMain.Controls.Add(this.ChkPaid);
            this.pnlMain.Controls.Add(this.cboLeaveType);
            this.pnlMain.Controls.Add(this.lblLeaveType);
            this.pnlMain.Controls.Add(this.lblDateDisplay);
            this.pnlMain.Controls.Add(this.lblDate);
            this.pnlMain.Controls.Add(this.lblEmployee);
            this.pnlMain.Controls.Add(this.lblEmployeeName);
            this.pnlMain.Location = new System.Drawing.Point(6, 46);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(263, 119);
            this.pnlMain.TabIndex = 65;
            // 
            // ChkHalfDay
            // 
            this.ChkHalfDay.AutoSize = true;
            this.ChkHalfDay.Location = new System.Drawing.Point(82, 81);
            this.ChkHalfDay.Name = "ChkHalfDay";
            this.ChkHalfDay.Size = new System.Drawing.Size(64, 17);
            this.ChkHalfDay.TabIndex = 7;
            this.ChkHalfDay.Text = "HalfDay";
            this.ChkHalfDay.UseVisualStyleBackColor = true;
            //this.ChkHalfDay.CheckedChanged += new System.EventHandler(this.ChkHalfDay_CheckedChanged);
            // 
            // ChkPaid
            // 
            this.ChkPaid.AutoSize = true;
            this.ChkPaid.Location = new System.Drawing.Point(152, 81);
            this.ChkPaid.Name = "ChkPaid";
            this.ChkPaid.Size = new System.Drawing.Size(60, 17);
            this.ChkPaid.TabIndex = 6;
            this.ChkPaid.Text = "Unpaid";
            this.ChkPaid.UseVisualStyleBackColor = true;
            // 
            // cboLeaveType
            // 
            this.cboLeaveType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboLeaveType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboLeaveType.FormattingEnabled = true;
            this.cboLeaveType.Location = new System.Drawing.Point(82, 54);
            this.cboLeaveType.Name = "cboLeaveType";
            this.cboLeaveType.Size = new System.Drawing.Size(163, 21);
            this.cboLeaveType.TabIndex = 5;
            this.cboLeaveType.SelectedIndexChanged += new System.EventHandler(this.cboLeaveType_SelectedIndexChanged);
            this.cboLeaveType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboLeaveType_KeyDown);
            // 
            // lblLeaveType
            // 
            this.lblLeaveType.AutoSize = true;
            this.lblLeaveType.Location = new System.Drawing.Point(3, 54);
            this.lblLeaveType.Name = "lblLeaveType";
            this.lblLeaveType.Size = new System.Drawing.Size(64, 13);
            this.lblLeaveType.TabIndex = 4;
            this.lblLeaveType.Text = "Leave Type";
            // 
            // lblDateDisplay
            // 
            this.lblDateDisplay.AutoSize = true;
            this.lblDateDisplay.Location = new System.Drawing.Point(79, 31);
            this.lblDateDisplay.Name = "lblDateDisplay";
            this.lblDateDisplay.Size = new System.Drawing.Size(30, 13);
            this.lblDateDisplay.TabIndex = 3;
            this.lblDateDisplay.Text = "Date";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(3, 31);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 2;
            this.lblDate.Text = "Date";
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            this.lblEmployee.Location = new System.Drawing.Point(3, 9);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(53, 13);
            this.lblEmployee.TabIndex = 0;
            this.lblEmployee.Text = "Employee";
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.AutoSize = true;
            this.lblEmployeeName.Location = new System.Drawing.Point(79, 9);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(53, 13);
            this.lblEmployeeName.TabIndex = 1;
            this.lblEmployeeName.Text = "Employee";
            // 
            // FrmLeaveFromAttendance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(273, 202);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.pnlMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLeaveFromAttendance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Leave";
            this.Load += new System.EventHandler(this.FrmLeaveFromAttendance_Load);
            this.GroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.PictureBox PictureBox2;
        internal System.Windows.Forms.Label lblStatus;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Panel pnlMain;
        internal System.Windows.Forms.CheckBox ChkHalfDay;
        internal System.Windows.Forms.CheckBox ChkPaid;
        internal System.Windows.Forms.ComboBox cboLeaveType;
        internal System.Windows.Forms.Label lblLeaveType;
        internal System.Windows.Forms.Label lblDateDisplay;
        internal System.Windows.Forms.Label lblDate;
        internal System.Windows.Forms.Label lblEmployee;
        internal System.Windows.Forms.Label lblEmployeeName;
    }
}