﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    /*****************************************************
    * Created By       : Arun
    * Creation Date    : 17 Apr 2012
    * Description      : Handle Attendance Device Settings
    * FromID           : 128
    * ***************************************************/

    public partial class FrmLeaveFromAttendance : Form
    {



        public double PintLeaveCount;

        public bool PblnBtnOk;
        public int PintLeaveType;
        public bool PblnPaid;
        public bool PblnHalfday;
        public string PstrRemarks;
        //public bool PblnIsMorning;

        public int MintEmployeeID;
        string MStrEmployeeName;
        string MStrDate;
        int MintCompanyID;
        int MintLeavePolicyID = 0;

        MessageBoxIcon MmsgICon;
        string MstrMessageCommon = "";
        ArrayList MaMessageArr;//Error Message display
        private clsBLLAttendance MobjclsBLLAttendance;
        private clsConnection MobjclsConnection;
        private ClsNotification MobjClsNotification;
        private ClsLogWriter MobjClsLogs;
         public FrmLeaveFromAttendance(int intEmpID, string strEmpName, int intCmpID, string strDate)
        {
            InitializeComponent();
            MobjclsConnection = new clsConnection();
            MobjClsNotification = new ClsNotification();
            MobjClsLogs = new ClsLogWriter(Application.StartupPath);
            MobjclsBLLAttendance = new clsBLLAttendance();
            MintEmployeeID = intEmpID;
            MStrEmployeeName = strEmpName;
            MStrDate = strDate;
            MintCompanyID = intCmpID;
            lblEmployeeName.Text = strEmpName;
            lblDateDisplay.Text = strDate;
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}



        }

         //#region SetArabicControls
         //private void SetArabicControls()
         //{
         //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
         //    objDAL.SetArabicVersion((int)FormID.LeaveFromAttendance, this);
         //}
         //#endregion SetArabicControls

        private void LoadMessage()
        {
            // Loading Message
            MaMessageArr = new ArrayList();
            try
            {
                MaMessageArr = MobjClsNotification.FillMessageArray((int)FormID.LeaveFromAttendance,9);
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }

        private void FrmLeaveFromAttendance_Load(object sender, EventArgs e)
        {
            LoadMessage();
            LoadCombo();
            //if (ChkHalfDay.Checked == true)
            //{
            //    gBoxAMPM.Enabled = true;
            //}
            //else
            //{
            //    gBoxAMPM.Enabled = true;
            //}
            btnOk.Enabled = true;
            //if (ClsCommonSettings.IsHrPowerEnabled)
            //{
            //    if (ClsCommonSettings.IsArabicView)
            //    {
            //        lblStatus.Text = "تم دمج MyPayfriend مع HRPower. الرجاء استخدام خيار ه ق ق للدخول الإجازة.";
            //    }
            //    else
            //    {
            //        lblStatus.Text = "MyPayfriend is integrated with HRPower.Please use ESS option for leave entry.";
            //    }
            //}

        }
        private void LoadCombo()
        {
            try
            {
                //'filling Leave Combo Excluding Annual and maternity leaves
                //MobjclsConnection.Fill_Combo(cboLeaveType, " SELECT LeaveTypeID, LeaveType FROM PayLeaveTypeReference Where LeaveTypeID not in(4,1) order by LeaveTypeID ", "LeaveTypeID", "LeaveType");
                ////MintLeavePolicyID
                //cboLeaveType.SelectedIndex = -1;

                DataTable datCombos = new DataTable();
                datCombos = MobjclsBLLAttendance.FillCombos(new string[] { "LeavePolicyID", "EmployeeMaster", "EmployeeID=" + MintEmployeeID + "", "", "" });
                if (datCombos != null)
                {
                    if (datCombos.Rows.Count > 0)
                    {
                        MintLeavePolicyID = datCombos.Rows[0]["LeavePolicyID"].ToInt32();
                    }
                }
                datCombos = null;
                datCombos = MobjclsBLLAttendance.FillCombos(new string[] { "DISTINCT LD.LeaveTypeID, LR.LeaveType", "PayLeavePolicyDetail LD Inner  Join PayLeaveTypeReference LR on LD.LeaveTypeID=LR.LeaveTypeID  Inner Join PayLeavePolicyMaster LM on  LM.LeavePolicyID=LD.LeavePolicyID AND LD.LeaveTypeID not in(3,6) AND LM.LeavePolicyID=" + MintLeavePolicyID.ToString() + "", "1=1 order by LD.LeaveTypeID ", "", "" });
                cboLeaveType.DisplayMember = "LeaveType";
                cboLeaveType.ValueMember = "LeaveTypeID";
                cboLeaveType.DataSource = datCombos;

                 //MobjclsConnection.Fill_Combo(cboLeaveType, " SELECT DISTINCT LD.LeaveTypeID, LR.LeaveType FROM  PayLeavePolicyDetail LD "+
                 //                                           " Inner  Join PayLeaveTypeReference LR on LD.LeaveTypeID=LR.LeaveTypeID  " +
                 //                                           " Inner Join PayLeavePolicyMaster LM on  LM.LeavePolicyID=LD.LeavePolicyID AND LD.LeaveTypeID <> 3 AND LM.LeavePolicyID=" + MintLeavePolicyID.ToString() + " order by LD.LeaveTypeID ", "LeaveTypeID", "LeaveType");
                cboLeaveType.SelectedIndex = -1;
                //MintLeavePolicyID
                //SELECT LD.LeaveTypeID, LR.LeaveType FROM  PayLeavePolicyDetail LD
                //Inner  Join PayLeaveTypeReference LR on LD.LeaveTypeID=LR.LeaveTypeID   
                //Inner Join PayLeavePolicyMaster LM on  LM.LeavePolicyID=LD.LeavePolicyID And 






            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }

        }

        private double GetTakenleaves(int iEId, DateTime atnDate, int intType)
        {
            try
            {

                double dTaken = MobjclsBLLAttendance.GetTakenleaves(iEId, atnDate, intType);

                return dTaken;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
                return 0;
            }
        }

        private bool GetEmployeeLeaveStatus()
        {
            try
            {
                int iEmpId = MintEmployeeID;
                int ICmpID = MintCompanyID;
                DateTime dtDate = Convert.ToDateTime(MStrDate).Date;
                int intLeaveType = Convert.ToInt32(cboLeaveType.SelectedValue);

                double MdblMonthlyleave = 0;
                double MdblTakenLeaves = 0;
                //double dblBalanceLeaves = 0;


                   MdblMonthlyleave = MobjclsBLLAttendance.GetEmployeeLeaveStatus(iEmpId, ICmpID, dtDate.Date.ToString("dd MMM yyyy"), intLeaveType);


                    if (MdblMonthlyleave > 0)
                    {
                        string strdate = dtDate.Date.ToString("dd MMM yyyy");
                        MdblTakenLeaves = MobjclsBLLAttendance.GetTakenleaves(iEmpId, Convert.ToDateTime(strdate), intLeaveType);
                    }
                    else
                    {
                        MdblMonthlyleave = 0;
                    }
                
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
                return false;

            }
        }

        private void FillParametr()
        {
            PblnBtnOk = true;
            PintLeaveType = Convert.ToInt32(cboLeaveType.SelectedValue);
            PblnHalfday = ChkHalfDay.Checked;
            PblnPaid = ChkPaid.Checked;
            PstrRemarks = "";
            if (ChkHalfDay.Checked)
            {
                PstrRemarks = "Half day ";
            }
            else
            {
                PstrRemarks = "Full day ";
            }
            if (ChkPaid.Checked)
            {
                PstrRemarks = PstrRemarks + "unpaid ";
            }
            else
            {
                PstrRemarks = PstrRemarks + "paid ";
            }
            PstrRemarks = PstrRemarks + cboLeaveType.Text + " leave taken from Attendance";
            //if (ChkHalfDay.Checked == true)
            //{
            //    if (rbtnAM.Checked == true)
            //    {
            //PblnIsMorning = true;
            //    }
            //    else
            //    {
            //        PblnIsMorning = false;
            //    }
            //}

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(cboLeaveType.SelectedValue) > 0)
            {
                //if (ChkHalfDay.Checked == true)
                //{
                //    if (rbtnAM.Checked == false && rbtnPM.Checked == false)
                //    {
                //        rbtnAM.Checked = true;
                //    }
                //}

                if (GetEmployeeLeaveStatus())
                {
                    FillParametr();
                    this.Close();
                }
                else
                {
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MaMessageArr, 1588, out MmsgICon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cboLeaveType.Focus();
                }

            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            PblnBtnOk = false;
            this.Close();

        }

        private void cboLeaveType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboLeaveType.SelectedValue.ToInt32() > 0)
                {
                    MobjclsBLLAttendance.GetLeaveDetails(MintCompanyID, MintEmployeeID, cboLeaveType.SelectedValue.ToInt32(), MintLeavePolicyID, MStrDate);
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }

        }

        private void cboLeaveType_KeyDown(object sender, KeyEventArgs e)
        {
            cboLeaveType.DroppedDown = false;
        }

        //private void ChkHalfDay_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (ChkHalfDay.Checked == true)
        //    {
        //        gBoxAMPM.Enabled = true;
        //    }
        //    else
        //    {
        //        gBoxAMPM.Enabled = true;
        //    }

        //}

    }
}
