﻿
#region using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Linq;
using Microsoft.VisualBasic;
using System.Collections;
using System.Threading;
using System.IO;
using System.Data.OleDb;

#endregion using

namespace MyBooksERP
{
    // FORM ID=121

    /*****************************************************
   * Created By       : Arun
   * Creation Date    : 16 Apr 2012
   * Description      : Handle Attendance
   * FORM ID          : 121
   * ***************************************************/
    /*****************************************************
    * Modified By      : Sanju
    * Creation Date    : 25 Aug 2013
    * Description      : Code optimization
    * FORM ID          : 121
    * ***************************************************/
    public partial class FrmAttendance : DevComponents.DotNetBar.Office2007Form
    {
        #region DECLARTIONS

        public bool pbChild;
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission

        int MintDeviceID = 0;
        int MintTimingsColumnCnt = 0;

        int MintDisplayMode = 0;
        int MintDetailCount = 0;
        int MintShiftType = 0;
        int MintRowIndexformessge = 0;
        int MintEmployeeID = 0;
        int MintSearchIndex = 0;
        bool MintOnFormLoad = true;//to know now on form load


        double MdblMonthlyleave = 0;
        double MdblTakenLeaves = 0;

        string MstrMessCommon = "";
        string MstrEmployeeName = "";
        string MstrCellTime = "";

        int MintTemplateID = 0;
        int MintTemplateFileType = 0;
        bool MblnFetchData = false;


        bool MblDeleteMeassage = false;
        bool MblnIsConnected = false;//Device Connection State
        bool MblnShowData = false;
        bool MblnCorrectTime = true;
        bool Glbln24HrFormat = ClsCommonSettings.Glb24HourFormat;
        bool MbAutoFill = false;

        private MessageBoxIcon MmessageIcon;
        private ArrayList MsarMessageArr;                  // Error Message display


        ComboBox CboStatusComboBox;

        clsBLLAttendance MobjclsBLLAttendance;
        ClsLogWriter MobjClsLogs;
        ClsNotification MobjClsNotification;
        ClsExportDatagridviewToExcel MobjExportToExcel;

        #endregion DECLARTIONS

        #region CONSTRUCTOR

        public FrmAttendance()
        {
            InitializeComponent();
            MobjclsBLLAttendance = new clsBLLAttendance();
            MobjClsLogs = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotification();
            MobjExportToExcel = new ClsExportDatagridviewToExcel();
            MintDisplayMode = Convert.ToInt16(AttendanceMode.AttendanceManual);
            tmrClearlabels.Interval = ClsCommonSettings.TimerInterval > 0 ? ClsCommonSettings.TimerInterval : 1000;
            btnLive.Visible = ClsCommonSettings.AttendanceLiveEnable;
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }
        //#region SetArabicControls
        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.Attendance , this);
        //}
        //#endregion SetArabicControls
        #endregion CONSTRUCTOR

        #region METHODS

        private void SetPermissions()
        {
            // Function for setting permissions

            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.Attendance, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
               
                clsBLLPermissionSettings ObjPermission = new clsBLLPermissionSettings();
                DataTable dt = ObjPermission.GetMenuPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID);
                dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AttendanceMapping).ToString();
                btnMapping.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = btnMapping.Enabled = true;

            if (MblnAddPermission || MblnUpdatePermission || MblnAddUpdatePermission)
                MblnUpdatePermission = true;
            btnDelete.Enabled = MblnDeletePermission;
            btnSave.Enabled = MblnUpdatePermission;



        }

        private bool FillDevices()
        {
            try
            {
                DataTable datDevices = new DataTable();

                datDevices = MobjclsBLLAttendance.FillCombos(new string[] { "DeviceID,DeviceName +' (' +IPAddress +')'  as DeviceName", "PayAttendanceDeviceSettings", "DeviceTypeID in(" + Convert.ToInt32(AttendnaceDevices.FingerTech) + ")", "DeviceID", "DeviceName" });
                if (datDevices.Rows.Count > 0)
                {
                    for (int intCounter = 0; intCounter <= datDevices.Rows.Count - 1; intCounter++)
                    {
                        DevComponents.DotNetBar.ButtonItem btnItem = new DevComponents.DotNetBar.ButtonItem();
                        btnItem.Text = Convert.ToString(datDevices.Rows[intCounter]["DeviceName"]);
                        btnItem.Name = Convert.ToString(datDevices.Rows[intCounter]["DeviceName"]);
                        btnItem.Tag = Convert.ToString(datDevices.Rows[intCounter]["DeviceID"]);
                        btnItem.Click += new EventHandler(DevicesSubItems_Click);
                        btnDevices.SubItems.Add(btnItem);
                    }
                }


                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }

        private bool FillTemplates()
        {
            //Filling Attendance Templates for csv,accessdb,excel
            try
            {
                DataTable datTemplates = new DataTable();

                datTemplates = MobjclsBLLAttendance.FillCombos(new string[] { "TemplateMasterID,TemplateName", "PayAttendanceTemplateMaster", "FileTypeID IN(" + (int)AttendanceFiletype.Csv + "," + (int)AttendanceFiletype.Excel + "," + (int)AttendanceFiletype.AccessDB + ")" });
                if (datTemplates.Rows.Count > 0)
                {
                    for (int intCounter = 0; intCounter <= datTemplates.Rows.Count - 1; intCounter++)
                    {
                        DevComponents.DotNetBar.ButtonItem btnItem = new DevComponents.DotNetBar.ButtonItem();
                        btnItem.Text = Convert.ToString(datTemplates.Rows[intCounter]["TemplateName"]);
                        btnItem.Name = Convert.ToString(datTemplates.Rows[intCounter]["TemplateName"]);
                        btnItem.Tag = Convert.ToString(datTemplates.Rows[intCounter]["TemplateMasterID"]);
                        btnItem.Click += new EventHandler(TemplateSubItems_Click);
                        btnTemplates.SubItems.Add(btnItem);
                    }
                }


                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }

        private bool GetTemplatedata(int intTimeCount, ref  string strEmpNo, ref string strDate, ref string strTime1, ref string strTime2, ref string sTime3)
        {
            bool blnRetValue = false;

            DataTable DtFileDetils = MobjclsBLLAttendance.GetTemplateFieldDetails(MintTemplateID);
            if (DtFileDetils != null)
            {
                if (DtFileDetils.Rows.Count > 0)
                {
                    for (int iCounter = 0; iCounter <= DtFileDetils.Rows.Count - 1; iCounter++)
                    {

                        if (DtFileDetils.Rows[iCounter]["Templatefield"].ToStringCustom() == "EmployeeNumber")
                            strEmpNo = DtFileDetils.Rows[iCounter]["Actualfield"].ToStringCustom();

                        if (DtFileDetils.Rows[iCounter]["Templatefield"].ToStringCustom() == "Date")
                            strDate = DtFileDetils.Rows[iCounter]["Actualfield"].ToStringCustom();
                        if (intTimeCount == 1)
                        {
                            if (DtFileDetils.Rows[iCounter]["Templatefield"].ToStringCustom() == "Time")
                                strTime1 = DtFileDetils.Rows[iCounter]["Actualfield"].ToStringCustom();

                        }
                        else if (intTimeCount == 2)
                        {
                            if (strTime1 == "")
                            {
                                if (DtFileDetils.Rows[iCounter]["Templatefield"].ToStringCustom() == "Time")
                                    strTime1 = DtFileDetils.Rows[iCounter]["Actualfield"].ToStringCustom();
                            }


                            if (strTime1 != "")
                            {
                                if (DtFileDetils.Rows[iCounter]["Templatefield"].ToStringCustom() == "Time")
                                    strTime2 = DtFileDetils.Rows[iCounter]["Actualfield"].ToStringCustom();
                            }
                        }
                    }
                    blnRetValue = true;
                }
            }
            return blnRetValue;

        }

        private bool FetchData(string sFileName)
        {
            //Fetch data from csv file

            int iFileCount = 0;



            bool blnIsHeaderExists = false;
            int intPunchingCount = 1;
            string strDateFormat = "";
            bool blnIsTimeWithDate = false;

            string strEmpno = "";
            string strDate = "";
            string strTime1 = "";
            string strTime2 = "";
            string strTime3 = "";

            string strExtention = "";


            if (MintTemplateFileType <= 3)
            {
                FileInfo fInfo = new FileInfo(sFileName);
                if (fInfo.Exists)
                    strExtention = fInfo.Extension;
                else
                    return false;

            }

            MblnFetchData = true;

            MobjclsBLLAttendance.GetTemplateDetails(MintTemplateID, ref blnIsHeaderExists, ref intPunchingCount, ref strDateFormat, ref blnIsTimeWithDate);

            GetTemplatedata(intPunchingCount, ref strEmpno, ref strDate, ref strTime1, ref strTime2, ref strTime3);
            //if (ClsCommonSettings.IsArabicView)
            //{
            //    lblAttendnaceStatus.Text = "معالجة البيانات";
            //}
            //else
            //{
                lblAttendnaceStatus.Text = "Processing Data";
            //}
            DataTable dtData = new DataTable();

            lblProgressShow.Visible = true;
            lblPrgressPercentage.Visible = true;
            lblPrgressPercentage.BringToFront();
            lblProgressShow.BringToFront();
            lblProgressShow.Refresh();
            barStatusBottom.Refresh();
            if (MintTemplateFileType == 1)
            {
                //Getting the Id,time,date from the file

                for (int i = 1; i <= 3; i++)
                {
                    dtData.Columns.Add("Column" + i.ToString());
                }
                dtData.Columns.Add("Column4", System.Type.GetType("System.DateTime")); //'for Sorting time
                dtData.Columns.Add("Column5", System.Type.GetType("System.DateTime"));//'for Sorting Date

                using (Microsoft.VisualBasic.FileIO.TextFieldParser myreader = new Microsoft.VisualBasic.FileIO.TextFieldParser(sFileName))
                {

                    myreader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                    myreader.SetDelimiters(",");
                    string[] currentrow;

                    while (!myreader.EndOfData)
                    {

                        iFileCount += 1;
                        currentrow = myreader.ReadFields();
                        string[] sCurrent = new string[5];
                        if (intPunchingCount == 1)
                        {
                            try
                            {
                                sCurrent[0] = currentrow[strEmpno.Replace("Column", "").Trim().ToInt32() - 1];

                                sCurrent[1] = currentrow[strDate.Replace("Column", "").Trim().ToInt32() - 1];
                                DateTime dtResult = new DateTime();
                                if (DateFormat(sCurrent[1], strDateFormat, ref  dtResult))
                                {
                                    sCurrent[1] = dtResult.ToString("dd MMM yyyy");
                                    sCurrent[2] = currentrow[strTime1.Replace("Column", "").Trim().ToInt32() - 1];

                                    if (blnIsTimeWithDate)
                                        sCurrent[3] = Convert.ToDateTime(sCurrent[2]).ToString("dd MMM yyyy HH:mm tt");
                                    else
                                        sCurrent[3] = sCurrent[1] + " " + currentrow[strTime1.Replace("Column", "").Trim().ToInt32() - 1];

                                    sCurrent[4] = dtResult.ToString("dd MMM yyyy");

                                    if (MobjclsBLLAttendance.AttendanceAllReadyExists(MintTemplateID, sCurrent[0], sCurrent[1]) == false)
                                        dtData.Rows.Add(sCurrent);

                                }
                                lblPrgressPercentage.Text = iFileCount.ToString() + " Records processed";

                                lblPrgressPercentage.Refresh();
                            }
                            catch (Exception Ex)
                            {
                                MobjClsLogs.WriteLog("Error on FetchData Reading data from csv:FrmAttendance. TimeCount=1" + Ex.Message.ToString() + "", 2);
                            }
                        }
                        else if (intPunchingCount == 2)
                        {
                            try
                            {
                                sCurrent[0] = currentrow[strEmpno.Replace("Column", "").Trim().ToInt32() - 1];

                                sCurrent[1] = currentrow[strDate.Replace("Column", "").Trim().ToInt32() - 1];
                                DateTime dtResult = new DateTime();
                                if (DateFormat(sCurrent[1], strDateFormat, ref  dtResult))
                                {
                                    sCurrent[1] = dtResult.ToString("dd MMM yyyy");
                                    if (blnIsTimeWithDate)
                                        sCurrent[3] = Convert.ToDateTime(sCurrent[2]).ToString("dd MMM yyyy HH:mm tt");
                                    else
                                        sCurrent[3] = sCurrent[1] + " " + currentrow[strTime1.Replace("Column", "").Trim().ToInt32() - 1];

                                    sCurrent[4] = dtResult.ToString("dd MMM yyyy");

                                    if (MobjclsBLLAttendance.AttendanceAllReadyExists(MintTemplateID, sCurrent[0], sCurrent[1]) == false)
                                    {
                                        dtData.Rows.Add(sCurrent);
                                        sCurrent[2] = currentrow[strTime2.Replace("Column", "").Trim().ToInt32() - 1];
                                        sCurrent[3] = sCurrent[1] + " " + currentrow[strTime2.Replace("Column", "").Trim().ToInt32() - 1];
                                        dtData.Rows.Add(sCurrent);
                                    }
                                }
                                lblPrgressPercentage.Text = iFileCount.ToString() + " Records processed";

                                lblPrgressPercentage.Refresh();
                            }
                            catch (Exception Ex)
                            {
                                MobjClsLogs.WriteLog("Error on FetchData Reading data from csv:FrmAttendance. TimeCount=2" + Ex.Message.ToString() + "", 2);
                            }
                        }
                        Application.DoEvents();
                    }//End While
                    iFileCount = 0;

                }//End Using
            }//File type=1
            else if (MintTemplateFileType == 2)
            {
                for (int i = 1; i <= 3; i++)
                {
                    dtData.Columns.Add("Column" + i.ToString());
                }
                dtData.Columns.Add("Column4", System.Type.GetType("System.DateTime")); //'for Sorting time
                dtData.Columns.Add("Column5", System.Type.GetType("System.DateTime"));//'for Sorting Date

                DataSet ds = new DataSet();
                string strconn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + sFileName + ";Extended Properties=Excel 12.0;";
                OleDbDataAdapter mydata = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", strconn);
                mydata.TableMappings.Add("Table1", "ExcelTest");
                mydata.Fill(ds);

                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    string[] sCurrent = new string[5];
                    if (intPunchingCount == 1)
                    {
                        try
                        {

                            sCurrent[0] = ds.Tables[0].Rows[i][strEmpno.Replace("Column", "").Trim().ToInt32() - 1].ToStringCustom();
                            sCurrent[1] = ds.Tables[0].Rows[i][strDate.Replace("Column", "").Trim().ToInt32() - 1].ToStringCustom();
                            DateTime dtResult = new DateTime();
                            if (DateFormat(sCurrent[1], strDateFormat, ref dtResult))
                            {
                                sCurrent[1] = dtResult.ToString("dd MMM yyyy");
                                sCurrent[2] = ds.Tables[0].Rows[i][strTime1.Replace("Column", "").Trim().ToInt32() - 1].ToStringCustom();
                                if (blnIsTimeWithDate)
                                    sCurrent[3] = Convert.ToDateTime(sCurrent[2]).ToString("dd MMM yyyy HH:mm tt");
                                else
                                    sCurrent[3] = sCurrent[1] + " " + ds.Tables[0].Rows[i][strTime1.Replace("Column", "").Trim().ToInt32() - 1];


                                sCurrent[4] = dtResult.ToString("dd MMM yyyy");

                                if (MobjclsBLLAttendance.AttendanceAllReadyExists(MintTemplateID, sCurrent[0], sCurrent[1]) == false)
                                    dtData.Rows.Add(sCurrent);

                            }
                        }
                        catch (Exception Ex)
                        {
                            MobjClsLogs.WriteLog("Error on FetchData Reading data from Excel:FrmAttendance. TimeCount=1" + Ex.Message.ToString() + "", 2);
                        }
                    }
                    else if (intPunchingCount == 2)
                    {
                        try
                        {
                            sCurrent[0] = ds.Tables[0].Rows[i][strEmpno.Replace("Column", "").Trim().ToInt32() - 1].ToStringCustom();
                            sCurrent[1] = ds.Tables[0].Rows[i][strDate.Replace("Column", "").Trim().ToInt32() - 1].ToStringCustom();
                            DateTime dtResult = new DateTime();
                            if (DateFormat(sCurrent[1], strDateFormat, ref dtResult))
                            {
                                sCurrent[1] = dtResult.ToString("dd MMM yyyy");
                                sCurrent[2] = ds.Tables[0].Rows[i][strTime1.Replace("Column", "").Trim().ToInt32() - 1].ToStringCustom();
                                if (blnIsTimeWithDate)
                                    sCurrent[3] = Convert.ToDateTime(sCurrent[2]).ToString("dd MMM yyyy HH:mm tt");
                                else
                                    sCurrent[3] = sCurrent[1] + " " + ds.Tables[0].Rows[i][strTime1.Replace("Column", "").Trim().ToInt32() - 1];


                                sCurrent[4] = dtResult.ToString("dd MMM yyyy");

                                if (MobjclsBLLAttendance.AttendanceAllReadyExists(MintTemplateID, sCurrent[0], sCurrent[1]) == false)
                                {
                                    dtData.Rows.Add(sCurrent);
                                    sCurrent[2] = ds.Tables[0].Rows[i][strTime2.Replace("Column", "").Trim().ToInt32() - 1].ToStringCustom();
                                    sCurrent[3] = sCurrent[1] + " " + ds.Tables[0].Rows[i][strTime2.Replace("Column", "").Trim().ToInt32() - 1].ToStringCustom();
                                    dtData.Rows.Add(sCurrent);
                                }
                            }
                        }
                        catch (Exception Ex)
                        {
                            MobjClsLogs.WriteLog("Error on FetchData Reading data from Excel:FrmAttendance. TimeCount=2" + Ex.Message.ToString() + "", 2);
                        }
                    }
                    Application.DoEvents();
                }

            }

            else if (MintTemplateFileType == 3)//sExtention = ".mdb" Or sExtention = ".accdb" Then
            {
                string[] sCurrent = new string[5];
                DataTable dtDataFromAccessDb = new DataTable();
                DateTime dResult = new DateTime();
                dtDataFromAccessDb = GetDataFromAccessDb(sFileName, MintTemplateID, strEmpno, strDate, intPunchingCount, strTime1, strTime2, strTime3, strDateFormat);
                if (intPunchingCount == 1)
                {
                    for (int i = 1; i <= 3; i++)
                    {
                        dtData.Columns.Add("Column" + i.ToString());
                    }
                    dtData.Columns.Add("Column4", System.Type.GetType("System.DateTime")); //'for Sorting time
                    dtData.Columns.Add("Column5", System.Type.GetType("System.DateTime"));//'for Sorting Date

                    DataSet ds = new DataSet();

                    //Assingning the Data to DataTable
                    try
                    {
                        for (int i = 0; i <= dtDataFromAccessDb.Rows.Count - 1; i++)
                        {
                            Application.DoEvents();
                            iFileCount += 1;
                            sCurrent[0] = dtDataFromAccessDb.Rows[i][0].ToString();
                            sCurrent[1] = dtDataFromAccessDb.Rows[i][1].ToString();

                            if (sCurrent[1].IndexOf(":") > 0)
                            {

                                if (DateFormat(sCurrent[1], strDateFormat, ref dResult))
                                {
                                    sCurrent[1] = dResult.ToString("dd MMM yyyy");// Format(dResult, "dd MMM yyyy");
                                    string sTm = Convert.ToString(dtDataFromAccessDb.Rows[i][2].ToString());
                                    sCurrent[2] = sTm;
                                    if (blnIsTimeWithDate)
                                    {
                                        sCurrent[3] = Convert.ToDateTime(sCurrent[2]).ToString("dd MMM yyyy HH:mm tt");

                                    }
                                    else
                                    {
                                        sCurrent[3] = sCurrent[1] + " " + sTm;
                                    }

                                    sCurrent[4] = dResult.ToString("dd MMM yyyy");//Format(dResult, "dd MMM yyyy");
                                    //Validating AllReady Exist

                                    if (MobjclsBLLAttendance.AttendanceAllReadyExists(MintTemplateID, sCurrent[0], sCurrent[1]) == false)
                                    {
                                        dtData.Rows.Add(sCurrent);
                                    }
                                }
                            }
                            else
                            {
                                if (DateFormat(sCurrent[1], strDateFormat, ref dResult))
                                {
                                    sCurrent[1] = dResult.ToString("dd MMM yyyy");//Format(dResult, "dd MMM yyyy");
                                    string sTm = Convert.ToString(dtDataFromAccessDb.Rows[i][2]);
                                    sCurrent[2] = sTm;
                                    if (blnIsTimeWithDate)
                                    {
                                        sCurrent[3] = Convert.ToDateTime(sCurrent[2]).ToString("dd MMM yyyy HH:mm tt");
                                    }
                                    else
                                    {
                                        sCurrent[3] = sCurrent[1] + " " + sTm;
                                    }

                                    sCurrent[4] = dResult.ToString("dd MMM yyyy");//Format(dResult, "dd MMM yyyy");
                                    //Validating AllReady Exist
                                    if (MobjclsBLLAttendance.AttendanceAllReadyExists(MintTemplateID, sCurrent[0], sCurrent[1]) == false)
                                    {
                                        dtData.Rows.Add(sCurrent);
                                    }
                                }
                            }
                            Application.DoEvents();
                        }
                    }

                    catch (Exception ex)
                    {
                        MobjClsLogs.WriteLog("Error on FetchData Reading data from mdb or accessdb:FrmAttendance. TimeCount=1" + ex.Message.ToString() + "", 2);
                    }
                }
                else if (intPunchingCount == 2)
                {
                    for (int i = 1; i <= 3; i++)
                    {
                        dtData.Columns.Add("Column" + i.ToString());
                    }
                    dtData.Columns.Add("Column4", System.Type.GetType("System.DateTime")); //'for Sorting time
                    dtData.Columns.Add("Column5", System.Type.GetType("System.DateTime"));//'for Sorting Date
                    try
                    {

                        for (int i = 0; i <= dtDataFromAccessDb.Rows.Count - 1; i++)
                        {
                            Application.DoEvents();
                            iFileCount += 1;
                            sCurrent[0] = dtDataFromAccessDb.Rows[i][0].ToString();
                            sCurrent[1] = dtDataFromAccessDb.Rows[i][1].ToString();
                            if (sCurrent[1].IndexOf(":") > 0)
                            {
                                if (DateFormat(sCurrent[1], strDateFormat, ref dResult))
                                {
                                    sCurrent[1] = dResult.ToString("dd MMM yyyy");//Format(dResult, "dd MMM yyyy");
                                    string sTm = Convert.ToString(dtDataFromAccessDb.Rows[i][2].ToString());
                                    sCurrent[2] = sTm;
                                    if (blnIsTimeWithDate)
                                    {
                                        sCurrent[3] = Convert.ToDateTime(sCurrent[2]).ToString("dd MMM yyyy HH:mm tt");
                                    }
                                    else
                                    {
                                        sCurrent[3] = sCurrent[1] + " " + sTm;
                                    }

                                    sCurrent[4] = dResult.ToString("dd MMM yyyy");//Format(dResult, "dd MMM yyyy");
                                    //'Validating AllReady Exist
                                    if (MobjclsBLLAttendance.AttendanceAllReadyExists(MintTemplateID, sCurrent[0], sCurrent[1]) == false)
                                    {
                                        dtData.Rows.Add(sCurrent);
                                        sTm = Convert.ToString(dtDataFromAccessDb.Rows[i][3]);
                                        sCurrent[2] = sTm;
                                        if (blnIsTimeWithDate)
                                        {
                                            sCurrent[3] = Convert.ToDateTime(sCurrent[2]).ToString("dd MMM yyyy HH:mm tt");
                                        }
                                        else
                                        {
                                            sCurrent[3] = sCurrent[1] + " " + sTm;
                                        }

                                        dtData.Rows.Add(sCurrent);
                                    }
                                }
                                else
                                {
                                    if (DateFormat(sCurrent[1], strDateFormat, ref dResult))
                                    {
                                        sCurrent[1] = dResult.ToString("dd MMM yyyy");//Format(dResult, "dd MMM yyyy");
                                        string sTm = Convert.ToString(dtDataFromAccessDb.Rows[i][2]);
                                        sCurrent[2] = sTm;
                                        if (blnIsTimeWithDate)
                                        {
                                            sCurrent[3] = Convert.ToDateTime(sCurrent[2]).ToString("dd MMM yyyy HH:mm tt");
                                        }
                                        else
                                        {
                                            sCurrent[3] = sCurrent[1] + " " + sTm;
                                        }

                                        sCurrent[4] = dResult.ToString("dd MMM yyyy");//Format(dResult, "dd MMM yyyy");
                                        //'Validating AllReady Exist
                                        if (MobjclsBLLAttendance.AttendanceAllReadyExists(MintTemplateID, sCurrent[0], sCurrent[1]) == false)
                                        {
                                            dtData.Rows.Add(sCurrent);
                                            sTm = Convert.ToString(dtDataFromAccessDb.Rows[i][3]);
                                            sCurrent[2] = sTm;
                                            if (blnIsTimeWithDate)
                                            {
                                                sCurrent[3] = Convert.ToDateTime(sCurrent[2]).ToString("dd MMM yyyy HH:mm tt");
                                            }
                                            else
                                            {
                                                sCurrent[3] = sCurrent[1] + " " + sTm;
                                            }

                                            dtData.Rows.Add(sCurrent);
                                        }
                                    }
                                }
                                Application.DoEvents();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MobjClsLogs.WriteLog("Error on FetchData Reading data from mdb or accessdb:FrmAttendance. TimeCount=2" + ex.Message.ToString() + "", 2);
                    }
                }
            }
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {

                    lblProgressShow.Visible = false;
                    lblPrgressPercentage.Visible = false;

                    lblProgressShow.Refresh();
                    barStatusBottom.Refresh();


                    Application.DoEvents();

                    if (ConvertFetchToValidDataNew(dtData, strDateFormat, blnIsTimeWithDate, MintTemplateID))
                    {

                    }
                    else
                    {
                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1591, out  MmessageIcon);
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        tmrClearlabels.Enabled = true;

                    }

                }
                else
                {
                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1591, out  MmessageIcon);
                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                    tmrClearlabels.Enabled = true;
                }
            }
            else
            {
                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1591, out  MmessageIcon);
                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                tmrClearlabels.Enabled = true;
            }

            MblnFetchData = false;
            return true;
        }
        private DataTable GetDataFromAccessDb(string sFileName, int iTemplateId, string sEmpno, string sDate, int iTimeCount, string sTime1, string sTime2, string sTime3, string sdateFormat)
        {
            // 'sEmpno, sDate, iTimeCount, sTime1, sTime2, sTime3
            try
            {
                string sUserName = "";
                string sPassword = "";
                string sTableName = "";
                string strCon = "";
                string strSelectQuery = "";
                string sCriteria = "";
                string sServerName = "";
                string sDatabaseName = "";
                DataTable dtMyData = new DataTable();
                string sMaxDate = GetMaxAttendanceDate(); //Getting max date from attendancce details

                GetAccessDbAndSqlDbTableDetails(iTemplateId, ref sUserName, ref sPassword, ref sTableName, ref sServerName, ref sCriteria, ref sDatabaseName);// '//getting password
                if (sFileName != "" && sTableName != "")
                {
                    if (sPassword != "")
                    {
                        strCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + sFileName + "; Jet OLEDB:Database Password=" + sPassword;
                    }
                    else
                    {
                        strCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + sFileName;
                    }
                    OleDbConnection MObjOleDbConnection = new OleDbConnection();
                    MObjOleDbConnection.ConnectionString = strCon;
                    MObjOleDbConnection.Open();
                    if (sCriteria != "")
                    {
                        strSelectQuery = sCriteria;
                        if (sMaxDate != "")
                        {
                            sMaxDate = sMaxDate.ToDateTime().AddDays(-60).ToString("dd MMM yyyy");
                            strSelectQuery = strSelectQuery + " where Cdate( format(" + sTableName + "." + sDate + ",'" + sdateFormat + "')) > Cdate(format(#" + sMaxDate + "#,'" + sdateFormat + "'))";
                        }
                    }
                    else
                    {
                        if (iTimeCount == 1)
                        {
                            if (sMaxDate != "")
                            {
                                sMaxDate = sMaxDate.ToDateTime().AddDays(-60).ToString("dd MMM yyyy");
                                strSelectQuery = "Select  " + sEmpno + ",format(" + sDate + ",' " + sdateFormat + "') AS Dates," + sTime1 + " From " + sTableName + " where Cdate(format(" + sDate + ",'" + sdateFormat + "')) > Cdate(format(#" + sMaxDate + "#,'" + sdateFormat + "'))";
                            }
                            else
                            {
                                strSelectQuery = "Select  " + sEmpno + ",format(" + sDate + ",'" + sdateFormat + "') AS Dates," + sTime1 + " From " + sTableName;

                            }
                        }
                        else if (iTimeCount == 2)
                        {
                            if (sMaxDate != "")
                            {
                                sMaxDate = sMaxDate.ToDateTime().AddDays(-60).ToString("dd MMM yyyy");
                                strSelectQuery = "Select  " + sEmpno + ",format(" + sDate + ",'" + sdateFormat + "') AS Dates," + sTime1 + "," + sTime2 + " From " + sTableName + " where format(" + sDate + ",'" + sdateFormat + "') > format(#" + sMaxDate + "#,'" + sdateFormat + "')";
                            }
                            else
                            {
                                strSelectQuery = "Select  " + sEmpno + ",format(" + sDate + ",'" + sdateFormat + "') AS Dates," + sTime1 + "," + sTime2 + " From " + sTableName;
                            }
                        }
                    }
                    OleDbDataAdapter mydata = new OleDbDataAdapter(strSelectQuery, MObjOleDbConnection);
                    mydata.Fill(dtMyData);
                    MObjOleDbConnection.Close();

                    return dtMyData;
                }
                return null;
            }
            catch (Exception ex)
            {
                MobjClsLogs.WriteLog("Error on GetDataFromAccessDb() : " + this.Name + " " + ex.Message.ToString(), 2);
                return null;
                //'MessageBox.Show("Error on " & System.Reflection.MethodBase.GetCurrentMethod.Name & " : " & Me.Name & " " & Ex.Message.ToString(), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
            }
        }
        private bool GetAccessDbAndSqlDbTableDetails(int iTemplateId, ref string sUserName, ref string sPassword, ref string sTableName, ref string sServerName, ref string sCriteria, ref string sDatabaseName)
        {
            sUserName = "";
            sPassword = "";
            sTableName = "";
            sCriteria = "";
            sServerName = "";
            sDatabaseName = "";
            try
            {

                DataTable datData = MobjclsBLLAttendance.FillCombos(new string[] { "[UserName],[Password],[TableName],isnull(ServerName,'')as ServerName,isnull(Criteria,'') as Criteria, isnull (DatabaseName,'') as DatabaseName", "[PayAttendanceTemplateFileDetails]", "[TemplateMasterID]= " + iTemplateId });
                if (datData.Rows.Count > 0)
                {
                    sUserName = Convert.ToString(datData.Rows[0]["UserName"]);
                    sPassword = Convert.ToString(datData.Rows[0]["Password"]);
                    sTableName = Convert.ToString(datData.Rows[0]["TableName"]);
                    sCriteria = Convert.ToString(datData.Rows[0]["Criteria"]);
                    sServerName = Convert.ToString(datData.Rows[0]["ServerName"]);
                    sDatabaseName = Convert.ToString(datData.Rows[0]["DatabaseName"]);
                }

                return true;
            }
            catch (Exception ex)
            {
                MobjClsLogs.WriteLog("Error on GetAccessDbAndSqlDbTableDetails: " + this.Name + " " + ex.Message.ToString(), 2);
                return false;
            }
        }

        private string GetMaxAttendanceDate()
        {
            string sMaxDate = "";
            DataTable datData = MobjclsBLLAttendance.FillCombos(new string[] { "MIN(CONVERT(DATETIME,T.MaxDate,106)) as MaxDate", "(select EM.EmployeeID,Convert(varchar,Max(Convert(datetime,PAM.Date,106)),106) as MaxDate from [PayAttendanceMaster] PAM INNER JOIN EmployeeMaster EM ON EM.EmployeeID=PAM.EmployeeID AND EM.WorkStatusID>5 AND PAM.[AttendenceStatusID]= " + Convert.ToInt32(AttendanceStatus.Present) + " GROUP BY EM.EmployeeID) T", "" });

            if (datData.Rows.Count > 0)
            {
                sMaxDate = datData.Rows[0]["MaxDate"].ToString();
            }
            return sMaxDate;
        }
        private bool Connect(int intDeviceID)//code for finger tech device uncomment if the dll added ,dont remove the code
        {
            bool blnRetValue = false;
            //try
            //{
            //    DataTable DtDevice = MobjclsBLLAttendance.GetFingerTechDeviceSettings(intDeviceID);

            //    if (DtDevice.Rows.Count > 0)
            //    {

            //        string strIPAddress = Convert.ToString(DtDevice.Rows[0]["IPAddress"]);
            //        int intPort = Convert.ToInt32(DtDevice.Rows[0]["Port"]);
            //        int intCommKey = Convert.ToInt32(DtDevice.Rows[0]["CommKey"]);
            //        string strModel = Convert.ToString(DtDevice.Rows[0]["Model"]);
            //        int intDelay = Convert.ToInt32(DtDevice.Rows[0]["Delay"]);
            //        int intTimeout = Convert.ToInt32(DtDevice.Rows[0]["Timeout"]);
            //        int intDeviceNo = Convert.ToInt32(DtDevice.Rows[0]["DeviceNo"]);
            //        int intTemplateID = Convert.ToInt32(DtDevice.Rows[0]["DeviceID"]);

            //        lblAttendnaceStatus.Text = "Attempting To Connect....";
            //        this.Cursor = Cursors.WaitCursor;

            //        if (BioBridgeSDK.Connect_TCPIP(strModel, intDeviceNo, strIPAddress, intPort, intCommKey) == 0)
            //        {
            //            MblnIsConnected = true;
            //            int i = 0;
            //            if (BioBridgeSDK.GetDeviceStatus(6, ref i) == 0)
            //            {
            //                lblAttendnaceStatus.Text = "Connected Successfully!";
            //                MessageBox.Show("Connected Successfully");
            //                MblnIsConnected = true;
            //                blnRetValue = true;
            //            }
            //        }
            //        else
            //        {
            //            MobjClsLogs.WriteLog("Failed To Connect..-IP" + strIPAddress + this.Name + ":", 2);
            //            MblnIsConnected = false;
            //            lblAttendnaceStatus.Text = "Failed To Connect..";
            //        }

            //    }
            //    this.Cursor = Cursors.Default;
            //    tmrClearlabels.Enabled = true;

            //}
            //catch (Exception Ex)
            //{
            //    MblnIsConnected = false;
            //    this.Cursor = Cursors.Default;
            //    tmrClearlabels.Enabled = true;
            //    MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            //}
            return blnRetValue;
        }

        private void Disconnect()//code for finger tech device uncomment if the dll added ,dont remove the code
        {
            //try
            //{

            //    BioBridgeSDK.Disconnect();//Return 0 for Success,or -1 for fails
            //    lblAttendnaceStatus.Text = "Device Disconnected ";
            //    MblnIsConnected = false;
            //    tmrClearlabels.Enabled = true;
            //}
            //catch (Exception Ex)
            //{
            //    tmrClearlabels.Enabled = true;
            //    MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            //}
        }

        private bool ViewLogs()//code for finger tech device uncomment if the dll added ,dont remove the code
        {
            bool blnReadingData = false;
            //try
            //{
            //    //GetGeneralLog(int* EnrollNo, int* Year, int* Month, int* Day, int* Hour, int* Minute, int*
            //    //                        Second, int* VerifyMode, int* InOutMode, int* WorkCode)
            //    //EnrollNo = Enrollment number.
            //    //a. For Access Control Model(e.g. M2/R2, AC900 and etc)
            //    //VerifyMode = 0:(FP/PW/RF), 1:(FP), 2:(PIN), 3:(PW), 4:(RF), 5:(FP&RF), 6:(FP/PW), 7:(FP/
            //    //RF), 8:(PW/RF), 9:(PIN&FP),10:(FP&PW),11:(PW&RF), 12:(FP&PW&RF), 13:(PIN&FP&PW),
            //    //14:(FP&RF/PIN)
            //    //b. For Time Attendance Model (e.g. TA100)
            //    //VerifyMode = 0:Card/PW, 1:FP
            //    //InOutMode = In and Out mode 0:Check in, 1:Check Out, 2:Break, 3:Resume, 4:overtime
            //    //in, 5:overtime out
            //    //WorkCode = Workcode used.
            //    //[Return Value]
            //    //Return 0 for success, or –1 for fail.




            //    this.Cursor = Cursors.WaitCursor;
            //    string[] sCurrent = new string[3];
            //    int intEnrollNo = 0;
            //    int intYear = 0;
            //    int intMonth = 0;
            //    int intDay_Renamed = 0;
            //    int intHr = 0;
            //    int intMin = 0;
            //    int intSec = 0;
            //    int intver = 0;
            //    int intio = 0;
            //    int intWork = 0;
            //    int intLogSize = 0;
            //    DgvPunchingData.RowCount = 0;

            //    if (BioBridgeSDK.ReadGeneralLog(ref intLogSize) == 0)
            //    {
            //        int intLogCount = 0;
            //        DgvPunchingData.ColumnHeadersVisible = true;
            //        while (BioBridgeSDK.GetGeneralLog(ref intEnrollNo, ref intYear, ref intMonth, ref intDay_Renamed, ref intHr, ref intMin, ref intSec, ref intver, ref intio, ref intWork) == 0)
            //        {
            //            string smonth = GetMonth(intMonth);
            //            sCurrent[0] = intEnrollNo.ToString(); //Employee Punch ID
            //            sCurrent[1] = intDay_Renamed + " " + smonth + " " + intYear.ToString();// Date
            //            sCurrent[2] = Convert.ToString(intHr).PadLeft(2, '0') + ":" + Convert.ToString(intMin).PadLeft(2, '0') + ":" + intSec.ToString().PadLeft(2, '0');// Time
            //            // sCurrent[3] = intio.ToString();
            //            intLogCount += 1;
            //            DgvPunchingData.RowCount += 1;
            //            MintRowCount = DgvPunchingData.RowCount - 1;


            //            string strEmpName = "";
            //            int intEmployeeID = 0;
            //            intEmployeeID = MobjclsBLLAttendance.GetEmployee(sCurrent[0], ref strEmpName);


            //            DgvPunchingData.Rows[MintRowCount].Cells["ColDgvEmployeeName"].Value = strEmpName;
            //            DgvPunchingData.Rows[MintRowCount].Cells["ColDgvEmployeeName"].Tag = intEmployeeID;
            //            DgvPunchingData.Rows[MintRowCount].Cells["ColDgvPunchingDate"].Value = Convert.ToDateTime(sCurrent[1]).ToString("dd MMM yyyy");
            //            DgvPunchingData.Rows[MintRowCount].Cells["ColDgvWorkID"].Value = Convert.ToString(sCurrent[0]);
            //            DgvPunchingData.Rows[MintRowCount].Cells["ColDgvTimings"].Value = Convert.ToString(sCurrent[2]);
            //            DgvPunchingData.Rows[MintRowCount].Cells["ColDgvInOutStatus"].Value = "Verified";
            //            DgvPunchingData.Rows[MintRowCount].Cells["ColDgvPunchingResult"].Value = "Verified";


            //            DgvPunchingData.CurrentCell = DgvPunchingData[1, MintRowCount];
            //            DgvPunchingData.Refresh();
            //            lblAttendnaceStatus.Text = "Logs :" + intLogCount;


            //            MobjclsBLLAttendance.ClearAllPools();
            //            MobjclsBLLAttendance.clsDTOAttendanceDeviceLogs .intDeviceID = MintDeviceID;//Device ID
            //            MobjclsBLLAttendance.clsDTOAttendanceDeviceLogs.intEmployeeID = intEmployeeID > 0 ? intEmployeeID : 0;
            //            MobjclsBLLAttendance.clsDTOAttendanceDeviceLogs.strWorkID = sCurrent[0];//Workid
            //            MobjclsBLLAttendance.clsDTOAttendanceDeviceLogs.strAtnDate = Convert.ToDateTime(sCurrent[1]).ToString("dd MMM yyyy");// sCurrent[1];//Atendance  date
            //            MobjclsBLLAttendance.clsDTOAttendanceDeviceLogs.strAtnTime = sCurrent[2];//Timimngs
            //            MobjclsBLLAttendance.clsDTOAttendanceDeviceLogs.strAtnStatus = intWork.ToString();//
            //            MobjclsBLLAttendance.clsDTOAttendanceDeviceLogs.strAtnResult = intver.ToString();//
            //            MobjclsBLLAttendance.clsDTOAttendanceDeviceLogs.strAtnFormat = intio.ToString();//
            //            MobjclsBLLAttendance.clsDTOAttendanceDeviceLogs.intDeviceTypeID = Convert.ToInt32(AttendnaceDevices.FingerTech);
            //            blnReadingData = MobjclsBLLAttendance.SaveAllLogsFromFingerTek();
            //            //Clearing All
            //            intEnrollNo = 0;
            //            intYear = 0;
            //            intMonth = 0;
            //            intDay_Renamed = 0;
            //            intHr = 0;
            //            intMin = 0;
            //            intSec = 0;
            //            intver = 0;
            //            intio = 0;
            //            intWork = 0;
            //            sCurrent[0] = "";
            //            sCurrent[1] = "";
            //            sCurrent[2] = "";
            //            Application.DoEvents();

            //        }

            //    }
            //    else
            //    {
            //        blnReadingData = false;
            //        MessageBox.Show("There was an error while retrieving the logs from the reader.",ClsCommonSettings.MessageCaption,MessageBoxButtons.OK,MessageBoxIcon.Information);
            //        MobjClsLogs.WriteLog("Error on view logs :There was an error while retrieving the logs from the reader." + this.Name + ":", 2);

            //    }
            //    tmrClearlabels.Enabled = true;
            //    this.Cursor = Cursors.Default;           

            //}
            //catch (Exception ex)
            //{
            //    tmrClearlabels.Enabled = true;
            //    blnReadingData = false;
            //    this.Cursor = Cursors.Default;
            //    MobjClsLogs.WriteLog("Error On--View  Logs:" + ex.Message.ToString(), 2);
            //    MessageBox.Show("Error On:Reading Data from Device:" + ex.Message.ToString());
            //}
            return blnReadingData;

        }

        private string GetMonth(int iMonth)
        {
            //Get month name of a year
            string sMonth = "";
            if (iMonth >= 1 && iMonth <= 12)
                return Microsoft.VisualBasic.DateAndTime.MonthName(iMonth, true);

            return sMonth;
        }

        private bool GetEmployeeInfo(string sEmpNo, DateTime atnDate, ref  int iEmpId, ref string sEmpName, ref int iShiftId, ref string sShiftName, ref string sFromtime,
                                   ref string sTotime, ref string sDuration, ref int ishiftType, ref string sAllowedBreakTime, ref int iLate, ref int iEarly, ref int iCmpId, int iTemplateId,
                                     ref int iNoOfTimings, ref string sMinWorkHours, ref int iBuffer)
        {


            //Get employee shift related information
            DataTable dtEmployeeInfo;
            iEmpId = 0;
            sEmpName = "";
            iShiftId = 0;
            sShiftName = "";
            sFromtime = "";
            sTotime = "";
            sDuration = "";
            ishiftType = 0;
            sAllowedBreakTime = "";
            iLate = 0;
            iEarly = 0;
            iBuffer = 0;
            iCmpId = 0;

            iNoOfTimings = 0;
            sMinWorkHours = "0";
            int intDayID = 0;
            intDayID = Convert.ToInt32(atnDate.DayOfWeek) + 1;
            string sFromDate = atnDate.ToString("dd MMM yyyy");
            string sToDate = atnDate.ToString("dd MMM yyyy");

            try
            {

                dtEmployeeInfo = MobjclsBLLAttendance.GetEmployeeInfo(1, sEmpNo, iTemplateId, intDayID, sFromDate, sToDate, iCmpId, iEmpId);

                if (dtEmployeeInfo.Rows.Count > 0)
                {

                    iEmpId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EmployeeID"]);
                    iCmpId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["CompanyID"]);
                    iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftID"]);
                    iLate = Convert.ToInt32(dtEmployeeInfo.Rows[0]["LateAfter"]);
                    iEarly = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EarlyBefore"]);
                    ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                    iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                    sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                    sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["ShiftName"]);
                    sFromtime = Convert.ToString(dtEmployeeInfo.Rows[0]["FromTime"]);
                    sEmpName = Convert.ToString(dtEmployeeInfo.Rows[0]["Employee"]);
                    sTotime = Convert.ToString(dtEmployeeInfo.Rows[0]["ToTime"]);
                    sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                    sAllowedBreakTime = Convert.ToString(dtEmployeeInfo.Rows[0]["AllowedBreakTime"]);
                    iBuffer = Convert.ToInt32(dtEmployeeInfo.Rows[0]["Buffer"]);
                }

                if (iEmpId == 0)
                {
                    dtEmployeeInfo = MobjclsBLLAttendance.GetEmployeeInfo(2, sEmpNo, iTemplateId, intDayID, sFromDate, sToDate, iCmpId, iEmpId);
                    if (dtEmployeeInfo.Rows.Count > 0)
                    {
                        iEmpId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EmployeeID"]);
                        iCmpId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["CompanyID"]);
                        iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftId"]);
                        iLate = Convert.ToInt32(dtEmployeeInfo.Rows[0]["LateAfter"]);
                        iEarly = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EarlyBefore"]);
                        ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                        iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                        sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                        sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["ShiftName"]);
                        sFromtime = Convert.ToString(dtEmployeeInfo.Rows[0]["FromTime"]);
                        sEmpName = Convert.ToString(dtEmployeeInfo.Rows[0]["Employee"]);
                        sTotime = Convert.ToString(dtEmployeeInfo.Rows[0]["ToTime"]);
                        sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                        sAllowedBreakTime = Convert.ToString(dtEmployeeInfo.Rows[0]["AllowedBreakTime"]);
                        iBuffer = Convert.ToInt32(dtEmployeeInfo.Rows[0]["Buffer"]);
                    }
                }

                if (iEmpId > 0)
                {

                    dtEmployeeInfo = MobjclsBLLAttendance.GetEmployeeInfo(3, sEmpNo, iTemplateId, intDayID, sFromDate, sToDate, iCmpId, iEmpId);
                    if (dtEmployeeInfo.Rows.Count > 0)
                    {
                        ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                        iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                        sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                        iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftID"]);
                        sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["ShiftName"]);
                        sFromtime = Convert.ToString(dtEmployeeInfo.Rows[0]["FromTime"]);
                        sTotime = Convert.ToString(dtEmployeeInfo.Rows[0]["ToTime"]);
                        sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                        iBuffer = Convert.ToInt32(dtEmployeeInfo.Rows[0]["Buffer"]);

                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("GetEmployeeInfo--" + Ex.ToString());
                return false;
            }
        }

        private int GetDurationInMinutes(string sDuration)// 'Getting Duration In minutes
        {
            int iDurationInMinutes = 0;
            try
            {
                string[] sHourMinute = new string[2];

                if (sDuration.Contains('.'))
                {
                    sHourMinute = sDuration.Split('.');
                    iDurationInMinutes = Convert.ToInt32(sHourMinute[0]) * 60 + Convert.ToInt32(sHourMinute[1]);

                }
                else if (sDuration.Contains(':'))
                {
                    sHourMinute = sDuration.Split(':');
                    iDurationInMinutes = Convert.ToInt32(sHourMinute[0]) * 60 + Convert.ToInt32(sHourMinute[1]);
                }
                else if (sDuration != "")
                {
                    iDurationInMinutes = Convert.ToInt32(sDuration) * 60;
                }
                return iDurationInMinutes;
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return iDurationInMinutes;
            }
        }

        private string ConvertDate12(DateTime dateString)//Getting time format
        {
            try
            {
                DTPFormatTime.CustomFormat = "hh:mm tt";
                DTPFormatTime.Text = dateString.ToString();
                return DTPFormatTime.Text;
            }
            catch (Exception)
            {
                return " ";
            }
        }

        private bool ConvertFetchToValidDataNew(DataTable dtFetchData, string sdateFormat, bool bdatetime, int iTemplateMasterId)
        {
            //Sorting And HANDLING Night Shift
            bool blnRetValue = false;
            try
            {
                var drID = (from r in dtFetchData.AsEnumerable()
                            select r["column1"]).Distinct();
                var drDate = (from r in dtFetchData.AsEnumerable()
                              orderby r["Column5"] ascending
                              select r["column2"]).Distinct();
                int gCount = 0;

                lblAttendnaceStatus.Text = "";
                lblPrgressPercentage.Text = "";
                lblPrgressPercentage.Refresh();
                DgvAttendance.RowCount = 0;
                barProgressBarAttendance.Minimum = 0;
                barProgressBarAttendance.Value = 0;
                barProgressBarAttendance.Maximum = drID.Count() + 1;

                int iProCount = 0;
                barProgressBarAttendance.Visible = true;
                barProgressBarAttendance.Refresh();
                lblProgressShow.Visible = true;
                lblPrgressPercentage.Visible = true;
                lblPrgressPercentage.BringToFront();
                lblProgressShow.BringToFront();
                lblProgressShow.Refresh();
                barStatusBottom.Refresh();
                //------------------------------

                Application.DoEvents();
                foreach (string ID in drID)
                {

                    foreach (string dteDate in drDate)
                    {


                        int iEmpId = 0;
                        int iShiftId = 0;
                        int iLate = 0;
                        int iEarly = 0;
                        int ishiftType = 0;
                        int iCmpId = 0;
                        int iBuffer = 0;
                        int iNoOfTimings = 0;
                        string sFromTime = "";
                        string sToTime = "";
                        string sDuration = "";
                        string sAllowedBreakTime = "0";
                        string sEmpName = "";
                        string sShiftName = "";
                        string sMinWorkHours = "0";//

                        if (GetEmployeeInfo(Convert.ToString(ID).Trim(), Convert.ToDateTime(dteDate), ref iEmpId, ref sEmpName, ref iShiftId, ref sShiftName,
                                           ref sFromTime, ref  sToTime, ref  sDuration, ref ishiftType, ref  sAllowedBreakTime, ref  iLate, ref iEarly, ref iCmpId, iTemplateMasterId,
                                           ref  iNoOfTimings, ref  sMinWorkHours, ref iBuffer))
                        {


                            int iTempEmpId = 0;
                            int iTempShiftId = 0;
                            int iTempLate = 0;
                            int iTempEarly = 0;
                            int iTempCmpId = 0;
                            int iTempBuffer = 0;
                            int iTempNoOfTimings = 0;
                            string sTempFromTime = "";
                            string sTempToTime = "";
                            string sTempDuration = "";
                            string sTempAllowedBreakTime = "0";
                            string sTempEmpName = "";
                            string sTempShiftName = "";
                            string sTempMinWorkHours = "0";//
                            string sFilterExpression = "";

                            DataRow[] DataRow;
                            if (iBuffer > 0)
                            {
                                string sTemp1FromTime = Convert.ToDateTime(dteDate + " " + sFromTime).ToString("dd MMM yyyy HH:mm tt");
                                string sTemp2FromTime = "";

                                if (GetEmployeeInfo(Convert.ToString(ID).Trim(), Convert.ToDateTime(dteDate).AddDays(1), ref iTempEmpId, ref sTempEmpName, ref iTempShiftId, ref sTempShiftName, ref sTempFromTime,
                                    ref sTempToTime, ref sTempDuration, ref iTempShiftId, ref sTempAllowedBreakTime, ref iTempLate, ref iTempEarly, ref iTempCmpId, iTemplateMasterId, ref iTempNoOfTimings,
                                    ref sTempMinWorkHours, ref iTempBuffer))
                                {
                                    sTemp2FromTime = Convert.ToDateTime(Convert.ToDateTime(dteDate).AddDays(1).ToString("dd MMM yyyy") + " " + sTempFromTime).ToString("dd MMM yyyy HH:mm tt");

                                }
                                else
                                {
                                    sTemp2FromTime = Convert.ToDateTime(Convert.ToDateTime(dteDate).AddDays(1).ToString("dd MMM yyyy") + " " + sFromTime).ToString(" dd MMM yyyy HH:mm tt");

                                }
                                sTemp1FromTime = Convert.ToDateTime(sTemp1FromTime).AddMinutes(-iBuffer).ToString();
                                sTemp2FromTime = Convert.ToDateTime(sTemp2FromTime).AddMinutes(-iBuffer).ToString();
                                sFilterExpression = "column1='" + Convert.ToString(ID).Trim() + "' and column4>='" + sTemp1FromTime + "' and column4<'" + sTemp2FromTime + "'";
                                DataRow = dtFetchData.Select(sFilterExpression);

                            }
                            else
                            {
                                sFilterExpression = "column1='" + ID + "' and column2='" + dteDate + "'";
                                dtFetchData.DefaultView.Sort = "Column3";
                                dtFetchData.DefaultView.RowFilter = sFilterExpression;
                                DataRow = dtFetchData.DefaultView.Table.Select(sFilterExpression);

                            }

                            if (DataRow.Count() > 0)
                            {

                                DgvAttendance.RowCount += 1;
                                gCount = DgvAttendance.RowCount - 1;
                                DgvAttendance.Rows[gCount].Cells["ColDgvStatus"].Value = Convert.ToInt32(AttendanceStatus.Present);
                                DgvAttendance.Rows[gCount].Cells["ColDgvAtnDate"].Value = dteDate.ToString();
                                DgvAttendance.Rows[gCount].Cells["ColDgvDay"].Value = Convert.ToDateTime(dteDate).DayOfWeek;

                                DgvAttendance.Rows[gCount].Cells["ColDgvEmployee"].Tag = iEmpId;
                                DgvAttendance.Rows[gCount].Cells["ColDgvEmployee"].Value = sEmpName;
                                DgvAttendance.Rows[gCount].Cells["ColDgvCompanyID"].Value = iCmpId;
                                DgvAttendance.Rows[gCount].Cells["ColDgvShift"].Tag = iShiftId;
                                DgvAttendance.Rows[gCount].Cells["ColDgvShift"].Value = sShiftName;
                                DgvAttendance.Rows[gCount].Cells["ColDgvPunchID"].Value = ID.ToString();//Workid Saved for deleting handpunch logs
                                if (Glbln24HrFormat == false)
                                {
                                    sFromTime = string.Format("{0:hh:mm tt}", Convert.ToDateTime(sFromTime));
                                    sToTime = string.Format("{0:hh:mm tt}", Convert.ToDateTime(sToTime));
                                    DgvAttendance.Rows[gCount].Cells["ColDgvShiftTimeDisplay"].Value = Convert.ToString(sFromTime + " To " + sToTime + "(Duration:" + sDuration + " )");
                                }
                                else
                                {
                                    DgvAttendance.Rows[gCount].Cells["ColDgvShiftTimeDisplay"].Value = Convert.ToString(sFromTime + " To " + sToTime + "(Duration:" + sDuration + " )");
                                }

                                sAllowedBreakTime = Convert.ToString(sAllowedBreakTime == "" ? 0 : Convert.ToDouble(sAllowedBreakTime));

                                DgvAttendance.Rows[gCount].Cells["ColDgvAllowedBreakTime"].Tag = sAllowedBreakTime == "" ? 0 : Convert.ToDouble(sAllowedBreakTime);

                                sAllowedBreakTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAllowedBreakTime), DateTime.Today));
                                sAllowedBreakTime = Convert.ToDateTime(sAllowedBreakTime).ToString("HH:mm:ss");
                                DgvAttendance.Rows[gCount].Cells["ColDgvAllowedBreakTime"].Value = sAllowedBreakTime;

                                int intStart = DgvAttendance.Columns["ColDgvAddMore"].Index - 1;

                                for (int i = intStart; i <= DataRow.Count() + 1; i += 2)
                                {
                                    int intLoc = i;
                                    int intCnt = i / 2;
                                    DataGridViewTextBoxColumn adcolIn = new DataGridViewTextBoxColumn();
                                    adcolIn.Name = "TimeIn" + intCnt.ToString();
                                    adcolIn.HeaderText = "TimeIn" + intCnt.ToString();
                                    adcolIn.SortMode = DataGridViewColumnSortMode.NotSortable;
                                    adcolIn.Resizable = DataGridViewTriState.False;
                                    adcolIn.Width = 70;
                                    adcolIn.MaxInputLength = 9;

                                    DataGridViewTextBoxColumn adcolout = new DataGridViewTextBoxColumn();
                                    adcolout.Name = "TimeOut" + intCnt.ToString();
                                    adcolout.HeaderText = "TimeOut" + intCnt.ToString();
                                    adcolout.SortMode = DataGridViewColumnSortMode.NotSortable;
                                    adcolout.Resizable = DataGridViewTriState.False;
                                    adcolout.Width = 70;
                                    adcolout.MaxInputLength = 9;

                                    DgvAttendance.Columns.Insert(intLoc + 1, adcolIn);
                                    DgvAttendance.Columns.Insert(intLoc + 2, adcolout);
                                    MintTimingsColumnCnt += 2;

                                }
                                WorkTimeCalculation(DataRow, sFromTime, sdateFormat, bdatetime, iLate, sToTime, gCount, ishiftType, iNoOfTimings, sMinWorkHours, sDuration, iEarly);
                                if (rbtnMonthly.Checked)
                                    DgvAttendance.CurrentCell = DgvAttendance[ColDgvAtnDate.Index, gCount];
                                else
                                    DgvAttendance.CurrentCell = DgvAttendance[ColDgvEmployee.Index, gCount];
                                DgvAttendance.Refresh();
                                blnRetValue = true;
                            }//data row
                        }//get employeeinfo



                    }//for date

                    lblPrgressPercentage.Text = iProCount.ToString() + " Records processed";
                    iProCount += 1;
                    lblPrgressPercentage.Refresh();
                    barProgressBarAttendance.Value += 1;
                    barProgressBarAttendance.Visible = true;
                    barProgressBarAttendance.Refresh();

                }//for id 

                barProgressBarAttendance.Maximum = drID.Count() + 1;
                barProgressBarAttendance.Value = 0;

                lblProgressShow.Visible = false;
                lblPrgressPercentage.Visible = false;
                barProgressBarAttendance.Visible = false;

            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

                MobjClsLogs.WriteLog("ConvertFetchToValidData--" + Ex.ToString(), 2);
                //MessageBox.Show("ConvertFetchToValidData--" + Ex.ToString());
                blnRetValue = false;
            }
            return blnRetValue;
        }

        private bool ConvertFetchToValidData(DataTable dtFetchData, string sdateFormat, bool bdatetime, int iTemplateMasterId)
        {

            try
            {
                var drID = (from r in dtFetchData.AsEnumerable()
                            select r["column1"]).Distinct();
                var drDate = (from r in dtFetchData.AsEnumerable()
                              select r["column2"]).Distinct();
                int gCount = 0;

                lblAttendnaceStatus.Text = "";
                DgvAttendance.RowCount = 0;
                barProgressBarAttendance.Minimum = 0;
                barProgressBarAttendance.Value = 0;
                barProgressBarAttendance.Maximum = drID.Count() + 1;

                int iProCount = 0;
                barProgressBarAttendance.Visible = true;
                barProgressBarAttendance.Refresh();
                lblProgressShow.Visible = true;
                lblPrgressPercentage.Visible = true;
                lblPrgressPercentage.BringToFront();
                lblProgressShow.BringToFront();
                lblProgressShow.Refresh();
                barStatusBottom.Refresh();

                //------------------------------

                Application.DoEvents();
                foreach (string ID in drID)
                {

                    foreach (string dteDate in drDate)
                    {

                        string sFilterExpression = "column1='" + ID + "' and column2='" + dteDate + "'";
                        dtFetchData.DefaultView.Sort = "Column3";
                        DataRow[] DataRow = dtFetchData.DefaultView.Table.Select(sFilterExpression);
                        if (DataRow.Count() > 0)
                        {


                            int iEmpId = 0;
                            string sEmpName = "";
                            int iShiftId = 0;
                            string sFromTime = "";
                            string sToTime = "";
                            string sDuration = "";
                            string sAllowedBreakTime = "0";
                            int iLate = 0;
                            int iEarly = 0;
                            int ishiftType = 0;
                            int iCmpId = 0;
                            string sShiftName = "";

                            int iBuffer = 0;
                            int iNoOfTimings = 0;
                            string sMinWorkHours = "0";//

                            if (GetEmployeeInfo(Convert.ToString(ID).Trim(), Convert.ToDateTime(dteDate), ref iEmpId, ref sEmpName, ref iShiftId, ref sShiftName,
                                               ref sFromTime, ref  sToTime, ref  sDuration, ref ishiftType, ref  sAllowedBreakTime, ref  iLate, ref iEarly, ref iCmpId, iTemplateMasterId,
                                               ref  iNoOfTimings, ref  sMinWorkHours, ref iBuffer))
                            {


                                DgvAttendance.RowCount += 1;
                                gCount = DgvAttendance.RowCount - 1;
                                DgvAttendance.Rows[gCount].Cells["ColDgvStatus"].Value = Convert.ToInt32(AttendanceStatus.Present);
                                DgvAttendance.Rows[gCount].Cells["ColDgvAtnDate"].Value = dteDate.ToString();
                                DgvAttendance.Rows[gCount].Cells["ColDgvDay"].Value = Convert.ToDateTime(dteDate).DayOfWeek;

                                DgvAttendance.Rows[gCount].Cells["ColDgvEmployee"].Tag = iEmpId;
                                DgvAttendance.Rows[gCount].Cells["ColDgvEmployee"].Value = sEmpName;
                                DgvAttendance.Rows[gCount].Cells["ColDgvCompanyID"].Value = iCmpId;
                                DgvAttendance.Rows[gCount].Cells["ColDgvShift"].Tag = iShiftId;
                                DgvAttendance.Rows[gCount].Cells["ColDgvShift"].Value = sShiftName;
                                DgvAttendance.Rows[gCount].Cells["ColDgvPunchID"].Value = ID.ToString();//Workid Saved for deleting handpunch logs
                                if (Glbln24HrFormat == false)
                                {
                                    sFromTime = string.Format("{0:hh:mm tt}", Convert.ToDateTime(sFromTime));
                                    sToTime = string.Format("{0:hh:mm tt}", Convert.ToDateTime(sToTime));
                                    DgvAttendance.Rows[gCount].Cells["ColDgvShiftTimeDisplay"].Value = Convert.ToString(sFromTime + " To " + sToTime + "(Duration:" + sDuration + " )");
                                }
                                else
                                {
                                    DgvAttendance.Rows[gCount].Cells["ColDgvShiftTimeDisplay"].Value = Convert.ToString(sFromTime + " To " + sToTime + "(Duration:" + sDuration + " )");
                                }

                                sAllowedBreakTime = Convert.ToString(sAllowedBreakTime == "" ? 0 : Convert.ToDouble(sAllowedBreakTime));

                                DgvAttendance.Rows[gCount].Cells["ColDgvAllowedBreakTime"].Tag = sAllowedBreakTime == "" ? 0 : Convert.ToDouble(sAllowedBreakTime);

                                sAllowedBreakTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAllowedBreakTime), DateTime.Today));
                                sAllowedBreakTime = Convert.ToDateTime(sAllowedBreakTime).ToString("HH:mm:ss");
                                DgvAttendance.Rows[gCount].Cells["ColDgvAllowedBreakTime"].Value = sAllowedBreakTime;

                                int intStart = DgvAttendance.Columns["ColDgvAddMore"].Index - 1;

                                for (int i = intStart; i <= DataRow.Count() + 1; i += 2)
                                {
                                    int intLoc = i;
                                    int intCnt = i / 2;
                                    DataGridViewTextBoxColumn adcolIn = new DataGridViewTextBoxColumn();
                                    adcolIn.Name = "TimeIn" + intCnt.ToString();
                                    adcolIn.HeaderText = "TimeIn" + intCnt.ToString();
                                    adcolIn.SortMode = DataGridViewColumnSortMode.NotSortable;
                                    adcolIn.Resizable = DataGridViewTriState.False;
                                    adcolIn.Width = 70;
                                    adcolIn.MaxInputLength = 9;
                                    DataGridViewTextBoxColumn adcolout = new DataGridViewTextBoxColumn();
                                    adcolout.Name = "TimeOut" + intCnt.ToString();
                                    adcolout.HeaderText = "TimeOut" + intCnt.ToString();
                                    adcolout.SortMode = DataGridViewColumnSortMode.NotSortable;
                                    adcolout.Resizable = DataGridViewTriState.False;
                                    adcolout.Width = 70;
                                    adcolout.MaxInputLength = 9;
                                    DgvAttendance.Columns.Insert(intLoc + 1, adcolIn);
                                    DgvAttendance.Columns.Insert(intLoc + 2, adcolout);
                                    MintTimingsColumnCnt += 2;

                                }
                                WorkTimeCalculation(DataRow, sFromTime, sdateFormat, bdatetime, iLate, sToTime, gCount, ishiftType, iNoOfTimings, sMinWorkHours, sDuration, iEarly);
                                if (rbtnMonthly.Checked)
                                    DgvAttendance.CurrentCell = DgvAttendance[ColDgvAtnDate.Index, gCount];
                                else
                                    DgvAttendance.CurrentCell = DgvAttendance[ColDgvEmployee.Index, gCount];
                                DgvAttendance.Refresh();

                            }//get employeeinfo

                        }//datarow

                    }//for date

                    lblPrgressPercentage.Text = iProCount.ToString() + " Records processed";
                    iProCount += 1;
                    lblPrgressPercentage.Refresh();
                    barProgressBarAttendance.Value += 1;
                    barProgressBarAttendance.Visible = true;
                    barProgressBarAttendance.Refresh();

                }//for id 

                barProgressBarAttendance.Maximum = drID.Count() + 1;
                barProgressBarAttendance.Value = 0;

                lblProgressShow.Visible = false;
                lblPrgressPercentage.Visible = false;
                barProgressBarAttendance.Visible = false;

                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

                MobjClsLogs.WriteLog("ConvertFetchToValidData--" + Ex.ToString(), 2);
                //MessageBox.Show("ConvertFetchToValidData--" + Ex.ToString());
                return false;
            }
        }

        private bool WorkTimeCalculation(DataRow[] datarow, string sFromTime, string sdateFormat, bool bdatetime,
                                          int iLate, string sToTime, int intRowCount, int iShiftType, int iNoOfTimings, string sMinWorkHours
                                         , string sDuration, int iEarly)
        {
            try
            {
                string sWorkTime = "0";
                string sBreakTime = "0";
                string sOTBreakTime = "0";
                string sOt = "0";
                string sPreviousTime = "";
                bool bShiftChangeTime = true;
                bool bShiftChangeTimeBrkOT = true;
                bool bShiftChangeTimeEarlyWRk = true; //'Special condition
                bool bShiftChangeTimeEarlyBRK = true;
                int intDurationInMinutes = 0;
                int intMinWorkHrsInMinutes = 0;
                int intAdditionalMinutes = 0;
                int intAbsentTime = 0;
                int iShiftOrderNo = 0;
                bool blnIsMinWrkHRsAsOT = false;
                bool blnNightShiftFlag = false;
                DateTime atnDate = Convert.ToDateTime(DgvAttendance.Rows[intRowCount].Cells["ColDgvAtnDate"].Value);
                int intDayID = Convert.ToInt32(atnDate.DayOfWeek) + 1;

                if (datarow.Length % 2 == 1)
                {
                    DgvAttendance.Rows[intRowCount].DefaultCellStyle.ForeColor = Color.IndianRed;
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvValidFlag"].Value = 0;//if Zero then Invalid Data
                }
                else
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvValidFlag"].Value = 1;//if One then ValidData

                }


                //Leave Extension Additional Minutes
                bool bEarly = true;
                bool bLate = true;
                intAdditionalMinutes = MobjclsBLLAttendance.GetLeaveExAddMinutes(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvEmployee"].Tag), Convert.ToDateTime((DgvAttendance.Rows[intRowCount].Cells["ColDgvAtnDate"].Value)));


                DgvAttendance.Rows[intRowCount].Cells["ColDgvShiftOrderNo"].Value = 0;//Dynamic Shift

                DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Value = "00:00:00";
                DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Tag = 0;

                lblLateComing.Visible = false;
                DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Tag = 0;
                lblEarlyGoing.Visible = false;

                //--------------------------------------------------
                if (iShiftType == 3) //Then 'For dynamic shift
                {
                    string sFirstTime = datarow[0]["column3"].ToString();
                    sFirstTime = Convert.ToDateTime(sFirstTime).ToString("HH:mm");
                    iShiftOrderNo = GetShiftOrderNo(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag), sFirstTime, ref sFromTime, ref sToTime, ref sDuration);
                    string sAllowedBreakTime = "0";
                    MobjclsBLLAttendance.GetDynamicShiftDuration(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag), iShiftOrderNo, ref sMinWorkHours, ref sAllowedBreakTime);
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag = Convert.ToInt32(sAllowedBreakTime);
                    sAllowedBreakTime = DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAllowedBreakTime), DateTime.Today).ToString();
                    sAllowedBreakTime = Convert.ToDateTime(sAllowedBreakTime).ToString("HH:mm:ss");
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Value = sAllowedBreakTime;
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvShiftOrderNo"].Value = iShiftOrderNo;
                }

                if (intAdditionalMinutes > 0)
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag = Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag) + Convert.ToInt32(intAdditionalMinutes);

                    bEarly = false;
                    bLate = false;
                }

                intDurationInMinutes = GetDurationInMinutes(sDuration);//Getting Duration In mintes

                int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(intDurationInMinutes), Convert.ToDateTime(sToTime));
                if (tmDurationDiff < 0) //For Night Shift
                {
                    blnNightShiftFlag = false;
                    sFromTime = (Convert.ToDateTime(sFromTime)).ToString();
                    sToTime = Convert.ToString(Convert.ToDateTime(sToTime).AddDays(1));
                }

                blnIsMinWrkHRsAsOT = MobjclsBLLAttendance.isAfterMinWrkHrsAsOT(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag));
                intMinWorkHrsInMinutes = GetDurationInMinutes(sMinWorkHours);//Setting Min Work hrs


                int iBufferForOT = 0;
                int iMinOtMinutes = 0;
                bool blnConsiderBufferForOT = false;
                blnConsiderBufferForOT = MobjclsBLLAttendance.IsConsiderBufferTimeForOT(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag), ref iBufferForOT, ref iMinOtMinutes);

                //--------------------------------Calculation Starting------------------
                if (iShiftType <= 3)
                {

                    for (int j = 0; j <= datarow.Length - 1; j++)
                    {
                        string sTimeValue = datarow[j]["column3"].ToString();
                        sTimeValue = Convert.ToDateTime(sTimeValue).ToString("HH:mm");

                        if (iShiftType == 3)//'Modified for Dynamic Shift
                        {
                            if (blnNightShiftFlag == true)
                            {
                                if (Convert.ToDateTime(sTimeValue) >= Convert.ToDateTime("12:00 AM") &&
                                    Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime("12:00 PM"))
                                { sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString("dd MMM yyyy HH:mm tt"); }



                            }
                        }


                        if (j == 0) //first time' setting LateComing 
                        {
                            //--------------------------------- Attendnace Check first Punch in  next day -----
                            string sFrm = Convert.ToDateTime(sFromTime).ToString("HH:mm tt");
                            string sto = Convert.ToDateTime(sToTime).ToString("HH:mm tt");
                            if (blnNightShiftFlag && Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sto))
                            {
                                if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sFrm) && Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sFrm).AddMinutes(-120))
                                    sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString("dd MMM yyyy HH:mm tt");
                            }
                            //---

                            //'---------------------------------Late Coming
                            if ((Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate)) && (iShiftType != 2) && (bLate == true))
                            {

                                int il = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                if (il > 0)
                                {
                                    string sLate = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(il), DateTime.Today));
                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Value = Convert.ToDateTime(sLate).ToString("HH:mm:ss");
                                }

                                lblLateComing.Tag = 1;
                                lblLateComing.Visible = true;
                                DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Tag = 1;//'if 1 then LateComing=yes
                            }
                            else
                            {
                                lblLateComing.Tag = 0;
                                lblLateComing.Visible = false;
                                DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Value = "00:00:00"; //'if 1 then LateComing=yes
                                DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Tag = 0;
                            }
                        }
                        else //Work and Brek Time Calculation
                        {

                            int intTmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue));

                            string sPrevCurrent = intTmDiff.ToString();

                            if (intTmDiff < 0)
                            {
                                intTmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1));
                                sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString();
                            }

                            if (j % 2 == 1)// 'if mode 1 then wrk time else Break time
                            {
                                //'------------------------------------------------Early going Case Checking

                                if ((Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly)) && (iShiftType != 2) && (bEarly = true))
                                {
                                    int iE = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sTimeValue), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                    if (iE > 0)
                                    {
                                        string sEarly = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iE), DateTime.Today));
                                        DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Value = Convert.ToDateTime(sEarly).ToString("HH:mm:ss");
                                    }
                                    lblEarlyGoing.Tag = 1;

                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Tag = 1; //'Earlygoing yes
                                }

                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                {


                                    if (bShiftChangeTime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime))
                                    {
                                        bShiftChangeTime = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime));
                                        int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));

                                        sOt = Convert.ToString(Convert.ToDouble(sOt) + idiffCurtShiftOT);

                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                    }
                                    else
                                    {
                                        sOt = Convert.ToString(Convert.ToDouble(sOt) + Convert.ToDouble(intTmDiff)); //'WorkTime Calculation 
                                    }

                                }
                                else
                                {
                                    bool bflag = false;
                                    if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sFromTime))
                                    {
                                        bflag = true;
                                    }
                                    if (bShiftChangeTimeEarlyWRk && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime))
                                    {
                                        bShiftChangeTimeEarlyWRk = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sTimeValue));
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                        bflag = true;
                                    }
                                    if (bflag == false)
                                    {
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + Convert.ToDouble(intTmDiff));
                                    }

                                }

                            }
                            else //Break time Calculation
                            {
                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                {
                                    lblEarlyGoing.Visible = false;
                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Value = "00:00:00"; //'Earlygoing No
                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Tag = 0;

                                    if (bShiftChangeTimeBrkOT = true && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                    {
                                        bShiftChangeTimeBrkOT = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                        int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(idiffPrevShitWork));// 'Break Time Calculation
                                        sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(idiffCurtShiftOT));
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                            sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(intTmDiff));// 'Break Time Calculation
                                    }
                                }
                                else
                                {
                                    bool bflag = false;
                                    if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sFromTime).AddMinutes(iLate))
                                    {
                                        bflag = true;
                                    }
                                    if (bShiftChangeTimeEarlyBRK && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sFromTime).AddMinutes(iLate) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate))
                                    {
                                        bShiftChangeTimeEarlyBRK = false;
                                        int idiffPrevShitBreak = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + idiffPrevShitBreak);
                                        bflag = true;
                                    }
                                    if (bflag == false)
                                    {
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(intTmDiff));
                                    }


                                    lblEarlyGoing.Visible = false;
                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Value = "00:00:00"; //'Earlygoing No
                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Tag = 0;
                                    //'-------------------------------------Early Going 

                                }
                            }
                        }
                        sPreviousTime = sTimeValue;
                        if (Glbln24HrFormat == false)
                        {
                            sTimeValue = Convert.ToDateTime(sTimeValue).ToString("hh:mm tt");
                            DgvAttendance.Rows[intRowCount].Cells[j + 3].Value = Convert.ToDateTime(sTimeValue).ToString("hh:mm tt");
                        }
                        else
                        {
                            DgvAttendance.Rows[intRowCount].Cells[j + 3].Value = Convert.ToDateTime(sTimeValue).ToString("HH:mm");
                        }
                    }//for
                }
                else if (iShiftType == 4)
                {
                    sWorkTime = "0";
                    sBreakTime = "0";
                    sOt = "0";
                    int intTotalSplitAbsentTm = 0;
                    int intTotalAbsentTm = 0;


                    intDurationInMinutes = GetSplitShiftTotalDuration(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag));

                    WorktimeCalculationForSplitShift(datarow, Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag), intRowCount, sdateFormat, bdatetime,
                                                    sFromTime, sToTime, iLate, iEarly, bLate, bEarly, "", blnIsMinWrkHRsAsOT, blnConsiderBufferForOT, iBufferForOT, ref sWorkTime, ref sBreakTime, ref sOt, ref intTotalSplitAbsentTm, ref intTotalAbsentTm);

                    if (intDurationInMinutes > intMinWorkHrsInMinutes)  //'Split shift Absent           
                    {
                        if (intAdditionalMinutes > 0)
                            intAbsentTime = intMinWorkHrsInMinutes - (Convert.ToInt32(sWorkTime) + intAdditionalMinutes);
                        else
                            intAbsentTime = intMinWorkHrsInMinutes - Convert.ToInt32(sWorkTime);
                    }
                    else
                    {
                        if (intTotalSplitAbsentTm > (Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag) + iEarly + iLate))
                        {
                            intAbsentTime = intTotalAbsentTm + (intTotalSplitAbsentTm - (Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag) + iEarly + iLate));
                        }
                        else
                        {
                            intAbsentTime = intTotalAbsentTm;
                        }
                    }

                }
                if (blnIsMinWrkHRsAsOT == true)
                {
                    if (Convert.ToDouble(sWorkTime) == Convert.ToDouble(intMinWorkHrsInMinutes))
                    { }//'
                    else if (Convert.ToDouble(sWorkTime) < Convert.ToDouble(intMinWorkHrsInMinutes))
                    {
                        if (intDurationInMinutes > intMinWorkHrsInMinutes)
                        { sOt = "0"; }
                        else// 'duration and minWorkhours are same then Consider Convert.ToInt32(sAllowedBreak) + iEarly + iLate)
                        {
                            if (Convert.ToInt32(sWorkTime) + Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag) + iEarly + iLate < intMinWorkHrsInMinutes)
                            { sOt = "0"; }
                        }
                    }
                    else if (Convert.ToDouble(sWorkTime) > Convert.ToDouble(intMinWorkHrsInMinutes))
                    {
                        int Ot = Convert.ToInt32(sWorkTime) - Convert.ToInt32(intMinWorkHrsInMinutes);
                        sOt = Convert.ToString(Convert.ToInt32(sOt) + Ot);
                        sWorkTime = intMinWorkHrsInMinutes.ToString();
                    }
                }

                if (Convert.ToInt32(sOt) < iMinOtMinutes)
                    sOt = "0";
                if (sOt != "" && sOt != "0")//Ot Template
                    sOt = GetOt(sOt);

                bool blnIsHoliday = false;
                blnIsHoliday = MobjclsBLLAttendance.IsHoliday(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvEmployee"].Tag), Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvCompanyID"].Value), atnDate, intDayID); //'cheking current date is holiday
                if (iShiftType <= 3)
                {
                    if (intAdditionalMinutes > 0)
                        intMinWorkHrsInMinutes = intMinWorkHrsInMinutes - intAdditionalMinutes;
                    if (intDurationInMinutes > intMinWorkHrsInMinutes)//Absent Time Calculation
                    {
                        intAbsentTime = intMinWorkHrsInMinutes - Convert.ToInt32(sWorkTime);
                    }
                    else
                    {
                        intAbsentTime = intDurationInMinutes - (Convert.ToInt32(sWorkTime) + Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag) + iEarly + iLate);
                    }
                }

                int iShortage = 0;
                if (iShiftType == 2 || iShiftType == 4)
                { }
                else
                {
                    if (blnIsHoliday == false)
                    {
                        iShortage = Convert.ToInt32(sBreakTime == "0" ? "0" : sBreakTime) - Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag);
                        if (iShortage > intAbsentTime)
                        {
                            intAbsentTime = iShortage;
                        }
                    }
                }

                DgvAttendance.Rows[intRowCount].Cells["ColDgvAbsentTime"].Value = intAbsentTime > 0 ? intAbsentTime : 0;
                int iAbsent = intAbsentTime > 0 ? intAbsentTime : 0;
                lblShortageTime.ForeColor = iAbsent > 0 ? Color.DarkRed : System.Drawing.SystemColors.ControlText;
                string sShortage = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iAbsent), DateTime.Today));
                lblShortageTime.Text = Convert.ToDateTime(sShortage).ToString("HH:mm:ss");

                //---------------------------------------------------------------------

                lblAllowedBreakTime.Text = Convert.ToString(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Value);

                DgvAttendance.Rows[intRowCount].Cells["ColDgvWorkTime"].Tag = Convert.ToDouble(sWorkTime);
                sWorkTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sWorkTime), DateTime.Today));
                sWorkTime = Convert.ToDateTime(sWorkTime).ToString("HH:mm:ss");
                DgvAttendance.Rows[intRowCount].Cells["ColDgvWorkTime"].Value = sWorkTime;
                lblWorkTime.Text = sWorkTime;


                if (sBreakTime != "" && sBreakTime != "0")
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvBreakTime"].Tag = sBreakTime;
                    sBreakTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sBreakTime), DateTime.Today));
                    sBreakTime = Convert.ToDateTime(sBreakTime).ToString("HH:mm:ss");
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvBreakTime"].Value = sBreakTime;
                    lblBreakTime.Text = sBreakTime;
                }
                else
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvBreakTime"].Tag = 0;
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvBreakTime"].Value = "";
                    lblBreakTime.Text = "00:00:00";
                }

                if (Convert.ToString(DgvAttendance.Rows[intRowCount].Cells["ColDgvWorkTime"].Value) != "" && Convert.ToDouble(sOt) > 0)
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvExcessTime"].Tag = sOt;
                    sOt = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sOt), DateTime.Today));
                    sOt = Convert.ToDateTime(sOt).ToString("HH:mm:ss");
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvExcessTime"].Value = sOt;
                    lblExcessTime.Text = sOt;
                }
                else
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvExcessTime"].Tag = 0;
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvExcessTime"].Value = "";
                    lblExcessTime.Text = "00:00:00";
                }

                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("WorktimeCalculation datarow[]" + Ex.ToString(), 2);
                //MessageBox.Show("WorkTimeCalculation[]--" + Ex.ToString());
                return false;
            }
        }
        private bool WorkTimeCalculationLive(DataRow[] datarow, string sFromTime, string sdateFormat, bool bdatetime,
                                   int iLate, string sToTime, int intRowCount, int iShiftType, int iNoOfTimings, string sMinWorkHours
                                  , string sDuration, int iEarly)
        {
            try
            {
                string sWorkTime = "0";
                string sBreakTime = "0";
                string sOTBreakTime = "0";
                string sOt = "0";
                string sPreviousTime = "";
                bool bShiftChangeTime = true;
                bool bShiftChangeTimeBrkOT = true;
                bool bShiftChangeTimeEarlyWRk = true; //'Special condition
                bool bShiftChangeTimeEarlyBRK = true;
                int intDurationInMinutes = 0;
                int intMinWorkHrsInMinutes = 0;
                int intAdditionalMinutes = 0;
                int intAbsentTime = 0;
                int iShiftOrderNo = 0;
                bool blnIsMinWrkHRsAsOT = false;
                bool blnNightShiftFlag = false;
                DateTime atnDate = Convert.ToDateTime(DgvAttendance.Rows[intRowCount].Cells["ColDgvAtnDate"].Value);
                int intDayID = Convert.ToInt32(atnDate.DayOfWeek) + 1;
                if (datarow.Length % 2 == 1)
                {
                    DgvAttendance.Rows[intRowCount].DefaultCellStyle.ForeColor = Color.IndianRed;
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvValidFlag"].Value = 0;//if Zero then Invalid Data
                }
                else
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvValidFlag"].Value = 1;//if One then ValidData

                }

                //Leave Extension Additional Minutes
                bool bEarly = true;
                bool bLate = true;
                //if (MblnPayExists)//pay friend exists
                //{
                intAdditionalMinutes = MobjclsBLLAttendance.GetLeaveExAddMinutes(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvEmployee"].Tag), Convert.ToDateTime((DgvAttendance.Rows[intRowCount].Cells["ColDgvAtnDate"].Value)));
                //}

                DgvAttendance.Rows[intRowCount].Cells["ColDgvShiftOrderNo"].Value = 0;//Dynamic Shift

                DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Value = "00:00:00";
                DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Tag = 0;

                lblLateComing.Visible = false;
                DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Tag = 0;
                lblEarlyGoing.Visible = false;

                //--------------------------------------------------
                if (iShiftType == 3) //Then 'For dynamic shift
                {
                    string sFirstTime = datarow[0]["AtnDate"].ToString();
                    sFirstTime = Convert.ToDateTime(sFirstTime).ToString("HH:mm");
                    iShiftOrderNo = GetShiftOrderNo(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag), sFirstTime, ref sFromTime, ref sToTime, ref sDuration);
                    string sAllowedBreakTime = "0";
                    MobjclsBLLAttendance.GetDynamicShiftDuration(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag), iShiftOrderNo, ref sMinWorkHours, ref sAllowedBreakTime);
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag = Convert.ToInt32(sAllowedBreakTime);
                    sAllowedBreakTime = DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAllowedBreakTime), DateTime.Today).ToString();
                    sAllowedBreakTime = Convert.ToDateTime(sAllowedBreakTime).ToString("HH:mm:ss");
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Value = sAllowedBreakTime;
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvShiftOrderNo"].Value = iShiftOrderNo;
                }

                if (intAdditionalMinutes > 0)
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag = Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag) + Convert.ToInt32(intAdditionalMinutes);

                    bEarly = false;
                    bLate = false;
                }

                intDurationInMinutes = GetDurationInMinutes(sDuration);//Getting Duration In mintes

                int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(intDurationInMinutes), Convert.ToDateTime(sToTime));
                if (tmDurationDiff < 0) //For Night Shift
                {
                    blnNightShiftFlag = false;
                    sFromTime = (Convert.ToDateTime(sFromTime)).ToString();
                    sToTime = Convert.ToString(Convert.ToDateTime(sToTime).AddDays(1));
                }

                blnIsMinWrkHRsAsOT = MobjclsBLLAttendance.isAfterMinWrkHrsAsOT(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag));
                intMinWorkHrsInMinutes = GetDurationInMinutes(sMinWorkHours);//Setting Min Work hrs


                int iBufferForOT = 0;
                int iMinOtMinutes = 0;
                bool blnConsiderBufferForOT = false;
                blnConsiderBufferForOT = MobjclsBLLAttendance.IsConsiderBufferTimeForOT(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag), ref iBufferForOT, ref iMinOtMinutes);

                //--------------------------------Calculation Starting------------------
                if (iShiftType <= 3)
                {

                    for (int j = 0; j <= datarow.Length - 1; j++)
                    {
                        string sTimeValue = datarow[j]["AtnDate"].ToString();
                        sTimeValue = Convert.ToDateTime(sTimeValue).ToString("HH:mm");

                        if (iShiftType == 3)//'Modified for Dynamic Shift
                        {
                            if (blnNightShiftFlag == true)
                            {
                                if (Convert.ToDateTime(sTimeValue) >= Convert.ToDateTime("12:00 AM") &&
                                    Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime("12:00 PM"))
                                { sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString("dd MMM yyyy HH:mm tt"); }
                            }
                        }


                        if (j == 0) //first time' setting LateComing 
                        {
                            //--------------------------------- Attendnace Check first Punch in  next day -----
                            string sFrm = Convert.ToDateTime(sFromTime).ToString("HH:mm tt");
                            string sto = Convert.ToDateTime(sToTime).ToString("HH:mm tt");
                            if (blnNightShiftFlag && Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sto))
                            {
                                if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sFrm) && Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sFrm).AddMinutes(-120))
                                    sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString("dd MMM yyyy HH:mm tt");
                            }
                            //---

                            //'---------------------------------Late Coming
                            if ((Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate)) && (iShiftType != 2) && (bLate == true))
                            {

                                int il = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                if (il > 0)
                                {
                                    string sLate = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(il), DateTime.Today));
                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Value = Convert.ToDateTime(sLate).ToString("HH:mm:ss");
                                }

                                lblLateComing.Tag = 1;
                                lblLateComing.Visible = true;
                                DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Tag = 1;//'if 1 then LateComing=yes
                            }
                            else
                            {
                                lblLateComing.Tag = 0;
                                lblLateComing.Visible = false;
                                DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Value = "00:00:00"; //'if 1 then LateComing=yes
                                DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Tag = 0;
                            }


                        }
                        else //Work and Brek Time Calculation
                        {

                            int intTmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue));

                            string sPrevCurrent = intTmDiff.ToString();

                            if (intTmDiff < 0)
                            {
                                intTmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1));
                                sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString();
                            }



                            if (j % 2 == 1)// 'if mode 1 then wrk time else Break time
                            {
                                //'------------------------------------------------Early going Case Checking

                                if ((Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly)) && (iShiftType != 2) && (bEarly = true))
                                {
                                    int iE = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sTimeValue), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                    if (iE > 0)
                                    {
                                        string sEarly = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iE), DateTime.Today));
                                        DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Value = Convert.ToDateTime(sEarly).ToString("HH:mm:ss");
                                    }
                                    lblEarlyGoing.Tag = 1;

                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Tag = 1; //'Earlygoing yes
                                }


                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                {


                                    if (bShiftChangeTime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime))
                                    {
                                        bShiftChangeTime = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime));
                                        int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                        sOt = Convert.ToString(Convert.ToDouble(sOt) + idiffCurtShiftOT);
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                    }
                                    else
                                    {
                                        sOt = Convert.ToString(Convert.ToDouble(sOt) + Convert.ToDouble(intTmDiff)); //'WorkTime Calculation 
                                    }

                                }
                                else
                                {
                                    bool bflag = false;
                                    if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sFromTime))
                                    {
                                        bflag = true;
                                    } //'sWorkTime = Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff)
                                    if (bShiftChangeTimeEarlyWRk && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime))
                                    {
                                        bShiftChangeTimeEarlyWRk = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sTimeValue));
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                        bflag = true;
                                    }
                                    if (bflag == false)
                                    {
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + Convert.ToDouble(intTmDiff));
                                    }
                                }

                            }
                            else //Break time Calculation
                            {
                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                {
                                    lblEarlyGoing.Visible = false;
                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Value = "00:00:00"; //'Earlygoing No
                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Tag = 0;

                                    if (bShiftChangeTimeBrkOT = true && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                    {
                                        bShiftChangeTimeBrkOT = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                        int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(idiffPrevShitWork));// 'Break Time Calculation
                                        sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(idiffCurtShiftOT));
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                            sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(intTmDiff));// 'Break Time Calculation
                                    }
                                }
                                else
                                {
                                    bool bflag = false;
                                    if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sFromTime).AddMinutes(iLate))
                                    {
                                        bflag = true;
                                    }
                                    if (bShiftChangeTimeEarlyBRK && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sFromTime).AddMinutes(iLate) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate))
                                    {
                                        bShiftChangeTimeEarlyBRK = false;
                                        int idiffPrevShitBreak = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + idiffPrevShitBreak);
                                        bflag = true;
                                    }
                                    if (bflag == false)
                                    {
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(intTmDiff));
                                    }


                                    lblEarlyGoing.Visible = false;
                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Value = "00:00:00"; //'Earlygoing No
                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Tag = 0;
                                    //'-------------------------------------Early Going 

                                }
                            }
                        }
                        sPreviousTime = sTimeValue;
                        if (Glbln24HrFormat == false)
                        {
                            sTimeValue = Convert.ToDateTime(sTimeValue).ToString("hh:mm tt");
                            DgvAttendance.Rows[intRowCount].Cells[j + 3].Value = Convert.ToDateTime(sTimeValue).ToString("hh:mm tt");
                        }
                        else
                        {
                            DgvAttendance.Rows[intRowCount].Cells[j + 3].Value = Convert.ToDateTime(sTimeValue).ToString("HH:mm");
                        }
                    }//for
                }
                else if (iShiftType == 4)
                {
                    sWorkTime = "0";
                    sBreakTime = "0";
                    sOt = "0";
                    int intTotalSplitAbsentTm = 0;
                    int intTotalAbsentTm = 0;


                    intDurationInMinutes = GetSplitShiftTotalDuration(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag));
                    WorktimeCalculationForSplitShift(datarow, Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag), intRowCount, sdateFormat, bdatetime,
                                                    sFromTime, sToTime, iLate, iEarly, bLate, bEarly, "", blnIsMinWrkHRsAsOT, blnConsiderBufferForOT, iBufferForOT, ref sWorkTime, ref sBreakTime, ref sOt, ref intTotalSplitAbsentTm, ref intTotalAbsentTm);

                    if (intDurationInMinutes > intMinWorkHrsInMinutes)  //'Split shift Absent           
                    {
                        if (intAdditionalMinutes > 0)
                            intAbsentTime = intMinWorkHrsInMinutes - (Convert.ToInt32(sWorkTime) + intAdditionalMinutes);
                        else
                            intAbsentTime = intMinWorkHrsInMinutes - Convert.ToInt32(sWorkTime);
                    }
                    else
                    {
                        if (intTotalSplitAbsentTm > (Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag) + iEarly + iLate))
                        {
                            intAbsentTime = intTotalAbsentTm + (intTotalSplitAbsentTm - (Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag) + iEarly + iLate));
                        }
                        else
                        {
                            intAbsentTime = intTotalAbsentTm;
                        }
                    }

                }
                if (blnIsMinWrkHRsAsOT == true)
                {
                    if (Convert.ToDouble(sWorkTime) == Convert.ToDouble(intMinWorkHrsInMinutes))
                    { }//'
                    else if (Convert.ToDouble(sWorkTime) < Convert.ToDouble(intMinWorkHrsInMinutes))
                    { //'sOt = "0"
                        if (intDurationInMinutes > intMinWorkHrsInMinutes)
                        { sOt = "0"; }
                        else// 'duration and minWorkhours are same then Consider Convert.ToInt32(sAllowedBreak) + iEarly + iLate)
                        {
                            if (Convert.ToInt32(sWorkTime) + Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag) + iEarly + iLate < intMinWorkHrsInMinutes)
                            { sOt = "0"; }
                        }
                    }
                    else if (Convert.ToDouble(sWorkTime) > Convert.ToDouble(intMinWorkHrsInMinutes))
                    {
                        int Ot = Convert.ToInt32(sWorkTime) - Convert.ToInt32(intMinWorkHrsInMinutes);
                        sOt = Convert.ToString(Convert.ToInt32(sOt) + Ot);
                        sWorkTime = intMinWorkHrsInMinutes.ToString();
                    }
                }

                if (Convert.ToInt32(sOt) < iMinOtMinutes)
                    sOt = "0";
                if (sOt != "" && sOt != "0")//Ot Template
                    sOt = GetOt(sOt);

                bool blnIsHoliday = false;
                blnIsHoliday = MobjclsBLLAttendance.IsHoliday(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvEmployee"].Tag), Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvCompanyID"].Value), atnDate, intDayID); //'cheking current date is holiday
                if (iShiftType <= 3)
                {
                    if (intAdditionalMinutes > 0)
                        intMinWorkHrsInMinutes = intMinWorkHrsInMinutes - intAdditionalMinutes;
                    if (intDurationInMinutes > intMinWorkHrsInMinutes)//Absent Time Calculation
                    {
                        intAbsentTime = intMinWorkHrsInMinutes - Convert.ToInt32(sWorkTime);
                    }
                    else
                    {
                        intAbsentTime = intDurationInMinutes - (Convert.ToInt32(sWorkTime) + Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag) + iEarly + iLate);
                    }
                }

                int iShortage = 0;
                if (iShiftType == 2 || iShiftType == 4)
                { }
                else
                {
                    if (blnIsHoliday == false)
                    {
                        iShortage = Convert.ToInt32(sBreakTime == "0" ? "0" : sBreakTime) - Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag);
                        if (iShortage > intAbsentTime)
                        {
                            intAbsentTime = iShortage;
                        }
                    }
                }

                DgvAttendance.Rows[intRowCount].Cells["ColDgvAbsentTime"].Value = intAbsentTime > 0 ? intAbsentTime : 0;
                int iAbsent = intAbsentTime > 0 ? intAbsentTime : 0;
                lblShortageTime.ForeColor = iAbsent > 0 ? Color.DarkRed : System.Drawing.SystemColors.ControlText;
                string sShortage = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iAbsent), DateTime.Today));
                lblShortageTime.Text = Convert.ToDateTime(sShortage).ToString("HH:mm:ss");

                //---------------------------------------------------------------------

                lblAllowedBreakTime.Text = Convert.ToString(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Value);

                DgvAttendance.Rows[intRowCount].Cells["ColDgvWorkTime"].Tag = Convert.ToDouble(sWorkTime);
                sWorkTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sWorkTime), DateTime.Today));
                sWorkTime = Convert.ToDateTime(sWorkTime).ToString("HH:mm:ss");
                DgvAttendance.Rows[intRowCount].Cells["ColDgvWorkTime"].Value = sWorkTime;
                lblWorkTime.Text = sWorkTime;

                if (sBreakTime != "" && sBreakTime != "0")
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvBreakTime"].Tag = sBreakTime;
                    sBreakTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sBreakTime), DateTime.Today));
                    sBreakTime = Convert.ToDateTime(sBreakTime).ToString("HH:mm:ss");
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvBreakTime"].Value = sBreakTime;
                    lblBreakTime.Text = sBreakTime;
                }
                else
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvBreakTime"].Tag = 0;
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvBreakTime"].Value = "";
                    lblBreakTime.Text = "00:00:00";
                }

                if (Convert.ToString(DgvAttendance.Rows[intRowCount].Cells["ColDgvWorkTime"].Value) != "" && Convert.ToDouble(sOt) > 0)
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvExcessTime"].Tag = sOt;
                    sOt = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sOt), DateTime.Today));
                    sOt = Convert.ToDateTime(sOt).ToString("HH:mm:ss");
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvExcessTime"].Value = sOt;
                    lblExcessTime.Text = sOt;
                }
                else
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvExcessTime"].Tag = 0;
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvExcessTime"].Value = "";
                    lblExcessTime.Text = "00:00:00";
                }

                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("WorktimeCalculation datarow[]" + Ex.ToString(), 2);
                //MessageBox.Show("WorkTimeCalculation[]--" + Ex.ToString());
                return false;
            }
        }
        private bool WorktimeCalculationForSplitShift(DataRow[] datarow, int iShiftID, int gCount, string sdateFormat, bool bdatetime,
                                                   string sFromTime, string sToTime, int iLate, int iEarly, bool bLate,
                                                  bool bEarly, string sEmpName, bool isAfterMinWrkHrsAsOT, bool blnConsiderBufferForOT, int iBufferForOT,
                                                  ref string sWorkTime, ref string sBreakTime, ref string sOt, ref int intTotalSplitAbsentTm, ref int intTotalAbsentTm)
        {
            //'Worktime Calculation for Split shift  

            try
            {
                DgvAttendance.Rows[gCount].Cells["ColDgvShiftOrderNo"].Value = 0;//Dynamic Shift
                DgvAttendance.Rows[gCount].Cells["ColDgvLateComing"].Value = "00:00:00";
                DgvAttendance.Rows[gCount].Cells["ColDgvLateComing"].Tag = 0;
                lblLateComing.Visible = false;
                DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Tag = 0;
                lblEarlyGoing.Visible = false;

                sWorkTime = "0";
                sBreakTime = "0";
                sOt = "0";
                string sOvertime = "0";
                bool nightFlag = false;

                int intWorkTm = 0;
                int intBreakTm = 0;
                int intAbsentTm = 0;
                int intSplitAbsentTm = 0;
                intTotalSplitAbsentTm = 0;
                intTotalAbsentTm = 0;

                DataTable DtShiftDetails = MobjclsBLLAttendance.GetDynamicShiftDetails(iShiftID);

                if (DtShiftDetails.Rows.Count > 0)
                {
                    for (int i = 0; i <= DtShiftDetails.Rows.Count - 1; i++)
                    {

                        string sSplitFromTime = Convert.ToString(DtShiftDetails.Rows[i]["FromTime"]);
                        string sSplitToTime = Convert.ToString(DtShiftDetails.Rows[i]["ToTime"]);
                        string sDuration = Convert.ToString(DtShiftDetails.Rows[i]["Duration"]);
                        int iMinWorkingHrsMin = GetDurationInMinutes(sDuration);
                        //'Modified for Split Shift
                        if (nightFlag == true)
                        {
                            sSplitFromTime = Convert.ToDateTime(sSplitFromTime).AddDays(1).ToString();
                            sSplitToTime = Convert.ToDateTime(sSplitToTime).AddDays(1).ToString();
                        }

                        int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime).AddMinutes(iMinWorkingHrsMin), Convert.ToDateTime(sSplitToTime));
                        if (tmDurationDiff < 0)
                        {
                            nightFlag = true;
                            sSplitFromTime = Convert.ToDateTime(sSplitFromTime).ToString();
                            sSplitToTime = Convert.ToDateTime(sSplitToTime).AddDays(1).ToString();
                        }
                        string sPreviousTime = "";
                        bool bShiftChangeTime = true;
                        bool bShiftChangeOvertime = true;
                        bool bShiftChangeTimeBrkOT = true;
                        bool bShiftChangeTimeEarlyWRk = true;
                        bool bShiftChangeTimeEarlyBRK = true;
                        string sOTBreakTime = "0";

                        for (int j = 0; j <= datarow.Length - 1; j++)
                        {
                            string sTimeValue = datarow[j]["column3"].ToString();
                            string dResult = "";
                            if (sTimeValue == "")
                            {
                                DgvAttendance.Rows[gCount].DefaultCellStyle.ForeColor = Color.IndianRed;
                                DgvAttendance.Rows[gCount].Cells["ColDgvValidFlag"].Value = 0;
                                continue;
                            }

                            if (Datetimeformat(sTimeValue, sdateFormat, bdatetime, ref dResult))
                            { sTimeValue = Convert.ToDateTime(dResult).ToString("HH:mm"); }

                            if (j == 0)// ''first time' Grater than Shift time Checking eg:09:00AM to 06:00PM
                            {//'---------------------------------Late Coming
                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate) && bLate == true)// Then 'bFlexi = False
                                {
                                    int il = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                    if (il > 0)
                                    {
                                        string sLate = DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(il), DateTime.Today).ToString();
                                        DgvAttendance.Rows[gCount].Cells["ColDgvLateComing"].Value = Convert.ToDateTime(sLate).ToString("HH:mm:ss");
                                    }
                                    lblLateComing.Tag = 1;
                                    lblLateComing.Visible = true;
                                    DgvAttendance.Rows[gCount].Cells["ColDgvLateComing"].Tag = 1;//'if 1 then LateComing=yes
                                }
                                else
                                {
                                    lblLateComing.Tag = 0;
                                    lblLateComing.Visible = false;
                                    DgvAttendance.Rows[gCount].Cells["ColDgvLateComing"].Value = "00:00:00"; //'if 1 then LateComing=yes
                                    DgvAttendance.Rows[gCount].Cells["ColDgvLateComing"].Tag = 0;
                                }
                                //'---------------------------------Late Coming
                            }
                            else
                            {
                                if (sPreviousTime != "")
                                {
                                    int tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue));
                                    string sPrevCurrent = tmDiff.ToString();

                                    if (tmDiff < 0)// 'if date diff less than Zero then adding day to the StimeValue
                                    {
                                        tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1));
                                        sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString();
                                    }
                                    if (j % 2 == 1) //Then           'if mode 1 then wrk time else Break time
                                    {
                                        //'------------------------------------------------Early going Case Checking
                                        if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly) && bEarly == true)//Then 'bFlexi = False
                                        {
                                            int iE = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sTimeValue), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                            if (iE > 0)
                                            {
                                                string sEarly = DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iE), DateTime.Today).ToString();
                                                DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Value = Convert.ToDateTime(sEarly).ToString("HH:mm:ss");
                                            }

                                            lblEarlyGoing.Visible = true;
                                            DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Tag = 1; //'Earlygoing yes
                                        } //'------------------------------------------------Early going 
                                        //'---------------------------------------------Split Over Time-------------------------------------------

                                        if (i == DtShiftDetails.Rows.Count - 1)
                                        {
                                            if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))//Then
                                            {
                                                if (bShiftChangeOvertime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime))
                                                {
                                                    bShiftChangeOvertime = false;
                                                    int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                                    if (i == DtShiftDetails.Rows.Count - 1)
                                                    {
                                                        sOvertime = Convert.ToString(Convert.ToDouble(sOvertime) + idiffCurtShiftOT);

                                                    }
                                                }
                                                else
                                                {
                                                    if (i == DtShiftDetails.Rows.Count - 1)
                                                    {

                                                        sOvertime = Convert.ToString(Convert.ToDouble(sOvertime) + Convert.ToDouble(tmDiff));

                                                    } //'WorkTime Calculation 
                                                }
                                            }
                                        }// '---------------------------------------------Split Over Time-------------------------------------------

                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime)) //Then
                                        {
                                            if (bShiftChangeTime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitToTime))
                                            {
                                                bShiftChangeTime = false;
                                                int idiffPrevShitWork = 0;
                                                if (Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime)) //Then 'modified 28 dec
                                                {
                                                    idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sSplitToTime));
                                                }
                                                else
                                                {
                                                    idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sSplitToTime));
                                                }
                                                int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitToTime), Convert.ToDateTime(sTimeValue));
                                                if (i == DtShiftDetails.Rows.Count - 1)
                                                {
                                                    sOt = Convert.ToString(Convert.ToDouble(sOt) + idiffCurtShiftOT);
                                                }

                                                sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                            }
                                            else
                                            {
                                                if (i == DtShiftDetails.Rows.Count - 1)
                                                {
                                                    sOt = Convert.ToString(Convert.ToDouble(sOt) + Convert.ToDouble(tmDiff));

                                                }// 'WorkTime Calculation 
                                            }
                                        }
                                        else
                                        {
                                            bool bflag = false;
                                            if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sSplitFromTime))
                                            {
                                                bflag = true;
                                            }

                                            if (bShiftChangeTimeEarlyWRk && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitFromTime))
                                            {
                                                bShiftChangeTimeEarlyWRk = false;
                                                int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sTimeValue));
                                                sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                                bflag = true;
                                            }
                                            if (bflag == false)
                                            {
                                                sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff));
                                            }
                                        }

                                    }
                                    else //'-------------------------------------------------------Break Time Calculation 
                                    {
                                        if (i == DtShiftDetails.Rows.Count - 1)
                                        {
                                            if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                            {
                                                if (bEarly)
                                                {

                                                    lblEarlyGoing.Visible = false;
                                                    DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Tag = 0; //'Earlygoing yes
                                                    DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                                                }

                                            }
                                        }

                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime))
                                        {
                                            if (bShiftChangeTimeBrkOT = true && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitToTime))
                                            {
                                                bShiftChangeTimeBrkOT = false;
                                                int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sSplitToTime));
                                                int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitToTime), Convert.ToDateTime(sTimeValue));
                                                sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(idiffPrevShitWork));// 'Break Time Calculation
                                                sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(idiffCurtShiftOT));
                                            }
                                            else
                                            {
                                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime)) { sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(tmDiff)); } //'Then ot Break Time Calculation
                                            }

                                        }
                                        else
                                        {
                                            bool bflag = false;
                                            if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sSplitFromTime))
                                            { bflag = true; }
                                            if (bShiftChangeTimeEarlyBRK && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitFromTime))
                                            {
                                                bShiftChangeTimeEarlyBRK = false;
                                                int idiffPrevShitBreak = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sTimeValue));
                                                sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + idiffPrevShitBreak);
                                                bflag = true;

                                            }
                                            if (bflag == false)
                                            {
                                                sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(tmDiff));
                                            }
                                            //'-------------------------------------Early Going validating

                                            lblEarlyGoing.Visible = false;
                                            DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Tag = 0; //'Earlygoing yes
                                            DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                                            //'-------------------------------------Early Going 
                                        }
                                    }//else breaktime Cal
                                }//if Previous Time
                            } //'j=0

                            sPreviousTime = sTimeValue;
                            if (Glbln24HrFormat == false)
                                DgvAttendance.Rows[gCount].Cells[j + 3].Value = Convert.ToDateTime(sTimeValue).ToString("hh:mm tt");
                            else
                                DgvAttendance.Rows[gCount].Cells[j + 3].Value = Convert.ToDateTime(sTimeValue).ToString("HH:mm");
                        } //'j = 0 To datarow.Length - 1 '

                        //'------------------------------------Shoratge Calculation starts----------------------------------
                        intSplitAbsentTm = 0;

                        intAbsentTm = 0;
                        if (intWorkTm == Convert.ToInt32(sWorkTime) && intBreakTm == Convert.ToInt32(sBreakTime))
                        { intAbsentTm = iMinWorkingHrsMin; }
                        else
                        {
                            intSplitAbsentTm = iMinWorkingHrsMin - (Convert.ToInt32(sWorkTime) - intWorkTm);
                            if (intSplitAbsentTm > 0)
                            { intTotalSplitAbsentTm += intSplitAbsentTm; }
                        }
                        if (intAbsentTm > 0)
                        { intTotalAbsentTm += intAbsentTm; }


                        intWorkTm = Convert.ToInt32(sWorkTime);
                        intBreakTm = Convert.ToInt32(sBreakTime);
                        //'------------------------------------Shoratge Calculation Ends------------------------------------

                    } //'For i As Integer = 0 To DtShiftDetails.Rows.Count - 1
                    if (isAfterMinWrkHrsAsOT == false)
                    {
                        sOt = sOvertime;
                    }
                }//'  If DtShiftDetails.Rows.Count > 0
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on WorktimeCalculationForSplitShiftDataRow():Attendance Auto." + Ex.Message.ToString() + "", 3);
                return false;
            }
        }

        private bool DateFormat(string sDate, string sFormat, ref DateTime dDate)
        {

            //To format date
            bool blnRetValue = false;
            int iMonth = 0;
            int iYear = 0;
            int iDay = 0;
            string sMonth = "";
            string[] sSplit = new string[3];
            DateTime dtDate;

            if (sDate.IndexOf(@"\") != -1)
            {
                sDate = sDate.Replace("\\", "/");
            }
            if (sFormat.IndexOf(@"\") != -1)
            {
                sFormat = sFormat.Replace("\\", "/");
            }

            if (DateTime.TryParse(sDate, out dtDate))
            {
                //sDate = dtDate.ToString("dd-MMM-yyyy");

            }
            try
            {

                char cSeparator = '/';

                if (sDate.IndexOf('/') != -1)
                {
                    cSeparator = '/';
                }
                else if (sDate.IndexOf('-') != -1)
                {
                    cSeparator = '-';
                }
                else if (sDate.IndexOf(@"\") != -1)
                {
                    cSeparator = '\\';
                }
                else if (sDate.IndexOf(' ') != -1)
                {
                    cSeparator = ' ';
                }

                if (sFormat == "dd/MM/yyyy" || sFormat == "dd-MM-yyyy" || sFormat == @"dd\MM\yyyy")
                {
                    //ThenOr sFormat = "dd\MM\yyyy"
                    sSplit = sDate.Split(cSeparator);
                    iYear = Convert.ToInt32(sSplit[2]);
                    iMonth = Convert.ToInt32(sSplit[1]);
                    iDay = Convert.ToInt32(sSplit[0]);
                }

                else if (sFormat == "MM/dd/yyyy" || sFormat == "MM-dd-yyyy" || sFormat == @"MM\dd\yyyy")
                {//Or sFormat = "MM\dd\yyyy" Then
                    sSplit = sDate.Split(cSeparator);
                    iYear = Convert.ToInt32(sSplit[2]);
                    iMonth = Convert.ToInt32(sSplit[0]);
                    iDay = Convert.ToInt32(sSplit[1]);

                }
                else if (sFormat == "dd/MM/yy" || sFormat == "dd-MM-yy" || sFormat == @"dd\MM\yy")
                {
                    sSplit = sDate.Split(cSeparator);
                    iYear = Convert.ToInt32(sSplit[2]);
                    iMonth = Convert.ToInt32(sSplit[1]);
                    iDay = Convert.ToInt32(sSplit[0]);
                }
                else if (sFormat == "MM/dd/yy" || sFormat == "MM-dd-yy" || sFormat == @"MM\dd\yy")
                {
                    sSplit = sDate.Split(cSeparator);
                    iYear = Convert.ToInt32(sSplit[2]);
                    iMonth = Convert.ToInt32(sSplit[0]);
                    iDay = Convert.ToInt32(sSplit[1]);
                }
                else if (sFormat == "d/M/yyyy" || sFormat == "d-M-yyyy" || sFormat == @"d\M\yyyy")
                {
                    sSplit = sDate.Split(cSeparator);
                    iYear = Convert.ToInt32(sSplit[2]);
                    iMonth = Convert.ToInt32(sSplit[1]);
                    iDay = Convert.ToInt32(sSplit[0]);
                }

                else if (sFormat == "M/d/yyyy" || sFormat == "M-d-yyyy" || sFormat == @"M\d\yyyy")//Or  Then
                {
                    sSplit = sDate.Split(cSeparator);
                    iYear = Convert.ToInt32(sSplit[2]);
                    iMonth = Convert.ToInt32(sSplit[0]);
                    iDay = Convert.ToInt32(sSplit[1]);
                }

                else if (sFormat == "d/M/yy" || sFormat == "d-M-yy" || sFormat == @"d\M\yy")
                {
                    sSplit = sDate.Split(cSeparator);
                    iYear = Convert.ToInt32(sSplit[2]);
                    iMonth = Convert.ToInt32(sSplit[1]);
                    iDay = Convert.ToInt32(sSplit[0]);
                }
                else if (sFormat == "M/d/yy" || sFormat == "M-d-yy" || sFormat == @"M\d\yy")// Then
                {
                    sSplit = sDate.Split(cSeparator);
                    iYear = Convert.ToInt32(sSplit[2]);
                    iMonth = Convert.ToInt32(sSplit[0]);
                    iDay = Convert.ToInt32(sSplit[1]);
                }
                else if (sFormat == "yyyy/MM/dd" || sFormat == "yyyy-MM-dd" | sFormat == @"yyyy\MM\dd")//| Then
                {
                    sSplit = sDate.Split(cSeparator);
                    iYear = Convert.ToInt32(sSplit[0]);
                    iMonth = Convert.ToInt32(sSplit[1]);
                    iDay = Convert.ToInt32(sSplit[2]);
                }
                else if (sFormat == "yyyy/M/d" || sFormat == "yyyy-M-d" || sFormat == @"yyyy\M\d")//  Then
                {
                    sSplit = sDate.Split(cSeparator);
                    iYear = Convert.ToInt32(sSplit[0]);
                    iMonth = Convert.ToInt32(sSplit[1]);
                    iDay = Convert.ToInt32(sSplit[2]);
                }
                else if (sFormat == "yy/MM/dd" || sFormat == "yy-MM-dd" || sFormat == @"yy\MM\dd")//  Then
                {
                    sSplit = sDate.Split(cSeparator);
                    iYear = Convert.ToInt32(sSplit[0]);
                    iMonth = Convert.ToInt32(sSplit[1]);
                    iDay = Convert.ToInt32(sSplit[2]);
                }
                else if (sFormat == "yy/M/d" || sFormat == "yy-M-d" || sFormat == @"yy\M\d")// Then
                {
                    sSplit = sDate.Split(cSeparator);
                    iYear = Convert.ToInt32(sSplit[0]);
                    iMonth = Convert.ToInt32(sSplit[1]);
                    iDay = Convert.ToInt32(sSplit[2]);
                }
                else if (sFormat == "dd MMM yyyy" || sFormat == "dd-MMM-yyyy" || sFormat == @"yy\M\d")// Then
                {
                    sSplit = sDate.Split(cSeparator);
                    iYear = Convert.ToInt32(sSplit[0]);
                    iMonth = Convert.ToInt32(sSplit[1]);
                    iDay = Convert.ToInt32(sSplit[2]);
                }
                else if (sFormat == "dd/MMM/yyyy" || sFormat == "dd-MMM-yyyy" || sFormat == @"dd\MMM\yyyy")
                {
                    sSplit = sDate.Split(cSeparator);
                    iYear = sSplit[2].ToInt32();
                    string strMonth = "";
                    strMonth = sSplit[1].Trim();
                    if (strMonth.Length > 2)
                        iMonth = DateAndTime.Month(dtDate);

                    iDay = sSplit[0].ToInt32();

                }
                sMonth = GetMonth(iMonth);
                sDate = iDay.ToString().PadLeft(2, '0') + " " + sMonth + " " + iYear.ToString().PadLeft(3, '0').PadLeft(4, '2');
                dDate = Convert.ToDateTime(sDate);
                blnRetValue = true;
            }
            catch (Exception Ex)
            {
                blnRetValue = false;
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);


            }

            return blnRetValue;
        }

        private bool Datetimeformat(string sDate, string sFormat, bool bDateTime, ref string dDate)
        {

            try
            {
                int iMonth = 0;
                int iYear = 0;
                int iDay = 0;
                string sMonth = "";
                string[] sSplit = new string[3];

                char cSeparator = '/';

                if (sDate.IndexOf('/') != -1)
                {
                    cSeparator = '/';
                }
                else if (sDate.IndexOf('-') != -1)
                {
                    cSeparator = '-';
                }
                else if (sDate.IndexOf(' ') != -1)
                {
                    cSeparator = ' ';
                }
                if (bDateTime)
                {
                    string sDatePart = sDate.Substring(0, sDate.IndexOf(" "));
                    string stimePart = sDate.Substring(sDate.IndexOf(" "), sDate.Length - sDate.IndexOf(" "));


                    if (sFormat == "dd/MM/yyyy" || sFormat == "dd-MM-yyyy")
                    {
                        //ThenOr sFormat = "dd\MM\yyyy"
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[0]);
                    }
                    else if (sFormat == "MM/dd/yyyy" || sFormat == "MM-dd-yyyy")
                    {//Or sFormat = "MM\dd\yyyy" Then
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[0]);
                        iDay = Convert.ToInt32(sSplit[1]);

                    }
                    else if (sFormat == "dd/MM/yy" || sFormat == "dd-MM-yy")// Or sFormat = "dd\MM\yy" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[0]);
                    }
                    else if (sFormat == "MM/dd/yy" || sFormat == "MM-dd-yy")//Or sFormat = "MM\dd\yy" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[0]);
                        iDay = Convert.ToInt32(sSplit[1]);
                    }
                    else if (sFormat == "d/M/yyyy" || sFormat == "d-M-yyyy")//Or sFormat = "d\M\yyyy" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[0]);
                    }
                    else if (sFormat == "M/d/yyyy" || sFormat == "M-d-yyyy")//Or sFormat = "M\d\yyyy" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[0]);
                        iDay = Convert.ToInt32(sSplit[1]);
                    }
                    else if (sFormat == "d/M/yy" || sFormat == "d-M-yy")//|| sFormat = "d\M\yy" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[0]);
                    }
                    else if (sFormat == "M/d/yy" || sFormat == "M-d-yy")//|| sFormat = "M\d\yy" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[0]);
                        iDay = Convert.ToInt32(sSplit[1]);
                    }
                    else if (sFormat == "yyyy/MM/dd" || sFormat == "yyyy-MM-dd")//|| sFormat = "yyyy\MM\dd" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[0]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[2]);
                    }
                    else if (sFormat == "yyyy/M/d" || sFormat == "yyyy-M-d")// || sFormat = "yyyy\M\d" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[0]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[2]);
                    }
                    else if (sFormat == "yy/MM/dd" || sFormat == "yy-MM-dd")// || sFormat = "yy\MM\dd" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[0]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[2]);
                    }
                    else if (sFormat == "yy/M/d" || sFormat == "yy-M-d")//|| sFormat = "yy\M\d" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[0]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[2]);
                    }
                    else if (sFormat == "dd MMM yyyy" || sFormat == "dd-MMM-yyyy")//|| sFormat = "yy\M\d" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[0]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[2]);
                    }
                    sMonth = GetMonth(iMonth);
                    sDate = iDay.ToString().PadLeft(2, '0') + " " + sMonth + " " + iYear.ToString().PadLeft(3, '0').PadLeft(4, '2') + stimePart;

                }
                else
                {
                    sDate = "01 Jan 2000 " + sDate;
                }
                dDate = sDate;
                return true;
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }

        private int GetSplitShiftTotalDuration(int ishiftID)
        {
            //'in split Shift Total Duration is Sum of  details shift duration
            int intTotalSplitShiftDurInMinutes = 0;
            try
            {

                DataTable DtShiftDetails = MobjclsBLLAttendance.GetDynamicShiftDetails(ishiftID);
                if (DtShiftDetails.Rows.Count > 0)
                {
                    for (int i = 0; i <= DtShiftDetails.Rows.Count - 1; i++)
                    {
                        string strDuration = Convert.ToString(DtShiftDetails.Rows[i]["Duration"]);
                        int intDurInMin = GetDurationInMinutes(strDuration);
                        intTotalSplitShiftDurInMinutes += intDurInMin;
                    }
                }
                return intTotalSplitShiftDurInMinutes;
            }
            catch (Exception ex)
            {
                MobjClsLogs.WriteLog("Error on GetSplitShiftTotalDuration  :" + this.Name + " --" + ex.Message.ToString(), 1);
                return 0;
            }
        }

        private int GetShiftOrderNo(int iShiftId, string sFirstTime, ref string sFromTime, ref string sToTime, ref string sDur) // 'Finding shift OrderNo from First Punch time
        {
            int iShiftOrderNo = 0;
            sFromTime = "";
            sToTime = "";
            bool blnNightshiftFlag = false;
            try
            {

                DataTable DtShiftDetails = MobjclsBLLAttendance.GetDynamicShiftDetails(iShiftId);
                if (DtShiftDetails.Rows.Count > 0)
                {
                    if (sFirstTime != "")
                    {
                        int iBufferTime = 0;

                        int iDuration = 0;
                        for (int i = 0; i <= DtShiftDetails.Rows.Count - 1; i++)
                        {

                            iShiftOrderNo = 0;

                            sDur = Convert.ToString(DtShiftDetails.Rows[i]["Duration"]);
                            iBufferTime = Convert.ToInt32(DtShiftDetails.Rows[i]["BufferTime"]);
                            sFromTime = Convert.ToString((DtShiftDetails.Rows[i]["FromTime"]));
                            sToTime = Convert.ToString((DtShiftDetails.Rows[i]["ToTime"]));

                            int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iDuration), Convert.ToDateTime(sToTime));
                            if (tmDurationDiff < 0)
                            {
                                blnNightshiftFlag = true;
                                sFromTime = Convert.ToString(Convert.ToDateTime(sFromTime));
                                sToTime = Convert.ToString(Convert.ToDateTime(sToTime).AddDays(1));
                            }

                            if (Convert.ToDateTime(sFirstTime) >= Convert.ToDateTime(sFromTime).AddMinutes(-iBufferTime) &&
                                Convert.ToDateTime(sFirstTime) <= Convert.ToDateTime(sFromTime).AddMinutes(iBufferTime))
                            {
                                iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNo"]);
                                break;
                            }

                        } //for i

                        if (iShiftOrderNo <= 0)
                        {
                            int iTime = 0;
                            iShiftOrderNo = 0;
                            string sFrom = "";
                            string sTo = "";

                            for (int i = 0; i <= DtShiftDetails.Rows.Count - 1; i++)
                            {

                                iDuration = 0;
                                blnNightshiftFlag = false;
                                sDur = Convert.ToString(DtShiftDetails.Rows[i]["Duration"]);
                                iBufferTime = Convert.ToInt32(DtShiftDetails.Rows[i]["BufferTime"]);
                                sFromTime = Convert.ToString((DtShiftDetails.Rows[i]["FromTime"]));
                                sToTime = Convert.ToString((DtShiftDetails.Rows[i]["ToTime"]));

                                int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iDuration), Convert.ToDateTime(sToTime));
                                if (tmDurationDiff < 0)
                                {
                                    blnNightshiftFlag = true;
                                    sFromTime = Convert.ToString(Convert.ToDateTime(sFromTime));
                                    sToTime = Convert.ToString(Convert.ToDateTime(sToTime).AddDays(1));
                                }
                                if (i == 0)
                                {
                                    iTime = Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)));
                                    iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNo"]);
                                    sFrom = sFromTime;
                                    sTo = sToTime;
                                }
                                else
                                {
                                    if (blnNightshiftFlag)
                                    {
                                        bool flag = false;
                                        if (Convert.ToDateTime(sFirstTime) >= Convert.ToDateTime("12:00 AM") && Convert.ToDateTime(sFirstTime) <= Convert.ToDateTime("12:00 PM"))
                                        {
                                            flag = true;
                                        }

                                        if (Convert.ToDateTime(sFirstTime) >= Convert.ToDateTime(sFromTime) && Convert.ToDateTime(sFirstTime) <= Convert.ToDateTime("11:59 PM"))
                                        {
                                            if (Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime))) < iTime)
                                            {
                                                iTime = Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)));
                                                iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNo"]);
                                                sFrom = sFromTime;
                                                sTo = sToTime;
                                            }
                                        }
                                        else if (Convert.ToDateTime(sFirstTime) < Convert.ToDateTime(sFromTime))
                                        {
                                            if (Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime))) < iTime)
                                            {
                                                iTime = Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)));
                                                iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNo"]);
                                                sFrom = sFromTime;
                                                sTo = sToTime;
                                            }
                                        }
                                        else
                                        {
                                            if (Math.Abs(1440 - Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)))) < iTime)
                                            {
                                                iTime = Math.Abs(1440 - Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime))));
                                                iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNo"]);
                                                sFrom = sFromTime;
                                                sTo = sToTime;
                                            }
                                        }

                                        if (flag == true)
                                        {
                                            if (Math.Abs(1440 - Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)))) < iTime)
                                            {
                                                iTime = Math.Abs(1440 - Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime))));
                                                iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNo"]);
                                                sFrom = sFromTime;
                                                sTo = sToTime;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime))) < iTime)
                                        {
                                            iTime = Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)));
                                            iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNo"]);
                                            sFrom = sFromTime;
                                            sTo = sToTime;
                                        }
                                    }
                                }

                            }//For 
                            sFromTime = sFrom;
                            sToTime = sTo;
                        }

                    }
                    else
                    {
                        iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[0]["OrderNo"]);
                        if (Glbln24HrFormat == false)
                        {
                            sFromTime = ConvertDate12(Convert.ToDateTime(DtShiftDetails.Rows[0]["FromTime"]));
                            sToTime = ConvertDate12(Convert.ToDateTime(DtShiftDetails.Rows[0]["ToTime"]));

                        }
                        else
                        {
                            sFromTime = ConvertDate24(Convert.ToDateTime(DtShiftDetails.Rows[0]["FromTime"]));
                            sToTime = ConvertDate24(Convert.ToDateTime(DtShiftDetails.Rows[0]["ToTime"]));
                        }
                        sDur = Convert.ToString(DtShiftDetails.Rows[0]["Duration"]);
                    }
                    if (blnNightshiftFlag)
                    {
                        if (Glbln24HrFormat == false)
                        {

                            sFromTime = ConvertDate12(Convert.ToDateTime(sFromTime));
                            sToTime = ConvertDate12(Convert.ToDateTime(sToTime));
                        }
                        else
                        {
                            sFromTime = ConvertDate24(Convert.ToDateTime(sFromTime));
                            sToTime = ConvertDate24(Convert.ToDateTime(sToTime));

                        }
                    }

                }

                return iShiftOrderNo;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString());
                return iShiftOrderNo;
            }
        }

        private bool SaveAttendanceInfo()
        {
            bool rtnValue = false;
            int EmployeeID = 0;
            if (DgvAttendance.CurrentCell != null)
            {

                try
                {
                    DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    if (rbtnMonthly.Checked)
                        DgvAttendance.CurrentCell = DgvAttendance[ColDgvAtnDate.Index, DgvAttendance.CurrentCell.RowIndex];
                    else
                        DgvAttendance.CurrentCell = DgvAttendance[ColDgvEmployee.Index, DgvAttendance.CurrentCell.RowIndex];


                }
                catch
                {
                    return false;
                }
            }

            try
            {

                this.Cursor = Cursors.WaitCursor;
                if (DgvAttendance.RowCount > 0)
                {

                    if (FormValidation())
                    {
                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1601, out  MmessageIcon);
                        if (MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        {
                            this.Cursor = Cursors.Default;
                            return false;
                        }
                        if (MbAutoFill == false)
                        {
                            if (MblnCorrectTime == false)
                            {
                                this.Cursor = Cursors.Default;
                                return false;
                            }
                        }
                        if (DeleteAttendenceDetails() == false)
                        {
                            this.Cursor = Cursors.Default;
                            return false;
                        }
                        DgvAttendance.Focus();
                        barProgressBarAttendance.Visible = true;
                        barProgressBarAttendance.Minimum = 0;
                        barProgressBarAttendance.Maximum = DgvAttendance.Rows.Count + 1;
                        barProgressBarAttendance.Value = 0;
                        int iCount = 0;
                        DataTable DtEmployee = new DataTable();// For Absent Marking
                        DtEmployee.Columns.Add("EmployeeID");
                        DtEmployee.Columns.Add("Date");
                        DtEmployee.Columns.Add("Month");
                        string[] sRows = new string[3];

                        for (int i = 0; i <= DgvAttendance.Rows.Count - 1; i++)
                        {
                            rtnValue = false;

                            if (ClsCommonSettings.AttendanceAutofillForSinglePunch && (DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value.ToInt32() != Convert.ToInt32(AttendanceStatus.Leave)) && (DgvAttendance.Rows[i].Cells["TimeIn1"].Value.ToStringCustom() != string.Empty && DgvAttendance.Rows[i].Cells["TimeOut1"].Value.ToStringCustom() == string.Empty))
                            {
                                //option to autofill the row if only the first column is entered 
                                fillAttendanceRow(i);
                                DgvAttendance.Rows[i].DefaultCellStyle.ForeColor = Color.Black;
                                DgvAttendance.Rows[i].Cells["ColDgvValidFlag"].Value = 1;
                            }

                            if (DgvAttendance.Rows[i].DefaultCellStyle.ForeColor == Color.IndianRed)
                                continue;



                            int iStatus = 0;
                            if (DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value != DBNull.Value && DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value != null && Microsoft.VisualBasic.Information.IsNumeric(DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value))
                                iStatus = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value);

                            if (iStatus == Convert.ToInt32(AttendanceStatus.Present) && Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvValidFlag"].Value) == 0)
                                continue;
                            else if (iStatus == Convert.ToInt32(AttendanceStatus.Present) && (DgvAttendance.Rows[i].Cells[ColDgvStatus.Index + 1].Value == DBNull.Value || DgvAttendance.Rows[i].Cells[ColDgvStatus.Index + 1].Value == null || Convert.ToString(DgvAttendance.Rows[i].Cells[ColDgvStatus.Index + 1].Value) == ""))
                                continue;
                            else if (iStatus == 0)
                            { continue; }
                            else if (iStatus == Convert.ToInt32(AttendanceStatus.Leave))
                            {
                                if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveType"].Value) <= 0)
                                    continue;

                            }
                            DataTable datLeaveRemarks = MobjclsBLLAttendance.GetLeaveRemarks(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value.ToDateTime());
                            if (datLeaveRemarks.Rows.Count > 0)
                            {
                                if (datLeaveRemarks.Rows[0]["HalfDay"].ToInt32() == 0)//if leave from leave entry and full day leave
                                {
                                    continue;
                                }
                                else
                                {
                                    if (DgvAttendance.Rows[i].Cells["TimeIn1"].Value.ToStringCustom() == string.Empty || DgvAttendance.Rows[i].Cells["TimeOut1"].Value.ToStringCustom() == string.Empty)
                                    {
                                        continue;
                                    }
                                }
                            }
                            MobjclsBLLAttendance.ClearAllPools();// Bulk Data So conncettion pools are cleared
                            string sCurr = Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value).ToString("dd MMM yyyy");
                            string sNow = Strings.Format(Microsoft.VisualBasic.DateAndTime.Now.Date, "dd MMM yyyy");
                            string sworkID = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvPunchID"].Value).TrimEnd().TrimStart();
                            string sDate = Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value).ToString("dd MMM yyyy");
                            int iCompanyID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value);
                            int iEmpID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag);
                            int iShiftID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvShift"].Tag);

                            if (iEmpID <= 0 || iShiftID <= 0)
                                continue;
                            string sJoiningDate = MobjclsBLLAttendance.GetJoiningDate(iEmpID);
                            DateTime dtJoiningDate = Convert.ToDateTime(sJoiningDate);
                            //'Cheking With Current Date
                            if (Convert.ToDateTime(sCurr) > Convert.ToDateTime(sNow))
                                continue;
                            if (Convert.ToDateTime(sCurr).Date < dtJoiningDate.Date)
                                continue;

                            if (MobjclsBLLAttendance.IsEmployeeInService(iEmpID) == false)
                                continue;
                            if (MobjclsBLLAttendance.IsSalaryReleased(iEmpID, iCompanyID, Convert.ToDateTime(sCurr).Date) == true)
                                continue;

                            if (FillParameterAtnMaster(i) == false)
                                continue;

                            rtnValue = MobjclsBLLAttendance.SaveAttendanceInfo();

                            if (rtnValue == true)
                            {
                                EmployeeID = iEmpID;
                                iCount += 1;
                                sRows[0] = iEmpID.ToString();
                                sRows[1] = sDate;
                                int iMonth = Convert.ToDateTime(sDate).Month;
                                string smonthname = Microsoft.VisualBasic.DateAndTime.MonthName(iMonth, true);
                                string smonth = smonthname + " " + Convert.ToDateTime(sDate).Year.ToString();
                                sRows[2] = smonth;
                                DtEmployee.Rows.Add(sRows);
                                barProgressBarAttendance.Value += 1;

                            }

                        }//for

                        if (iCount > 0)
                        {
                            // Modified By Laxmi for Attendance summary updation
                            MobjclsBLLAttendance.SaveAttendanceSummaryConsequence(EmployeeID, dtpFromDate.Value.Month, dtpFromDate.Value.Year);

                            if (DgvAttendance.Rows.Count > 0)
                            {
                                SetLeaveEntryFromConsequence(EmployeeID);
                            }

                            //

                            if (DtEmployee.Rows.Count > 0)
                            {
                                Absentmarking(DtEmployee);//Absent Marking
                            }
                            rtnValue = true;
                        }
                        else
                        {
                            barProgressBarAttendance.Visible = false;
                        }

                        barProgressBarAttendance.Value = DgvAttendance.Rows.Count + 1;
                    }
                }//If rowcount >0
                else
                {
                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1602, out  MmessageIcon);
                    MessageBox.Show(MstrMessCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                    tmrClearlabels.Enabled = true;
                }

            }
            catch (Exception Ex)
            {

                this.Cursor = Cursors.Default;

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString());
                return false;
            }
            this.Cursor = Cursors.Default;
            return rtnValue;
        }

        private bool Absentmarking(DataTable dtEmployee)
        {
            var drID = (from r in dtEmployee.AsEnumerable() select r["EmployeeID"]).Distinct();
            var drDate = (from r in dtEmployee.AsEnumerable() select r["Month"]).Distinct();

            try
            {
                foreach (string sEmpID in drID)
                {
                    foreach (string sDate in drDate)
                    {
                        string sFilterExpression = "EmployeeID='" + sEmpID + "' and Month='" + sDate + "'";
                        DataRow[] datarow = dtEmployee.Select(sFilterExpression);
                        dtEmployee.DefaultView.RowFilter = sFilterExpression;
                        DataTable dt = dtEmployee.DefaultView.ToTable().Copy();
                        if (rbtnDaily.Checked)
                        {
                            var drSDate = (from r in dt.AsEnumerable() select r["Date"]).Distinct();
                            foreach (string sNewDate in drSDate)
                            {

                                string sStartdate = Convert.ToDateTime(sNewDate).ToString("dd MMM yyyy");
                                string sEndDate = sStartdate;
                                DataTable dtAbsentdays = MobjclsBLLAttendance.GetAbsentDays(sStartdate, sEndDate, Convert.ToInt32(sEmpID));
                                if (dtAbsentdays.Rows.Count > 0)
                                {
                                    for (int l = 0; l <= dtAbsentdays.Rows.Count - 1; l++)
                                    {
                                        int iShiftID = 0;
                                        int iPolicyId = 0;
                                        int IcomID = 0;
                                        string strDate = Convert.ToDateTime(dtAbsentdays.Rows[l][0]).ToString("dd MMM yyyy");
                                        DateTime dtDates = Convert.ToDateTime(strDate);
                                        int iDayId = Convert.ToInt32(dtDates.DayOfWeek) + 2 > 7 ? 1 : Convert.ToInt32(dtDates.DayOfWeek) + 2;
                                        string sDuration = "0";
                                        string sMinWorkHrs = "0";
                                        int iShiftType = 0;
                                        string sAllowedBreak = "0";
                                        int intAbsentTime = 0;
                                        MobjclsBLLAttendance.GetEmployeeInfoForAbsentMarking(Convert.ToInt32(sEmpID), iDayId, strDate, ref iShiftID, ref iPolicyId, ref IcomID, ref sDuration,
                                                                                               ref sMinWorkHrs, ref iShiftType, ref sAllowedBreak);

                                        intAbsentTime = GetDurationInMinutes(sMinWorkHrs);
                                        MobjclsBLLAttendance.AbsentSaving(Convert.ToInt32(sEmpID), strDate, iShiftID, 0, intAbsentTime, iPolicyId, IcomID);//' Absent Marking

                                    }//for  l
                                }//if(dtAbsentdays
                            }//for j sMonth
                        }
                        else 
                        {
                            var drSmonth = (from r in dt.AsEnumerable() select r["Month"]).Distinct();
                            foreach (string sMonth in drSmonth)
                            {

                                string sStartdate = "01 " + sMonth;
                                sStartdate = Convert.ToDateTime(sStartdate).ToString("dd MMM yyyy");
                                int ilastday = System.Data.Linq.SqlClient.SqlMethods.DateDiffDay(Convert.ToDateTime(sStartdate), Convert.ToDateTime(sStartdate).AddMonths(1));
                                string sEndDate = ilastday + " " + sMonth;
                                sEndDate = Convert.ToDateTime(sEndDate).ToString("dd MMM yyyy");
                                DataTable dtAbsentdays = MobjclsBLLAttendance.GetAbsentDays(sStartdate, sEndDate, Convert.ToInt32(sEmpID));
                                if (dtAbsentdays.Rows.Count > 0)
                                {
                                    for (int l = 0; l <= dtAbsentdays.Rows.Count - 1; l++)
                                    {
                                        int iShiftID = 0;
                                        int iPolicyId = 0;
                                        int IcomID = 0;
                                        string strDate = Convert.ToDateTime(dtAbsentdays.Rows[l][0]).ToString("dd MMM yyyy");
                                        DateTime dtDates = Convert.ToDateTime(strDate);
                                        int iDayId = Convert.ToInt32(dtDates.DayOfWeek) + 2 > 7 ? 1 : Convert.ToInt32(dtDates.DayOfWeek) + 2;
                                        string sDuration = "0";
                                        string sMinWorkHrs = "0";
                                        int iShiftType = 0;
                                        string sAllowedBreak = "0";
                                        int intAbsentTime = 0;
                                        MobjclsBLLAttendance.GetEmployeeInfoForAbsentMarking(Convert.ToInt32(sEmpID), iDayId, strDate, ref iShiftID, ref iPolicyId, ref IcomID, ref sDuration,
                                                                                               ref sMinWorkHrs, ref iShiftType, ref sAllowedBreak);

                                        intAbsentTime = GetDurationInMinutes(sMinWorkHrs);
                                        MobjclsBLLAttendance.AbsentSaving(Convert.ToInt32(sEmpID), strDate, iShiftID, 0, intAbsentTime, iPolicyId, IcomID);//' Absent Marking

                                    }//for  l
                                }//if(dtAbsentdays
                            }//for j sMonth
                        }
                    } //For k sDate
                } //For (i) SID
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString());
                return false;
            }
        }

        //private bool FillParameterAtnMaster(int i)
        //{
        //    try
        //    {

        //        int iCompanyID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value);
        //        int iEmpID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag);
        //        int iShiftID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvShift"].Tag);
        //        int iShiftOrderNo = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvShiftOrderNo"].Value);
        //        DateTime dtDates = Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value.ToString());
        //        string sDtDates = dtDates.ToString("dd MMM yyyy");

        //        short bHalfDay = 0;
        //        short bLOP = 0;
        //        double dConseqAmt = 0;
        //        int iConseqID = 0;
        //        int iPolicyId = 0;
        //        int DayID = 0;
        //        if (Convert.ToInt32(dtDates.DayOfWeek) + 2 > 7)
        //        {
        //            DayID = 1;
        //        }
        //        else
        //        {
        //            DayID = (Convert.ToInt32(dtDates.DayOfWeek) + 2);
        //        }



        //        if (MblnPayExists && MobjclsBLLAttendance.IsHoliday(iEmpID, iCompanyID, Convert.ToDateTime(sDtDates), DayID) == false)
        //        {
        //            ApplyPolicyConsequence(iCompanyID, sDtDates, iEmpID, iShiftID, Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvWorkTime"].Tag), Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvBreakTime"].Tag),
        //                                                   Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvAllowedBreakTime"].Tag), Convert.ToBoolean(DgvAttendance.Rows[i].Cells["ColDgvLateComing"].Tag), Convert.ToBoolean(DgvAttendance.Rows[i].Cells["ColDgvEarlyGoing"].Tag),
        //                                                   ref  bHalfDay, ref  bLOP, ref  iConseqID, ref  iPolicyId, ref   dConseqAmt, iShiftOrderNo);
        //        }
        //        else
        //        {
        //            ApplyPolicyConsequence(iCompanyID, sDtDates, iEmpID, iShiftID, Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvWorkTime"].Tag), Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvBreakTime"].Tag),
        //                                                   Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvAllowedBreakTime"].Tag), Convert.ToBoolean(DgvAttendance.Rows[i].Cells["ColDgvLateComing"].Tag), Convert.ToBoolean(DgvAttendance.Rows[i].Cells["ColDgvEarlyGoing"].Tag),
        //                                                   ref  bHalfDay, ref  bLOP, ref  iConseqID, ref  iPolicyId, ref   dConseqAmt, iShiftOrderNo);
        //        }

        //        MobjclsBLLAttendance.clsDTOAttendance.strDate = DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value.ToString();
        //        MobjclsBLLAttendance.clsDTOAttendance.intEmployeeID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag);
        //        MobjclsBLLAttendance.clsDTOAttendance.intShiftID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvShift"].Tag);
        //        MobjclsBLLAttendance.clsDTOAttendance.strWorkTime = DgvAttendance.Rows[i].Cells["ColDgvWorkTime"].Value.ToString();
        //        MobjclsBLLAttendance.clsDTOAttendance.strBreakTime = DgvAttendance.Rows[i].Cells["ColDgvBreakTime"].Value.ToString();
        //        MobjclsBLLAttendance.clsDTOAttendance.strLate = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLateComing"].Value);
        //        MobjclsBLLAttendance.clsDTOAttendance.strEarly = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvEarlyGoing"].Value);
        //        MobjclsBLLAttendance.clsDTOAttendance.blnHalfDay = Convert.ToBoolean(bHalfDay);
        //        MobjclsBLLAttendance.clsDTOAttendance.strOT = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvExcessTime"].Value);
        //        MobjclsBLLAttendance.clsDTOAttendance.blnLOP = Convert.ToBoolean(bLOP);
        //        MobjclsBLLAttendance.clsDTOAttendance.intCompanyId = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value);
        //        MobjclsBLLAttendance.clsDTOAttendance.intConsequenceID = iConseqID;
        //        MobjclsBLLAttendance.clsDTOAttendance.intPolicyID = GetEmployeePolicyID(Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag));
        //        MobjclsBLLAttendance.clsDTOAttendance.decAmountDed = Convert.ToDecimal(dConseqAmt);
        //        //MobjclsBLLAttendance.clsDTOAttendance.intProjectID=
        //        MobjclsBLLAttendance.clsDTOAttendance.intAttendenceStatusID = 1;//Present
        //        MobjclsBLLAttendance.clsDTOAttendance.intAbsentTime = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvAbsentTime"].Value);
        //        MobjclsBLLAttendance.clsDTOAttendance.intShiftOrderID = iShiftOrderNo;
        //        MobjclsBLLAttendance.clsDTOAttendance.strRemarks = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvRemarks"].Value);
        //        FillParmeterAtnDetails(i);
        //        return true;
        //    }
        //    catch (Exception Ex)
        //    {
        //        MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
        //        return false;
        //    }

        //}
        private void SetLeaveEntryFromConsequence(int EmployeeID)
        {
            int LateAllowedDays = 0;
            int EarlyAllowedDays = 0;
            int iCmpID = Convert.ToInt32(DgvAttendance.Rows[0].Cells["ColDgvCompanyID"].Value);
            int Late = 0;
            int Early = 0;

            DateTime dtAtnDate = Convert.ToDateTime(DgvAttendance.Rows[0].Cells["ColDgvAtnDate"].Value);//.ToString("dd MMM yyyy")
            DataTable dtConsequence = MobjclsBLLAttendance.CheckConsequence(Convert.ToInt32(DgvAttendance.Rows[0].Cells["ColDgvEmployee"].Tag), dtAtnDate.Month, dtAtnDate.Year);

            if (dtConsequence.Rows.Count > 0)
            {
                LateAllowedDays = Convert.ToInt32(dtConsequence.Rows[0]["AllowdLateCt"]);
                EarlyAllowedDays = Convert.ToInt32(dtConsequence.Rows[0]["AllowedEarlyCt"]);
            }

            if (Convert.ToInt32(dtConsequence.Rows[0]["LeaveCount"]) > 0)
            {
                foreach (DataRow Row in dtConsequence.Rows)
                {
                    dtAtnDate = Convert.ToDateTime(Row["Date"]);

                    if (Late < LateAllowedDays)
                    {
                        if (Convert.ToInt32(Row["IsLate"]) == 0 && Row["LeaveID"].ToInt32() > 0)
                        {
                            DateTime dtDate = Convert.ToDateTime(Row["Date"]);

                            MobjclsBLLAttendance.DeleteLeaveEntry(EmployeeID, iCmpID, dtDate);
                            Late++;
                        }

                    }
                    if (Early < EarlyAllowedDays)
                    {
                        if (Convert.ToInt32(Row["IsEarly"]) == 0 && Row["LeaveID"].ToInt32() > 0)
                        {
                            DateTime dtDate = Convert.ToDateTime(Row["Date"]);

                            MobjclsBLLAttendance.DeleteLeaveEntry(EmployeeID, iCmpID, dtDate);
                            Early++;

                        }
                    }
                }
            }
        }  

        private bool FillParmeterAtnDetails(int iRow)
        {
            try
            {
                MobjclsBLLAttendance.clsDTOAttendance.lstclsDTOAttendanceDetails = new List<clsDTOAttendanceDetails>();
                MintDetailCount = 0;
                for (int j = 3; j < DgvAttendance.Columns["ColDgvAddMore"].Index; j++)
                {
                    if (DgvAttendance.Rows[iRow].Cells[j].Value.ToStringCustom() == string.Empty)
                        break;

                    clsDTOAttendanceDetails MobjclsDTOAttendanceDetails = new clsDTOAttendanceDetails();
                    MobjclsDTOAttendanceDetails.intOrderNo = j - 2;
                    MobjclsDTOAttendanceDetails.strTime = DgvAttendance.Rows[iRow].Cells[j].Value.ToString();
                    MobjclsBLLAttendance.clsDTOAttendance.lstclsDTOAttendanceDetails.Add(MobjclsDTOAttendanceDetails);
                    MintDetailCount += 1;
                }
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }

        private bool FillParameterAtnMaster(int i)
        {
            MobjclsBLLAttendance.clsDTOAttendance.lstclsDTOAttendanceDetails = null;
            int iWorkPolicyID = 0;
            int iStatus = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value);
            int iempId = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag);
            int iCmpID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value);
            DateTime dtDates = Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value);
            string sDtDates = dtDates.ToString("dd MMM yyyy");
            int iShiftID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvShift"].Tag);
            int iShiftOrderNo = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvShiftOrderNo"].Value);
            int DayID = GetDayOfWeek(dtDates);
            int iLeaveID = 0;
            short bHalfDay = 0;
            short bLOP = 0;
            double dConseqAmt = 0;
            int iConseqID = 0;
            int iPolicyid = 0;

            iWorkPolicyID = MobjclsBLLAttendance.GetEmpPolicyID(Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag));

            if (MbAutoFill == false)
            {
                if (MobjclsBLLAttendance.IsHoliday(iempId, iCmpID, dtDates, DayID) == false)
                {
                    if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Present))
                    {
                        ApplyPolicyConsequence(iCmpID, sDtDates, iempId, iShiftID, DgvAttendance.Rows[i].Cells["ColDgvWorkTime"].Tag == null ? 0 : Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvWorkTime"].Tag), DgvAttendance.Rows[i].Cells["ColDgvBreakTime"].Tag == null ? 0 : Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvBreakTime"].Tag),
                            DgvAttendance.Rows[i].Cells["ColDgvAllowedBreakTime"].Tag == null ? 0 : Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvAllowedBreakTime"].Tag), DgvAttendance.Rows[i].Cells["ColDgvLateComing"].Tag == null ? false : Convert.ToBoolean(DgvAttendance.Rows[i].Cells["ColDgvLateComing"].Tag) == false ? false : true,
                            DgvAttendance.Rows[i].Cells["ColDgvEarlyGoing"].Tag == null ? false : Convert.ToBoolean(DgvAttendance.Rows[i].Cells["ColDgvEarlyGoing"].Tag) == false ? false : true, ref bHalfDay, ref bLOP, ref iConseqID, ref iPolicyid, ref dConseqAmt, iShiftOrderNo);
                    }
                    else if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Leave)) //Then 'Half Day Leave
                    {
                        iLeaveID = MobjclsBLLAttendance.LeaveEntry(iCmpID, iempId, Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Value), dtDates, Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveType"].Value),
                           DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Tag.ToInt32());
                        if (iLeaveID == 0)
                            return false;
                    }
                }

            }
            MobjclsBLLAttendance.clsDTOAttendance.strDate = DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value.ToString();
            MobjclsBLLAttendance.clsDTOAttendance.intEmployeeID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag);
            MobjclsBLLAttendance.clsDTOAttendance.intShiftID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvShift"].Tag);

            MobjclsBLLAttendance.clsDTOAttendance.intProjectID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvProjectID"].Value);

            if (iStatus == Convert.ToInt32(AttendanceStatus.Rest) || iStatus == Convert.ToInt32(AttendanceStatus.Absent)) //Then 'full day leave and absent
            {
                MobjclsBLLAttendance.clsDTOAttendance.strWorkTime = "00:00:00";
                MobjclsBLLAttendance.clsDTOAttendance.strBreakTime = "";//"00:00:00";
                MobjclsBLLAttendance.clsDTOAttendance.strLate = "00:00:00";
                MobjclsBLLAttendance.clsDTOAttendance.strEarly = "00:00:00";
                MobjclsBLLAttendance.clsDTOAttendance.strOT = "";//"00:00:00";

                if (iStatus == Convert.ToInt32(AttendanceStatus.Absent))//absent
                {
                    int iabsent = GetAbsentTime(Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvShift"].Tag), iWorkPolicyID, DayID); //'if employee is absent shift duration is saving as absent time
                    MobjclsBLLAttendance.clsDTOAttendance.intAbsentTime = iabsent;
                }
                else//full day leave
                {
                    MobjclsBLLAttendance.clsDTOAttendance.intAbsentTime = 0;
                }
            }
            else if (iStatus == Convert.ToInt32(AttendanceStatus.Leave))
            {
                if (Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Value) == "0")
                {
                    MobjclsBLLAttendance.clsDTOAttendance.strWorkTime = "00:00:00";
                    MobjclsBLLAttendance.clsDTOAttendance.strBreakTime = "";//"00:00:00";
                    MobjclsBLLAttendance.clsDTOAttendance.strLate = "00:00:00";
                    MobjclsBLLAttendance.clsDTOAttendance.strEarly = "00:00:00";
                    MobjclsBLLAttendance.clsDTOAttendance.strOT = "";//"00:00:00";
                    MobjclsBLLAttendance.clsDTOAttendance.intAbsentTime = 0;
                }
                else
                {
                    if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvWorkTime"].Tag) > 0)
                    {
                        MobjclsBLLAttendance.clsDTOAttendance.strWorkTime = DgvAttendance.Rows[i].Cells["ColDgvWorkTime"].Value.ToStringCustom();
                        MobjclsBLLAttendance.clsDTOAttendance.strBreakTime = DgvAttendance.Rows[i].Cells["ColDgvBreakTime"].Value.ToString();
                        MobjclsBLLAttendance.clsDTOAttendance.strLate = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLateComing"].Value);
                        MobjclsBLLAttendance.clsDTOAttendance.strEarly = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvEarlyGoing"].Value);
                        MobjclsBLLAttendance.clsDTOAttendance.strOT = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvExcessTime"].Value);
                        MobjclsBLLAttendance.clsDTOAttendance.intAbsentTime = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvAbsentTime"].Value);
                    }
                    else
                    {
                        MobjclsBLLAttendance.clsDTOAttendance.strWorkTime = "00:00:00";
                        MobjclsBLLAttendance.clsDTOAttendance.strBreakTime = "";//"00:00:00";
                        MobjclsBLLAttendance.clsDTOAttendance.strLate = "00:00:00";
                        MobjclsBLLAttendance.clsDTOAttendance.strEarly = "00:00:00";
                        MobjclsBLLAttendance.clsDTOAttendance.strOT = "";//"00:00:00";
                        MobjclsBLLAttendance.clsDTOAttendance.intAbsentTime = 0;
                        bLOP = 1;

                    }
                }

            }
            else //'normal
            {
                MobjclsBLLAttendance.clsDTOAttendance.strWorkTime = DgvAttendance.Rows[i].Cells["ColDgvWorkTime"].Value.ToStringCustom();
                if (DgvAttendance.Rows[i].Cells["ColDgvBreakTime"].Value != null)
                {
                    MobjclsBLLAttendance.clsDTOAttendance.strBreakTime = DgvAttendance.Rows[i].Cells["ColDgvBreakTime"].Value.ToString();
                }
                if (DgvAttendance.Rows[i].Cells["ColDgvLateComing"] != null)
                {
                    MobjclsBLLAttendance.clsDTOAttendance.strLate = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLateComing"].Value);
                }
                MobjclsBLLAttendance.clsDTOAttendance.strEarly = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvEarlyGoing"].Value);
                MobjclsBLLAttendance.clsDTOAttendance.strOT = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvExcessTime"].Value);
                MobjclsBLLAttendance.clsDTOAttendance.intAbsentTime = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvAbsentTime"].Value);

            }

            MobjclsBLLAttendance.clsDTOAttendance.blnIsHalfDay = Convert.ToBoolean(bHalfDay);
            MobjclsBLLAttendance.clsDTOAttendance.blnIsLOP = Convert.ToBoolean(bLOP);
            MobjclsBLLAttendance.clsDTOAttendance.intCompanyId = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value);
            MobjclsBLLAttendance.clsDTOAttendance.intConsequenceID = iConseqID;
            MobjclsBLLAttendance.clsDTOAttendance.intPolicyID = iWorkPolicyID;
            MobjclsBLLAttendance.clsDTOAttendance.decAmountDed = Convert.ToDecimal(dConseqAmt);
            MobjclsBLLAttendance.clsDTOAttendance.intAttendenceStatusID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value);//Present
            if (iLeaveID > 0)
            {
                DgvAttendance.Rows[i].Cells["ColDgvLeaveId"].Value = iLeaveID;
                MobjclsBLLAttendance.clsDTOAttendance.intLeaveID = iLeaveID;
            }
            else
            {
                MobjclsBLLAttendance.clsDTOAttendance.intLeaveID = 0;
            }

            if (iShiftOrderNo > 0)
            { MobjclsBLLAttendance.clsDTOAttendance.intShiftOrderID = iShiftOrderNo; }
            else { MobjclsBLLAttendance.clsDTOAttendance.intShiftOrderID = 0; }

            MobjclsBLLAttendance.clsDTOAttendance.strRemarks = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvRemarks"].Value);
            if (iStatus == Convert.ToInt32(AttendanceStatus.Absent) || iStatus == Convert.ToInt32(AttendanceStatus.Rest))
            {
                return true;
            }
            if (iStatus == Convert.ToInt32(AttendanceStatus.Leave))
            {
                if (DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Value.ToStringCustom() == "0")
                {
                    return true;
                }

            }
            //MobjclsBLLAttendance.clsDTOAttendance.intProjectID = 9;// will add later dont delete
            //MobjclsBLLAttendance.clsDTOAttendance.intWorkLocationID = 7;// will add later dont delete
            FillParmeterAtnDetails(i);
            return true;
        }

        public int GetAbsentTime(int iShiftid, int iWorkPolicy, int dayid)
        {
            int iAbsentTime = 0;
            string sBreakTime = "0";
            string sMinWorkHours = "0";
            MobjclsBLLAttendance.GetAbsentTime(iShiftid, iWorkPolicy, dayid, ref sBreakTime, ref sMinWorkHours);
            iAbsentTime = GetDurationInMinutes(sMinWorkHours);
            return iAbsentTime;
        }

        private int GetEmployeePolicyID(int intEmpID)
        {
            int intPolicyID = 0;
            try
            {
                intPolicyID = MobjclsBLLAttendance.GetEmpPolicyID(intEmpID);
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }
            return intPolicyID;

        }

        private bool ApplyPolicyConsequence(int iCompanyId, string sDate, int iEmpId, int iEmpShiftID, int iWorkTime, int iBreakTime,
                                                    int iAllowedBreak, bool bLate, bool bEarly, ref short bHalfDay, ref short bLOP, ref int iConseqID,
                                                    ref int iPolicyId, ref  double dConseqAmt, int iShiftOrderNo)
        {
            try
            {
                bHalfDay = 0;
                bLOP = 0;
                dConseqAmt = 0;
                iConseqID = 0;

                double iMnLeave = 0;
                double iTakenLeave = 0;
                int iLeaveid = 0;
                int iDayId = 0;
                int iLop = 3; // 'none
                int iCasual = 3; //'none
                int iPolicyBreakAllowed = 0;
                int iLateMin = 0;
                int iEarlyMin = 0;
                int iAdditionalMinutes = 0;
                int intShiftType = 0;
                string sMinWorkHours = "0";
                string sDuration = "";
                string sAllowedBreak = "0";
                bool bHalfTaken = false;
                DateTime dt = Convert.ToDateTime(sDate);
                iDayId = (Convert.ToInt32(dt.DayOfWeek) + 1);
                iMnLeave = MobjclsBLLAttendance.GetEmployeeLeaveStatus(iEmpId, iCompanyId, sDate);
                iAdditionalMinutes = MobjclsBLLAttendance.GetLeaveExAddMinutes(iEmpId, dt);
                bHalfTaken = MobjclsBLLAttendance.GetEmployeeIsHalfDay(iEmpId, sDate);
                if (iMnLeave > 0)
                    iTakenLeave = MobjclsBLLAttendance.GetTakenleaves(iEmpId, dt, 2);//intLeveType=3 Casual
                else
                    iMnLeave = 0;
                MobjclsBLLAttendance.GetShiftInfo(iEmpShiftID, ref intShiftType, ref sDuration, ref sMinWorkHours, ref iPolicyBreakAllowed, ref iLateMin, ref iEarlyMin);//getting shiftInfo
                MobjclsBLLAttendance.GetEmployeePolicy(iEmpId, iDayId, ref iPolicyId);
                if (intShiftType == 3)// only For Dynamic Shift
                {
                    sDuration = MobjclsBLLAttendance.GetDynamicShiftDuration(iEmpShiftID, iShiftOrderNo, ref sMinWorkHours, ref sAllowedBreak);
                    iPolicyBreakAllowed = Convert.ToInt32(sAllowedBreak);
                }

                if (iAdditionalMinutes > 0)
                {
                    bEarly = false;
                    bLate = false;
                }


                if (bHalfTaken == true)
                {
                    bEarly = true;
                    bLate = true;
                }
                int iDurationInMinute = 0;
                if (intShiftType == 4)
                {
                    iDurationInMinute = GetSplitShiftTotalDuration(iEmpShiftID);
                }
                else
                {
                    iDurationInMinute = GetDurationInMinutes(sDuration);
                }
                int iMinWorkingHrsMin = GetDurationInMinutes(sMinWorkHours);


                if (sDuration != "")
                {

                    if (intShiftType == 1 || intShiftType == 3 || intShiftType == 4)//'bFlexi = False
                    {


                        if (iDurationInMinute > iMinWorkingHrsMin)
                        {
                            sDuration = Convert.ToString(iMinWorkingHrsMin);
                        }
                        else
                        {
                            sDuration = Convert.ToString(iDurationInMinute);
                            iWorkTime += iPolicyBreakAllowed + iLateMin + iEarlyMin;
                        }
                    }
                    else
                    {

                        sDuration = Convert.ToString(iMinWorkingHrsMin);
                    }

                    iWorkTime += iAdditionalMinutes;

                    if (bHalfTaken)
                        sDuration = Convert.ToString(Convert.ToDouble(sDuration) / 2);

                    if (iWorkTime < Convert.ToDouble(sDuration))
                    {
                        MobjclsBLLAttendance.GetPolicyConsequenceWorktime(iEmpId, ref iLop, ref iCasual, ref dConseqAmt, ref iConseqID);

                        if (bHalfTaken && iLop == 2)
                            iLop = 1;
                        if (bHalfTaken && iCasual == 2)
                            iCasual = 1;
                        if (iLop == 1)//    'half day lop
                        {
                            bHalfDay = 1;
                            bLOP = 1;
                        }
                        else if (iLop == 2)//   'full day lop
                        {
                            bHalfDay = 0;
                            bLOP = 1;
                        }
                        else if (iCasual == 1)// 'half day casual
                        {
                            bHalfDay = 1;
                            if (iTakenLeave >= iMnLeave)
                            {
                                bLOP = 1;
                            }
                            else
                            {
                                iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, 1, dt, 2);
                                if (iLeaveid > 0)
                                {
                                    bLOP = 0;
                                    return true;
                                }
                                else
                                {
                                    bLOP = 1;

                                }

                            }
                        }
                        else if (iCasual == 2)
                        {
                            bHalfDay = 0;
                            if (iTakenLeave >= iMnLeave)
                            {
                                bLOP = 1;
                                return true;
                            }
                            else
                            {
                                int ihalfday;
                                if (iMnLeave - iTakenLeave == 0.5)
                                {
                                    bHalfDay = 1;
                                    bLOP = 1;

                                    ihalfday = 1;
                                }
                                else
                                {
                                    bHalfDay = 0;
                                    bLOP = 0;
                                    ihalfday = 0;

                                }
                                iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, ihalfday, dt, 2);
                                if (iLeaveid > 0)
                                {
                                    if (iMnLeave - iTakenLeave == 0.5)
                                    { return true; }
                                    bLOP = 0;
                                    return true;
                                }
                                else
                                {
                                    bLOP = 1;
                                }

                            }
                        }
                        else if (dConseqAmt > 0)
                        {
                            bHalfDay = 0;
                            bLOP = 0;
                        }
                        else
                        {
                            iConseqID = 0;
                        }//else if s

                    }//iWorkTime < Convert.ToDouble(sDuration)

                }//sduration


                if (intShiftType == 2)//  ''Flexi then Exit after worktime consequence, Dynamic shift only worktime consequence
                {
                    return true;
                }

                if (iLeaveid <= 0)//Then 'break time
                {
                    if (iBreakTime > 0 && iConseqID == 0)
                    {
                        if (iBreakTime > iPolicyBreakAllowed + iAdditionalMinutes)
                        {
                            MobjclsBLLAttendance.GetPolicyConsequenceBreaktime(iPolicyId, ref iLop, ref  iCasual, ref dConseqAmt, ref iConseqID);

                            if (bHalfTaken && iLop == 2)
                                iLop = 1;
                            if (bHalfTaken && iCasual == 2)
                                iCasual = 1;
                            if (iLop == 1) //Then        'half day lop
                            {
                                bHalfDay = 1;
                                bLOP = 1;
                            }
                            else if (iLop == 2) //Then    'full day lop
                            {
                                bHalfDay = 0;
                                bLOP = 1;
                            }

                            else if (iCasual == 1) //Then 'half day casual
                            {
                                bHalfDay = 1;

                                if (iTakenLeave >= iMnLeave)
                                {
                                    bLOP = 1;
                                }
                                else
                                {
                                    iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, 1, dt, 2);

                                    if (iLeaveid > 0)
                                    {
                                        bLOP = 0;
                                        return true;
                                    }
                                    else
                                    {
                                        bLOP = 1;
                                    }

                                }

                            }
                            else if (iCasual == 2)//Then 'full day casual
                            {

                                bHalfDay = 0;
                                if (iTakenLeave >= iMnLeave)
                                {
                                    bLOP = 1;
                                    return true;
                                }
                                else
                                {


                                    int HalfDay;
                                    if (iMnLeave - iTakenLeave == 0.5)
                                    {
                                        bHalfDay = 1;
                                        bLOP = 1;
                                        HalfDay = 1;
                                    }
                                    else
                                    {
                                        bHalfDay = 0;
                                        bLOP = 0;
                                        HalfDay = 1;
                                    }
                                    iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, HalfDay, dt, 2);
                                    if (iLeaveid > 0)
                                    {
                                        if (iMnLeave - iTakenLeave == 0.5)
                                        { return true; }
                                        bLOP = 0;
                                        return true;
                                    }
                                    else
                                    {
                                        bLOP = 1;
                                    }

                                }
                            }
                            else if (dConseqAmt > 0)
                            {

                                bHalfDay = 0;
                                bLOP = 0;
                            }
                            else
                            {
                                iConseqID = 0;
                            }//else if's

                        }//( iBreakTime > iPolicyBreakAllowed + iAdditionalMinutes )
                    }//(iBreakTime > 0)
                }//(iLeaveid <= 0 )

                if (iLeaveid <= 0)//Then 'Late Coming
                {
                    if (bLate && iConseqID == 0)
                    {

                        MobjclsBLLAttendance.GetPolicyConsequenceLateComing(iEmpId, ref iLop, ref iCasual, ref dConseqAmt, ref iConseqID);
                        if (bHalfTaken && iLop == 2)
                            iLop = 1;
                        if (bHalfTaken && iCasual == 2)
                            iCasual = 1;
                        if (iLop == 1) //Then        'half day lop
                        {
                            bHalfDay = 1;
                            bLOP = 1;
                        }
                        else if (iLop == 2)   //Then 'full day lop
                        {
                            bHalfDay = 0;
                            bLOP = 1;
                        }
                        else if (iCasual == 1) //Then 'half day casual
                        {

                            bHalfDay = 1;
                            if (iTakenLeave >= iMnLeave)
                            {
                                bLOP = 1;
                            }
                            else
                            {

                                iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, 1, dt, 2);
                                if (iLeaveid > 0)
                                {
                                    bLOP = 0;
                                    return true;
                                }
                                else
                                {
                                    bLOP = 1;
                                }

                            }
                        }
                        else if (iCasual == 2) //Then 'full day casual
                        {
                            bHalfDay = 0;
                            if (iTakenLeave >= iMnLeave)
                            {
                                bLOP = 1;
                                return true;
                            }
                            else
                            {
                                int HalfDay;

                                if (iMnLeave - iTakenLeave == 0.5)
                                {
                                    bHalfDay = 1;
                                    bLOP = 1;
                                    HalfDay = 1;

                                }
                                else
                                {
                                    bHalfDay = 0;
                                    bLOP = 0;
                                    HalfDay = 0;

                                }
                                iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, HalfDay, dt, 2);
                                if (iLeaveid > 0)
                                {
                                    if (iMnLeave - iTakenLeave == 0.5) { return true; }
                                    bLOP = 0;
                                    return true;
                                }
                                else
                                {
                                    bLOP = 1;
                                }

                            }
                        }
                        else if (dConseqAmt > 0)
                        {
                            bHalfDay = 0;
                            bLOP = 0;
                        }
                        else
                        {
                            iConseqID = 0;
                        }// else if's


                    }//( bLate)

                }//( iLeaveid <= 0 )//Then 'Late Coming

                if (iLeaveid <= 0)//Then early Going
                {
                    if (bEarly && iConseqID == 0)
                    {
                        MobjclsBLLAttendance.GetPolicyConsequenceEarlyGoing(iEmpId, ref iLop, ref  iCasual, ref dConseqAmt, ref iConseqID);

                        if (bHalfTaken && iLop == 2) { iLop = 1; }
                        if (bHalfTaken && iCasual == 2) { iCasual = 1; }
                        if (iLop == 1)//Then        'half day lop
                        {
                            bHalfDay = 1;
                            bLOP = 1;
                        }
                        else if (iLop == 2)// Then    'full day lop
                        {
                            bHalfDay = 0;
                            bLOP = 1;
                        }
                        else if (iCasual == 1)//Then 'half day casual
                        {
                            bHalfDay = 1;
                            if (iTakenLeave >= iMnLeave)
                                bLOP = 1;
                            else
                            {
                                iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, 1, dt, 2);
                                if (iLeaveid > 0)
                                {
                                    bLOP = 0;
                                    return true;
                                }
                                else
                                {
                                    bLOP = 1;
                                }

                            }
                        }
                        else if (iCasual == 2)//Then 'full day casual
                        {
                            bHalfDay = 0;
                            if (iTakenLeave >= iMnLeave)
                            {
                                bLOP = 1;
                                return true;
                            }
                            else
                            {

                                int HalfDay;

                                if (iMnLeave - iTakenLeave == 0.5)
                                {
                                    bHalfDay = 1;
                                    bLOP = 1;
                                    HalfDay = 1;

                                }
                                else
                                {
                                    bHalfDay = 0;
                                    bLOP = 0;
                                    HalfDay = 0;
                                }
                                iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, HalfDay, dt, 2);

                                if (iLeaveid > 0)
                                {
                                    if (iMnLeave - iTakenLeave == 0.5) { return true; }
                                    bLOP = 0;
                                    return true;
                                }
                                else
                                {
                                    bLOP = 1;
                                }
                            }

                        }
                        else if (dConseqAmt > 0)
                        {
                            bHalfDay = 0;
                            bLOP = 0;
                        }
                        else
                        {
                            iConseqID = 0;
                        }//else if's

                    }//(bEarly)
                }//( iLeaveid <= 0 )Then early Going


                return true;
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }

        private bool GetData()// for Convert to valid Data
        {
            try
            {


                DataTable dtOnlineData;
                dtOnlineData = new DataTable();
                dtOnlineData = MobjclsBLLAttendance.GetDeviceLogs();//GET ALL DATA FROM TEMP TABLE 
                if (dtOnlineData.Rows.Count > 0)
                {
                    DgvAttendance.ColumnHeadersVisible = true;
                    DgvPunchingData.Visible = false;
                    DgvAttendance.Visible = true;
                    int intTemplateMasterID = 0;

                    this.Cursor = Cursors.WaitCursor;
                    ConvertFetchToValidDataNew(dtOnlineData, "", false, intTemplateMasterID);
                    this.Cursor = Cursors.Default;
                    return true;
                }
                return false;
            }
            catch (Exception Ex)
            {
                this.Cursor = Cursors.Default;
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString());
                return false;

            }
        }

        private void ClearAllControls()
        {

            try
            {
                InitializeGrid();
                DgvAttendance.Rows.Clear();
                DgvPunchingData.Rows.Clear();
                DgvAttendance.Refresh();
                barProgressBarAttendance.Visible = false;
                barProgressBarAttendance.Maximum = 0;
                lblEarlyGoing.Visible = false;
                lblLateComing.Visible = false;
                //'rbtnMonthly.Checked = True
                ChkAbsent.Checked = false;
                ChkConsequence.Checked = false;
                ChkEarly.Checked = false;
                ChkExtendedBreak.Checked = false;
                ChkLate.Checked = false;
                ChkLeave.Checked = false;
                ChkOverTime.Checked = false;
                ChkShortage.Checked = false;
                lblAbsentDispaly.Text = "0";
                lblLeaveCountDispaly.Text = "0";
                lblPresentDisplay.Text = "0";
                lblShortageCountDispaly.Text = "0";
                ClearBottomPanel();
                txtSearch.Text = "";
                MintSearchIndex = 0;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
            }
        }

        private void ClearBottomPanel()
        {

            lblAllowedBreakTime.Text = "00:00:00";
            lblBreakTime.Text = "00:00:00";
            lblWorkTime.Text = "00:00:00";
            lblShortageTime.Text = "00:00:00";
            lblShortageTime.ForeColor = System.Drawing.SystemColors.ControlText;
            lblExcessTime.Text = "00:00:00";
            lblShiftName.Text = "";
            lblConsequence.Text = "";
            lblConsequence.Visible = false;
            lblEarlyGoing.Visible = false;
            lblLateComing.Visible = false;

        }

        private bool InitializeGrid()
        {
            try
            {

                int iColIndex = DgvAttendance.Columns["ColDgvAddMore"].Index - 1;
                int iCounter;
                int iLimit;
                iLimit = ColDgvStatus.Index + 1;
                for (iCounter = iColIndex; iCounter >= iLimit; iCounter -= 1)
                {
                    if (DgvAttendance.Columns[iCounter].Name.IndexOf("Time") != -1)
                    {
                        DgvAttendance.Columns.RemoveAt(iCounter);
                        DgvAttendance.Columns[iCounter].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    }
                }
                MintTimingsColumnCnt = 0;
                if (MintDisplayMode == Convert.ToInt16(AttendanceMode.AttendanceManual))
                {
                    DataGridViewTextBoxColumn adcolIn = new DataGridViewTextBoxColumn();
                    DataGridViewTextBoxColumn adcolOut = new DataGridViewTextBoxColumn();

                    adcolIn.Name = "TimeIn1";
                    adcolIn.HeaderText = "TimeIn1";
                    adcolIn.SortMode = DataGridViewColumnSortMode.NotSortable;
                    adcolIn.Resizable = DataGridViewTriState.False;
                    adcolIn.Width = 70;
                    adcolIn.MaxInputLength = 9;

                    adcolOut.Name = "TimeOut1";
                    adcolOut.HeaderText = "TimeOut1";
                    adcolOut.SortMode = DataGridViewColumnSortMode.NotSortable;
                    adcolOut.Resizable = DataGridViewTriState.False;
                    adcolOut.Width = 70;
                    adcolIn.MaxInputLength = 9;

                    DgvAttendance.Columns.Insert(iLimit, adcolIn);
                    DgvAttendance.Columns.Insert(iLimit + 1, adcolOut);
                    MintTimingsColumnCnt = 2;
                    if (rbtnMonthly.Checked)
                    {
                        ColDgvEmployee.Visible = false;
                        ColDgvAtnDate.Visible = true;
                    }
                    else
                    {
                        ColDgvEmployee.Visible = true;
                        ColDgvAtnDate.Visible = false;
                    }
                }
                else
                {
                    ColDgvEmployee.Visible = true;
                    ColDgvAtnDate.Visible = true;
                }
                DgvAttendance.Columns["ColDgvAtnDate"].Width = 75;
                DgvAttendance.Columns["ColDgvAddMore"].Width = 35;
                DgvAttendance.Columns["ColDgvAddMore"].MinimumWidth = 35;
                DgvAttendance.Columns["ColDgvAddMore"].Resizable = DataGridViewTriState.False;
                DgvAttendance.Refresh();
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
                return false;
            }
        }

        private void LoadMessage()
        {
            // Loading Message
            MsarMessageArr = new ArrayList();
            try
            {
                MsarMessageArr = MobjClsNotification.FillMessageArray(121, 2);
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }

        private bool IsValidTime(string sTime)
        {
            try
            {
                //'Sample  Format  "13:14"
                //DateTime TheDate ;
                //TheDate = sTime;
                string Format1 = "^*(1[0-2]|[1-9]):[0-5][0-9]*(a|p|A|P)(m|M)*$";
                System.Text.RegularExpressions.Regex TryValidateFormat1 = new System.Text.RegularExpressions.Regex(Format1);
                string Format2 = "([0-1][0-9]|2[0-3]):([0-5][0-9])";
                System.Text.RegularExpressions.Regex TryValidateFormat2 = new System.Text.RegularExpressions.Regex(Format2);
                return TryValidateFormat2.IsMatch(sTime) || TryValidateFormat1.IsMatch(sTime);
            }
            catch (Exception)
            {

                return false;
            }
        }

        private void FormatGrid(int iRow)
        {
            Int32 icolName;
            DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);
            if (Convert.ToInt32(DgvAttendance.Rows[iRow].Cells["ColDgvHolidayFlag"].Value) == 1 || Convert.ToInt32(DgvAttendance.Rows[iRow].Cells["ColDgvLeaveFlag"].Value) == 1) //Then 'HOLIDAY CHEKINGG
            {
                //bool isHalfDay = MobjclsBLLAttendance.GetEmployeeIsHalfDay(DgvAttendance.Rows[iRow].Cells["ColDgvEmployee"].Tag.ToInt32(), Convert.ToDateTime(DgvAttendance.Rows[iRow].Cells["ColDgvAtnDate"].Value).ToString("dd MMM yyyy"));
                bool isHalfDay = (DgvAttendance.Rows[iRow].Cells["ColDgvLeaveInfo"].Value.ToInt32() == 1) ? true : false;
                if (DgvAttendance.Rows[iRow].Cells["ColDgvWorkTime"].Value == DBNull.Value)
                {
                    DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                    DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                }
                else
                {
                    if (Convert.ToString(DgvAttendance.Rows[iRow].Cells["ColDgvWorkTime"].Value) == "")
                    {
                        DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                    }
                    else
                    {
                        if (isHalfDay)
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Black;
                        }
                        else
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Red;

                        }
                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 1;
                    }
                }
                for (Int16 k = 2; k <= MintTimingsColumnCnt; k += 2)
                {
                    icolName = Convert.ToInt16(Math.Floor(Convert.ToDouble(k / 2)));

                    if (DgvAttendance.Rows[iRow].Cells["TimeIn1"].Value.ToStringCustom() == string.Empty && DgvAttendance.Rows[iRow].Cells["TimeOut1"].Value.ToStringCustom() == string.Empty)
                    {
                        DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                        break;
                    }
                    else if (DgvAttendance.Rows[iRow].Cells["TimeIn" + icolName].Value.ToStringCustom() != string.Empty && DgvAttendance.Rows[iRow].Cells["TimeOut" + icolName].Value.ToStringCustom() == string.Empty)
                    {
                        DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                        break;
                    }
                    else if (DgvAttendance.Rows[iRow].Cells["TimeIn" + icolName].Value.ToStringCustom() == string.Empty && DgvAttendance.Rows[iRow].Cells["TimeOut" + icolName].Value.ToStringCustom() != string.Empty)
                    {
                        DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                        break;
                    }
                    else
                    {
                        if (isHalfDay)
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Black;
                        }
                        else
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Red;

                        }

                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 1;
                    }
                }
            }
            else// 'normal case
            {

                if (DgvAttendance.Rows[iRow].Cells["ColDgvWorkTime"].Value == DBNull.Value)
                {
                    DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                    DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                }
                else
                {
                    if (Convert.ToString(DgvAttendance.Rows[iRow].Cells["ColDgvWorkTime"].Value) == "")
                    {
                        DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                    }
                    else
                    {
                        DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Black;
                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 1;
                    }
                }
                for (Int16 k = 2; k <= MintTimingsColumnCnt; k += 2)
                {
                    icolName = Convert.ToInt16(Math.Floor(Convert.ToDouble(k / 2)));
                    if (DgvAttendance.Rows[iRow].Cells["TimeIn" + icolName].Value.ToStringCustom() != string.Empty && DgvAttendance.Rows[iRow].Cells["TimeIn" + icolName].Value.ToStringCustom() != string.Empty)
                    {
                        if (DgvAttendance.Rows[iRow].Cells["TimeIn1"].Value.ToStringCustom() == string.Empty && DgvAttendance.Rows[iRow].Cells["TimeOut1"].Value.ToStringCustom() == string.Empty)
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                            DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                            break;
                        }
                        else if (DgvAttendance.Rows[iRow].Cells["TimeIn" + icolName].Value.ToStringCustom() != string.Empty && DgvAttendance.Rows[iRow].Cells["TimeOut" + icolName].Value.ToStringCustom() == string.Empty)
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                            DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                            break;
                        }
                        else if (DgvAttendance.Rows[iRow].Cells["TimeIn" + icolName].Value.ToStringCustom() == string.Empty && DgvAttendance.Rows[iRow].Cells["TimeOut" + icolName].Value.ToStringCustom() != string.Empty) //Then 'modified
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                            DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                            break;
                        }
                        else
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Black;
                            DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 1;
                        }
                    }
                }
            }
        }

        private bool GetShiftInfo(int iCmpID, int iEmpId, int iShiftId, DateTime dtdate, ref string sFromTime, ref string sToTime, ref string sDuration, ref  int iShiftType, ref int iNoOfTimings,
                              ref string sminWorkhours, ref string sAllowedBreakTime, ref int iLate, ref int iEarly)
        {

            DataTable dtShift = new DataTable();
            sFromTime = "";
            sToTime = "";
            sDuration = "";
            iShiftType = 0;
            iNoOfTimings = 0;
            sminWorkhours = "0";

            try
            {

                if (iShiftId > 0)
                {
                    dtShift = MobjclsBLLAttendance.GetShift(iShiftId, iEmpId);
                }
                else
                {
                    dtShift = MobjclsBLLAttendance.GetShift(iShiftId, iEmpId);
                }

                if (dtShift.Rows.Count > 0)
                {
                    MintShiftType = Convert.ToInt32(dtShift.Rows[0]["ShiftTypeID"]);
                    iNoOfTimings = Convert.ToInt32(dtShift.Rows[0]["NoOfTimings"]);
                    sminWorkhours = Convert.ToString(dtShift.Rows[0]["MinWorkingHours"]);
                    sAllowedBreakTime = Convert.ToString(dtShift.Rows[0]["AllowedBreakTime"]);
                    iLate = Convert.ToInt32(dtShift.Rows[0]["LateAfter"]);
                    iEarly = Convert.ToInt32(dtShift.Rows[0]["EarlyBefore"]);

                    iShiftType = MintShiftType;
                    if (Glbln24HrFormat == false)
                    {
                        sFromTime = ConvertDate12(Convert.ToDateTime(dtShift.Rows[0]["FromTime"])).ToString();

                        sToTime = ConvertDate12(Convert.ToDateTime(dtShift.Rows[0]["ToTime"])).ToString();
                    }
                    else
                    {
                        sFromTime = ConvertDate24(Convert.ToDateTime(dtShift.Rows[0]["FromTime"])).ToString();

                        sToTime = ConvertDate24(Convert.ToDateTime(dtShift.Rows[0]["ToTime"])).ToString();

                    }
                    sDuration = Convert.ToString(dtShift.Rows[0]["Duration"]);
                    lblShiftName.Text = sFromTime + " To " + sToTime + " ( Duration : " + sDuration + ")";
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        private void AddNewColumns(int iColCounts)
        {
            try
            {

                if (MintTimingsColumnCnt >= 2)
                {
                    DgvAttendance.Columns["ColDgvAddMore"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    DgvAttendance.Columns["ColDgvAddMore"].Width = 35;
                    DgvAttendance.Columns["ColDgvAddMore"].MinimumWidth = 35;
                    int cnt = MintTimingsColumnCnt + 2;

                    cnt = Convert.ToInt32(Math.Floor(Convert.ToDecimal((cnt + 1) / 2)));
                    DataGridViewTextBoxColumn adcolIn = new DataGridViewTextBoxColumn();
                    DataGridViewTextBoxColumn adcolOut = new DataGridViewTextBoxColumn();

                    adcolIn.Name = "TimeIn" + cnt;
                    adcolIn.HeaderText = "TimeIn" + cnt;
                    adcolIn.SortMode = DataGridViewColumnSortMode.NotSortable;
                    adcolIn.Resizable = DataGridViewTriState.False;
                    adcolIn.Width = 70;
                    adcolIn.MaxInputLength = 9;

                    adcolOut.Name = "TimeOut" + cnt;
                    adcolOut.HeaderText = "TimeOut" + cnt;
                    adcolOut.SortMode = DataGridViewColumnSortMode.NotSortable;
                    adcolOut.Resizable = DataGridViewTriState.False;
                    adcolOut.Width = 70;
                    adcolOut.MaxInputLength = 9;

                    DgvAttendance.Columns.Insert(MintTimingsColumnCnt + 3, adcolIn);
                    DgvAttendance.Columns.Insert(MintTimingsColumnCnt + 4, adcolOut);
                    MintTimingsColumnCnt += 2;
                    DgvAttendance.CurrentCell = DgvAttendance[DgvAttendance.CurrentCell.ColumnIndex - iColCounts, DgvAttendance.CurrentCell.RowIndex];
                }
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

            }
        }

        private bool WorkTimeCalculation(int iRowIndex, bool bInOutOnly)
        {
            try
            {
                string sWorkTime = "0";
                string sBreakTime = "0";
                string sOTBreakTime = "0";
                int iAbsentTime = 0;
                string sOt = "0";
                string sPreviousTime = "";
                bool bShiftChangeTime = true;
                bool bShiftChangeTimeBrkOT = true;
                bool bShiftChangeTimeEarlyWRk = true;
                bool bShiftChangeTimeEarlyBRK = true;
                bool blnNightShiftFlag = false;
                int intDurationInMinutes = 0;
                int intMinWorkHrsInMinutes = 0;
                bool blnIsMinWrkHRsAsOT = false;
                int iCountIndex = DgvAttendance.Columns["ColDgvAddMore"].Index - 1;
                if (bInOutOnly)
                    iCountIndex = 4;

                int k = 0;
                for (int j = 3; j <= iCountIndex; j++)
                {
                    if (Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells[j].Value).Trim() == "")
                        break;
                    if (Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells[j].Value).Trim() != "")
                        k = k + 1;
                }

                if (k % 2 == 1)
                {
                    DgvAttendance.Rows[iRowIndex].DefaultCellStyle.ForeColor = Color.IndianRed;
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvValidFlag"].Value = 0;
                }
                else if (k == 0)
                {
                    return false;
                }
                else
                {
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvValidFlag"].Value = 1;
                    if (DgvAttendance.Rows[iRowIndex].Cells["ColDgvHolidayFlag"].Value.ToInt32() == 1 || DgvAttendance.Rows[iRowIndex].Cells["ColDgvLeaveFlag"].Value.ToInt32() == 1) //Then 'HOLIDAY CHEKINGG
                    {
                        if (DgvAttendance.Rows[iRowIndex].Cells["ColDgvLeaveInfo"].Value.ToInt32() == 1)
                        {

                            DgvAttendance.Rows[iRowIndex].DefaultCellStyle.ForeColor = Color.Black;
                        }
                        else
                        {
                            DgvAttendance.Rows[iRowIndex].DefaultCellStyle.ForeColor = Color.Red;
                        }
                    }
                    else
                    {
                        DgvAttendance.Rows[iRowIndex].DefaultCellStyle.ForeColor = Color.Black;
                    }
                }

                DateTime atnDate = Convert.ToDateTime(DgvAttendance.Rows[iRowIndex].Cells["ColDgvAtnDate"].Value);
                string strAtnDate = atnDate.ToString("dd MMM yyyy");
                int iEmpId = Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvEmployee"].Tag);
                int iCmpId = Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvCompanyID"].Value);

                int iShiftId = Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvShift"].Tag);

                string sAllowedBreakTime = DgvAttendance.Rows[iRowIndex].Cells["ColDgvAllowedBreakTime"].Tag != null && DgvAttendance.Rows[iRowIndex].Cells["ColDgvAllowedBreakTime"].Tag.ToStringCustom() != string.Empty ? Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells["ColDgvAllowedBreakTime"].Tag) : "0";
                int iDayID = Convert.ToInt32(atnDate.DayOfWeek) + 1;
                string sFromDate = "";
                string sToDate = "";
                string sFromTime = "";
                string sToTime = "";
                string sDuration = "";

                int iShiftType = 0;
                int iNoOfTimings = 0;
                string sMinWorkHours = "0";
                int iLate = 0;
                int iEarly = 0;
                bool bOffDay = true;
                int iShiftOrderNo = 0;
                DataTable dtShiftInfo;
                dtShiftInfo = MobjclsBLLAttendance.GetShiftInfo(4, iEmpId, iShiftId, iDayID, sFromDate, sToDate, iCmpId);
                if (dtShiftInfo.Rows.Count > 0)
                {
                    bOffDay = false;
                    sFromTime = Convert.ToString(dtShiftInfo.Rows[0]["FromTime"]);
                    sToTime = Convert.ToString(dtShiftInfo.Rows[0]["ToTime"]);
                    sDuration = Convert.ToString(dtShiftInfo.Rows[0]["Duration"]);
                    iShiftType = Convert.ToInt32(dtShiftInfo.Rows[0]["ShiftTypeID"]);
                    iNoOfTimings = Convert.ToInt32(dtShiftInfo.Rows[0]["NoOfTimings"]);
                    sMinWorkHours = Convert.ToString(dtShiftInfo.Rows[0]["MinWorkingHours"]);
                    iLate = Convert.ToInt32(dtShiftInfo.Rows[0]["LateAfter"]);
                    iEarly = Convert.ToInt32(dtShiftInfo.Rows[0]["EarlyBefore"]);
                    sAllowedBreakTime = Convert.ToString(dtShiftInfo.Rows[0]["AllowedBreakTime"]);
                }
                if (bOffDay == true)
                {
                    dtShiftInfo = MobjclsBLLAttendance.GetShiftInfo(5, iEmpId, iShiftId, iDayID, sFromDate, sToDate, iCmpId);
                    if (dtShiftInfo.Rows.Count > 0)
                    {
                        sFromTime = Convert.ToString(dtShiftInfo.Rows[0]["FromTime"]);
                        sToTime = Convert.ToString(dtShiftInfo.Rows[0]["ToTime"]);
                        sDuration = Convert.ToString(dtShiftInfo.Rows[0]["Duration"]);
                        iShiftType = Convert.ToInt32(dtShiftInfo.Rows[0]["ShiftTypeID"]);
                        iNoOfTimings = Convert.ToInt32(dtShiftInfo.Rows[0]["NoOfTimings"]);
                        sMinWorkHours = Convert.ToString(dtShiftInfo.Rows[0]["MinWorkingHours"]);
                        sAllowedBreakTime = Convert.ToString(dtShiftInfo.Rows[0]["AllowedBreakTime"]);
                    }
                }
                if (iShiftType == 3)// Then 'for Dynamic Shift
                {
                    string dFirstTime = Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells[3].Value);
                    string sFirstTime = Convert.ToDateTime(dFirstTime).ToString("HH:mm");

                    iShiftOrderNo = GetShiftOrderNo(iShiftId, sFirstTime, ref sFromTime, ref  sToTime, ref sDuration);
                    MobjclsBLLAttendance.GetDynamicShiftDuration(iShiftId, iShiftOrderNo, ref sMinWorkHours, ref sAllowedBreakTime);
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvShiftOrderNo"].Value = iShiftOrderNo;
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvAllowedBreakTime"].Tag = Convert.ToDouble(sAllowedBreakTime);
                    string sAlldBreakT = sAllowedBreakTime;
                    sAlldBreakT = Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAlldBreakT), DateTime.Today).ToString();
                    sAlldBreakT = Convert.ToDateTime(sAlldBreakT).ToString("HH:mm:ss");
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvAllowedBreakTime"].Value = sAlldBreakT;

                }

                dtShiftInfo = MobjclsBLLAttendance.GetShiftInfo(3, iEmpId, iShiftId, iDayID, sFromDate, sToDate, iCmpId);
                blnIsMinWrkHRsAsOT = MobjclsBLLAttendance.isAfterMinWrkHrsAsOT(iShiftId);
                intDurationInMinutes = GetDurationInMinutes(sDuration);//Getting Duration In mintes
                int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(intDurationInMinutes), Convert.ToDateTime(sToTime));
                if (tmDurationDiff < 0) //For Night Shift
                {
                    blnNightShiftFlag = true;
                    sFromTime = (Convert.ToDateTime(sFromTime)).ToString();
                    sToTime = Convert.ToString(Convert.ToDateTime(sToTime).AddDays(1));
                }

                int iAdditionalMinutes = 0;
                bool bEarly = true;
                bool bLate = true;
                iAdditionalMinutes = MobjclsBLLAttendance.GetLeaveExAddMinutes(iEmpId, Convert.ToDateTime(strAtnDate));
                if (iAdditionalMinutes > 0)
                {
                    sAllowedBreakTime = Convert.ToString(Convert.ToInt32(sAllowedBreakTime) + Convert.ToInt32(iAdditionalMinutes));
                    bEarly = false;
                    bLate = false;
                }
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = "00:00:00";
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 0;
                lblLateComing.Visible = false;
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;
                lblEarlyGoing.Visible = false;

                int iBufferForOT = 0;
                bool blnConsiderBufferForOT = false;
                int iMinOtMinutes = 0;
                blnConsiderBufferForOT = MobjclsBLLAttendance.IsConsiderBufferTimeForOT(iShiftId, ref iBufferForOT, ref iMinOtMinutes);
                intMinWorkHrsInMinutes = GetDurationInMinutes(sMinWorkHours);//Minwork hrs Setting

                if (iShiftType <= 3)
                {

                    for (int j = 3; j <= k + 2; j++)
                    {

                        DateTime dResult = Convert.ToDateTime(DgvAttendance.Rows[iRowIndex].Cells[j].Value);
                        string sTimeValue = Convert.ToDateTime(dResult).ToString("HH:mm");
                        if (iShiftType == 3)//'Modified for Dynamic Shift
                        {
                            if (blnNightShiftFlag == true)
                            {
                                if (Convert.ToDateTime(sTimeValue) >= Convert.ToDateTime("12:00 AM") &&
                                    Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime("12:00 PM"))
                                { sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString("dd MMM yyyy HH:mm tt"); }

                            }
                        }

                        if (j == 3)
                        {
                            string sFrm = Convert.ToDateTime(sFromTime).ToString("HH:mm tt");
                            string sto = Convert.ToDateTime(sToTime).ToString("HH:mm tt");
                            if (blnNightShiftFlag && Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sto))
                            {
                                if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sFrm) && Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sFrm).AddMinutes(-120))
                                    sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString("dd MMM yyyy HH:mm tt");
                            }
                            if (bLate)
                            {
                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate) && iShiftType != 2)
                                {

                                    int il = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                    if (il > 0)
                                    {
                                        string sLate = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(il), DateTime.Today));
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = Convert.ToDateTime(sLate).ToString("HH:mm:ss");
                                    }

                                    lblLateComing.Tag = 1;
                                    lblLateComing.Visible = true;
                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 1;//'if 1 then LateComing=yes

                                }
                                else
                                {
                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = "00:00:00";
                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 0;
                                    lblLateComing.Tag = 0;
                                    lblLateComing.Visible = false;
                                }
                            }
                        }
                        else
                        {

                            int tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue));

                            if (tmDiff < 0)
                            {
                                tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1));
                                sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString();
                            }
                            if (j % 2 == 0)
                            {
                                if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly) && iShiftType != 2 && bEarly == true)
                                {
                                    int iE = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sTimeValue), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                    if (iE > 0)
                                    {
                                        string sEarly = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iE), DateTime.Today));
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = Convert.ToDateTime(sEarly).ToString("HH:mm:ss");
                                    }

                                    lblEarlyGoing.Visible = true;
                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 1; //'Earlygoing yes

                                }

                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                {

                                    if (bShiftChangeTime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime))
                                    {
                                        bShiftChangeTime = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime));
                                        int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));

                                        sOt = Convert.ToString(Convert.ToDouble(sOt) + idiffCurtShiftOT);
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                    }

                                    else
                                    {

                                        sOt = Convert.ToString(Convert.ToDouble(sOt) + Convert.ToDouble(tmDiff));

                                    }
                                }
                                else
                                {
                                    bool bflag = false;

                                    if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sFromTime))
                                        bflag = true;

                                    if (bShiftChangeTimeEarlyWRk && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime))
                                    {
                                        bShiftChangeTimeEarlyWRk = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sTimeValue));
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork.ToString());
                                        bflag = true;
                                    }
                                    if (bflag == false)
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff));

                                }

                            }
                            else//brek time
                            {


                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                {
                                    lblEarlyGoing.Visible = false;
                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;

                                    if (bShiftChangeTimeBrkOT = true && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                    {
                                        bShiftChangeTimeBrkOT = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                        int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(idiffPrevShitWork));
                                        sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(idiffCurtShiftOT));
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                            sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(tmDiff));
                                    }
                                }
                                else
                                {

                                    bool bflag = false;
                                    if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sFromTime).AddMinutes(iLate))
                                    {
                                        bflag = true;
                                    }
                                    if (bShiftChangeTimeEarlyBRK && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sFromTime).AddMinutes(iLate) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate))
                                    {
                                        bShiftChangeTimeEarlyBRK = false;
                                        int idiffPrevShitBreak = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + idiffPrevShitBreak);
                                        bflag = true;
                                    }
                                    if (bflag == false)
                                    {
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(tmDiff));
                                    }

                                    lblEarlyGoing.Visible = false;
                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;
                                }

                            }
                        }

                        sPreviousTime = sTimeValue;
                        if (Glbln24HrFormat == false)
                        {
                            sTimeValue = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                            DgvAttendance.Rows[iRowIndex].Cells[j].Value = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                        }
                        else
                        {
                            DgvAttendance.Rows[iRowIndex].Cells[j].Value = Convert.ToDateTime(dResult).ToString("HH:mm");
                        }


                    }//for
                }
                else if (iShiftType == 4)
                {
                    int intMinWorkHrsInMinutesIf = 0;
                    intDurationInMinutes = GetSplitShiftTotalDuration(iShiftId);
                    sWorkTime = "0";
                    sBreakTime = "0";
                    sOt = "0";
                    int intTotalSplitAbsentTm = 0;
                    int intTotalAbsentTm = 0;
                    WorktimeCalculationForSplitShift(iShiftId, k, iRowIndex, sFromTime, sToTime, iLate, iEarly, bLate,
                                                     bEarly, "", blnIsMinWrkHRsAsOT, blnConsiderBufferForOT, iBufferForOT, ref sWorkTime, ref sBreakTime,
                                                     ref sOt, ref intTotalSplitAbsentTm, ref intTotalAbsentTm);


                    if (intDurationInMinutes > intMinWorkHrsInMinutes)  //'Split shift Absent           
                    {

                        if (Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Leave)) //Then 'for halfday
                        {
                            intMinWorkHrsInMinutesIf = intMinWorkHrsInMinutes / 2;
                        }
                        else
                        {
                            intMinWorkHrsInMinutesIf = intMinWorkHrsInMinutes;
                        }

                        if (iAdditionalMinutes > 0)
                            iAbsentTime = intMinWorkHrsInMinutesIf - (Convert.ToInt32(sWorkTime) + iAdditionalMinutes);
                        else
                            iAbsentTime = intMinWorkHrsInMinutesIf - Convert.ToInt32(sWorkTime);
                    }
                    else
                    {
                        if (intTotalSplitAbsentTm > (Convert.ToInt32(sAllowedBreakTime) + iEarly + iLate))
                        {
                            iAbsentTime = intTotalAbsentTm + (intTotalSplitAbsentTm - (Convert.ToInt32(sAllowedBreakTime) + iEarly + iLate));
                        }
                        else
                        {
                            iAbsentTime = intTotalAbsentTm;
                        }
                    }
                }

                if (Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Leave)) //Then 'for halfday
                {
                    if (Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells["ColDgvLeaveInfo"].Value) == "1")
                    {
                        if (Convert.ToDouble(sWorkTime) >= (intMinWorkHrsInMinutes / 2))
                        {

                            if (blnIsMinWrkHRsAsOT == true)// Then 'Checking whether calculation is based on minimum working hour 
                            {
                                int ihalfday = (intMinWorkHrsInMinutes / 2);
                                int addOt = Convert.ToInt32(sWorkTime) - ihalfday;
                                sWorkTime = ihalfday.ToString();
                                sOt = Convert.ToString(Convert.ToInt32(sOt) + addOt);
                            }
                        }
                        if (intDurationInMinutes > 0)
                            intDurationInMinutes = intDurationInMinutes / 2;
                        if (intMinWorkHrsInMinutes > 0)
                            intMinWorkHrsInMinutes = intMinWorkHrsInMinutes / 2;
                    }
                }
                else
                {

                    if (blnIsMinWrkHRsAsOT == true)
                    {
                        if (Convert.ToDouble(sWorkTime) == Convert.ToDouble(intMinWorkHrsInMinutes))
                        { }//'
                        else if (Convert.ToDouble(sWorkTime) < Convert.ToDouble(intMinWorkHrsInMinutes))
                        { //'sOt = "0"
                            if (intDurationInMinutes > intMinWorkHrsInMinutes)
                            { sOt = "0"; }
                            else// 'duration and minWorkhours are same then Consider Convert.ToInt32(sAllowedBreak) + iEarly + iLate)
                            {
                                if (Convert.ToInt32(sWorkTime) + Convert.ToInt32(sAllowedBreakTime) + iEarly + iLate < intMinWorkHrsInMinutes)
                                { sOt = "0"; }
                            }
                        }
                        else if (Convert.ToDouble(sWorkTime) > Convert.ToDouble(intMinWorkHrsInMinutes))
                        {
                            int Ot = Convert.ToInt32(sWorkTime) - Convert.ToInt32(intMinWorkHrsInMinutes);
                            sOt = Convert.ToString(Convert.ToInt32(sOt) + Ot);
                            sWorkTime = intMinWorkHrsInMinutes.ToString();
                        }
                    }

                }
                if (Convert.ToInt32(sOt) < iMinOtMinutes)
                    sOt = "0";
                if (sOt != "" && sOt != "0")//Ot Template
                    sOt = GetOt(sOt);

                bool blnIsHoliday = false;
                blnIsHoliday = MobjclsBLLAttendance.IsHoliday(iEmpId, iCmpId, atnDate, iDayID); //'cheking current date is holiday

                if (iShiftType <= 3)
                {

                    if (iAdditionalMinutes > 0)
                        intMinWorkHrsInMinutes = intMinWorkHrsInMinutes - iAdditionalMinutes;

                    if (intDurationInMinutes > intMinWorkHrsInMinutes)
                    {
                        iAbsentTime = intMinWorkHrsInMinutes - Convert.ToInt32(sWorkTime);
                    }
                    else
                    {
                        iAbsentTime = intMinWorkHrsInMinutes - (Convert.ToInt32(sWorkTime) + Convert.ToInt32(sAllowedBreakTime) + iEarly + iLate);
                    }
                }

                int iShortage = 0;
                if (iShiftType == 2 || iShiftType == 4)
                { }
                else
                {
                    if (blnIsHoliday == false)
                    {
                        iShortage = Convert.ToInt32(sBreakTime == "0" ? "0" : sBreakTime) - Convert.ToInt32(sAllowedBreakTime);
                        if (iShortage > iAbsentTime)
                        {
                            iAbsentTime = iShortage;
                        }
                    }
                }
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvAbsentTime"].Value = iAbsentTime > 0 ? iAbsentTime : 0;

                int iAbsent = iAbsentTime > 0 ? iAbsentTime : 0;
                lblShortageTime.ForeColor = iAbsent > 0 ? Color.DarkRed : System.Drawing.SystemColors.ControlText;
                string sShortage = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iAbsent), DateTime.Today));
                lblShortageTime.Text = Convert.ToDateTime(sShortage).ToString("HH:mm:ss");
                //------------------------------------

                lblAllowedBreakTime.Text = Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells["ColDgvAllowedBreakTime"].Value);

                DgvAttendance.Rows[iRowIndex].Cells["ColDgvWorkTime"].Tag = Convert.ToDouble(sWorkTime);
                sWorkTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sWorkTime), DateTime.Today));
                sWorkTime = Convert.ToDateTime(sWorkTime).ToString("HH:mm:ss");
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvWorkTime"].Value = sWorkTime;
                lblWorkTime.Text = sWorkTime;
                if (sBreakTime != "" && sBreakTime != "0")
                {
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvBreakTime"].Tag = sBreakTime;
                    sBreakTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sBreakTime), DateTime.Today));
                    sBreakTime = Convert.ToDateTime(sBreakTime).ToString("HH:mm:ss");
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvBreakTime"].Value = sBreakTime;
                    lblBreakTime.Text = sBreakTime;
                }
                else
                {
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvBreakTime"].Tag = 0;
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvBreakTime"].Value = "";
                    lblBreakTime.Text = "00:00:00";
                }

                if (Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells["ColDgvWorkTime"].Value) != "" && Convert.ToDouble(sOt) > 0)
                {
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvExcessTime"].Tag = sOt;
                    sOt = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sOt), DateTime.Today));
                    sOt = Convert.ToDateTime(sOt).ToString("HH:mm:ss");
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvExcessTime"].Value = sOt;
                    lblExcessTime.Text = sOt;
                }
                else
                {
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvExcessTime"].Tag = 0;
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvExcessTime"].Value = "";
                    lblExcessTime.Text = "00:00:00";
                }

                return true;
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }

        }

        private bool WorktimeCalculationForSplitShift(int iShiftID, int k, int iRowIndex, string sFromTime, string sToTime, int iLate, int iEarly,
                                                      bool bLate, bool bEarly, string sEmpName, bool isAfterMinWrkHrsAsOT, bool blnConsiderBufferForOT, int iBufferForOT,
                                                      ref string sWorkTime, ref  string sBreakTime, ref string sOt, ref int intTotalSplitAbsentTm, ref int intTotalAbsentTm)
        {//'Worktime Calculation for Split shift 
            try
            {
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvShiftOrderNo"].Value = 0;//Dynamic Shift
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = "00:00:00";
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 0;
                lblLateComing.Visible = false;
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;
                lblEarlyGoing.Visible = false;

                sWorkTime = "0";
                sBreakTime = "0";
                sOt = "0";
                string sOvertime = "0";
                bool nightFlag = false;

                int intWorkTm = 0;
                int intBreakTm = 0;
                int intAbsentTm = 0;
                int intSplitAbsentTm = 0;
                intTotalSplitAbsentTm = 0;
                intTotalAbsentTm = 0;

                DataTable DtShiftDetails = MobjclsBLLAttendance.GetDynamicShiftDetails(iShiftID);

                if (DtShiftDetails.Rows.Count > 0)
                {
                    for (int i = 0; i <= DtShiftDetails.Rows.Count - 1; i++)
                    {

                        string sSplitFromTime = Convert.ToString(DtShiftDetails.Rows[i]["FromTime"]);
                        string sSplitToTime = Convert.ToString(DtShiftDetails.Rows[i]["ToTime"]);
                        string sDuration = Convert.ToString(DtShiftDetails.Rows[i]["Duration"]);
                        int iMinWorkingHrsMin = GetDurationInMinutes(sDuration);
                        //'Modified for Split Shift
                        if (nightFlag == true)
                        {
                            sSplitFromTime = Convert.ToDateTime(sSplitFromTime).AddDays(1).ToString();
                            sSplitToTime = Convert.ToDateTime(sSplitToTime).AddDays(1).ToString();
                        }

                        int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime).AddMinutes(iMinWorkingHrsMin), Convert.ToDateTime(sSplitToTime));
                        if (tmDurationDiff < 0)
                        {
                            nightFlag = true;
                            sSplitFromTime = Convert.ToDateTime(sSplitFromTime).ToString();
                            sSplitToTime = Convert.ToDateTime(sSplitToTime).AddDays(1).ToString();
                        }

                        string sPreviousTime = "";
                        bool bShiftChangeTime = true;
                        bool bShiftChangeOvertime = true;
                        bool bShiftChangeTimeBrkOT = true;
                        bool bShiftChangeTimeEarlyWRk = true;
                        bool bShiftChangeTimeEarlyBRK = true;
                        string sOTBreakTime = "0";

                        for (int j = 3; j <= k + 2; j++)
                        {
                            string dResult = Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells[j].Value);

                            string sTimeValue = Convert.ToDateTime(dResult).ToString("HH:mm");

                            if (j == 3)// Then ''first time
                            {
                                if (bLate)
                                {
                                    if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate)) //'bFlexi = False
                                    {
                                        int il = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                        if (il > 0)
                                        {
                                            string sLate = DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(il), DateTime.Today).ToString();
                                            DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = Convert.ToDateTime(sLate).ToString("HH:mm:ss");
                                        }
                                        lblLateComing.Tag = 1;
                                        lblLateComing.Visible = true;
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 1;//'if 1 then LateComing=yes
                                    }
                                    else
                                    {
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = "00:00:00";
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 0;
                                        lblLateComing.Tag = 0;
                                        lblLateComing.Visible = false;
                                    }
                                }
                            }
                            else
                            {
                                int tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue));
                                //' If tmDiff < 0 Then tmDiff = DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1))
                                if (tmDiff < 0)//Then 'if date diff less than Zero then adding day to the StimeValue
                                {
                                    tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1));
                                    sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString();
                                }
                                //'------------------------------------------------------------------------------------------------
                                //' workTime Time Calculation-
                                if (j % 2 == 0) //Then 'Mode=0 for workTime
                                { //'-------------------------------------Early Going validating 
                                    if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly) && bEarly == true)//Then 'bFlexi = False
                                    {
                                        int iE = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sTimeValue), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                        if (iE > 0)
                                        {
                                            string sEarly = DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iE), DateTime.Today).ToString();
                                            DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = Convert.ToDateTime(sEarly).ToString("HH:mm:ss");
                                        }

                                        lblEarlyGoing.Visible = true;
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 1; //'Earlygoing yes
                                    } //'-------------------------------------Early Going 
                                    //'---------------------------------------------Split Over Time-------------------------------------------

                                    if (i == DtShiftDetails.Rows.Count - 1)
                                    {
                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                        {
                                            if (bShiftChangeOvertime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime))
                                            {
                                                bShiftChangeOvertime = false;
                                                int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                                if (i == DtShiftDetails.Rows.Count - 1)
                                                {
                                                    sOvertime = Convert.ToString(Convert.ToDouble(sOvertime) + idiffCurtShiftOT);
                                                }
                                            }
                                            else
                                            {
                                                if (i == DtShiftDetails.Rows.Count - 1)
                                                {
                                                    sOvertime = Convert.ToString(Convert.ToDouble(sOvertime) + Convert.ToDouble(tmDiff));//'WorkTime Calculation 
                                                }
                                            }
                                        }
                                    }
                                    //'--------------------------------------------------------------------------------------------------------
                                    if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime))
                                    { //' DgvAttendanceFetch.Rows(iRowIndex).Cells("EarlyGoing").Value = 0
                                        if (bShiftChangeTime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitToTime))
                                        {
                                            bShiftChangeTime = false;
                                            int idiffPrevShitWork = 0;
                                            if (Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime))// 'modified 28 dec
                                            {
                                                idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sSplitToTime));
                                            }
                                            else
                                            {
                                                idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sSplitToTime));
                                            }
                                            int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitToTime), Convert.ToDateTime(sTimeValue));
                                            if (i == DtShiftDetails.Rows.Count - 1)
                                            {
                                                sOt = Convert.ToString(Convert.ToDouble(sOt) + idiffCurtShiftOT);
                                            }
                                            sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                        }
                                        else
                                        {
                                            if (i == DtShiftDetails.Rows.Count - 1)
                                            {
                                                sOt = Convert.ToString(Convert.ToDouble(sOt) + Convert.ToDouble(tmDiff));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //'Modification -----------------------------12-11-2010
                                        bool bflag = false;
                                        if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sSplitFromTime))
                                        {
                                            bflag = true;
                                        } //'sWorkTime = Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff)
                                        if (bShiftChangeTimeEarlyWRk && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitFromTime))
                                        {
                                            bShiftChangeTimeEarlyWRk = false;
                                            int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sTimeValue));
                                            sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                            bflag = true;
                                        }
                                        if (bflag == false)
                                        {
                                            sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff));
                                        }
                                        //'Modification -----------------------------End 12-11-2010
                                    }
                                    //'---------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                }
                                else //'Break Time Calculation-where mode=1
                                {
                                    if (i == DtShiftDetails.Rows.Count - 1)
                                    {
                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                        {
                                            if (bEarly)
                                            {

                                                lblEarlyGoing.Visible = false;
                                                DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;
                                                DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                                            }
                                        }
                                    }

                                    if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime))
                                    {
                                        if (bShiftChangeTimeBrkOT == true && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitToTime))
                                        {
                                            bShiftChangeTimeBrkOT = false;
                                            int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sSplitToTime));//'break time= before ToTime- Early giong minutes
                                            int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitToTime), Convert.ToDateTime(sTimeValue));// 'Ot Break time = after To time
                                            sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(idiffPrevShitWork));//;'Break Time Calculation
                                            sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(idiffCurtShiftOT));
                                        }
                                        else
                                        {
                                            if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime)) { sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(tmDiff)); } //'Break Time Calculation Ot
                                        }
                                    }
                                    else
                                    {
                                        //'Modification -----------------------------12-11-2010
                                        bool bflag = false;
                                        if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sSplitFromTime))
                                        {
                                            bflag = true;
                                        } //'sWorkTime = Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff)
                                        if (bShiftChangeTimeEarlyBRK && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitFromTime))
                                        {
                                            bShiftChangeTimeEarlyBRK = false;
                                            int idiffPrevShitBreak = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sTimeValue));
                                            sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + idiffPrevShitBreak);
                                            bflag = true;
                                        }
                                        if (bflag == false)
                                        {
                                            sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(tmDiff));
                                        }


                                        lblEarlyGoing.Tag = 0;
                                        lblEarlyGoing.Visible = false;
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";

                                    }
                                }
                            }
                            sPreviousTime = sTimeValue;
                            if (Glbln24HrFormat == false)
                            {
                                sTimeValue = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                                DgvAttendance.Rows[iRowIndex].Cells[j].Value = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                            }
                            else
                            {
                                DgvAttendance.Rows[iRowIndex].Cells[j].Value = Convert.ToDateTime(dResult).ToString("HH:mm");
                            }

                        } //'For j = 3 To k + 2 'Time selecting filter wit date and id 

                        //'------------------------------------Shoratge Calculation starts----------------------------------
                        intSplitAbsentTm = 0;

                        intAbsentTm = 0;
                        if (intWorkTm == Convert.ToInt32(sWorkTime) && intBreakTm == Convert.ToInt32(sBreakTime))
                        { intAbsentTm = iMinWorkingHrsMin; }
                        else
                        {
                            intSplitAbsentTm = iMinWorkingHrsMin - (Convert.ToInt32(sWorkTime) - intWorkTm);
                            if (intSplitAbsentTm > 0)
                            { intTotalSplitAbsentTm += intSplitAbsentTm; }
                        }
                        if (intAbsentTm > 0)
                        { intTotalAbsentTm += intAbsentTm; }


                        intWorkTm = Convert.ToInt32(sWorkTime);
                        intBreakTm = Convert.ToInt32(sBreakTime);
                        //'------------------------------------Shoratge Calculation Ends------------------------------------

                    } //'For i As Integer = 0 To DtShiftDetails.Rows.Count - 1

                    //if (Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Leave) && DgvAttendance.Rows[iRowIndex].Cells["ColDgvLeaveInfo"].Value.ToInt32() == 1)
                    //{
                    if (Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Leave) || DgvAttendance.Rows[iRowIndex].Cells["ColDgvLeaveFlag"].Value.ToInt32() == 1)
                    {
                        intTotalSplitAbsentTm = 0;
                        intTotalAbsentTm = 0;
                    }
                    if (isAfterMinWrkHrsAsOT == false)
                    {
                        sOt = sOvertime;
                    }
                } //'  If DtShiftDetails.Rows.Count > 0

                return true;
            }

            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on WorktimeCalculationForSplitShift:Attendance auto." + Ex.Message.ToString() + "", 3);
                return false;
            }
        }
        private string GetOt(string StrOt)
        {
            string StrOtReturn = "0";
            try
            {
                int iOt = 0;
                if (Int32.TryParse(StrOt, out iOt) == false)
                    StrOt = "0";

                StrOtReturn = StrOt;
                string strStartMin = "";
                string strEndMin = "";
                string strOTMin = "";

                DataTable dtOtDetails = MobjclsBLLAttendance.GetOtDetails();
                if (dtOtDetails != null)
                {
                    if (dtOtDetails.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtOtDetails.Rows)
                        {
                            strStartMin = (row["StartMin"] == DBNull.Value ? "0" : Convert.ToString(row["StartMin"]));
                            strEndMin = (row["EndMin"] == DBNull.Value ? "0" : Convert.ToString(row["EndMin"]));
                            strOTMin = (row["OTMin"] == DBNull.Value ? "0" : Convert.ToString(row["OTMin"]));
                            if (iOt >= Convert.ToInt32(strStartMin) && iOt <= Convert.ToInt32(strEndMin))
                            {
                                StrOtReturn = strOTMin;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

            }
            return StrOtReturn;
        }

        private bool LoadCombo(int iType)
        {
            try
            {
                DataTable datCombos;

                if (iType == 0 || iType == 1)
                {
                    datCombos = new DataTable();
                    //'Load Comapny Combo
                    CboCompany.DataSource = null;
                    datCombos = MobjclsBLLAttendance.FillCombos(new string[] { "Distinct C.CompanyID,C.CompanyName", "CompanyMaster AS C INNER JOIN UserCompanyDetails AS U ON C.CompanyID = U.CompanyID", " U.UserID = "+ ClsCommonSettings.UserID +" AND U.CompanyID= "+ ClsCommonSettings.LoginCompanyID  +"", "CompanyID", "CompanyName" });
                    CboCompany.DisplayMember = "CompanyName";
                    CboCompany.ValueMember = "CompanyID";
                    CboCompany.DataSource = datCombos;
                }

                if (iType == 0 || iType == 2)
                {
                    //Load WorkStatus Combo
                    datCombos = new DataTable();
                    cboEmpWorkstatus.DataSource = null;
                    datCombos = MobjclsBLLAttendance.FillCombos(new string[] { "WorkStatusID,WorkStatus", "WorkStatusReference union select 0,'All' as WorkStatus from WorkStatusReference  union select -1,'' as WorkStatus from WorkStatusReference  ORDER BY WorkStatus ", " ", "WorkStatusID", "WorkStatus" });
                    cboEmpWorkstatus.ValueMember = "WorkStatusID";
                    cboEmpWorkstatus.DisplayMembers = "WorkStatus";
                    cboEmpWorkstatus.DataSource = datCombos;
                }

                if (iType == 0 || iType == 3)
                {
                    CboEmpFilter.Items.Add("All");
                    CboEmpFilter.Items.Add("Department");
                    CboEmpFilter.Items.Add("Designation");
                    CboEmpFilter.Items.Add("Employment Type");
                    CboEmpFilter.SelectedIndex = 0;

                }

                if (iType == 0 || iType == 4)
                {
                    //' CboEmpFilter.Text = "All"  
                    CboEmpFilterType.DataSource = null;
                    CboEmpFilterType.Items.Clear();

                    datCombos = new DataTable();
                    datCombos = MobjclsBLLAttendance.FillCombos(new string[] { " DISTINCT '0' as DepartmentID,'All' as Description ", "DepartmentReference", " ", "DepartmentID", "Description" });
                    CboEmpFilterType.ValueMember = "DepartmentID";
                    CboEmpFilterType.DisplayMember = "Description";
                    CboEmpFilterType.DataSource = datCombos;
                    CboEmpFilterType.SelectedIndex = 0;

                }
                if (iType == 5)
                {
                    CboEmpFilterType.DataSource = null;
                    CboEmpFilterType.Items.Clear();
                    datCombos = new DataTable();

                    datCombos = MobjclsBLLAttendance.FillCombos(new string[] { "0 AS DepartmentID,'All' AS Description UNION SELECT DepartmentID,Department AS Description ", "DepartmentReference", "" });
                    CboEmpFilterType.ValueMember = "DepartmentID";
                    CboEmpFilterType.DisplayMember = "Description";
                    CboEmpFilterType.DataSource = datCombos;
                    CboEmpFilterType.SelectedIndex = 0;

                }
                if (iType == 6)
                {
                    CboEmpFilterType.DataSource = null;
                    CboEmpFilterType.Items.Clear();
                    datCombos = new DataTable();

                    //CboEmpFilter.SelectedIndex = 2-Designation
                    datCombos = MobjclsBLLAttendance.FillCombos(new string[] { "0 AS DesignationID,'All' AS Description UNION SELECT DesignationID,Designation AS Description ", "DesignationReference", "" });
                    CboEmpFilterType.ValueMember = "DesignationID";
                    CboEmpFilterType.DisplayMember = "Description";
                    CboEmpFilterType.DataSource = datCombos;
                    if (CboEmpFilterType.Items.Count > 0)
                    {
                        CboEmpFilterType.SelectedIndex = 0;
                    }

                }

                if (iType == 7)
                {

                    CboEmpFilterType.DataSource = null;
                    CboEmpFilterType.Items.Clear();

                    datCombos = new DataTable();

                    datCombos = MobjclsBLLAttendance.FillCombos(new string[] { "0 AS EmploymentTypeID,'All' AS Description UNION SELECT EmploymentTypeID,EmploymentType AS Description ", "EmploymentTypeReference", "" });
                    CboEmpFilterType.ValueMember = "EmploymentTypeID";
                    CboEmpFilterType.DisplayMember = "Description";
                    CboEmpFilterType.DataSource = datCombos;//' CboEmpFilter.SelectedIndex = 3 -Employment Type                   
                    CboEmpFilterType.SelectedIndex = 0;
                    datCombos = null;

                }

                if (iType == 0 || iType == 8) //Then 'Loading Attendnace Status
                {
                    datCombos = new DataTable();
                    datCombos = MobjclsBLLAttendance.FillCombos(new string[] { "AttendenceStatusID,AttendanceStatus", "PayAttendenceStatusReference", "", "AttendenceStatusID", "AttendanceStatus" });
                    ColDgvStatus.ValueMember = "AttendenceStatusID";
                    ColDgvStatus.DisplayMember = "AttendanceStatus";
                    ColDgvStatus.DataSource = datCombos;
                }
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on LoadCombo " + this.Name + " " + Ex.Message.ToString(), 1);
                //MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }

        private string ConvertDate24(DateTime dateString)
        {
            try
            {
                DTPFormatTime.CustomFormat = "HH:mm";
                DTPFormatTime.Text = dateString.ToString();
                return DTPFormatTime.Text;
            }
            catch (Exception)
            {
                return " ";
            }
        }

        private string Convert24HrFormat(string StrValue)
        {
            try
            {
                StrValue = FormatString(StrValue);

                if (StrValue.IndexOf(".") != -1)
                    StrValue = StrValue.Replace(".", ":");

                string ReturnTime = "";
                if (StrValue.IndexOf(":") != -1)
                {
                    string LeftTime = "";
                    string RightTime = "";
                    string[] strar = new string[2];
                    strar = StrValue.Split(':');
                    LeftTime = strar[0];
                    RightTime = strar[1];
                    if (Microsoft.VisualBasic.Information.IsNumeric(RightTime))
                    {
                        if ((RightTime.Length) > 2)
                            return Microsoft.VisualBasic.Strings.Left(StrValue, 2);

                    }
                }
                if (StrValue.IndexOf(":") != -1)
                {
                    string LeftTime = "";
                    string RightTime = "";
                    string Time1 = "";
                    string time2 = "";
                    string[] strar = new string[2];
                    strar = StrValue.Split(':');
                    LeftTime = strar[0];
                    RightTime = strar[1];
                    if (Microsoft.VisualBasic.Information.IsNumeric(LeftTime))
                    {
                        if (Convert.ToInt32(LeftTime) > 23)
                            return StrValue;
                        else
                        {
                            if (LeftTime.Length == 2)
                                Time1 = LeftTime;
                            else if (LeftTime.Length == 1)
                                Time1 = "0" + LeftTime;

                        }
                    }
                    else
                        return "10p";

                    if (Microsoft.VisualBasic.Information.IsNumeric(RightTime))
                        if (Convert.ToInt32(RightTime) > 59)
                            return StrValue;
                        else
                        {
                            if (RightTime.Length == 2)
                                time2 = RightTime;
                            else if (RightTime.Length == 1)
                                time2 = "0" + RightTime;

                        }
                    else
                        return "10p";

                    ReturnTime = Time1 + ":" + time2;
                    return ReturnTime;
                }
                else
                {

                    if (Microsoft.VisualBasic.Information.IsNumeric(StrValue))
                    {
                        if (Convert.ToInt32(StrValue) > 23)
                            return StrValue;
                        else
                        {
                            if (StrValue.Length == 2)
                            {
                                ReturnTime = StrValue + ":" + "00";
                                return ReturnTime;
                            }
                            else if (StrValue.Length == 1)
                            {
                                ReturnTime = "0" + StrValue + ":" + "00";
                                return ReturnTime;
                            }
                            else
                                return StrValue;

                        }
                    }
                    else
                    {
                        return StrValue;
                    }

                }
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return StrValue;
            }
        }

        private string ConvertDate24New(string dateString)
        {
            try
            {
                DTPFormatTime.CustomFormat = "HH:mm";
                DTPFormatTime.Text = dateString;
                return DTPFormatTime.Text.ToString();
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
                return DTPFormatTime.Text.ToString();
            }

        }

        private string AMPMConvert(string StrValue)
        {

            try
            {
                StrValue = FormatString(StrValue);
                if (StrValue.IndexOf(".") != -1)
                    StrValue = StrValue.Replace('.', ':');

                if (Microsoft.VisualBasic.Information.IsNumeric(StrValue))
                {
                    if ((StrValue).Length == 1)
                    {
                        if (Convert.ToInt32(StrValue) < 12)
                            StrValue = StrValue + "A";
                        else
                            StrValue = StrValue + "P";

                    }
                    else if (StrValue.Length == 2)
                    {
                        if (Convert.ToInt32(StrValue) < 12)
                            StrValue = StrValue + "A";
                        else
                            StrValue = StrValue + "P";

                    }
                }

                if (StrValue.IndexOf(":") != -1)
                {
                    string LeftTime = "";
                    string RightTime = "";
                    string[] strar = new string[2];

                    strar = StrValue.Split(':');
                    LeftTime = strar[0];
                    RightTime = strar[1];
                    if (Microsoft.VisualBasic.Information.IsNumeric(RightTime))
                    {
                        if (RightTime.Length > 2)
                            return Microsoft.VisualBasic.Strings.Left(StrValue, 2);

                    }
                }


                DateTime CForDate;
                if (StrValue.IndexOf("AM") != -1)
                {

                    StrValue = StrValue.Substring(0, StrValue.IndexOf("M") + 1);

                    CForDate = Convert.ToDateTime(StrValue.Replace("AM", "AM"));
                    if (Glbln24HrFormat == false)
                        return CForDate.ToString("hh:mm tt").Trim();
                    else
                        return CForDate.ToString("HH:mm").Trim();

                    // return AMPMConvert("");
                }
                else if (StrValue.IndexOf("A") != -1)
                {
                    StrValue = StrValue.Substring(0, StrValue.IndexOf("A") + 1);
                    CForDate = Convert.ToDateTime(StrValue.Replace("A", "AM"));
                    if (Glbln24HrFormat == false)
                        return CForDate.ToString("hh:mm tt").Trim();
                    else
                        return CForDate.ToString("HH:mm").Trim();

                    //Return AMPMConvert
                }
                else if (StrValue.IndexOf("PM") != -1)
                {
                    StrValue = StrValue.Substring(0, StrValue.IndexOf("M") + 1);
                    CForDate = Convert.ToDateTime(StrValue.Replace("PM", "PM"));
                    if (Glbln24HrFormat == false)
                        return CForDate.ToString("hh:mm tt").Trim();
                    else
                        return CForDate.ToString("HH:mm").Trim();

                    //Return AMPMConvert
                }
                else if (StrValue.IndexOf("P") != -1)
                {

                    StrValue = StrValue.Substring(0, StrValue.IndexOf("P") + 1);
                    CForDate = Convert.ToDateTime(StrValue.Replace("P", "PM"));
                    if (Glbln24HrFormat == false)
                        return CForDate.ToString("hh:mm tt").Trim();
                    else
                        return CForDate.ToString("HH:mm").Trim();

                    //Return AMPMConvert
                }
                else
                {
                    if (StrValue.Length > 3)
                        return Microsoft.VisualBasic.Strings.Left(StrValue, 2);

                    return StrValue;
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return StrValue;
            }

        }

        private string FormatString(string strTime)
        {
            if (strTime.ToStringCustom() == string.Empty)
                return "00:00";

            string strTT = "";
            if (strTime.ToLower().Contains('a'))
                strTT = "AM";
            else if (strTime.ToLower().Contains('p'))
                strTT = "PM";

            strTime = strTime.ToLower();
            strTime = strTime.Replace(":", ".");
            strTime = strTime.Replace("a", "");
            strTime = strTime.Replace("p", "");
            strTime = strTime.Replace("m", "");

            string strOutput = "00:00";
            decimal decOutput = 0M;

            if (Decimal.TryParse(strTime, out decOutput))
            {
                strOutput = String.Format("{0:N2}", decOutput).Replace(".", ":").Replace(",", "");

                if (decOutput > 0)
                {
                    string[] arr = strOutput.Split(':');
                    if (arr.Length >= 2)
                    {
                        int intLeft = Convert.ToInt32(arr[0]);
                        int intRight = Convert.ToInt32(arr[1]);
                        intLeft = intLeft > 23 ? 0 : intLeft;
                        intRight = intRight > 59 ? 0 : intRight;
                        if (intRight < 10)
                        {
                            string strRight = "0" + intRight.ToString();
                            strOutput = String.Format("{0:N2}", Convert.ToDecimal(intLeft.ToString() + "." + strRight)).Replace(".", ":").Replace(",", "");

                        }
                        else
                        {
                            strOutput = String.Format("{0:N2}", Convert.ToDecimal(intLeft.ToString() + "." + intRight.ToString())).Replace(".", ":").Replace(",", "");

                        }

                    }
                }
            }
            strOutput = strOutput + " " + strTT;
            strOutput = strOutput.TrimEnd().TrimStart();
            return strOutput;

        }

        private bool UpdatedPunchingData(ArrayList PunchingData, int IRow)
        {
            try
            {
                if (PunchingData.Count % 2 == 0)//'1 odd
                {
                    DgvAttendance.Rows[IRow].DefaultCellStyle.ForeColor = Color.Black;
                    DgvAttendance.Rows[IRow].Cells["ColDgvValidFlag"].Value = 1;// ' if Zero then Invalid Data
                }
                int intStart = DgvAttendance.Columns["ColDgvAddMore"].Index - 1;

                for (int i = intStart; i <= PunchingData.Count + 1; i += 2)
                {
                    int intLoc = i;
                    int intCnt = i / 2;
                    DataGridViewTextBoxColumn adcolIn = new DataGridViewTextBoxColumn();
                    adcolIn.Name = "TimeIn" + intCnt.ToString();
                    adcolIn.HeaderText = "TimeIn" + intCnt.ToString();
                    adcolIn.SortMode = DataGridViewColumnSortMode.NotSortable;
                    adcolIn.Resizable = DataGridViewTriState.False;
                    adcolIn.Width = 70;
                    adcolIn.MaxInputLength = 9;

                    DataGridViewTextBoxColumn adcolout = new DataGridViewTextBoxColumn();
                    adcolout.Name = "TimeOut" + intCnt.ToString();
                    adcolout.HeaderText = "TimeOut" + intCnt.ToString();
                    adcolout.SortMode = DataGridViewColumnSortMode.NotSortable;
                    adcolout.Resizable = DataGridViewTriState.False;
                    adcolout.Width = 70;
                    adcolout.MaxInputLength = 9;

                    DgvAttendance.Columns.Insert(intLoc + 1, adcolIn);
                    DgvAttendance.Columns.Insert(intLoc + 2, adcolout);
                    MintTimingsColumnCnt += 2;
                }

                for (int iPunch = 0; iPunch <= PunchingData.Count - 1; iPunch++)
                {
                    DgvAttendance.Rows[IRow].Cells[iPunch + 3].Value = PunchingData[iPunch];
                }
                DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);
                DgvAttendance.Rows[IRow].Clone();

                return true;
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
                return false;
            }
        }

        private bool FormValidation()
        {
            if (CboCompany.SelectedIndex == -1)
            {
                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 3019, out  MmessageIcon);
                lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                tmrClearlabels.Enabled = true;
                return false;
            }
            string sCurrent = GetStratDate();
            string sNow = DateTime.Now.Date.ToString("dd MMM yyyy");
            if (MintDisplayMode == Convert.ToInt32(AttendanceMode.AttendanceManual))
            {
                if (Convert.ToDateTime(sCurrent) > Convert.ToDateTime(sNow))
                {
                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 3064, out  MmessageIcon);
                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    tmrClearlabels.Enabled = true;
                    return false;
                }
            }
            if (DgvAttendance.RowCount <= 0)
            {

                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1551, out  MmessageIcon);
                lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                tmrClearlabels.Enabled = true;
                return false;
            }

            if (MintDisplayMode == Convert.ToInt32(AttendanceMode.AttendanceManual) && rbtnMonthly.Checked)
            {
                if (MobjclsBLLAttendance.IsEmployeeInService(MintEmployeeID) == false)
                {
                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1561, out  MmessageIcon);
                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    tmrClearlabels.Enabled = true;
                    return false;
                }
            }

            if (CheckTimeInTimeOut() == false)
            {
                return false;
            }

            return true;
        }

        private bool CheckTimeInTimeOut()
        {

            DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);

            foreach (DataGridViewRow DgvRow in DgvAttendance.Rows)
            {

                for (int iCounter = ColDgvStatus.Index; iCounter <= MintTimingsColumnCnt; iCounter += 2)
                {
                    int intLoc = iCounter;
                    int intCnt = iCounter / 2;
                    string strCount = intCnt.ToString().Trim();
                    string strTimeInColName = "TimeIn" + strCount; ;
                    string strTimeInColOutName = "TimeOut" + strCount;
                    if (DgvAttendance.Columns.Contains(strTimeInColOutName))
                    {
                        if (DgvRow.Cells[strTimeInColOutName].Value.ToStringCustom() != string.Empty)
                        {
                            try
                            {
                                if (Glbln24HrFormat == false)
                                {
                                    DgvRow.Cells[strTimeInColOutName].Value = AMPMConvert(Microsoft.VisualBasic.Strings.UCase(DgvRow.Cells[strTimeInColOutName].Value.ToStringCustom()));
                                }
                                else
                                {
                                    DgvRow.Cells[strTimeInColOutName].Value = Convert24HrFormat(Microsoft.VisualBasic.Strings.UCase(DgvRow.Cells[strTimeInColOutName].Value.ToStringCustom()));
                                }
                            }
                            catch
                            {
                            }

                            if (IsValidTime(DgvRow.Cells[strTimeInColOutName].Value.ToString()) == false)
                            {
                                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1556, out  MmessageIcon);
                                if (Glbln24HrFormat == false)
                                {
                                    string strSecondMsg = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1557, out  MmessageIcon);
                                    MstrMessCommon = MstrMessCommon + strSecondMsg.Remove(0, strSecondMsg.IndexOf("#") + 1).Trim();
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    tmrClearlabels.Enabled = true;
                                }
                                else
                                {
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    tmrClearlabels.Enabled = true;
                                }

                                DgvAttendance.CurrentCell = DgvAttendance[strTimeInColOutName, DgvRow.Index];
                                return false;
                            }
                        }
                    }
                    if (DgvAttendance.Columns.Contains(strTimeInColName))
                    {
                        if (DgvRow.Cells[strTimeInColName].Value.ToStringCustom() != string.Empty)
                        {
                            try
                            {
                                if (Glbln24HrFormat == false)
                                {
                                    DgvRow.Cells[strTimeInColName].Value = AMPMConvert(Microsoft.VisualBasic.Strings.UCase(DgvRow.Cells[strTimeInColName].Value.ToStringCustom()));
                                }
                                else
                                {
                                    DgvRow.Cells[strTimeInColName].Value = Convert24HrFormat(Microsoft.VisualBasic.Strings.UCase(DgvRow.Cells[strTimeInColName].Value.ToStringCustom()));

                                }
                            }
                            catch
                            {
                            }
                            if (IsValidTime(DgvRow.Cells[strTimeInColName].Value.ToString()) == false)
                            {
                                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1556, out  MmessageIcon);
                                if (Glbln24HrFormat == false)
                                {
                                    string strSecondMsg = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1557, out  MmessageIcon);
                                    MstrMessCommon = MstrMessCommon + strSecondMsg.Remove(0, strSecondMsg.IndexOf("#") + 1).Trim();
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    tmrClearlabels.Enabled = true;
                                }
                                else
                                {
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    tmrClearlabels.Enabled = true;
                                }

                                DgvAttendance.CurrentCell = DgvAttendance[strTimeInColName, DgvRow.Index];
                                return false;
                            }

                        }
                    }
                }//For icounter
            }//For Each
            return true;
        }

        private bool SaveAttendanceInfoManual()
        {

            bool blnSaveFlag = false;
            try
            {
                if (DgvAttendance.RowCount > 0)
                {
                    barProgressBarAttendance.Visible = true;
                    barProgressBarAttendance.Minimum = 0;
                    barProgressBarAttendance.Maximum = DgvAttendance.Rows.Count + 1;
                    barProgressBarAttendance.Value = 0;

                    DataTable DtEmployee = new DataTable();// For Absent Marking
                    DtEmployee.Columns.Add("EmployeeID");
                    DtEmployee.Columns.Add("Date");
                    DtEmployee.Columns.Add("Month");
                    string[] sRows = new string[3];

                    for (int i = 0; i <= DgvAttendance.Rows.Count - 1; i++)
                    {
                        int iStatus = 0;
                        if (DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value != DBNull.Value && DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value != null && Microsoft.VisualBasic.Information.IsNumeric(DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value))
                            iStatus = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value);

                        if (iStatus == Convert.ToInt32(AttendanceStatus.Present) && Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvValidFlag"].Value) == 0)
                            continue;
                        else if (iStatus == Convert.ToInt32(AttendanceStatus.Present) && (DgvAttendance.Rows[i].Cells[ColDgvStatus.Index + 1].Value == DBNull.Value || DgvAttendance.Rows[i].Cells[ColDgvStatus.Index + 1].Value == null || Convert.ToString(DgvAttendance.Rows[i].Cells[ColDgvStatus.Index + 1].Value) == ""))
                            continue;
                        else if (iStatus == 0)
                        { continue; }
                        else if (iStatus == Convert.ToInt32(AttendanceStatus.Leave))
                        {
                            if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveType"].Value) <= 0)
                                continue;

                        }

                        if (DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag != null && DgvAttendance.Rows[i].Cells["ColDgvShift"].Tag != null)
                        {
                            int intEmpID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag);
                            int intCmpID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value);
                            DateTime dtDates = Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value);
                            string sJoiningDate = MobjclsBLLAttendance.GetJoiningDate(intEmpID);
                            DateTime dtJoiningDate = Convert.ToDateTime(sJoiningDate);
                            //'Cheking With Current Date
                            string sCurr = Strings.Format(dtDates.Date, "dd MMM yyyy");
                            string sNow = Strings.Format(DateTime.Now.Date, "dd MMM yyyy");
                            if (Convert.ToDateTime(sCurr) > Convert.ToDateTime(sNow))
                                continue;
                            if (dtDates.Date < dtJoiningDate.Date)
                                continue;
                            //MblnPayExists == true &&
                            if (MobjclsBLLAttendance.IsSalaryReleased(intEmpID, intCmpID, dtDates) == true)
                                continue;

                            FillParameterAtnMaster(i);
                            if (MobjclsBLLAttendance.SaveAttendanceInfo() == true)
                            {
                                blnSaveFlag = true;

                                sRows[0] = intEmpID.ToString();
                                sRows[1] = dtDates.ToString("dd MMM yyyy");
                                int iMonth = Convert.ToDateTime(dtDates).Month;
                                string smonthname = Microsoft.VisualBasic.DateAndTime.MonthName(iMonth, true);
                                string smonth = smonthname + " " + dtDates.Year.ToString();
                                sRows[2] = smonth;
                                DtEmployee.Rows.Add(sRows);
                            }
                        }

                        if (blnSaveFlag == true)
                        {
                            barProgressBarAttendance.Visible = true;
                            barProgressBarAttendance.Value += 1;
                            barProgressBarAttendance.Refresh();
                            barStatusBottom.Refresh();
                        }
                        else
                        {
                            barProgressBarAttendance.Visible = false;
                        }


                    }

                    if (blnSaveFlag == true)
                        barProgressBarAttendance.Value = DgvAttendance.Rows.Count + 1;
                    else
                        barProgressBarAttendance.Visible = false;

                    if (DtEmployee.Rows.Count > 0)
                    {
                        Absentmarking(DtEmployee);//Absent Marking

                    }
                    barProgressBarAttendance.Visible = false;
                    barProgressBarAttendance.Value = 0;

                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            return blnSaveFlag;

        }

        private bool DeleteAttendenceDetails()
        {
            MblDeleteMeassage = false;

            bool blnReturn = false;
            try
            {
                int iEmpID = 0;
                int iCmpID = 0;

                for (int i = 0; i <= DgvAttendance.Rows.Count - 1; i++)
                {
                    bool blnIsValid = false;
                    if (ChkLate.Checked == false && ChkEarly.Checked == false && ChkExtendedBreak.Checked == false && ChkConsequence.Checked == false && ChkAbsent.Checked == false && ChkLeave.Checked == false && ChkOverTime.Checked == false && ChkShortage.Checked == false)
                    {
                        if (DgvAttendance[ColDgvStatus.Index, i].Value != null && DgvAttendance[ColDgvStatus.Index, i].Value != DBNull.Value && DgvAttendance[ColDgvStatus.Index, i].Value.ToInt32() > 0)
                            blnIsValid = true;
                    }
                    else
                    {
                        if (DgvAttendance[ColDgvStatus.Index, i].Value != null && DgvAttendance[ColDgvStatus.Index, i].Value != DBNull.Value && DgvAttendance[ColDgvStatus.Index, i].Value.ToInt32() > 0)
                            blnIsValid = true;
                    }
                    if (blnIsValid)
                    {
                        MblDeleteMeassage = true;

                        DateTime dtDate = Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value);
                        iEmpID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag);
                        iCmpID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value);

                        //if (MobjclsBLLAttendance.IsSalaryReleased(iEmpID, iCmpID, dtDate) == false)
                        //{
                        //    MobjclsBLLAttendance.DeleteLeaveEntry(iEmpID, iCmpID, dtDate);
                        //    int ileave = 0;
                        //    if (Int32.TryParse(Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLeaveId"].Value), out ileave))
                        //    {
                        //        ileave = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveId"].Value);
                        //    }

                        //    if (ileave > 0)
                        //    {
                        //        MobjclsBLLAttendance.DeleteLeaveFromAttendance(iEmpID, iCmpID, dtDate, Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveId"].Value));
                        //    }
                        //    if (MobjclsBLLAttendance.DeleteAttendance(iEmpID, iCmpID, dtDate))
                        //    {
                        //        blnReturn = true;
                        //    }

                        //}
                        //else
                        //{
                        //    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1573, out  MmessageIcon).Replace("period", "date").Replace("#", "").Trim();
                        //    // lblAttendnaceStatus.Text = "Cannot update/delete employee attendance. Salary released for this Date " + dtDate + "";
                        //    lblAttendnaceStatus.Text = MstrMessCommon + " " + dtDate + "";
                        //    tmrClearlabels.Enabled = true;
                        //}
                        int ileave = 0;
                        if (Int32.TryParse(Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLeaveId"].Value), out ileave))
                        {
                            ileave = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveId"].Value);
                        }
                        if (MobjclsBLLAttendance.DeleteAttendanceWithLeave(iEmpID, iCmpID, dtDate,ileave))
                        {
                                blnReturn = true;
                        }
                        else
                        {
                            MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1573, out  MmessageIcon).Replace("period", "date").Trim();
                            // lblAttendnaceStatus.Text = "Cannot update/delete employee attendance. Salary released for this Date " + dtDate + "";
                            lblAttendnaceStatus.Text =MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim() +" " + dtDate + "";
                            tmrClearlabels.Enabled = true;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                blnReturn = false;
            }
            return blnReturn;
        }

        private void GetSearchCondition(ref string strSearchConditionEmployee, ref string strSearchConditionAttendance)
        {
            strSearchConditionEmployee = "";
            strSearchConditionAttendance = "";
            if (Convert.ToInt32(CboCompany.SelectedValue) > 0)
            {
                strSearchConditionEmployee = "EM.CompanyID=" + Convert.ToInt32(CboCompany.SelectedValue) + " AND";
            }

            if (cboEmpWorkstatus.Text.ToUpper() != "ALL")
            {
                if (cboEmpWorkstatus.SelectedValue.ToInt32() <= 0)
                    strSearchConditionEmployee = strSearchConditionEmployee + " EM.WorkStatusID >=6 AND";
                else
                    strSearchConditionEmployee = strSearchConditionEmployee + " EM.WorkStatusID= " + Convert.ToInt32(cboEmpWorkstatus.SelectedValue) + " AND";
            }

            if (CboEmpFilter.Text.ToUpper() == "ALL")
            { }
            else if (CboEmpFilter.Text.ToUpper() == "DEPARTMENT")
            {
                if (CboEmpFilterType.Text.ToUpper() != "ALL")
                {
                    strSearchConditionEmployee = strSearchConditionEmployee + " EM.DepartmentID =" + CboEmpFilterType.SelectedValue + " AND";
                }
            }
            else if (CboEmpFilter.Text.ToUpper() == "DESIGNATION")
            {
                if (CboEmpFilterType.Text.ToUpper() != "ALL")
                {
                    strSearchConditionEmployee = strSearchConditionEmployee + " EM.DesignationID =" + CboEmpFilterType.SelectedItem + " AND";
                }
            }
            else if (CboEmpFilter.Text.ToUpper() == "EMPLOYEMENT TYPE")
            {
                if (CboEmpFilterType.Text.ToUpper() != "ALL")
                {
                    strSearchConditionEmployee = strSearchConditionEmployee + " EM.EmploymentType =" + CboEmpFilterType.SelectedItem + " AND";
                }
            }

            if (strSearchConditionEmployee.Length > 3)
            {
                strSearchConditionEmployee = strSearchConditionEmployee.Substring(0, strSearchConditionEmployee.Length - 4);
            }
            if (ChkLate.Checked)
            {
                strSearchConditionAttendance = " replace(isnull(ASM.Late,'0'),':','')>0 AND";
            }

            if (ChkEarly.Checked)
            {
                strSearchConditionAttendance = strSearchConditionAttendance + " replace(isnull(ASM.Early,'0'),':','')>0 AND";
            }
            if (ChkExtendedBreak.Checked)
            {
                strSearchConditionAttendance = strSearchConditionAttendance + " (SELECT dbo.fnPayGetMin(cast((Select " +
                                               " cast(LEFT(rtrim(ltrim(Replace(convert(char(8),cast(CASE WHEN (ASM.BreakTime = ' ' OR ASM.BreakTime = '' OR isnull(ASM.BreakTime, '00:00:00') = '00:00:00') " +
                                               " THEN '' ELSE ASM.BreakTime END as datetime),8),':','.'))),5) as Decimal(18,2)) )as Decimal(12,2)) ))> (CASE SR.ShiftTypeID When 3 Then SD.AllowedBreakTime Else SR.AllowedBreakTime  End) AND SR.ShiftTypeID<>2  AND";//SR.AllowedBreaktime  AND
            }

            if (ChkOverTime.Checked)
            {
                strSearchConditionAttendance = strSearchConditionAttendance + " replace(isnull(ASM.OT,'0'),':','')>0 AND";
            }
            if (ChkShortage.Checked)
            {
                strSearchConditionAttendance = strSearchConditionAttendance + " isnull(ASM.AbsentTime,0)>0 AND isnull(ASM.AttendenceStatusID,0) <> " + Convert.ToInt32(AttendanceStatus.Absent) + " AND";
            }
            if (ChkLeave.Checked)
            {
                strSearchConditionAttendance = strSearchConditionAttendance + " isnull(ASM.AttendenceStatusID,0)=" + Convert.ToInt32(AttendanceStatus.Leave) + " AND";
            }
            if (ChkAbsent.Checked)
            {
                strSearchConditionAttendance = strSearchConditionAttendance + " isnull(ASM.AttendenceStatusID,0)=" + Convert.ToInt32(AttendanceStatus.Absent) + " AND";
            }
            if (ChkConsequence.Checked)
            {
                strSearchConditionAttendance = strSearchConditionAttendance + " isnull(ASM.ConsequenceID,0)>0 AND";
            }
            if (strSearchConditionAttendance.Length > 3)
            {
                strSearchConditionAttendance = strSearchConditionAttendance.Substring(0, strSearchConditionAttendance.Length - 4);
            }
        }

        private bool SearchData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearBottomPanel();
                string sAutoEmpIds = "";
                string strInOut = "";
                string strInOut1 = "";
                string strSearchConditionEmployee = "";
                string strSearchConditionAttendance = "";

                InitializeGrid();
                MblnShowData = true;
                int intCmpID = Convert.ToInt32(CboCompany.SelectedValue);
                int intEmpID = MintEmployeeID;
                DateTime dtDate = dtpFromDate.Value;
                bool blnMonthly = rbtnMonthly.Checked ? true : false;
                bool blnBreakExceed = ChkExtendedBreak.Checked ? true : false;
                GetSearchCondition(ref strSearchConditionEmployee, ref strSearchConditionAttendance);

                int intMaxCount = MobjclsBLLAttendance.GetMaxColumnCount(intEmpID, intCmpID, dtDate, blnMonthly, blnBreakExceed, strSearchConditionEmployee, strSearchConditionAttendance);
                if (intMaxCount > 0)
                {
                    MintTimingsColumnCnt = intMaxCount;
                    if (intMaxCount % 2 == 0)
                    {
                        intMaxCount = intMaxCount / 2;
                    }
                    else
                    {
                        intMaxCount = intMaxCount + 1;
                        intMaxCount = (intMaxCount / 2);
                    }

                    strInOut = CountQuery2(intMaxCount);
                    strInOut1 = CountQuery(intMaxCount);
                }
                DataTable DtGetData = new DataTable();
                DtGetData = MobjclsBLLAttendance.GetAttendance(strInOut1, strInOut, intEmpID, intCmpID, dtDate, blnMonthly, strSearchConditionEmployee, strSearchConditionAttendance);//Getting attendance
                DgvAttendance.RowCount = 0;

                if (DtGetData.Rows.Count > 0)
                {

                    int i = 0;
                    int cnt = 0;
                    int iLoc = 0;
                    int itimecnt = 2;
                    for (i = 4; i <= MintTimingsColumnCnt + 1; i = i + 2)
                    {
                        iLoc = i;

                        cnt = itimecnt;
                        DataGridViewTextBoxColumn adcolIn = new DataGridViewTextBoxColumn();
                        DataGridViewTextBoxColumn adcolOut = new DataGridViewTextBoxColumn();

                        adcolIn.Name = "TimeIn" + cnt;
                        adcolIn.HeaderText = "TimeIn" + cnt;
                        adcolIn.SortMode = DataGridViewColumnSortMode.NotSortable;
                        adcolIn.Resizable = DataGridViewTriState.False;
                        adcolIn.Width = 70;
                        adcolIn.MaxInputLength = 9;

                        adcolOut.Name = "TimeOut" + cnt;
                        adcolOut.HeaderText = "TimeOut" + cnt;
                        adcolOut.SortMode = DataGridViewColumnSortMode.NotSortable;
                        adcolOut.Resizable = DataGridViewTriState.False;
                        adcolOut.Width = 70;
                        adcolOut.MaxInputLength = 9;

                        DgvAttendance.Columns.Insert(iLoc + 1, adcolIn);
                        DgvAttendance.Columns.Insert(iLoc + 2, adcolOut);
                        itimecnt += 1;
                    }
                    DgvAttendance.Columns["ColDgvAtnDate"].Width = 75;
                    DgvAttendance.Columns["ColDgvAddMore"].Width = 35;
                    DgvAttendance.Columns["ColDgvAddMore"].MinimumWidth = 35;

                    DgvAttendance.RowCount = 0;

                    if (rbtnDaily.Checked)
                    {
                        for (int iRow = 0; iRow <= DtGetData.Rows.Count - 1; iRow++)
                        {

                            DgvAttendance.RowCount += 1;
                            DgvAttendance.Rows[iRow].Cells["ColDgvAtnDate"].Value = DtGetData.Rows[iRow]["Date"];
                            DgvAttendance.Rows[iRow].Cells["ColDgvDay"].Value = Convert.ToDateTime(DtGetData.Rows[iRow]["Date"]).DayOfWeek.ToString();
                            DgvAttendance.Rows[iRow].Cells["ColDgvEmployee"].Tag = DtGetData.Rows[iRow]["EmployeeID"];
                            DgvAttendance.Rows[iRow].Cells["ColDgvEmployee"].Value = DtGetData.Rows[iRow]["Employee"];
                            DgvAttendance.Rows[iRow].Cells["ColDgvShift"].Tag = DtGetData.Rows[iRow]["ShiftID"];
                            DgvAttendance.Rows[iRow].Cells["ColDgvShift"].Value = DtGetData.Rows[iRow]["Shift"];
                            DgvAttendance.Rows[iRow].Cells["ColDgvLeaveId"].Value = Convert.ToInt32(DtGetData.Rows[iRow]["LeaveID"]);
                            DgvAttendance.Rows[iRow].Cells["ColDgvCompanyID"].Value = Convert.ToInt32(DtGetData.Rows[iRow]["CompanyID"]);
                            DgvAttendance.Rows[iRow].Cells["ColDgvProjectID"].Value = Convert.ToInt32(DtGetData.Rows[iRow]["ProjectID"]);

                            int iType = 0;
                            if (CheckIfValidDate(iRow, ref iType) == false)
                            {
                                if (iType == 3 || iType == 2 || iType == 4)// 'off day and off day without shift and Holiday
                                {
                                    DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Red;
                                    DgvAttendance.Rows[iRow].Cells["ColDgvHolidayFlag"].Value = 1;
                                    DgvAttendance.Rows[iRow].Cells["ColDgvHolidayFlag"].Tag = 1;
                                    DgvAttendance.Rows[iRow].Cells["ColDgvLeaveFlag"].Tag = 0;
                                    DgvAttendance.Rows[iRow].Cells["ColDgvLeaveFlag"].Value = 0;
                                }
                                else if (iType == 1)//Then 'leave
                                {
                                    DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Red;
                                    DgvAttendance.Rows[iRow].Cells["ColDgvLeaveFlag"].Tag = 1;
                                    DgvAttendance.Rows[iRow].Cells["ColDgvLeaveFlag"].Value = 1;
                                    DgvAttendance.Rows[iRow].Cells["ColDgvHolidayFlag"].Value = 0;
                                    DgvAttendance.Rows[iRow].Cells["ColDgvHolidayFlag"].Tag = 0;
                                }

                            }

                            if (DgvAttendance.Rows[iRow].Cells["ColDgvLeaveFlag"].Value.ToInt32() == 0)
                            {
                                if (MobjclsBLLAttendance.CheckHalfdayLeave(DgvAttendance.Rows[iRow].Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.Rows[iRow].Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.Rows[iRow].Cells["ColDgvAtnDate"].Value.ToDateTime()))
                                {
                                    DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Red;
                                    DgvAttendance.Rows[iRow].Cells["ColDgvLeaveFlag"].Value = 1;
                                }
                                else
                                {
                                    DgvAttendance.Rows[iRow].Cells["ColDgvLeaveFlag"].Value = 0;
                                }
                            }

                            DgvAttendance.Rows[iRow].Cells["ColDgvAbsentTime"].Value = DtGetData.Rows[iRow]["AbsentTime"].ToInt32();
                            DgvAttendance.Rows[iRow].Cells["ColDgvAllowedBreakTime"].Tag = Convert.ToDouble(DtGetData.Rows[iRow]["AllowedBreakTime"]);
                            string sAllowedBreakTime = Convert.ToString(DtGetData.Rows[iRow]["AllowedBreakTime"]);
                            sAllowedBreakTime = Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAllowedBreakTime), DateTime.Today).ToString();
                            sAllowedBreakTime = Convert.ToDateTime(sAllowedBreakTime).ToString("HH:mm:ss");
                            DgvAttendance.Rows[iRow].Cells["ColDgvAllowedBreakTime"].Value = sAllowedBreakTime;

                            DgvAttendance.Rows[iRow].Cells["ColDgvLeaveType"].Value = DtGetData.Rows[iRow]["LeaveTypeID"];
                            DgvAttendance.Rows[iRow].Cells["ColDgvLeaveInfo"].Value = DtGetData.Rows[iRow]["halfdayLeave"];
                            DgvAttendance.Rows[iRow].Cells["ColDgvLeaveInfo"].Tag = DtGetData.Rows[iRow]["PaidLeave"];
                            DgvAttendance.Rows[iRow].Cells["ColDgvRemarks"].Value = DtGetData.Rows[iRow]["Remarks"];

                            if (DgvAttendance.Rows[iRow].Cells["ColDgvLeaveId"].Value.ToInt32() <= 0)
                            {
                                int iLeaveTypeID = 0;
                                if (MobjclsBLLAttendance.CheckHalfdayLeaveNew(DgvAttendance.Rows[iRow].Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.Rows[iRow].Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.Rows[iRow].Cells["ColDgvAtnDate"].Value.ToDateTime()) > 0)
                                {
                                    DgvAttendance.Rows[iRow].Cells["ColDgvLeaveType"].Tag = iLeaveTypeID;
                                    DgvAttendance.Rows[iRow].Cells["ColDgvLeaveType"].Value = iLeaveTypeID;
                                    DgvAttendance.Rows[iRow].Cells["ColDgvLeaveInfo"].Value = 1;
                                }
                            }

                            int iDurationMinute = 0;
                            DateTime dtdate1;
                            DateTime dtdate2;
                            DgvAttendance.Rows[iRow].Cells["ColDgvBreakTime"].Value = DtGetData.Rows[iRow]["BreakTime"];
                            DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 1;
                            if (Convert.ToString(DtGetData.Rows[iRow]["BreakTime"]) != "")
                            {
                                dtdate1 = Convert.ToDateTime(DtGetData.Rows[iRow]["BreakTime"]);
                                dtdate2 = Convert.ToDateTime("00:00:00");
                                iDurationMinute = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(dtdate2), Convert.ToDateTime(dtdate1));
                            }
                            DgvAttendance.Rows[iRow].Cells["ColdgvBreakTime"].Tag = iDurationMinute;

                            DgvAttendance.Rows[iRow].Cells["ColDgvWorkTime"].Value = DtGetData.Rows[iRow]["WorkTime"];
                            iDurationMinute = 0;
                            if (Convert.ToString(DtGetData.Rows[iRow]["WorkTime"]) != "")
                            {
                                dtdate1 = Convert.ToDateTime(DtGetData.Rows[iRow]["WorkTime"]);
                                dtdate2 = Convert.ToDateTime("00:00:00");
                                iDurationMinute = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(dtdate2), Convert.ToDateTime(dtdate1));
                            }
                            DgvAttendance.Rows[iRow].Cells["ColDgvWorkTime"].Tag = iDurationMinute;

                            DgvAttendance.Rows[iRow].Cells["ColDgvExcessTime"].Value = DtGetData.Rows[iRow]["OT"];
                            iDurationMinute = 0;
                            if (Convert.ToString(DtGetData.Rows[iRow]["OT"]) != "")
                            {
                                dtdate1 = Convert.ToDateTime(DtGetData.Rows[iRow]["OT"]);
                                dtdate2 = Convert.ToDateTime("00:00:00");
                                iDurationMinute = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(dtdate2), Convert.ToDateTime(dtdate1));
                            }
                            DgvAttendance.Rows[iRow].Cells["ColDgvExcessTime"].Tag = iDurationMinute;


                            DgvAttendance.Rows[iRow].Cells["ColDgvLateComing"].Value = DtGetData.Rows[iRow]["Late"];
                            DgvAttendance.Rows[iRow].Cells["ColDgvEarlyGoing"].Value = DtGetData.Rows[iRow]["Early"];
                            DgvAttendance.Rows[iRow].Cells["ColDgvAtnDate"].Value = DtGetData.Rows[iRow]["Date"];
                            sAutoEmpIds = sAutoEmpIds + Convert.ToString(DtGetData.Rows[iRow]["EmployeeID"]) + ",";
                            DgvAttendance.Rows[iRow].Cells["ColDgvStatus"].Value = DtGetData.Rows[iRow]["AttendenceStatusID"];
                            int iStatus = Convert.ToInt32(DtGetData.Rows[iRow]["AttendenceStatusID"]);
                            if (iStatus == Convert.ToInt32(AttendanceStatus.Absent) || iStatus == Convert.ToInt32(AttendanceStatus.Rest) || iStatus == 0)
                            { continue; }
                            if (iStatus == Convert.ToInt32(AttendanceStatus.Leave))
                            {
                                if (Convert.ToString(DgvAttendance.Rows[iRow].Cells["ColDgvLeaveInfo"].Value) == "0")
                                { continue; }

                            }

                            string StrDate;
                            for (int j = 1; j <= Math.Floor(Convert.ToDouble(MintTimingsColumnCnt + 1) / 2); j++)
                            {
                                if (Glbln24HrFormat == false)
                                {
                                    if (Convert.ToBoolean(DtGetData.Columns.Contains("TimeIn" + j) == true))
                                    {
                                        if (Convert.ToString(DtGetData.Rows[iRow]["TimeIn" + j]) != "")
                                        {
                                            StrDate = ConvertDate12(Convert.ToDateTime(DtGetData.Rows[iRow]["TimeIn" + j])).ToString();
                                            if (Microsoft.VisualBasic.Strings.Right(DTPFormatTime.Text.Trim(), 2) == "AM")
                                            {
                                                DgvAttendance.Rows[iRow].Cells["TimeIn" + j].Value = DTPFormatTime.Text.Replace(":AM", " AM");
                                            }
                                            else
                                            {
                                                DgvAttendance.Rows[iRow].Cells["TimeIn" + j].Value = DTPFormatTime.Text.Replace(":PM", " PM");

                                            }
                                        }
                                        if (Convert.ToString(DtGetData.Rows[iRow]["TimeOut" + j]) != "")
                                        {
                                            StrDate = ConvertDate12(Convert.ToDateTime(DtGetData.Rows[iRow]["TimeOut" + j])).ToString();
                                            if (Microsoft.VisualBasic.Strings.Right(DTPFormatTime.Text.Trim(), 2) == "AM")
                                            {
                                                DgvAttendance.Rows[iRow].Cells["TimeOut" + j].Value = DTPFormatTime.Text.Replace(":AM", " AM");
                                            }
                                            else
                                            {
                                                DgvAttendance.Rows[iRow].Cells["TimeOut" + j].Value = DTPFormatTime.Text.Replace(":PM", " PM");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (Convert.ToString(DtGetData.Rows[iRow]["TimeIn" + j]) != "")
                                    {
                                        StrDate = ConvertDate24(Convert.ToDateTime(DtGetData.Rows[iRow]["TimeIn" + j])).ToString();
                                        DgvAttendance.Rows[iRow].Cells["TimeIn" + j].Value = DTPFormatTime.Text;
                                    }
                                    if (Convert.ToString(DtGetData.Rows[iRow]["TimeOut" + j]) != "")
                                    {
                                        StrDate = ConvertDate24(Convert.ToDateTime(DtGetData.Rows[iRow]["TimeOut" + j])).ToString();
                                        DgvAttendance.Rows[iRow].Cells["TimeOut" + j].Value = DTPFormatTime.Text;
                                    }
                                }//if glb24format
                            } // For j
                            WorkTimeCalculation(iRow, false);
                        }//For Irow
                    }//Monthly
                    else if (rbtnMonthly.Checked)
                    {

                        if (MintEmployeeID == 0)
                            return false;

                        FillDateToGrid(MintEmployeeID);

                        bool blnSalaryprocessed = false;
                        int iCmbID = CboCompany.SelectedValue.ToInt32();

                        if (MobjclsBLLAttendance.IsSalaryReleased(MintEmployeeID, iCmbID, dtpFromDate.Value.Date))
                            blnSalaryprocessed = true;

                        for (int iRw = 0; iRw <= DgvAttendance.Rows.Count - 1; iRw++)
                        {
                            DgvAttendance.Rows[iRw].Cells["ColDgvEmployee"].Tag = DtGetData.Rows[0]["EmployeeID"];
                            DgvAttendance.Rows[iRw].Cells["ColDgvEmployee"].Value = DtGetData.Rows[0]["Employee"];

                            string rowDate = DgvAttendance.Rows[iRw].Cells["ColDgvAtnDate"].Value.ToString();

                            for (i = 0; i <= DtGetData.Rows.Count - 1; i++)
                            {
                                string dtdate = Convert.ToDateTime(DtGetData.Rows[i]["Date"]).ToString("dd MMM yyyy");

                                if (Convert.ToDateTime(dtdate) == Convert.ToDateTime(rowDate))
                                {
                                    DgvAttendance.Rows[iRw].Cells["ColDgvShift"].Tag = 0;
                                    DgvAttendance.Rows[iRw].Cells["ColDgvShift"].Value = "";
                                    DgvAttendance.Rows[iRw].Cells["ColDgvShift"].Tag = DtGetData.Rows[i]["ShiftID"];
                                    DgvAttendance.Rows[iRw].Cells["ColDgvShift"].Value = DtGetData.Rows[i]["Shift"];
                                    DgvAttendance.Rows[iRw].Cells["ColDgvStatus"].Value = DtGetData.Rows[i]["AttendenceStatusID"];
                                    DgvAttendance.Rows[iRw].Cells["ColDgvAbsentTime"].Value = DtGetData.Rows[i]["AbsentTime"];
                                    DgvAttendance.Rows[iRw].Cells["ColDgvLeaveId"].Value = Convert.ToInt32(DtGetData.Rows[i]["LeaveID"]);
                                    DgvAttendance.Rows[iRw].Cells["ColDgvCompanyID"].Value = Convert.ToInt32(DtGetData.Rows[i]["CompanyID"]);
                                    DgvAttendance.Rows[iRw].Cells["ColDgvRemarks"].Value = DtGetData.Rows[i]["Remarks"];
                                    DgvAttendance.Rows[iRw].Cells["ColDgvProjectID"].Value = Convert.ToInt32(DtGetData.Rows[i]["ProjectID"]);
                                    DgvAttendance.Rows[iRw].Cells["ColDgvAllowedBreakTime"].Tag = Convert.ToDouble(DtGetData.Rows[i]["AllowedBreakTime"]);
                                    string sAllowedBreakTime = Convert.ToString(DtGetData.Rows[i]["AllowedBreakTime"]);
                                    sAllowedBreakTime = Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAllowedBreakTime), DateTime.Today).ToString();
                                    sAllowedBreakTime = Convert.ToDateTime(sAllowedBreakTime).ToString("HH:mm:ss");
                                    DgvAttendance.Rows[iRw].Cells["ColDgvAllowedBreakTime"].Value = sAllowedBreakTime;
                                    DgvAttendance.Rows[iRw].Cells["ColDgvValidFlag"].Value = 1;
                                    DgvAttendance.Rows[iRw].Cells["ColDgvLeaveType"].Value = DtGetData.Rows[i]["LeaveTypeID"];
                                    DgvAttendance.Rows[iRw].Cells["ColDgvLeaveInfo"].Value = DtGetData.Rows[i]["halfdayLeave"];
                                    DgvAttendance.Rows[iRw].Cells["ColDgvLeaveInfo"].Tag = DtGetData.Rows[i]["PaidLeave"];
                                    if (DgvAttendance.Rows[iRw].Cells["ColDgvLeaveId"].Value.ToInt32() <= 0)
                                    {
                                        int iLeaveTypeID = 0;
                                        if (MobjclsBLLAttendance.CheckHalfdayLeaveNew(DgvAttendance.Rows[iRw].Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.Rows[iRw].Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.Rows[iRw].Cells["ColDgvAtnDate"].Value.ToDateTime()) > 0)
                                        {
                                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveType"].Tag = iLeaveTypeID;
                                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveType"].Value = iLeaveTypeID;
                                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveInfo"].Value = 1;

                                        }
                                    }

                                    int iDurationMinute = 0;
                                    DateTime dtdate1;
                                    DateTime dtdate2;
                                    DgvAttendance.Rows[iRw].Cells["ColDgvBreakTime"].Value = DtGetData.Rows[i]["BreakTime"];

                                    if (Convert.ToString(DtGetData.Rows[i]["BreakTime"]) != "")
                                    {
                                        dtdate1 = Convert.ToDateTime(DtGetData.Rows[i]["BreakTime"]);
                                        dtdate2 = Convert.ToDateTime("00:00:00");
                                        iDurationMinute = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(dtdate2), Convert.ToDateTime(dtdate1));
                                    }
                                    DgvAttendance.Rows[iRw].Cells["ColDgvBreakTime"].Tag = iDurationMinute;

                                    DgvAttendance.Rows[iRw].Cells["ColDgvWorkTime"].Value = DtGetData.Rows[i]["WorkTime"];
                                    iDurationMinute = 0;
                                    if (Convert.ToString(DtGetData.Rows[i]["WorkTime"]) != "")
                                    {
                                        dtdate1 = Convert.ToDateTime(DtGetData.Rows[i]["WorkTime"]);
                                        dtdate2 = Convert.ToDateTime("00:00:00");
                                        iDurationMinute = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(dtdate2), Convert.ToDateTime(dtdate1));
                                    }

                                    DgvAttendance.Rows[iRw].Cells["ColDgvWorkTime"].Tag = iDurationMinute;

                                    DgvAttendance.Rows[iRw].Cells["ColDgvExcessTime"].Value = DtGetData.Rows[i]["OT"];
                                    iDurationMinute = 0;
                                    if (Convert.ToString(DtGetData.Rows[i]["OT"]) != "")
                                    {
                                        dtdate1 = Convert.ToDateTime(DtGetData.Rows[i]["OT"]);
                                        dtdate2 = Convert.ToDateTime("00:00:00");
                                        iDurationMinute = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(dtdate2), Convert.ToDateTime(dtdate1));
                                    }
                                    DgvAttendance.Rows[iRw].Cells["ColDgvExcessTime"].Tag = iDurationMinute;

                                    //' DgvAttendance.Rows[iRw].Cells("FoodBreakTime").Value = dt.Rows(i).Item("FoodBreakTime")
                                    DgvAttendance.Rows[iRw].Cells["ColDgvLateComing"].Value = DtGetData.Rows[i]["Late"];
                                    DgvAttendance.Rows[iRw].Cells["ColDgvLateComing"].Value = DtGetData.Rows[i]["Early"];
                                    DgvAttendance.Rows[iRw].Cells["ColDgvAtnDate"].Value = DtGetData.Rows[i]["Date"];
                                    sAutoEmpIds = sAutoEmpIds + Convert.ToString(DtGetData.Rows[i]["EmployeeID"]) + ",";
                                    int iStatus = Convert.ToInt32(DtGetData.Rows[i]["AttendenceStatusID"]);
                                    if (iStatus == Convert.ToInt32(AttendanceStatus.Absent) || iStatus == Convert.ToInt32(AttendanceStatus.Rest) || iStatus == 0)
                                    {
                                        continue;
                                    }

                                    if (iStatus == Convert.ToInt32(AttendanceStatus.Leave))
                                    {
                                        if (Convert.ToString(DgvAttendance.Rows[iRw].Cells["ColDgvLeaveInfo"].Value) == "0")
                                        {
                                            continue;
                                        }
                                        else
                                        {
                                            DgvAttendance.Rows[iRw].DefaultCellStyle.ForeColor = Color.Black;
                                        }

                                    }

                                    string StrDate = "";
                                    for (int j = 1; j <= Math.Floor(Convert.ToDouble(MintTimingsColumnCnt + 1) / 2); j++)
                                    {
                                        if (Glbln24HrFormat == false)
                                        {
                                            if (DtGetData.Columns.Contains("TimeIn" + j.ToString()))
                                            {
                                                if (Convert.ToString(DtGetData.Rows[i]["TimeIn" + j]) != "")
                                                {
                                                    StrDate = ConvertDate12(Convert.ToDateTime(DtGetData.Rows[i]["TimeIn" + j])).ToString();
                                                    if (Microsoft.VisualBasic.Strings.Right(DTPFormatTime.Text.Trim(), 2) == "AM")
                                                    {
                                                        DgvAttendance.Rows[iRw].Cells["TimeIn" + j].Value = DTPFormatTime.Text.Replace(":AM", " AM");
                                                    }
                                                    else
                                                    {
                                                        DgvAttendance.Rows[iRw].Cells["TimeIn" + j].Value = DTPFormatTime.Text.Replace(":PM", " PM");

                                                    }
                                                }
                                            }
                                            if (DtGetData.Columns.Contains("TimeOut" + j.ToString()))
                                            {
                                                if (Convert.ToString(DtGetData.Rows[i]["TimeOut" + j]) != "")
                                                {
                                                    StrDate = ConvertDate12(Convert.ToDateTime(DtGetData.Rows[i]["TimeOut" + j])).ToString();
                                                    if (Microsoft.VisualBasic.Strings.Right(DTPFormatTime.Text.Trim(), 2) == "AM")
                                                    {
                                                        DgvAttendance.Rows[iRw].Cells["TimeOut" + j].Value = DTPFormatTime.Text.Replace(":AM", " AM");
                                                    }
                                                    else
                                                    {
                                                        DgvAttendance.Rows[iRw].Cells["TimeOut" + j].Value = DTPFormatTime.Text.Replace(":PM", " PM");
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (DtGetData.Columns.Contains("TimeIn" + j.ToString()))
                                            {

                                                if (Convert.ToString(DtGetData.Rows[i]["TimeIn" + j]) != "")
                                                {
                                                    StrDate = ConvertDate24(Convert.ToDateTime(DtGetData.Rows[i]["TimeIn" + j])).ToString();
                                                    DgvAttendance.Rows[iRw].Cells["TimeIn" + j].Value = DTPFormatTime.Text;
                                                }
                                            }
                                            if (DtGetData.Columns.Contains("TimeOut" + j.ToString()))
                                            {
                                                if (Convert.ToString(DtGetData.Rows[i]["TimeOut" + j]) != "")
                                                {
                                                    StrDate = ConvertDate24(Convert.ToDateTime(DtGetData.Rows[i]["TimeOut" + j])).ToString();
                                                    DgvAttendance.Rows[iRw].Cells["TimeOut" + j].Value = DTPFormatTime.Text;
                                                }
                                            }
                                        }//if glb24format ''''''''
                                    }
                                    if (blnSalaryprocessed == false)
                                        WorkTimeCalculation(iRw, false);

                                }//if date

                            }//For i

                        }//For iRow
                    }
                }//dtgetdata.rowCount >0

                if (rbtnDaily.Checked)
                {
                    if (sAutoEmpIds != "")
                    {
                        if (ChkLate.Checked == false && ChkEarly.Checked == false && ChkExtendedBreak.Checked == false && ChkConsequence.Checked == false && ChkAbsent.Checked == false && ChkLeave.Checked == false && ChkOverTime.Checked == false && ChkShortage.Checked == false)
                        {
                            FillEmployesToGrid(sAutoEmpIds, false);
                        }
                    }
                    else
                    {
                        if (ChkLate.Checked == false && ChkEarly.Checked == false && ChkExtendedBreak.Checked == false && ChkConsequence.Checked == false && ChkAbsent.Checked == false && ChkLeave.Checked == false && ChkOverTime.Checked == false && ChkShortage.Checked == false)
                        {

                            FillEmployesToGrid(sAutoEmpIds, true);
                        }
                    }
                }
                else
                {
                    if (DtGetData == null)
                    {
                        FillDateToGrid(MintEmployeeID);
                    }
                    else
                    {
                        if (DtGetData.Rows.Count <= 0)
                        {
                            FillDateToGrid(MintEmployeeID);
                        }
                        if (DgvAttendance.RowCount == 0)
                        {
                            DgvAttendance.RowCount += 1;
                        }
                    }

                }
                for (int iRw = 0; iRw <= DgvAttendance.Rows.Count - 1; iRw++)
                {
                    if (DgvAttendance.Rows[iRw].Cells["ColDgvHolidayFlag"].Value.ToInt32() != 1)
                    {
                        DataTable datLeaveRemarks = MobjclsBLLAttendance.GetLeaveRemarks(DgvAttendance.Rows[iRw].Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.Rows[iRw].Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.Rows[iRw].Cells["ColDgvAtnDate"].Value.ToDateTime());
                        if (datLeaveRemarks.Rows.Count > 0)
                        {
                            DgvAttendance.Rows[iRw].Cells["ColDgvRemarks"].Value = datLeaveRemarks.Rows[0]["Remarks"].ToStringCustom();
                            DgvAttendance.Rows[iRw].Cells["ColDgvStatus"].Value = (int)AttendanceStatus.Leave;
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveType"].Value = datLeaveRemarks.Rows[0]["LeaveTypeID"].ToInt32();
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveInfo"].Value = datLeaveRemarks.Rows[0]["HalfDay"].ToInt32();
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveType"].Tag = datLeaveRemarks.Rows[0]["LeaveTypeID"].ToInt32();
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveFlag"].Tag = 1;
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveFlag"].Value = 1;
                        }
                    }
                }
                DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);
                Cursor.Current = Cursors.Default;
                MblnShowData = false;
                return false;
            }
            catch (Exception Ex)
            {
                MblnShowData = false;
                Cursor.Current = Cursors.Default;
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void FillDateToGrid(int intEmpID)
        {
            try
            {
                DgvAttendance.RowCount = 0;
                if (intEmpID == 0)
                {
                    return;
                }
                int iRw = 0;
                DateTime dpDate;
                int iDaycount = 30;
                bool bLeave = false;
                int intCmpID = Convert.ToInt32(CboCompany.SelectedValue);
                dpDate = Convert.ToDateTime(GetStratDate());
                iDaycount = System.Data.Linq.SqlClient.SqlMethods.DateDiffDay(dpDate, dpDate.AddMonths(1));
                DgvAttendance.Columns["ColDgvAddMore"].Width = 35;
                DgvAttendance.Columns["ColDgvAddMore"].MinimumWidth = 35;
                DgvAttendance.Columns["ColDgvAtnDate"].Width = 75;

                for (int i = 0; i <= iDaycount - 1; i++)
                {
                    bLeave = false;
                    bool leaveflag = false;
                    bool blnHoliday = false;
                    bool blnOffDay = false;
                    bool blnLeaveDay = false;

                    blnOffDay = MobjclsBLLAttendance.CheckOffDay(intEmpID, dpDate.DayOfWeek.ToString(), dpDate);
                    blnHoliday = MobjclsBLLAttendance.CheckHoliday(intCmpID, dpDate.ToString("dd MMM yyyy"));
                    if (blnOffDay != true && blnHoliday != true)//leave only handled if it is not holiday and off day
                    {
                        blnLeaveDay = MobjclsBLLAttendance.CheckLeave(intEmpID, dpDate.ToString("MM/dd/yyyy"), ref leaveflag);
                    }

                    if (blnLeaveDay == false && blnHoliday == false && blnOffDay == false)
                        bLeave = false;
                    else
                        bLeave = true;

                    DgvAttendance.RowCount += 1;
                    iRw = DgvAttendance.RowCount - 1;
                    DgvAttendance.Rows[iRw].Cells["ColDgvAtnDate"].Value = Convert.ToString(dpDate.ToString("dd MMM yyyy"));
                    DgvAttendance.Rows[iRw].Cells["ColDgvDay"].Value = dpDate.DayOfWeek.ToString();
                    DgvAttendance.Rows[iRw].Cells["ColDgvValidFlag"].Value = 0;
                    if (bLeave == false)
                    {

                        DgvAttendance.Rows[iRw].Cells["ColDgvHolidayFlag"].Value = 0;
                        DgvAttendance.Rows[iRw].Cells["ColDgvHolidayFlag"].Tag = 0;
                        DgvAttendance.Rows[iRw].Cells["ColDgvLeaveFlag"].Tag = 0;
                        DgvAttendance.Rows[iRw].Cells["ColDgvLeaveFlag"].Value = 0;
                    }
                    else
                    {
                        if (leaveflag == false)//'leave entry 
                        {
                            DgvAttendance.Rows[iRw].DefaultCellStyle.ForeColor = Color.Red;
                            DgvAttendance.Rows[iRw].Cells["ColDgvHolidayFlag"].Value = 1;
                            DgvAttendance.Rows[iRw].Cells["ColDgvHolidayFlag"].Tag = 1;
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveFlag"].Tag = 0;
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveFlag"].Value = 0;
                        }

                        else
                        {

                            //if (DgvAttendance.Rows[iRw].Cells["ColDgvLeaveInfo"].Value.ToInt32() == 1)
                            //{
                            //    DgvAttendance.Rows[iRw].DefaultCellStyle.ForeColor = Color.Black;
                            //}
                            //else
                            //{
                            //    DgvAttendance.Rows[iRw].DefaultCellStyle.ForeColor = Color.Red;
                            //}
                            DgvAttendance.Rows[iRw].DefaultCellStyle.ForeColor = Color.Red;
                            DgvAttendance.Rows[iRw].Cells["ColDgvHolidayFlag"].Value = 0;
                            DgvAttendance.Rows[iRw].Cells["ColDgvHolidayFlag"].Tag = 0;
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveFlag"].Tag = 1;
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveFlag"].Value = 1;

                            DataTable datLeaveRemarks = MobjclsBLLAttendance.GetLeaveRemarks(intCmpID, intEmpID, DgvAttendance.Rows[iRw].Cells["ColDgvAtnDate"].Value.ToDateTime());
                            if (datLeaveRemarks.Rows.Count > 0)
                            {
                                DgvAttendance.Rows[iRw].Cells["ColDgvRemarks"].Value = datLeaveRemarks.Rows[0]["Remarks"].ToStringCustom();
                                DgvAttendance.Rows[iRw].Cells["ColDgvStatus"].Value = (int)AttendanceStatus.Leave;
                                DgvAttendance.Rows[iRw].Cells["ColDgvLeaveType"].Value = datLeaveRemarks.Rows[0]["LeaveTypeID"].ToInt32();
                                DgvAttendance.Rows[iRw].Cells["ColDgvLeaveInfo"].Value = datLeaveRemarks.Rows[0]["HalfDay"].ToInt32();
                                DgvAttendance.Rows[iRw].Cells["ColDgvLeaveType"].Tag = datLeaveRemarks.Rows[0]["LeaveTypeID"].ToInt32();
                                if (DgvAttendance.Rows[iRw].Cells["ColDgvLeaveInfo"].Value.ToInt32() == 1)
                                {
                                    DgvAttendance.Rows[iRw].DefaultCellStyle.ForeColor = Color.Black;
                                }
                            }
                        }
                    }

                    DgvAttendance.Rows[iRw].Cells["ColDgvEmployee"].Tag = intEmpID;
                    DgvAttendance.Rows[iRw].Cells["ColDgvEmployee"].Value = MstrEmployeeName;
                    DgvAttendance.Rows[iRw].Cells["ColDgvCompanyID"].Value = intCmpID;

                    int iShiftId = 0;

                    string sShiftName = "";
                    string sFromtime = "";
                    string sTotime = "";

                    string sDuration = "";
                    int ishiftType = 0;
                    int iNoOfTimings = 0;
                    string sMinWorkHours = "";
                    string sAllowedBreak = "0";
                    int iLate = 0;
                    int iEarly = 0;

                    if (GetEmployeeShiftInfo(intEmpID, intCmpID, Convert.ToDateTime(DgvAttendance.Rows[iRw].Cells["ColDgvAtnDate"].Value), ref iShiftId,
                           ref sShiftName, ref sFromtime, ref sTotime, ref sDuration, ref ishiftType, ref iNoOfTimings, ref sMinWorkHours, ref sAllowedBreak, ref iLate, ref iEarly))
                    {
                        DgvAttendance.Rows[iRw].Cells["ColDgvShift"].Tag = iShiftId;
                        DgvAttendance.Rows[iRw].Cells["ColDgvShift"].Value = sShiftName;
                        sAllowedBreak = sAllowedBreak.ToStringCustom() == string.Empty ? "0" : sAllowedBreak;
                        DgvAttendance.Rows[iRw].Cells["ColDgvAllowedBreakTime"].Tag = sAllowedBreak;
                        sAllowedBreak = Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAllowedBreak), DateTime.Today).ToString();
                        sAllowedBreak = Convert.ToDateTime(sAllowedBreak).ToString("HH:mm:ss");
                        DgvAttendance.Rows[iRw].Cells["ColDgvAllowedBreakTime"].Value = sAllowedBreak;
                    }

                    dpDate = Microsoft.VisualBasic.DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Day, 1, dpDate);
                }//For i;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool FillEmployesToGrid(string sEmpIds, bool bDel)
        {

            try
            {
                string strSearchConditionEmployee = "";
                string strSearchConditionAttendance = "";

                GetSearchCondition(ref strSearchConditionEmployee, ref strSearchConditionAttendance);

                DateTime dtdate = dtpFromDate.Value.Date;
                int i = 0;
                int iRw = 0;
                if (sEmpIds != "" && sEmpIds.Length > 0)
                    sEmpIds = sEmpIds.Substring(0, sEmpIds.LastIndexOf(","));
                DataTable dtEmp = MobjclsBLLAttendance.FillEmployes(sEmpIds, dtdate, strSearchConditionEmployee, strSearchConditionAttendance);
                if (bDel)
                {
                    DgvAttendance.RowCount = 0;
                }
                for (i = 0; i <= dtEmp.Rows.Count - 1; i++)
                {
                    iRw = DgvAttendance.RowCount;
                    DgvAttendance.RowCount += 1;
                    DgvAttendance.Rows[iRw].Cells["ColDgvEmployee"].Tag = dtEmp.Rows[i]["EmployeeID"];
                    DgvAttendance.Rows[iRw].Cells["ColDgvEmployee"].Value = dtEmp.Rows[i]["Employee"];
                    DgvAttendance.Rows[iRw].Cells["ColDgvAtnDate"].Value = dtpFromDate.Value.ToString("dd MMM yyyy");
                    DgvAttendance.Rows[iRw].Cells["ColDgvDay"].Value = dtpFromDate.Value.Date.DayOfWeek.ToString();
                    DgvAttendance.Rows[iRw].Cells["ColDgvCompanyID"].Value = dtEmp.Rows[i]["CompanyID"];

                    int iType = 0;
                    if (CheckIfValidDate(iRw, ref iType) == false)
                    {
                        if (iType == 3 || iType == 2 || iType == 4)//Then 'off day and off day without shift and Holiday
                        {
                            DgvAttendance.Rows[iRw].DefaultCellStyle.ForeColor = Color.Red;
                            DgvAttendance.Rows[iRw].Cells["ColDgvHolidayFlag"].Value = 1;
                            DgvAttendance.Rows[iRw].Cells["ColDgvHolidayFlag"].Tag = 1;
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveFlag"].Tag = 0;
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveFlag"].Value = 0;
                        }
                        else if (iType == 1)// Then 'leave
                        {
                            DgvAttendance.Rows[iRw].DefaultCellStyle.ForeColor = Color.Red;
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveFlag"].Tag = 1;
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveFlag"].Value = 1;
                            DgvAttendance.Rows[iRw].Cells["ColDgvHolidayFlag"].Value = 0;
                            DgvAttendance.Rows[iRw].Cells["ColDgvHolidayFlag"].Tag = 0;
                        }
                    }

                    int shiftId = 0;
                    string shiftDesc = "";
                    string Fromtime = "";
                    string Totime = "";
                    int iShiftType = 0;
                    int iNoOfTimings = 0;
                    string minWorkHours = "0";
                    string duration = "0";
                    string sAllowedBreakTime = "0";
                    int iLate = 0;
                    int iEarly = 0;
                    if (GetEmployeeShiftInfo(Convert.ToInt32(DgvAttendance.Rows[iRw].Cells["ColDgvEmployee"].Tag), Convert.ToInt32(DgvAttendance.Rows[iRw].Cells["ColDgvCompanyID"].Value),
                        dtpFromDate.Value.Date, ref shiftId, ref shiftDesc, ref Fromtime, ref Totime, ref duration,
                               ref iShiftType, ref iNoOfTimings, ref minWorkHours, ref sAllowedBreakTime, ref iLate, ref iEarly) == true)
                    {
                        DgvAttendance.Rows[iRw].Cells["ColDgvShift"].Tag = shiftId;
                        DgvAttendance.Rows[iRw].Cells["ColDgvShift"].Value = shiftDesc;
                        DgvAttendance.Rows[iRw].Cells["ColDgvAllowedBreakTime"].Tag = sAllowedBreakTime;
                        sAllowedBreakTime = Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAllowedBreakTime), DateTime.Today).ToString();
                        sAllowedBreakTime = Convert.ToDateTime(sAllowedBreakTime).ToString("HH:mm:ss");
                        DgvAttendance.Rows[iRw].Cells["ColDgvAllowedBreakTime"].Value = sAllowedBreakTime;

                    }
                    DgvAttendance.CurrentCell = DgvAttendance[ColDgvEmployee.Index, iRw];
                    DgvAttendance.Refresh();
                }

                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private bool CheckIfValidDate(int rowindex, ref  int Type)
        {
            try
            {
                int iCompID = Convert.ToInt32(CboCompany.SelectedValue);
                int iEmpId = Convert.ToInt32(DgvAttendance.Rows[rowindex].Cells["ColDgvEmployee"].Tag);
                DateTime dCurrentDate = Convert.ToDateTime(DgvAttendance.Rows[rowindex].Cells["ColDgvAtnDate"].Value);
                int intDayID = Convert.ToInt32(dCurrentDate.DayOfWeek) + 1;
                if (MobjclsBLLAttendance.IsEmployeeOnLeave(iEmpId, iCompID, dCurrentDate) == true)
                {
                    Type = 1;
                    return false;
                }
                if (MobjclsBLLAttendance.CheckDay(iEmpId, intDayID, dCurrentDate) == false)
                {
                    Type = 3;
                    if (MobjclsBLLAttendance.CheckOffDayShift(iEmpId, intDayID, dCurrentDate) == false)
                    {
                        Type = 2;       //''offday without shift
                        return false;
                    }
                    return false;
                }
                if (MobjclsBLLAttendance.CheckHoliday(iCompID, dCurrentDate.ToString("dd MMM yyyy")) == true)
                {
                    Type = 4;//       ''Holiday
                    return false;
                }
                return true;

            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        private bool GetEmployeeShiftInfo(int intEmpID, int intCmpID, DateTime atnDate, ref int iShiftId, ref string sShiftName, ref string sFromtime,
                                      ref string sTotime, ref string sDuration, ref int ishiftType,
                                        ref int iNoOfTimings, ref string sMinWorkHours, ref string sAllowedBreak, ref int iLate, ref int iEarly)
        {
            DataTable dtEmployeeInfo = new DataTable();

            iShiftId = 0;
            sShiftName = "";
            sFromtime = "";
            sTotime = "";
            sDuration = "";
            ishiftType = 0;
            iNoOfTimings = 0;
            sMinWorkHours = "0";
            sAllowedBreak = "0";
            iLate = 0;
            iEarly = 0;

            int intDayID = 0;
            intDayID = (int)atnDate.DayOfWeek + 1;
            string sFromDate = atnDate.ToString("dd MMM yyyy");
            string sToDate = atnDate.ToString("dd MMM yyyy");

            try
            {
                dtEmployeeInfo = MobjclsBLLAttendance.GetEmployeeShiftInfo(intEmpID, intCmpID, sFromDate);//Shift Schedule fromDAte and toDAte are same
                if (dtEmployeeInfo.Rows.Count > 0)
                {

                    ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                    iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                    sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                    iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftID"]);
                    sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["ShiftName"]);
                    sFromtime = Convert.ToString(dtEmployeeInfo.Rows[0]["FromTime"]);
                    sTotime = Convert.ToString(dtEmployeeInfo.Rows[0]["ToTime"]);
                    sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                    sAllowedBreak = Convert.ToString(dtEmployeeInfo.Rows[0]["AllowedBreakTime"]);
                    iLate = Convert.ToInt32(dtEmployeeInfo.Rows[0]["LateAfter"]);
                    iEarly = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EarlyBefore"]);
                }
                else
                {
                    bool dailyShift = MobjclsBLLAttendance.CheckDay(intEmpID, intDayID, atnDate);

                    if (dailyShift == true)
                    {

                        dtEmployeeInfo = MobjclsBLLAttendance.GetDailyShift(intEmpID, intDayID, atnDate);
                        if (dtEmployeeInfo.Rows.Count > 0)
                        {
                            iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftID"]);
                            sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["Shift"]);
                            sFromtime = Convert.ToString(ConvertDate24(Convert.ToDateTime(dtEmployeeInfo.Rows[0]["TimeIn1"])));
                            sTotime = Convert.ToString(ConvertDate24(Convert.ToDateTime(dtEmployeeInfo.Rows[0]["TimeOut1"])));
                            ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                            sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                            iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                            sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                            sAllowedBreak = Convert.ToString(dtEmployeeInfo.Rows[0]["AllowedBreakTime"]);
                            iLate = Convert.ToInt32(dtEmployeeInfo.Rows[0]["LateAfter"]);
                            iEarly = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EarlyBefore"]);
                        }

                    }
                    else
                    {
                        dtEmployeeInfo = MobjclsBLLAttendance.GetOffDayShift(intEmpID, intDayID, atnDate);
                        if (dtEmployeeInfo.Rows.Count > 0)
                        {
                            iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftID"]);
                            sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["Shift"]);
                            sFromtime = Convert.ToString(ConvertDate24(Convert.ToDateTime(dtEmployeeInfo.Rows[0]["TimeIn1"])));
                            sTotime = Convert.ToString(ConvertDate24(Convert.ToDateTime((dtEmployeeInfo.Rows[0]["TimeOut1"]))));
                            ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                            sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                            iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                            sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                            sAllowedBreak = Convert.ToString(dtEmployeeInfo.Rows[0]["AllowedBreakTime"]);
                            iLate = Convert.ToInt32(dtEmployeeInfo.Rows[0]["LateAfter"]);
                            iEarly = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EarlyBefore"]);
                        }

                    }
                }
                return true;
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private string GetStratDate()
        {

            string sMonth = "";
            switch (dtpFromDate.Value.Month)
            {
                case 1:
                    sMonth = "Jan";
                    break;
                case 2:
                    sMonth = "Feb";
                    break;
                case 3:
                    sMonth = "Mar";
                    break;
                case 4:
                    sMonth = "Apr";
                    break;
                case 5:
                    sMonth = "May";
                    break;
                case 6:
                    sMonth = "Jun";
                    break;
                case 7:
                    sMonth = "Jul";
                    break;
                case 8:
                    sMonth = "Aug";
                    break;
                case 9:
                    sMonth = "Sep";
                    break;
                case 10:
                    sMonth = "Oct";
                    break;
                case 11:
                    sMonth = "Nov";
                    break;
                case 12:
                    sMonth = "Dec";
                    break;
            }
            return Convert.ToString("01-" + sMonth + "-" + dtpFromDate.Value.Year);
        }

        private string CountQuery(int cnt)
        {
            string strInOut = "";
            int j = 1;
            for (int i = 1; i <= cnt; i++)
            {
                if (i == 1)
                {
                    strInOut = "[" + j + "] as TimeIn" + i + ",";
                    j = j + 1;
                    strInOut = strInOut + "[" + j + "] as TimeOut" + i;
                }
                else
                {
                    strInOut = strInOut + ",[" + j + "] as TimeIn" + i + ",";
                    j = j + 1;
                    strInOut = strInOut + "[" + j + "] as TimeOut" + i;
                }
                j = j + 1;
            }
            return strInOut;
        }

        private string CountQuery2(int cnt)
        {
            string strInOut2 = "";
            int j = 1;
            for (int i = 1; i <= cnt; i++)
            {
                if (i == 1)
                {
                    strInOut2 = "[" + j + "],";
                    j = j + 1;
                    strInOut2 = strInOut2 + "[" + j + "]";
                }
                else
                {
                    strInOut2 = strInOut2 + ",[" + j + "],";
                    j = j + 1;
                    strInOut2 = strInOut2 + "[" + j + "]";
                }
                j = j + 1;
            }
            return strInOut2;
        }

        private int GetDayOfWeek(DateTime Day)
        {
            switch (Day.DayOfWeek)
            {

                case DayOfWeek.Sunday:
                    return 1;
                case DayOfWeek.Monday:
                    return 2;
                case DayOfWeek.Tuesday:
                    return 3;
                case DayOfWeek.Wednesday:
                    return 4;
                case DayOfWeek.Thursday:
                    return 5;
                case DayOfWeek.Friday:
                    return 6;
                case DayOfWeek.Saturday:
                    return 7;
            }
            return 0;
        }

        private bool AutoFillEmployees()
        {
            try
            {

                lblShiftName.Text = "";
                DgvAttendance.Columns["ColDgvAddMore"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                DgvAttendance.Columns["ColDgvAddMore"].Width = 75;

                InitializeGrid();
                MintTimingsColumnCnt = 2;
                DateTime dStartDate;
                DateTime dEndDate;
                dStartDate = Convert.ToDateTime(GetStratDate());
                dEndDate = Microsoft.VisualBasic.DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Day, -1, Microsoft.VisualBasic.DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Month, 1, dStartDate));
                if (rbtnMonthly.Checked == true)
                {
                    FillDateToGrid(MintEmployeeID);
                }

                for (int i = 0; i <= DgvAttendance.RowCount - 1; i++)
                {
                    fillAttendanceRow(i);
                }

                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }

        }
        private void fillAttendanceRow(int i)
        {
            int iEmp = 0;
            DateTime? dJoiningDate = null;
            string StrDate = "";
            iEmp = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag);
            string strJoining = MobjclsBLLAttendance.GetEmployeeJoiningDate(iEmp);
            if (strJoining != "")
            {
                dJoiningDate = Convert.ToDateTime(strJoining);
            }

            DateTime day = Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value);
            if (day.Date < Convert.ToDateTime(dJoiningDate).Date)
            {
                //continue;
                return;
            }
            if (day.Date > DateTime.Now.Date)
            {
                //continue;
                return;
            }
            bool blnCheckDay = false;
            int iShiftID = 0;
            string sShiftName = "";
            string sFromtime = "";
            string sTotime = "";

            string sDuration = "";
            int ishiftType = 0;
            int iNoOfTimings = 0;
            string sMinWorkHours = "";
            int iShiftOrderNO = 0;
            int iAllowedBreaktime = 0;
            int iDayID = GetDayOfWeek(day);
            int iPolicyID = 0;
            string sAllowedBreaktime = "0";
            int iEarly = 0;
            int iLate = 0;
            //string strRemarks = MobjclsBLLAttendance.GetLeaveRemarks(Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value), iEmp, DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value.ToDateTime());
            //if (strRemarks.Trim() != "")
            //{
            //    DgvAttendance.Rows[i].Cells["ColDgvRemarks"].Value = strRemarks;
            //}
            if (GetEmployeeShiftInfo(iEmp, Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value), Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value),
                                        ref iShiftID, ref sShiftName, ref sFromtime, ref sTotime, ref sDuration, ref ishiftType, ref iNoOfTimings, ref sMinWorkHours, ref sAllowedBreaktime, ref iLate, ref iEarly) == true)
            {

                MobjclsBLLAttendance.GetEmployeePolicy(iEmp, iDayID, ref iPolicyID);
                if (ishiftType == 3)
                {
                    iShiftOrderNO = GetShiftOrderNo(iShiftID, "", ref sFromtime, ref sTotime, ref sDuration);
                    MobjclsBLLAttendance.GetDynamicShiftDuration(iShiftID, iShiftOrderNO, ref sMinWorkHours, ref sAllowedBreaktime);
                }
                blnCheckDay = MobjclsBLLAttendance.CheckDay(iEmp, GetDayOfWeek(day), DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value.ToDateTime());
                iAllowedBreaktime = Convert.ToInt32(sAllowedBreaktime);

                DgvAttendance.Rows[i].Cells["ColDgvAllowedBreakTime"].Tag = iAllowedBreaktime;
                string sAlBreak = Convert.ToString(iAllowedBreaktime);
                sAlBreak = Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAlBreak), DateTime.Today).ToString();
                sAlBreak = Convert.ToDateTime(sAlBreak).ToString("HH:mm:ss");
                DgvAttendance.Rows[i].Cells["ColDgvAllowedBreakTime"].Value = sAlBreak;

                DgvAttendance.Rows[i].Cells["ColDgvShift"].Tag = iShiftID;
                DgvAttendance.Rows[i].Cells["ColDgvShift"].Value = sShiftName;
                DgvAttendance.Rows[i].Cells["ColDgvShiftOrderNo"].Value = iShiftOrderNO;

                int iType = 0;
                if (CheckIfValidDate(i, ref iType))
                {
                    DgvAttendance.Rows[i].Cells["ColDgvHolidayFlag"].Value = 0;
                    DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value = 0;
                }

                else
                {
                    if (iType == 1)
                    {
                        DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value = 1;
                        DgvAttendance.Rows[i].Cells["ColDgvHolidayFlag"].Value = 0;
                    }
                    else if (iType == 2 || iType == 3 || iType == 4)
                    {
                        DgvAttendance.Rows[i].Cells["ColDgvHolidayFlag"].Value = 1;
                        DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value = 0;
                    }
                }
                if (DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value != null && Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value) == 0)
                {
                    if (MobjclsBLLAttendance.CheckHalfdayLeave(Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value), iEmp, Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value)))
                        DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value = 1;
                    else
                        DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value = 0;

                }

                if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvHolidayFlag"].Value) == 0 && Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value) == 0)
                {
                    if (ishiftType <= 3)
                    {
                        DgvAttendance.Rows[i].Cells["TimeIn1"].Value = sFromtime;
                    }
                    DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value = 1;
                }
                DateTime dtDuration;
                int iDurationInMinutes = GetDurationInMinutes(sMinWorkHours);
                dtDuration = Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, iDurationInMinutes, sFromtime.ToDateTime());

                if (Convert.ToInt32(ishiftType) == 2)//'flexi =true
                {

                    if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvHolidayFlag"].Value) == 0 && Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value) == 0)
                        DgvAttendance.Rows[i].Cells["TimeOut1"].Value = dtDuration.Hour.ToString() + ":" + dtDuration.Minute.ToString() + ":" + dtDuration.Second.ToString();

                }
                else if (ishiftType == 4)//Then 'Flexi
                {
                    DataTable DtShiftDetails = MobjclsBLLAttendance.GetDynamicShiftDetails(iShiftID);
                    if (DtShiftDetails.Rows.Count > 0)
                    {
                        int ICount = DtShiftDetails.Rows.Count;
                        int iStrat = DgvAttendance.Columns["ColDgvAddMore"].Index - 1;
                        for (int ij = iStrat; ij <= ICount * 2 + 1; ij += 2)
                        {
                            int iColumnCount = Convert.ToInt32(Math.Floor(Convert.ToDouble(ij / 2)));
                            DataGridViewTextBoxColumn adcolIn = new DataGridViewTextBoxColumn();
                            DataGridViewTextBoxColumn adcolOut = new DataGridViewTextBoxColumn();

                            adcolIn.Name = "TimeIn" + iColumnCount.ToString();
                            adcolIn.HeaderText = "TimeIn" + iColumnCount.ToString();
                            adcolIn.SortMode = DataGridViewColumnSortMode.NotSortable;
                            adcolIn.Resizable = DataGridViewTriState.False;
                            adcolIn.Width = 70;
                            adcolIn.MaxInputLength = 9;

                            adcolOut.Name = "TimeOut" + iColumnCount.ToString();
                            adcolOut.HeaderText = "TimeOut" + iColumnCount.ToString();
                            adcolOut.SortMode = DataGridViewColumnSortMode.NotSortable;
                            adcolOut.Resizable = DataGridViewTriState.False;
                            adcolOut.Width = 70;
                            adcolOut.MaxInputLength = 9;

                            DgvAttendance.Columns.Insert(ij + 1, adcolIn);
                            DgvAttendance.Columns.Insert(ij + 2, adcolOut);

                            MintTimingsColumnCnt += 2;
                        }//For 
                        for (int intCount = 0; intCount <= DtShiftDetails.Rows.Count - 1; intCount++)
                        {
                            string sSplitFromTime = Convert.ToString(DtShiftDetails.Rows[intCount]["FromTime"]);
                            string sSplitToTime = Convert.ToString(DtShiftDetails.Rows[intCount]["ToTime"]);
                            if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvHolidayFlag"].Value) == 0 && Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value) == 0)
                            {
                                DgvAttendance.Rows[i].Cells["TimeIn" + (intCount + 1)].Value = sSplitFromTime;
                                DgvAttendance.Rows[i].Cells["TimeOut" + (intCount + 1)].Value = sSplitToTime;
                            }
                        }

                    }
                }
                else
                {
                    if (Convert.ToString(sTotime).Trim() != "")
                    {
                        if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvHolidayFlag"].Value) == 0 && Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value) == 0)
                        {
                            //DgvAttendance.Rows[i].Cells["TimeOut1"].Value = sTotime;
                            if (iAllowedBreaktime > 0)
                            {
                                int intDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromtime), Convert.ToDateTime(sTotime));

                                if (intDurationDiff < 0)
                                {
                                    intDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromtime), Convert.ToDateTime(sTotime).AddDays(1));
                                }

                                if ((intDurationDiff - iAllowedBreaktime) < iDurationInMinutes)
                                {
                                    DgvAttendance.Rows[i].Cells["TimeOut1"].Value = dtDuration.Hour.ToString() + ":" + dtDuration.Minute.ToString() + ":" + dtDuration.Second.ToString();//'' Totime

                                }
                                else
                                {
                                    int ICount = 3;
                                    int iStrat = DgvAttendance.Columns["ColDgvAddMore"].Index - 1;

                                    for (int ij = iStrat; ij <= ICount + 2; ij = ij + 2)
                                    {
                                        DataGridViewTextBoxColumn adcolIn = new DataGridViewTextBoxColumn();
                                        DataGridViewTextBoxColumn adcolOut = new DataGridViewTextBoxColumn();

                                        adcolIn.Name = "TimeIn" + (ij - 2);
                                        adcolIn.HeaderText = "TimeIn" + (ij - 2);
                                        adcolIn.SortMode = DataGridViewColumnSortMode.NotSortable;
                                        adcolIn.Resizable = DataGridViewTriState.False;
                                        adcolIn.Width = 70;
                                        adcolIn.MaxInputLength = 9;

                                        adcolOut.Name = "TimeOut" + (ij - 2);
                                        adcolOut.HeaderText = "TimeOut" + (ij - 2);
                                        adcolOut.SortMode = DataGridViewColumnSortMode.NotSortable;
                                        adcolOut.Resizable = DataGridViewTriState.False;
                                        adcolOut.Width = 70;
                                        adcolOut.MaxInputLength = 9;

                                        DgvAttendance.Columns.Insert(ij + 1, adcolIn);
                                        DgvAttendance.Columns.Insert(ij + 2, adcolOut);
                                        MintTimingsColumnCnt += 2;
                                    }

                                    dtDuration = Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, 30, Convert.ToDateTime(sFromtime)); //''initial default break
                                    DgvAttendance.Rows[i].Cells["TimeOut1"].Value = dtDuration.Hour.ToString() + ":" + dtDuration.Minute.ToString() + ":" + dtDuration.Second.ToString();//'' Totime
                                    dtDuration = Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, iAllowedBreaktime, Convert.ToDateTime(dtDuration));
                                    DgvAttendance.Rows[i].Cells["TimeIn2"].Value = dtDuration.Hour.ToString() + ":" + dtDuration.Minute.ToString() + ":" + dtDuration.Second.ToString();// '' Totime
                                    dtDuration = Convert.ToDateTime(sTotime);
                                    DgvAttendance.Rows[i].Cells["TimeOut2"].Value = dtDuration.Hour.ToString() + ":" + dtDuration.Minute.ToString() + ":" + dtDuration.Second.ToString();// '' Totime
                                }
                            }
                            else
                            {
                                DgvAttendance.Rows[i].Cells["TimeOut1"].Value = dtDuration.Hour.ToString() + ":" + dtDuration.Minute.ToString() + ":" + dtDuration.Second.ToString();// '' Totime
                            }

                        }

                    }

                }

                for (int j = 1; j <= Math.Floor(Convert.ToDouble((MintTimingsColumnCnt + 1) / 2)); j++)
                {
                    if (Glbln24HrFormat == false)
                    {
                        if ((Microsoft.VisualBasic.Information.IsNothing(DgvAttendance.Rows[i].Cells["TimeIn" + j].Value.ToStringCustom()) ? "" : DgvAttendance.Rows[i].Cells["TimeIn" + j].Value.ToStringCustom()) != "")
                        {
                            StrDate = ConvertDate12(Convert.ToDateTime(DgvAttendance.Rows[i].Cells["TimeIn" + j].Value));
                            if (Microsoft.VisualBasic.Strings.Right(DTPFormatTime.Text.Trim(), 2) == "AM")
                                DgvAttendance.Rows[i].Cells["TimeIn" + j].Value = DTPFormatTime.Text.Replace(":AM", " AM");
                            else
                                DgvAttendance.Rows[i].Cells["TimeIn" + j].Value = DTPFormatTime.Text.Replace(":PM", " PM");

                        }
                        if ((Microsoft.VisualBasic.Information.IsNothing(DgvAttendance.Rows[i].Cells["TimeOut" + j].Value.ToStringCustom()) ? "" : DgvAttendance.Rows[i].Cells["TimeOut" + j].Value.ToStringCustom()) != "")
                        {
                            StrDate = ConvertDate12(Convert.ToDateTime(DgvAttendance.Rows[i].Cells["TimeOut" + j].Value));
                            if (Microsoft.VisualBasic.Strings.Right(DTPFormatTime.Text.Trim(), 2) == "AM")
                                DgvAttendance.Rows[i].Cells["TimeOut" + j].Value = DTPFormatTime.Text.Replace(":AM", " AM");
                            else
                                DgvAttendance.Rows[i].Cells["TimeOut" + j].Value = DTPFormatTime.Text.Replace(":PM", " PM");

                        }
                    }
                    else
                    {
                        if ((Microsoft.VisualBasic.Information.IsNothing(DgvAttendance.Rows[i].Cells["TimeIn" + j].Value.ToStringCustom()) ? "" : DgvAttendance.Rows[i].Cells["TimeIn" + j].Value.ToStringCustom()) != "")
                        {
                            StrDate = ConvertDate24(Convert.ToDateTime(DgvAttendance.Rows[i].Cells["TimeIn" + j].Value));
                            DgvAttendance.Rows[i].Cells["TimeIn" + j].Value = DTPFormatTime.Text;
                        }
                        if ((Microsoft.VisualBasic.Information.IsNothing(DgvAttendance.Rows[i].Cells["TimeOut" + j].Value) ? "" : Convert.ToString(DgvAttendance.Rows[i].Cells["TimeOut" + j].Value)) != "")
                        {
                            StrDate = ConvertDate24(Convert.ToDateTime(DgvAttendance.Rows[i].Cells["TimeOut" + j].Value));
                            DgvAttendance.Rows[i].Cells["TimeOut" + j].Value = DTPFormatTime.Text;
                        }
                    }
                }// For 

            }
            if (Convert.ToString(DgvAttendance.Rows[i].Cells["TimeIn1"].Value) != "" && Convert.ToString(DgvAttendance.Rows[i].Cells["TimeOut1"].Value) != "" &&
            DgvAttendance.Rows[i].DefaultCellStyle.ForeColor != Color.IndianRed)
            {
                WorkTimeCalculation(i, false);
            }
        }
        private bool CheckingLeaves(int intLeaveType, ref  int Type)
        {

            double dblHalfdayLeave = 0;
            double intFulldayleave = 0;
            double TotalLeaves = 0;
            double dblTakenLeavesFrmAtten = 0;
            try
            {
                //if (MblnPayExists)
                //{


                for (int i = 0; i <= DgvAttendance.Rows.Count - 1; i++)
                {
                    if (DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value.ToInt32() == Convert.ToInt32(AttendanceStatus.Leave))
                    {
                        if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveType"].Value) == intLeaveType)
                        {
                            DataTable datLeaveRemarks = MobjclsBLLAttendance.GetLeaveRemarks(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value.ToDateTime());
                            if (datLeaveRemarks.Rows.Count <= 0)
                            {

                                if (Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Value) == "1")
                                {
                                    dblHalfdayLeave += 0.5;
                                    if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveId"].Value) > 0)
                                    {
                                        dblTakenLeavesFrmAtten += 0.5;
                                    }
                                }
                                else if (Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Value) == "0")
                                {
                                    intFulldayleave += 1;
                                    if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveId"].Value) > 0)
                                    {
                                        dblTakenLeavesFrmAtten += 1;
                                    }
                                }
                            }
                        }
                    }
                }
                TotalLeaves = intFulldayleave + dblHalfdayLeave - dblTakenLeavesFrmAtten;

                if (MdblMonthlyleave > 0)
                {
                    if ((MdblMonthlyleave - MdblTakenLeaves) >= TotalLeaves)
                    {
                        if ((MdblMonthlyleave - MdblTakenLeaves) == 0.5)
                        //'half day casual
                        { Type = 1; }//'half day allowed
                        else if ((MdblMonthlyleave - MdblTakenLeaves) >= 1)
                        {//'full day casual or halfday
                            Type = 2;
                        }
                        else
                        {
                            Type = 0;
                        }
                    }
                    else
                    {
                        Type = 0;
                    }
                }
                else
                {
                    Type = 0;
                }
                // }
                return true;

            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                //return false;
                return false;
            }
        }
        private bool CheckingYearlyLeaves(int intCompanyID, int intEmployee, int intLeaveType, DateTime dteDate)
        {
            try
            {
                double dblHalfdayLeave = 0;
                double intFulldayleave = 0;
                for (int i = 0; i <= DgvAttendance.Rows.Count - 1; i++)
                {
                    if (DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value.ToInt32() == Convert.ToInt32(AttendanceStatus.Leave))
                    {
                        if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveType"].Value) == intLeaveType)
                        {
                            if (DgvAttendance.Rows[i].Cells["ColDgvLeaveId"].Value.ToInt32() == 0)
                            {

                                DataTable datLeaveRemarks = MobjclsBLLAttendance.GetLeaveRemarks(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value.ToDateTime());
                                if (datLeaveRemarks.Rows.Count <= 0)
                                {

                                    if (Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Value) == "1")
                                    {
                                        dblHalfdayLeave += 0.5;

                                    }
                                    else if (Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Value) == "0")
                                    {
                                        intFulldayleave += 1;

                                    }
                                }
                            }
                        }
                    }
                }
                if ((intFulldayleave + dblHalfdayLeave) > 0)
                {
                    DataSet dtsLeave = MobjclsBLLAttendance.GetBalanceAndHolidaysdetails(intCompanyID, intEmployee, intLeaveType, dteDate.ToString("dd MMM yyyy"));
                    if (dtsLeave.Tables.Count > 0)
                    {
                        if (dtsLeave.Tables[0].Rows.Count > 0)
                        {
                            double dblBalanceLeave = dtsLeave.Tables[0].Rows[0][0].ToDouble();
                            if ((intFulldayleave + dblHalfdayLeave) <= dblBalanceLeave)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }

                }
                return false;
            }
            catch (Exception Ex)
            {
                return false;
            }
        }
        private bool GetEmployeeLeaveStatus(int iEmpId, int iCmpID, DateTime dtDate, int iLeaveType)
        {
            try
            {
                MdblMonthlyleave = 0;
                MdblTakenLeaves = 0;

                MdblMonthlyleave = MobjclsBLLAttendance.GetEmployeeLeaveStatus(iEmpId, iCmpID, dtDate.Date.ToString("dd MMM yyyy"), iLeaveType);

                if (MdblMonthlyleave > 0)
                {
                    string strdate = dtDate.Date.ToString("dd MMM yyyy");
                    MdblTakenLeaves = MobjclsBLLAttendance.GetTakenleaves(iEmpId, Convert.ToDateTime(strdate), iLeaveType);
                }
                else
                {
                    MdblMonthlyleave = 0;
                }
                return true;
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                // MessageBox.Show("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                //;
                return false;
            }
        }

        private void LoadForm()
        {
            try
            {

                SetPermissions();
                ClsCommonSettings.MessageCaption = ClsCommonSettings.MessageCaption == null ? "MPF" : ClsCommonSettings.MessageCaption;

                //DgvAttendance.Visible = true;
                //DgvAttendance.BringToFront();
                //DgvPunchingData.Visible = false;

                //ColDgvDay.Visible = false;
                //ColDgvWorkTime.Visible = false;
                //ColDgvLateComing.Visible = false;
                //ColDgvEarlyGoing.Visible = false;
                //ColDgvBreakTime.Visible = false;
                //ColDgvExcessTime.Visible = false;
                DgvAttendance.Columns["ColDgvAddMore"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                DgvAttendance.Columns["ColDgvAddMore"].Width = 35;
                DgvAttendance.Columns["ColDgvAddMore"].MinimumWidth = 35;

                btnViewDay.Checked = false;
                btnViewLateComing.Checked = false;
                btnViewEarlyGoing.Checked = false; ;
                btnViewWorkTime.Checked = false;
                btnViewBreakTime.Checked = false;
                btnViewExcessTime.Checked = false;

                dtpFromDate.Value = DateTime.Now.Date;
                //rbtnMonthly.Checked = true;
                //dtpFromDate.CustomFormat = "MMM yyyy";
                //dtpFromDate.Format = DateTimePickerFormat.Custom;

                //dtpFromDate.CustomFormat = "MMM yyyy";
                //grpDaySummary.Text = "Month Summary";
                //lblAbsentDispaly.Text = "0";
                //lblLeaveCountDispaly.Text = "0";
                //lblPresentDisplay.Text = "0";
                //lblShortageCountDispaly.Text = "0";
                //lblEmployeeSelectedValue.Text = MstrEmployeeName;
                ColDgvEmployee.Visible = false;
                ColDgvAtnDate.Visible = true;

                this.Cursor = Cursors.WaitCursor;
                if (DgvAttendance.RowCount >= 1)
                {
                    DgvAttendance.ClearSelection();
                }
                InitializeGrid();
                this.Cursor = Cursors.Default;
                tmrClearlabels.Enabled = true;
                //----------set status to LoadEmployeeGrid ---
                MintOnFormLoad = false;
                LoadEmployeeGrid();
                //--------------------------------------------
                FillDevices();
                FillTemplates();
                if (DgvEmployee.RowCount >= 1)
                {
                    DgvEmployee.ClearSelection();
                }
            }
            catch (Exception Ex)
            {
                this.Cursor = Cursors.Default;
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }
        }

        private void ShowConsequenceAndShiftInfo(int intRowIndex)
        {
            try
            {
                string sfromtime = "";
                string stotime = "";
                string sduration = "";
                int iShiftType = 0;
                int iNoOfTimings = 0;
                string minWorkhours = "0";
                int iLate = 0;
                int iEarly = 0;
                string sAllowedBreak = "0";

                DateTime dtdate = Convert.ToDateTime(DgvAttendance.Rows[intRowIndex].Cells["ColDgvAtnDate"].Value);//.ToString("dd MMM yyyy")

                string StrConseqInfo = MobjclsBLLAttendance.ShowConsequence(Convert.ToInt32(DgvAttendance.Rows[intRowIndex].Cells["ColDgvEmployee"].Tag), dtdate);

                if (StrConseqInfo != "")
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    lblConsequence.Text = " نتيجة " + StrConseqInfo;
                    //}
                    //else
                    //{
                        lblConsequence.Text = " Consequence : " + StrConseqInfo;
                    //}
                    lblConsequence.Visible = true;
                }
                else
                {
                    lblConsequence.Visible = false;
                }

                if (DgvAttendance.Rows[intRowIndex].Cells["ColDgvShift"].Tag != null)
                {
                    GetShiftInfo(Convert.ToInt32(DgvAttendance.Rows[intRowIndex].Cells["ColDgvCompanyID"].Value),
                        Convert.ToInt32(DgvAttendance.Rows[intRowIndex].Cells["ColDgvEmployee"].Tag),
                        Convert.ToInt32(DgvAttendance.Rows[intRowIndex].Cells["ColDgvShift"].Tag), dtdate, ref sfromtime, ref  stotime,
                        ref sduration, ref iShiftType, ref iNoOfTimings, ref  minWorkhours, ref sAllowedBreak, ref iLate, ref iEarly);
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }

        private void ShowCount()
        {
            try
            {
                string strSearchConditionEmployee = "";
                string strSearchConditionAttendance = "";
                GetSearchCondition(ref strSearchConditionEmployee, ref  strSearchConditionAttendance);
                bool blnIsMOnthly = rbtnMonthly.Checked;
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    if (rbtnMonthly.Checked)
                //        grpDaySummary.Text = "ملخص الشهر";
                //    else
                //        grpDaySummary.Text = "ملخص اليوم";
                //}
                //else
                //{
                    if (rbtnMonthly.Checked)
                        grpDaySummary.Text = "Month Summary";
                    else
                        grpDaySummary.Text = "Day Summary";
                //}
                DataSet dtGetData = MobjclsBLLAttendance.ShowCount(dtpFromDate.Value.Date, MintEmployeeID, strSearchConditionEmployee, blnIsMOnthly);

                decimal decHalfdayLeaveCount = 0;
                decimal decFulldayleaveCount = 0;
                decimal decLeaveFromLeaveEntry = 0;
                decimal decLeaveCount = 0;
                if (dtGetData.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= dtGetData.Tables[0].Rows.Count - 1; i++)
                    {

                        if (Convert.ToString(dtGetData.Tables[0].Rows[i]["Status"]) == "Present")
                            lblPresentDisplay.Text = Convert.ToString(dtGetData.Tables[0].Rows[i]["Count"]);
                        else if (Convert.ToString(dtGetData.Tables[0].Rows[i]["Status"]) == "Absent")
                            lblAbsentDispaly.Text = Convert.ToString(dtGetData.Tables[0].Rows[i]["Count"]);
                        else if (Convert.ToString(dtGetData.Tables[0].Rows[i]["Status"]) == "HalfDayLeave")
                            decHalfdayLeaveCount = dtGetData.Tables[0].Rows[i]["Count"].ToDecimal();
                        else if (Convert.ToString(dtGetData.Tables[0].Rows[i]["Status"]) == "FullDayLeave")
                            decFulldayleaveCount = dtGetData.Tables[0].Rows[i]["Count"].ToDecimal();
                        else if (Convert.ToString(dtGetData.Tables[0].Rows[i]["Status"]) == "Shortage")
                            lblShortageCountDispaly.Text = Convert.ToString(dtGetData.Tables[0].Rows[i]["Count"]);

                    }


                }
                if (dtGetData.Tables[1].Rows.Count > 0)
                {
                    decLeaveFromLeaveEntry = dtGetData.Tables[1].Rows[0]["Count"].ToDecimal();
                }
                if (decHalfdayLeaveCount > 0)
                    decHalfdayLeaveCount = decHalfdayLeaveCount * 0.5M;
                decLeaveCount = decFulldayleaveCount + decHalfdayLeaveCount + decLeaveFromLeaveEntry;
                lblLeaveCountDispaly.Text = decLeaveCount.ToString();

                grpDaySummary.Refresh();
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

            }
        }

        private bool SearchEmp()
        {
            try //'Searching Employee On grid
            {
                if (txtSearch.Text != "")
                {
                    if (DgvEmployee.Rows.Count > 0)
                    {

                        if (MintSearchIndex == DgvEmployee.RowCount)
                            MintSearchIndex = 0;
                        MintSearchIndex = MintSearchIndex == 0 ? 0 : MintSearchIndex;
                        for (int i = MintSearchIndex; i <= DgvEmployee.Rows.Count - 1; i++)
                        {
                            if (DgvEmployee.Rows[i].Cells[3].Value.ToString().Trim().ToLower().StartsWith(txtSearch.Text.Trim().ToLower()))
                            {
                                DgvEmployee.CurrentCell = DgvEmployee[2, i];
                                MintSearchIndex = i + 1;
                                return false;
                            }
                            else if (DgvEmployee.Rows[i].Cells[2].Value.ToString().Trim().ToLower().StartsWith(txtSearch.Text.Trim().ToLower()))
                            {

                                DgvEmployee.CurrentCell = DgvEmployee[2, i];
                                MintSearchIndex = i + 1;
                                return false;
                            }

                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }

            return true;
        }

        private void LoadEmployeeGrid()
        {
            if (MintOnFormLoad)
                return;
            if (Convert.ToInt32(CboCompany.SelectedValue) <= 0)
                return;

            int intCompanyID;

            DataTable dtEmp;
            intCompanyID = Convert.ToInt32(CboCompany.SelectedValue);

            string strEmpFilterType = CboEmpFilterType.Text;
            string strEmpWorkstatus = cboEmpWorkstatus.Text;
            int intEmpFilter = 0;

            int intEmpFilterTypeValue = Convert.ToInt32(CboEmpFilterType.SelectedValue);
            int intWorkStatusID = Convert.ToInt32(cboEmpWorkstatus.SelectedValue);

            if (CboEmpFilterType.Text != "All")
            {
                if (CboEmpFilter.SelectedIndex == 1)
                {
                    intEmpFilter = 1;
                }
                else if (CboEmpFilter.SelectedIndex == 2)
                {
                    intEmpFilter = 2;
                }
                else if (CboEmpFilter.SelectedIndex == 3)
                {
                    intEmpFilter = 3;

                }

            }
            dtEmp = MobjclsBLLAttendance.LoadEmployeeGrid(strEmpFilterType, strEmpWorkstatus, intEmpFilter, intCompanyID, intEmpFilterTypeValue, intWorkStatusID);
            DgvEmployee.ColumnHeadersVisible = false;
            DgvEmployee.DataSource = null;
            if (dtEmp != null && dtEmp.Rows.Count > 0)
            {
                DgvEmployee.ColumnHeadersVisible = true;
                DgvEmployee.DataSource = dtEmp;

                DataTable dt1 = new DataTable();
                DataTable dt2 = new DataTable();
                DataView dv = new DataView(dtEmp);
                dt2 = dv.ToTable(true, "Name");
                dt1 = dv.ToTable(true, "Number");
                dt1.Columns["Number"].ColumnName = "Name";
                dt2.Merge(dt1);
                this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
                this.txtSearch.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt2);

                DgvEmployee.Columns[1].Visible = false;
                DataGridViewCellStyle STYLE = new DataGridViewCellStyle();
                STYLE.Font = new Font(DgvEmployee.Font, FontStyle.Bold);
                DgvEmployee.Columns[2].DefaultCellStyle = STYLE;
                DgvEmployee.Columns[3].Width = this.Width - 250;
                this.DgvEmployee.Columns[4].Visible = false;
                this.DgvEmployee.Columns[5].Visible = false;
                DgvEmployee.ColumnHeadersVisible = true;
                DgvEmployee.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
                DgvEmployee.ScrollBars = ScrollBars.Vertical;

                for (int i = 0; i <= DgvEmployee.Rows.Count - 1; i++)
                {
                    if (DgvEmployee.Rows[i].Cells["Gender"].Value.ToString().ToUpper()  == "Male".ToUpper() )// 'Male
                    {

                        Bitmap b = (Bitmap)ImageListEmployee.Images[0];
                        if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 1)// 'Absconding
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[0];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 2)// 'Expired
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[1];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 3)// 'Retired
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[5];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 4)// 'Resigned
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[4];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 5)// 'Terminated
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[6];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 6)// 'In service
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[2];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 7)//'Probation
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[3];
                        else
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[15];

                    }
                    else if (DgvEmployee.Rows[i].Cells["Gender"].Value.ToString().ToUpper() != "Male".ToUpper()) //'Female
                    {
                        if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 1) //Then 'Absconding
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[7];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 2)//Then 'Expired
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[8];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 3) //Then 'Retired
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[12];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 4) //Then 'Resigned
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[11];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 5)// Then 'Terminated
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[13];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 6)//Then 'In service
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[9];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 7)// Then Probation
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[10];
                        else
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[15];

                    }
                }//For
                DgvEmployee.Update();
            }


        }
        private bool ConvertLiveData(DataTable dtFetchData, string sdateFormat, bool bdatetime, int iTemplateMasterId)//To read punch data direct from table log
        {
            //Sorting And HANDLING Night Shift
            bool blnRetValue = false;
            try
            {
                var drID = (from r in dtFetchData.AsEnumerable()
                            select r["EmployeeID"]).Distinct();
                var drDate = (from r in dtFetchData.AsEnumerable()
                              orderby r["AtnDate"] ascending
                              select r["AtnDate"].ToDateTime().ToString("dd MMM yyyy")).Distinct();
                int gCount = 0;

                lblAttendnaceStatus.Text = "";
                lblPrgressPercentage.Text = "";
                lblPrgressPercentage.Refresh();
                DgvAttendance.RowCount = 0;
                barProgressBarAttendance.Minimum = 0;
                barProgressBarAttendance.Value = 0;
                barProgressBarAttendance.Maximum = drID.Count() + 1;

                int iProCount = 0;
                barProgressBarAttendance.Visible = true;
                barProgressBarAttendance.Refresh();
                lblProgressShow.Visible = true;
                lblPrgressPercentage.Visible = true;
                lblPrgressPercentage.BringToFront();
                lblProgressShow.BringToFront();
                lblProgressShow.Refresh();
                barStatusBottom.Refresh();
                Application.DoEvents();
                foreach (Int64 ID in drID)
                {

                    foreach (string dteDate in drDate)
                    {

                        int iEmpId = 0;
                        int iShiftId = 0;
                        int iLate = 0;
                        int iEarly = 0;
                        int ishiftType = 0;
                        int iCmpId = 0;
                        int iBuffer = 0;
                        int iNoOfTimings = 0;
                        string sFromTime = "";
                        string sToTime = "";
                        string sDuration = "";
                        string sAllowedBreakTime = "0";
                        string sEmpName = "";
                        string sShiftName = "";
                        string sMinWorkHours = "0";//

                        if (GetEmployeeInfo(Convert.ToString(ID).Trim(), Convert.ToDateTime(dteDate), ref iEmpId, ref sEmpName, ref iShiftId, ref sShiftName,
                                           ref sFromTime, ref  sToTime, ref  sDuration, ref ishiftType, ref  sAllowedBreakTime, ref  iLate, ref iEarly, ref iCmpId, iTemplateMasterId,
                                           ref  iNoOfTimings, ref  sMinWorkHours, ref iBuffer))
                        {


                            int iTempEmpId = 0;
                            int iTempShiftId = 0;
                            int iTempLate = 0;
                            int iTempEarly = 0;
                            int iTempCmpId = 0;
                            int iTempBuffer = 0;
                            int iTempNoOfTimings = 0;
                            string sTempFromTime = "";
                            string sTempToTime = "";
                            string sTempDuration = "";
                            string sTempAllowedBreakTime = "0";
                            string sTempEmpName = "";
                            string sTempShiftName = "";
                            string sTempMinWorkHours = "0";
                            string sFilterExpression = "";
                            DataRow[] DataRow;
                            if (iBuffer > 0)
                            {
                                string sTemp1FromTime = dteDate;
                                string sTemp2FromTime = "";

                                if (GetEmployeeInfo(Convert.ToString(ID).Trim(), Convert.ToDateTime(dteDate).AddDays(1), ref iTempEmpId, ref sTempEmpName, ref iTempShiftId, ref sTempShiftName, ref sTempFromTime,
                                    ref sTempToTime, ref sTempDuration, ref iTempShiftId, ref sTempAllowedBreakTime, ref iTempLate, ref iTempEarly, ref iTempCmpId, iTemplateMasterId, ref iTempNoOfTimings,
                                    ref sTempMinWorkHours, ref iTempBuffer))
                                {
                                    sTemp2FromTime = Convert.ToDateTime(Convert.ToDateTime(dteDate).AddDays(1).ToString("dd MMM yyyy") + " " + sTempFromTime).ToString("dd MMM yyyy HH:mm tt");

                                }
                                else
                                {
                                    sTemp2FromTime = Convert.ToDateTime(Convert.ToDateTime(dteDate).AddDays(1).ToString("dd MMM yyyy") + " " + sFromTime).ToString(" dd MMM yyyy HH:mm tt");

                                }
                                sTemp1FromTime = Convert.ToDateTime(sTemp1FromTime).AddMinutes(-iBuffer).ToString();
                                sTemp2FromTime = Convert.ToDateTime(sTemp2FromTime).AddMinutes(-iBuffer).ToString();
                                sFilterExpression = "EmployeeID='" + Convert.ToString(ID).Trim() + "' and AtnDate>='" + sTemp1FromTime + "' and AtnDate<'" + sTemp2FromTime + "'";
                                DataRow = dtFetchData.Select(sFilterExpression);

                            }
                            else
                            {
                                sFilterExpression = "EmployeeID='" + ID + "' and AtnDate >='" + Convert.ToDateTime(dteDate).ToString("dd MMM yyyy") + "' and AtnDate <='" + Convert.ToDateTime(dteDate).AddDays(1).ToString("dd MMM yyyy") + "'";
                                dtFetchData.DefaultView.Sort = "AtnDate";
                                dtFetchData.DefaultView.RowFilter = sFilterExpression;
                                DataRow = dtFetchData.DefaultView.Table.Select(sFilterExpression);

                            }
                            if (DataRow.Count() > 0)
                            {

                                DgvAttendance.RowCount += 1;
                                gCount = DgvAttendance.RowCount - 1;
                                DgvAttendance.Rows[gCount].Cells["ColDgvStatus"].Value = Convert.ToInt32(AttendanceStatus.Present);
                                DgvAttendance.Rows[gCount].Cells["ColDgvAtnDate"].Value = dteDate.ToString();
                                DgvAttendance.Rows[gCount].Cells["ColDgvDay"].Value = Convert.ToDateTime(dteDate).DayOfWeek;

                                DgvAttendance.Rows[gCount].Cells["ColDgvEmployee"].Tag = iEmpId;
                                DgvAttendance.Rows[gCount].Cells["ColDgvEmployee"].Value = sEmpName;
                                DgvAttendance.Rows[gCount].Cells["ColDgvCompanyID"].Value = iCmpId;
                                DgvAttendance.Rows[gCount].Cells["ColDgvShift"].Tag = iShiftId;
                                DgvAttendance.Rows[gCount].Cells["ColDgvShift"].Value = sShiftName;
                                DgvAttendance.Rows[gCount].Cells["ColDgvPunchID"].Value = ID.ToString();//Workid Saved for deleting handpunch logs
                                if (Glbln24HrFormat == false)
                                {
                                    sFromTime = string.Format("{0:hh:mm tt}", Convert.ToDateTime(sFromTime));
                                    sToTime = string.Format("{0:hh:mm tt}", Convert.ToDateTime(sToTime));
                                    DgvAttendance.Rows[gCount].Cells["ColDgvShiftTimeDisplay"].Value = Convert.ToString(sFromTime + " To " + sToTime + "(Duration:" + sDuration + " )");
                                }
                                else
                                {
                                    DgvAttendance.Rows[gCount].Cells["ColDgvShiftTimeDisplay"].Value = Convert.ToString(sFromTime + " To " + sToTime + "(Duration:" + sDuration + " )");
                                }

                                sAllowedBreakTime = Convert.ToString(sAllowedBreakTime == "" ? 0 : Convert.ToDouble(sAllowedBreakTime));

                                DgvAttendance.Rows[gCount].Cells["ColDgvAllowedBreakTime"].Tag = sAllowedBreakTime == "" ? 0 : Convert.ToDouble(sAllowedBreakTime);

                                sAllowedBreakTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAllowedBreakTime), DateTime.Today));
                                sAllowedBreakTime = Convert.ToDateTime(sAllowedBreakTime).ToString("HH:mm:ss");
                                DgvAttendance.Rows[gCount].Cells["ColDgvAllowedBreakTime"].Value = sAllowedBreakTime;

                                int intStart = DgvAttendance.Columns["ColDgvAddMore"].Index - 1;

                                for (int i = intStart; i <= DataRow.Count() + 1; i += 2)
                                {
                                    int intLoc = i;
                                    int intCnt = i / 2;
                                    DataGridViewTextBoxColumn adcolIn = new DataGridViewTextBoxColumn();
                                    adcolIn.Name = "TimeIn" + intCnt.ToString();
                                    adcolIn.HeaderText = "TimeIn" + intCnt.ToString();
                                    adcolIn.SortMode = DataGridViewColumnSortMode.NotSortable;
                                    adcolIn.Resizable = DataGridViewTriState.False;
                                    adcolIn.Width = 70;
                                    adcolIn.MaxInputLength = 9;

                                    DataGridViewTextBoxColumn adcolout = new DataGridViewTextBoxColumn();
                                    adcolout.Name = "TimeOut" + intCnt.ToString();
                                    adcolout.HeaderText = "TimeOut" + intCnt.ToString();
                                    adcolout.SortMode = DataGridViewColumnSortMode.NotSortable;
                                    adcolout.Resizable = DataGridViewTriState.False;
                                    adcolout.Width = 70;
                                    adcolout.MaxInputLength = 9;

                                    DgvAttendance.Columns.Insert(intLoc + 1, adcolIn);
                                    DgvAttendance.Columns.Insert(intLoc + 2, adcolout);
                                    MintTimingsColumnCnt += 2;

                                }

                                WorkTimeCalculationLive(DataRow, sFromTime, sdateFormat, bdatetime, iLate, sToTime, gCount, ishiftType, iNoOfTimings, sMinWorkHours, sDuration, iEarly);

                                DgvAttendance.CurrentCell = DgvAttendance[ColDgvAtnDate.Index, gCount];
                                DgvAttendance.Refresh();
                                blnRetValue = true;
                            }//data row
                        }//get employeeinfo



                    }//for date

                    lblPrgressPercentage.Text = iProCount.ToString() + " Records processed";
                    iProCount += 1;
                    lblPrgressPercentage.Refresh();
                    barProgressBarAttendance.Value += 1;
                    barProgressBarAttendance.Visible = true;
                    barProgressBarAttendance.Refresh();

                }//for id 

                barProgressBarAttendance.Maximum = drID.Count() + 1;
                barProgressBarAttendance.Value = 0;

                lblProgressShow.Visible = false;
                lblPrgressPercentage.Visible = false;
                barProgressBarAttendance.Visible = false;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

                MobjClsLogs.WriteLog("ConvertFetchToValidData--" + Ex.ToString(), 2);
                blnRetValue = false;
            }
            return blnRetValue;
        }

        #endregion METHODS

        #region CUSTOM EVENTS

        private void DevicesSubItems_Click(Object sender, EventArgs e)
        {
            //Device Item Click
            MintEmployeeID = 0;
            MstrEmployeeName = "";
            lblEmployeeSelectedValue.Text = MstrEmployeeName;

            expandableSplitterLeft.Expanded = false;
            ExpandablepnlSearch.Expanded = false;
            expandableSplitterLeft.Enabled = false;
            ExpandablepnlSearch.Enabled = false;
            MintDisplayMode = Convert.ToInt16(AttendanceMode.AttendanceAuto);
            DevComponents.DotNetBar.ButtonItem btnDevice = (DevComponents.DotNetBar.ButtonItem)sender;
            MintDeviceID = Convert.ToInt32(btnDevice.Tag);
            if (MblnIsConnected)
                Disconnect();

            DgvPunchingData.Rows.Clear();
            DgvPunchingData.Visible = true;
            DgvPunchingData.ColumnHeadersVisible = false;
            DgvAttendance.Visible = false;
            DgvPunchingData.RowCount = 0;

            if (Connect(MintDeviceID))
            {

                if (ViewLogs())
                {

                }

                Disconnect();

            }
            expandableSplitterLeft.Enabled = true;
            ExpandablepnlSearch.Enabled = true;


        }

        private void TemplateSubItems_Click(Object sender, EventArgs e)
        {
            //Template Item Click
            MintEmployeeID = 0;
            MstrEmployeeName = "";
            lblEmployeeSelectedValue.Text = MstrEmployeeName;

            expandableSplitterLeft.Expanded = false;
            ExpandablepnlSearch.Expanded = false;
            expandableSplitterLeft.Enabled = false;
            ExpandablepnlSearch.Enabled = false;
            MintDisplayMode = Convert.ToInt16(AttendanceMode.AttendanceAuto);
            DevComponents.DotNetBar.ButtonItem btnTemplateItem = (DevComponents.DotNetBar.ButtonItem)sender;
            MintTemplateID = Convert.ToInt32(btnTemplateItem.Tag);
            MintTemplateFileType = MobjclsBLLAttendance.GetTemplateFileType(MintTemplateID);
            DgvPunchingData.Visible = false;
            DgvPunchingData.ColumnHeadersVisible = false;
            DgvAttendance.Visible = true;
            if (DgvAttendance.Rows.Count > 0 && MintDisplayMode == Convert.ToInt32(AttendanceMode.AttendanceAuto))
            {
                //Fetched data already exists. Do you wish to continue?
                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1572, out  MmessageIcon);
                if (MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    DgvAttendance.DataSource = null;
                    DgvAttendance.Rows.Clear();
                    ClearAllControls();
                }
                else
                {
                    this.Cursor = Cursors.Default;
                    expandableSplitterLeft.Enabled = true;
                    ExpandablepnlSearch.Enabled = true;
                    return;
                }
            }
            else
            {
                DgvAttendance.DataSource = null;
                DgvAttendance.Rows.Clear();
                ClearAllControls();
            }
            string sFileName = "";
            if (MintTemplateFileType <= 3)
            {
                using (OpenFileDialog dlgbox = new OpenFileDialog())
                {

                    switch (MintTemplateFileType)
                    {
                        case 1:
                            dlgbox.Filter = "CSV Files (*.csv)|*.csv| Supported Files (*.csv)|*.csv";
                            break;
                        case 2:
                            dlgbox.Filter = " Excel File (*.xls;*.xlsx)|*.xls;*.xlsx|  Supported Files (*.xls;*.xlsx;)|*.xls;*.xlsx;";
                            break;
                        case 3:
                            dlgbox.Filter = "Access File (*.accdb;*.mdb)|*.accdb;*.mdb| Supported Files (*.accdb;*.mdb)|*.accdb;*.mdb";
                            break;

                    }

                    if (dlgbox.ShowDialog() == DialogResult.OK)
                    {
                        sFileName = dlgbox.FileName;
                    }
                    else
                    {
                        expandableSplitterLeft.Enabled = true;
                        ExpandablepnlSearch.Enabled = true;
                        return;
                    }

                }
            }

            if (sFileName != "")
                FetchData(sFileName);


            expandableSplitterLeft.Enabled = true;
            ExpandablepnlSearch.Enabled = true;
        }

        private void txtInt_KeyPress(object sender, KeyPressEventArgs e)
       {
            string strInvalidChars = " bcdefghijklnoqrstuvwxyz~!@#$%^&*()_+|}{?></,`-=\\[]";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
                return;
            }
            string strValidChars = " amp.:";
            if ((strValidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 && ((TextBox)sender).Text.ToLower().Contains(e.KeyChar.ToString())))
            {
                e.Handled = true;
                return;
            }
            if ((strValidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 && ((TextBox)sender).Text.ToLower().Equals("") && ((TextBox)sender).SelectedText.Equals("")))
            {
                e.Handled = true;
                return;
            }
            if ((strValidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 && ((TextBox)sender).Text.Length == ((TextBox)sender).SelectedText.Length))
            {
                e.Handled = true;
                return;
            }
            if ((e.KeyChar.ToString().ToLower() == "p" && ((TextBox)sender).Text.ToLower().Contains('a')))
            {
                e.Handled = true;
                return;
            }
            if ((e.KeyChar.ToString().ToLower() == "a" && ((TextBox)sender).Text.ToLower().Contains('p')))
            {
                e.Handled = true;
                return;
            }
        }

        #endregion CUSTOM EVENTS

        #region CONTROL EVENTS

        private void FrmAttendance_Load(object sender, EventArgs e)
        {
            LoadMessage();
            lblEarlyGoing.Visible = lblLateComing.Visible = false;
        }

        private void FrmAttendance_Shown(object sender, EventArgs e)
        {
            LoadCombo(0);
            LoadForm();
        }
        private void FrmAttendance_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (MblnFetchData)
                e.Cancel = true;
            else
                e.Cancel = false;
        }
        private void DgvPunchingData_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                return;
            }
            catch (Exception)
            {
                return;
            }
        }

        private void DgvAttendance_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                int iRowIndex;
                int iColIndex;
                DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);
                iRowIndex = e.RowIndex;
                iColIndex = e.ColumnIndex;

                if (MintDisplayMode == Convert.ToInt32(AttendanceMode.AttendanceAuto))
                {
                    for (int j = 3; j <= iColIndex - 1; j++)
                    {
                        if (Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells[j].Value) == "" || DgvAttendance.Rows[iRowIndex].Cells[j].Value == null)
                        {
                            //MsMessCommon = New ClsNotification().GetErrorMessage(MaMessageArr, 3057)
                            //MessageBox.Show(MsMessCommon.Replace("#", "").Trim & " " & DgvAttendanceFetch.Columns(j).HeaderText.ToString, GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            DgvAttendance.CurrentCell = DgvAttendance[j, iRowIndex];
                            e.Cancel = true;
                            return;
                        }
                    }
                }
                else
                {
                    if (e.ColumnIndex > 0 && e.RowIndex > -1)
                    {
                        if (e.ColumnIndex >= 2)// ' Editing Disabling for salary released attendance
                        {
                            int iEmpID = 0;
                            iEmpID = Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvEmployee"].Tag);
                            int iCmpID = Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvCompanyID"].Value);
                            string CurDate = DgvAttendance.Rows[e.RowIndex].Cells["ColDgvAtnDate"].Value.ToString();
                            if (MobjclsBLLAttendance.IsSalaryReleased(iEmpID, iCmpID, Convert.ToDateTime(CurDate)))
                            {
                                //'MsMessCommon = "Cannot update/delete employee attendance. Salary released for this Date  " & CurDate & "" ', GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information
                                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1573, out MmessageIcon);
                                MstrMessCommon = MstrMessCommon.Replace("save", "update");
                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                tmrClearlabels.Enabled = true;
                                e.Cancel = true;
                                return;
                            }


                            if (e.ColumnIndex == 3)//Then ' Editing Disabling for if date is less than date of joing of employee
                            {
                                string sdtDates = Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvAtnDate"].Value);
                                DateTime dJoiningDate = Convert.ToDateTime(MobjclsBLLAttendance.GetJoiningDate(Convert.ToInt32(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvEmployee"].Tag)));
                                string sDate = dJoiningDate.ToString("dd MMM yyyy");
                                if (Convert.ToDateTime(sdtDates).Date < Convert.ToDateTime(sDate).Date)
                                {
                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1560, out MmessageIcon);
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    tmrClearlabels.Enabled = true;
                                    e.Cancel = true;
                                    return;
                                }
                                //'Cheking With Current Date
                                string sNow = DateTime.Now.Date.ToString("dd MMM yyyy");
                                if (Convert.ToDateTime(sdtDates).Date > Convert.ToDateTime(sNow).Date)
                                {
                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 3064, out MmessageIcon);
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    tmrClearlabels.Enabled = true;
                                    e.Cancel = true;
                                    return;
                                }
                            }
                            if (DgvAttendance.Columns["TimeIn1"].Index == e.ColumnIndex)
                            {
                                if (Convert.ToString(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value) == "" || Convert.ToString(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value) == null)
                                {
                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1589, out MmessageIcon); //"Please Enter Status";
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    tmrClearlabels.Enabled = true;
                                    e.Cancel = true;
                                    return;
                                }
                                else
                                {
                                    if (Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Rest) || Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Absent))
                                    {
                                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1590, out MmessageIcon);//"Please Change Status";
                                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                        tmrClearlabels.Enabled = true;
                                        e.Cancel = true;
                                        return;
                                    }
                                    if (Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Leave))
                                    {
                                        if (Convert.ToString(DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value) == "0")
                                        {
                                            MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1590, out MmessageIcon);
                                            MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                            tmrClearlabels.Enabled = true;
                                            e.Cancel = true;
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                        if (e.ColumnIndex >= 3 && e.ColumnIndex <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1)
                        {
                            if (MbAutoFill == true)
                            {
                                MstrCellTime = Convert.ToString(DgvAttendance.CurrentCell.Value).Trim();
                            }
                        }
                        if (e.RowIndex >= 0)
                        {
                            if (e.ColumnIndex == 3)
                            {
                                if (DgvAttendance.Rows[iRowIndex].Cells["ColDgvEmployee"].Tag == DBNull.Value || DgvAttendance.Rows[iRowIndex].Cells["ColDgvEmployee"].Tag == null)
                                {
                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 15, out MmessageIcon);
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    if (rbtnMonthly.Checked)
                                        DgvAttendance.CurrentCell = DgvAttendance[ColDgvAtnDate.Index, iRowIndex];
                                    else
                                        DgvAttendance.CurrentCell = DgvAttendance[ColDgvEmployee.Index, iRowIndex];
                                    tmrClearlabels.Enabled = true;
                                    e.Cancel = true;
                                    return;
                                }
                            }
                        }
                        int iType = 0;

                        if (CheckIfValidDate(e.RowIndex, ref iType) == false)
                        {
                            if (MintRowIndexformessge != DgvAttendance.CurrentRow.Index)
                            {
                                if (iType == 1)// 'leave
                                {
                                    if (DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveId"].Value.ToInt32() == 0)
                                    {
                                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1586, out MmessageIcon);
                                        MstrMessCommon = MstrMessCommon + " " + DgvAttendance.Rows[iRowIndex].Cells["ColDgvAtnDate"].Value.ToString(); //objNotification.GetErrorMessage(MaMessageArr, 1586) & " " & AttendanceDataGridView.Rows(iRowIndex).Cells("AtnDate").Value
                                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                        tmrClearlabels.Enabled = true;
                                        e.Cancel = true;
                                        return;
                                    }
                                }
                                else if (iType == 2)//Then '' offday witout shift
                                {
                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1587, out MmessageIcon);//.Replace("#", "").Trim();
                                    MstrMessCommon = MstrMessCommon + "  " + DgvAttendance.Rows[iRowIndex].Cells["ColDgvAtnDate"].Value.ToString();//objNotification.GetErrorMessage(MaMessageArr, 1587) & " " & AttendanceDataGridView.Rows(iRowIndex).Cells("AtnDate").Value

                                    //MstrMessCommon = "No shift is assigned on " + DgvAttendance.Rows[iRowIndex].Cells["ColDgvAtnDate"].Value.ToString();//objNotification.GetErrorMessage(MaMessageArr, 1587) & " " & AttendanceDataGridView.Rows(iRowIndex).Cells("AtnDate").Value
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    lblAttendnaceStatus.Enabled = true;
                                    e.Cancel = true;
                                    return;
                                }
                                else if (iType == 3)//Then 'off day
                                {
                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1559, out MmessageIcon);


                                    // MstrMessCommon = "This date is set as an off day. ";//objNotification.GetErrorMessage(MaMessageArr, 1559)
                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    tmrClearlabels.Enabled = true;
                                }
                                else if (iType == 4)//Then 'Holiday
                                {
                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1555, out MmessageIcon);

                                    //MstrMessCommon = "This date recorded is set as a Holiday";//.objNotification.GetErrorMessage(MaMessageArr, 1555)
                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    tmrClearlabels.Enabled = true;
                                }
                            }
                        }
                        for (int j = 3; j <= iColIndex - 1; j++)
                        {
                            if (DgvAttendance.Rows[iRowIndex].Cells[j].Value == null)
                                DgvAttendance.Rows[iRowIndex].Cells[j].Value = "";
                            if ((iColIndex - 1) < ColDgvAddMore.Index)
                            {
                                if (DgvAttendance.Rows[iRowIndex].Cells[j].Value.ToString().Trim() == "")
                                {
                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1603, out MmessageIcon);
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " " + DgvAttendance.Columns[j].HeaderText.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim()+ " " + DgvAttendance.Columns[j].HeaderText.ToString();
                                    tmrClearlabels.Enabled = true;
                                    DgvAttendance.CurrentCell = DgvAttendance[j, iRowIndex];
                                    e.Cancel = true;
                                    return;
                                }
                            }
                        }

                    }
                }

            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + "-Form- " + this.Name.ToString() + " " + Ex.Message.ToString());
            }


        }


        private void DgvAttendance_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex != -1 && e.RowIndex != -1)//  'And IsValidTime(DgvAttendanceFetch.CurrentRow.Cells(e.ColumnIndex).Value) = True
                {
                    ClearBottomPanel();
                    lblWorkTime.Text = DgvAttendance.CurrentRow.Cells["ColDgvWorkTime"].Value.ToStringCustom() == string.Empty ? "00:00:00" : DgvAttendance.CurrentRow.Cells["ColDgvWorkTime"].Value.ToStringCustom();
                    lblBreakTime.Text = Convert.ToString(DgvAttendance.CurrentRow.Cells["ColDgvBreakTime"].Value).Trim() == "" ? "00:00:00" : Convert.ToString(DgvAttendance.CurrentRow.Cells["ColDgvBreakTime"].Value);
                    lblAllowedBreakTime.Text = Convert.ToString(DgvAttendance.CurrentRow.Cells["ColDgvAllowedBreakTime"].Value);
                    lblExcessTime.Text = Convert.ToString(DgvAttendance.CurrentRow.Cells["ColDgvExcessTime"].Value).Trim() == "" ? "00:00:00" : Convert.ToString(DgvAttendance.CurrentRow.Cells["ColDgvExcessTime"].Value);
                    //ShiftDesTextBox.Text = Convert.ToString(DgvAttendance.CurrentRow.Cells["Shift"].Value);

                    int iAbsent = DgvAttendance.CurrentRow.Cells["ColDgvAbsentTime"].Value.ToInt32() > 0 ? Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvAbsentTime"].Value) : 0;
                    lblShortageTime.ForeColor = iAbsent > 0 ? Color.DarkRed : System.Drawing.SystemColors.ControlText;
                    string sShortage = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iAbsent), DateTime.Today));
                    lblShortageTime.Text = Convert.ToDateTime(sShortage).ToString("HH:mm:ss");

                    if (Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvEarlyGoing"].Tag) == 1)
                    {//Then 'Earlygoing yes

                        lblEarlyGoing.Visible = true;
                        lblEarlyGoing.Refresh();
                    }
                    else
                    {

                        lblEarlyGoing.Visible = false;
                        lblEarlyGoing.Refresh();
                    }

                    if (Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvLateComing"].Tag) == 1)// Then 'LateComing yes
                    {

                        lblLateComing.Visible = true;
                    }
                    else
                    {

                        lblLateComing.Visible = false;
                    }
                    this.Refresh();
                    ShowConsequenceAndShiftInfo(e.RowIndex);

                    int iStatus = DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value.ToInt32();
                    if (iStatus == (int)AttendanceStatus.Absent || iStatus == 0)
                    {
                        for (int i = 3; i <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1; i++)
                        {
                            DgvAttendance.Rows[e.RowIndex].Cells[i].Value = "";
                        }
                    }

                    if (DgvAttendance.Columns[e.ColumnIndex].Name.IndexOf("ColDgvAddMore") != -1)
                    {
                        if (DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex - 2].Value != null && DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex - 1].Value != null)
                        {
                            if (Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex - 2].Value).Trim() == "" || Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex - 1].Value).Trim() == "")
                                return;
                            else
                                AddNewColumns(2);
                        }
                        else
                        {
                            return;
                        }
                    }
                    else if (DgvAttendance.Columns[e.ColumnIndex].Name.IndexOf("Time") != -1 || DgvAttendance.Columns[e.ColumnIndex].Name.IndexOf("Employee") != -1)
                    {
                        if (DgvAttendance.Rows[e.RowIndex].Cells["TimeIn1"].Value != null && Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells["TimeIn1"].Value) != "" && Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells["TimeOut1"].Value) != "" &&
                             DgvAttendance.Rows[e.RowIndex].DefaultCellStyle.ForeColor != Color.IndianRed)
                            WorkTimeCalculation(e.RowIndex, false);

                    }
                    else if (iStatus >= 1 || iStatus <= 3)
                    {
                        if (Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells["TimeIn1"].Value) != "" && Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells["TimeOut1"].Value) != "" &&
                            DgvAttendance.Rows[e.RowIndex].Cells["TimeIn1"].Value != null && DgvAttendance.Rows[e.RowIndex].DefaultCellStyle.ForeColor != Color.IndianRed)
                            WorkTimeCalculation(e.RowIndex, false);
                    }
                    if (DgvAttendance.CurrentRow.Cells["ColDgvLeaveFlag"].Value.ToInt32() == 1 || iStatus == (int)AttendanceStatus.Leave)
                    {
                        if (lblConsequence.Text.Trim() == "")
                        {
                            lblConsequence.Visible = true;
                            lblConsequence.Text = DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value.ToStringCustom();
                        }
                        if (Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells["TimeIn1"].Value).Trim() == "" && Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells["TimeOut1"].Value).Trim() == "")
                        {
                            lblAllowedBreakTime.Text = "00:00:00";
                            lblBreakTime.Text = "00:00:00";
                            lblWorkTime.Text = "00:00:00";
                            lblShortageTime.Text = "00:00:00";
                            lblShortageTime.ForeColor = System.Drawing.SystemColors.ControlText;
                            lblExcessTime.Text = "00:00:00";

                            DgvAttendance.Rows[e.RowIndex].Cells["ColDgvAllowedBreakTime"].Tag = 0;
                            DgvAttendance.Rows[e.RowIndex].Cells["ColDgvAllowedBreakTime"].Value = "";
                            DgvAttendance.Rows[e.RowIndex].Cells["ColDgvBreakTime"].Tag = 0;
                            DgvAttendance.Rows[e.RowIndex].Cells["ColDgvBreakTime"].Value = "";
                            DgvAttendance.Rows[e.RowIndex].Cells["ColDgvWorkTime"].Tag = 0;
                            DgvAttendance.Rows[e.RowIndex].Cells["ColDgvWorkTime"].Value = "";
                            DgvAttendance.Rows[e.RowIndex].Cells["ColDgvAbsentTime"].Tag = 0;
                            DgvAttendance.Rows[e.RowIndex].Cells["ColDgvAbsentTime"].Value = "";
                            DgvAttendance.Rows[e.RowIndex].Cells["ColDgvExcessTime"].Tag = 0;
                            DgvAttendance.Rows[e.RowIndex].Cells["ColDgvExcessTime"].Value = "";

                        }
                    }
                    else if (MobjclsBLLAttendance.CheckHalfdayLeave(DgvAttendance.CurrentRow.Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.CurrentRow.Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.CurrentRow.Cells["ColDgvAtnDate"].Value.ToDateTime()))
                    {
                        if (lblConsequence.Text.Trim() == "")
                        {
                            lblConsequence.Visible = true;
                            lblConsequence.Text = DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value.ToStringCustom();
                        }
                    }
                }

            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
            }

        }
        private void DgvAttendance_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= ColDgvStatus.Index && e.ColumnIndex < DgvAttendance.Columns["ColDgvAddMore"].Index && e.RowIndex >= 0)
                {
                    DgvAttendance.EndEdit();//'CellEndEdit For commit the cell value
                    if (DgvAttendance.CurrentCell != null)//
                    {
                        if (DgvAttendance.Rows[e.RowIndex].Cells[ColDgvStatus.Index + 1].Value == DBNull.Value || Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells[ColDgvStatus.Index + 1].Value).Trim().Equals(String.Empty))
                            return;
                    }
                    //'---------------------------------------------------------------------------------
                    int iRowIndex = e.RowIndex;
                    int iColIndex = e.ColumnIndex;
                    int iEmpID = 0;
                    iEmpID = Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvEmployee"].Tag);
                    int iCmpID = Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvCompanyID"].Value);
                    string CurDate = Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvAtnDate"].Value);
                    if (MobjclsBLLAttendance.IsSalaryReleased(iEmpID, iCmpID, Convert.ToDateTime(CurDate)))
                    {
                        //'MsMessCommon = "Cannot update/delete employee attendance. Salary released for this Date  " & CurDate & "" ', GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information
                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1573, out MmessageIcon);
                        MstrMessCommon = MstrMessCommon.Replace("save", "update");
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        tmrClearlabels.Enabled = true;
                        return;
                    }
                    string sdtDates = Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvAtnDate"].Value);
                    string dJoiningDate = MobjclsBLLAttendance.GetJoiningDate(Convert.ToInt32(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvEmployee"].Tag));
                    string sDate = Convert.ToDateTime(dJoiningDate).ToString("dd MMM yyyy");

                    if (Convert.ToDateTime(sdtDates).Date < Convert.ToDateTime(sDate).Date)
                    {
                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1560, out MmessageIcon);
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        tmrClearlabels.Enabled = true;
                        return;
                    }
                    //'Cheking With Current Date
                    string sNow = DateTime.Now.Date.ToString("dd MMM yyyy");
                    if (Convert.ToDateTime(sdtDates).Date > Convert.ToDateTime(sNow).Date)
                    {
                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 3064, out MmessageIcon);
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        tmrClearlabels.Enabled = true;
                        return;
                    }

                    if (Convert.ToString(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value) == "" || Convert.ToString(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value) == null)
                    {
                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1589, out MmessageIcon);
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        tmrClearlabels.Enabled = true;
                        return;
                    }
                    else
                    {
                        if (Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Rest) || Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Absent))
                        {
                            MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1590, out MmessageIcon);
                            MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                            tmrClearlabels.Enabled = true;
                            return;
                        }
                        if (Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Leave))
                        {
                            if (Convert.ToString(DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value) == "0")
                            {
                                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1590, out MmessageIcon);
                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                tmrClearlabels.Enabled = true;

                                return;
                            }
                        }
                    }
                    //'-------------------------------------------------------------------------------------------------------------
                }

                //---------------------------------------------------------------------------------
                ArrayList RowArray = new ArrayList();
                if (e.ColumnIndex >= 3 && e.ColumnIndex < DgvAttendance.Columns["ColDgvAddMore"].Index)
                {
                    DgvAttendance.EndEdit();//'CellEndEdit For commit the cell value
                }
                DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);
                if ((e.ColumnIndex > ColDgvStatus.Index && e.ColumnIndex < ColDgvAddMore.Index) && e.RowIndex >= 0)
                {

                    string[] DetailData = new string[14];
                    DetailData[0] = Convert.ToString(DgvAttendance.Rows[DgvAttendance.CurrentRow.Index].Cells["ColDgvEmployee"].Value);
                    DetailData[1] = Convert.ToString(DgvAttendance.Rows[DgvAttendance.CurrentRow.Index].Cells["ColDgvEmployee"].Tag); //'Employee
                    DetailData[2] = Convert.ToString(DgvAttendance.Rows[DgvAttendance.CurrentRow.Index].Cells["ColDgvCompanyID"].Value);
                    DetailData[3] = Convert.ToString(DgvAttendance.Rows[DgvAttendance.CurrentRow.Index].Cells["ColDgvShift"].Value);
                    DetailData[4] = Convert.ToString(DgvAttendance.Rows[DgvAttendance.CurrentRow.Index].Cells["ColDgvShift"].Tag);
                    DetailData[5] = Convert.ToString(DgvAttendance.Rows[DgvAttendance.CurrentRow.Index].Cells["ColDgvAtnDate"].Value);
                    DetailData[6] = Convert.ToString(DgvAttendance.Rows[DgvAttendance.CurrentRow.Index].Cells["ColDgvBreakTime"].Value);
                    DetailData[7] = Convert.ToString(DgvAttendance.Rows[DgvAttendance.CurrentRow.Index].Cells["ColDgvLateComing"].Value);//'LateComing 1-yes  /0-NO ,chkd=true->1
                    DetailData[8] = Convert.ToString(DgvAttendance.Rows[DgvAttendance.CurrentRow.Index].Cells["ColDgvEarlyGoing"].Value);//' 'LateComing 1-yes  /0-NO
                    DetailData[9] = Convert.ToString(DgvAttendance.Rows[DgvAttendance.CurrentRow.Index].Cells["ColDgvWorkTime"].Value);
                    DetailData[10] = Convert.ToString(DgvAttendance.Rows[DgvAttendance.CurrentRow.Index].Cells["ColDgvAllowedBreakTime"].Value);
                    DetailData[11] = Convert.ToString(DgvAttendance.Rows[DgvAttendance.CurrentRow.Index].Cells["ColDgvExcessTime"].Value);
                    DetailData[12] = Convert.ToString(DgvAttendance.Rows[DgvAttendance.CurrentRow.Index].Cells["ColDgvShift"].Value);//ShiftDesTextBox.Text;
                    DetailData[13] = Convert.ToString(DgvAttendance.Rows[DgvAttendance.CurrentRow.Index].Cells["ColDgvAllowedBreakTime"].Tag);//ShiftDesTextBox.Text;
                    bool bValidData = true;
                    for (int i = 3; i <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1; i++)
                    {

                        if (DgvAttendance.Rows[e.RowIndex].Cells[i].Value == null || Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells[i].Value) == "")
                            break;
                        else
                        {
                            if (IsValidTime(Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells[i].Value)) == true)
                            {
                                RowArray.Add(DgvAttendance.Rows[e.RowIndex].Cells[i].Value);
                            }
                            else
                            {
                                bValidData = false;
                                break;
                            }
                        }

                    }//for 
                    if (bValidData == true)
                    {
                        FrmUpdate UpdateForm = new FrmUpdate(RowArray, DetailData); //'Cunstructor
                        UpdateForm.ShowDialog();
                        if (UpdateForm.PblnOkbutton) //'Clear the previous data from the grid
                        {
                            if (MbAutoFill == true && UpdateForm.PblnChange == true)
                            {
                                MbAutoFill = false; //Auto Fill Data is updated Through upadte form then auto fill is false
                            }

                            for (int iRows = 3; iRows <= DgvAttendance.Columns["ColDgvAddMore"].Index; iRows++)
                            {

                                DgvAttendance.Rows[e.RowIndex].Cells[iRows].Value = "";
                            }
                            UpdatedPunchingData(UpdateForm.punchingCollection, e.RowIndex);
                            DgvAttendance.EndEdit();
                            DgvAttendance.Refresh();
                            WorkTimeCalculation(e.RowIndex, false);
                        }
                        UpdateForm.Dispose();
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
            }
        }
        private void DgvAttendance_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex != -1 && e.RowIndex != -1)
                {

                    //----------CELL Value Change------------------------------------------------------------------------------------------------------------------------
                    if (DgvAttendance.Rows.Count > 0)
                    {
                        if (Microsoft.VisualBasic.Information.IsNumeric(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value))
                            if (Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value) != Convert.ToInt32(AttendanceStatus.Present))
                                MbAutoFill = false;

                        if (e.ColumnIndex == ColDgvStatus.Index && MblnShowData == false)
                        {
                            int iStatus = DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value != DBNull.Value && !Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value).Trim().Equals(String.Empty) && Microsoft.VisualBasic.Information.IsNumeric(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value) == true ? Convert.ToInt32(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value) : 0;
                            if (iStatus == Convert.ToInt32(AttendanceStatus.Absent) || iStatus == Convert.ToInt32(AttendanceStatus.Rest))// Then '--Or iStatus = 5
                            {
                                for (int i = ColDgvStatus.Index + 1; i <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1; i++)
                                {
                                    DgvAttendance.Rows[e.RowIndex].Cells[i].Value = "";
                                }
                                DgvAttendance.Rows[e.RowIndex].Cells["ColDgvWorkTime"].Value = "";
                                DgvAttendance.Rows[e.RowIndex].Cells["ColDgvWorkTime"].Tag = 0;
                                if (Convert.ToInt32(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveId"].Value) <= 0)
                                {
                                    DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveType"].Tag = "";
                                    DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveType"].Value = "";
                                    DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveInfo"].Value = "";
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = "";
                                    DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "";

                                }
                                if (Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvHolidayFlag"].Value) == 1)
                                {
                                    DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = 0;
                                    DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = "";
                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1555, out MmessageIcon);
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    tmrClearlabels.Enabled = true;
                                    DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                                    DgvAttendance.CancelEdit();
                                    return;
                                }
                                if (DgvAttendance.CurrentRow.Cells["ColDgvLeaveFlag"].Value.ToInt32() == 1)
                                {

                                    if (DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveId"].Value.ToInt32() <= 0)
                                    {
                                        DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = 0;
                                        DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = "";
                                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1586, out MmessageIcon);
                                        //MstrMessCommon = MstrMessCommon.Replace("leave on", "Half day leave on this day");
                                        MstrMessCommon = MstrMessCommon.Replace("leave on", "leave on this day");
                                        if (MobjclsBLLAttendance.CheckHalfdayLeave(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.Rows[e.RowIndex].Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.Rows[e.RowIndex].Cells["ColDgvAtnDate"].Value.ToDateTime()))
                                        {
                                            MstrMessCommon = MstrMessCommon.Replace("leave on", "Half day leave on this day");
                                        }
                                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                        tmrClearlabels.Enabled = true;
                                        DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                                        DgvAttendance.CancelEdit();
                                        return;
                                    }
                                }
                                DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "";

                                //if (iStatus == Convert.ToInt32(AttendanceStatus.Absent))// Then '--Or iStatus = 5
                                //{
                                //    DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "Employee is absent from Attendance";
                                //}
                                //if (iStatus == Convert.ToInt32(AttendanceStatus.Rest))// Then '--Or iStatus = 5
                                //{
                                //    DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "Employee is Rest from Attendance";
                                //}

                            }
                            else if (iStatus == Convert.ToInt32(AttendanceStatus.Leave))
                            {
                                if (Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvHolidayFlag"].Value) == 1)
                                {
                                    DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = 0;
                                    DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = "";
                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1555, out MmessageIcon);
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    tmrClearlabels.Enabled = true;
                                    DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                                    DgvAttendance.CancelEdit();
                                    return;
                                }

                                if (DgvAttendance.CurrentRow.Cells["ColDgvLeaveFlag"].Value.ToInt32() == 1)
                                {
                                    int iLeaveType = MobjclsBLLAttendance.CheckHalfdayLeaveNew(DgvAttendance.CurrentRow.Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.CurrentRow.Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.CurrentRow.Cells["ColDgvAtnDate"].Value.ToDateTime());

                                    if (iLeaveType > 0 && (DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveId"].Value.ToInt32() <= 0))
                                    {
                                        DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Value = iLeaveType;
                                        DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value = 1;
                                        DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Tag = iLeaveType;
                                        return;
                                    }
                                    else
                                    {
                                        if (DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveId"].Value.ToInt32() <= 0)
                                            return;
                                    }
                                }

                                int iEmpId = Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvEmployee"].Tag);
                                int iCmpID = Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvCompanyID"].Value);
                                string StrEmp = Convert.ToString(DgvAttendance.CurrentRow.Cells["ColDgvEmployee"].Value);
                                string strDate = Convert.ToString(DgvAttendance.CurrentRow.Cells["ColDgvAtnDate"].Value);
                                DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Value = "";
                                DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Tag = "";
                                DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value = "";
                                DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = "";

                                DgvAttendance.Rows[e.RowIndex].Cells["ColDgvWorkTime"].Value = "";
                                DgvAttendance.Rows[e.RowIndex].Cells["ColDgvWorkTime"].Tag = 0;
                                FrmLeaveFromAttendance objFrmLeaveFromAttendance = new FrmLeaveFromAttendance(iEmpId, StrEmp, iCmpID, strDate);

                                objFrmLeaveFromAttendance.ShowDialog();
                                if (objFrmLeaveFromAttendance.PblnBtnOk)
                                {
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Value = objFrmLeaveFromAttendance.PintLeaveType;
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value = objFrmLeaveFromAttendance.PblnHalfday ? 1 : 0; //'Contains Haldfday or fullday
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = objFrmLeaveFromAttendance.PblnPaid ? 0 : 1; //'paid or unpaid
                                    DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = objFrmLeaveFromAttendance.PstrRemarks;

                                    GetEmployeeLeaveStatus(iEmpId, iCmpID, Convert.ToDateTime(strDate), objFrmLeaveFromAttendance.PintLeaveType);
                                    int iType = 0;
                                    if (!CheckingYearlyLeaves(DgvAttendance.CurrentRow.Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.CurrentRow.Cells["ColDgvEmployee"].Tag.ToInt32(), objFrmLeaveFromAttendance.PintLeaveType, DgvAttendance.CurrentRow.Cells["ColDgvAtnDate"].Value.ToDateTime()))
                                    {
                                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1588, out MmessageIcon);
                                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                        tmrClearlabels.Enabled = true;
                                        DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                                        DgvAttendance.CancelEdit();
                                        DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value = (int)AttendanceStatus.Absent;
                                        DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Value = "";
                                        DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value = "";
                                        DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = "";
                                        DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "";
                                    }
                                    else
                                    {
                                        CheckingLeaves(objFrmLeaveFromAttendance.PintLeaveType, ref  iType);
                                        if (iType == 1)
                                        {
                                            //'half day only
                                            if (objFrmLeaveFromAttendance.PblnHalfday == false)
                                            {
                                                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1588, out MmessageIcon);
                                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                                tmrClearlabels.Enabled = true;
                                                DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                                                DgvAttendance.CancelEdit();
                                                DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value = (int)AttendanceStatus.Absent;
                                                DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Value = "";
                                                DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value = "";
                                                DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = "";
                                                DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "";
                                            }
                                        }
                                        else if (iType == 2)
                                        { 
                                            //code later
                                        }
                                        else
                                        {
                                            MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1588, out MmessageIcon);
                                            MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                            tmrClearlabels.Enabled = true;
                                            DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                                            DgvAttendance.CancelEdit();
                                            DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value = (int)AttendanceStatus.Absent;
                                            DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Value = "";
                                            DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value = "";
                                            DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = "";
                                            DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "";
                                        }
                                    }

                                    if (objFrmLeaveFromAttendance.PblnHalfday)
                                    {
                                        WorkTimeCalculation(e.RowIndex, false);
                                    }
                                    else
                                    {

                                        for (int i = ColDgvStatus.Index + 1; i <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1; i++)
                                        {
                                            DgvAttendance.Rows[e.RowIndex].Cells[i].Value = "";
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = ColDgvStatus.Index + 1; i <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1; i++)
                                    {
                                        DgvAttendance.Rows[e.RowIndex].Cells[i].Value = "";
                                    }
                                    DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value = (int)AttendanceStatus.Absent;
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Value = "";
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Tag = "";
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value = "";
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = "";
                                }

                            }
                            else
                            {
                                if (Convert.ToInt32(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveId"].Value) <= 0)
                                {
                                    DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveType"].Tag = "";
                                    DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveType"].Value = "";
                                    DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveInfo"].Value = "";
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = "";

                                }
                                DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "";

                                if (iStatus == (int)AttendanceStatus.Present && DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveFlag"].Value.ToInt32() == 1)
                                {

                                    if (DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveId"].Value.ToInt32() <= 0)
                                    {
                                        DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = 0;
                                        DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = "";
                                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1586, out MmessageIcon);
                                        MstrMessCommon = MstrMessCommon.Replace("leave on", "leave on this day");
                                        if (MobjclsBLLAttendance.CheckHalfdayLeave(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.Rows[e.RowIndex].Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.Rows[e.RowIndex].Cells["ColDgvAtnDate"].Value.ToDateTime()))
                                        {
                                            MstrMessCommon = MstrMessCommon.Replace("leave on", "Half day leave on this day");
                                        }
                                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                        tmrClearlabels.Enabled = true;
                                        DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                                        DgvAttendance.CancelEdit();
                                    }
                                }
                            }
                        }

                    }//Dgv.row.count>0

                    if (DgvAttendance.IsCurrentCellDirty)
                    {
                        if (DgvAttendance.CurrentCell != null)
                        {
                            DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);
                        }
                    }
                    if (e.ColumnIndex == ColDgvStatus.Index)
                    {
                        if (DgvAttendance.CurrentCell.Value != DBNull.Value && !Convert.ToString(DgvAttendance.CurrentCell.Value).Trim().Equals(String.Empty))
                        {
                            if (!Microsoft.VisualBasic.Information.IsNumeric(DgvAttendance.CurrentCell.Value))
                            {
                                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1594, out MmessageIcon);
                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                DgvAttendance.CurrentCell = DgvAttendance[ColDgvStatus.Index, e.RowIndex];
                                tmrClearlabels.Enabled = true;
                                return;
                            }
                        }
                    }

                    if (DgvAttendance.CurrentRow.Cells["TimeIn1"].Value.ToStringCustom() != string.Empty)
                    {
                        FormatGrid(e.RowIndex);
                    }
                    else
                    {
                        if (Microsoft.VisualBasic.Information.IsNumeric(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value))
                        {
                            if (Convert.ToInt32(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value) != Convert.ToInt32(AttendanceStatus.Present))
                            {
                                if (DgvAttendance.Rows[e.RowIndex].DefaultCellStyle.ForeColor != Color.Red)
                                {
                                    DgvAttendance.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Black;
                                }
                            }
                        }

                    }

                    if (e.ColumnIndex >= 3 && e.ColumnIndex <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1)
                    {
                        if (MbAutoFill == true)
                        {
                            if (Convert.ToString(DgvAttendance.CurrentRow.Cells[e.ColumnIndex].Value).Trim() != MstrCellTime)
                            {
                                MbAutoFill = false;
                            }
                            if (Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value) != 1)
                                MbAutoFill = false;
                        }
                    }
                    MintRowIndexformessge = e.RowIndex;

                    if (e.ColumnIndex >= 1)
                    {
                        if (DgvAttendance.Rows[e.RowIndex].Cells["TimeIn1"].Value.ToStringCustom() != string.Empty && DgvAttendance.Rows[e.RowIndex].Cells["TimeOut1"].Value.ToStringCustom() != string.Empty &&
                                     DgvAttendance.Rows[e.RowIndex].DefaultCellStyle.ForeColor != Color.IndianRed)
                            WorkTimeCalculation(e.RowIndex, false);

                    }

                }

            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
            }

        }

        private void DgvAttendance_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                string date2 = "";
                DateTime ConvertTime;
                if (e.RowIndex != -1)
                {
                    if (e.ColumnIndex > ColDgvStatus.Index && e.ColumnIndex <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1)
                    {
                        if (Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value) == "" || DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null)
                            return;
                        else
                        {

                            if (Glbln24HrFormat == false)
                            {
                                DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = AMPMConvert(Microsoft.VisualBasic.Strings.UCase(DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()));
                            }
                            else
                            {
                                DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = Convert24HrFormat(Microsoft.VisualBasic.Strings.UCase(DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()));

                            }
                            date2 = DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                        }
                        if (IsValidTime(DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()) == false)
                        {
                            MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1556, out  MmessageIcon);
                            if (Glbln24HrFormat == false)
                            {
                                //message
                                string strSecondMsg = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1557, out  MmessageIcon);
                                MstrMessCommon = MstrMessCommon + strSecondMsg.Remove(0, strSecondMsg.IndexOf("#") + 1).Trim();
                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                tmrClearlabels.Enabled = true;

                            }
                            else
                            {

                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                tmrClearlabels.Enabled = true;

                            }
                            DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                            e.Cancel = true;
                            return;
                        }
                        else
                        {
                            ConvertTime = Convert.ToDateTime(date2);
                        }

                        if (IsValidTime(DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()) == true)
                        {
                            string STimeIn = DgvAttendance.CurrentRow.Cells[e.ColumnIndex].Value.ToString();
                            if (Glbln24HrFormat == false)
                            {
                                STimeIn = ConvertDate24New(STimeIn);

                            }
                        }
                        string CurrentDateDisplay = "";
                        string FirstOne = "";
                        MblnCorrectTime = true;

                        if (e.ColumnIndex >= 3 && e.ColumnIndex <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1)
                        {
                            //'CurrentDateDisplay = DTPDate.Value.ToString("dd-MMM-yyyy")
                            CurrentDateDisplay = Microsoft.VisualBasic.DateAndTime.Now.Date.ToString("dd MMM yyyy");
                            if (DgvAttendance.Rows[e.RowIndex].Cells[3].Value != null)
                            {
                                if (Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells[3].Value) != "")
                                {
                                    FirstOne = CurrentDateDisplay.Trim() + " " + DgvAttendance.Rows[e.RowIndex].Cells[3].FormattedValue.ToString();
                                }
                            }

                            string strStart = FirstOne;
                            string strLast = Convert.ToDateTime(strStart).AddDays(1).ToString();
                            string DayChange = "";
                            bool flag = false;
                            string strCurrent = "";
                            string sPreviousTime = "";
                            int iDiff = 0;

                            for (int k = 3; k <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1; k++)
                            {
                                if (Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells[k].Value) == "")
                                {
                                    break;
                                }
                                if (k > 3)
                                {
                                    if (flag == true)
                                    {
                                        strCurrent = DayChange + " " + DgvAttendance.Rows[e.RowIndex].Cells[k].FormattedValue;
                                        iDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(strCurrent));
                                        if (iDiff < 0)
                                        {
                                            strCurrent = Convert.ToDateTime(strCurrent).AddDays(1).ToString();
                                            DayChange = Strings.Format(Convert.ToDateTime(strCurrent), "dd MMM yyyy");
                                            if (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(strStart), Convert.ToDateTime(strCurrent)) >= 1440)
                                            {
                                                MblnCorrectTime = false;
                                                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1562, out MmessageIcon);
                                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                                tmrClearlabels.Enabled = true;
                                                e.Cancel = true;
                                                return;
                                            }
                                        }
                                        if (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(strStart), Convert.ToDateTime(strCurrent)) >= 1440)
                                        {
                                            MblnCorrectTime = false;
                                            MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1562, out MmessageIcon);
                                            MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                            tmrClearlabels.Enabled = true;
                                            e.Cancel = true;
                                            return;
                                        }
                                    }
                                    else
                                    {

                                        strCurrent = Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells[k].FormattedValue);
                                        iDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(strCurrent));
                                        if (iDiff < 0)
                                        {
                                            strCurrent = Convert.ToDateTime(strCurrent).AddDays(1).ToString();
                                            DayChange = Strings.Format(Convert.ToDateTime(strCurrent), "dd MMM yyyy");
                                            flag = true;
                                            if (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(strStart), Convert.ToDateTime(strCurrent)) >= 1440)
                                            {
                                                MblnCorrectTime = false;
                                                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1562, out MmessageIcon);
                                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                                tmrClearlabels.Enabled = true;
                                                e.Cancel = true;
                                                return;
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    strCurrent = Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells[3].Value);
                                }
                                sPreviousTime = strCurrent;
                            }

                        }//(e.ColumnIndex >= 3 )
                    }

                }//rowindex =-1

            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }

        }

        private void DgvAttendance_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

            try
            {

            }
            catch (Exception)
            {

            }
        }

        private void DgvAttendance_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex > ColDgvStatus.Index && e.ColumnIndex <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1)
            {
                if (Microsoft.VisualBasic.Information.IsNothing(e.Value) || String.IsNullOrEmpty(e.Value.ToString()))
                {
                    return;
                }

                DateTime dtThedate;
                System.Text.StringBuilder sdateString = new System.Text.StringBuilder();
                if (!DateTime.TryParse(e.Value.ToString(), out dtThedate))//&& dtThedate==#12:00:00 AM#
                {
                }
                else
                {
                    if (Glbln24HrFormat != false)
                    {
                        e.Value = dtThedate.ToString("HH:mm");
                        e.FormattingApplied = true;
                    }
                    else
                    {
                        e.Value = dtThedate.ToString("hh:mm tt");
                        e.FormattingApplied = true;
                    }

                }
            }
        }

        private void DgvAttendance_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (DgvAttendance.RowCount <= 0)//Then Exit Sub
                { return; }
                int iGridRowIndex = DgvAttendance.CurrentRow.Index;
                int iGridColIndex = DgvAttendance.CurrentCell.ColumnIndex;
                switch (e.KeyCode)
                {
                    case Keys.Enter:
                        if (DgvAttendance.Columns[iGridColIndex].Name.IndexOf("ColDgvAddMore") != -1)
                        {
                            if ((DgvAttendance.Rows[iGridRowIndex].Cells[iGridColIndex - 2].Value != null) && DgvAttendance.Rows[iGridRowIndex].Cells[iGridColIndex - 1].Value != null)
                            {
                                if (Convert.ToString(DgvAttendance.Rows[iGridRowIndex].Cells[iGridColIndex - 2].Value).Trim() == "" || Convert.ToString(DgvAttendance.Rows[iGridRowIndex].Cells[iGridColIndex - 1].Value).Trim() == "")
                                    return;
                                else
                                    AddNewColumns(3);

                            }
                            else
                            {
                                return;
                            }
                        }
                        break;
                    case Keys.Delete:
                        if (MblnDeletePermission)
                        {
                            if (MintDisplayMode == Convert.ToInt32(AttendanceMode.AttendanceAuto))
                            {
                                if (DgvAttendance.CurrentRow.Selected)
                                    return;
                                int iUpto = DgvAttendance.Columns["ColDgvAddMore"].Index;
                                bool bChange = false;
                                foreach (DataGridViewCell cell in DgvAttendance.SelectedCells)
                                {
                                    for (int i = cell.ColumnIndex; i <= iUpto - 1; i++)
                                    {
                                        if (cell.ColumnIndex > 2)
                                        {
                                            if (DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex].Value != null && DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex + 1].Value != null)
                                            {
                                                if (DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex].Value != DBNull.Value && DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex + 1].Value != DBNull.Value)
                                                {
                                                    if (DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex].Value.ToStringCustom() != string.Empty)
                                                    {
                                                        if (i == iUpto - 1)
                                                        { DgvAttendance.Rows[cell.RowIndex].Cells[i].Value = ""; }
                                                        else
                                                        {
                                                            DgvAttendance.Rows[cell.RowIndex].Cells[i].Value = DgvAttendance.Rows[cell.RowIndex].Cells[i + 1].Value;
                                                            bChange = true;
                                                        }
                                                    }

                                                }
                                                else if (DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex].Value == DBNull.Value && DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex + 1].Value != DBNull.Value)
                                                {
                                                    if (i == iUpto - 1)
                                                    { DgvAttendance.Rows[cell.RowIndex].Cells[i].Value = ""; }
                                                    else
                                                    {
                                                        DgvAttendance.Rows[cell.RowIndex].Cells[i].Value = DgvAttendance.Rows[cell.RowIndex].Cells[i + 1].Value;
                                                        bChange = true;
                                                    }
                                                }
                                                else if (DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex].Value != DBNull.Value && DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex + 1].Value == DBNull.Value)
                                                {
                                                    if (i == iUpto - 1)
                                                    {
                                                        DgvAttendance.Rows[cell.RowIndex].Cells[i].Value = "";
                                                    }
                                                    else
                                                    {
                                                        DgvAttendance.Rows[cell.RowIndex].Cells[i].Value = DgvAttendance.Rows[cell.RowIndex].Cells[i + 1].Value;
                                                        bChange = true;
                                                    }
                                                }

                                            }
                                        }
                                    }//for
                                }//for each
                                ClearBottomPanel();
                                if (bChange)
                                    WorkTimeCalculation(DgvAttendance.CurrentRow.Index, false);
                            }
                            if (iGridRowIndex >= 0)
                            {
                                bool blnDelete = false;
                                foreach (DataGridViewRow DgvRow in DgvAttendance.SelectedRows)
                                {
                                    int iGridSelectedRowIndex = DgvRow.Index;
                                    if (iGridSelectedRowIndex >= 0)
                                    {
                                        if (DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvEmployee"].Tag != null)
                                        {

                                            DateTime dtDate = Convert.ToDateTime(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvAtnDate"].Value);

                                            if (MobjclsBLLAttendance.IsSalaryReleased(Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvEmployee"].Tag), Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvCompanyID"].Value), dtDate) == false) //Then 'If CheckReleasedSalary(Convert.ToInt32(AttendanceDataGridView.Rows(iGridSelectedRowIndex).Cells("Employee").Tag), dtDate) = False Then
                                            {

                                                MobjclsBLLAttendance.DeleteLeaveEntry(Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvEmployee"].Tag), Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvCompanyID"].Value), dtDate);
                                                if (Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvLeaveId"].Value) > 0)
                                                {
                                                    MobjclsBLLAttendance.DeleteLeaveFromAttendance(Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvEmployee"].Tag), Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvCompanyID"].Value), dtDate, Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvLeaveId"].Value));
                                                }
                                                MobjclsBLLAttendance.DeleteAttendance(Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvEmployee"].Tag), Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvCompanyID"].Value), dtDate);
                                                blnDelete = true;
                                            }
                                            else
                                            {

                                                if (MobjclsBLLAttendance.IsAttendanceExistsManual(Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvCompanyID"].Value), Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvEmployee"].Tag), dtDate) != true)
                                                {
                                                    //MessageBox.Show("Cannot delete employee attendance. Salary released.", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1599, out MmessageIcon);
                                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                                    tmrClearlabels.Enabled = true;
                                                    e.Handled = true;
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (blnDelete)
                                {
                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 4, out MmessageIcon);
                                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    tmrClearlabels.Enabled = true;
                                }

                            }
                        }
                        else
                        {
                            MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1599, out MmessageIcon).Replace("#", "").Trim();

                            // MessageBox.Show("You Don't have enough permission to delete attendance", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            MessageBox.Show(MstrMessCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                            tmrClearlabels.Enabled = true;
                            e.Handled = true;
                            return;
                        }
                        break;
                    case Keys.Up:
                        ClearBottomPanel();
                        if (iGridRowIndex > 0)
                        {
                            WorkTimeCalculation(iGridRowIndex - 1, false);

                            ShowConsequenceAndShiftInfo(iGridRowIndex - 1);
                        }
                        break;
                    case Keys.Down:
                        ClearBottomPanel();
                        if (iGridRowIndex < DgvAttendance.RowCount - 1)
                        {
                            WorkTimeCalculation(iGridRowIndex + 1, false);
                            ShowConsequenceAndShiftInfo(iGridRowIndex + 1);
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }

        private void DgvAttendance_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (DgvAttendance.IsCurrentCellDirty)
                {
                    if (DgvAttendance.CurrentCell != null)
                        DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);

                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }

        private void DgvAttendance_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (DgvAttendance.IsCurrentCellDirty)
                {
                    if (DgvAttendance.CurrentCell != null)
                        DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);

                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }

        private void DgvAttendance_Leave(object sender, EventArgs e)
        {
            if (DgvAttendance.RowCount > 0)
                DgvAttendance.ClearSelection();

        }

        private void DgvAttendance_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {

                if (e.RowIndex < -1)
                {
                    string sfromtime = "";
                    string stotime = "";
                    string sduration = "";
                    int iShiftType = 0;
                    int iNoOfTimings = 0;
                    string minWorkhours = "0";
                    string sAllowedBreakTime = "";
                    int iLate = 0;
                    int iEarly = 0;
                    DateTime dtdate = Convert.ToDateTime(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvAtnDate"].Value);
                    if (DgvAttendance.Rows[e.RowIndex].Cells["ColDgvShift"].Tag != null)
                    {
                        GetShiftInfo(Convert.ToInt32(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvCompanyID"].Value), Convert.ToInt32(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvEmployee"].Tag), Convert.ToInt32(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvShift"].Tag), dtdate, ref  sfromtime, ref stotime, ref sduration, ref  iShiftType, ref iNoOfTimings, ref  minWorkhours, ref  sAllowedBreakTime, ref iLate, ref iEarly);

                    }

                    if (DgvAttendance.Rows[e.RowIndex].Cells["TimeIn1"].Value != null && Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells["TimeIn1"].Value) != "" &&
                        Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells["TimeOut1"].Value) != "" && DgvAttendance.Rows[e.RowIndex].DefaultCellStyle.ForeColor != Color.IndianRed)
                        WorkTimeCalculation(e.RowIndex, false);

                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

            }
        }

        private void DgvAttendance_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (DgvAttendance.IsCurrentCellDirty)
                {
                    if (DgvAttendance.CurrentCell != null)
                        DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);

                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }

        private void DgvAttendance_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (DgvAttendance.CurrentCell != null)
                {
                    if (DgvAttendance.CurrentCell.ColumnIndex == ColDgvStatus.Index)
                    {
                        CboStatusComboBox = (ComboBox)e.Control;
                        if (CboStatusComboBox != null)
                        {
                            CboStatusComboBox.KeyPress -= new KeyPressEventHandler(ComboBox_KeyPress);
                            CboStatusComboBox.KeyPress += new KeyPressEventHandler(ComboBox_KeyPress);
                        }

                        //DgvAttendance.NotifyCurrentCellDirty(true);
                    }
                    if (DgvAttendance.CurrentCell.ColumnIndex > ColDgvStatus.Index && DgvAttendance.CurrentCell.ColumnIndex < ColDgvAddMore.Index)
                    {
                        if (DgvAttendance.CurrentCell is DataGridViewTextBoxCell)
                        {
                            TextBox txt = (TextBox)(e.Control);
                            txt.KeyPress -= new KeyPressEventHandler(txtInt_KeyPress);
                            txt.KeyPress += new KeyPressEventHandler(txtInt_KeyPress);

                        }
                    }
                    if (DgvAttendance.CurrentCell.ColumnIndex == ColDgvRemarks.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress -= new KeyPressEventHandler(txtInt_KeyPress);
                    }

                }
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }
        }

        private void btnFetchData_Click(object sender, EventArgs e)
        {
            try
            {//Fetching Data from Temp Table
                MintEmployeeID = 0;
                MstrEmployeeName = "";
                lblEmployeeSelectedValue.Text = MstrEmployeeName;

                expandableSplitterLeft.Expanded = false;
                ExpandablepnlSearch.Expanded = false;

                MintDisplayMode = Convert.ToInt32(AttendanceMode.AttendanceAuto);
                DgvPunchingData.Visible = false;
                DgvAttendance.Visible = true;

                if (DgvAttendance.Rows.Count > 0 && MintDisplayMode == Convert.ToInt32(AttendanceMode.AttendanceAuto))
                {//'//'Fetched data already exists. Do you wish to continue?
                    if (MessageBox.Show(new ClsNotification().GetErrorMessage(MsarMessageArr, 1572, out  MmessageIcon).Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DgvAttendance.Rows.Clear();
                        ClearAllControls();
                    }
                    else
                    {
                        this.Cursor = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    DgvAttendance.Rows.Clear();
                    ClearAllControls();
                }

                GetData();
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString());

            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (DgvAttendance.Rows.Count > 0)
                {

                    if (SaveAttendanceInfo())
                    {
                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1600, out MmessageIcon);

                        //MstrMessCommon = "Saved Successfully.";
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        tmrClearlabels.Enabled = true;
                        barProgressBarAttendance.Visible = false;
                        barProgressBarAttendance.Value = 0;
                        ClearAllControls();
                    }

                }
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (DgvAttendance.RowCount > 0)
                {
                    if (rbtnMonthly.Checked)//Then ''employee wise
                    {
                        if (MintEmployeeID <= 0)
                        {
                            MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1577, out MmessageIcon);
                            MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                            tmrClearlabels.Enabled = true;
                            return;
                        }
                    }

                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 13, out MmessageIcon).Replace("#", "").Trim();
                    if (MessageBox.Show(MstrMessCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        return;
                    }
                    if (DeleteAttendenceDetails())
                    {
                        rbtnMonthly.Checked = rbtnMonthly.Checked = false ? true : true;
                        DgvAttendance.RowCount = 0;
                        MintEmployeeID = 0;
                        MstrEmployeeName = "";
                        lblEmployeeSelectedValue.Text = MstrEmployeeName;
                        DgvEmployee.ClearSelection();
                        lblLateComing.Visible = false;
                        lblEarlyGoing.Visible = false;
                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 4, out MmessageIcon);
                        lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        tmrClearlabels.Enabled = true;
                    }
                    else
                    {
                        if (MblDeleteMeassage)
                        {
                            MblDeleteMeassage = false;
                            MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1573, out MmessageIcon);
                            MstrMessCommon = MstrMessCommon.Replace("save", "Delete");
                            MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                            tmrClearlabels.Enabled = true;
                        }
                    }

                    if (DgvAttendance.RowCount == 1)
                        //MblnChangeStatus = false;

                        if (DgvAttendance.RowCount >= 0)
                            DgvAttendance.ClearSelection();

                }
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearAllControls();
        }

        private void btnAutoFill_Click(object sender, EventArgs e)
        {
            if (ChkLate.Checked == false && ChkEarly.Checked == false && ChkExtendedBreak.Checked == false && ChkConsequence.Checked == false && ChkAbsent.Checked == false && ChkLeave.Checked == false && ChkOverTime.Checked == false && ChkShortage.Checked == false)
            {
                if (Convert.ToInt32(CboCompany.SelectedValue) > 0)
                {

                    MbAutoFill = true;
                    if (rbtnMonthly.Checked)
                    {
                        if (MintEmployeeID > 0)
                            AutoFillEmployees();
                    }
                    else
                        AutoFillEmployees();
                }
                else
                {
                    MbAutoFill = false;
                }
            }
        }
        private void btnMapping_Click(object sender, EventArgs e)
        {
            FrmAttendanceMapping OBJ = new FrmAttendanceMapping();
            OBJ.ShowDialog();
        }

        private void btnViewValidTimings_Click(object sender, EventArgs e)
        {
            try
            {

                for (int i = 0; i <= DgvAttendance.RowCount - 1; i++)
                {
                    if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvValidFlag"].Value) == 1)
                    {
                        DgvAttendance.Rows[i].Visible = true;
                    }
                    else
                    {
                        DgvAttendance.Rows[i].Visible = false;
                    }
                }
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }

        private void btnInvalidTimings_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i <= DgvAttendance.RowCount - 1; i++)
                {
                    if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvValidFlag"].Value) == 1)
                    {
                        DgvAttendance.Rows[i].Visible = false;
                    }
                    else
                    {
                        DgvAttendance.Rows[i].Visible = true;
                    }
                }
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }

        private void btnViewAllTimings_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i <= DgvAttendance.RowCount - 1; i++)
                {

                    DgvAttendance.Rows[i].Visible = true;

                }
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }

        private void btnViewFirstandLastTimings_Click(object sender, EventArgs e)
        {
            try
            {
                lblEarlyGoing.Visible = false;
                lblLateComing.Visible = false;
                for (int iRow = 0; iRow <= DgvAttendance.RowCount - 1; iRow++)
                {


                    for (int icolumn = 4; icolumn <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1; icolumn++)
                    {

                        if (Convert.ToString(DgvAttendance.Rows[iRow].Cells[icolumn].Value) == "")
                        {
                            DgvAttendance.Rows[iRow].Cells[4].Value = DgvAttendance.Rows[iRow].Cells[icolumn - 1].Value;
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Black;
                            DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 1;
                            break;

                        }
                        else if (icolumn == DgvAttendance.Columns["ColDgvAddMore"].Index - 1)
                        {
                            DgvAttendance.Rows[iRow].Cells[4].Value = DgvAttendance.Rows[iRow].Cells[icolumn].Value;
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Black;
                            DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 1;
                            break;
                        }

                    }
                    WorkTimeCalculation(iRow, true);

                    if (DgvAttendance.Rows[iRow].Cells["ColDgvStatus"].Value.ToInt32() == 0)
                        DgvAttendance.Rows[iRow].Visible = false;
                }

                for (int i = DgvAttendance.Columns["ColDgvAddMore"].Index - 1; i >= 5; i--)
                {
                    if (DgvAttendance.Columns[i].Name.IndexOf(DgvAttendance.Columns[i].Name) != -1)
                    {
                        DgvAttendance.Columns.RemoveAt(i);
                    }


                }
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);


            }
        }

        private void btnViewDay_Click(object sender, EventArgs e)
        {
            if (btnViewDay.Checked)
                btnViewDay.Checked = false;
            else
                btnViewDay.Checked = true;

            if (btnViewDay.Checked)
                ColDgvDay.Visible = true;
            else
                ColDgvDay.Visible = false;

            DgvAttendance.Refresh();

        }

        private void btnViewWorkTime_Click(object sender, EventArgs e)
        {
            if (btnViewWorkTime.Checked)
                btnViewWorkTime.Checked = false;
            else
                btnViewWorkTime.Checked = true;


            if (btnViewWorkTime.Checked)
                ColDgvWorkTime.Visible = true;
            else
                ColDgvWorkTime.Visible = false;

            DgvAttendance.Refresh();
        }

        private void btnViewBreakTime_Click(object sender, EventArgs e)
        {
            if (btnViewBreakTime.Checked)
                btnViewBreakTime.Checked = false;
            else
                btnViewBreakTime.Checked = true;

            if (btnViewBreakTime.Checked)
                ColDgvBreakTime.Visible = true;
            else
                ColDgvBreakTime.Visible = false;

            DgvAttendance.Refresh();
        }

        private void btnViewExcessTime_Click(object sender, EventArgs e)
        {
            if (btnViewExcessTime.Checked)
                btnViewExcessTime.Checked = false;
            else
                btnViewExcessTime.Checked = true;

            if (btnViewExcessTime.Checked)
                ColDgvExcessTime.Visible = true;
            else
                ColDgvExcessTime.Visible = false;

            DgvAttendance.Refresh();
        }

        private void btnViewLateComing_Click(object sender, EventArgs e)
        {
            if (btnViewLateComing.Checked)
                btnViewLateComing.Checked = false;
            else
                btnViewLateComing.Checked = true;

            if (btnViewLateComing.Checked)
                ColDgvLateComing.Visible = true;
            else
                ColDgvLateComing.Visible = false;


            DgvAttendance.Refresh();
        }

        private void btnViewEarlyGoing_Click(object sender, EventArgs e)
        {
            if (btnViewEarlyGoing.Checked)
                btnViewEarlyGoing.Checked = false;
            else
                btnViewEarlyGoing.Checked = true;

            if (btnViewEarlyGoing.Checked)
                ColDgvEarlyGoing.Visible = true;
            else
                ColDgvEarlyGoing.Visible = false;

            DgvAttendance.Refresh();
        }

        private void expandableSplitterLeft_ExpandedChanging(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
        {
            if (MintDisplayMode == Convert.ToInt16(AttendanceMode.AttendanceAuto))
            {

                if (DgvAttendance.Rows.Count > 0 || DgvPunchingData.Rows.Count > 0)
                {
                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1593, out MmessageIcon);
                    if (MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DgvAttendance.Visible = true;
                        DgvAttendance.BringToFront();
                        DgvPunchingData.Visible = false;

                        DgvAttendance.DataSource = null;
                        DgvAttendance.Rows.Clear();
                        DgvPunchingData.Rows.Clear();
                        ClearAllControls();
                        tmrClearlabels.Enabled = true;
                        MintDisplayMode = Convert.ToInt16(AttendanceMode.AttendanceManual);
                        InitializeGrid();
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    DgvAttendance.Visible = true;
                    DgvAttendance.BringToFront();
                    DgvPunchingData.Visible = false;
                    MintDisplayMode = Convert.ToInt16(AttendanceMode.AttendanceManual);
                    ClearAllControls();
                }
            }
            DgvAttendance.Columns["ColDgvAtnDate"].Width = 75;
            DgvAttendance.Columns["ColDgvAddMore"].Width = 35;
            DgvAttendance.Columns["ColDgvAddMore"].MinimumWidth = 35;
            ColDgvAddMore.FillWeight = 35;
            DgvAttendance.Columns["ColDgvAddMore"].Resizable = DataGridViewTriState.False;
            DgvAttendance.Refresh();
        }

        private void ExpandablepnlSearch_ExpandedChanging(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
        {

            if (MintDisplayMode == Convert.ToInt16(AttendanceMode.AttendanceAuto))
            {

                if (DgvAttendance.Rows.Count > 0 || DgvPunchingData.Rows.Count > 0)
                {
                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1593, out MmessageIcon);
                    if (MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DgvAttendance.Visible = true;
                        DgvAttendance.BringToFront();
                        DgvPunchingData.Visible = false;

                        DgvAttendance.DataSource = null;
                        DgvAttendance.Rows.Clear();
                        DgvPunchingData.Rows.Clear();
                        ClearAllControls();
                        tmrClearlabels.Enabled = true;
                        MintDisplayMode = Convert.ToInt16(AttendanceMode.AttendanceManual);
                        InitializeGrid();
                    }
                    else
                    {

                        e.Cancel = true;
                    }
                }
                else
                {
                    DgvAttendance.Visible = true;
                    DgvAttendance.BringToFront();
                    DgvPunchingData.Visible = false;
                    MintDisplayMode = Convert.ToInt16(AttendanceMode.AttendanceManual);
                    InitializeGrid();
                }
            }
        }

        private void CboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboCompany.SelectedIndex != -1)
            {
                MintEmployeeID = 0;
                MstrEmployeeName = "";
                lblEmployeeSelectedValue.Text = MstrEmployeeName;

                LoadCombo(2);
                LoadCombo(4);
                ClearAllControls();
                DgvEmployee.RowHeadersVisible = false;
                DgvEmployee.ColumnHeadersVisible = false;
                DgvEmployee.DataSource = null;
                LoadEmployeeGrid();
            }

        }

        private void cboEmpWorkstatus_SelectedIndexChanged(object sender, EventArgs e)
        {

            MintEmployeeID = 0;
            MstrEmployeeName = "";
            lblEmployeeSelectedValue.Text = MstrEmployeeName;

            if (CboEmpFilter.SelectedIndex > -1)
            {
                if (CboEmpFilter.Text == "All")
                {
                    LoadCombo(4);
                }
                else if (CboEmpFilter.Text == "Department")
                {
                    LoadCombo(5);
                }
                else if (CboEmpFilter.Text == "Designation")
                {
                    LoadCombo(6);
                }
                else if (CboEmpFilter.Text == "Employment Type")
                {
                    LoadCombo(7);
                }
            }
            ClearAllControls();
            DgvEmployee.RowHeadersVisible = false;
            DgvEmployee.ColumnHeadersVisible = false;
            DgvEmployee.DataSource = null;
            LoadEmployeeGrid();
        }

        private void CboEmpFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboEmpFilter.SelectedIndex > -1)
            {
                if (CboEmpFilter.Text == "All")
                {
                    LoadCombo(4);
                }
                else if (CboEmpFilter.Text == "Department")
                {
                    LoadCombo(5);
                }
                else if (CboEmpFilter.Text == "Designation")
                {
                    LoadCombo(6);
                }
                else if (CboEmpFilter.Text == "Employment Type")
                {
                    LoadCombo(7);
                }
                MintEmployeeID = 0;
                MstrEmployeeName = "";
                lblEmployeeSelectedValue.Text = MstrEmployeeName;

                ClearAllControls();
                DgvEmployee.RowHeadersVisible = false;
                DgvEmployee.ColumnHeadersVisible = false;
                DgvEmployee.DataSource = null;
                LoadEmployeeGrid();
            }
        }

        private void CboEmpFilterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            MintEmployeeID = 0;
            MstrEmployeeName = "";
            lblEmployeeSelectedValue.Text = MstrEmployeeName;

            ClearAllControls();
            LoadEmployeeGrid();

        }
        //----------------------------------------------------------MAnual--------------------------------------------------------------------
        private void btnSearch_Click(object sender, EventArgs e)
        {
            MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1595, out MmessageIcon);//(MsarMessageArr, 1595,MmessageIcon);
            if (txtSearch.Text != "" )
            {
                if (DgvEmployee.Rows.Count > 0)
                {
                    if (SearchEmp() == true)
                    {
                        if (MintSearchIndex == 0)
                        {
                            MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                            tmrClearlabels.Enabled = true;
                            DgvEmployee.CurrentCell = DgvEmployee[2, 0];
                            DgvEmployee.Refresh();

                        }
                        MintSearchIndex = 0;
                    }
                }
            }
            else
            {
                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                tmrClearlabels.Enabled = true;
            }
        }

        private void DgvEmployee_Sorted(object sender, EventArgs e)
        {
            if (DgvEmployee.Rows.Count > 0)
            {
                for (int i = 0; i <= DgvEmployee.Rows.Count - 1; i++)
                {
                    //DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[6];
                    if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["Gender"].Value) == 0)// 'Male
                    {
                        if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 1)// 'Absconding
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[0];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 2)// 'Expired
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[1];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 3)// 'Retired
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[5];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 4)// 'Resigned
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[4];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 5)// 'Terminated
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[6];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 6)// 'In service
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[2];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 7)//'In service
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[3];
                        else
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[15];

                    }
                    else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["Gender"].Value) == 1) //'Female
                    {
                        if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 1) //Then 'Absconding
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[7];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 2)//Then 'Expired
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[8];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 3) //Then 'Retired
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[12];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 4) //Then 'Resigned
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[11];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 5)// Then 'Terminated
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[13];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 6)//Then 'In service
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[9];
                        else if (Convert.ToInt32(DgvEmployee.Rows[i].Cells["WorkStatusID"].Value) == 7)// Then 'In service
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[10];
                        else
                            DgvEmployee.Rows[i].Cells["Column1"].Value = ImageListEmployee.Images[15];

                    }
                }//For
            }
        }

        private void ComboBox_KeyPress(object Sender, KeyPressEventArgs e)
        {
            CboStatusComboBox.DroppedDown = false;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            MbAutoFill = false;
            MintDisplayMode = Convert.ToInt32(AttendanceMode.AttendanceManual);
            DgvPunchingData.Visible = false;
            DgvAttendance.Visible = true;
            if (rbtnMonthly.Checked)
            {
                if (MintEmployeeID <= 0)
                {
                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1577, out MmessageIcon);
                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                    tmrClearlabels.Enabled = true;
                    return;
                }
            }

            SearchData();
            ShowCount();
        }

        private void DgvEmployee_Click(object sender, EventArgs e)
        {

            if (DgvEmployee.RowCount > 0 && DgvEmployee.CurrentRow != null)
            {

                MintEmployeeID = Convert.ToInt32(DgvEmployee.Rows[DgvEmployee.CurrentRow.Index].Cells["EmployeeID"].Value);
                MstrEmployeeName = Convert.ToString(DgvEmployee.Rows[DgvEmployee.CurrentRow.Index].Cells["Name"].Value) + " - " + Convert.ToString(DgvEmployee.Rows[DgvEmployee.CurrentRow.Index].Cells["Number"].Value);
                lblEmployeeSelectedValue.Text = MstrEmployeeName;

                if (rbtnMonthly.Checked == false)
                {
                    rbtnMonthly.Checked = true;
                }
                lblAbsentDispaly.Text = "0";
                lblLeaveCountDispaly.Text = "0";
                lblPresentDisplay.Text = "0";
                lblShortageCountDispaly.Text = "0";

                DgvAttendance.RowCount = 0;
                InitializeGrid();
                ClearBottomPanel();
            }
        }

        private void DgvEmployee_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (DgvEmployee.RowCount > 0 && DgvEmployee.CurrentRow != null)
            {

                MintEmployeeID = Convert.ToInt32(DgvEmployee.Rows[DgvEmployee.CurrentRow.Index].Cells["EmployeeID"].Value);
                MstrEmployeeName = Convert.ToString(DgvEmployee.Rows[DgvEmployee.CurrentRow.Index].Cells["Name"].Value) + " - " + Convert.ToString(DgvEmployee.Rows[DgvEmployee.CurrentRow.Index].Cells["Number"].Value);
                lblEmployeeSelectedValue.Text = MstrEmployeeName;

                if (rbtnMonthly.Checked == false)
                {
                    rbtnMonthly.Checked = true;
                }
                lblAbsentDispaly.Text = "0";
                lblLeaveCountDispaly.Text = "0";
                lblPresentDisplay.Text = "0";
                lblShortageCountDispaly.Text = "0";

                DgvAttendance.RowCount = 0;
                InitializeGrid();
                ClearBottomPanel();
            }
        }

        private void rbtnMonthly_CheckedChanged(object sender, EventArgs e)
        {

            if (rbtnMonthly.Checked)
            {
                dtpFromDate.CustomFormat = "MMM yyyy";
                grpDaySummary.Text = "Month Summary";
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    grpDaySummary.Text = "ملخص الشهر";
                //}
                lblAbsentDispaly.Text = "0";
                lblLeaveCountDispaly.Text = "0";
                lblPresentDisplay.Text = "0";
                lblShortageCountDispaly.Text = "0";
                lblEmployeeSelectedValue.Text = MstrEmployeeName;
                ColDgvEmployee.Visible = false;
                ColDgvAtnDate.Visible = true;
            }
            else
            {
                grpDaySummary.Text = "Day Summary";
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    grpDaySummary.Text = "ملخص اليوم";
                //}
                ColDgvEmployee.Visible = true;
                ColDgvAtnDate.Visible = false;
                lblAbsentDispaly.Text = "0";
                lblLeaveCountDispaly.Text = "0";
                lblPresentDisplay.Text = "0";
                lblShortageCountDispaly.Text = "0";
                dtpFromDate.CustomFormat = "dd MMM yyyy";
                lblEmployeeSelectedValue.Text = "";
            }
            DgvAttendance.RowCount = 0;
        }

        private void ExpPanelEmployee_ExpandedChanging(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
        {
            CboCompany.Visible = true;
            CboCompany.BringToFront();
            CboCompany.Refresh();
        }

        private void btnviewLogsFromHandpunch_Click(object sender, EventArgs e)
        {
            if (DgvPunchingData.Visible == false && DgvAttendance.Visible == true)
            {
                DgvPunchingData.Visible = true;
                DgvAttendance.Visible = false;
            }
            else if (DgvPunchingData.Visible == true && DgvAttendance.Visible == false)
            {
                DgvPunchingData.Visible = false;
                DgvAttendance.Visible = true;
            }
            this.Refresh();
        }

        private void tmrClearlabels_Tick(object sender, EventArgs e)
        {
            tmrClearlabels.Enabled = false;
            lblAttendnaceStatus.Text = "";
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            MintSearchIndex = 0;
        }

        private void cboEmpSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            MintSearchIndex = 0;
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {

            try
            {
                if (rbtnMonthly.Checked)
                    dtpFromDate.Value = GetStratDate().ToDateTime();

            }
            catch
            {
            }

        }
        private void dtpFromDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (rbtnMonthly.Checked)
                    dtpFromDate.Value = GetStratDate().ToDateTime();

            }
            catch
            {
            }
        }
        private void CboCompany_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                CboCompany.DroppedDown = false;
                if (e.KeyData == Keys.Enter)
                    CboCompany_SelectedIndexChanged(sender, new EventArgs());
            }
            catch
            {
            }
        }

        private void CboEmpFilterType_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                CboEmpFilterType.DroppedDown = false;
                if (e.KeyData == Keys.Enter)
                    CboEmpFilterType_SelectedIndexChanged(sender, new EventArgs());
            }
            catch
            {
            }
        }

        private void CboEmpFilter_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                CboEmpFilter.DroppedDown = false;
                if (e.KeyData == Keys.Enter)
                    CboEmpFilter_SelectedIndexChanged(sender, new EventArgs());
            }
            catch
            {
            }
        }

        private void ExpPanelEmployee_ExpandedChanged(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
        {
            CboCompany.Visible = true;
            CboCompany.BringToFront();
            CboCompany.Refresh();
        }

        private void btnLeaveOpening_Click(object sender, EventArgs e)
        {
            using (FrmLeaveOpening objLeaveOpening = new FrmLeaveOpening())
            {
                objLeaveOpening.ShowDialog();
            }
        }

        private void btnLive_Click(object sender, EventArgs e)
        {
            if (MintEmployeeID <= 0)
            {
                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1577, out MmessageIcon);
                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                tmrClearlabels.Enabled = true;
                return;
            }
            DataTable datLiveData = MobjclsBLLAttendance.getAttendanceLive(MintEmployeeID, 1, dtpFromDate.Value);
            if (datLiveData.Rows.Count > 0)
            {
                ConvertLiveData(datLiveData, "dd/MM/yyyy", true, 0);
            }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Attendance_Auto";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void FrmAttendance_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.Alt | Keys.R:
                        if (MblnDeletePermission)
                            btnDelete_Click(sender, new EventArgs());//delete
                        break;
                }
            }
            catch { }
        }

        private void btnExportToExcel_Click(object sender, EventArgs e)
        {
            int iStartColum = 0;
            int iEndColumn = 0;
            string sFileName = "";

            if ((DgvAttendance.Visible == true && DgvAttendance.RowCount <= 0) || (DgvPunchingData.Visible == true && DgvPunchingData.RowCount <= 0))
            {
                return;
            }
            MobjClsLogs.WriteLog("btnExportToExcel", 2);
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Title = "Save ";
                sfd.Filter = " Excel File (*.xls)|*.xls|  Supported Files (*.xls)|*.xls;";
                if (sfd.ShowDialog() == DialogResult.OK)
                {

                    sFileName = sfd.FileName;
                    MobjClsLogs.WriteLog("btnExportToExcel_Click(object sender, EventArgs e)--FileName=" + sFileName, 2);
                }
                else
                {
                    return;
                }
            }
            MobjClsLogs.WriteLog("btnExportToExcel_Click_Click(object sender, EventArgs e)", 2);
            try
            {
                if (sFileName != "")
                {

                    if (DgvAttendance.Visible == true && DgvAttendance.RowCount > 0)
                    {
                        MobjClsLogs.WriteLog("DgvAttendance.Visible == true && DgvAttendance.RowCount>0", 2);
                        iStartColum = 0;
                        iEndColumn = DgvAttendance.Columns["ColDgvShift"].Index;

                        MobjExportToExcel.ExportToExcel(DgvAttendance, sFileName, iStartColum, iEndColumn, true);
                        //reportHeader, ".xls", My.Computer.FileSystem.SpecialDirectories.Temp)DgvFetchData,"Data",".xls",Application.StartupPath,0,9);
                    }
                    else if (DgvPunchingData.Visible == true && DgvPunchingData.RowCount > 0)
                    {
                        MobjClsLogs.WriteLog("DgvPunchingData.Visible == true && DgvPunchingData.RowCount>0", 2);
                        iStartColum = 0;
                        iEndColumn = DgvPunchingData.Columns["ColDgvPunchingResult"].Index;
                        MobjExportToExcel.ExportToExcel(DgvPunchingData, sFileName, iStartColum, iEndColumn, false);
                    }
                }
                else
                {
                    MessageBox.Show("Please Enter Name");
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

            }
        }
        #endregion CONTROL EVENTS

    } //Class
}//nameSpace