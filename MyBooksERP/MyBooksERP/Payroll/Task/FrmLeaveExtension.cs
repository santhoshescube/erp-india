﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace MyBooksERP
{
    /*****************************************************
   * Created By       : Siny
   * Creation Date    : 06 May 2013
   * Description      : Handle Leave Extension
   * FormID           : 156
   * ***************************************************/

    /*****************************************************
    * Modified By   :Sanju
    * Creation Date : 19 Aug 2013
    * Description   : Optimization of form
    *****************************************************/
    public partial class FrmLeaveExtension : Form
    {

        #region DECLARATIONS
        public int PExtensionID = -1; //'ID pass from Another forms
        public int PFromEmployeeID = 0;
        private int RecordCnt, CurrentRecCnt;
        public Boolean ImageFlag;
        private Boolean MChangeStatus;//Check state of the page
        private Boolean MAddStatus;//Add/Update mode 
        public Boolean Mblnfill = false;//Show Diolog
        private Boolean bComboFill = false;
        private Boolean bFlag = false;//Avoid looping
        private String EndDate1;
        private ArrayList MaMessageArr;                 // Error Message display
        private string MsMessageCaption;
        private string MsMessageCommon;                 //  variable for assigning message
        private MessageBoxIcon MmessageIcon;
        public int intNavflg = 0;

        //------------permission-------------------//
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        //--------------------------------------------
        ClsCommonUtility objCommon;
        clsBLLLeaveExtension objLeaveExtension;
        ClsNotification mObjNotification;
        clsDTOLeaveExtension objDTOLeaveExtension;
        private Boolean TempStatus = true;
        private string strSearchString;//to pass search string
        public Int32 PintEmployeeID = 0;//to pass EmployeeID
        public Int32 MintEmployeeID = 0;//to for display
        string strBindingOf = "Of ";
        #endregion
        #region CONSTRUCTORS
        public FrmLeaveExtension()
        {
            InitializeComponent();
            objCommon = new ClsCommonUtility();
            objLeaveExtension = new clsBLLLeaveExtension();
            MsMessageCaption = ClsCommonSettings.MessageCaption;
            mObjNotification = new ClsNotification();
            objDTOLeaveExtension = new clsDTOLeaveExtension();

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.LeaveExtension, this);

        //    strBindingOf = "من ";
        //    txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
        //    ToolStripStatusLabel2.Text = "حالة :";
        //}
        #endregion
        #region EVENTS
        private void FrmLeaveExtension_Load(object sender, EventArgs e)
        {
            LoadCombos(0);
            LoadMessages();
            SetPermissions();
            ErrorProviderLeave.Clear();
            MChangeStatus = false;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(RecordCnt);
            BindingNavigatorPositionItem.Text = RecordCnt.ToString();
            TmStripClear.Interval = ClsCommonSettings.TimerInterval;
            ExtensionTypeComboBox.Focus();
            if (PintEmployeeID > 0)
            {
                MintEmployeeID = PintEmployeeID;
                GetRecordCount();
                if (RecordCnt > 0)
                {
                    BindingNavigatorMoveFirstItem_Click(null, null);
                }
                else
                {
                    AddNewLeaveExtension();

                }

            }
            else
            {
                AddNewLeaveExtension();

            }
        }
        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewLeaveExtension();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CancelToolStripButton_Click(object sender, EventArgs e)
        {
            if (MblnAddPermission)
                AddNewLeaveExtension();
        }

        private void CboLeaveType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (intNavflg == 0)
            {
                if (Convert.ToInt32(CboLeaveType.SelectedValue) == (int)EmployeeLeaveType.Maternity && EmployeeComboBox.Text != "")//Maternity
                {
                    if (objLeaveExtension.CheckMaleOrFemale(Convert.ToInt32(EmployeeComboBox.SelectedValue)))//Maternity leave is for female employees only
                    {
                        ShowErrorMessage(CboLeaveType, 10168, false, false);

                        CboLeaveType.SelectedIndex = -1;
                        this.Cursor = Cursors.Default;
                    }
                }
            }
            TxtBalLeave.Text = "0";
            if (Convert.ToInt32(CboLeaveType.SelectedValue) > 0)
                ShowBalanceLeave();
        }

        private void ExtensionTypeComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            EmployeeComboBox.SelectedIndex = -1;
            bFlag = true;
        }

        private void EmployeeComboBox_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void ExtensionTypeComboBox_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void cboPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Assigning DtpMonthYear value from cboPeriod selected value
            if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.LeaveExtension)
            {
                if (cboPeriod.SelectedIndex != -1)
                {
                    if (DtpMonthYear.MinDate <= cboPeriod.SelectedValue.ToDateTime())
                        DtpMonthYear.Value = cboPeriod.SelectedValue.ToDateTime();
                    else
                    {
                        DtpMonthYear.MinDate = cboPeriod.SelectedValue.ToDateTime();
                        DtpMonthYear.Value = cboPeriod.SelectedValue.ToDateTime();
                    }
                }
            }
        }

        private void cboPeriod_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void CboLeaveType_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void NoOfDaysOrMinuteTextBox_TextChanged(object sender, EventArgs e)
        {

            Changestatus();
        }

        private void NoOfDaysOrMinuteTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {

                if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.LeaveExtension)
                {
                    if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking not Digit,".",backspace
                    {
                        e.Handled = true;
                    }
                    if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && ((e.KeyChar != 53) && (e.KeyChar != 08) && (e.KeyChar != 13)))//checking whether it is not 5 after "."
                    {
                        e.Handled = true;
                    }
                }
                else
                {
                    if (!((Char.IsDigit(e.KeyChar)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking not Digit,backspace
                    {
                        e.Handled = true;
                    }
                }
            }
            catch { }
        }

        private void TxtBalLeave_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void DtpMonthYear_ValueChanged(object sender, EventArgs e)
        {
            if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.TimeExtensionMonth)
            {
                //Formating DtpMonthYear value
                string[] Str = DtpMonthYear.Text.Split('-');
                if (Str.Length < 3 && Str.Length > 1)
                {
                    if (DtpMonthYear.MinDate <= Convert.ToDateTime(DtpMonthYear.MinDate.Day.ToString() + "-" + Str[0] + "-" + Str[1] + " 11:59:59 PM"))
                        DtpMonthYear.Value = Convert.ToDateTime(DtpMonthYear.MinDate.Day.ToString() + "-" + Str[0] + "-" + Str[1] + " 11:59:59 PM");
                }
            }
            if (bFlag)
                GetShiftandLeaveInfo();
            CboLeaveType.SelectedIndex = -1;
            NoOfDaysOrMinuteTextBox.Text = "";
            Changestatus();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            BindingNavigatorSaveItem_Click(sender, e);
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {

            if (!MAddStatus && !MblnUpdatePermission)//Permission checking
            {
                ShowErrorMessage(BtnSave, 9131, false, true);
                return;
            }

            if (ValidateForm())
            {
                if (Validation())
                {
                    if (CboLeaveType.SelectedIndex == -1 && ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.LeaveExtension)//checking leave type entered for LeaveExtension
                        return;
                    int icmpid = objLeaveExtension.GetEmployeeCompany(Convert.ToInt32(EmployeeComboBox.SelectedValue));

                    if (objLeaveExtension.CheckLeavesTakenForEdit(DtpMonthYear.Value.ToString("dd-MMM-yyyy"), Convert.ToInt32(EmployeeComboBox.SelectedValue), icmpid, Convert.ToInt32(CboLeaveType.SelectedValue), Convert.ToDouble(NoOfDaysOrMinuteTextBox.Text)))
                    {
                        ShowErrorMessage(BtnSave, 10158, false, true);
                        return;
                    }
                    if (SaveDetail())
                    {
                        DisplayInfo();
                        if (MAddStatus)
                        {
                            BindingNavigatorMoveLastItem_Click(sender, e);
                            ShowErrorMessage(BtnSave, 2, false, true);
                        }
                        else
                        {
                            ShowErrorMessage(BtnSave, 2, false, true);
                            CurrentRecCnt = CurrentRecCnt + 1;
                            BindingNavigatorMovePreviousItem_Click(sender, e);
                        }
                        SetAutoCompleteList();
                        TmStripClear.Enabled = false;//to stop and start
                        TmStripClear.Enabled = true;
                    }
                }
            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            MAddStatus = false;
            GetRecordCount();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = CurrentRecCnt - 1;
                if (CurrentRecCnt <= 0)
                    CurrentRecCnt = 1;
            }
            else
                CurrentRecCnt = 1;

            //if (ClsCommonSettings.IsArabicView)
            //    ToolStripStatusLabel1.Text = "سجل السابق";
            //else
                ToolStripStatusLabel1.Text = "Previous Record";

            DisplayLeaveExtension();
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            MAddStatus = false;

            GetRecordCount();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = 1;
            }
            else
                CurrentRecCnt = 0;

            //if (ClsCommonSettings.IsArabicView)
            //    ToolStripStatusLabel1.Text = "سجل الأول";
            //else
                ToolStripStatusLabel1.Text = "First Record";
            
            DisplayLeaveExtension();
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            MAddStatus = false;

            GetRecordCount();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = CurrentRecCnt + 1;
                if (CurrentRecCnt >= RecordCnt)
                    CurrentRecCnt = RecordCnt;
            }
            else
                CurrentRecCnt = 1;

            //if (ClsCommonSettings.IsArabicView)
            //    ToolStripStatusLabel1.Text = "سجل المقبل";
            //else
                ToolStripStatusLabel1.Text = "Next Record";
            
            DisplayLeaveExtension();
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            MAddStatus = false;

            GetRecordCount();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = RecordCnt;
            }
            else
                CurrentRecCnt = 0;

            //if (ClsCommonSettings.IsArabicView)
            //    ToolStripStatusLabel1.Text = "سجل مشاركة";
            //else
                ToolStripStatusLabel1.Text = "Last Record";
            
            DisplayLeaveExtension();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (Validation())
            {
                if (Convert.ToInt64(lblExtensionId.Tag) == 0)
                {
                    ShowErrorMessage(lblExtensionId, 9132, false, false);
                    return;
                }
                if (EmployeeComboBox.SelectedIndex == -1)
                    return;
                if (ExtensionTypeComboBox.SelectedIndex == -1)
                    return;
                if (objLeaveExtension.CheckPaymentRelease(Convert.ToInt32(EmployeeComboBox.SelectedValue), DtpMonthYear.Value.ToString("dd-MMM-yyyy"), 2))
                {
                    ShowErrorMessage(cboPeriod, 10157, false, false);
                    return;
                }
                if (Convert.ToInt32(ExtensionTypeComboBox.SelectedValue) == (int)LeaveExtensionType.LeaveExtension)
                {
                    if (CboLeaveType.SelectedIndex == -1)
                        return;
                    int icmpid = objLeaveExtension.GetEmployeeCompany(Convert.ToInt32(EmployeeComboBox.SelectedValue));
                    if (objLeaveExtension.CheckLeavesTakenForDelete(DtpMonthYear.Value.ToString("dd-MMM-yyyy"), Convert.ToInt32(EmployeeComboBox.SelectedValue), icmpid, Convert.ToInt32(CboLeaveType.SelectedValue)))
                    {
                        ShowErrorMessage(cboPeriod, 10152, false, false);
                        return;
                    }
                }
                if (!objLeaveExtension.CheckIfExtensionIDExists(Convert.ToInt32(lblExtensionId.Tag)))
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;
                    if (objLeaveExtension.DeleteLeaveExtension(Convert.ToInt32(lblExtensionId.Tag)))
                    {
                        ShowErrorMessage(cboPeriod, 4, false, false);
                        AddNewLeaveExtension();
                    }
                    else
                        ShowErrorMessage(cboPeriod, 9132, false, false);
                }
            }
        }

        private void OKSaveButton_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {
                if (Validation())
                {
                    if (CboLeaveType.SelectedIndex == -1 && ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.LeaveExtension)//checking leave type entered for LeaveExtension
                        return;
                    int icmpid = objLeaveExtension.GetEmployeeCompany(Convert.ToInt32(EmployeeComboBox.SelectedValue));

                    if (objLeaveExtension.CheckLeavesTakenForEdit(DtpMonthYear.Value.ToString("dd-MMM-yyyy"), Convert.ToInt32(EmployeeComboBox.SelectedValue), icmpid, Convert.ToInt32(CboLeaveType.SelectedValue), Convert.ToDouble(NoOfDaysOrMinuteTextBox.Text)))
                    {
                        ShowErrorMessage(BtnSave, 10158, false, true);
                        return;
                    }
                    if (SaveDetail())
                    {
                        MChangeStatus = false;
                        this.Close();
                    }
                }
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            ShowReport();
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Leave Extension";
                    ObjEmailPopUp.EmailFormType = EmailFormID.LeaveExtension;
                    int icmpyid = objLeaveExtension.GetEmployeeCompany(Convert.ToInt32(EmployeeComboBox.SelectedValue));

                    ObjEmailPopUp.EmailSource = objLeaveExtension.ShowReport(Convert.ToInt32(EmployeeComboBox.SelectedValue), icmpyid);
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch { }
        }

        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void ReasonTextBox_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "LeaveExtension";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim() == "")
            {
                ShowErrorMessage(txtSearch, 17610, true, true);
                return;
            }
            strSearchString = txtSearch.Text.Trim();
            GetRecordCount();
            if (RecordCnt > 0)
            {
                BindingNavigatorMoveFirstItem_Click(null, null);

            }
            else
            {
                ShowErrorMessage(txtSearch, 40, true, true);
                AddNewLeaveExtension();
            }
        }

        private void TmStripClear_Tick(object sender, EventArgs e)
        {
            ToolStripStatusLabel1.Text = "";
            TmStripClear.Enabled = false;
            ErrorProviderLeave.Clear();
        }

        private void EmployeeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (EmployeeComboBox.SelectedIndex != -1)
            {
                LoadCombos(3);//cboPeriod
                LoadCombos(4);//LeaveType
            }
            else
            {
                DtpMonthYear.Value = DateTime.Now;
                //ExtensionTypeComboBox.SelectedIndex = -1;
                CboLeaveType.SelectedIndex = -1;
                NoOfDaysOrMinuteTextBox.Text = "";
                ReasonTextBox.Text = "";
                BtnPrint.Enabled = false;
                EmployeeComboBox.Text = "";
                //ExtensionTypeComboBox.Text = "";
                CboLeaveType.Text = "";
                TxtBalLeave.Text = "";
                //ExtensionTypeComboBox.Focus();
            }
            // Employee Dateof Joining Checking
            int intEmployeeID = Convert.ToInt32(EmployeeComboBox.SelectedValue);
            clsBLLLeaveExtension obj = new clsBLLLeaveExtension();
            if (intEmployeeID > 0)
            {
                DtpMonthYear.MinDate = obj.GetJoiningDate(intEmployeeID);
            }
            if (Convert.ToInt32(EmployeeComboBox.SelectedValue) > 0)
            {
                if (bFlag)
                    GetShiftandLeaveInfo();
            }
        }

        private void ExtensionTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.LeaveExtension)
            {
                lblPeriod.Visible = true;
                cboPeriod.Visible = true;
                lblMY.Visible = false;
                DtpMonthYear.Visible = false;
            }
            else
            {
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.TimeExtensionDay)
                //        lblMY.Text = "تاريخ";
                //    else if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.TimeExtensionMonth)
                //        lblMY.Text = "الشهر والسنة";
                //}
                //else
                //{
                    if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.TimeExtensionDay)
                        lblMY.Text = "Date";
                    else if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.TimeExtensionMonth)
                        lblMY.Text = "Month && Year";
                //}

                lblPeriod.Visible = false;
                cboPeriod.Visible = false;
                lblMY.Visible = true;
                DtpMonthYear.Visible = true;

                if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.TimeExtensionMonth)//Time extension
                {
                    string[] Str = new string[3];
                    Str = DtpMonthYear.Text.Split('-');
                    if (Str.Length < 3 && Str.Length > 1)
                    {
                        if (DtpMonthYear.MinDate.ToString("dd-mm-yyyy").ToDateTime() <= (DtpMonthYear.MinDate.Day.ToString() + "-" + Str[0] + "-" + Str[1] + " 11:59:59 PM").ToDateTime())
                        {
                            DtpMonthYear.Value = (DtpMonthYear.MinDate.Day.ToString() + "-" + Str[0] + "-" + Str[1] + " 11:59:59 PM").ToDateTime();
                        }
                    }
                }
            }
            if (MAddStatus)
                bFlag = true;
            if (Convert.ToInt32(ExtensionTypeComboBox.SelectedValue) > 0)
                GetShiftandLeaveInfo();
        }

        private void FrmLeaveExtension_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                        objHelp.strFormName = "LeaveExtension";
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        if (MblnAddPermission)
                            BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (MblnDeletePermission && BindingNavigatorDeleteItem.Enabled)
                            BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        if (MblnPrintEmailPermission && BtnPrint.Enabled)
                            BtnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        if (MblnPrintEmailPermission && BtnEmail.Enabled)
                            BtnEmail_Click(sender, new EventArgs());//Email
                        break;
                }
            }
            catch { }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch_Click(null, null);
            }
        }

        #endregion
        #region METHODS
        #region SetPermissions
        public void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.LeaveExtension, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            //if (ClsCommonSettings.IsHrPowerEnabled)
            //    MblnAddPermission = MblnUpdatePermission = MblnDeletePermission = false;

            BtnEmail.Enabled = BtnPrint.Enabled = MblnPrintEmailPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
        }
        #endregion SetPermissions

        #region LoadMessages
        public void LoadMessages()
        {
            MaMessageArr = new ArrayList();
            MaMessageArr = new ClsNotification().FillMessageArray((int)FormID.LeaveExtension, 2);
        }
        #endregion LoadMessages

        #region ChangeStatus
        private void Changestatus()
        {
            //function for changing status
            if (MAddStatus)
            {
                OKSaveButton.Enabled = MblnAddPermission;
                BtnSave.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
                CancelToolStripButton.Enabled = MblnAddPermission;
            }
            else
            {
                OKSaveButton.Enabled = MblnUpdatePermission;
                BtnSave.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            ErrorProviderLeave.Clear();
            MChangeStatus = true;

        }
        #endregion ChangeStatus

        #region GetRecordCount
        private void GetRecordCount()
        {
            RecordCnt = 0;
            RecordCnt = objLeaveExtension.GetRecordCount(strSearchString, MintEmployeeID);
            if (RecordCnt < 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                RecordCnt = 0;
            }
            else
            {
                BindingNavigatorCountItem.Text = strBindingOf + RecordCnt.ToString();
            }

        }
        #endregion GetRecordCount

        #region AddNewLeaveExtension
        public void AddNewLeaveExtension()
        {
            strSearchString = "";
            MintEmployeeID = 0;
            txtSearch.Text = "";
            intNavflg = 1;
            ClearFields();
            BtnEmail.Enabled = BtnPrint.Enabled = false;

            //if (ClsCommonSettings.IsArabicView)
            //    ToolStripStatusLabel1.Text = "ترك التمديد إضافة إجازة";
            //else
                ToolStripStatusLabel1.Text = "Add new leave extension";
            TmStripClear.Enabled = true;
            BtnPrint.Enabled = false;
            BtnEmail.Enabled = false;
            CboLeaveType.Enabled = true;
            MAddStatus = true;

            EnableDisableCombo();
            BindingNavigatorAddNewItem.Enabled = false;
            ErrorProviderLeave.Clear();
            ExtensionTypeComboBox.Focus();
            GetRecordCount();
            if (RecordCnt < 1)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorPositionItem.Enabled = false;
            }
            else
            {
                setBindingAddNew();
            }
            RecordCnt = RecordCnt + 1;
            CurrentRecCnt = RecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(RecordCnt) + "";
            BindingNavigatorPositionItem.Text = RecordCnt.ToString();
            TmStripClear.Enabled = true;
            intNavflg = 0;
            SetAutoCompleteList();
            EnableDisableButtons(true);
            bFlag = true;
        }
        #endregion AddNewLeaveExtension

        #region SetAutoCompleteList
        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsUtilities.GetAutoCompleteList(txtSearch.Text.Trim(), 0, "", "PayLeaveExtenstion");
            this.txtSearch.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt);
        }
        #endregion SetAutoCompleteList

        #region ClearFields
        public void ClearFields()
        {
            LoadCombos(0);
            TempStatus = true;
            ControlEnableDisable();
            lblExtensionId.Tag = "0";
            ErrorProviderLeave.Clear();
            DtpMonthYear.Value = DateTime.Now;
            EmployeeComboBox.SelectedIndex = -1;
            ExtensionTypeComboBox.SelectedIndex = -1;
            CboLeaveType.SelectedIndex = -1;
            NoOfDaysOrMinuteTextBox.Text = "";
            ReasonTextBox.Text = "";
            BtnPrint.Enabled = false;
            BtnEmail.Enabled = false;
            EmployeeComboBox.Text = "";
            ExtensionTypeComboBox.Text = "";
            CboLeaveType.Text = "";
            TxtBalLeave.Text = "";
            ExtensionTypeComboBox.Focus();
        }
        #endregion ClearFields

        #region ControlEnableDisable
        public void ControlEnableDisable()
        {
            ExtensionTypeComboBox.Enabled = TempStatus;
            CboLeaveType.Enabled = TempStatus;
            EmployeeComboBox.Enabled = TempStatus;
            cboPeriod.Enabled = TempStatus;
            DtpMonthYear.Enabled = TempStatus;
            BindingNavigatorDeleteItem.Enabled = TempStatus;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            EnableDisableCombo();
        }
        #endregion ControlEnableDisable

        #region EnableDisableCombo
        public void EnableDisableCombo()
        {
            if (MAddStatus)
            {
                ExtensionTypeComboBox.Enabled = true;
                EmployeeComboBox.Enabled = true;
            }
            else
            {
                ExtensionTypeComboBox.Enabled = false;
                EmployeeComboBox.Enabled = false;
            }
        }
        #endregion EnableDisableCombo

        #region setBindingAddNew
        public void setBindingAddNew()
        {
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = false;
            BindingNavigatorMoveLastItem.Enabled = false;
            BindingNavigatorPositionItem.Enabled = true;
            bFlag = true;
        }
        #endregion setBindingAddNew

        #region EnableDisableButtons
        public void EnableDisableButtons(bool bEnable)
        {
            if (bEnable)
            {
                BindingNavigatorSaveItem.Enabled = false;
                BtnSave.Enabled = false;
                OKSaveButton.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                BindingNavigatorAddNewItem.Enabled = false;
                CancelToolStripButton.Enabled = false;

                MainPanel.Enabled = true;
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                BtnSave.Enabled = false;
                OKSaveButton.Enabled = false;
                BindingNavigatorAddNewItem.Enabled = true;
                BindingNavigatorAddNewItem.Enabled = (MAddStatus) ? false : MblnAddPermission;
                CancelToolStripButton.Enabled = false;
            }
            else
            {
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;


                if (MblnAddPermission && MAddStatus)
                    CancelToolStripButton.Enabled = MblnAddPermission;      //For Set status of clear button
            }
        }
        #endregion EnableDisableButtons

        #region SetBindingNavigatorButtons
        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = RecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
        }
        #endregion SetBindingNavigatorButtons

        #region LoadCombos
        public void LoadCombos(int intType)
        {
            DataTable dtCombos = new DataTable();
            bComboFill = true;
            if (intType == 0 || intType == 1)//ExtensionType
            {
                dtCombos = null;
                //if (ClsCommonSettings.IsArabicView)
                //    dtCombos = objCommon.FillCombos("select LeaveExtensionTypeID,LeaveExtensionTypeArb AS LeaveExtensionType from PayLeaveExtensionTypeReference where LeaveExtensionTypeID in (1,2,3)");
                //else
                    dtCombos = objCommon.FillCombos("select LeaveExtensionTypeID,LeaveExtensionType from PayLeaveExtensionTypeReference where LeaveExtensionTypeID in (1,2,3)");
                ExtensionTypeComboBox.DisplayMember = "LeaveExtensionType";
                ExtensionTypeComboBox.ValueMember = "LeaveExtensionTypeID";
                ExtensionTypeComboBox.DataSource = dtCombos;
                ExtensionTypeComboBox.SelectedIndex = -1;

            }
            if (intType == 0 || intType == 2)//Employee
            {
                dtCombos = null;
                //if (ClsCommonSettings.IsArabicView)
                //    dtCombos = objCommon.FillCombos("SELECT   E.EmployeeID, " +
                //        "ISNULL(E.EmployeeFullNameArb, '')+ ' - ' + cast(E.EmployeeNumber as varchar) AS Employee " +
                //        "FROM  EmployeeMaster AS E WHERE E.WorkStatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") AND " +
                //        "LeavePolicyID IS NOT NULL ORDER BY E.FirstName");
                //else
                    dtCombos = objCommon.FillCombos("SELECT   E.EmployeeID, " +
                        "ISNULL(E.EmployeeFullName, '')+ ' - ' + cast(E.EmployeeNumber as varchar) AS Employee " +
                        "FROM  EmployeeMaster AS E WHERE E.WorkStatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") AND " +
                        "LeavePolicyID IS NOT NULL ORDER BY E.FirstName");
                EmployeeComboBox.DisplayMember = "Employee";
                EmployeeComboBox.ValueMember = "EmployeeID";
                EmployeeComboBox.DataSource = dtCombos;
                EmployeeComboBox.SelectedIndex = -1;

            }
            if (intType == 3)//Period
            {
                dtCombos = null;
                dtCombos = objCommon.FillCombos("select distinct ELS.FinYearStartDate,convert(varchar,ELS.FinYearStartDate,106) + ' - ' + convert(varchar,dateadd(year,1,dateadd(day,-1,ELS.FinYearStartDate)),106) " +
                        " as finperiod from PayEmployeeLeaveSummary ELS inner join EmployeeMaster EM on EM.EmployeeID=ELS.EmployeeID where ELS.EmployeeId=" + Convert.ToInt32(EmployeeComboBox.SelectedValue) + /*" and companyID=" + Convert.ToInt32(GetCompanyID(EmployeeComboBox.SelectedValue) +*/ " AND ELS.FinYearStartDate<= getDate() order by ELS.FinYearStartDate desc");
                cboPeriod.DisplayMember = "finperiod";
                cboPeriod.ValueMember = "FinYearStartDate";
                cboPeriod.DataSource = dtCombos;
                cboPeriod.SelectedIndex = -1;
            }
            if (intType == 4)//LeaveType
            {
                dtCombos = null;
                int intEmployeeID = Convert.ToInt32(EmployeeComboBox.SelectedValue);

                //if (ClsCommonSettings.IsArabicView)
                //{
                //    if (intEmployeeID > 0)
                //        dtCombos = objCommon.FillCombos("SELECT  L.LeaveTypeID, L.LeaveTypeArb AS LeaveType FROM PayLeavePolicyMaster AS CLM  INNER JOIN EmployeeMaster AS E ON CLM.LeavePolicyID = E.LeavePolicyID INNER JOIN PayLeavePolicyDetail AS CLD ON CLM.LeavePolicyID = CLD.LeavePolicyID  INNER JOIN PayLeaveTypeReference AS L ON CLD.LeaveTypeID = L.LeaveTypeID WHERE E.EmployeeID = " + intEmployeeID + " ORDER BY LeaveType");
                //    else
                //        dtCombos = objCommon.FillCombos("SELECT LeaveTypeID,LeaveTypeArb AS LeaveType FROM PayLeaveTypeReference");
                //}
                //else
                //{
                    if (intEmployeeID > 0)
                        dtCombos = objCommon.FillCombos("SELECT  L.LeaveTypeID, L.LeaveType FROM PayLeavePolicyMaster AS CLM  INNER JOIN EmployeeMaster AS E ON CLM.LeavePolicyID = E.LeavePolicyID INNER JOIN PayLeavePolicyDetail AS CLD ON CLM.LeavePolicyID = CLD.LeavePolicyID  INNER JOIN PayLeaveTypeReference AS L ON CLD.LeaveTypeID = L.LeaveTypeID WHERE E.EmployeeID = " + intEmployeeID + " ORDER BY LeaveType");
                    else
                //        dtCombos = objCommon.FillCombos("SELECT LeaveTypeID,LeaveType FROM PayLeaveTypeReference");
                //}
                CboLeaveType.DisplayMember = "LeaveType";
                CboLeaveType.ValueMember = "LeaveTypeID";
                CboLeaveType.DataSource = dtCombos;
                CboLeaveType.SelectedIndex = -1;
            }

            bComboFill = false;
        }
        #endregion LoadCombos

        #region ValidatingForNavigation
        public void ValidatingForNavigation()
        {
            if (CboLeaveType.SelectedValue != null && EmployeeComboBox.SelectedValue != null)
            {
                if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.LeaveExtension)
                    EndDate1 = cboPeriod.Text.Split('-').First();
                else
                    EndDate1 = DtpMonthYear.Value.ToString("dd-MMM-yyyy");
                DataTable dtTemp = objLeaveExtension.GetEmployeeLeaveSummary(Convert.ToInt32(EmployeeComboBox.SelectedValue), EndDate1, Convert.ToInt32(CboLeaveType.SelectedValue));
                if (dtTemp.Rows.Count > 0)
                {
                    if (Convert.ToDecimal(dtTemp.Rows[0]["TakenLeaves"]) > (Convert.ToDecimal(dtTemp.Rows[0]["BalanceLeaves"]) + Convert.ToDecimal(dtTemp.Rows[0]["CarryForwardLeaves"])))
                        TempStatus = false;
                    else
                        TempStatus = true;
                }
                else
                    TempStatus = true;

            }
            ControlEnableDisable();
        }
        #endregion ValidatingForNavigation

        #region GetShiftandLeaveInfo
        public void GetShiftandLeaveInfo()
        {
            if (bFlag == false)
                return;
            bFlag = true;

            //Day extension
            if (Convert.ToInt32(ExtensionTypeComboBox.SelectedValue) == (int)LeaveExtensionType.LeaveExtension)
            {
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    lblBal.Text = "التوازن اترك";
                //    lblTimeDay.Text = "عدد الأيام";
                //}
                //else
                //{
                    lblBal.Text = "Balance Leave";
                    lblTimeDay.Text = "Number of Days";
                //}
                
                DtpMonthYear.CustomFormat = "MMM-yyyy";                
                TxtBalLeave.Text = "0";
                CboLeaveType.Enabled = true;
                LoadCombos(4);
                if (DtpMonthYear.MinDate <= (Convert.ToDateTime(DtpMonthYear.Value.ToString("01 MMM yyyy"))))
                    DtpMonthYear.Value = Convert.ToDateTime(DtpMonthYear.Value.ToString("01/MMM/yyyy"));
                CboLeaveType.SelectedIndex = -1;
            }
            //Time Extension
            else if (Convert.ToInt32(ExtensionTypeComboBox.SelectedValue) == (int)LeaveExtensionType.TimeExtensionDay)
            {
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    lblBal.Text = "تحول المدة";
                //    lblTimeDay.Text = "(انقطاع المدة (الحد الأدنى";
                //}
                //else
                //{
                    lblBal.Text = "Shift Duration";
                    lblTimeDay.Text = "Break Duration(Min)";
                //}
                DtpMonthYear.CustomFormat = "dd-MMM-yyyy";
                CboLeaveType.Enabled = false;
                CboLeaveType.SelectedIndex = -1;
                CboLeaveType.Text = "";
                TxtBalLeave.Text = "0";

                if (EmployeeComboBox.SelectedValue != null && Convert.ToInt32(EmployeeComboBox.SelectedValue) != 0)
                {
                    clsBLLLeaveExtension obj = new clsBLLLeaveExtension();
                    int intDayID = 0;
                    if ((Convert.ToInt32(DtpMonthYear.Value.DayOfWeek) + 1) > 7)
                        intDayID = 1;
                    else
                        intDayID = (Convert.ToInt32(DtpMonthYear.Value.DayOfWeek) + 1);
                    if (obj.CheckIfPolicyExists(Convert.ToInt32(EmployeeComboBox.SelectedValue), intDayID, DtpMonthYear.Value))
                    {
                        TxtBalLeave.Text = obj.GetShiftDuration(Convert.ToInt32(EmployeeComboBox.SelectedValue), intDayID, DtpMonthYear.Value);
                    }
                    else
                        TmStripClear.Enabled = true;
                }
            }
        }
        #endregion GetShiftandLeaveInfo

        #region ValidateForm
        public bool ValidateForm()
        {
            ErrorProviderLeave.Clear();
            int StatusFlg;


            if (ExtensionTypeComboBox.SelectedIndex == -1)//extension type
            {
                ShowErrorMessage(ExtensionTypeComboBox, 10163, true, true);
                return false;
            }
            if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.LeaveExtension)
            {
                NoOfDaysOrMinuteTextBox.Text = NoOfDaysOrMinuteTextBox.Text.Trim().ToDecimal().ToString();
            }
            else
            {
                NoOfDaysOrMinuteTextBox.Text = NoOfDaysOrMinuteTextBox.Text.Trim().ToInt32().ToString();
            }
            if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.LeaveExtension)
                DtpMonthYear.MinDate = "01-Jan-1900".ToDateTime();

            if (EmployeeComboBox.SelectedIndex == -1)//Employee
            {
                ShowErrorMessage(EmployeeComboBox, 9124, true, true);
                return false;
            }
            if ((cboPeriod.SelectedIndex == -1 || cboPeriod.Text == "") && ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.LeaveExtension)//Period
            {
                ShowErrorMessage(cboPeriod, 10169, true, true);
                return false;
            }
            if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.LeaveExtension)
                EndDate1 = cboPeriod.Text.Split('-').Last();
            else
                EndDate1 = DtpMonthYear.Value.ToString("dd-MMM-yyyy");

            if (objLeaveExtension.CheckPaymentRelease(Convert.ToInt32(EmployeeComboBox.SelectedValue), EndDate1, 0))//Salary Release
            {
                //Entry must be greater than salary release date
                ShowErrorMessage(cboPeriod, 10154, true, true);
                return false;
            }

            if (Convert.ToInt32(ExtensionTypeComboBox.SelectedValue) == (int)LeaveExtensionType.LeaveExtension)
            {
                if (CboLeaveType.SelectedIndex == -1)//'Leave type'.
                {
                    ShowErrorMessage(CboLeaveType, 10164, true, true);
                    return false;
                }
            }

            if (MAddStatus)
                StatusFlg = 1;
            else
                StatusFlg = 2;
            if (lblExtensionId.Tag.ToString() == "")
                lblExtensionId.Tag = "0";

            if (Convert.ToInt32(ExtensionTypeComboBox.SelectedValue) == (int)LeaveExtensionType.LeaveExtension)
            {
                if (objLeaveExtension.CheckLeaveDuplicate(DtpMonthYear.Value.ToString("dd-MMM-yyyy"), Convert.ToInt32(EmployeeComboBox.SelectedValue), Convert.ToInt32(CboLeaveType.SelectedValue), Convert.ToInt32(ExtensionTypeComboBox.SelectedValue), StatusFlg, Convert.ToInt32(lblExtensionId.Tag)))
                {
                    //duplicate values are not allowed.
                    ShowErrorMessage(ExtensionTypeComboBox, 10166, true, true);
                    ////Carry forward leave cannot be greater than the total number of leaves.
                    //ShowErrorMessage(NoOfDaysOrMinuteTextBox, 10165, true, true);
                    return false;
                }
            }
            else
            {
                if (objLeaveExtension.CheckTimeDuplicate(DtpMonthYear.Value.ToString("dd-MMM-yyyy"), Convert.ToInt32(EmployeeComboBox.SelectedValue), Convert.ToInt32(ExtensionTypeComboBox.SelectedValue), StatusFlg, Convert.ToInt32(lblExtensionId.Tag)))
                {
                    //duplicate values are not allowed.
                    ShowErrorMessage(ExtensionTypeComboBox, 10166, true, true);
                    return false;
                }

            }

            int MaxDaysInMonth = 0;
            DateTime BeginDate, EndDate;
            BeginDate = Convert.ToDateTime(DtpMonthYear.Value.ToString("01/MMM/yyyy"));
            EndDate = BeginDate.AddMonths(1).AddDays(-1);
            MaxDaysInMonth = EndDate.Day; //---------------Max days in a Month

            if (Convert.ToInt32(ExtensionTypeComboBox.SelectedValue) == (int)LeaveExtensionType.LeaveExtension)
            {
                if (NoOfDaysOrMinuteTextBox.Text.Trim() == "" || NoOfDaysOrMinuteTextBox.Text.Trim() == "0")
                {
                    //number of days.
                    ShowErrorMessage(NoOfDaysOrMinuteTextBox, 10167, true, true);
                    return false;
                }


                if (365 < NoOfDaysOrMinuteTextBox.Text.Trim().ToDecimal())
                {
                    //No Of Days should be less than 365 
                    ShowErrorMessage(NoOfDaysOrMinuteTextBox, 10150, true, true);
                    return false;

                }
            }
            else if (Convert.ToInt32(ExtensionTypeComboBox.SelectedValue) == (int)LeaveExtensionType.TimeExtensionDay || Convert.ToInt32(ExtensionTypeComboBox.SelectedValue) == (int)LeaveExtensionType.TimeExtensionMonth)
            {
                if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.LeaveExtension)
                    EndDate1 = cboPeriod.Text.Split('-').Last();
                else
                    EndDate1 = DtpMonthYear.Value.ToString("dd-MMM-yyyy");

                if (objLeaveExtension.CheckPaymentRelease(Convert.ToInt32(EmployeeComboBox.SelectedValue), EndDate1, 0))//Salary Release
                {
                    ShowErrorMessage(cboPeriod, 10154, true, true);
                    return false;
                }
            }

            return true;

        }
        #endregion ValidateForm

        #region Validation
        public bool Validation()
        {
            if (ExtensionTypeComboBox.SelectedValue.ToInt32() != (int)LeaveExtensionType.LeaveExtension)
            {
                DataTable dtEmployee = objLeaveExtension.GetEmployeesWithLeaveExtension(Convert.ToInt32(lblExtensionId.Tag));
                if (dtEmployee.Rows.Count > 0)
                {
                    if (objLeaveExtension.CheckPaymentRelease(Convert.ToInt32(dtEmployee.Rows[0]["EmployeeID"]), Convert.ToDateTime(dtEmployee.Rows[0]["MonthYear"]).ToString("dd-MMM-yyyy"), 0))
                    {
                        ShowErrorMessage(cboPeriod, 10159, false, false);
                        return false;
                    }
                }

                if (NoOfDaysOrMinuteTextBox.Text.IsInteger())
                {
                    if (Convert.ToDouble(NoOfDaysOrMinuteTextBox.Text) <= 0)
                    {
                        ShowErrorMessage(NoOfDaysOrMinuteTextBox, 10161, false, false);
                        return false;
                    }
                    if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.TimeExtensionDay)
                    {
                        if (Convert.ToDouble(NoOfDaysOrMinuteTextBox.Text) > TxtBalLeave.Text.Trim().ToInt32())
                        {
                            ShowErrorMessage(NoOfDaysOrMinuteTextBox, 10153, false, false);
                            return false;
                        }
                        if (DtpMonthYear.Value.Date > ClsCommonSettings.GetServerDate())
                        {
                            ShowErrorMessage(NoOfDaysOrMinuteTextBox, 17621, false, false);
                            return false;
                        }
                    }
                }
                else
                {
                    ShowErrorMessage(NoOfDaysOrMinuteTextBox, 10161, false, false);
                    return false;
                }
            }
            else
            {
                if (NoOfDaysOrMinuteTextBox.Text.Trim().ToDecimal() <= 0)
                {
                    ShowErrorMessage(NoOfDaysOrMinuteTextBox, 10160, false, false);
                    return false;
                }
            }
            return true;
        }
        #endregion Validation

        #region ShowErrorMessage
        private void ShowErrorMessage(Control ctrl, int ErrorCode, bool ShowErrProvider, bool ShowStatus)
        {
            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, ErrorCode, out MmessageIcon);

            if (ShowErrProvider)
                ErrorProviderLeave.SetError(ctrl, MsMessageCommon.Replace("#", "").Trim());
            if (ShowStatus)
            {
                ToolStripStatusLabel1.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf('#') + 1).Trim();
                TmStripClear.Enabled = true;
            }
            else
                ToolStripStatusLabel1.Text = "";

            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);

            ctrl.Focus();
        }
        #endregion ShowErrorMessage

        #region ShowBalanceLeave
        public void ShowBalanceLeave()
        {
            string testdate;
            int iLeavepolicyID;
            if (!bComboFill)
            {
                if (Convert.ToInt32(ExtensionTypeComboBox.SelectedValue) != (int)LeaveExtensionType.LeaveExtension)
                    return;
                if (Convert.ToInt32(EmployeeComboBox.SelectedValue) > 0)
                {
                    int iCmpId = objLeaveExtension.GetEmployeeCompany(Convert.ToInt32(EmployeeComboBox.SelectedValue));
                    if (CboLeaveType.SelectedIndex != -1 && iCmpId > 0 && EmployeeComboBox.SelectedIndex != -1)
                    {
                        testdate = objLeaveExtension.GetFinYearStartDate(iCmpId, DtpMonthYear.Value.ToString("dd-MMM-yyyy"));
                        iLeavepolicyID = objLeaveExtension.GetLeavePolicy(Convert.ToInt32(EmployeeComboBox.SelectedValue));
                        DataTable dtLeave = objLeaveExtension.GetBalanceLeave(Convert.ToInt32(EmployeeComboBox.SelectedValue), iCmpId, testdate, iLeavepolicyID, Convert.ToInt32(CboLeaveType.SelectedValue));
                        if (dtLeave.Rows.Count > 0)
                        {
                            if (Convert.ToDecimal(dtLeave.Rows[0]["BalanceLeaves"]) < 0)
                                TxtBalLeave.Text = "0";
                            else
                                TxtBalLeave.Text = Convert.ToDecimal(dtLeave.Rows[0]["BalanceLeaves"]).ToString("0.0");
                        }
                    }
                }
            }
        }
        #endregion ShowBalanceLeave

        #region DisplayLeaveExtension
        private void DisplayLeaveExtension()
        {
            //Settings to display Leave Extension
            bFlag = false;
            LoadCombos(0);
            DataTable dtCombos = new DataTable();
            //if (ClsCommonSettings.IsArabicView)
            //    dtCombos = objCommon.FillCombos("SELECT LeaveTypeID,LeaveTypeArb AS LeaveType FROM PayLeaveTypeReference");
            //else
                dtCombos = objCommon.FillCombos("SELECT LeaveTypeID,LeaveType FROM PayLeaveTypeReference");
            CboLeaveType.DisplayMember = "LeaveType";
            CboLeaveType.ValueMember = "LeaveTypeID";
            CboLeaveType.DataSource = dtCombos;

            DisplayInfo();

            BindingNavigatorPositionItem.Text = CurrentRecCnt.ToString();
            ImageFlag = false;
            ErrorProviderLeave.Clear();
            MAddStatus = false;
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            BtnEmail.Enabled = BtnPrint.Enabled = MblnPrintEmailPermission;
            CboLeaveType.Enabled = false;
            MChangeStatus = false;
            OKSaveButton.Enabled = MChangeStatus;
            BtnSave.Enabled = MChangeStatus;
            MAddStatus = false;
            TmStripClear.Enabled = true;
            EnableDisableButtons(true);
            EnableDisableButtons(false);
            SetBindingNavigatorButtons();
            ValidatingForNavigation();
            UpdatingNullValues();
        }
        #endregion

        #region DisplayInfo
        public void DisplayInfo()
        {
            DataTable dtLeaveExtension = objLeaveExtension.GetLeaveExtensionDetails(CurrentRecCnt, Convert.ToInt32(ExtensionTypeComboBox.SelectedValue), strSearchString, MintEmployeeID);
            if (dtLeaveExtension.Rows.Count > 0)
            {
                intNavflg = 1;
                lblExtensionId.Tag = dtLeaveExtension.Rows[0]["ExtensionID"];
                EmployeeComboBox.DisplayMember = "Employee";
                EmployeeComboBox.ValueMember = "EmployeeID";
                //if (ClsCommonSettings.IsArabicView)
                //    EmployeeComboBox.DataSource = objCommon.FillCombos("SELECT  E.EmployeeID, " +
                //        "ISNULL(E.EmployeeFullNameArb, '')+ ' - ' + cast(E.EmployeeNumber as varchar) AS Employee " +
                //        "FROM  EmployeeMaster AS E   WHERE E.WorkStatusID >= 6 AND E.CompanyID = " + ClsCommonSettings.CurrentCompanyID + " " +
                //        "UNION SELECT   E.EmployeeID, ISNULL(E.EmployeeFullNameArb, '')+ ' - ' + cast(E.EmployeeNumber as varchar) AS Employee " +
                //        "FROM  EmployeeMaster AS E  WHERE  E.EmployeeID = " + Convert.ToInt32(dtLeaveExtension.Rows[0]["EmployeeID"]) + " ORDER BY Employee");
                //else
                    EmployeeComboBox.DataSource = objCommon.FillCombos("SELECT  E.EmployeeID, " +
                        "ISNULL(E.EmployeeFullName, '')+ ' - ' + cast(E.EmployeeNumber as varchar) AS Employee " +
                        "FROM  EmployeeMaster AS E   WHERE E.WorkStatusID >= 6 AND E.CompanyID = " + ClsCommonSettings.LoginCompanyID + " " +
                        "UNION SELECT   E.EmployeeID, ISNULL(E.EmployeeFullName, '')+ ' - ' + cast(E.EmployeeNumber as varchar) AS Employee " +
                        "FROM  EmployeeMaster AS E  WHERE  E.EmployeeID = " + Convert.ToInt32(dtLeaveExtension.Rows[0]["EmployeeID"]) + " ORDER BY Employee");
                bFlag = true;
                EmployeeComboBox.SelectedValue = dtLeaveExtension.Rows[0]["EmployeeID"];
                ExtensionTypeComboBox.SelectedValue = dtLeaveExtension.Rows[0]["LeaveExtensionTypeID"];

                if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.LeaveExtension)
                {
                    lblPeriod.Visible = true;
                    cboPeriod.Visible = true;
                    lblMY.Visible = false;
                    DtpMonthYear.Visible = false;
                    cboPeriod.SelectedValue = dtLeaveExtension.Rows[0]["MonthYear"];
                }
                else
                {
                    lblPeriod.Visible = false;
                    cboPeriod.Visible = false;
                    lblMY.Visible = true;
                    DtpMonthYear.Visible = true;
                    cboPeriod.SelectedIndex = -1;
                    if (ExtensionTypeComboBox.SelectedValue.ToInt32() == (int)LeaveExtensionType.TimeExtensionMonth)
                    {
                        string[] Str = DtpMonthYear.Text.Split('-');
                        if (Str.Length < 3 && Str.Length > 1)
                        {

                            if (DtpMonthYear.MinDate <= Convert.ToDateTime(DtpMonthYear.MinDate.Day.ToString() + "-" + Str[0] + "-" + Str[1] + " 11:59:59 PM"))
                                DtpMonthYear.Value = Convert.ToDateTime(DtpMonthYear.MinDate.Day.ToString() + "-" + Str[0] + "-" + Str[1] + " 11:59:59 PM");

                        }
                    }
                }

                //if (ClsCommonSettings.IsArabicView)
                //{
                //    if (Convert.ToInt32(ExtensionTypeComboBox.SelectedValue) == (int)LeaveExtensionType.LeaveExtension)
                //        lblTimeDay.Text = "عدد الأيام";
                //    else
                //        lblTimeDay.Text = "(انقطاع المدة (الحد الأدنى";
                //}
                //else
                //{
                    if (Convert.ToInt32(ExtensionTypeComboBox.SelectedValue) == (int)LeaveExtensionType.LeaveExtension)
                        lblTimeDay.Text = "Number of Days";
                    else
                        lblTimeDay.Text = "Break Duration(Min)";
                //}

                if (Convert.ToInt32(ExtensionTypeComboBox.SelectedValue) == (int)LeaveExtensionType.LeaveExtension)
                    DtpMonthYear.MinDate = "01-Jan-1900".ToDateTime();

                if (DtpMonthYear.MinDate <= dtLeaveExtension.Rows[0]["MonthYear"].ToDateTime())
                    DtpMonthYear.Value = dtLeaveExtension.Rows[0]["MonthYear"].ToDateTime();
                LoadCombos(4);//Leavetype combo
                CboLeaveType.SelectedValue = dtLeaveExtension.Rows[0]["LeaveTypeID"];
                if (Convert.ToInt32(ExtensionTypeComboBox.SelectedValue) == (int)LeaveExtensionType.LeaveExtension)
                {
                    NoOfDaysOrMinuteTextBox.Text = Convert.ToString(dtLeaveExtension.Rows[0]["NoOfDays"].ToDecimal());
                }
                else
                {
                    NoOfDaysOrMinuteTextBox.Text = dtLeaveExtension.Rows[0]["NoOfDays"].ToDecimal().ToString("0");
                }
                ReasonTextBox.Text = Convert.ToString(dtLeaveExtension.Rows[0]["Reason"]);
                ShowBalanceLeave();
                BtnEmail.Enabled = BtnPrint.Enabled = MblnPrintEmailPermission;
                intNavflg = 0;
            }
        }
        #endregion DisplayInfo

        #region UpdatingNullValues
        public void UpdatingNullValues()
        {
            //Enabling / Disabling controls according to the combobox value
            bool Status = true;

            if (ExtensionTypeComboBox.SelectedValue != null)
            {
                if (ExtensionTypeComboBox.SelectedValue.ToString() == "")
                {
                    LoadCombos(1);
                    ExtensionTypeComboBox.SelectedValue = (int)LeaveExtensionType.LeaveExtension;
                    Status = false;
                }
                else
                    Status = true;
            }
            if (Status)
            {
                MainPanel.Enabled = true;
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = TempStatus;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BtnSave.Enabled = false;
                OKSaveButton.Enabled = false;
                BindingNavigatorAddNewItem.Enabled = true;
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            }
            else
            {
                MainPanel.Enabled = false;
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                BtnSave.Enabled = false;
                OKSaveButton.Enabled = false;
                BindingNavigatorAddNewItem.Enabled = true;
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            }
            if (Convert.ToInt32(ExtensionTypeComboBox.SelectedValue) != (int)LeaveExtensionType.LeaveExtension)
                CboLeaveType.Enabled = false;

        }
        #endregion UpdatingNullValues

        #region SaveDetail
        public bool SaveDetail()
        {
            if (MAddStatus)
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);
            else
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
            if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                return false;

            if (MAddStatus)
                objLeaveExtension.PobjDTOLeaveExtension.ExtensionID = 0;
            else
                objLeaveExtension.PobjDTOLeaveExtension.ExtensionID = Convert.ToInt32(lblExtensionId.Tag);

            objLeaveExtension.PobjDTOLeaveExtension.EmployeeID = Convert.ToInt32(EmployeeComboBox.SelectedValue);
            objLeaveExtension.PobjDTOLeaveExtension.LeaveExtensionTypeID = Convert.ToInt32(ExtensionTypeComboBox.SelectedValue);
            objLeaveExtension.PobjDTOLeaveExtension.MonthYear = DtpMonthYear.Value.ToString("dd-MMM-yyyy");
            objLeaveExtension.PobjDTOLeaveExtension.LeaveTypeID = Convert.ToInt32(CboLeaveType.SelectedValue);
            objLeaveExtension.PobjDTOLeaveExtension.EntryDate = DtpMonthYear.Value.ToString("dd-MMM-yyyy");
            objLeaveExtension.PobjDTOLeaveExtension.NoOfDays = NoOfDaysOrMinuteTextBox.Text.Trim().ToDecimal();
            objLeaveExtension.PobjDTOLeaveExtension.AdditionalMinutes = NoOfDaysOrMinuteTextBox.Text.Trim().ToDecimal();
            objLeaveExtension.PobjDTOLeaveExtension.Reason = ReasonTextBox.Text;

            if (objLeaveExtension.SaveLeaveExtension(MAddStatus))
                ClearFields();
            return true;

        }
        #endregion SaveDetail

        #region ShowReport
        public void ShowReport()
        {
            int icmpid = objLeaveExtension.GetEmployeeCompany(Convert.ToInt32(EmployeeComboBox.SelectedValue));
            FrmReportviewer ObjViewer = new FrmReportviewer();
            ObjViewer.PsFormName = "Leave Extension";
            ObjViewer.PintCompany = icmpid;
            ObjViewer.PsExecutive = EmployeeComboBox.Text;
            ObjViewer.PintExecutive = Convert.ToInt32(EmployeeComboBox.SelectedValue);
            ObjViewer.PiFormID = (int)FormID.LeaveExtension;
            ObjViewer.ShowDialog();
        }
        #endregion ShowReport

        #endregion

    }
}
