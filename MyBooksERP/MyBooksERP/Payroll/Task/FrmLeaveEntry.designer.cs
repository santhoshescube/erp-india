﻿namespace MyBooksERP
{
    partial class FrmLeaveEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label LeaveTypeIDLabel;
            System.Windows.Forms.Label EmployeeIDLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLeaveEntry));
            this.BtnSave = new System.Windows.Forms.Button();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.BtnOk = new System.Windows.Forms.Button();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorCancelItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.TmFocus = new System.Windows.Forms.Timer(this.components);
            this.ErrorProviderEmpLeave = new System.Windows.Forms.ErrorProvider(this.components);
            this.TimerLeave = new System.Windows.Forms.Timer(this.components);
            this.EmployeeLeaveDetailsBindingSourceRpt = new System.Windows.Forms.BindingSource(this.components);
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.EmployeeLeaveDetailsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnMoreActions = new System.Windows.Forms.ToolStripDropDownButton();
            this.BtnLeaveExtension = new System.Windows.Forms.ToolStripMenuItem();
            this.Label1 = new System.Windows.Forms.Label();
            this.RemarksTextBox = new System.Windows.Forms.TextBox();
            this.CboEmployee = new System.Windows.Forms.ComboBox();
            this.CboLeaveType = new System.Windows.Forms.ComboBox();
            this.HalfDayCheckBox = new System.Windows.Forms.CheckBox();
            this.LblBalLeave = new System.Windows.Forms.Label();
            this.LblDateTo = new System.Windows.Forms.Label();
            this.LblDateFrom = new System.Windows.Forms.Label();
            this.LblRejoinDate = new System.Windows.Forms.Label();
            this.LeaveDateFromDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.lblJoiningDateTag = new System.Windows.Forms.Label();
            this.LblDOJ = new System.Windows.Forms.Label();
            this.LeaveDateToDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.RejoinDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.lblnoofholidays = new System.Windows.Forms.Label();
            this.chkPaidUnPaid = new System.Windows.Forms.CheckBox();
            this.GrpMain = new System.Windows.Forms.GroupBox();
            this.LblBalLeaveColn = new System.Windows.Forms.Label();
            this.lblnoofholidaysColn = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblholidaysValue = new System.Windows.Forms.Label();
            this.lblBalLeaveValue = new System.Windows.Forms.Label();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            LeaveTypeIDLabel = new System.Windows.Forms.Label();
            EmployeeIDLabel = new System.Windows.Forms.Label();
            this.StatusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderEmpLeave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeLeaveDetailsBindingSourceRpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeLeaveDetailsBindingNavigator)).BeginInit();
            this.EmployeeLeaveDetailsBindingNavigator.SuspendLayout();
            this.GrpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // LeaveTypeIDLabel
            // 
            LeaveTypeIDLabel.AutoSize = true;
            LeaveTypeIDLabel.Location = new System.Drawing.Point(13, 60);
            LeaveTypeIDLabel.Name = "LeaveTypeIDLabel";
            LeaveTypeIDLabel.Size = new System.Drawing.Size(64, 13);
            LeaveTypeIDLabel.TabIndex = 50;
            LeaveTypeIDLabel.Text = "Leave Type";
            // 
            // EmployeeIDLabel
            // 
            EmployeeIDLabel.AutoSize = true;
            EmployeeIDLabel.Location = new System.Drawing.Point(13, 25);
            EmployeeIDLabel.Name = "EmployeeIDLabel";
            EmployeeIDLabel.Size = new System.Drawing.Size(53, 13);
            EmployeeIDLabel.TabIndex = 2;
            EmployeeIDLabel.Text = "Employee";
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(7, 322);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 1;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(392, 322);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 3;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(311, 322);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 2;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // EmployeeLeaveDetailsBindingNavigatorSaveItem
            // 
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("EmployeeLeaveDetailsBindingNavigatorSaveItem.Image")));
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem.Name = "EmployeeLeaveDetailsBindingNavigatorSaveItem";
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem.Text = "Save Data";
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem.Click += new System.EventHandler(this.EmployeeLeaveDetailsBindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorCancelItem
            // 
            this.BindingNavigatorCancelItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorCancelItem.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.BindingNavigatorCancelItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BindingNavigatorCancelItem.Name = "BindingNavigatorCancelItem";
            this.BindingNavigatorCancelItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorCancelItem.Text = "Clear";
            this.BindingNavigatorCancelItem.Click += new System.EventHandler(this.BindingNavigatorCancelItem_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.lblstatus});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 352);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(474, 22);
            this.StatusStrip1.TabIndex = 84;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(45, 17);
            this.StatusLabel.Text = "Status :";
            this.StatusLabel.ToolTipText = "Status";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // TmFocus
            // 
            this.TmFocus.Tick += new System.EventHandler(this.TmFocus_Tick);
            // 
            // ErrorProviderEmpLeave
            // 
            this.ErrorProviderEmpLeave.ContainerControl = this;
            this.ErrorProviderEmpLeave.RightToLeft = true;
            // 
            // TimerLeave
            // 
            this.TimerLeave.Interval = 2000;
            this.TimerLeave.Tick += new System.EventHandler(this.TimerLeave_Tick);
            // 
            // EmployeeLeaveDetailsBindingSourceRpt
            // 
            this.EmployeeLeaveDetailsBindingSourceRpt.DataMember = "EmployeeLeaveDetails";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Enabled = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            this.BindingNavigatorPositionItem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.BindingNavigatorPositionItem_KeyPress);
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // EmployeeLeaveDetailsBindingNavigator
            // 
            this.EmployeeLeaveDetailsBindingNavigator.AddNewItem = null;
            this.EmployeeLeaveDetailsBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.EmployeeLeaveDetailsBindingNavigator.DeleteItem = null;
            this.EmployeeLeaveDetailsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorCancelItem,
            this.ToolStripSeparator1,
            this.bnMoreActions,
            this.ToolStripSeparator2,
            this.BtnPrint,
            this.BtnEmail,
            this.ToolStripSeparator3,
            this.BtnHelp});
            this.EmployeeLeaveDetailsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.EmployeeLeaveDetailsBindingNavigator.MoveFirstItem = null;
            this.EmployeeLeaveDetailsBindingNavigator.MoveLastItem = null;
            this.EmployeeLeaveDetailsBindingNavigator.MoveNextItem = null;
            this.EmployeeLeaveDetailsBindingNavigator.MovePreviousItem = null;
            this.EmployeeLeaveDetailsBindingNavigator.Name = "EmployeeLeaveDetailsBindingNavigator";
            this.EmployeeLeaveDetailsBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.EmployeeLeaveDetailsBindingNavigator.Size = new System.Drawing.Size(474, 25);
            this.EmployeeLeaveDetailsBindingNavigator.TabIndex = 79;
            this.EmployeeLeaveDetailsBindingNavigator.Text = "BindingNavigator1";
            // 
            // bnMoreActions
            // 
            this.bnMoreActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnLeaveExtension});
            this.bnMoreActions.Image = global::MyBooksERP.Properties.Resources.Add1;
            this.bnMoreActions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnMoreActions.Name = "bnMoreActions";
            this.bnMoreActions.Size = new System.Drawing.Size(76, 22);
            this.bnMoreActions.Text = "Actions";
            this.bnMoreActions.ToolTipText = "Actions";
            // 
            // BtnLeaveExtension
            // 
            this.BtnLeaveExtension.Name = "BtnLeaveExtension";
            this.BtnLeaveExtension.Size = new System.Drawing.Size(157, 22);
            this.BtnLeaveExtension.Text = "Leave Extension";
            this.BtnLeaveExtension.ToolTipText = "Leave Extension";
            this.BtnLeaveExtension.Click += new System.EventHandler(this.BtnLeaveExtension_Click);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(7, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(68, 13);
            this.Label1.TabIndex = 86;
            this.Label1.Text = "Leave Info";
            // 
            // RemarksTextBox
            // 
            this.RemarksTextBox.Location = new System.Drawing.Point(83, 157);
            this.RemarksTextBox.MaxLength = 500;
            this.RemarksTextBox.Multiline = true;
            this.RemarksTextBox.Name = "RemarksTextBox";
            this.RemarksTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.RemarksTextBox.Size = new System.Drawing.Size(365, 90);
            this.RemarksTextBox.TabIndex = 6;
            this.RemarksTextBox.TextChanged += new System.EventHandler(this.RemarksTextBox_TextChanged);
            // 
            // CboEmployee
            // 
            this.CboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.CboEmployee.DropDownHeight = 134;
            this.CboEmployee.FormattingEnabled = true;
            this.CboEmployee.IntegralHeight = false;
            this.CboEmployee.Location = new System.Drawing.Point(83, 22);
            this.CboEmployee.Name = "CboEmployee";
            this.CboEmployee.Size = new System.Drawing.Size(198, 21);
            this.CboEmployee.TabIndex = 0;
            this.CboEmployee.Leave += new System.EventHandler(this.CboEmployee_Leave);
            this.CboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboEmployee_KeyPress);
            this.CboEmployee.SelectedValueChanged += new System.EventHandler(this.CboEmployee_SelectedValueChanged);
            // 
            // CboLeaveType
            // 
            this.CboLeaveType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboLeaveType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboLeaveType.BackColor = System.Drawing.SystemColors.Info;
            this.CboLeaveType.DropDownHeight = 134;
            this.CboLeaveType.FormattingEnabled = true;
            this.CboLeaveType.IntegralHeight = false;
            this.CboLeaveType.Location = new System.Drawing.Point(83, 57);
            this.CboLeaveType.Name = "CboLeaveType";
            this.CboLeaveType.Size = new System.Drawing.Size(198, 21);
            this.CboLeaveType.TabIndex = 1;
            this.CboLeaveType.SelectionChangeCommitted += new System.EventHandler(this.CboLeaveType_SelectionChangeCommitted);
            this.CboLeaveType.Leave += new System.EventHandler(this.CboLeaveType_Leave);
            this.CboLeaveType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboLeaveType_KeyPress);
            // 
            // HalfDayCheckBox
            // 
            this.HalfDayCheckBox.AutoSize = true;
            this.HalfDayCheckBox.Location = new System.Drawing.Point(83, 92);
            this.HalfDayCheckBox.Name = "HalfDayCheckBox";
            this.HalfDayCheckBox.Size = new System.Drawing.Size(67, 17);
            this.HalfDayCheckBox.TabIndex = 2;
            this.HalfDayCheckBox.Text = "Half Day";
            this.HalfDayCheckBox.UseVisualStyleBackColor = true;
            this.HalfDayCheckBox.CheckedChanged += new System.EventHandler(this.HalfDayCheckBox_CheckedChanged);
            // 
            // LblBalLeave
            // 
            this.LblBalLeave.AutoSize = true;
            this.LblBalLeave.Location = new System.Drawing.Point(287, 60);
            this.LblBalLeave.Name = "LblBalLeave";
            this.LblBalLeave.Size = new System.Drawing.Size(79, 13);
            this.LblBalLeave.TabIndex = 52;
            this.LblBalLeave.Text = "Balance Leave";
            this.LblBalLeave.VisibleChanged += new System.EventHandler(this.LblBalLeave_VisibleChanged);
            // 
            // LblDateTo
            // 
            this.LblDateTo.AutoSize = true;
            this.LblDateTo.Location = new System.Drawing.Point(218, 124);
            this.LblDateTo.Name = "LblDateTo";
            this.LblDateTo.Size = new System.Drawing.Size(46, 13);
            this.LblDateTo.TabIndex = 54;
            this.LblDateTo.Text = "Date To";
            // 
            // LblDateFrom
            // 
            this.LblDateFrom.AutoSize = true;
            this.LblDateFrom.Location = new System.Drawing.Point(13, 124);
            this.LblDateFrom.Name = "LblDateFrom";
            this.LblDateFrom.Size = new System.Drawing.Size(56, 13);
            this.LblDateFrom.TabIndex = 53;
            this.LblDateFrom.Text = "Date From";
            // 
            // LblRejoinDate
            // 
            this.LblRejoinDate.AutoSize = true;
            this.LblRejoinDate.Location = new System.Drawing.Point(481, 225);
            this.LblRejoinDate.Name = "LblRejoinDate";
            this.LblRejoinDate.Size = new System.Drawing.Size(63, 13);
            this.LblRejoinDate.TabIndex = 55;
            this.LblRejoinDate.Text = "Rejoin Date";
            this.LblRejoinDate.Visible = false;
            // 
            // LeaveDateFromDateTimePicker
            // 
            this.LeaveDateFromDateTimePicker.CustomFormat = "dd-MMM-yyyy";
            this.LeaveDateFromDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.LeaveDateFromDateTimePicker.Location = new System.Drawing.Point(83, 123);
            this.LeaveDateFromDateTimePicker.Name = "LeaveDateFromDateTimePicker";
            this.LeaveDateFromDateTimePicker.Size = new System.Drawing.Size(106, 20);
            this.LeaveDateFromDateTimePicker.TabIndex = 4;
            this.LeaveDateFromDateTimePicker.ValueChanged += new System.EventHandler(this.LeaveDateFromDateTimePicker_ValueChanged);
            // 
            // lblJoiningDateTag
            // 
            this.lblJoiningDateTag.AutoSize = true;
            this.lblJoiningDateTag.Location = new System.Drawing.Point(287, 25);
            this.lblJoiningDateTag.Name = "lblJoiningDateTag";
            this.lblJoiningDateTag.Size = new System.Drawing.Size(66, 13);
            this.lblJoiningDateTag.TabIndex = 75;
            this.lblJoiningDateTag.Text = "Joining Date";
            // 
            // LblDOJ
            // 
            this.LblDOJ.AutoSize = true;
            this.LblDOJ.Location = new System.Drawing.Point(387, 25);
            this.LblDOJ.Name = "LblDOJ";
            this.LblDOJ.Size = new System.Drawing.Size(28, 13);
            this.LblDOJ.TabIndex = 1;
            this.LblDOJ.Text = "DOJ";
            // 
            // LeaveDateToDateTimePicker
            // 
            this.LeaveDateToDateTimePicker.CustomFormat = "dd-MMM-yyyy";
            this.LeaveDateToDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.LeaveDateToDateTimePicker.Location = new System.Drawing.Point(290, 123);
            this.LeaveDateToDateTimePicker.Name = "LeaveDateToDateTimePicker";
            this.LeaveDateToDateTimePicker.Size = new System.Drawing.Size(102, 20);
            this.LeaveDateToDateTimePicker.TabIndex = 5;
            this.LeaveDateToDateTimePicker.ValueChanged += new System.EventHandler(this.LeaveDateToDateTimePicker_ValueChanged);
            // 
            // RejoinDateDateTimePicker
            // 
            this.RejoinDateDateTimePicker.CustomFormat = "dd-MMM-yyyy";
            this.RejoinDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.RejoinDateDateTimePicker.Location = new System.Drawing.Point(490, 192);
            this.RejoinDateDateTimePicker.Name = "RejoinDateDateTimePicker";
            this.RejoinDateDateTimePicker.Size = new System.Drawing.Size(106, 20);
            this.RejoinDateDateTimePicker.TabIndex = 9;
            this.RejoinDateDateTimePicker.Visible = false;
            this.RejoinDateDateTimePicker.ValueChanged += new System.EventHandler(this.RejoinDateDateTimePicker_ValueChanged);
            // 
            // lblnoofholidays
            // 
            this.lblnoofholidays.AutoSize = true;
            this.lblnoofholidays.Location = new System.Drawing.Point(287, 93);
            this.lblnoofholidays.Name = "lblnoofholidays";
            this.lblnoofholidays.Size = new System.Drawing.Size(78, 13);
            this.lblnoofholidays.TabIndex = 77;
            this.lblnoofholidays.Text = "No Of Holidays";
            this.lblnoofholidays.Visible = false;
            this.lblnoofholidays.VisibleChanged += new System.EventHandler(this.lblnoofholidays_VisibleChanged);
            // 
            // chkPaidUnPaid
            // 
            this.chkPaidUnPaid.AutoSize = true;
            this.chkPaidUnPaid.Location = new System.Drawing.Point(221, 92);
            this.chkPaidUnPaid.Name = "chkPaidUnPaid";
            this.chkPaidUnPaid.Size = new System.Drawing.Size(60, 17);
            this.chkPaidUnPaid.TabIndex = 3;
            this.chkPaidUnPaid.Text = "Unpaid";
            this.chkPaidUnPaid.UseVisualStyleBackColor = true;
            this.chkPaidUnPaid.CheckedChanged += new System.EventHandler(this.chkPaidUnPaid_CheckedChanged);
            // 
            // GrpMain
            // 
            this.GrpMain.Controls.Add(this.CboEmployee);
            this.GrpMain.Controls.Add(this.LblBalLeaveColn);
            this.GrpMain.Controls.Add(this.Label1);
            this.GrpMain.Controls.Add(this.lblnoofholidaysColn);
            this.GrpMain.Controls.Add(this.label2);
            this.GrpMain.Controls.Add(this.lblholidaysValue);
            this.GrpMain.Controls.Add(this.lblBalLeaveValue);
            this.GrpMain.Controls.Add(this.chkPaidUnPaid);
            this.GrpMain.Controls.Add(this.lblRemarks);
            this.GrpMain.Controls.Add(this.lblnoofholidays);
            this.GrpMain.Controls.Add(this.LeaveDateToDateTimePicker);
            this.GrpMain.Controls.Add(this.LblDOJ);
            this.GrpMain.Controls.Add(this.lblJoiningDateTag);
            this.GrpMain.Controls.Add(this.LeaveDateFromDateTimePicker);
            this.GrpMain.Controls.Add(this.LblDateFrom);
            this.GrpMain.Controls.Add(this.LblDateTo);
            this.GrpMain.Controls.Add(this.LblBalLeave);
            this.GrpMain.Controls.Add(this.HalfDayCheckBox);
            this.GrpMain.Controls.Add(this.CboLeaveType);
            this.GrpMain.Controls.Add(EmployeeIDLabel);
            this.GrpMain.Controls.Add(LeaveTypeIDLabel);
            this.GrpMain.Controls.Add(this.RemarksTextBox);
            this.GrpMain.Location = new System.Drawing.Point(7, 56);
            this.GrpMain.Name = "GrpMain";
            this.GrpMain.Size = new System.Drawing.Size(460, 260);
            this.GrpMain.TabIndex = 0;
            this.GrpMain.TabStop = false;
            // 
            // LblBalLeaveColn
            // 
            this.LblBalLeaveColn.AutoSize = true;
            this.LblBalLeaveColn.Location = new System.Drawing.Point(371, 60);
            this.LblBalLeaveColn.Name = "LblBalLeaveColn";
            this.LblBalLeaveColn.Size = new System.Drawing.Size(10, 13);
            this.LblBalLeaveColn.TabIndex = 83;
            this.LblBalLeaveColn.Text = ":";
            // 
            // lblnoofholidaysColn
            // 
            this.lblnoofholidaysColn.AutoSize = true;
            this.lblnoofholidaysColn.Location = new System.Drawing.Point(371, 93);
            this.lblnoofholidaysColn.Name = "lblnoofholidaysColn";
            this.lblnoofholidaysColn.Size = new System.Drawing.Size(10, 13);
            this.lblnoofholidaysColn.TabIndex = 82;
            this.lblnoofholidaysColn.Text = ":";
            this.lblnoofholidaysColn.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(371, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 13);
            this.label2.TabIndex = 81;
            this.label2.Text = ":";
            // 
            // lblholidaysValue
            // 
            this.lblholidaysValue.AutoSize = true;
            this.lblholidaysValue.Location = new System.Drawing.Point(387, 93);
            this.lblholidaysValue.Name = "lblholidaysValue";
            this.lblholidaysValue.Size = new System.Drawing.Size(13, 13);
            this.lblholidaysValue.TabIndex = 80;
            this.lblholidaysValue.Text = "0";
            this.lblholidaysValue.Visible = false;
            // 
            // lblBalLeaveValue
            // 
            this.lblBalLeaveValue.AutoSize = true;
            this.lblBalLeaveValue.Location = new System.Drawing.Point(387, 60);
            this.lblBalLeaveValue.Name = "lblBalLeaveValue";
            this.lblBalLeaveValue.Size = new System.Drawing.Size(13, 13);
            this.lblBalLeaveValue.TabIndex = 79;
            this.lblBalLeaveValue.Text = "0";
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(13, 160);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 78;
            this.lblRemarks.Text = "Remarks";
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(474, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 90;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyBooksERP.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // FrmLeaveEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 374);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.RejoinDateDateTimePicker);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.GrpMain);
            this.Controls.Add(this.LblRejoinDate);
            this.Controls.Add(this.EmployeeLeaveDetailsBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLeaveEntry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Leave Entry";
            this.Load += new System.EventHandler(this.FrmLeaveEntry_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmLeaveEntry_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmLeaveEntry_KeyDown);
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderEmpLeave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeLeaveDetailsBindingSourceRpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeLeaveDetailsBindingNavigator)).EndInit();
            this.EmployeeLeaveDetailsBindingNavigator.ResumeLayout(false);
            this.EmployeeLeaveDetailsBindingNavigator.PerformLayout();
            this.GrpMain.ResumeLayout(false);
            this.GrpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.Button BtnOk;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        internal System.Windows.Forms.ToolStripButton EmployeeLeaveDetailsBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorCancelItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.Timer TmFocus;
        internal System.Windows.Forms.ErrorProvider ErrorProviderEmpLeave;
        internal System.Windows.Forms.BindingNavigator EmployeeLeaveDetailsBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Timer TimerLeave;
        internal System.Windows.Forms.BindingSource EmployeeLeaveDetailsBindingSourceRpt;
        internal System.Windows.Forms.CheckBox chkPaidUnPaid;
        internal System.Windows.Forms.DateTimePicker RejoinDateDateTimePicker;
        internal System.Windows.Forms.GroupBox GrpMain;
        internal System.Windows.Forms.Label lblnoofholidays;
        internal System.Windows.Forms.DateTimePicker LeaveDateToDateTimePicker;
        internal System.Windows.Forms.Label LblDOJ;
        internal System.Windows.Forms.Label lblJoiningDateTag;
        internal System.Windows.Forms.DateTimePicker LeaveDateFromDateTimePicker;
        internal System.Windows.Forms.Label LblDateFrom;
        internal System.Windows.Forms.Label LblDateTo;
        internal System.Windows.Forms.Label LblBalLeave;
        internal System.Windows.Forms.CheckBox HalfDayCheckBox;
        internal System.Windows.Forms.ComboBox CboLeaveType;
        internal System.Windows.Forms.ComboBox CboEmployee;
        internal System.Windows.Forms.TextBox RemarksTextBox;
        internal System.Windows.Forms.Label LblRejoinDate;
        internal System.Windows.Forms.Label lblRemarks;
        internal System.Windows.Forms.Label lblholidaysValue;
        internal System.Windows.Forms.Label lblBalLeaveValue;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        private System.Windows.Forms.ToolStripDropDownButton bnMoreActions;
        private System.Windows.Forms.ToolStripMenuItem BtnLeaveExtension;
        internal System.Windows.Forms.Label LblBalLeaveColn;
        internal System.Windows.Forms.Label lblnoofholidaysColn;
        internal System.Windows.Forms.Label label2;
    }
}