﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{  /*****************************************************
    * Created By       : Arun
    * Creation Date    : 12 Apr 2012
    * Description      : Handle Salary Advance
    * FormID           : 119
    * ***************************************************/

    public partial class FrmSalaryAdvance : Form
    {


        #region Declartions
        public int PintSalaryAdvanceID = 0;   // From Refernce Form 
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission

        private string MstrCommonMessage;
        private bool MblnIsEditMode = false;
        private int MintRecordCnt = 0;
        private int MintCurrentRecCnt = 0;
        private bool MblnChangeStatus = false;
        private bool MblbtnOk = false;
        private bool MblnIsSettled = false;//Checking Record IS settled
        private string MstrSalaryAdvanceDate = "";//advance date of the record;

        private clsBLLSalaryAdvance MobjclsBLLSalaryAdvance = null;
        private clsMessage ObjUserMessage = null;
        #endregion Declartions

        #region Properties
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.SalaryAdvance);
                return this.ObjUserMessage;
            }
        }
        private clsBLLSalaryAdvance BLLSalaryAdvance
        {
            get
            {
                if (this.MobjclsBLLSalaryAdvance == null)
                    this.MobjclsBLLSalaryAdvance = new clsBLLSalaryAdvance();

                return this.MobjclsBLLSalaryAdvance;
            }
        }
        #endregion Properties




        public FrmSalaryAdvance()
        {
            InitializeComponent();
            tmrClear.Interval = ClsCommonSettings.TimerInterval;
        }



        #region Methods

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.SalaryAdvance, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }
        private void ClearAllControls()
        {
           
            this.GrpMain.Tag = 0;
            this.cboEmployee.SelectedIndex = -1;
            this.cboEmployee.Text = "";
            this.lblDateOfJoing.Text = "";
            this.dtpDate.Value = DateTime.Now;
            this.txtAmount.Text = "";
            this.txtRemarks.Text = "";
            this.txtChequeNo.Text = "";
            this.cboTransactionType.SelectedIndex = -1;
            this.cboTransactionType.Text = "";
            this.cboAccount.SelectedIndex = -1;
            this.cboAccount.Text = "";
            this.dtpChequeDate.Value = DateTime.Now;
            this.lblBasicPayAmount.Text = "";
            this.lblSalAdvPercentAmt.Text = "";
            this.MblnIsSettled = false;
            this.MstrSalaryAdvanceDate = "";
            this.lblWarning.Text = "";
           
        }
        private void Changestatus()
        {
            //function for changing status


            if (!MblnIsEditMode)
            {
                btnOk.Enabled = MblnAddPermission;
                btnSave.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
            else
            {
                btnOk.Enabled = MblnUpdatePermission;
                btnSave.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            errorProSalary.Clear();
            MblnChangeStatus = true;

        }
        private void SetTransactionType()
        {
            if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentModes.Bank)
            {
                cboAccount.Enabled = true;
                txtChequeNo.Enabled = true;
                dtpChequeDate.Enabled = true;
                cboAccount.BackColor = System.Drawing.SystemColors.Info;
                txtChequeNo.BackColor = System.Drawing.SystemColors.Info;
                
            }
            else
            {
                cboAccount.Enabled = false;
                txtChequeNo.Enabled = false;
                dtpChequeDate.Enabled = false;
                cboAccount.SelectedIndex = -1;
                txtChequeNo.Text = "";
                dtpChequeDate.Value = DateTime.Now;
                cboAccount.BackColor = System.Drawing.SystemColors.Window;
                txtChequeNo.BackColor = System.Drawing.SystemColors.Window;
            }
        }
        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
        }
        private bool LoadCombos(int intType)
        {
            bool blnRetvalue = false;
            DataTable datCombos = new DataTable();
            try
            {

                if (intType == 0 || intType == 1)//Employee
                {
                    if (MblnIsEditMode==false)
                        {
                            datCombos = BLLSalaryAdvance.FillCombos(new string[] { "DISTINCT EM.EmployeeID," +
                            "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullName + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EmployeeFullName + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                            "EmployeeMaster EM INNER JOIN PaySalaryStructure SS "+
                            "ON EM.EmployeeID = SS.EmployeeID AND SS.PaymentClassificationID = 1 AND  SS.PayCalculationTypeID = 1 " +
                            "INNER JOIN CompanyMaster C ON EM.CompanyID=C.CompanyID", "" +
                            "EM.WorkStatusID >= 6 AND EM.CompanyID = " + ClsCommonSettings.LoginCompanyID + " ORDER BY EmployeeName" });
                        }
                        else
                        {
                            datCombos = BLLSalaryAdvance.FillCombos(new string[] { "EmployeeID," +
                            "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullName + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EmployeeFullName + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", "1=1 ORDER BY EmployeeName" });
                        }
                    cboEmployee.ValueMember = "EmployeeID";
                    cboEmployee.DisplayMember = "EmployeeName";
                    cboEmployee.DataSource = datCombos;
                    blnRetvalue = true;
                    datCombos = null;
                }



                if (intType == 0 || intType == 2)//Transaction type
                {
                    datCombos = BLLSalaryAdvance.FillCombos(new string[] { "TransactionTypeID,TransactionType", "TransactionTypeReference", "TransactionTypeID IN("+(int)PaymentModes.Bank+","+(int)PaymentModes.Cash +")" });
                    cboTransactionType.ValueMember = "TransactionTypeID";
                    cboTransactionType.DisplayMember = "TransactionType";
                    cboTransactionType.DataSource = datCombos;
                    blnRetvalue = true;
                    datCombos = null;
                }
                if (intType == 0 || intType == 3)//Accounts
                {
                    int iEmpID = cboEmployee.SelectedValue.ToInt32();
                    datCombos = BLLSalaryAdvance.FillCombos(new string[] { "AM.AccountID,AM.AccountName", " AccAccountMaster  AM " +
                                                                            " INNER JOIN BankBranchReference BBR ON BBR.AccountID=AM.AccountID AND AM.AccountGroupID="+ (int)AccountGroups.BankAccounts +" "+
                                                                            " INNER JOIN CompanyBankAccountDetails CBA ON CBA.BankBranchID=BBR.BankBranchID " +
                                                                            " INNER  JOIN EmployeeMaster EM ON EM.CompanyID=CBA.CompanyID  AND EM.EmployeeID="+ iEmpID.ToStringCustom(), "" });
                    


                   


                    //datCombos = BLLSalaryAdvance.FillCombos(new string[] { "AccountID,AccountName", "AccAccountMaster", "AccountGroupID="+(int)AccountGroups.BankAccounts+" " });
                    cboAccount.ValueMember = "AccountID";
                    cboAccount.DisplayMember = "AccountName";
                    cboAccount.DataSource = datCombos;
                    blnRetvalue = true;
                    datCombos = null;
                }

               
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
                blnRetvalue = false;
            }
            return blnRetvalue;
        }
        private void AddNewSalaryAdvance()
        {
            MblnIsSettled = false;
            MblnIsEditMode = false;
            lblstatus.Text = "";
            tmrClear.Enabled = true;
            errorProSalary.Clear();

            ClearAllControls();  //Clear all controls in the form
            GetRecordCount();
            MintRecordCnt = MintRecordCnt + 1;
            MintCurrentRecCnt = MintRecordCnt;
            BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();
            cboEmployee.Enabled = true;
             GrpMain.Enabled = true;
             MblnIsEditMode = false ;
             LoadCombos(0);
            BindingNavigatorAddNewItem.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = false;
            btnPrint.Enabled = false;
            btnEmail.Enabled = false;
            btnOk.Enabled = false;
            btnSave.Enabled = false;
            BindingNavigatorSaveItem.Enabled = false;
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;
            cboEmployee.Focus();
            MblnChangeStatus = false;

        }
        private void GetRecordCount()
        {
            MintRecordCnt = 0;
            MintRecordCnt = BLLSalaryAdvance.GetRecordCount();
            if (MintRecordCnt < 0)
            {
                BindingNavigatorCountItem.Text = "of 0";
                MintRecordCnt = 0;
            }

        }
        private void RefernceDisplay()
        {

            int intRowNum = 0;
            intRowNum = BLLSalaryAdvance.GetRowNumber(PintSalaryAdvanceID);
            if (intRowNum > 0)
            {
                MintCurrentRecCnt = intRowNum;
                DisplaySalaryAdvanceInfo();
            }

        }
        private void DisplaySalaryAdvanceInfo()
        {
            MblnIsEditMode = true;
            LoadCombos(0);
            FillSalaryAdvanceInfo();

            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            btnPrint.Enabled = MblnPrintEmailPermission;
            btnEmail.Enabled = MblnPrintEmailPermission;
            if (MblnIsSettled)
            {
                GrpMain.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
            }
            else
            {
                GrpMain.Enabled = true;
            }
            cboEmployee.Enabled = false;

            BindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
            BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";
            MblnIsEditMode = true;
            SetBindingNavigatorButtons();
            MblnChangeStatus = false;
            BindingNavigatorSaveItem.Enabled = MblnChangeStatus;
            btnOk.Enabled = MblnChangeStatus;
            btnSave.Enabled = MblnChangeStatus;
        }
        private void FillSalaryAdvanceInfo()
        {
            ClearAllControls();
            if (BLLSalaryAdvance.DisplaySalaryAdvanceinfo( MintCurrentRecCnt))
            {
                GrpMain.Tag = BLLSalaryAdvance.DTOSalaryAdvance.SalaryAdvanceID;
                cboEmployee.SelectedValue = BLLSalaryAdvance.DTOSalaryAdvance.EmployeeID;
                dtpDate.Value = Convert.ToDateTime(BLLSalaryAdvance.DTOSalaryAdvance.Date);
                txtAmount.Text = BLLSalaryAdvance.DTOSalaryAdvance.Amount.ToStringCustom();
                txtRemarks.Text = BLLSalaryAdvance.DTOSalaryAdvance.Remarks;
                cboTransactionType.SelectedValue = BLLSalaryAdvance.DTOSalaryAdvance.TransactionTypeID;
                if (BLLSalaryAdvance.DTOSalaryAdvance.TransactionTypeID == (int)PaymentModes.Bank)
                {
                    cboAccount.SelectedValue = BLLSalaryAdvance.DTOSalaryAdvance.AccountID;
                    txtChequeNo.Text = BLLSalaryAdvance.DTOSalaryAdvance.ChequeNumber;
                    dtpChequeDate.Value = BLLSalaryAdvance.DTOSalaryAdvance.ChequeDate.ToDateTime();
                }





                MblnIsSettled = BLLSalaryAdvance.DTOSalaryAdvance.IsSettled;
                MstrSalaryAdvanceDate = Convert.ToDateTime(BLLSalaryAdvance.DTOSalaryAdvance.Date).ToString("dd-MMM-yyyy");
            }

        }
        private bool FormValidations()
        {

            if (cboEmployee.SelectedValue.ToInt32() == 0)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(3066);
                errorProSalary.SetError(cboEmployee, MstrCommonMessage);
                UserMessage.ShowMessage(3066);
                lblstatus.Text = MstrCommonMessage;
                tmrClear.Enabled = true;
                cboEmployee.Focus();
                return false;
            }

            if (txtAmount.Text.ToDecimal() <= 0)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(3068);
                errorProSalary.SetError(txtAmount, MstrCommonMessage);
                UserMessage.ShowMessage(3068);
                tmrClear.Enabled = true;
                txtAmount.Focus();
                return false;
            }

            if (DateTime.Now.Date < dtpDate.Value.Date)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(3072);
                errorProSalary.SetError(dtpDate, MstrCommonMessage);
                UserMessage.ShowMessage(3072);
                lblstatus.Text = MstrCommonMessage;
                tmrClear.Enabled = true;
                dtpDate.Focus();
                return false;
            }
            if (cboTransactionType.SelectedValue.ToInt32() <= 0)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(3083);
                errorProSalary.SetError(cboTransactionType, MstrCommonMessage);
                UserMessage.ShowMessage(3083);
                lblstatus.Text = MstrCommonMessage;
                tmrClear.Enabled = true;
                cboTransactionType.Focus();
                return false;
            }
            if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentModes.Bank)
            {

                if (cboAccount.SelectedValue.ToInt32() <=0)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(3087);
                    errorProSalary.SetError(cboAccount, MstrCommonMessage);
                    UserMessage.ShowMessage(3087);
                    lblstatus.Text = MstrCommonMessage;
                    tmrClear.Enabled = true;
                    cboAccount.Focus();
                    return false;

                }
                if (txtChequeNo.Text.ToStringCustom() == string.Empty)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(3084);
                    errorProSalary.SetError(txtChequeNo, MstrCommonMessage);
                    UserMessage.ShowMessage(3084);
                    lblstatus.Text = MstrCommonMessage;
                    tmrClear.Enabled = true;
                    txtChequeNo.Focus();
                    return false;
                }
                if (dtpChequeDate.Value.Date < DateTime.Now.Date)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(3085);
                    errorProSalary.SetError(dtpChequeDate, MstrCommonMessage);
                    UserMessage.ShowMessage(3085);
                    lblstatus.Text = MstrCommonMessage;
                    tmrClear.Enabled = true;
                    dtpChequeDate.Focus();
                    return false;
                }
            }

            if (GrpMain.Tag.ToInt32() > 0)
            {
                if (BLLSalaryAdvance.IsSalaryAdavanceIsSettled(GrpMain.Tag.ToInt32()))
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(3101);
                    UserMessage.ShowMessage(3101);

                    lblstatus.Text = MstrCommonMessage;
                    cboEmployee.Focus();
                    tmrClear.Enabled = true;
                    return false;
                }
            }

            if (CheckFinYearStartDate() == false)
            {
                UserMessage.ShowMessage(3078);
                lblstatus.Text = UserMessage.GetMessageByCode(3078);
                tmrClear.Enabled = true;
                dtpDate.Focus();
                return false;
            }
            int intWorkStatusID = BLLSalaryAdvance.GetEmployeeWorkStatus(cboEmployee.SelectedValue.ToInt32());
            if (intWorkStatusID > 0 && intWorkStatusID < 6)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(3079);
                UserMessage.ShowMessage(3079);
                lblstatus.Text = MstrCommonMessage;
                cboEmployee.Focus();
                tmrClear.Enabled = true;
                return false;
            }
            if (BLLSalaryAdvance.IsSalAdvAccountExists(cboEmployee.SelectedValue.ToInt32()) == false)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(3086);
                UserMessage.ShowMessage(3086);
                lblstatus.Text = MstrCommonMessage;
                cboEmployee.Focus();
                tmrClear.Enabled = true;
                return false;
            }

            int intPayCalculationTypeID = 0;
            int intPaymentClassificationID = 0;
            string strDate = "";
            if (BLLSalaryAdvance.GetEmployeeSalaryStructreDetails(cboEmployee.SelectedValue.ToInt32(), ref intPayCalculationTypeID, ref intPaymentClassificationID, ref strDate))
            {
                if (intPayCalculationTypeID != 1 && intPaymentClassificationID != 1)
                {

                    MstrCommonMessage = UserMessage.GetMessageByCode(3080);
                    UserMessage.ShowMessage(3080);
                    lblstatus.Text = MstrCommonMessage;
                    tmrClear.Enabled = true;
                    cboEmployee.Focus();
                    return false;
                }
                if (strDate != "")
                {
                    if (Convert.ToDateTime(strDate) > dtpDate.Value.Date)
                    {
                        MstrCommonMessage = UserMessage.GetMessageByCode(3081);
                        UserMessage.ShowMessage(3081);
                        errorProSalary.SetError(dtpDate, MstrCommonMessage);
                        lblstatus.Text = MstrCommonMessage;
                        tmrClear.Enabled = true;
                        dtpDate.Focus();
                        return false;

                    }
                }


            }
            if (CheckSalaryStructure() == false)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(3082);
                UserMessage.ShowMessage(3082);
                errorProSalary.SetError(txtAmount, MstrCommonMessage);
                lblstatus.Text = MstrCommonMessage;
                tmrClear.Enabled = true;
                txtAmount.Focus();
                return false;
            }
            string strAdvDate = MstrSalaryAdvanceDate != "" ? MstrSalaryAdvanceDate : dtpDate.Value.ToString("dd-MMM-yyyy");
            if (BLLSalaryAdvance.IsSalaryProcessed(cboEmployee.SelectedValue.ToInt32(), strAdvDate))
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(3076);
                UserMessage.ShowMessage(3076);
                lblstatus.Text = MstrCommonMessage;
                tmrClear.Enabled = true;
                return false;
            }
                        
            return true;
        }
        private decimal GetAmountInCompanyCurrency(decimal decAmount,ref string strCmpCurrency)
        {

            decimal decRetValue = decAmount;
            decimal decExchangeRate = 0;
            strCmpCurrency="";
            int intEmpID = cboEmployee.SelectedValue.ToInt32();

            decExchangeRate =this.BLLSalaryAdvance.GetExchangeRate (intEmpID, ref strCmpCurrency);

            if (decExchangeRate > 0)
                decRetValue = decAmount * decExchangeRate;
            return decRetValue;
        }


        private bool CheckSalaryStructure()
        {
            bool blnRetValue = true;

            decimal decAmount = txtAmount.Text.ToDecimal();

            if (decAmount > 0)
            {
                string strCurCode = "";
                decimal decConfigurationValue = 0;
                decimal decDefaultValue = 0;
                decimal decPaidTotalMonthlyAmt = BLLSalaryAdvance.GetMonthlyTotalAdvanceAmount(GrpMain.Tag.ToInt32(), cboEmployee.SelectedValue.ToInt32(), dtpDate.Value.ToString("dd-MMM-yyyy"));
                decimal decBasicPay = BLLSalaryAdvance.GetEmployeeBasicPay(cboEmployee.SelectedValue.ToInt32());
                decBasicPay = GetAmountInCompanyCurrency(decBasicPay, ref strCurCode);//Converting Amount in CompanyCurrency
                decimal decAmountCalculated = 0;
                decimal decSalPerAmount = 0;
                if (BLLSalaryAdvance.GetSalaryAdvancePercentage(ref  decDefaultValue, ref decConfigurationValue))
                {
                    decAmountCalculated = (decBasicPay * decConfigurationValue) / 100;
                    if (decAmount > decAmountCalculated)
                    {
                        blnRetValue = false;
                    }
                }
                decAmountCalculated = 0;

                decAmount = decAmount + decPaidTotalMonthlyAmt;
                decAmountCalculated = (decBasicPay * decConfigurationValue) / 100;
                if (blnRetValue && (decAmount > decAmountCalculated))
                {
                    blnRetValue = false;
                }

                decSalPerAmount = decConfigurationValue;
                if (decConfigurationValue <= 0)
                    decSalPerAmount = decDefaultValue;

                if (decSalPerAmount <= 0)
                    decSalPerAmount = 50;

                decAmountCalculated = 0;
                decAmountCalculated = (decBasicPay * decSalPerAmount) / 100;
                if (blnRetValue && (decAmount > decAmountCalculated))
                {
                    blnRetValue = false;
                }

            }
            return blnRetValue;
        }
        private bool CheckFinYearStartDate()
        {
            bool blnRetValue = true;
            string strDate = "";
            if (BLLSalaryAdvance.GetCompanyFinancialYearStartDate(cboEmployee.SelectedValue.ToInt32(), ref strDate))
            {
                if (strDate != string.Empty)
                {
                    if (dtpDate.Value.Date < Convert.ToDateTime(strDate))
                    {
                        blnRetValue = false;
                    }
                }
            }
            return blnRetValue;

        }
        private bool DeleteValidation()
        {
            if (GrpMain.Tag.ToInt32() == 0)
            {
                UserMessage.ShowMessage(3060);

                return false;
            }
            if (BLLSalaryAdvance.IsSalaryAdavanceIsSettled(GrpMain.Tag.ToInt32()))
            {
                UserMessage.ShowMessage(3101);
                lblstatus.Text = UserMessage.GetMessageByCode(3101);
                cboEmployee.Focus();
                tmrClear.Enabled = true;
                return false;
            }
            if (BLLSalaryAdvance.IsSalaryProcessed(cboEmployee.SelectedValue.ToInt32(), dtpDate.Value.Date.ToString("dd-MMM-yyyy")))
            {
                UserMessage.ShowMessage(3076);
                return false;
            }
            if (UserMessage.ShowMessage(3071) == false)
            {
                return false;
            }
            return true;
        }
        private void FillParameterMaster()
        {
            if (GrpMain.Tag.ToInt32() > 0)
                BLLSalaryAdvance.DTOSalaryAdvance.SalaryAdvanceID = GrpMain.Tag.ToInt32();
            else
                BLLSalaryAdvance.DTOSalaryAdvance.SalaryAdvanceID = 0;
            BLLSalaryAdvance.DTOSalaryAdvance.EmployeeID = cboEmployee.SelectedValue.ToInt32();
            BLLSalaryAdvance.DTOSalaryAdvance.Date = dtpDate.Value.Date.ToString("dd-MMM-yyyy");
            BLLSalaryAdvance.DTOSalaryAdvance.Amount = txtAmount.Text.ToDecimal();
            BLLSalaryAdvance.DTOSalaryAdvance.Remarks = txtRemarks.Text.ToStringCustom().Trim();
            BLLSalaryAdvance.DTOSalaryAdvance.TransactionTypeID = cboTransactionType.SelectedValue.ToInt32();
            if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentModes.Bank)
            {
                BLLSalaryAdvance.DTOSalaryAdvance.AccountID = cboAccount.SelectedValue.ToInt32();
                BLLSalaryAdvance.DTOSalaryAdvance.ChequeNumber = txtChequeNo.Text.Trim();
                BLLSalaryAdvance.DTOSalaryAdvance.ChequeDate = dtpChequeDate.Value.Date.ToString("dd-MMM-yyyy");
            }


            
        }
        private bool SaveSalaryAdvanceInfo()
        {
            bool blnRetValue = false;

            if (FormValidations())
            {
                int intMessageCode = 0;
                if (GrpMain.Tag.ToInt32() > 0)
                    intMessageCode = 3070;
                else
                    intMessageCode = 3069;

                if (UserMessage.ShowMessage(intMessageCode))
                {
                    FillParameterMaster();
                     if (BLLSalaryAdvance.SalaryAdvanceSave())
                    {
                        blnRetValue = true;
                        GrpMain.Tag = BLLSalaryAdvance.DTOSalaryAdvance.SalaryAdvanceID;
                        BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                        btnEmail.Enabled = MblnPrintEmailPermission;
                        btnPrint.Enabled = MblnPrintEmailPermission;
                        BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                    }

                }
            }

            return blnRetValue;
        }
        private bool DeleteSalaryAdvanceInfo()
        {
            bool blnRetValue = false;
            if (DeleteValidation())
            {
                BLLSalaryAdvance.DTOSalaryAdvance.SalaryAdvanceID = GrpMain.Tag.ToInt32();
                if (BLLSalaryAdvance.DeleteSalaryAdvanceInfo())
                {
                    lblstatus.Text = UserMessage.GetMessageByCode(4);
                    tmrClear.Enabled = true;
                    UserMessage.ShowMessage(4);
                    blnRetValue = true;
                    AddNewSalaryAdvance();
                }

            }
            return blnRetValue;

        }

        private void FillRemarks()
        {
            try{
            txtRemarks.Text = "";
            if (txtAmount.Text.Trim() != "" && cboEmployee.Text != "" )
            {

                txtRemarks.Text = UserMessage.GetMessageByCode(3077) + " - " + cboEmployee.Text.Trim() +
                                " - " + " of " + txtAmount.Text;
            }
            }
        catch ( Exception ex)
            {
            ClsLogWriter.WriteLog(ex, Log.LogSeverity.Error);
        }
        }
        #endregion Methods

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {

            GetRecordCount();
            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;
                DisplaySalaryAdvanceInfo();
                lblstatus.Text = UserMessage.GetMessageByCode(9);
                tmrClear.Enabled = true;
            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    DisplaySalaryAdvanceInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(10);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 1;
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                }

                if (MintCurrentRecCnt > MintRecordCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                }
                else
                {
                    DisplaySalaryAdvanceInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(11);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 0;
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    DisplaySalaryAdvanceInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(12);
                    tmrClear.Enabled = true;
                }
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewSalaryAdvance();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (DeleteSalaryAdvanceInfo())
            {
                
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveSalaryAdvanceInfo())
            {
                lblstatus.Text = UserMessage.GetMessageByCode(2);
                UserMessage.ShowMessage(2);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            AddNewSalaryAdvance();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveSalaryAdvanceInfo())
            {
                lblstatus.Text = UserMessage.GetMessageByCode(2);
                UserMessage.ShowMessage(2);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveSalaryAdvanceInfo())
            {
                MblbtnOk = true;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmSalaryAdvance_Load(object sender, EventArgs e)
        {
            SetPermissions();
             LoadCombos(0);
            if (PintSalaryAdvanceID > 0)
            {
                GetRecordCount();
                RefernceDisplay();
            }
            else
            {
                AddNewSalaryAdvance();
            }
        }

        private void FrmSalaryAdvance_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MblbtnOk == false && MblnChangeStatus == true)
            {
                if (UserMessage.ShowMessage(8))
                {
                    e.Cancel = false;
                }
                else
                { e.Cancel = true; }
            }
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
            FillRemarks();
            Changestatus();
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboEmployee.SelectedValue.ToInt32()>0)
            {
                lblWarning.Text = "";
                string strDate="";
                string strCmpCurrencyCode = "";
                decimal decBasicPay=0;
                string strSalAdvPercent ="";
                    lblDateOfJoing.Text="";
                    lblBasicPayAmount.Text = "";
                    lblSalAdvPercentAmt.Text = "";
                    if (BLLSalaryAdvance.GetEmployeeJoiningDate(cboEmployee.SelectedValue.ToInt32(),ref strDate ,ref decBasicPay,ref strSalAdvPercent))
                    {
                        lblDateOfJoing.Text=strDate;
                        decBasicPay = GetAmountInCompanyCurrency(decBasicPay, ref strCmpCurrencyCode);
                        if (strCmpCurrencyCode != "")
                            lblWarning.Text = "*Amounts are in company currency " + " ( " + strCmpCurrencyCode + " )";
                        else
                            lblWarning.Text = "*Amounts are in company currency ";
                        lblBasicPayAmount.Text = decBasicPay.ToString();
                        lblSalAdvPercentAmt.Text = strSalAdvPercent;
                    }
                    if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentModes.Bank)
                    {
                        LoadCombos(3);//Bank
                    }
              

            }
            Changestatus();
        }

        private void txtRemarks_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
           string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
        
        }

        private void cboEmployee_KeyDown(object sender, KeyEventArgs e)
        {
            cboEmployee.DroppedDown = false;
        }

        private void tmrClear_Tick(object sender, EventArgs e)
        {
            tmrClear.Enabled = false;
            lblstatus.Text = "";
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void cboTransactionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus();
            SetTransactionType();
            if (cboTransactionType.SelectedValue.ToInt32() > 0)
            {
                if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentModes.Bank && cboEmployee.SelectedValue.ToInt32()>0)
                {
                    LoadCombos(3);//Bank
                }
            }
        }

        private void cboAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtChequeNo_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void btnAccountsettings_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmAccountSettings(), true);    
        }

        



    }
}
