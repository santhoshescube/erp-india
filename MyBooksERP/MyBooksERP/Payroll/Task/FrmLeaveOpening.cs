﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace MyBooksERP
{
    public partial class FrmLeaveOpening : Form
    {
        private clsBLLLeaveOpening MobjclsBLLLeaveOpening = null;
        private clsMessage ObjUserMessage = null;
        private ClsLogWriter mObjLogs;
        private int MintSearchIndex = 0;
        //'------------permission-------------------'
        private bool MblnPrintEmailPermission = false;     //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;       //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        //'------------permission-------------------'
        private string MsMessageCaption;    //Message caption

        #region Properties

        private clsBLLLeaveOpening BLLLeaveOpening
        {
            get
            {
                if (this.MobjclsBLLLeaveOpening == null)
                    this.MobjclsBLLLeaveOpening = new clsBLLLeaveOpening();

                return this.MobjclsBLLLeaveOpening;
            }
        }


        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.LeaveOpening);
                return this.ObjUserMessage;
            }
        }



        #endregion Properties

        public FrmLeaveOpening()
        {

            InitializeComponent();
            MobjclsBLLLeaveOpening = new clsBLLLeaveOpening();

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.LeaveOpening, this);

        //    txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
        //    label10.Text = "شركة";
        //}

        #region Methods

        #region SetPermissions
        /// <summary>
        /// Set permissions for add,update,delete,print/email
        /// </summary>
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID>3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.LeaveOpening, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
            {
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            }
            btnUpdate.Enabled = MblnUpdatePermission;
        }
        #endregion SetPermissions

        #region LoadCombos
        /// <summary>
        /// Load company combo
        /// </summary>
        /// <param name="intType"></param>
        private void LoadCombos(int intType)
        {
            DataTable datCombos = new DataTable();
            if (intType == 0 || intType == 1)//Company
           
            {
                datCombos = MobjclsBLLLeaveOpening.getDetailsInComboBox(5); // Get Company
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.DataSource = datCombos;
                datCombos = null;
            }
        }
        #endregion LoadCombos

        #region FillEmployees
        /// <summary>
        /// Fill the grid with employees having vacation policy
        /// </summary>
        private void FillEmployees()
        {
            try
            {
                dgvEmployeeLeaveOpening.Rows.Clear();
                DataTable dtEmployees; int PCompanyID;
                if (cboCompany.SelectedIndex != -1)
                {
                    PCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                }
                else
                {
                    PCompanyID = ClsCommonSettings.LoginCompanyID;
                }
                dtEmployees = MobjclsBLLLeaveOpening.GetEmployeeList(ClsCommonSettings.UserID, PCompanyID);

                for (int intICounter = 0; dtEmployees.Rows.Count - 1 >= intICounter; ++intICounter)
                {
                    dgvEmployeeLeaveOpening.Rows.Add();
                    dgvEmployeeLeaveOpening.Rows[intICounter].Cells[EmployeeID.Index].Value = dtEmployees.Rows[intICounter]["EmployeeID"];
                    dgvEmployeeLeaveOpening.Rows[intICounter].Cells[EmployeeName.Index].Value =  dtEmployees.Rows[intICounter]["EmployeeName"];
                    dgvEmployeeLeaveOpening.Rows[intICounter].Cells[TakenLeaves.Index].Value = dtEmployees.Rows[intICounter]["TakenLeaves"];
                    dgvEmployeeLeaveOpening.Rows[intICounter].Cells[PaidDays.Index].Value = dtEmployees.Rows[intICounter]["PaidDays"];
                    dgvEmployeeLeaveOpening.Rows[intICounter].Cells[TakenTickets.Index].Value = dtEmployees.Rows[intICounter]["TakenTickets"];
                    dgvEmployeeLeaveOpening.Rows[intICounter].Cells[PaidTicketAmount.Index].Value = dtEmployees.Rows[intICounter]["PaidTicketAmount"];
                }
                dgvEmployeeLeaveOpening.ClearSelection();

            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on form " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }
        #endregion FillEmployees

        #region SearchEmp
        /// <summary>
        /// Search employee according to employee number and name
        /// </summary>
        /// <returns></returns>
        private bool SearchEmp()
        {
            try
            {
                if (txtSearch.Text != "")
                {
                    if (dgvEmployeeLeaveOpening.Rows.Count > 0)
                    {
                        string strEmpName = "";
                        if (MintSearchIndex >= dgvEmployeeLeaveOpening.RowCount)
                        {
                            MintSearchIndex = 0;
                        }
                        MintSearchIndex = ((MintSearchIndex == 0) ? 0 : MintSearchIndex);
                        if (MintSearchIndex > 0)
                        {
                            dgvEmployeeLeaveOpening.Rows[MintSearchIndex - 1].Selected = false;
                        }
                        else
                        {
                            dgvEmployeeLeaveOpening.Rows[MintSearchIndex].Selected = false;
                        }
                        for (int i = MintSearchIndex; dgvEmployeeLeaveOpening.Rows.Count - 1 >= i; ++i)
                        {
                            strEmpName = "";
                           //Search by employeeName
                                strEmpName = Convert.ToString(dgvEmployeeLeaveOpening.Rows[i].Cells["EmployeeName"].Value);
                                strEmpName = strEmpName.TrimStart().TrimEnd();
                                if (strEmpName.ToLower().StartsWith(txtSearch.Text.Trim().ToLower()))
                                {
                                    dgvEmployeeLeaveOpening.CurrentCell = dgvEmployeeLeaveOpening[EmployeeName.Index, i];
                                    dgvEmployeeLeaveOpening.CurrentCell.Selected = false;
                                    dgvEmployeeLeaveOpening.Rows[i].Selected = true;
                                    MintSearchIndex = i + 1;
                                    return false;
                                }
                            //Search by EmployeeNumber
                                strEmpName = Convert.ToString(dgvEmployeeLeaveOpening.Rows[i].Cells["EmployeeName"].Value);
                                strEmpName = strEmpName.Substring(strEmpName.IndexOf("-") + 1);
                                strEmpName = strEmpName.TrimStart().TrimEnd();
                                if (strEmpName.ToLower().StartsWith(txtSearch.Text.Trim().ToLower()))
                                {
                                    dgvEmployeeLeaveOpening.CurrentCell = dgvEmployeeLeaveOpening[EmployeeName.Index, i];
                                    dgvEmployeeLeaveOpening.CurrentCell.Selected = false;
                                    dgvEmployeeLeaveOpening.Rows[i].Selected = true;
                                    MintSearchIndex = i + 1;
                                    return false;
                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on form " + this.Name + " " + ex.Message.ToString(), 2);
            }
            return true;
        }
        #endregion SearchEmp

        #region SetAutoCompleteList
        /// <summary>
        /// Set AutoComplete List
        /// </summary>
        private void SetAutoCompleteList()
        {

            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsUtilities.GetAutoCompleteListWithCompany(cboCompany.SelectedValue.ToInt32(),txtSearch.Text.Trim(), 0, string.Empty,"");
            this.txtSearch.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt);
        }
        #endregion SetAutoCompleteList

        #endregion Methods

        #region Events
        private void FrmLeaveOpening_Load(object sender, EventArgs e)
        {

            MsMessageCaption = ClsCommonSettings.MessageCaption; // Message caption
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            SetPermissions();
            LoadCombos(0);
            cboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;
            //cboCompany.Enabled = false;
            FillEmployees();
            SetAutoCompleteList();
            
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string strMessage = "No Items Found !";
                dgvEmployeeLeaveOpening.ClearSelection();

                if (this.txtSearch.Text.Trim() != string.Empty)//&& (cboSearchType.Text == "Employee Name" || cboSearchType.Text == "Employee No"
                {
                    if (dgvEmployeeLeaveOpening.Rows.Count > 0)
                    {

                        if (SearchEmp() == true)
                        {
                            if (MintSearchIndex == 0)
                            {
                                MessageBox.Show(strMessage, MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                dgvEmployeeLeaveOpening.Refresh();
                            }
                            MintSearchIndex = 0;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please enter a text to search.", MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSearch.Focus();
                }
            }
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1, Log.LogSeverity.Error);

            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to update ?", MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    BLLLeaveOpening.DTOLeaveOpening.lstclsDTOLeaveOpeningDetail = new List<clsDTOLeaveOpeningDetail>();
                    foreach (DataGridViewRow dr in dgvEmployeeLeaveOpening.Rows)
                    {
                        clsDTOLeaveOpeningDetail ObjclsDTOLeaveOpeningDetail = new clsDTOLeaveOpeningDetail();
                        ObjclsDTOLeaveOpeningDetail.Mode = 1;
                        ObjclsDTOLeaveOpeningDetail.EmployeeID = Convert.ToInt32(dr.Cells[EmployeeID.Index].Value);
                        ObjclsDTOLeaveOpeningDetail.TakenLeaves = Convert.ToDecimal(dr.Cells[TakenLeaves.Index].Value);
                        ObjclsDTOLeaveOpeningDetail.PaidDays = Convert.ToDecimal(dr.Cells[PaidDays.Index].Value);
                        ObjclsDTOLeaveOpeningDetail.TakenTickets = Convert.ToDecimal(dr.Cells[TakenTickets.Index].Value);
                        ObjclsDTOLeaveOpeningDetail.PaidTicketAmount = Convert.ToDecimal(dr.Cells[PaidTicketAmount.Index].Value);
                        BLLLeaveOpening.DTOLeaveOpening.lstclsDTOLeaveOpeningDetail.Add(ObjclsDTOLeaveOpeningDetail);
                    }
                    MobjclsBLLLeaveOpening.SaveEmployeeLeave();
                    DeductionPolicyStatusStrip.Text = "Updated Successfully";
                    MessageBox.Show("Updated Successfully", MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnUpdate.Enabled = false;
                }
                catch (Exception ex)
                {
                    DeductionPolicyStatusStrip.Text = "Updation failed";
                    MessageBox.Show("Updation failed", MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    mObjLogs.WriteLog("Error on form " + this.Name + " " + ex.Message.ToString(), 2);
                }
            }
        }

        private void dgvEmployeeLeaveOpening_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (dgvEmployeeLeaveOpening.CurrentCell != null)
                {

                    if (dgvEmployeeLeaveOpening.CurrentCell.ColumnIndex != 0)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }

        }

        void txtDecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            e.Handled = false;

            //if (ClsCommonSettings.IsAmountRoundByZero)
            //{
            //    strInvalidChars = strInvalidChars + ".";
            //}


            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
        }


        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillEmployees();
            txtSearch.Text = string.Empty;
            SetAutoCompleteList();
        }

        private void cboCompany_TextChanged(object sender, EventArgs e)
        {
            FillEmployees();
        }

        private void txtSearch_Click(object sender, EventArgs e)
        {
            dgvEmployeeLeaveOpening.ClearSelection();
            MintSearchIndex = 0;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvEmployeeLeaveOpening_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            btnUpdate.Enabled = MblnUpdatePermission;
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "LeaveOpening";
            objHelp.ShowDialog();
            objHelp = null;
        }
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
                btnSearch_Click(null, null);

        }

        #endregion Events

        private void FrmLeaveOpening_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.F1:
                        btnHelp_Click(sender, new EventArgs());//help
                        break;
                }
            }
            catch
            {
            }
        }
    }
}
