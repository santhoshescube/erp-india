﻿namespace MyBooksERP
{
    partial class FrmLeaveOpening
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLeaveOpening));
            this.dgvEmployeeLeaveOpening = new System.Windows.Forms.DataGridView();
            this.EmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TakenLeaves = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PaidDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TakenTickets = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PaidTicketAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.Label1 = new System.Windows.Forms.Label();
            this.TextboxNumeric = new System.Windows.Forms.TextBox();
            this.txtNumeric1 = new System.Windows.Forms.TextBox();
            this.DeductionPolicyStatusStrip = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ProjectCreationStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.textBoxX1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.label10 = new System.Windows.Forms.Label();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            this.btnHelp = new DevComponents.DotNetBar.ButtonItem();
            this.bSearch = new DevComponents.DotNetBar.Bar();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployeeLeaveOpening)).BeginInit();
            this.DeductionPolicyStatusStrip.SuspendLayout();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.bar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvEmployeeLeaveOpening
            // 
            this.dgvEmployeeLeaveOpening.AllowUserToAddRows = false;
            this.dgvEmployeeLeaveOpening.AllowUserToDeleteRows = false;
            this.dgvEmployeeLeaveOpening.AllowUserToResizeRows = false;
            this.dgvEmployeeLeaveOpening.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvEmployeeLeaveOpening.BackgroundColor = System.Drawing.Color.White;
            this.dgvEmployeeLeaveOpening.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmployeeLeaveOpening.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EmployeeID,
            this.EmployeeName,
            this.TakenLeaves,
            this.PaidDays,
            this.TakenTickets,
            this.PaidTicketAmount});
            this.dgvEmployeeLeaveOpening.Location = new System.Drawing.Point(3, 34);
            this.dgvEmployeeLeaveOpening.Name = "dgvEmployeeLeaveOpening";
            this.dgvEmployeeLeaveOpening.RowHeadersVisible = false;
            this.dgvEmployeeLeaveOpening.Size = new System.Drawing.Size(764, 366);
            this.dgvEmployeeLeaveOpening.TabIndex = 0;
            this.dgvEmployeeLeaveOpening.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEmployeeLeaveOpening_CellEndEdit);
            this.dgvEmployeeLeaveOpening.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvEmployeeLeaveOpening_EditingControlShowing);
            // 
            // EmployeeID
            // 
            this.EmployeeID.HeaderText = "EmployeeID";
            this.EmployeeID.MaxInputLength = 10;
            this.EmployeeID.Name = "EmployeeID";
            this.EmployeeID.Visible = false;
            // 
            // EmployeeName
            // 
            this.EmployeeName.HeaderText = "EmployeeName";
            this.EmployeeName.Name = "EmployeeName";
            this.EmployeeName.ReadOnly = true;
            this.EmployeeName.Width = 250;
            // 
            // TakenLeaves
            // 
            this.TakenLeaves.HeaderText = "Taken Leaves";
            this.TakenLeaves.MaxInputLength = 15;
            this.TakenLeaves.Name = "TakenLeaves";
            this.TakenLeaves.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TakenLeaves.Width = 125;
            // 
            // PaidDays
            // 
            this.PaidDays.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.PaidDays.HeaderText = "Paid Days";
            this.PaidDays.MaxInputLength = 15;
            this.PaidDays.Name = "PaidDays";
            this.PaidDays.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PaidDays.Width = 125;
            // 
            // TakenTickets
            // 
            this.TakenTickets.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.TakenTickets.HeaderText = "Taken Tickets";
            this.TakenTickets.MaxInputLength = 3;
            this.TakenTickets.Name = "TakenTickets";
            this.TakenTickets.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TakenTickets.Width = 125;
            // 
            // PaidTicketAmount
            // 
            this.PaidTicketAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PaidTicketAmount.HeaderText = "Paid Ticket Amount";
            this.PaidTicketAmount.MaxInputLength = 15;
            this.PaidTicketAmount.Name = "PaidTicketAmount";
            this.PaidTicketAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(583, 407);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 1;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(671, 407);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Cancel";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(1, 402);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(278, 11);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "* Employees having vacation policy will be listed above";
            // 
            // TextboxNumeric
            // 
            this.TextboxNumeric.Location = new System.Drawing.Point(365, 160);
            this.TextboxNumeric.Name = "TextboxNumeric";
            this.TextboxNumeric.Size = new System.Drawing.Size(29, 20);
            this.TextboxNumeric.TabIndex = 1008;
            this.TextboxNumeric.Visible = false;
            // 
            // txtNumeric1
            // 
            this.txtNumeric1.Location = new System.Drawing.Point(373, 168);
            this.txtNumeric1.Name = "txtNumeric1";
            this.txtNumeric1.Size = new System.Drawing.Size(29, 20);
            this.txtNumeric1.TabIndex = 1009;
            this.txtNumeric1.Visible = false;
            // 
            // DeductionPolicyStatusStrip
            // 
            this.DeductionPolicyStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.ProjectCreationStatusLabel});
            this.DeductionPolicyStatusStrip.Location = new System.Drawing.Point(0, 436);
            this.DeductionPolicyStatusStrip.Name = "DeductionPolicyStatusStrip";
            this.DeductionPolicyStatusStrip.Size = new System.Drawing.Size(771, 22);
            this.DeductionPolicyStatusStrip.TabIndex = 1010;
            this.DeductionPolicyStatusStrip.Text = "StatusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // ProjectCreationStatusLabel
            // 
            this.ProjectCreationStatusLabel.Name = "ProjectCreationStatusLabel";
            this.ProjectCreationStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(4, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(53, 18);
            this.labelX1.TabIndex = 1;
            this.labelX1.Text = "Company";
            // 
            // textBoxX1
            // 
            // 
            // 
            // 
            this.textBoxX1.Border.Class = "TextBoxBorder";
            this.textBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX1.Location = new System.Drawing.Point(277, 3);
            this.textBoxX1.Name = "textBoxX1";
            this.textBoxX1.Size = new System.Drawing.Size(306, 23);
            this.textBoxX1.TabIndex = 3;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Image = global::MyBooksERP.Properties.Resources.Search;
            this.buttonX1.Location = new System.Drawing.Point(602, 4);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(27, 23);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.TabIndex = 4;
            this.buttonX1.Text = "buttonX1";
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.bar1);
            this.pnlMain.Controls.Add(this.bSearch);
            this.pnlMain.Controls.Add(this.DeductionPolicyStatusStrip);
            this.pnlMain.Controls.Add(this.txtNumeric1);
            this.pnlMain.Controls.Add(this.TextboxNumeric);
            this.pnlMain.Controls.Add(this.Label1);
            this.pnlMain.Controls.Add(this.btnClose);
            this.pnlMain.Controls.Add(this.btnUpdate);
            this.pnlMain.Controls.Add(this.dgvEmployeeLeaveOpening);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(771, 458);
            this.pnlMain.TabIndex = 59;
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BackColor = System.Drawing.Color.Transparent;
            this.bar1.Controls.Add(this.label10);
            this.bar1.Controls.Add(this.cboCompany);
            this.bar1.Controls.Add(this.txtSearch);
            this.bar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch,
            this.btnHelp});
            this.bar1.Location = new System.Drawing.Point(0, 2);
            this.bar1.Name = "bar1";
            this.bar1.PaddingLeft = 10;
            this.bar1.Size = new System.Drawing.Size(771, 25);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 1012;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 15);
            this.label10.TabIndex = 4;
            this.label10.Text = "Company";
            // 
            // cboCompany
            // 
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.Location = new System.Drawing.Point(70, 3);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(203, 23);
            this.cboCompany.TabIndex = 3;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.TextChanged += new System.EventHandler(this.cboCompany_TextChanged);
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(285, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(320, 23);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.Click += new System.EventHandler(this.txtSearch_Click);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "                                                                                " +
                "                   ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyBooksERP.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Tooltip = "Search";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnHelp
            // 
            this.btnHelp.Icon = ((System.Drawing.Icon)(resources.GetObject("btnHelp.Icon")));
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.buttonX1);
            this.bSearch.Controls.Add(this.textBoxX1);
            this.bSearch.Controls.Add(this.labelX1);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.bSearch.Location = new System.Drawing.Point(0, 0);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(771, 2);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 1011;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // FrmLeaveOpening
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(771, 458);
            this.Controls.Add(this.pnlMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLeaveOpening";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Leave Opening";
            this.Load += new System.EventHandler(this.FrmLeaveOpening_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmLeaveOpening_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployeeLeaveOpening)).EndInit();
            this.DeductionPolicyStatusStrip.ResumeLayout(false);
            this.DeductionPolicyStatusStrip.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.bar1.ResumeLayout(false);
            this.bar1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.bSearch.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.DataGridView dgvEmployeeLeaveOpening;
        internal System.Windows.Forms.DataGridViewTextBoxColumn EmployeeID;
        internal System.Windows.Forms.DataGridViewTextBoxColumn EmployeeName;
        internal System.Windows.Forms.DataGridViewTextBoxColumn TakenLeaves;
        internal System.Windows.Forms.DataGridViewTextBoxColumn PaidDays;
        internal System.Windows.Forms.DataGridViewTextBoxColumn TakenTickets;
        internal System.Windows.Forms.DataGridViewTextBoxColumn PaidTicketAmount;
        internal System.Windows.Forms.Button btnUpdate;
        internal System.Windows.Forms.Button btnClose;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox TextboxNumeric;
        internal System.Windows.Forms.TextBox txtNumeric1;
        internal System.Windows.Forms.StatusStrip DeductionPolicyStatusStrip;
        internal System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        internal System.Windows.Forms.ToolStripStatusLabel ProjectCreationStatusLabel;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX1;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        internal System.Windows.Forms.Panel pnlMain;
        private DevComponents.DotNetBar.Bar bSearch;
        private DevComponents.DotNetBar.Bar bar1;
        private System.Windows.Forms.Label label10;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        private System.Windows.Forms.ComboBox cboCompany;
        private DevComponents.DotNetBar.ButtonItem btnHelp;

    }
}