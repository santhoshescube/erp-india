﻿namespace MyBooksERP
{
    partial class FrmSalaryAdvance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label EmployeeIDLabel;
            System.Windows.Forms.Label DateLabel;
            System.Windows.Forms.Label AmountLabel;
            System.Windows.Forms.Label lblTransType;
            System.Windows.Forms.Label lblAccount;
            System.Windows.Forms.Label lblCheqNo;
            System.Windows.Forms.Label lblCheqDate;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSalaryAdvance));
            this.AbsentPolicyBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.btnAccountsettings = new System.Windows.Forms.ToolStripButton();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.GrpMain = new System.Windows.Forms.GroupBox();
            this.lblSalAdvPercentAmt = new System.Windows.Forms.Label();
            this.lblSalAdvPercent = new System.Windows.Forms.Label();
            this.lblBasicPayAmount = new System.Windows.Forms.Label();
            this.lblBasicPay = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.txtChequeNo = new System.Windows.Forms.TextBox();
            this.dtpChequeDate = new System.Windows.Forms.DateTimePicker();
            this.cboAccount = new System.Windows.Forms.ComboBox();
            this.cboTransactionType = new System.Windows.Forms.ComboBox();
            this.lblDateOfJoing = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblStatusShow = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.errorProSalary = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblWarning = new System.Windows.Forms.Label();
            EmployeeIDLabel = new System.Windows.Forms.Label();
            DateLabel = new System.Windows.Forms.Label();
            AmountLabel = new System.Windows.Forms.Label();
            lblTransType = new System.Windows.Forms.Label();
            lblAccount = new System.Windows.Forms.Label();
            lblCheqNo = new System.Windows.Forms.Label();
            lblCheqDate = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.AbsentPolicyBindingNavigator)).BeginInit();
            this.AbsentPolicyBindingNavigator.SuspendLayout();
            this.GrpMain.SuspendLayout();
            this.ssStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProSalary)).BeginInit();
            this.SuspendLayout();
            // 
            // EmployeeIDLabel
            // 
            EmployeeIDLabel.AutoSize = true;
            EmployeeIDLabel.Location = new System.Drawing.Point(8, 23);
            EmployeeIDLabel.Name = "EmployeeIDLabel";
            EmployeeIDLabel.Size = new System.Drawing.Size(53, 13);
            EmployeeIDLabel.TabIndex = 1;
            EmployeeIDLabel.Text = "Employee";
            // 
            // DateLabel
            // 
            DateLabel.AutoSize = true;
            DateLabel.Location = new System.Drawing.Point(8, 48);
            DateLabel.Name = "DateLabel";
            DateLabel.Size = new System.Drawing.Size(30, 13);
            DateLabel.TabIndex = 3;
            DateLabel.Text = "Date";
            // 
            // AmountLabel
            // 
            AmountLabel.AutoSize = true;
            AmountLabel.Location = new System.Drawing.Point(8, 73);
            AmountLabel.Name = "AmountLabel";
            AmountLabel.Size = new System.Drawing.Size(43, 13);
            AmountLabel.TabIndex = 7;
            AmountLabel.Text = "Amount";
            // 
            // lblTransType
            // 
            lblTransType.AutoSize = true;
            lblTransType.Location = new System.Drawing.Point(8, 98);
            lblTransType.Name = "lblTransType";
            lblTransType.Size = new System.Drawing.Size(64, 13);
            lblTransType.TabIndex = 1031;
            lblTransType.Text = "Trans: Type";
            // 
            // lblAccount
            // 
            lblAccount.AutoSize = true;
            lblAccount.Location = new System.Drawing.Point(8, 123);
            lblAccount.Name = "lblAccount";
            lblAccount.Size = new System.Drawing.Size(47, 13);
            lblAccount.TabIndex = 1033;
            lblAccount.Text = "Account";
            // 
            // lblCheqNo
            // 
            lblCheqNo.AutoSize = true;
            lblCheqNo.Location = new System.Drawing.Point(8, 148);
            lblCheqNo.Name = "lblCheqNo";
            lblCheqNo.Size = new System.Drawing.Size(46, 13);
            lblCheqNo.TabIndex = 1034;
            lblCheqNo.Text = "Chq: No";
            // 
            // lblCheqDate
            // 
            lblCheqDate.AutoSize = true;
            lblCheqDate.Location = new System.Drawing.Point(183, 150);
            lblCheqDate.Name = "lblCheqDate";
            lblCheqDate.Size = new System.Drawing.Size(55, 13);
            lblCheqDate.TabIndex = 1035;
            lblCheqDate.Text = "Chq: Date";
            // 
            // AbsentPolicyBindingNavigator
            // 
            this.AbsentPolicyBindingNavigator.AddNewItem = null;
            this.AbsentPolicyBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.AbsentPolicyBindingNavigator.DeleteItem = null;
            this.AbsentPolicyBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorSaveItem,
            this.btnClear,
            this.btnAccountsettings,
            this.btnPrint,
            this.btnEmail,
            this.ToolStripSeparator2,
            this.btnHelp});
            this.AbsentPolicyBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.AbsentPolicyBindingNavigator.MoveFirstItem = null;
            this.AbsentPolicyBindingNavigator.MoveLastItem = null;
            this.AbsentPolicyBindingNavigator.MoveNextItem = null;
            this.AbsentPolicyBindingNavigator.MovePreviousItem = null;
            this.AbsentPolicyBindingNavigator.Name = "AbsentPolicyBindingNavigator";
            this.AbsentPolicyBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.AbsentPolicyBindingNavigator.Size = new System.Drawing.Size(350, 25);
            this.AbsentPolicyBindingNavigator.TabIndex = 302;
            this.AbsentPolicyBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save Data";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnAccountsettings
            // 
            this.btnAccountsettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAccountsettings.Image = global::MyBooksERP.Properties.Resources.AccountSettings;
            this.btnAccountsettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAccountsettings.Name = "btnAccountsettings";
            this.btnAccountsettings.Size = new System.Drawing.Size(23, 22);
            this.btnAccountsettings.ToolTipText = "AccountSettings";
            this.btnAccountsettings.Click += new System.EventHandler(this.btnAccountsettings_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 20);
            this.btnPrint.Text = "Print";
            this.btnPrint.Visible = false;
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 20);
            this.btnEmail.Text = "Email";
            this.btnEmail.Visible = false;
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 20);
            this.btnHelp.Text = "He&lp";
            this.btnHelp.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(11, 339);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(265, 339);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(184, 339);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // GrpMain
            // 
            this.GrpMain.Controls.Add(this.lblWarning);
            this.GrpMain.Controls.Add(this.lblSalAdvPercentAmt);
            this.GrpMain.Controls.Add(this.lblSalAdvPercent);
            this.GrpMain.Controls.Add(this.lblBasicPayAmount);
            this.GrpMain.Controls.Add(this.lblBasicPay);
            this.GrpMain.Controls.Add(this.dtpDate);
            this.GrpMain.Controls.Add(DateLabel);
            this.GrpMain.Controls.Add(this.txtChequeNo);
            this.GrpMain.Controls.Add(this.dtpChequeDate);
            this.GrpMain.Controls.Add(lblCheqDate);
            this.GrpMain.Controls.Add(lblCheqNo);
            this.GrpMain.Controls.Add(lblAccount);
            this.GrpMain.Controls.Add(this.cboAccount);
            this.GrpMain.Controls.Add(lblTransType);
            this.GrpMain.Controls.Add(this.cboTransactionType);
            this.GrpMain.Controls.Add(this.lblDateOfJoing);
            this.GrpMain.Controls.Add(this.Label3);
            this.GrpMain.Controls.Add(this.Label2);
            this.GrpMain.Controls.Add(this.Label1);
            this.GrpMain.Controls.Add(this.cboEmployee);
            this.GrpMain.Controls.Add(this.txtAmount);
            this.GrpMain.Controls.Add(EmployeeIDLabel);
            this.GrpMain.Controls.Add(AmountLabel);
            this.GrpMain.Controls.Add(this.txtRemarks);
            this.GrpMain.Controls.Add(this.ShapeContainer1);
            this.GrpMain.Location = new System.Drawing.Point(6, 28);
            this.GrpMain.Name = "GrpMain";
            this.GrpMain.Size = new System.Drawing.Size(337, 306);
            this.GrpMain.TabIndex = 0;
            this.GrpMain.TabStop = false;
            // 
            // lblSalAdvPercentAmt
            // 
            this.lblSalAdvPercentAmt.AutoSize = true;
            this.lblSalAdvPercentAmt.Location = new System.Drawing.Point(259, 98);
            this.lblSalAdvPercentAmt.Name = "lblSalAdvPercentAmt";
            this.lblSalAdvPercentAmt.Size = new System.Drawing.Size(61, 13);
            this.lblSalAdvPercentAmt.TabIndex = 1039;
            this.lblSalAdvPercentAmt.Text = "Advance %";
            // 
            // lblSalAdvPercent
            // 
            this.lblSalAdvPercent.AutoSize = true;
            this.lblSalAdvPercent.Location = new System.Drawing.Point(183, 96);
            this.lblSalAdvPercent.Name = "lblSalAdvPercent";
            this.lblSalAdvPercent.Size = new System.Drawing.Size(61, 13);
            this.lblSalAdvPercent.TabIndex = 1038;
            this.lblSalAdvPercent.Text = "Advance %";
            // 
            // lblBasicPayAmount
            // 
            this.lblBasicPayAmount.AutoSize = true;
            this.lblBasicPayAmount.Location = new System.Drawing.Point(259, 74);
            this.lblBasicPayAmount.Name = "lblBasicPayAmount";
            this.lblBasicPayAmount.Size = new System.Drawing.Size(54, 13);
            this.lblBasicPayAmount.TabIndex = 1037;
            this.lblBasicPayAmount.Text = "Basic Pay";
            // 
            // lblBasicPay
            // 
            this.lblBasicPay.AutoSize = true;
            this.lblBasicPay.Location = new System.Drawing.Point(183, 73);
            this.lblBasicPay.Name = "lblBasicPay";
            this.lblBasicPay.Size = new System.Drawing.Size(54, 13);
            this.lblBasicPay.TabIndex = 1036;
            this.lblBasicPay.Text = "Basic Pay";
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(80, 46);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(92, 20);
            this.dtpDate.TabIndex = 1;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // txtChequeNo
            // 
            this.txtChequeNo.Location = new System.Drawing.Point(80, 148);
            this.txtChequeNo.MaxLength = 99;
            this.txtChequeNo.Name = "txtChequeNo";
            this.txtChequeNo.Size = new System.Drawing.Size(97, 20);
            this.txtChequeNo.TabIndex = 5;
            this.txtChequeNo.TextChanged += new System.EventHandler(this.txtChequeNo_TextChanged);
            // 
            // dtpChequeDate
            // 
            this.dtpChequeDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpChequeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpChequeDate.Location = new System.Drawing.Point(244, 148);
            this.dtpChequeDate.Name = "dtpChequeDate";
            this.dtpChequeDate.Size = new System.Drawing.Size(85, 20);
            this.dtpChequeDate.TabIndex = 6;
            // 
            // cboAccount
            // 
            this.cboAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAccount.BackColor = System.Drawing.SystemColors.HighlightText;
            this.cboAccount.DropDownHeight = 134;
            this.cboAccount.FormattingEnabled = true;
            this.cboAccount.IntegralHeight = false;
            this.cboAccount.Location = new System.Drawing.Point(80, 122);
            this.cboAccount.Name = "cboAccount";
            this.cboAccount.Size = new System.Drawing.Size(249, 21);
            this.cboAccount.TabIndex = 4;
            this.cboAccount.SelectedIndexChanged += new System.EventHandler(this.cboAccount_SelectedIndexChanged);
            // 
            // cboTransactionType
            // 
            this.cboTransactionType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboTransactionType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTransactionType.BackColor = System.Drawing.SystemColors.Info;
            this.cboTransactionType.DropDownHeight = 134;
            this.cboTransactionType.FormattingEnabled = true;
            this.cboTransactionType.IntegralHeight = false;
            this.cboTransactionType.Location = new System.Drawing.Point(80, 96);
            this.cboTransactionType.Name = "cboTransactionType";
            this.cboTransactionType.Size = new System.Drawing.Size(92, 21);
            this.cboTransactionType.TabIndex = 3;
            this.cboTransactionType.SelectedIndexChanged += new System.EventHandler(this.cboTransactionType_SelectedIndexChanged);
            // 
            // lblDateOfJoing
            // 
            this.lblDateOfJoing.AutoSize = true;
            this.lblDateOfJoing.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateOfJoing.Location = new System.Drawing.Point(259, 46);
            this.lblDateOfJoing.Name = "lblDateOfJoing";
            this.lblDateOfJoing.Size = new System.Drawing.Size(58, 13);
            this.lblDateOfJoing.TabIndex = 1028;
            this.lblDateOfJoing.Text = "Joing Date";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(183, 46);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(66, 13);
            this.Label3.TabIndex = 1029;
            this.Label3.Text = "Joining Date";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(2, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(82, 13);
            this.Label2.TabIndex = 0;
            this.Label2.Text = "Advance Info";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(2, 173);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(65, 13);
            this.Label1.TabIndex = 74;
            this.Label1.Text = "Other Info";
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(80, 20);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(249, 21);
            this.cboEmployee.TabIndex = 0;
            this.cboEmployee.SelectedIndexChanged += new System.EventHandler(this.cboEmployee_SelectedIndexChanged);
            this.cboEmployee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboEmployee_KeyDown);
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtAmount.Location = new System.Drawing.Point(80, 71);
            this.txtAmount.MaxLength = 10;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(92, 20);
            this.txtAmount.TabIndex = 2;
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmount.TextChanged += new System.EventHandler(this.txtAmount_TextChanged);
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(11, 192);
            this.txtRemarks.MaxLength = 200;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRemarks.Size = new System.Drawing.Size(317, 92);
            this.txtRemarks.TabIndex = 7;
            this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.ShapeContainer1.Size = new System.Drawing.Size(331, 287);
            this.ShapeContainer1.TabIndex = 17;
            this.ShapeContainer1.TabStop = false;
            // 
            // LineShape1
            // 
            this.LineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 62;
            this.LineShape1.X2 = 324;
            this.LineShape1.Y1 = 166;
            this.LineShape1.Y2 = 166;
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusShow,
            this.lblstatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 367);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(350, 22);
            this.ssStatus.TabIndex = 311;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblStatusShow
            // 
            this.lblStatusShow.Name = "lblStatusShow";
            this.lblStatusShow.Size = new System.Drawing.Size(48, 17);
            this.lblStatusShow.Text = "Status : ";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // tmrClear
            // 
            this.tmrClear.Tick += new System.EventHandler(this.tmrClear_Tick);
            // 
            // errorProSalary
            // 
            this.errorProSalary.ContainerControl = this;
            this.errorProSalary.RightToLeft = true;
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarning.Location = new System.Drawing.Point(8, 288);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(67, 11);
            this.lblWarning.TabIndex = 1040;
            this.lblWarning.Text = "*** Basic Pay";
            // 
            // FrmSalaryAdvance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 389);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.GrpMain);
            this.Controls.Add(this.AbsentPolicyBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSalaryAdvance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Salary Advance";
            this.Load += new System.EventHandler(this.FrmSalaryAdvance_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSalaryAdvance_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.AbsentPolicyBindingNavigator)).EndInit();
            this.AbsentPolicyBindingNavigator.ResumeLayout(false);
            this.AbsentPolicyBindingNavigator.PerformLayout();
            this.GrpMain.ResumeLayout(false);
            this.GrpMain.PerformLayout();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProSalary)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator AbsentPolicyBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.GroupBox GrpMain;
        internal System.Windows.Forms.Label lblDateOfJoing;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.ComboBox cboEmployee;
        internal System.Windows.Forms.TextBox txtAmount;
        internal System.Windows.Forms.DateTimePicker dtpDate;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatusShow;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        private System.Windows.Forms.Timer tmrClear;
        private System.Windows.Forms.ErrorProvider errorProSalary;
        internal System.Windows.Forms.ComboBox cboAccount;
        internal System.Windows.Forms.ComboBox cboTransactionType;
        private System.Windows.Forms.TextBox txtChequeNo;
        internal System.Windows.Forms.DateTimePicker dtpChequeDate;
        internal System.Windows.Forms.Label lblSalAdvPercentAmt;
        internal System.Windows.Forms.Label lblSalAdvPercent;
        internal System.Windows.Forms.Label lblBasicPayAmount;
        internal System.Windows.Forms.Label lblBasicPay;
        private System.Windows.Forms.ToolStripButton btnAccountsettings;
        internal System.Windows.Forms.Label lblWarning;
    }
}