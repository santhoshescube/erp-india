﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

/*****************************************************
 * Created By    :Ranju Mathew
 * Creation Date : 02 May 2013
 * Description   : Handles Employee Leave Entry
 * FormID        : 150
 *****************************************************/
/*****************************************************
 * Modified By    :Sanju
 * Creation Date : 14 Aug 2013
 * Description   : Optimization of form
 *****************************************************/
namespace MyBooksERP
{
    public partial class FrmLeaveEntry : Form
    {
        #region DECLARATIONS
        private bool MbChangeStatus;  //Check state of the page
        private bool MbAddStatus; //Add/Update mode 
        private string MsMessageBody;// Messagebox display
        public ArrayList MaMessage; //   Error Message display
        private ArrayList MaStatusMessage;
        public int MiCompanyID = 0; // Selected Employees CompanyID
        public int MiLeavepolicyID = 0;
        //------------permission-------------------//
        private bool MbAddPermission;
        private bool MbUpdatePermission;
        private bool MbDeletePermission;
        private bool MbPrintEmailPermission;
        private bool MblnISattendanceExists = false;
        private bool mblnViewLeaveExtension = false;
        private bool mlnLeaveExtensionPermission = false;
        //--------------------------------------------//
        private bool MblnSearch = false;//Search 
        public int PiEmpid = 0;
        private DateTime FinYearDate;
        private decimal OldNoOfLeaves = 0;
        private decimal dEncasheddays = 0;
        private bool bSearchFlag = false;
        private bool bIncludeHolidays = true;
        private int iLeavetypeID = 0;
        public int TotalRecordCnt, CurrentRecCnt;
        private int TempLeaveType = 0;
        private string MstrCommonMessage;
        public int GlYearCounter;
        private string MstrMessageCaption;
        private clsBLLLeaveEntry mobjclsBLLLeaveEntry = null;
        private ClsLogWriter mObjLogs = null;
        private ClsNotification mObjNotification = null;
        private clsBLLPermissionSettings objClsBLLPermissionSettings = null;
        private MessageBoxIcon MmessageIcon;
        private clsConnection objclsConnection = null;
        public int EmployeeID
        {
            set
            {
                PiEmpid = value;
            }
        }
        string strBindingOf = "Of ";
        #endregion
        #region CONSTRUCTOR
        public FrmLeaveEntry()
        {
            InitializeComponent();
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            mobjclsBLLLeaveEntry = new clsBLLLeaveEntry();
            objclsConnection = new clsConnection();
            MstrMessageCaption = ClsCommonSettings.MessageCaption;

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.LeaveEntry, this);

        //    bnMoreActions.Text = "الإجراءات";
        //    bnMoreActions.ToolTipText = "الإجراءات";
        //    btnDocument.Text = "وثائق";
        //    BtnLeaveExtension.Text = "ترك التمديد";
        //    StatusLabel.Text = "حالة :";
        //    txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
        //    strBindingOf = "من ";
        //}
        #endregion
        #region METHODS
        private void LoadMesage()
        {
            mObjNotification = new ClsNotification();
            MaMessage = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessage = mObjNotification.FillMessageArray((int)FormID.LeaveEntry, 2);
            MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.LeaveEntry, 2);
        }

        private void GetRecordCount(int iEmpID)
        {
            try
            {
                TotalRecordCnt = 0;
                CurrentRecCnt = 0;
                DataTable dtCombos = null;
                if (iEmpID == 0)
                {
                    dtCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "isnull(count(1),0)", "PayEmployeeLeaveDetails", "CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + " and CompanyID=" + ClsCommonSettings.LoginCompanyID + ")" });
                    //TSQL = "Select isnull(count(1),0) from employeeleavedetails";
                }
                else
                {
                    // TSQL = "Select isnull(count(1),0) from employeeleavedetails WHERE employeeid=" + iEmpID + "";
                    dtCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "isnull(count(1),0)", "PayEmployeeLeaveDetails", " EmployeeID=" + iEmpID  });
                }


                if (dtCombos.Rows.Count > 0 && dtCombos != null)
                {
                    TotalRecordCnt = dtCombos.Rows[0][0].ToInt32();
                    CurrentRecCnt = TotalRecordCnt;
                    BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(TotalRecordCnt) + "";
                    BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                }
                else
                {
                    BindingNavigatorCountItem.Text = strBindingOf + "0";
                    CurrentRecCnt = 0;
                    TotalRecordCnt = 0;
                }

            }
            catch (Exception ex){ }
        }

        private void RecCountNavigate(int iEmpID)
        {
            try
            {
                TotalRecordCnt = 0;
                DataTable dtCombos = null;
                if (iEmpID == 0)
                {
                    dtCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "isnull(count(1),0)", "PayEmployeeLeaveDetails", "CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +")" });

                }
                else
                {

                    dtCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "isnull(count(1),0)", "PayEmployeeLeaveDetails", " EmployeeID=" + iEmpID + " AND CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +")" });
                }


                if (dtCombos.Rows.Count > 0 && dtCombos != null)
                {
                    TotalRecordCnt = dtCombos.Rows[0][0].ToInt32();
                    BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(TotalRecordCnt) + "";
                    BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                }
                else
                {
                    BindingNavigatorCountItem.Text = strBindingOf + "0";
                    TotalRecordCnt = 0;
                }

            }
            catch { }
        }

        private void EnableDisablButtons()
        {
            if (MbAddStatus)
            {

                BindingNavigatorAddNewItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                BtnPrint.Enabled = BtnEmail.Enabled = false;
                bnMoreActions.Enabled = false;
                if (MbChangeStatus)
                {
                    EmployeeLeaveDetailsBindingNavigatorSaveItem.Enabled = MbAddPermission;
                    BtnSave.Enabled = MbAddPermission;
                    BtnOk.Enabled = MbAddPermission;
                    BindingNavigatorCancelItem.Enabled = MbAddPermission;
                }
                else
                {
                    EmployeeLeaveDetailsBindingNavigatorSaveItem.Enabled = false;
                    BtnSave.Enabled = false;
                    BtnOk.Enabled = false;
                    BindingNavigatorCancelItem.Enabled = false;
                }

            }
            else
            {

                BindingNavigatorAddNewItem.Enabled = MbAddPermission;
                BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
                BindingNavigatorCancelItem.Enabled = false;
                BtnPrint.Enabled = BtnEmail.Enabled = MbPrintEmailPermission;
                bnMoreActions.Enabled = true;
                BtnLeaveExtension.Enabled = mblnViewLeaveExtension;
                //if(!ClsCommonSettings.IsHrPowerEnabled)
                //    btnDocument.Enabled = (MbAddPermission || MbUpdatePermission) ? true : false;
                //else
                   //btnDocument.Enabled  =  CboEmployee.SelectedIndex > -1 ;
                    
                   

                if (MbChangeStatus)
                {
                    EmployeeLeaveDetailsBindingNavigatorSaveItem.Enabled = MbUpdatePermission;
                    BtnSave.Enabled = MbUpdatePermission;
                    BtnOk.Enabled = MbUpdatePermission;
                }
                else
                {
                    EmployeeLeaveDetailsBindingNavigatorSaveItem.Enabled = false;
                    BtnSave.Enabled = false;
                    BtnOk.Enabled = false;
                }
            }

        }

        private void setBindingNavigatorbuttons()
        {
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            if (CurrentRecCnt == TotalRecordCnt)
            {
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }

            if (CurrentRecCnt == 1)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
            }
        }

        private void setBindingAddNew()
        {
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = false;
            BindingNavigatorMoveLastItem.Enabled = false;
        }

        private void ClearAllControls()
        {
            LoadCombos(1);
            bIncludeHolidays = true;
            ErrorProviderEmpLeave.Clear();
            MbAddStatus = true;
            CboEmployee.DataSource = null;

            DataTable datCombos = new DataTable();
            //if (ClsCommonSettings.IsArabicView)
            //    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "E.EmployeeID , " +
            //        "ISNULL((isnull(E.EmployeeFullNameArb,'') +'-' + isnull(cast(E.EmployeeNumber as varchar(30)),'')),'') + ' ['+C.ShortName+ ']' AS FirstName," +
            //        "E.EmployeeNumber ", " EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID ", " " +
            //        "isnull(E.LeavePolicyID,0)<> 0 And E.WorkstatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") order by E.FirstName" });
            //else
                datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "E.EmployeeID , " +
                    "ISNULL((isnull(E.EmployeeFullName,'') +'-' + isnull(cast(E.EmployeeNumber as varchar(30)),'')),'') + ' ['+C.ShortName+ ']' AS FirstName," +
                    "E.EmployeeNumber ", " EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID ", " " +
                    "isnull(E.LeavePolicyID,0)<> 0 And E.WorkstatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + " and CompanyID ="+ ClsCommonSettings.LoginCompanyID   +") order by E.FirstName" });
            CboEmployee.ValueMember = "EmployeeId";
            CboEmployee.DisplayMember = "FirstName";
            CboEmployee.DataSource = datCombos;
            if (CboEmployee.Items.Count > 0)
            {
                CboEmployee.SelectedIndex = -1;
            }
            LblDOJ.Text = "";
            lblholidaysValue.Text = "0";
            lblBalLeaveValue.Text = "0";
            LeaveDateFromDateTimePicker.Value = System.DateTime.Now;
            LeaveDateToDateTimePicker.Value = System.DateTime.Now;
            RemarksTextBox.Text = "";
            RejoinDateDateTimePicker.Value = LeaveDateToDateTimePicker.Value.AddDays(1);
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(TotalRecordCnt) + "";
            BindingNavigatorPositionItem.Text = CurrentRecCnt.ToString();
        }

        private void ClearSearchControls()
        {

            txtSearch.Text = "";
            MblnSearch = false;

        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                //objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID , (Int32)eModuleID.Payroll, (Int32)eMenuID.LeaveExtension, out mlnLeaveExtensionPermission, out mlnLeaveExtensionPermission, out mlnLeaveExtensionPermission, out mlnLeaveExtensionPermission, out mblnViewLeaveExtension);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (int)eMenuID.LeaveEntry, out MbPrintEmailPermission, out MbAddPermission, out MbUpdatePermission, out MbDeletePermission);
            }
            else
                mblnViewLeaveExtension = MbAddPermission = MbUpdatePermission = MbDeletePermission = MbPrintEmailPermission = true;

            //if (ClsCommonSettings.IsHrPowerEnabled)
            //    MbAddPermission = MbUpdatePermission = MbDeletePermission = false;
        }

        public void LoadCombos(int intFillType)
        {
            try
            {
                DataTable datCombos = new DataTable();
                if (intFillType == 1)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    if (PiEmpid == 0)
                    //    {
                    //        datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "E.EmployeeID , " +
                    //            "ISNULL((isnull(E.EmployeeFullNameArb,'') +'-' + isnull(cast(E.EmployeeNumber as varchar(30)),'')),'') + ' ['+C.ShortName+ ']' AS FirstName," +
                    //            "E.EmployeeNumber ", " EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID ", " isnull(E.LeavePolicyID,0)<> 0 and  E.WorkStatusID >= 6 " +
                    //            "AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") order by E.FirstName" });
                    //        CboEmployee.ValueMember = "EmployeeID";
                    //        CboEmployee.DisplayMember = "FirstName";
                    //        CboEmployee.DataSource = datCombos;
                    //        CboEmployee.SelectedIndex = -1;
                    //    }
                    //    else
                    //    {
                    //        datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "E.EmployeeID , " +
                    //            "ISNULL((isnull(E.EmployeeFullNameArb,'') +'-' + isnull(cast(E.EmployeeNumber as varchar(30)),'')),'') + ' ['+C.ShortName+ ']' AS FirstName ," +
                    //            "E.EmployeeNumber", " EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID ", " isnull(E.LeavePolicyID,0)<> 0 and E.EmployeeID =" + PiEmpid + " and  " +
                    //            "E.WorkStatusID >= 6 order by E.FirstName" });
                    //        CboEmployee.ValueMember = "EmployeeID";
                    //        CboEmployee.DisplayMember = "FirstName";
                    //        CboEmployee.DataSource = datCombos;
                    //    }
                    //}
                    //else
                    //{
                        if (PiEmpid == 0)
                        {
                            datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "E.EmployeeID , " +
                                "ISNULL((isnull(E.EmployeeFullName,'') +'-' + isnull(cast(E.EmployeeNumber as varchar(30)),'')),'') + ' ['+C.ShortName+ ']' AS FirstName," +
                                "E.EmployeeNumber ", " EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID ", " isnull(E.LeavePolicyID,0)<> 0 and  E.WorkStatusID >= 6 " +
                                "AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + " and CompanyID="+ ClsCommonSettings.LoginCompanyID +") order by E.FirstName" });
                            CboEmployee.ValueMember = "EmployeeID";
                            CboEmployee.DisplayMember = "FirstName";
                            CboEmployee.DataSource = datCombos;
                            CboEmployee.SelectedIndex = -1;
                        }
                        else
                        {
                            datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "E.EmployeeID , " +
                                "ISNULL((isnull(E.EmployeeFullName,'') +'-' + isnull(cast(E.EmployeeNumber as varchar(30)),'')),'') + ' ['+C.ShortName+ ']' AS FirstName ," +
                                "E.EmployeeNumber", " EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID ", " isnull(E.LeavePolicyID,0)<> 0 and E.EmployeeID =" + PiEmpid + " and  " +
                                "E.WorkStatusID >= 6 and CompanyID="+ ClsCommonSettings.LoginCompanyID + " order by E.FirstName" });
                            CboEmployee.ValueMember = "EmployeeID";
                            CboEmployee.DisplayMember = "FirstName";
                            CboEmployee.DataSource = datCombos;
                        }
                    //}
                }

                //if (ClsCommonSettings.IsArabicView)
                //    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LeaveTypeID,LeaveTypeArb AS LeaveType ", "PayLeaveTypeReference", " 1=1 order by LeaveTypeID" });
                //else
                    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LeaveTypeID,LeaveType ", "PayLeaveTypeReference", " 1=1 order by LeaveTypeID" });

                CboLeaveType.ValueMember = "LeaveTypeID";
                CboLeaveType.DisplayMember = "LeaveType";
                CboLeaveType.DataSource = datCombos;

                if (intFillType == 0)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "E.EmployeeID," +
                    //        "ISNULL((isnull(E.EmployeeFullNameArb,'') +'-' + isnull(cast(E.EmployeeNumber as varchar(30)),'')),'') + ' ['+C.ShortName+ ']' AS FirstName," +
                    //        "E.EmployeeNumber", " EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", " E.WorkstatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +") order by E.FirstName" });
                    //else
                        datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "E.EmployeeID," +
                            "ISNULL((isnull(E.EmployeeFullName,'') +'-' + isnull(cast(E.EmployeeNumber as varchar(30)),'')),'') + ' ['+C.ShortName+ ']' AS FirstName," +
                            "E.EmployeeNumber", " EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", " E.WorkstatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + " and CompanyID ="+ ClsCommonSettings.LoginCompanyID   +") order by E.FirstName" });

                    CboEmployee.ValueMember = "EmployeeID";
                    CboEmployee.DisplayMember = "FirstName";
                    CboEmployee.DataSource = datCombos;
                }

                if (intFillType == 2)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "E.EmployeeID , " +
                    //        "ISNULL((isnull(E.EmployeeFullNameArb,'') +'-' + isnull(cast(E.EmployeeNumber as varchar(30)),'')),'') + ' ['+C.ShortName+ ']' AS FirstName," +
                    //        "E.EmployeeNumber ", " EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID ", " isnull(E.LeavePolicyID,0)<> 0  And e.WorkstatusID >= 6 " +
                    //        "AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") order by E.FirstName" });
                    //else
                        datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "E.EmployeeID , " +
                            "ISNULL((isnull(E.EmployeeFullName,'') +'-' + isnull(cast(E.EmployeeNumber as varchar(30)),'')),'') + ' ['+C.ShortName+ ']' AS FirstName," +
                            "E.EmployeeNumber ", " EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID ", " isnull(E.LeavePolicyID,0)<> 0  And e.WorkstatusID >= 6 " +
                            "AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + " and CompanyID ="+ ClsCommonSettings.LoginCompanyID   +") order by E.FirstName" });
                    CboEmployee.ValueMember = "EmployeeID";
                    CboEmployee.DisplayMember = "FirstName";
                    CboEmployee.DataSource = datCombos;
                }

                if (CboEmployee.Items.Count > 0)
                {
                    CboEmployee.SelectedIndex = -1;
                    CboLeaveType.SelectedIndex = -1;
                }

                if (PiEmpid == 0)
                    CboEmployee.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in LoadCombos() " + ex.Message);
                mObjLogs.WriteLog("Error in LoadCombos() " + ex.Message, 2);
            }
        }

        private void LoadInitial()
        {
            try
            {

                EnableDisablButtons();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadInitial:LeaveEntry." + ex.Message);
                mObjLogs.WriteLog("Error on LoadInitial:LeaveEntry." + ex.Message, 2);
            }
        }

        private void SetControls()
        {

            HalfDayCheckBox.Checked = false;
            LblDateTo.Visible = true;
            LeaveDateToDateTimePicker.Visible = true;
            LblRejoinDate.Visible = false;
            RejoinDateDateTimePicker.Visible = false;

        }

        public void AddNewLeaveEntry()
        {
            try
            {
                ClearSearchControls();
                bSearchFlag = true;
                LoadCombos(1);
                ErrorProviderEmpLeave.Clear();
                MbAddStatus = true;
                LoadCombos(2);
                CboEmployee.Text = "";
                CboEmployee.Tag = 0;
                if (CboEmployee.Items.Count > 0)
                {
                    CboEmployee.SelectedIndex = -1;
                }
                LblDOJ.Text = "";
                lblholidaysValue.Text = "0";
                lblBalLeaveValue.Text = "0";
                LeaveDateFromDateTimePicker.Value = ClsCommonSettings.GetServerDate();
                LeaveDateFromDateTimePicker.MaxDate = LeaveDateFromDateTimePicker.Value.AddYears(1);
                LeaveDateToDateTimePicker.MaxDate = LeaveDateFromDateTimePicker.Value.AddYears(1).AddDays(1);
                LeaveDateToDateTimePicker.Value = LeaveDateFromDateTimePicker.Value;
                GetRecordCount(PiEmpid);
                TotalRecordCnt = TotalRecordCnt + 1;
                CurrentRecCnt = CurrentRecCnt + 1;
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(TotalRecordCnt) + "";
                BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                RemarksTextBox.Text = "";
                RejoinDateDateTimePicker.Value = LeaveDateToDateTimePicker.Value.AddDays(1);
                LblBalLeave.Visible = true;
                lblBalLeaveValue.Visible = true;
                SetControls();
                if (CboLeaveType.Items.Count > 0)
                {
                    CboLeaveType.SelectedIndex = -1;
                }

                CboEmployee.Enabled = true;
                CboEmployee.Focus();
                bSearchFlag = false;
                if (PiEmpid > 0)
                {
                    CboEmployee.DropDownStyle = ComboBoxStyle.Simple;//when called from other forms if data not exist it will go to add mode,if employee not valid the combobox will be empty 
                    CboEmployee.Enabled = false;
                    CboEmployee.SelectedValue = PiEmpid;
                    CboLeaveType.Focus();
                }
                else // if (PiEmpid == 0)
                {
                    CboEmployee.SelectedIndex = -1;
                    bSearchFlag = true;
                }
                GrpMain.Enabled = true;
                TimerLeave.Enabled = true;
                setBindingAddNew();
                if (TotalRecordCnt == 1)
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
                chkPaidUnPaid.Checked = false;
                bSearchFlag = false;
                MbChangeStatus = false;
                SetAutoCompleteList();
                //if (ClsCommonSettings.IsArabicView)
                //    lblstatus.Text = "إضافة إجازة إدخال جديد";
                //else
                    lblstatus.Text = "Add new leave entry";
                TimerLeave.Enabled = true;
                EnableDisablButtons();

            }

            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorAddNewItem_Click:LeaveEntry." + ex.Message);
                mObjLogs.WriteLog(" Error on BindingNavigatorAddNewItem_Click:LeaveEntry." + ex.Message, 2);
            }

        }

        private bool checkWithBalLeave()
        {
            try
            {
                ErrorProviderEmpLeave.Clear();
                if (HalfDayCheckBox.Checked == true)
                {
                    LeaveDateToDateTimePicker.Value = LeaveDateFromDateTimePicker.Value;
                }
                //When Adding new Leave Detail


                if (MbAddStatus == true)
                {

                    decimal LeaveTaken = 0;

                    if (HalfDayCheckBox.Checked == false)
                        LeaveTaken = (LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value).TotalDays.ToInt32() + 1;
                    else if (HalfDayCheckBox.Checked == true)
                        LeaveTaken = 0.5M;

                    if (LeaveTaken > (lblBalLeaveValue.Text).ToDecimal() || (lblBalLeaveValue.Text).ToDecimal() == 0)
                    {
                        ErrorProviderEmpLeave.SetError(LeaveDateFromDateTimePicker, new ClsNotification().GetErrorMessage(MaMessage, 10133, out MmessageIcon));
                        MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10100, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10100, out MmessageIcon);
                        TimerLeave.Enabled = true;
                        LeaveDateFromDateTimePicker.Focus();
                        return false;
                    }
                }
                // When Updating new Leave detail
                else if (MbAddStatus == false)
                {
                    string testdate = "";
                    string s1 = LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy");
                    FinYearDate = mobjclsBLLLeaveEntry.GetFinYearStartDate(MiCompanyID, s1);
                    testdate = FinYearDate.ToString();
                    double TakenLeave = 0;
                    decimal CurrentTakenLeave = 0;
                    decimal decBalanceLeave;
                    TakenLeave = mobjclsBLLLeaveEntry.GetLeaveTaken(MiCompanyID, MiLeavepolicyID, Convert.ToInt32(CboEmployee.SelectedValue), Convert.ToInt32(CboLeaveType.SelectedValue), testdate, out decBalanceLeave).ToDouble();
                    lblBalLeaveValue.Text = decBalanceLeave.ToString();
                    CurrentTakenLeave = mobjclsBLLLeaveEntry.GetCurrentLeaveTaken(CboEmployee.Tag.ToInt32());
                    decimal dGetExtendedleave = GetExtensionleave(MiCompanyID, Convert.ToInt32(CboEmployee.SelectedValue), Convert.ToInt32(CboLeaveType.SelectedValue), LeaveDateFromDateTimePicker.Value.Month, LeaveDateFromDateTimePicker.Value.Year);
                    decimal dballeave = Convert.ToDecimal(lblBalLeaveValue.Text);
                    lblBalLeaveValue.Text = dballeave.ToString() + dGetExtendedleave.ToString();
                    decimal LeaveTaken = 0;
                    if (HalfDayCheckBox.Checked == false)
                        LeaveTaken = (LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value).TotalDays.ToInt32() + 1;
                    else if (HalfDayCheckBox.Checked == true)
                        LeaveTaken = 0.5M;
                    if (LeaveTaken > (lblBalLeaveValue.Text).ToDecimal() + CurrentTakenLeave)
                    {
                        ErrorProviderEmpLeave.SetError(LeaveDateFromDateTimePicker, new ClsNotification().GetErrorMessage(MaMessage, 10133, out MmessageIcon));
                        MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10100, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10100, out MmessageIcon);
                        TimerLeave.Enabled = true;
                        LeaveDateFromDateTimePicker.Focus();

                        return false;
                    }
                }
                return true;
            }

            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorAddNewItem_Click:LeaveEntry." + ex.Message);
                mObjLogs.WriteLog(" Error on BindingNavigatorAddNewItem_Click:LeaveEntry." + ex.Message, 2);
                return false;
            }
        }

        private DateTime GetFinYear(int iCompanyId, System.DateTime Leavedate)
        {

            DateTime dCurEmpFinYear, dtstartdate = ClsCommonSettings.GetServerDate();
            int icnt = 0;
            string cFinYer = "";
            DataTable sdrFinYear = null;
            sdrFinYear = mobjclsBLLLeaveEntry.FillCombos(new string[] { " convert(char(15),FinYearStartDate,106) as finyearstartdate", "CompanyMaster", " CompanyID = " + iCompanyId });
            if (sdrFinYear.Rows.Count > 0 && sdrFinYear != null)
            {
                cFinYer = Convert.ToString(sdrFinYear.Rows[0]["finyearstartdate"]);
                cFinYer = cFinYer.Substring(7, 4).Trim();
                dtstartdate = sdrFinYear.Rows[0]["finyearstartdate"].ToDateTime();
            }
            dCurEmpFinYear = dtstartdate;

            icnt = ClsCommonSettings.GetServerDate().Year + GlYearCounter - cFinYer.ToInt32();
            //icnt = (Year(Now()) + GlYearCounter) - cFinYer;
            for (int i = 0; i <= icnt - 1; i++)
            {
                DateTime dFRomdate = Convert.ToDateTime(dtstartdate.ToString("dd-MMM-yyyy"));//Convert.ToString(Format(dtstartdate, "dd-MMM-yyyy"));
                DateTime dTodate = Convert.ToDateTime(dtstartdate.AddYears(1).AddDays(-1).ToString("dd-MMM-yyyy")); //Convert.ToString(Format(dtstartdate.AddYears(1).AddDays(-1), "dd-MMM-yyyy"));

                if (Leavedate >= dFRomdate & Leavedate <= dTodate)
                {
                    FinYearDate = dFRomdate;
                    return dFRomdate;
                }
                dtstartdate = dtstartdate.AddYears(1);
            }

            return dCurEmpFinYear;

        }

        private decimal GetExtensionleave(int ComiD, int EmpID, int LeavetypeID, int iMonth, int iyear)
        {

            decimal dextendedleaves = 0;
            string FinStarDate = GetFinYear(ComiD, LeaveDateFromDateTimePicker.Value.Date).ToString("dd-MMM-yyyy");
            DataSet dt = objclsConnection.FillDataSet("select distinct isnull(NoOfDays,0) as NoOfDays from PayLeaveExtenstion where LeaveExtensionTypeID = 1 and " + " CompanyID=" + ComiD + " and EmployeeID=" + EmpID + " and LeavetypeID=" + LeavetypeID + " and MonthYear=convert(datetime,'" + FinStarDate + "',106)");
            if (dt.Tables[0].Rows.Count > 0)
            {
                dextendedleaves = ((dt.Tables[0].Rows[0]["NoOfDays"].ToDecimal() > 0) ? dt.Tables[0].Rows[0]["NoOfDays"].ToDecimal() : 0);
            }
            dt = null;
            return dextendedleaves;

        }

        private bool SaveEmpLeave()
        {
            if (!Checkvisibility(CboEmployee.SelectedValue.ToInt32(), true))
            {
                return false;
            }
            decimal dNoOfHoliday = 0;
            decimal iholidays = 0;
            decimal iNoOfDays = 0;
            iholidays = lblholidaysValue.Text.ToDecimal();
            if (HalfDayCheckBox.Checked == false)
            {
                iNoOfDays = (LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value).TotalDays.ToInt32() + 1;
            }
            else
            {
                iNoOfDays = 0.5M;
            }

            if (MbAddStatus == true)
            {
                MsMessageBody = new ClsNotification().GetErrorMessage(MaMessage, 1, out MmessageIcon).Replace("#", "");
            }
            else
            {
                MsMessageBody = new ClsNotification().GetErrorMessage(MaMessage, 3, out MmessageIcon).Replace("#", "");
            }
            ErrorProviderEmpLeave.Clear();
            if ((this.Validate()))
            {

                if (MessageBox.Show(MsMessageBody, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    if (iNoOfDays > 1)
                    {
                        if (bIncludeHolidays == false)
                        {
                            iNoOfDays = iNoOfDays - iholidays;
                            dNoOfHoliday = iholidays;
                        }

                    }
                    //---------------------------------------------------------------------------------------------

                    //-----------------------------Start(Leave entry insertion)---------------------------------
                    object iResultID = null;
                    if (MbAddStatus == true)
                    {
                        mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intLeaveID = 0;

                    }
                    else
                    {
                        mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intLeaveID = Convert.ToInt32(CboEmployee.Tag);
                    }
                    mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intEmployeeID = Convert.ToInt32(CboEmployee.SelectedValue);
                    mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intLeaveTypeID = Convert.ToInt32(CboLeaveType.SelectedValue);
                    mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.blnHalfDay = HalfDayCheckBox.Checked;
                    mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.strLeaveDateFrom = LeaveDateFromDateTimePicker.Value.Date.ToString();
                    mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.strLeaveDateTo = LeaveDateToDateTimePicker.Value.Date.ToString();
                    mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.strRejoinDate = RejoinDateDateTimePicker.Value.Date.ToString();
                    mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.strRemarks = RemarksTextBox.Text.Trim();
                    mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intCompanyID = MiCompanyID;
                    mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.dblNoOfDays = iNoOfDays.ToDouble();
                    mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.dblNoOfHolidays = dNoOfHoliday.ToDouble();
                    mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.blnPaidLeave = (chkPaidUnPaid.CheckState == CheckState.Checked ? false : true);

                    //If Absent is alredy marked from Attendance then it deleted first
                    mobjclsBLLLeaveEntry.DeleteAbsentEntryFromAttendance();
                    //Leave entry insertion
                    mobjclsBLLLeaveEntry.SaveLeaveEntry(MbAddStatus, iResultID);
                    //mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry = null;


                    //---------------------------- -End(Leave entry insertion)-----------------------------------
                    if (MblnISattendanceExists == true)
                    {
                        mobjclsBLLLeaveEntry.UpdateAttendance();
                        //updating Full Day Attendance To HAlf day leave
                        MblnISattendanceExists = false;
                    }

                    //------------------------------------------------------------------------------------------------
                }
                else
                {
                    return false;
                }
                UpdateEmployeeLeaveStructure();
            }
            MbChangeStatus = false;
            SetAutoCompleteList();
            EnableDisablButtons();
            return true;

        }

        public void UpdateEmployeeLeaveStructure()
        {
            DateTime dFinYearDate;
            string s1 = LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy");
            DateTime sdrfinyer = mobjclsBLLLeaveEntry.GetFinYearStartDate(MiCompanyID, s1);
            dFinYearDate = sdrfinyer;
            mobjclsBLLLeaveEntry.DeleteEmployeeLeaveSummaryDetails(MiCompanyID, dFinYearDate, MiLeavepolicyID, CboEmployee.SelectedValue.ToInt32(), CboLeaveType.SelectedValue.ToInt32());
        }

        private void ShowErrorMessage(Control ctrl, int ErrorCode, bool ShowErrProvider, bool ShowStatus)
        {
            MstrCommonMessage = mObjNotification.GetErrorMessage(MaMessage, ErrorCode, out MmessageIcon);
            MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);

            if (ShowErrProvider)
                ErrorProviderEmpLeave.SetError(ctrl, MstrCommonMessage.Replace("#", "").Trim());
            ctrl.Focus();
        }

        private bool GetBalanceAndHolidays()
        {
            try
            {
                decimal LeaveAssigned = 0.0M;
                int iHolidaycount = 0;
                lblholidaysValue.Text = "0";
                //this.Cursor = Cursors.WaitCursor;
                if (CboEmployee.SelectedIndex != -1 && CboLeaveType.SelectedIndex != -1 & MiLeavepolicyID != 0 && MiCompanyID != 0)
                {
                    if (CboEmployee.SelectedValue.ToInt32() != 0 & CboLeaveType.SelectedValue.ToInt32() != 0)
                    {
                        string s1 = LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy");
                        string s2 = LeaveDateToDateTimePicker.Value.ToString("dd-MMM-yyyy");
                        DataSet dst = mobjclsBLLLeaveEntry.GetBalanceAndHolidaysdetails(MiCompanyID, CboEmployee.SelectedValue.ToInt32(), CboLeaveType.SelectedValue.ToInt32(), MiLeavepolicyID, s1, s2, MbAddStatus);
                        if (dst.Tables[0].Rows.Count > 0)
                        {
                            LeaveAssigned = Convert.ToDecimal(dst.Tables[0].Rows[0][0]);
                            dEncasheddays = Convert.ToDecimal(dst.Tables[0].Rows[0][1]);
                            iHolidaycount = Convert.ToInt32(dst.Tables[0].Rows[0][2]);
                            lblBalLeaveValue.Text = LeaveAssigned.ToString();
                            //-->Leave Extension replace month 2 year
                            decimal dMonthLeaveTaken = default(decimal);
                            decimal dMonthLeaveAssigned = default(decimal);
                            dMonthLeaveTaken = GetMonthLeaveTaken();
                            dMonthLeaveAssigned = GetMonthLeaveAssigned();

                            lblBalLeaveValue.Text = LeaveAssigned.ToString("0.0");

                            decimal dleave = 0.0M;
                            dleave = lblBalLeaveValue.Text.ToDecimal();
                            if (dleave < 0)
                            {
                                lblBalLeaveValue.Text = "0";
                            }

                            lblholidaysValue.Text = iHolidaycount.ToString();
                            if (iHolidaycount > 0)
                            {
                                lblholidaysValue.Visible = true;
                                lblnoofholidays.Visible = true;
                            }
                            else
                            {
                                lblholidaysValue.Visible = false;
                                lblnoofholidays.Visible = false;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public decimal GetMonthLeaveTaken()
        {

            decimal dMonthLeaveTaken = 0.0M;

            dMonthLeaveTaken = mobjclsBLLLeaveEntry.GetMonthLeaveTaken(CboEmployee.SelectedValue.ToInt32(), LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy"), LeaveDateToDateTimePicker.Value.ToString("dd-MMM-yyyy"), MiCompanyID, Convert.ToInt32(CboLeaveType.SelectedValue));

            return dMonthLeaveTaken;
        }

        private decimal GetMonthLeaveAssigned()
        {

            decimal dbal = 0;
            int iMonthCnt = 0;
            decimal dmonth = 0;
            DateTime FinYearDate;

            string s1 = LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy");
            FinYearDate = mobjclsBLLLeaveEntry.GetFinYearStartDate(MiCompanyID, s1);
            DateTime testdate = FinYearDate;
            iMonthCnt = ((LeaveDateToDateTimePicker.Value.Month - LeaveDateFromDateTimePicker.Value.Month) + (12 * (LeaveDateToDateTimePicker.Value.Year - LeaveDateFromDateTimePicker.Value.Year)) + 1);
            decimal dMonthLeave = 0.0M;

            DataTable dtCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "BalanceLeaves ,MonthLeaves", "PayEmployeeLeaveSummary", "CompanyID =" + MiCompanyID + " AND LeavePolicyID=" + MiLeavepolicyID + " and EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) + " " + " and LeaveTypeID=" + Convert.ToInt32(CboLeaveType.SelectedValue) + " and " + " Finyearstartdate = '" + testdate.ToString("dd/MMM/yyyy") + "'" });

            if (dtCombos.Rows.Count > 0)
            {
                dmonth = dtCombos.Rows[0][1].ToDecimal();
                dbal = dtCombos.Rows[0][0].ToDecimal();
            }

            if (iMonthCnt == 0)
                iMonthCnt = 1;

            if (Convert.ToInt32(CboLeaveType.SelectedValue) == 3)
            {
                dMonthLeave = dbal;
            }
            else
            {
                dMonthLeave = dmonth * iMonthCnt;

            }
            return dMonthLeave;
        }

        private bool DisplayLeaveInformation(int iCurID)
        {
            int iEmpID = 0;

            bool BlnRetValue = false;

            if (MblnSearch)
            {
                int iSearchTotalCount = 0;
                string sSearchText = txtSearch.Text;
                BlnRetValue = mobjclsBLLLeaveEntry.SearchLeave(iCurID, sSearchText, out iSearchTotalCount);
                if (iSearchTotalCount > 0 && iCurID == 1)
                {
                    TotalRecordCnt = iSearchTotalCount;
                    BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(TotalRecordCnt) + "";
                    BindingNavigatorPositionItem.Text = CurrentRecCnt.ToString();
                    setBindingNavigatorbuttons();

                }
            }
            else
            {
                BlnRetValue = mobjclsBLLLeaveEntry.DisplayLeave(iCurID, PiEmpid);
            }


            if (mobjclsBLLLeaveEntry.clsDTOLeaveEntry.blnFromAttendence == true)
            {
                GrpMain.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
            }
            else
            {
                GrpMain.Enabled = true;
                BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
            }

            if (BlnRetValue)
            {
                DataTable datCombos = new DataTable();
                //if (ClsCommonSettings.IsArabicView)
                //    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "E.EmployeeID," +
                //        "ISNULL((isnull(E.EmployeeFullNameArb,'') +'-' + isnull(cast(E.EmployeeNumber as varchar(30)),'')),'') + ' ['+C.ShortName+ ']' AS FirstName," +
                //        "E.EmployeeNumber", "EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", " E.WorkstatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + " ) " +
                //        "UNION SELECT E.EmployeeID, ISNULL(E.EmployeeFullNameArb, '')+ ' - ' + cast(E.EmployeeNumber as varchar) + ' ['+C.ShortName+ ']' AS FirstName," +
                //        "E.EmployeeNumber FROM  EmployeeMaster AS E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID WHERE  E.EmployeeID = " + mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intEmployeeID + " order by FirstName" });
                //else
                    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "E.EmployeeID," +
                        "ISNULL((isnull(E.EmployeeFullName,'') +'-' + isnull(cast(E.EmployeeNumber as varchar(30)),'')),'') + ' ['+C.ShortName+ ']' AS FirstName," +
                        "E.EmployeeNumber", "EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", " E.WorkstatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where CompanyID = "+ ClsCommonSettings.LoginCompanyID +" and UserID=" + ClsCommonSettings.UserID +")  " +
                        "UNION SELECT   E.EmployeeID, ISNULL(E.EmployeeFullName, '')+ ' - ' + cast(E.EmployeeNumber as varchar) + ' ['+C.ShortName+ ']' AS FirstName," +
                        "E.EmployeeNumber FROM  EmployeeMaster AS E INNER JOIN PayEmployeeLeaveDetails CE ON E.EmployeeID=CE.EmployeeID  INNER JOIN CompanyMaster C ON CE.CompanyID=C.CompanyID  WHERE  E.EmployeeID = " + mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intEmployeeID + " order by FirstName" });
                CboEmployee.ValueMember = "EmployeeID";
                CboEmployee.DisplayMember = "FirstName";
                CboEmployee.DataSource = datCombos;
                CboEmployee.Tag = mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intLeaveID;

                if (mobjclsBLLLeaveEntry.clsDTOLeaveEntry.blnFromAttendence == true)
                {
                    GrpMain.Enabled = false;
                    BindingNavigatorDeleteItem.Enabled = false;
                }
                else
                {
                    GrpMain.Enabled = true;
                    BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
                }

                CboEmployee.SelectedValue = mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intEmployeeID;
                CboLeaveType.SelectedValue = mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intLeaveTypeID;
                MiCompanyID = mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intCompanyID;
                TempLeaveType = mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intLeaveTypeID;
                HalfDayCheckBox.Checked = mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.blnHalfDay;
                chkPaidUnPaid.Checked = (mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.blnPaidLeave == false ? true : false);
                ///'''''''''''''''' Employee Dateof Joining Checking - Changes Made By Tijo As On 27-Jul-2011 '''''''''''''''''''''''
                try
                {
                    if (CboEmployee.SelectedIndex != -1)
                    {

                        DataTable dtdrChecking = mobjclsBLLLeaveEntry.FillCombos(new string[] { "convert(datetime,convert(varchar(12),Dateofjoining,106),106) AS Dateofjoining", "EmployeeMaster", " EmployeeID=" + CboEmployee.SelectedValue });

                        if (dtdrChecking != null && dtdrChecking.Rows.Count > 0)
                        {
                            LeaveDateFromDateTimePicker.MinDate = dtdrChecking.Rows[0][0].ToDateTime();
                            LeaveDateToDateTimePicker.MinDate = dtdrChecking.Rows[0][0].ToDateTime();
                            RejoinDateDateTimePicker.MinDate = dtdrChecking.Rows[0][0].ToDateTime();
                        }
                    }

                }
                catch { }
                ///'''''''''''''''''''''''''''''''''' END ''''''''''''''''''''''''''''''''''''''''''
                if (LeaveDateFromDateTimePicker.MaxDate < Convert.ToDateTime(mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.strLeaveDateFrom))
                {
                    LeaveDateFromDateTimePicker.MaxDate = Convert.ToDateTime(mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.strLeaveDateFrom);
                }
                if (LeaveDateToDateTimePicker.MaxDate < Convert.ToDateTime(mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.strLeaveDateTo))
                {
                    LeaveDateToDateTimePicker.MaxDate = Convert.ToDateTime(mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.strLeaveDateTo);
                }
                LeaveDateFromDateTimePicker.Value = Convert.ToDateTime(mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.strLeaveDateFrom);
                LeaveDateToDateTimePicker.Value = Convert.ToDateTime(mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.strLeaveDateTo);
                if (string.IsNullOrEmpty(mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.strRejoinDate))
                {
                    RejoinDateDateTimePicker.Value = LeaveDateToDateTimePicker.Value.AddDays(1);

                }
                else
                {
                    RejoinDateDateTimePicker.Value = Convert.ToDateTime(mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.strRejoinDate);
                }

                RemarksTextBox.Text = mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.strRemarks;
                if (mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intEmployeeID > 0)
                {
                    CboEmployee.SelectedValue = mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intEmployeeID;
                    iEmpID = Convert.ToInt32(mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intEmployeeID);
                }
                SetCurrentLeaves(iEmpID);
                GetBalanceAndHolidays();
                if (MblnSearch & iCurID == 1)
                {
                    SetControlsSearch();
                }
                MbAddStatus = false;
                EnableDisablButtons();
            }

            return BlnRetValue;
        }

        private void SetControlsSearch()
        {
            this.CboEmployee.Enabled = false;
            LblBalLeave.Visible = false;
            lblBalLeaveValue.Visible = false;
            MbChangeStatus = false;
            bSearchFlag = false;
            lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10141, out MmessageIcon);
            TimerLeave.Enabled = true;
            setBindingNavigatorbuttons();
            if ((CboEmployee.SelectedValue.ToInt32() > 0))
            {
                if ((TempLeaveType == 1))
                {
                    CboLeaveType.SelectedValue = TempLeaveType;
                }
                else
                {
                    DataTable dtCombos = new DataTable();
                    //if (ClsCommonSettings.IsArabicView)
                    //    dtCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType", " EmployeeMaster EM INNER JOIN PayLeavePolicyDetail CLD " + "ON EM.LeavePolicyID=CLD.LeavePolicyID INNER JOIN PayLeaveTypeReference LTR ON CLD.LeaveTypeID=LTR.LeaveTypeID " + "WHERE EmployeeID=" + CboEmployee.SelectedValue + "  UNION " + "SELECT ELD.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType FROM PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID ", "EmployeeID=" + CboEmployee.SelectedValue + "  UNION " + "SELECT ELS.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType FROM PayEmployeeLeaveSummary ELS INNER JOIN PayLeaveTypeReference LTR " + "ON ELS.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + " " });
                    //else
                        dtCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveType ", " EmployeeMaster EM INNER JOIN PayLeavePolicyDetail CLD " + "ON EM.LeavePolicyID=CLD.LeavePolicyID INNER JOIN PayLeaveTypeReference LTR ON CLD.LeaveTypeID=LTR.LeaveTypeID " + "WHERE EmployeeID=" + CboEmployee.SelectedValue + "  UNION " + "SELECT ELD.LeaveTypeID,LTR.LeaveType FROM PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID ", "EmployeeID=" + CboEmployee.SelectedValue + "  UNION " + "SELECT ELS.LeaveTypeID,LTR.LeaveType FROM PayEmployeeLeaveSummary ELS INNER JOIN PayLeaveTypeReference LTR " + "ON ELS.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + " " });
                    CboLeaveType.ValueMember = "LeaveTypeID";
                    CboLeaveType.DisplayMember = "FirstName";
                    CboLeaveType.DataSource = dtCombos;
                    CboLeaveType.SelectedValue = TempLeaveType;

                }
            }
            EnableDisablButtons();
            if (Checkvisibility(PiEmpid, false))
            {
                //lblstatus.Text = New ClsNotification().GetErrorMessage(MaStatusMessage, 19, False)
                //imerLeave.Enabled = True
            }
            else
            {
                this.BindingNavigatorDeleteItem.Enabled = false;
            }
        }

        private bool Checkvisibility(int iEmpID, bool IsMessageNeeded)
        {

            int iLeaveID = Convert.ToInt32(CboEmployee.Tag);

            GrpMain.Enabled = true;
            if (RemarksTextBox.Text.Trim() == "Employee Settled during vacation")
            {
                GrpMain.Enabled = false;

                return false;
            }
            else
            {

                DataTable dtCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "IsFromAttendence", " PayEmployeeLeaveDetails ", " LeaveID=" + iLeaveID + " and Employeeid=" + Convert.ToInt32(CboEmployee.SelectedValue) });
                if (dtCombos != null && dtCombos.Rows.Count > 0)
                {
                    if (dtCombos.Rows[0]["IsFromAttendence"].ToBoolean() == true)
                    {
                        GrpMain.Enabled = false;
                        return false;
                    }
                }
            }
            int ityppe = 0;
            DataTable dtCombos1 = mobjclsBLLLeaveEntry.FillCombos(new string[] { "EmploymentTypeID", " EmployeeMaster ", " EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) });
            if (dtCombos1 != null && dtCombos1.Rows.Count > 0)
            {
                ityppe = dtCombos1.Rows[0]["EmploymentTypeID"].ToInt32();
            }

            if (ityppe != 2)
            {
                if (chckPaymentExists())
                {
                    if (IsMessageNeeded)
                        MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10136, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10136, out MmessageIcon);

                    //GrpMain.Enabled = false;
                    return false;
                }
            }

            return true;
        }

        public bool chckPaymentExists()
        {
            bool bReturn = false;

            DataTable datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "PaymentID", "  PayEmployeePayment ", " EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) + " " + " and convert(datetime,'" + LeaveDateFromDateTimePicker.Text + "',106)  between " + " convert(datetime,convert(char(15),PeriodFrom,106),106) and convert(datetime,convert(char(15),PeriodTo,106),106)" });

            if (datCombos.Rows.Count > 0 && datCombos != null)
            {
                bReturn = true;
            }
            else
            {
                bReturn = false;
            }
            return bReturn;
        }

        private void SetCurrentLeaves(int iEmpID)
        {
            try
            {
                lblholidaysValue.Visible = false;
                lblnoofholidays.Visible = false;
                lblholidaysValue.Text = "0";
                iLeavetypeID = Convert.ToInt32(CboLeaveType.SelectedValue);

                DataTable dtsdrLeave = mobjclsBLLLeaveEntry.FillCombos(new string[] { "Convert(varchar(11),DateofJoining,106),LeavePolicyID ", " EmployeeMaster ", " EmployeeID =" + Convert.ToInt32(CboEmployee.SelectedValue) });
                if (dtsdrLeave != null && dtsdrLeave.Rows.Count > 0)
                {

                    LblDOJ.Text = dtsdrLeave.Rows[0][0].ToString();

                    MiLeavepolicyID = dtsdrLeave.Rows[0]["LeavePolicyID"].ToInt32();
                }

                DataTable DtsdrExpense = mobjclsBLLLeaveEntry.FillCombos(new string[] { " CR.CurrencyID,CR.CurrencyName,E.CompanyID", " CurrencyReference CR LEFT OUTER JOIN CompanyMaster C on C.CurrencyId=CR.CurrencyId " + " LEFT OUTER JOIN Employeemaster E on E.CompanyID=C.CompanyID ", " E.EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) });
                if (DtsdrExpense != null && DtsdrExpense.Rows.Count > 0)
                {
                    MiCompanyID = DtsdrExpense.Rows[0][2].ToInt32();

                }
                OldNoOfLeaves = 0;
                if (HalfDayCheckBox.Checked == true)
                {
                    OldNoOfLeaves = 0.5M;
                }
                else if (HalfDayCheckBox.Checked == false)
                {
                    OldNoOfLeaves = (LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value).TotalDays.ToInt32() + 1;

                }
                if (MiCompanyID > 0)
                {
                    string s1 = LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy");
                    FinYearDate = mobjclsBLLLeaveEntry.GetFinYearStartDate(MiCompanyID, s1);

                }
            }
            catch { }
        }

        private void getNoOfHolidays()
        {
            try
            {
                decimal iHolidaycount = 0;
                lblholidaysValue.Text = "0";


                string s1 = LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy");
                string s2 = LeaveDateToDateTimePicker.Value.ToString("dd-MMM-yyyy");

                iHolidaycount = mobjclsBLLLeaveEntry.GetHoildaysWithOffDays(s1, s2, CboEmployee.SelectedValue.ToInt32()).ToDecimal();

                lblholidaysValue.Text = iHolidaycount.ToString(); ;
                if (iHolidaycount > 0)
                {
                    lblholidaysValue.Visible = true;
                    lblnoofholidays.Visible = true;
                }
                else
                {
                    lblholidaysValue.Visible = false;
                    lblnoofholidays.Visible = false;
                }
            }
            catch { }
        }

        private bool CheckAttendanceexits()
        {
            try
            {

                int iDays = 0;
                DateTime Leavedate = LeaveDateFromDateTimePicker.Value.Date;
                if (HalfDayCheckBox.Checked == true)
                {
                    LeaveDateToDateTimePicker.Value = LeaveDateFromDateTimePicker.Value;
                }
                iDays = (LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value).TotalDays.ToInt32() + 1;
                if (iDays > 0)
                {
                    for (int i = 0; i <= iDays - 1; i++)
                    {
                        if (chckAttendanceExists())
                        {
                            return false;
                        }
                        Leavedate = Leavedate.AddDays(1);
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
        private bool CheckAnyAttendanceexits()
        {
            try
            {

                int iDays = 0;
                DateTime Leavedate = LeaveDateFromDateTimePicker.Value.Date;
                if (HalfDayCheckBox.Checked == true)
                {
                    LeaveDateToDateTimePicker.Value = LeaveDateFromDateTimePicker.Value;
                }
                iDays = (LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value).TotalDays.ToInt32() + 1;
                if (iDays > 0)
                {
                    for (int i = 0; i <= iDays - 1; i++)
                    {
                        if (chckAnyAttendanceExists())
                        {
                            return false;
                        }
                        Leavedate = Leavedate.AddDays(1);
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool chckAttendanceExists()
        {
            bool bReturn = false;

            DataTable datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { " 1 ", "PayAttendanceMaster", " EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) + " and ( AttendenceStatusID = 1 OR AttendenceStatusID = 3 )and convert(datetime,convert(char(15),Date,106),106) between convert(datetime,'" + LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106) and  convert(datetime,'" + LeaveDateToDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106)" });

            if (datCombos.Rows.Count > 0 && datCombos != null)
            {
                bReturn = true;
            }
            else
            {
                bReturn = false;
            }
            return bReturn;
        }
        public bool chckAnyAttendanceExists()
        {
            bool bReturn = false;

            DataTable datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { " 1 ", "PayAttendanceMaster", " EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) + " and  convert(datetime,convert(char(15),Date,106),106) between convert(datetime,'" + LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106) and  convert(datetime,'" + LeaveDateToDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106)" });

            if (datCombos.Rows.Count > 0 && datCombos != null)
            {
                bReturn = true;
            }
            else
            {
                bReturn = false;
            }
            return bReturn;
        }
        public bool chckAttendanceManualExists()
        {

            // select 1 from AttendanceManual where EmployeeID=" & Convert.ToInt32(CboEmployee.SelectedValue) & " and  AttendenceStatusID=1 and convert(datetime,convert(char(15),Date,106),106) between convert(datetime,'" & LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy") & "',106) and  convert(datetime,'" & LeaveDateToDateTimePicker.Value.ToString("dd-MMM-yyyy") & "',106)")
            bool bReturn = false;
            DataTable datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "1", "PayAttendanceManual", "EmployeeID = " + Convert.ToInt32(CboEmployee.SelectedValue) + " and  AttendenceStatusID=1 and convert(datetime,convert(char(15),Date,106),106) between convert(datetime,'" + LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106) and  convert(datetime,'" + LeaveDateToDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106)" });

            if (datCombos.Rows.Count > 0 && datCombos != null)
            {
                bReturn = true;
            }
            else
            {
                bReturn = false;
            }
            return bReturn;
        }

        //private bool Checvacationexits()
        //{
        //    try
        //    {
        //        ClsConnection ObjCon2 = new ClsConnection();
        //        int iDays = 0;
        //        DateTime Leavedate = LeaveDateFromDateTimePicker.Value.Date;
        //        if (HalfDayCheckBox.Checked == true)
        //        {
        //            LeaveDateToDateTimePicker.Value = LeaveDateFromDateTimePicker.Value;
        //        }
        //        iDays = (LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value).TotalDays.ToInt32()+ 1;
        //        if (iDays > 0)
        //        {
        //            if (ObjCon2.Chk_Duplicate("select SettlementID from EmployeeSettlement where EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) + " and SettlementType=6 and " + "  convert(datetime,'" + LeaveDateFromDateTimePicker.Text + "',106)  between " + " convert(datetime,convert(char(15),Fromdate,106),106) and convert(datetime,convert(char(15),Todate,106),106)"))
        //            {
        //                return false;
        //            }
        //        }

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        private int GetDay(string day)
        {
            switch (day)
            {
                case "Saturday":
                    return 6;
                case "Sunday":
                    return 0;
                case "Monday":
                    return 1;
                case "Tuesday":
                    return 2;
                case "Wednesday":
                    return 3;
                case "Thursday":
                    return 4;
                case "Friday":
                    return 5;

                default: return 7;
            }

        }

        private bool chkdupicate()
        {
            bool blnRetValue = false;
            try
            {
                int TempIdentityID = 0;
                TempIdentityID = Convert.ToInt32(CboEmployee.Tag);
                if (MbAddStatus == true)
                {
                    if (mobjclsBLLLeaveEntry.FillCombos(new string[] { "LeaveID", "PayEmployeeLeaveDetails", "EmployeeID=" + CboEmployee.SelectedValue + "  and (convert(datetime,'" + LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106)  between LeaveDateFrom  and LeaveDateTo  or convert(datetime,'" + LeaveDateToDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106) between LeaveDateFrom  and LeaveDateTo)" }).Rows.Count > 0)
                    {

                        blnRetValue = true;
                    }
                    else
                    {
                        blnRetValue = false;
                    }
                }
                else
                {
                    if (mobjclsBLLLeaveEntry.FillCombos(new string[] { "LeaveID", "PayEmployeeLeaveDetails", "LeaveID <> " + TempIdentityID + " and  EmployeeID=" + CboEmployee.SelectedValue + "  and (convert(datetime,'" + LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106)  between LeaveDateFrom  and LeaveDateTo  or convert(datetime,'" + LeaveDateToDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106) between LeaveDateFrom  and LeaveDateTo)" }).Rows.Count > 0)
                    {
                        blnRetValue = true;
                    }
                    else
                    {
                        blnRetValue = false;
                    }

                    if (blnRetValue == false)
                    {
                        if (mobjclsBLLLeaveEntry.FillCombos(new string[] { "LeaveID", "PayEmployeeLeaveDetails", "LeaveID <> " + TempIdentityID + " and  EmployeeID =" + CboEmployee.SelectedValue + "  and " + "  (LeaveDateFrom   between convert(datetime,'" + LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106)  and  convert(datetime,'" + LeaveDateToDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106) or " + " LeaveDateTo between  convert(datetime,'" + LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106)  and  convert(datetime,'" + LeaveDateToDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106))" }).Rows.Count > 0)
                        {
                            blnRetValue = true;
                        }
                        else
                        {
                            blnRetValue = false;
                        }

                    }
                }
                return blnRetValue;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on fnchkUniquevalue:Employee information." + ex.Message.ToString());
                mObjLogs.WriteLog("Error on fnchkUniquevalue:Employee information." + ex.Message.ToString() + "", 3);
                return false;
            }

        }

        private void CheckLeavestructure()
        {
            try
            {
                if ((CboEmployee.Text != "") && (CboLeaveType.Text != "") && MiLeavepolicyID != 0 && MiCompanyID != 0)
                {
                    if (CboEmployee.SelectedValue.ToInt32() != 0 && CboLeaveType.SelectedValue.ToInt32() != 0)
                    {
                        string s1 = LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy");
                        mobjclsBLLLeaveEntry.CheckLeavestructures(MiCompanyID, CboEmployee.SelectedValue.ToInt32(), CboLeaveType.SelectedValue.ToInt32(), MiLeavepolicyID, s1);
                    }
                }
            }
            catch { }
        }

        public bool ChkFinYearFreeze1(DateTime chkYear)
        {

            DateTime dtEndDate = ClsCommonSettings.GetServerDate();
            DateTime CurDate = LeaveDateFromDateTimePicker.Value.Date;
            dtEndDate = chkYear.AddDays(1);

            if ((CurDate >= chkYear & CurDate <= dtEndDate))
            {
                return true;
            }
            return false;
        }

        private bool DeleteLeaveEntry()
        {
            try
            {
                ErrorProviderEmpLeave.Clear();

                if (Convert.ToInt32(CboEmployee.Tag) == 0)
                {
                    MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10141, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK);
                    return false;
                }
                if (MbAddStatus == true)
                {
                    MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10141, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK);
                    lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10141, out MmessageIcon);
                    TimerLeave.Enabled = true;
                }
                else
                {

                    if (CheckSalaryRelease() == false)
                    {
                        MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10139, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK);
                        return false;
                    }
                    if (CheckAnyAttendanceexits() == false)
                    {
                        MsMessageBody = new ClsNotification().GetErrorMessage(MaMessage, 10150, out MmessageIcon).Replace("#", "");
                    }
                    else
                    {
                        MsMessageBody = new ClsNotification().GetErrorMessage(MaMessage, 4213, out MmessageIcon).Replace("#", "");

                    }
                    if (MessageBox.Show(MsMessageBody, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        this.Validate();
                        mobjclsBLLLeaveEntry.DeleteLeave(Convert.ToInt32(CboEmployee.Tag));

                    }
                    else
                    {
                        return false;
                    }
                }

                MbChangeStatus = false;
                EnableDisablButtons();
                return true;
            }
            catch (System.Data.SqlClient.SqlException Ex)
            {
                string MstrMessageCommon = "";
                if (Ex.Number == (int)SqlErrorCodes.ForeignKeyErrorNumber)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    MstrMessageCommon = "التفاصيل موجودة في النظام. لا يمكن حذف";
                    //else
                        MstrMessageCommon = "Details exists in the system.Cannot delete";
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }
                else
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    MstrMessageCommon = "التفاصيل موجودة في النظام. لا يمكن حذف";
                    //else
                        MstrMessageCommon = "Details exists in the system.Cannot delete";
                    //MstrMessageCommon = "Error on DeleteLeaveEntry:LeaveEntry" + Ex.Message.ToString();
                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    mObjLogs.WriteLog("Error on DeleteLeaveEntry:LeaveEntry." + Ex.Message.ToString() + "", 3);
                }
                return false;
            }
            catch (Exception ex)
            {

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteLeaveEntry:LeaveEntry." + ex.Message.ToString());
                mObjLogs.WriteLog("Error on DeleteLeaveEntry:LeaveEntry." + ex.Message.ToString() + "", 3);
                return false;
            }
        }

        private bool CheckSalaryRelease()
        {
            //DataTable dtCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "PaymentID ", "PayEmployeePayment", " CompanyID=(select CompanyID from EmployeeMaster where EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) + ") AND EmployeeID = " + Convert.ToInt32(CboEmployee.SelectedValue) + " and Released=1 " + " and  month(Periodfrom)= " + LeaveDateFromDateTimePicker.Value.Month + " and year(Periodfrom)=" + LeaveDateFromDateTimePicker.Value.Year });
            DataTable dtCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "PaymentID ", "PayEmployeePayment", " CompanyID=(select CompanyID from EmployeeMaster where EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) + ") AND EmployeeID = " + Convert.ToInt32(CboEmployee.SelectedValue) + " and Released=1 " + " AND ( (CONVERT(DATETIME,CONVERT(VARCHAR,PeriodFrom,106)) BETWEEN CONVERT(DATETIME,'"+LeaveDateFromDateTimePicker.Value.Date.ToString("dd MMM yyyy")+"') AND CONVERT(DATETIME,'"+LeaveDateToDateTimePicker.Value.Date.ToString("dd MMM yyyy")+"')) OR (CONVERT(DATETIME,CONVERT(VARCHAR,PeriodTo,106)) BETWEEN CONVERT(DATETIME,'"+LeaveDateFromDateTimePicker.Value.Date.ToString("dd MMM yyyy")+"') AND CONVERT(DATETIME,'"+LeaveDateToDateTimePicker.Value.Date.ToString("dd MMM yyyy")+"')))" });
            if (dtCombos != null && dtCombos.Rows.Count > 0)
            {
                if (dtCombos.Rows[0]["PaymentID"] != DBNull.Value)
                {
                    return false;
                }

            }
            return true;
        }

        private bool Formvalidation()
        {
            RemarksTextBox.Focus();
            decimal dHolidays = 0;

            ErrorProviderEmpLeave.Clear();
            if (CboEmployee.SelectedIndex == -1)
            {
                ErrorProviderEmpLeave.SetError(CboEmployee, new ClsNotification().GetErrorMessage(MaMessage, 10132, out MmessageIcon));
                MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10132, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10132, out MmessageIcon);
                TimerLeave.Enabled = true;
                CboEmployee.Focus();
                return false;
            }
            if ((CboEmployee.SelectedValue.ToInt32() == 0))
            {
                ErrorProviderEmpLeave.SetError(CboEmployee, new ClsNotification().GetErrorMessage(MaMessage, 10133, out MmessageIcon));
                MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10133, out MmessageIcon).Replace("#", "").Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10133, out MmessageIcon);
                TimerLeave.Enabled = true;
                CboEmployee.Focus();
                return false;
            }
            if (CboLeaveType.SelectedIndex == -1)
            {
                ErrorProviderEmpLeave.SetError(CboLeaveType, new ClsNotification().GetErrorMessage(MaMessage, 10134, out MmessageIcon));
                MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10134, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10134, out MmessageIcon);
                TimerLeave.Enabled = true;
                CboLeaveType.Focus();
                return false;
            }
            if ((CboLeaveType.SelectedValue.ToInt32() == 0))
            {
                ErrorProviderEmpLeave.SetError(CboLeaveType, new ClsNotification().GetErrorMessage(MaMessage, 10135, out MmessageIcon));
                MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10135, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10135, out MmessageIcon);
                TimerLeave.Enabled = true;
                CboLeaveType.Focus();
                return false;
            }

            if (LeaveDateFromDateTimePicker.Value.ToDateTime() > ClsCommonSettings.GetServerDate() || LeaveDateToDateTimePicker.Value.ToDateTime() > ClsCommonSettings.GetServerDate())
            {
                ErrorProviderEmpLeave.SetError(LeaveDateToDateTimePicker, new ClsNotification().GetErrorMessage(MaMessage, 10135, out MmessageIcon));
                MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10143, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10143, out MmessageIcon);
                TimerLeave.Enabled = true;
                LeaveDateToDateTimePicker.Focus();
                return false;
            }
            //if (chkVacationTakenOnTheDate() == true)
            //{
            //    ErrorProviderEmpLeave.SetError(LeaveDateFromDateTimePicker, new ClsNotification().GetErrorMessage(MaMessage, 1254, out MmessageIcon));
            //    MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 1254, out MmessageIcon), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    lblstatus.Text = new ClsNotification().GetErrorMessage(MaMessage, 1254, out MmessageIcon);
            //    TimerLeave.Enabled = true;
            //    LeaveDateFromDateTimePicker.Focus();
            //    return false;
            //}

            if (MbAddStatus == true)
            {
                if ((CboEmployee.SelectedValue.ToInt32() != 0 & CboLeaveType.SelectedValue.ToInt32() != 0))
                {
                    if (lblBalLeaveValue.Text.ToDecimal() <= 0.0M)
                    {
                        //ErrorProviderEmpLeave.SetError(lblBalLeaveValue, New ClsNotification().GetErrorMessage(MaMessage, 1201, True))
                        MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10100, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10100, out MmessageIcon);
                        TimerLeave.Enabled = true;
                        //lblBalLeaveValue.Focus()
                        return false;
                    }
                }
            }
            // modified on 29 July 2010
            if (HalfDayCheckBox.Checked == true)
            {
                LeaveDateToDateTimePicker.Value = LeaveDateFromDateTimePicker.Value;
            }
            if (MbAddStatus == true)
            {
                decimal LeaveTaken = 0;
                if (HalfDayCheckBox.Checked == false)
                {
                    LeaveTaken = (LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value).TotalDays.ToInt32() + 1;
                }
                else if (HalfDayCheckBox.Checked == true)
                {
                    LeaveTaken = 0.5M;
                }
                if ((lblBalLeaveValue.Text.ToDecimal()) == 0)
                {
                    if (LeaveTaken > (lblBalLeaveValue.Text.ToDecimal()) || (lblBalLeaveValue.Text.ToDecimal()) == 0)
                    {
                        ErrorProviderEmpLeave.SetError(LeaveDateFromDateTimePicker, new ClsNotification().GetErrorMessage(MaMessage, 10100, out MmessageIcon));
                        MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10100, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10100, out MmessageIcon);
                        TimerLeave.Enabled = true;
                        LeaveDateFromDateTimePicker.Focus();
                        return false;
                    }
                }
            }

            int ityppe = 0;
            DataTable dtCombos1 = mobjclsBLLLeaveEntry.FillCombos(new string[] { "EmploymentTypeID", " EmployeeMaster ", " EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) });
            if (dtCombos1 != null && dtCombos1.Rows.Count > 0)
            {
                ityppe = dtCombos1.Rows[0]["EmploymentTypeID"].ToInt32();
            }

            else if (ityppe != 2)
            {
                if (mobjclsBLLLeaveEntry.FillCombos(new string[] { "PaymentID", "  PayEmployeePayment ", " EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) + " " + " and convert(datetime,'" + LeaveDateFromDateTimePicker.Text + "',106)  between " + " convert(datetime,convert(char(15),PeriodFrom,106),106) and convert(datetime,convert(char(15),PeriodTo,106),106)" }).Rows.Count > 0)
                {
                    MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10117, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
            if ((LeaveDateFromDateTimePicker.Value) == null)
            {
                ErrorProviderEmpLeave.SetError(LeaveDateFromDateTimePicker, new ClsNotification().GetErrorMessage(MaMessage, 10101, out MmessageIcon));
                MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10101, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10101, out MmessageIcon);
                TimerLeave.Enabled = true;
                LeaveDateFromDateTimePicker.Focus();
                return false;
            }
            if (HalfDayCheckBox.Checked == false)
            {
                if ((LeaveDateToDateTimePicker.Value) == null)
                {
                    ErrorProviderEmpLeave.SetError(LeaveDateToDateTimePicker, new ClsNotification().GetErrorMessage(MaMessage, 10102, out MmessageIcon));
                    MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10102, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10102, out MmessageIcon);
                    TimerLeave.Enabled = true;
                    LeaveDateToDateTimePicker.Focus();
                    return false;
                }
            }
            if (HalfDayCheckBox.Checked == false)
            {
                if ((LeaveDateToDateTimePicker.Value.Date) < (LeaveDateFromDateTimePicker.Value.Date))
                {
                    ErrorProviderEmpLeave.SetError(LeaveDateToDateTimePicker, new ClsNotification().GetErrorMessage(MaMessage, 10103, out MmessageIcon));
                    MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10103, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10103, out MmessageIcon);
                    TimerLeave.Enabled = true;
                    LeaveDateToDateTimePicker.Focus();
                    return false;
                }
            }

            if (HalfDayCheckBox.Checked == false)
            {
                if (RejoinDateDateTimePicker.Value != null)
                {
                    if ((LeaveDateToDateTimePicker.Value.Date) >= (RejoinDateDateTimePicker.Value.Date))
                    {
                        ErrorProviderEmpLeave.SetError(RejoinDateDateTimePicker, new ClsNotification().GetErrorMessage(MaMessage, 10104, out MmessageIcon));
                        MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10104, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10104, out MmessageIcon);
                        TimerLeave.Enabled = true;
                        RejoinDateDateTimePicker.Focus();
                        return false;
                    }
                }
            }

            try
            {
                if (Convert.ToInt32(CboEmployee.SelectedValue.ToInt32()) != 0)
                {
                    if (GetEmployeeList(CboEmployee.SelectedValue.ToInt32()) == false)
                    {
                        MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10137, out MmessageIcon), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10137, out MmessageIcon).Replace("#", "");
                        TimerLeave.Enabled = true;
                        CboEmployee.Focus();
                        return false;
                    }
                }
                decimal dextendedmonthleave = GetMonthExtensionleave(MiCompanyID, Convert.ToInt32(CboEmployee.SelectedValue), Convert.ToInt32(CboLeaveType.SelectedValue), LeaveDateFromDateTimePicker.Value.Month, LeaveDateFromDateTimePicker.Value.Year);
                decimal dMonthLeaveAssigned = GetMonthLeaveAssigned();
                //+ dextendedmonthleave
                decimal dMonthLeaveTaken = GetMonthLeaveTaken();

                ///'''''''''''''''''''''''' For Balance Leave ''''''''''''''''''''''''''

                string s1 = LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy");
                FinYearDate = mobjclsBLLLeaveEntry.GetFinYearStartDate(MiCompanyID, s1);
                string testdate = FinYearDate.ToString("dd/MMM/yyyy");

                DataTable dtTemp = mobjclsBLLLeaveEntry.FillCombos(new string[] { "(BalanceLeaves + CarryForwardDays + Exceeded) - TakenLeaves AS Balance", "PayEmployeeLeaveSummary", " CompanyID = " + MiCompanyID + " and LeavePolicyID=" + MiLeavepolicyID + " and EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) + " and LeavetypeID=" + Convert.ToInt32(CboLeaveType.SelectedValue) + " and Finyearstartdate = '" + testdate + "'" });

                if ((CboEmployee.Tag.ToInt32() > 0))
                {
                    DataTable dtTemp2 = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LeaveDateFrom,LeaveDateTo", "PayEmployeeLeaveDetails", "LeaveID=" + CboEmployee.Tag });
                }
                ///'''''''''''''''''''''''''''''' End ''''''''''''''''''''''''''''''''''

                //------------------
                if (MbAddStatus == false)
                {
                    decimal dOldNoOfDay = 0;
                    decimal dTotal = 0;
                    decimal ddays = 0;
                    dOldNoOfDay = dMonthLeaveTaken - OldNoOfLeaves;

                    if (HalfDayCheckBox.Checked == true)
                    {
                        ddays = 0.5M;
                    }
                    else
                    {
                        ddays = (LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value).TotalDays.ToInt32() + 1;
                    }

                    if (lblholidaysValue.Visible)
                        dHolidays = lblholidaysValue.Text.ToInt32();

                    dTotal = dOldNoOfDay + ddays - dHolidays;
                    DataTable dtTemp1 = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LeaveDateFrom,LeaveDateTo", "PayEmployeeLeaveDetails", "LeaveID=" + CboEmployee.Tag });

                    if (((LeaveDateFromDateTimePicker.Text != Convert.ToDateTime(dtTemp1.Rows[0]["LeaveDateFrom"].ToString()).ToString("dd-MMM-yyyy")) || (LeaveDateToDateTimePicker.Text != Convert.ToDateTime(dtTemp1.Rows[0]["LeaveDateTo"].ToString()).ToString("dd-MMM-yyyy"))))
                    {
                        if (dMonthLeaveAssigned > 0)
                        {
                            if ((dTotal > dMonthLeaveAssigned || Convert.ToDecimal(dtTemp.Rows[0]["Balance"].ToString()) == 0))
                            {
                                if (((Convert.ToDateTime(dtTemp1.Rows[0]["LeaveDateTo"].ToString()) - Convert.ToDateTime(dtTemp1.Rows[0]["LeaveDateFrom"].ToString())) < (LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value)))
                                {
                                    MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10113, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10113, out MmessageIcon);
                                    TimerLeave.Enabled = true;
                                    LeaveDateFromDateTimePicker.Focus();
                                    return false;
                                }
                            }
                        }
                    }


                }

                if (chkdupicate() == true)
                {
                    ErrorProviderEmpLeave.SetError(LeaveDateFromDateTimePicker, new ClsNotification().GetErrorMessage(MaMessage, 10114, out MmessageIcon));
                    MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10114, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10114, out MmessageIcon);
                    TimerLeave.Enabled = true;
                    LeaveDateFromDateTimePicker.Focus();
                    return false;
                }

                //modified on 14 June 2010
                ///''''''''''''''''''''''''''''
                MblnISattendanceExists = false;
                if (CheckAttendanceexits() == false)
                {
                    if ((HalfDayCheckBox.Checked == false))
                    {
                        MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10116, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    MblnISattendanceExists = true;
                }

                //if (Checvacationexits() == false)
                //{
                //    MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10123, out MmessageIcon), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    return false;
                //}


                DataTable dtCombos3 = mobjclsBLLLeaveEntry.FillCombos(new string[] { "convert(datetime,convert(varchar(12),Dateofjoining,106),106) AS DateofJoining", "EmployeeMaster", " EmployeeID =" + CboEmployee.SelectedValue });
                if (dtCombos3.Rows.Count > 0 && dtCombos3 != null)
                {
                    if ((LeaveDateFromDateTimePicker.Value) < (dtCombos3.Rows[0][0]).ToDateTime())
                    {
                        ErrorProviderEmpLeave.SetError(LeaveDateFromDateTimePicker, new ClsNotification().GetErrorMessage(MaMessage, 10106, out MmessageIcon));
                        MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10106, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10106, out MmessageIcon);
                        TimerLeave.Enabled = true;
                        LeaveDateFromDateTimePicker.Focus();

                        return false;
                    }
                }
                int iLeavepolicyid = 0;
                DataTable dtCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "WorkPolicyID", "EmployeeMaster", "EmployeeID =" + Convert.ToInt32(CboEmployee.SelectedValue) });
                if (dtCombos.Rows.Count > 0 && dtCombos != null)
                {
                    iLeavepolicyid = dtCombos.Rows[0]["WorkPolicyID"].ToInt32();
                }

                if (iLeavepolicyid == 0)
                {
                    MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10120, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10120, out MmessageIcon);
                    TimerLeave.Enabled = true;
                    return false;
                }

                //-------------------------------------
                bIncludeHolidays = true;



                if (lblholidaysValue.Text.ToInt32() > 0 && CboLeaveType.SelectedValue.ToInt32() != 3)
                {
                    if (MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10138, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        bIncludeHolidays = false;
                        if (lblholidaysValue.Text != "")
                        {
                            dHolidays = lblholidaysValue.Text.ToDecimal();
                            if (dHolidays > 0 && (HalfDayCheckBox.Checked || ((LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value).TotalDays.ToInt32() + 1) == dHolidays))
                            {
                                return false; // returns if all leave days are to be considered as holidays
                            }
                        }
                    }
                }

                if (MbAddStatus == true)
                {
                    if (HalfDayCheckBox.Checked == false)
                    {
                        if (LeaveDateFromDateTimePicker.Value != null)
                        {
                            //(LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value).TotalDays.ToInt32();
                            if ((dMonthLeaveTaken + ((LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value).TotalDays.ToInt32() + 1 - dHolidays)) > dMonthLeaveAssigned)
                            {
                                MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10108, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10108, out MmessageIcon);
                                TimerLeave.Enabled = true;
                                CboEmployee.Focus();
                                return false;
                            }
                        }
                    }
                }
                if (MbAddStatus == true)
                {
                    if (HalfDayCheckBox.Checked == true)
                    {
                        if (LeaveDateFromDateTimePicker.Value != null)
                        {
                            if (HalfDayCheckBox.Checked == true)
                            {
                                LeaveDateToDateTimePicker.Value = LeaveDateFromDateTimePicker.Value;
                            }
                            if (dMonthLeaveTaken + ((LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value).TotalDays.ToDecimal() + 0.5M) > dMonthLeaveAssigned)
                            {
                                MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10108, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10108, out MmessageIcon);
                                TimerLeave.Enabled = true;
                                CboEmployee.Focus();
                                return false;
                            }
                        }
                    }
                }
                decimal iNoOfDays = 0;
                if (HalfDayCheckBox.Checked == false)
                {
                    iNoOfDays = (LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value).TotalDays.ToInt32() + 1;
                }
                else
                {
                    iNoOfDays = 0.5M;
                }
                //PAY7923
                if (iNoOfDays > 0)
                {
                    DataSet dt = objclsConnection.FillDataSet("select DayID  from DayReference where DayID " + " not in (select pd.DayID from PayWorkPolicyMaster pm left outer join PayWorkPolicyDetail pd " + " on pm.WorkPolicyID = pd.WorkPolicyID left outer join EmployeeMaster e on " + " e.WorkPolicyID = pm.WorkPolicyID where e.EmployeeID= " + Convert.ToInt32(CboEmployee.SelectedValue) + " ) ");

                    int i = 0;
                    DateTime LeaveFromdate = LeaveDateToDateTimePicker.Value.Date;
                    if (dt.Tables[0].Rows.Count > 0)
                    {
                        for (i = 0; i <= dt.Tables[0].Rows.Count - 1; i++)
                        {
                            int DayID = 0;
                            int CurDayID = 0;
                            DayID = Convert.ToInt32(dt.Tables[0].Rows[i]["DayID"]);
                            int Curday = LeaveFromdate.DayOfWeek.ToInt32();
                            CurDayID = Curday + 2;
                            if (CurDayID > 7)
                            {
                                CurDayID = 1;
                            }


                        }
                    }
                    dt = null;
                }

                // Check with month leave
                if (HalfDayCheckBox.Checked == false)
                {
                    if (dMonthLeaveAssigned > 0)
                    {
                        if ((((LeaveDateToDateTimePicker.Value - LeaveDateFromDateTimePicker.Value).TotalDays.ToInt32() + 1) - dHolidays) > dMonthLeaveAssigned)
                        {
                            ErrorProviderEmpLeave.SetError(LeaveDateFromDateTimePicker, new ClsNotification().GetErrorMessage(MaMessage, 10109, out MmessageIcon));
                            MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10109, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10109, out MmessageIcon);
                            TimerLeave.Enabled = true;
                            LeaveDateFromDateTimePicker.Focus();
                            return false;
                        }
                    }
                }
                if (MbAddStatus == true)
                {
                    DataTable dtCombos2 = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LeaveID", "PayEmployeeLeaveDetails", "EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) + "  and (CONVERT(DATETIME,convert(varchar,LeaveDateFrom,106),106) bETWEEN CONVERT(DATETIME,'" + LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106)  AND CONVERT(DATETIME,'" + LeaveDateToDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106) or CONVERT(DATETIME,convert(varchar,LeaveDateTo,106),106) bETWEEN CONVERT(DATETIME,'" + LeaveDateFromDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106)  AND CONVERT(DATETIME,'" + LeaveDateToDateTimePicker.Value.ToString("dd-MMM-yyyy") + "',106))" });
                    if (dtCombos2.Rows.Count > 0 && dtCombos2 != null)
                    {
                        MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10105, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10105, out MmessageIcon);
                        TimerLeave.Enabled = true;
                        return false;
                    }
                    else
                    {

                        return true;
                    }
                }
                //---------------------
                return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Formvalidation:LeaveEntry." + ex.Message);
                mObjLogs.WriteLog("Error on Formvalidation:LeaveEntry." + ex.Message, 2);
                return false;
            }
        }

        private decimal GetMonthExtensionleave(int ComiD, int EmpID, int LeavetypeID, int iMonth, int iYear)
        {
            try
            {
                decimal dMonthextendedleaves = 0;

                DataTable dtCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "isnull(NoOfDays,0) as NoOfDays", "PayLeaveExtenstion", " LeaveExtensionTypeID=1 and " + " CompanyID=" + ComiD + " and EmployeeID=" + EmpID + " and LeavetypeID=" + LeavetypeID + " and Month(MonthYear)=" + iMonth + " and Year(MonthYear)=" + iYear });
                if (dtCombos != null && dtCombos.Rows.Count > 0)
                {
                    dMonthextendedleaves = dtCombos.Rows[0]["NoOfDays"].ToDecimal();
                }
                return dMonthextendedleaves;
            }
            catch
            {
                return 0;
            }
        }

        public bool GetEmployeeList(int EmpID)
        {
            DataTable datcombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { " WorkStatusID ", "EmployeeMaster", "EmployeeID = " + EmpID });
            if (datcombos.Rows[0]["WorkStatusID"].ToInt32() > 5)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsUtilities.GetAutoCompleteList(txtSearch.Text.Trim(), 0, "", "PayEmployeeLeaveDetails");
            this.txtSearch.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt);
        }

        #endregion
        #region EVENTS
        void FrmLeaveEntry_Load(object Sender, EventArgs e)
        {
            MbAddStatus = true;
            //LoadInitial();
            LoadMesage();
            SetPermissions();                 //Permission setting
            ErrorProviderEmpLeave.Clear();
            bSearchFlag = true;
            LoadCombos(2);
            MbChangeStatus = false;
            TmFocus.Enabled = true;
            LeaveDateFromDateTimePicker.MaxDate = Convert.ToDateTime(ClsCommonSettings.GetServerDate()).AddYears(1);
            LeaveDateToDateTimePicker.MaxDate = Convert.ToDateTime(ClsCommonSettings.GetServerDate()).AddYears(1).AddDays(1);
            bSearchFlag = false;
            MblnSearch = false;
            EnableDisablButtons();
            TimerLeave.Interval = ClsCommonSettings.TimerInterval;
            if (PiEmpid > 0)
            {
                RecCountNavigate(PiEmpid);
                if (TotalRecordCnt > 0)
                {
                    BindingNavigatorMoveFirstItem_Click(Sender, e);
                }
                else
                {
                    BindingNavigatorAddNewItem_Click(Sender, e);
                }
                txtSearch.Enabled = false;
                btnSearch.Enabled = false;
            }
            else
            {
                if (MbAddPermission == true)
                {
                    BindingNavigatorAddNewItem_Click(Sender, e);
                }
                else
                    BindingNavigatorMoveLastItem_Click(Sender, e);
            }

        }

        private void BindingEnableDisable(object sender, EventArgs e)
        {
            GetRecordCount(PiEmpid);
            if (TotalRecordCnt == 0)
            {
                //--------------permission-----------------
                if (MbAddPermission == true)
                    BindingNavigatorAddNewItem_Click(sender, e);
                //------------------------------------
                //Me.BindingNavigatorPositionItem.Enabled = False
                this.BindingNavigatorMovePreviousItem.Enabled = false;
                this.BindingNavigatorMoveFirstItem.Enabled = false;
                this.BindingNavigatorMoveNextItem.Enabled = false;
                this.BindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                //'--------------permission-----------------
                if (MbAddPermission == true)
                    BindingNavigatorAddNewItem_Click(sender, e);
                //-----------------------------------------------------------
                //Me.BindingNavigatorPositionItem.Enabled = True
                this.BindingNavigatorMovePreviousItem.Enabled = true;
                this.BindingNavigatorMoveFirstItem.Enabled = true;
                this.BindingNavigatorMoveNextItem.Enabled = true;
                this.BindingNavigatorMoveLastItem.Enabled = true;
            }
        }

        private void EmployeeLeaveDetailsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (MbChangeStatus == true)
            //--------------permission------------------------------------
            {
                if (MbAddStatus == false && MbUpdatePermission == false)
                {
                    return;
                }
                //--------------permission------------------------------------
                if (Formvalidation() == true)
                {
                    if (checkWithBalLeave() == true)
                    {


                        if (SaveEmpLeave() == true)
                        {
                            lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, (MbAddStatus ? 2 : 21), out MmessageIcon);
                            if (MbAddStatus == true)
                            {
                                if (MbAddPermission == true)
                                {
                                    MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 2, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    BindingNavigatorMoveLastItem_Click(sender, e);
                                    lblstatus.Text = "";

                                }
                            }
                            else
                            {
                                //CurrentRecCnt = CurrentRecCnt + 1
                                //BindingNavigatorMovePreviousItem_Click(sender, e)
                                MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 21, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                MbAddStatus = false;
                            }

                            TimerLeave.Enabled = true;

                        }

                    }
                }
            }

        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (MbChangeStatus == true)
            {
                //--------------permission------------------------------------'
                if (MbAddStatus == false & MbUpdatePermission == false)
                {
                    MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10140, out MmessageIcon).Replace("#", ""));
                    return;
                }
                //----------------------------------------------------------
                if (Formvalidation() == true)
                {
                    if (checkWithBalLeave() == true)
                    {

                        if (SaveEmpLeave() == true)
                        {
                            this.Close();
                        }

                    }
                }
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (MbChangeStatus == true)
            {
                //--------------permission------------------------------------'
                if (MbAddStatus == false & MbUpdatePermission == false)
                {
                    MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10140, out MmessageIcon).Replace("#", ""));
                    return;
                }
                //----------------------------------------------------------
                if (Formvalidation() == true)
                {
                    if (checkWithBalLeave() == true)
                    {
                        if (SaveEmpLeave() == true)
                        {
                            lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, (MbAddStatus ? 2 : 21), out MmessageIcon);
                            if (MbAddStatus == true)
                            {
                                if (MbAddPermission == true)
                                {
                                    MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 2, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    BindingNavigatorMoveLastItem_Click(sender, e);
                                    lblstatus.Text = "";

                                }
                            }
                            else
                            {
                                //CurrentRecCnt = CurrentRecCnt + 1
                                //BindingNavigatorMovePreviousItem_Click(sender, e)
                                MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 21, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                MbAddStatus = false;
                            }

                            TimerLeave.Enabled = true;
                        }

                    }
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim() == "")
            {
                MsMessageBody = new ClsNotification().GetErrorMessage(MaMessage, 10129, out MmessageIcon).Replace("#", "");
                MessageBox.Show(MsMessageBody, MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10129, out MmessageIcon);
                TimerLeave.Enabled = true;
                txtSearch.Focus();
                return;
            }
            CurrentRecCnt = 1;
            MblnSearch = true;
            if (DisplayLeaveInformation(1) == false)
            {
                MblnSearch = false;
                MsMessageBody = new ClsNotification().GetErrorMessage(MaMessage, 10131, out MmessageIcon).Replace("#", "");
                MessageBox.Show(MsMessageBody, MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10131, out MmessageIcon);
                TimerLeave.Enabled = true;
                ClearSearchControls();
                BindingNavigatorAddNewItem_Click(sender, e);
            }
        }

        private void FrmLeaveEntry_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                        objHelp.strFormName = "LeaveEntry";
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        if (MbAddPermission)
                            BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (MbDeletePermission && BindingNavigatorDeleteItem.Enabled)
                            BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        if (MbPrintEmailPermission && BtnPrint.Enabled)
                            BtnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        if (MbPrintEmailPermission && BtnEmail.Enabled)
                            BtnEmail_Click(sender, new EventArgs());//Email
                        break;
                }
            }
            catch { }
        }

        private void FrmLeaveEntry_FormClosing(object sender, FormClosingEventArgs e)
        {
            PiEmpid = 0;
        }

        private void TimerLeave_Tick(object sender, EventArgs e)
        {
            lblstatus.Text = "";
            TimerLeave.Enabled = false;
            ErrorProviderEmpLeave.Clear();
        }

        private void TmFocus_Tick(object sender, EventArgs e)
        {
            CboEmployee.Focus();
            TmFocus.Enabled = false;
        }

        private void HalfDayCheckBox_CheckedChanged(object sender, EventArgs e)
        {

            if (HalfDayCheckBox.Checked == true)
            {
                LeaveDateToDateTimePicker.Value = LeaveDateFromDateTimePicker.Value;
            }
            else
            {
                try
                {
                    LeaveDateToDateTimePicker.Value = LeaveDateFromDateTimePicker.Value.AddDays(1);
                }
                catch
                {
                    LeaveDateToDateTimePicker.Value = LeaveDateFromDateTimePicker.Value;
                }

            }
            if ((CboLeaveType.Text != "") && (CboEmployee.Text != ""))
            {
                GetBalanceAndHolidays();
            }
            if (HalfDayCheckBox.Checked == true)
            {
                LblDateTo.Visible = false;
                LeaveDateToDateTimePicker.Visible = false;
                LblRejoinDate.Visible = false;
                RejoinDateDateTimePicker.Visible = false;
                //if (ClsCommonSettings.IsArabicView)
                //    LblDateFrom.Text = "تاريخ";
                //else
                    LblDateFrom.Text = "Date";
            }
            else
            {
                LblDateTo.Visible = true;
                LeaveDateToDateTimePicker.Visible = true;
                LblRejoinDate.Visible = true;
                RejoinDateDateTimePicker.Visible = true;
                //if (ClsCommonSettings.IsArabicView)
                //    LblDateFrom.Text = "تاريخ من";
                //else
                    LblDateFrom.Text = "Date From";
            }
            MbChangeStatus = true;
            EnableDisablButtons();
        }

        private void BtnLeaveExtension_Click(object sender, EventArgs e)
        {
            using (FrmLeaveExtension Mobj = new FrmLeaveExtension())
            {
                Mobj.PintEmployeeID = CboEmployee.SelectedValue.ToInt32();
                Mobj.ShowDialog();
            }

        }

        private void chkPaidUnPaid_CheckedChanged(object sender, EventArgs e)
        {
            MbChangeStatus = true;
            EnableDisablButtons();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            MblnSearch = false;
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            FrmReportviewer ObjViewer = new FrmReportviewer();
            ObjViewer.PsFormName = "LeaveEntry";
            ObjViewer.PiRecId = mobjclsBLLLeaveEntry.MobjclsDTOLeaveEntry.intLeaveID;
            ObjViewer.PiFormID = (int)FormID.LeaveEntry;
            ObjViewer.ShowDialog();
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {

            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Leave Entry";
                    ObjEmailPopUp.EmailFormType = EmailFormID.LeaveEntry;

                    ObjEmailPopUp.EmailSource = mobjclsBLLLeaveEntry.DisplayEmployeeLeaveEntry();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch { }
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {

            int ityppe = 0;

            DataTable dtCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "EmploymentTypeID", "EmployeeMaster", " EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) });
            if (dtCombos != null && dtCombos.Rows.Count > 0)
            {
                ityppe = dtCombos.Rows[0]["EmploymentTypeID"].ToInt32();
            }
            else if (ityppe != 2)
            {
                if (mobjclsBLLLeaveEntry.FillCombos(new string[] { " PaymentID ", "PayEmployeePayment", " EmployeeID = " + Convert.ToInt32(CboEmployee.SelectedValue) + " " + " and convert(datetime,'" + LeaveDateFromDateTimePicker.Text + "',106)  between " + " convert(datetime,convert(char(15),PeriodFrom,106),106) and convert(datetime,convert(char(15),PeriodTo,106),106)" }).Rows.Count > 0)
                {
                    MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10142, out MmessageIcon).Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            int iEmpid = Convert.ToInt32(CboEmployee.SelectedValue);
            int ileavetypeid = Convert.ToInt32(CboLeaveType.SelectedValue);
            System.DateTime dYear = LeaveDateFromDateTimePicker.Value.Date;
            if (DeleteLeaveEntry() == true)
            {
                lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 4, out MmessageIcon);
                TimerLeave.Enabled = true;
                LoadInitial();
                BindingEnableDisable(sender, e);
            }

        }

        private void LeaveDateFromDateTimePicker_ValueChanged(object sender, EventArgs e)
        {

            if (bSearchFlag == true)
                return;

            if (HalfDayCheckBox.Checked == true)
            {
                LeaveDateToDateTimePicker.Value = LeaveDateFromDateTimePicker.Value;
            }
            else
            {
                try
                {
                    LeaveDateToDateTimePicker.Value = LeaveDateFromDateTimePicker.Value.AddDays(1);                           //DateAdd(DateInterval.Day, 1, LeaveDateFromDateTimePicker.Value);
                }
                catch
                {
                    LeaveDateToDateTimePicker.Value = LeaveDateFromDateTimePicker.Value;
                }

            }

            if ((CboLeaveType.Text != "") && (CboEmployee.Text != ""))
            {
                if ((LeaveDateToDateTimePicker.Value.Year - LeaveDateFromDateTimePicker.Value.Year).ToInt32() < 2 && (LeaveDateToDateTimePicker.Value.Year - LeaveDateFromDateTimePicker.Value.Year).ToInt32() >= 0)
                {
                    GetBalanceAndHolidays();
                }
            }
            MbChangeStatus = true;
            EnableDisablButtons();
        }

        private void LeaveDateToDateTimePicker_ValueChanged(object sender, EventArgs e)
        {

            if (bSearchFlag == true)
                return;
            if ((CboLeaveType.Text != "") && (CboEmployee.Text != ""))
            {
                if ((LeaveDateToDateTimePicker.Value.Year - LeaveDateFromDateTimePicker.Value.Year).ToInt32() < 2 && (LeaveDateToDateTimePicker.Value.Year - LeaveDateFromDateTimePicker.Value.Year).ToInt32() >= 0)
                {
                    GetBalanceAndHolidays();
                }
            }
            RejoinDateDateTimePicker.Value = LeaveDateToDateTimePicker.Value.AddDays(1);
            MbChangeStatus = true;
            EnableDisablButtons();
        }

        private void RejoinDateDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            if (bSearchFlag == true)
                return;
            if (RejoinDateDateTimePicker.Value >= LeaveDateToDateTimePicker.Value)
            {
                ErrorProviderEmpLeave.Clear();
            }
            MbChangeStatus = true;
            EnableDisablButtons();
        }

        private void RemarksTextBox_TextChanged(object sender, EventArgs e)
        {
            MbChangeStatus = true;
            EnableDisablButtons();
        }

        private void BindingNavigatorPositionItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.CboEmployee.Enabled = false;
            MbAddStatus = false;
            MbChangeStatus = false;
            EnableDisablButtons();

        }

        private void CboEmployee_Leave(object sender, EventArgs e)
        {

            if (MbAddStatus == false)
            {
                if (CboEmployee.SelectedIndex == -1)
                {
                    //MsMessageBody = new ClsNotification().GetErrorMessage(MaMessage, 1, out MmessageIcon);
                    //TimerLeave.Enabled = true;me
                    //CboEmployee.Focus();
                }
            }
            else
            {
                if (MbAddStatus == true)
                {
                    ErrorProviderEmpLeave.SetError(CboEmployee, "");
                }

            }
        }

        private void CboLeaveType_Leave(object sender, EventArgs e)
        {

        }

        private void CboEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboEmployee.DroppedDown = false;
            MbChangeStatus = true;
            EnableDisablButtons();
        }

        private void CboLeaveType_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboLeaveType.DroppedDown = false;
            MbChangeStatus = true;
            EnableDisablButtons();
        }

        private void CboEmployee_SelectedValueChanged(object sender, EventArgs e)
        {
            if (bSearchFlag == true)
                return;
            ErrorProviderEmpLeave.Clear();
            LblDOJ.Text = "";
            if (CboEmployee.SelectedIndex != -1)
            {

                if (MbAddStatus == true)
                {
                    //ClsEmployee ObjEmp = new ClsEmployee();
                    if (Convert.ToInt32(CboEmployee.SelectedValue) != 0)
                    {
                        if (GetEmployeeList(CboEmployee.SelectedValue.ToInt32()) == false)
                        {
                            MessageBox.Show(new ClsNotification().GetErrorMessage(MaMessage, 10137, out MmessageIcon).Replace("#", "").Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10137, out MmessageIcon);
                            TimerLeave.Enabled = true;
                            CboEmployee.Focus();
                            this.Cursor = Cursors.Default;
                            return;

                        }
                        CboLeaveType.DataSource = null;
                        if (CboEmployee.SelectedValue.ToInt32() > 0)
                        {
                            DataTable datCombos = new DataTable();

                            ////if (ClsCommonSettings.IsArabicView)
                            ////    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType", " EmployeeMaster EM INNER JOIN PayLeavePolicyDetail CLD " + "ON EM.LeavePolicyID = CLD.LeavePolicyID INNER JOIN PayLeaveTypeReference LTR ON CLD.LeaveTypeID = LTR.LeaveTypeID ", "EmployeeID=" + CboEmployee.SelectedValue.ToInt32() + " " });// 
                            ////else
                                datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveType", " EmployeeMaster EM INNER JOIN PayLeavePolicyDetail CLD " + "ON EM.LeavePolicyID = CLD.LeavePolicyID INNER JOIN PayLeaveTypeReference LTR ON CLD.LeaveTypeID = LTR.LeaveTypeID ", "EmployeeID=" + CboEmployee.SelectedValue.ToInt32() + " " });// 

                            CboLeaveType.ValueMember = "LeaveTypeID";
                            CboLeaveType.DisplayMember = "LeaveType";
                            CboLeaveType.DataSource = datCombos;
                        }
                    }

                    if ((CboEmployee.SelectedValue.ToInt32() > 0))
                    {

                        DataTable dtsdrLeave = mobjclsBLLLeaveEntry.FillCombos(new string[] { "Convert(varchar(11),DateofJoining,106),LeavepolicyID ", " EmployeeMaster ", " EmployeeID =" + Convert.ToInt32(CboEmployee.SelectedValue) });
                        if (dtsdrLeave != null && dtsdrLeave.Rows.Count > 0)
                        {

                            LblDOJ.Text = dtsdrLeave.Rows[0][0].ToString();

                            MiLeavepolicyID = dtsdrLeave.Rows[0]["Leavepolicyid"].ToInt32();
                        }


                    }

                    if ((CboEmployee.SelectedValue.ToInt32() > 0))
                    {
                        DataTable DtsdrExpense = new DataTable();
                        //if (ClsCommonSettings.IsArabicView)
                        //    DtsdrExpense = mobjclsBLLLeaveEntry.FillCombos(new string[] { " CR.CurrencyID,CR.CurrencyNameArb AS CurrencyName,E.CompanyID", " CurrencyReference CR LEFT OUTER JOIN CompanyMaster C on C.CurrencyId=CR.CurrencyId " + " LEFT OUTER JOIN Employeemaster E on E.CompanyID=C.CompanyID ", " E.EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) });
                        //else
                            DtsdrExpense = mobjclsBLLLeaveEntry.FillCombos(new string[] { " CR.CurrencyID,CR.CurrencyName AS CurrencyName,E.CompanyID", " CurrencyReference CR LEFT OUTER JOIN CompanyMaster C on C.CurrencyId=CR.CurrencyId " + " LEFT OUTER JOIN Employeemaster E on E.CompanyID=C.CompanyID ", " E.EmployeeID=" + Convert.ToInt32(CboEmployee.SelectedValue) });

                        if (DtsdrExpense != null && DtsdrExpense.Rows.Count > 0)
                        {
                            MiCompanyID = DtsdrExpense.Rows[0][2].ToInt32();

                        }

                    }
                    else
                    {
                        CboLeaveType.DataSource = null;
                        if ((CboEmployee.SelectedValue.ToInt32() > 0))
                        {
                            DataTable datCombos = new DataTable();
                            //if (ClsCommonSettings.IsArabicView)
                            //    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType", "PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID", "EmployeeID=" + CboEmployee.SelectedValue + "  UNION " + "SELECT ELD.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType FROM PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + "  UNION " + "SELECT ELS.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType FROM PayEmployeeLeaveSummary ELS INNER JOIN PayLeaveTypeReference LTR " + "ON ELS.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + " " });
                            //else
                                datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveType", "PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID", "EmployeeID=" + CboEmployee.SelectedValue + "  UNION " + "SELECT ELD.LeaveTypeID,LTR.LeaveType FROM PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + "  UNION " + "SELECT ELS.LeaveTypeID,LTR.LeaveType FROM PayEmployeeLeaveSummary ELS INNER JOIN PayLeaveTypeReference LTR " + "ON ELS.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + " " });
                            CboLeaveType.ValueMember = "LeaveTypeID";
                            CboLeaveType.DisplayMember = "LeaveType";
                            CboLeaveType.DataSource = datCombos;
                        }
                    }
                    ErrorProviderEmpLeave.SetError(CboEmployee, "");
                    lblstatus.Text = "";
                    TimerLeave.Enabled = true;
                    CboLeaveType.SelectedIndex = -1;
                    lblBalLeaveValue.Text = "";
                }

                MbChangeStatus = true;
                this.Cursor = Cursors.Default;

                ///'''''''''''''''' Employee Dateof Joining Checking - Changes Made By Tijo As On 27-Jul-2011 '''''''''''''''''''''''
                try
                {
                    if ((CboEmployee.SelectedValue.ToInt32() > 0))
                    {

                        DataTable dtdrdrChecking = mobjclsBLLLeaveEntry.FillCombos(new string[] { "convert(datetime,convert(varchar(12),Dateofjoining,106),106) AS Dateofjoining", "EmployeeMaster", " EmployeeID=" + CboEmployee.SelectedValue });

                        if (dtdrdrChecking != null && dtdrdrChecking.Rows.Count > 0)
                        {
                            LeaveDateFromDateTimePicker.MinDate = dtdrdrChecking.Rows[0][0].ToDateTime();
                            LeaveDateToDateTimePicker.MinDate = dtdrdrChecking.Rows[0][0].ToDateTime();
                            RejoinDateDateTimePicker.MinDate = dtdrdrChecking.Rows[0][0].ToDateTime();
                        }
                    }
                }
                catch { }
                EnableDisablButtons();
                ///'''''''''''''''''''''''''''''''''' END ''''''''''''''''''''''''''''''''''''''''''
            }

        }

        private void CboLeaveType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                clsBLLLeaveExtension objLeaveExtension = new clsBLLLeaveExtension();
                if (bSearchFlag == true)
                    return;
                this.Cursor = Cursors.WaitCursor;
                if (Convert.ToInt32(CboLeaveType.SelectedValue) == 3 && CboEmployee.Text != "")//Maternity
                {
                    if (objLeaveExtension.CheckMaleOrFemale(Convert.ToInt32(CboEmployee.SelectedValue)))//Maternity leave is for female employees only
                    {
                        ShowErrorMessage(CboLeaveType, 10168, false, false);

                        CboLeaveType.SelectedValue = 1;
                        this.Cursor = Cursors.Default;
                    }
                }
                lblBalLeaveValue.Text = "0";
                if (CboLeaveType.SelectedIndex != -1)
                {
                    ErrorProviderEmpLeave.SetError(CboLeaveType, "");
                    lblstatus.Text = "";
                    TimerLeave.Enabled = true;
                }
                CboLeaveType.Text = "";
                lblBalLeaveValue.Text = "";
                CboLeaveType.DroppedDown = false;
                if (CboLeaveType.SelectedIndex != -1 & CboEmployee.SelectedIndex != -1)
                {
                    GetBalanceAndHolidays();
                }
                MbChangeStatus = true;
                this.Cursor = Cursors.Default;
                EnableDisablButtons();
            }
            catch { }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewLeaveEntry();

        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            try
            {
                bSearchFlag = true;
                MiCompanyID = 0;
                MiLeavepolicyID = 0;
                MbAddStatus = false;

                LoadCombos(0);
                if (MblnSearch == false)
                    RecCountNavigate(PiEmpid);

                if (TotalRecordCnt > 0)
                {
                    CurrentRecCnt = TotalRecordCnt;
                }
                else
                {
                    CurrentRecCnt = 0;
                }
                BindingNavigatorPositionItem.Text = CurrentRecCnt.ToString();
                DisplayLeaveInformation(CurrentRecCnt);

                CboEmployee.Enabled = false;

                LblBalLeave.Visible = false;
                lblBalLeaveValue.Visible = false;
                MbChangeStatus = false;
                bSearchFlag = false;
                lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 12, out MmessageIcon);
                TimerLeave.Enabled = true;
                setBindingNavigatorbuttons();
                if ((CboEmployee.SelectedValue.ToInt32() > 0))
                {
                    if ((TempLeaveType == 1))
                    {
                        DataTable datCombos =  new DataTable();
                        //if (ClsCommonSettings.IsArabicView)
                        //    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType", " EmployeeMaster EM INNER JOIN PayLeavePolicyDetail CLD " + "ON EM.LeavePolicyID = CLD.LeavePolicyID INNER JOIN PayLeaveTypeReference LTR ON CLD.LeaveTypeID = LTR.LeaveTypeID ", "EmployeeID=" + CboEmployee.SelectedValue.ToInt32() + "  " });
                        //else
                            datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveType", " EmployeeMaster EM INNER JOIN PayLeavePolicyDetail CLD " + "ON EM.LeavePolicyID = CLD.LeavePolicyID INNER JOIN PayLeaveTypeReference LTR ON CLD.LeaveTypeID = LTR.LeaveTypeID ", "EmployeeID=" + CboEmployee.SelectedValue.ToInt32() + "  " });
                        CboLeaveType.ValueMember = "LeaveTypeID";
                        CboLeaveType.DisplayMember = "LeaveType";
                        CboLeaveType.DataSource = datCombos;

                        CboLeaveType.SelectedValue = TempLeaveType;
                    }
                    else
                    {
                        DataTable datCombos =  new DataTable();
                        //if (ClsCommonSettings.IsArabicView)
                        //    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType", "PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID", "EmployeeID=" + CboEmployee.SelectedValue + " UNION " + "SELECT ELD.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType FROM PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + " UNION " + "SELECT ELS.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType FROM PayEmployeeLeaveSummary ELS INNER JOIN PayLeaveTypeReference LTR " + "ON ELS.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + " " });
                        //else
                            datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveType", "PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID", "EmployeeID=" + CboEmployee.SelectedValue + " UNION " + "SELECT ELD.LeaveTypeID,LTR.LeaveType FROM PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + " UNION " + "SELECT ELS.LeaveTypeID,LTR.LeaveType FROM PayEmployeeLeaveSummary ELS INNER JOIN PayLeaveTypeReference LTR " + "ON ELS.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + " " });
                        CboLeaveType.ValueMember = "LeaveTypeID";
                        CboLeaveType.DisplayMember = "LeaveType";
                        CboLeaveType.DataSource = datCombos;
                        CboLeaveType.SelectedValue = TempLeaveType;
                    }
                }
                EnableDisablButtons();
                if (Checkvisibility(PiEmpid, false))
                {
                    //lblstatus.Text = New ClsNotification().GetErrorMessage(MaStatusMessage, 18, False)
                    //TimerLeave.Enabled = True
                }
                else
                {
                    this.BindingNavigatorDeleteItem.Enabled = false;
                }

            }
            catch
            {
                bSearchFlag = false;
            }
            //------------------------------------------------------------------------
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            //------------Permission-----------------------------------------
            try
            {
                bSearchFlag = true;
                MiCompanyID = 0;
                MiLeavepolicyID = 0;
                MbAddStatus = false;

                LoadCombos(0);

                if (MblnSearch == false)
                    RecCountNavigate(PiEmpid);

                if (TotalRecordCnt > 0)
                {
                    CurrentRecCnt = 1;
                }
                else
                {
                    CurrentRecCnt = 0;
                }
                BindingNavigatorPositionItem.Text = CurrentRecCnt.ToString();
                DisplayLeaveInformation(CurrentRecCnt);
                CboEmployee.Enabled = false;

                LblBalLeave.Visible = false;
                lblBalLeaveValue.Visible = false;
                MbChangeStatus = false;
                bSearchFlag = false;
                setBindingNavigatorbuttons();
                lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 9, out MmessageIcon);
                TimerLeave.Enabled = true;
                if ((CboEmployee.SelectedValue.ToInt32() > 0))
                {
                    if ((TempLeaveType == 1))
                    {
                        DataTable datCombos = new DataTable();
                        //if (ClsCommonSettings.IsArabicView)
                        //    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType", " EmployeeMaster EM INNER JOIN PayLeavePolicyDetail CLD " + "ON EM.LeavePolicyID = CLD.LeavePolicyID INNER JOIN PayLeaveTypeReference LTR ON CLD.LeaveTypeID = LTR.LeaveTypeID ", "EmployeeID=" + CboEmployee.SelectedValue.ToInt32() + "  " });
                        //else
                            datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveType", " EmployeeMaster EM INNER JOIN PayLeavePolicyDetail CLD " + "ON EM.LeavePolicyID = CLD.LeavePolicyID INNER JOIN PayLeaveTypeReference LTR ON CLD.LeaveTypeID = LTR.LeaveTypeID ", "EmployeeID=" + CboEmployee.SelectedValue.ToInt32() + "  " });
                        CboLeaveType.ValueMember = "LeaveTypeID";
                        CboLeaveType.DisplayMember = "LeaveType";
                        CboLeaveType.DataSource = datCombos;

                        CboLeaveType.SelectedValue = TempLeaveType;
                    }
                    else
                    {
                        DataTable datCombos = new DataTable();
                        //if (ClsCommonSettings.IsArabicView)
                        //    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType", "PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID", "EmployeeID=" + CboEmployee.SelectedValue + " UNION " + "SELECT ELD.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType FROM PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + " UNION " + "SELECT ELS.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType FROM PayEmployeeLeaveSummary ELS INNER JOIN PayLeaveTypeReference LTR " + "ON ELS.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + "" });
                        //else
                            datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveType", "PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID", "EmployeeID=" + CboEmployee.SelectedValue + " UNION " + "SELECT ELD.LeaveTypeID,LTR.LeaveType FROM PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + " UNION " + "SELECT ELS.LeaveTypeID,LTR.LeaveType FROM PayEmployeeLeaveSummary ELS INNER JOIN PayLeaveTypeReference LTR " + "ON ELS.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + "" });
                        CboLeaveType.ValueMember = "LeaveTypeID";
                        CboLeaveType.DisplayMember = "LeaveType";
                        CboLeaveType.DataSource = datCombos;
                        CboLeaveType.SelectedValue = TempLeaveType;
                    }
                }
                EnableDisablButtons();
                if (Checkvisibility(PiEmpid, false))
                {
                    //lblstatus.Text = New ClsNotification().GetErrorMessage(MaStatusMessage, 16, False)
                    //TimerLeave.Enabled = True
                }
                else
                {
                    BindingNavigatorDeleteItem.Enabled = false;
                }

            }
            catch 
            {
                bSearchFlag = false;
            }
            //-----------------------------------------------------------------
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            try
            {
                bSearchFlag = true;
                MiCompanyID = 0;
                MiLeavepolicyID = 0;
                MbAddStatus = false;
                LoadCombos(0);
                if (MblnSearch == false)
                    RecCountNavigate(PiEmpid);

                if (TotalRecordCnt > 0)
                {
                    CurrentRecCnt = CurrentRecCnt - 1;
                    if (CurrentRecCnt <= 0)
                    {
                        CurrentRecCnt = 1;
                    }
                }
                else
                {
                    CurrentRecCnt = 1;
                }
                BindingNavigatorPositionItem.Text = CurrentRecCnt.ToString();
                DisplayLeaveInformation(CurrentRecCnt);
                MbAddStatus = false;

                CboEmployee.Enabled = false;

                LblBalLeave.Visible = false;
                lblBalLeaveValue.Visible = false;
                MbChangeStatus = false;
                bSearchFlag = false;
                setBindingNavigatorbuttons();
                lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 10, out MmessageIcon);
                TimerLeave.Enabled = true;
                if ((CboEmployee.SelectedValue.ToInt32() > 0))
                {
                    if ((TempLeaveType == 1))
                    {
                        DataTable datCombos = new DataTable();
                        //if (ClsCommonSettings.IsArabicView)
                        //    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType", " EmployeeMaster EM INNER JOIN PayLeavePolicyDetail CLD " + "ON EM.LeavePolicyID = CLD.LeavePolicyID INNER JOIN PayLeaveTypeReference LTR ON CLD.LeaveTypeID = LTR.LeaveTypeID ", "EmployeeID=" + CboEmployee.SelectedValue.ToInt32() + "" });
                        //else
                            datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveType", " EmployeeMaster EM INNER JOIN PayLeavePolicyDetail CLD " + "ON EM.LeavePolicyID = CLD.LeavePolicyID INNER JOIN PayLeaveTypeReference LTR ON CLD.LeaveTypeID = LTR.LeaveTypeID ", "EmployeeID=" + CboEmployee.SelectedValue.ToInt32() + "" });
                        CboLeaveType.ValueMember = "LeaveTypeID";
                        CboLeaveType.DisplayMember = "LeaveType";
                        CboLeaveType.DataSource = datCombos;

                        CboLeaveType.SelectedValue = TempLeaveType;
                    }
                    else
                    {
                        DataTable datCombos = new DataTable();
                        //if (ClsCommonSettings.IsArabicView)
                        //    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType", "PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID", "EmployeeID=" + CboEmployee.SelectedValue + " UNION " + "SELECT ELD.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType FROM PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + " UNION " + "SELECT ELS.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType FROM PayEmployeeLeaveSummary ELS INNER JOIN PayLeaveTypeReference LTR " + "ON ELS.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + "" });
                        //else
                            datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveType", "PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID", "EmployeeID=" + CboEmployee.SelectedValue + " UNION " + "SELECT ELD.LeaveTypeID,LTR.LeaveType FROM PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + " UNION " + "SELECT ELS.LeaveTypeID,LTR.LeaveType FROM PayEmployeeLeaveSummary ELS INNER JOIN PayLeaveTypeReference LTR " + "ON ELS.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + "" });
                        CboLeaveType.ValueMember = "LeaveTypeID";
                        CboLeaveType.DisplayMember = "LeaveType";
                        CboLeaveType.DataSource = datCombos;
                        CboLeaveType.SelectedValue = TempLeaveType;
                    }
                }
                EnableDisablButtons();
                if (Checkvisibility(PiEmpid, false))
                {
                    //lblstatus.Text = New ClsNotification().GetErrorMessage(MaStatusMessage, 17, False)
                    //TimerLeave.Enabled = True
                }
                else
                {
                    this.BindingNavigatorDeleteItem.Enabled = false;
                }

            }
            catch
            {
                bSearchFlag = false;
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            try
            {
                bSearchFlag = true;
                MiCompanyID = 0;
                MiLeavepolicyID = 0;
                MbAddStatus = false;
                LoadCombos(0);
                if (MblnSearch == false)
                    RecCountNavigate(PiEmpid);
                if (TotalRecordCnt > 0)
                {
                    CurrentRecCnt = CurrentRecCnt + 1;
                    if (CurrentRecCnt >= TotalRecordCnt)
                    {
                        CurrentRecCnt = TotalRecordCnt;
                    }
                }
                else
                {
                    CurrentRecCnt = 0;
                }
                BindingNavigatorPositionItem.Text = CurrentRecCnt.ToString();
                DisplayLeaveInformation(CurrentRecCnt);
                CboEmployee.Enabled = false;
                LblBalLeave.Visible = false;
                lblBalLeaveValue.Visible = false;
                MbChangeStatus = false;
                bSearchFlag = false;
                lblstatus.Text = new ClsNotification().GetErrorMessage(MaStatusMessage, 11, out MmessageIcon);
                TimerLeave.Enabled = true;
                setBindingNavigatorbuttons();
                if ((CboEmployee.SelectedValue.ToInt32() > 0))
                {
                    if ((TempLeaveType == 1))
                    {
                        DataTable datCombos = new DataTable();
                        //if (ClsCommonSettings.IsArabicView)
                        //    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType", " EmployeeMaster EM INNER JOIN PayLeavePolicyDetail CLD " + "ON EM.LeavePolicyID = CLD.LeavePolicyID INNER JOIN PayLeaveTypeReference LTR ON CLD.LeaveTypeID = LTR.LeaveTypeID ", "EmployeeID=" + CboEmployee.SelectedValue.ToInt32() + "" });
                        //else
                            datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveType", " EmployeeMaster EM INNER JOIN PayLeavePolicyDetail CLD " + "ON EM.LeavePolicyID = CLD.LeavePolicyID INNER JOIN PayLeaveTypeReference LTR ON CLD.LeaveTypeID = LTR.LeaveTypeID ", "EmployeeID=" + CboEmployee.SelectedValue.ToInt32() + "" });
                        CboLeaveType.ValueMember = "LeaveTypeID";
                        CboLeaveType.DisplayMember = "LeaveType";
                        CboLeaveType.DataSource = datCombos;

                        CboLeaveType.SelectedValue = TempLeaveType;
                    }
                    else
                    {
                        DataTable datCombos = new DataTable();
                        //if (ClsCommonSettings.IsArabicView)
                        //    datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType", "PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID", "EmployeeID=" + CboEmployee.SelectedValue + "  UNION " + "SELECT ELD.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType FROM PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + "  UNION " + "SELECT ELS.LeaveTypeID,LTR.LeaveTypeArb AS LeaveType FROM PayEmployeeLeaveSummary ELS INNER JOIN PayLeaveTypeReference LTR " + "ON ELS.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + " " });
                        //else
                            datCombos = mobjclsBLLLeaveEntry.FillCombos(new string[] { "LTR.LeaveTypeID,LTR.LeaveType", "PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID", "EmployeeID=" + CboEmployee.SelectedValue + "  UNION " + "SELECT ELD.LeaveTypeID,LTR.LeaveType FROM PayEmployeeLeaveDetails ELD INNER JOIN PayLeaveTypeReference LTR " + "ON ELD.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + "  UNION " + "SELECT ELS.LeaveTypeID,LTR.LeaveType FROM PayEmployeeLeaveSummary ELS INNER JOIN PayLeaveTypeReference LTR " + "ON ELS.LeaveTypeID=LTR.LeaveTypeID WHERE EmployeeID=" + CboEmployee.SelectedValue + " " });
                        CboLeaveType.ValueMember = "LeaveTypeID";
                        CboLeaveType.DisplayMember = "LeaveType";
                        CboLeaveType.DataSource = datCombos;
                        CboLeaveType.SelectedValue = TempLeaveType;
                    }
                }
                EnableDisablButtons();
                if (Checkvisibility(PiEmpid, false))
                {
                    //lblstatus.Text = New ClsNotification().GetErrorMessage(MaStatusMessage, 19, False)
                    //TimerLeave.Enabled = True
                }
                else
                {
                    BindingNavigatorDeleteItem.Enabled = false;
                }
            }
            catch 
            {
                bSearchFlag = false;
            }
            //-------------------------------------------------------
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindingNavigatorCancelItem_Click(object sender, EventArgs e)
        {
            try
            {

                ClearSearchControls();
                //if (PiEmpid == 0)
                //    LoadInitial();
                //else
                //    BindingNavigatorAddNewItem.Enabled = true;
                ErrorProviderEmpLeave.Clear();
                MbAddStatus = false;
                if (MbAddPermission == true)
                {
                    BindingNavigatorAddNewItem_Click(sender, e);
                }
                else
                    ClearAllControls();

                MbChangeStatus = false;
                chkPaidUnPaid.Checked = false;
                EnableDisablButtons();

            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorCancelItem_Click(:LeaveEntry." + ex.Message);
                mObjLogs.WriteLog(" Error on BindingNavigatorCancelItem_Click(:LeaveEntry." + ex.Message, 2);

            }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "LeaveEntry";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void btnDocument_Click(object sender, EventArgs e)
        {
            //using (FrmScanning objScanning = new FrmScanning())
            //{
            //    objScanning.MintDocumentTypeid = (int)DocumentType.Leave_Entry;
            //    objScanning.MlngReferenceID = CboEmployee.SelectedValue.ToInt64();
            //    objScanning.MintOperationTypeID = (int)OperationType.Employee;
            //    objScanning.MstrReferenceNo = ((DataRowView)(CboEmployee.SelectedItem)).Row.ItemArray[2].ToStringCustom() + " [" + LeaveDateFromDateTimePicker.Value.ToString("dd MMM yyyy") + " - " + LeaveDateToDateTimePicker.Value.ToString("dd MMM yyyy") + "]";
            //    objScanning.PblnEditable = true;
            //    //objScanning.MintDocumentID = CboEmployee.Tag.ToInt32();
            //    objScanning.ShowDialog();
            //}
        }

        private void LblBalLeave_VisibleChanged(object sender, EventArgs e)
        {
            LblBalLeaveColn.Visible = LblBalLeave.Visible;
        }

        private void lblnoofholidays_VisibleChanged(object sender, EventArgs e)
        {
            lblnoofholidaysColn.Visible = lblnoofholidays.Visible;
        }
      

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch_Click(null, null);
            }
        } 
        #endregion
    }
}
