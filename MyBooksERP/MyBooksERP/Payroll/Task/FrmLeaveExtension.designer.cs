﻿namespace MyBooksERP
{
    partial class FrmLeaveExtension
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLeaveExtension));
            this.Label6 = new System.Windows.Forms.Label();
            this.FullPayment = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.lblBal = new System.Windows.Forms.Label();
            this.lblExtensionId = new System.Windows.Forms.Label();
            this.TxtBalLeave = new System.Windows.Forms.TextBox();
            this.lblMY = new System.Windows.Forms.Label();
            this.lblTimeDay = new System.Windows.Forms.Label();
            this.CboLeaveType = new System.Windows.Forms.ComboBox();
            this.EmployeeComboBox = new System.Windows.Forms.ComboBox();
            this.MainPanel = new System.Windows.Forms.GroupBox();
            this.DtpMonthYear = new System.Windows.Forms.DateTimePicker();
            this.lblPeriod = new System.Windows.Forms.Label();
            this.cboPeriod = new System.Windows.Forms.ComboBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.ReasonTextBox = new System.Windows.Forms.TextBox();
            this.NoOfDaysOrMinuteTextBox = new System.Windows.Forms.TextBox();
            this.ExtensionTypeComboBox = new System.Windows.Forms.ComboBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.ShapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.RptLeaveExtenstionSummaryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.OKSaveButton = new System.Windows.Forms.Button();
            this.ErrorProviderLeave = new System.Windows.Forms.ErrorProvider(this.components);
            this.TmStripClear = new System.Windows.Forms.Timer(this.components);
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.CancelToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.BtnSave = new System.Windows.Forms.Button();
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            this.ReceivedAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PaymentAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvoiceAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvoiceNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SlNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RptLeaveExtenstionSummaryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderLeave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator)).BeginInit();
            this.BindingNavigator.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(7, 156);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(49, 13);
            this.Label6.TabIndex = 14;
            this.Label6.Text = "Reason";
            // 
            // FullPayment
            // 
            this.FullPayment.HeaderText = "FullPayment";
            this.FullPayment.Name = "FullPayment";
            this.FullPayment.ReadOnly = true;
            this.FullPayment.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FullPayment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // lblBal
            // 
            this.lblBal.AutoSize = true;
            this.lblBal.Location = new System.Drawing.Point(213, 131);
            this.lblBal.Name = "lblBal";
            this.lblBal.Size = new System.Drawing.Size(79, 13);
            this.lblBal.TabIndex = 17;
            this.lblBal.Text = "Balance Leave";
            // 
            // lblExtensionId
            // 
            this.lblExtensionId.AutoSize = true;
            this.lblExtensionId.Location = new System.Drawing.Point(8, 24);
            this.lblExtensionId.Name = "lblExtensionId";
            this.lblExtensionId.Size = new System.Drawing.Size(80, 13);
            this.lblExtensionId.TabIndex = 9;
            this.lblExtensionId.Text = "Extension Type";
            // 
            // TxtBalLeave
            // 
            this.TxtBalLeave.Enabled = false;
            this.TxtBalLeave.Location = new System.Drawing.Point(301, 128);
            this.TxtBalLeave.Name = "TxtBalLeave";
            this.TxtBalLeave.Size = new System.Drawing.Size(80, 20);
            this.TxtBalLeave.TabIndex = 16;
            this.TxtBalLeave.TextChanged += new System.EventHandler(this.TxtBalLeave_TextChanged);
            // 
            // lblMY
            // 
            this.lblMY.AutoSize = true;
            this.lblMY.Location = new System.Drawing.Point(8, 78);
            this.lblMY.Name = "lblMY";
            this.lblMY.Size = new System.Drawing.Size(30, 13);
            this.lblMY.TabIndex = 4;
            this.lblMY.Text = "Date";
            // 
            // lblTimeDay
            // 
            this.lblTimeDay.AutoSize = true;
            this.lblTimeDay.Location = new System.Drawing.Point(8, 131);
            this.lblTimeDay.Name = "lblTimeDay";
            this.lblTimeDay.Size = new System.Drawing.Size(85, 13);
            this.lblTimeDay.TabIndex = 13;
            this.lblTimeDay.Text = "Number Of Days";
            // 
            // CboLeaveType
            // 
            this.CboLeaveType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboLeaveType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboLeaveType.BackColor = System.Drawing.SystemColors.Info;
            this.CboLeaveType.DropDownHeight = 134;
            this.CboLeaveType.FormattingEnabled = true;
            this.CboLeaveType.IntegralHeight = false;
            this.CboLeaveType.Location = new System.Drawing.Point(110, 101);
            this.CboLeaveType.Name = "CboLeaveType";
            this.CboLeaveType.Size = new System.Drawing.Size(271, 21);
            this.CboLeaveType.TabIndex = 3;
            this.CboLeaveType.SelectionChangeCommitted += new System.EventHandler(this.CboLeaveType_SelectionChangeCommitted);
            this.CboLeaveType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            this.CboLeaveType.TextChanged += new System.EventHandler(this.CboLeaveType_TextChanged);
            // 
            // EmployeeComboBox
            // 
            this.EmployeeComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.EmployeeComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.EmployeeComboBox.BackColor = System.Drawing.SystemColors.Info;
            this.EmployeeComboBox.DropDownHeight = 134;
            this.EmployeeComboBox.FormattingEnabled = true;
            this.EmployeeComboBox.IntegralHeight = false;
            this.EmployeeComboBox.Location = new System.Drawing.Point(110, 48);
            this.EmployeeComboBox.Name = "EmployeeComboBox";
            this.EmployeeComboBox.Size = new System.Drawing.Size(271, 21);
            this.EmployeeComboBox.TabIndex = 1;
            this.EmployeeComboBox.SelectedIndexChanged += new System.EventHandler(this.EmployeeComboBox_SelectedIndexChanged);
            this.EmployeeComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            this.EmployeeComboBox.TextChanged += new System.EventHandler(this.EmployeeComboBox_TextChanged);
            // 
            // MainPanel
            // 
            this.MainPanel.Controls.Add(this.DtpMonthYear);
            this.MainPanel.Controls.Add(this.lblPeriod);
            this.MainPanel.Controls.Add(this.cboPeriod);
            this.MainPanel.Controls.Add(this.Label3);
            this.MainPanel.Controls.Add(this.ReasonTextBox);
            this.MainPanel.Controls.Add(this.Label6);
            this.MainPanel.Controls.Add(this.lblBal);
            this.MainPanel.Controls.Add(this.lblExtensionId);
            this.MainPanel.Controls.Add(this.TxtBalLeave);
            this.MainPanel.Controls.Add(this.lblMY);
            this.MainPanel.Controls.Add(this.lblTimeDay);
            this.MainPanel.Controls.Add(this.CboLeaveType);
            this.MainPanel.Controls.Add(this.EmployeeComboBox);
            this.MainPanel.Controls.Add(this.NoOfDaysOrMinuteTextBox);
            this.MainPanel.Controls.Add(this.ExtensionTypeComboBox);
            this.MainPanel.Controls.Add(this.Label4);
            this.MainPanel.Controls.Add(this.Label2);
            this.MainPanel.Controls.Add(this.ShapeContainer2);
            this.MainPanel.Location = new System.Drawing.Point(7, 56);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(390, 250);
            this.MainPanel.TabIndex = 0;
            this.MainPanel.TabStop = false;
            // 
            // DtpMonthYear
            // 
            this.DtpMonthYear.AllowDrop = true;
            this.DtpMonthYear.CustomFormat = "dd-MMM-yyyy";
            this.DtpMonthYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpMonthYear.Location = new System.Drawing.Point(110, 75);
            this.DtpMonthYear.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DtpMonthYear.Name = "DtpMonthYear";
            this.DtpMonthYear.Size = new System.Drawing.Size(127, 20);
            this.DtpMonthYear.TabIndex = 2;
            this.DtpMonthYear.ValueChanged += new System.EventHandler(this.DtpMonthYear_ValueChanged);
            // 
            // lblPeriod
            // 
            this.lblPeriod.AutoSize = true;
            this.lblPeriod.Location = new System.Drawing.Point(8, 78);
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Size = new System.Drawing.Size(37, 13);
            this.lblPeriod.TabIndex = 206;
            this.lblPeriod.Text = "Period";
            this.lblPeriod.Visible = false;
            // 
            // cboPeriod
            // 
            this.cboPeriod.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboPeriod.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboPeriod.BackColor = System.Drawing.SystemColors.Info;
            this.cboPeriod.DropDownHeight = 134;
            this.cboPeriod.FormattingEnabled = true;
            this.cboPeriod.IntegralHeight = false;
            this.cboPeriod.Location = new System.Drawing.Point(110, 75);
            this.cboPeriod.Name = "cboPeriod";
            this.cboPeriod.Size = new System.Drawing.Size(271, 21);
            this.cboPeriod.TabIndex = 2;
            this.cboPeriod.Visible = false;
            this.cboPeriod.SelectedIndexChanged += new System.EventHandler(this.cboPeriod_SelectedIndexChanged);
            this.cboPeriod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            this.cboPeriod.TextChanged += new System.EventHandler(this.cboPeriod_TextChanged);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(7, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(88, 13);
            this.Label3.TabIndex = 19;
            this.Label3.Text = "Extension Info";
            // 
            // ReasonTextBox
            // 
            this.ReasonTextBox.Location = new System.Drawing.Point(10, 180);
            this.ReasonTextBox.MaxLength = 500;
            this.ReasonTextBox.Multiline = true;
            this.ReasonTextBox.Name = "ReasonTextBox";
            this.ReasonTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ReasonTextBox.Size = new System.Drawing.Size(371, 60);
            this.ReasonTextBox.TabIndex = 5;
            this.ReasonTextBox.TextChanged += new System.EventHandler(this.ReasonTextBox_TextChanged);
            // 
            // NoOfDaysOrMinuteTextBox
            // 
            this.NoOfDaysOrMinuteTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.NoOfDaysOrMinuteTextBox.Location = new System.Drawing.Point(110, 128);
            this.NoOfDaysOrMinuteTextBox.MaxLength = 5;
            this.NoOfDaysOrMinuteTextBox.Name = "NoOfDaysOrMinuteTextBox";
            this.NoOfDaysOrMinuteTextBox.Size = new System.Drawing.Size(86, 20);
            this.NoOfDaysOrMinuteTextBox.TabIndex = 4;
            this.NoOfDaysOrMinuteTextBox.TextChanged += new System.EventHandler(this.NoOfDaysOrMinuteTextBox_TextChanged);
            this.NoOfDaysOrMinuteTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NoOfDaysOrMinuteTextBox_KeyPress);
            // 
            // ExtensionTypeComboBox
            // 
            this.ExtensionTypeComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ExtensionTypeComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExtensionTypeComboBox.BackColor = System.Drawing.SystemColors.Info;
            this.ExtensionTypeComboBox.DropDownHeight = 134;
            this.ExtensionTypeComboBox.FormattingEnabled = true;
            this.ExtensionTypeComboBox.IntegralHeight = false;
            this.ExtensionTypeComboBox.Location = new System.Drawing.Point(110, 21);
            this.ExtensionTypeComboBox.Name = "ExtensionTypeComboBox";
            this.ExtensionTypeComboBox.Size = new System.Drawing.Size(271, 21);
            this.ExtensionTypeComboBox.TabIndex = 0;
            this.ExtensionTypeComboBox.SelectionChangeCommitted += new System.EventHandler(this.ExtensionTypeComboBox_SelectionChangeCommitted);
            this.ExtensionTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.ExtensionTypeComboBox_SelectedIndexChanged);
            this.ExtensionTypeComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            this.ExtensionTypeComboBox.TextChanged += new System.EventHandler(this.ExtensionTypeComboBox_TextChanged);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(8, 51);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(53, 13);
            this.Label4.TabIndex = 10;
            this.Label4.Text = "Employee";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(8, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(64, 13);
            this.Label2.TabIndex = 8;
            this.Label2.Text = "Leave Type";
            // 
            // ShapeContainer2
            // 
            this.ShapeContainer2.Location = new System.Drawing.Point(3, 16);
            this.ShapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer2.Name = "ShapeContainer2";
            this.ShapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.ShapeContainer2.Size = new System.Drawing.Size(384, 231);
            this.ShapeContainer2.TabIndex = 18;
            this.ShapeContainer2.TabStop = false;
            // 
            // LineShape1
            // 
            this.LineShape1.BorderColor = System.Drawing.Color.Silver;
            this.LineShape1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 38;
            this.LineShape1.X2 = 380;
            this.LineShape1.Y1 = 148;
            this.LineShape1.Y2 = 148;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnCancel.Location = new System.Drawing.Point(322, 312);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 3;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // RptLeaveExtenstionSummaryBindingSource
            // 
            this.RptLeaveExtenstionSummaryBindingSource.DataMember = "RptLeaveExtenstionSummary";
            // 
            // OKSaveButton
            // 
            this.OKSaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OKSaveButton.Enabled = false;
            this.OKSaveButton.Location = new System.Drawing.Point(239, 312);
            this.OKSaveButton.Name = "OKSaveButton";
            this.OKSaveButton.Size = new System.Drawing.Size(75, 23);
            this.OKSaveButton.TabIndex = 2;
            this.OKSaveButton.Text = "&Ok";
            this.OKSaveButton.UseVisualStyleBackColor = true;
            this.OKSaveButton.Click += new System.EventHandler(this.OKSaveButton_Click);
            // 
            // ErrorProviderLeave
            // 
            this.ErrorProviderLeave.ContainerControl = this;
            this.ErrorProviderLeave.RightToLeft = true;
            // 
            // TmStripClear
            // 
            this.TmStripClear.Interval = 3000;
            this.TmStripClear.Tick += new System.EventHandler(this.TmStripClear_Tick);
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.BackColor = System.Drawing.Color.White;
            this.BindingNavigatorPositionItem.Enabled = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.ReadOnly = true;
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigator
            // 
            this.BindingNavigator.AddNewItem = null;
            this.BindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.BindingNavigator.DeleteItem = null;
            this.BindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.CancelToolStripButton,
            this.ToolStripSeparator1,
            this.BtnPrint,
            this.BtnEmail,
            this.ToolStripSeparator2,
            this.BtnHelp});
            this.BindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.BindingNavigator.MoveFirstItem = null;
            this.BindingNavigator.MoveLastItem = null;
            this.BindingNavigator.MoveNextItem = null;
            this.BindingNavigator.MovePreviousItem = null;
            this.BindingNavigator.Name = "BindingNavigator";
            this.BindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.BindingNavigator.Size = new System.Drawing.Size(404, 25);
            this.BindingNavigator.TabIndex = 107;
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.ToolTipText = "Add New Company";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Remove";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // CancelToolStripButton
            // 
            this.CancelToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CancelToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CancelToolStripButton.Image")));
            this.CancelToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CancelToolStripButton.Name = "CancelToolStripButton";
            this.CancelToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.CancelToolStripButton.Text = "Clear";
            this.CancelToolStripButton.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print/Fax";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel2,
            this.ToolStripStatusLabel1});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 342);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(404, 22);
            this.StatusStrip1.TabIndex = 110;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // ToolStripStatusLabel2
            // 
            this.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2";
            this.ToolStripStatusLabel2.Size = new System.Drawing.Size(45, 17);
            this.ToolStripStatusLabel2.Text = "Status :";
            // 
            // ToolStripStatusLabel1
            // 
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // BtnSave
            // 
            this.BtnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSave.Location = new System.Drawing.Point(7, 312);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 1;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(404, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 111;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyBooksERP.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // ReceivedAmt
            // 
            this.ReceivedAmt.HeaderText = "Received Amt";
            this.ReceivedAmt.Name = "ReceivedAmt";
            this.ReceivedAmt.ReadOnly = true;
            // 
            // PaymentAmt
            // 
            this.PaymentAmt.HeaderText = "Payment Amt";
            this.PaymentAmt.Name = "PaymentAmt";
            // 
            // InvoiceAmt
            // 
            this.InvoiceAmt.HeaderText = "Invoice Amt";
            this.InvoiceAmt.Name = "InvoiceAmt";
            this.InvoiceAmt.ReadOnly = true;
            // 
            // InvoiceNo
            // 
            this.InvoiceNo.HeaderText = "InvoiceNo";
            this.InvoiceNo.Name = "InvoiceNo";
            this.InvoiceNo.ReadOnly = true;
            this.InvoiceNo.Width = 150;
            // 
            // SlNo
            // 
            this.SlNo.HeaderText = "SlNo";
            this.SlNo.Name = "SlNo";
            this.SlNo.ReadOnly = true;
            // 
            // FrmLeaveExtension
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 364);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.MainPanel);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.OKSaveButton);
            this.Controls.Add(this.BindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLeaveExtension";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Leave Extension";
            this.Load += new System.EventHandler(this.FrmLeaveExtension_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmLeaveExtension_KeyDown);
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RptLeaveExtenstionSummaryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderLeave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator)).EndInit();
            this.BindingNavigator.ResumeLayout(false);
            this.BindingNavigator.PerformLayout();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Label6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn FullPayment;
        private System.Windows.Forms.Label lblBal;
        private System.Windows.Forms.Label lblExtensionId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceivedAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn PaymentAmt;
        internal System.Windows.Forms.TextBox TxtBalLeave;
        private System.Windows.Forms.Label lblMY;
        private System.Windows.Forms.Label lblTimeDay;
        private System.Windows.Forms.ComboBox CboLeaveType;
        private System.Windows.Forms.ComboBox EmployeeComboBox;
        internal System.Windows.Forms.GroupBox MainPanel;
        private System.Windows.Forms.Label lblPeriod;
        internal System.Windows.Forms.ComboBox cboPeriod;
        private System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox ReasonTextBox;
        internal System.Windows.Forms.DateTimePicker DtpMonthYear;
        internal System.Windows.Forms.TextBox NoOfDaysOrMinuteTextBox;
        private System.Windows.Forms.ComboBox ExtensionTypeComboBox;
        private System.Windows.Forms.Label Label4;
        private System.Windows.Forms.Label Label2;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer2;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvoiceAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvoiceNo;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.BindingSource RptLeaveExtenstionSummaryBindingSource;
        internal System.Windows.Forms.Button OKSaveButton;
        internal System.Windows.Forms.ErrorProvider ErrorProviderLeave;
        internal System.Windows.Forms.BindingNavigator BindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton CancelToolStripButton;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.Timer TmStripClear;
        private System.Windows.Forms.DataGridViewTextBoxColumn SlNo;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel2;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel1;
        internal System.Windows.Forms.Button BtnSave;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
    }
}