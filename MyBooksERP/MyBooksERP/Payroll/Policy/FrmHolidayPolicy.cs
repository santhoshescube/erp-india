﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{
    /*****************************************************
   * Created By       : Arun
   * Creation Date    : 10 Apr 2012
   * Description      : Handle Holiday Policy
   * ***************************************************/
    public partial class FrmHolidayPolicy : Form
    {

        #region Declartions
        public int PintHolidayPolicyID = 0;   // From Refernce Form 
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission

        private string MstrCommonMessage;
        private bool MblnIsEditMode = false;
        private int MintRecordCnt = 0;
        private int MintCurrentRecCnt = 0;
        private bool MblnChangeStatus = false;
        private bool MblbtnOk = false;

        private clsBLLHolidayPolicy MobjclsBLLHolidayPolicy = null;
        private clsMessage ObjUserMessage = null;
        string strBindingOf = "Of ";
        #endregion Declartions
        /// <summary>
        /// FORM ID=112
        /// Constructor
        /// </summary>
        #region Constructor
        public FrmHolidayPolicy()
        {
            InitializeComponent();

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.HolidayPolicy, this);

        //    strBindingOf = "من ";
        //    dgvHolidayDetails.Columns["colHolidayParticulars"].HeaderText = "تفاصيل";
        //}
        #endregion Constructor
        #region Properties
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.HolidayPolicy);
                return this.ObjUserMessage;
            }
        }
        private clsBLLHolidayPolicy BLLHolidayPolicy
        {
            get
            {
                if (this.MobjclsBLLHolidayPolicy == null)
                    this.MobjclsBLLHolidayPolicy = new clsBLLHolidayPolicy();

                return this.MobjclsBLLHolidayPolicy;
            }
        }
        #endregion Properties

        #region Methods

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.HolidayPolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }

        private void ClearAllControls()
        {
            this.txtPolicyName.Text = "";
            this.txtPolicyName.Tag = 0;
            this.rdbCompanyBased.Checked = true;
            this.rdbActualMonth.Checked = false;
            this.cboCalculationBased.SelectedIndex = -1;
            this.cboCalculationBased.Text = "";
            this.dgvHolidayDetails.Rows.Clear();
            this.chkRatePerDay.Checked = false;
            this.chkRateOnly.Checked = false;
            this.txtHoursPerDay.Text = "";
            this.txtRate.Text = "";
            this.chkHourBasedShift.Checked = false;
            this.txtCalculationPercent.Text = "";
        }

        private void Changestatus()
        {
            //function for changing status


            if (!MblnIsEditMode)
            {
                btnOk.Enabled = MblnAddPermission;
                btnSave.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
            else
            {
                btnOk.Enabled = MblnUpdatePermission;
                btnSave.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            errHolidayPolicy.Clear();
            MblnChangeStatus = true;

        }
        private bool LoadCombos(int intType)
        {
            bool blnRetvalue = false;
            DataTable datCombos = new DataTable();
            try
            {

                if (intType == 0 || intType == 1)//CAlculation type
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    datCombos = BLLHolidayPolicy.FillCombos(new string[] { "CalculationID,CalculationArb AS Calculation", "PayCalculationReference", "CalculationID in(1,2)" });
                    //else
                        datCombos = BLLHolidayPolicy.FillCombos(new string[] { "CalculationID,Calculation", "PayCalculationReference", "CalculationID in(1,2)" });
                    cboCalculationBased.ValueMember = "CalculationID";
                    cboCalculationBased.DisplayMember = "Calculation";
                    cboCalculationBased.DataSource = datCombos;
                    blnRetvalue = true;
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
                blnRetvalue = false;
            }
            return blnRetvalue;
        }
        private void FillAdditionDeductionsGrid(DataGridView Dgv, DataTable dtAddDedDetails)
        {
            Dgv.Rows.Clear();
            if (dtAddDedDetails != null)
            {
                if (dtAddDedDetails.Rows.Count > 0)
                {
                    for (int iCounter = 0; iCounter <= dtAddDedDetails.Rows.Count - 1; iCounter++)
                    {
                        Dgv.RowCount = Dgv.RowCount + 1;
                        Dgv.Rows[iCounter].Cells[0].Value = dtAddDedDetails.Rows[iCounter]["AddDedID"].ToInt32();//Addition deduction Policy
                        Dgv.Rows[iCounter].Cells[1].Value = dtAddDedDetails.Rows[iCounter]["Checked"].ToBoolean(); ;//ChkBox
                        Dgv.Rows[iCounter].Cells[2].Value = dtAddDedDetails.Rows[iCounter]["Description"].ToStringCustom();//Description
                    }
                }
            }


        }

        private void GetRecordCount()
        {
            MintRecordCnt = 0;
            MintRecordCnt = BLLHolidayPolicy.GetRecordCount();
            if (MintRecordCnt < 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                MintRecordCnt = 0;
            }

        }
        private void RefernceDisplay()
        {

            int intRowNum = 0;
            intRowNum = BLLHolidayPolicy.GetRowNumber(PintHolidayPolicyID);
            if (intRowNum > 0)
            {
                MintCurrentRecCnt = intRowNum;
                DisplayHolidayPolicyInfo();
            }

        }
        private void AddNewHolidayPolicy()
        {
            MblnChangeStatus = false;
            MblnIsEditMode = false;
            lblstatus.Text = "";
            tmrClear.Enabled = true;
            errHolidayPolicy.Clear();

            ClearAllControls();  //Clear all controls in the form
            GetRecordCount();
            MintRecordCnt = MintRecordCnt + 1;
            MintCurrentRecCnt = MintRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();

            BindingNavigatorAddNewItem.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = false;
            btnPrint.Enabled = false;
            btnEmail.Enabled = false;
            btnOk.Enabled = false;
            btnSave.Enabled = false;
            BindingNavigatorSaveItem.Enabled = false;
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;

            txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtRate.Enabled = chkRateOnly.Checked ? true : false;

            txtHoursPerDay.BackColor = chkRatePerDay.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtHoursPerDay.Enabled = chkRatePerDay.Checked ? true : false;
            btnClear.Enabled = MblnAddPermission;

        }
        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
        }
        private void DisplayHolidayPolicyInfo()
        {

            FillHolidayPolicyInfo();

            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            btnPrint.Enabled = MblnPrintEmailPermission;
            btnEmail.Enabled = MblnPrintEmailPermission;
            BindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            MblnIsEditMode = true;
            SetBindingNavigatorButtons();
            MblnChangeStatus = false;
            BindingNavigatorSaveItem.Enabled = MblnChangeStatus;
            btnOk.Enabled = MblnChangeStatus;
            btnSave.Enabled = MblnChangeStatus;

            txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtRate.Enabled = chkRateOnly.Checked ? true : false;

            txtHoursPerDay.BackColor = chkRatePerDay.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtHoursPerDay.Enabled = chkRatePerDay.Checked ? true : false;
            btnClear.Enabled = false;
        }
        private void FillHolidayPolicyInfo()
        {
            ClearAllControls();
            DataTable dtHolidayPolicy = BLLHolidayPolicy.DisplayHolidayPolicy(MintCurrentRecCnt);
            
            if (dtHolidayPolicy.Rows.Count > 0)
            {
                    txtPolicyName.Text = dtHolidayPolicy.Rows[0]["HolidayPolicy"].ToStringCustom();
                    txtPolicyName.Tag = dtHolidayPolicy.Rows[0]["HolidayPolicyID"].ToInt32();

                    cboCalculationBased.SelectedValue = dtHolidayPolicy.Rows[0]["CalculationID"].ToInt32();

                    if (dtHolidayPolicy.Rows[0]["IsCompanyBasedOn"].ToInt32() == 1)
                    {
                        rdbCompanyBased.Checked = true;
                    }
                    else 
                    {
                        rdbActualMonth.Checked = true;
                    }
                    //else
                    //{

                    //    rdbWorkingDays.Checked = true;
                    //}
                    txtCalculationPercent.Text = dtHolidayPolicy.Rows[0]["CalculationPercentage"].ToString(); 
                    chkHourBasedShift.Checked = dtHolidayPolicy.Rows[0]["IsHourBasedShift"].ToBoolean();
                    chkRateOnly.Checked = dtHolidayPolicy.Rows[0]["IsRateOnly"].ToBoolean();
                    txtRate.Text = dtHolidayPolicy.Rows[0]["HolidayRate"].ToDecimal().ToStringCustom();
                    chkRatePerDay.Checked = dtHolidayPolicy.Rows[0]["IsRateBasedOnHour"].ToBoolean();
                    txtHoursPerDay.Text = dtHolidayPolicy.Rows[0]["HoursPerDay"].ToStringCustom();
                    if (dtHolidayPolicy.Rows[0]["CalculationID"].ToInt32() == (int)CalculationType.GrossSalary)
                    {
                        DataTable DtAddDedRef = BLLHolidayPolicy.DisplayAddDedDetails(txtPolicyName.Tag.ToInt32(),(int)PolicyType.Holiday);
                        FillAdditionDeductionsGrid(dgvHolidayDetails, DtAddDedRef);
                    }

                    //if (ClsCommonSettings.IsAmountRoundByZero)
                    //{
                    //    txtRate.Text = txtRate.Text.ToDecimal().ToString("F" + 0);
                    //    txtHoursPerDay.Text = txtHoursPerDay.Text.ToDecimal().ToString("F" + 0);
                    //}


            }
            if (chkHourBasedShift.Checked == true)
            {
                txtHoursPerDay.Text = "0";
                chkRatePerDay.Checked = false;
                txtHoursPerDay.Enabled = false;
            }
           


        }
        private bool FormValidation()
        {

            errHolidayPolicy.Clear();
            Control control = null;
            bool blnReturnValue = true;


            if (chkHourBasedShift.Checked == true)
            {
                txtHoursPerDay.Text = "0";
                chkRatePerDay.Checked = false;
                txtHoursPerDay.Enabled = false;
            }
     

            if (txtPolicyName.Text.Trim().Length == 0)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10000);
                control = txtPolicyName;
                blnReturnValue = false;
            }
            else if (chkRateOnly.Checked == false && cboCalculationBased.SelectedValue.ToInt32() == 0)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10001);
                control = cboCalculationBased;
                blnReturnValue = false;
            }
            else if (chkHourBasedShift.Checked == false && chkRatePerDay.Checked == true && (txtHoursPerDay.Text.Trim().Length == 0 ? 0.ToDecimal() : txtHoursPerDay.Text.ToDecimal()) == 0.ToDecimal())
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10008);
                control = txtHoursPerDay;
                blnReturnValue = false;
            }
            else if (chkRatePerDay.Checked == true && (txtHoursPerDay.Text.Trim().Length == 0 ? 0.ToDecimal() : txtHoursPerDay.Text.ToDecimal()) > 24.ToDecimal())
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10008);
                control = txtHoursPerDay;
                blnReturnValue = false;
            }
            else if (chkRateOnly.Checked && (txtRate.Text.Trim().Length == 0 || (txtRate.Text.ToDecimal()==0)))
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10002);
                control = txtRate;
                blnReturnValue = false;
            }
            else if (chkRateOnly.Checked ==false && (txtCalculationPercent.Text.Trim() == "" ||txtCalculationPercent.Text.ToDecimal ()==0 ))
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10004);
                control = txtCalculationPercent;
                blnReturnValue = false;
            }
            else if (chkRateOnly.Checked ==false  && txtCalculationPercent.Text.Trim().ToDecimal() <=0)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10004);
                control = txtCalculationPercent;
                blnReturnValue = false;
            }
            else if (txtRate.Text.Trim().Length > 0)
            {
                try
                {
                    decimal rate = Convert.ToDecimal(txtRate.Text);
                }
                catch
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(10002);
                    control = txtRate;
                    blnReturnValue = false;
                }
            }


            if (blnReturnValue == false)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrClear.Enabled = true;
                if (control != null)
                {
                    errHolidayPolicy.SetError(control, MstrCommonMessage);
                    control.Focus();
                }
                return blnReturnValue;
            }
            if (blnReturnValue)
            {
                if (BLLHolidayPolicy.CheckDuplicate(txtPolicyName.Tag.ToInt32(), txtPolicyName.Text.Trim()))//CheckDuplicate()
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(10009);
                    control = txtPolicyName;
                    blnReturnValue = false;
                }
            }
            if (blnReturnValue == false)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrClear.Enabled = true;
                if (control != null)
                {
                    errHolidayPolicy.SetError(control, MstrCommonMessage);
                    control.Focus();
                }
            }

            return blnReturnValue;
        }
        private bool DeleteValidation()
        {
            if (txtPolicyName.Tag.ToInt32() == 0)
            {
                UserMessage.ShowMessage(9015);
                return false;

            }
            if (BLLHolidayPolicy.PolicyIDExists(txtPolicyName.Tag.ToInt32()))
            {
                //if (ClsCommonSettings.IsArabicView)
                //    MessageBox.Show(" التفاصيل موجودة في النظام. لا يمكن حذف", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //else
                    MessageBox.Show("Details exists in the system.Cannot delete", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (UserMessage.ShowMessage(10010) == false)
            {
                return false;
            }
            return true;

        }
        private void FillParameterMaster()
        {
            BLLHolidayPolicy.DTOHolidayPolicy.HolidayPolicyID = txtPolicyName.Tag.ToInt32();
            BLLHolidayPolicy.DTOHolidayPolicy.HolidayPolicy = txtPolicyName.Text.Trim();

            BLLHolidayPolicy.DTOHolidayPolicy.CalculationID = cboCalculationBased.SelectedValue.ToInt32();
            if (rdbCompanyBased.Checked == true)
            {
                BLLHolidayPolicy.DTOHolidayPolicy.CompanyBasedOn = 1;
            }
            else if (rdbActualMonth.Checked == true)
            {
                BLLHolidayPolicy.DTOHolidayPolicy.CompanyBasedOn = 2;
            }
            else
            {
                BLLHolidayPolicy.DTOHolidayPolicy.CompanyBasedOn = 3;
            }

            if (txtCalculationPercent.Text.Trim() != "")
            {
                BLLHolidayPolicy.DTOHolidayPolicy.CalculationPercentage = txtCalculationPercent.Text.ToDecimal();
            }
           
            BLLHolidayPolicy.DTOHolidayPolicy.IsRateOnly = chkRateOnly.Checked;
            BLLHolidayPolicy.DTOHolidayPolicy.HolidayRate = chkRateOnly.Checked ? txtRate.Text.Trim().ToDecimal() : 0;

            BLLHolidayPolicy.DTOHolidayPolicy.IsRateBasedOnHour = chkRatePerDay.Checked;
            BLLHolidayPolicy.DTOHolidayPolicy.HoursPerDay = chkRatePerDay.Checked ? txtHoursPerDay.Text.Trim().ToDecimal() : 0;
            BLLHolidayPolicy.DTOHolidayPolicy.IsHourBasedShift = chkHourBasedShift.Checked;

        }
      
        private void FillParameterSalPolicyDetails()
        {
            BLLHolidayPolicy.DTOHolidayPolicy.DTOSalaryPolicyDetailHoliday = new List<clsDTOSalaryPolicyDetailHoliday>();

            if (dgvHolidayDetails.Rows.Count > 0)
            {
                dgvHolidayDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                dgvHolidayDetails.CurrentCell = dgvHolidayDetails["colHolidayParticulars", 0];
                foreach (DataGridViewRow row in dgvHolidayDetails.Rows)
                {
                    if (Convert.ToBoolean(row.Cells["colHolidaySelectedItem"].Value) == true)
                    {
                        clsDTOSalaryPolicyDetailHoliday objSalaryPolicyDetail = new clsDTOSalaryPolicyDetailHoliday();
                        objSalaryPolicyDetail.PolicyType = (int)PolicyType.Holiday;
                        objSalaryPolicyDetail.AdditionDeductionID = row.Cells["colHolidayAddDedID"].Value.ToInt32();
                        BLLHolidayPolicy.DTOHolidayPolicy.DTOSalaryPolicyDetailHoliday.Add(objSalaryPolicyDetail);
                    }
                }
            }
          

        }
        private bool SaveHolidayPolicy()
        {
            bool blnRetValue = false;

            if (FormValidation())
            {
                int intMessageCode = 0;
                if (txtPolicyName.Tag.ToInt32() > 0)
                    intMessageCode = 3;
                else
                    intMessageCode = 1;

                if (UserMessage.ShowMessage(intMessageCode))
                {
                    FillParameterMaster();
                    FillParameterSalPolicyDetails();
                    if (BLLHolidayPolicy.HolidayPolicyMasterSave())
                    {
                        if (txtPolicyName.Tag.ToInt32() > 0)
                        {
                            lblstatus.Text = UserMessage.GetMessageByCode(21);
                            UserMessage.ShowMessage(21);
                        }
                        else
                        {
                            lblstatus.Text = UserMessage.GetMessageByCode(2);
                            UserMessage.ShowMessage(2);

                        }

                     

                        blnRetValue = true;
                        txtPolicyName.Tag = BLLHolidayPolicy.DTOHolidayPolicy.HolidayPolicyID;
                        PintHolidayPolicyID = BLLHolidayPolicy.DTOHolidayPolicy.HolidayPolicyID;
                        BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                        btnEmail.Enabled = MblnPrintEmailPermission;
                        btnPrint.Enabled = MblnPrintEmailPermission;
                        BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                        btnClear.Enabled = false;
                    }

                }
            }

            return blnRetValue;
        }

        private bool DeleteHolidayPolicy()
        {
            bool blnRetValue = false;
            if (DeleteValidation())
            {
                BLLHolidayPolicy.DTOHolidayPolicy.HolidayPolicyID = txtPolicyName.Tag.ToInt32();
                if (BLLHolidayPolicy.DeleteHolidayPolicy())
                {
                    AddNewHolidayPolicy();
                    blnRetValue = true;
                }

            }
            return blnRetValue;

        }
        private void ResetForm(object sender, System.EventArgs e)
        {
            if (sender is CheckBox)
            {
                CheckBox chk = (CheckBox)sender;
                if (chk.Name == chkRateOnly.Name)
                {
                    txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
                    txtRate.Enabled = chkRateOnly.Checked ? true : false;

                    cboCalculationBased.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Window : System.Drawing.SystemColors.Info;
                    if (chkRateOnly.Checked)
                    {
                        cboCalculationBased.SelectedIndex = -1;
                        txtCalculationPercent.Enabled = false;
                    }
                    else
                    {
                        cboCalculationBased.SelectedIndex = 0;
                        txtRate.Text = "";
                        txtCalculationPercent.Enabled = true;
                    }
                    cboCalculationBased.Enabled = chkRateOnly.Checked ? false : true;
                    rdbCompanyBased.Enabled = chkRateOnly.Checked ? false : true;
                    rdbActualMonth.Enabled = chkRateOnly.Checked ? false : true;
                    //rdbWorkingDays.Enabled = chkRateOnly.Checked ? false : true;
                    txtHoursPerDay.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
                    txtCalculationPercent.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Window : System.Drawing.SystemColors.Info;
                    if (chkRateOnly.Checked)
                        chkRatePerDay.Checked = false;
                    txtHoursPerDay.Enabled = !txtRate.Enabled;
                }
                else if (chk.Name == chkRatePerDay.Name)
                {
                    if (chkRatePerDay.Checked)
                    {
                        chkRateOnly.Checked = false;
                    }
                    else
                        txtHoursPerDay.Text = "";

                }

                txtHoursPerDay.BackColor = chkRatePerDay.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
                txtHoursPerDay.Enabled = chkRatePerDay.Checked ? true : false;
            }
            Changestatus();



        }
        #endregion Methods
        #region Events

        private void FrmHolidayPolicy_Load(object sender, EventArgs e)
        {
            SetPermissions();
            LoadCombos(0);
            if (PintHolidayPolicyID > 0)
            {
                GetRecordCount();
                RefernceDisplay();
            }
            else
            {
                AddNewHolidayPolicy();
            }
            txtPolicyName.Focus();
            txtPolicyName.Select();
        }

        private void FrmHolidayPolicy_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnOk.Enabled)
            {
                if (UserMessage.ShowMessage(8))
                {
                    e.Cancel = false;
                }
                else
                { e.Cancel = true; }
            }
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;
                DisplayHolidayPolicyInfo();
                lblstatus.Text = UserMessage.GetMessageByCode(9);
                tmrClear.Enabled = true;
            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    DisplayHolidayPolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(10);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 1;
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                }

                if (MintCurrentRecCnt > MintRecordCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                }
                else
                {
                    DisplayHolidayPolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(11);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 0;
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    DisplayHolidayPolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(12);
                    tmrClear.Enabled = true;
                }
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewHolidayPolicy();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (DeleteHolidayPolicy())
            {
                lblstatus.Text = UserMessage.GetMessageByCode(4);
                tmrClear.Enabled = true;
                UserMessage.ShowMessage(4);
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            txtPolicyName.Focus();
            if (SaveHolidayPolicy())
            {
                
                btnOk.Enabled = false;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            AddNewHolidayPolicy();
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveHolidayPolicy())
            {
               
                btnOk.Enabled = false;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveHolidayPolicy())
            {
                MblbtnOk = true;
                btnOk.Enabled = false;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void rdbCompanyBased_CheckedChanged(object sender, EventArgs e)
        {
            ResetForm(sender, e);
        }

        private void chkRatePerDay_CheckedChanged(object sender, EventArgs e)
        {
            ResetForm(sender, e);
            if (chkRateOnly.Checked == true)
            {
                chkHourBasedShift.Checked = false;
            }
            chkHourBasedShift.Checked = false;
        }

        private void chkRateOnly_CheckedChanged(object sender, EventArgs e)
        {
            ResetForm(sender, e);
        }

        private void rdbShortBasedOnCompany_CheckedChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

       
        private void tmrClear_Tick(object sender, EventArgs e)
        {
            tmrClear.Enabled = false;
            lblstatus.Text = "";
        }

        private void txtDecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars;

            strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            //if (ClsCommonSettings.IsAmountRoundByZero)
            //{
            //    strInvalidChars = strInvalidChars + ".";
            //}

            e.Handled = false;
            if (((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))))
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }

        private void txtint_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[].";
            e.Handled = false;
            if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }

      

      
      
        private void txtPolicyName_TextChanged(object sender, EventArgs e)
        {
            ResetForm(sender, e);
            Changestatus();
        }

        private void dgvHolidayDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {

            }
            catch
            {
            }
        }

        private void dgvHolidayDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvHolidayDetails.IsCurrentCellDirty)
                {

                    dgvHolidayDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }

      
        private void dgvShortageDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Changestatus();
        }

        private void dgvHolidayDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Changestatus();
        }

        private void dgvShortageDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void cboCalculationBased_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Holiday Tab
            Changestatus();
            if (cboCalculationBased.SelectedValue.ToInt32() > 0)
            {
                dgvHolidayDetails.Enabled = true;

                if (cboCalculationBased.SelectedValue.ToInt32() == (int)CalculationType.GrossSalary)
                {
                    DataTable dtADDDedd = BLLHolidayPolicy.GetAdditionDeductions();
                    FillAdditionDeductionsGrid(dgvHolidayDetails, dtADDDedd);
                }
                else
                {
                    dgvHolidayDetails.Rows.Clear();
                    dgvHolidayDetails.Enabled = false;
                }
            }
            else
            {
                dgvHolidayDetails.Rows.Clear();
                dgvHolidayDetails.Enabled = false;
            }
        }

       

        private void cboCalculationBased_KeyDown(object sender, KeyEventArgs e)
        {
            cboCalculationBased.DroppedDown = false;
            if (e.KeyData == Keys.Enter)
                cboCalculationBased_SelectedIndexChanged(sender, new EventArgs());

        }

       
       
        #endregion Events

        private void chkHourBasedShift_CheckedChanged(object sender, EventArgs e)
        {
            if (chkHourBasedShift.Checked == true)
            {
                txtHoursPerDay.Text = "0";
                chkRatePerDay.Checked = false;
                txtHoursPerDay.Enabled = false;
            }
  
            Changestatus();
        }

        private void txtRate_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtCalculationPercent_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void dgvHolidayDetails_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            Changestatus();
        }

        private void dgvHolidayDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            Changestatus();
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
           
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "SalaryStructure";
            objHelp.ShowDialog();
            objHelp = null;
        }

 



    }
}
