﻿namespace MyBooksERP
{
    partial class FrmShiftPolicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmShiftPolicy));
            this.DescriptionTextBox = new System.Windows.Forms.TextBox();
            this.FromTimeDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.ToTimeDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.cboShiftType = new System.Windows.Forms.ComboBox();
            this.lblNoTimes = new System.Windows.Forms.Label();
            this.labelX7 = new System.Windows.Forms.Label();
            this.labelX6 = new System.Windows.Forms.Label();
            this.labelX5 = new System.Windows.Forms.Label();
            this.labelX4 = new System.Windows.Forms.Label();
            this.labelX3 = new System.Windows.Forms.Label();
            this.labelX2 = new System.Windows.Forms.Label();
            this.BtnSave = new System.Windows.Forms.Button();
            this.dtpMinWorkingHours = new System.Windows.Forms.DateTimePicker();
            this.ErrorProviderShift = new System.Windows.Forms.ErrorProvider(this.components);
            this.TmrComapny = new System.Windows.Forms.Timer(this.components);
            this.TmrFocus = new System.Windows.Forms.Timer(this.components);
            this.BtnOk = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.txtEarlyBefore = new DemoClsDataGridview.NumericTextBox();
            this.txtLateAfter = new DemoClsDataGridview.NumericTextBox();
            this.chkISBufferForOT = new System.Windows.Forms.CheckBox();
            this.labelX15 = new System.Windows.Forms.Label();
            this.labelX14 = new System.Windows.Forms.Label();
            this.labelX12 = new System.Windows.Forms.Label();
            this.txtMinTimeForOT = new DemoClsDataGridview.NumericTextBox();
            this.lblMinTimeForOT = new System.Windows.Forms.Label();
            this.txtBufferTime = new DemoClsDataGridview.NumericTextBox();
            this.lblBufferTime = new System.Windows.Forms.Label();
            this.txtAllowedBreakTime = new DemoClsDataGridview.NumericTextBox();
            this.chkIsMinWorkOT = new System.Windows.Forms.CheckBox();
            this.labelX9 = new System.Windows.Forms.Label();
            this.txtDuration = new System.Windows.Forms.TextBox();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabShiftDetails = new System.Windows.Forms.TabPage();
            this.DgvShift = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtpTimeFormat = new System.Windows.Forms.DateTimePicker();
            this.tabDynamicShiftDetails = new System.Windows.Forms.TabPage();
            this.dgvDynamicShift = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ShiftID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FromTime = new DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn();
            this.ToTime = new DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn();
            this.Duration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AllowedBreakTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MinWorkingHrs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BufferTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboShiftOTPolicyID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ActualDuration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboOTPolicy = new System.Windows.Forms.ComboBox();
            this.lblOTPolicy = new System.Windows.Forms.Label();
            this.numericTextBox2 = new DemoClsDataGridview.NumericTextBox();
            this.txtNoOfTimings = new DemoClsDataGridview.NumericTextBox();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.dtpHourFormat12 = new System.Windows.Forms.DateTimePicker();
            this.ShiftBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.ShiftReferenceBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.CancelToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblEarly = new System.Windows.Forms.Label();
            this.lblLate = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtWeekWrkHrs = new DemoClsDataGridview.NumericTextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderShift)).BeginInit();
            this.tabMain.SuspendLayout();
            this.tabShiftDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvShift)).BeginInit();
            this.tabDynamicShiftDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDynamicShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftBindingNavigator)).BeginInit();
            this.ShiftBindingNavigator.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.ssStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // DescriptionTextBox
            // 
            this.DescriptionTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.DescriptionTextBox.Location = new System.Drawing.Point(87, 17);
            this.DescriptionTextBox.MaxLength = 30;
            this.DescriptionTextBox.Name = "DescriptionTextBox";
            this.DescriptionTextBox.Size = new System.Drawing.Size(256, 20);
            this.DescriptionTextBox.TabIndex = 1;
            this.DescriptionTextBox.TextChanged += new System.EventHandler(this.DescriptionTextBox_TextChanged);
            // 
            // FromTimeDateTimePicker
            // 
            this.FromTimeDateTimePicker.CustomFormat = "HH:mm";
            this.FromTimeDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FromTimeDateTimePicker.Location = new System.Drawing.Point(87, 91);
            this.FromTimeDateTimePicker.Name = "FromTimeDateTimePicker";
            this.FromTimeDateTimePicker.ShowUpDown = true;
            this.FromTimeDateTimePicker.Size = new System.Drawing.Size(75, 20);
            this.FromTimeDateTimePicker.TabIndex = 3;
            this.FromTimeDateTimePicker.ValueChanged += new System.EventHandler(this.FromTimeDateTimePicker_ValueChanged);
            // 
            // ToTimeDateTimePicker
            // 
            this.ToTimeDateTimePicker.CustomFormat = "HH:mm";
            this.ToTimeDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ToTimeDateTimePicker.Location = new System.Drawing.Point(87, 117);
            this.ToTimeDateTimePicker.Name = "ToTimeDateTimePicker";
            this.ToTimeDateTimePicker.ShowUpDown = true;
            this.ToTimeDateTimePicker.Size = new System.Drawing.Size(75, 20);
            this.ToTimeDateTimePicker.TabIndex = 4;
            this.ToTimeDateTimePicker.ValueChanged += new System.EventHandler(this.ToTimeDateTimePicker_ValueChanged);
            // 
            // cboShiftType
            // 
            this.cboShiftType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboShiftType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboShiftType.BackColor = System.Drawing.SystemColors.Info;
            this.cboShiftType.DropDownHeight = 134;
            this.cboShiftType.FormattingEnabled = true;
            this.cboShiftType.IntegralHeight = false;
            this.cboShiftType.Location = new System.Drawing.Point(87, 43);
            this.cboShiftType.MaxDropDownItems = 10;
            this.cboShiftType.Name = "cboShiftType";
            this.cboShiftType.Size = new System.Drawing.Size(103, 21);
            this.cboShiftType.TabIndex = 2;
            this.cboShiftType.SelectedIndexChanged += new System.EventHandler(this.cboShiftType_SelectedIndexChanged);
            // 
            // lblNoTimes
            // 
            this.lblNoTimes.AutoSize = true;
            this.lblNoTimes.Location = new System.Drawing.Point(272, 49);
            this.lblNoTimes.Name = "lblNoTimes";
            this.lblNoTimes.Size = new System.Drawing.Size(74, 13);
            this.lblNoTimes.TabIndex = 43;
            this.lblNoTimes.Text = "No Of Timings";
            // 
            // labelX7
            // 
            this.labelX7.AutoSize = true;
            this.labelX7.Location = new System.Drawing.Point(272, 170);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(86, 13);
            this.labelX7.TabIndex = 42;
            this.labelX7.Text = "Min Working Hrs";
            // 
            // labelX6
            // 
            this.labelX6.Location = new System.Drawing.Point(13, 43);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(64, 23);
            this.labelX6.TabIndex = 41;
            this.labelX6.Text = "Shift Type";
            // 
            // labelX5
            // 
            this.labelX5.AutoSize = true;
            this.labelX5.Location = new System.Drawing.Point(13, 148);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(47, 13);
            this.labelX5.TabIndex = 40;
            this.labelX5.Text = "Duration";
            // 
            // labelX4
            // 
            this.labelX4.AutoSize = true;
            this.labelX4.Location = new System.Drawing.Point(13, 121);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(46, 13);
            this.labelX4.TabIndex = 39;
            this.labelX4.Text = "To Time";
            // 
            // labelX3
            // 
            this.labelX3.AutoSize = true;
            this.labelX3.Location = new System.Drawing.Point(13, 94);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(56, 13);
            this.labelX3.TabIndex = 38;
            this.labelX3.Text = "From Time";
            // 
            // labelX2
            // 
            this.labelX2.Location = new System.Drawing.Point(13, 15);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(62, 23);
            this.labelX2.TabIndex = 37;
            this.labelX2.Text = "Shift Name";
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(384, 244);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(73, 23);
            this.BtnSave.TabIndex = 13;
            this.BtnSave.Text = "&Save";
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // dtpMinWorkingHours
            // 
            this.dtpMinWorkingHours.CustomFormat = "HH:mm";
            this.dtpMinWorkingHours.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMinWorkingHours.Location = new System.Drawing.Point(382, 168);
            this.dtpMinWorkingHours.Name = "dtpMinWorkingHours";
            this.dtpMinWorkingHours.ShowUpDown = true;
            this.dtpMinWorkingHours.Size = new System.Drawing.Size(75, 20);
            this.dtpMinWorkingHours.TabIndex = 10;
            this.dtpMinWorkingHours.ValueChanged += new System.EventHandler(this.dtpMinWorkingHours_ValueChanged);
            // 
            // ErrorProviderShift
            // 
            this.ErrorProviderShift.ContainerControl = this;
            this.ErrorProviderShift.RightToLeft = true;
            // 
            // TmrComapny
            // 
            this.TmrComapny.Interval = 2000;
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(321, 476);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 1;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(402, 475);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 2;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // txtEarlyBefore
            // 
            this.txtEarlyBefore.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEarlyBefore.Location = new System.Drawing.Point(382, 116);
            this.txtEarlyBefore.MaxLength = 3;
            this.txtEarlyBefore.Name = "txtEarlyBefore";
            this.txtEarlyBefore.ShortcutsEnabled = false;
            this.txtEarlyBefore.Size = new System.Drawing.Size(75, 20);
            this.txtEarlyBefore.TabIndex = 7;
            this.txtEarlyBefore.TextChanged += new System.EventHandler(this.txtEarlyBefore_TextChanged);
            // 
            // txtLateAfter
            // 
            this.txtLateAfter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLateAfter.Location = new System.Drawing.Point(382, 90);
            this.txtLateAfter.MaxLength = 3;
            this.txtLateAfter.Name = "txtLateAfter";
            this.txtLateAfter.ShortcutsEnabled = false;
            this.txtLateAfter.Size = new System.Drawing.Size(75, 20);
            this.txtLateAfter.TabIndex = 8;
            this.txtLateAfter.TextChanged += new System.EventHandler(this.txtLateAfter_TextChanged);
            // 
            // chkISBufferForOT
            // 
            this.chkISBufferForOT.AutoSize = true;
            this.chkISBufferForOT.Location = new System.Drawing.Point(308, 223);
            this.chkISBufferForOT.Name = "chkISBufferForOT";
            this.chkISBufferForOT.Size = new System.Drawing.Size(160, 17);
            this.chkISBufferForOT.TabIndex = 1027;
            this.chkISBufferForOT.Text = "Consider Buffer Time For OT";
            this.chkISBufferForOT.UseVisualStyleBackColor = true;
            this.chkISBufferForOT.Visible = false;
            // 
            // labelX15
            // 
            this.labelX15.AutoSize = true;
            this.labelX15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX15.Location = new System.Drawing.Point(11, 479);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(183, 13);
            this.labelX15.TabIndex = 1038;
            this.labelX15.Text = "*** Time should be entered in minutes";
            // 
            // labelX14
            // 
            this.labelX14.AutoSize = true;
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            this.labelX14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX14.Location = new System.Drawing.Point(8, 200);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(109, 13);
            this.labelX14.TabIndex = 1037;
            this.labelX14.Text = "Over Time Criteria";
            // 
            // labelX12
            // 
            this.labelX12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX12.Location = new System.Drawing.Point(8, 68);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(85, 24);
            this.labelX12.TabIndex = 1035;
            this.labelX12.Text = "Shift Timings";
            // 
            // txtMinTimeForOT
            // 
            this.txtMinTimeForOT.Location = new System.Drawing.Point(147, 247);
            this.txtMinTimeForOT.MaxLength = 3;
            this.txtMinTimeForOT.Name = "txtMinTimeForOT";
            this.txtMinTimeForOT.ShortcutsEnabled = false;
            this.txtMinTimeForOT.Size = new System.Drawing.Size(54, 20);
            this.txtMinTimeForOT.TabIndex = 11;
            this.txtMinTimeForOT.TextChanged += new System.EventHandler(this.txtMinTimeForOT_TextChanged);
            // 
            // lblMinTimeForOT
            // 
            this.lblMinTimeForOT.AutoSize = true;
            this.lblMinTimeForOT.BackColor = System.Drawing.Color.Transparent;
            this.lblMinTimeForOT.Location = new System.Drawing.Point(13, 250);
            this.lblMinTimeForOT.Name = "lblMinTimeForOT";
            this.lblMinTimeForOT.Size = new System.Drawing.Size(128, 13);
            this.lblMinTimeForOT.TabIndex = 1032;
            this.lblMinTimeForOT.Text = "Minimum time for overtime";
            // 
            // txtBufferTime
            // 
            this.txtBufferTime.Location = new System.Drawing.Point(87, 169);
            this.txtBufferTime.MaxLength = 3;
            this.txtBufferTime.Name = "txtBufferTime";
            this.txtBufferTime.ShortcutsEnabled = false;
            this.txtBufferTime.Size = new System.Drawing.Size(75, 20);
            this.txtBufferTime.TabIndex = 6;
            this.txtBufferTime.TextChanged += new System.EventHandler(this.txtBufferTime_TextChanged);
            // 
            // lblBufferTime
            // 
            this.lblBufferTime.AutoSize = true;
            this.lblBufferTime.Location = new System.Drawing.Point(13, 175);
            this.lblBufferTime.Name = "lblBufferTime";
            this.lblBufferTime.Size = new System.Drawing.Size(61, 13);
            this.lblBufferTime.TabIndex = 1028;
            this.lblBufferTime.Text = "Buffer Time";
            // 
            // txtAllowedBreakTime
            // 
            this.txtAllowedBreakTime.Location = new System.Drawing.Point(382, 142);
            this.txtAllowedBreakTime.MaxLength = 3;
            this.txtAllowedBreakTime.Name = "txtAllowedBreakTime";
            this.txtAllowedBreakTime.ShortcutsEnabled = false;
            this.txtAllowedBreakTime.Size = new System.Drawing.Size(75, 20);
            this.txtAllowedBreakTime.TabIndex = 9;
            this.txtAllowedBreakTime.TextChanged += new System.EventHandler(this.txtAllowedBreakTime_TextChanged);
            // 
            // chkIsMinWorkOT
            // 
            this.chkIsMinWorkOT.AutoSize = true;
            this.chkIsMinWorkOT.Location = new System.Drawing.Point(16, 222);
            this.chkIsMinWorkOT.Name = "chkIsMinWorkOT";
            this.chkIsMinWorkOT.Size = new System.Drawing.Size(279, 17);
            this.chkIsMinWorkOT.TabIndex = 12;
            this.chkIsMinWorkOT.Text = "Over time calculation based on minimum working hour";
            this.chkIsMinWorkOT.UseVisualStyleBackColor = true;
            this.chkIsMinWorkOT.CheckedChanged += new System.EventHandler(this.checkBoxX1_CheckedChanged);
            // 
            // labelX9
            // 
            this.labelX9.AutoSize = true;
            this.labelX9.Location = new System.Drawing.Point(272, 144);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(101, 13);
            this.labelX9.TabIndex = 1020;
            this.labelX9.Text = "Allowed Break Time";
            // 
            // txtDuration
            // 
            this.txtDuration.BackColor = System.Drawing.SystemColors.Window;
            this.txtDuration.Enabled = false;
            this.txtDuration.Location = new System.Drawing.Point(87, 143);
            this.txtDuration.MaxLength = 10;
            this.txtDuration.Name = "txtDuration";
            this.txtDuration.Size = new System.Drawing.Size(75, 20);
            this.txtDuration.TabIndex = 5;
            this.txtDuration.TabStop = false;
            this.txtDuration.TextChanged += new System.EventHandler(this.txtDuration_TextChanged);
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabShiftDetails);
            this.tabMain.Controls.Add(this.tabDynamicShiftDetails);
            this.tabMain.Location = new System.Drawing.Point(7, 278);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(461, 163);
            this.tabMain.TabIndex = 45;
            // 
            // tabShiftDetails
            // 
            this.tabShiftDetails.Controls.Add(this.DgvShift);
            this.tabShiftDetails.Controls.Add(this.dtpTimeFormat);
            this.tabShiftDetails.Location = new System.Drawing.Point(4, 22);
            this.tabShiftDetails.Name = "tabShiftDetails";
            this.tabShiftDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tabShiftDetails.Size = new System.Drawing.Size(453, 137);
            this.tabShiftDetails.TabIndex = 0;
            this.tabShiftDetails.Text = "Shift Details";
            this.tabShiftDetails.UseVisualStyleBackColor = true;
            // 
            // DgvShift
            // 
            this.DgvShift.AllowUserToAddRows = false;
            this.DgvShift.AllowUserToDeleteRows = false;
            this.DgvShift.AllowUserToResizeColumns = false;
            this.DgvShift.AllowUserToResizeRows = false;
            this.DgvShift.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvShift.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.DgvShift.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvShift.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.CompanyID});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvShift.DefaultCellStyle = dataGridViewCellStyle8;
            this.DgvShift.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvShift.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvShift.Location = new System.Drawing.Point(3, 3);
            this.DgvShift.MultiSelect = false;
            this.DgvShift.Name = "DgvShift";
            this.DgvShift.ReadOnly = true;
            this.DgvShift.RowHeadersVisible = false;
            this.DgvShift.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvShift.Size = new System.Drawing.Size(447, 131);
            this.DgvShift.TabIndex = 0;
            this.DgvShift.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvShift_CellClick);
            // 
            // Column7
            // 
            this.Column7.HeaderText = "ShiftID";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column7.Visible = false;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Shift Name";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column8.Width = 125;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "From Time";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column9.Width = 60;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "To Time";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column10.Width = 60;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "Duration";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column11.Width = 60;
            // 
            // Column12
            // 
            this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column12.HeaderText = "Shift Type";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CompanyID
            // 
            this.CompanyID.HeaderText = "CompanyID";
            this.CompanyID.Name = "CompanyID";
            this.CompanyID.ReadOnly = true;
            this.CompanyID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CompanyID.Visible = false;
            // 
            // dtpTimeFormat
            // 
            this.dtpTimeFormat.CustomFormat = "HH:mm";
            this.dtpTimeFormat.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTimeFormat.Location = new System.Drawing.Point(243, 80);
            this.dtpTimeFormat.Name = "dtpTimeFormat";
            this.dtpTimeFormat.ShowUpDown = true;
            this.dtpTimeFormat.Size = new System.Drawing.Size(68, 20);
            this.dtpTimeFormat.TabIndex = 1015;
            // 
            // tabDynamicShiftDetails
            // 
            this.tabDynamicShiftDetails.Controls.Add(this.dgvDynamicShift);
            this.tabDynamicShiftDetails.Controls.Add(this.cboOTPolicy);
            this.tabDynamicShiftDetails.Controls.Add(this.lblOTPolicy);
            this.tabDynamicShiftDetails.Controls.Add(this.numericTextBox2);
            this.tabDynamicShiftDetails.Location = new System.Drawing.Point(4, 22);
            this.tabDynamicShiftDetails.Name = "tabDynamicShiftDetails";
            this.tabDynamicShiftDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tabDynamicShiftDetails.Size = new System.Drawing.Size(453, 137);
            this.tabDynamicShiftDetails.TabIndex = 1;
            this.tabDynamicShiftDetails.Text = "Split Details";
            this.tabDynamicShiftDetails.UseVisualStyleBackColor = true;
            // 
            // dgvDynamicShift
            // 
            this.dgvDynamicShift.AllowUserToAddRows = false;
            this.dgvDynamicShift.AllowUserToDeleteRows = false;
            this.dgvDynamicShift.AllowUserToResizeColumns = false;
            this.dgvDynamicShift.AllowUserToResizeRows = false;
            this.dgvDynamicShift.BackgroundColor = System.Drawing.Color.White;
            this.dgvDynamicShift.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDynamicShift.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ShiftID,
            this.OrderNo,
            this.FromTime,
            this.ToTime,
            this.Duration,
            this.AllowedBreakTime,
            this.MinWorkingHrs,
            this.BufferTime,
            this.cboShiftOTPolicyID,
            this.ActualDuration});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDynamicShift.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgvDynamicShift.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDynamicShift.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvDynamicShift.Location = new System.Drawing.Point(3, 3);
            this.dgvDynamicShift.MultiSelect = false;
            this.dgvDynamicShift.Name = "dgvDynamicShift";
            this.dgvDynamicShift.RowHeadersVisible = false;
            this.dgvDynamicShift.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDynamicShift.Size = new System.Drawing.Size(447, 131);
            this.dgvDynamicShift.TabIndex = 66;
            this.dgvDynamicShift.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvDynamicShift_CellValidating);
            this.dgvDynamicShift.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDynamicShift_CellClick);
            this.dgvDynamicShift.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvDynamicShift_EditingControlShowing);
            this.dgvDynamicShift.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvDynamicShift_CurrentCellDirtyStateChanged);
            this.dgvDynamicShift.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvDynamicShift_DataError);
            // 
            // ShiftID
            // 
            this.ShiftID.HeaderText = "ShiftID";
            this.ShiftID.Name = "ShiftID";
            this.ShiftID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ShiftID.Visible = false;
            this.ShiftID.Width = 50;
            // 
            // OrderNo
            // 
            this.OrderNo.HeaderText = "SlNo";
            this.OrderNo.Name = "OrderNo";
            this.OrderNo.ReadOnly = true;
            this.OrderNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.OrderNo.Width = 40;
            // 
            // FromTime
            // 
            // 
            // 
            // 
            this.FromTime.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.FromTime.BackgroundStyle.Class = "DataGridViewDateTimeBorder";
            this.FromTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.FromTime.BackgroundStyle.TextColor = System.Drawing.SystemColors.ControlText;
            this.FromTime.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.FromTime.HeaderText = "FromTime";
            this.FromTime.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            // 
            // 
            // 
            this.FromTime.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.FromTime.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.FromTime.MonthCalendar.BackgroundStyle.Class = "";
            this.FromTime.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.FromTime.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.FromTime.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.FromTime.MonthCalendar.DisplayMonth = new System.DateTime(2011, 5, 1, 0, 0, 0, 0);
            this.FromTime.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.FromTime.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.FromTime.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.FromTime.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.FromTime.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.FromTime.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.FromTime.Name = "FromTime";
            this.FromTime.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FromTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FromTime.Width = 60;
            // 
            // ToTime
            // 
            // 
            // 
            // 
            this.ToTime.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.ToTime.BackgroundStyle.Class = "DataGridViewDateTimeBorder";
            this.ToTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ToTime.BackgroundStyle.TextColor = System.Drawing.SystemColors.ControlText;
            this.ToTime.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.ToTime.HeaderText = "ToTime";
            this.ToTime.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            // 
            // 
            // 
            this.ToTime.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.ToTime.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.ToTime.MonthCalendar.BackgroundStyle.Class = "";
            this.ToTime.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ToTime.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.ToTime.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ToTime.MonthCalendar.DisplayMonth = new System.DateTime(2011, 5, 1, 0, 0, 0, 0);
            this.ToTime.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.ToTime.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.ToTime.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.ToTime.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.ToTime.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ToTime.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.ToTime.Name = "ToTime";
            this.ToTime.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ToTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ToTime.Width = 60;
            // 
            // Duration
            // 
            this.Duration.HeaderText = "Duration";
            this.Duration.MinimumWidth = 50;
            this.Duration.Name = "Duration";
            this.Duration.ReadOnly = true;
            this.Duration.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Duration.Width = 60;
            // 
            // AllowedBreakTime
            // 
            this.AllowedBreakTime.HeaderText = "Break Time";
            this.AllowedBreakTime.MaxInputLength = 3;
            this.AllowedBreakTime.Name = "AllowedBreakTime";
            this.AllowedBreakTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AllowedBreakTime.Width = 60;
            // 
            // MinWorkingHrs
            // 
            this.MinWorkingHrs.HeaderText = "Min Working Hrs";
            this.MinWorkingHrs.MaxInputLength = 6;
            this.MinWorkingHrs.Name = "MinWorkingHrs";
            this.MinWorkingHrs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MinWorkingHrs.Width = 84;
            // 
            // BufferTime
            // 
            this.BufferTime.HeaderText = "BufferTime";
            this.BufferTime.MaxInputLength = 3;
            this.BufferTime.Name = "BufferTime";
            this.BufferTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BufferTime.Width = 80;
            // 
            // cboShiftOTPolicyID
            // 
            this.cboShiftOTPolicyID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboShiftOTPolicyID.HeaderText = "Shift OT Policy";
            this.cboShiftOTPolicyID.MinimumWidth = 100;
            this.cboShiftOTPolicyID.Name = "cboShiftOTPolicyID";
            this.cboShiftOTPolicyID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cboShiftOTPolicyID.Visible = false;
            this.cboShiftOTPolicyID.Width = 131;
            // 
            // ActualDuration
            // 
            this.ActualDuration.HeaderText = "ActualDuration";
            this.ActualDuration.Name = "ActualDuration";
            this.ActualDuration.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ActualDuration.Visible = false;
            // 
            // cboOTPolicy
            // 
            this.cboOTPolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOTPolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOTPolicy.BackColor = System.Drawing.Color.White;
            this.cboOTPolicy.DropDownHeight = 134;
            this.cboOTPolicy.FormattingEnabled = true;
            this.cboOTPolicy.IntegralHeight = false;
            this.cboOTPolicy.Location = new System.Drawing.Point(266, 29);
            this.cboOTPolicy.MaxDropDownItems = 10;
            this.cboOTPolicy.Name = "cboOTPolicy";
            this.cboOTPolicy.Size = new System.Drawing.Size(200, 21);
            this.cboOTPolicy.TabIndex = 1039;
            this.cboOTPolicy.Visible = false;
            // 
            // lblOTPolicy
            // 
            this.lblOTPolicy.BackColor = System.Drawing.Color.Transparent;
            this.lblOTPolicy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOTPolicy.Location = new System.Drawing.Point(201, 28);
            this.lblOTPolicy.Name = "lblOTPolicy";
            this.lblOTPolicy.Size = new System.Drawing.Size(56, 23);
            this.lblOTPolicy.TabIndex = 1038;
            this.lblOTPolicy.Text = "OT Policy";
            this.lblOTPolicy.Visible = false;
            // 
            // numericTextBox2
            // 
            this.numericTextBox2.Location = new System.Drawing.Point(362, 56);
            this.numericTextBox2.MaxLength = 3;
            this.numericTextBox2.Name = "numericTextBox2";
            this.numericTextBox2.ShortcutsEnabled = false;
            this.numericTextBox2.Size = new System.Drawing.Size(75, 20);
            this.numericTextBox2.TabIndex = 1033;
            // 
            // txtNoOfTimings
            // 
            this.txtNoOfTimings.Location = new System.Drawing.Point(382, 46);
            this.txtNoOfTimings.MaxLength = 2;
            this.txtNoOfTimings.Name = "txtNoOfTimings";
            this.txtNoOfTimings.ShortcutsEnabled = false;
            this.txtNoOfTimings.Size = new System.Drawing.Size(75, 20);
            this.txtNoOfTimings.TabIndex = 11;
            this.txtNoOfTimings.TextChanged += new System.EventHandler(this.txtNoOfTimings_TextChanged);
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 106;
            this.lineShape2.X2 = 460;
            this.lineShape2.Y1 = 194;
            this.lineShape2.Y2 = 194;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 70;
            this.lineShape1.X2 = 462;
            this.lineShape1.Y1 = 59;
            this.lineShape1.Y2 = 59;
            // 
            // dtpHourFormat12
            // 
            this.dtpHourFormat12.CustomFormat = "HH:mm";
            this.dtpHourFormat12.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpHourFormat12.Location = new System.Drawing.Point(484, 147);
            this.dtpHourFormat12.Name = "dtpHourFormat12";
            this.dtpHourFormat12.ShowUpDown = true;
            this.dtpHourFormat12.Size = new System.Drawing.Size(68, 20);
            this.dtpHourFormat12.TabIndex = 62;
            this.dtpHourFormat12.Visible = false;
            // 
            // ShiftBindingNavigator
            // 
            this.ShiftBindingNavigator.AddNewItem = null;
            this.ShiftBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.ShiftBindingNavigator.CountItem = null;
            this.ShiftBindingNavigator.DeleteItem = null;
            this.ShiftBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorAddNewItem,
            this.ShiftReferenceBindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.CancelToolStripButton,
            this.ToolStripSeparator1,
            this.btnPrint,
            this.btnEmail,
            this.BtnHelp});
            this.ShiftBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.ShiftBindingNavigator.MoveFirstItem = null;
            this.ShiftBindingNavigator.MoveLastItem = null;
            this.ShiftBindingNavigator.MoveNextItem = null;
            this.ShiftBindingNavigator.MovePreviousItem = null;
            this.ShiftBindingNavigator.Name = "ShiftBindingNavigator";
            this.ShiftBindingNavigator.PositionItem = null;
            this.ShiftBindingNavigator.Size = new System.Drawing.Size(484, 25);
            this.ShiftBindingNavigator.TabIndex = 1;
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.ToolTipText = "Add New Company";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // ShiftReferenceBindingNavigatorSaveItem
            // 
            this.ShiftReferenceBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ShiftReferenceBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("ShiftReferenceBindingNavigatorSaveItem.Image")));
            this.ShiftReferenceBindingNavigatorSaveItem.Name = "ShiftReferenceBindingNavigatorSaveItem";
            this.ShiftReferenceBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.ShiftReferenceBindingNavigatorSaveItem.Text = "Save";
            this.ShiftReferenceBindingNavigatorSaveItem.Click += new System.EventHandler(this.CompanyMasterBindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Remove";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // CancelToolStripButton
            // 
            this.CancelToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CancelToolStripButton.Image")));
            this.CancelToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CancelToolStripButton.Name = "CancelToolStripButton";
            this.CancelToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.CancelToolStripButton.ToolTipText = "Clear";
            this.CancelToolStripButton.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Visible = false;
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ShiftID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Shift Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 120;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "From Time";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 80;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "To Time";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 80;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.HeaderText = "Duration";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.HeaderText = "Shift Type";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "CompanyID";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "ShiftID";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Visible = false;
            this.dataGridViewTextBoxColumn8.Width = 50;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "OrderNo";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Width = 125;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "FromTime";
            this.dataGridViewTextBoxColumn10.MinimumWidth = 50;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Visible = false;
            this.dataGridViewTextBoxColumn10.Width = 80;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn11.HeaderText = "ToTime";
            this.dataGridViewTextBoxColumn11.MaxInputLength = 3;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "BreakTime";
            this.dataGridViewTextBoxColumn12.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Width = 75;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn13.HeaderText = "Duration";
            this.dataGridViewTextBoxColumn13.MaxInputLength = 3;
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "ActualDuration";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtWeekWrkHrs);
            this.groupBox1.Controls.Add(this.lblEarly);
            this.groupBox1.Controls.Add(this.lblLate);
            this.groupBox1.Controls.Add(this.chkISBufferForOT);
            this.groupBox1.Controls.Add(this.txtEarlyBefore);
            this.groupBox1.Controls.Add(this.txtLateAfter);
            this.groupBox1.Controls.Add(this.tabMain);
            this.groupBox1.Controls.Add(this.BtnSave);
            this.groupBox1.Controls.Add(this.lblMinTimeForOT);
            this.groupBox1.Controls.Add(this.chkIsMinWorkOT);
            this.groupBox1.Controls.Add(this.txtMinTimeForOT);
            this.groupBox1.Controls.Add(this.labelX14);
            this.groupBox1.Controls.Add(this.labelX9);
            this.groupBox1.Controls.Add(this.dtpMinWorkingHours);
            this.groupBox1.Controls.Add(this.labelX7);
            this.groupBox1.Controls.Add(this.txtAllowedBreakTime);
            this.groupBox1.Controls.Add(this.labelX3);
            this.groupBox1.Controls.Add(this.labelX4);
            this.groupBox1.Controls.Add(this.labelX5);
            this.groupBox1.Controls.Add(this.FromTimeDateTimePicker);
            this.groupBox1.Controls.Add(this.ToTimeDateTimePicker);
            this.groupBox1.Controls.Add(this.txtDuration);
            this.groupBox1.Controls.Add(this.lblBufferTime);
            this.groupBox1.Controls.Add(this.txtBufferTime);
            this.groupBox1.Controls.Add(this.txtNoOfTimings);
            this.groupBox1.Controls.Add(this.labelX2);
            this.groupBox1.Controls.Add(this.DescriptionTextBox);
            this.groupBox1.Controls.Add(this.labelX6);
            this.groupBox1.Controls.Add(this.cboShiftType);
            this.groupBox1.Controls.Add(this.labelX12);
            this.groupBox1.Controls.Add(this.lblNoTimes);
            this.groupBox1.Controls.Add(this.shapeContainer2);
            this.groupBox1.Location = new System.Drawing.Point(4, 22);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(474, 447);
            this.groupBox1.TabIndex = 1041;
            this.groupBox1.TabStop = false;
            // 
            // lblEarly
            // 
            this.lblEarly.AutoSize = true;
            this.lblEarly.Location = new System.Drawing.Point(272, 116);
            this.lblEarly.Name = "lblEarly";
            this.lblEarly.Size = new System.Drawing.Size(71, 13);
            this.lblEarly.TabIndex = 1039;
            this.lblEarly.Text = "Early Leaving";
            // 
            // lblLate
            // 
            this.lblLate.AutoSize = true;
            this.lblLate.Location = new System.Drawing.Point(272, 94);
            this.lblLate.Name = "lblLate";
            this.lblLate.Size = new System.Drawing.Size(66, 13);
            this.lblLate.TabIndex = 1038;
            this.lblLate.Text = "Late Coming";
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(468, 428);
            this.shapeContainer2.TabIndex = 1037;
            this.shapeContainer2.TabStop = false;
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblstatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 506);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(484, 22);
            this.ssStatus.TabIndex = 1042;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // txtWeekWrkHrs
            // 
            this.txtWeekWrkHrs.Location = new System.Drawing.Point(321, 247);
            this.txtWeekWrkHrs.MaxLength = 3;
            this.txtWeekWrkHrs.Name = "txtWeekWrkHrs";
            this.txtWeekWrkHrs.ShortcutsEnabled = false;
            this.txtWeekWrkHrs.Size = new System.Drawing.Size(48, 20);
            this.txtWeekWrkHrs.TabIndex = 1043;
            this.txtWeekWrkHrs.TextChanged += new System.EventHandler(this.txtWeekWrkHrs_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(207, 250);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 1044;
            this.label1.Text = "Weekly OT  limit(Hrs)";
            // 
            // FrmShiftPolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(484, 528);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labelX15);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.ShiftBindingNavigator);
            this.Controls.Add(this.dtpHourFormat12);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmShiftPolicy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shift Policy";
            this.Load += new System.EventHandler(this.ShiftPolicy_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmShiftPolicy_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderShift)).EndInit();
            this.tabMain.ResumeLayout(false);
            this.tabShiftDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvShift)).EndInit();
            this.tabDynamicShiftDetails.ResumeLayout(false);
            this.tabDynamicShiftDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDynamicShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftBindingNavigator)).EndInit();
            this.ShiftBindingNavigator.ResumeLayout(false);
            this.ShiftBindingNavigator.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox DescriptionTextBox;
        internal System.Windows.Forms.DateTimePicker FromTimeDateTimePicker;
        internal System.Windows.Forms.DateTimePicker ToTimeDateTimePicker;
        internal System.Windows.Forms.ComboBox cboShiftType;
        internal System.Windows.Forms.ErrorProvider ErrorProviderShift;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        internal System.Windows.Forms.DateTimePicker dtpMinWorkingHours;
        internal System.Windows.Forms.Timer TmrComapny;
        internal System.Windows.Forms.Timer TmrFocus;
        private DemoClsDataGridview.NumericTextBox txtNoOfTimings;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Label lblNoTimes;
        private System.Windows.Forms.Label labelX7;
        private System.Windows.Forms.Label labelX6;
        private System.Windows.Forms.Label labelX5;
        private System.Windows.Forms.Label labelX4;
        private System.Windows.Forms.Label labelX3;
        private System.Windows.Forms.Label labelX2;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabShiftDetails;
        private DevComponents.DotNetBar.Controls.DataGridViewX DgvShift;
        private System.Windows.Forms.TabPage tabDynamicShiftDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvDynamicShift;
        internal System.Windows.Forms.DateTimePicker dtpHourFormat12;
        internal System.Windows.Forms.TextBox txtDuration;
        private System.Windows.Forms.CheckBox chkIsMinWorkOT;
        private System.Windows.Forms.Label labelX9;
        internal System.Windows.Forms.DateTimePicker dtpTimeFormat;
        private DemoClsDataGridview.NumericTextBox txtEarlyBefore;
        private DemoClsDataGridview.NumericTextBox txtLateAfter;
        private DemoClsDataGridview.NumericTextBox txtAllowedBreakTime;
        private System.Windows.Forms.Label lblBufferTime;
        private System.Windows.Forms.CheckBox chkISBufferForOT;
        private DemoClsDataGridview.NumericTextBox txtBufferTime;
        private System.Windows.Forms.Label lblMinTimeForOT;
        private DemoClsDataGridview.NumericTextBox txtMinTimeForOT;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Label labelX12;
        private System.Windows.Forms.Label labelX14;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private System.Windows.Forms.Label labelX15;
        internal System.Windows.Forms.ComboBox cboOTPolicy;
        private System.Windows.Forms.Label lblOTPolicy;
        private DemoClsDataGridview.NumericTextBox numericTextBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        internal System.Windows.Forms.BindingNavigator ShiftBindingNavigator;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton ShiftReferenceBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton CancelToolStripButton;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        private System.Windows.Forms.GroupBox groupBox1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyID;
        private System.Windows.Forms.Label lblEarly;
        private System.Windows.Forms.Label lblLate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShiftID;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderNo;
        private DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn FromTime;
        private DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn ToTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Duration;
        private System.Windows.Forms.DataGridViewTextBoxColumn AllowedBreakTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn MinWorkingHrs;
        private System.Windows.Forms.DataGridViewTextBoxColumn BufferTime;
        private System.Windows.Forms.DataGridViewComboBoxColumn cboShiftOTPolicyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActualDuration;
        private DemoClsDataGridview.NumericTextBox txtWeekWrkHrs;
        private System.Windows.Forms.Label label1;
    }
}