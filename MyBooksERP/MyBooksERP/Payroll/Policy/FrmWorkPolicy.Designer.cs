﻿namespace MyBooksERP
{
	partial class FrmWorkPolicy
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmWorkPolicy));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.WorkpolicyBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BtnClear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnShiftPolicy = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.DgvPolicyConsequence = new System.Windows.Forms.DataGridView();
            this.PolicyConsequenceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PolicyyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AllowedDaysPerMonth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOP = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Casual = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConsequenceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPolicyConsequence = new DevComponents.DotNetBar.TabItem(this.components);
            this.TmWorkPolicy = new System.Windows.Forms.Timer(this.components);
            this.ErrEmployee = new System.Windows.Forms.ErrorProvider(this.components);
            this.TmrPolicy = new System.Windows.Forms.Timer(this.components);
            this.CopyDetailsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.applyToAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cboOffDay = new System.Windows.Forms.ComboBox();
            this.txtPolicyname = new System.Windows.Forms.TextBox();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.tabPolicy = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.DgvPolicyDetail = new DemoClsDataGridview.ClsDataGirdView();
            this.PolicyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DayID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkingDay = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShiftDetailID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPolicydetail = new DevComponents.DotNetBar.TabItem(this.components);
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.SsMessage = new System.Windows.Forms.StatusStrip();
            this.LblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblHeader = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.WorkpolicyBindingNavigator)).BeginInit();
            this.WorkpolicyBindingNavigator.SuspendLayout();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvPolicyConsequence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrEmployee)).BeginInit();
            this.CopyDetailsContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabPolicy)).BeginInit();
            this.tabPolicy.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvPolicyDetail)).BeginInit();
            this.SsMessage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // WorkpolicyBindingNavigator
            // 
            this.WorkpolicyBindingNavigator.AddNewItem = null;
            this.WorkpolicyBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.WorkpolicyBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.WorkpolicyBindingNavigator.DeleteItem = null;
            this.WorkpolicyBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.BtnClear,
            this.ToolStripSeparator1,
            this.BtnShiftPolicy,
            this.toolStripSeparator2,
            this.btnPrint,
            this.toolStripSeparator3,
            this.BtnHelp});
            this.WorkpolicyBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.WorkpolicyBindingNavigator.MoveFirstItem = null;
            this.WorkpolicyBindingNavigator.MoveLastItem = null;
            this.WorkpolicyBindingNavigator.MoveNextItem = null;
            this.WorkpolicyBindingNavigator.MovePreviousItem = null;
            this.WorkpolicyBindingNavigator.Name = "WorkpolicyBindingNavigator";
            this.WorkpolicyBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.WorkpolicyBindingNavigator.Size = new System.Drawing.Size(591, 25);
            this.WorkpolicyBindingNavigator.TabIndex = 2;
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Enabled = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.ReadOnly = true;
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.ToolTipText = "Add New Company";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Remove";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BtnClear
            // 
            this.BtnClear.Image = ((System.Drawing.Image)(resources.GetObject("BtnClear.Image")));
            this.BtnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnClear.Name = "BtnClear";
            this.BtnClear.Size = new System.Drawing.Size(23, 22);
            this.BtnClear.ToolTipText = "Clear";
            this.BtnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnShiftPolicy
            // 
            this.BtnShiftPolicy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnShiftPolicy.Image = ((System.Drawing.Image)(resources.GetObject("BtnShiftPolicy.Image")));
            this.BtnShiftPolicy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnShiftPolicy.Name = "BtnShiftPolicy";
            this.BtnShiftPolicy.Size = new System.Drawing.Size(23, 22);
            this.BtnShiftPolicy.Text = "Shift Policy";
            this.BtnShiftPolicy.Click += new System.EventHandler(this.BtnShiftPolicy_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel2.Controls.Add(this.DgvPolicyConsequence);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(560, 178);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            this.tabControlPanel2.TabItem = this.tabPolicyConsequence;
            // 
            // DgvPolicyConsequence
            // 
            this.DgvPolicyConsequence.AllowUserToAddRows = false;
            this.DgvPolicyConsequence.AllowUserToDeleteRows = false;
            this.DgvPolicyConsequence.AllowUserToResizeColumns = false;
            this.DgvPolicyConsequence.AllowUserToResizeRows = false;
            this.DgvPolicyConsequence.BackgroundColor = System.Drawing.Color.White;
            this.DgvPolicyConsequence.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvPolicyConsequence.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PolicyConsequenceID,
            this.PolicyyID,
            this.Type,
            this.AllowedDaysPerMonth,
            this.LOP,
            this.Casual,
            this.Amount,
            this.ConsequenceID});
            this.DgvPolicyConsequence.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvPolicyConsequence.Location = new System.Drawing.Point(1, 1);
            this.DgvPolicyConsequence.Name = "DgvPolicyConsequence";
            this.DgvPolicyConsequence.RowHeadersWidth = 20;
            this.DgvPolicyConsequence.Size = new System.Drawing.Size(558, 176);
            this.DgvPolicyConsequence.TabIndex = 13;
            this.DgvPolicyConsequence.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DgvPolicyConsequence_CellBeginEdit);
            this.DgvPolicyConsequence.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvPolicyConsequence_CellEndEdit);
            this.DgvPolicyConsequence.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DgvPolicyConsequence_EditingControlShowing);
            this.DgvPolicyConsequence.CurrentCellDirtyStateChanged += new System.EventHandler(this.DgvPolicyConsequence_CurrentCellDirtyStateChanged);
            this.DgvPolicyConsequence.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DgvPolicyConsequence_DataError);
            // 
            // PolicyConsequenceID
            // 
            this.PolicyConsequenceID.DataPropertyName = "ConsequenceID";
            this.PolicyConsequenceID.HeaderText = "PolicyConsequenceID";
            this.PolicyConsequenceID.Name = "PolicyConsequenceID";
            this.PolicyConsequenceID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PolicyConsequenceID.Visible = false;
            // 
            // PolicyyID
            // 
            this.PolicyyID.DataPropertyName = "WorkPolicyID";
            this.PolicyyID.HeaderText = "PolicyID";
            this.PolicyyID.Name = "PolicyyID";
            this.PolicyyID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PolicyyID.Visible = false;
            // 
            // Type
            // 
            this.Type.DataPropertyName = "Type";
            this.Type.HeaderText = "Consequence Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            this.Type.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Type.Width = 120;
            // 
            // AllowedDaysPerMonth
            // 
            this.AllowedDaysPerMonth.DataPropertyName = "AllowedDaysPerMonth";
            this.AllowedDaysPerMonth.HeaderText = "Allowed Days / Month";
            this.AllowedDaysPerMonth.MaxInputLength = 2;
            this.AllowedDaysPerMonth.Name = "AllowedDaysPerMonth";
            // 
            // LOP
            // 
            this.LOP.DataPropertyName = "LOP";
            this.LOP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LOP.HeaderText = "LOP";
            this.LOP.Name = "LOP";
            this.LOP.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Casual
            // 
            this.Casual.DataPropertyName = "Casual";
            this.Casual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Casual.HeaderText = "Casual";
            this.Casual.Name = "Casual";
            this.Casual.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Amount
            // 
            this.Amount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Amount.DataPropertyName = "Amount";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.Amount.DefaultCellStyle = dataGridViewCellStyle1;
            this.Amount.HeaderText = "Amount";
            this.Amount.MaxInputLength = 8;
            this.Amount.Name = "Amount";
            this.Amount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ConsequenceID
            // 
            this.ConsequenceID.DataPropertyName = "ConsequenceID";
            this.ConsequenceID.HeaderText = "ConsequenceID";
            this.ConsequenceID.Name = "ConsequenceID";
            this.ConsequenceID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ConsequenceID.Visible = false;
            // 
            // tabPolicyConsequence
            // 
            this.tabPolicyConsequence.AttachedControl = this.tabControlPanel2;
            this.tabPolicyConsequence.Name = "tabPolicyConsequence";
            this.tabPolicyConsequence.Text = "Policy Consequences";
            // 
            // ErrEmployee
            // 
            this.ErrEmployee.ContainerControl = this;
            this.ErrEmployee.RightToLeft = true;
            // 
            // TmrPolicy
            // 
            this.TmrPolicy.Interval = 2000;
            // 
            // CopyDetailsContextMenuStrip
            // 
            this.CopyDetailsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.applyToAllToolStripMenuItem});
            this.CopyDetailsContextMenuStrip.Name = "CopyDetailsContextMenuStrip";
            this.CopyDetailsContextMenuStrip.Size = new System.Drawing.Size(140, 26);
            // 
            // applyToAllToolStripMenuItem
            // 
            this.applyToAllToolStripMenuItem.Name = "applyToAllToolStripMenuItem";
            this.applyToAllToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.applyToAllToolStripMenuItem.Text = "Apply To All";
            this.applyToAllToolStripMenuItem.Click += new System.EventHandler(this.applyToAllToolStripMenuItem_Click);
            // 
            // cboOffDay
            // 
            this.cboOffDay.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOffDay.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOffDay.BackColor = System.Drawing.SystemColors.Info;
            this.cboOffDay.DropDownHeight = 134;
            this.cboOffDay.FormattingEnabled = true;
            this.cboOffDay.IntegralHeight = false;
            this.cboOffDay.Location = new System.Drawing.Point(353, 22);
            this.cboOffDay.Name = "cboOffDay";
            this.cboOffDay.Size = new System.Drawing.Size(212, 21);
            this.cboOffDay.TabIndex = 10;
            this.cboOffDay.SelectionChangeCommitted += new System.EventHandler(this.cboOffDay_SelectionChangeCommitted);
            this.cboOffDay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboOffDay_KeyDown);
            // 
            // txtPolicyname
            // 
            this.txtPolicyname.BackColor = System.Drawing.SystemColors.Info;
            this.txtPolicyname.Location = new System.Drawing.Point(83, 22);
            this.txtPolicyname.MaxLength = 49;
            this.txtPolicyname.Name = "txtPolicyname";
            this.txtPolicyname.Size = new System.Drawing.Size(193, 20);
            this.txtPolicyname.TabIndex = 9;
            this.txtPolicyname.TextChanged += new System.EventHandler(this.txtPolicyname_TextChanged);
            // 
            // BtnSave
            // 
            this.BtnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnSave.Location = new System.Drawing.Point(12, 293);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 13;
            this.BtnSave.Text = "&Save";
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnOk.Location = new System.Drawing.Point(428, 293);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 14;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnCancel.Location = new System.Drawing.Point(509, 293);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 15;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // tabPolicy
            // 
            this.tabPolicy.BackColor = System.Drawing.Color.Transparent;
            this.tabPolicy.CanReorderTabs = true;
            this.tabPolicy.Controls.Add(this.tabControlPanel1);
            this.tabPolicy.Controls.Add(this.tabControlPanel2);
            this.tabPolicy.Location = new System.Drawing.Point(6, 53);
            this.tabPolicy.Name = "tabPolicy";
            this.tabPolicy.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabPolicy.SelectedTabIndex = 1;
            this.tabPolicy.Size = new System.Drawing.Size(560, 200);
            this.tabPolicy.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tabPolicy.TabIndex = 11;
            this.tabPolicy.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tabPolicy.Tabs.Add(this.tabPolicydetail);
            this.tabPolicy.Tabs.Add(this.tabPolicyConsequence);
            this.tabPolicy.Text = "tabControl1";
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tabControlPanel1.Controls.Add(this.DgvPolicyDetail);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(560, 178);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 1;
            this.tabControlPanel1.TabItem = this.tabPolicydetail;
            // 
            // DgvPolicyDetail
            // 
            this.DgvPolicyDetail.AddNewRow = false;
            this.DgvPolicyDetail.AllowUserToAddRows = false;
            this.DgvPolicyDetail.AllowUserToDeleteRows = false;
            this.DgvPolicyDetail.AllowUserToResizeColumns = false;
            this.DgvPolicyDetail.AllowUserToResizeRows = false;
            this.DgvPolicyDetail.AlphaNumericCols = new int[0];
            this.DgvPolicyDetail.BackgroundColor = System.Drawing.Color.White;
            this.DgvPolicyDetail.CapsLockCols = new int[0];
            this.DgvPolicyDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvPolicyDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PolicyID,
            this.DayID,
            this.WorkingDay,
            this.Description,
            this.ShiftDetailID});
            this.DgvPolicyDetail.DecimalCols = new int[0];
            this.DgvPolicyDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvPolicyDetail.HasSlNo = false;
            this.DgvPolicyDetail.LastRowIndex = 0;
            this.DgvPolicyDetail.Location = new System.Drawing.Point(1, 1);
            this.DgvPolicyDetail.Name = "DgvPolicyDetail";
            this.DgvPolicyDetail.NegativeValueCols = new int[0];
            this.DgvPolicyDetail.NumericCols = new int[0];
            this.DgvPolicyDetail.RowHeadersVisible = false;
            this.DgvPolicyDetail.RowHeadersWidth = 25;
            this.DgvPolicyDetail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.DgvPolicyDetail.Size = new System.Drawing.Size(558, 176);
            this.DgvPolicyDetail.TabIndex = 12;
            this.DgvPolicyDetail.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DgvPolicyDetail_CellMouseClick);
            this.DgvPolicyDetail.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DgvPolicyDetail_CellBeginEdit);
            this.DgvPolicyDetail.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvPolicyDetail_CellEndEdit);
            this.DgvPolicyDetail.CurrentCellDirtyStateChanged += new System.EventHandler(this.DgvPolicyDetail_CurrentCellDirtyStateChanged);
            this.DgvPolicyDetail.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DgvPolicyDetail_DataError);
            // 
            // PolicyID
            // 
            this.PolicyID.DataPropertyName = "WorkPolicyID";
            this.PolicyID.HeaderText = "PolicyID ";
            this.PolicyID.Name = "PolicyID";
            this.PolicyID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PolicyID.Visible = false;
            this.PolicyID.Width = 80;
            // 
            // DayID
            // 
            this.DayID.DataPropertyName = "DayID";
            this.DayID.HeaderText = "DayID";
            this.DayID.Name = "DayID";
            this.DayID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DayID.Visible = false;
            // 
            // WorkingDay
            // 
            this.WorkingDay.DataPropertyName = "WorkingDay";
            this.WorkingDay.HeaderText = "Working Day";
            this.WorkingDay.Name = "WorkingDay";
            this.WorkingDay.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Description
            // 
            this.Description.DataPropertyName = "DayName";
            this.Description.HeaderText = "Day";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            this.Description.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Description.Width = 200;
            // 
            // ShiftDetailID
            // 
            this.ShiftDetailID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ShiftDetailID.DataPropertyName = "ShiftDetailID";
            this.ShiftDetailID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ShiftDetailID.HeaderText = "Shift";
            this.ShiftDetailID.Name = "ShiftDetailID";
            this.ShiftDetailID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // tabPolicydetail
            // 
            this.tabPolicydetail.AttachedControl = this.tabControlPanel1;
            this.tabPolicydetail.Name = "tabPolicydetail";
            this.tabPolicydetail.Text = "Policy Detail                       ";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.Class = "";
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(7, 19);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 18;
            this.labelX2.Text = "Policy Name";
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.Class = "";
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(279, 18);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(70, 23);
            this.labelX3.TabIndex = 19;
            this.labelX3.Text = "General Shift";
            // 
            // SsMessage
            // 
            this.SsMessage.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblStatus});
            this.SsMessage.Location = new System.Drawing.Point(0, 322);
            this.SsMessage.Name = "SsMessage";
            this.SsMessage.Size = new System.Drawing.Size(591, 22);
            this.SsMessage.TabIndex = 311;
            this.SsMessage.Text = "StatusStrip1";
            // 
            // LblStatus
            // 
            this.LblStatus.Name = "LblStatus";
            this.LblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblHeader.Location = new System.Drawing.Point(4, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(77, 13);
            this.lblHeader.TabIndex = 313;
            this.lblHeader.Text = "General Info";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabPolicy);
            this.groupBox1.Controls.Add(this.labelX3);
            this.groupBox1.Controls.Add(this.lblHeader);
            this.groupBox1.Controls.Add(this.labelX2);
            this.groupBox1.Controls.Add(this.cboOffDay);
            this.groupBox1.Controls.Add(this.txtPolicyname);
            this.groupBox1.Location = new System.Drawing.Point(8, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(574, 262);
            this.groupBox1.TabIndex = 314;
            this.groupBox1.TabStop = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "PolicyID";
            this.dataGridViewTextBoxColumn1.HeaderText = "PolicyID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "DayID";
            this.dataGridViewTextBoxColumn2.HeaderText = "DayID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Description";
            this.dataGridViewTextBoxColumn3.HeaderText = "Day";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "AllowedBreakTime";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn4.HeaderText = "Break Duration";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 2;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "LateAfter";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn5.HeaderText = "Late After";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 2;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Visible = false;
            this.dataGridViewTextBoxColumn5.Width = 120;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "EarlyBefore";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn6.HeaderText = "Early Before";
            this.dataGridViewTextBoxColumn6.MaxInputLength = 2;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "PolicyConsequenceID";
            this.dataGridViewTextBoxColumn7.HeaderText = "PolicyConsequenceID";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "PolicyID";
            this.dataGridViewTextBoxColumn8.HeaderText = "PolicyID";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Amount";
            this.dataGridViewTextBoxColumn9.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn10.DataPropertyName = "ConsequenceID";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn10.HeaderText = "ConsequenceID";
            this.dataGridViewTextBoxColumn10.MaxInputLength = 8;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "ConsequenceID";
            this.dataGridViewTextBoxColumn11.HeaderText = "ConsequenceID";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Visible = false;
            // 
            // FrmWorkPolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(591, 344);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.SsMessage);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.WorkpolicyBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmWorkPolicy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Work Policy";
            this.Load += new System.EventHandler(this.FrmWorkPolicy_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmWorkPolicy_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmWorkPolicy_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.WorkpolicyBindingNavigator)).EndInit();
            this.WorkpolicyBindingNavigator.ResumeLayout(false);
            this.WorkpolicyBindingNavigator.PerformLayout();
            this.tabControlPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvPolicyConsequence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrEmployee)).EndInit();
            this.CopyDetailsContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabPolicy)).EndInit();
            this.tabPolicy.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvPolicyDetail)).EndInit();
            this.SsMessage.ResumeLayout(false);
            this.SsMessage.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

        internal System.Windows.Forms.BindingNavigator WorkpolicyBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BtnClear;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        private System.Windows.Forms.DataGridView DgvPolicyConsequence;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.Timer TmWorkPolicy;
        private System.Windows.Forms.ErrorProvider ErrEmployee;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        internal System.Windows.Forms.Timer TmrPolicy;
        private System.Windows.Forms.ToolStripButton BtnShiftPolicy;
        private System.Windows.Forms.ContextMenuStrip CopyDetailsContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem applyToAllToolStripMenuItem;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private DevComponents.DotNetBar.TabItem tabPolicyConsequence;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.TabControl tabPolicy;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private DemoClsDataGridview.ClsDataGirdView DgvPolicyDetail;
        private DevComponents.DotNetBar.TabItem tabPolicydetail;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.TextBox txtPolicyname;
        private System.Windows.Forms.ComboBox cboOffDay;
        internal System.Windows.Forms.StatusStrip SsMessage;
        internal System.Windows.Forms.ToolStripStatusLabel LblStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.Label lblHeader;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.DataGridViewTextBoxColumn PolicyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn DayID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn WorkingDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewComboBoxColumn ShiftDetailID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PolicyConsequenceID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PolicyyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn AllowedDaysPerMonth;
        private System.Windows.Forms.DataGridViewComboBoxColumn LOP;
        private System.Windows.Forms.DataGridViewComboBoxColumn Casual;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConsequenceID;
	}
}