﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyBooksERP
{

 /*****************************************************
  * Created By       : Ranju Mathew
  * Creation Date    : 12 Aug 2013
  * Description      : Handle UnEarned Policy
  * ***************************************************/

    public partial class FrmUnearnedPolicy : Form
    {

        #region Declartions

        public int PintUnEarnedPolicyID = 0;   // From Refernce Form 
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission

        private string MstrCommonMessage;
        private bool MblnIsEditMode = false;  //  To Find Whether Is add mode or Edit Mode
        private int MintRecordCnt = 0;     //Total Record Count
        private int MintCurrentRecCnt = 0; // Current Record Count
        private bool MblnChangeStatus = false;
        private bool MblbtnOk = false;

        private clsBLLUnearnedPolicy  MobjclsBLLUnearnedPolicy = new clsBLLUnearnedPolicy();
        private clsMessage ObjUserMessage = null;
        string strBindingOf = "Of ";
        #endregion Declartions

        #region Constructor
        public FrmUnearnedPolicy()
        {
            InitializeComponent();

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.UnearnedPolicy, this);

        //    strBindingOf = "من ";
        //    dgvAbsentDetails.Columns["colAbsentParticulars"].HeaderText = "تفاصيل";
        //}
        #endregion Constructor

        #region Properties
        private clsMessage UserMessage  // For Notification Message
        {
            get
            {   // Messages From 10510 - 10521  
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.UnearnedPolicy);
                return this.ObjUserMessage;
            }
        }
        
        #endregion Properties

        #region Methods

        #region SetPermissions
        /// <summary>
        ///  Function for setting permissions Add/Update/Delete/Email-Print
        /// </summary>
        private void SetPermissions()  
        {

            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID >3 )
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.UnearnedPolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }
        #endregion SetPermissions

        #region ClearAllControls

        /// <summary>
        /// Funtion to clear All Controls
        /// </summary>
        private void ClearAllControls()
        {
            this.txtPolicyName.Text = "";
            this.txtPolicyName.Tag = 0;
            this.rdbCompanyBased.Checked = true;
            this.rdbActualMonth.Checked = false;
            //this.rdbWorkingDays.Checked = false;
            this.cboCalculationBased.SelectedIndex = -1;
            this.cboCalculationBased.Text = "";
            this.dgvAbsentDetails.Rows.Clear();
            this.chkRatePerDay.Checked = false;
            this.chkRateOnly.Checked = false;
            this.txtHoursPerDay.Text = "";
            this.txtRate.Text = "";
            this.rdbCompanyBased.Enabled = true; 
        }
        #endregion ClearAllControls

        #region Changestatus
        /// <summary>
        /// function for changing status
        /// </summary>
        /// <param name="sender"> object</param>
        /// <param name="e">EventArgs </param>

        private void Changestatus(object sender, EventArgs e) //
        {

            if (!MblnIsEditMode)
            {
                btnSave.Enabled = btnOk.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
                btnClear.Enabled = true;
            }
            else
            {
                btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                btnClear.Enabled = false;
            }
            errAbsentPolicy.Clear();
            MblnChangeStatus = true;

        }

        #endregion Changestatus

        #region LoadCombos
        /// <summary>
        /// For Loading the Combos 
        /// </summary>
        /// <param name="intType">
        /// IntType = 1 --> Calculation type
        /// </param>
        /// <returns></returns>
        private bool LoadCombos(int intType)
        {
            bool blnRetvalue = false;
            DataTable datCombos = new DataTable();
            try
            {

                if (intType == 0 || intType == 1)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    datCombos = MobjclsBLLUnearnedPolicy.FillCombos(new string[] { "CalculationID,CalculationArb AS Calculation", "PayCalculationReference", " CalculationID in(1,2,4)" });
                    //else
                        datCombos = MobjclsBLLUnearnedPolicy.FillCombos(new string[] { "CalculationID,Calculation", "PayCalculationReference", "CalculationID in(1,2,4)" });
                    cboCalculationBased.ValueMember = "CalculationID";
                    cboCalculationBased.DisplayMember = "Calculation";
                    cboCalculationBased.DataSource = datCombos;
                    blnRetvalue = true;
                }


            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
                blnRetvalue = false;
            }
            return blnRetvalue;
        }
        #endregion LoadCombos

        #region FillAdditionDeductionsGrid
        /// <summary>
        /// To fill AdditionDeduction Details in Grid
        /// </summary>
        /// <param name="Dgv"> object of dgv </param>
        /// <param name="dtAddDedDetails"> object of datatable </param>
        private void FillAdditionDeductionsGrid(DataGridView Dgv, DataTable dtAddDedDetails)
        {
            Dgv.Rows.Clear();
            if (dtAddDedDetails != null)
            {
                if (dtAddDedDetails.Rows.Count > 0)
                {
                    for (int iCounter = 0; iCounter <= dtAddDedDetails.Rows.Count - 1; iCounter++)
                    {
                        Dgv.RowCount = Dgv.RowCount + 1;
                        Dgv.Rows[iCounter].Cells[0].Value = dtAddDedDetails.Rows[iCounter]["AddDedID"].ToInt32();//Addition deduction Policy
                        Dgv.Rows[iCounter].Cells[1].Value = dtAddDedDetails.Rows[iCounter]["Checked"].ToBoolean(); ;//ChkBox
                        Dgv.Rows[iCounter].Cells[2].Value = dtAddDedDetails.Rows[iCounter]["Description"].ToStringCustom();//Description
                    }
                }
            }
        }
        #endregion FillAdditionDeductionsGrid

        #region GetRecordCount
        /// <summary>
        /// TO Get Record Count
        /// </summary>
        private void GetRecordCount()
        {
            MintRecordCnt = 0;
            MintRecordCnt = MobjclsBLLUnearnedPolicy.GetRecordCount();
            if (MintRecordCnt < 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                MintRecordCnt = 0;
            }
        }
        #endregion GetRecordCount

        #region RefernceDisplay
        /// <summary>
        /// To Display Details In Edit Mode
        /// </summary>
        private void RefernceDisplay()
        {

            int intRowNum = 0;
            intRowNum = MobjclsBLLUnearnedPolicy.GetRowNumber(PintUnEarnedPolicyID);
            if (intRowNum > 0)
            {
                MintCurrentRecCnt = intRowNum;
                DisplayUnearnedPolicyInfo();
            }
        }
        #endregion RefernceDisplay

        #region AddNewUnEarnedPolicy

        /// <summary>
        /// Add Mode
        /// </summary>

        private void AddNewUnEarnedPolicy()
        {
            MblnChangeStatus = false;
            MblnIsEditMode = false;
            lblstatus.Text = UserMessage.GetMessageByCode((int)CommonMessages.NewRecord);
            tmrClear.Enabled = true;
            errAbsentPolicy.Clear();

            ClearAllControls();  //Clear all controls in the form
            GetRecordCount();
            MintRecordCnt = MintRecordCnt + 1;
            MintCurrentRecCnt = MintRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();

            BindingNavigatorAddNewItem.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = false;
            btnPrint.Enabled = false;
            btnEmail.Enabled = false;
            btnOk.Enabled = false;
            btnSave.Enabled = false;
            btnClear.Enabled = true;
            BindingNavigatorSaveItem.Enabled = false;
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = false;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = false;

            txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtRate.Enabled = chkRateOnly.Checked ? true : false;

            txtHoursPerDay.BackColor = chkRatePerDay.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtHoursPerDay.Enabled = chkRatePerDay.Checked ? true : false;
            txtPolicyName.Focus();
            txtPolicyName.Select();

        }
        #endregion AddNewUnEarnedPolicy

        #region SetBindingNavigatorButtons
        /// <summary>
        /// 
        /// </summary>
        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
        }
        #endregion SetBindingNavigatorButtons

        #region DisplayUnearnedPolicyInfo
        /// <summary>
        /// To Display Detail Info
        /// </summary>
        private void DisplayUnearnedPolicyInfo()
        {

            FillUnEarnedPolicyInfo();

            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            btnPrint.Enabled = MblnPrintEmailPermission;
            btnEmail.Enabled = MblnPrintEmailPermission;
            BindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            MblnIsEditMode = true;
            SetBindingNavigatorButtons();
            MblnChangeStatus = false;
            BindingNavigatorSaveItem.Enabled = MblnChangeStatus;
            btnOk.Enabled = MblnChangeStatus;
            btnSave.Enabled = MblnChangeStatus;
            btnClear.Enabled = false;
            txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtRate.Enabled = chkRateOnly.Checked ? true : false;

            txtHoursPerDay.BackColor = chkRatePerDay.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtHoursPerDay.Enabled = chkRatePerDay.Checked ? true : false;
        }
        #endregion DisplayUnearnedPolicyInfo

        #region FillUnEarnedPolicyInfo
        /// <summary>
        /// To fill UnEarned Policy Info
        /// </summary>

        #endregion FillUnEarnedPolicyInfo

        #region FillUnEarnedPolicyInfo
        /// <summary>
        /// Fill UnEarnedpolicy Info 
        /// </summary>
        /// <returns></returns>
        private void FillUnEarnedPolicyInfo()
        {
            ClearAllControls();
            DataTable dtAbsentPolicy = MobjclsBLLUnearnedPolicy.DisplayUnearnedPolicy(MintCurrentRecCnt);

            if (dtAbsentPolicy.Rows.Count > 0)
            {

                txtPolicyName.Text = dtAbsentPolicy.Rows[0]["UnearnedPolicy"].ToStringCustom();
                txtPolicyName.Tag = dtAbsentPolicy.Rows[0]["UnearnedPolicyID"].ToInt32();
                for (int iCounter = 0; iCounter <= dtAbsentPolicy.Rows.Count - 1; iCounter++)
                {
                    cboCalculationBased.SelectedValue = dtAbsentPolicy.Rows[iCounter]["CalculationID"].ToInt32();

                    if (dtAbsentPolicy.Rows[iCounter]["CompanyBasedOn"].ToInt32() == 1)
                    {
                        rdbCompanyBased.Checked = true;
                    }
                    else
                    {
                        rdbActualMonth.Checked = true;
                    }

                    chkRateOnly.Checked = dtAbsentPolicy.Rows[iCounter]["IsRateOnly"].ToBoolean();
                    txtRate.Text = dtAbsentPolicy.Rows[iCounter]["UnearnedRate"].ToDecimal().ToStringCustom();
                    chkRatePerDay.Checked = dtAbsentPolicy.Rows[iCounter]["IsRateBasedOnHour"].ToBoolean();
                    txtHoursPerDay.Text = dtAbsentPolicy.Rows[iCounter]["HoursPerDay"].ToStringCustom();
                    if (dtAbsentPolicy.Rows[iCounter]["CalculationID"].ToInt32() == (int)CalculationType.GrossSalary)
                    {
                        DataTable DtAddDedRef = MobjclsBLLUnearnedPolicy.DisplayAddDedDetails(txtPolicyName.Tag.ToInt32(), (int)PolicyType.UnEarnedAmt);
                        FillAdditionDeductionsGrid(dgvAbsentDetails, DtAddDedRef);
                    }

                    //if (ClsCommonSettings.IsAmountRoundByZero)
                    //{
                    //    txtRate.Text = txtRate.Text.ToDecimal().ToString("F" + 0);
                    //}


                }

            }

        }
        #endregion FillUnEarnedPolicyInfo

        #region FormValidation
        /// <summary>
        /// For Validation
        /// </summary>
        /// <returns></returns>
        private bool FormValidation()
        {

            errAbsentPolicy.Clear();
            Control control = null;
            bool blnReturnValue = true;

            if (txtPolicyName.Text.Trim().Length == 0)     // Please enter Policy Name
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10510);
                control = txtPolicyName;
                blnReturnValue = false;
            }
            else if (chkRateOnly.Checked == false && cboCalculationBased.SelectedValue.ToInt32() == 0) //Please select Calculation based for Unearned policy
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10511);
                control = cboCalculationBased;
                blnReturnValue = false;
            }
            else if (chkRatePerDay.Checked == true && (txtHoursPerDay.Text.Trim().Length == 0 ? 0.ToDecimal() : txtHoursPerDay.Text.ToDecimal()) == 0.ToDecimal())
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10517); //Please enter a 'Hour per Day'  between 1 and 24 for Unearned policy
                control = txtHoursPerDay;
                blnReturnValue = false;
            }
            else if (chkRatePerDay.Checked == true && (txtHoursPerDay.Text.Trim().Length == 0 ? 0.ToDecimal() : txtHoursPerDay.Text.ToDecimal()) > 24.ToDecimal())
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10517); //Please enter a 'Hour per Day'  between 1 and 24 for Unearned policy
                control = txtHoursPerDay;
                blnReturnValue = false;
            }
            else if (chkRateOnly.Checked && (txtRate.Text.Trim().Length == 0 || txtRate.Text.ToDecimal()==0))
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(10513); //Please enter valid Rate for Unearned policy
                control = txtRate;
                blnReturnValue = false;
            }
            else if (txtRate.Text.Trim().Length > 0)
            {
                try
                {
                    decimal rate = Convert.ToDecimal(txtRate.Text);
                }
                catch
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(10513);  //Please enter valid Rate for Unearned policy
                    control = txtRate;
                    blnReturnValue = false;
                }
            }


            if (blnReturnValue == false)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrClear.Enabled = true;
                if (control != null)
                {
                    errAbsentPolicy.SetError(control, MstrCommonMessage);
                    control.Focus();

                }
                return blnReturnValue;
            }

            //if (cboCalculationBased.SelectedValue.ToInt32() == (int)CalculationType.WorkDays)
            //{
            //    rdbActualMonth.Checked = true; 
            //}

            if (blnReturnValue)
            {
                if (MobjclsBLLUnearnedPolicy.CheckDuplicate(txtPolicyName.Tag.ToInt32(), txtPolicyName.Text.Trim()))//CheckDuplicate()
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(10521); //Duplicate policy name.Please change policy name
                    control = txtPolicyName;
                    blnReturnValue = false;
                }
            }
            if (blnReturnValue == false)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrClear.Enabled = true;
                if (control != null)
                {
                    errAbsentPolicy.SetError(control, MstrCommonMessage);
                    control.Focus();

                }
            }

            return blnReturnValue;
        }
        #endregion FormValidation

        #region DeleteValidation
        /// <summary>
        /// Delete validation To check Whether Reference Exist Or Not
        /// </summary>
        /// <returns></returns>
        private bool DeleteValidation()
        {
            if (txtPolicyName.Tag.ToInt32() == 0)
            {
                UserMessage.ShowMessage(9015); // Sorry,No Data Found
                return false;

            }
            if (MobjclsBLLUnearnedPolicy.PolicyIDExists(txtPolicyName.Tag.ToInt32()))
            {
                UserMessage.ShowMessage(10514); //Details exists for this Unearned Policy in the system.Please delete the existing details to proceed with Unearned Policy deletion.
                return false;
            }
            if (UserMessage.ShowMessage(10520) == false) // Do you wish to Delete Unearned policy information?
            {
                return false;
            }
            return true;

        }
        #endregion DeleteValidation

        #region FillParameterMaster
        /// <summary>
        /// To Fill Master Details to Dto Variables
        /// </summary>
        private void FillParameterMaster()
        {
            MobjclsBLLUnearnedPolicy.DTOUnearnedPolicy.UnearnedPolicyID = txtPolicyName.Tag.ToInt32();
            MobjclsBLLUnearnedPolicy.DTOUnearnedPolicy.UnearnedPolicy = txtPolicyName.Text.Trim();

        }
        #endregion FillParameterMaster

        #region FillParameterDetails
        /// <summary>
        /// To Fill Det Info To Dto variables
        /// </summary>
        private void FillParameterDetails()
        {
            MobjclsBLLUnearnedPolicy.DTOUnearnedPolicy.DTOUnearnedPolicyDetails = new List<clsDTOUnearnedPolicyDetails>();

            clsDTOUnearnedPolicyDetails ObjclsDTOUnearnedPolicyDetails = new clsDTOUnearnedPolicyDetails();
            ObjclsDTOUnearnedPolicyDetails.CalculationID = cboCalculationBased.SelectedValue.ToInt32();
            if (rdbCompanyBased.Checked == true)
            {
                ObjclsDTOUnearnedPolicyDetails.CompanyBasedOn = 1;  // CompanyBasedOn
            }
            else if (rdbActualMonth.Checked == true)
            {
                ObjclsDTOUnearnedPolicyDetails.CompanyBasedOn = 2;  // ActualMonth
            }
            else
            {
                ObjclsDTOUnearnedPolicyDetails.CompanyBasedOn = 3;
            }
            ObjclsDTOUnearnedPolicyDetails.IsRateOnly = chkRateOnly.Checked;
            ObjclsDTOUnearnedPolicyDetails.UnearnedRate = chkRateOnly.Checked ? txtRate.Text.Trim().ToDecimal() : 0;

            ObjclsDTOUnearnedPolicyDetails.IsRateBasedOnHour = chkRatePerDay.Checked;
            ObjclsDTOUnearnedPolicyDetails.HoursPerDay = chkRatePerDay.Checked ? txtHoursPerDay.Text.Trim().ToDecimal() : 0;

            MobjclsBLLUnearnedPolicy.DTOUnearnedPolicy.DTOUnearnedPolicyDetails.Add(ObjclsDTOUnearnedPolicyDetails);
        }
        #endregion FillParameterDetails

        #region FillParameterSalPolicyDetails
        /// <summary>
        /// Fill AddtionDeduction Details
        /// </summary>
        private void FillParameterSalPolicyDetails()
        {
            MobjclsBLLUnearnedPolicy.DTOUnearnedPolicy.DTOSalaryPolicyDetail = new List<clsDTOSalaryPolicyUNDetails>();

            if (dgvAbsentDetails.Rows.Count > 0)
            {
                dgvAbsentDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                dgvAbsentDetails.CurrentCell = dgvAbsentDetails["colAbsentParticulars", 0];
                foreach (DataGridViewRow row in dgvAbsentDetails.Rows)
                {
                    if (Convert.ToBoolean(row.Cells["colAbsentSelectedItem"].Value) == true)
                    {
                        clsDTOSalaryPolicyUNDetails objSalaryPolicyDetail = new clsDTOSalaryPolicyUNDetails();
                        objSalaryPolicyDetail.PolicyType = (int)PolicyType.UnEarnedAmt;
                        objSalaryPolicyDetail.AdditionDeductionID = row.Cells["colAbsentAddDedID"].Value.ToInt32();
                        MobjclsBLLUnearnedPolicy.DTOUnearnedPolicy.DTOSalaryPolicyDetail.Add(objSalaryPolicyDetail);
                    }
                }
            }

        }
        #endregion FillParameterSalPolicyDetails

        #region SaveUnEarnedPolicy
        /// <summary>
        /// Save UnEarned Policy Details
        /// </summary>
        /// <returns></returns>
        private bool SaveUnEarnedPolicy()
        {
            bool blnRetValue = false;

            if (FormValidation())
            {
                int intMessageCode = 0;
                if (txtPolicyName.Tag.ToInt32() > 0)
                {
                    intMessageCode = 10519;
                    MblnIsEditMode = true;
                }
                else
                {
                    intMessageCode = 10518;
                    MblnIsEditMode = false;
                }

                if (UserMessage.ShowMessage(intMessageCode))
                {
                    FillParameterMaster();
                    FillParameterDetails();
                    FillParameterSalPolicyDetails();
                    if (MobjclsBLLUnearnedPolicy.UnearnedPolicyMasterSave())
                    {
                        blnRetValue = true;
                        txtPolicyName.Tag = MobjclsBLLUnearnedPolicy.DTOUnearnedPolicy.UnearnedPolicyID;
                        PintUnEarnedPolicyID = MobjclsBLLUnearnedPolicy.DTOUnearnedPolicy.UnearnedPolicyID;
                        BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                        btnEmail.Enabled = MblnPrintEmailPermission;
                        btnPrint.Enabled = MblnPrintEmailPermission;
                        BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                        btnSave.Enabled = btnOk.Enabled = BindingNavigatorSaveItem.Enabled = false;
                        if (!MblnIsEditMode)
                        {
                            lblstatus.Text = UserMessage.GetMessageByCode(2);
                            UserMessage.ShowMessage(2);
                        }
                        else
                        {
                            lblstatus.Text = UserMessage.GetMessageByCode(21);
                            UserMessage.ShowMessage(21);
                        }
                    }

                }
            }

            return blnRetValue;
        }
        #endregion SaveUnEarnedPolicy

        #region DeleteUnEarnedPolicy
        /// <summary>
        /// Delete UnEarned Policy Reference
        /// </summary>
        /// <returns></returns>
        private bool DeleteUnEarnedPolicy()
        {
            bool blnRetValue = false;
            if (DeleteValidation())
            {
                MobjclsBLLUnearnedPolicy.DTOUnearnedPolicy.UnearnedPolicyID = txtPolicyName.Tag.ToInt32();
                if (MobjclsBLLUnearnedPolicy.DeleteUnearnedPolicy())
                {
                    AddNewUnEarnedPolicy();
                    blnRetValue = true;
                }

            }
            return blnRetValue;

        }
        #endregion DeleteUnEarnedPolicy
      
        #endregion Methods

        #region Events

        private void ResetForm(object sender, System.EventArgs e) // To reset Froms
        {
            if (sender is CheckBox)
            {
                CheckBox chk = (CheckBox)sender;
                if (chk.Name == chkRateOnly.Name)
                {
                    txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
                    txtRate.Enabled = chkRateOnly.Checked ? true : false;

                    cboCalculationBased.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Window : System.Drawing.SystemColors.Info;
                    if (chkRateOnly.Checked)
                    {
                        cboCalculationBased.SelectedIndex = -1;
                    }
                    else
                    {
                        cboCalculationBased.SelectedIndex = 0;
                        txtRate.Text = "";
                    }
                    cboCalculationBased.Enabled = chkRateOnly.Checked ? false : true;

                    txtHoursPerDay.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
                    if (chkRateOnly.Checked)
                        chkRatePerDay.Checked = false;

                    txtHoursPerDay.Enabled = !txtRate.Enabled;
                }
                else if (chk.Name == chkRatePerDay.Name)
                {
                    if (chkRatePerDay.Checked)
                    {
                        chkRateOnly.Checked = false;
                    }
                    else
                        txtHoursPerDay.Text = "";

                }
                rdbCompanyBased.Enabled = (chkRateOnly.Checked) ? false : true;
                rdbActualMonth.Enabled = (chkRateOnly.Checked) ? false : true;
                //rdbWorkingDays.Enabled = (chkRateOnly.Checked ) ? false : true;

                txtHoursPerDay.BackColor = chkRatePerDay.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
                txtHoursPerDay.Enabled = chkRatePerDay.Checked ? true : false;

            }
            Changestatus(null, null);

        }

        private void FrmUnearnedPolicy_Load(object sender, EventArgs e)
        {

            SetPermissions();
            LoadCombos(0);
            if (PintUnEarnedPolicyID > 0)  // Calling Unearned Policy From Another Froms 
            {
                GetRecordCount();
                RefernceDisplay();
            }
            else
            {
                AddNewUnEarnedPolicy();
            }
            txtPolicyName.Select();
        }


        private void txtPolicyName_TextChanged(object sender, EventArgs e)  //txtPolicyName_TextChange Event 
        {
            ResetForm(sender, e);
            Changestatus(null, null);
        }

        private void rdbCompanyBased_CheckedChanged(object sender, EventArgs e) //rdbCompanyBased_CheckedChanged
        {
            ResetForm(sender, e);
        }

        private void rdbActualMonth_CheckedChanged(object sender, EventArgs e) //rdbActualMonth_CheckedChanged
        {
            ResetForm(sender, e);
        }

        private void cboCalculationBased_SelectedIndexChanged(object sender, EventArgs e) //cboCalculationBased_SelectedIndexChanged
        {
            Changestatus(null, null);
            if (cboCalculationBased.SelectedValue.ToInt32() > 0)
            {
                dgvAbsentDetails.Enabled = true;

                if (cboCalculationBased.SelectedValue.ToInt32() == (int)CalculationType.GrossSalary)
                {
                    DataTable dtADDDedd = MobjclsBLLUnearnedPolicy.GetAdditionDeductions();
                    FillAdditionDeductionsGrid(dgvAbsentDetails, dtADDDedd);
                }
                //else if (cboCalculationBased.SelectedValue.ToInt32() == (int)CalculationType.WorkDays)
                //{
                //    dgvAbsentDetails.Enabled = false;
                //    rdbActualMonth.Checked = true;
                //    //rdbCompanyBased.Enabled = false; 

                //}
                else
                {
                    dgvAbsentDetails.Rows.Clear();
                    dgvAbsentDetails.Enabled = false;
                }
            }
            else
            {
                dgvAbsentDetails.Rows.Clear();
                dgvAbsentDetails.Enabled = false;
            }
        }

        private void cboCalculationBased_KeyDown(object sender, KeyEventArgs e) //cboCalculationBased_KeyDown
        {
            cboCalculationBased.DroppedDown = false;
            if (e.KeyData == Keys.Enter)
                cboCalculationBased_SelectedIndexChanged(sender, new EventArgs());
        }

        private void dgvAbsentDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e) //dgvAbsentDetails_CellBeginEdit
        {
            Changestatus(null, null);
        }

        private void dgvAbsentDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e) //dgvAbsentDetails_CellValueChanged
        {
            Changestatus(null, null);
        }

        private void chkRatePerDay_CheckedChanged(object sender, EventArgs e) //chkRatePerDay_CheckedChanged
        {
            ResetForm(sender, e);
        }


        private void btnOk_Click(object sender, EventArgs e) //btnOk_Click
        {
            btnSave_Click(sender, e);
            btnSave.Enabled = false;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e) //btnCancel_Click
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e) //btnSave_Click
        {
            SaveUnEarnedPolicy();
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e) //BindingNavigatorMoveFirstItem_Click
        {
            GetRecordCount();
            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;
                DisplayUnearnedPolicyInfo();
                lblstatus.Text = UserMessage.GetMessageByCode(9);
               
            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e) //BindingNavigatorMovePreviousItem_Click
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    DisplayUnearnedPolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(10);
                   
                }
            }
            else
            {
                MintCurrentRecCnt = 1;
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e) //BindingNavigatorMoveNextItem_Click
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                }

                if (MintCurrentRecCnt > MintRecordCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                }
                else
                {
                    DisplayUnearnedPolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(11);
                }
            }
            else
            {
                MintCurrentRecCnt = 0;
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)//BindingNavigatorMoveLastItem_Click
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    DisplayUnearnedPolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(12);
                   
                }
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e) //BindingNavigatorAddNewItem_Click
        {
            AddNewUnEarnedPolicy();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e) //BindingNavigatorDeleteItem_Click
        {
            if (DeleteUnEarnedPolicy())
            {
                lblstatus.Text = UserMessage.GetMessageByCode(4);               
                UserMessage.ShowMessage(4);
             
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e) //BindingNavigatorSaveItem_Click
        {
            btnSave_Click(sender,e);
        }

        private void FrmUnearnedPolicy_FormClosing(object sender, FormClosingEventArgs e) //FrmUnearnedPolicy_FormClosing
        {
            if (btnSave.Enabled)
            {
                if (UserMessage.ShowMessage(8))
                {
                    e.Cancel = false;
                }
                else
                { e.Cancel = true; }
            }
        }

        private void tmrClear_Tick(object sender, EventArgs e) //tmrClear_Tick
        {
            tmrClear.Enabled = false;
            lblstatus.Text = "";
        }
        private void txtDecimal_KeyPress(object sender, KeyPressEventArgs e) // Key Press For TextDecimal
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            e.Handled = false;

            //if (ClsCommonSettings.IsAmountRoundByZero)
            //{
            //    strInvalidChars = strInvalidChars + ".";
            //}

            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
        }

        private void txtint_KeyPress(object sender, KeyPressEventArgs e) // Key Press For txtint_KeyPress
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[].";
            e.Handled = false;
            if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }

        private void chkRateOnly_CheckedChanged(object sender, EventArgs e) //chkRateOnly_CheckedChanged
        {
            ResetForm(sender, e);
        }

        private void FrmUnearnedPolicy_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        btnHelp_Click(null, null);
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        btnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());//Prev item
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());//Next item
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());//First item
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());//Last item
                        break;
                   
                    //case Keys.Control | Keys.M:
                    //    BtnEmail_Click(sender, new EventArgs());//Cancel
                    //    break;
                }
            }
            catch (Exception)
            {
            }


        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            AddNewUnEarnedPolicy();
        }
        #endregion Events

        private void btnHelp_Click(object sender, EventArgs e)
        {
            try
            {
                FrmHelp objHelp = new FrmHelp();
                objHelp.strFormName = "UnPolicy";
                objHelp.ShowDialog();
                objHelp = null;
            }
            catch (Exception Ex)
            {
              

            }
        }

    }
}
