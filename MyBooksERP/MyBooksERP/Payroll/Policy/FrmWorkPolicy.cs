﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using BLL;
using DAL;
using DTO;
/*****************************************************
    * Created By       : 
    * Creation Date    : 
    * Description      : Handle Work Policy
    * Modified by      : Siny
    * Modified date    : 13/8/2013
    * Description      : Tuning performance improving  
    * FormID           : 107 
    * ***************************************************/

namespace MyBooksERP
{
    public partial class FrmWorkPolicy : Form
    {
        #region Declarations
        // permissions
        private bool MblnAddPermission = false;         //To Set Add Permission
        private bool MblnUpdatePermission = false;      //To Set Update Permission
        private bool MblnDeletePermission = false;      //To Set Delete Permission
        private bool MblnPrintEmailPermission = false;  //To Set PrintEmail Permission

        private bool MblnAddStatus;                    //Add/Update mode 
        private bool MblnChangeStatus = false;
        private int MintCurrentRecCnt;                 // Current record count
        private int MintRecordCnt;                     // Total record count

        // Error Message display
        private string MstrMessageCommon;
        private string MsMessageCaption;
        private ArrayList MsarMessageArr;             
        private ArrayList MaStatusMessage;
        MessageBoxIcon MsMessageBoxIcon;

        public int PintCompanyId = 0;
        public int PintWorkPolicyID = 0;

        //Class declarations
        clsBLLWorkPolicy MobjClsBLLWorkPolicy; //Object for BLL class
        ClsLogWriter MObjClsLogWriter;         // Object of the LogWriter class
        ClsNotification MObjClsNotification;   // Object of the Notification class
        string strBindingOf = "Of ";
        #endregion Declarations

        #region Constructor
        public FrmWorkPolicy()
		{
            InitializeComponent();
            MsMessageCaption = ClsCommonSettings.MessageCaption; //Message caption
            MobjClsBLLWorkPolicy  = new clsBLLWorkPolicy();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);   // Application startup path
            MObjClsNotification = new ClsNotification();
            TmWorkPolicy.Interval = ClsCommonSettings.TimerInterval; // Set time interval of the timer

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.WorkPolicy, this);

        //    tabPolicydetail.Text = "تفاصيل السياسة";
        //    tabPolicyConsequence.Text = "عواقب السياسة";
        //    DgvPolicyDetail.Columns["WorkingDay"].HeaderText = "يوم عمل";
        //    DgvPolicyDetail.Columns["Description"].HeaderText = "يوم";
        //    DgvPolicyDetail.Columns["ShiftDetailID"].HeaderText = "تغير";
        //    DgvPolicyConsequence.Columns["Type"].HeaderText = "نوع النتيجة";
        //    DgvPolicyConsequence.Columns["LOP"].HeaderText = "لوب";
        //    DgvPolicyConsequence.Columns["Casual"].HeaderText = "عرضي";
        //    DgvPolicyConsequence.Columns["Amount"].HeaderText = "مبلغ";
        //    strBindingOf = "من ";
        //}
        #endregion Constructor

        #region Methods
        #region SetPermissions
        /// <summary>
        /// To set permission for add,update,print/email,delete
        /// For admin (roleid=1) and super admin all permissions are set to true
        /// </summary>
        private void SetPermissions()
        {
            try
            {
                clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
                if (ClsCommonSettings.RoleID >3 )
                {
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.WorkPolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                    DataTable datCombos = MobjClsBLLWorkPolicy.FillCombos(new string[] { "IsView", "RoleDetails", "RoleID=" + ClsCommonSettings.RoleID + " AND MenuID=" + (Int32)eMenuID.ShiftPolicy + "" });
                    if (datCombos == null || datCombos.Rows.Count <= 0)
                        BtnShiftPolicy.Enabled = false;
                    else
                        BtnShiftPolicy.Enabled = Convert.ToBoolean(datCombos.Rows[0]["IsView"]);
                }
                else
                {
                    MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
                    BtnShiftPolicy.Enabled = true;
                }
            }
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1, Log.LogSeverity.Error);

            }
                
        }
        #endregion SetPermissions

        #region LoadMessage
        /// <summary>
        /// Method to fill the message array according to form
        /// </summary>
        private void LoadMessage()
        {
            MsarMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MsarMessageArr = MObjClsNotification.FillMessageArray((int)FormID.ShiftPolicy ,ClsCommonSettings.ProductID);
        }
        #endregion LoadMessage

        #region AddNewItem
        /// <summary>
        /// Set controls to add new mode
        /// </summary>
        private void AddNewItem()
        {
            MobjClsBLLWorkPolicy = new clsBLLWorkPolicy();
            MblnAddStatus = true;
            MintRecordCnt = MobjClsBLLWorkPolicy.RecCountNavigate();
            ClearControls();    
            EnableDisableButtons(false);
            FillPolicyDetailGrid();
            btnPrint.Enabled = false;
            //LblStatus.Text = "Add new work policy";
            TmWorkPolicy.Enabled = true;
            BindingNavigatorAddNewItem.Enabled = false;
            txtPolicyname.Enabled = true;
            SetNavigatorEnability();
            txtPolicyname.Focus();
            txtPolicyname.Select();
        }
        #endregion AddNewItem

        #region FillPolicyDetails
        /// <summary>
        /// Fill Policy details
        /// </summary>
        private void FillPolicyDetails()
        {
            if (PintWorkPolicyID > 0)
                MintCurrentRecCnt =PintWorkPolicyID;
            if (MintCurrentRecCnt <= 0)
                MintCurrentRecCnt = 1;
            BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
            Fill();
        }
        #endregion FillPolicyDetails

        #region FillPolicyDetailGrid
        /// <summary>
        /// Fill Plicy details and consequences of a specific policy
        /// </summary>
        private void FillPolicyDetailGrid()
        {
            MobjClsBLLWorkPolicy =new clsBLLWorkPolicy();
            try
            {
                MobjClsBLLWorkPolicy.clsDTOWorkPolicy.PolicyID = -1;
                DataSet ds = MobjClsBLLWorkPolicy.GetPolicyDetails();
                DgvPolicyDetail.DataSource = ds.Tables[0];
                DgvPolicyConsequence.DataSource = ds.Tables[1];
                SetWorkingDaysChecked();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on FillPolicyDetailGrid " + this.Name + " " + Ex.Message.ToString(), 1);
            }
        }
        #endregion FillPolicyDetailGrid

        #region SetWorkingDaysChecked
        /// <summary>
        /// Check working days
        /// </summary>
        private void SetWorkingDaysChecked()
        {
            try
            {
                    for (int i = 0; i < DgvPolicyDetail.Rows.Count; i++)
                    {
                            DgvPolicyDetail.Rows[i].Cells[2].Value = CheckState.Checked;
                    }
            }
            catch (Exception ex)
            {
                MObjClsLogWriter.WriteLog("SetWorkingDaysChecked" + ex.Message, 1);
            }
        }
        #endregion SetWorkingDaysChecked

        #region ClearControls
        /// <summary>
        /// For clearing all the controls
        /// </summary>
        private void ClearControls()
        {
            txtPolicyname.Tag = 0;                //set PolicyID to 0
            txtPolicyname.Text = string.Empty;
            cboOffDay.SelectedIndex = -1;         
            DgvPolicyDetail.ClearSelection();
            DgvPolicyConsequence.ClearSelection();
        }
        #endregion ClearControls

        #region EnableDisableButtons
        /// <summary>
        /// Set button enability according to the boolean value passed
        /// </summary>
        /// <param name="bEnable">whether buttons should be enabled or  not</param>
        private void EnableDisableButtons(bool bEnable)
        {
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission ;
            BindingNavigatorDeleteItem.Enabled =MblnDeletePermission ;
            btnPrint.Enabled = MblnPrintEmailPermission;     
            if (bEnable)
            {
                //if (MblnAddStatus)
                //{
                    BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = BtnClear.Enabled = MblnAddPermission;
                //}
                //else
                //{
                //    BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                //    BtnClear.Enabled = false;
                //}//=BtnClear.Enabled
            }
            else
            {
                BindingNavigatorSaveItem.Enabled = BtnOk.Enabled = BtnSave.Enabled =  false ;
            }
            tabPolicy.SelectedTab = tabPolicydetail;
        }
        #endregion EnableDisableButtons
        
        #region LoadCombos
        /// <summary>
        /// Load the combos-company,shiftpolicy,off day,LOP,Casual
       /// </summary>
       /// <param name="iType">
        /// iType=0->Load all combos
        /// iType=2->Loads offday combo and shift detail combo only
        /// iType=4->Loads LOP combo only
        /// iType=5->Loads Casual combo only
       /// </param>
       /// <returns>success/failure</returns>
        private bool LoadCombos(int iType)
        {
            try
            {
                   DataTable datCombos = new DataTable();
                   if (iType == 0 || iType == 2)
                   {
                       datCombos = null;
                       cboOffDay.DataSource = null;
                       datCombos = MobjClsBLLWorkPolicy.FillCombos(new string[] { "ShiftID, ShiftName", "PayShiftReference", "" });
                       cboOffDay.ValueMember = "ShiftID";
                       cboOffDay.DisplayMember = "ShiftName";
                       cboOffDay.DataSource = datCombos;
                       ((DataGridViewComboBoxColumn)DgvPolicyDetail.Columns["ShiftDetailID"]).DataSource = datCombos.Copy();
                       ((DataGridViewComboBoxColumn)DgvPolicyDetail.Columns["ShiftDetailID"]).ValueMember = "ShiftID";
                       ((DataGridViewComboBoxColumn)DgvPolicyDetail.Columns["ShiftDetailID"]).DisplayMember = "ShiftName";
                   }
                   if (iType == 0 || iType == 4)
                   {
                        datCombos = null;
                        //if (ClsCommonSettings.IsArabicView)
                        //    datCombos = MobjClsBLLWorkPolicy.FillCombos(new string[] { "DayTypeID,DayTypeArb AS DayType", "PayDayTypeReference", "" });
                        //else
                            datCombos = MobjClsBLLWorkPolicy.FillCombos(new string[] { "DayTypeID,DayType", "PayDayTypeReference", "" });
                        LOP.DataSource = null;
                        LOP.ValueMember = "DayTypeID";
                        LOP.DisplayMember = "DayType";
                        LOP.DataSource = datCombos;
                   }
                   if (iType == 0 || iType == 5)
                   {
                       datCombos = null;
                       Casual.DataSource = null;
                       //if (ClsCommonSettings.IsArabicView)
                       //    datCombos = MobjClsBLLWorkPolicy.FillCombos(new string[] { "DayTypeID,DayTypeArb AS DayType", "PayDayTypeReference", "" });
                       //else
                           datCombos = MobjClsBLLWorkPolicy.FillCombos(new string[] { "DayTypeID,DayType", "PayDayTypeReference", "" });
                       Casual.ValueMember = "DayTypeID";
                       Casual.DisplayMember = "DayType";
                       Casual.DataSource = datCombos;
                   }
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }
        #endregion LoadCombos

        #region ChangeStatus
        /// <summary>
        /// To enable or disable save buttons according to addstatus and add/update permission
        /// </summary>
        private void ChangeStatus()
        {
            MblnChangeStatus = true;
            ErrEmployee.Clear();
            if (MblnAddStatus)
            {
                BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = BtnClear.Enabled = MblnAddPermission;
            }
            else
            {
                BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                BtnClear.Enabled = false;
            }
        }
        #endregion ChangeStatus

        #region SetNavigatorEnability
        /// <summary>
        /// Set enability of binding naigator buttons
        /// </summary>
        private void SetNavigatorEnability()
        {
            if (!MblnAddStatus)
            {
                BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
                int iRecordCnt = MintRecordCnt;  // Total count of the records
            }
            else
            {
                MintRecordCnt = MintRecordCnt + 1;
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                BindingNavigatorPositionItem.Text = Convert.ToString(MintRecordCnt);
                MintCurrentRecCnt = MintRecordCnt;
            }
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;
            if (MintRecordCnt == 0)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                if (BindingNavigatorPositionItem.Text.ToInt32() == MintRecordCnt)
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
                if (BindingNavigatorPositionItem.Text.ToInt32() == 1)
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
            }
            if (MblnAddStatus)
            {
                btnPrint.Enabled = BindingNavigatorDeleteItem.Enabled = false;
                BtnClear.Enabled = MblnAddPermission;
            }
            else
            {
                btnPrint.Enabled = MblnPrintEmailPermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BtnClear.Enabled = false;
            }
        }
        #endregion SetNavigatorEnability

        #region SaveWorkPolicy
        /// <summary>
        /// Save work policy
        /// </summary>
        /// <returns>success/failure</returns>
        private bool SaveWorkPolicy()
        {
            if (DgvPolicyConsequence.CurrentCell != null)
            {
                try
                {
                    DgvPolicyConsequence.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    DgvPolicyConsequence.CurrentCell = DgvPolicyConsequence[DgvPolicyConsequence.Columns["Type"].Index, 0];
                }
                catch { }
            }
            if (Validation() == false) return false;

            if (MblnAddStatus == true)//insert
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 1, out MsMessageBoxIcon);
            }
            else
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 3, out MsMessageBoxIcon);
            }
         //   LblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);

            if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                return false;

            FillPolicyParameters();
            return MobjClsBLLWorkPolicy.SaveWorkPolicy(MblnAddStatus);

        }
        #endregion SaveWorkPolicy

        #region Validation
        /// <summary>
        /// Validating work policy details
        /// </summary>
        /// <returns></returns>
        private bool Validation()
        {
            ErrEmployee.Clear();
            LblStatus.Text = "";

            if (Convert.ToString(txtPolicyname.Text.Trim()) == string.Empty)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 252, out MsMessageBoxIcon);
                ErrEmployee.SetError(txtPolicyname, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                LblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrPolicy.Enabled = true;
                tabPolicy.SelectedTab = tabPolicydetail;
                txtPolicyname.Focus();
                return false;
            }
            if (Convert.ToInt32(cboOffDay.SelectedIndex) == -1)
            {
                //if (ClsCommonSettings.IsArabicView)
                //    MstrMessageCommon = "الرجاء اختيار تحول عام";
                //else
                    MstrMessageCommon = "Please select general shift";
                ErrEmployee.SetError(cboOffDay, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                LblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrPolicy.Enabled = true;
                tabPolicy.SelectedTab = tabPolicydetail;
                cboOffDay.Focus();
                return false;
            }
            if (!MblnAddStatus)
            {
                if(MobjClsBLLWorkPolicy.IsExistsAttendanceWithoutProcess(txtPolicyname.Tag.ToInt32()))
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    MstrMessageCommon = "الحضور موجود لا يمكن تحديث سياسة!";
                    //else
                        MstrMessageCommon = "Attendance exists!Policy cannot be updated";
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    LblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPolicy.Enabled = true;
                    tabPolicy.SelectedTab = tabPolicydetail;
                    return false;
                }
            }
          
            

            if (MobjClsBLLWorkPolicy.CheckDuplication(MblnAddStatus, new string[] { txtPolicyname.Text.Replace("'", "").Trim() }, Convert.ToInt32(txtPolicyname.Tag), 1))
            {
                //Policy name exists.
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 253, out MsMessageBoxIcon);
                ErrEmployee.SetError(txtPolicyname, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                LblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrPolicy.Enabled = true;
                tabPolicy.SelectedTab = tabPolicydetail;
                txtPolicyname.Focus();
                return false;
            }
            //if (!(chkDefaultPolicy.Checked) && !(chkActivePolicy.Checked))
            //{
            //    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 255, out MsMessageBoxIcon);
            //    ErrEmployee.SetError(chkDefaultPolicy, MstrMessageCommon.Replace("#", "").Trim());
            //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
            //    LblEmployeeStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            //    TmrPolicy.Enabled = true;
            //    tabPolicy.SelectedTab = tabPolicydetail;
            //    chkDefaultPolicy.Focus();
            //    return false;
            //}

            MobjClsBLLWorkPolicy.clsDTOWorkPolicy.PolicyID = Convert.ToInt32(txtPolicyname.Tag);
            //if ((chkDefaultPolicy.Checked) && (chkActivePolicy.Checked))
            //{
            //    if (MobjClsBLLWorkPolicy.HasDefaultPolicy())
            //    {
            //        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 254, out MsMessageBoxIcon);
            //        ErrEmployee.SetError(chkDefaultPolicy, MstrMessageCommon.Replace("#", "").Trim());
            //        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
            //        LblEmployeeStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            //        TmrPolicy.Enabled = true;
            //        tabPolicy.SelectedTab = tabPolicydetail;
            //        chkDefaultPolicy.Focus();
            //        return false;
            //    }
            //}
            for (int i = 0; i < DgvPolicyConsequence.Rows.Count; i++)
            {
                if (DgvPolicyConsequence["AllowedDaysPerMonth", i].Value != DBNull.Value)
                {
                    if (DgvPolicyConsequence["AllowedDaysPerMonth", i].Value.ToInt32() > 31)
                    {
                        //Allowed Days cannot be greater than 31 days
                        DgvPolicyConsequence.CurrentCell = DgvPolicyConsequence[3, i];
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 261, out MsMessageBoxIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                        tabPolicy.SelectedTab = tabPolicyConsequence;
                        return false;
                    }
                }
            }
            int iOffDay = 0;
            for (int i = 0; i < DgvPolicyDetail.Rows.Count; i++)
            {
                if (DgvPolicyDetail["WorkingDay", i].Value != DBNull.Value)
                {
                    if (Convert.ToBoolean(DgvPolicyDetail["WorkingDay", i].Value) == true)
                    {
                        iOffDay = 1;
                        if (DgvPolicyDetail["ShiftDetailID", i].Value == DBNull.Value)
                        {
                            //Please select shift
                            DgvPolicyDetail.CurrentCell = DgvPolicyDetail[4, i];
                            MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 256, out MsMessageBoxIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                            tabPolicy.SelectedTab = tabPolicydetail;
                            return false;
                        }
                    }
                }
            }
            if (iOffDay == 0)
            {
                //if (ClsCommonSettings.IsArabicView)
                //    MstrMessageCommon = "الرجاء اختيار عامل أيام";
                //else
                    MstrMessageCommon = "Please select Workingdays";
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                LblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrPolicy.Enabled = true;
                tabPolicy.SelectedTab = tabPolicydetail;
                return false;
            }

            return true;
        }
        #endregion Validation

        #region FillPolicyParameters
        /// <summary>
        /// Fill allthe parameters before saving workpolicy
        /// </summary>
        private void FillPolicyParameters()
        {
            try
            {
                if (MblnAddStatus == true)
                    MobjClsBLLWorkPolicy.clsDTOWorkPolicy.PolicyID = 0;
                else
                    MobjClsBLLWorkPolicy.clsDTOWorkPolicy.PolicyID = Convert.ToInt32(txtPolicyname.Tag);
                MobjClsBLLWorkPolicy.clsDTOWorkPolicy.Description = Convert.ToString(txtPolicyname.Text.Trim());
                MobjClsBLLWorkPolicy.clsDTOWorkPolicy.OffDayShiftID = Convert.ToInt32(cboOffDay.SelectedValue);
                //MobjClsBLLWorkPolicy.clsDTOWorkPolicy.DefaultPolicy = chkDefaultPolicy.Checked;
                //MobjClsBLLWorkPolicy.clsDTOWorkPolicy.Active = chkActivePolicy.Checked;

                FillPolicyDetailParameters();
                FillPolicyConsequenceParameters();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on FillPolicyParameters " + this.Name + " " + Ex.Message.ToString(), 1);
            }

        }
        #endregion FillPolicyParameters

        #region FillPolicyDetailParameters
        /// <summary>
        /// Fill policy details in dto object
        /// </summary>
        private void FillPolicyDetailParameters()
        {
            MobjClsBLLWorkPolicy.clsDTOWorkPolicy.lstclsDTOWorkPolicyDetails = new System.Collections.Generic.List<DTO.clsDTOWorkPolicyDetails>();

            try
            {

                for (int i = 0; i < DgvPolicyDetail.Rows.Count; i++)
                {

                    if (DgvPolicyDetail["WorkingDay", i].Value != DBNull.Value)
                    {
                        if (Convert.ToBoolean(DgvPolicyDetail["WorkingDay", i].Value) == true)
                        {
                            clsDTOWorkPolicyDetails objclsDTOWorkPolicyDetails = new clsDTOWorkPolicyDetails();

                            objclsDTOWorkPolicyDetails.DayID = Convert.ToInt32(DgvPolicyDetail["DayID", i].Value);

                            objclsDTOWorkPolicyDetails.ShiftID = Convert.ToInt32(DgvPolicyDetail["ShiftDetailID", i].Value);
                            MobjClsBLLWorkPolicy.clsDTOWorkPolicy.lstclsDTOWorkPolicyDetails.Add(objclsDTOWorkPolicyDetails);
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on FillPolicyDetailParameters " + this.Name + " " + Ex.Message.ToString(), 1);
            }
        }
        #endregion FillPolicyDetailParameters

        #region FillPolicyConsequenceParameters
        /// <summary>
        /// Fill policy consequence in consequence object
        /// </summary>
        private void FillPolicyConsequenceParameters()
        {
            MobjClsBLLWorkPolicy.clsDTOWorkPolicy.lstclsDTOWorkPolicyConsequences = new System.Collections.Generic.List<DTO.clsDTOWorkPolicyConsequences>();
            try
            {
                for (int i = 0; i < DgvPolicyConsequence.Rows.Count; i++)
                {
                    clsDTOWorkPolicyConsequences objclsDTOWorkPolicyConsequences = new clsDTOWorkPolicyConsequences();

                    objclsDTOWorkPolicyConsequences.ConsequenceID = Convert.ToInt32(DgvPolicyConsequence["PolicyConsequenceID", i].Value);
                    if (DgvPolicyConsequence["LOP", i].Value != DBNull.Value)
                        objclsDTOWorkPolicyConsequences.LOP = Convert.ToInt32(DgvPolicyConsequence["LOP", i].Value);
                    if (DgvPolicyConsequence["AllowedDaysPerMonth", i].Value != DBNull.Value)
                        objclsDTOWorkPolicyConsequences.AllowedDaysPerMonth = Convert.ToInt32(DgvPolicyConsequence["AllowedDaysPerMonth", i].Value);
                    if (DgvPolicyConsequence["Casual", i].Value != DBNull.Value)
                        objclsDTOWorkPolicyConsequences.Casual = Convert.ToInt32(DgvPolicyConsequence["Casual", i].Value);
                    if (DgvPolicyConsequence["Amount", i].Value != DBNull.Value)
                        objclsDTOWorkPolicyConsequences.Amount = Convert.ToDecimal(DgvPolicyConsequence["Amount", i].Value);

                    MobjClsBLLWorkPolicy.clsDTOWorkPolicy.lstclsDTOWorkPolicyConsequences.Add(objclsDTOWorkPolicyConsequences);

                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on FillPolicyConsequenceParameters " + this.Name + " " + Ex.Message.ToString(), 1);
            }
        }
        #endregion FillPolicyConsequenceParameters

        #region Fill
        /// <summary>
        /// Fill policy details
        /// </summary>
        private void Fill()
        {
            MobjClsBLLWorkPolicy = new clsBLLWorkPolicy();
            try
            {
                if (MobjClsBLLWorkPolicy.GetWorkpolicy(MintCurrentRecCnt))
                {
                    MblnAddStatus = false;
                    txtPolicyname.Tag = MobjClsBLLWorkPolicy.clsDTOWorkPolicy.PolicyID;
                    txtPolicyname.Text = MobjClsBLLWorkPolicy.clsDTOWorkPolicy.Description;
                    LoadCombos(2);
                    cboOffDay.SelectedValue = MobjClsBLLWorkPolicy.clsDTOWorkPolicy.OffDayShiftID;

                    DataSet dt = MobjClsBLLWorkPolicy.GetPolicyDetails();
                    DgvPolicyDetail.DataSource = dt.Tables[0];
                    DgvPolicyConsequence.DataSource = dt.Tables[1];
                    EnableDisableButtons(false);
                    SetNavigatorEnability();
                    MblnChangeStatus = false;
                    if (!MblnAddStatus && MobjClsBLLWorkPolicy.IsExistsAttendance(txtPolicyname.Tag.ToInt32()))
                    {
                        txtPolicyname.Enabled = false;
                    }
                    else
                        txtPolicyname.Enabled = true;
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on Fill " + this.Name + " " + Ex.Message.ToString(), 1);
            }
        }
        #endregion Fill

        #region DeleteValidation
        /// <summary>
        /// Validation done before deleting policy
        /// </summary>
        /// <returns></returns>
        private bool DeleteValidation()
        {

            try
            {
                MobjClsBLLWorkPolicy.clsDTOWorkPolicy.PolicyID = Convert.ToInt32(txtPolicyname.Tag);
                if (MobjClsBLLWorkPolicy.IsExists())//Details exists in the system
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 9001, out MsMessageBoxIcon);
                    //if (ClsCommonSettings.IsArabicView)
                    //    MstrMessageCommon = MstrMessageCommon.Replace("*", "سياسة العمل");
                    //else
                        MstrMessageCommon = MstrMessageCommon.Replace("*", "Work Policy");

                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    LblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrPolicy.Enabled = true;
                    return false;
                }
                //Do you wish to delete this information?
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 13, out MsMessageBoxIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;
                else
                    return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on DeleteValidation " + this.Name + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }
        #endregion DeleteValidation

        #region IsNumeric
        /// <summary>
        /// Check if value entered is numeric
        /// </summary>
        /// <param name="IsPolicyDetail"></param>
        private void IsNumeric(bool IsPolicyDetail)
        {
            try
            {
                if (!IsPolicyDetail)
                {
                    if ((DgvPolicyConsequence.CurrentCell.OwningColumn.Name == "Amount") && (DgvPolicyConsequence.CurrentCell.Value != null || DgvPolicyConsequence.CurrentCell.Value != DBNull.Value) && (Convert.ToString(DgvPolicyConsequence.CurrentCell.Value) != string.Empty))
                    {
                        DgvPolicyConsequence.CurrentCell.Value = DgvPolicyConsequence.CurrentCell.Value.ToDecimal();
                    }
                }
            }
            catch (Exception ex)
            {
                MObjClsLogWriter.WriteLog("IsNumeric()" + ex.Message, 1);
            }
        }
        #endregion IsNumeric
        #endregion Methods

        #region Events

        private void FrmWorkPolicy_Load(object sender, EventArgs e)
        {
            SetPermissions();    //Method to set permissions(Add,update,delete,print,email)
            LoadCombos(0);       // Method for loading comboboxes            
            LoadMessage();      // Method for loading messages 
            if (PintWorkPolicyID > 0)
            {
                MobjClsBLLWorkPolicy = new clsBLLWorkPolicy();
                MblnAddStatus = true;
                MintRecordCnt = MobjClsBLLWorkPolicy.RecCountNavigate();
                ClearControls();
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                BindingNavigatorPositionItem.Text = Convert.ToString(MintRecordCnt);
                MintCurrentRecCnt = MintRecordCnt;
                EnableDisableButtons(false);
                FillPolicyDetailGrid();
                btnPrint.Enabled = false;
                //LblStatus.Text = "Add new work policy";
                TmWorkPolicy.Enabled = true;
                BindingNavigatorAddNewItem.Enabled = false;
                FillPolicyDetails();
            }
            else
            {
                AddNewItem();
                FillPolicyDetails();
            }
            txtPolicyname.Select();
        }      

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewItem();
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveWorkPolicy())
            {
                txtPolicyname.Tag = MobjClsBLLWorkPolicy.clsDTOWorkPolicy.PolicyID;
                MintRecordCnt = MobjClsBLLWorkPolicy.RecCountNavigate();
                MintCurrentRecCnt = MintRecordCnt;
                if (MblnAddStatus)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    LblStatus.Text = "حفظ بنجاح";
                    //else
                        LblStatus.Text = " Saved Successfully";
                    MblnAddStatus = false;
                    Fill();
                }
                else
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    LblStatus.Text = "تحديث بنجاح";
                    //else
                        LblStatus.Text = " Updated Successfully";
                    EnableDisableButtons(false);
                    //SetNavigatorEnability();
                }

               
            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            MintRecordCnt = MobjClsBLLWorkPolicy.RecCountNavigate();

            if (MintRecordCnt > 0)
            {
                MblnAddStatus = false;
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                    MintCurrentRecCnt = 1;
            }
            else
            {
                MintRecordCnt = 0;
            }

            MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 10, out MsMessageBoxIcon);
            LblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            TmWorkPolicy.Enabled = true;
            BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
            Fill();
        }
        private void BtnClear_Click(object sender, EventArgs e)
        {
            AddNewItem();
        }
        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
           
            MintRecordCnt =  MobjClsBLLWorkPolicy.RecCountNavigate();
            if (MintRecordCnt > 0)
            {
                MblnAddStatus = false;
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                MintCurrentRecCnt = 1;
            }
            else
                MintRecordCnt = 0;

            MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 9, out MsMessageBoxIcon);
            LblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            TmWorkPolicy.Enabled = true;
            BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
            Fill();

        }
        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            MintRecordCnt = MobjClsBLLWorkPolicy.RecCountNavigate();
            if (MintRecordCnt > 0)
            {
                MblnAddStatus = false;
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                MintCurrentRecCnt = MintCurrentRecCnt + 1;

                if (MintCurrentRecCnt >= MintRecordCnt)
                    MintCurrentRecCnt = MintRecordCnt;
               
            }
            else
            {
                MintRecordCnt = 0;
            }
            MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 11, out MsMessageBoxIcon);
            LblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            TmWorkPolicy.Enabled = true;
            BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
            Fill();
        }
        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
          
            MintRecordCnt = MobjClsBLLWorkPolicy.RecCountNavigate();

            if (MintRecordCnt > 0)
            {
                MblnAddStatus = false;
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                MintCurrentRecCnt = MintRecordCnt;
            }
            else
                MintRecordCnt = 0;

            MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 12, out MsMessageBoxIcon);
            LblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            TmWorkPolicy.Enabled = true;
            BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
            Fill();
           
        }

        private void DgvPolicyConsequence_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (DgvPolicyConsequence.IsCurrentCellDirty)
            {
                if (DgvPolicyConsequence.CurrentCell != null)
                    DgvPolicyConsequence.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
        }
        private void DgvPolicyDetail_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (DgvPolicyDetail.IsCurrentCellDirty)
            {
                if (DgvPolicyDetail.CurrentCell != null)
                    DgvPolicyDetail.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
        }
        private void DgvPolicyDetail_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }
        private void DgvPolicyConsequence_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }              
        void EditingControlConsequence_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (DgvPolicyConsequence.CurrentCell.OwningColumn.Name == "Amount")
            {
                if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (!string.IsNullOrEmpty(DgvPolicyConsequence.EditingControl.Text) &&  (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true;
                }
            }
            if (DgvPolicyConsequence.CurrentCell.OwningColumn.Name == "AllowedDaysPerMonth")
            {
                if (!((Char.IsDigit(e.KeyChar)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,backspace(08)
                {
                    e.Handled = true;
                }
            }
        }

        private void DgvPolicyConsequence_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            this.DgvPolicyConsequence.EditingControl.KeyPress += new KeyPressEventHandler(EditingControlConsequence_KeyPress);
        }
        private void DgvPolicyConsequence_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                switch (e.ColumnIndex)
                {
                    case 4:
                        if (DgvPolicyConsequence.CurrentRow.Cells["LOP"] != null)
                        {
                            if (DgvPolicyConsequence.CurrentRow.Cells["LOP"].Value != DBNull.Value)
                            {
                                if (Convert.ToInt32(DgvPolicyConsequence.CurrentRow.Cells["LOP"].Value) == 1 || Convert.ToInt32(DgvPolicyConsequence.CurrentRow.Cells["LOP"].Value) == 2)
                                {
                                    DgvPolicyConsequence.CurrentRow.Cells["Casual"].Value = 3;
                                    DgvPolicyConsequence.CurrentRow.Cells["Amount"].Value = 0;
                                }
                            }

                        }
                        break;
                    case 5:
                        if (DgvPolicyConsequence.CurrentRow.Cells["Casual"] != null)
                        {
                            if (DgvPolicyConsequence.CurrentRow.Cells["Casual"].Value != DBNull.Value)
                            {
                                if (Convert.ToInt32(DgvPolicyConsequence.CurrentRow.Cells["Casual"].Value) == 1 || Convert.ToInt32(DgvPolicyConsequence.CurrentRow.Cells["Casual"].Value) == 2)
                                {
                                    DgvPolicyConsequence.CurrentRow.Cells["LOP"].Value = 3;
                                    DgvPolicyConsequence.CurrentRow.Cells["Amount"].Value = 0;
                                }
                            }

                        }
                        break;
                    case 6:
                        if (DgvPolicyConsequence.CurrentCell != null)
                            IsNumeric(false);

                        if (DgvPolicyConsequence.CurrentRow.Cells["Amount"] != null)
                        {
                            if (DgvPolicyConsequence.CurrentRow.Cells["Amount"].Value != DBNull.Value)
                            {
                                if (Convert.ToDecimal(DgvPolicyConsequence.CurrentRow.Cells["Amount"].Value) > 0)
                                {
                                    DgvPolicyConsequence.CurrentRow.Cells["Casual"].Value = 3;
                                    DgvPolicyConsequence.CurrentRow.Cells["LOP"].Value = 3;
                                }
                            }
                        }
                        break;

                }

            }
            catch (Exception ex)
            {
                MObjClsLogWriter.WriteLog("DgvPolicyConsequence_CellEndEdit()" + ex.Message, 1);
            }

        }



        private void BtnShiftPolicy_Click(object sender, EventArgs e)
        {

            try
            {
                using (FrmShiftPolicy objShiftPolicy = new FrmShiftPolicy())
                {
                    //objShiftPolicy.PiCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                    objShiftPolicy.PiShiftID = Convert.ToInt32(cboOffDay.SelectedValue);
                    objShiftPolicy.ShowDialog();
                }
                LoadCombos(2);
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on BtnShiftPolicy_Click " + this.Name + " " + Ex.Message.ToString(), 1);
            }
            
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPolicyname.Tag.ToInt32() > 0 && DeleteValidation())
                {
                    //MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr,4, out MsMessageBoxIcon);
                    //if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    //{
                    MobjClsBLLWorkPolicy.clsDTOWorkPolicy.PolicyID = Convert.ToInt32(txtPolicyname.Tag);
                    if (MobjClsBLLWorkPolicy.Delete())
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 4, out MsMessageBoxIcon);
                        LblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrPolicy.Enabled = true;
                        AddNewItem();
                    }
                    //}
                }
            }
            catch (System.Data.SqlClient.SqlException Ex)
            {
                string MstrMessageCommon = "";
                if (Ex.Number == (int)SqlErrorCodes.ForeignKeyErrorNumber)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    MstrMessageCommon = " التفاصيل موجودة في النظام. لا يمكن حذف";
                    //else
                        MstrMessageCommon = "Details exists in the sysytem.Cannot delete";
                }
                else
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //    MstrMessageCommon = " التفاصيل موجودة في النظام. لا يمكن حذف";
                    //else
                        MstrMessageCommon = "Details exists in the sysytem.Cannot delete";
                }

                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1, Log.LogSeverity.Error);

            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       
        private void DgvPolicyDetail_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int Mrowindex = DgvPolicyDetail.CurrentRow.Index;
                int MColindex = DgvPolicyDetail.CurrentCell.ColumnIndex;

                if (DgvPolicyDetail.CurrentRow.Cells["WorkingDay"].Value != DBNull.Value)
                {
                    if (Convert.ToString(DgvPolicyDetail.CurrentRow.Cells["WorkingDay"].Value) == "true" || Convert.ToInt32(DgvPolicyDetail.CurrentRow.Cells["WorkingDay"].Value) == 1)
                    {
                        if (Mrowindex != -1 && MColindex >= 0)
                        {
                            if (MColindex == 4 )// MColindex != 4 &&MColindex != 3 &&MColindex !=2 
                            {
                                if (e.Button == MouseButtons.Right)
                                {
                                    this.CopyDetailsContextMenuStrip.Show(this.DgvPolicyDetail, this.DgvPolicyDetail.PointToClient(Cursor.Position));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on DgvPolicyDetail_CellMouseClick " + this.Name + " " + Ex.Message.ToString(), 1);  
            }
        }

        /// <summary>
        /// Apply shift policies to all rows in grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void applyToAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int Mrowindex = DgvPolicyDetail.CurrentRow.Index;
                int MColindex = DgvPolicyDetail.CurrentCell.ColumnIndex;
                if (Mrowindex < 0) return;
                if (DgvPolicyDetail.Rows[Mrowindex].Cells[MColindex].Value != DBNull.Value)
                {
                    for (int i = 0; i < DgvPolicyDetail.Rows.Count; i++)
                    {
                        if (Convert.ToString(DgvPolicyDetail.Rows[i].Cells["WorkingDay"].Value) == "true" ||DgvPolicyDetail.Rows[i].Cells["WorkingDay"].Value.ToInt32() == 1)
                        {
                            if (Mrowindex != i)
                            {
                                DgvPolicyDetail.Rows[i].Cells["ShiftDetailID"].Value = DgvPolicyDetail.Rows[Mrowindex].Cells["ShiftDetailID"].Value;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on applyToAllToolStripMenuItem_Click " + this.Name + " " + Ex.Message.ToString(), 1);
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            BindingNavigatorSaveItem_Click(null, null);
        }

        /// <summary>
        /// Save policy 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (SaveWorkPolicy())
            {
                MblnChangeStatus = false;
                this.Close();
            }
        }

        private void DgvPolicyDetail_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {


            }
            catch (Exception ex)
            {
                MObjClsLogWriter.WriteLog("DgvPolicyConsequence_CellEndEdit()" + ex.Message, 1);
            }
        }
       
        /// <summary>
        /// Print policy details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = "WorkPolicy";
                ObjViewer.PiRecId = txtPolicyname.Tag.ToInt32();
                ObjViewer.PiFormID = (int)FormID.WorkPolicy;
                ObjViewer.ShowDialog();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in BtnPrint_Click() " + ex.Message);

            }
        }

        /// <summary>
        /// show confirmation message before closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmWorkPolicy_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            if (MblnChangeStatus)
            {
                // Checking the changes are not saved and shows warning to the user
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 8, out MsMessageBoxIcon);
               // MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.FormClosing, out MsMessageBoxIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim().Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MobjClsBLLWorkPolicy = null;
                    MObjClsLogWriter = null;
                    MObjClsNotification = null;
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// set shortcut keys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmWorkPolicy_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        BtnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());//Prev item
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());//Next item
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());//First item
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());//Last item
                        break;
                    case Keys.Control | Keys.P:
                        btnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.F1:
                        BtnHelp_Click(sender, new EventArgs());//help
                        break;
                }
            }
            catch (Exception)
            {
            }
        }
        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "WorkPolicy";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void txtPolicyname_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }
        private void DgvPolicyConsequence_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            ChangeStatus();
        }

        private void DgvPolicyDetail_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            ChangeStatus();
        }

        private void cboOffDay_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboOffDay_KeyDown(object sender, KeyEventArgs e)
        {
            cboOffDay.DroppedDown = false;
        }
        #endregion Events     
      
    }
}
