﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;

using System.Windows.Forms;
using Microsoft.VisualBasic;
namespace MyBooksERP
{
    /*****************************************************
   * Created By       : Arun
   * Creation Date    : 11 Apr 2012
   * Description      : Holiday Calendar
   * Modified by      : Siny
   * Modified date    : 16 Aug 2013
   * Description      : Tuning performance improvement 
   * FormID           : 113  
   * ***************************************************/

    public partial class FrmHolidayCalender : Form
    {
        #region Constructor
        public FrmHolidayCalender()
        {
            InitializeComponent();

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.HolidayCalendar, this);
        //}
        #endregion Constructor

        #region Declarations
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;               //To set Add Permission
        private bool MblnUpdatePermission = false;            //To set Update Permission
        private bool MblnDeletePermission = false;            //To set Delete Permission
        private bool MblnAddUpdatePermission = false;         //To set Add Update Permission

        private DateTime MdtFirstDate;         //First date of the selected month               
        private DateTime MdtLastDate;          //Last date of the selected month 
        private int MintFirstDateNo = 0;       //First day of the selected month 
        private int MintLastDateNo = 0;        //Last day of the selected month 
        private int MintDate = 0;              //Current date
        private int MintCol = 0;               //Current column number
        private int MintRow = 0;               //Current row number
        private int MintRows = 0;              //Total number of column according to total number of days in month
        private int MintFirstCol = 0;          //Column index of first day
        private int MintLastCol = 0;           //Column index of last day
        private int MintYear = 0;              //Selected year
        private int MintMonth = 0;             //Selected month
        private int MintHolidayType = 0;       //Holiday type
        string[] strOffDays=null;              //To set the offdays
        private bool MblndelFlag = false;      //For checking if payment exists

        private clsMessage ObjUserMessage = null;
        private clsBLLHolidayCalendar MobjclsBLLHolidayCalendar = null;
        #endregion Declarations

        #region Properties
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.HolidayCalendar);
                return this.ObjUserMessage;
            }
        }
        private clsBLLHolidayCalendar BLLHolidayCalendar
        {
            get
            {
                if (this.MobjclsBLLHolidayCalendar == null)
                    this.MobjclsBLLHolidayCalendar = new clsBLLHolidayCalendar();

                return this.MobjclsBLLHolidayCalendar;
            }
        }
        #endregion Properties

        #region Methods

        #region SetPermissions
        /// <summary>
        /// Function for setting permissions
        /// </summary>
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID >3 )
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.HolidayCalender, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
            {
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            }
            if (MblnAddPermission == true || MblnUpdatePermission == true)
            {
                MblnAddUpdatePermission = true;
            }
            dgvMonth.Enabled = MblnAddUpdatePermission;
        }
        #endregion SetPermissions

        #region LoadCombos
        /// <summary>
        /// Load the combos according to intType
        /// </summary>
        /// <param name="intType">
        /// intType=0->Load all the combos
        /// intType=1->Load company combo only
        ///  intType=2->Load month combo only
        /// </param>
        /// <returns></returns>
        private bool LoadCombos(int intType)
        {
            bool blnRetvalue = false;
            DataTable datCombos = new DataTable();
            try
            {

                if (intType == 0 || intType == 1)
                {
                    datCombos = BLLHolidayCalendar.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + " and CompanyID ="+ ClsCommonSettings.LoginCompanyID    +" )" });
                    cboCompany.ComboBox.ValueMember = "CompanyID";
                    cboCompany.ComboBox.DisplayMember = "CompanyName";
                    cboCompany.ComboBox.DataSource = datCombos;
                    blnRetvalue = true;
                    datCombos = null;
                }

                if (intType == 0 || intType == 2)
                {
                    datCombos = BLLHolidayCalendar.GetMonths(dtpYear.Value.Year.ToStringCustom());
                    CboMonth.ComboBox.ValueMember = "MonthID";
                    CboMonth.ComboBox.DisplayMember = "MonthName";
                    CboMonth.ComboBox.DataSource = datCombos;
                    blnRetvalue = true;
                    datCombos = null;
                }

            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
                blnRetvalue = false;
            }
            return blnRetvalue;
        }
        #endregion LoadCombos

        #region DrawGrid
        /// <summary>
        /// Gt an outline of calender according to days of the selected month
        /// </summary>
        private void DrawGrid()
        {
            try
            {
                if (CboMonth.SelectedIndex == -1)
                    return;
                MintYear = dtpYear.Value.Year.ToInt32();
                MintMonth = CboMonth.SelectedIndex + 1;

                MintFirstDateNo = 1;
                MintLastDateNo = DateTime.DaysInMonth(MintYear, MintMonth);
                MdtFirstDate = DateAndTime.DateSerial(MintYear, MintMonth, 1);
                MdtLastDate = DateAndTime.DateSerial(MintYear, MintMonth, MintLastDateNo);
                MintFirstCol = DateAndTime.Weekday(MdtFirstDate, FirstDayOfWeek.Sunday) - 1;
                MintLastCol = DateAndTime.Weekday(MdtLastDate.Date, FirstDayOfWeek.Sunday) - 1;
                MintRows = (DateAndTime.DateDiff(DateInterval.Weekday, MdtFirstDate, MdtLastDate, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1) + 1).ToInt32();
                //dgvMonth.RowCount = 0;
                dgvMonth.RowCount = 1;
                MintCol = MintFirstCol - 1;
                MintRow = 0;
                dgvMonth.Rows.Add();
                for (MintDate = MintFirstDateNo; MintDate <= MintLastDateNo; MintDate++)
                {
                    MintCol = MintCol + 1;
                    if (MintCol > 6)
                    {
                        dgvMonth.Rows.Add();
                        dgvMonth.Rows.Add();
                        MintRow = MintRow + 2;
                        MintCol = 0;
                    }
                    dgvMonth.Rows[MintRow].Cells[MintCol].Style.ForeColor = Color.Black;
                    dgvMonth.Rows[MintRow].Cells[MintCol].Value = MintDate;
                    dgvMonth.Rows[MintRow + 1].Cells[MintCol].Tag = DateAndTime.DateSerial(MintYear, MintMonth, MintDate).Day;
                }
            }
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1, Log.LogSeverity.Error);

            }
        }
        #endregion DrawGrid

        #region SizeGrid
        /// <summary>
        /// 1. Resize the height of main cells
        /// 2. Set offdays and holidays
        /// </summary>
        private void SizeGrid()
        {
            try
            {
                if (CboMonth.SelectedIndex == -1)
                    return;
                //Set the height and width of the odd rows
                int iRowHeight = 60;
                for (MintRow = 1; MintRow <= dgvMonth.Rows.Count - 1; MintRow++)
                {
                    dgvMonth.Rows[MintRow].Height = iRowHeight;
                    dgvMonth.Rows[MintRow - 1].Height = iRowHeight - 44;
                    MintRow = MintRow + 1;
                }

                MintYear = dtpYear.Value.Year;
                MintMonth = CboMonth.SelectedIndex + 1;
                //Get off days of the company
                if (cboCompany.ComboBox.SelectedValue.ToInt32() == 0)
                {
                    strOffDays = BLLHolidayCalendar.GetCompanyOffDay(ClsCommonSettings.LoginCompanyID);
                }
                else
                {
                    strOffDays = BLLHolidayCalendar.GetCompanyOffDay(cboCompany.ComboBox.SelectedValue.ToInt32());
                }

                for (MintRow = 0; MintRow <= dgvMonth.Rows.Count - 2; MintRow++)
                {
                    //Make even rows readonly
                    if (MintRow % 2 == 0)
                    {
                        for (MintCol = 0; MintCol <= 6; MintCol++)
                        {
                            dgvMonth[MintCol, MintRow].ReadOnly = true;
                        }
                    }
                    dgvMonth.Rows[MintRow].DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8, FontStyle.Bold);
                    MintRow = MintRow + 1;
                    dgvMonth.AllowUserToResizeColumns = false;
                    dgvMonth.AllowUserToResizeRows = false;
                    dgvMonth.AllowUserToAddRows = false;
                    dgvMonth.AllowUserToOrderColumns = false;
                }
                //For getting dates of prev month
                for (int i = 0; i <= MintFirstCol - 1; i++)
                {
                    dgvMonth[i, 1].ReadOnly = true;

                    dgvMonth.Rows[0].Cells[i].Style.ForeColor = Color.DarkGray;
                    dgvMonth.Rows[0].Cells[i].Value = DateAndTime.DateSerial(MintYear, MintMonth, 1).AddDays(-MintFirstCol + i).Day;

                    dgvMonth.Rows[1].Cells[i].Tag = -1;
                    dgvMonth.Rows[0].Cells[i].Tag = -1;
                }
                //For getting dates of next month
                MintRows = dgvMonth.RowCount - 1;
                int j = 0;
                for (int i = MintLastCol + 1; i <= 6; i++)
                {
                    dgvMonth[i, MintRows].ReadOnly = true;
                    dgvMonth.Rows[MintRows - 1].Cells[i].Style.ForeColor = Color.DarkGray;
                    j = j + 1;
                    dgvMonth.Rows[MintRows - 1].Cells[i].Value = j;
                    dgvMonth.Rows[MintRows - 1].Cells[i].Tag = 32;
                    dgvMonth.Rows[MintRows].Cells[i].Tag = 32;
                }
                //Set off days and holidays
                for (MintRow = 1; MintRow <= dgvMonth.RowCount - 1; MintRow += 2)
                {
                    for (MintCol = 0; MintCol <= 6; MintCol++)
                    {
                        if (dgvMonth[MintCol, MintRow].Tag.ToInt32() > 0 && dgvMonth[MintCol, MintRow].Tag.ToInt32() < 32)
                        {
                            int iCmpID = 0;
                            if (cboCompany.ComboBox.SelectedValue.ToInt32() == 0)
                            {
                                iCmpID = ClsCommonSettings.LoginCompanyID;
                            }
                            else
                            {
                                iCmpID = cboCompany.ComboBox.SelectedValue.ToInt32();
                            }
                            string strRemarks = "";
                            string strColor = "";
                            int intHolidayType = 0;
                            string strDate = DateAndTime.DateSerial(MintYear, MintMonth, dgvMonth[MintCol, MintRow].Tag.ToInt32()).ToString("dd-MMM-yyyy");
                            dgvMonth.Rows[MintRow].Cells[MintCol].Value = "";
                            //Fetches all the company holidays and details and set remarks and colour
                            if (BLLHolidayCalendar.GetCompanyHolidayDetails(iCmpID, strDate, ref strRemarks, ref strColor, ref intHolidayType))
                            {
                                Application.DoEvents();  //Modified to make sure that prev value is cleared before assigning new values
                                dgvMonth.Rows[MintRow].Cells[MintCol].Value = strRemarks;
                                MintHolidayType = intHolidayType;
                                dgvMonth.Rows[MintRow].Cells[MintCol].Style.BackColor = Color.FromName(strColor);
                            }

                            //Set Company off days in 'coral' colour
                            if (strOffDays.Contains((MintCol + 1).ToString().Trim()))
                            {
                                dgvMonth.Rows[MintRow].Cells[MintCol].Style.BackColor = Color.Coral;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1, Log.LogSeverity.Error);

            }
        }
        #endregion SizeGrid

        #region PopulateContextMenu
        /// <summary>
        /// Fill context menu strip with holiday types
        /// </summary>
        private void PopulateContextMenu()
        {
            try
            {
                DataTable dtHolidayTypes = BLLHolidayCalendar.GetHolidayTypes();
                cmsHolidayType.Items.Clear();
                Bitmap newBitmap;
                Graphics g;
                if (dtHolidayTypes.Rows.Count > 0)
                {
                    for (int i = 0; i <= dtHolidayTypes.Rows.Count - 1; i++)
                    {
                        newBitmap = new Bitmap(8, 8, PixelFormat.Format16bppRgb555);
                        g = Graphics.FromImage(newBitmap);
                        g.FillRectangle(new SolidBrush(Color.FromName(dtHolidayTypes.Rows[i]["Color"].ToStringCustom())), new Rectangle(0, 0, 8, 8));
                        cmsHolidayType.Items.Add(dtHolidayTypes.Rows[i]["HolidayType"].ToStringCustom(), newBitmap);
                    }
                }
                newBitmap = new Bitmap(8, 8, PixelFormat.Format16bppRgb555);
                g = Graphics.FromImage(newBitmap);
                g.FillRectangle(new SolidBrush(Color.White), new Rectangle(0, 0, 8, 8));
                if (MblnDeletePermission)
                {
                    cmsHolidayType.Items.Add("Remove Holiday", newBitmap);
                }
            }
            catch
            {

            }
        }
        #endregion PopulateContextMenu

        #region FillParameters
        /// <summary>
        /// Fill holiday parameters according to row and column indices
        /// </summary>
        /// <param name="iColumnIndex">column index</param>
        /// <param name="intRowIndex">row index</param>
        private void FillParameters(int iColumnIndex, int intRowIndex)
        {
            this.BLLHolidayCalendar.DTOHolidayCalendar.CompanyID = cboCompany.ComboBox.SelectedValue.ToInt32();
            this.BLLHolidayCalendar.DTOHolidayCalendar.HolidayTypeID = 0;
            this.BLLHolidayCalendar.DTOHolidayCalendar.HolidayDate = DateAndTime.DateSerial(MintYear, MintMonth, dgvMonth[iColumnIndex, intRowIndex].Tag.ToInt32()).ToString("dd-MMM-yyyy");
            this.BLLHolidayCalendar.DTOHolidayCalendar.Remarks = dgvMonth[iColumnIndex, intRowIndex].Value.ToStringCustom();

        }
        #endregion FillParameters

        #region SaveHoliday
        /// <summary>
        /// Insert holiday details
        /// </summary>
        /// <param name="iColumnIndex">Column index</param>
        /// <param name="intRowIndex">row index</param>
        /// <returns>success/failure</returns>
        private bool SaveHoliday(int iColumnIndex, int intRowIndex)
        {
            try
            {
                bool blnRetValue = false;
                FillParameters(iColumnIndex, intRowIndex);
                if (this.BLLHolidayCalendar.InsertHoiday())
                {
                    blnRetValue = true;
                }
                return blnRetValue;
            }
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1, Log.LogSeverity.Error);
                return false;
            }
        }
        #endregion SaveHoliday

        #endregion Methods

        #region Events
        private void FrmHolidayCalender_Load(object sender, EventArgs e)
        {
            SetPermissions();
            LoadCombos(1);
            //1 day substracted from current date if current month is february and year is leap year to avoid error due to leap year
            dtpYear.Value = (DateTime.Today.Month == 2 && DateTime.Today.Day == 29) ? DateTime.Today.AddDays(-1) : DateTime.Today;
            LoadCombos(2);
            CboMonth.SelectedIndex = dtpYear.Value.Month - 1;
            //cboCompany.Enabled = false;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            if (CboMonth.SelectedIndex < 0)
                return;

            dgvMonth.EndEdit();
            if (CboMonth.SelectedIndex == 0)
            {
                if (dtpYear.Value.Date.Year != 1753)
                {
                    dtpYear.Value = dtpYear.Value.Date.AddYears(-1);
                    CboMonth.SelectedIndex = 11;
                }
            }
            else
            {
                CboMonth.SelectedIndex = CboMonth.SelectedIndex - 1;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            dgvMonth.EndEdit();
            if (CboMonth.SelectedIndex == 11)
            {
                if (dtpYear.Value.Date.Year != 9998)
                {
                    dtpYear.Value = dtpYear.Value.Date.AddYears(1);
                    CboMonth.SelectedIndex = 0;
                }
            }
            else
            {
                CboMonth.SelectedIndex = CboMonth.SelectedIndex + 1;
            }
        }


        private void dgvMonth_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            if (e.RowIndex < 0 || e.RowIndex % 2 == 0)
                return;
            try
            {
                //If clicked on next month date show prev month calender
                if (dgvMonth.Rows[e.RowIndex].Cells[e.ColumnIndex].Tag.ToInt32() == 32)
                {
                    btnNext.PerformClick();
                }
                //If clicked on prev month date show prev month calender
                else if (dgvMonth.Rows[e.RowIndex].Cells[e.ColumnIndex].Tag.ToInt32() == -1)
                {
                    btnPrevious.PerformClick();
                }
                else//To avoid black color in cell
                {
                    if (dgvMonth.CurrentCell.Style.SelectionBackColor.Name == "0")
                    {
                        int iCmpID = 0;
                        if (cboCompany.ComboBox.SelectedValue.ToInt32() == 0)
                        {
                            iCmpID = ClsCommonSettings.LoginCompanyID;
                        }
                        else
                        {
                            iCmpID = cboCompany.ComboBox.SelectedValue.ToInt32();
                        }
                        string strRemarks = "";
                        string strColor = "";
                        int intHolidayType = 0;
                        string strDate = DateAndTime.DateSerial(MintYear, MintMonth, dgvMonth[e.ColumnIndex, e.RowIndex].Tag.ToInt32()).ToString("dd-MMM-yyyy");
                        if (BLLHolidayCalendar.GetCompanyHolidayDetails(iCmpID, strDate, ref strRemarks, ref strColor, ref intHolidayType))
                        {
                            if (strColor != "")
                                dgvMonth.CurrentCell.Style.BackColor = Color.FromName(strColor);
                            else if (strOffDays.Contains((e.ColumnIndex + 1).ToString().Trim()))
                            {
                                dgvMonth.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Coral;
                            }
                            else
                                dgvMonth.CurrentCell.Style.BackColor = Color.White;
                        }
                        else if (strOffDays.Contains((e.ColumnIndex + 1).ToString().Trim()))
                        {
                            dgvMonth.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Coral;
                        }
                        else
                        {
                            dgvMonth.CurrentCell.Style.BackColor = Color.White;
                        }
                    }
                }
            }
            catch
            {
            }
            //if (e.RowIndex % 2 == 0)
            //{
            //    dgvMonth.CurrentCell.Style.SelectionBackColor = Color.Transparent;
            //    dgvMonth.CurrentCell.Style.SelectionForeColor = Color.Transparent;
            //}
            //else
            //{
            //   // dgvMonth.CurrentCell.Style.SelectionBackColor = Color.Linen;
            //    dgvMonth.CurrentCell.Style.SelectionForeColor = Color.Black;
            //}
        }

        private void dgvMonth_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvMonth.CurrentCell == null)
                return;
            try
            {
                DateTime curDate = DateAndTime.DateSerial(MintYear, MintMonth, dgvMonth[dgvMonth.CurrentCell.ColumnIndex, dgvMonth.CurrentCell.RowIndex].Tag.ToInt32());
                if (dgvMonth[dgvMonth.CurrentCell.ColumnIndex, dgvMonth.CurrentCell.RowIndex].Value.ToStringCustom() == string.Empty)
                {
                    dgvMonth[dgvMonth.CurrentCell.ColumnIndex, dgvMonth.CurrentCell.RowIndex].Value = "";
                }
                if (cboCompany.ComboBox.SelectedValue.ToInt32() == 0)
                    return;
                //Check if payment exists that month
                if (BLLHolidayCalendar.CheckPaymentExists(cboCompany.ComboBox.SelectedValue.ToInt32(), curDate.ToString("dd-MMM-yyyy")))
                {
                    MblndelFlag = true;
                }
                else
                {
                    MblndelFlag = false;
                }
                if (dgvMonth[dgvMonth.CurrentCell.ColumnIndex, dgvMonth.CurrentCell.RowIndex].Value.ToStringCustom() != string.Empty)
                {
                    //if length of entered text is greater than 200 message shows that text will be trimmed
                    if (dgvMonth[dgvMonth.CurrentCell.ColumnIndex, dgvMonth.CurrentCell.RowIndex].Value.ToStringCustom().Length > 200)
                    {
                        UserMessage.ShowMessage(7352);
                        int intlength = dgvMonth[dgvMonth.CurrentCell.ColumnIndex, dgvMonth.CurrentCell.RowIndex].Value.ToStringCustom().Length;
                        dgvMonth[dgvMonth.CurrentCell.ColumnIndex, dgvMonth.CurrentCell.RowIndex].Value = dgvMonth[dgvMonth.CurrentCell.ColumnIndex, dgvMonth.CurrentCell.RowIndex].Value.ToStringCustom().Remove(200, intlength - 200);
                    }
                    if (BLLHolidayCalendar.CheckHolidayExists(cboCompany.ComboBox.SelectedValue.ToInt32(), curDate.ToString("dd-MMM-yyyy")))
                    {
                        BLLHolidayCalendar.UpdateHoliday(cboCompany.ComboBox.SelectedValue.ToInt32(), curDate.ToString("dd-MMM-yyyy"), 0, dgvMonth[dgvMonth.CurrentCell.ColumnIndex, dgvMonth.CurrentCell.RowIndex].Value.ToStringCustom());
                    }
                    else
                    {
                        if (MblndelFlag == false)
                        {
                            SaveHoliday(dgvMonth.CurrentCell.ColumnIndex, dgvMonth.CurrentCell.RowIndex);
                        }
                        else
                        {
                            //Case when payment exists
                            dgvMonth[dgvMonth.CurrentCell.ColumnIndex, dgvMonth.CurrentCell.RowIndex].Value = "";
                            cmsHolidayType.SendToBack();
                            UserMessage.ShowMessage(7351);
                            cmsHolidayType.BringToFront();
                        }
                    }
                }
                else//Case when an existing text is removed
                {
                    if (MblndelFlag == false)
                    {
                        if (this.BLLHolidayCalendar.DeleteHoliday(cboCompany.ComboBox.SelectedValue.ToInt32(), curDate.ToString("dd-MMM-yyyy")))
                        {
                            if (!strOffDays.Contains((dgvMonth.CurrentCell.ColumnIndex + 1).ToString()))//If the cell selected is offday colour should not change to white
                                dgvMonth[dgvMonth.CurrentCell.ColumnIndex, dgvMonth.CurrentCell.RowIndex].Style.BackColor = Color.White;
                        }
                    }
                    else
                    {
                        cmsHolidayType.SendToBack();
                        UserMessage.ShowMessage(7351);
                        cmsHolidayType.BringToFront();
                    }
                }
            }
            catch { }
        }

        private void dgvMonth_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == 0)
                return;
            //if (e.RowIndex % 2 == 0)
            //{
            //    dgvMonth.CurrentCell.Style.SelectionBackColor = Color.Transparent;
            //    dgvMonth.CurrentCell.Style.SelectionForeColor = Color.Transparent;
            //}
            //else
            //{
            //    //dgvMonth.CurrentCell.Style.SelectionBackColor = Color.Linen;
            //    dgvMonth.CurrentCell.Style.SelectionForeColor = Color.Black;
            //}
        }

        private void dgvMonth_CellStyleContentChanged(object sender, DataGridViewCellStyleContentChangedEventArgs e)
        {
            try
            {
                if (MintRow <= 0 && MintCol >= 7)
                    return;
                if (MintRow >= dgvMonth.Rows.Count || MintCol >= dgvMonth.Rows[MintRow].Cells.Count)
                    return;
                if (dgvMonth.Rows[MintRow].Cells[MintCol].Tag.ToInt32() >= 32 && dgvMonth.Rows[MintRow].Cells[MintCol].Tag.ToInt32() <= 0)
                    return;
                if (cboCompany.ComboBox.SelectedValue.ToInt32() == 0)
                    return;
                DateTime curDate = DateAndTime.DateSerial(MintYear, MintMonth, dgvMonth[MintCol, MintRow].Tag.ToInt32());
                int iCmpID = cboCompany.ComboBox.SelectedValue.ToInt32();
                if (BLLHolidayCalendar.CheckHolidayExists(iCmpID, curDate.ToString("dd-MMM-yyyy")))
                {
                    //In case holidaytype is changed
                    BLLHolidayCalendar.UpdateHoliday(iCmpID, curDate.ToString("dd-MMM-yyyy"), MintHolidayType, dgvMonth[MintCol, MintRow].Value.ToStringCustom());
                }
            }
            catch { }
        }

        private void dgvMonth_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                //Populate the holiday type on right click
                if (e.Button == MouseButtons.Right)
                {
                    //Get the row and column indices of the cell on which mouse is clicked 
                    DataGridView.HitTestInfo htinfo = dgvMonth.HitTest(e.X, e.Y);
                    MintRow = htinfo.RowIndex;
                    MintCol = htinfo.ColumnIndex;
                    //If the selected cell is the cell with date, right click is disabled
                    if (MintRow % 2 != 0 && dgvMonth.Rows[MintRow].Cells[MintCol].Value.ToStringCustom() != string.Empty)
                        PopulateContextMenu();
                    else
                        cmsHolidayType.Items.Clear();
                }
            }
            catch { }
        }
        private void dgvMonth_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvMonth.IsCurrentCellDirty)
                {
                    dgvMonth.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }

        private void dgvMonth_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }
        private void dtpYear_ValueChanged(object sender, EventArgs e)
        {
            DrawGrid();
            SizeGrid();
            dgvMonth.ClearSelection();
        }

        private void CboMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            DrawGrid();
            SizeGrid();
            dgvMonth.ClearSelection();
        }

        private void cmsHolidayType_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Text == "Remove Holiday")
            {
                if (cboCompany.SelectedIndex == -1)
                    return;
                if (cboCompany.ComboBox.SelectedValue.ToInt32() == 0)
                    return;
                DateTime curDate = DateAndTime.DateSerial(MintYear, MintMonth, dgvMonth[MintCol, MintRow].Tag.ToInt32());

                if (BLLHolidayCalendar.CheckPaymentExists(cboCompany.ComboBox.SelectedValue.ToInt32(), curDate.ToString("dd-MMM-yyyy")))
                {
                    MblndelFlag = true;
                }
                else
                {
                    MblndelFlag = false;
                }
                if (MblndelFlag == false)
                {
                    if (BLLHolidayCalendar.DeleteHoliday(cboCompany.ComboBox.SelectedValue.ToInt32(), curDate.ToString("dd-MMM-yyyy")))
                    {
                        DrawGrid();
                        SizeGrid();
                        dgvMonth.ClearSelection();
                    }
                    return;
                }
                else
                {
                    cmsHolidayType.SendToBack();
                    UserMessage.ShowMessage(7351);
                    cmsHolidayType.BringToFront();
                }
            }
            string strColr = "";
            int intHolidayTypeID = 0;
            string strHolidayType = e.ClickedItem.Text.Trim();
            if (BLLHolidayCalendar.GetHolidayTypeDetails(strHolidayType, ref  strColr, ref  intHolidayTypeID))
            {
                MintHolidayType = intHolidayTypeID;
                if (MintRow % 2 == 0)
                    return;
                if (dgvMonth[MintCol, MintRow].Value.ToStringCustom() != string.Empty)
                {
                    dgvMonth[MintCol, MintRow].Style.BackColor = Color.FromName(strColr);
                }
            }
            DrawGrid();
            SizeGrid();
            dgvMonth.ClearSelection();     
        }

        private void btnHolidayType_Click(object sender, EventArgs e)
        {
            using (FrmHolidayType ObjHolidayType = new FrmHolidayType())
            {
                ObjHolidayType.ShowDialog();
            }
            DrawGrid();
            SizeGrid();
            dgvMonth.ClearSelection();
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            DrawGrid();
            SizeGrid();
            dgvMonth.ClearSelection();
        }

        #endregion Events       
    }
}
