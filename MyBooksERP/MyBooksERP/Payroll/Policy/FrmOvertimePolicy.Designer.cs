﻿namespace MyBooksERP
{
    partial class FrmOvertimePolicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lblExclude;
            System.Windows.Forms.Label Label5;
            System.Windows.Forms.Label Label4;
            System.Windows.Forms.Label Label2;
            System.Windows.Forms.Label ProjectManagerLabel;
            System.Windows.Forms.Label DescriptionLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOvertimePolicy));
            this.PolicyMasterBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnLeaveTypes = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.GrpMain = new System.Windows.Forms.GroupBox();
            this.dgvParticulars = new DemoClsDataGridview.ClsDataGirdView();
            this.AdditionDeductionID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SelectedVal = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.AdditionDeduction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblWorkingHoursOrYear = new System.Windows.Forms.Label();
            this.chkCalculationPercentage = new System.Windows.Forms.CheckBox();
            this.txtCalculationPercent = new System.Windows.Forms.TextBox();
            this.chkRateOnly = new System.Windows.Forms.CheckBox();
            this.rdbActualMonth = new System.Windows.Forms.RadioButton();
            this.txtOverTimeRate = new System.Windows.Forms.TextBox();
            this.rdbCompanyBased = new System.Windows.Forms.RadioButton();
            this.Label1 = new System.Windows.Forms.Label();
            this.cboCalculationBased = new System.Windows.Forms.ComboBox();
            this.txtWorkingHoursOrYear = new System.Windows.Forms.TextBox();
            this.txtOTPolicyName = new System.Windows.Forms.TextBox();
            this.SelectValue = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.StatusStripOTPolicy = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.errProOTPolicy = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.AddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Particulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            lblExclude = new System.Windows.Forms.Label();
            Label5 = new System.Windows.Forms.Label();
            Label4 = new System.Windows.Forms.Label();
            Label2 = new System.Windows.Forms.Label();
            ProjectManagerLabel = new System.Windows.Forms.Label();
            DescriptionLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PolicyMasterBindingNavigator)).BeginInit();
            this.PolicyMasterBindingNavigator.SuspendLayout();
            this.GrpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParticulars)).BeginInit();
            this.StatusStripOTPolicy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errProOTPolicy)).BeginInit();
            this.SuspendLayout();
            // 
            // lblExclude
            // 
            lblExclude.AutoSize = true;
            lblExclude.Location = new System.Drawing.Point(10, 107);
            lblExclude.Name = "lblExclude";
            lblExclude.Size = new System.Drawing.Size(101, 13);
            lblExclude.TabIndex = 42;
            lblExclude.Text = "Exclude from Gross ";
            // 
            // Label5
            // 
            Label5.AutoSize = true;
            Label5.Location = new System.Drawing.Point(10, 257);
            Label5.Name = "Label5";
            Label5.Size = new System.Drawing.Size(117, 13);
            Label5.TabIndex = 17;
            Label5.Text = "Calculation Percentage";
            // 
            // Label4
            // 
            Label4.AutoSize = true;
            Label4.Location = new System.Drawing.Point(10, 23);
            Label4.Name = "Label4";
            Label4.Size = new System.Drawing.Size(81, 13);
            Label4.TabIndex = 15;
            Label4.Text = "Calculation Day";
            // 
            // Label2
            // 
            Label2.AutoSize = true;
            Label2.Location = new System.Drawing.Point(10, 333);
            Label2.Name = "Label2";
            Label2.Size = new System.Drawing.Size(75, 13);
            Label2.TabIndex = 14;
            Label2.Text = "Overtime Rate";
            // 
            // ProjectManagerLabel
            // 
            ProjectManagerLabel.AutoSize = true;
            ProjectManagerLabel.Location = new System.Drawing.Point(10, 80);
            ProjectManagerLabel.Name = "ProjectManagerLabel";
            ProjectManagerLabel.Size = new System.Drawing.Size(107, 13);
            ProjectManagerLabel.TabIndex = 7;
            ProjectManagerLabel.Text = "Calculation Based on";
            // 
            // DescriptionLabel
            // 
            DescriptionLabel.AutoSize = true;
            DescriptionLabel.Location = new System.Drawing.Point(10, 54);
            DescriptionLabel.Name = "DescriptionLabel";
            DescriptionLabel.Size = new System.Drawing.Size(84, 13);
            DescriptionLabel.TabIndex = 6;
            DescriptionLabel.Text = "OT Policy Name";
            // 
            // PolicyMasterBindingNavigator
            // 
            this.PolicyMasterBindingNavigator.AddNewItem = null;
            this.PolicyMasterBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.PolicyMasterBindingNavigator.DeleteItem = null;
            this.PolicyMasterBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorSaveItem,
            this.btnClear,
            this.btnPrint,
            this.btnEmail,
            this.ToolStripSeparator2,
            this.btnLeaveTypes,
            this.ToolStripSeparator1,
            this.btnHelp});
            this.PolicyMasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.PolicyMasterBindingNavigator.MoveFirstItem = null;
            this.PolicyMasterBindingNavigator.MoveLastItem = null;
            this.PolicyMasterBindingNavigator.MoveNextItem = null;
            this.PolicyMasterBindingNavigator.MovePreviousItem = null;
            this.PolicyMasterBindingNavigator.Name = "PolicyMasterBindingNavigator";
            this.PolicyMasterBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.PolicyMasterBindingNavigator.Size = new System.Drawing.Size(404, 25);
            this.PolicyMasterBindingNavigator.TabIndex = 301;
            this.PolicyMasterBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save Data";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Visible = false;
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            this.ToolStripSeparator2.Visible = false;
            // 
            // btnLeaveTypes
            // 
            this.btnLeaveTypes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLeaveTypes.Image = global::MyBooksERP.Properties.Resources.Leave1;
            this.btnLeaveTypes.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLeaveTypes.Name = "btnLeaveTypes";
            this.btnLeaveTypes.Size = new System.Drawing.Size(23, 22);
            this.btnLeaveTypes.Text = "Leave Type";
            this.btnLeaveTypes.Visible = false;
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            this.ToolStripSeparator1.Visible = false;
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 22);
            this.btnHelp.Text = "He&lp";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // GrpMain
            // 
            this.GrpMain.Controls.Add(this.dgvParticulars);
            this.GrpMain.Controls.Add(lblExclude);
            this.GrpMain.Controls.Add(this.lblWorkingHoursOrYear);
            this.GrpMain.Controls.Add(this.chkCalculationPercentage);
            this.GrpMain.Controls.Add(this.txtCalculationPercent);
            this.GrpMain.Controls.Add(Label5);
            this.GrpMain.Controls.Add(this.chkRateOnly);
            this.GrpMain.Controls.Add(this.rdbActualMonth);
            this.GrpMain.Controls.Add(Label4);
            this.GrpMain.Controls.Add(this.txtOverTimeRate);
            this.GrpMain.Controls.Add(this.rdbCompanyBased);
            this.GrpMain.Controls.Add(Label2);
            this.GrpMain.Controls.Add(this.Label1);
            this.GrpMain.Controls.Add(this.cboCalculationBased);
            this.GrpMain.Controls.Add(ProjectManagerLabel);
            this.GrpMain.Controls.Add(this.txtWorkingHoursOrYear);
            this.GrpMain.Controls.Add(DescriptionLabel);
            this.GrpMain.Controls.Add(this.txtOTPolicyName);
            this.GrpMain.Location = new System.Drawing.Point(5, 27);
            this.GrpMain.Name = "GrpMain";
            this.GrpMain.Size = new System.Drawing.Size(393, 362);
            this.GrpMain.TabIndex = 4;
            this.GrpMain.TabStop = false;
            // 
            // dgvParticulars
            // 
            this.dgvParticulars.AddNewRow = false;
            this.dgvParticulars.AllowUserToAddRows = false;
            this.dgvParticulars.AllowUserToDeleteRows = false;
            this.dgvParticulars.AlphaNumericCols = new int[0];
            this.dgvParticulars.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvParticulars.CapsLockCols = new int[0];
            this.dgvParticulars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvParticulars.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AdditionDeductionID,
            this.SelectedVal,
            this.AdditionDeduction});
            this.dgvParticulars.DecimalCols = new int[0];
            this.dgvParticulars.HasSlNo = false;
            this.dgvParticulars.LastRowIndex = 0;
            this.dgvParticulars.Location = new System.Drawing.Point(150, 107);
            this.dgvParticulars.Name = "dgvParticulars";
            this.dgvParticulars.NegativeValueCols = new int[0];
            this.dgvParticulars.NumericCols = new int[0];
            this.dgvParticulars.RowHeadersWidth = 20;
            this.dgvParticulars.Size = new System.Drawing.Size(237, 108);
            this.dgvParticulars.TabIndex = 2;
            this.dgvParticulars.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvParticulars_CellValueChanged);
            this.dgvParticulars.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvParticulars_CurrentCellDirtyStateChanged);
            this.dgvParticulars.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvParticulars_DataError);
            // 
            // AdditionDeductionID
            // 
            this.AdditionDeductionID.HeaderText = "Column1";
            this.AdditionDeductionID.Name = "AdditionDeductionID";
            this.AdditionDeductionID.Visible = false;
            // 
            // SelectedVal
            // 
            this.SelectedVal.HeaderText = "";
            this.SelectedVal.Name = "SelectedVal";
            this.SelectedVal.Width = 30;
            // 
            // AdditionDeduction
            // 
            this.AdditionDeduction.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AdditionDeduction.HeaderText = "Particulars";
            this.AdditionDeduction.Name = "AdditionDeduction";
            // 
            // lblWorkingHoursOrYear
            // 
            this.lblWorkingHoursOrYear.AutoSize = true;
            this.lblWorkingHoursOrYear.Location = new System.Drawing.Point(10, 284);
            this.lblWorkingHoursOrYear.Name = "lblWorkingHoursOrYear";
            this.lblWorkingHoursOrYear.Size = new System.Drawing.Size(102, 13);
            this.lblWorkingHoursOrYear.TabIndex = 18;
            this.lblWorkingHoursOrYear.Text = "Working Hours/Day";
            // 
            // chkCalculationPercentage
            // 
            this.chkCalculationPercentage.AutoSize = true;
            this.chkCalculationPercentage.Location = new System.Drawing.Point(150, 230);
            this.chkCalculationPercentage.Name = "chkCalculationPercentage";
            this.chkCalculationPercentage.Size = new System.Drawing.Size(197, 17);
            this.chkCalculationPercentage.TabIndex = 3;
            this.chkCalculationPercentage.Text = "Calculation Percentage is applicable";
            this.chkCalculationPercentage.UseVisualStyleBackColor = true;
            this.chkCalculationPercentage.CheckedChanged += new System.EventHandler(this.chkCalculationPercentage_CheckedChanged);
            // 
            // txtCalculationPercent
            // 
            this.txtCalculationPercent.BackColor = System.Drawing.SystemColors.Window;
            this.txtCalculationPercent.Location = new System.Drawing.Point(150, 257);
            this.txtCalculationPercent.MaxLength = 5;
            this.txtCalculationPercent.Name = "txtCalculationPercent";
            this.txtCalculationPercent.Size = new System.Drawing.Size(197, 20);
            this.txtCalculationPercent.TabIndex = 3;
            this.txtCalculationPercent.TextChanged += new System.EventHandler(this.txtCalculationPercent_TextChanged);
            this.txtCalculationPercent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // chkRateOnly
            // 
            this.chkRateOnly.AutoSize = true;
            this.chkRateOnly.Location = new System.Drawing.Point(150, 310);
            this.chkRateOnly.Name = "chkRateOnly";
            this.chkRateOnly.Size = new System.Drawing.Size(71, 17);
            this.chkRateOnly.TabIndex = 7;
            this.chkRateOnly.Text = "Rate only";
            this.chkRateOnly.UseVisualStyleBackColor = true;
            this.chkRateOnly.CheckedChanged += new System.EventHandler(this.chkRateOnly_CheckedChanged);
            // 
            // rdbActualMonth
            // 
            this.rdbActualMonth.AutoSize = true;
            this.rdbActualMonth.Location = new System.Drawing.Point(273, 23);
            this.rdbActualMonth.Name = "rdbActualMonth";
            this.rdbActualMonth.Size = new System.Drawing.Size(88, 17);
            this.rdbActualMonth.TabIndex = 7;
            this.rdbActualMonth.TabStop = true;
            this.rdbActualMonth.Text = "Actual Month";
            this.rdbActualMonth.UseVisualStyleBackColor = true;
            this.rdbActualMonth.CheckedChanged += new System.EventHandler(this.rdbActualMonth_CheckedChanged);
            // 
            // txtOverTimeRate
            // 
            this.txtOverTimeRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtOverTimeRate.Location = new System.Drawing.Point(150, 333);
            this.txtOverTimeRate.MaxLength = 8;
            this.txtOverTimeRate.Name = "txtOverTimeRate";
            this.txtOverTimeRate.Size = new System.Drawing.Size(199, 20);
            this.txtOverTimeRate.TabIndex = 8;
            this.txtOverTimeRate.TextChanged += new System.EventHandler(this.txtOverTimeRate_TextChanged);
            this.txtOverTimeRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // rdbCompanyBased
            // 
            this.rdbCompanyBased.AutoSize = true;
            this.rdbCompanyBased.Location = new System.Drawing.Point(150, 23);
            this.rdbCompanyBased.Name = "rdbCompanyBased";
            this.rdbCompanyBased.Size = new System.Drawing.Size(117, 17);
            this.rdbCompanyBased.TabIndex = 5;
            this.rdbCompanyBased.TabStop = true;
            this.rdbCompanyBased.Text = "Based on Company";
            this.rdbCompanyBased.UseVisualStyleBackColor = true;
            this.rdbCompanyBased.CheckedChanged += new System.EventHandler(this.rdbCompanyBased_CheckedChanged);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Label1.Location = new System.Drawing.Point(3, -2);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(122, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Overtime Policy Info";
            // 
            // cboCalculationBased
            // 
            this.cboCalculationBased.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCalculationBased.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCalculationBased.BackColor = System.Drawing.SystemColors.Info;
            this.cboCalculationBased.DropDownHeight = 134;
            this.cboCalculationBased.FormattingEnabled = true;
            this.cboCalculationBased.IntegralHeight = false;
            this.cboCalculationBased.Location = new System.Drawing.Point(150, 80);
            this.cboCalculationBased.MaxDropDownItems = 10;
            this.cboCalculationBased.Name = "cboCalculationBased";
            this.cboCalculationBased.Size = new System.Drawing.Size(197, 21);
            this.cboCalculationBased.TabIndex = 1;
            this.cboCalculationBased.SelectedIndexChanged += new System.EventHandler(this.cboCalculationBased_SelectedIndexChanged);
            // 
            // txtWorkingHoursOrYear
            // 
            this.txtWorkingHoursOrYear.BackColor = System.Drawing.SystemColors.Info;
            this.txtWorkingHoursOrYear.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtWorkingHoursOrYear.Location = new System.Drawing.Point(150, 284);
            this.txtWorkingHoursOrYear.MaxLength = 4;
            this.txtWorkingHoursOrYear.Name = "txtWorkingHoursOrYear";
            this.txtWorkingHoursOrYear.Size = new System.Drawing.Size(197, 20);
            this.txtWorkingHoursOrYear.TabIndex = 6;
            this.txtWorkingHoursOrYear.TextChanged += new System.EventHandler(this.txtWorkingHoursOrYear_TextChanged);
            this.txtWorkingHoursOrYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // txtOTPolicyName
            // 
            this.txtOTPolicyName.BackColor = System.Drawing.SystemColors.Info;
            this.txtOTPolicyName.Location = new System.Drawing.Point(150, 54);
            this.txtOTPolicyName.MaxLength = 50;
            this.txtOTPolicyName.Name = "txtOTPolicyName";
            this.txtOTPolicyName.Size = new System.Drawing.Size(238, 20);
            this.txtOTPolicyName.TabIndex = 0;
            this.txtOTPolicyName.TextChanged += new System.EventHandler(this.txtOTPolicyName_TextChanged);
            // 
            // SelectValue
            // 
            this.SelectValue.HeaderText = "";
            this.SelectValue.Name = "SelectValue";
            this.SelectValue.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SelectValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SelectValue.Width = 30;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(323, 395);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(242, 395);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 304;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(5, 397);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // StatusStripOTPolicy
            // 
            this.StatusStripOTPolicy.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.StatusStripOTPolicy.Location = new System.Drawing.Point(0, 426);
            this.StatusStripOTPolicy.Name = "StatusStripOTPolicy";
            this.StatusStripOTPolicy.Size = new System.Drawing.Size(404, 22);
            this.StatusStripOTPolicy.TabIndex = 328;
            this.StatusStripOTPolicy.Text = "StatusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // errProOTPolicy
            // 
            this.errProOTPolicy.ContainerControl = this;
            this.errProOTPolicy.RightToLeft = true;
            // 
            // AddDedID
            // 
            this.AddDedID.HeaderText = "AddDedID";
            this.AddDedID.Name = "AddDedID";
            this.AddDedID.ReadOnly = true;
            this.AddDedID.Visible = false;
            // 
            // Particulars
            // 
            this.Particulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Particulars.HeaderText = "Particulars";
            this.Particulars.Name = "Particulars";
            this.Particulars.ReadOnly = true;
            // 
            // FrmOvertimePolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 448);
            this.Controls.Add(this.StatusStripOTPolicy);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.GrpMain);
            this.Controls.Add(this.PolicyMasterBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmOvertimePolicy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Overtime Policy";
            this.Load += new System.EventHandler(this.FrmOvertimePolicy_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmOvertimePolicy_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmOvertimePolicy_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.PolicyMasterBindingNavigator)).EndInit();
            this.PolicyMasterBindingNavigator.ResumeLayout(false);
            this.PolicyMasterBindingNavigator.PerformLayout();
            this.GrpMain.ResumeLayout(false);
            this.GrpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParticulars)).EndInit();
            this.StatusStripOTPolicy.ResumeLayout(false);
            this.StatusStripOTPolicy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errProOTPolicy)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator PolicyMasterBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton btnLeaveTypes;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        internal System.Windows.Forms.GroupBox GrpMain;
        internal System.Windows.Forms.Label lblWorkingHoursOrYear;
        internal System.Windows.Forms.CheckBox chkCalculationPercentage;
        internal System.Windows.Forms.TextBox txtCalculationPercent;
        internal System.Windows.Forms.CheckBox chkRateOnly;
        internal System.Windows.Forms.TextBox txtOverTimeRate;
        internal System.Windows.Forms.RadioButton rdbCompanyBased;
        internal System.Windows.Forms.RadioButton rdbActualMonth;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.ComboBox cboCalculationBased;
        internal System.Windows.Forms.TextBox txtWorkingHoursOrYear;
        internal System.Windows.Forms.TextBox txtOTPolicyName;
        internal System.Windows.Forms.DataGridViewTextBoxColumn AddDedID;
        internal System.Windows.Forms.DataGridViewCheckBoxColumn SelectValue;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Particulars;
        private DemoClsDataGridview.ClsDataGirdView dgvParticulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn AdditionDeductionID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SelectedVal;
        private System.Windows.Forms.DataGridViewTextBoxColumn AdditionDeduction;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.StatusStrip StatusStripOTPolicy;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ErrorProvider errProOTPolicy;
        private System.Windows.Forms.Timer tmrClear;
    }
}