﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DTO;
using BLL;
using DAL;

namespace MyBooksERP
{
    public partial class FrmSettlementPolicy : Form
    {

        bool MbChangeStatus;
        bool MbAddStatus = false;


        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnPrintEmailPermission = false;//To Set PrintEmail Permission

        string MsMessageCommon;
        int RecordCnt = 0;
        int iRowIndex;
        //int RecordCnt;
        int CurrentRecCnt;
        //int TempParamID;




        //int RecordCnt = 0;


        //private string MsMessageCommon; //Messagebox display
        private string MstrMessageCaption;              //Message caption

        private ArrayList MsarMessageArr; // Error Message display
        //private ArrayList MaStatusMessage;// Status bar error message display
        public int PiRecID = 0;// Reference From Work policy

        public int PiCompanyID = 0;// Reference From Work policy
        //private int TotalRecordCnt;
        private int MintTimerInterval;                  // To set timer interval
        private int MintCompanyId;                      // current companyid
        private int MintUserId;

        private MessageBoxIcon MmessageIcon;            // to set the message icon
        //string strPolicyNameCheck = "";
        private bool Glb24HourFormat;

        ClsLogWriter mObjLogs;
        ClsNotification mObjNotification;
        clsBLLSettlementPolicy MobjclsBLLSettlementPolicy;
        string strBindingOf = "Of ";

        public FrmSettlementPolicy()
        {
            //Constructor                        
            InitializeComponent();
            MintCompanyId = ClsCommonSettings.LoginCompanyID; 
            MintUserId = ClsCommonSettings.UserID;
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            Glb24HourFormat = ClsCommonSettings.Glb24HourFormat;
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            MobjclsBLLSettlementPolicy = new clsBLLSettlementPolicy();

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.SettlementPolicy, this);

        //    strBindingOf = "من ";
        //    DetailsDataGridViewGratuity.Columns["txtParticulars"].HeaderText = "تفاصيل";
        //}
        private void FrmSettlementPolicy_Load(object sender, EventArgs e)
        {
            LoadCombos(0);
            SetPermissions();
            LoadMessage();
            AddMode();

            BindingNavigatorAddNewItem_Click(sender, e);
            chkRateOnlyGratuity_CheckedChanged(sender, e);
            if (PiRecID > 0)
            {
                int RowNum = 0;
                RowNum = MobjclsBLLSettlementPolicy.GetRowNum(PiRecID);

                RecordCnt = MobjclsBLLSettlementPolicy.RecCountNavigate();
                MobjclsBLLSettlementPolicy.DisplayDetails(RowNum);
                DisplayDetails(CurrentRecCnt);
                MbAddStatus = false;
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;

                BindingNavigatorPositionItem.Text = "1";
                bnCountItem.Text = strBindingOf + "1";
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled =
                    BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
        }

        private void AddMode()
        {
            try
            {
                txtPolicyName.Text = "";
                MobjclsBLLSettlementPolicy = new clsBLLSettlementPolicy();
                cboCalculationBasedGratuity.SelectedIndex = -1;
                chkRateOnlyGratuity.Checked = false;
                chkIncludeLeave.Checked = false;
                txtRateGratuity.Text = "";
                if (grdGratuity.RowCount >= 0)
                {
                    grdGratuity.ClearSelection();
                }

                grdGratuity.Rows.Clear();

                this.grdGratuity.AlternatingRowsDefaultCellStyle.BackColor = SystemColors.ControlLight;
                LoadCombos(2);

            }
            catch (Exception ex)
            {
            }
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.SettlementPolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }
        private void LoadMessage()
        {
            // Loading Message
            try
            {
                MsarMessageArr = new ArrayList();
                MsarMessageArr = mObjNotification.FillMessageArray((int)FormID.SettlementPolicy, 2);
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on LoadMessage " + this.Name + " " + Ex.Message.ToString(), 1);
            }

        }


        private void ChangeStatus(object sender, EventArgs e)
        {

            if (MbAddStatus == true)
            {
                this.BindingNavigatorSaveItem.Enabled = MblnAddPermission;
                this.OKSaveButton.Enabled = BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                BtnSave.Enabled = MblnAddPermission;
            }
            else
            {
                this.BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                this.OKSaveButton.Enabled = BindingNavigatorAddNewItem.Enabled = MblnUpdatePermission;
                this.BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BtnSave.Enabled = MblnUpdatePermission;
            }
            MbChangeStatus = true;
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            RecordCnt = MobjclsBLLSettlementPolicy.RecCountNavigate();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = CurrentRecCnt - 1;
                if (CurrentRecCnt <= 0)
                {
                    CurrentRecCnt = 1;
                }
                if (CurrentRecCnt <= 1)
                {
                    BindingNavigatorMovePreviousItem.Enabled = false;
                    BindingNavigatorMoveFirstItem.Enabled = false;
                }
                if (RecordCnt > 1 && CurrentRecCnt < RecordCnt)
                {
                    BindingNavigatorMoveNextItem.Enabled = true;
                    BindingNavigatorMoveLastItem.Enabled = true;
                }
                else
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
            }
            else
            {
                CurrentRecCnt = 0;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }

            MbAddStatus = false;

            this.BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            DisplayDetails(CurrentRecCnt);
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) - 1).ToString();
            if (Convert.ToInt32(BindingNavigatorPositionItem.Text) <= 0)
                BindingNavigatorPositionItem.Text = "1";
            if (RecordCnt > 1)
                bnCountItem.Text = strBindingOf + RecordCnt.ToString();
            else
                bnCountItem.Text = strBindingOf + "1";

            BindingNavigatorSaveItem.Enabled = false;
            BtnSave.Enabled = false;
            OKSaveButton.Enabled = false;
            TmrSettlement.Stop();
            TmrSettlement.Start();

        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            RecordCnt = MobjclsBLLSettlementPolicy.RecCountNavigate();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = 1;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveFirstItem.Enabled = false;
                if (RecordCnt > 1 && CurrentRecCnt < RecordCnt)
                {
                    BindingNavigatorMoveNextItem.Enabled = true;
                    BindingNavigatorMoveLastItem.Enabled = true;
                }
                else
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
            }
            else
            {
                CurrentRecCnt = 0;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }


            MbAddStatus = false;
            this.BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            DisplayDetails(CurrentRecCnt);

            BindingNavigatorPositionItem.Text = "1";
            if (RecordCnt > 1)
                bnCountItem.Text = strBindingOf + RecordCnt.ToString();
            else
                bnCountItem.Text = strBindingOf + "1";

            BindingNavigatorSaveItem.Enabled = false;
            BtnSave.Enabled = false;
            OKSaveButton.Enabled = false;
            MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9, out MmessageIcon);
            lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
            TmrSettlement.Stop();
            TmrSettlement.Start();

        }

        private void CompanySettlementPolicyBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            txtPolicyName.Focus();
            if (FormValidation() == true)
            {
                if (MbAddStatus == true)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 1, out MmessageIcon);
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                }
                else
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3, out MmessageIcon);
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                }
                if (MessageBox.Show(MsMessageCommon, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
                if (MobjclsBLLSettlementPolicy.SaveCompanySettlementPolicy() == true)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    MbAddStatus = false;
                    BindingNavigatorSaveItem.Enabled = false;
                    BtnSave.Enabled = false;
                    BtnCancel.Enabled = OKSaveButton.Enabled = false;
                    BindingNavigatorMoveLastItem_Click(sender, e);
                }
            }


        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            RecordCnt = MobjclsBLLSettlementPolicy.RecCountNavigate();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = CurrentRecCnt + 1;
                if (CurrentRecCnt >= RecordCnt)
                {
                    CurrentRecCnt = RecordCnt;
                }
                if (CurrentRecCnt >= RecordCnt)
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
                if (RecordCnt > 1 && CurrentRecCnt > 1)
                {
                    BindingNavigatorMovePreviousItem.Enabled = true;
                    BindingNavigatorMoveFirstItem.Enabled = true;
                }
                else
                {
                    BindingNavigatorMovePreviousItem.Enabled = false;
                    BindingNavigatorMoveFirstItem.Enabled = false;
                }
            }
            else
            {
                CurrentRecCnt = 0;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }

            MbAddStatus = false;
            this.BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            TabGrauity.SelectedIndex = 0;
            MobjclsBLLSettlementPolicy.DisplayDetails(CurrentRecCnt);

            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) + 1).ToString();
            if (RecordCnt < Convert.ToInt32(BindingNavigatorPositionItem.Text))
            {
                if (RecordCnt > 1)
                    BindingNavigatorPositionItem.Text = RecordCnt.ToString();
                else
                    BindingNavigatorPositionItem.Text = "1";
            }
            if (RecordCnt > 1)
                bnCountItem.Text = strBindingOf + RecordCnt.ToString();
            else
                bnCountItem.Text = strBindingOf + "1";

            BindingNavigatorSaveItem.Enabled = false;
            BtnSave.Enabled = false;
            OKSaveButton.Enabled = false;
            MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9, out MmessageIcon);
            lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
            TmrSettlement.Stop();
            TmrSettlement.Start();
            DisplayDetails(CurrentRecCnt);

        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            RecordCnt = MobjclsBLLSettlementPolicy.RecCountNavigate();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = RecordCnt;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
                if (RecordCnt > 1 && CurrentRecCnt > 1)
                {
                    BindingNavigatorMovePreviousItem.Enabled = true;
                    BindingNavigatorMoveFirstItem.Enabled = true;
                }
                else
                {
                    BindingNavigatorMovePreviousItem.Enabled = false;
                    BindingNavigatorMoveFirstItem.Enabled = false;
                }
            }
            else
            {
                CurrentRecCnt = 0;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            MbAddStatus = false;
            this.BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;

            DisplayDetails(CurrentRecCnt);

            if (RecordCnt > 1)
            {
                BindingNavigatorPositionItem.Text = RecordCnt.ToString();
                bnCountItem.Text = strBindingOf + RecordCnt.ToString();
            }
            else
            {
                BindingNavigatorPositionItem.Text = "1";
                bnCountItem.Text = strBindingOf + "1";
            }

            BindingNavigatorSaveItem.Enabled = false;
            BtnSave.Enabled = false;
            OKSaveButton.Enabled = false;
            MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9, out MmessageIcon);
            lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
            TmrSettlement.Stop();
            TmrSettlement.Start();
        }
        private void DisplayDetails(int CurrentRecCnt)
        {
            try
            {
                int iCounter = 0;
                TabGrauity.SelectedIndex = 0;
                MobjclsBLLSettlementPolicy.DisplayDetails(CurrentRecCnt);
                txtPolicyName.Text = MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.DescriptionPolicy;
                cboCalculationBasedGratuity.SelectedValue = MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationID;


                if (MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.IsRateOnly == 1)
                {
                    chkRateOnlyGratuity.Checked = true;
                    txtRateGratuity.Text = MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.RatePerDay.ToString();
                }
                else
                {
                    chkRateOnlyGratuity.Checked = false;
                    txtRateGratuity.Text = MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.RatePerDay.ToString();
                    cboCalculationBasedGratuity.SelectedValue = MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationID;
                }
                chkIncludeLeave.Checked = MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.blnIsIncludeLeave;


                iCounter = 0;
                grdGratuity.Rows.Clear();
                foreach (clsDTOSettlementPolicyDetail objclsDTOSettlementPolicyDetail in MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetail)
                {
                    grdGratuity.RowCount = grdGratuity.RowCount + 1;


                    grdGratuity.Rows[iCounter].Cells["ParameterID"].Value = objclsDTOSettlementPolicyDetail.ParameterID;
                    grdGratuity.Rows[iCounter].Cells["ParameterID"].Tag = objclsDTOSettlementPolicyDetail.ParameterID;
                    grdGratuity.Rows[iCounter].Cells["ParameterID"].Value = grdGratuity.Rows[iCounter].Cells["ParameterID"].FormattedValue;

                    grdGratuity.Rows[iCounter].Cells["NoOfMonths"].Value = objclsDTOSettlementPolicyDetail.NoOfMonths;
                    grdGratuity.Rows[iCounter].Cells["NoOfDays"].Value = objclsDTOSettlementPolicyDetail.NoOfDays;
                    grdGratuity.Rows[iCounter].Cells["TerminationDays"].Value = objclsDTOSettlementPolicyDetail.decTerminationDays;
                    grdGratuity.Rows[iCounter].Cells["GratuityID"].Value = 0;//MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationID; 
                    iCounter = iCounter + 1;

                }


                iCounter = 0;
                grdGratuityFive.Rows.Clear();
                foreach (clsDTOSettlementPolicyDetailFive objclsDTOSettlementPolicyDetailFive in MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailFive)
                {
                    grdGratuityFive.RowCount = grdGratuityFive.RowCount + 1;


                    grdGratuityFive.Rows[iCounter].Cells["ParameterIDFive"].Value = objclsDTOSettlementPolicyDetailFive.ParameterID;
                    grdGratuityFive.Rows[iCounter].Cells["ParameterIDFive"].Tag = objclsDTOSettlementPolicyDetailFive.ParameterID;
                    grdGratuityFive.Rows[iCounter].Cells["ParameterIDFive"].Value = grdGratuityFive.Rows[iCounter].Cells["ParameterIDFive"].FormattedValue;

                    grdGratuityFive.Rows[iCounter].Cells["NoOfMonthsFive"].Value = objclsDTOSettlementPolicyDetailFive.NoOfMonths;
                    grdGratuityFive.Rows[iCounter].Cells["NoOfDaysFive"].Value = objclsDTOSettlementPolicyDetailFive.NoOfDays;
                    grdGratuityFive.Rows[iCounter].Cells["TerminationDaysFive"].Value = objclsDTOSettlementPolicyDetailFive.decTerminationDays;
                    grdGratuityFive.Rows[iCounter].Cells["GratuityIDFive"].Value = 0;//MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationID; 
                    iCounter = iCounter + 1;

                }

                iCounter = 0;
                DetailsDataGridViewGratuity.Rows.Clear();
                foreach (clsDTOInExGra objclsDTOInExGra in MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOInExGra)
                {
                    if (Convert.ToInt32(cboCalculationBasedGratuity.SelectedValue) == 2)
                    {
                        DetailsDataGridViewGratuity.RowCount = DetailsDataGridViewGratuity.RowCount + 1;
                        DetailsDataGridViewGratuity.Rows[iCounter].Cells["txtAddDedID"].Value = objclsDTOInExGra.AdditionDeductionID;
                        DetailsDataGridViewGratuity.Rows[iCounter].Cells["chkParticular"].Value = objclsDTOInExGra.Sel;
                        DetailsDataGridViewGratuity.Rows[iCounter].Cells["txtParticulars"].Value = objclsDTOInExGra.DescriptionStr;
                        iCounter = iCounter + 1;
                    }
                }

                iCounter = 0;

                BtnCancel.Enabled = false;




            }
            catch (Exception e)
            {

            }
        }

        private void DetailsDataGridViewGratuity_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {

            }
        }

        private void grdGratuity_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {

            }



        }

        private void dgvExcludeEncash_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

            try
            {

            }
            catch (Exception ex)
            {

            }


        }


        private void LoadCombos(int intType)
        {


            DataTable datCombos = new DataTable();

            if (intType == 0 || intType == 1)
            {
                //if (ClsCommonSettings.IsArabicView)
                //    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "CalculationID, CalculationArb AS Calculation", "PayCalculationReference", "CalculationID in(1,2,3)", "CalculationID", "Calculation" });
                //else
                    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "CalculationID, Calculation", "PayCalculationReference", "CalculationID in(1,2,3)", "CalculationID", "Calculation" });
                cboCalculationBasedGratuity.ValueMember = "CalculationID";
                cboCalculationBasedGratuity.DisplayMember = "Calculation";
                cboCalculationBasedGratuity.DataSource = datCombos;
            }
            if (intType == 0 || intType == 2)
            {
                datCombos = null;
                //if (ClsCommonSettings.IsArabicView)
                //    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                //else
                    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                ParameterID.ValueMember = "ParameterId";
                ParameterID.DisplayMember = "ParameterName";
                ParameterID.DataSource = datCombos;

                ParameterIDFive.ValueMember = "ParameterId";
                ParameterIDFive.DisplayMember = "ParameterName";
                ParameterIDFive.DataSource = datCombos;
            }

        

        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            MbAddStatus = true;
            grdGratuity.RowCount = 1;
            grdGratuityFive.RowCount = 1;
            AddMode();
            BindingNavigatorAddNewItem.Enabled = false;
            txtRateGratuity.Enabled = false;
            //txtRateEncash.Enabled = false;
            RecCount();
            TabGrauity.SelectedIndex = 0;
            bnCountItem.Text = strBindingOf + Convert.ToString(RecordCnt) + "";
            BindingNavigatorPositionItem.Text = RecordCnt.ToString();

            MbChangeStatus = false;
            OKSaveButton.Enabled = MbChangeStatus;
            BindingNavigatorDeleteItem.Enabled = false;
            BtnSave.Enabled = MbChangeStatus;
            this.BindingNavigatorSaveItem.Enabled = MbChangeStatus;
            this.BindingNavigatorDeleteItem.Enabled = MbChangeStatus;

            TmrSettlement.Stop();
            TmrSettlement.Start();
            tbSettlement.SelectTab(0);
            BtnCancel.Enabled = true;
            txtPolicyName.Focus();
            txtPolicyName.Select();
        }

        private void RecCount()
        {
            RecordCnt = MobjclsBLLSettlementPolicy.RecCount();

            if (RecordCnt > 0)
            {
                bnCountItem.Text = strBindingOf + Convert.ToString(RecordCnt) + "";
                BindingNavigatorPositionItem.Text = Convert.ToString(RecordCnt);
                CurrentRecCnt = RecordCnt;
            }
            else
            {
                CurrentRecCnt = 0;
                bnCountItem.Text = strBindingOf + "0";
            }


            BindingNavigatorMoveNextItem.Enabled = false;
            BindingNavigatorMoveLastItem.Enabled = false;
            if (RecordCnt > 1)
            {
                BindingNavigatorMovePreviousItem.Enabled = true;
                BindingNavigatorMoveFirstItem.Enabled = true;
            }
            else
            {
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveFirstItem.Enabled = false;
            }


        }

        private void TmrSettlement_Tick(object sender, EventArgs e)
        {
            try
            {
                lblstatus.Text = "";
                TmrSettlement.Enabled = false;
            }
            catch
            {
            }
        }

        private bool FormValidation()
        {
            try
            {

                ErrSettlement.Clear();
                grdGratuity.CommitEdit(DataGridViewDataErrorContexts.Commit);
                grdGratuityFive.CommitEdit(DataGridViewDataErrorContexts.Commit);
                if (txtPolicyName.Text.Trim() == "")
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2212, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ErrSettlement.SetError(txtPolicyName, MsMessageCommon.Replace("#", "").Trim());
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrSettlement.Enabled = true;
                    tbSettlement.SelectTab(0);
                    txtPolicyName.Focus();
                    return false;
                }
                if (MobjclsBLLSettlementPolicy.CheckploicyNameDup(MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.SetPolicyID, txtPolicyName.Text.Trim()))
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9900, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ErrSettlement.SetError(txtPolicyName, MsMessageCommon.Replace("#", "").Trim());
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrSettlement.Enabled = true;
                    tbSettlement.SelectTab(0);
                    txtPolicyName.Focus();
                    return false;
                }

                if (chkRateOnlyGratuity.Checked == false && cboCalculationBasedGratuity.SelectedIndex == -1)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2208, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ErrSettlement.SetError(cboCalculationBasedGratuity, MsMessageCommon.Replace("#", "").Trim());
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrSettlement.Enabled = true;
                    tbSettlement.SelectTab(0);
                    cboCalculationBasedGratuity.Focus();
                    return false;
                }

                if (chkRateOnlyGratuity.Checked == true && txtRateGratuity.Text.Trim().Length == 0)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2207, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ErrSettlement.SetError(txtRateGratuity, MsMessageCommon.Replace("#", "").Trim());
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrSettlement.Enabled = true;
                    tbSettlement.SelectTab(0);
                    txtRateGratuity.Focus();
                    return false;
                }

                grdGratuity.Focus();

                if (cboCalculationBasedGratuity.SelectedIndex >= 0 && chkRateOnlyGratuity.Checked == false)
                {
                    if (grdGratuity.Rows.Count == 1)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2201, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        TmrSettlement.Enabled = true;
                        tbSettlement.SelectTab(0);
                        grdGratuity.Focus();
                        return false;
                    }
                }



                //if (cboCalculationBasedGratuity.SelectedIndex >= 0 && chkRateOnlyGratuity.Checked == false)
                //{
                //    if (grdGratuityFive.Rows.Count == 1)
                //    {
                //        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2201, out MmessageIcon);
                //        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                //        TmrSettlement.Enabled = true;
                //        TabGrauity.SelectedIndex = 1;
                //        grdGratuityFive.Focus();
                //        return false;
                //    }
                //}


                int i;
                bool blnIsRepeat = false; ;
                bool blnIsRepeatExactly = false;
                for (i = 0; grdGratuity.RowCount - 2 >= i; ++i)
                {
                    //if (grdGratuity.Rows[i].Cells[0].Value==null  ||  grdGratuity.Rows[i].Cells[1].Value==null ||  grdGratuity.Rows[i].Cells[2].Value=null )
                    //  {
                    if (grdGratuity.Rows[i].Cells[0].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2202, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        grdGratuity.CurrentCell = grdGratuity[0, i];
                        tbSettlement.SelectTab(0);
                        return false;
                    }
                    if (grdGratuity.Rows[i].Cells[1].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2213, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        grdGratuity.CurrentCell = grdGratuity[1, i];
                        return false;
                    }
                    if (grdGratuity.Rows[i].Cells[2].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2214, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        grdGratuity.CurrentCell = grdGratuity[2, i];
                        tbSettlement.SelectTab(0);
                        return false;
                    }
                    if (grdGratuity.Rows[i].Cells["ParameterID"].Tag.ToInt32() == 6)
                    {
                        blnIsRepeat = true;
                    }
                    if (grdGratuity.Rows[i].Cells["ParameterID"].Tag.ToInt32() == 7)
                    {
                        blnIsRepeatExactly = true;
                    }
                    for (int k = 0; grdGratuity.Rows.Count.ToInt32() - 2 >= k; ++k)
                    {
                        for (int j = k + 1; grdGratuity.Rows.Count.ToInt32() - 2 >= j; ++j)
                        {
                            if ((grdGratuity.Rows[k].Cells["ParameterID"].Tag.ToInt32() == grdGratuity.Rows[j].Cells["ParameterID"].Tag.ToInt32()) && (grdGratuity.Rows[k].Cells["NoOfMonths"].Value.ToDecimal() == grdGratuity.Rows[j].Cells["NoOfMonths"].Value.ToDecimal()) && (grdGratuity.Rows[k].Cells["NoOfDays"].Value.ToDecimal() == grdGratuity.Rows[j].Cells["NoOfDays"].Value.ToDecimal()))
                            {
                                grdGratuity.CurrentCell = grdGratuity.Rows[j].Cells["NoOfMonths"];
                                MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 15812, out MmessageIcon);
                                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                                TmrSettlement.Enabled = true;
                                return false;

                            }
                        }
                    }
                  
                }

                //----------------------------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------
                for (i = 0; grdGratuityFive.RowCount - 2 >= i; ++i)
                {
                    if (grdGratuityFive.Rows[i].Cells[0].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2202, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        TabGrauity.SelectedIndex = 1;
                        grdGratuityFive.CurrentCell = grdGratuityFive[0, i];
                        tbSettlement.SelectTab(0);
                        return false;
                    }
                    if (grdGratuityFive.Rows[i].Cells[1].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2213, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        TabGrauity.SelectedIndex = 1;
                        grdGratuityFive.CurrentCell = grdGratuityFive[1, i];
                        return false;
                    }
                    if (grdGratuityFive.Rows[i].Cells[2].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2214, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        TabGrauity.SelectedIndex = 1;
                        grdGratuityFive.CurrentCell = grdGratuityFive[2, i];
                        tbSettlement.SelectTab(0);
                        return false;
                    }
                    if (grdGratuityFive.Rows[i].Cells["ParameterIDFive"].Tag.ToInt32() == 6)
                    {
                        blnIsRepeat = true;
                    }
                    if (grdGratuityFive.Rows[i].Cells["ParameterIDFive"].Tag.ToInt32() == 7)
                    {
                        blnIsRepeatExactly = true;
                    }
                    for (int k = 0; grdGratuityFive.Rows.Count.ToInt32() - 2 >= k; ++k)
                    {
                        for (int j = k + 1; grdGratuityFive.Rows.Count.ToInt32() - 2 >= j; ++j)
                        {
                            if ((grdGratuityFive.Rows[k].Cells["ParameterIDFive"].Tag.ToInt32() == grdGratuityFive.Rows[j].Cells["ParameterIDFive"].Tag.ToInt32()) && (grdGratuityFive.Rows[k].Cells["NoOfMonthsFive"].Value.ToDecimal() == grdGratuityFive.Rows[j].Cells["NoOfMonthsFive"].Value.ToDecimal()) && (grdGratuityFive.Rows[k].Cells["NoOfDaysFive"].Value.ToDecimal() == grdGratuityFive.Rows[j].Cells["NoOfDaysFive"].Value.ToDecimal()))
                            {
                                grdGratuityFive.CurrentCell = grdGratuityFive.Rows[j].Cells["NoOfMonthsFive"];
                                MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 15812, out MmessageIcon);
                                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                                TabGrauity.SelectedIndex = 1;
                                TmrSettlement.Enabled = true;
                                return false;

                            }
                        }
                    }

                }
                //----------------------------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------
                if (blnIsRepeat)
                {
                    for (i = 0; grdGratuity.RowCount - 2 >= i; ++i)
                    {
                        if (grdGratuity.Rows[i].Cells["ParameterID"].Tag.ToInt32() != 6)
                        {
                            //MessageBox.Show("Please select all entries as Repeat or None");
                            grdGratuity.CurrentCell = grdGratuity.Rows[i].Cells["ParameterID"];
                            MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 15813, out MmessageIcon);
                            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                            TmrSettlement.Enabled = true;
                            return false;
                        }
                    }
                }
                if (blnIsRepeatExactly)
                {
                    for (i = 0; grdGratuity.RowCount - 2 >= i; ++i)
                    {
                        if (grdGratuity.Rows[i].Cells["ParameterID"].Tag.ToInt32() != 7)
                        {
                            //MessageBox.Show("Please select all entries as RepeatExactly or None");
                            grdGratuity.CurrentCell = grdGratuity.Rows[i].Cells["ParameterID"];
                            MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 15814, out MmessageIcon);
                            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                            TmrSettlement.Enabled = true;
                            return false;
                        }
                    }
                }

                //--------------------------------------------------------------------------------------------------------------------------
                //--------------------------------------------------------------------------------------------------------------------------
                if (blnIsRepeat)
                {
                    for (i = 0; grdGratuityFive.RowCount - 2 >= i; ++i)
                    {
                        if (grdGratuityFive.Rows[i].Cells["ParameterIDFive"].Tag.ToInt32() != 6)
                        {
                            //MessageBox.Show("Please select all entries as Repeat or None");
                            grdGratuityFive.CurrentCell = grdGratuityFive.Rows[i].Cells["ParameterIDFive"];
                            MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 15813, out MmessageIcon);
                            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                            TabGrauity.SelectedIndex = 1;
                            TmrSettlement.Enabled = true;
                            return false;
                        }
                    }
                }
                if (blnIsRepeatExactly)
                {
                    for (i = 0; grdGratuityFive.RowCount - 2 >= i; ++i)
                    {
                        if (grdGratuityFive.Rows[i].Cells["ParameterIDFive"].Tag.ToInt32() != 7)
                        {
                            //MessageBox.Show("Please select all entries as RepeatExactly or None");
                            grdGratuityFive.CurrentCell = grdGratuityFive.Rows[i].Cells["ParameterIDFive"];
                            MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 15814, out MmessageIcon);
                            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                            TabGrauity.SelectedIndex = 1;
                            TmrSettlement.Enabled = true;
                            return false;
                        }
                    }
                }
                //--------------------------------------------------------------------------------------------------------------------------
                //--------------------------------------------------------------------------------------------------------------------------


                if (chkRateOnlyGratuity.Checked == true)
                {
                    try
                    {
                        if (Convert.ToDouble(txtRateGratuity.Text) <= 0)
                        {
                            MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2207, out MmessageIcon);
                            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ErrSettlement.SetError(txtRateGratuity, MsMessageCommon.Replace("#", "").Trim());
                            lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                            TmrSettlement.Enabled = true;
                            tbSettlement.SelectTab(0);
                            txtRateGratuity.Focus();
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2207, out MmessageIcon);
                        ErrSettlement.SetError(txtRateGratuity, MsMessageCommon.Replace("#", "").Trim());
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        TmrSettlement.Enabled = true;
                        tbSettlement.SelectTab(0);
                        txtRateGratuity.Focus();
                        return false;
                    }
                }

                FillParameter();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }



        private void FillParameter()
        {
            try
            {
                int iCounter = 0;


                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.DescriptionPolicy = txtPolicyName.Text.Trim();

                if (chkRateOnlyGratuity.Checked == true)
                {
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.IsRateOnly = 1;
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.RatePerDay = Convert.ToDouble(txtRateGratuity.Text);
                }
                else
                {
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.IsRateOnly = 0;
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationID = Convert.ToInt32(cboCalculationBasedGratuity.SelectedValue);
                }

                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.blnIsIncludeLeave = chkIncludeLeave.Checked;


                if (MbAddStatus == false)
                {
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.ModeAddEdit = 2;
                }
                else
                {
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.ModeAddEdit = 1;
                }




                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetail = new System.Collections.Generic.List<DTO.clsDTOSettlementPolicyDetail>();


                for (int i = 0; Convert.ToInt32(grdGratuity.Rows.Count) - 2 >= i; ++i)
                {
                    clsDTOSettlementPolicyDetail objclsDTOSettlementPolicyDetail = new clsDTOSettlementPolicyDetail();
                    objclsDTOSettlementPolicyDetail.ParameterID = grdGratuity.Rows[i].Cells["ParameterID"].Tag.ToInt32();
                    if (Convert.ToInt32(objclsDTOSettlementPolicyDetail.ParameterID) == 0)
                    {
                        continue;
                    }
                    objclsDTOSettlementPolicyDetail.NoOfMonths = Convert.ToInt32(grdGratuity.Rows[i].Cells["NoOfMonths"].Value);
                    objclsDTOSettlementPolicyDetail.NoOfDays = Convert.ToDecimal(grdGratuity.Rows[i].Cells["NoOfDays"].Value);
                    objclsDTOSettlementPolicyDetail.decTerminationDays = grdGratuity.Rows[i].Cells["TerminationDays"].Value.ToDecimal();
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetail.Add(objclsDTOSettlementPolicyDetail);
                }

                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailFive = new System.Collections.Generic.List<DTO.clsDTOSettlementPolicyDetailFive>();


                for (int i = 0; Convert.ToInt32(grdGratuityFive.Rows.Count) - 2 >= i; ++i)
                {
                    clsDTOSettlementPolicyDetailFive objclsDTOSettlementPolicyDetailFive = new clsDTOSettlementPolicyDetailFive();
                    objclsDTOSettlementPolicyDetailFive.ParameterID = grdGratuityFive.Rows[i].Cells["ParameterIDFive"].Tag.ToInt32();
                    if (Convert.ToInt32(objclsDTOSettlementPolicyDetailFive.ParameterID) == 0)
                    {
                        continue;
                    }
                    objclsDTOSettlementPolicyDetailFive.NoOfMonths = Convert.ToInt32(grdGratuityFive.Rows[i].Cells["NoOfMonthsFive"].Value);
                    objclsDTOSettlementPolicyDetailFive.NoOfDays = Convert.ToDecimal(grdGratuityFive.Rows[i].Cells["NoOfDaysFive"].Value);
                    objclsDTOSettlementPolicyDetailFive.decTerminationDays = grdGratuityFive.Rows[i].Cells["TerminationDaysFive"].Value.ToDecimal();
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailFive.Add(objclsDTOSettlementPolicyDetailFive);
                }

       

                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOInExGra = new System.Collections.Generic.List<DTO.clsDTOInExGra>();


                for (int i = 0; Convert.ToInt32(DetailsDataGridViewGratuity.Rows.Count) - 1 >= i; ++i)
                {
                    clsDTOInExGra objclsDTOInExGra = new clsDTOInExGra();
                    objclsDTOInExGra.AdditionDeductionID = Convert.ToInt32(DetailsDataGridViewGratuity.Rows[i].Cells["txtAddDedID"].Value);
                    objclsDTOInExGra.Sel = Convert.ToBoolean(DetailsDataGridViewGratuity.Rows[i].Cells["chkParticular"].Value);
                    objclsDTOInExGra.DescriptionStr = Convert.ToString(DetailsDataGridViewGratuity.Rows[i].Cells["txtParticulars"].Value);
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOInExGra.Add(objclsDTOInExGra);
                }




            }
            catch (Exception e)
            {

            }
        }



        private void chkRateOnlyGratuity_CheckedChanged(object sender, EventArgs e)
        {

            try
            {
                if (chkRateOnlyGratuity.Checked == true)
                {
                    txtRateGratuity.Enabled = true;
                    grdGratuity.Rows.Clear();
                    grdGratuity.RowCount = 1;

                    grdGratuityFive.Rows.Clear();
                    grdGratuityFive.RowCount = 1;

                    grdGratuity.Enabled = false;
                    grdGratuityFive.Enabled = false;

                    lblCalcGratuity.Enabled = false;
                    cboCalculationBasedGratuity.Enabled = false;
                    lblGrossGratuity.Enabled = false;
                    DetailsDataGridViewGratuity.Enabled = false;
                    txtRateGratuity.Enabled = true;
                }
                else
                {
                    txtRateGratuity.Text = "";
                    txtRateGratuity.Enabled = false;
                    grdGratuity.Enabled = true;
                    grdGratuityFive.Enabled = true;

                    lblCalcGratuity.Enabled = true;
                    cboCalculationBasedGratuity.Enabled = true;
                    lblGrossGratuity.Enabled = true;
                    DetailsDataGridViewGratuity.Enabled = true;
                    txtRateGratuity.Enabled = false;
                }
            }
            catch
            {
            }


        }

       
        private void cboCalculationBasedGratuity_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCalculationBasedGratuity.DroppedDown = false;
        }

      


        void txtDecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]'\"";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
        }
        void txtInt_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}.{:?></,`-=\\[]'\"";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0))
            {
                e.Handled = true;
            }
        }

        void cb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            cb.DroppedDown = false;
        }





        private void DetailsDataGridViewGratuity_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DetailsDataGridViewGratuity.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
            catch
            {

            }
        }

     
        private void grdGratuity_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {

            try
            {

                DataTable datCombos = new DataTable();
                ChangeStatus(sender, e);
                if (e.RowIndex >= 0)
                {
                    iRowIndex = grdGratuity.CurrentRow.Index;
                    if (grdGratuity.RowCount >= 1)
                    {
                        if (e.ColumnIndex == 0)
                        {


                            datCombos = null;
                            //if (ClsCommonSettings.IsArabicView)
                            //    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            //else
                                datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            ParameterID.ValueMember = "ParameterId";
                            ParameterID.DisplayMember = "ParameterName";
                            ParameterID.DataSource = datCombos;
                            if (e.RowIndex > 0)
                            {
                                if (grdGratuity.Rows[0].Cells["ParameterID"].Value.ToInt32().ToString() == "6")
                                {
                                    grdGratuity.CurrentCell = grdGratuity[0, iRowIndex];
                                    e.Cancel = true;
                                    return;
                                }
                            }

                            //TempParamID = Convert.ToInt32(grdGratuity.Rows[0].Cells["ParameterID"].Value);
                        }
                        if (e.ColumnIndex == 1)
                        {
                            if (grdGratuity.CurrentRow.Cells[0].Value == null)
                            {
                                grdGratuity.CurrentCell = grdGratuity[0, iRowIndex];
                                e.Cancel = true;
                            }
                        }

                        if (e.ColumnIndex == 2)
                        {
                            if (grdGratuity.CurrentRow.Cells[1].Value == null)
                            {
                                grdGratuity.CurrentCell = grdGratuity[1, iRowIndex];
                                e.Cancel = true;
                            }
                        }
                    }
                }

            }
            catch
            {
            }

        }

        private void grdGratuity_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (grdGratuity.CurrentCell != null)
                {
                    if (grdGratuity.CurrentCell.ColumnIndex == ParameterID.Index)
                    {
                        ComboBox cb = (ComboBox)e.Control;
                        cb.KeyPress += new KeyPressEventHandler(cb_KeyPress);
                        cb.DropDownHeight = 120;
                        cb.DropDownStyle = ComboBoxStyle.DropDown;
                        cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        cb.AutoCompleteSource = AutoCompleteSource.ListItems;
                    }
                    if (grdGratuity.CurrentCell.ColumnIndex == NoOfMonths.Index || grdGratuity.CurrentCell.ColumnIndex == NoOfDays.Index || grdGratuity.CurrentCell.ColumnIndex == TerminationDays.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
                this.grdGratuity.EditingControl.KeyPress += new KeyPressEventHandler(EditingControlGratuity_KeyPress);

            }
            catch (Exception Ex)
            {
                //ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }


        void EditingControlGratuity_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (grdGratuity.CurrentCell.ColumnIndex == NoOfMonths.Index)
            {
                if (!((Char.IsDigit(e.KeyChar)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,backspace(08)
                {
                    e.Handled = true;
                }
            }
        }


        private void DetailsDataGridViewGratuity_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            ChangeStatus(sender, e);
        }

        private void dgvExcludeEncash_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            ChangeStatus(sender, e);
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                int ExistFlg = 0;

                ExistFlg = MobjclsBLLSettlementPolicy.ExistsCompanySettlementPolicy();
                if (ExistFlg == 1)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2204, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrSettlement.Enabled = true;
                    return;
                }

                MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 13, out MmessageIcon);
                lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MobjclsBLLSettlementPolicy.DeleteCompanySettlementPolicy();
                    BtnCancel_Click(sender, e);
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 4, out MmessageIcon);
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrSettlement.Enabled = true;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {

            BindingNavigatorAddNewItem_Click(sender, e);
        }

        private void cboCalculationBasedGratuity_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                OKSaveButton.Enabled = true;
                BtnSave.Enabled = true;
                if (Convert.ToInt32(cboCalculationBasedGratuity.SelectedValue) == 2)
                {
                    LoadAdditionsList(1);
                    DetailsDataGridViewGratuity.Enabled = true;
                }
                else
                {
                    LoadAdditionsList(1);
                    DetailsDataGridViewGratuity.Enabled = false;
                    DetailsDataGridViewGratuity.Rows.Clear();
                    DetailsDataGridViewGratuity.RowCount = 1;
                }

                ChangeStatus(sender, e);
            }
            catch (Exception Ex)
            {
            }



        }
        private void LoadAdditionsList(int iMode)
        {
            try
            {
                if (iMode == 0)
                {
                    DetailsDataGridViewGratuity.Rows.Clear();
                    DataTable DtablePolicy;
                    DtablePolicy = MobjclsBLLSettlementPolicy.FillAdddetailAddMode();
                    if (DtablePolicy.Rows.Count > 0)
                    {
                        DetailsDataGridViewGratuity.RowCount = 0;
                        for (int i = 0; DtablePolicy.Rows.Count - 1 >= i; ++i)
                        {
                            DetailsDataGridViewGratuity.RowCount = DetailsDataGridViewGratuity.RowCount + 1;
                            DetailsDataGridViewGratuity.Rows[i].Cells["txtAddDedID"].Value = Convert.ToInt32(DtablePolicy.Rows[i]["AddDedID"]);
                            DetailsDataGridViewGratuity.Rows[i].Cells["txtParticulars"].Value = Convert.ToString(DtablePolicy.Rows[i]["DescriptionStr"]);
                            DetailsDataGridViewGratuity.Rows[i].Cells["chkParticular"].Value = Convert.ToBoolean(DtablePolicy.Rows[i]["Sel"]);
                        }

                    }
                    else
                    {
                        DetailsDataGridViewGratuity.RowCount = 1;
                    }
                }
                else if (iMode == 1)
                {
                    DetailsDataGridViewGratuity.Rows.Clear();
                    DataTable DtablePolicy;
                    DtablePolicy = MobjclsBLLSettlementPolicy.FillAdddetailAddMode();
                    if (DtablePolicy.Rows.Count > 0)
                    {
                        DetailsDataGridViewGratuity.RowCount = 0;
                        for (int i = 0; DtablePolicy.Rows.Count - 1 >= i; ++i)
                        {
                            DetailsDataGridViewGratuity.RowCount = DetailsDataGridViewGratuity.RowCount + 1;
                            DetailsDataGridViewGratuity.Rows[i].Cells["txtAddDedID"].Value = Convert.ToInt32(DtablePolicy.Rows[i]["AddDedID"]);
                            DetailsDataGridViewGratuity.Rows[i].Cells["txtParticulars"].Value = Convert.ToString(DtablePolicy.Rows[i]["DescriptionStr"]);
                            DetailsDataGridViewGratuity.Rows[i].Cells["chkParticular"].Value = Convert.ToBoolean(DtablePolicy.Rows[i]["Sel"]);
                        }
                    }
                    else
                    {
                        DetailsDataGridViewGratuity.RowCount = 1;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }



        private void BtnBottomCancel_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private void OKSaveButton_Click(object sender, EventArgs e)
        {

            CompanySettlementPolicyBindingNavigatorSaveItem_Click(sender, e);
            if (!BtnSave.Enabled)
            {
                this.Close();
            }


        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            CompanySettlementPolicyBindingNavigatorSaveItem_Click(sender, e);
        }

        private void DetailsDataGridViewGratuity_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (DetailsDataGridViewGratuity.IsCurrentCellDirty)
                {

                    DetailsDataGridViewGratuity.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }

        private void FrmSettlementPolicy_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (BtnSave.Enabled)
            {

                MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 8, out MmessageIcon);

                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        private void txtRateGratuity_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
        }

        private void bnHelp_Click(object sender, EventArgs e)
        {
            try
            {
                FrmHelp objHelp = new FrmHelp();
                objHelp.strFormName = "SettlementPolicy";
                objHelp.ShowDialog();
                objHelp = null;
            }
            catch
            {


            }
        }

        private void FrmSettlementPolicy_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        bnHelp_Click(null, null);
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        if (BindingNavigatorAddNewItem.Enabled)
                            BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (BindingNavigatorDeleteItem.Enabled)
                            BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if (BtnCancel.Enabled)
                            BtnCancel_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        if (BindingNavigatorMovePreviousItem.Enabled)
                            BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());//Prev item
                        break;
                    case Keys.Control | Keys.Right:
                        if (BindingNavigatorMoveNextItem.Enabled)
                            BindingNavigatorMoveNextItem_Click(sender, new EventArgs());//Next item
                        break;
                    case Keys.Control | Keys.Up:
                        if (BindingNavigatorMoveFirstItem.Enabled)
                            BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());//First item
                        break;
                    case Keys.Control | Keys.Down:
                        if (BindingNavigatorMoveFirstItem.Enabled)
                            BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());//Last item
                        break;

                    //case Keys.Control | Keys.M:
                    //    BtnEmail_Click(sender, new EventArgs());//Cancel
                    //    break;
                }
            }
            catch
            {
            }
        }

        private void grdGratuity_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    grdGratuity.EndEdit();
                    //------------------------------------------------------------UOM----------------------------------------------------------

                    if (e.ColumnIndex == ParameterID.Index)
                    {

                        grdGratuity.CurrentRow.Cells["ParameterID"].Tag = grdGratuity.CurrentRow.Cells["ParameterID"].Value;
                        grdGratuity.CurrentRow.Cells["ParameterID"].Value = grdGratuity.CurrentRow.Cells["ParameterID"].FormattedValue;

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void grdGratuity_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //LoadParameter();
        }
        public void LoadParameter()
        {

            DataTable datCombos;
            int IntParameterID = 0;

            if (MobjclsBLLSettlementPolicy == null)
                MobjclsBLLSettlementPolicy = new clsBLLSettlementPolicy();

            grdGratuity.CommitEdit(DataGridViewDataErrorContexts.Commit);

            if (grdGratuity.Rows.Count > 1)
            {
                IntParameterID = grdGratuity.Rows[0].Cells[0].Tag.ToInt32();

                // Modified by Laxmi

                int GridCount = grdGratuity.Rows.Count - 2;

                if (IntParameterID == 6 || IntParameterID == 7)
                {

                    for (int i = GridCount; i >= 1; i--)
                    {
                        grdGratuity.Rows.RemoveAt(i);
                    }
                }

                if (IntParameterID < 6)
                {
                    //grdGratuity.Rows[1].ReadOnly = false;
                    //if (ClsCommonSettings.IsArabicView)
                    //    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                    //else
                        datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                }
                else if (IntParameterID == 6)
                {

                    //grdGratuity.Rows[1].ReadOnly = true;
                    //if (ClsCommonSettings.IsArabicView)
                    //    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                    //else
                        datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                }
                else
                {
                    //grdGratuity.Rows[1].ReadOnly = true;
                    //if (ClsCommonSettings.IsArabicView)
                    //    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId ", "ParameterName" });
                    //else
                        datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId ", "ParameterName" });
                }

                ParameterID.ValueMember = "ParameterId";
                ParameterID.DisplayMember = "ParameterName";
                ParameterID.DataSource = datCombos;
            }
        }

        private void grdGratuity_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            if (grdGratuity.Rows.Count == 1)
            {
                grdGratuity.Rows.Clear();
                //  LoadParameter();
            }
        }



        private void grdGratuityFive_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    grdGratuity.EndEdit();
                    //------------------------------------------------------------UOM----------------------------------------------------------

                    if (e.ColumnIndex == ParameterID.Index)
                    {

                        grdGratuityFive.CurrentRow.Cells["ParameterIDFive"].Tag = grdGratuityFive.CurrentRow.Cells["ParameterIDFive"].Value;
                        grdGratuityFive.CurrentRow.Cells["ParameterIDFive"].Value = grdGratuityFive.CurrentRow.Cells["ParameterIDFive"].FormattedValue;

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void grdGratuityFive_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void grdGratuityFive_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {

                DataTable datCombos = new DataTable();
                ChangeStatus(sender, e);
                if (e.RowIndex >= 0)
                {
                    iRowIndex = grdGratuityFive.CurrentRow.Index;
                    if (grdGratuityFive.RowCount >= 1)
                    {
                        if (e.ColumnIndex == 0)
                        {
                            datCombos = null;
                            //if (ClsCommonSettings.IsArabicView)
                            //    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            //else
                                datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            ParameterIDFive.ValueMember = "ParameterId";
                            ParameterIDFive.DisplayMember = "ParameterName";
                            ParameterIDFive.DataSource = datCombos;
                            if (e.RowIndex > 0)
                            {
                                if (grdGratuityFive.Rows[0].Cells["ParameterIDFive"].Value.ToInt32().ToString() == "6")
                                {
                                    grdGratuityFive.CurrentCell = grdGratuityFive[0, iRowIndex];
                                    e.Cancel = true;
                                    return;
                                }
                            }

                        }
                        if (e.ColumnIndex == 1)
                        {
                            if (grdGratuityFive.CurrentRow.Cells[0].Value == null)
                            {
                                grdGratuityFive.CurrentCell = grdGratuityFive[0, iRowIndex];
                                e.Cancel = true;
                            }
                        }

                        if (e.ColumnIndex == 2)
                        {
                            if (grdGratuityFive.CurrentRow.Cells[1].Value == null)
                            {
                                grdGratuityFive.CurrentCell = grdGratuityFive[1, iRowIndex];
                                e.Cancel = true;
                            }
                        }
                    }
                }

            }
            catch
            {
            }
        }

        private void grdGratuityFive_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (grdGratuityFive.CurrentCell != null)
                {
                    if (grdGratuityFive.CurrentCell.ColumnIndex == ParameterID.Index)
                    {
                        ComboBox cb = (ComboBox)e.Control;
                        cb.KeyPress += new KeyPressEventHandler(cb_KeyPress);
                        cb.DropDownHeight = 120;
                        cb.DropDownStyle = ComboBoxStyle.DropDown;
                        cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        cb.AutoCompleteSource = AutoCompleteSource.ListItems;
                    }
                    if (grdGratuityFive.CurrentCell.ColumnIndex == NoOfMonths.Index || grdGratuityFive.CurrentCell.ColumnIndex == NoOfDays.Index || grdGratuityFive.CurrentCell.ColumnIndex == TerminationDays.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
                this.grdGratuityFive.EditingControl.KeyPress += new KeyPressEventHandler(EditingControlGratuity_KeyPress);

            }
            catch (Exception Ex)
            {
                //ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }
        }

    }
}
