﻿namespace MyBooksERP 
{
    partial class FrmLeavePolicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lblLeaveInfo;
            System.Windows.Forms.Label lblApplicableFrom;
            System.Windows.Forms.Label lblPolicyName;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLeavePolicy));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PolicyMasterBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.btnLeaveTypes = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.StatusStripLeavePolicy = new System.Windows.Forms.StatusStrip();
            this.lblStatusShow = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.grpMain = new System.Windows.Forms.GroupBox();
            this.tbLeave = new System.Windows.Forms.TabControl();
            this.tbpgLeaveTypes = new System.Windows.Forms.TabPage();
            this.dgvLeaveDetail = new DemoClsDataGridview.ClsDataGirdView();
            this.colSerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLeavePolicyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLeaveType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colNoOfLeave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIsEncashable = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colMonthLeave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCarryForwardLeave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIsCommon = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colEncashDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBackupLvDtID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNoofleavesOriginal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LeaveTypeExistance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbpgConsequenceTypes = new System.Windows.Forms.TabPage();
            this.dgvLeaveConsequence = new DemoClsDataGridview.ClsDataGirdView();
            this.colLeaveConsequenceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLeaveTypeID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colCalculationBasedID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colDeductionPercentage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colApplicableDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CboFinYear = new System.Windows.Forms.ComboBox();
            this.txtPolicyName = new System.Windows.Forms.TextBox();
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.LeaveType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Encashable = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Common = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.LeaveTypeID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.CalculationBasedID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.errProLeavePolicy = new System.Windows.Forms.ErrorProvider(this.components);
            this.LeaveDetailid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LeaveId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Noofleaves = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Monthleave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Carryforwardleave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EnchashableDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BackupLvdtlid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoofleavesOriginal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LeaveConsequenceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CalculationPercentage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ApplicableDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            lblLeaveInfo = new System.Windows.Forms.Label();
            lblApplicableFrom = new System.Windows.Forms.Label();
            lblPolicyName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PolicyMasterBindingNavigator)).BeginInit();
            this.PolicyMasterBindingNavigator.SuspendLayout();
            this.StatusStripLeavePolicy.SuspendLayout();
            this.grpMain.SuspendLayout();
            this.tbLeave.SuspendLayout();
            this.tbpgLeaveTypes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLeaveDetail)).BeginInit();
            this.tbpgConsequenceTypes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLeaveConsequence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProLeavePolicy)).BeginInit();
            this.SuspendLayout();
            // 
            // lblLeaveInfo
            // 
            lblLeaveInfo.AutoSize = true;
            lblLeaveInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblLeaveInfo.Location = new System.Drawing.Point(7, 46);
            lblLeaveInfo.Name = "lblLeaveInfo";
            lblLeaveInfo.Size = new System.Drawing.Size(109, 13);
            lblLeaveInfo.TabIndex = 0;
            lblLeaveInfo.Text = "Leave Information";
            // 
            // lblApplicableFrom
            // 
            lblApplicableFrom.AutoSize = true;
            lblApplicableFrom.Location = new System.Drawing.Point(347, 25);
            lblApplicableFrom.Name = "lblApplicableFrom";
            lblApplicableFrom.Size = new System.Drawing.Size(82, 13);
            lblApplicableFrom.TabIndex = 310;
            lblApplicableFrom.Text = "Applicable From";
            // 
            // lblPolicyName
            // 
            lblPolicyName.AutoSize = true;
            lblPolicyName.Location = new System.Drawing.Point(13, 25);
            lblPolicyName.Name = "lblPolicyName";
            lblPolicyName.Size = new System.Drawing.Size(66, 13);
            lblPolicyName.TabIndex = 114;
            lblPolicyName.Text = "Policy Name";
            // 
            // PolicyMasterBindingNavigator
            // 
            this.PolicyMasterBindingNavigator.AddNewItem = null;
            this.PolicyMasterBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.PolicyMasterBindingNavigator.DeleteItem = null;
            this.PolicyMasterBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.btnClear,
            this.toolStripSeparator3,
            this.btnEmail,
            this.btnLeaveTypes,
            this.ToolStripSeparator1,
            this.btnPrint,
            this.btnHelp});
            this.PolicyMasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.PolicyMasterBindingNavigator.MoveFirstItem = null;
            this.PolicyMasterBindingNavigator.MoveLastItem = null;
            this.PolicyMasterBindingNavigator.MoveNextItem = null;
            this.PolicyMasterBindingNavigator.MovePreviousItem = null;
            this.PolicyMasterBindingNavigator.Name = "PolicyMasterBindingNavigator";
            this.PolicyMasterBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.PolicyMasterBindingNavigator.Size = new System.Drawing.Size(744, 25);
            this.PolicyMasterBindingNavigator.TabIndex = 300;
            this.PolicyMasterBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save Data";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Visible = false;
            // 
            // btnLeaveTypes
            // 
            this.btnLeaveTypes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLeaveTypes.Image = global::MyBooksERP.Properties.Resources.Leave1;
            this.btnLeaveTypes.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLeaveTypes.Name = "btnLeaveTypes";
            this.btnLeaveTypes.Size = new System.Drawing.Size(23, 22);
            this.btnLeaveTypes.Text = "Leave Type";
            this.btnLeaveTypes.Click += new System.EventHandler(this.btnLeaveTypes_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 22);
            this.btnHelp.Text = "He&lp";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // StatusStripLeavePolicy
            // 
            this.StatusStripLeavePolicy.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusShow,
            this.lblStatus});
            this.StatusStripLeavePolicy.Location = new System.Drawing.Point(0, 334);
            this.StatusStripLeavePolicy.Name = "StatusStripLeavePolicy";
            this.StatusStripLeavePolicy.Size = new System.Drawing.Size(744, 22);
            this.StatusStripLeavePolicy.TabIndex = 327;
            this.StatusStripLeavePolicy.Text = "StatusStrip1";
            // 
            // lblStatusShow
            // 
            this.lblStatusShow.Name = "lblStatusShow";
            this.lblStatusShow.Size = new System.Drawing.Size(0, 17);
            this.lblStatusShow.ToolTipText = "Status";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(648, 305);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(8, 305);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Enabled = false;
            this.btnOk.Location = new System.Drawing.Point(569, 305);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // grpMain
            // 
            this.grpMain.Controls.Add(this.tbLeave);
            this.grpMain.Controls.Add(this.CboFinYear);
            this.grpMain.Controls.Add(lblLeaveInfo);
            this.grpMain.Controls.Add(lblApplicableFrom);
            this.grpMain.Controls.Add(lblPolicyName);
            this.grpMain.Controls.Add(this.txtPolicyName);
            this.grpMain.Controls.Add(this.ShapeContainer1);
            this.grpMain.Location = new System.Drawing.Point(5, 27);
            this.grpMain.Name = "grpMain";
            this.grpMain.Size = new System.Drawing.Size(734, 272);
            this.grpMain.TabIndex = 0;
            this.grpMain.TabStop = false;
            // 
            // tbLeave
            // 
            this.tbLeave.Controls.Add(this.tbpgLeaveTypes);
            this.tbLeave.Controls.Add(this.tbpgConsequenceTypes);
            this.tbLeave.Location = new System.Drawing.Point(9, 65);
            this.tbLeave.Name = "tbLeave";
            this.tbLeave.SelectedIndex = 0;
            this.tbLeave.Size = new System.Drawing.Size(716, 197);
            this.tbLeave.TabIndex = 5;
            // 
            // tbpgLeaveTypes
            // 
            this.tbpgLeaveTypes.Controls.Add(this.dgvLeaveDetail);
            this.tbpgLeaveTypes.Location = new System.Drawing.Point(4, 22);
            this.tbpgLeaveTypes.Name = "tbpgLeaveTypes";
            this.tbpgLeaveTypes.Padding = new System.Windows.Forms.Padding(3);
            this.tbpgLeaveTypes.Size = new System.Drawing.Size(708, 171);
            this.tbpgLeaveTypes.TabIndex = 0;
            this.tbpgLeaveTypes.Text = "Leave";
            this.tbpgLeaveTypes.UseVisualStyleBackColor = true;
            // 
            // dgvLeaveDetail
            // 
            this.dgvLeaveDetail.AddNewRow = false;
            this.dgvLeaveDetail.AllowUserToResizeColumns = false;
            this.dgvLeaveDetail.AllowUserToResizeRows = false;
            this.dgvLeaveDetail.AlphaNumericCols = new int[0];
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.ControlLight;
            this.dgvLeaveDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvLeaveDetail.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvLeaveDetail.CapsLockCols = new int[0];
            this.dgvLeaveDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLeaveDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSerialNo,
            this.colLeavePolicyID,
            this.colLeaveType,
            this.colNoOfLeave,
            this.colIsEncashable,
            this.colMonthLeave,
            this.colCarryForwardLeave,
            this.colIsCommon,
            this.colEncashDays,
            this.colBackupLvDtID,
            this.colNoofleavesOriginal,
            this.colStatus,
            this.LeaveTypeExistance});
            this.dgvLeaveDetail.DecimalCols = new int[0];
            this.dgvLeaveDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLeaveDetail.HasSlNo = false;
            this.dgvLeaveDetail.LastRowIndex = 0;
            this.dgvLeaveDetail.Location = new System.Drawing.Point(3, 3);
            this.dgvLeaveDetail.Name = "dgvLeaveDetail";
            this.dgvLeaveDetail.NegativeValueCols = new int[0];
            this.dgvLeaveDetail.NumericCols = new int[0];
            this.dgvLeaveDetail.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvLeaveDetail.Size = new System.Drawing.Size(702, 165);
            this.dgvLeaveDetail.TabIndex = 0;
            this.dgvLeaveDetail.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvLeaveDetail_CellBeginEdit);
            this.dgvLeaveDetail.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLeaveDetail_CellEndEdit);
            this.dgvLeaveDetail.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLeaveDetail_RowValidated);
            this.dgvLeaveDetail.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvLeaveDetail_EditingControlShowing);
            this.dgvLeaveDetail.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvLeaveDetail_CurrentCellDirtyStateChanged);
            this.dgvLeaveDetail.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvLeaveDetail_DataError);
            this.dgvLeaveDetail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvLeaveDetail_KeyDown);
            this.dgvLeaveDetail.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLeaveDetail_CellEnter);
            // 
            // colSerialNo
            // 
            this.colSerialNo.HeaderText = "SerialNo";
            this.colSerialNo.Name = "colSerialNo";
            this.colSerialNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colSerialNo.Visible = false;
            // 
            // colLeavePolicyID
            // 
            this.colLeavePolicyID.HeaderText = "LeavePolicyID";
            this.colLeavePolicyID.Name = "colLeavePolicyID";
            this.colLeavePolicyID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colLeavePolicyID.Visible = false;
            // 
            // colLeaveType
            // 
            this.colLeaveType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colLeaveType.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.colLeaveType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.colLeaveType.HeaderText = "Leave Type";
            this.colLeaveType.Name = "colLeaveType";
            this.colLeaveType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // colNoOfLeave
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "N1";
            dataGridViewCellStyle12.NullValue = null;
            this.colNoOfLeave.DefaultCellStyle = dataGridViewCellStyle12;
            this.colNoOfLeave.HeaderText = "Days";
            this.colNoOfLeave.MaxInputLength = 4;
            this.colNoOfLeave.Name = "colNoOfLeave";
            this.colNoOfLeave.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colNoOfLeave.Width = 110;
            // 
            // colIsEncashable
            // 
            this.colIsEncashable.HeaderText = "IsEncashable";
            this.colIsEncashable.Name = "colIsEncashable";
            this.colIsEncashable.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colIsEncashable.Visible = false;
            // 
            // colMonthLeave
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "N1";
            dataGridViewCellStyle13.NullValue = null;
            this.colMonthLeave.DefaultCellStyle = dataGridViewCellStyle13;
            this.colMonthLeave.HeaderText = "Monthly";
            this.colMonthLeave.MaxInputLength = 4;
            this.colMonthLeave.Name = "colMonthLeave";
            this.colMonthLeave.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colMonthLeave.Width = 110;
            // 
            // colCarryForwardLeave
            // 
            this.colCarryForwardLeave.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.Format = "N1";
            dataGridViewCellStyle14.NullValue = null;
            this.colCarryForwardLeave.DefaultCellStyle = dataGridViewCellStyle14;
            this.colCarryForwardLeave.HeaderText = "Carry Forward";
            this.colCarryForwardLeave.MaxInputLength = 4;
            this.colCarryForwardLeave.Name = "colCarryForwardLeave";
            this.colCarryForwardLeave.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colIsCommon
            // 
            this.colIsCommon.HeaderText = "IsCommon";
            this.colIsCommon.Name = "colIsCommon";
            this.colIsCommon.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colIsCommon.Visible = false;
            // 
            // colEncashDays
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle15.Format = "N1";
            dataGridViewCellStyle15.NullValue = null;
            this.colEncashDays.DefaultCellStyle = dataGridViewCellStyle15;
            this.colEncashDays.HeaderText = "Encashable Days";
            this.colEncashDays.MaxInputLength = 4;
            this.colEncashDays.Name = "colEncashDays";
            this.colEncashDays.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colEncashDays.Width = 140;
            // 
            // colBackupLvDtID
            // 
            this.colBackupLvDtID.HeaderText = "BackupLvDtID";
            this.colBackupLvDtID.Name = "colBackupLvDtID";
            this.colBackupLvDtID.Visible = false;
            // 
            // colNoofleavesOriginal
            // 
            this.colNoofleavesOriginal.HeaderText = "NoofleavesOriginal";
            this.colNoofleavesOriginal.Name = "colNoofleavesOriginal";
            this.colNoofleavesOriginal.Visible = false;
            // 
            // colStatus
            // 
            this.colStatus.HeaderText = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.Visible = false;
            // 
            // LeaveTypeExistance
            // 
            this.LeaveTypeExistance.HeaderText = "LeaveTypeExistance";
            this.LeaveTypeExistance.Name = "LeaveTypeExistance";
            this.LeaveTypeExistance.Visible = false;
            // 
            // tbpgConsequenceTypes
            // 
            this.tbpgConsequenceTypes.Controls.Add(this.dgvLeaveConsequence);
            this.tbpgConsequenceTypes.Location = new System.Drawing.Point(4, 22);
            this.tbpgConsequenceTypes.Name = "tbpgConsequenceTypes";
            this.tbpgConsequenceTypes.Padding = new System.Windows.Forms.Padding(3);
            this.tbpgConsequenceTypes.Size = new System.Drawing.Size(708, 171);
            this.tbpgConsequenceTypes.TabIndex = 1;
            this.tbpgConsequenceTypes.Text = "Consequence";
            this.tbpgConsequenceTypes.UseVisualStyleBackColor = true;
            // 
            // dgvLeaveConsequence
            // 
            this.dgvLeaveConsequence.AddNewRow = false;
            this.dgvLeaveConsequence.AlphaNumericCols = new int[0];
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.dgvLeaveConsequence.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvLeaveConsequence.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvLeaveConsequence.CapsLockCols = new int[0];
            this.dgvLeaveConsequence.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLeaveConsequence.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colLeaveConsequenceID,
            this.colOrderNo,
            this.colLeaveTypeID,
            this.colCalculationBasedID,
            this.colDeductionPercentage,
            this.colApplicableDays});
            this.dgvLeaveConsequence.DecimalCols = new int[0];
            this.dgvLeaveConsequence.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLeaveConsequence.HasSlNo = false;
            this.dgvLeaveConsequence.LastRowIndex = 0;
            this.dgvLeaveConsequence.Location = new System.Drawing.Point(3, 3);
            this.dgvLeaveConsequence.Name = "dgvLeaveConsequence";
            this.dgvLeaveConsequence.NegativeValueCols = new int[0];
            this.dgvLeaveConsequence.NumericCols = new int[0];
            this.dgvLeaveConsequence.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvLeaveConsequence.Size = new System.Drawing.Size(702, 165);
            this.dgvLeaveConsequence.TabIndex = 0;
            this.dgvLeaveConsequence.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvLeaveConsequence_CellBeginEdit);
            this.dgvLeaveConsequence.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLeaveConsequence_CellEndEdit);
            this.dgvLeaveConsequence.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvLeaveConsequence_EditingControlShowing);
            this.dgvLeaveConsequence.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvLeaveConsequence_CurrentCellDirtyStateChanged);
            this.dgvLeaveConsequence.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvLeaveConsequence_DataError);
            this.dgvLeaveConsequence.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvLeaveConsequence_KeyDown);
            // 
            // colLeaveConsequenceID
            // 
            this.colLeaveConsequenceID.HeaderText = "ConsequenceID";
            this.colLeaveConsequenceID.Name = "colLeaveConsequenceID";
            this.colLeaveConsequenceID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colLeaveConsequenceID.Visible = false;
            // 
            // colOrderNo
            // 
            this.colOrderNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colOrderNo.HeaderText = "OrderNo";
            this.colOrderNo.MaxInputLength = 2;
            this.colOrderNo.Name = "colOrderNo";
            this.colOrderNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colLeaveTypeID
            // 
            this.colLeaveTypeID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.colLeaveTypeID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.colLeaveTypeID.HeaderText = "Leave Type";
            this.colLeaveTypeID.Name = "colLeaveTypeID";
            this.colLeaveTypeID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colLeaveTypeID.Width = 135;
            // 
            // colCalculationBasedID
            // 
            this.colCalculationBasedID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.colCalculationBasedID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.colCalculationBasedID.HeaderText = "Calculation Based";
            this.colCalculationBasedID.Name = "colCalculationBasedID";
            this.colCalculationBasedID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colCalculationBasedID.Width = 140;
            // 
            // colDeductionPercentage
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colDeductionPercentage.DefaultCellStyle = dataGridViewCellStyle17;
            this.colDeductionPercentage.HeaderText = "Deduction Percentage";
            this.colDeductionPercentage.MaxInputLength = 4;
            this.colDeductionPercentage.Name = "colDeductionPercentage";
            this.colDeductionPercentage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colDeductionPercentage.Width = 130;
            // 
            // colApplicableDays
            // 
            this.colApplicableDays.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle18.Format = "N1";
            dataGridViewCellStyle18.NullValue = null;
            this.colApplicableDays.DefaultCellStyle = dataGridViewCellStyle18;
            this.colApplicableDays.HeaderText = "Applicable Days";
            this.colApplicableDays.MaxInputLength = 5;
            this.colApplicableDays.Name = "colApplicableDays";
            this.colApplicableDays.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CboFinYear
            // 
            this.CboFinYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboFinYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboFinYear.BackColor = System.Drawing.SystemColors.Info;
            this.CboFinYear.DropDownHeight = 134;
            this.CboFinYear.FormattingEnabled = true;
            this.CboFinYear.IntegralHeight = false;
            this.CboFinYear.Location = new System.Drawing.Point(450, 21);
            this.CboFinYear.Name = "CboFinYear";
            this.CboFinYear.Size = new System.Drawing.Size(206, 21);
            this.CboFinYear.TabIndex = 2;
            this.CboFinYear.SelectionChangeCommitted += new System.EventHandler(this.CboFinYear_SelectionChangeCommitted);
            this.CboFinYear.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboFinYear_KeyDown);
            // 
            // txtPolicyName
            // 
            this.txtPolicyName.BackColor = System.Drawing.SystemColors.Info;
            this.txtPolicyName.Location = new System.Drawing.Point(100, 21);
            this.txtPolicyName.MaxLength = 50;
            this.txtPolicyName.Name = "txtPolicyName";
            this.txtPolicyName.Size = new System.Drawing.Size(226, 20);
            this.txtPolicyName.TabIndex = 1;
            this.txtPolicyName.TextChanged += new System.EventHandler(this.txtPolicyName_TextChanged);
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.ShapeContainer1.Size = new System.Drawing.Size(728, 253);
            this.ShapeContainer1.TabIndex = 312;
            this.ShapeContainer1.TabStop = false;
            // 
            // LineShape1
            // 
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 89;
            this.LineShape1.X2 = 716;
            this.LineShape1.Y1 = 36;
            this.LineShape1.Y2 = 36;
            // 
            // LeaveType
            // 
            this.LeaveType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LeaveType.HeaderText = "LeaveType";
            this.LeaveType.Name = "LeaveType";
            this.LeaveType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Encashable
            // 
            this.Encashable.HeaderText = "Encashable";
            this.Encashable.Name = "Encashable";
            this.Encashable.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Encashable.Visible = false;
            // 
            // Common
            // 
            this.Common.HeaderText = "Common";
            this.Common.Name = "Common";
            this.Common.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Common.Visible = false;
            // 
            // LeaveTypeID
            // 
            this.LeaveTypeID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LeaveTypeID.HeaderText = "Leave Type";
            this.LeaveTypeID.Name = "LeaveTypeID";
            this.LeaveTypeID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LeaveTypeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.LeaveTypeID.Width = 200;
            // 
            // CalculationBasedID
            // 
            this.CalculationBasedID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CalculationBasedID.HeaderText = "Calculation Based";
            this.CalculationBasedID.Name = "CalculationBasedID";
            this.CalculationBasedID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CalculationBasedID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.CalculationBasedID.Width = 200;
            // 
            // tmrClear
            // 
            this.tmrClear.Tick += new System.EventHandler(this.tmrClear_Tick);
            // 
            // errProLeavePolicy
            // 
            this.errProLeavePolicy.ContainerControl = this;
            this.errProLeavePolicy.RightToLeft = true;
            // 
            // LeaveDetailid
            // 
            this.LeaveDetailid.HeaderText = "LeaveDetailid";
            this.LeaveDetailid.Name = "LeaveDetailid";
            this.LeaveDetailid.Visible = false;
            // 
            // LeaveId
            // 
            this.LeaveId.HeaderText = "LeaveId";
            this.LeaveId.Name = "LeaveId";
            this.LeaveId.Visible = false;
            // 
            // Noofleaves
            // 
            this.Noofleaves.HeaderText = "Days";
            this.Noofleaves.MaxInputLength = 3;
            this.Noofleaves.Name = "Noofleaves";
            this.Noofleaves.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Monthleave
            // 
            this.Monthleave.HeaderText = "Monthly";
            this.Monthleave.MaxInputLength = 2;
            this.Monthleave.Name = "Monthleave";
            this.Monthleave.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Carryforwardleave
            // 
            this.Carryforwardleave.HeaderText = "Carry Forward";
            this.Carryforwardleave.MaxInputLength = 3;
            this.Carryforwardleave.Name = "Carryforwardleave";
            this.Carryforwardleave.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // EnchashableDays
            // 
            this.EnchashableDays.HeaderText = "Encashable Days";
            this.EnchashableDays.MaxInputLength = 3;
            this.EnchashableDays.Name = "EnchashableDays";
            this.EnchashableDays.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BackupLvdtlid
            // 
            this.BackupLvdtlid.HeaderText = "BackupLvdtlid";
            this.BackupLvdtlid.Name = "BackupLvdtlid";
            this.BackupLvdtlid.Visible = false;
            // 
            // NoofleavesOriginal
            // 
            this.NoofleavesOriginal.HeaderText = "NoofleavesOriginal";
            this.NoofleavesOriginal.Name = "NoofleavesOriginal";
            this.NoofleavesOriginal.ReadOnly = true;
            this.NoofleavesOriginal.Visible = false;
            // 
            // Status
            // 
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Visible = false;
            // 
            // LeaveConsequenceID
            // 
            dataGridViewCellStyle19.Format = "N2";
            dataGridViewCellStyle19.NullValue = null;
            this.LeaveConsequenceID.DefaultCellStyle = dataGridViewCellStyle19;
            this.LeaveConsequenceID.HeaderText = "LeaveConsequenceID";
            this.LeaveConsequenceID.Name = "LeaveConsequenceID";
            this.LeaveConsequenceID.Visible = false;
            // 
            // OrderNo
            // 
            this.OrderNo.HeaderText = "OrderNo";
            this.OrderNo.MaxInputLength = 3;
            this.OrderNo.Name = "OrderNo";
            this.OrderNo.Width = 60;
            // 
            // CalculationPercentage
            // 
            dataGridViewCellStyle20.Format = "N2";
            dataGridViewCellStyle20.NullValue = null;
            this.CalculationPercentage.DefaultCellStyle = dataGridViewCellStyle20;
            this.CalculationPercentage.HeaderText = "Calculation Percentage";
            this.CalculationPercentage.MaxInputLength = 6;
            this.CalculationPercentage.Name = "CalculationPercentage";
            // 
            // ApplicableDays
            // 
            this.ApplicableDays.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ApplicableDays.HeaderText = "Applicable Days";
            this.ApplicableDays.MaxInputLength = 3;
            this.ApplicableDays.Name = "ApplicableDays";
            // 
            // FrmLeavePolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 356);
            this.Controls.Add(this.StatusStripLeavePolicy);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.grpMain);
            this.Controls.Add(this.PolicyMasterBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLeavePolicy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Leave Policy";
            this.Load += new System.EventHandler(this.FrmLeavePolicy_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmLeavePolicy_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmLeavePolicy_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.PolicyMasterBindingNavigator)).EndInit();
            this.PolicyMasterBindingNavigator.ResumeLayout(false);
            this.PolicyMasterBindingNavigator.PerformLayout();
            this.StatusStripLeavePolicy.ResumeLayout(false);
            this.StatusStripLeavePolicy.PerformLayout();
            this.grpMain.ResumeLayout(false);
            this.grpMain.PerformLayout();
            this.tbLeave.ResumeLayout(false);
            this.tbpgLeaveTypes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLeaveDetail)).EndInit();
            this.tbpgConsequenceTypes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLeaveConsequence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProLeavePolicy)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator PolicyMasterBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripButton btnLeaveTypes;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        internal System.Windows.Forms.StatusStrip StatusStripLeavePolicy;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatusShow;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.GroupBox grpMain;
        internal System.Windows.Forms.TabControl tbLeave;
        internal System.Windows.Forms.TabPage tbpgLeaveTypes;
        internal System.Windows.Forms.TabPage tbpgConsequenceTypes;
        internal System.Windows.Forms.ComboBox CboFinYear;
        internal System.Windows.Forms.TextBox txtPolicyName;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.DataGridViewTextBoxColumn LeaveDetailid;
        internal System.Windows.Forms.DataGridViewTextBoxColumn LeaveId;
        internal System.Windows.Forms.DataGridViewComboBoxColumn LeaveType;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Noofleaves;
        internal System.Windows.Forms.DataGridViewCheckBoxColumn Encashable;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Monthleave;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Carryforwardleave;
        internal System.Windows.Forms.DataGridViewTextBoxColumn EnchashableDays;
        internal System.Windows.Forms.DataGridViewCheckBoxColumn Common;
        internal System.Windows.Forms.DataGridViewTextBoxColumn BackupLvdtlid;
        internal System.Windows.Forms.DataGridViewTextBoxColumn NoofleavesOriginal;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Status;
        internal System.Windows.Forms.DataGridViewTextBoxColumn LeaveConsequenceID;
        internal System.Windows.Forms.DataGridViewTextBoxColumn OrderNo;
        internal System.Windows.Forms.DataGridViewComboBoxColumn LeaveTypeID;
        internal System.Windows.Forms.DataGridViewComboBoxColumn CalculationBasedID;
        internal System.Windows.Forms.DataGridViewTextBoxColumn CalculationPercentage;
        internal System.Windows.Forms.DataGridViewTextBoxColumn ApplicableDays;
        private DemoClsDataGridview.ClsDataGirdView dgvLeaveConsequence;
        private DemoClsDataGridview.ClsDataGirdView dgvLeaveDetail;
        private System.Windows.Forms.Timer tmrClear;
        private System.Windows.Forms.ErrorProvider errProLeavePolicy;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLeaveConsequenceID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOrderNo;
        private System.Windows.Forms.DataGridViewComboBoxColumn colLeaveTypeID;
        private System.Windows.Forms.DataGridViewComboBoxColumn colCalculationBasedID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeductionPercentage;
        private System.Windows.Forms.DataGridViewTextBoxColumn colApplicableDays;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLeavePolicyID;
        private System.Windows.Forms.DataGridViewComboBoxColumn colLeaveType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNoOfLeave;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIsEncashable;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMonthLeave;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCarryForwardLeave;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIsCommon;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEncashDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBackupLvDtID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNoofleavesOriginal;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn LeaveTypeExistance;
    }
}