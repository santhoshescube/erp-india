﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection; //

namespace MyBooksERP 
{


    /*****************************************************
    * Created By       : Alvin
    * Creation Date    : 12 Apr 2012
    * Description      : Handle Holiday Type
    * ***************************************************/

    public partial class FrmHolidayType : Form
    {
        private ComboBox cboColor;

        private bool MblnPrintEmailPermission = false;     //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;       //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;  //To set Add Update Permission

        public bool MblnIsEditMode = false;

        private int MintRecordCnt = 0;
        private int MintCurrentRecCnt = 0;

        private clsMessage ObjUserMessage = null;
        private clsBLLHolidayType MobjBLLHolidayType = null;

        public FrmHolidayType()
        {
            InitializeComponent();
            InitializeCombo();
            this.GrpHolidayType.Controls.Add(cboColor);

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.HolidayType, this);
        //}

        private void FrmHolidayType_Load(object sender, EventArgs e)
        {
            FilterCombo();
            SetPermissions();
            
            //LoadInitial();
            AddNewHolidayType();
        }

        private clsMessage objUserMessage = null;
        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.HolidayType);

                return this.objUserMessage;
            }
        }

        public clsBLLHolidayType BLLHolidayType  
        {
            get
            {
                if (this.MobjBLLHolidayType == null)
                    this.MobjBLLHolidayType = new clsBLLHolidayType();

                return this.MobjBLLHolidayType;
            }
        }



    #region Methods

        private void SetPermissions()
        {
            // Function for setting permissions
            //clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            //if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            //{
            //    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)ModuleID.Company, (Int32)MenuID.NewCompany, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            //}
            //else
            MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }
        private void InitializeCombo()
        {
            cboColor = new ComboBox();
            cboColor.AutoCompleteMode =AutoCompleteMode.SuggestAppend;
            cboColor.AutoCompleteSource = AutoCompleteSource.ListItems;
            cboColor.AllowDrop = false;
            cboColor.DrawMode = DrawMode.OwnerDrawFixed;
            cboColor.DropDownStyle = ComboBoxStyle.DropDownList;
            cboColor.FlatStyle = FlatStyle.Popup;
            cboColor.DropDownHeight = 134;
            cboColor.Location = new Point(102,55);
            cboColor.Width = 170;
            cboColor.Height = 20;
            cboColor.DrawItem += new DrawItemEventHandler(cboColor_DrawItem);
            cboColor.Leave += new EventHandler(cboColor_Leave);
            cboColor.SelectedIndexChanged += new EventHandler(cboColor_SelectedIndexChanged);

        }

        private void FilterCombo()
        {

            Type colorType = typeof(System.Drawing.Color);
            PropertyInfo[] propInfoList = colorType.GetProperties(BindingFlags.Static | BindingFlags.DeclaredOnly | BindingFlags.Public);
            this.cboColor.Items.Clear();
            foreach (PropertyInfo c in propInfoList)
            {
                this.cboColor.Items.Add(c.Name);
            }

        }

        private void GetRecordCount()
        {
            MintRecordCnt = 0;

            MintRecordCnt = BLLHolidayType.GetRecordCount();
            if (MintRecordCnt < 0)
            {
                BindingNavigatorCountItem.Text = "of 0";
                MintRecordCnt = 0;
            }

        }

        private void AddNewHolidayType()
        {
            MblnIsEditMode = false;
            tmrClear.Enabled = true;
            errProHolidayType.Clear();
            ClearAllControls();
            FilterCombo();

            GetRecordCount();
            MintRecordCnt = MintRecordCnt + 1;
            MintCurrentRecCnt = MintRecordCnt;
            BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();

            lblStatus.Text = "";


            BindingNavigatorAddNewItem.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = false;
            btnOk.Enabled = false;
            btnSave.Enabled = false;
            BindingNavigatorSaveItem.Enabled = false;
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = false;
            BindingNavigatorMoveNextItem.Enabled = false;
        }

        private void DisplayHoidayTypeInfo()
        {
            FillHoidayTypeInfo();
            MblnIsEditMode = true;
            BindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
            BindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            BindingNavigatorSaveItem.Enabled = false;
            btnSave.Enabled = false;
            btnOk.Enabled = false;
            SetBindingNavigatorButtons();
        }

        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
        }

        private void FillHoidayTypeInfo()
        {
                if (BLLHolidayType.DisplayHolidayType(MintCurrentRecCnt))
                {
                    txtDescription.Text = BLLHolidayType.DTOHolidayType.strHolidayType.ToString();
                    cboColor.Text = BLLHolidayType.DTOHolidayType.strColor.ToString();
                }
        }

        private bool SaveHolidayType()
        {
            bool blnRetValue = false;
            if (FormValidation())
            {
                int intMessageCode = 0;
                if (MblnIsEditMode == false)
                    intMessageCode = 8050;
                else
                    intMessageCode = 8051;

                if (UserMessage.ShowMessage(intMessageCode))
                {
                    FillParametrHolidayType();
                    blnRetValue = BLLHolidayType.HolidayTypeSave(MblnIsEditMode);

                    MblnIsEditMode = true;
                }

            }

            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorSaveItem.Enabled = false;
            btnSave.Enabled = false;
            btnOk.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;

            return blnRetValue;

        }

        private bool FormValidation()
        {
            errProHolidayType.Clear();

            if (txtDescription.Text == "")
            {
                errProHolidayType.SetError(txtDescription, UserMessage.GetMessageByCode(8055));
                UserMessage.ShowMessage(8055);
                txtDescription.Focus();
                return false;
            }

            if (cboColor.Text == "")
            {
                errProHolidayType.SetError(cboColor, UserMessage.GetMessageByCode(8056));
                UserMessage.ShowMessage(8056);
                cboColor.Focus();
                return false;
            }

            if(BLLHolidayType.CheckDuplicate(BLLHolidayType.DTOHolidayType.intHolidayTypeID,txtDescription.Text.Trim()))
            {
                errProHolidayType.SetError(txtDescription, UserMessage.GetMessageByCode(8057));
                UserMessage.ShowMessage(8057);
                cboColor.Focus();
                return false;
            }

            return true;
        }


        private void ClearAllControls()
        {
            txtDescription.Text = "";
            cboColor.SelectedValue = -1;
        }

        private void FillParametrHolidayType()
        {
            BLLHolidayType.DTOHolidayType.strHolidayType = txtDescription.Text.ToString();
            BLLHolidayType.DTOHolidayType.strColor = cboColor.Text;
        }


        private void Changestatus()
        {
            //function for changing status

            if (!MblnIsEditMode)
            {
                btnOk.Enabled = MblnAddPermission;
                btnSave.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
            else
            {
                btnOk.Enabled = MblnUpdatePermission;
                btnSave.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            errProHolidayType.Clear();
        }

        private bool DeleteHolidayType()
        {
            bool blnRetValue = false;
            if (DeleteValidation())
            {
                if (UserMessage.ShowMessage(8052) == true)
                {
                    if (BLLHolidayType.DeleteHolidayType())
                    {
                        blnRetValue = true;
                   }
                }

            }
            return blnRetValue;

        }


        private bool DeleteValidation()
        {
            if (BLLHolidayType.HolidayTypeIDExists())
            {
                UserMessage.ShowMessage(8054);
                return false;
            }
            return true;

        }

       
    #endregion


        private void cboColor_DrawItem(object sender, DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            Rectangle rect = e.Bounds;
            if (e.Index >= 0)
            {
                string n = ((ComboBox)sender).Items[e.Index].ToStringCustom();
                Font f = new Font("Arial", 9, FontStyle.Regular);
                Color c = Color.FromName(n);
                Brush b = new SolidBrush(c);
                StringFormat sf = new StringFormat();
                sf.Alignment = StringAlignment.Near;
                g.DrawString(n, f, Brushes.Black, rect.X, rect.Top);

                if (e.State == DrawItemState.NoAccelerator || e.State == DrawItemState.NoFocusRect)
                {
                    e.Graphics.FillRectangle(new SolidBrush(Color.White), rect);
                    e.Graphics.DrawString(n, f, new SolidBrush(Color.Black), rect, sf);
                    e.DrawFocusRectangle();

                }
                else
                {
                    e.Graphics.FillRectangle(new SolidBrush(Color.LightBlue), rect);
                    e.Graphics.DrawString(n, f, new SolidBrush(Color.Red), rect, sf);
                    e.DrawFocusRectangle();
                }

                g.FillRectangle(b, rect.X + 130, rect.Y + 2, rect.Width - 10, rect.Height - 4);

            }
        }

        private void cboColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void cboColor_Leave(object sender, EventArgs e)
        {

        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            try
            {
                AddNewHolidayType();
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);

            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    DisplayHoidayTypeInfo();
                    lblStatus.Text = UserMessage.GetMessageByCode(10);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 1;
            }


        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {

            GetRecordCount();
            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;
                DisplayHoidayTypeInfo();
                lblStatus.Text = UserMessage.GetMessageByCode(9);
                tmrClear.Enabled = true;

            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                }

                if (MintCurrentRecCnt > MintRecordCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                }
                else
                {
                    DisplayHoidayTypeInfo();
                    lblStatus.Text = UserMessage.GetMessageByCode(11);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 0;
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    DisplayHoidayTypeInfo();
                     lblStatus.Text = UserMessage.GetMessageByCode(12);
                    tmrClear.Enabled = true;
                }
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveHolidayType())
            {
                UserMessage.ShowMessage(2);
                lblStatus.Text = UserMessage.GetMessageByCode(2);
            }
        }

        private void txtDescription_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveHolidayType())
            {
                UserMessage.ShowMessage(2);
                lblStatus.Text = UserMessage.GetMessageByCode(2);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveHolidayType())
            {
                UserMessage.ShowMessage(2);
                lblStatus.Text = UserMessage.GetMessageByCode(2);
                this.Close();

            }
           
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (DeleteHolidayType())
            {
                UserMessage.ShowMessage(8053);
                lblStatus.Text = UserMessage.GetMessageByCode(8053);
                AddNewHolidayType();
            }
        }

    }
}
