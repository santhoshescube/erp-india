﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Microsoft.VisualBasic;
using BLL;
using DTO;
namespace MyBooksERP
{

    /*****************************************************
     * Modification By  : Ranju Mathew
     * Creation Date    : 13 Aug 2013
     * Description      : Tuning and performance improving-Vacation Policy 
     * ***************************************************/
    public partial class frmVacationPolicy : Form
    {
        #region Declartions

        private bool MbChangeStatus;//Check state of the page
        private bool MblnAddStatus;//Add/Update mode 
        private bool MbKeydown;
        private bool MblnbtnOk = false;
        // permissions
        int CurrentRecCnt = 0;
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnAddUpdatePermission = false; // To set add/update permission
        private bool MblnPrintEmailPermission = false;//To Set PrintEmail Permission

        int RecordCnt = 0;

        private bool MblnShowErrorMess;

        private string MstrMessageCommon; //Messagebox display
        private string mDurationGap;
        private string MstrMessageCaption;              //Message caption

        private ArrayList MsarMessageArr; // Error Message display
        private ArrayList MaStatusMessage;// Status bar error message display

        private TabPage TempTab;//To store Dynamic Shift Tab
        private int MiShiftID = 0;// Display of details when clicking grid
        public int PiCompanyID = 0;// Reference From Work policy
        public int PiShiftID = 0; // Reference from Shift policy
        private int TotalRecordCnt;
        private int MintTimerInterval;                  // To set timer interval
        private int MintCompanyId;                      // current companyid
        private int MintUserId;
        public int intVacationPolicyID { get; set; }

        private MessageBoxIcon MmessageIcon;            // to set the message icon
        string strPolicyNameCheck = "";
        private bool Glb24HourFormat;

        ClsLogWriter mObjLogs;
        ClsNotification mObjNotification;
        clsBLLVacationPolicy MobjclsBLLVacationPolicy;
        string strBindingOf = "Of ";
        #endregion Declartions

        #region Constructor

        public frmVacationPolicy()
        {
            //Constructor
            InitializeComponent();
            MblnShowErrorMess = true;
            MintCompanyId = ClsCommonSettings.LoginCompanyID;
            MintUserId = ClsCommonSettings.UserID;
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            Glb24HourFormat = ClsCommonSettings.Glb24HourFormat;
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            MobjclsBLLVacationPolicy = new clsBLLVacationPolicy();

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.VacationPolicy, this);

        //    strBindingOf = "من ";
        //    DetailsDataGridViewVacation.Columns["txtParticulars"].HeaderText = "تفاصيل";
        //}
        #endregion Constructor

        #region methods

        #region ClearAllControls
        /// <summary>
        /// To Clear all the Details
        /// </summary>
        private void ClearAllControls()
        {

            this.txtPolicyName.Text = "";
            this.cboCalculationBasedVacation.SelectedIndex = -1;
            this.DetailsDataGridViewVacation.Rows.Clear();
            //this.chkRateOnlyVacation.Checked = false;
            //this.txtRateVacation.Text = "";
            //this.txtFixedNoofTickets.Text = "";
            this.txtOnTimeRejoinBenefit.Text = "";
            //this.txtFixedTicketAmount.Text = "";
            this.dgvVacation.Rows.Clear();
        }

        #endregion ClearAllControls

        #region SaveCompanyVacationPolicy
        /// <summary>
        /// To Save vacation Policy
        /// </summary>
        /// <returns></returns>
        private bool SaveCompanyVacationPolicy()
        {
            FillMasterParameter();
            bool flag = false;
            flag = MobjclsBLLVacationPolicy.InsertVacationPolicyMaster();
            if (flag == true)
            {
                FillAndInsertParticularExcludeParameter();
                FillAndInsertDetailsParameter();
                return true;
            }
            return false;
        }
        #endregion SaveCompanyVacationPolicy

        #region FillAndInsertParticularExcludeParameter
        /// <summary>
        /// To Fill and Insert ParticularExclude Details 
        /// </summary>
        private void FillAndInsertParticularExcludeParameter()
        {
            if (!MblnAddStatus)
            {
                MobjclsBLLVacationPolicy.DeleteSalaryPolicyDetail();
            }
            for (int i = 0; DetailsDataGridViewVacation.Rows.Count - 1 >= i; ++i)
            {

                if (Convert.ToBoolean(DetailsDataGridViewVacation.Rows[i].Cells["chkParticular"].Value) == true)
                {
                    MobjclsBLLVacationPolicy.clsDTOVacationPolicy.intParticularID = Convert.ToInt32(DetailsDataGridViewVacation.Rows[i].Cells["txtAddDedID"].Value);
                    MobjclsBLLVacationPolicy.InsertSalaryPolicyDetail();
                }
            }
        }
        #endregion FillAndInsertParticularExcludeParameter

        #region SetPermissions
        /// <summary>
        /// To Set Permissions For Add /Update /Delete/ Email
        /// </summary>
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID >3 )
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.VacationPolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }
        #endregion SetPermissions

        #region LoadMessage
        /// <summary>
        /// To Load Messages From Notification Master
        /// </summary>
        private void LoadMessage()
        {
            // Loading Message
            try
            {
                MsarMessageArr = new ArrayList();
                MsarMessageArr = mObjNotification.FillMessageArray((int)FormID.VacationPolicy, 2);
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on LoadMessage " + this.Name + " " + Ex.Message.ToString(), 1);
            }

        }
        #endregion LoadMessage

        #region UpdateCompanyVacationPolicy
        /// <summary>
        /// To Update CompanyVacationPolicy 
        /// </summary>
        /// <returns></returns>
        private bool UpdateCompanyVacationPolicy()
        {
            FillMasterParameter();
            bool flag = false;
            flag = MobjclsBLLVacationPolicy.UpdateVacationPolicyMaster();
            if (flag == true)
            {
                //MobjclsVacationPolicy.DeleteSalaryPolicyDetail();
                FillAndInsertParticularExcludeParameter();
                //MobjclsVacationPolicy.DeleteVacationPolicyDetail();
                FillAndInsertDetailsParameter();
                return true;
            }
            return false;
        }
        #endregion UpdateCompanyVacationPolicy

        #region FillAndInsertDetailsParameter
        /// <summary>
        /// To Fill And Insert Details Parameter
        /// </summary>
        private void FillAndInsertDetailsParameter()
        {


            MobjclsBLLVacationPolicy.clsDTOVacationPolicy.lstclsDTOVacationPolicyDetail = new System.Collections.Generic.List<DTO.clsDTOVacationPolicyDetail>();


            for (int i = 0; Convert.ToInt32(dgvVacation.Rows.Count) - 2 >= i; ++i)
            {
                clsDTOVacationPolicyDetail objclsDTOVacationPolicyDetail = new clsDTOVacationPolicyDetail();
                objclsDTOVacationPolicyDetail.ParameterID = dgvVacation.Rows[i].Cells["cboParameter"].Tag.ToInt32();
                if (Convert.ToInt32(objclsDTOVacationPolicyDetail.ParameterID) == 0)
                {
                    continue;
                }
                if (Microsoft.VisualBasic.Information.IsNumeric(dgvVacation.Rows[i].Cells["txtNoOfMonths"].Value) == true)
                {
                    objclsDTOVacationPolicyDetail.NoOfMonths = Convert.ToDouble(dgvVacation.Rows[i].Cells["txtNoOfMonths"].Value);
                }
                else
                {
                    objclsDTOVacationPolicyDetail.NoOfMonths = 0;
                }
                if (Microsoft.VisualBasic.Information.IsNumeric(dgvVacation.Rows[i].Cells["txtNoOfDays"].Value) == true)
                {
                    objclsDTOVacationPolicyDetail.NoOfDays = Convert.ToDouble(dgvVacation.Rows[i].Cells["txtNoOfDays"].Value);
                }
                else
                {
                    objclsDTOVacationPolicyDetail.NoOfDays = 0;
                }

                if (Microsoft.VisualBasic.Information.IsNumeric(dgvVacation.Rows[i].Cells["txtNoOfTickets"].Value) == true)
                {
                    objclsDTOVacationPolicyDetail.NoOfTickets = Convert.ToDouble(dgvVacation.Rows[i].Cells["txtNoOfTickets"].Value);
                }
                else
                {
                    objclsDTOVacationPolicyDetail.NoOfTickets = 0;
                }

                if (Microsoft.VisualBasic.Information.IsNumeric(dgvVacation.Rows[i].Cells["txtTicketAmount"].Value) == true)
                {
                    objclsDTOVacationPolicyDetail.TicketAmount = Convert.ToDouble(dgvVacation.Rows[i].Cells["txtTicketAmount"].Value);
                }
                else
                {
                    objclsDTOVacationPolicyDetail.TicketAmount = 0;
                }
                MobjclsBLLVacationPolicy.clsDTOVacationPolicy.lstclsDTOVacationPolicyDetail.Add(objclsDTOVacationPolicyDetail);


            }



            MobjclsBLLVacationPolicy.InsertVacationPolicyDetails();

        }
        #endregion FillAndInsertDetailsParameter

        #region FillMasterParameter
        /// <summary>
        /// To Fill Master Parameters to Dto 
        /// </summary>
        private void FillMasterParameter()
        {

            MobjclsBLLVacationPolicy.clsDTOVacationPolicy.VacationPolicy = txtPolicyName.Text.Trim();
            if (cboCalculationBasedVacation.SelectedIndex != -1)
            {
                MobjclsBLLVacationPolicy.clsDTOVacationPolicy.CalculationID = Convert.ToInt32(cboCalculationBasedVacation.SelectedValue);
            }
            else
            {
                MobjclsBLLVacationPolicy.clsDTOVacationPolicy.CalculationID = 0;
            }

         
            if (Microsoft.VisualBasic.Information.IsNumeric(txtOnTimeRejoinBenefit.Text))
            {
                MobjclsBLLVacationPolicy.clsDTOVacationPolicy.dblOnTimeRejoinBenefit = Convert.ToDecimal(txtOnTimeRejoinBenefit.Text);
            }
            else
            {
                MobjclsBLLVacationPolicy.clsDTOVacationPolicy.dblOnTimeRejoinBenefit = 0;
            }
            MobjclsBLLVacationPolicy.clsDTOVacationPolicy.chkFullSalary = chkFullSalary.Checked;
            MobjclsBLLVacationPolicy.clsDTOVacationPolicy.chkLeaveOnly = chkLeaveOnly.Checked;
            MobjclsBLLVacationPolicy.clsDTOVacationPolicy.IsRateOnly = false;
            MobjclsBLLVacationPolicy.clsDTOVacationPolicy.RatePerDay = 0;
            MobjclsBLLVacationPolicy.clsDTOVacationPolicy.NoOfTickets = 0;
            MobjclsBLLVacationPolicy.clsDTOVacationPolicy.TicketAmount = 0;
            // }
        }
        #endregion FillMasterParameter

        #region EnableTimer
        /// <summary>
        /// To Enable Timer
        /// </summary>
        private void EnableTimer()
        {
            tmrVacationPolicy.Enabled = false;
            tmrVacationPolicy.Enabled = true;
        }
        #endregion EnableTimer

        #region formValidation
        /// <summary>
        /// Form Validation 
        /// </summary>
        /// <returns></returns>
        private bool formValidation()
        {
            ////////////////////////////////////////////////////////////////

            if (txtPolicyName.Text.Trim() == "")
            {
                //Enter PolicyName
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7510, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                EnableTimer();
                txtPolicyName.Focus();
                return false;
            }
            if (MobjclsBLLVacationPolicy.CheckploicyNameDup(MobjclsBLLVacationPolicy.clsDTOVacationPolicy.VacationPolicyID, txtPolicyName.Text.Trim()))
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7511, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                EnableTimer();
                txtPolicyName.Focus();
                return false;
            }
            if (MblnAddStatus == true)
            {
                //Policy name duplication(add mode)
                if (MobjclsBLLVacationPolicy.checkDuplication(txtPolicyName.Text.Trim()))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7511, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                    EnableTimer();
                    txtPolicyName.Focus();
                    return false;
                }
            }
            else
            {
                //Policy name duplication(update mode)
                if (strPolicyNameCheck != txtPolicyName.Text.Trim())
                {
                    if (MobjclsBLLVacationPolicy.checkDuplication(txtPolicyName.Text.Trim()))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7511, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                        EnableTimer();
                        txtPolicyName.Focus();
                        return false;
                    }
                }
            }
            //if (chkRateOnlyVacation.Checked == true && (txtRateVacation.Text == "" ))
            //{
            //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7518, out MmessageIcon);
            //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
            //    EnableTimer();
            //    txtPolicyName.Focus();
            //    return false;
            //}
            bool blnValuePresent = false; int i = 0;

            if (cboCalculationBasedVacation.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7512, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                EnableTimer();
                cboCalculationBasedVacation.Focus();
                return false;
            }
            if (dgvVacation.Rows.Count == 1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7519, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                EnableTimer();
                dgvVacation.Focus();
                return false;

            }
            if (dgvVacation.Rows.Count > 1)
            {
                for (i = 0; i <= dgvVacation.Rows.Count.ToInt32() - 2; ++i)
                {
                    if (Convert.ToString(dgvVacation.Rows[i].Cells["cboParameter"].Value) != "")
                    {

                        if (dgvVacation.Rows[i].Cells["txtNoOfMonths"].Value.ToDecimal() <= 0 )
                        {
                            //if (ClsCommonSettings.IsArabicView)
                            //    MstrMessageCommon = "يرجى إدخال رقم أشهر.";
                            //else
                                MstrMessageCommon = " Please Enter No Of Months ";
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                            EnableTimer();
                            return false;
                        }
                        if ((dgvVacation.Rows[i].Cells["txtNoOfDays"].Value.ToDecimal() <= 0 ) && (dgvVacation.Rows[i].Cells["txtNoOfTickets"].Value.ToDecimal() <= 0 ))
                        {
                            //if (ClsCommonSettings.IsArabicView)
                            //    MstrMessageCommon = "الرجاء إدخال عدد الأوراق.";
                            //else
                                MstrMessageCommon = " Please Enter No Of Leave Days OR No of Tickets ";
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                            EnableTimer();
                            return false;

                        }
                    }
                }
            }

            for (i = 0; dgvVacation.Rows.Count.ToInt32() - 2 >= i; ++i)
            {
                for (int j = i + 1; dgvVacation.Rows.Count.ToInt32() - 2 >= j; ++j)
                {
                    if ((dgvVacation.Rows[i].Cells["cboParameter"].Tag.ToInt32() == dgvVacation.Rows[j].Cells["cboParameter"].Tag.ToInt32()) &&(dgvVacation.Rows[i].Cells["txtNoOfMonths"].Value.ToDecimal() == dgvVacation.Rows[j].Cells["txtNoOfMonths"].Value.ToDecimal()) && (dgvVacation.Rows[i].Cells["txtNoOfDays"].Value.ToDecimal() == dgvVacation.Rows[j].Cells["txtNoOfDays"].Value.ToDecimal() ))
                    {
                   
                        //MessageBox.Show("Parameter and months duplicating")
                        dgvVacation.CurrentCell = dgvVacation.Rows[j].Cells["txtNoOfMonths"];
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7517, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                        EnableTimer();
                        return false;
                      
                    }
                }
            }
            return true;
        }
        #endregion formValidation

        #region UpdateMode
        /// <summary>
        /// To Change The Status
        /// </summary>
        private void UpdateMode()
        {
            MblnAddStatus = false;
            Clear();
        }
        #endregion UpdateMode

        #region Clear
        /// <summary>
        /// To Clear All the Field
        /// </summary>
        private void Clear()
        {
            txtPolicyName.Text = "";
            cboCalculationBasedVacation.SelectedIndex = -1;
            txtOnTimeRejoinBenefit.Text = "";
            DetailsDataGridViewVacation.Rows.Clear();
            chkLeaveOnly.Checked = false;
            chkFullSalary.Checked = false; 
            dgvVacation.Rows.Clear();
            txtPolicyName.Focus();
        }
        #endregion Clear

        #region RecCountNavigate
        /// <summary>
        /// To assgin ReCount
        /// </summary>
        private void RecCountNavigate()
        {

            RecordCnt = 0;
            RecordCnt = MobjclsBLLVacationPolicy.RecCountNavigate();
            if (RecordCnt > 0)
            {
                bnCountItem.Text = strBindingOf + Convert.ToString(RecordCnt) + "";
                bnPositionItem.Text = Convert.ToString(RecordCnt);
            }
            else
            {
                CurrentRecCnt = 0;
                bnCountItem.Text = strBindingOf + "0";
                RecordCnt = 0;
            }

        }
        #endregion RecCountNavigate

        #region RecCount
        /// <summary>
        /// To Get The recCount
        /// </summary>
        private void RecCount()
        {
            RecordCnt = MobjclsBLLVacationPolicy.RecCount();

            if (RecordCnt > 0)
            {
                bnCountItem.Text = strBindingOf + Convert.ToString(RecordCnt) + "";
                bnPositionItem.Text = Convert.ToString(RecordCnt);
                CurrentRecCnt = RecordCnt;
            }
            else
            {
                CurrentRecCnt = 0;
                bnCountItem.Text = strBindingOf + "0";
            }

            bnMoveNextItem.Enabled = false;
            bnMoveLastItem.Enabled = false;
            if (RecordCnt > 1)
            {
                bnPreviousItem.Enabled = true;
                bnMoveFirstItem.Enabled = true;
            }
            else
            {
                bnPreviousItem.Enabled = false;
                bnMoveFirstItem.Enabled = false;
            }


        }
        #endregion RecCount

        #region LoadParameter
        /// <summary>
        /// To Load Parameter In Cellvalue change of grid dgvVacation
        /// 
        /// </summary>
        public void LoadParameter()
        {

            DataTable datCombos;
            int IntParameterID = 0;

            if (MobjclsBLLVacationPolicy == null)
                MobjclsBLLVacationPolicy = new clsBLLVacationPolicy();

            DetailsDataGridViewVacation.CommitEdit(DataGridViewDataErrorContexts.Commit);

                if (dgvVacation.Rows.Count > 1)
                {
                    IntParameterID = dgvVacation.Rows[0].Cells[0].Tag.ToInt32();

                    // Modified by Laxmi

                      int GridCount = dgvVacation.Rows.Count-2;
                      if (IntParameterID == 6 || IntParameterID == 7)
                      {
                         
                              for (int i = GridCount; i  >= 1; i--)
                              {
                                  dgvVacation.Rows.RemoveAt(i);
                              }   
                      }

                    if (IntParameterID < 6)
                    {
                        dgvVacation.Rows[1].ReadOnly = false;
                        //if (ClsCommonSettings.IsArabicView)
                        //    datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                        //else
                            datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                    }
                    else if (IntParameterID == 6)
                    {
                        dgvVacation.Rows[1].ReadOnly = true;

                        //if (ClsCommonSettings.IsArabicView)
                        //    datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                        //else
                            datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                    }
                    else
                    {
                        dgvVacation.Rows[1].ReadOnly = true;
                        //if (ClsCommonSettings.IsArabicView)
                        //    datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId ", "ParameterName" });
                        //else
                            datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId ", "ParameterName" });
                    }

                    cboParameter.ValueMember = "ParameterId";
                    cboParameter.DisplayMember = "ParameterName";
                    cboParameter.DataSource = datCombos;
                }
        }

        #endregion LoadParameter

        #region DisplayDetails
        /// <summary>
        /// To Display Det Details
        /// </summary>
        /// <param name="RowNum"></param>
        /// <param name="intVacationPolicyID"></param>
        private void DisplayDetails(int RowNum, int intVacationPolicyID)
        {
            DataSet DtSet;
            DataTable DtTabDetail;
            DataTable DtTabSalary;
            if (intVacationPolicyID > 0)
                DtSet = MobjclsBLLVacationPolicy.SelectVacationPolicyByVacationPolicyID(intVacationPolicyID);
            else
                DtSet = MobjclsBLLVacationPolicy.SelectVacationPolicy(RowNum);

            DtTabDetail = DtSet.Tables[1];
            DtTabSalary = DtSet.Tables[2];

            txtPolicyName.Text = MobjclsBLLVacationPolicy.clsDTOVacationPolicy.VacationPolicy;
            strPolicyNameCheck = MobjclsBLLVacationPolicy.clsDTOVacationPolicy.VacationPolicy;

            cboCalculationBasedVacation.SelectedValue = MobjclsBLLVacationPolicy.clsDTOVacationPolicy.CalculationID;
            txtOnTimeRejoinBenefit.Text = Convert.ToString(MobjclsBLLVacationPolicy.clsDTOVacationPolicy.dblOnTimeRejoinBenefit);

            chkLeaveOnly.Checked =  Convert.ToBoolean(MobjclsBLLVacationPolicy.clsDTOVacationPolicy.chkLeaveOnly);
            chkFullSalary.Checked = Convert.ToBoolean(MobjclsBLLVacationPolicy.clsDTOVacationPolicy.chkFullSalary);       
          
            if (DtTabSalary.Rows.Count > 0)
            {
                for (int i = 0; DetailsDataGridViewVacation.Rows.Count - 1 >= i; ++i)
                {
                    for (int j = 0; DtTabSalary.Rows.Count - 1 >= j; ++j)
                    {
                        if (Convert.ToInt32(DetailsDataGridViewVacation.Rows[i].Cells["txtAddDedID"].Value) == Convert.ToInt32(DtTabSalary.Rows[j]["AdditionDeductionID"]))
                        {
                            DetailsDataGridViewVacation.Rows[i].Cells["chkParticular"].Value = true;
                        }
                    }
                }
            }


            for (int i = 0; (DtTabDetail.Rows.Count - 1 >= i); ++i)
            {
                dgvVacation.Rows.Add();

                dgvVacation.Rows[i].Cells["cboParameter"].Value = DtTabDetail.Rows[i]["ParameterID"].ToInt32();
                dgvVacation.Rows[i].Cells["cboParameter"].Tag = DtTabDetail.Rows[i]["ParameterID"].ToInt32();
                dgvVacation.Rows[i].Cells["cboParameter"].Value = dgvVacation.Rows[i].Cells["cboParameter"].FormattedValue;

                dgvVacation.Rows[i].Cells["txtNoOfMonths"].Value = Convert.ToDecimal(DtTabDetail.Rows[i]["NoOfMonths"]);
                dgvVacation.Rows[i].Cells["txtNoOfDays"].Value = Convert.ToDecimal(DtTabDetail.Rows[i]["NoOfDays"]);
                dgvVacation.Rows[i].Cells["txtNoOfTickets"].Value = Convert.ToDecimal(DtTabDetail.Rows[i]["NoOfTickets"]);
                dgvVacation.Rows[i].Cells["txtTicketAmount"].Value = Convert.ToDecimal(DtTabDetail.Rows[i]["TicketAmount"]);
                //if (ClsCommonSettings.IsAmountRoundByZero)
                //{
                //    dgvVacation.Rows[i].Cells["txtTicketAmount"].Value = dgvVacation.Rows[i].Cells["txtTicketAmount"].Value.ToDecimal().ToString("F" + 0);
                //}
            }
            MbChangeStatus = false;
            DisableControlsAtUpdateMode();
        }
        #endregion DisplayDetails

        #region DisableControlsAtUpdateMode
        /// <summary>
        /// Function to Disable Controls in Update Mode
        /// </summary>
        private void DisableControlsAtUpdateMode()
        {
            this.btnAdd.Enabled = MblnAddPermission;
            this.BindingNavigatorSaveItem.Enabled = false;
            this.bnDeleteItem.Enabled = MblnDeletePermission;
            this.OKSaveButton.Enabled = false;
            BtnSave.Enabled = false;
            bnClear.Enabled = false;
            bnEmail.Enabled = MblnPrintEmailPermission;
            bnPrint.Enabled = MblnPrintEmailPermission;
        }
        #endregion DisableControlsAtUpdateMode

        #region ShowParticularVacationPolicy
        /// <summary>
        /// To Display ShowParticularVacationPolicy
        /// </summary>
        /// <param name="intPolicyID"></param>
        private void ShowParticularVacationPolicy(int intPolicyID)
        {
            RecCountNavigate();
            if (RecordCnt > 0)
            {

                CurrentRecCnt = MobjclsBLLVacationPolicy.getVacationPolicyRowNumber(intPolicyID);
                if (CurrentRecCnt <= 0 || CurrentRecCnt > RecordCnt)
                {
                    AddMode();
                    return;
                }
                if (CurrentRecCnt <= 1)
                {
                    bnPreviousItem.Enabled = false;
                    bnMoveFirstItem.Enabled = false;
                }
                if (RecordCnt > 1 && CurrentRecCnt < RecordCnt)
                {
                    bnMoveNextItem.Enabled = true;
                    bnMoveLastItem.Enabled = true;
                }
                else
                {
                    bnMoveNextItem.Enabled = false;
                    bnMoveLastItem.Enabled = false;
                }
            }
            else
            {
                CurrentRecCnt = 0;
                bnPreviousItem.Enabled = false;
                bnMoveFirstItem.Enabled = false;
                bnMoveNextItem.Enabled = false;
                bnMoveLastItem.Enabled = false;
            }
            UpdateMode();
            DisplayDetails(CurrentRecCnt, 0);
            bnPositionItem.Text = Convert.ToString(CurrentRecCnt);


            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 33, out MmessageIcon);
            lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();


            EnableTimer();
        }
        #endregion ShowParticularVacationPolicy

        #region LoadCombos
        /// <summary>
        /// Load Combos
        /// </summary>
        /// <param name="intType">
        /// intType = 1 --->PayCalculationReference
        /// intType = 2 --->PayParameters
        /// </param>
        private void LoadCombos(int intType)
        {


            DataTable datCombos = new DataTable();

            if (intType == 0 || intType == 1)
            {
                //if (ClsCommonSettings.IsArabicView)
                //    datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "CalculationID, CalculationArb AS Calculation", "PayCalculationReference", "CalculationID in(1,2,3)", "CalculationID", "Calculation" });
                //else
                    datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "CalculationID, Calculation", "PayCalculationReference", "CalculationID in(1,2,3)", "CalculationID", "Calculation" });
                cboCalculationBasedVacation.ValueMember = "CalculationID";
                cboCalculationBasedVacation.DisplayMember = "Calculation";
                cboCalculationBasedVacation.DataSource = datCombos;
            }
            if (intType == 0 || intType == 2)
            {
                datCombos = null;
                //if (ClsCommonSettings.IsArabicView)
                //    datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                //else
                    datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                cboParameter.ValueMember = "ParameterId";
                cboParameter.DisplayMember = "ParameterName";
                cboParameter.DataSource = datCombos;
            }
        }
        #endregion LoadCombos

        #region ChangeStatus
        /// <summary>
        /// To change Status In Upadte/add Mode
        /// </summary>
        private void ChangeStatus()
        {
            if (MblnAddStatus == true)
            {
                this.BindingNavigatorSaveItem.Enabled = MblnAddPermission;
                this.OKSaveButton.Enabled = btnAdd.Enabled = MblnAddPermission;
                BtnSave.Enabled = MblnAddPermission;
                bnClear.Enabled = true;
            }
            else
            {
                this.BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                this.OKSaveButton.Enabled = btnAdd.Enabled = MblnUpdatePermission;
                this.bnDeleteItem.Enabled = MblnDeletePermission;
                BtnSave.Enabled = MblnUpdatePermission;
                bnClear.Enabled = false;
            }
            MbChangeStatus = true;
        }
        #endregion ChangeStatus

        #region AddMode
        /// <summary>
        /// Add new Mode
        /// </summary>
        private void AddMode()
        {
            MblnAddStatus = true;
            strPolicyNameCheck = "";
            Clear();
            MobjclsBLLVacationPolicy = new clsBLLVacationPolicy();
            RecCount();
            this.btnAdd.Enabled = false;
            this.BindingNavigatorSaveItem.Enabled = false;
            this.bnDeleteItem.Enabled = false;
            this.OKSaveButton.Enabled = false;
            BtnSave.Enabled = false;
            bnEmail.Enabled = false;
            bnPrint.Enabled = false;
            bnClear.Enabled = true ;
            LoadCombos(2);
            MstrMessageCommon = "";//mObjNotification.GetErrorMessage(MsarMessageArr, 35, out MmessageIcon);
            lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();

            MbChangeStatus = false;
            EnableTimer();
            txtPolicyName.Focus();
            txtPolicyName.Select();
        }
        #endregion AddMode

        #region LoadAdditionsList
        /// <summary>
        /// To Load Addition List in Grid (DetailsDataGridViewVacation)
        /// </summary>
        private void LoadAdditionsList()
        {
            DetailsDataGridViewVacation.Rows.Clear();
            DataTable DtablePolicy = new DataTable();

            DtablePolicy = MobjclsBLLVacationPolicy.getParticular();
            if (DtablePolicy.Rows.Count > 0)
            {
                DetailsDataGridViewVacation.RowCount = 0;
                for (int i = 0; DtablePolicy.Rows.Count - 1 >= i; ++i)
                {
                    DetailsDataGridViewVacation.RowCount = DetailsDataGridViewVacation.RowCount + 1;
                    DetailsDataGridViewVacation.Rows[i].Cells["txtAddDedID"].Value = Convert.ToInt32(DtablePolicy.Rows[i]["AdditionDeductionID"]);
                    DetailsDataGridViewVacation.Rows[i].Cells["txtParticulars"].Value = Convert.ToString(DtablePolicy.Rows[i]["DescriptionStr"]);
                    DetailsDataGridViewVacation.Rows[i].Cells["chkParticular"].Value = Convert.ToBoolean(DtablePolicy.Rows[i]["Sel"]);
                }
            }
            else
            {
                DetailsDataGridViewVacation.RowCount = 0;
            }
        }
        #endregion LoadAdditionsList

        #endregion methods

        #region Events

        private void bnPreviousItem_Click(object sender, EventArgs e) // Button click To Navigate to PreviousItem
        {

            RecCountNavigate();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = CurrentRecCnt - 1;
                if (CurrentRecCnt <= 0)
                {
                    CurrentRecCnt = 1;
                }
                if (CurrentRecCnt <= 1)
                {
                    bnPreviousItem.Enabled = false;
                    bnMoveFirstItem.Enabled = false;
                }
                if (RecordCnt > 1 && CurrentRecCnt < RecordCnt)
                {
                    bnMoveNextItem.Enabled = true;
                    bnMoveLastItem.Enabled = true;
                }
                else
                {
                    bnMoveNextItem.Enabled = false;
                    bnMoveLastItem.Enabled = false;
                }
            }
            else
            {
                CurrentRecCnt = 0;
                bnPreviousItem.Enabled = false;
                bnMoveFirstItem.Enabled = false;
                bnMoveNextItem.Enabled = false;
                bnMoveLastItem.Enabled = false;
            }
            UpdateMode();
            DisplayDetails(CurrentRecCnt, 0);
            bnPositionItem.Text = Convert.ToString(CurrentRecCnt);

            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 10, out MmessageIcon);
            lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();


            EnableTimer();
        }

        private void bnMoveFirstItem_Click(object sender, EventArgs e) // Button click To Navigate to MoveFirstItem
        {
            RecCountNavigate();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = 1;
                bnPreviousItem.Enabled = false;
                bnMoveFirstItem.Enabled = false;
                if (RecordCnt > 1 && CurrentRecCnt < RecordCnt)
                {
                    bnMoveNextItem.Enabled = true;
                    bnMoveLastItem.Enabled = true;
                }
                else
                {
                    bnMoveNextItem.Enabled = false;
                    bnMoveLastItem.Enabled = false;
                }
            }
            else
            {
                CurrentRecCnt = 0;
                bnPreviousItem.Enabled = false;
                bnMoveFirstItem.Enabled = false;
                bnMoveNextItem.Enabled = false;
                bnMoveLastItem.Enabled = false;
            }
            UpdateMode();
            DisplayDetails(CurrentRecCnt, 0);
            bnPositionItem.Text = Convert.ToString(CurrentRecCnt);
            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9, out MmessageIcon);
            lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
            EnableTimer();
        }

        private void bnMoveNextItem_Click(object sender, EventArgs e) // Button click To Navigate to MoveNextItem
        {

            RecCountNavigate();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = CurrentRecCnt + 1;
                if (CurrentRecCnt >= RecordCnt)
                {
                    CurrentRecCnt = RecordCnt;
                }
                if (CurrentRecCnt >= RecordCnt)
                {
                    bnMoveNextItem.Enabled = false;
                    bnMoveLastItem.Enabled = false;
                }
                if (RecordCnt > 1 && CurrentRecCnt > 1)
                {
                    bnPreviousItem.Enabled = true;
                    bnMoveFirstItem.Enabled = true;
                }
                else
                {
                    bnPreviousItem.Enabled = false;
                    bnMoveFirstItem.Enabled = false;
                }
            }
            else
            {
                CurrentRecCnt = 0;
                bnPreviousItem.Enabled = false;
                bnMoveFirstItem.Enabled = false;
                bnMoveNextItem.Enabled = false;
                bnMoveLastItem.Enabled = false;
            }

            UpdateMode();
            DisplayDetails(CurrentRecCnt, 0);
            bnPositionItem.Text = Convert.ToString(CurrentRecCnt);

            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 11, out MmessageIcon);
            lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();

            EnableTimer();
        }

        private void bnMoveLastItem_Click(object sender, EventArgs e) // Button click To Navigate to MoveLastItem
        {
            RecCountNavigate();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = RecordCnt;
                bnMoveNextItem.Enabled = false;
                bnMoveLastItem.Enabled = false;
                if (RecordCnt > 1 && CurrentRecCnt > 1)
                {
                    bnPreviousItem.Enabled = true;
                    bnMoveFirstItem.Enabled = true;
                }
                else
                {
                    bnPreviousItem.Enabled = false;
                    bnMoveFirstItem.Enabled = false;
                }
            }
            else
            {
                CurrentRecCnt = 0;
                bnPreviousItem.Enabled = false;
                bnMoveFirstItem.Enabled = false;
                bnMoveNextItem.Enabled = false;
                bnMoveLastItem.Enabled = false;
            }

            UpdateMode();
            DisplayDetails(CurrentRecCnt, 0);
            bnPositionItem.Text = Convert.ToString(CurrentRecCnt);

            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 12, out MmessageIcon);
            lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();

            EnableTimer();
        }

        private void txtPolicyName_TextChanged(object sender, EventArgs e) //txtPolicyName_TextChanged
        {
            ChangeStatus();
        }

        private void cboCalculationBasedVacation_SelectedIndexChanged(object sender, EventArgs e) //cboCalculationBasedVacation_SelectedIndexChanged
        {
            if (cboCalculationBasedVacation.SelectedIndex == -1)
                return;
            if (Convert.ToInt32(cboCalculationBasedVacation.SelectedValue) == 2)
            {
                LoadAdditionsList();
                DetailsDataGridViewVacation.Enabled = true;
            }
            else
            {
                LoadAdditionsList();
                DetailsDataGridViewVacation.Enabled = false;
                DetailsDataGridViewVacation.Rows.Clear();
            }


            ChangeStatus();
        }

        private void btnAdd_Click(object sender, EventArgs e) //btnAdd_Click
        {
            AddMode();
        }

        private void frmVacationPolicy_Load(object sender, EventArgs e) //frmVacationPolicy_Load
        {
            LoadCombos(0);
            SetPermissions();
            LoadMessage();

            if (intVacationPolicyID > 0)
            {
                DisplayDetails(0, intVacationPolicyID);
                bnPositionItem.Text = "1";
                bnCountItem.Text = strBindingOf + "1";
                bnMoveFirstItem.Enabled = bnPreviousItem.Enabled = bnMoveNextItem.Enabled = bnMoveLastItem.Enabled = false;
            }
            else
                AddMode();
            txtPolicyName.Select();

            //MbChangeStatus = false;
            //BtnOk.Enabled = MbChangeStatus;
            //BtnSave.Enabled = MbChangeStatus;
            //BindingNavigatorDeleteItem.Enabled = MbChangeStatus;
            //tmrVacationPolicy.Interval = GlDelayTimeStatusBar;

            //if (GlintVacationPolicyId > 0 )
            //{
            //    ShowParticularVacationPolicy(GlintVacationPolicyId);
            //}
        }

        private void CompanyVacationPolicyBindingNavigatorSaveItem_Click(object sender, EventArgs e) //CompanyVacationPolicyBindingNavigatorSaveItem_Click
        {
            try
            {
                dgvVacation.CommitEdit(DataGridViewDataErrorContexts.Commit);

                if (formValidation() != true)
                {
                    return;
                }


                if (MblnAddStatus == true)
                {
                    //MstrMessageCommon = New ClsNotification().GetErrorMessage(MaMessageArr, 1);
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 1, out MmessageIcon);
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (SaveCompanyVacationPolicy() == true)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                            EnableTimer();
                            bnMoveLastItem_Click(sender, e);
                        }
                    }
                }
                else
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3, out MmessageIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("#", "").Trim();
                    if (MessageBox.Show(MstrMessageCommon, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (UpdateCompanyVacationPolicy() == true)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                            EnableTimer();
                            DisableControlsAtUpdateMode();
                        }
                    }
                }
            }
            catch (Exception)
            {

            }


        }

        private void txtRateVacation_TextChanged(object sender, EventArgs e) //txtRateVacation_TextChanged
        {
            ChangeStatus();
        }

        //private void txtFixedNoofTickets_TextChanged(object sender, EventArgs e) // txtFixedNoofTickets_TextChanged
        //{

        //    if (txtFixedNoofTickets.Text.Trim() != "")
        //    {
        //        if (Convert.ToDecimal(txtFixedNoofTickets.Text) != 0)
        //        {
        //            txtFixedTicketAmount.Text = "0";
        //        }
        //    }
        //    ChangeStatus();
        //}

        private void txtOnTimeRejoinBenefit_TextChanged(object sender, EventArgs e) //txtOnTimeRejoinBenefit_TextChanged
        {
            ChangeStatus();
        }

        //private void txtFixedTicketAmount_TextChanged(object sender, EventArgs e) // txtFixedTicketAmount_TextChanged
        //{
        //    if (txtFixedTicketAmount.Text.Trim() != "")
        //    {
        //        if (Convert.ToDecimal(txtFixedTicketAmount.Text) != 0)
        //        {
        //            txtFixedNoofTickets.Text = "0";
        //        }
        //    }

        //    ChangeStatus();
        //}

        private void DetailsDataGridViewVacation_CellValueChanged(object sender, DataGridViewCellEventArgs e) //DetailsDataGridViewVacation_CellValueChanged
        {
            ChangeStatus();
        }

        private void dgvVacation_CellValueChanged(object sender, DataGridViewCellEventArgs e) //dgvVacation_CellValueChanged
        {
            LoadParameter();
        }

        private void bnClear_Click(object sender, EventArgs e) //bnClear_Click
        {
            Clear();
            AddMode();
        }

        private void chkRateOnlyVacation_CheckedChanged(object sender, EventArgs e) //chkRateOnlyVacation_CheckedChanged
        {
            //if (chkRateOnlyVacation.Checked == true )
            //{
            //    txtRateVacation.Enabled = true;
            //    txtRateVacation.BackColor = Color.LightYellow;
            //    txtFixedNoofTickets.Enabled = true;
            //    txtFixedTicketAmount.Enabled = true;
            //    dgvVacation.Enabled =false;
            //    cboCalculationBasedVacation.SelectedIndex = -1;
            //    cboCalculationBasedVacation.Enabled = false;
            //    dgvVacation.Rows.Clear();
            //}
            //else
            //{
            //txtRateVacation.Enabled = false;
            //txtRateVacation.BackColor = Color.White;
            //txtFixedNoofTickets.Enabled = false;
            //txtFixedTicketAmount.Enabled = false;
            //txtRateVacation.Text = "";
            //txtFixedNoofTickets.Text = "";
            //txtFixedTicketAmount.Text = "";
            dgvVacation.Enabled = true;
            cboCalculationBasedVacation.SelectedIndex = 0;
            cboCalculationBasedVacation.Enabled = true;
            //}
            //ChangeStatus();
        }

        private void bnDeleteItem_Click(object sender, EventArgs e) //bnDeleteItem_Click
        {

            if (MobjclsBLLVacationPolicy.checkVacationPolicyUsed() == true) // checking whether vacation policy exists or not in Employee Master
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 7514, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                EnableTimer();
                return;
            }

            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 13, out MmessageIcon);
            MstrMessageCommon = MstrMessageCommon.Replace("#", "").Trim();

            if (MessageBox.Show(MstrMessageCommon, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (MobjclsBLLVacationPolicy.DeleteVacationPolicy() == true)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 4, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    EnableTimer();
                }
                AddMode();
            }

        }

        private void frmVacationPolicy_FormClosing(object sender, FormClosingEventArgs e)//frmVacationPolicy_FormClosing
        {
            if (MblnbtnOk == false && MbChangeStatus == true)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 4214, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
            }

        }

        private void OKSaveButton_Click(object sender, EventArgs e) //OKSaveButton_Click
        {
            CompanyVacationPolicyBindingNavigatorSaveItem_Click(sender, e);
            if (BtnSave.Enabled == false)
            {
                MblnbtnOk = false;
                this.Close();
            }
        }

        private void BtnSave_Click(object sender, EventArgs e) //BtnSave_Click
        {
            CompanyVacationPolicyBindingNavigatorSaveItem_Click(sender, e);
        }

        void txtDecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            e.Handled = false;


            //if (ClsCommonSettings.IsAmountRoundByZero == true)
            //{
            //    strInvalidChars = strInvalidChars + ".";
            //}

            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
        }

        void cb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            cb.DroppedDown = false;
        }

        private void dgvVacation_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e) //dgvVacation_EditingControlShowing
        {
            try
            {
                if (dgvVacation.CurrentCell != null)
                {
                    if (dgvVacation.CurrentCell.ColumnIndex == cboParameter.Index)
                    {
                        ComboBox cb = (ComboBox)e.Control;
                        cb.KeyPress += new KeyPressEventHandler(cb_KeyPress);
                        cb.DropDownHeight = 120;
                        cb.DropDownStyle = ComboBoxStyle.DropDown;
                        cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        cb.AutoCompleteSource = AutoCompleteSource.ListItems;
                    }
                    if (dgvVacation.CurrentCell.ColumnIndex == txtTicketAmount.Index || dgvVacation.CurrentCell.ColumnIndex == txtNoOfMonths.Index ||
                        dgvVacation.CurrentCell.ColumnIndex == txtNoOfDays.Index || dgvVacation.CurrentCell.ColumnIndex == txtNoOfTickets.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
            }
            catch (Exception Ex)
            {
                //ClsLogWriter.WriteLog(Ex,Log.LogSeverity.Error);
            }



        }

        private void BtnBottomCancel_Click(object sender, EventArgs e) // Button Cancel_Click 
        {
            //frmVacationPolicy_FormClosing(new object(), new FormClosingEventArgs(CloseReason.UserClosing,true ));
            this.Close();
        }

        private void dgvVacation_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e) //dgvVacation_CellBeginEdit
        {
            try
            {
                DataTable datCombos = null;
                if (dgvVacation.Rows.Count > 1)
                {
                    if (e.ColumnIndex == dgvVacation.Columns["txtNoOfTickets"].Index)
                    {
                        dgvVacation.Rows[e.RowIndex].Cells["txtTicketAmount"].Value = "0";
                    }
                    if (e.ColumnIndex == dgvVacation.Columns["txtTicketAmount"].Index)
                    {
                        dgvVacation.Rows[e.RowIndex].Cells["txtNoOfTickets"].Value = "0";
                    }
                }

                int iRowIndex = 0;
                //Modified by laxmi
                if (e.RowIndex >= 0)
                {
                    iRowIndex = dgvVacation.CurrentRow.Index;
                    if (dgvVacation.RowCount >= 1)
                    {
                        if (e.ColumnIndex == 0)
                        {
                            if (e.RowIndex == 0)
                            {
                                datCombos = null;
                                //if (ClsCommonSettings.IsArabicView)
                                //    datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                                //else
                                    datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                                cboParameter.ValueMember = "ParameterId";
                                cboParameter.DisplayMember = "ParameterName";
                                cboParameter.DataSource = datCombos;

                            }
                            else if (dgvVacation.Rows[0].Cells["cboParameter"].Value == "6")
                            {
                                datCombos = null;
                                //if (ClsCommonSettings.IsArabicView)
                                //    datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "ParameterId=6", "ParameterId", "ParameterName" });
                                //else
                                    datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "ParameterId=6", "ParameterId", "ParameterName" });
                                cboParameter.ValueMember = "ParameterId";
                                cboParameter.DisplayMember = "ParameterName";
                                cboParameter.DataSource = datCombos;
                            }
                            else if (dgvVacation.Rows[0].Cells["cboParameter"].Value == "7")
                            {
                                datCombos = null;
                                //if (ClsCommonSettings.IsArabicView)
                                //    datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "ParameterId=7", "ParameterId", "ParameterName" });
                                //else
                                    datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "ParameterId=7", "ParameterId", "ParameterName" });
                                cboParameter.ValueMember = "ParameterId";
                                cboParameter.DisplayMember = "ParameterName";
                                cboParameter.DataSource = datCombos;
                            }
                            else
                            {
                                datCombos = null;
                                //if (ClsCommonSettings.IsArabicView)
                                //    datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "ParameterId<6", "ParameterId", "ParameterName" });
                                //else
                                    datCombos = MobjclsBLLVacationPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "ParameterId<6", "ParameterId", "ParameterName" });
                                cboParameter.ValueMember = "ParameterId";
                                cboParameter.DisplayMember = "ParameterName";
                                cboParameter.DataSource = datCombos;
                            }

                            if (e.RowIndex > 0)
                            {
                                if (Convert.ToInt32(dgvVacation.Rows[0].Cells["cboParameter"].Value).ToString() == "6")
                                {
                                    //MsMessageCommon =mObjNotification.GetErrorMessage(MsarMessageArr,2211, out MmessageIcon);
                                    //MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    //lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                                    dgvVacation.CurrentCell = dgvVacation[0, iRowIndex];
                                    e.Cancel = true;
                                    return;
                                }
                            }
                        }
                        //if (e.ColumnIndex == 1)
                        //{
                        //    if (grdGratuity.CurrentRow.Cells[0].Value == null)
                        //    {
                        //        //MsMessageCommon =mObjNotification.GetErrorMessage(MsarMessageArr,2202, out MmessageIcon);
                        //        //MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //        //lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        //        grdGratuity.CurrentCell = grdGratuity[0, iRowIndex];
                        //        e.Cancel = true;
                        //    }
                        //}

                        //if (e.ColumnIndex == 2)
                        //{
                        //    if (grdGratuity.CurrentRow.Cells[1].Value == null)
                        //    {
                        //        //MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3044, out MmessageIcon);
                        //        //MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //        //lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        //        grdGratuity.CurrentCell = grdGratuity[1, iRowIndex];
                        //        e.Cancel = true;
                        //    }
                        //}
                    }
                }

                ChangeStatus();   
            }
            catch (Exception)
            {
            }
        }


        private void DetailsDataGridViewVacation_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (DetailsDataGridViewVacation.IsCurrentCellDirty)
                {

                    DetailsDataGridViewVacation.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        } //DetailsDataGridViewVacation_CurrentCellDirtyStateChanged

        private void DetailsDataGridViewVacation_DataError(object sender, DataGridViewDataErrorEventArgs e) //DetailsDataGridViewVacation_DataError
        {

            try
            {
            }
            catch
            {
            }
        }

        #endregion Events

        private void bnHelp_Click(object sender, EventArgs e)
        {
            try
            {
                FrmHelp objHelp = new FrmHelp();
                objHelp.strFormName = "VacationPolicy";
                objHelp.ShowDialog();
                objHelp = null;
            }
            catch (Exception Ex)
            {


            }
        }

        private void frmVacationPolicy_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        bnHelp_Click(null, null);
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        if (btnAdd.Enabled)
                            btnAdd_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (bnDeleteItem.Enabled)
                            bnDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if (bnClear.Enabled)
                            bnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        if (bnPreviousItem.Enabled)
                            bnPreviousItem_Click(sender, new EventArgs());//Prev item
                        break;
                    case Keys.Control | Keys.Right:
                        if (bnMoveNextItem.Enabled)
                            bnMoveNextItem_Click(sender, new EventArgs());//Next item
                        break;
                    case Keys.Control | Keys.Up:
                        if (bnMoveFirstItem.Enabled)
                            bnMoveFirstItem_Click(sender, new EventArgs());//First item
                        break;
                    case Keys.Control | Keys.Down:
                        if (bnMoveLastItem.Enabled)
                            bnMoveLastItem_Click(sender, new EventArgs());//Last item
                        break;

                    //case Keys.Control | Keys.M:
                    //    BtnEmail_Click(sender, new EventArgs());//Cancel
                    //    break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void dgvVacation_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch (Exception)
            {

            }
        }

        private void dgvVacation_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    dgvVacation.EndEdit();
                    //------------------------------------------------------------UOM----------------------------------------------------------

                    if (e.ColumnIndex == cboParameter.Index)
                    {

                        dgvVacation.CurrentRow.Cells["cboParameter"].Tag = dgvVacation.CurrentRow.Cells["cboParameter"].Value;
                        dgvVacation.CurrentRow.Cells["cboParameter"].Value = dgvVacation.CurrentRow.Cells["cboParameter"].FormattedValue;

                    }
                }
            }
            catch (Exception ex)
            {

            }

        }

        private void dgvVacation_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (DetailsDataGridViewVacation.IsCurrentCellDirty)
                {

                    DetailsDataGridViewVacation.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }

        private void dgvVacation_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e) //dgvVacation_UserDeletingRow
        {
            ChangeStatus();
        }
        private void dgvVacation_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            if (dgvVacation.Rows.Count ==1)
            {
                dgvVacation.Rows.Clear();
              //  LoadParameter();
            }
        }

        private void chkWithSalary_CheckedChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void chkLeaveOnly_CheckedChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void chkFullSalary_CheckedChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

    }
}
