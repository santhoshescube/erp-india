﻿namespace MyBooksERP
{
    partial class frmEncashPolicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label2;
            System.Windows.Forms.Label Label4;
            System.Windows.Forms.Label Label1;
            System.Windows.Forms.Label DescriptionLabel;
            System.Windows.Forms.Label Label5;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEncashPolicy));
            this.HolidayPolicyBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bnMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bnMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnCancel = new System.Windows.Forms.ToolStripButton();
            this.bnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.cboCalculationBased = new System.Windows.Forms.ComboBox();
            this.rdbActualMonth = new System.Windows.Forms.RadioButton();
            this.rdbCompanyBased = new System.Windows.Forms.RadioButton();
            this.lblHoursPerDay = new System.Windows.Forms.Label();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.chkRateOnly = new System.Windows.Forms.CheckBox();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.SelectValue = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SelectValueS = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblHeader = new System.Windows.Forms.Label();
            this.errEncashPolicy = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.txtCalculationPercent = new System.Windows.Forms.TextBox();
            this.dgvParticular = new DemoClsDataGridview.ClsDataGirdView();
            this.colEncashAddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEncashSelectedItem = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colEncashParticulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Particulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddDedIDS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParticularsS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            Label2 = new System.Windows.Forms.Label();
            Label4 = new System.Windows.Forms.Label();
            Label1 = new System.Windows.Forms.Label();
            DescriptionLabel = new System.Windows.Forms.Label();
            Label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayPolicyBindingNavigator)).BeginInit();
            this.HolidayPolicyBindingNavigator.SuspendLayout();
            this.ssStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errEncashPolicy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParticular)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label2
            // 
            Label2.AutoSize = true;
            Label2.Location = new System.Drawing.Point(16, 165);
            Label2.Name = "Label2";
            Label2.Size = new System.Drawing.Size(101, 13);
            Label2.TabIndex = 38;
            Label2.Text = "Exclude from Gross ";
            // 
            // Label4
            // 
            Label4.AutoSize = true;
            Label4.Location = new System.Drawing.Point(7, 59);
            Label4.Name = "Label4";
            Label4.Size = new System.Drawing.Size(81, 13);
            Label4.TabIndex = 17;
            Label4.Text = "Calculation Day";
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Location = new System.Drawing.Point(16, 132);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(109, 13);
            Label1.TabIndex = 36;
            Label1.Text = "Calculation Based On";
            // 
            // DescriptionLabel
            // 
            DescriptionLabel.AutoSize = true;
            DescriptionLabel.Location = new System.Drawing.Point(16, 33);
            DescriptionLabel.Name = "DescriptionLabel";
            DescriptionLabel.Size = new System.Drawing.Size(66, 13);
            DescriptionLabel.TabIndex = 306;
            DescriptionLabel.Text = "Policy Name";
            // 
            // Label5
            // 
            Label5.AutoSize = true;
            Label5.Location = new System.Drawing.Point(16, 292);
            Label5.Name = "Label5";
            Label5.Size = new System.Drawing.Size(117, 13);
            Label5.TabIndex = 315;
            Label5.Text = "Calculation Percentage";
            // 
            // HolidayPolicyBindingNavigator
            // 
            this.HolidayPolicyBindingNavigator.AddNewItem = null;
            this.HolidayPolicyBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.HolidayPolicyBindingNavigator.DeleteItem = null;
            this.HolidayPolicyBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnMoveFirstItem,
            this.bnMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.bnAddNewItem,
            this.bnDeleteItem,
            this.bnSaveItem,
            this.bnCancel,
            this.bnPrint,
            this.btnEmail,
            this.ToolStripSeparator2,
            this.btnHelp});
            this.HolidayPolicyBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.HolidayPolicyBindingNavigator.MoveFirstItem = null;
            this.HolidayPolicyBindingNavigator.MoveLastItem = null;
            this.HolidayPolicyBindingNavigator.MoveNextItem = null;
            this.HolidayPolicyBindingNavigator.MovePreviousItem = null;
            this.HolidayPolicyBindingNavigator.Name = "HolidayPolicyBindingNavigator";
            this.HolidayPolicyBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.HolidayPolicyBindingNavigator.Size = new System.Drawing.Size(411, 25);
            this.HolidayPolicyBindingNavigator.TabIndex = 300;
            this.HolidayPolicyBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bnMoveFirstItem
            // 
            this.bnMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
            this.bnMoveFirstItem.Name = "bnMoveFirstItem";
            this.bnMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveFirstItem.Text = "Move first";
            this.bnMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // bnMovePreviousItem
            // 
            this.bnMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem.Image")));
            this.bnMovePreviousItem.Name = "bnMovePreviousItem";
            this.bnMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bnMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bnMovePreviousItem.Text = "Move previous";
            this.bnMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoveNextItem
            // 
            this.bnMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
            this.bnMoveNextItem.Name = "bnMoveNextItem";
            this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveNextItem.Text = "Move next";
            this.bnMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // bnMoveLastItem
            // 
            this.bnMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
            this.bnMoveLastItem.Name = "bnMoveLastItem";
            this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveLastItem.Text = "Move last";
            this.bnMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bnAddNewItem.Text = "Add new";
            this.bnAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnDeleteItem.Image = global::MyBooksERP.Properties.Resources.Delete;
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteItem.Text = "Delete";
            this.bnDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bnSaveItem.Text = "Save Data";
            this.bnSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // bnCancel
            // 
            this.bnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnCancel.Image = global::MyBooksERP.Properties.Resources.Clear;
            this.bnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnCancel.Name = "bnCancel";
            this.bnCancel.Size = new System.Drawing.Size(23, 22);
            this.bnCancel.Text = "Clear";
            this.bnCancel.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // bnPrint
            // 
            this.bnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnPrint.Image = global::MyBooksERP.Properties.Resources.Print;
            this.bnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(23, 22);
            this.bnPrint.Text = "Print";
            this.bnPrint.Visible = false;
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyBooksERP.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Visible = false;
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 22);
            this.btnHelp.Text = "He&lp";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // cboCalculationBased
            // 
            this.cboCalculationBased.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCalculationBased.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCalculationBased.BackColor = System.Drawing.SystemColors.Info;
            this.cboCalculationBased.DropDownHeight = 134;
            this.cboCalculationBased.FormattingEnabled = true;
            this.cboCalculationBased.IntegralHeight = false;
            this.cboCalculationBased.Location = new System.Drawing.Point(143, 129);
            this.cboCalculationBased.MaxDropDownItems = 10;
            this.cboCalculationBased.Name = "cboCalculationBased";
            this.cboCalculationBased.Size = new System.Drawing.Size(249, 21);
            this.cboCalculationBased.TabIndex = 2;
            this.cboCalculationBased.SelectedIndexChanged += new System.EventHandler(this.cboCalculationBased_SelectedIndexChanged);
            this.cboCalculationBased.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCalculationBased_KeyDown);
            // 
            // rdbActualMonth
            // 
            this.rdbActualMonth.AutoSize = true;
            this.rdbActualMonth.Location = new System.Drawing.Point(229, 91);
            this.rdbActualMonth.Name = "rdbActualMonth";
            this.rdbActualMonth.Size = new System.Drawing.Size(88, 17);
            this.rdbActualMonth.TabIndex = 1;
            this.rdbActualMonth.Text = "Actual Month";
            this.rdbActualMonth.UseVisualStyleBackColor = true;
            this.rdbActualMonth.CheckedChanged += new System.EventHandler(this.ResetForm);
            // 
            // rdbCompanyBased
            // 
            this.rdbCompanyBased.AutoSize = true;
            this.rdbCompanyBased.Checked = true;
            this.rdbCompanyBased.Location = new System.Drawing.Point(65, 91);
            this.rdbCompanyBased.Name = "rdbCompanyBased";
            this.rdbCompanyBased.Size = new System.Drawing.Size(117, 17);
            this.rdbCompanyBased.TabIndex = 0;
            this.rdbCompanyBased.TabStop = true;
            this.rdbCompanyBased.Text = "Based on Company";
            this.rdbCompanyBased.UseVisualStyleBackColor = true;
            this.rdbCompanyBased.CheckedChanged += new System.EventHandler(this.rdbCompanyBased_CheckedChanged);
            // 
            // lblHoursPerDay
            // 
            this.lblHoursPerDay.AutoSize = true;
            this.lblHoursPerDay.Location = new System.Drawing.Point(16, 328);
            this.lblHoursPerDay.Name = "lblHoursPerDay";
            this.lblHoursPerDay.Size = new System.Drawing.Size(71, 13);
            this.lblHoursPerDay.TabIndex = 15;
            this.lblHoursPerDay.Text = "Rate Per Day";
            // 
            // txtRate
            // 
            this.txtRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtRate.Location = new System.Drawing.Point(164, 325);
            this.txtRate.MaxLength = 5;
            this.txtRate.Name = "txtRate";
            this.txtRate.ShortcutsEnabled = false;
            this.txtRate.Size = new System.Drawing.Size(65, 20);
            this.txtRate.TabIndex = 5;
            this.txtRate.TextChanged += new System.EventHandler(this.ResetForm);
            this.txtRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtint_KeyPress);
            // 
            // chkRateOnly
            // 
            this.chkRateOnly.AutoSize = true;
            this.chkRateOnly.Location = new System.Drawing.Point(143, 328);
            this.chkRateOnly.Name = "chkRateOnly";
            this.chkRateOnly.Size = new System.Drawing.Size(15, 14);
            this.chkRateOnly.TabIndex = 4;
            this.chkRateOnly.UseVisualStyleBackColor = true;
            this.chkRateOnly.CheckedChanged += new System.EventHandler(this.chkRatePerDay_CheckedChanged);
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 3;
            this.lineShape3.X2 = 386;
            this.lineShape3.Y1 = 64;
            this.lineShape3.Y2 = 64;
            // 
            // SelectValue
            // 
            this.SelectValue.HeaderText = "";
            this.SelectValue.Name = "SelectValue";
            this.SelectValue.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SelectValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SelectValue.Width = 30;
            // 
            // SelectValueS
            // 
            this.SelectValueS.HeaderText = "";
            this.SelectValueS.Name = "SelectValueS";
            this.SelectValueS.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SelectValueS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SelectValueS.Width = 30;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(332, 395);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(244, 395);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(5, 395);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Info;
            this.txtDescription.Location = new System.Drawing.Point(143, 30);
            this.txtDescription.MaxLength = 50;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(249, 20);
            this.txtDescription.TabIndex = 0;
            this.txtDescription.TextChanged += new System.EventHandler(this.txtPolicyName_TextChanged);
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblstatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 425);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(411, 22);
            this.ssStatus.TabIndex = 310;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblHeader.Location = new System.Drawing.Point(2, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(66, 13);
            this.lblHeader.TabIndex = 312;
            this.lblHeader.Text = "Policy Info";
            // 
            // errEncashPolicy
            // 
            this.errEncashPolicy.ContainerControl = this;
            this.errEncashPolicy.RightToLeft = true;
            // 
            // tmrClear
            // 
            this.tmrClear.Tick += new System.EventHandler(this.tmrClear_Tick);
            // 
            // txtCalculationPercent
            // 
            this.txtCalculationPercent.BackColor = System.Drawing.SystemColors.Info;
            this.txtCalculationPercent.Location = new System.Drawing.Point(143, 289);
            this.txtCalculationPercent.MaxLength = 5;
            this.txtCalculationPercent.Name = "txtCalculationPercent";
            this.txtCalculationPercent.Size = new System.Drawing.Size(208, 20);
            this.txtCalculationPercent.TabIndex = 314;
            this.txtCalculationPercent.TextChanged += new System.EventHandler(this.txtCalculationPercent_TextChanged);
            this.txtCalculationPercent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // dgvParticular
            // 
            this.dgvParticular.AddNewRow = false;
            this.dgvParticular.AllowUserToAddRows = false;
            this.dgvParticular.AllowUserToDeleteRows = false;
            this.dgvParticular.AllowUserToResizeColumns = false;
            this.dgvParticular.AllowUserToResizeRows = false;
            this.dgvParticular.AlphaNumericCols = new int[0];
            this.dgvParticular.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvParticular.CapsLockCols = new int[0];
            this.dgvParticular.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvParticular.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colEncashAddDedID,
            this.colEncashSelectedItem,
            this.colEncashParticulars});
            this.dgvParticular.DecimalCols = new int[0];
            this.dgvParticular.HasSlNo = false;
            this.dgvParticular.LastRowIndex = 0;
            this.dgvParticular.Location = new System.Drawing.Point(143, 156);
            this.dgvParticular.MultiSelect = false;
            this.dgvParticular.Name = "dgvParticular";
            this.dgvParticular.NegativeValueCols = new int[0];
            this.dgvParticular.NumericCols = new int[0];
            this.dgvParticular.RowHeadersWidth = 35;
            this.dgvParticular.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvParticular.Size = new System.Drawing.Size(249, 122);
            this.dgvParticular.TabIndex = 3;
            this.dgvParticular.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvParticular_CellBeginEdit);
            // 
            // colEncashAddDedID
            // 
            this.colEncashAddDedID.HeaderText = "AddDedID";
            this.colEncashAddDedID.Name = "colEncashAddDedID";
            this.colEncashAddDedID.Visible = false;
            // 
            // colEncashSelectedItem
            // 
            this.colEncashSelectedItem.HeaderText = "";
            this.colEncashSelectedItem.Name = "colEncashSelectedItem";
            this.colEncashSelectedItem.Width = 30;
            // 
            // colEncashParticulars
            // 
            this.colEncashParticulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colEncashParticulars.HeaderText = "Particulars";
            this.colEncashParticulars.Name = "colEncashParticulars";
            this.colEncashParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AddDedID
            // 
            this.AddDedID.HeaderText = "AddDedID";
            this.AddDedID.Name = "AddDedID";
            this.AddDedID.ReadOnly = true;
            this.AddDedID.Visible = false;
            // 
            // Particulars
            // 
            this.Particulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Particulars.HeaderText = "Particulars";
            this.Particulars.Name = "Particulars";
            this.Particulars.ReadOnly = true;
            // 
            // AddDedIDS
            // 
            this.AddDedIDS.HeaderText = "AddDedID";
            this.AddDedIDS.Name = "AddDedIDS";
            this.AddDedIDS.ReadOnly = true;
            this.AddDedIDS.Visible = false;
            // 
            // ParticularsS
            // 
            this.ParticularsS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ParticularsS.HeaderText = "Particulars";
            this.ParticularsS.Name = "ParticularsS";
            this.ParticularsS.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDescription);
            this.groupBox1.Controls.Add(this.lblHeader);
            this.groupBox1.Controls.Add(DescriptionLabel);
            this.groupBox1.Controls.Add(this.dgvParticular);
            this.groupBox1.Controls.Add(Label4);
            this.groupBox1.Controls.Add(this.txtCalculationPercent);
            this.groupBox1.Controls.Add(Label1);
            this.groupBox1.Controls.Add(Label5);
            this.groupBox1.Controls.Add(this.rdbActualMonth);
            this.groupBox1.Controls.Add(this.rdbCompanyBased);
            this.groupBox1.Controls.Add(this.chkRateOnly);
            this.groupBox1.Controls.Add(this.cboCalculationBased);
            this.groupBox1.Controls.Add(Label2);
            this.groupBox1.Controls.Add(this.lblHoursPerDay);
            this.groupBox1.Controls.Add(this.txtRate);
            this.groupBox1.Controls.Add(this.shapeContainer2);
            this.groupBox1.Location = new System.Drawing.Point(5, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(402, 361);
            this.groupBox1.TabIndex = 316;
            this.groupBox1.TabStop = false;
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape3});
            this.shapeContainer2.Size = new System.Drawing.Size(396, 342);
            this.shapeContainer2.TabIndex = 316;
            this.shapeContainer2.TabStop = false;
            // 
            // frmEncashPolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 447);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.HolidayPolicyBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEncashPolicy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Encash Policy";
            this.Load += new System.EventHandler(this.FrmEncashPolicy_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmEncashPolicy_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.HolidayPolicyBindingNavigator)).EndInit();
            this.HolidayPolicyBindingNavigator.ResumeLayout(false);
            this.HolidayPolicyBindingNavigator.PerformLayout();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errEncashPolicy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParticular)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator HolidayPolicyBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton bnMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton bnMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton bnMoveNextItem;
        internal System.Windows.Forms.ToolStripButton bnMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton bnAddNewItem;
        internal System.Windows.Forms.ToolStripButton bnDeleteItem;
        internal System.Windows.Forms.ToolStripButton bnSaveItem;
        internal System.Windows.Forms.ToolStripButton bnCancel;
        internal System.Windows.Forms.ToolStripButton bnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        internal System.Windows.Forms.ComboBox cboCalculationBased;
        internal System.Windows.Forms.RadioButton rdbActualMonth;
        internal System.Windows.Forms.RadioButton rdbCompanyBased;
        internal System.Windows.Forms.Label lblHoursPerDay;
        internal System.Windows.Forms.TextBox txtRate;
        internal System.Windows.Forms.CheckBox chkRateOnly;
        internal System.Windows.Forms.DataGridViewTextBoxColumn AddDedID;
        internal System.Windows.Forms.DataGridViewCheckBoxColumn SelectValue;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Particulars;
        internal System.Windows.Forms.DataGridViewTextBoxColumn AddDedIDS;
        internal System.Windows.Forms.DataGridViewCheckBoxColumn SelectValueS;
        internal System.Windows.Forms.DataGridViewTextBoxColumn ParticularsS;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.TextBox txtDescription;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.Label lblHeader;
        private DemoClsDataGridview.ClsDataGirdView dgvParticular;
        private System.Windows.Forms.ErrorProvider errEncashPolicy;
        private System.Windows.Forms.Timer tmrClear;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        internal System.Windows.Forms.TextBox txtCalculationPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEncashAddDedID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colEncashSelectedItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEncashParticulars;
        private System.Windows.Forms.GroupBox groupBox1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
    }
}