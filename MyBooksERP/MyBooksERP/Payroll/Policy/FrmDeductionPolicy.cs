﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public partial class FrmDeductionPolicy : Form
    {
        #region Declartions
       // permissions
        private bool MblnAddPermission = false;        //To Set Add Permission
        private bool MblnUpdatePermission = false;     //To Set Update Permission
        private bool MblnDeletePermission = false;     //To Set Delete Permission
        private bool MblnPrintEmailPermission = false; //To Set PrintEmail Permission
        private bool MblnAddStatus;                    //Add/Update mode 
        private bool MblnChangeStatus;
        private int MintCurrentRecCnt;                // Current recourd count
        private int MintRecordCnt;                    // Total record count
        private bool blnIsAddition = false;  
        private string MstrMessageCommon;
        private string MsMessageCaption;
        private ArrayList MsarMessageArr;           // Error Message display

        clsBLLDeductionPolicy MobjclsBLLDeductionPolicy;
        ClsLogWriter MObjClsLogWriter;      // Object of the LogWriter class
        ClsNotification MObjClsNotification; // Object of the Notification class
        MessageBoxIcon MsMessageBoxIcon;
        string strBindingOf = "Of ";
        #endregion Declarations

        #region Constructor
        public FrmDeductionPolicy()
        {
            InitializeComponent();
            MobjclsBLLDeductionPolicy = new clsBLLDeductionPolicy();
            MsMessageCaption = ClsCommonSettings.MessageCaption; //Message caption
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);   // Application startup path
            MObjClsNotification = new ClsNotification();

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        //private void SetArabicControls()
        //{
        //    ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        //    objDAL.SetArabicVersion((int)FormID.DeductionPolicy, this);

        //    strBindingOf = "من ";
        //    dgvDeductionDetails.Columns["DParticulars"].HeaderText = "تفاصيل";
        //}
        #endregion Constructor

        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID >3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.LoginCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.DeductionPolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }
        private void EnableDisableButtons(bool bEnable)
        {
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            if (bEnable)
            {
                ProjectsBindingNavigatorSaveItem.Enabled = BtnOk.Enabled = BtnSave.Enabled = BtnCancel.Enabled = MblnAddPermission;
            }
            else
            {
                ProjectsBindingNavigatorSaveItem.Enabled = BtnOk.Enabled = BtnSave.Enabled = false;
            }
            
        }

        private void LoadCombos()
        {
            DataTable datCombos = new DataTable();
            //CboAdditions
            //if (ClsCommonSettings.IsArabicView)
            //    datCombos = MobjclsBLLDeductionPolicy.FillCombos(new string[] { "AdditionDeductionID,AdditionDeductionArb AS AdditionDeduction", "PayAdditionDeductionReference", " ISNULL(IsAddition,0)=1 AND AdditionDeductionID IN(1,18) ", "AdditionDeductionID", "AdditionDeduction" });
            //else
                datCombos = MobjclsBLLDeductionPolicy.FillCombos(new string[] { "AdditionDeductionID,AdditionDeduction", "PayAdditionDeductionReference", " ISNULL(IsAddition,0)=1 AND AdditionDeductionID IN(1,18) ", "AdditionDeductionID", "AdditionDeduction" });
            CboAdditions.ValueMember = "AdditionDeductionID";
            CboAdditions.DisplayMember = "AdditionDeduction";
            CboAdditions.DataSource = datCombos;

            //cboParticulars
            datCombos = null;
            //if (ClsCommonSettings.IsArabicView)
            //    datCombos = MobjclsBLLDeductionPolicy.FillCombos(new string[] { "AdditionDeductionID,AdditionDeductionArb AS AdditionDeduction", "PayAdditionDeductionReference", "ISNULL(IsAddition,0)=0 and (AdditionDeductionID >2) And (IsPredefined =0 and AdditionDeductionID<>7) ", "AdditionDeductionID", "AdditionDeduction" });
            //else
                datCombos = MobjclsBLLDeductionPolicy.FillCombos(new string[] { "AdditionDeductionID,AdditionDeduction", "PayAdditionDeductionReference", "ISNULL(IsAddition,0)=0 and (AdditionDeductionID >2) And (IsPredefined =0 and AdditionDeductionID<>7) ", "AdditionDeductionID", "AdditionDeduction" });
            cboParticulars.ValueMember = "AdditionDeductionID";
            cboParticulars.DisplayMember = "AdditionDeduction";
            cboParticulars.DataSource = datCombos;

            //CboParameter
            datCombos = null;
            //if (ClsCommonSettings.IsArabicView)
            //    datCombos = MobjclsBLLDeductionPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "[ParameterId] <= 5 ORDER BY [ParameterId]", "ParameterId", "ParameterName" });
            //else
                datCombos = MobjclsBLLDeductionPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "[ParameterId] <= 5 ORDER BY [ParameterId]", "ParameterId", "ParameterName" });
            CboParameter.ValueMember = "ParameterId";
            CboParameter.DisplayMember = "ParameterName";
            CboParameter.DataSource = datCombos;
        }
       
        private void LoadInitials()
        {
            if (blnIsAddition)
            {
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    LblParticulars.Text = "إضافة";
                //    lblAmount.Text = "بالإضافة إلى ذلك المبلغ ( ٪ )";
                //}
                //else
                //{
                    LblParticulars.Text = "Addition";
                    lblAmount.Text = "Addition Amount(%)";
                //}
            }
            else
            {
                //if (ClsCommonSettings.IsArabicView)
                //    LblParticulars.Text = "حسم";
                //else
                    LblParticulars.Text = "Deduction";
            }
        }

        private void LoadMessage()
        {
            MsarMessageArr = new ArrayList();
            MsarMessageArr = MObjClsNotification.FillMessageArray((int)FormID.DeductionPolicy, ClsCommonSettings.ProductID);
        }

        private void FrmDeductionPolicy_Load(object sender, EventArgs e)
        {
            LoadInitials();
            LoadCombos();
            LoadMessage();
            LoadAdditionsList();
            SetPermissions();
            AddNewDeductionPolicy();
            MblnChangeStatus = false;
            GetRecordCount();
            MintRecordCnt = MintRecordCnt + 1;
            MintCurrentRecCnt = MintRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorPositionItem.Text = Convert.ToString(MintRecordCnt);
            DeductionPolicyErrorProvider.Clear();
            Txtpolicyname.Select();
        }

           private void LoadAdditionsList()
           {
               dgvDeductionDetails.Rows.Clear();
               dgvDeductionDetails.RowCount = 1;
               DataTable DtablePolicy = new DataTable();
               DtablePolicy = MobjclsBLLDeductionPolicy.FillDeductiondetailAddMode();
               DtablePolicy.DefaultView.RowFilter = "";

               if (DtablePolicy.DefaultView.ToTable().Rows.Count > 0)
               {
                   dgvDeductionDetails.RowCount = 0;
                   for (int i = 0; i < DtablePolicy.Rows.Count; i++)
                   {
                       dgvDeductionDetails.RowCount = dgvDeductionDetails.RowCount + 1;

                       dgvDeductionDetails.Rows[i].Cells["DAddDedID"].Value = Convert.ToInt32(DtablePolicy.Rows[i]["AdditionDeductionID"]);
                       dgvDeductionDetails.Rows[i].Cells["DParticulars"].Value = Convert.ToString(DtablePolicy.Rows[i]["DescriptionStr"]);
                       dgvDeductionDetails.Rows[i].Cells["DSelectValue"].Value = Convert.ToBoolean(DtablePolicy.Rows[i]["Sel"]);
                   }
               }
           }

           private void dgvDeductionDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
           {
               try { }
               catch { }
           }
        private bool FormValidation()
        {

           bool bFlag  = false;
            DeductionPolicyErrorProvider.Clear();

            if(String.IsNullOrEmpty(Txtpolicyname.Text.Trim()) == true)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 10072, out MsMessageBoxIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption , MessageBoxButtons.OK, MsMessageBoxIcon);
                DeductionPolicyErrorProvider.SetError(Txtpolicyname, MstrMessageCommon.Replace("#", "").Trim());
                ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                DeductionPolicyTimer.Enabled = true;
                Txtpolicyname.Focus();
               return false;
                    
            }

            if(cboParticulars.SelectedIndex == -1)
            {
               
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 10080, out MsMessageBoxIcon);
          
                if(blnIsAddition)
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 10081, out MsMessageBoxIcon);
                }
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption , MessageBoxButtons.OK, MsMessageBoxIcon);
                DeductionPolicyErrorProvider.SetError(cboParticulars, MstrMessageCommon.Replace("#", "").Trim());
                ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                DeductionPolicyTimer.Enabled = true;
                cboParticulars.Focus();
               return false;
            }

            if (String.IsNullOrEmpty(CboAdditions.Text.Trim()) == true && rdbFixedAmount.Checked == false && rdbRateOnly.Checked==false) 
            {
                
                   MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 10073, out MsMessageBoxIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption , MessageBoxButtons.OK, MsMessageBoxIcon);
                DeductionPolicyErrorProvider.SetError(CboAdditions, MstrMessageCommon.Replace("#", "").Trim());
                ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                DeductionPolicyTimer.Enabled = true;
                CboAdditions.Focus();
               return false;
            }

            if (CboAdditions.SelectedIndex == -1 && rdbFixedAmount.Checked == false && rdbRateOnly.Checked==false)
            {
             
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 10074, out MsMessageBoxIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption , MessageBoxButtons.OK, MsMessageBoxIcon);
                DeductionPolicyErrorProvider.SetError(CboAdditions, MstrMessageCommon.Replace("#", "").Trim());
                ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                DeductionPolicyTimer.Enabled = true;
                CboAdditions.Focus();
               return false;
            }
            
            if(blnIsAddition == false) 
            {

                if (String.IsNullOrEmpty(CboParameter.Text) == true && rdbFixedAmount.Checked == false && rdbRateOnly.Checked == false) 
                {
                  
                            MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 10075, out MsMessageBoxIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption , MessageBoxButtons.OK, MsMessageBoxIcon);
                DeductionPolicyErrorProvider.SetError(CboParameter, MstrMessageCommon.Replace("#", "").Trim());
                ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                DeductionPolicyTimer.Enabled = true;
                CboParameter.Focus();
               return false;
                }
                if (CboParameter.SelectedIndex == -1 && rdbFixedAmount.Checked == false && rdbRateOnly.Checked == false)
                {

                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 10076, out MsMessageBoxIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption , MessageBoxButtons.OK, MsMessageBoxIcon);
                DeductionPolicyErrorProvider.SetError(CboParameter, MstrMessageCommon.Replace("#", "").Trim());
                ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                DeductionPolicyTimer.Enabled = true;
                CboParameter.Focus();
               return false;
                   
                }

                if (rdbFixedAmount.Checked == true)
                {
                    TxtAmount.Text = "0";
                }

                if ((TxtAmount.ReadOnly == false && TxtAmount.Text == "") && (rdbFixedAmount.Checked==false && rdbRateOnly.Checked==false) ) 
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 10077, out MsMessageBoxIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    DeductionPolicyErrorProvider.SetError(TxtAmount, MstrMessageCommon.Replace("#", "").Trim());
                    ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    DeductionPolicyTimer.Enabled = true;
                    TxtAmount.Focus();
                    return false;
                }
                if (rdbFixedAmount.Checked == true && (txtFixedAmount.Text == "" || txtFixedAmount.Text.Trim().ToDecimal() == 0))
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 10095, out MsMessageBoxIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    DeductionPolicyErrorProvider.SetError(txtFixedAmount, MstrMessageCommon.Replace("#", "").Trim());
                    ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    DeductionPolicyTimer.Enabled = true;
                    txtFixedAmount.Focus();
                    return false;
                }

                decimal decAmount  =0;

                if (TxtEmprPer.Text.ToDecimal() <= 0 && TxtEmpPer.Text.ToDecimal() <= 0)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    MessageBox.Show("من فضلك ادخل صاحب العمل / الموظف مساهمة", MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    //    DeductionPolicyErrorProvider.SetError(TxtEmpPer, "من فضلك ادخل صاحب العمل / الموظف مساهمة");
                    //}
                    //else
                    //{
                        MessageBox.Show("Please enter employer/employee Contribution", MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                        DeductionPolicyErrorProvider.SetError(TxtEmpPer, "Please enter employer/employee Contribution");
                    //}
                    TxtEmpPer.Focus();                    
                    return false;
                }

                bool blnParsed  = Decimal.TryParse(TxtEmprPer.Text.Trim(),out decAmount);

                if(blnParsed == true)
                {
                    if (decAmount >0 && (rdbParticulars.Checked == true || rdbFixedAmount.Checked == true)) 
                    {
                       if(decAmount > 100 || decAmount <= 0)
                        {
                            //if (ClsCommonSettings.IsArabicView)
                            //{
                            //    MessageBox.Show("يجب أن تكون نسبة ما بين 0 و 100", MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                            //    DeductionPolicyErrorProvider.SetError(TxtEmprPer, "يجب أن تكون نسبة ما بين 0 و 100");
                            //}
                            //else
                            //{
                                MessageBox.Show("Percentage should be in between 0 and 100", MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                                DeductionPolicyErrorProvider.SetError(TxtEmprPer, "Percentage should be in between 0 and 100");
                            //}

                            TxtEmprPer.Focus();                           
                           return false;

                        }
                    }
                }
           

               decimal decAmounta = 0;

                bool blnParseda = Decimal.TryParse(TxtEmpPer.Text.Trim(), out decAmounta);
                if(blnParseda == true)
                {
                    if (decAmounta > 0 && (rdbParticulars.Checked == true || rdbFixedAmount.Checked == true))
                   {
                        if(decAmounta > 100 || decAmounta <= 0) 
                       {
                           //if (ClsCommonSettings.IsArabicView)
                           //{
                           //    MessageBox.Show("يجب أن تكون نسبة ما بين 0 و 100", MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                           //    DeductionPolicyErrorProvider.SetError(TxtEmpPer, "يجب أن تكون نسبة ما بين 0 و 100");
                           //}
                           //else
                           //{
                               MessageBox.Show("Percentage should be in between 0 and 100", MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                               DeductionPolicyErrorProvider.SetError(TxtEmpPer, "Percentage should be in between 0 and 100");
                           //}

                            TxtEmpPer.Focus();
                            
                            return false;
                       }
                    }
                }
             
               for(int i = 0 ;i< dgvDeductionDetails.Rows.Count;i++)
               {
                   if(Convert.ToBoolean(dgvDeductionDetails.Rows[i].Cells["DSelectValue"].Value) == true)
                   {
                        bFlag = true;
                     
                   }
                }
            }
            
                
            
            if(rdbParticulars.Checked==true)
            {
                if(blnIsAddition == false && bFlag == false)
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 10078, out MsMessageBoxIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption , MessageBoxButtons.OK, MsMessageBoxIcon);
                       
                 return false;
                }
            }

          if(MblnAddStatus==true)
          {
              if (MobjclsBLLDeductionPolicy.CheckDuplicate(MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intDeductionPolicyId, Txtpolicyname.Text.Trim(), MblnAddStatus))
              {
                  MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 10083, out MsMessageBoxIcon);
                  MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                  DeductionPolicyErrorProvider.SetError(Txtpolicyname, MstrMessageCommon.Replace("#", "").Trim());
                  ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                  DeductionPolicyTimer.Enabled = true;
                  Txtpolicyname.Focus();
                  return false;
              }
          }
       
            if(MblnAddStatus==false)
          {

              if (MobjclsBLLDeductionPolicy.CheckDuplicate(Txtpolicyname.Tag.ToInt32(), Txtpolicyname.Text.Trim(), MblnAddStatus))
              {
                  MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 10083, out MsMessageBoxIcon);
                  MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                  DeductionPolicyErrorProvider.SetError(Txtpolicyname, MstrMessageCommon.Replace("#", "").Trim());
                  ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                  DeductionPolicyTimer.Enabled = true;
                  Txtpolicyname.Focus();
                  return false;
              }
            }

                

            if(TxtEmprPer.Text.Trim() == "" )
                TxtEmprPer.Text = "0";
            

            if(TxtEmpPer.Text.Trim() == "") 
                TxtEmpPer.Text = "0";

            if (TxtEmpPer.Text.Trim() == "")
                TxtEmpPer.Text = "0";

            return true;
            }

                       
        private void ProjectsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {

            Txtpolicyname.Focus();
            if (MblnChangeStatus == true)
            {
                if (MblnAddStatus == false && MblnUpdatePermission == false)
                {
                    //if (ClsCommonSettings.IsArabicView)
                    //{
                    //    MessageBox.Show("لم يكن لديك إذن ما يكفي لتنفيذ هذا الإجراء", MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    //}
                    //else
                    //{
                        MessageBox.Show("You do not have enough permission to perform this action", MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    //}
                    return ;
                }
                if (FormValidation() == false)
                    return ;

                if (SavePolicy())
                {

                }
            }
        }
           private void FillPolicyParameters()
           {
               if (MblnAddStatus == true)
               
                   MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intDeductionPolicyId = 0;
               
               else
               

                   MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intDeductionPolicyId = Convert.ToInt32(Txtpolicyname.Tag);

                   MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.strPolicyName = Txtpolicyname.Text.Replace("'", "").Trim();

                   MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intAdditionID = Convert.ToInt32(CboAdditions.SelectedValue);
                   MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intAdditionDeductionID = Convert.ToInt32(cboParticulars.SelectedValue);

                   if (blnIsAddition)
                   {
                       MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.dEmployerPart = 0.00M;
                       MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.dEmployerPart = Convert.ToDecimal(txtAdditionAmount.Text);
                       if (MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intAdditionID == 18)
                       {
                           MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intParameterId = -1;
                           MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.decAmountLimit = -1;
                       }
                       else
                       {
                           MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intParameterId = Convert.ToInt32(CboParameter.SelectedValue);
                           MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.decAmountLimit = Convert.ToDecimal(TxtAmount.Text);
                       }

                   }
                   else
                   {
                       MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.decAmountLimit = TxtAmount.Text.ToDecimal();
                       MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.dEmployeePart = TxtEmpPer.Text.ToDecimal();
                       MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.dEmployerPart = TxtEmprPer.Text.ToDecimal();
                       MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intParameterId = CboParameter.SelectedValue.ToInt32();
                       MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.decAmountLimit = TxtAmount.Text.ToDecimal();
                       MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.decFixedAmount = txtFixedAmount.Text.ToDecimal();
                   }


                   MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.strWithEffect = DTPWithEffect.Value.ToString("dd-MMM-yyyy");

                   if (rdbParticulars.Checked == true)
                       MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intRateOnly = 2;
                   else if (rdbRateOnly.Checked == true)
                       MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intRateOnly = 1;
                   else if (rdbFixedAmount.Checked == true)
                       MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intRateOnly = 3;

               
               FillPolicyDetailParameters();
           }
           private void FillPolicyDetailParameters()
           {
               MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.lstclsDTOdeductionPolicyDetails = new List<clsDTOdeductionPolicyDetails>();

               try
               {

                   for (int i = 0; i <dgvDeductionDetails.Rows.Count; i++)
                   {

                       if (dgvDeductionDetails["DSelectValue", i].Value != DBNull.Value)
                       {
                           if (Convert.ToBoolean(dgvDeductionDetails["DSelectValue", i].Value) == true)
                           {
                               clsDTOdeductionPolicyDetails objclsDTOdeductionPolicyDetails = new clsDTOdeductionPolicyDetails();



                             objclsDTOdeductionPolicyDetails.intDetAddDedID = Convert.ToInt32(dgvDeductionDetails["DAddDedID", i].Value);
                               MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.lstclsDTOdeductionPolicyDetails.Add(objclsDTOdeductionPolicyDetails);
                           }
                       }
                   }
               }
               catch (Exception Ex)
               {
                   MObjClsLogWriter.WriteLog("Error on FillPolicyDetailParameters " + this.Name + " " + Ex.Message.ToString(), 1);
               }
           }
         public void Clearcontrols()
         {
            Txtpolicyname.Tag = 0;
            Txtpolicyname.Text = "";
            TxtAmount.Text = "0";
            TxtEmpPer.Text = "0";
            TxtEmprPer.Text = "0";
            txtFixedAmount.Text = "";
           
            DTPWithEffect.Value = System.DateTime.Now.Date;

            TxtAmount.Text = String.Empty;
            txtAdditionAmount.Text = String.Empty;
            rdbRateOnly.Checked = false;
            rdbRateOnly.Enabled = true;
            CboParameter.Enabled = true;
            CboAdditions.Enabled = true;
            txtAdditionAmount.Enabled = true;
            TxtAmount.Enabled = true;
            cboParticulars.SelectedIndex = -1;
            CboAdditions.SelectedIndex = -1;
            CboParameter.SelectedIndex = -1;

            rdbParticulars.Checked = true;
            rdbRateOnly.Checked = false;
            rdbFixedAmount.Checked = false;

            LoadAdditionsList();
         }
           private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
           {
               AddNewDeductionPolicy();
           }
           private bool SavePolicy()
           {
 
               int iDeductionPolicyId=0;

               if (MblnAddStatus == true)//insert
               {
                   MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 1, out MsMessageBoxIcon);
               }
               else
               {
                   MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 3, out MsMessageBoxIcon);
               }

               if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                   return false;
               DeductionPolicyErrorProvider.Clear();
               FillPolicyParameters();
               MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intDeductionPolicyId = iDeductionPolicyId = MobjclsBLLDeductionPolicy.SavePolicy(MblnAddStatus);
              if (iDeductionPolicyId > 0)
              {
                  Txtpolicyname.Tag = iDeductionPolicyId;


                  if (MblnAddStatus)
                  {
                      MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 2, out MsMessageBoxIcon);
                  }
                  else
                  {
                      MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 21, out MsMessageBoxIcon);
                  }

                  MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                  ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                  DeductionPolicyTimer.Enabled = true;
                  MblnAddStatus = false;
                  MblnChangeStatus = false;
                  BtnOk.Enabled = MblnChangeStatus;
                  BtnSave.Enabled = MblnChangeStatus;
                  ProjectsBindingNavigatorSaveItem.Enabled = MblnChangeStatus;
                  BindingNavigatorAddNewItem.Enabled = true;
                  BtnCancel.Enabled = false;
                  BindingNavigatorDeleteItem.Enabled = MblnDeletePermission ;
                  BtnPrint.Enabled = true;
                  BtnEmail.Enabled = true;
                  
                  return true;
              }
              else
              {
                  return false;
              }

           }
        public void Changestatus()
        {
            DeductionPolicyErrorProvider.Clear();
            MblnChangeStatus = true;
           BtnOk.Enabled = MblnAddPermission;
           BtnSave.Enabled = MblnAddPermission;
           ProjectsBindingNavigatorSaveItem.Enabled = MblnAddPermission;
        }

        private void Txtpolicyname_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }
        private void GetRecordCount()
        {
            MintRecordCnt = 0;
            MintRecordCnt = MobjclsBLLDeductionPolicy.RecCountNavigate();
            if (MintRecordCnt < 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                MintRecordCnt = 0;
            }

        }
        private void AddNewDeductionPolicy()
        {
            MblnAddStatus = true;
            DeductionPolicyErrorProvider.Clear();
            //if (ClsCommonSettings.IsArabicView)
            //    ProjectCreationStatusLabel.Text = "إضافة السياسة خصم جديدة";
            //else
                ProjectCreationStatusLabel.Text = "Add new Deduction policy";

            DeductionPolicyTimer.Enabled = true;
          
         
            GetRecordCount();
            MintRecordCnt = MintRecordCnt + 1;
            MintCurrentRecCnt = MintRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorPositionItem.Text = Convert.ToString(MintRecordCnt);
            
            Clearcontrols();
            BindingNavigatorAddNewItem.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = false;
            BtnPrint.Enabled = false;
            BtnEmail.Enabled = false;
            BtnOk.Enabled = false;
            BtnSave.Enabled = false;
            ProjectsBindingNavigatorSaveItem.Enabled = false;
            BtnCancel.Enabled= BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = false;
            BindingNavigatorMoveNextItem.Enabled = false;
            MblnChangeStatus = false;
            
        }

        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled =BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
        }
        
        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        
        {
               MblnAddStatus = false;
               GetRecordCount();
               if(MintRecordCnt>0)
               {
                    MintCurrentRecCnt = MintCurrentRecCnt - 1;
                    if(MintCurrentRecCnt <= 0 )
                        MintCurrentRecCnt = 1;
                    DisplayDeductionPolicy(MintCurrentRecCnt);
               }
               else
               {
                    MintCurrentRecCnt = 0;
               }

               MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 10, out MsMessageBoxIcon);
               ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
               DeductionPolicyTimer.Enabled = true;              
        }

        private void DisplayDeductionPolicy(int MintCurrentRecCnt)
        {
            if(MobjclsBLLDeductionPolicy.Getpolicy(MintCurrentRecCnt))
            {
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BtnPrint.Enabled = MblnPrintEmailPermission;
                BtnEmail.Enabled = MblnPrintEmailPermission;
                BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";

                Txtpolicyname.Tag = MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intDeductionPolicyId;
                Txtpolicyname.Text = MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.strPolicyName;
                cboParticulars.SelectedValue = MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intAdditionDeductionID;
                CboParameter.SelectedValue =MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intParameterId;
                CboAdditions.SelectedValue = MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intAdditionID;
                TxtAmount.Text = MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.decAmountLimit.ToString();

                if(blnIsAddition) 
                {
                    if (MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intAdditionID == 18)
                    {
                       rdbRateOnly.Checked = false;
                       CboParameter.SelectedIndex = -1;
                        CboParameter.Enabled = false;
                       TxtAmount.Enabled = false;
                        rdbRateOnly.Enabled = false;
                        TxtAmount.Text = String.Empty;
                    }
                    else
                    {
                       rdbRateOnly.Checked = false;
                       CboParameter.Enabled =true;
                       TxtAmount.Enabled = true;
                       rdbRateOnly.Enabled = true;
                    }
                }            

                TxtEmpPer.Text = Convert.ToString(MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.dEmployeePart);
                TxtEmprPer.Text = Convert.ToString(MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.dEmployerPart);
                txtFixedAmount.Text = Convert.ToString(MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.decFixedAmount);
                if (MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intRateOnly == 2)
                {
                    rdbParticulars.Checked = true;
                    txtFixedAmount.Text = "";
                }
                else if (MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intRateOnly ==1)
                {
                    rdbRateOnly.Checked = true;
                    txtFixedAmount.Text = "";
                }
                else if (MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intRateOnly == 3)
                {
                    rdbFixedAmount.Checked = true;
                }
                if (MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.strWithEffect != null)
                {
                    DTPWithEffect.Value = Convert.ToDateTime(MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.strWithEffect);
                }
               txtAdditionAmount.Text = MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.dEmployerPart.ToString();

               //if (ClsCommonSettings.IsAmountRoundByZero)
               //{
               //    TxtEmpPer.Text = TxtEmpPer.Text.ToDecimal().ToString("F" + 0);
               //    TxtEmprPer.Text = TxtEmprPer.Text.ToDecimal().ToString("F" + 0);
               //    txtFixedAmount.Text = txtFixedAmount.Text.ToDecimal().ToString("F" + 0);
               //    txtAdditionAmount.Text = txtAdditionAmount.Text.ToDecimal().ToString("F" + 0);
               //}

                DataTable DtablePolicy =new DataTable();

               if(!blnIsAddition)
               {

                   DtablePolicy = MobjclsBLLDeductionPolicy.FillDeductiondetail(Convert.ToInt32(Txtpolicyname.Tag));

                    dgvDeductionDetails.DataSource = null;
                    dgvDeductionDetails.Rows.Clear();

                    if(DtablePolicy.Rows.Count > 0)
                    {
                        if(blnIsAddition == true) 
                        {}
                        else
                        {
                            for (int i=0;i<DtablePolicy.Rows.Count;i++)
                            {
                           
                                dgvDeductionDetails.RowCount = dgvDeductionDetails.RowCount + 1;
                                dgvDeductionDetails.Rows[i].Cells["DAddDedID"].Value = Convert.ToInt32(DtablePolicy.Rows[i]["AdditionDeductionID"]);
                                dgvDeductionDetails.Rows[i].Cells["DParticulars"].Value = Convert.ToString(DtablePolicy.Rows[i]["DescriptionStr"]);
                                dgvDeductionDetails.Rows[i].Cells["DSelectValue"].Value = Convert.ToBoolean(DtablePolicy.Rows[i]["Sel"]);
                            }
                        }
                  }
                   


                 
               
                Txtpolicyname.Focus();

                }

               SetBindingNavigatorButtons();
               MblnChangeStatus = false;
               ProjectsBindingNavigatorSaveItem.Enabled = MblnChangeStatus;
               BtnOk.Enabled = MblnChangeStatus;
               BtnSave.Enabled = MblnChangeStatus;
               BtnCancel.Enabled = false;
        }

    }

        private void rdbRateOnly_CheckedChanged(object sender, EventArgs e)
        {
            DeductFromChanged();
            ChangelabelText();
        }

        private void ChangelabelText()
        {
            if (rdbRateOnly.Checked == true)
            {
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    lblEmp.Text = "مساهمة الموظف";
                //    lblEmper.Text = "صاحب العمل مساهمة";
                //    lblAmount.Text = "بالإضافة إلى ذلك المبلغ";
                //}
                //else
                //{
                    lblEmp.Text = "Employee Contribution";
                    lblEmper.Text = "Employer Contribution";
                    lblAmount.Text = "Addition Amount";
                //}
            }
            else
            {
                //if (ClsCommonSettings.IsArabicView)
                //{
                //    lblEmp.Text = "مساهمة الموظف (%)";
                //    lblEmper.Text = "صاحب العمل مساهمة (%)";
                //    lblAmount.Text = "بالإضافة إلى ذلك المبلغ (%)";
                //}
                //else
                //{
                    lblEmp.Text = "Employee Contribution(%)";
                    lblEmper.Text = "Employer Contribution(%)";
                    lblAmount.Text = "Addition Amount(%)";
                //}
            }

        }
        private void DeductFromChanged()
        {
            Changestatus();
            if (rdbRateOnly.Checked == true || rdbFixedAmount.Checked == true)
            {
                dgvDeductionDetails.Enabled = false;


                for (int i = 0; i < dgvDeductionDetails.Rows.Count; i++)
                {
                    dgvDeductionDetails.Rows[i].Cells["DSelectValue"].Value = false;

                }
                //if (rdbFixedAmount.Checked == true)
                //{
                    TxtAmount.Enabled = false;
                    txtFixedAmount.Enabled = true;
                    txtFixedAmount.BackColor = Color.LightYellow;
                    TxtAmount.BackColor = Color.White;
                //}
                //else
                //{
                    //TxtAmount.Enabled = true;
                    //txtFixedAmount.Enabled = false;
                    //txtFixedAmount.BackColor = Color.White;
                    //TxtAmount.BackColor = Color.LightYellow;

                //}
            }
            else
            {
            
                dgvDeductionDetails.Enabled = true;
                txtFixedAmount.Enabled = false;
                TxtAmount.Enabled = true;
                txtFixedAmount.BackColor = Color.White;
                TxtAmount.BackColor = Color.LightYellow;

            }
            if (rdbParticulars.Checked == true || rdbRateOnly.Checked == true)
            {
                txtFixedAmount.Text = "";
            }
            else
            {
                TxtAmount.Text = "";
            }
            if (rdbFixedAmount.Checked == true || rdbRateOnly.Checked == true)
            {
                CboAdditions.SelectedIndex = -1;
                CboParameter.SelectedIndex = -1;
                TxtAmount.Text = "";
                gbSlab.Enabled = false;
            }
            else
            {
                gbSlab.Enabled = true;
            }
        }

        private void cboParticulars_SelectedIndexChanged(object sender, EventArgs e)
        {
           Changestatus();
       }

        private void TxtAmount_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            ProjectsBindingNavigatorSaveItem_Click(sender, e);
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            
              MblnAddStatus = false;
             GetRecordCount();
             if(MintRecordCnt>0)
              {
             MintCurrentRecCnt = MintCurrentRecCnt + 1;
             if(MintCurrentRecCnt >= MintRecordCnt)
             MintCurrentRecCnt = MintRecordCnt;
             DisplayDeductionPolicy(MintCurrentRecCnt);
             }
             else
             {
              MintCurrentRecCnt = 0;
             }

             MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 11, out MsMessageBoxIcon);
             ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
             DeductionPolicyTimer.Enabled = true;
           
        
        }
        private bool DeleteValidation()
        {           

               MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intDeductionPolicyId = Convert.ToInt32(Txtpolicyname.Tag);
                if (MobjclsBLLDeductionPolicy.IsExists())
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 9001, out MsMessageBoxIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Deduction Policy");


                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    DeductionPolicyTimer.Enabled = true;
                    return false;
                }

                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 13, out MsMessageBoxIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;
                else
                    return true;
            

            
        }
        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {

                if (Txtpolicyname.Tag.ToInt32() > 0 && DeleteValidation())
                {
                    MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intDeductionPolicyId = Convert.ToInt32(Txtpolicyname.Tag);
                    if (MobjclsBLLDeductionPolicy.Delete())
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 4, out MsMessageBoxIcon);
                        ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        DeductionPolicyTimer.Enabled = true;
                        AddNewDeductionPolicy();
                    }
                }
            }
            catch (SqlException Ex)
            {
                if (Ex.Number == (int)SqlErrorCodes.ForeignKeyErrorNumber)
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 9001, out MsMessageBoxIcon);
                    MstrMessageCommon = MstrMessageCommon.Replace("*", "Deduction Policy");
                }
                else
                    MstrMessageCommon = "Error on DeleteItem_Click() " + Ex.Message.ToString();

                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                DeductionPolicyTimer.Enabled = true;
              

            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on DeleteItem:DeleteItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteItem_Click() " + Ex.Message.ToString());
               
            }

        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();

            if (MintRecordCnt > 0)
            {
                MblnAddStatus = false;
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                MintCurrentRecCnt = MintRecordCnt;
                DisplayDeductionPolicy(MintCurrentRecCnt);
            }
            else
                MintRecordCnt = 0;

            MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 12, out MsMessageBoxIcon);
            ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            DeductionPolicyTimer.Enabled = true;



           
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();

            if (MintRecordCnt > 0)
            {
                MblnAddStatus = false;
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                MintCurrentRecCnt = 1;
                DisplayDeductionPolicy(MintCurrentRecCnt);
            }
            else
                MintRecordCnt = 0;

            MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 9, out MsMessageBoxIcon);
            ProjectCreationStatusLabel.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            DeductionPolicyTimer.Enabled = true;           
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            FrmReportviewer ObjViewer = new FrmReportviewer();
            ObjViewer.PsFormName = "DeductionPolicy";
            ObjViewer.PiRecId = MobjclsBLLDeductionPolicy.clsDTOdeductionPolicy.intDeductionPolicyId;
            ObjViewer.PiFormID = (int)FormID.DeductionPolicy;
            ObjViewer.ShowDialog();
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Deduction Policy";
                    ObjEmailPopUp.EmailFormType = EmailFormID.DeductionPolicy;

                    ObjEmailPopUp.EmailSource = MobjclsBLLDeductionPolicy.DisplayDeductionPolicy();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, Log.LogSeverity.Error);
            }
        }

        private void TxtEmpPer_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void TxtEmprPer_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            AddNewDeductionPolicy();
        }

        private void BtnBottomCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (FormValidation() == false)
                return;

            if (SavePolicy())
            {
                BtnSave.Enabled = false;
                this.Close();
            }
        }

        private void TxtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 46)
            {
                e.Handled = true;
            }
            else if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true;
            }
            if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            {
                e.Handled = true;
            }
        }


        private void dgvDeductionDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void txtEPSEmployer_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtEDLI_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtEPFAdmin_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtEDLIAdmin_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void TxtEmprPer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 46 )
            {
                e.Handled = true;
            }
            else if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true;
            }
            if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            {
                e.Handled = true;
            }
        }

        private void TxtEmpPer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 46)
            {
                e.Handled = true;
            }
            else if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true;
            }
            if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            {
                e.Handled = true;
            }
        }

        private void txtEPSEmployer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true;
            }
            if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            {
                e.Handled = true;
            }
        }

        private void txtEDLI_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true;
            }
            if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            {
                e.Handled = true;
            }
        }

        private void txtEPFAdmin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true;
            }
            if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            {
                e.Handled = true;
            }
        }

        private void txtEDLIAdmin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true;
            }
            if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            {
                e.Handled = true;
            }
        }

        private void CboAdditions_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void CboParameter_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void dgvDeductionDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                Changestatus();
            }
        }

        private void chkRateOnly_CheckStateChanged(object sender, EventArgs e)
        {

        }

        private void rdbParticulars_CheckedChanged(object sender, EventArgs e)
        {
            DeductFromChanged();
            ChangelabelText();
        }

        private void rdbFixedAmount_CheckedChanged(object sender, EventArgs e)
        {
            DeductFromChanged();
            ChangelabelText();
        }

        private void txtFixedAmount_TextChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void txtFixedAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true;
            }
            if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
            {
                e.Handled = true;
            }
        }

        private void DTPWithEffect_ValueChanged(object sender, EventArgs e)
        {
            Changestatus();
        }

        private void FrmDeductionPolicy_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (BtnSave.Enabled)
            {

                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MsarMessageArr, 8, out MsMessageBoxIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MsMessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        private void ToolStripbtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "SalaryStructure";
            objHelp.ShowDialog();
            objHelp = null;
        }
}
}
